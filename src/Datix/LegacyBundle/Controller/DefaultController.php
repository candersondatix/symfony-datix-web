<?php

namespace Datix\LegacyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * Defers all non-routed requests to the legacy appication.
     */
    public function indexAction()
    {
        // change working dir to route of legacy application
        chdir($this->get('kernel')->locateResource('@DatixLegacyBundle/Legacy'));
        
        // override error reporting
        error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_WARNING);
        
        // consume global config information
        require_once 'client/DATIXConfig.php';
        foreach (get_defined_vars() as $globalConfigKey => $globalConfigValue)
        {
            $GLOBALS[$globalConfigKey] = $globalConfigValue;
        }
        
        // defer to legacy app
        ob_start();
        require_once 'index.php';

        return new Response(ob_get_clean());
    }
}