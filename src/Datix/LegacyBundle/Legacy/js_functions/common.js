  /*
Global: txt
Used to hold those $txt values from php that we need to access from js
 */
if(!txt) {

  var txt = new Object();
}

/*
Global: globals
Used to hold global parameters from php that we need to access from js
 */
if(!globals) {

    var globals = new Object();
}

/**
 * Used to store qTip objects
 * @type {Object}
 */
var qTips = new Object();

/**
 * Used to store spellcheck objects
 * @type {Object}
 */
var spellcheck = new Object();

/**
 * Used to fix the buttons being used for the spellcheck when activated in jQuery
 * @type {null}
 */
var spellcheckButton = null;


 /**
 * on-the-spot JS date validation.
 *
 * @param obj element: The HTML element containing the date to be checked
 *
 * @returns bool: returns true if input is valid and false otherwise.
 */
function isDate(element)
{
    var datePat = /^(\d{1,2})([\-/\.])(\d{1,2})([\-/\.])(\d{2,4})$/;

    if(jQuery(element).attr('type') == 'date' && Modernizr.inputtypes.date) {

        datePat = /^(\d{2,4})(?:[\-])(\d{1,2})(?:[\-])(\d{1,2})$/;
    }
    else if(globals.dateFormatCode == 'US') {

        datePat = /^(\d{1,2})([\-/.])(\d{1,2})([\-/.])(\d{2,4})$/;
    }

    var matchArray = element.value.match(datePat); // is the format ok?

    if (element.value != '')
    {

        if (matchArray == null)
        {
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if(jQuery(element).attr('type') == 'date' && Modernizr.inputtypes.date) {

            day = matchArray[3];
            month = matchArray[2];
            year = matchArray[1];
        }
        else if(globals.dateFormatCode == 'US') {

            day = matchArray[3];
            month = matchArray[1];
            year = matchArray[5];
        }
        else {

            day = matchArray[1];
            month = matchArray[3];
            year = matchArray[5];
        }

        if (year.length != 4 || year < 1753 /*SQL Server 2005 limitation*/)
        {
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if (month < 1 || month > 12)
        {
            // check month range
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if (day < 1 || day > 31)
        {
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if ((month==4 || month==6 || month==9 || month==11) && day==31)
        {
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if (month == 2)
        {
            // check for february 29th
            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));

            if (day > 29 || (day==29 && !isleap))
            {
                alertWithNoSubmit ('You did not enter a valid date', element);
                element.value = '';
                element.focus();
                return false;
            }
        }

        if (day.length == 1)
        {
            day = '0' + day;
        }

        if (month.length == 1)
        {
            month = '0' + month;
        }

        if(jQuery(element).attr('type') == 'date' &&  ! Modernizr.inputtypes.date) {

            if(globals.dateFormatCode == 'US') {

                element.value = month + '/' + day + '/' + year;
            }
            else {

                element.value = day + '/' + month + '/' + year;
            }
        }
    }

    return true; // date is valid
}

 /**
 * on-the-spot JS date validation for US-style dates.
 *
 * @param obj element: The HTML element containing the date to be checked
 *
 * @returns bool: returns true if input is valid and false otherwise.
 */
function isAmericanDate(element)
{

    var datePat = /^(\d{1,2})([\-/.])(\d{1,2})([\-/.])(\d{2,4})$/;
    var matchArray = element.value.match(datePat); // is the format ok?

    if (element.value != '')
    {

        if (matchArray == null)
        {
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        day = matchArray[3];
        month = matchArray[1];
        year = matchArray[5];

        if (year.length != 4)
        {
           alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if (month < 1 || month > 12) { // check month range
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if (day < 1 || day > 31)
        {
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if ((month==4 || month==6 || month==9 || month==11) && day==31)
        {
            alertWithNoSubmit ('You did not enter a valid date', element);
            element.value = '';
            element.focus();
            return false;
        }

        if (month == 2)
        {
            // check for february 29th
            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));

            if (day > 29 || (day==29 && !isleap))
            {
                alertWithNoSubmit ('You did not enter a valid date', element);
                element.value = '';
                element.focus();
                return false;
            }
        }

        if (day.length == 1)
        {
            day = '0' + day;
        }

        if (month.length == 1)
        {
            month = '0' + month;
        }

        element.value = month + '/' + day + '/' + year;
    }

    return true; // date is valid
}

 /**
 * on-the-spot JS time validation.
 *
 * @param obj element: The HTML element containing the time to be checked
 *
 * @returns bool: returns true if input is valid and false otherwise.
 */
function isTime(element)
{
    var datePat = /^([012]?\d)([:.]){0,1}(\d{2})$/;
    var matchArray = element.value.match(datePat); // is the format ok?

    if (element.value != '')
    {
        if (matchArray == null)
        {
            alertWithNoSubmit ('You did not enter a valid time', element);
	        element.value = '';
	        element.focus();
            return false;
        }

        hours = matchArray[1];
        minutes = matchArray[3];

        if (hours < 0 || hours > 23)
        {
            alertWithNoSubmit ('You did not enter a valid time', element);
	        element.value = '';
	        element.focus();
            return false;
        }

        if (minutes < 0 || minutes > 59)
        {
            alertWithNoSubmit ('You did not enter a valid time', element);
	        element.value = '';
	        element.focus();
            return false;
        }

        if (hours.length == 1)
        {
            hours = "0" + hours;
        }

        element.value = hours + ":" + minutes
    }

    return true; // time is valid
}

  /**
   * on-the-spot validation for number fields
   *
   * @param obj element: The HTML element containing the number to be checked
   *
   * @returns bool: returns true if input is valid and false otherwise.
   */
  function CheckNumber(elementID)
  {
      //remove whitespace
      var numberField = jQuery('#' + elementID);
      var numberValue = numberField.val().replace(/ /g,"");

      //check to see its numeric
      if (numberValue != '' && !jQuery.isNumeric(numberValue))
      {
          alertWithNoSubmit('You did not enter a valid number');
          numberField.focus();
          return false;
      }

      return true;
  }


  /**
   * On-the-spot validation for money fields
   *
   * @param string  fieldValue:          Value being evaluated as containing a valid representation of a monetary value
   * @param boolean allowSearchSymbols: Money fields can contain various symbols used in a search, which are not
   *                                    allowed when saving the actual value
   *
   * @returns bool: returns true if input is valid and false otherwise.
   */
  function CheckMoney(fieldElement, allowSearchSymbols)
  {
      allowSearchSymbols = setDefaultParameter(allowSearchSymbols , false);
	  var fieldValue = fieldElement.val()
      var currencyChar = globals.currencyChar;	  
	  
      //remove symbols/characters/strings that are allowed by Datix to be searched on in money fields
      if(allowSearchSymbols) {
          //check the allowed Search symbols are being used correctly (e.g. 1 or 2 equal symbols are allowed, 3 together are not)
          //Note: this is not meant to subtract from the string in preparation for the isNumeric test
		  //Strings that should be allowed are '*', '=', '==', '!=', '>', '>=', '<', '<=', ':'
		  //Examples of strings that are NOT allowed include '**', '===', '>>', '>>=', '<<', '<<=', '::', ect. 
          if(new RegExp(/\*{2,}|={3,}|!{2,}|>{2,}|<{2,}|:{2,}|>[^>=]+=|<[^<=]+=|![^!=]+=|=[^=]+=/g).test(fieldValue))
          {
              notifyInvalidValue(fieldElement);
              return false;
          }

          //Remove any allowed search symbols in preparation for the isNumeric test
          fieldValue = fieldValue.replace(/ |\*|=|!|>|<|:/g, "");
      }

      //Remove allowed special characters (such as currency characters, decimal point, comma and negative sign)
      var fieldValue = fieldValue.replace(new RegExp("\\.|-|,|\\"+currencyChar, ['g']),"");

      if (fieldValue != '' && !jQuery.isNumeric(fieldValue))
      {
          notifyInvalidValue(fieldElement);
          return false;
      }

      return true;
  }

  /**
   * Focus on an element that has failed validation, after dispalying an error message
   *
   * @param JQuery Object fieldElement focus this element after displaying message
   *
   * @returns {boolean}
   */
  function notifyInvalidValue(fieldElement)
  {
      alertWithNoSubmit('You did not enter a valid value');
      fieldElement.focus();
      return false;
  }

 /**
 * Toggle function on checkboxes to show and hide an HTML element
 *
 * @param string whichEl: The id of the HTML element to be toggled
 * @param obj fld: The HTML checkbox element that prompts the change
 */
function expandIt(whichEl, fld)
{
    try
    {
        if (fld.checked)
        {
            document.getElementById(whichEl).style.display = '';

            if ($('show_section_'+whichEl) && fld.id != 'show_section_'+whichEl)
            {
                $('show_section_'+whichEl).value = "1";
            }
        }
        else
        {
            try  //because we don't know if the function exists
            {
                eval('var sectionshow = CheckSectionShow_'+whichEl+'();');
            }
            catch(e) {}

            if (!sectionshow)
            {
                document.getElementById(whichEl).style.display = 'none';

                if ($('show_section_'+whichEl) && fld.id != 'show_section_'+whichEl)
                {
                    $('show_section_'+whichEl).value = "0";
                }
            }
        }

        initialiseSpellchecker();
    }
    catch(e) {}
}

 /**
 * Toggle function on yes/no fields to show and hide an HTML element (only used for non-popup dropdowns)
 *
 * @param string whichEl: The id of the HTML element to be toggled
 * @param obj fld: The yes/no control that prompts the change
 */
function expandItYN(whichEl, fld)
{
    try
    {
        if (fld.value == "Y")
        {
            document.getElementById(whichEl).style.display = '';

            if ($('show_section_'+whichEl) && fld.id != 'show_section_'+whichEl)
            {
                $('show_section_'+whichEl).value = "1";
            }
        }
        else
        {
            try  //because we don't know if the function exists
            {
                eval('var sectionshow = CheckSectionShow_'+whichEl+'();');
            }
            catch(e) {}

            if (!sectionshow)
            {
                document.getElementById(whichEl).style.display = 'none';

                if ($('show_section_'+whichEl) && fld.id != 'show_section_'+whichEl)
                {
                    $('show_section_'+whichEl).value = "0";
                }
            }
        }
    }
    catch(e) {}
}

/*
Variable: GlobalChangeTracker
False on pageload - changed to true when a field is changed (in setChanged()), to trigger a message if the user tries to leave the form
*/
var GlobalChangeTracker = false;
/*
Variable: AlertAfterChange
set to true on pageload if the page is one that prevents the user from navigating away.
*/
var AlertAfterChange = false;

 /**
 * Called when a user tries to navigate away from a form - checks whether they have any changes to save
 *
 * @returns bool: true if the user is allowed to navigate away, false if not.
 */
function CheckChange()
{
    if (GlobalChangeTracker && AlertAfterChange)
    {
        alert("You must save or cancel the changes you have made to this record before you can access this feature.");
        return false;
    }
    else
    {
        return true;
    }
}

function CheckMax(element, maxNum)
{
    var value = unformatCurrency(element.value);

    if (jQuery.isNumeric(value) && value > maxNum)  // check is not letters and not just spaces
    {
        alertWithNoSubmit("The maximum value allowed is " + maxNum, element);
        element.focus();
        return false;
    }

    return true;
}

function CheckMin(element, minNum)
{
    var value = unformatCurrency(element.value);

    if (jQuery.isNumeric(value) && value < minNum)  // check is not letters and not just spaces
    {
        alertWithNoSubmit("The minimum value allowed is " + minNum, element);
        element.focus();
        return false;
    }

    return true;
}

 /**
 * called when a field changes - sets a hidden field to use for auditing and the GlobalChangeTracker flag
 * to prevent people from navigating away and losing their changes.
 *
 * @param string fldName: The id of the field that has been changed.
 *
 * @returns true.
 */
function setChanged(fldName)
{
	var elem = document.getElementById('CHANGED-' + fldName);

    if (elem)
    {
		elem.value = "1";
	}

    GlobalChangeTracker = true;

	return true;
}

//keeps track of rico grids on the page.
var ricoGrids = new Array();

 /**
 * called when a user clicks on a panel menu option - shows the selected panel and hides the current one.
 *
 * @param string panel: The id of the new panel.
 */
function showFormPanel(panel)
{
    jQuery('.panel').hide();
    jQuery('#'+panel).show();

    // if the form panel input exists set its value to the current panel name
    var panelInput = jQuery('#form-panel');
    if(panelInput.length) {

        // remove "panel-" from the panel value so that it can be used in the url properly
        panelPattern = /^panel\-(.+)$/i;
        panelName = panelPattern.exec(panel);

        panelInput.val(panelName[1]);
    }

    // resize dropdowns when panel is displayed
    formWidth = null;
    jQuery('#'+panel).find('input[class~=ff_select]').each(function()
    {
        var $target = jQuery(this);

        if($target.length > 0 && jQuery.isFunction($target.resizeInput)) {

            $target.resizeInput();
        }
    });

    // resize multiselect lists when panel is displayed
    jQuery('#'+panel).find('input[class~=multi_select]').each(function()
    {
        var $target = jQuery(this);

        if($target.length > 0 && jQuery.isFunction($target.resizeValuesList)) {

            $target.resizeValuesList();
        }
    });

    //need to make sure rico grids are resized when changing panels, since otherwise they behave weirdly and have no size.
    if (ricoGrids)
    {
        for (var i = 0; i < ricoGrids.length; i++)
        {
            if (ricoGrids[i]['grid'])
            {
                ricoGrids[i]['grid'].sizeDivs();
                ricoGrids[i]['grid'].updateHeightDiv();
            }
        }
    }

    initialiseSpellchecker();
}

function moveScreenToField(fieldName, divName)
{
    if (!fieldName)
    {
        return false;
    }

    var fld = $(fieldName);

    if (divName)
    {
        var showdiv = "panel-"+divName;
    }

    if (fld)
    {
        if($(showdiv))
        {
            showFormPanel(showdiv);
        }

        fld.scrollTo();
    }
}

function validateEmpty(fldValue, errfld, div, errs)
{
	var elem = document.getElementById(errfld);

    if (div)
    {
        var divelem = document.getElementById(div);
    }

	if (fldValue == undefined || fldValue == "")
    {
        if (elem && ( document.getElementById(errfld.replace('err','')) || document.getElementsByName(errfld.replace('err','')).length > 0 ) )
        {
            elem.style.display = 'block';
        }

        return false;
	}
	else
    {
        if (elem)
        {
            elem.style.display = 'none';
        }

        if (document.getElementById('errinc_result') && document.getElementById('inc_result') && document.getElementById('inc_result').value != "")
        {
            document.getElementById('errinc_result').style.display = 'none';
        }

        return true;
	}

	return false;
}

function alertWithNoSubmit(message, field)
{
    var originalOnSubmit = document.forms[0].onsubmit;
    document.forms[0].onsubmit = new Function("",'return false;');
    OldAlert(message);

    document.forms[0].onsubmit = originalOnSubmit;
}

function setIsetImagemage(whichEl, newimage)
{
    document.getElementById(whichEl).src=newimage;
}

/*
 * Used for rendering form sections via AJAX. Used for for actions in INC & COM as well as
 * for loading all sections in form design (except in the case of expand all)
 *
 * @param   Obj         The html id of the header of the section to be rendered
 * @param   data        json encoded array of data used in the rendering of a section
 * @param   callback    function called after this function completes (if provided)
 */
function renderFields(Obj, data, callback)
{
    if(typeof callback === 'undefined')
    {
       callback = null;
    }

    var $trigger = jQuery(Obj),
        $group = $trigger.closest('.toggle-group'),
        $target = $group.find('.toggle-target'),
        $loading = $group.find('.toggle-loading');

    if (!$target.data('rendered'))
    {
        if (!$loading.length)
        {
            $loading = jQuery('<div/>', {
                'class': 'toggle-loading',
                'text': 'Please wait, loading section content...'

            }).css({
                'display': 'none'
            });

            $target.before($loading);
        }

        if(data['section_name'].match('contact') && checkForMaxSuffix())
        {
            data['maxSuffixExists'] = true;
        }

        $loading.slideDown(function(){

            jQuery.ajax({
                url: scripturl+"?action=httprequest&type=renderfields",
                type: "POST",
                async: true,
                data: data
            }).done(function(html) {

                $target.html(html).data('rendered', true);
                $trigger.data('render', null);
                $loading.slideUp(function(){$loading.hide();});

                //Initialise calendars and other field functions
                initialiseCalendars();

                $target.find('textarea').limitTextareaChars();

                if (typeof callback === 'function')
                {
                    callback();
                }
            });
        });
    }
}

/*
 * Finds out if there is a max_suffix html element on the page, used for deciding the suffix
 * of new contacts added onto a form. Used by the renderFields method.
 *
 * @return  bool    true if there is a max_suffix element already on the page, else false.
 */
function checkForMaxSuffix()
{
    return jQuery('#max_suffix').length > 0;
}

function ToggleFormDesignSection(SectionName)
{
	var opend,
        $sectionTitleRow = jQuery('#'+SectionName+'_title_row'),
	    table = $sectionTitleRow.find('> table');
	/* IE6 and IE7 hack */ // TODO - remove this as I'm sure it's not necessery, that's what jQuery is for
    if( jQuery.browser.msie && ( jQuery.browser.version == 7 || jQuery.browser.version == 6 ) && table.length > 0 )
    {
    	table.toggle();
    	opend = table.first().is(":visible");
    }
    else
    {
        $sectionTitleRow.siblings().toggle();
    	opend = $sectionTitleRow.siblings('table,li').first().is(":visible");
    }

    if ( opend )
    {
    	jQuery('#twisty_image_'+SectionName).attr('src','Images/collapse.gif');
        jQuery("#SECTIONEXPANDED-"+SectionName+"-"+SectionName).val("1");
    }
    else 
    {
    	jQuery('#twisty_image_'+SectionName).attr('src','Images/expand.gif');
        jQuery("#SECTIONEXPANDED-"+SectionName+"-"+SectionName).val("");
    }
}

function ToggleTwistyExpand(whichEl, ImageId, ImageExpand, ImageCollapse)
{
    if (document.getElementById(whichEl).style.display != "none")
    {
        document.getElementById(whichEl).style.display = "none";

        if (ImageExpand)
        {
            setIsetImagemage(ImageId, ImageExpand);
        }
    }
    else
    {
        document.getElementById(whichEl).style.display = "";

        if (ImageCollapse)
        {
            setIsetImagemage(ImageId, ImageCollapse);
        }
    }
}

function getAbsenceDays(df, suffix)
{
    var suffixString = suffix ? '_'+suffix : '';

    if ($('link_daysaway'+suffixString))
    {
        var abs_start = document.getElementById('link_abs_start'+suffixString).value;
        var abs_end = document.getElementById('link_abs_end'+suffixString).value;
        var daysaway = document.getElementById('link_daysaway'+suffixString).value;
        var difference;
        var dateformat = df;

        if (abs_start && abs_end)
        {
            difference = days_between(abs_start, abs_end, dateformat);

            if (difference !== false)
            {
                $('link_daysaway'+suffixString).value = difference;
            }
        }
    }
}

function days_between(date_start, date_end, dateformat)
{
    // isDate validation was successful
    ds_Y = date_start.substring(6,10);

    if (dateformat == 'US')
    {
        ds_M = date_start.substring(0,2);
        ds_d = date_start.substring(3,5);
    }
    else
    {
        ds_M = date_start.substring(3,5);
        ds_d = date_start.substring(0,2);
    }

    ds_M = ds_M - 1;
    date1 = new Date(ds_Y, ds_M, ds_d);

    ds_Y = date_end.substring(6, 10);

    if (dateformat == 'US')
    {
        ds_M = date_end.substring(0,2);
        ds_d = date_end.substring(3,5);
    }
    else
    {
        ds_M = date_end.substring(3,5);
        ds_d = date_end.substring(0,2);
    }

    ds_M = ds_M - 1;
    date2 = new Date(ds_Y, ds_M, ds_d);

    if (date2 < date1)
    {
        return false;
    }

    var one_day = 1000 * 60 * 60 * 24;

    var difference_abs_ms = Math.abs(date2.getTime() - date1.getTime());
    var difference = Math.round(difference_abs_ms/one_day);

    return  difference;
}

/* addUDFSearch adds a new row to a search form, containing search
 * boxes of an appropriate type with which to search for the udf with
 * field id = id. mod holds the current module code.
 */
var sAjaxReturn;

function addUDFSearch(sListId, aParams)
{
    var mod = aParams["mod"];

    var id = $(sListId).options[$(sListId).selectedIndex].value;

    new Ajax.Request
    (
        scripturl+"?action=httprequest&type=addudfsearchrow&id="+id+"&mod="+mod,
        {
            method:'get',
            asynchronous:false,
            onSuccess: function(r)
            {
                var HTML = SplitJSAndHTML(r.responseText.toString());
                sAjaxReturn = '<div class="fmd_field_div">'+HTML+'</div>';
                RunGlobalJavascript();
            }
        }
    );

    //adds the added row's id to the list of ids not shown when the form is reloaded after error.
    htmlField = document.getElementById('udfRowArray');
    htmlField.value+= id + '|';

    // remove option from UDF dropdown list
    var udfdropdown = document.getElementById('UDFSearchSelect');

    if (udfdropdown.options.length == 1)   // if udf dropdown is empty, replace the row with a title bar.
    {
        $('UDFTitleRow').innerHTML = '<td class="titlebg" colspan="2"><b>Extra Fields</b></td>';
    }

    return sAjaxReturn;
}

/* addUDFType returns the HTML for a form input box with which to search for a UDF.
 *
 * id holds the field id of the udf
 * mod holds the current module code.
 * type holds the type of field
 */
function addUDFType(id, mod, type)
{
    var name = "UDF_"+id+"_"+type;

    switch(type)
    {
        case "D": // date field
            // CB 2010-11-15: this code references a calendar library that has since been stripped out (calendar.php),
            // but since this function is never called it shouldn't be too much of a problem.  Be wary of this if this function is ever reused...
            return '<input type="text"  name="'+name+'" id="'+name+'" size="15" /><img class="cal" src="Images/calendar.gif" border="0" alt="Calendar" style="cursor:pointer" onclick="popupwin=window.open(\'calendar.php?field='+name+'\', \'cal\', \'dependent=yes, width=190, height=250, screenX=200, screenY=300, titlebar=yes\');popupwin.focus();setChanged(\''+name+'\')" /><input type="hidden" name="CHANGED-'+name+'" id="CHANGED-'+name+'" />';
            break;
        case "M": // money
            return '<input type="text" name="'+name+'" id="'+name+'" size="10" value="" onchange="setChanged(\''+name+'\')" /><input type="hidden" name="CHANGED-'+name+'" id="CHANGED-'+name+'" />'
            break;
        case "N": // number
            return '<input type="text" maxlength="32" size="15" name="'+name+'" value="" onchange="setChanged(\''+name+'\')" /><input type="hidden" name="CHANGED-'+name+'" id="CHANGED-'+name+'" />'
            break;
        case "S": // text field
            return '<input type="text" size="15" name="'+name+'" value="" onchange="setChanged(\''+name+'\')" /><input type="hidden" name="CHANGED-'+name+'" id="CHANGED-'+name+'" />'
            break;
        case "Y": // yes/no field
            return '<select name="'+name+'" value="" onchange="setChanged(\''+name+'\')" /><option value="">Choose</option><option value="Y">Yes</option><option value="N">No</option></select><input type="hidden" name="CHANGED-'+name+'" id="CHANGED-'+name+'" />'
            break;
        default:  // dropdown etc.
            return '<input type="text" name="'+name+'" id="'+name+'" style="width: 200px" /><img src="Images/DATIX_Dropdown_n.gif" border="0" width="16" height="16" valign="middle" alt="codes" style="cursor:pointer" onclick="window.open(\'?action=codelist&amp;module='+mod+'&amp;type='+type+'&amp;field='+id+'&amp;search=1&amp;udf=1&amp;value=\' + escape(document.getElementById(\''+name+'\').value), \'wndCodeList\', \'dependent,menubar=false,screenX=150,screenY=150,width=400,height=400,titlebar,scrollbars,resizable\')" onmouseover="this.src=\'Images/DATIX_Dropdown_n.gif\'" onmouseout="this.src=\'Images/CodeList16n.gif\'"/><input type="hidden" name="CHANGED-'+name+'" id="CHANGED-'+name+'" />';
    }
}
/*
 * Validates a list of comma seperated email addresses
 *
 * @param   element     The text field that is to be validated.
 * @param   noAlert     True if there is to be no alert box, else false.
 * @return  bool        True if the list of email addresses provided is valid, else false.
 */
function isEmail(element, noAlert)
{
    // We need to mantain element object intact instead of a jQuery object because function alertWithNoSubmit
    // uses standard JavaScript objects
    var newelement = jQuery(element);

    newelement.val(newelement.val().replace(/ /g, '')); //get rid of spaces.

    if (newelement.val() != '')
    {
        var exploded = newelement.val().split(",");
        //TODO - this pattern doesn't conform to RFC 5322 so may not be accurate enough and could fail on genuine emails
        var emailPat = /^(([a-zA-Z0-9\'_-]+(\.))*[a-zA-Z0-9\&\'_-]+|[a-zA-Z0-9\&\'_-]+)@([a-zA-Z0-9_-]{1,63}\.([a-zA-Z0-9_-]+(\.))*[a-zA-Z0-9]+)$/;
        var send = true;

        for (var i = 0; i < exploded.length; i++)
        {
            var matchArray = exploded[i].match(emailPat); // is the format ok?

            if (matchArray == null)
            {
                send = false;
            }
        }

        if (!send)
        {
            if (!noAlert)
            {
                alertWithNoSubmit('One of the email addresses you entered was not of the correct form.\nIf you are trying to enter multiple addresses, please check you have separated them with a comma', element);
            }

            setTimeout(function()
            {
                newelement.select();
            }, (globals.deviceDetect.isTablet ? 500 : 100));

            return false;
        }

        return true;
    }
}
/*
 * Validates a single email address in a text field.
 *
 * @param   element     The text field that is to be validated.
 * @param   noAlert     True if there is to be no alert box, else false.
 * @return  bool        True if the email address provided is valid, else false.
 */
function isSingleEmail(element, noAlert)
{
    element.value = element.value.replace(/ /g,""); //get rid of spaces.

    if (element.value != '')
    {
        var emailPat = /^(([a-zA-Z0-9\'_-]+(\.))*[a-zA-Z0-9\'_-]+|[a-zA-Z0-9\'_-]+)@([a-zA-Z0-9_-]{1,63}\.([a-zA-Z0-9_-]+(\.))*[a-zA-Z0-9]+)$/;
        var send = true;
        var matchArray = element.value.match(emailPat); // is the format ok?

        if (matchArray == null)
        {
            send = false;
        }

        if (!send)
        {
            if (!noAlert)
            {
                alertWithNoSubmit('The email address you entered was not of the correct form.', element);
            }

            setTimeout(function()
            {
                element.select();
            }, (globals.deviceDetect.isTablet ? 500 : 100));

            return false;
        }

        return true;
    }
}

// Using this function for debugging JavaScript instead of alert()
// or printf() could be helpful.
// Calling log(element.value); prints the value into a separate window.
// Don't forget to remove log() calls from the source after debugging.
// TODO is this really neccessary anymore? can it be removed?
function log(message)
{
    if (!log.window_ || log.window_.closed)
    {
        var win = window.open("", null, "width=400,height=200," +
                              "scrollbars=yes,resizable=yes,status=no," +
                              "location=no,menubar=no,toolbar=no");
        if (!win)
        {
            return;
        }

        var doc = win.document;
        doc.write("<html><head><title>Debug Log</title></head>" +
                  "<body></body></html>");
        doc.close();
        log.window_ = win;
    }

    var logLine = log.window_.document.createElement("div");
    logLine.appendChild(log.window_.document.createTextNode(message));
    log.window_.document.body.appendChild(logLine);
}

var currentPerson = 0;

function setClass(obj, classname)
{
    if (browser.isIE && browser.version <= 7)
    {
        // IE7 and below
        obj.setAttribute("className",classname);
    }
    else
    {
        obj.setAttribute("class",classname);
    }
}

function insertInjuryRow(params)
{
    //load any extra js into global
    var TextToPrint = SplitJSAndHTML(params['js']),
        personSuffix = params['person_suffix'] ? '_'+params['person_suffix'] : '',
        injury_table = document.getElementById("injury_table"+personSuffix), numrows = injury_table.rows.length,
        newrow = injury_table.insertRow(numrows-1),
        cell1 = newrow.insertCell(0),
        cell2 = newrow.insertCell(1);

    cell1.innerHTML = SplitJSAndHTML(params['cells'][0]);
    cell2.innerHTML = SplitJSAndHTML(params['cells'][1]);

    if (params['cells'][2] != undefined)
    {
        // we may only be returning two cells if one has been hidden
        var cell3 = newrow.insertCell(2)
        cell3.innerHTML = SplitJSAndHTML(params['cells'][2]);
    }

    // set style attributes for the new row
    setClass(newrow, "windowbg2");

    newrow.setAttribute("id",'injury_row'+personSuffix+'_'+params['row_suffix']);

    var retain_combo_children = 'true';

    if ($('RETAIN_COMBO_CHILDREN').value != 1)
    {
        retain_combo_children = 'false';
    }

    TempJS = '\r\n function OnChange_injury'+personSuffix+'_'+params['row_suffix']+'() {jQuery(\'#bodypart'+personSuffix+'_'+params['row_suffix']+'_title\').checkClearChildField('+retain_combo_children+');}';

    JavascriptToExecute.push(TempJS);

    RunGlobalJavascript();
}

/**
 * Inserts new row into property table on contact form.
 *
 * @param array params Array of parameters (from json).
 * @param array params[cells] Array of html cell data
 * @param string params[person_suffix] contact suffix number
 * @param string params[row_suffix] property row suffix number
 * @param string params[js] additional js to add to the page header
 */
function insertPropertyRow(params)
{
    var TextToPrint = SplitJSAndHTML(params['js']); //load any extra js into global
    var personSuffix = params['person_suffix'] ? '_'+params['person_suffix'] : '';

    var injury_table = document.getElementById("property_table"+personSuffix);

    var numrows = injury_table.rows.length;
    var newrow = injury_table.insertRow(numrows-1)

    var cell1 = newrow.insertCell(0);
    cell1.innerHTML = SplitJSAndHTML(params['cells'][0]);

    var cell2 = newrow.insertCell(1)
    cell2.innerHTML = SplitJSAndHTML(params['cells'][1]);

    var cell3 = newrow.insertCell(2)
    cell3.innerHTML = SplitJSAndHTML(params['cells'][2]);

    if (params['cells'][3] != undefined)
    {
        // we may only be returning two cells if one has been hidden
        var cell4 = newrow.insertCell(3)
        cell4.innerHTML = SplitJSAndHTML(params['cells'][3]);
    }

    // set style attributes for the new row
    setClass(newrow, "windowbg2 property_row");

    newrow.setAttribute("id",'property_row'+personSuffix+'_'+params['row_suffix']);

    RunGlobalJavascript();
}

function deleteInjuryRow(row_suffix, person_suffix)
{
    var personSuffix = (person_suffix != '' && person_suffix != 0) ? '_'+person_suffix : '';
    var injury_row = $('injury_row'+personSuffix+'_'+row_suffix).rowIndex;

    if (injury_row)
    {
        document.getElementById('injury_table'+personSuffix).deleteRow(injury_row);
    }
}

/**
 * Deletes the specified property table row from the contact form.
 *
 * @param string person_suffix contact suffix number
 * @param string row_suffix property row suffix number
 */
function deletePropertyRow(row_suffix, person_suffix)
{
    var personSuffix = (person_suffix != '' && person_suffix != 0) ? '_'+person_suffix : '';
    var property_row = $('property_row'+personSuffix+'_'+row_suffix).rowIndex;

    if (property_row)
    {
        document.getElementById('property_table'+personSuffix).deleteRow(property_row);
    }
}

var dif1_section_suffix = new Object;

function writeSuffixLimits()
{
    for (i in dif1_section_suffix)
    {
        if ($(i+'_max_suffix'))
        {
            $(i+'_max_suffix').value = dif1_section_suffix[i];
        }
        else
        {
            var hidden = document.createElement("input");
            hidden.type = "hidden";
            hidden.value = dif1_section_suffix[i];
            hidden.name = i+'_max_suffix';
            hidden.id = i+'_max_suffix';
            document.forms[0].appendChild(hidden);
        }
    }
}

function isVisible(oElement)
{
    while (oElement && oElement.tagName != 'body')
    {
        if (oElement.style)
        {
            if (oElement.style.display == 'none')
            {
                return false;
            }
        }

        oElement = oElement.parentNode;
    }

    return true
}

function insertAfter(parent, node, referenceNode)
{
    parent.insertBefore(node, referenceNode.nextSibling);
}

var currentElement;
var currentElementInsertAfter;

 /* AddSectionToForm makes an ajax call to create a new HTML section to add to the page
 */
function AddSectionToForm(type, subtype, element, module, data, insertafter, spellChecker, formID, callback)
{
    var suffix;
    var id;

    if ($(type + '_max_suffix'))
    {
        // we're storing the max suffix value in hidden form inputs instead of a global JavaScript variable.
        // This prevents problems incrementing suffix values for nested sections.
        suffix = id = Number($(type + '_max_suffix').getValue());

        // if the type itself has a suffix, then we're creating a nested dynamic section
        if (type.indexOf("_") > 0)
        {
            var typeParts = type.split("_");

            for (var i = typeParts.length-1; i > -1; i--)
            {
                if (isNaN(typeParts[i]))
                {
                    break;
                }
                else
                {
                    suffix = typeParts[i] + '_' + suffix;
                }
            }
        }
    }
    else
    {
        suffix = id = dif1_section_suffix[type];
    }

    if (element)
    {
        // used in the response to decide which element (button) was clicked
        currentElement = element;
    }

    if (insertafter)
    {
        // we want to insert after rather than before the specified element.
        currentElementInsertAfter = insertafter;
    }
    else
    {
        currentElementInsertAfter = false;
    }

    if (!data)
    {
        data = '';
    }

    var postVals = "suffix="+suffix+"&type="+type+"&subtype="+subtype+"&module="+module+(jQuery('input[name=formlevel]').length ? "&level="+jQuery('input[name=formlevel]').val() : "")+(formID != null ? '&form_id='+formID : '')+data;

    new Ajax.Request
    (
        scripturl+"?action=httprequest&type=adddynamicsection",
        {
            method:'post',
            postBody: postVals,
            onSuccess: function(r)
            {
                var ListOfSections = currentElement.parentNode;

                if(type == 'recipients' || type == 'filter') {

                    var SectionListItem = document.createElement('li');
                }
                else {

                    var SectionListItem = document.createElement('div');
                }
                SectionListItem.id = type+'_section_div_'+id;

                if (currentElementInsertAfter)
                {
                    insertAfter(ListOfSections, SectionListItem, currentElement)
                }
                else
                {
                    ListOfSections.insertBefore(SectionListItem, currentElement)
                }

                InsertSectionIntoForm(r, SectionListItem);

                initialiseSpellchecker();

                if (callback != undefined)
                {
                    callback();
                }
            }
        }
    );

    if ($(type + '_max_suffix'))
    {
        $(type + '_max_suffix').setValue(id+1);
    }
    else
    {
        dif1_section_suffix[type]++;
    }
}

 /* CopySectionToForm makes an ajax call to create a new HTML section to add to the page
  * that mirrors another section on the form
 */
var copyfromsuffix,copytosuffix;

function CopySectionToForm(type, subtype, element, module, fromsuffix, insertAfter, spellChecker, formID)
{
    copyfromsuffix = fromsuffix;
    copytosuffix = dif1_section_suffix[type];

    currentElement = element;
    currentElementInsertAfter = insertAfter;

    var postVals = "type="+type+"&subtype="+subtype+"&module="+module;

    new Ajax.Request
    (
        scripturl+"?action=httprequest&type=getrowlist&responsetype=json",
        {
            method:'post',
            postBody: postVals,
            onSuccess: function(r)
            {
                var rows = r.responseText.split(',');

                var data ='';

                for (var i = 0; i < rows.length; i++)
                {
                    var $obj = jQuery("[name^=" + rows[i] + '_' + copyfromsuffix + "]");
                    var isRadio = $obj.is(":radio");
                    if(isRadio) {
                        data += '&' + rows[i] + '=' + $obj.filter(function () {
                            if (jQuery(this).val() != undefined)
                                return (jQuery(this).attr("checked") == "checked");
                            return false;
                        }).val();
                    }else {
                        data += '&' + rows[i] + '=' + $obj.val();
                    }
                }

                AddSectionToForm(type, subtype, currentElement, module, data, currentElementInsertAfter, spellChecker, formID);
            }
        }
    );
}

/* InsertSectionIntoForm takes the HTML code returned from the function AddSectionToForm
 * and inserts it into a specified element in the form.
 *
 * Before inserting the new HTML, any javascript needs to be extracted, and after inserting
 * it, the javascript needs to be added to the document head, in order for it to be taken
 * into account.
 */
function InsertSectionIntoForm(request, element)
{
    var TextToPrint = SplitJSAndHTML(request.responseText.toString());

    jQuery(element).html(TextToPrint);

    RunGlobalJavascript();
}


function AddHiddenField(element)
{
    var hidden = document.createElement("input");
    hidden.type = "hidden";
    hidden.value = element.value;
    hidden.name = element.name;
    hidden.id = element.id;
    element.parentNode.appendChild(hidden);
}

function AddCustomHiddenField(id, name, value)
{
    var hidden = document.createElement("input");
    hidden.type = "hidden";
    hidden.value = value;
    hidden.name = name;
    hidden.id = id;
    document.forms[0].appendChild(hidden);
}

function ReplaceSection(module, suffix, type, subtype, spellChecker, parentFormId)
{
    var postVals = "suffix="+suffix+"&type="+type+"&subtype="+subtype+"&module="+module+"&clearsection=1&level="+$('formlevel').value+"&parent_form_id="+parentFormId;

    new Ajax.Request
    (
        scripturl+"?action=httprequest&type=adddynamicsection",
        {
            method:'post',
            postBody: postVals,
            onSuccess: function(r)
            {
                $(type+'_section_div_'+suffix).innerHTML = '';

                InsertSectionIntoForm(r, $(type+'_section_div_'+suffix));

                initialiseSpellchecker();
            }
        }
    );
}

var JavascriptToExecute = new Array();

/**
 * Run javascript stored in the global variable JavascriptToExecute;
 * @param function callback a function to be triggered when javascript has been appended
 * @constructor
 */
function RunGlobalJavascript(callback)
{
    if(typeof callback == undefined) {

        callback = null;
    }

    AppendJavascript(JavascriptToExecute, callback);
}

/* SplitJSAndHTML takes a string of HTML and Javascript and seperates them, returning the rest of the text
 * as a string and storing the Javascript in a global, to be appended to the document head later.
 */
function SplitJSAndHTML(InputText, pattern)
{
    //var MatchArray = new Array();
    var TempArray = new Array();
    var TextToPrint = '';

    if (pattern == undefined)
    {
        pattern = /<script(?: (?:language="JavaScript"|type="text\/javascript"|src="([^"]+)"))+\s*>([\s\S.]*?)<\/script>/gmi;
    }

    if(pattern instanceof RegExp) {

        var matches, match;
        while(matches = pattern.exec(InputText)) {
            TempArray.push(matches[1] ? matches[1] : matches[2] ? matches[2] : false);
        }
        //TempArray = InputText.match(pattern);
        TextToPrint = InputText.replace(pattern, '');

        for(var i = 0; i < TempArray.length; i++) {

            JavascriptToExecute.push(TempArray[i]);
        }
    }
    else {

        TempArray =  InputText.split(pattern);

        TextToPrint += TempArray[0];

        for (var i = 1; i < TempArray.length; i++)
        {
            TempArray2 = TempArray[i].split('</script>');
            // we might not want to run this until after the HTML has been added to the page, so we store it in a global.
            JavascriptToExecute.push(TempArray2[0]);
            TextToPrint += TempArray2[1];
        }
    }

    return TextToPrint;
}

/* AppendJavascript takes an array of Javascript scripts and appends them as new
 * 'script' elements to the head of the document, allowing the browser to recognise
 * them as valid scripts.
 */
function AppendJavascript(javascriptArray, callback) {

    if(typeof callback == undefined) {

        callback = null;
    }

    var loadScript = javascriptArray.shift();

    if(loadScript != undefined) {

        // check if content is a js src link
        if(/^[a-z0-9\/\._-]*\.js(?:\?v=[0-9]+)?$/i.test(loadScript)) {

            isSrc = true;

            jQuery.getScript(loadScript, function() {

                AppendJavascript(javascriptArray, callback);
            });
        }
        else {

            script = document.createElement('script');
            script.text = loadScript;
            script.type = 'text/javascript';
            head = document.getElementsByTagName('head').item(0);
            head.appendChild(script); //add function definitions to document head.

            script = null;
            eval(loadScript); //ensure that any non-function-definition code that has been returned is run.

            AppendJavascript(javascriptArray, callback);
        }
    }
    else {

        if(typeof callback == 'function') {

            callback.call(this);
        }
    }

    JavascriptToExecute = [];
}

function addInjury(el, suffix, type, formID)
{
    personSuffix = '';
    currentPerson = '';

    if (suffix != '' && suffix != 0)
    {
        currentPerson = suffix;
        personSuffix = '_' + suffix;
    }

    nextRow = el.getAttribute("nextSuffix") || "1";

    var last_inj_row = '';
    var checkRow = parseInt(nextRow)+1;
    var foundrow = 0;

    while (foundrow == 0 && checkRow>0)
    {
        checkRow--;

        if ($('injury'+ personSuffix + '_' + checkRow))
        {
            last_inj_row = $('injury'+ personSuffix + '_' + checkRow).value;
            foundrow = 1;
        }
    }

    if (last_inj_row != '' || checkRow == 0)
    {
        el.disabled = true;
        new Ajax.Request
        (
            scripturl+'?action=httprequest&type=insertinjuryrow&'+(currentPerson != 0 ? 'person_suffix='+currentPerson : '')+'&row_suffix='+nextRow+'&linktype='+type+(formID != null ? '&form_id='+formID : '')+'&responsetype=json',
            {
                method:'get',
                asynchronous: false,
                onSuccess: function(r)
                {
                    insertInjuryRow(r.responseText.evalJSON());
                }
            }
        );

        el.disabled = false;
        el.setAttribute('nextSuffix', (parseInt(nextRow) + 1));
    }
    else
    {
        alert("Please select a valid injury before adding a new row");
    }
}

function addProperty(el, suffix, type)
{
    personSuffix = '';
    currentPerson = '';

    if (suffix != '' && suffix != 0)
    {
        currentPerson = suffix;
        personSuffix = '_' + suffix;
    }

    nextRow = el.getAttribute("nextSuffix") || "1";

    var last_property_row = '';
    var checkRow = parseInt(nextRow)+1;
    var foundrow = 0;

    while (foundrow == 0 && checkRow>0)
    {
        checkRow--;

        if ($('ipp_description'+ personSuffix + '_' + checkRow))
        {
            last_property_row = $('ipp_description'+ personSuffix + '_' + checkRow).value;
            foundrow = 1;
        }
    }

    if (last_property_row != '' || checkRow == 0)
    {
        el.disabled = true;
        new Ajax.Request
        (
            scripturl+'?action=httprequest&type=insertpropertyrow&'+(currentPerson != 0 ? 'person_suffix='+currentPerson : '')+'&row_suffix='+nextRow+'&linktype='+type+'&responsetype=json&'+($$('input[name=form_id]')[0] ? 'form_id='+$$('input[name=form_id]')[0].value : ''), //need the form_id value for getting the DIF1 form in order to identify the subform designs we are using.
            {
                method:'get',
                asynchronous: false,
                onSuccess: function(r)
                {
                    insertPropertyRow(r.responseText.evalJSON());
                }
            }
        );
        el.disabled = false;
        el.setAttribute('nextSuffix', (parseInt(nextRow) + 1));
    }
    else
    {
        alert("Please add a description before adding a new row");
    }
}

function getElementsByName_iefix(tag, name)
{
    var elem = document.getElementsByTagName(tag);
    var arr = new Array();

    for (i = 0,iarr = 0; i < elem.length; i++)
    {
        att = elem[i].getAttribute("name");

        if(att == name)
        {
            arr[iarr] = elem[i];
            iarr++;
        }
    }

    return arr;
}

function ToggleInnerHTML(el, option1, option2)
{
	if (el.innerHTML == option1)
	{
		el.innerHTML = option2;
	}
	else
	{
		el.innerHTML = option1;
	}
}

function getFirstNonNull(aChoice)
{
	for (var i = 0; i < aChoice.length ; i++)
	{
		if (aChoice[i])
		{
			return aChoice[i];
		}
	}

	return 0;
}

function getElementPosition(obj)
{
	var curleft = curtop = 0;

	if (obj.offsetParent)
    {
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;

		while (obj = obj.offsetParent)
        {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		}
	}

	return [curleft,curtop];
}

function Browser()
{
    var ua, s, i;

    this.isIE    = false;
    this.isNS    = false;
    this.isOpera = false;
    this.version = null;

    ua = navigator.userAgent;

    // IE 11's only identifiable part of it's user agent header is the rather ambiguous ' rv:version.number '
    // I am doing a  regex here rather indexOf due to the identifying string 'rv' being less than unique
    // Also, I have decided not to match spaces either side as I am not confident that future versions will have these
    var ieGreaterThanVerTen = ua.match(/rv:([0-9]+\.[0-9]+)/);
    if(ieGreaterThanVerTen !== null && ieGreaterThanVerTen.length === 2)
    {
        this.isIE = true;
        this.version = parseFloat(ieGreaterThanVerTen[1]);
        return;
    }

    s = "MSIE";

    if ((i = ua.indexOf(s)) >= 0)
    {
        this.isIE = true;
        this.version = parseFloat(ua.substr(i + s.length));
        return;
    }

    s = "Netscape6/";

    if ((i = ua.indexOf(s)) >= 0)
    {
        this.isNS = true;
        this.version = parseFloat(ua.substr(i + s.length));
        return;
    }

    // Treat any other "Gecko" browser as NS 6.1.
    s = "Gecko";

    if ((i = ua.indexOf(s)) >= 0)
    {
        this.isNS = true;
        this.version = 6.1;
        return;
    }

    s = "Opera/";

    if ((i = ua.indexOf(s)) >= 0)
    {
        this.isOpera = true;
        this.version = parseFloat(ua.substr(i + s.length));
        return;
    }
}

function getWindowHeight()
{
    var windowHeight = 0;

    if (typeof(window.innerHeight) == 'number')
    {
        windowHeight = window.innerHeight;
    }
    else
    {
        if (document.documentElement && document.documentElement.clientHeight)
        {
            windowHeight = document.documentElement.clientHeight;
        }
        else
        {
            if (document.body&&document.body.clientHeight)
            {
                windowHeight = document.body.clientHeight;
            }
        }
    }

    return windowHeight;
}

function getViewPortSize()
{
	var viewportSize = new Array();

	viewportSize[0] = 0;
	viewportSize[1] = 0;

	if (typeof(window.innerWidth) == 'number')
	{
		viewportSize[0] = window.innerWidth;
		viewportSize[1] = window.innerHeight;
	}
	else if (document.documentElement && document.documentElement.clientWidth)
	{
		viewportSize[0] = document.documentElement.clientWidth;
		viewportSize[1] = document.documentElement.clientHeight;
	}
	else if (document.body && document.body.clientWidth)
	{
		viewportSize[0] = document.body.clientWidth;
		viewportSize[1] = document.body.clientHeight;
	}

	return viewportSize;
}

function SetGlobal(Global, Value)
{
    new Ajax.Request
    (
        scripturl+"?action=httprequest&type=setglobal&global="+Global+"&value="+Value,
        {
            method:'get'
        }
    );
}

function ForceSetGlobal(Global, Value)
{
    new Ajax.Request
    (
        scripturl+"?action=httprequest&type=setglobal&global="+Global+"&value="+Value+"&update_session=1",
        {
            method:'get'
        }
    );
}

function SetUserParm(Global, Value, UserId)
{
    jQuery.get(scripturl+"?action=httprequest&type=setglobal&global="+Global+"&value="+Value+'&user='+UserId);
}

var PopupDivs = new Array();
var ActivePopupDivs = 0;

function AddNewFloatingDiv(id)
{
    // check to see if the id already exists in the array before adding
    if (id && ! jQuery.grep(PopupDivs, function(elem){ return elem.id === id; }).length)
    {
        PopupDivs.push(new FloatingWindow(id, PopupDivs.length));
    }

    return GetFloatingDiv(id);
}

function GetFloatingDiv(id)
{
    if (jQuery('#floating_window_numkey_'+id))
    {
        if (PopupDivs[jQuery('#floating_window_numkey_'+id).val()])
        {
            return PopupDivs[jQuery('#floating_window_numkey_'+id).val()];
        }
    }
}

function PopupDivFromURL(id, title, url, post, buttons, width, onBeforeClose, onClose)
{
    onBeforeClose = setDefaultParameter(onBeforeClose, null);
    onClose = setDefaultParameter(onClose, null);

    new Ajax.Request
    (
        url,
        {
            method:'post',
            postBody: post,
            asynchronous:false,
            onSuccess: function(r)
            {
                AddNewFloatingDiv(id);

                var CurrentPopupDiv = GetFloatingDiv(id);

                var HTML = SplitJSAndHTML(r.responseText.toString());

                CurrentPopupDiv.setContents(HTML);
                CurrentPopupDiv.setTitle(title);

                var buttonHTML = '';

                if(buttons.length == 0) {

                   buttons = new Array();
                   buttons.push({'value':'Close','onclick':'GetFloatingDiv(\''+id+'\').CloseFloatingControl()'});
                }

                CurrentPopupDiv.setButtons(buttons);

                if(width) {

                    CurrentPopupDiv.setWidth(width);
                }

                if(onBeforeClose) {

                    CurrentPopupDiv.OnBeforeCloseAction = onBeforeClose;
                }

                if(onClose) {

                    CurrentPopupDiv.OnCloseAction = onClose;
                }

                CurrentPopupDiv.display();

                RunGlobalJavascript();
            }
        }
    );
}

function LimitTextareaChars(obj, limit)
{
    /**
     * Replaces new lines with two pipe (|) characters so that they can be counted properly as part of the limit
     * @type {*}
     */
    var $target = jQuery(obj),
        value = $target.val().replace(/(\r\n|\r|\n)/g, "||");

    if (value.length > limit)
    {
        var limited = value.replace(/(^\s+|\s+$)/g, '').slice(0, limit);

        /**
         * Replaces the pipes with a new line and removes any single pipes that may have been split by the limit
         * @type {string}
         */
        var replaceText = limited.replace(/\|\|/g, "\r\n").replace('|', '').replace(/(^\s+|\s+$)/g, '');

        if(globals.WEB_SPELLCHECKER && $target.hasClass('spellcheck')) {

            $target.closest('.field_input_div').find('.livespell_textarea').text(replaceText);
        }
        else {

            $target.val(replaceText);
        }

        OldAlert('You have reached the character limit set for this field');
    }
}

// used to find the values of fields when checking conditions for
// showing/hiding sections/fields.
function getValuesArray(id)
{
    if (id)
    {
        eval ('var Field = $("'+id+'");');

        var Values = new Array();

        if (Field)
        {
            if (Field.tagName == 'SELECT')
            {
                if (Field.multiple)
                {
                    for (var i = 0; i < Field.options.length; i++)
                    {
                        Values.push(Field.options[i].value);
                    }
                }
                else
                {
                    Values.push(Field.options[Field.selectedIndex].value);
                }
            }
            else if (Field.tagName == 'INPUT')
            {
                if (Field.type == 'text')
                {
                    Values = Field.value.split('|');
                }
                else if (Field.type == 'checkbox')
                {
                    if (Field.checked)
                    {
                        Values.push('Y');
                    }
                }
                else if (Field.type == 'hidden')
                {
                    Values = Field.value.split(' ');
                }
                else if (Field.type == 'radio')
                {
                    Values.push (jQuery('input[name='+id+']:checked').val());
                }
                else
                {
                    Values.push(Field.value);
                }
            }
        }
        else if (jQuery("#"+id+"_title").length && jQuery("#"+id+"_title").data("search"))
        {
            Values = jQuery("#"+id+"_title").val().split('|');
        }

        // deal with radio buttons (without node id)
        else if (
            jQuery('input[name='+id+']').length &&
            jQuery('input[name='+id+']').first().attr('type') == 'radio'
        ) {
            Values.push (jQuery('input[name='+id+']:checked').val());
        }

        return Values;
    }

    return false
}

function getPrintURL()
{
    var currentURL = window.location.href;

    if (currentURL.search('#panel') != -1)
    {
        newURL = currentURL.replace(/#panel/, '&print=1#panel');
    }
    else
    {
        newURL = currentURL + '&print=1';
    }

    return newURL;
}

Array.prototype.inArray = function (valArray)
{
    var i,j;

    for (i = 0; i < this.length; i++)
    {
        for (j = 0; j < valArray.length; j++)
        {
            if (this[i] === valArray[j])
            {
                return true;
            }
        }
    }

    return false;
};

 /**
 * Called only when the contents of a field actually changes (i.e. not when it is shown/hidden)
 *
 * @param string fieldname
 */
function FieldChanged(fieldname)
{
    disableAllButtons();
    CascadeCheckedFields = new Array();

    setChanged(fieldname);

    var functionExists = null;

    /*
    Function defined on pageload: OnChange_[fieldname]
    Just handles any javascript alert() messages that need to be displayed.
     */
    functionToCall = 'OnChange_' + fieldname;

    if (window[functionToCall])
    {
        window[functionToCall]();
    }

    TriggerOnChange(fieldname);
    enableAllButtons();
}

function SetDisplayDivSize(element)
{
    element.style.width = "auto";

    if (element.innerHTML == '' || element.offsetWidth < 200)
    {
         element.style.width = "200px";
    }
}

var CascadeCheckedFields = new Array(); //prevents recursion if people have circular forms.

 /**
 * Called whenever a field alters in a manner that might affect other areas of the form.
 * - when the value of the field changes
 * - when the field is shown or hidden by an action
 * - when the section the field is in is shown or hidden by an action
 *
 * @param string fieldname
 */
function TriggerOnChange(fieldname)
{
    if (CascadeCheckedFields.indexOf(fieldname) != -1)
    {
        //we've already checked this field, so we shouldn't check it again in case we get caught in a loop.
        return;
    }

    CascadeCheckedFields.push(fieldname);

    var functionToCall = null;

    /*
    Function defined on pageload: CascadeCheckState_[fieldname]
    Calls CheckState_[fieldname] for every field controlled from this one by a field action
    Calls CheckState_[section] for every section controlled from this one by an action
     */
    functionToCall = 'CascadeCheckState_' + fieldname;

    if (window[functionToCall])
    {
        window[functionToCall]();
    }

    functionToCall = null;

    /*
    Function defined on pageload: OnChange_Extra_[fieldname]
    Runs any custom javascript linked to the field
     */
    functionToCall = 'OnChange_Extra_' + fieldname;

    if (window[functionToCall])
    {
        window[functionToCall]();
    }
}

 /**
 * Called whenever a section alters in a manner that might affect other areas of the form. i.e. when the section is shown or hidden by an action.
 *
 * @param string section
 */
function TriggerOnChangeSection(section)
{
    /*
    Function defined on pageload: OnSectionChange_[fieldname]
    Calls TriggerOnChange_[fieldname] for every field in this section
     */
    eval('var functionExists = window.OnSectionChange_' + section + ';');

    if (functionExists)
    {
        eval('OnSectionChange_' + section + '();');
    }
}

 /**
 * Called when a currently-hidden section needs to be displayed. May trigger other form events as a result
 *
 * @param string Section
 */
function ShowSection(Section)
{

    if ($(Section))
    {
        $(Section).style.display = '';
    }

    if ($('show_section_' + Section))
    {
        $('show_section_' + Section).value = '1';
    }

    if(jQuery('#' + Section).find('.toggle-target').data('rendered')) {

        TriggerOnChangeSection(Section);
    }
}

 /**
 * Called when a currently-displayed section needs to be hidden. May trigger other form events as a result
 *
 * @param string Section
 */
function HideSection(Section)
{
    if ($(Section))
    {
        $(Section).style.display = 'none';
    }

    if ($('show_section_' + Section))
    {
        $('show_section_' + Section).value = '0';
    }
    
    TriggerOnChangeSection(Section);
}

 /**
 * Called when a currently-hidden field needs to be displayed. May trigger other form events as a result
 *
 * @param string FieldName
 */
function ShowFieldRow(FieldName)
{
    if ($(FieldName+ '_row'))
    {
        $(FieldName+ '_row').style.display = '';
    }

    if ($('show_field_' + FieldName))
    {
        $('show_field_' + FieldName).value = '1';
    }

    TriggerOnChange(FieldName);
}

 /**
 * Called when a currently-displayed field needs to be hidden. May trigger other form events as a result
 *
 * @param string FieldName
 */
function HideFieldRow(FieldName)
{
    if ($(FieldName+ '_row'))
    {
        $(FieldName+ '_row').style.display = 'none';
    }

    if ($('show_field_' + FieldName))
    {
        $('show_field_' + FieldName).value = '0';
    }

    TriggerOnChange(FieldName);
}

 /**
 * Called when the "delete" button is pressed on the datix multicode control. Removes selected items from the list.
 *
 * @param string FieldName
 *
 * @returns bool: true if the field is shown on the form or hidden with a default value (i.e. the two situations in which the data will be posted).
 */
 function FieldIsAvailable(FieldName)
 {
     var $showField = jQuery('#show_field_' + FieldName),
         $field = jQuery('#' + FieldName),
         $showSection = jQuery('#show_section_' + FieldSection[FieldName]);

     //hidden field with default
     if( ! $showField && $field.length && $field.val() != '' && $showSection.val() == '1') {

         return true;
     }

     if($showField && $showField.val() == '1' && $showSection.val() == '1') {

         return true;
     }

     return false;
 }

 /**
 * Called when the "delete" button is pressed on the datix multicode control. Removes selected items from the list.
 *
 * @param string fieldName: id of the field
 */
function removeSelectedItemsFromMultiListbox(fieldName, showNumbers)
{
    eleMultiListbox  = $(fieldName);

    if  (eleMultiListbox.selectedIndex != -1)
    {
        while (eleMultiListbox.selectedIndex != -1)
        {
            var code = eleMultiListbox.options[eleMultiListbox.selectedIndex].value;
            var description = eleMultiListbox.options[eleMultiListbox.selectedIndex].text;

            eleMultiListbox.options[eleMultiListbox.selectedIndex] = null;
        }

        setChanged(eleMultiListbox.name.replace("[]",""));

        FieldChanged(fieldName);
    }

    checkMultiWidth(eleMultiListbox);
    
    if (showNumbers)
    {
        var text = '';
        jQuery("#"+fieldName).find("option").each(function(i, option)
        {
            text = jQuery(option).text().replace(/^\d+:\s/, "");
            jQuery(option).text((i+1)+": "+text);
        });
    }
}

/**
 * Called when the "edit" button is pressed on the datix multicode control.
 *
 * @param string fieldName id of the field
 */
function editSelectedItemFromMultiListbox(fieldName)
{
    var selected = jQuery("select#"+fieldName).val(),
        index = jQuery('#'+fieldName).prop('selectedIndex'),
        buttons  = [];
    
    if (selected == null)
    {
        OldAlert("Please select a value to edit");
        return;
    }
    
    if (selected.length > 1)
    {
        OldAlert("You can only select a single value to edit");
        return;
    }
    
    buttons[0]={'value':'Edit','onclick':'jQuery(jQuery(\'#'+fieldName+'\').find(\'option\')['+index+']).prop(\'value\', jQuery(\'#code\').val()).text(\''+(index+1)+': \'+jQuery(\'#code\').val());GetFloatingDiv(\'edit_multicode_value\').CloseFloatingControl();'};
    buttons[1]={'value':'Cancel','onclick':'GetFloatingDiv(\'edit_multicode_value\').CloseFloatingControl();'};
    
    PopupDivFromURL("edit_multicode_value", "Edit Code", "app.php?action=editmulticodevalue&token="+token, "fieldName="+fieldName+"&value="+selected[0], buttons);
}

function urlencode(str)
{
//    str = escape(str);
//    str = str.replace('+', '%2B');
//    str = str.replace('%20', '+');
//    str = str.replace('*', '%2A');
//    str = str.replace('/', '%2F');
//    str = str.replace('@', '%40');

    str = encodeURIComponent (str);
    return str;
}

 /**
 * Validates feedback data and sends out email via ajax, reporting the result to the user dynamically.
 *
 * @param {string} module
 * @param {int} id
 */
function SendFeedbackEmail(module, id)
{
    var emailTo = '',
        globalAdressBook = '',
        fbk_to = $('fbk_to'),
        i,
        ajaxEmailStatus = $('ajax_email_status'),
        fbk_gab = $('fbk_gab'),
        fbk_email = $('fbk_email'),
        emailAdditional,
        fbk_html = $('fbk_html');

    $('feedback_btn').disabled = true;
    ajaxEmailStatus.innerHTML = '<img src="Images/SendMail.png" /><p>Sending Feedback';
    ajaxEmailStatus.show();

    if (fbk_to != null) {
        if (fbk_to.options) {
            if (fbk_to.options.length > 0) {
                emailTo = fbk_to.options[0].value;
            }

            for (i = 1; i < fbk_to.options.length; i++) {
                emailTo += ',' + fbk_to.options[i].value;
            }
        }
        else {
            if (fbk_to.value != '') {
                emailTo = fbk_to.value;
            }
            else {
                emailTo = '';
            }
        }
    }

    if (fbk_gab != null) {
        if (fbk_gab.options) {
            if (fbk_gab.options.length > 0) {
                globalAdressBook = fbk_gab.options[0].value;
            }

            for (i = 1; i < fbk_gab.options.length; i++) {
                globalAdressBook += ',' + fbk_gab.options[i].value;
            }
        }
        else {
            if (fbk_gab.value != '') {
                globalAdressBook += fbk_gab.value;
            }
            else {
                globalAdressBook = '';
            }
        }
    }

    if (fbk_email)
    {
        emailAdditional = fbk_email.value;
    }
    else
    {
        emailAdditional = '';
    }

    var emailSubj = urlencode($('fbk_subject').value);
    var emailBody = urlencode($('fbk_body').value);

    var url = 'app.php?action=httprequest&type=sendfeedback';

    // Clear Additional recipients values
    if (fbk_email)
    {
        fbk_email.value = '';
    }

    // Clear Staff and contacts attached to this record values
    if (fbk_to != null) {
        if (fbk_to.length) {
            while (fbk_to.length > 0) {
                fbk_to.remove(0);
            }
        }

        if (jQuery('#fbk_to_values > ul').children()) {
            jQuery.each(jQuery('#fbk_to_values > ul').children(), function (index, value) {
                jQuery(value).remove();
            });
        }
    }

    // Clear All users values
    if (fbk_gab != null) {
        if (fbk_gab.length) {
            while (fbk_gab.length > 0) {
                fbk_gab.remove(0);
            }
        }

        if (jQuery('#fbk_gab_values > ul').children()) {
            jQuery.each(jQuery('#fbk_gab_values > ul').children(), function (index, value) {
                jQuery(value).remove();
            });
        }
    }

    new Ajax.Request
    (
        url,
        {
            method:'post',
            postBody:'module='+module+'&recordid='+id+'&to='+emailTo+'&subject='+emailSubj+'&body='+emailBody+'&fbk_email='+emailAdditional+'&fbk_gab='+globalAdressBook+'&is_html='+fbk_html.value,
            asynchronous:false,
            onSuccess: function(r)
            {
                $('ajax_email_status').innerHTML = r.responseText;

                $('CHANGED-fbk_body').value = '';

                $('feedback_btn').disabled = false;
            }
        }
    );
}

function completeActionPost(id, element, act_progress, module, callback)
{
    var data = {"act_progress" : act_progress, "id" : id, "module" : module};

    jQuery.ajax(
    {
        url: scripturl+"?action=httprequest&type=actioncomplete&responsetype=json",
        type: "POST",
        data: data,
        success: function(data)
        {
            if (callback != undefined)
            {
                SendTo(callback);
            }
            else
            {
                jQuery(element).html(data);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert('Error: Action could not be updated');
        }
    });
}

var browser = new Browser();

var FieldSection = new Array();

 /**
 * Quick bool function that returns true if browser is IE6 (used for special js cases).
 *
 * @returns bool
 */
function isIE6()
{
    return (browser.isIE && browser.version == 6);
}

 /**
 * Used to check that XML is valid before applying an XSL transform to it -
 * the only way I could do this was by running the serialiser and checking for an error.
 *
 * @param obj xmlObj: xml object to check
 *
 * @returns bool: true if the xml is valid, false if not.
 */
function XMLIsValid(xmlObj)
{
    var xmlStringCheck = '';

    if (!browser.isIE)
    {
        // XMLSerializer exists in current Mozilla browsers
        serializer = new XMLSerializer();
        xmlStringCheck = serializer.serializeToString(xmlObj.xmlDocument);
    }
    else
    {
        // Internet Explorer has a different approach to serializing XML
        xmlStringCheck = xmlObj.xmlDocument.xml;
    }

    if (xmlStringCheck == '' || xmlStringCheck.match("<parsererror") != null)
    {
        return false;
    }

    return true;
}

var StatusesNoMandatory = new Array();
var mandatoryArray = new Array();
var mandatoryArraySpecial = new Array();
mandatoryArraySpecial.REJECT = new Array();

 /**
 * Compares both parts of the mandatory field array entry to determine if there are duplicates. Returns an array with these duplicates removed. .
 *
 * @returns array: mandatory field array with duplicates removed.
 */
function removeDuplicateMandatoryFields()
{
    var DuplicateCheck = new Array();
    var Duplicate;

    for (var i = 0; i < mandatoryArray.length; i++)
    {
        Duplicate = false;

        for (var j = 0; j < DuplicateCheck.length; j++)
        {
            if (DuplicateCheck[j][0] == mandatoryArray[i][0] && DuplicateCheck[j][1] == mandatoryArray[i][1])
            {
                Duplicate = true;
            }
        }

        if (!Duplicate)
        {
            DuplicateCheck.push(mandatoryArray[i]);
        }
    }

    return DuplicateCheck;
}

var windowopen = false;

 /**
 * Creates code selection object and adds it to the global array, before calling the specific function to populate it.
 *
 * @param string field: The field id
 * @param bool normal: true if it is a normal codelist, false if it is a codecheck (for parenting).
 */
function CreateCodeList(field, normal)
{
    if (windowopen != true || !normal)
    {
        windowopen = true;
        var NewControl = new CodeSelectionCtrl();
        ControlIndex = GlobalCodeSelectControls.length;
        NewControl.setIndex(ControlIndex)
        GlobalCodeSelectControls.push(NewControl);

        //prevents js errors if the field is readonly or hidden.
        eval('var isFunc = (typeof CreateCodeList_'+field+' == \'function\')');

        if (isFunc)
        {
            eval('CreateCodeList_'+field+'(ControlIndex, normal)');
        }
        else
        {
            windowopen = false;
        }
    }
}

 /**
 * Turns an array into an object.
 *
 * @param array a
 *
 * @returns obj
 */
function oc(a)
{
    var o = {};

    for (var i = 0; i < a.length; i++)
    {
        o[a[i]] = '';
    }

    return o;
}

 /**
 * Checks whether complainant chain dates can be recalculated when a user tries to save a complainant having altered the date recieved.
 *
 * @returns bool True if the dates cannot be re-calculated, false otherwise
 */
function CheckComplaintDates()
{
    if(
        (($('lcom_dack') && $('lcom_dack').value != '') ||
        ($('lcom_dactioned') && $('lcom_dactioned').value != '') ||
        ($('lcom_dresponse') && $('lcom_dresponse').value != '') ||
        ($('lcom_dholding') && $('lcom_dholding').value != '') ||
        ($('lcom_dreplied') && $('lcom_dreplied').value != ''))
        &&
        ($('CHANGED-lcom_dreceived') && $('CHANGED-lcom_dreceived').value == '1'))
    {
        return true;
    }

    return false;
}

 /**
 * Adds a new texarea to the configuration form when the user is designing the alert messages to display on login. Called in GetLoginMessageControl()
 *
 * @param string Field: The value to put as the field id.
 */
function AddNewMessageBox(Field)
{
    var HTML = '';

    var BoxID = parseInt($('max'+Field+'Id').value) + 1;

    HTML = '<div><textarea rows="6" cols="60" id="'+Field+'_'+BoxID+'" name="'+Field+'_'+BoxID+'" ></textarea><div>';

    var NewDiv = document.createElement("div");
    NewDiv.innerHTML = HTML;

    $(Field+'Div').appendChild(NewDiv);

    $('max'+Field+'Id').value = BoxID;

}

var FeedbackValidationNeeded = false;

 /**
 * Called when a user tries to save a record to ensure that no feedback changes are lost by prompting them to send an email.
 *
 * @param string module
 * @param int recordid
 *
 * @returns bool False if the user is to be presented with a message, true if feedback can be sent.
 */
function DoFeedbackValidation(module, recordid)
{
    var buttons = new Array();

    if (($('fbk_to') && $('fbk_to').length > 0) || ($('fbk_email') && $('fbk_email').value != '') || ($('fbk_gab') && $('fbk_gab').length > 0))
    {
        buttons.push({'value': txt['btn_send_and_save'], 'onclick': 'SendFeedbackEmail(\''+module+'\','+recordid+');writeSuffixLimits();document.forms[0].submit()'});
        buttons.push({'value': txt['btn_save_without_sending'], 'onclick': 'writeSuffixLimits(); document.forms[0].submit()'});
        buttons.push({'value': txt['btn_feedback_cancel'], 'onclick': 'feedbackCancel()'});

        DivAlert('Feedback', txt['fbk_email_check_recipients'], buttons, null);

        return false;
    }
    else if (($('CHANGED-fbk_body') && $('CHANGED-fbk_body').value == '1') || ($('CHANGED-fbk_subject') && $('CHANGED-fbk_body').value == '1'))
    {
        buttons.push({'value': txt['btn_save_without_sending'], 'onclick': 'writeSuffixLimits();document.forms[0].submit()'});
        buttons.push({'value': txt['btn_feedback_cancel'], 'onclick': 'feedbackCancel()'});

        DivAlert('Feedback', txt['fbk_email_check_amend'], buttons, null);
        return false;
    }

    return true;
}

/**
* Called when clicking Cancel on the feedback validation popup.
*
* Ensures form remains usable by hiding loading popup, re-enabling form buttons
* and removing feedback validation popup.
*/
function feedbackCancel()
{
    GetFloatingDiv('alert' + (PopupDivs.length - 1)).CloseFloatingControl();
    hideLoadPopup();
    enableAllButtons();
}

 /**
 * Checks all fields are filled correctly and then calls SendFeedbackEmail()
 *
 * @param string module
 * @param int recordid
 */
function SendFeedback(module, recordid)
{
    selectAllMultiCodes();

    var ffbk_to = jQuery('#fbk_to'),
        ffbk_to_values = jQuery('#fbk_to_values > ul').children().length,
        ffbk_gab = jQuery('#fbk_gab'),
        ffbk_gab_values = jQuery('#fbk_gab_values > ul').children().length,
        FbkEmail = jQuery('#fbk_email'),
        bSend = false,
        bError = false;

    if ((FbkEmail && FbkEmail.val() == '') && (ffbk_to && ffbk_to_values <= 0) && (ffbk_gab && ffbk_gab_values <= 0))
    {
        bError = true;
    }

    if (bError)
    {
        alert(txt['email_address_missing_err']);
    }
    else
    {
        if (FbkEmail.val() && isEmail(FbkEmail, true))
        {
            bSend = true;
        }
        else if (ffbk_to && ffbk_to_values > 0)
        {
            bSend = true;
        }
        else if (ffbk_gab && ffbk_gab_values > 0)
        {
            bSend = true;
        }

        if (bSend)
        {
            SendFeedbackEmail(module, recordid);
        }
    }
}

 /**
 * Used in the form designer, unchecks and disables all ' Mandatory?' checkboxes
 * within the section if the section is made read-only
 *
 * @param obj input: The element that has triggered this function
 * @param string section: The section that has been made read only
 */
function UncheckMandatoryFields(input, section)
{
    var elements = $(section).select('input[name^=MANDATORY-' + section + ']');

    for (var i = 0; i < elements.length; i++)
    {
        if (input.checked)
        {
            elements[i].writeAttribute('checked', '');
            elements[i].writeAttribute('disabled', 'disabled');
        }
        else
        {
            elements[i].writeAttribute('disabled', '');
        }
    }
}

 /**
 * Sends out overdue emails via ajax, updating a progress meter on the page as it does.
 */
function sendOverdueEmails(module)
{
    $(module+'_overdue_progress_bar').update('Please wait while emails are computed and sent...');
    $(module+'_overdue_progress').update('');

    if(module == 'INC')
    {
        var statuses = getValuesArray('OVERDUE_EMAIL_STATUS').join('|');
        var users = getValuesArray('OVERDUE_EMAIL_USERS').join('|');
    }
    else if (module == 'COM')
    {
        var statuses = getValuesArray('OVERDUE_EMAIL_STATUS_COM').join('|');
        var users = getValuesArray('OVERDUE_EMAIL_USERS_COM').join('|');
    }

    new Ajax.Request
    (
        scripturl+'?action=httprequest&type=getoverdueemailrecipients&statuses='+statuses+'&users='+users+'&module='+module+'&responsetype=json',
        {
            method:'get',
            onSuccess: function(r)
            {
                var jsonResponse = r.responseText.evalJSON()

                if (!jsonResponse.contacts || jsonResponse.contacts.length == 0)
                {
                    $(module+'_overdue_progress_bar').update('No emails sent');
                }
                else
                {
                    $(module+'_overdue_progress_bar').insert('Sending emails: <span id="'+module+'_overdue_progress_percent">0</span>%');

                    for (var i = 0; i < jsonResponse.contacts.length; i++)
                    {
                        var email = jsonResponse.contacts[i].email;
                        var records = jsonResponse.contacts[i].records.join('|');
                        var percent = Math.round((1/jsonResponse.contacts.length)*100);
                        var last = (i == (jsonResponse.contacts.length -1) ? 1 : 0);

                        new Ajax.Request
                        (
                            scripturl+'?action=httprequest&type=sendoverdueemails&responsetype=json&email='+email+'&module='+module+'&records='+records+'&percent='+percent+'&last='+last,
                            {
                                method:'get',
                                onSuccess: function(r)
                                {
                                    var jsonResponse = r.responseText.evalJSON()
                                    $(module+'_overdue_progress').insert(jsonResponse.html);
                                    var percent = parseInt($(module+'_overdue_progress_percent').innerHTML) + parseInt(jsonResponse.get.percent);
                                    $(module+'_overdue_progress_percent').update(percent);

                                    if (jsonResponse.get.last == '1')
                                    {
                                        $(module+'_overdue_progress_bar').update('All overdue email messages have been processed:');
                                    }
                                }
                            }
                        );
                    }
                }
            }
        }
    );
}

 /**
 * Should compare sta_profile and sta_profile_old and if they differ, prompt the user with an alert box.
 * For some reason this causes a lot of problems, so this may need to be refactored.
 */
function checkProfileSave(alwaysAsk)
{
    var buttons = new Array();

    if (!alwaysAsk)
    {
        var CurrentValues = getValuesArray('sta_profile');
        var CurrentValue = CurrentValues[0];

        if (CurrentValue == '' || !$('sta_profile'))
        {
            CurrentValue = '0';
        }

        var OldValue = $('sta_profile_old').value;

        if (OldValue == '')
        {
            OldValue = '0';
        }
    }

    if (alwaysAsk || (parseInt(OldValue) != parseInt(CurrentValue) && parseInt(CurrentValue) != '0'))
    {
        buttons.push({'value': 'Yes', 'onclick': 'jQuery(\'#clearUserSettings\').val(\'1\');continueSaveProfile();'});
        buttons.push({'value': 'No', 'onclick': 'jQuery(\'#clearUserSettings\').val(\'\');continueSaveProfile();'});

        DivAlert('Clear user settings?', txt['profile_clear_check'], buttons, function() {continueSaveProfile();});
    }
    else
    {
        jQuery('#clearUserSettings').val('');
        continueSaveProfile();
    }
}

 /**
 * Called after the check for profile data deletion has been made in checkProfileSave()
 * Needed because we don't have a generic interrupt-type alert yet.
 */
function continueSaveProfile()
{
    selectAllMultiCodes();
    submitClicked = true;
    document.forms[0].submit();
}

 /**
 * Loops through all input.button elements and disables them.
 * Used when submitting forms.
 */
function disableAllButtons()
{
    for (var i = 0; i < $$('input.button').length; i++)
    {
        $$('input.button')[i].disabled = true;
    }
}

 /**
 * Loops through all input.button elements and enables them.
 * Used when submitting forms.
 */
function enableAllButtons()
{
    for (var i = 0; i < $$('input.button').length; i++)
    {
        $$('input.button')[i].disabled = false;
    }
}

/**
* Displays a "Please wait" popup on e.g. form submission.
 *
 * @paaram string messageText Text to display - defualts to txt['save_wait_msg'] if not set
*
* @global string txt['save_wait_msg'] The message for the pop up dialogue on form submission.
*/
function displayLoadPopup(messageText)
{
    // Sets the default value for messageText if it's no passed
    messageText = setDefaultParameter(messageText, txt['save_wait_msg']);

    if (GetFloatingDiv('loading_popup') == undefined)
    {
        var loadingPopup = AddNewFloatingDiv('loading_popup');
        var contents = '<div style="width:200px; text-align:center; overflow:hidden;">' + messageText + '</div>';
        // preload loading image to prevent problems with it not displaying
        var loadingImage = new Image();
        loadingImage.src = 'Images/loading.gif';
        contents += '<img src="Images/loading.gif" style="margin-left:90px; margin-top:10px;" />';
        loadingPopup.setContents(contents);
        loadingPopup.hideCloseControl();
        loadingPopup.display();
    }
}

/**
* Removes the loading popup.
*/
function hideLoadPopup()
{
    var loadingPopup = GetFloatingDiv('loading_popup');

    if (loadingPopup !== undefined)
    {
        loadingPopup.CloseFloatingControl();
    }
}

/**
* Displays a pop up dialogue with a 'Cancel' button which gives users the
* opportunity to cancel AJAX calls if they're taking too long.
*
* @param object request The AJAX request object.
*/
function displayWaitPopup(request)
{
    var waitPopup = AddNewFloatingDiv('wait_popup');
    var contents = '<div style="width:200px; text-align:center; overflow:hidden;">' + txt['ajax_wait_msg'] + '</div>';

    // preload loading image to prevent problems with it not displaying
    var loadingImage = new Image();
    loadingImage.src = 'Images/loading.gif';
    contents += '<img src="Images/loading.gif" style="margin-left:90px; margin-top:10px;" />';
    waitPopup.setContents(contents);
    waitPopup.hideCloseControl();

    var buttons = new Array();
    buttons.push({'value':'Cancel','onclick':''});
    waitPopup.setButtons(buttons);

    waitPopup.display();

    // add an event listener to the Cancel button to abort the AJAX request
    Event.observe('button_0', 'click', function(event)
    {
        event.stop();

        // NB aborting the request still results in the onSuccess event being fired,
        // so the wait dialogue is closed on this event in Ajax.Responders (as it would be if the request completed successfully)
        request.transport.abort();

        // Run the onFailure method if we set one up when creating the AJAX object
        if (request.options['onFailure'])
        {
            request.options['onFailure'](request.transport, request.json);
        }
    });
}

 /**
 * Either checks or unchecks all checkboxes with a particular id.
 * Loops through all elements in a form and checks/unchecks matching boxes.
 * (could probably be rewritten using prototype/jquery notation).
 *
 * @param string check_id_name: id of the elements to change
 * @param bool check: value to set the "checked" value of the checkboxes to.
 */
function ToggleCheckAll(check_id_name, check)
{
    for (var i = 0, l = document.forms[0].length; i < l; i++)
    {
        if (document.forms[0].elements[i].type == "checkbox" && document.forms[0].elements[i].id == check_id_name)
        {
            document.forms[0].elements[i].checked = check
        }
    }
}

 /**
 * Either checks or unchecks all checkboxes with a particular class.
 * Loops through all elements in a form and checks/unchecks matching boxes.
 *
 * @param string checkbox_class: class of the elements to change
 * @param bool check: value to set the "checked" value of the checkboxes to.
 */
function ToggleCheckAllClass(checkbox_class, check)
{
    if (check)
    {
        jQuery('.'+checkbox_class).attr('checked','checked');
    }
    else
    {
        jQuery('.'+checkbox_class).removeAttr('checked');
    }
}

function checkForEnter()
{
    if (event.keyCode == "13")
    {
        return true;
    }
    else
    {
        return false;
    }
}

function SelectCodeOnEnter(ctrlidx)
{
    if (checkForEnter())
    {
        GlobalCodeSelectControls[ctrlidx].makeSelection(this);
    }
}

function moveUp(obj)
{
    obj = (typeof obj == "string") ? document.getElementById(obj) : obj;

    if (obj.tagName.toLowerCase() != "select" && obj.length < 2)
    {
        return false;
    }

    var sel = new Array();

    for (var i = 0; i < obj.length; i++)
    {
        if (obj[i].selected == true)
        {
            sel[sel.length] = i;
        }
    }

    for (i in sel)
    {
        if (sel[i] != 0 && obj[sel[i]-1])
        {
            var tmp = new Array((document.body.innerHTML ? obj[sel[i]-1].innerHTML : obj[sel[i]-1].text), obj[sel[i]-1].value, obj[sel[i]-1].style.color);

            if (document.body.innerHTML)
            {
                obj[sel[i]-1].innerHTML = obj[sel[i]].innerHTML;
            }
            else
            {
                obj[sel[i]-1].text = obj[sel[i]].text;
            }

            obj[sel[i]-1].value = obj[sel[i]].value;
            obj[sel[i]-1].style.color = obj[sel[i]].style.color;

            if (document.body.innerHTML)
            {
                obj[sel[i]].innerHTML = tmp[0];
            }
            else
            {
                obj[sel[i]].text = tmp[0];
            }

            obj[sel[i]].value = tmp[1];
            obj[sel[i]].style.color = tmp[2];
            obj[sel[i]-1].selected = true;
            obj[sel[i]].selected = false;
        }
    }

    reDrawPreview();
}

function moveDown(obj)
{
    obj = (typeof obj == "string") ? document.getElementById(obj) : obj;

    if (obj.tagName.toLowerCase() != "select" && obj.length < 2)
    {
        return false;
    }

    var sel = new Array();

    for (var i = obj.length-1; i > -1; i--)
    {
        if (obj[i].selected == true)
        {
            sel[sel.length] = i;
        }
    }

    for (i in sel)
    {
        if (sel[i] != obj.length-1 && obj[sel[i]+1])
        {
            var tmp = new Array((document.body.innerHTML ? obj[sel[i]+1].innerHTML : obj[sel[i]+1].text), obj[sel[i]+1].value, obj[sel[i]+1].style.color);

            if (document.body.innerHTML)
            {
                obj[sel[i]+1].innerHTML = obj[sel[i]].innerHTML;
            }
            else
            {
                obj[sel[i]+1].text = obj[sel[i]].text;
            }

            obj[sel[i]+1].value = obj[sel[i]].value;
            obj[sel[i]+1].style.color = obj[sel[i]].style.color;

            if (document.body.innerHTML)
            {
                obj[sel[i]].innerHTML = tmp[0];
            }
            else
            {
                obj[sel[i]].text = tmp[0];
            }

            obj[sel[i]].value = tmp[1];
            obj[sel[i]].style.color = tmp[2];
            obj[sel[i]+1].selected = true;
            obj[sel[i]].selected = false;
        }
    }

    reDrawPreview();
}


 /**
 * Creates a custom datix popup with a title bar, main display area and buttons.
 *
 * @param string title: Title of the popup (appears in title bar)
 * @param string message: Main body of the popup - can contain HTML
 * @param array buttons: in format {'value': button label, 'onclick': javascript action to take}
 */
function DivAlert(title, message, buttons, onclose)
{
    var suffix = PopupDivs.length;

    AlertWindow = AddNewFloatingDiv('alert'+suffix);

    message = '<div>'+message+'</div>';

    AlertWindow.setContents(message);
    AlertWindow.setTitle(title);

    AlertWindow.maxWidth = 300; //popups will be restricted to this width unless the buttons are wider than this.

    if (buttons.length == 0) //add a default "ok" button
    {
        buttons.push({'value': 'OK', 'onclick': 'GetFloatingDiv(\'alert'+suffix+'\').CloseFloatingControl()'});
    }
    else
    {
        for (var i = 0; i < buttons.length; i++)
        {
            if (!buttons[i].onclick)
            {
                buttons[i].onclick = 'GetFloatingDiv(\'alert'+suffix+'\').CloseFloatingControl()';
            }
        }
    }

    AlertWindow.setButtons(buttons);

    if (onclose)
    {
        AlertWindow.OnCloseAction = onclose;
    }

    AlertWindow.display();
}

function reDrawPreview()
{
    var columns = jQuery('#columns_list2 option');

    var table = '<table id="listing_preview_table" class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0"><tr name="element_section_row" id="element_section_row">';

    for (var i = 0; i < columns.length; i++)
    {
        var width = 0;

        if (jQuery('#col_width_'+columns[i].value).val())
        {
            width = jQuery('#col_width_'+columns[i].value).val();
        }

        table += '<th id="col_heading_'+columns[i].value+'" class="windowbg" width="'+width+'%"><b>'+columns[i].text+'</b></th>';
    }

    table += '</tr><tr>';

    for (var i = 0; i < columns.length; i++)
    {
        var width = 0;

        if (jQuery('#col_width_'+columns[i].value).val())
        {
            width = jQuery('#col_width_'+columns[i].value).val();
        }

        table += '<td class="windowbg" style="text-align:center"><input id="col_width_'+columns[i].value+'" name="col_width_'+columns[i].value+'" type="string" size="2" maxlength="3" value="'+width+'" onchange="jQuery(\'#col_heading_'+columns[i].value+'\').width(this.value.toString()+\'%\')"></td>';
    }

    table += '</tr></table>';

    jQuery('#listing_preview_table').remove();
    jQuery('#listing_preview_table_wrapper').append(table);
}


/**
Reformats the NHS Number field contents to XXX XXX XXXX.

@param object field:    jQuery object representing the NHS Number field
*/
function formatNhsNo(field)
{
    if (field.val() != 'undefined')
    {
        var oldValue = field.val().replace(/\s/g, '');

        var newValue = oldValue.substr(0, 3);

        if (oldValue.length > 3)
        {
            newValue += ' ' + oldValue.substr(3, 3);
        }

        if (oldValue.length > 6)
        {
            newValue += ' ' + oldValue.substr(6,4);
        }

        field.val(newValue);

    }
}
/**
 *  Used to decide whether a value is valid for an "Order" field.
 *
 **/
function IsPositiveInteger(myfield, e, dec)
{
    var key;
    var keychar;

    if (window.event)
    {
       key = window.event.keyCode;
    }
    else if (e)
    {
       key = e.which;
    }
    else
    {
       return true;
    }

    keychar = String.fromCharCode(key);

    // control keys
    if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27))
    {
       return true;
    }
    // numbers
    else if ((("0123456789").indexOf(keychar) > -1))
    {
       return true;
    }

    return false;
}

/**
 * Sends the browser to a particular URL - function used so we can intercept/audit this later if necessary.
 *
 * 31/07/2012 - Added parameter target to open URL on a new tab/window
*/
function SendTo(url, target)
{
    // add CSRF attack prevention token if defined
    if (typeof token != 'undefined' && token.length > 0 && !(url.search(".htm") > 0))
    {
        var parts = url.split('#');
        var hasQueryString = parts[0].indexOf('?') > -1;

        if (parts[0].length > 0) {
            parts[0] = parts[0] + (hasQueryString ? '&':'?') + 'token=' + token;
        }

        url = parts.length > 1 ? parts.join('#') : parts.join();
    }

    // redirect
    if (target)
    {
        window.open(url, target);
    }
    else
    {
        window.location.href = url;
    }
}

/**
* Used to toggle TextAreaWithEdit fields into edit mode.
*
* @param string  id            The field id.
* @param object  link          The DOM object represending the edit link which was clicked.
* @param boolean spellchecker  Whether or not the spellchecker should be enabled.
*/
function editTextArea(id, spellchecker)
{
    var Editorclass = spellchecker ? "spellcheck" : "";
    jQuery("#"+id).attr({
        'title': "",
        'class': Editorclass
    }).show();

    initialiseSpellchecker();

    jQuery("#"+id+"_editlink, #"+id+"_readonly").hide();
}

function DeleteFormDesignSection(id)
{
    jQuery('#EXTRASECTIONS-'+id+'-'+id).val('');

    submitClicked = true;
    document.forms[0].btnSaveDesign.click();
}

function addNewFormDesignSection(id)
{
    var input = document.createElement('input');
    input.type='hidden';
    input.id='EXTRASECTIONS-'+id+'-'+id;
    input.name='EXTRASECTIONS-'+id+'-'+id;
    input.value= 'section id';

    document.forms[0].appendChild(input);

    submitClicked = true;
    document.forms[0].btnSaveDesign.click();
}

function getFieldForBatchUpdate(module, fieldSelectId, suffix)
{
    var HTML = null;

    jQuery.getJSON(scripturl+'?service=batchupdate&event=getfieldhtml_json&module='+module+'&suffix='+suffix+'&field='+jQuery('#'+fieldSelectId).val(),
        function(data)
        {
            HTML = SplitJSAndHTML(data.html_old.toString());
            jQuery('#old_field_select_wrapper_'+suffix).html(HTML);
            HTML = SplitJSAndHTML(data.html_new.toString());
            jQuery('#new_field_select_wrapper_'+suffix).html(HTML);
            AppendJavascript(data.js);
            RunGlobalJavascript();
        }
    );
}

function AddAnotherBatchUpdateRow(module, suffix)
{
    jQuery.getJSON(scripturl+'?service=batchupdate&event=getbatchupdaterow_json&module='+module+'&suffix='+suffix,
        function(data)
        {
            var HTML = SplitJSAndHTML(data.html.toString())
            jQuery('#batch_update_table tr:last').before(HTML);

            AppendJavascript(data.js);
            RunGlobalJavascript();
        }
    );
}

function getSavedQueryClauseForTrigger(query_id)
{
    new Ajax.Request
    (
        scripturl + '?action=httprequest&type=getquerywhere&responsetype=json&recordid=' + query_id,
        {
            method: 'get',
            onSuccess: function(r)
            {
                var data = r.responseText.evalJSON();

                jQuery('#trg_expression_row > div.field_input_div').html(data["where"]+'<input type="hidden" name="trg_expression" value="'+data["where"]+'" /><input type="hidden" name="CHANGED-trg_expression" id="CHANGED-trg_expression" value="" />');
            },
            onFailure: function()
            {
                alert('Error retrieving query...');
            }
        }

    );
}

function checkBatchUpdateInfo()
{
    if (jQuery('#field_select').val() == '')
    {
        OldAlert('You need to choose a field to update');
        return false
    }

    return true;
}

function addJavascriptFile(jsname,pos)
{
    var th = document.getElementsByTagName(pos)[0];
    var s = document.createElement('script');
    s.setAttribute('type','text/javascript');
    s.setAttribute('src',jsname);
    th.appendChild(s);
}

String.prototype.contains = function(t) { return this.indexOf(t) >= 0 ? true : false }

/*
Function: OldAlert
Creates a standard javascript alert box. Needed because we are hijacking "alert()" to use for our own custom popups.
 */
OldAlert = window.alert;

window.alert = new Function ("divAlertFunc()");

divAlertFunc = function(s)
{
    if (document.body)
    {
        DivAlert(txt['alert_title'], s, new Array(), null);
    }
    else
    {
        OldAlert(s);
    }
}

window.alert = divAlertFunc;

/**
* Creates the dialogue which is displayed when uploading table data.
*/
function importTableData(ccs2enabled)
{
    /**
     * Find the Import button and blur focus so that it can't be triggered when the floating div is active.
     */
    jQuery('input').filter(':button').trigger('blur');

    var file = jQuery("#importfile").val();

    // check to see if upload file is selected
    if (file == '')
    {
        alert('Please select a load file');
        return;
    }

    // check to see that upload file has .txt extension
    if (file.substr(file.length - 4) != ".txt")
    {
        alert('The load file must be of type .txt');
        return;
    }

    // create upload dialogue
    var importDialogue = AddNewFloatingDiv('import_dialogue');
    importDialogue.setContents('<div id="uploading" style="width:200px; height:50px; margin-top:15px;"><div style="text-align:center;">Loading data</div><img src="Images/loading.gif" style="margin-left:90px; margin-top:10px;" /></div>');
    importDialogue.setTitle(txt['import_table_data']);
    importDialogue.hideCloseControl();
    importDialogue.display();

    // create iframe used to upload and process file
    jQuery("<iframe id=\"import_iframe\" name=\"import_iframe\" />")
        .css({
            "width" : "0",
            "height" : "0",
            "border" : "none"
        })
        .bind("load", function(){
            var message = jQuery(this).contents().text()
                html = "";

            if (message == "")
            {
                // the only messages passed back are errors, so we can assume success if blank
                message = "File import completed successfully.";
            }

            // Shows a dialog box if we are import CCS2 loadfiles with a version previous to the one that's on the system
            if (message == 'CCS2')
            {
                if (confirm("You're attempting to upload an old version of CCS2, are you sure you want to continue?"))
                {
                    var div = GetFloatingDiv('import_dialogue');

                    div.CloseFloatingControl();
                    jQuery(this).remove();
                    importTableData('ccs2');
                }
                else
                {
                    var div = GetFloatingDiv('import_dialogue');

                    div.CloseFloatingControl();
                    jQuery(this).remove();
                }
            }
            else
            {
                html = '<div style="text-align:center;">'+message+'</div"><div><input type="button" value="OK" onClick="var div = GetFloatingDiv(\'import_dialogue\');div.CloseFloatingControl()" style="margin-top:10px;" /></div>';

                jQuery("#uploading").html(html).find('input').filter(':button').trigger('focus');
                jQuery(this).remove();
            }
        })
        .appendTo('body');

    // submit file by posting to iframe
    jQuery(document.forms[0])
        .attr({
            target:  "import_iframe",
            action:  scripturl + "?action=httprequest&type=importtabledata&responsetype=json" + ((ccs2enabled && ccs2enabled == 'ccs2') ? '&ccs2=verified' : '')
        })
        .submit();
}

function ClearReadOnlyField(field)
{
    jQuery('#'+field+'_title').text('');
    jQuery('#'+field).val('');
    FieldChanged(field);
}

function SetSessionValue(key, value)
{
    jQuery.ajax({url: scripturl + '?action=httprequest&type=setsession&var='+key+'&value='+value, type: 'POST'});
}

function SetTimezoneValue(action)
{
    jQuery.ajax({url: scripturl + '?action=httprequest&type=setsession&var=Timezone&value='+new Date().getTimezoneOffset()/60, type: 'POST'})
        .done(function(){ window.location.reload(true); });
}

function UnlockRecord(table, recordid)
{
    //If not async, this doesn't work for next/prev navigation buttons, for some reason
    jQuery.ajax({url: scripturl + '?action=httprequest&type=unlockrecord&table='+table+'&link_id='+recordid, type: 'POST', async: false});
}

/**
* Retrieves querystring parameters.
*
* @param  string  name  The parameter name
*
* @return string        The parameter value
*/
function getQueryStringParm(name)
{
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);

    if (results == null)
    {
        return "";
    }
    else
    {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

/**
 * Escapes special characters in ids before being used by jquery
 * @param url
 */
function jqescape(myid)
{
    return myid.replace(/(\/)/g,'\\$1');
    return myid.replace(/(|)/g,'\\$1');
    return myid.replace(/(:)/g,'\\$1');
    return myid.replace(/(\.)/g,'\\$1');
}

/**
* Popups a the url in a new window.
*
* @param string url to be opened in new window
*
*/
function popitup(url)
{
    if (url.length == 0)
    {
        return;
    }

    if (!url.match('http://') && !url.match('https://'))
    {
        url = 'http://' + url;
    }

    SendTo(url, '_blank');
}

/**
 * Appends or replaces an existing query parameter in the current href and reloads the page
 *
 * @param String parameter The name of the parameter to append or replace
 * @param String value The value of the parameter
 * @return void
 */
function setQueryStringParam(parameter, value)
{
	var location = window.location.href;
	var pattern  = new RegExp('(&|\\?)(' + parameter + '=)[a-z]+(&|$)', 'i');

	if (location.match(pattern))
    {
		location = location.replace(pattern, '$1$2' + value + '$3');
	}
    else
    {
		location += '&module=' + value;
	}

	window.location = location;
}

/**
 * Disables the onclick event of the options span on the Dashboard panel
 * @param integer recordId
 */
function disableDashboardOptions(recordId)
{
    jQuery('#dashboard_options_'+recordId).attr('disabled','disabled');
    setTimeout('enableDashboardOptions(\'' + recordId + '\')', 2000);
}

/**
 * Enables the onclick event of the options span on the Dashboard panel
 * @param integer recordId
 */
function enableDashboardOptions(recordId)
{
    jQuery('#dashboard_options_'+recordId).removeAttr ('disabled');
}

/**
 * Validates the Step numbers when defining an Action Chain
 */
function validateSteps()
{
    var steps = null, stepsValues = new Array(), error = null;

    steps = jQuery('input[name^=acs_order]');

    if (steps)
    {
        steps.each(function (index, obj) {
            stepsValues.push(obj.value);
        });

        if (stepsValues.length > 0)
        {
            if (findDuplicatesInArray(stepsValues) === true)
            {
                error = txt['validate_step_numbers_error_msg'];
                alert(error);
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    return true;
}

/**
* Checks page for listing checkboxes and arranges them into a string to pass via GET
*/
function getListingCheckboxStateString()
{
    urlstring = '';

    if (jQuery('#listing_checkbox_select_all').val() != '')
    {
        urlstring += '&listing_checkbox_select_all='+jQuery('#listing_checkbox_select_all').val();
    }

    jQuery('.listing_checkbox').each(function(i, checkbox) {
        urlstring += '&flags['+checkbox.id.replace('listing_checkbox_', '')+']=';

        if (checkbox.checked)
        {
            urlstring += '1';
        }
        else
        {
            urlstring += '0';
        }
    });

    return urlstring;
}

/**
 * Gets all checked checkboxes matching name and joins the values 
 * in a symbol separated string
 */
function getCheckedCheckboxesValuesAsString (name, symbol)
{
    symbol = typeof symbol !== 'undefined' ? symbol : ' ';

    var checked = [];

    jQuery("input[name='"+name+"[]']:checked").each(function () { 
        checked.push(jQuery(this).val()); 
    });

    return checked.join(symbol);
}

/**
 * Sets a field value with the concatenation of checked checkboxes values
 */
function updateFieldWithCheckboxesValues (id)
{
    // note: checkboxes should have the name like {id}_options
    var value = getCheckedCheckboxesValuesAsString (id+'_options', ' ');
    jQuery('#'+id).val(value);
}

/**
* Used when distributing assigned assessment templates in accreditation.
* Prompts for a value for the field adm_year and then redirects to the next page in the process.
*/
function PromptForAdmYear()
{
    jQuery.ajax({
        url: scripturl+'?action=httprequest&type=getadmyearprompttext',
        success: function(data)
        {
            var sHTML = SplitJSAndHTML(data);

            AlertWindow = AddNewFloatingDiv('PromptAdmYear');

            AlertWindow.setContents(sHTML);
            AlertWindow.setTitle("Choose starting assessment period");
            AlertWindow.setWidth('500px');

            var buttonHTML = '';
            var buttons = new Array();

            buttonHTML += '<input type="button" value="Okay" onClick="if(jQuery(\'#adm_year\').val() != \'\'){SendTo(scripturl+\'?action=list&module=LOC&creatingassignments=1&sidemenu=ACR&adm_year=\'+jQuery(\'#adm_year\').val())}else{OldAlert(\'You must choose an assessment period.\')};" />';
            buttonHTML += '<input type="button" value="Cancel" onClick="var div = GetFloatingDiv(\'PromptAdmYear\');div.CloseFloatingControl();" />';
            AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);
            AlertWindow.display();
            RunGlobalJavascript();
        }
    });
}


/**
 * Finds duplicated values in an array
 * @param Array arr
 */
function findDuplicatesInArray(arr)
{
    var len = arr.length, out = [], counts = {};

    for (var i = 0; i < len; i++)
    {
        var item = arr[i];
        var count = counts[item];
        counts[item] = (counts[item] >= 1) ? counts[item] + 1 : 1;
    }

    for (var item in counts)
    {
        if (counts[item] > 1)
        {
            out.push(item);
        }
    }

    if (out.length == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * 
 * @param id ID of the padlock img next to the field
 * @param ob refrence to the field
 * @param dependency array of ID padlock images that should have the same lock status
 */
function changeLock (id, ob, dependency)
{
    var img = 'Images/icons/lock.png';
    var tip = txt['locked'];
    var value = 1;

    if(jQuery('#'+id).val() == '1')
    {
        img = 'Images/icons/lock_open.png';
        tip = txt['unlocked'];
        value = 0;
    }


    jQuery('#'+id).val(value);

    jQuery('#'+id+'-img').attr({
        'src': img,
        'alt': tip,
        'title': tip
    });

    for (var i in dependency)
    {
        jQuery('#'+dependency[i]).val(0);
        jQuery('#'+dependency[i]+'-img').attr({
            'src': img,
            'alt': tip,
            'title': tip
        });
    }

}

function dnChooserPopup(form_element,rdn)
{
    mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
    mywindow.location.href = scripturl+'?action=ldapbrowse&form_element=' + form_element + '&rdn=' + rdn + '&token=' + token;
    if (mywindow.opener == null)
    {
        mywindow.opener = self;
    }
}

/**
 * Remove all fields which are still with class is_disabled
 */
function RemoveDisabledFields()
{
    // Find all fields with class is_disabled
    jQuery('input.is_disabled, textarea.is_disabled, select.is_disabled').each(function(index) {
        // And remove them entirely
        jQuery(this).remove();
    });

    return true;
}

/**
 * Adds an input hidden field to the form
 *
 * @param inputID The ID of the field.
 */
function addInputHidden(inputID)
{
    if (jQuery("#"+inputID).length <= 0)
    {
        jQuery("<input>").attr({
            type: "hidden",
            id: inputID,
            name: inputID
        }).appendTo("form");
    }
}

/**
 * Sets the value of the input hidden SECTIONEXPANDED to 1 to allow keeping the section expanded between form
 * design saves.
 */
function setSectionsExpanded()
{
    var sections = null,
        section = null,
        sectionName = null;

    sections = jQuery("img[id^=twisty_image]");

    sections.each(function() {
        if (jQuery(this).attr("src") == "Images/collapse.gif")
        {
            section = jQuery(this).attr("id").split('_');
            sectionName = section[2];
            jQuery("#SECTIONEXPANDED-"+sectionName+"-"+sectionName).val("1");
        }
    });
}

/**
 * Executes when clicking the copy button on the copy record(s) screen.
 *
 * @param int selectedRecords Number of records selected to copy.
 */
function copyBtnOnClick(selectedRecords)
{
    var numRecords = 0;

    numRecords = selectedRecords * jQuery("#copies").val();

    if (confirm('This process will create '+numRecords+' new records. Are you sure you want to create these copies?'))
    {
        displayLoadPopup();
        document.forms[0].rbWhat.value = 'copy';
        document.forms[0].submit();
    }
}

/**
 * Adds a datepicker to an objId or an objClass.
 *
 * @param $target The jQuery object that should have a datepicker.
 * @param calendarOnClick The value of the global CALENDAR_ONCLICK.
 * @param dateFormat The format of the date that should be displayed by the calendar.
 * @param weekStartDay The value of the global WEEK_START_DAY.
 * @param calendarWeekend The value of the global CALENDAR_WEEKEND.
 */
function createCalendar($target, calendarOnClick, dateFormat, weekStartDay, calendarWeekend) {

    if( ! globals.deviceDetect.isTablet || ($target.attr('type') == 'date' && ! Modernizr.inputtypes.date)) {

        if (calendarOnClick == 'Y') {

            $target.each(function(){

                $elem = jQuery(this);

                $elem.datepicker({
                    dateFormat: dateFormat,
                    firstDay: (parseInt(weekStartDay) > 0 ? parseInt(weekStartDay) - 1 : 6),
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    showButtonPanel: true,
                    constrainInput: false,
                    changeMonth: true,
                    changeYear: true,
                    maxDate: ($elem.data('not-future') ? 0 : null),
                    beforeShowDay: function(date) {

                        // As we were utilising a bug in jQUery UI to set weekends before, we need to convert it to the proper]
                        // date number that we want to use
                        weekendDays = [(parseInt(calendarWeekend) + 5) % 7, (parseInt(calendarWeekend) + 6) % 7];

                        var weekend = false;

                        if(jQuery.inArray(date.getDay(), weekendDays) > -1) {

                            weekend = true;
                        }
                        return [true, weekend ? 'highlight-weekend' : ''];
                    }
                });
            });
        }
        else {

            $target.each(function(){

                $elem = jQuery(this);

                $elem.datepicker({
                    showOn: "button",
                    buttonImage: "Images/calendar.gif",
                    buttonImageOnly: true,
                    buttonText: 'Select date',
                    dateFormat: dateFormat,
                    firstDay: (parseInt(weekStartDay) > 0 ? parseInt(weekStartDay) - 1 : 6),
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    showButtonPanel: true,
                    constrainInput: false,
                    changeMonth: true,
                    changeYear: true,
                    maxDate: ($elem.data('not-future') ? 0 : null),
                    beforeShowDay: function(date) {

                        // As we were utilising a bug in jQUery UI to set weekends before, we need to convert it to the proper]
                        // date number that we want to use
                        weekendDays = [(parseInt(calendarWeekend) + 5) % 7, (parseInt(calendarWeekend) + 6) % 7];

                        var weekend = false;

                        if(jQuery.inArray(date.getDay(), weekendDays) > -1) {

                            weekend = true;
                        }
                        return [true, weekend ? 'highlight-weekend' : ''];
                    }
                });
            });
        }
    }
    else {

        jQuery('.field_date_format').hide();
    }
}

/**
 * This function has no reason to exist, but the code that makes merged level-1 contacts is all inside an inline onclick
 * attribute, necessitating this to handle the quotes properly.
 */
function getCheckboxByName(Field)
{
    return jQuery('input[name="'+Field+'_options[]"]:checkbox');
}

/**
 * This function has no reason to exist, but the code that makes merged level-1 contacts is all inside an inline onclick
 * attribute, necessitating this to handle the quotes properly.
 */
function getCheckboxByNameAndValue(Field, Value)
{
    return jQuery('input[name="'+Field+'_options[]"][value='+Value+']:checkbox');
}


/**
 * destorys instance of qTip and removes any associated classes added to the DOM
 *
 * used to removed instances of qTip completely and clean up and classes added to elements
 */

function destroyQtips() {

    if(qTips.length) {

        for(x in qTips) {

            for(var i = 0; i < qTips[x].length; i++) {

                qTips[x][i].destroy();
            }
        }
        jQuery('.qtip').remove();
    }
}

/**
 * Initialises the spellchecker
 */
function initialiseSpellchecker() {

    if(globals['WEB_SPELLCHECKER'] &&  ! globals.deviceDetect.isTablet) {

        /**
         * Override document.write functions so they can be used within jQuery.ready()
         * @param content
         */
        document.write = function(content) {
            spellcheckButton = content;
        };

        document.writeln = function(content) {
            spellcheckButton = content;
        };

        jQuery('.spellcheck').filter(':visible').each(function() {

            var $target = jQuery(this),
                targetId = $target.attr('id');

            /**
             * Check if the target textarea already has the spellcheck initialised for it
             */
            if(!$target.siblings('.spellcheck-icon').length){

                spellcheck[targetId] = new LiveSpellInstance();
                spellcheck[targetId].ServerModel = 'php';
                spellcheck[targetId].Fields = targetId;
                spellcheck[targetId].DrawSpellImageButton(false, '../../Images/icon_spellcheck.png', '../../Images/icon_spellcheck.png', 'Open spellchecker', 'spellcheck-icon');

                /**
                 * Remove activateAsYouType option for IE < 9 as it causes odd issues with cursor position while you type
                 */
                if(!browser.isIE || browser.isIE && browser.version > 8) {

                    spellcheck[targetId].ActivateAsYouType();
                }

                $target.after(spellcheckButton);
            }
        });
    }
}

function initialiseCalendars() {

    //TODO need to make sure this is only used in the templatecontroller context
    if(jQuery.type(globals) === 'object' && ! jQuery.isEmptyObject(globals) && globals.calendarOnclick && globals.dateFormat && globals.calendarWeekend) {

        createCalendar(jQuery('.date'), globals.calendarOnclick, globals.dateFormat, globals.weekStartDay, globals.calendarWeekend);
    }
    else if(typeof(doCreateCalendar) === 'function'){

        /**
         * This is because we don't have access to the other globals values for setting up the calendar outside of a controller
         * context, so this triggers the original function build in subs.php using values set in PHP
         */
        doCreateCalendar();
    }
}

  /**
   * Unformats a currency string so that it can be used in number calculations. Strips currency symbols and , separators
   * and returns a float eg. £10,000.50 -> 10000.50
   *
   * @param value The formatted currency string to be unformatted
   * @returns {*}
   */

function unformatCurrency(value) {

  if(value) {

      var decMatch = value.match(/(.+?)(\.[0-9]{1,2})?$/),
      decimalValue = typeof decMatch[2] != 'undefined' ? decMatch[2] : '',
      regex = new RegExp("[^0-9\.-]", ["g"]),
      amount = parseInt(decMatch[1].replace(regex, ''), 10) || 0;

      return parseFloat(amount + decimalValue);
  }
  else {

      return 0;
  }
}

  /**
   * Formats a numerical value as currecy, adds currency symbol and thousand separators eg. 10000.50 -> £10,000.50
   *
   * @param value The numerical value to be formatted as a currency value
   * @returns {string}
   */

function formatCurrency(value) {
      
  return globals.currencyChar + value.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
* Funciton to set default parameters for javascript functions. This can be used to allow functions to have optional
* parameters which default to a certain value when not set on call.
* Use like this:
* function newFunction(requiredParam, optionalParam) {
*
*   optionalParam = setDefaultParameter(optionalParam, 'default value');
*
*   ...
* }
* @param paramValue
* @param defaultValue
* @returns {*}
*/
function setDefaultParameter(paramValue, defaultValue) {

    return typeof paramValue !== 'undefined' ? paramValue : defaultValue;
}

/**
 * Fix for IE not having console defined
 */

if (typeof console == "undefined") {

    window.console = {
        log: function () {}
    };
}