function XMLObject(async)
{
	if (!async)
	{
		async = false;
	}

	if (browser.isIE)  //IE
	{
		var xml = new ActiveXObject("Microsoft.XMLDOM")
		xml.async = async;
	}
	else if (browser.isNS || browser.isOpera)
	{
		var xml = document.implementation.createDocument("","",null);
		xml.async = async;
	}

	this.xmlDocument = xml;
}

XMLObject.prototype.loadXML = function(strXMLData)
{
	if (browser.isIE)  //IE
	{
		this.xmlDocument.loadXML(strXMLData);
	}
	else if (browser.isNS || browser.isOpera)  //for FF
	{
		var oParser = new DOMParser()
		this.xmlDocument = oParser.parseFromString(strXMLData, 'text/xml');
	}
}

XMLObject.prototype.load = function(xmlFile)
{
	this.xmlDocument.load(xmlFile);
}

XMLObject.prototype.transform = function(xsl)
{
	if (browser.isIE)  //IE
	{
		var htmlResult = this.xmlDocument.transformNode(xsl);
	}
	else if (browser.isNS || browser.isOpera)  //for FF
	{
		var oProcessor = new XSLTProcessor()
		oProcessor.importStylesheet(xsl);

		// Transform
		var xmlDocResult = oProcessor.transformToDocument(this.xmlDocument);
		var htmlResult = (new XMLSerializer()).serializeToString(xmlDocResult);
	}

	return htmlResult;
}

