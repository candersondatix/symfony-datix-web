// File to deal with js functions for listing pages

function FlagRecord(module, id)
{
    jQuery.ajax({
        url: scripturl + "?action=httprequest&type=flagrecord&module="+module+"&recordid="+id
    });
}

function FlagAllRecords(module)
{
    jQuery.ajax({
        url: scripturl + "?action=httprequest&type=flagallrecords&module="+module
    });
}

function UnFlagRecord(module, id)
{
    jQuery.ajax({
        url: scripturl + "?action=httprequest&type=unflagrecord&module="+module+"&recordid="+id
    });
}

function UnFlagAllRecords(module)
{
    jQuery.ajax({
        url: scripturl + "?action=httprequest&type=unflagallrecords&module="+module
    });
}

jQuery(function( $ ) {

    $('#listing_checkbox_all').on('change', function() {

        if($(this).is(':checked')) {

            $('#listing_checkbox_select_all').val('1');
            $('.listing_checkbox').not(':disabled').prop('checked', true);
        }
        else {

            $('#listing_checkbox_select_all').val('0');
            $('.listing_checkbox').prop('checked', false);
        }
    });

    $('.listing_checkbox').on('change', function() {

        var $checkAll = $('#listing_checkbox_all');

        if( ! $(this).is(':checked') && $checkAll.is(':checked')) {

            $('#listing_checkbox_select_all').val('2');
            $checkAll.prop('checked', false);
        }
    });
});