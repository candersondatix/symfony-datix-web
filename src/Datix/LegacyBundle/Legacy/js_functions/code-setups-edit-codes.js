var colorpicker, colorBox;

// create a dynamic function from settings passed from PHP
window[codeSetups.codeTable + '_FormInit'] = function() {
// initialize color picker
	var colorpicker=new Rico.ColorPicker('colorpicker1');
	colorpicker.atLoad();
	colorpicker.returnValue=ProcessColorSelection;
	colorBox=$(codeSetups.codeTable + '_6');
	RicoEditControls.register(colorpicker, Rico.imgDir+'dotbutton.gif');
}

function ProcessColorSelection(newVal) {
	colorBox.value=newVal;
	// set text box background to the selected color
	colorBox.style.backgroundColor=newVal;
	colorBox.style.color= Prototype.Browser.WebKit ? 'black' : TextColor(newVal);
}

// choose black or white text - whichever gives the best contrast
function TextColor(hexval) {
	var objColor=Rico.Color.createFromHex(hexval);
	return (objColor.rgb.g > 160 || objColor.rgb.r+objColor.rgb.g+objColor.rgb.b > 480) ? 'black' : 'white';
}