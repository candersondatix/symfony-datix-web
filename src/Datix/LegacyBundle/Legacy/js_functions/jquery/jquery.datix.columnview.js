;(function($, window, document, undefined) {
	$.widget("datix.columnview", {
		disabled: false,
		
		options: {},
		
		minColumnWidth: 202, 
        
		nodes: [],
		
		activeTier: -1,
		
		activeNode: null,
		
		_create: function() {
			var children,
				self = this,
				path;
				
			this.element.css('height', $(window).height() - this.element.offset().top - $('#F').height());
			this.nodes = this.options.nodes;
			
			children = this.getNodesAtDepth(0);
			
			if (children) {
				this._addColumn(children);
			} else {
				// TODO do something to cater for blank slate scenario
			}
			
            // add click event to column items
			this.element.on('click', 'li', function(e) {

				self.activeNode = $(e.currentTarget);
				self.activeTier = self.activeNode.parentsUntil(self.element, '.column').prev().index();
				self.next();
				e.preventDefault();
			});
			
			if (this.options.selectedLocation) {
				// grab path to node
				path = this.getPathToNode(this.options.selectedLocation);
				
				// for each node in path except for last, trigger click function
				$.each(path, function(i, value) {
					$('#location-' + this.recordid).click();
				});
			}
		},
		
		_addColumn: function(children) {
			var column,
				self = this;

			column  = $('<ul class="column" />').css({
				width: this.minColumnWidth
			}).appendTo(this.element);
				
			// add children to div
			$.each(children, function(i, child) {
				self._createNode(child.loc_name, child.recordid, child.loc_active, child.loc_active_inherit).appendTo(column);
			});

            this._resizeColumns();            
		},
		
        _resizeColumns: function() {

            var columnsEl = jQuery('#locations ul');
            var col = columnsEl.length;

            // iterate last column rows
            var colEl = jQuery('#locations ul:nth-child('+col+')');
            var listEl = colEl.children('li');
            var numRows = listEl.length;
            var newWidth = this.minColumnWidth;

            var optionsWidth = 0;

            // find largest string width
            for (var row = 0; row < numRows; row++) {
                var spans = listEl.eq(row).children('span');

                // get width of option links
                if (optionsWidth == 0) {
                    optionsWidth = spans.last().outerWidth(true);
                }

                // get width of location text and option links
                var totalSize = spans.first().outerWidth(true) + optionsWidth + 17; // scrollbar width on IE

                newWidth = Math.max (newWidth, totalSize);
            }

            // set column and tier width
            colEl.width(newWidth);

            var tiers = jQuery('#tiers li');
            tiers.eq(col-1).width(newWidth-1); // border pixel

            // since column titles have absolute positioning
            // we need to rearrange them

            // change tier absolute positioning
            var numTiers = tiers.length;

            // first col position
            var leftPos = numTiers > 0 ? jQuery('#tiers li:first').offset().left : 0;

            // total size for columns container
            var total = 0;

            // shift every tier from the 2nd
            for (var tier = 2; tier <= numTiers; tier++) {

                // sum previous column width with the left position
                var previousColWidth = columnsEl.eq(tier-2).outerWidth();
                if (previousColWidth == undefined) { previousColWidth = 0; }
                
                // assume at least minColumnWidth 
                var leftPos = leftPos + Math.max (previousColWidth, this.minColumnWidth);
                tiers.eq(tier-1).offset({ left: leftPos });

                total += previousColWidth;
            }

            total += columnsEl.last().outerWidth();

            jQuery('.columns').width(total);
        },

		_createNode: function(name, id, loc_active, loc_active_inherit) {
			var node = $('<li id="location-' + id + '">' +
				'<span class="desc">' + name.replace('<','&lt;') + '</span>' +
				'<span class="controls">' +
					'<a class="edit">Edit</a>' +
					'<a class="delete">Delete</a>' +
				'</span>' +
			'</li>');
			
			node.find('.controls').hide();
			if(loc_active_inherit == 'N' || loc_active == 'N') {
				node.addClass('loc_active_inactive');
			}
			
			node.data('loc_active',loc_active);
			node.data('loc_active_inherit',loc_active_inherit);
			
			return node;
		},
		
		_scrollViewport: function() {
			var target   = this.element.find('.column:eq(' + (this.activeTier) + ')'),
				viewport = this.element.parent('.viewport')
				trgtLeft = parseInt(target.css('left')),
				trgtRght = parseInt(target.next().css('left')) + this.minColumnWidth,  // TODO use real column width 
				viewLeft = viewport.scrollLeft(),
				viewRght = viewport.scrollLeft() + this.element.width();
				
			if (trgtLeft < viewLeft) {
				viewport.scrollLeft(trgtLeft);
			} else if (trgtRght > viewRght) {
				viewport.scrollLeft(viewLeft + (trgtRght - viewRght));
			}
		},
		
		next: function(e) {
			var children,
				activeTier,
                numTiers = $('#tiers li').length;
			
			if (this.disabled) {
				return;
			}
			
			activeTier = this.element.find('.column:eq(' + (++this.activeTier) + ')');
			
			// remove any columns to the right
			activeTier.nextAll().remove();

			// remove active class from all columns
			this.element.find('.column').removeClass('active');
			activeTier.addClass('active').find('.active').removeClass('active');

			// add active class
			this.activeNode.addClass('active');
			
			children = this.findNode(this.activeNode.attr('id').match(/\d+/)[0]).children;
            
            if (this.activeTier < numTiers -1) {
			    this._addColumn(children || []);
            }
			
			this._scrollViewport();
		},
		
		prev: function() {
			if (this.disabled) {
				return;
			}
			
			this.activeTier--;
			
			this.element.find('.column:gt(' + this.activeTier + ')').remove();
			this.element.find('.column').removeClass('active');
			this.element.find('.column:eq(' + this.activeTier + ')').addClass('active');
			this.activeNode = this.element.find('.column:eq(' + this.activeTier + ')').find('li.active');
		},
		
		disable: function() {
			this.disabled = true;
		},
		
		enable: function() {
			this.disabled = false;
		},
		
		appendTo: function(name, id, parentId, rearrange) {
			var column,
				parentNode;
			
			if (parentId) {
				if ($('#' + parentId).parents('.column').next().length < 1) {
					this._addColumn([]);
				}
				
				column = $('#' + parentId).parents('.column').next();
			} else {
				column = $('.column:first', this.element);
			}
			
			column.append(this._createNode(name, id));
			
			if (parentId) {
				parentNode = this.findNode(parentId.match(/\d+/)[0]);
				parentNode.children.push({
					recordid: id,
					loc_name: name,
					children: []
				});
			} else {
				this.nodes.push({
					recordid: id,
					loc_name: name,
					children: []
				});
			}
   
            // resize column  
            if (rearrange == true) {
                this._resizeColumns();
            }
		},
		
		remove: function(id) {
			$('#location-' + id).remove();
			this.deleteNode(id);
		},
		
		/**
		 * Gets all nodes at a particular depth in the tree
		 *
		 * @param {Number} depth The depth of the subtree to get nodes from
		 * @return {jQuery} jQuery object containing all nodes found at depth
		 */
		getNodesAtDepth: function(depth) {
			var currentDepth = 0, 
				returnNodes = [];
			
			(function _descendTree(data) {
				$.each(data, function(index, value) {
					if (currentDepth == depth) {
						returnNodes.push(value)
					} else if (value.children) {
						currentDepth++;
						_descendTree(value.children);
					}
				});
			})(this.nodes);
			
			return returnNodes;
		},
		
		/**
		 * Gets the depth of a given node
		 *
		 * @param {Number} Id of node
		 * @return {Number} depth of node
		 */
		getDepthOfNode: function(nodeId) {
			return $(nodeId).parentsUntil(this.element, 'ul').length;
		},
		
		/**
		 * Needs to look up the original node and grab its children
		 */
		getChildNodes: function(node) {
			var originalEl = $(node).data('originalEl');
			return $('#' + originalEl).children('ul').children('li');
		},
		
		findNode: function(lookupId) {
			var node;

	        (function _descendTree(data) {
	            $.each(data, function(index, value) {
	                if (value.recordid == lookupId) {
	                    node = value;
						return false;
	                }

					if (value.children) {
						_descendTree(value.children);
					} else {
						return;
					}

	            });
	        })(this.nodes);

	        return node;
		},
		
		deleteNode: function(lookupId) {
			(function _descendTree(data) {
	            $.each(data, function(index, value) {
	                if (value.recordid == lookupId) {
	                    data.splice(index, 1);
						return false;
	                }

					if (value.children.length > 0) {
						_descendTree(value.children);
					} else {
						return;
					}

	            });
	        })(this.nodes);
		},
		
		getPathToNode: function(lookupId) {
			var path = [],
				found;

			(function _descendTree(tree, level) {
				$.each(tree, function(i, value) {
					if (found) return false;

					path.splice(level, 1, value);

					if (value.recordid == lookupId) {
						found = true;
						return false;
					}

					if (value.children.length > 0) {
						_descendTree(value.children, level + 1);
					} else {
						return;
					}
				});
			})(this.nodes, 0);

			return path;
		}
	});
})(jQuery, window, document);