;(function($, window, document, undefined) {
	$.fn.equalHeights = function() {
		var maxHeight = 0;

		this.each(function(i){
			maxHeight = Math.max(maxHeight, $(this).height());
		});
		
		// for ie6, set height since min-height isn't supported
		// TODO Use browser.support instead of browser sniffing
		if ($.browser.msie && $.browser.version == 6.0) { 
			this.css({'height': maxHeight}); 
		}

		this.css({'min-height': maxHeight});
		
		return this;
	}
})(jQuery);