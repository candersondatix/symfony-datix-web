var MedicationSelectionCtrl = {};

MedicationSelectionCtrl.create = function(config)
{

	this.config = config;

	if (document.getElementById("medicationlist_window")) {
		this.destroy();
	}

	var popupWindow = document.createElement("div");
	popupWindow.id = "medicationlist_window";
	popupWindow.style.visibility = "hidden";
	document.body.appendChild(popupWindow);

	var posArray = getElementPosition($('check_btn_' + config.fieldname));

	var ctrlPosX = posArray[0];
	var ctrlPosY = posArray[1];

	popupWindow.style.left = ctrlPosX + 20 + "px";
	popupWindow.style.top = ctrlPosY + "px";
     
    MedicationSelectionCtrl.MatchExistingMedication(popupWindow);
}

MedicationSelectionCtrl.destroy = function()
{
	var ele = document.getElementById("medicationlist_window");
	ele.parentNode.removeChild(ele);
	MedicationSelectionCtrl.hideElements();
}

MedicationSelectionCtrl.fixControlSize = function(elePopup)
{   
    $('medicationlist_window').style.width = "auto";
    $('medicationlist_title').style.width = "auto";
    $('medicationlist_table').style.width = "auto";  
    $('medicationlist_container').style.width = (parent.screen.width*0.45) + 'px';
    $('medicationlist_window').style.width = ((parent.screen.width*0.45)+7) + 'px';
    $('medicationlist_container').style.height = $('medicationlist_container').scrollHeight > 300 ? "300px" : "auto" ;
    
    var viewportSize = getViewPortSize();
    var viewportXY = Position.page(elePopup);

    var ctrlHeight = elePopup.offsetHeight;
    var ctrlWidth = elePopup.offsetWidth;

    var currentPos = getElementPosition($('medicationlist_window'));


    if (viewportXY[1] + ctrlHeight > viewportSize[1])
    {
        elePopup.style.top = currentPos[1] - (viewportXY[1] + ctrlHeight - viewportSize[1]) + "px";
    }

    if (viewportXY[0] + ctrlWidth > viewportSize[0])
    {
        elePopup.style.left = currentPos[0] - viewportXY[0] - ctrlWidth + viewportSize[0] + "px";
    }

}

MedicationSelectionCtrl.hideElements = function(d) 
{
	if (!browser.isIE || browser.isIE && browser.version > 6) {
		return;
	}

	var tags = new Array("applet", "iframe", "select");

	if (document.getElementById('medicationlist_window') == null) {
		MedicationSelectionCtrl.__created = false;
	}
	else {
		MedicationSelectionCtrl.__created = true;
		var pos = getElementPosition($('medicationlist_window'));
		MedicationSelectionCtrl.__position = {
			left: pos[0],
			top: pos[1],
			right: pos[0] + $('medicationlist_window').offsetWidth,
			bottom: pos[1] + $('medicationlist_window').offsetHeight
		}
	}

	for (var i = 0; i < tags.length; i++) {
		var objects = document.getElementsByTagName(tags[i]);
		var curEle = null;
		for (var j = 0; j < objects.length; j++) {
			curEle = objects[j];
			if (curEle.id != 'code_list_select') {
				if (typeof curEle.__position == 'undefined') {
					var pos = getElementPosition(curEle);
					curEle.__position = {
						left: pos[0],
						top: pos[1],
						right: pos[0] + curEle.offsetWidth,
						bottom: pos[1] + curEle.offsetHeight
					}
				}

				if (typeof curEle.__visible == 'undefined') {
					curEle.__visible = Element.visible(curEle);
				}

				if (!MedicationSelectionCtrl.__created ||
					(curEle.__position.left > MedicationSelectionCtrl.__position.right) ||
					(curEle.__position.right < MedicationSelectionCtrl.__position.left) ||
					(curEle.__position.top > MedicationSelectionCtrl.__position.bottom) ||
					(curEle.__position.bottom < MedicationSelectionCtrl.__position.top)) {
					if (curEle.__visible) {
						Element.show(curEle);
						try {
							if (curEle.multiple) {
								// also hide add and delete buttons
								Element.show($('img_add_' + curEle.id));
								Element.show($('img_del_' + curEle.id));
							}
						} catch(e) {}
					}
				}
				else {
					try {
						if (curEle.multiple) {
							// also hide add and delete buttons
							Element.hide($('img_add_' + curEle.id));
							Element.hide($('img_del_' + curEle.id));
						}
					} catch(e) {}
					Element.hide(curEle);
				}
			}
		}
	}
}

var overrideArray = new Array();


MedicationSelectionCtrl.MatchExistingMedication = function(popupWindow)
{

    var postbodystring = "";
    var fields = this.config.fieldlist.split(",");

    for (var i=0; i < fields.length; i++)
    {
        postbodystring += '&' + fields[i] + '=' + $(this.config[fields[i]]).value;
    }

    var oAjaxCall = new Ajax.Request
    (
        scripturl + '?action=httprequest&type=medicationlist',
        {
            method: 'post',
            postBody:
                'module=' + this.config.module +
                '&fieldname=' + this.config.fieldname +
                '&suffix=' + this.config.suffix +
                '&searchmappingtype=' + this.config.searchmappingtype +  
                '&fieldlist=' + this.config.fieldlist +
                '&parentformid=' + this.config.parentformid +
                postbodystring,
            onSuccess: function(r)
            {
                document.getElementById('medicationlist_window').innerHTML = r.responseText;
                popupWindow.style.visibility = "visible";

                MedicationSelectionCtrl.fixControlSize(popupWindow);
                
                jQuery('#medicationlist_title').mousedown(function(){MedicationSelectionCtrl.createDraggable();});
                jQuery('#medicationlist_title').mouseup(function(){MedicationSelectionCtrl.destroyDraggable();});
                
                MedicationSelectionCtrl.hideElements();
            },

            onFailure: function()
            {
                alert('Error retrieving medication list...');
            }
        }
    );
}

MedicationSelectionCtrl.createDraggable = function()
{
    this.Draggable = new Draggable('medicationlist_window', {starteffect:'', endeffect:'', onEnd:DragEnd});
}

MedicationSelectionCtrl.destroyDraggable = function()
{
    if (this.Draggable != null)
    {
        this.Draggable.destroy();   
    }
}
