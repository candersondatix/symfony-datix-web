var EquipmentSelectionCtrl = {};

EquipmentSelectionCtrl.create = function(config)
{

    this.config = config;

    if (document.getElementById("equipmentlist_window")) {
        this.destroy();
    }

    var popupWindow = document.createElement("div");
    popupWindow.id = "equipmentlist_window";
    popupWindow.style.visibility = "hidden";
    document.body.appendChild(popupWindow);

    var posArray = getElementPosition($('check_btn_' + config.fieldname));

    var ctrlPosX = posArray[0];
    var ctrlPosY = posArray[1];
    
    popupWindow.style.left = ctrlPosX + 20 + "px";
    popupWindow.style.top = ctrlPosY + "px";
}

EquipmentSelectionCtrl.destroy = function()
{
    var ele = document.getElementById("equipmentlist_window");
    ele.parentNode.removeChild(ele);
    EquipmentSelectionCtrl.hideElements();
}

EquipmentSelectionCtrl.fixControlSize = function(elePopup)
{
    // Default width for select element is 400px if greater, increase control width
    var diff = 0;
/*    if ($('code_list_select').offsetWidth > 400 && $('code_list_select').options.length)
    {
        diff = $('code_list_select').offsetWidth - 400;
    }        */

    $('equipmentlist_window').style.width = 408 + diff + 'px';
    $('equipmentlist_title').style.width = 400 + diff + 'px';
    $('equipmentlist_container').style.width = 400 + diff + 'px';
    //$('codelist_search').style.width = 400 + diff + 'px';
    //$('searchtext').style.width = 400 + diff - 6 + 'px';
    //$('codelist_select').style.width = 400 + diff + 'px';
    //$('code_list_select').style.width = 400 + diff + 'px';

    var viewportSize = getViewPortSize();
    var viewportXY = Position.page(elePopup);

    var ctrlHeight = elePopup.offsetHeight;
    var ctrlWidth = elePopup.offsetWidth;

    var currentPos = getElementPosition($('equipmentlist_window'));


    if (viewportXY[1] + ctrlHeight > viewportSize[1])
    {
        elePopup.style.top = currentPos[1] - (viewportXY[1] + ctrlHeight - viewportSize[1]) + "px";
    }

    if (viewportXY[0] + ctrlWidth > viewportSize[0])
    {
        elePopup.style.left = currentPos[0] - viewportXY[0] - ctrlWidth + viewportSize[0] + "px";
    }     
}

EquipmentSelectionCtrl.hideElements = function(d) 
{
    if (!browser.isIE || browser.isIE && browser.version > 6) {
        return;
    }

    var tags = new Array("applet", "iframe", "select");

    if (document.getElementById('equipmentlist_window') == null) {
        EquipmentSelectionCtrl.__created = false;
    }
    else {
        EquipmentSelectionCtrl.__created = true;
        var pos = getElementPosition($('equipmentlist_window'));
        EquipmentSelectionCtrl.__position = {
            left: pos[0],
            top: pos[1],
            right: pos[0] + $('equipmentlist_window').offsetWidth,
            bottom: pos[1] + $('equipmentlist_window').offsetHeight
        }
    }

    for (var i = 0; i < tags.length; i++) {
        var objects = document.getElementsByTagName(tags[i]);
        var curEle = null;
        for (var j = 0; j < objects.length; j++) {
            curEle = objects[j];
            if (curEle.id != 'code_list_select') {
                if (typeof curEle.__position == 'undefined') {
                    var pos = getElementPosition(curEle);
                    curEle.__position = {
                        left: pos[0],
                        top: pos[1],
                        right: pos[0] + curEle.offsetWidth,
                        bottom: pos[1] + curEle.offsetHeight
                    }
                }

                if (typeof curEle.__visible == 'undefined') {
                    curEle.__visible = Element.visible(curEle);
                }

                if (!EquipmentSelectionCtrl.__created ||
                    (curEle.__position.left > EquipmentSelectionCtrl.__position.right) ||
                    (curEle.__position.right < EquipmentSelectionCtrl.__position.left) ||
                    (curEle.__position.top > EquipmentSelectionCtrl.__position.bottom) ||
                    (curEle.__position.bottom < EquipmentSelectionCtrl.__position.top)) {
                    if (curEle.__visible) {
                        Element.show(curEle);
                        try {
                            if (curEle.multiple) {
                                // also hide add and delete buttons
                                Element.show($('img_add_' + curEle.id));
                                Element.show($('img_del_' + curEle.id));
                            }
                        } catch(e) {}
                    }
                }
                else {
                    try {
                        if (curEle.multiple) {
                            // also hide add and delete buttons
                            Element.hide($('img_add_' + curEle.id));
                            Element.hide($('img_del_' + curEle.id));
                        }
                    } catch(e) {}
                    Element.hide(curEle);
                }
            }
        }
    }
}

var overrideArray = new Array();

EquipmentSelectionCtrl.createDraggable = function()
{
    this.Draggable = new Draggable('equipmentlist_window', {starteffect:'', endeffect:'', onEnd:DragEnd});
}

EquipmentSelectionCtrl.destroyDraggable = function()
{
    if (this.Draggable != null)
    {
        this.Draggable.destroy();   
    }
}