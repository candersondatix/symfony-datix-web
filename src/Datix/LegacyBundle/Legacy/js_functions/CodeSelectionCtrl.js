function CodeSelectionCtrl()
{

}

CodeSelectionCtrl.prototype.setIndex = function(nGlobIndex)
{
    this.ControlIndex = nGlobIndex;
    CurrentCodeControlIndex = nGlobIndex;
    this.valArray = new Array();
}

CodeSelectionCtrl.prototype.create = function(config)
{
	/*
	Supported configuration parameters:

	module          - The current module for which the control is created
	field           - The database field name
	fieldname       - The name to be given to the HTML element storing the code
	contactsuffix   - The contact suffix from the form design contact section
	multilistbox    - true/false to indicates a multiple or single selection listbox
	searchmode      - true/false to indicate whether we are in search mode
	freetext        - true/false to indicate whether the free text can be typed in the field,
					  and whether the description is to be saved instead of the code

	>>> The following configuration options are for backward compatibility only <<<
	>>> Try not to use them unless you REALLY have to.                          <<<
	parent1         - The name of the HTML field with the value of the first parent code
					  if different from the DB field name
	parent2         - The name of the HTML field with the value of the first parent code
					  if different from the DB field name
	child1          - The name of the first dependant HTML field which will have its value cleared
					  if its parent is changed.
	child2          - The name of the second dependant HTML field which will have its value cleared
					  if its parent is changed.

    >> added 13/9/07
    overrideArray   - Array object in the form {code: name, code2: name2... }. Will be used instead of
                      querying for the codes associated with the database field name.

    overrideTitle   - title of popup (used when passing array with overrideArray).

    selectFunction  - codes can be retrieved using a custom function

	Parameters must be passed as an object e.g. {module: 'INC', field: 'inc_type', searchmode: true}

	*/
    config.searchmode = (config.fieldmode == 'Search');
	config.udf = (config.field.substr(0,3) == "UDF");

	this.config = config;

    this.parentFields = new Array();
    this.childFields = new Array();

    // create floating div and store reference in popupWindow
    this.popupWindow = AddNewFloatingDiv('codelist'+this.config.fieldname);
    this.popupWindow.codelist = true;

    // Check for possible parent and child fields
    // Then see whether those fields are on the form, use them and their values to get the codes
    if(this.parentFields.length == 0)
    {
        if (config.parent1 !== undefined)
        {
            this.parentFields[0] = config.parent1;

            if (config.parent2 !== undefined)
            {
                this.parentFields[1] = config.parent2;
            }
        }
    }

    if(this.config.overrideArray)
    {
        this.config.overrideArray = overrideArray[this.config.fieldname];

        this.XMLString = this.getXMLFromArray();

        this.populateFromXML();
    }
    else if (this.config.selectFunction)
    {
        this.selectFunction();
    }
    else
    {
        this.getCodes();
    }
}

CodeSelectionCtrl.prototype.destroy = function()
{
    this.popupWindow.CloseFloatingControl();

    //refocus back on the control trigger that we would have clicked (doesn't work in FF for some reason).
    if($('img_add_'+this.config.fieldname))
    {
        $('img_add_'+this.config.fieldname).parentNode.focus();
    }
}

CodeSelectionCtrl.prototype.highlightSelection = function(value)
{
	value = value.replace(/[\)\(]/g,""); //get rid of parentheses.

    var ele = document.getElementById('code_list_select_'+this.ControlIndex);
	ele.selectedIndex = -1;
	if (value != '') {
		for (var i = 0; i < ele.options.length; i++)
		{
			var str = ele.options[i].text;
			str = str.toLowerCase();
			var re = value.toLowerCase();
			if (str.search(re) == 0)
			{
				ele.options[i].selected = true;
				break;
			}
		}
	}
}

CodeSelectionCtrl.prototype.makeSelection = function(evt)
{
	var options = document.getElementById('code_list_select_'+this.ControlIndex).options;
	var eleSourceField = document.getElementById(this.config.fieldname);
	var eleCodeDescr = jQuery('#'+this.config.fieldname + '_title');
	var selVals = new Array();
	eleSourceField.value = '';
	var codeDescription = '';
	var separator = (this.config.searchmode ? '|' : ' ');
    var allow_dups = (this.config.allow_dups ? this.config.allow_dups : false);

	for (var i = 0; i < options.length; i++)
	{
		if (options[i].selected)
		{
			if (this.config.multilistbox == true)
			{
				if (this.config.returnCodeOnly == true)
                {
                    addCodeDescr(options[i].value, options[i].value, eleSourceField, false, allow_dups);
                }
                else
                {
                    addCodeDescr(options[i].value, options[i].text, eleSourceField, false, allow_dups);
                }
                if (!allow_dups)
                {
                    options[i] = null;
				    i--;
                }
			}
			else
			{
				if (eleSourceField.value != '')
				{
					eleSourceField.value += separator;
				}
				eleSourceField.value += options[i].value;
				codeDescription = options[i].text;
			}
		}
	}

	if (this.config.freetext == false)
	{
		if (this.config.searchmode == false && eleCodeDescr)
		{
			eleCodeDescr.html(codeDescription.replace('&', '&amp;'));
		}

		if (this.config.multilistbox == true)
		{
			checkMultiWidth(eleSourceField);
		}
		else if (this.config.searchmode == false && isIE6())
		{
            SetDisplayDivSize(eleCodeDescr);
		}
	}

	if (this.config.multilistbox == false)
	{
		this.destroy();
	}
	else
	{
		this.fixControlSize();
	}

    // Only chek for dependancies for single selection dropdowns and not in search mode
   /* if (this.config.searchmode == false)
    {
        this.checkChildren();
    } */

    if(this.config.fieldname == 'merge_contact')
    {
        MergeContactSelectionDone();
    }

    this.popupWindow.hideElements();

    // ensure background div still covers full screen width
    if($('floating_window_back_panel') && !isIE6())
    {
        // broswer does not appear to resize div if width is set to same as current value, so:
        $('floating_window_back_panel').setStyle({width: '99%'});
        $('floating_window_back_panel').setStyle({width: '100%'});
    }

    FieldChanged(this.config.fieldname);

    //refocus back on field we've just changed (doesn't work in FF for some reason).
    if($('img_add_'+this.config.fieldname))
    {
        $('img_add_'+this.config.fieldname).parentNode.focus();
    }
    
    this.updateNumbers();
}

/*CodeSelectionCtrl.prototype.SetDisplayDivSize = function(element)
{
    element.style.width = "auto";

    if (element.innerHTML == '' || element.offsetWidth < 200)
    {
         element.style.width = "200px";
    }
}*/

/*CodeSelectionCtrl.prototype.checkChildren = function()
{
    if(this.config.child1)
    {
        this.childFields.push(this.config.child1);
    }
    if(this.config.child2)
    {
        this.childFields.push(this.config.child2);
    }

    this.childFields = this.childFields.uniq(); //ensure we don't have duplicates.

    for (i = 0; i < this.childFields.length; i++)
    {
        //CheckClearChildField(this.childFields[i], this.config.childcheck);

        var ele = document.getElementById(this.childFields[i]);
        if (ele)
        {
            if (ele.tagName == 'INPUT')
            {
                if(this.config.childcheck)
                {
                    if($(this.childFields[i]).value != '')
                    {
                        eval('CreateCodeList(\''+this.childFields[i]+'\',false);');
                    }
                }
                else
                {
                    if($(this.childFields[i]))
                    {
                        $(this.childFields[i]).value = '';
                    }
                    if($(this.childFields[i] + '_title'))
                    {
                        $(this.childFields[i] + '_title').innerHTML = ''
                        this.SetDisplayDivSize($(this.childFields[i] + '_title'));
                    }
                    FieldChanged(this.childFields[i]);
                }
            }
            else if (ele.tagName == 'SELECT')
            {
                if(this.config.childcheck)
                {
                    if($(this.childFields[i]).options.length > 0)
                    {
                        eval('CreateCodeList(\''+this.childFields[i]+'\',false);');
                    }
                }
                else
                {
                    if($(this.childFields[i]))
                    {
                        while (ele.options.length > 0)
                        {
                            ele.options[0] = null;
                        }
                        checkMultiWidth(ele);
                    }
                    FieldChanged(this.childFields[i]);
                }
            }
        }
    }

    this.popupWindow.hideElements();
}*/

/*CodeSelectionCtrl.prototype.checkChild = function(childField, childValue)
{
	var oAjaxCall = new Ajax.Request
	(
		scripturl + '?action=httprequest&type=checkchild',
		{
			method: 'post',
			postBody:
				'module=' + this.config.module +
				'&parent_field=' + this.config.field +
				"&parent_value=" + this.config.fieldname +
				'&child_field=' + childField +
				"&child_value=" + childValue,
			onSuccess: function(r)
			{
				//alert(r.responseText);
			},
			onFailure: function()
			{
                GlobalCodeSelectControls[CurrentCodeControlIndex].codeError();
			}
		}
	);
}       */

CodeSelectionCtrl.prototype.removeItemFromMultiListbox = function(eleMultiListbox)
{
	//get .text and .value of selected option before deleting the entry
	if (eleMultiListbox.selectedIndex != -1 && document.getElementById('code_list_select_'+this.ControlIndex))
	{
		var code = eleMultiListbox.options[eleMultiListbox.selectedIndex].value;
		var description = eleMultiListbox.options[eleMultiListbox.selectedIndex].text;

		eleMultiListbox.options[eleMultiListbox.selectedIndex] = null;

		if (document.getElementById('code_list_select_'+this.ControlIndex))
		{
			addCodeDescr(code, description, document.getElementById('code_list_select_'+this.ControlIndex), false, true);
			this.fixControlSize();
		}

		FieldChanged(eleMultiListbox.name.replace("[]",""));
	}

	checkMultiWidth(eleMultiListbox);

    this.popupWindow.hideElements();
}

CodeSelectionCtrl.prototype.fixControlSize = function()
{
	//all positioning is now handled in FloatingWindowClass.js

    if($('code_list_select_'+this.ControlIndex))
    {
        if($('code_list_select_'+this.ControlIndex).options.length > 20)
        {
            $('code_list_select_'+this.ControlIndex).size = 20;
        }
		else if($('code_list_select_'+this.ControlIndex).options.length > 1)
		{
        	$('code_list_select_'+this.ControlIndex).size = $('code_list_select_'+this.ControlIndex).options.length;
	    }
		else
	    {
	        $('code_list_select_'+this.ControlIndex).size = 2;
	    }

        // Default width for select element is 280px if greater, increase control width
	    var diff = 0;
	    if ($('code_list_select_'+this.ControlIndex).offsetWidth > 280 && $('code_list_select_'+this.ControlIndex).options.length)
	    {
		    diff = $('code_list_select_'+this.ControlIndex).offsetWidth - 280;
	    }
    }

    if($('codelist_search'))
    {
	    $('codelist_search').style.width = 280 + diff + 'px';
    }

	if($('searchtext'))
    {
        $('searchtext').style.width = 280 + diff - 6 + 'px';
    }
    if($('codelist_select'))
    {
	    $('codelist_select').style.width = 280 + diff + 'px';
    }
    if($('code_list_select_'+this.ControlIndex))
    {
	    $('code_list_select_'+this.ControlIndex).style.width = 280 + diff + 'px';
    }

    this.popupWindow.hideElements();
}

CodeSelectionCtrl.prototype.getCodes = function()
{
    this.setParentsUrl();

	// Get current values
	if (this.config.multilistbox)
	{
		var valuesUrlStr = '';

		var options = $(this.config.fieldname).options;
		for (var i = 0; i < options.length; i++)
		{
			valuesUrlStr += "&value[]=" + options[i].value;
		}
	}
	else
	{
		var valuesUrlStr = "&value[]=" + $(this.config.fieldname).value;
	}

    if(this.config.xml) //use xml
    {
        var oAjaxCall = new Ajax.Request
        (
            scripturl + '?action=httprequest&type=codelist',
            {
                method: 'post',
                postBody:
                    'module=' + this.config.module +
                    '&field=' + this.config.field +
                    '&ctrlidx=' + this.ControlIndex +
                    (this.config.overrideTitle ? '&overridetitle=' + this.config.overrideTitle : "") +
                    (this.config.mandatory ? "&mandatory=mandatory" : "") +
                    (this.config.fieldmode ? "&fieldmode=" + this.config.fieldmode : "") +
                    valuesUrlStr +
                    (this.config.multilistbox || this.config.searchmode ? "&multiple=multiple" : "") +
                    (this.config.freetext ? "&freetext=freetext" : "") +
                    (this.config.multilistbox ? "&multilistbox=multilistbox" : "") +
                    (this.config.fieldname ? "&fieldname=" + this.config.fieldname : "") +
                    (this.config.udf ? "&udf=1" : "") +
                    (this.config.codecheck ? "&codecheck=1" : "") +
                    this.url_parents,
                onSuccess: function(r)
                {
                    //need to do it like this in case the XML returned is invalid.
                    var ctrlidx = r.responseText.match("<ctrlidx>([0-9]+)</ctrlidx>")[1];

                    GlobalCodeSelectControls[ctrlidx].XMLString = r.responseText;

                    GlobalCodeSelectControls[ctrlidx].populateFromXML();
                },

                onFailure: function()
                {
                    GlobalCodeSelectControls[CurrentCodeControlIndex].codeError();
                }
            }
        );
    }
    else //use json
    {
        var oAjaxCall = new Ajax.Request
        (
            scripturl + '?action=httprequest&type=codelist&responsetype=json',
            {
                method: 'post',
                postBody:
                    'module=' + this.config.module +
                    '&field=' + this.config.field +
                    '&ctrlidx=' + this.ControlIndex +
                    (this.config.overrideTitle ? '&overridetitle=' + this.config.overrideTitle : "") +
                    (this.config.mandatory ? "&mandatory=mandatory" : "") +
                    (this.config.fieldmode ? "&fieldmode=" + this.config.fieldmode : "") +
                    valuesUrlStr +
                    (this.config.multilistbox || this.config.searchmode ? "&multiple=multiple" : "") +
                    (this.config.freetext ? "&freetext=freetext" : "") +
                    (this.config.multilistbox ? "&multilistbox=multilistbox" : "") +
                    (this.config.fieldname ? "&fieldname=" + this.config.fieldname : "") +
                    (this.config.udf ? "&udf=1" : "") +
                    (this.config.codecheck ? "&codecheck=1" : "") +
                    (this.config.select_sql ? "&select_sql=" + this.config.select_sql : "") + 
                    (this.config.dupcheck ? "&dupcheck=" + this.config.dupcheck : "") + 
                    (this.config.table ? "&table=" + this.config.table : "") +
                    this.url_parents,
                onSuccess: function(r)
                {
                    var JSONresponse = r.responseText.evalJSON();

                    var ctrlidx = JSONresponse.ctrlidx;

                    GlobalCodeSelectControls[ctrlidx].populateFromJSON(JSONresponse);
                },

                onFailure: function()
                {
                    GlobalCodeSelectControls[CurrentCodeControlIndex].codeError();
                }
            }
        );
    }
}

/**
* Makes an AJAX call to retrieve codes via a custom select function.
*/
CodeSelectionCtrl.prototype.selectFunction = function()
{
    this.setParentsUrl();

    var parameters = '';
    if (this.config.selectFunctionParameters)
    {
        for (var parm in this.config.selectFunctionParameters)
        {
            parameters += '&' + parm + '=' + this.config.selectFunctionParameters[parm];
        }
    }

    // Get current values
    var current = '';
    if (this.config.multilistbox)
    {
        for(var i = 0; i<$(this.config.fieldname).options.length; i++)
        {
            current += '&current[]=' + $(this.config.fieldname).options[i].value;
        }
    }
    else
    {
        current = $(this.config.fieldname).value;
    }

    if(this.config.xml) //use xml
    {
        var oAjaxSelectFunction = new Ajax.Request
        (
            scripturl + '?action=httprequest&type=selectfunction',
            {
                method: 'post',
                postBody: 'function=' + this.config.selectFunction + parameters +
                          '&title=' + this.config.overrideTitle +
                          '&ctrlidx=' + this.ControlIndex +
                          '&searchmode=' + this.config.searchmode +
                          '&multilistbox=' + this.config.multilistbox +
                          '&nochoose=' + this.config.nochoose +
                          '&method=xml' +
                          current +
                          this.url_parents,
                onSuccess: function(r)
                {
                    var ctrlidx = r.responseText.match("<ctrlidx>([0-9]+)</ctrlidx>")[1];
                    GlobalCodeSelectControls[ctrlidx].XMLString = GlobalCodeSelectControls[ctrlidx].getXMLHeader();
                    GlobalCodeSelectControls[ctrlidx].XMLString += r.responseText;
                    GlobalCodeSelectControls[ctrlidx].populateFromXML();
                },
                onFailure: function()
                {
                    GlobalCodeSelectControls[CurrentCodeControlIndex].codeError();
                }
            }
        );
    }
    else
    {
        var oAjaxSelectFunction = new Ajax.Request
        (
            scripturl + '?action=httprequest&type=selectfunction&responsetype=json',
            {
                method: 'post',
                postBody: 'function=' + this.config.selectFunction + parameters +
                          '&title=' + this.config.overrideTitle +
                          '&ctrlidx=' + this.ControlIndex +
                          '&searchmode=' + this.config.searchmode +
                          '&multilistbox=' + this.config.multilistbox +
                          '&nochoose=' + this.config.nochoose +
                          current +
                          this.url_parents,
                onSuccess: function(r)
                {
                    var JSONresponse = r.responseText.evalJSON();
                    var ctrlidx = JSONresponse.ctrlidx;
                    GlobalCodeSelectControls[ctrlidx].populateFromJSON(JSONresponse);
                },
                onFailure: function()
                {
                    GlobalCodeSelectControls[CurrentCodeControlIndex].codeError();
                }
            }
        );
    }
}

CodeSelectionCtrl.prototype.setParentsUrl = function()
{
    var url_parents = '';

    for (i = 0; i < this.parentFields.length; i++)
    {
        var ele = document.getElementById(this.parentFields[i]);
        url_parents += '&parent_value[]=';
        if (ele)
        {
            url_parents += getValuesArray(this.parentFields[i]).join('|');
        }
    }

    this.url_parents = url_parents;
}

//this is messy (we have to assume there will only ever be one code list visible), but otherwise we have
//no way of tracking the current list when an error occurs.
var CurrentCodeControlIndex;

CodeSelectionCtrl.prototype.codeError = function()
{
    this.destroy();
    if (globals['AJAX_WAIT_MSG'] != 'Y')
    {
        alert('Could not retrieve codes.');
    }
}

/**
* Displays a customisable error message.
*
* @param string errorMessage: The custom error.
*/
CodeSelectionCtrl.prototype.customCodeError = function(errorMessage)
{
    this.destroy();
    alert(errorMessage);
}

CodeSelectionCtrl.prototype.populateFromXML = function()
{
    var buttons = new Array();

    // Load XML
    var xml = new XMLObject(false);

    xml.loadXML(this.XMLString);

    if(XMLIsValid(xml))
    {
        var error = xml.xmlDocument.getElementsByTagName('error');
        if (error.length > 0)
        {
            //display custom error message
            this.customCodeError(error[0].firstChild.nodeValue);
            return;
        }

        // Load XSL
        var xsl = new XMLObject(false);
        xsl.load(scripturl.substr(0, scripturl.lastIndexOf('/')) + '/xsl_files/CodeSelectionCtrl.xsl');

        // Load Title XSL
        var xsl_title = new XMLObject(false);
        xsl_title.load(scripturl.substr(0, scripturl.lastIndexOf('/')) + '/xsl_files/CodeTitle.xsl');

        // Transform
        try
        {
            this.popupWindow.setTitle(xml.transform(xsl_title.xmlDocument));
            this.popupWindow.setContents(xml.transform(xsl.xmlDocument));
        }
        catch (e)
        {
            // error gracefully if the stylesheet weren't properly retrieved (e.g. because of network errors)
            GlobalCodeSelectControls[this.ControlIndex].destroy();
            alert('Could not retrieve codes.');
            return;
        }

        this.getValArray(xml);

        if(!this.config.codecheck)
        {
            if((this.config.multilistbox && this.valArray.numCodes != 0) || this.config.searchmode)
            {
                buttons.push({'value':'Add','onclick':'GlobalCodeSelectControls['+this.ControlIndex+'].makeSelection(this)'});
            }

            buttons.push({'value':'Close','onclick':'GlobalCodeSelectControls['+this.ControlIndex+'].destroy()'});

            this.popupWindow.setButtons(buttons);

            this.popupWindow.display();

            this.fixControlSize();
            this.popupWindow.fixPosition();
        }
        else if(this.config.childcheck)
        {
            this.checkValue();
        }
    }
    else
    {
        this.popupWindow.setTitle('Error');
        this.popupWindow.setContents('An XML error occured while trying to retrieve the codes');
        buttons.push({'value':'Close','onclick':'GlobalCodeSelectControls['+this.ControlIndex+'].destroy()'});

        this.popupWindow.setButtons(buttons);
        this.popupWindow.display();

        this.fixControlSize();
        this.popupWindow.fixPosition();
    }

}

CodeSelectionCtrl.prototype.buildCodeControl = function(JSONresponse)
{
    var ControlHTML = '<div>';

    if(!JSONresponse['empty'])
    {
        ControlHTML += '<div id="codelist_search">'
        + '<input tabindex="1001" name="searchtext" id="searchtext" size="52" maxlength="254" value="" style="width: 274px;" onkeyup="GlobalCodeSelectControls['+JSONresponse['ctrlidx']+'].highlightSelection(this.value, document.getElementById(\'code_list_select\'));" onkeypress="SelectCodeOnEnter('+JSONresponse['ctrlidx']+')" type="text" />'
        + '        </div>'
        + '        <div id="codelist_select">';

        if(JSONresponse['multiple'])
        {
            ControlHTML += '<select tabindex="1002" name="code_list_select" id="code_list_select_'+JSONresponse['ctrlidx']+'" size="5" multiple="multiple" ctrlidx="'+JSONresponse['ctrlidx']+'" ondblclick="GlobalCodeSelectControls['+JSONresponse['ctrlidx']+'].makeSelection(this);">';

            for(var i = 0; i< JSONresponse['code'].length; i++)
            {
                ControlHTML += '<option value="'+JSONresponse['code'][i]['value']+'" '+(JSONresponse['code'][i]['selected'] ? 'selected="selected"' : '')+'>'+JSONresponse['code'][i]['description']+'</option>';
            }

            ControlHTML += '</select>';
        }
        else
        {
            ControlHTML += '<select tabindex="1002" name="code_list_select" id="code_list_select_'+JSONresponse['ctrlidx']+'" size="5" ctrlidx="'+JSONresponse['ctrlidx']+'" onclick="GlobalCodeSelectControls['+JSONresponse['ctrlidx']+'].makeSelection(this);" onkeypress="SelectCodeOnEnter('+JSONresponse['ctrlidx']+')">';

            for(var i = 0; i< JSONresponse['code'].length; i++)
            {
                ControlHTML += '<option value="'+JSONresponse['code'][i]['value']+'" '+(JSONresponse['code'][i]['selected'] ? 'selected="selected"' : '')+'>'+JSONresponse['code'][i]['description']+'</option>';
            }
            ControlHTML += '</select>';
        }

        ControlHTML += '       </div>';
    }
    else
    {
        ControlHTML += '<div id="codelist_select">'
        + '           <div>There are no codes to display.</div>'
        + '                <div style="display:none">'
        + '                    <select name="code_list_select" id="code_list_select_'+JSONresponse['ctrlidx']+'" size="5" multiple="multiple">'
        + '                    </select>'
        + '                </div>'
        + '            </div>'
    }
    ControlHTML += '</div><input type="hidden" id="code_list_field" value=""/>';

    return ControlHTML;
}

CodeSelectionCtrl.prototype.populateFromJSON = function(JSONresponse)
{
    if (JSONresponse['!ERROR!'])
    {
        //display custom error message
        this.customCodeError(JSONresponse['!ERROR!'])
        return;
    }

    var buttons = new Array();

    // Transform
    this.popupWindow.setTitle('<div>'+JSONresponse['title']+'</div>');
    this.popupWindow.setContents(this.buildCodeControl(JSONresponse));

    this.getValArrayJSON(JSONresponse);

    if(!this.config.codecheck)
    {
        if((this.config.multilistbox && this.valArray.numCodes != 0) || this.config.searchmode)
        {
            buttons.push({'value':'Add','onclick':'GlobalCodeSelectControls['+this.ControlIndex+'].makeSelection(this)'});
        }

        buttons.push({'value':'Close','onclick':'GlobalCodeSelectControls['+this.ControlIndex+'].destroy()'});

        this.popupWindow.setButtons(buttons);

        this.popupWindow.display();

        this.fixControlSize();
        this.popupWindow.fixPosition();
    }
    else if(this.config.childcheck)
    {
        this.checkValue();
    }
}

CodeSelectionCtrl.prototype.getValArray = function(xml)
{
    var codeList = xml.xmlDocument.getElementsByTagName('code');
    this.valArray.numCodes = 0;

    for(var i=0; i<codeList.length; i++)
    {
        var CodeDetails = codeList[i].getElementsByTagName('value');
        if(CodeDetails[0].firstChild)
        {
            var Code = CodeDetails[0].firstChild.nodeValue;
        }

        var DescDetails = codeList[i].getElementsByTagName('description');
        if(DescDetails[0].firstChild)
        {
            var Desc = DescDetails[0].firstChild.nodeValue;
        }

        if(Code)
        {
            this.valArray[Code] = Desc;
            this.valArray.numCodes++;
        }
    }
}

CodeSelectionCtrl.prototype.getValArrayJSON = function(JSONresponse)
{
    this.valArray.numCodes = 0;

    if(!JSONresponse['empty'])
    {
        var codeList = JSONresponse['code'];

        for(var i=0; i<codeList.length; i++)
        {
            if(JSONresponse['code'][i]['value'])
            {
                this.valArray[JSONresponse['code'][i]['value']] = JSONresponse['code'][i]['description'];
                this.valArray.numCodes++;
            }
        }
    }
}

CodeSelectionCtrl.prototype.checkValue = function()
{
    if($(this.config.fieldname))
    {
        var CurrentVals = getValuesArray(this.config.fieldname);// $(this.config.fieldname).value;

        if(this.config.multilistbox)
        {
            for(var i = 0; i<CurrentVals.length; i++)
            {
                if(this.valArray[CurrentVals[i]] == undefined)
                {
                    for(var j = 0; j<$(this.config.fieldname).options.length; j++)
                    {
                        if($(this.config.fieldname).options[j].value == CurrentVals[i])
                        {
                            $(this.config.fieldname).options[j] = null;
                            break;
                        }
                    }
                }
            }
        }
        else if(CurrentVals.length > 0)
        {
            if(this.valArray[CurrentVals[0]] == undefined)
            {
                if($(this.config.fieldname))
                {
                    $(this.config.fieldname).value = '';
                }
                if($(this.config.fieldname + '_title'))
                {
                    $(this.config.fieldname + '_title').innerHTML = '';
                    SetDisplayDivSize($(this.config.fieldname + '_title'));
                }

                FieldChanged(this.config.fieldname);
            }
        }
    }

    this.destroy();
}

CodeSelectionCtrl.prototype.getXMLHeader = function()
{
    var xml;
    xml = '<!DOCTYPE codes [';
    xml += '<!ELEMENT value (#PCDATA)>';
    xml += '<!ELEMENT title (#PCDATA)>';
    xml += '<!ELEMENT description (#PCDATA)>';
    xml += '<!ELEMENT selected (#PCDATA)>';
    xml += '<!ELEMENT multiple (#PCDATA)>';
    xml += '<!ELEMENT search (#PCDATA)>';
    xml += '<!ELEMENT freetext (#PCDATA)>';
    xml += '<!ELEMENT empty (#PCDATA)>';
    xml += '<!ELEMENT ctrlidx (#PCDATA)>';
    xml += '<!ELEMENT error (#PCDATA)>';
    xml += '<!ELEMENT codes ((title, ctrlidx, multiple?, search?, freetext?, empty?, code*, error?))>';
    xml += '<!ELEMENT code ((value, description, selected?))>';
    xml += '<!ENTITY nbsp "&#160;">'; // no-break space = non-breaking space, U+00A0 ISOnum
    xml += '<!ENTITY iexcl "&#161;">'; // inverted exclamation mark, U+00A1 ISOnum
    xml += '<!ENTITY cent "&#162;">'; // cent sign, U+00A2 ISOnum
    xml += '<!ENTITY pound "&#163;">'; // pound sign, U+00A3 ISOnum
    xml += '<!ENTITY curren "&#164;">'; // currency sign, U+00A4 ISOnum
    xml += '<!ENTITY yen "&#165;">'; // yen sign = yuan sign, U+00A5 ISOnum
    xml += '<!ENTITY brvbar "&#166;">'; // broken bar = broken vertical bar, U+00A6 ISOnum
    xml += '<!ENTITY sect "&#167;">'; // section sign, U+00A7 ISOnum
    xml += '<!ENTITY uml "&#168;">'; // diaeresis = spacing diaeresis, U+00A8 ISOdia
    xml += '<!ENTITY copy "&#169;">'; // copyright sign, U+00A9 ISOnum
    xml += '<!ENTITY ordf "&#170;">'; // feminine ordinal indicator, U+00AA ISOnum
    xml += '<!ENTITY laquo "&#171;">'; // left-pointing double angle quotation mark = left pointing guillemet, U+00AB ISOnum
    xml += '<!ENTITY not "&#172;">'; // not sign, U+00AC ISOnum
    xml += '<!ENTITY shy "&#173;">'; // soft hyphen = discretionary hyphen, U+00AD ISOnum
    xml += '<!ENTITY reg "&#174;">'; // registered sign = registered trade mark sign, U+00AE ISOnum
    xml += '<!ENTITY macr "&#175;">'; // macron = spacing macron = overline = APL overbar, U+00AF ISOdia
    xml += '<!ENTITY deg "&#176;">'; // degree sign, U+00B0 ISOnum
    xml += '<!ENTITY plusmn "&#177;">'; // plus-minus sign = plus-or-minus sign, U+00B1 ISOnum
    xml += '<!ENTITY sup2 "&#178;">'; // superscript two = superscript digit two = squared, U+00B2 ISOnum
    xml += '<!ENTITY sup3 "&#179;">'; // superscript three = superscript digit three = cubed, U+00B3 ISOnum
    xml += '<!ENTITY acute "&#180;">'; // acute accent = spacing acute, U+00B4 ISOdia
    xml += '<!ENTITY micro "&#181;">'; // micro sign, U+00B5 ISOnum
    xml += '<!ENTITY para "&#182;">'; // pilcrow sign = paragraph sign, U+00B6 ISOnum
    xml += '<!ENTITY middot "&#183;">'; // middle dot = Georgian comma = Greek middle dot, U+00B7 ISOnum
    xml += '<!ENTITY cedil "&#184;">'; // cedilla = spacing cedilla, U+00B8 ISOdia
    xml += '<!ENTITY sup1 "&#185;">'; // superscript one = superscript digit one, U+00B9 ISOnum
    xml += '<!ENTITY ordm "&#186;">'; // masculine ordinal indicator, U+00BA ISOnum
    xml += '<!ENTITY raquo "&#187;">'; // right-pointing double angle quotation mark = right pointing guillemet, U+00BB ISOnum
    xml += '<!ENTITY frac14 "&#188;">'; // vulgar fraction one quarter = fraction one quarter, U+00BC ISOnum
    xml += '<!ENTITY frac12 "&#189;">'; // vulgar fraction one half = fraction one half, U+00BD ISOnum
    xml += '<!ENTITY frac34 "&#190;">'; // vulgar fraction three quarters = fraction three quarters, U+00BE ISOnum
    xml += '<!ENTITY iquest "&#191;">'; // inverted question mark = turned question mark, U+00BF ISOnum
    xml += '<!ENTITY Agrave "&#192;">'; // latin capital letter A with grave = latin capital letter A grave, U+00C0 ISOlat1
    xml += '<!ENTITY Aacute "&#193;">'; // latin capital letter A with acute, U+00C1 ISOlat1
    xml += '<!ENTITY Acirc "&#194;">'; // latin capital letter A with circumflex, U+00C2 ISOlat1
    xml += '<!ENTITY Atilde "&#195;">'; // latin capital letter A with tilde, U+00C3 ISOlat1
    xml += '<!ENTITY Auml "&#196;">'; // latin capital letter A with diaeresis, U+00C4 ISOlat1
    xml += '<!ENTITY Aring "&#197;">'; // latin capital letter A with ring above = latin capital letter A ring, U+00C5 ISOlat1
    xml += '<!ENTITY AElig "&#198;">'; // latin capital letter AE = latin capital ligature AE, U+00C6 ISOlat1
    xml += '<!ENTITY Ccedil "&#199;">'; // latin capital letter C with cedilla, U+00C7 ISOlat1
    xml += '<!ENTITY Egrave "&#200;">'; // latin capital letter E with grave, U+00C8 ISOlat1
    xml += '<!ENTITY Eacute "&#201;">'; // latin capital letter E with acute, U+00C9 ISOlat1
    xml += '<!ENTITY Ecirc "&#202;">'; // latin capital letter E with circumflex, U+00CA ISOlat1
    xml += '<!ENTITY Euml "&#203;">'; // latin capital letter E with diaeresis, U+00CB ISOlat1
    xml += '<!ENTITY Igrave "&#204;">'; // latin capital letter I with grave, U+00CC ISOlat1
    xml += '<!ENTITY Iacute "&#205;">'; // latin capital letter I with acute, U+00CD ISOlat1
    xml += '<!ENTITY Icirc "&#206;">'; // latin capital letter I with circumflex, U+00CE ISOlat1
    xml += '<!ENTITY Iuml "&#207;">'; // latin capital letter I with diaeresis, U+00CF ISOlat1
    xml += '<!ENTITY ETH "&#208;">'; // latin capital letter ETH, U+00D0 ISOlat1
    xml += '<!ENTITY Ntilde "&#209;">'; // latin capital letter N with tilde, U+00D1 ISOlat1
    xml += '<!ENTITY Ograve "&#210;">'; // latin capital letter O with grave, U+00D2 ISOlat1
    xml += '<!ENTITY Oacute "&#211;">'; // latin capital letter O with acute, U+00D3 ISOlat1
    xml += '<!ENTITY Ocirc "&#212;">'; // latin capital letter O with circumflex, U+00D4 ISOlat1
    xml += '<!ENTITY Otilde "&#213;">'; // latin capital letter O with tilde, U+00D5 ISOlat1
    xml += '<!ENTITY Ouml "&#214;">'; // latin capital letter O with diaeresis, U+00D6 ISOlat1
    xml += '<!ENTITY times "&#215;">'; // multiplication sign, U+00D7 ISOnum
    xml += '<!ENTITY Oslash "&#216;">'; // latin capital letter O with stroke = latin capital letter O slash, U+00D8 ISOlat1
    xml += '<!ENTITY Ugrave "&#217;">'; // latin capital letter U with grave, U+00D9 ISOlat1
    xml += '<!ENTITY Uacute "&#218;">'; // latin capital letter U with acute, U+00DA ISOlat1
    xml += '<!ENTITY Ucirc "&#219;">'; // latin capital letter U with circumflex, U+00DB ISOlat1
    xml += '<!ENTITY Uuml "&#220;">'; // latin capital letter U with diaeresis, U+00DC ISOlat1
    xml += '<!ENTITY Yacute "&#221;">'; // latin capital letter Y with acute, U+00DD ISOlat1
    xml += '<!ENTITY THORN "&#222;">'; // latin capital letter THORN, U+00DE ISOlat1
    xml += '<!ENTITY szlig "&#223;">'; // latin small letter sharp s = ess-zed, U+00DF ISOlat1
    xml += '<!ENTITY agrave "&#224;">'; // latin small letter a with grave = latin small letter a grave, U+00E0 ISOlat1
    xml += '<!ENTITY aacute "&#225;">'; // latin small letter a with acute, U+00E1 ISOlat1
    xml += '<!ENTITY acirc "&#226;">'; // latin small letter a with circumflex, U+00E2 ISOlat1
    xml += '<!ENTITY atilde "&#227;">'; // latin small letter a with tilde, U+00E3 ISOlat1
    xml += '<!ENTITY auml "&#228;">'; // latin small letter a with diaeresis, U+00E4 ISOlat1
    xml += '<!ENTITY aring "&#229;">'; // latin small letter a with ring above = latin small letter a ring,U+00E5 ISOlat1
    xml += '<!ENTITY aelig "&#230;">'; // latin small letter ae = latin small ligature ae, U+00E6 ISOlat1
    xml += '<!ENTITY ccedil "&#231;">'; // latin small letter c with cedilla, U+00E7 ISOlat1
    xml += '<!ENTITY egrave "&#232;">'; // latin small letter e with grave, U+00E8 ISOlat1
    xml += '<!ENTITY eacute "&#233;">'; // latin small letter e with acute, U+00E9 ISOlat1
    xml += '<!ENTITY ecirc "&#234;">'; // latin small letter e with circumflex, U+00EA ISOlat1
    xml += '<!ENTITY euml "&#235;">'; // latin small letter e with diaeresis, U+00EB ISOlat1
    xml += '<!ENTITY igrave "&#236;">'; // latin small letter i with grave, U+00EC ISOlat1
    xml += '<!ENTITY iacute "&#237;">'; // latin small letter i with acute, U+00ED ISOlat1
    xml += '<!ENTITY icirc "&#238;">'; // latin small letter i with circumflex, U+00EE ISOlat1
    xml += '<!ENTITY iuml "&#239;">'; // latin small letter i with diaeresis, U+00EF ISOlat1
    xml += '<!ENTITY eth "&#240;">'; // latin small letter eth, U+00F0 ISOlat1
    xml += '<!ENTITY ntilde "&#241;">'; // latin small letter n with tilde, U+00F1 ISOlat1
    xml += '<!ENTITY ograve "&#242;">'; // latin small letter o with grave, U+00F2 ISOlat1
    xml += '<!ENTITY oacute "&#243;">'; // latin small letter o with acute, U+00F3 ISOlat1
    xml += '<!ENTITY ocirc "&#244;">'; // latin small letter o with circumflex, U+00F4 ISOlat1
    xml += '<!ENTITY otilde "&#245;">'; // latin small letter o with tilde, U+00F5 ISOlat1
    xml += '<!ENTITY ouml "&#246;">'; // latin small letter o with diaeresis, U+00F6 ISOlat1
    xml += '<!ENTITY divide "&#247;">'; // division sign, U+00F7 ISOnum
    xml += '<!ENTITY oslash "&#248;">'; // latin small letter o with stroke, = latin small letter o slash, U+00F8 ISOlat1
    xml += '<!ENTITY ugrave "&#249;">'; // latin small letter u with grave, U+00F9 ISOlat1
    xml += '<!ENTITY uacute "&#250;">'; // latin small letter u with acute, U+00FA ISOlat1
    xml += '<!ENTITY ucirc "&#251;">'; // latin small letter u with circumflex, U+00FB ISOlat1
    xml += '<!ENTITY uuml "&#252;">'; // latin small letter u with diaeresis, U+00FC ISOlat1
    xml += '<!ENTITY yacute "&#253;">'; // latin small letter y with acute, U+00FD ISOlat1
    xml += '<!ENTITY thorn "&#254;">'; // latin small letter thorn, U+00FE ISOlat1
    xml += '<!ENTITY yuml "&#255;">'; // latin small letter y with diaeresis, U+00FF ISOlat1
    xml += '<!ENTITY Alpha "&#913;">'; // greek capital letter alpha, U+0391
    xml += '<!ENTITY Beta "&#914;">'; // greek capital letter beta, U+0392
    xml += '<!ENTITY Gamma "&#915;">'; // greek capital letter gamma, U+0393 ISOgrk3
    xml += '<!ENTITY Delta "&#916;">'; // greek capital letter delta, U+0394 ISOgrk3
    xml += '<!ENTITY Epsilon "&#917;">'; // greek capital letter epsilon, U+0395
    xml += '<!ENTITY Zeta "&#918;">'; // greek capital letter zeta, U+0396
    xml += '<!ENTITY Eta  "&#919;">'; // greek capital letter eta, U+0397
    xml += '<!ENTITY Theta "&#920;">'; // greek capital letter theta, U+0398 ISOgrk3
    xml += '<!ENTITY Iota "&#921;">'; // greek capital letter iota, U+0399
    xml += '<!ENTITY Kappa "&#922;">'; // greek capital letter kappa, U+039A
    xml += '<!ENTITY Lambda "&#923;">'; // greek capital letter lambda, U+039B ISOgrk3
    xml += '<!ENTITY Mu  "&#924;">'; // greek capital letter mu, U+039C
    xml += '<!ENTITY Nu  "&#925;">'; // greek capital letter nu, U+039D
    xml += '<!ENTITY Xi  "&#926;">'; // greek capital letter xi, U+039E ISOgrk3
    xml += '<!ENTITY Omicron "&#927;">'; // greek capital letter omicron, U+039F
    xml += '<!ENTITY Pi  "&#928;">'; // greek capital letter pi, U+03A0 ISOgrk3
    xml += '<!ENTITY Rho  "&#929;">'; // greek capital letter rho, U+03A1
    xml += '<!ENTITY Sigma "&#931;">'; // greek capital letter sigma, U+03A3 ISOgrk3
    xml += '<!ENTITY Tau  "&#932;">'; // greek capital letter tau, U+03A4
    xml += '<!ENTITY Upsilon "&#933;">'; // greek capital letter upsilon, U+03A5 ISOgrk3
    xml += '<!ENTITY Phi  "&#934;">'; // greek capital letter phi, U+03A6 ISOgrk3
    xml += '<!ENTITY Chi  "&#935;">'; // greek capital letter chi, U+03A7
    xml += '<!ENTITY Psi  "&#936;">'; // greek capital letter psi, U+03A8 ISOgrk3
    xml += '<!ENTITY Omega "&#937;">'; // greek capital letter omega, U+03A9 ISOgrk3
    xml += '<!ENTITY alpha "&#945;">'; // greek small letter alpha, U+03B1 ISOgrk3
    xml += '<!ENTITY beta "&#946;">'; // greek small letter beta, U+03B2 ISOgrk3
    xml += '<!ENTITY gamma "&#947;">'; // greek small letter gamma, U+03B3 ISOgrk3
    xml += '<!ENTITY delta "&#948;">'; // greek small letter delta, U+03B4 ISOgrk3
    xml += '<!ENTITY epsilon "&#949;">'; // greek small letter epsilon, U+03B5 ISOgrk3
    xml += '<!ENTITY zeta "&#950;">'; // greek small letter zeta, U+03B6 ISOgrk3
    xml += '<!ENTITY eta  "&#951;">'; // greek small letter eta, U+03B7 ISOgrk3
    xml += '<!ENTITY theta "&#952;">'; // greek small letter theta, U+03B8 ISOgrk3
    xml += '<!ENTITY iota "&#953;">'; // greek small letter iota, U+03B9 ISOgrk3
    xml += '<!ENTITY kappa "&#954;">'; // greek small letter kappa, U+03BA ISOgrk3
    xml += '<!ENTITY lambda "&#955;">'; // greek small letter lambda, U+03BB ISOgrk3
    xml += '<!ENTITY mu  "&#956;">'; // greek small letter mu, U+03BC ISOgrk3
    xml += '<!ENTITY nu  "&#957;">'; // greek small letter nu, U+03BD ISOgrk3
    xml += '<!ENTITY xi  "&#958;">'; // greek small letter xi, U+03BE ISOgrk3
    xml += '<!ENTITY omicron "&#959;">'; // greek small letter omicron, U+03BF NEW
    xml += '<!ENTITY pi  "&#960;">'; // greek small letter pi, U+03C0 ISOgrk3
    xml += '<!ENTITY rho  "&#961;">'; // greek small letter rho, U+03C1 ISOgrk3
    xml += '<!ENTITY sigmaf "&#962;">'; // greek small letter final sigma, U+03C2 ISOgrk3
    xml += '<!ENTITY sigma "&#963;">'; // greek small letter sigma, U+03C3 ISOgrk3
    xml += '<!ENTITY tau  "&#964;">'; // greek small letter tau, U+03C4 ISOgrk3
    xml += '<!ENTITY upsilon "&#965;">'; // greek small letter upsilon, U+03C5 ISOgrk3
    xml += '<!ENTITY phi  "&#966;">'; // greek small letter phi, U+03C6 ISOgrk3
    xml += '<!ENTITY chi  "&#967;">'; // greek small letter chi, U+03C7 ISOgrk3
    xml += '<!ENTITY psi  "&#968;">'; // greek small letter psi, U+03C8 ISOgrk3
    xml += '<!ENTITY omega "&#969;">'; // greek small letter omega, U+03C9 ISOgrk3
    xml += '<!ENTITY thetasym "&#977;">'; // greek small letter theta symbol, U+03D1 NEW
    xml += '<!ENTITY upsih "&#978;">'; // greek upsilon with hook symbol, U+03D2 NEW
    xml += '<!ENTITY piv  "&#982;">'; // greek pi symbol, U+03D6 ISOgrk3
    xml += '<!ENTITY bull "&#8226;">'; // bullet = black small circle, U+2022 ISOpub
    xml += '<!ENTITY hellip "&#8230;">'; // horizontal ellipsis = three dot leader, U+2026 ISOpub
    xml += '<!ENTITY prime "&#8242;">'; // prime = minutes = feet, U+2032 ISOtech
    xml += '<!ENTITY Prime "&#8243;">'; // double prime = seconds = inches,U+2033 ISOtech
    xml += '<!ENTITY oline "&#8254;">'; // overline = spacing overscore,U+203E NEW
    xml += '<!ENTITY frasl "&#8260;">'; // fraction slash, U+2044 NEW
    xml += '<!ENTITY weierp "&#8472;">'; // script capital P = power set = Weierstrass p, U+2118 ISOamso
    xml += '<!ENTITY image "&#8465;">'; // blackletter capital I = imaginary part, U+2111 ISOamso
    xml += '<!ENTITY real "&#8476;">'; // blackletter capital R = real part symbol, U+211C ISOamso
    xml += '<!ENTITY trade "&#8482;">'; // trade mark sign, U+2122 ISOnum
    xml += '<!ENTITY alefsym "&#8501;">'; // alef symbol = first transfinite cardinal, U+2135 NEW
    xml += '<!ENTITY larr "&#8592;">'; // leftwards arrow, U+2190 ISOnum
    xml += '<!ENTITY uarr "&#8593;">'; // upwards arrow, U+2191 ISOnum-->
    xml += '<!ENTITY rarr "&#8594;">'; // rightwards arrow, U+2192 ISOnum
    xml += '<!ENTITY darr "&#8595;">'; // downwards arrow, U+2193 ISOnum
    xml += '<!ENTITY harr "&#8596;">'; // left right arrow, U+2194 ISOamsa
    xml += '<!ENTITY crarr "&#8629;">'; // downwards arrow with corner leftwards = carriage return, U+21B5 NEW
    xml += '<!ENTITY lArr "&#8656;">'; // leftwards double arrow, U+21D0 ISOtech
    xml += '<!ENTITY uArr "&#8657;">'; // upwards double arrow, U+21D1 ISOamsa
    xml += '<!ENTITY rArr "&#8658;">'; // rightwards double arrow, U+21D2 ISOtech
    xml += '<!ENTITY dArr "&#8659;">'; // downwards double arrow, U+21D3 ISOamsa
    xml += '<!ENTITY hArr "&#8660;">'; // left right double arrow, U+21D4 ISOamsa
    xml += '<!ENTITY forall "&#8704;">'; // for all, U+2200 ISOtech
    xml += '<!ENTITY part "&#8706;">'; // partial differential, U+2202 ISOtech
    xml += '<!ENTITY exist "&#8707;">'; // there exists, U+2203 ISOtech
    xml += '<!ENTITY empty "&#8709;">'; // empty set = null set = diameter, U+2205 ISOamso
    xml += '<!ENTITY nabla "&#8711;">'; // nabla = backward difference, U+2207 ISOtech
    xml += '<!ENTITY isin "&#8712;">'; // element of, U+2208 ISOtech
    xml += '<!ENTITY notin "&#8713;">'; // not an element of, U+2209 ISOtech
    xml += '<!ENTITY ni  "&#8715;">'; // contains as member, U+220B ISOtech
    xml += '<!ENTITY prod "&#8719;">'; // n-ary product = product sign, U+220F ISOamsb
    xml += '<!ENTITY sum  "&#8721;">'; // n-ary sumation, U+2211 ISOamsb
    xml += '<!ENTITY minus "&#8722;">'; // minus sign, U+2212 ISOtech
    xml += '<!ENTITY lowast "&#8727;">'; // asterisk operator, U+2217 ISOtech
    xml += '<!ENTITY radic "&#8730;">'; // square root = radical sign, U+221A ISOtech
    xml += '<!ENTITY prop "&#8733;">'; // proportional to, U+221D ISOtech
    xml += '<!ENTITY infin "&#8734;">'; // infinity, U+221E ISOtech
    xml += '<!ENTITY ang  "&#8736;">'; // angle, U+2220 ISOamso
    xml += '<!ENTITY and  "&#8743;">'; // logical and = wedge, U+2227 ISOtech
    xml += '<!ENTITY or  "&#8744;">'; // logical or = vee, U+2228 ISOtech
    xml += '<!ENTITY cap  "&#8745;">'; // intersection = cap, U+2229 ISOtech
    xml += '<!ENTITY cup  "&#8746;">'; // union = cup, U+222A ISOtech
    xml += '<!ENTITY int  "&#8747;">'; // integral, U+222B ISOtech
    xml += '<!ENTITY there4 "&#8756;">'; // therefore, U+2234 ISOtech
    xml += '<!ENTITY sim  "&#8764;">'; // tilde operator = varies with = similar to, U+223C ISOtech
    xml += '<!ENTITY cong "&#8773;">'; // approximately equal to, U+2245 ISOtech
    xml += '<!ENTITY asymp "&#8776;">'; // almost equal to = asymptotic to, U+2248 ISOamsr
    xml += '<!ENTITY ne  "&#8800;">'; // not equal to, U+2260 ISOtech
    xml += '<!ENTITY equiv "&#8801;">'; // identical to, U+2261 ISOtech
    xml += '<!ENTITY le  "&#8804;">'; // less-than or equal to, U+2264 ISOtech
    xml += '<!ENTITY ge  "&#8805;">'; // greater-than or equal to, U+2265 ISOtech
    xml += '<!ENTITY sub  "&#8834;">'; // subset of, U+2282 ISOtech
    xml += '<!ENTITY sup  "&#8835;">'; // superset of, U+2283 ISOtech
    xml += '<!ENTITY nsub "&#8836;">'; // not a subset of, U+2284 ISOamsn
    xml += '<!ENTITY sube "&#8838;">'; // subset of or equal to, U+2286 ISOtech
    xml += '<!ENTITY supe "&#8839;">'; // superset of or equal to, U+2287 ISOtech
    xml += '<!ENTITY oplus "&#8853;">'; // circled plus = direct sum, U+2295 ISOamsb
    xml += '<!ENTITY otimes "&#8855;">'; // circled times = vector product, U+2297 ISOamsb
    xml += '<!ENTITY perp "&#8869;">'; // up tack = orthogonal to = perpendicular, U+22A5 ISOtech
    xml += '<!ENTITY sdot "&#8901;">'; // dot operator, U+22C5 ISOamsb
    xml += '<!ENTITY lceil "&#8968;">'; // left ceiling = apl upstile, U+2308 ISOamsc
    xml += '<!ENTITY rceil "&#8969;">'; // right ceiling, U+2309 ISOamsc
    xml += '<!ENTITY lfloor "&#8970;">'; // left floor = apl downstile, U+230A ISOamsc
    xml += '<!ENTITY rfloor "&#8971;">'; // right floor, U+230B ISOamsc
    xml += '<!ENTITY lang "&#9001;">'; // left-pointing angle bracket = bra, U+2329 ISOtech
    xml += '<!ENTITY rang "&#9002;">'; // right-pointing angle bracket = ket, U+232A ISOtech
    xml += '<!ENTITY loz  "&#9674;">'; // lozenge, U+25CA ISOpub
    xml += '<!ENTITY spades "&#9824;">'; // black spade suit, U+2660 ISOpub
    xml += '<!ENTITY clubs "&#9827;">'; // black club suit = shamrock, U+2663 ISOpub
    xml += '<!ENTITY hearts "&#9829;">'; // black heart suit = valentine, U+2665 ISOpub
    xml += '<!ENTITY diams "&#9830;">'; // black diamond suit, U+2666 ISOpub
    xml += '<!ENTITY OElig "&#338;">'; // latin capital ligature OE, U+0152 ISOlat2
    xml += '<!ENTITY oelig "&#339;">'; // latin small ligature oe, U+0153 ISOlat2
    xml += '<!ENTITY Scaron "&#352;">'; // latin capital letter S with caron, U+0160 ISOlat2
    xml += '<!ENTITY scaron "&#353;">'; // latin small letter s with caron, U+0161 ISOlat2
    xml += '<!ENTITY Yuml "&#376;">'; // latin capital letter Y with diaeresis, U+0178 ISOlat2
    xml += '<!ENTITY circ "&#710;">'; // modifier letter circumflex accent, U+02C6 ISOpub
    xml += '<!ENTITY tilde "&#732;">'; // small tilde, U+02DC ISOdia
    xml += '<!ENTITY ensp "&#8194;">'; // en space, U+2002 ISOpub
    xml += '<!ENTITY emsp "&#8195;">'; // em space, U+2003 ISOpub
    xml += '<!ENTITY thinsp "&#8201;">'; // thin space, U+2009 ISOpub
    xml += '<!ENTITY zwnj "&#8204;">'; // zero width non-joiner, U+200C NEW RFC 2070
    xml += '<!ENTITY zwj "&#8205;">'; // zero width joiner, U+200D NEW RFC 2070
    xml += '<!ENTITY lrm "&#8206;">'; // left-to-right mark, U+200E NEW RFC 2070
    xml += '<!ENTITY rlm "&#8207;">'; // right-to-left mark, U+200F NEW RFC 2070
    xml += '<!ENTITY ndash "&#8211;">'; // en dash, U+2013 ISOpub
    xml += '<!ENTITY mdash "&#8212;">'; // em dash, U+2014 ISOpub
    xml += '<!ENTITY lsquo "&#8216;">'; // left single quotation mark, U+2018 ISOnum
    xml += '<!ENTITY rsquo "&#8217;">'; // right single quotation mark, U+2019 ISOnum
    xml += '<!ENTITY sbquo "&#8218;">'; // single low-9 quotation mark, U+201A NEW
    xml += '<!ENTITY ldquo "&#8220;">'; // left double quotation mark, U+201C ISOnum
    xml += '<!ENTITY rdquo "&#8221;">'; // right double quotation mark, U+201D ISOnum
    xml += '<!ENTITY bdquo "&#8222;">'; // double low-9 quotation mark, U+201E NEW
    xml += '<!ENTITY dagger "&#8224;">'; // dagger, U+2020 ISOpub
    xml += '<!ENTITY Dagger "&#8225;">'; // double dagger, U+2021 ISOpub
    xml += '<!ENTITY permil "&#8240;">'; // per mille sign, U+2030 ISOtech
    xml += '<!ENTITY lsaquo "&#8249;">'; // single left-pointing angle quotation mark, U+2039 ISO proposed
    xml += '<!ENTITY rsaquo "&#8250;">'; // single right-pointing angle quotation mark, U+203A ISO proposed
    xml += '<!ENTITY euro "&#8364;">'; //euro sign, U+20AC NEW
    xml += ']>';

    return xml;
}

CodeSelectionCtrl.prototype.getXMLFromArray = function()
{
    var xml = this.getXMLHeader();

    xml += '<codes>';
    xml += '<title>' + this.config.overrideTitle + '</title>';
    xml += '<ctrlidx>'+this.ControlIndex+'</ctrlidx>';

    if(this.config.overrideArray.length == 0)
    {
        xml += '<empty>empty</empty>';
    }
    else
    {
        if (this.config.searchmode || this.config.multilistbox)
        {
            xml += '<multiple>multiple</multiple>';
        }

        if (!this.config.searchmode && !this.config.multilistbox && !this.config.nochoose)
        {
            xml += '<code><value/><description/></code>';
        }

        // Get current values
        var values = new Array();
        if (this.config.multilistbox)
        {
            for(var i = 0; i<$(this.config.fieldname).options.length; i++)
            {
                values.push($(this.config.fieldname).options[i].value);
            }
        }
        else
        {
            values.push($(this.config.fieldname).value);
        }
        var j=0;

        for(code in this.config.overrideArray)
        {
            if (!((values.indexOf(code) != -1) && (this.config.searchmode || this.config.multilistbox)))
            {
                xml += '<code>';
                xml += '<value>';
                xml += code;
                xml += '</value>';

                xml += '<description>';
                xml += this.config.overrideArray[code];
                xml += '</description>';

                if (values.indexOf(code) != -1)
                {
                    xml += '<selected>selected</selected>';
                }

                xml += '</code>';
            }
        }
    }

    xml += '</codes>';

    return xml;
}

CodeSelectionCtrl.prototype.getTitleFromXML = function(xml)
{
    for (var i = 0; i < xml.childNodes.length; i++)
    {
       if(xml.childNodes.item(i).tagName == 'codes')
       {
           for (j = 0; j < xml.childNodes.item(i).childNodes.length; j++)
           {
               if(xml.childNodes.item(i).childNodes.item(j).tagName == 'title')
               {
                    return xml.childNodes.item(i).childNodes.item(j).data
               }
           }
       }
    }
}

CodeSelectionCtrl.prototype.updateNumbers = function()
{
    if (!this.config.showNumbers)
    {
        return;
    }
    
    var text = '';
    jQuery("#"+this.config.fieldname).find("option").each(function(i, option)
    {
        text = jQuery(option).text().replace(/^\d+:\s/, "");
        jQuery(option).text((i+1)+": "+text);
    });
};

var overrideArray = new Array();
var CurrentCodeSelectionCtrl = new CodeSelectionCtrl('CurrentCodeSelectionCtrl');
var GlobalCodeSelectControls = new Array();
var undefined;
var returnCodeOnly;

function getClassName(obj)
{
    OldAlert(typeof obj);
    if (typeof obj != "object" || obj === null)
    {
        return false;
    }

    return /(\w+)\(/.exec(obj.constructor.toString())[1];
}

/* --- Custom AJAX timeout functionality --- */

/**
* Determines whether or not an AJAX request is currently still in progress.
*
* @param  object   xmlhttp  The AJAX request transport.
*
* @return boolean           Whether or not this request is still in progress.
*/
function callInProgress(xmlhttp)
{
    switch (xmlhttp.readyState)
    {
        case 1:
        case 2:
        case 3:
            return true;
        break;

        // Case 4 and 0
        default:
            return false;
        break;
    }
}

/**
* Determines whether or not an AJAX request should be controlled by our custom timeout
* functionality by comparing the URL of the request to a list of predefined URLs.
*
* @param  string   url     The URL of the AJAX request.
*
* @return boolean  result  Whether or not this request is subject to timeout control.
*/
function controlAjaxRequest(url)
{
    var result = false;

    // controlled AJAX requests are currently retrieving code lists and adding dynamic form sections
    var validUrls = oc(['codelist','addcontacttoDIF1','adddynamicsection']);

    for (validUrl in validUrls)
    {
        if (url.indexOf(validUrl) > 0)
        {
            result = true;
            break;
        }
    }

    return result;
}

/**
* Initiates a global AJAX responder and defines call-back functions to handle timeout functionality.
*
* @global string globals['AJAX_WAIT_TIME'] The user-defined time period (in seconds) to wait after an Ajax request
*                                          is fired before displaying the 'Please wait' pop up dialogue.
*/
function setupAjaxTimeouts()
{
    // Register global responders that will occur on all AJAX requests
    Ajax.Responders.register(
    {
        onCreate: function(request)
        {
            // Filter out specific AJAX calls
            if (controlAjaxRequest(request.url))
            {
                request['timeoutId'] = window.setTimeout(
                    function()
                    {
                        // If we have hit the timeout and the AJAX request is active, display the wait dialogue
                        if (callInProgress(request.transport))
                        {
                            displayWaitPopup(request);
                        }
                    },
                    parseFloat(globals['AJAX_WAIT_TIME']) * 1000
                );
            }
        },

        onComplete: function(request)
        {
            if (controlAjaxRequest(request.url))
            {
                // Clear the timeout, the request completed ok
                window.clearTimeout(request['timeoutId']);
                GetFloatingDiv('wait_popup').CloseFloatingControl();
            }
        }
    });
}

/*
* Switches AJAX timeout functionality on if the system is configured to use it.
*
* @global string globals['AJAX_WAIT_TIME'] "Y" if the functionality is enabled in the system config.
*/
function switchOnAjaxTimeouts()
{
    if (globals['AJAX_WAIT_MSG'] == 'Y')
    {
        setupAjaxTimeouts();
    }
}

// switch on timeout functionality (if configured) on page load
Event.observe(window, 'load', switchOnAjaxTimeouts);

/* --- End of custom AJAX timeout functionality --- */