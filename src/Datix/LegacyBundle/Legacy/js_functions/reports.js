
function PromptUserAddToMyReports(rep_name)
{
    AlertWindow = AddNewFloatingDiv('PromptUserAddToMyReports');

    var message = '<div style="width:500px;text-align:left">' +
    '<br/>' +
    'Report name: <input id="new_rep_name" name="new_rep_name" type="text" size="50" value="' + rep_name + '" />' +
    '<br/></div>';

    AlertWindow.setContents(message);
    AlertWindow.setTitle("Add to My Reports");

    var buttonHTML = '';
    var buttons = new Array();

    buttonHTML += '<input type="button" value="Save" onClick="SetGlobalAddToMyReportsValues();var div = GetFloatingDiv(\'PromptUserAddToMyReports\');div.CloseFloatingControl();AddtoMyReports();" />';
    buttonHTML += '<input type="button" value="Cancel" onClick="var div = GetFloatingDiv(\'PromptUserAddToMyReports\');div.CloseFloatingControl();" />';
    AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);
    AlertWindow.display();
}

function SetGlobalAddToMyReportsValues()
{
    $('rep_name').value = $('new_rep_name').value;
}

/**
 *  Calls ajax function to add a new widget to the users default dashboard, then redirects browser to this dashboard.
 *
 * @param int reportid: The value to be stored in the dlink_package_id field - tracks the id of the packaged report we are linking to.
 * @param string reporttype: The type of widget we are adding.
 * @param string module: The code for the module the widget is related to.
 */

function AddToMyDashboard(reportid, reporttype, module)
{
    jQuery.get(scripturl+'?action=httprequest&type=addtomydashboard&report_id='+reportid+'&report_type='+reporttype+'&module='+module);

    SendTo(scripturl+'?action=dashboard');
}

/**
 *  Calls ajax function to add the current report as a new widget to the users default dashboard, then redirects browser to this dashboard.
 */
function AddCurrentToMyDashboard(ReportTitle)
{
    jQuery.get(scripturl+'?action=addcurrenttomydashboard&title='+ReportTitle);

    SendTo(scripturl+'?action=dashboard');
}

/**
 *  Calls ajax function to add the current report as a new widget to the users default dashboard, then redirects browser to this dashboard.
 */
function AddCurrentToMyReports(module, ReportTitle)
{
    jQuery.get(scripturl+'?action=httprequest&type=addcurrenttomyreports&title='+ReportTitle);

    SendTo(scripturl+'?action=listmyreports&module='+module+'&form=predefined');
}

/**
 * Similar to the PromptUserAddToMyReports() function, this function works on session-stored reports.
 *
 * Presents the user with a popup to name the current report, and then saves it to packaged reports. Can also add to the dashboard
 * if desired.
 */
function PromptForReportTitle(rep_name, module, dashboard, rep_id)
{
    jQuery.ajax({
        url: scripturl+'?action=httprequest&type=getreportprompttext&dashboard='+dashboard+'&module='+module+'&rep_id='+rep_id+'&rep_name='+rep_name,
        success: function(data)
        {
            var sHTML = SplitJSAndHTML(data);

            AlertWindow = AddNewFloatingDiv('PromptUserAddToMyReports');

            AlertWindow.setContents(sHTML);
            AlertWindow.setTitle("Set Report Title");

            var buttonHTML = '';
            var buttons = new Array();

            buttonHTML += '<input type="button" value="Save" onClick="SaveReportPrompt('+dashboard+', \''+module+'\');" />';
            buttonHTML += '<input type="button" value="Cancel" onClick="var div = GetFloatingDiv(\'PromptUserAddToMyReports\');div.CloseFloatingControl();" />';
            AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);
            AlertWindow.display();
            RunGlobalJavascript();
        },
        error: function(request, status, error)
        {
            OldAlert(request.responseText);
        }
    });
}

function SaveReportPrompt(dashboard, module)
{
    var ReportTitle = encodeURIComponent(jQuery('#new_rep_name').val());
    var UseValues = jQuery('#use_values').val();
    var TargetDashboard = jQuery('#target_dashboard').val();

    var div = GetFloatingDiv('PromptUserAddToMyReports');
    div.CloseFloatingControl();

    if(dashboard)
    {
        jQuery.getJSON(scripturl+'?action=addcurrenttomydashboard&responsetype=json&title='+ReportTitle+'&use_values='+UseValues+'&dash_id='+TargetDashboard, function(data) {
            if(data.success)
            {
                SendTo(scripturl+'?action=dashboard&dash_id='+data.dash_id);
            }
            else
            {
                OldAlert('An error occurred');
            }
        });
    }
    else
    {
        jQuery.get(scripturl+'?action=httprequest&type=addcurrenttomyreports&title='+ReportTitle+'&use_values='+UseValues,
            function(data){SendTo(scripturl+'?action=listmyreports&module='+module+'&form=predefined');});
    }
}


