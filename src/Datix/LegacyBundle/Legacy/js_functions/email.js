function filterTemplateTypes(id)
{
    mod = getValuesArray(id)
    //var mod = element.options[element.selectedIndex].value;
    
    if(mod)
    {
        new Ajax.Request
        (
            scripturl+'?action=httprequest&type=gettemplatetypes&module='+mod,
            {
                method:'get',
                onSuccess: function(r)
                {
                    $('emt_type_div').innerHTML = r.responseText;
                }
            }
        );
    }
}

function changePreviewedEmail(elementid, module, recordid, body_div, subj_div)
{
    new Ajax.Request
    (
        scripturl+'?action=httprequest&type=getemaildetails',
        {
            method:'post',
            postBody: 'returntype=subject&recordid='+recordid+'&module='+module+'&emailid='+getValuesArray(elementid),
            onSuccess: function(r)
            {
                $(subj_div).value = r.responseText;
            }
        }
    );

    new Ajax.Request
    (
        scripturl+'?action=httprequest&type=getemaildetails',
        {
            method:'post',
            postBody: 'returntype=body&recordid='+recordid+'&module='+module+'&emailid='+getValuesArray(elementid),
            onSuccess: function(r)
            {
                $(body_div).value = r.responseText;
            }
        }
    );
}

function ConfirmChoices(recordid)
{
    var elementsToCheck = document.getElementsByTagName('select');
    var postString = Array();
    
    for(var i=0;i<elementsToCheck.length;i++)
    {
        if(elementsToCheck[i].getAttribute('name')=='duplicate_select_box')
        {
            var code = elementsToCheck[i].getAttribute('id').toString().replace('duplicate_select_','');
            var code_choice = elementsToCheck[i].options[elementsToCheck[i].selectedIndex].value.replace(code,'');

            postString.push(code+'='+code_choice);
        }
    }
    
    var finalPostString = postString.join('&');

    new Ajax.Request
    (
        scripturl+'?action=httprequest&type=getemaildetails',
        {
            method:'post',
            postBody: finalPostString+'&returntype=body&returnformat=plain&recordid='+recordid+'&emailid='+document.getElementById('email_template_select').options[document.getElementById('email_template_select').selectedIndex].value,
            onSuccess: function(r)
            {
                $(body_tag+"_container").innerHTML = '<textarea id="'+body_tag+'" name="'+body_tag+'" rows="20" cols="70">'+r.responseText+'</textarea>';
                $("email_duplicate_choices").innerHTML = "";
                document.getElementsByName(button_name)[0].disabled = false;
            }
        }
    );
    new Ajax.Request
    (
        scripturl+'?action=httprequest&type=getemaildetails',
        {
            method:'post',
            postBody: finalPostString+'&returntype=subject&returnformat=plain&recordid='+recordid+'&emailid='+document.getElementById('email_template_select').options[document.getElementById('email_template_select').selectedIndex].value,
            onSuccess: function(r)
            {
                $(subj_tag+"_container").innerHTML = '<textarea id="'+subj_tag+'" name="'+subj_tag+'" rows="2" cols="70">'+r.responseText+'</textarea>';
            }
        }
    );
}

var global_module_choice='ALL';
function showHideModuleSections(element)
{
    var target_name = element.options[element.selectedIndex].value+'_defaults';
    
    var elementsToCheck = document.getElementsByTagName('tr');
    for(var i=0;i<elementsToCheck.length;i++)
    {
        if(elementsToCheck[i].getAttribute('name')==target_name)
        {
            elementsToCheck[i].style.display="";
        }
        else if(elementsToCheck[i].getAttribute('name')==global_module_choice+'_defaults')
        {
            elementsToCheck[i].style.display="none";
        }
    }
    
    global_module_choice = element.options[element.selectedIndex].value;
}

function emt_validateOnSubmit()
{
    if($('emt_module').value == '' || $('emt_type').value == '' || $('emt_name').value == '')
    {
        alert("There are mandatory fields that have not been filled in");
        return false;
    }
    
    return true;
}
