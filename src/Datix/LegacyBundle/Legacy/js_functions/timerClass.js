var oCountdown;

function Timer(nLength, sType, sHandle, sId)
{
    this.sId = sId;
    this.sType = sType;
    this.nLength = nLength;

    this.beforeAlert = 0;

    this.maxlengthmins = nLength;
    this.maxlength = nLength * 60000;

    this.sHandle = sHandle;
}

Timer.prototype.Reset = function(tTime)
{
    var currentDate = new Date();

    if(this.lastreset == null)
    {
        this.lastreset = currentDate;
    }
    else if(currentDate.getTime() - this.lastreset.getTime() > 60000)
    {
        new Ajax.Request
        (
            scripturl+"?action=httprequest&type=resettimer&timertype="+this.sType+"&id="+this.sId,
            {
                method:'get'
            }
        );

        this.lastreset = currentDate;
    }


    if(this.timer)
    {
        clearTimeout(this.timer);
    }

    if(tTime)
    {
        var timeLength = tTime;
    }
    else
    {
        var timeLength = this.maxlength;
    }

    this.timer = setTimeout(this.sHandle+".TimeEnd()", timeLength);
}

Timer.prototype.TimeEnd = function()
{
    var currentTimer = this;

    if(this.sType == "REC")
    {
        // need to check whether lock has become active yet.
        jQuery.getJSON(scripturl+'?action=httprequest&type=checklock&responsetype=json', function(data) {
            if(data.success)
            {
                if(data.lock_active == true)
                {
                    currentTimer.displayAlert();
                }
                else
                {
                    currentTimer.Reset();
                }
            }
            else
            {
                OldAlert('Error: '+data.message);
            }
            });
    }
    else
    {
        this.displayAlert();
    }
}

Timer.prototype.Unlock = function()
{
    if(this.sType == 'REC')
    {
        new Ajax.Request
        (
            scripturl+"?action=httprequest&type=unlockrecord&lock_id="+this.sId,
            {
                method:'get'
            }
        );

    }
}

Timer.prototype.displayAlert = function()
{
    this.beforeAlert = new Date();
    var buttons = new Array();

    if(this.sType == "REC")
    {
        var message = "This record has been requested by another user since it was locked by you. You may continue to edit the record or cancel your changes and return to the main menu.<br /><br />If you do not respond within one minute, the record will be unlocked and your changes lost.<br /><br />Seconds remaining: <span id='countdown_monitor'>60</span>";
        buttons[0]={'value':'Continue editing','onclick':'GetFloatingDiv(\'alert\').CloseFloatingControl();'+this.sHandle+'.extendTimer()'};
        buttons[1]={'value':'Cancel changes','onclick':'timedOut(\''+this.sType+'\')'};

        oCountdown = new Countdown(60,'timedOut(\''+this.sType+'\')','countdown_monitor', 'oCountdown');
    }
    else if(this.sType == "SES")
    {
        var message = "Your session has been inactive for more than "+this.maxlengthmins+" minute"+(this.maxlengthmins == 1 ? '' : 's')+". For your security, your connection will be logged out if there is no activity after one further minute.<br /><br />If you do not wish to be logged out, click 'Extend session' and your session will be extended for a further "+this.maxlengthmins+" minute"+(this.maxlengthmins == 1 ? '' : 's')+".<br /><br />Seconds remaining: <span id='countdown_monitor'>60</span>";
        buttons[0]={'value':'Extend session','onclick':'GetFloatingDiv(\'alert\').CloseFloatingControl();'+this.sHandle+'.extendTimer()'};
        buttons[1]={'value':'Log out','onclick':'timedOut(\''+this.sType+'\')'};

        oCountdown = new Countdown(60,'timedOut(\''+this.sType+'\')','countdown_monitor', 'oCountdown');
    }
    else if(this.sType == "LOGGED_OUT")
    {
        var message = "Your session has been inactive for more than "+this.maxlengthmins+" minute"+(this.maxlengthmins == 1 ? '' : 's')+".<br /><br />The form will be reset if there is no activity after one further minute. Please click 'Continue' to continue editing this form.<br /><br />Seconds remaining: <span id='countdown_monitor'>60</span>";
        buttons[0]={'value':'Continue','onclick':'GetFloatingDiv(\'alert\').CloseFloatingControl();'+this.sHandle+'.extendTimer()'};
        buttons[1]={'value':'Reset Form','onclick':'timedOut(\''+this.sType+'\')'};

        oCountdown = new Countdown(60,'timedOut(\''+this.sType+'\')','countdown_monitor', 'oCountdown');
    }

    this.DisplayFloatingAlert('Alert', message, buttons);
    window.focus();
}

Timer.prototype.extendTimer = function()
{
    var afterAlert = new Date();
    var difference = afterAlert.getTime() - this.beforeAlert.getTime();

    clearTimeout(oCountdown.timer);
    oCountdown = null;

    if (difference > 60000)
    {
        timedOut(this.sType);
    }
    else
    {
        this.Reset();
    }
}

Timer.prototype.DisplayFloatingAlert = function(title, message, buttons)
{
    AlertWindow = AddNewFloatingDiv('alert');

    message = '<div style="width:600px">'+message+'</div>';

    AlertWindow.setContents(message);
    AlertWindow.setTitle(title);

    var buttonHTML = '';

    for(var i=0; i<buttons.length; i++)
    {
        buttonHTML += '<input type="button" value="'+buttons[i].value+'" onClick="'+buttons[i].onclick+'" />';
    }

    AlertWindow.setHTMLSection('floating_window_buttons', buttonHTML);

    AlertWindow.display();
}

/*
 * Countdown class simulates a second-by-second countdown.
 *
 * nLength = seconds left
 * sEval = string that will be eval()'d when the countdown finishes
 * sMonitor = HTML element to update with the seconds left.
 * sHandle = The variable pointing to 'this'. Needed for use in setTimeout.
 */

function Countdown(nLength, sEval, sMonitor, sHandle)
{
    this.sEval = sEval;
    this.sMonitor = sMonitor;
    this.nLength = nLength;
    this.sHandle = sHandle;

    this.timer = '';

    this.Start()
}

Countdown.prototype.Start = function()
{
    this.timer = setTimeout(this.sHandle+".Count()",1000);
}

Countdown.prototype.Count = function()
{
    this.nLength--;

    if(this.nLength <= 0)
    {
        eval(this.sEval);
    }
    else
    {
        if($(this.sMonitor))
        {
            $(this.sMonitor).innerHTML = this.nLength;
        }
        this.timer = setTimeout(this.sHandle+".Count()",1000);
    }
}
