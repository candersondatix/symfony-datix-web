jQuery(document).ready(function() {
	if (jQuery('#unique-passwords-disable:checked').is(':checked')) {
		jQuery('#unique-passwords').attr('disabled', true);
	}
	
	jQuery('#unique-passwords-disable').change(function() {
		var checked = jQuery(this).is(':checked');
		jQuery('#unique-passwords').attr('disabled', checked);
	});
});