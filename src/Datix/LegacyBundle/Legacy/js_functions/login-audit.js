jQuery(document).ready(function() {
	if (jQuery('#login-inactivity-disable:checked').is(':checked')) {
		jQuery('#login-inactivity').attr('disabled', true);
	}
	
	jQuery('#login-inactivity-disable').change(function() {
		var checked = jQuery(this).is(':checked');
		jQuery('#login-inactivity').attr('disabled', checked);
	});
});