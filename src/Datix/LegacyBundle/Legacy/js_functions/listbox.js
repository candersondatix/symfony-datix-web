function addCode(selectBox, multiListBox)
{
	var currentCode = selectBox.options[selectBox.selectedIndex].value;
	var currentDescr = selectBox.options[selectBox.selectedIndex].text;

	if (currentCode == '')
	{
		alert("You must choose a value");
		return false;
	}

	return addCodeDescr(currentCode, currentDescr, multiListBox);
}

function add2Codes(selectBox1, selectBox2, multiListBox)
{

	var currentCode1 = jQuery(selectBox1).val();
	var currentDescr1 = jQuery("#"+selectBox1.id+"_title").val();

	var currentCode2 = jQuery(selectBox2).val();
	var currentDescr2 = jQuery("#"+selectBox2.id+"_title").val();

	if (currentCode1 == '')
	{
		alert("You must choose a value in the first dropdown");
		return false;
	}

	var combinedCode = currentCode1 + ' ' + currentCode2;
	var combinedDescr
    if(currentDescr2 != 'Choose')
        combinedDescr = currentDescr1 + ' - ' + currentDescr2;
    else
        combinedDescr = currentDescr1;

	return addCodeDescr(combinedCode, combinedDescr, multiListBox);
}

function addCodeDescr(currentCode, currentDescr, multiListBox, checkListWidth, allow_dups)
{
	if (!checkListWidth && checkListWidth !== false)
	{
		checkListWidth = true;
	}

	var found = false;

    if (allow_dups === undefined)
    {
        allow_dups = false;
    }

    if (!allow_dups)
    {
	    // add to the list box if it doesn't exist
	    for (var i = 0; i < multiListBox.options.length; i++)
	    {
		    if (multiListBox.options[i].value == currentCode)
		    {
			    found = true;
			    break;
		    }
	    }
    }

	if (!found)
	{
		var tableLength = multiListBox.length
		if (tableLength == 0)
		{
			multiListBox.options[0] = new Option (currentDescr, currentCode);
		}
		else
		{
			var newOption = document.createElement("option");
			newOption.text = currentDescr;
			newOption.value = currentCode;
			var newIndex = 0;

            if (allow_dups)
            {
                //just tag onto end of list without ordering.
                i = multiListBox.length + 1;
            }
            else
            {
			    for (var i = 0; i <= multiListBox.length; i++)
			    {
				    if (i < multiListBox.length && multiListBox.options[i].text.toLowerCase() > newOption.text.toLowerCase())
				    {
					    break;
				    }
			    }
            }

			multiListBox.options.add(newOption, i);
		}
		setChanged(multiListBox.name.replace("[]",""));
	}
	if (checkListWidth)
	{
		checkMultiWidth(multiListBox);
	}
}

function selectAllMultiple(multiListBox)
{
    if (multiListBox)
    {
        //if the controls are disabled, they will not post any values.
        multiListBox.disabled = false;
        for (var i = 0; i < multiListBox.length; i++)
	    {
		    multiListBox.options[i].selected = true;
	    }
	    return true;
    }
}

function selectAllMultiCodes() {
    for(var i=0,l=document.forms[0].length; i<l; i++)
    {
        if(document.forms[0].elements[i].multiple)
        {
            selectAllMultiple(document.forms[0].elements[i]);
        }
    }
}

function deleteSelected(multiListBox)
{
	for (var i = 0; i < multiListBox.length; i++)
	{
		if (multiListBox.options[i].selected)
		{
			multiListBox.options[i] = null;
			i--;
		}
		setChanged(multiListBox.name.replace("[]",""));
	}
	checkMultiWidth(multiListBox)
}

function addInputToListBox(inputField, multiListBox)
{
    if (inputField.value == "")
    {
        return;
    }

	var found = false;

	// add to the list box if it doesn't exist
	for (var i = 0; i < multiListBox.options.length; i++)
	{
		if (multiListBox.options[i].value == inputField.value)
		{
			found = true;
			break;
		}
	}

	if (!found)
	{

		var tableLength = multiListBox.length
		if (tableLength == 0)
		{
			multiListBox.options[0] = new Option (inputField.value, inputField.value);
		}
		else
		{
			multiListBox.options[tableLength] = new Option("", "");
			for (var i = tableLength; i >= 0; i--)
			{
				if (i == 0 || multiListBox.options[i - 1].value < inputField.value)
				{
					multiListBox.options[i].value = inputField.value;
					multiListBox.options[i].text = inputField.value;
					inputField.value = '';
					break;
				}
				multiListBox.options[i].text = multiListBox.options[i - 1].text;
				multiListBox.options[i].value = multiListBox.options[i - 1].value;
			}
		}

	}
}

/**
 * Sorts a SELECT listbox
 *
 * @param string lb: listbox field id.
 */
function sortListBox(lb)
{
    var arrObjects = new Array();
    var arrObjects = new Array();

    for(i=0; i<lb.length; i++)
    {
        arrObjects[i] = new Array(lb.options[i].text.toLowerCase().replace(' ', ''), new Object(lb.options[i]));
    }

    arrObjects.sort(
        function(a,b)
        {
            if(a[0] < b[0]) return -1;
            if(a[0] > b[0]) return 1;

            return 0;

        });
    
    jQuery(lb).children().each(function(index, option)
    {
        jQuery(option).remove();
    });
    
    jQuery.each(arrObjects, function()
    {
        jQuery(lb).append(this[1]);        
    });
}
