function organisationSearch(fields, ctrlData)
{
    var errorFields = [],
        includeFields = ctrlData.fieldlist.split(","),
        name,
        i,
        val;

    for (var id in fields)
    {
        if (jQuery('#' + id ).length)
        {
            if (jQuery('#' + id )[0].tagName.toLowerCase() == 'select')
            {
                val = jQuery('#' + id + ' option').length;
            }
            else
            {
                val = jQuery('#' + id).val();
            }

            if (val == false)
            {
                errorFields.push(fields[id]);
            }
        }
        else
        {
            // remove the field name from includeFields
            name = id.split("_");

            if (!isNaN(parseInt(name[name.length-1])))
            {
                name.splice(name.length-1, 1); // remove suffix from field id
            }

            name = name.join("_");

            for (i = 0; i < includeFields.length; i++)
            {
                if (includeFields[i] == name)
                {
                    includeFields.splice(i, 1);
                    break;
                }
            }
        }
    }

    if (errorFields.length < 1)
    {
        ctrlData.fieldlist = includeFields.join(",");
        OrganisationSelectionCtrl.create(ctrlData);
    }
    else
    {
        var msg = '<p>Please enter at least one character in all the following fields before searching:</p><ul>';

        for (i = 0, j = errorFields.length; i < j; i++)
        {
            msg += '<li>' + errorFields[i] + '</li>';
        }

        msg += '</ul>';

        alert(msg);
    }
}