function CreateNewDashboard()
{
    //TODO - this could be done by updating the view and not having to reload the page, however there would be extra things to consider by using this method
    jQuery.getJSON(scripturl + '?action=httprequest&responsetype=json&type=createnewdashboard', function(data) {

        if(data.success) {

            SendTo(scripturl+'?action=dashboard&dash_id='+data.dash_id);
        }
    });
}