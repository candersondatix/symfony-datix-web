<?php

    global $user, $unapproved, $scripturl, $num_unapproved, $yySetLocation;
    global $dtxtitle, $dtxdebug, $txt;

// ==========================================================================//
// Initialise conditions                                                     //
// ==========================================================================//

$record_url_part = 'fromsearch=1';

if ($_SESSION['SAB']['DRILL_QUERY'] instanceof \src\framework\query\Query &&
    isset($_REQUEST['from_report']) && $_REQUEST['from_report'] == '1' || isset($_REQUEST['fromcrosstab']) && $_REQUEST['fromcrosstab'] == '1')
{
    $DrillIntoReport = true;
    $record_url_part = 'from_report=1';
}

    $_SESSION["SAB"]["EXTRAWHERE"] = '';

    $AdminUser = $_SESSION["AdminUser"];

    $ListType = $_GET["listtype"];
    $OverdueType = $_GET["overduetype"];

    $SabPerms = $_SESSION["Globals"]["SAB_PERMS"];

    $ListTitle = "$txt[SABSNamesTitle] Listing";

// ==========================================================================//
// Build where clause according to access level, permission, timescales      //
// and criteria specific to the type of listing                              //
// ==========================================================================//

    $FromTable = $_REQUEST["table"];

    $orderby = $_SESSION["LIST"][$module]["ORDERBY"];
    $order = $_SESSION["LIST"][$module]["ORDER"];

    if (!$orderby)
        $orderby = "sab_dstart_due";

    if($_POST["listnum"])
        $listnum = Sanitize::SanitizeInt($_POST["listnum"]);

    if($_POST["overduetype"])
        $overduetype = Sanitize::SanitizeString($_POST["overduetype"]);
    else
        $overduetype = Sanitize::SanitizeString($_GET["overduetype"]);

    $SabsTable = "sabs_main";
    $ActionType = "sabs";

    // Store table(s) used for the query in session var, mainly used when reporting
    $_SESSION["SAB"]["TABLES"] = $SabsTable;

    $sqla = "SELECT recordid, sab_handler,
            updateid, sab_title, sab_reference, sab_dopened, sab_action_type, sab_descr, sab_dstart_due, sab_dend_due
            FROM $SabsTable";

    // Need to check if the Where clause is set otherwise a null entry will be added to the array and it will cause
    // the where clause to fail
    if (isset($ModuleDefs['SAB']['HARD_CODED_LISTINGS'][$ListType]['Where']))
    {
        $SabsWhere[] = $ModuleDefs['SAB']['HARD_CODED_LISTINGS'][$ListType]['Where'];
        $ListTitle = $ModuleDefs['SAB']['HARD_CODED_LISTINGS'][$ListType]['Title'];
    }

    // Overdue Safety alerts - those in holding area for more than DIF_OVERDUE_DAYS
    if ($_GET['overdue'])
    {
        if (!$OverdueType) // all overdue records.
        {
            $SabsWhere[] = $ModuleDefs['SAB']['HARD_CODED_LISTINGS']['alloverdue']['OverdueWhere'];
            $ListType = 'alloverdue';
        }
        else
        {
            $SabsWhere[] = $ModuleDefs['SAB']['HARD_CODED_LISTINGS'][$OverdueType]['OverdueWhere'];
            $TitleComments = $ModuleDefs['SAB']['HARD_CODED_LISTINGS'][$OverdueType]['Title'];
            $ListType = $OverdueType;
        }
    }

if ($_REQUEST['drillwhere'] != '')
{
    $SabsWhere[] = stripslashes(Sanitize::SanitizeRaw($_REQUEST['drillwhere']));
}
else if ($DrillIntoReport)
{
    $query = $_SESSION['SAB']['DRILL_QUERY'];

    $writer = new \src\framework\query\SqlWriter();

    list($statement, $parameters) = $writer->writeStatement($query);

    // Replace all occurrences of ? with values on the where clause
    $needle = '?';

    foreach ($parameters as $parameter)
    {
        $parameterPos = UnicodeString::strpos($statement, $needle);

        if ($parameterPos !== false)
        {
            $statement = UnicodeString::substr_replace($statement, DatixDBQuery::quote($parameter), $parameterPos, UnicodeString::strlen($needle));
        }
    }

    $SabsWhere[] = $ModuleDefs['SAB']['TABLE'].'.recordid IN ('.$statement.')';

    $_SESSION['SAB']['current_drillwhere'] = $SabsWhere[0];
}
else
{
    if (is_array($ExtraWhere) && !empty($ExtraWhere))
    {
        $SabsWhere[] =  implode(' AND ', $ExtraWhere);
        $_SESSION['SAB']['EXTRAWHERE'] = implode(' AND ', $ExtraWhere);
    }

    if ($_GET['fromcrosstab'] == '1')
    {
        $SabsWhere[] =  "(" . $_SESSION['CurrentReport']->ConstructCellWhereClause($_GET) . ")";
    }
    elseif ($ListType == 'search' && $_SESSION[$module]['WHERE'])
    {
        $SabsWhere[] =  '(' . $_SESSION['SAB']['WHERE'] . ')';
    }
}


    $ListWhereClause = MakeSecurityWhereClause($SabsWhere, 'SAB', $_SESSION["initials"]);

    if ($TitleComments)
    {
        $ListTitle .= " - $TitleComments";
    }

// ==========================================================================//
// Image file to display at the top left of the form                         //
// ==========================================================================//

$img = "Images/sabstitle.gif";

// ==========================================================================//
// Build the URL to be used when clicking on a record and column headers     //
// ==========================================================================//

// linking from a graphic report
if($_REQUEST["rows"] != '')
{
    $timescales_url = "$scripturl?". htmlspecialchars($_SERVER[QUERY_STRING]);
}
else
{
    $timescales_url = "$scripturl?action=list&amp;module=SAB&amp;listtype=$ListType&amp;overduetype=$overduetype";
}
if($_GET['fromcrosstab'] == '1')
{
    $timescales_url .= '&amp;fromcrosstab=1&amp;row='.$_GET['row'].'&amp;col='.$_GET['col'];
}

$record_url = "$scripturl?action=$ActionType&". $record_url_part;

// ==========================================================================//
// Columns to be displayed                                                   //
// ==========================================================================//

require_once 'Source/libs/ListingClass.php';
$list_columns = Listings_ModuleListingDesign::GetListingColumns('SAB');

//==========================================================================//
// Prepare SQL Statement to be executed by BrowseList.php                   //
//==========================================================================//

if (is_array($list_columns))
{
         foreach ($list_columns as $col_name => $col_info)
         {
             if(is_array($list_columns_extra[$col_name])){
                 if(!array_key_exists("condition", $list_columns_extra[$col_name]) || $list_columns_extra[$col_name]["condition"]){
                     $selectfields[$col_name] = $col_name;
                 }
                 else{
                     unset($list_columns[$col_name]);
                 }
                 if(array_key_exists("prefix", $list_columns_extra[$col_name]))
                     $col_info_extra['prefix'] = $list_columns_extra[$col_name]["prefix"];
                 if(array_key_exists("dataalign", $list_columns_extra[$col_name]))
                     $col_info_extra['dataalign'] = $list_columns_extra[$col_name]["dataalign"];
             }
             else
                 $selectfields[$col_name] = $col_name;
         }
}


    $list_request = GetSQLResultsTimeScale($selectfields, $SabsTable,
            $order, $orderby, $ListWhereClause, $listnum, $listdisplay, $listnum2, $listnumall, $sqla, null, $module);

?>