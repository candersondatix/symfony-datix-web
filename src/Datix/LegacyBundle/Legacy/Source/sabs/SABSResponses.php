<?php

function ShowResponseForm($rsp = "", $FormAction = "new")
{
	global $scripturl, $dtxtitle;

    $sql = "SELECT cod_code, cod_descr
            FROM code_types
            WHERE cod_type = 'SABRSPTYPE'
            ORDER BY cod_descr
		";
    $result = db_query($sql);
    $LinkTypes = array();
    while ($row = db_fetch_array($result))
		$LinkTypes[$row["cod_code"]] = $row["cod_descr"];

	template_header();

	echo '<form enctype="multipart/form-data" method="post" name="frmSabsSAB1" action="' . $scripturl . '?action=saveresponselinktomain">';

?>
<tr>
<td class="windowbg" width="100%">
<br />
<big><b><?= $rsp["link_exists"] ? $LinkTypes[$rsp["link_type"]]
			: "Response" ?></b></big>
<br />
<br />
<?php
	if ($error)
		echo '<font color="red"><b>' . $error["message"] . '</b></font><br /><br />';
?>
</td>
</tr>
<input type="hidden" name="sab_id" value="<?= Sanitize::SanitizeInt($_GET["sab_id"]) ?>">
<input type="hidden" name="rsp_id" value="<?= Sanitize::SanitizeInt($_POST["rsp_id"]) ?>">
<input type="hidden" name="updateid" value="<?= Sanitize::SanitizeString($rsp["updateid"]) ?>">
<input type="hidden" name="main_recordid" value="<?= Sanitize::SanitizeInt($rsp["main_recordid"]) ?>">
<input type="hidden" name="formtype" value="<?= Sanitize::SanitizeString($_GET["formtype"]) ?>">
<input type="hidden" name="holding_form" value="<?= Sanitize::SanitizeString($_GET["holding_form"]) ?>">
<input type="hidden" name="form_action" value="">
<tr>
	<td>
	<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
<?php

	$CTable = new FormTable();
	$FieldObj = new FormField();

	$CTable->MakeTitleRow('<b>Response Options</b>');

    $field = Forms_SelectFieldFactory::createSelectField('rsp_type', 'SAB', $rsp["rsp_type"], '');
    $field->setCustomCodes($LinkTypes);
	$CTable->MakeRow($error["rsp_type"] . '<b>Response type</b>', $field);

    $CTable->MakeRow($error["rsp_comments"] . '<b>Comments</b>',
    //$FieldObj->MakeInputField('rsp_comments', 50, 50, $rsp["rsp_comments"], ""));
    $FieldObj->MakeTextAreaField('rsp_comments', 7, 70, 254, $rsp["rsp_comments"]));

	$CTable->MakeTable();

	echo $CTable->GetFormTable();

?>
       <tr>
		<td width="100%" class="titlebg" align="center">
		<br />

<?php
    if ($FormAction == "new")
    {
		echo '<input type="submit" value="Insert Response">';
    }
        echo '  <input type="button" value="'._tk('btn_cancel').'" onClick="SendTo(\'app.php?action=sabs&recordid='.Sanitize::SanitizeInt($_GET["sab_id"]).'&panel=responses\');">';

?>

<?php
	footer();
	obExit();
}

function SaveResponseLinkToMain()
{
	global $scripturl, $yySetLocation;

	LoggedIn();


	$rsp_id = Sanitize::SanitizeInt($_POST["rsp_id"]);
	$sab_id = Sanitize::SanitizeInt($_POST["sab_id"]);

	if ($sab_id == "")
		fatal_error("No ID");

	$ErrorMark = '<font size="3" color="red"><b>*</b></font>';

	if ($_POST["rsp_type"] == "")
	{
		$error["message"] = "You must enter a response type.";
		$error["rsp_type"] = $ErrorMark;
	}

	if ($error)
	{
		ShowResponseForm();
		obExit();
	}

	$rsp = QuotePostArray(Sanitize::SanitizeRawArray($_POST));

    if ($rsp_id == "")
	{
        $rsp_id = GetNextRecordID("sab_responses", true);
	    $NewResponse = true;
    }


    $con_login_id = $_SESSION["contact_login_id"];


	$sql = "UPDATE sab_responses
		SET
		rsp_type = '$rsp[rsp_type]',
		rsp_comments = '$rsp[rsp_comments]',
        rsp_datetime = '" . date('d-M-Y H:i:s') . "',
        con_id = $con_login_id,
        sab_id = $sab_id
		WHERE recordid = $rsp_id";

	if (!db_query($sql))
		fatal_error("Could not insert response" . $sql);

	$yySetLocation = "$scripturl?action=sabs&recordid=$sab_id&panel=responses";
	redirectexit();
}

function ShowResponse()
{
	global $scripturl, $rsp, $FormArray;

	LoggedIn();

	$rsp_id = Sanitize::SanitizeInt($_GET["rsp_id"]);

    if ($rsp_id)
    {
        $sql =
            "SELECT
                recordid, con_id, rsp_comments, rsp_type, rsp_datetime
        	FROM
                sab_responses
            where
                sab_responses.recordid = $rsp_id";

        $request = db_query($sql);
        $rsp = db_fetch_array($request);


    }

    ShowResponseForm($rsp, "Print");

	obExit();
}
?>