<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "name" => array(
        "Title" => "Safety Alert name and reference",
        "MenuTitle" => "Details",
        'ReadOnly' => ($FormLevel == 1),
        "Rows" => array(
            "sab_title",
            "recordid",
            "sab_reference",
            "sab_dopened",
            "sab_handler"
        )
    ),
    "description" => array(
        "Title" => "Safety Alert details",
        'ReadOnly' => ($FormLevel == 1),
        "Rows" => array(
            "sab_source",
            "sab_type",
            "sab_subtype",
            "sab_issued_date",
            "sab_descr",
            "sab_action_type",
            "sab_action_descr",
            "sab_dstart_due",
            "sab_dend_due",
            "sab_dstart",
            "sab_dend",
            "sab_cost",
            "sab_cost_notes",
            "sab_read_by_due",
            "sab_dclosed"
        )
    ),
    "location" => array(
        "Title" => "Location",
        'ReadOnly' => ($FormLevel == 1),
        "Rows" => array(
            "sab_organisation",
            "sab_unit",
            "sab_clingroup",
            "sab_directorate",
            "sab_specialty",
            "sab_loctype",
            "sab_locactual"
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $sab['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('SAB', $sab['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        "Include" => "Source/sabs/MainSABS.php",
        "Function" => "ListDocuments",
        "NoFieldAdditions" => true,
        "NotModes" => array("Search", "New"),
        "Rows" => array()
    ),
    "response" => array("Title" => "Response",
        "Include" => "Source/sabs/MainSABS.php",
        "Function" => "SectionCurrentResponse",
        "Condition" => (($Level == 1 || $FormLevel == 1) && ($FormType == "Design" || $_SESSION["contact_login_id"] && $SABPerms != "SAB2" && $sab["rsp_id"] && $sab["link_type"] == "S")),
        "NoFieldAdditions" => true,
        "NotModes" => array("Search", "New"),
        "Rows" => array()
    ),
    "contacts_type_S" => array(
        "Title" => "For action by",
        "Include" => "Source/sabs/MainSABS.php",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'Listings' => array(
            'not' => array('module' => 'CON', 'Label' => 'Listing to use for "Not yet sent" section.'),
            'sent' => array('module' => 'CON', 'Label' => 'Listing to use for "Sent but not yet read" section.'),
            'read' => array('module' => 'CON', 'Label' => 'Listing to use for "Read but not yet responded" section.'),
            'resp' => array('module' => 'CON', 'Label' => 'Listing to use for "Read and Responded" section.'),
        ),
        'LinkedForms' => array('S' => array('module' => 'CON', 'level' => 2)),
        "Function" => "ListForActionBy",
        "Rows" => array()
    ),
    "contacts_type_I" => array(
        "Title" => "For information only",
        "Include" => "Source/sabs/MainSABS.php",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'Listings' => array(
            'not' => array('module' => 'CON', 'Label' => 'Listing to use for "Not yet sent" section.'),
            'sent' => array('module' => 'CON', 'Label' => 'Listing to use for "Sent Alert" section.'),
        ),
        'LinkedForms' => array('I' => array('module' => 'CON', 'level' => 2)),
        "Function" => "ListForInfoOnly",
        "Rows" => array()
    ),
    "contacts_type_N" => array(
        "Title" => "Contacts",
        "Include" => "Source/sabs/MainSABS.php",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'Listings' => array('contacts_type_N' => array('module' => 'CON')),
        'LinkedForms' => array('N' => array('module' => 'CON', 'level' => 2)),
        "Function" => "ListContacts",
        "Rows" => array()
    ),
    "linked_records" => array(
        "Title" => "Linked records",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        "Special" => "LinkedRecords",
        "Rows" => array()
    ),
    "notepad" =>  array(
        "Title" => "Notepad",
        "NewPanel" => true,
        "NoFieldAdditions" => false,
        'ReadOnly' => ($FormLevel == 1),
        "NotModes" => array("New", "Search"),
        "Rows" => array (
            "notes"
        )
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NewPanel' => true,
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "tprop" => array(
        "Title" => 'Equipment',
        "NoFieldAdditions" => true,
        "Special" => "LinkedEquipment",
        'LinkedForms' => array('linked_assets' => array('module' => 'AST', 'level' => 2)),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "sab_history" => array(
        "Title" => "History",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        "Function" => "ListHistory",
        "Condition" => ($FormType != "Print")
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT', 'level' => 2)),
        "Special" => "LinkedActions",
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "sabs_email_preview" => array(
        "Title" => "E-mail preview",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search", "Print"),
        "Function" => "EmailPreviewSection",
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController'
            )
        ),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    )
);

require_once 'Source/libs/ModuleLinks.php';

foreach (getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array(
        'module' => $link_mod,
        'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']
    );
}

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}