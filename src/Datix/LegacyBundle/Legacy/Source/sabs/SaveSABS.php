<?php

function ShowSaveSABS($sab = "", $Warning = "")
{
	global $dtxtitle, $person_action, $scripturl, $yySetLocation,
		$dtxtitle, $txt, $ModuleDefs;

    $sab_number = $sab["recordid"];

	if ($sab_number == "")
	{
		$yySetLocation = "$scripturl";
		redirectexit();
	}

	$dtxtitle = "$txt[SABSNameTitle] Number $sab_number";

    getPageTitleHTML(array(
         'title' => $dtxtitle,
         'module' => 'SAB'
         ));

    GetSideMenuHTML(array('module' => 'SAB'));

    template_header_nopadding();

    echo '<div class="section_title_row"><b>'.$txt["SABSNameTitle"] . " " . $sab_number.'</b></div>';

    echo '<div class="windowbg2 padded_wrapper">';

    if ($Warning)
    {
        echo $Warning;
    }

    echo 'The '.$txt["SABSName"].' has been saved. The ID number is '. $sab_number;

    echo '</div>';

    $recordurl = $scripturl.'?module='.$module.'&action='.$ModuleDefs[$module]['ACTION'].'&recordid='.$recordid;

    echo '
    <div class="button_wrapper">';
?>
    <input TYPE="button" VALUE="Back to <?= $txt["SABSName"] ?>" name="btnSABS" onClick="SendTo('<?= "$scripturl?action=sabs&recordid=$sab_number" ?>');">
<?php

        echo '
                    </div>';
        
    if (bYN(GetParm("WEB_TRIGGERS","N")))
    {
        require_once 'Source/libs/Triggers.php';
     	ExecuteTriggers($sab_number, $sab, $ModuleDefs['SAB']["MOD_ID"]);
    }

	footer();
	obExit();
}

function ContactBatchUnlink($sab)
{
    $sab_id = $sab['recordid'];
    $SABPerms = $_SESSION['Globals']['SAB_PERMS'];
    $LinkOwnerId = $_SESSION['contact_login_id'];

    $sql = "SELECT sab_link_contacts.con_id AS con_id
            FROM sab_link_contacts
            WHERE sab_link_contacts.sab_id = $sab_id";

    $request = db_query($sql);

    while ($row = db_fetch_array($request))
    {
        $con_id = $row['con_id'];

        if ($_POST['checkbox_include_' . $con_id] == 'on')
        {

            $sql = "DELETE FROM sab_link_contacts
		            WHERE con_id = $con_id
		            AND sab_id = $sab_id";

            if ($SABPerms != 'SAB2')
            {
                $sql .= " AND link_owner_id = $LinkOwnerId";
            }
            else
            {
                $sql .= " AND link_owner_id IS NULL";
            }

            db_query($sql);
            AuditContactUnlink('SAB', $sab_id, $con_id);
        }
    }
}
