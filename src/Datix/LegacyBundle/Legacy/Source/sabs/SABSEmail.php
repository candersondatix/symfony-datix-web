<?php

use src\framework\registry\Registry;

function EmailSABSAlert($sab, $SingleConID = "")
{
    $escapedSAB = QuotePostArray($sab);

    $sab_id = $sab['recordid'];

    $dtxtitle = "DATIX Risk Management - E-mail " . _tk('SABSNamesTitle');
    $WaitTitle = '<big>' . _tk('SABSNameTitle') . ' e-mails are being sent.  Please wait.</big>';
    $WaitSubTitle = 'Please do not click Back or Refresh in your browser or close the browser window.';
    $Progress = template_header($WaitTitle, $WaitSubTitle, true);

    // Increase the execution time limit
    set_time_limit(300);

    // Calculate progress meter
    if ($Progress)
    {
        $sql = "select count(*)
            from sab_link_contacts, contacts_main
            where sab_link_contacts.con_id = contacts_main.recordid
            and sab_link_contacts.sab_id = :sab_id";

        $NumStaff = DatixDBQuery::PDO_fetch($sql, array('sab_id' => $sab_id), PDO::FETCH_COLUMN);

        if ($NumStaff > 0)
        {
            $UpdateBy = 100 / $NumStaff;
        }
        else
        {
            $UpdateBy = 100;
        }
    }

    $NumProcessed['Total'] = 0;
    $NumProcessed['Success'] = 0;
    $NumProcessed['Fail'] = 0;

    $sql = "select sab_link_contacts.recordid AS link_id, con_email, con_dod, contacts_main.recordid AS con_id, link_type,
            sab_title, sab_handler, sab_reference,
            sab_source, sab_type, sab_subtype,sab_issued_date,sab_descr,
            sab_action_type,sab_action_descr,sab_dstart_due,
            sab_dend_due,sab_dstart,sab_dend,
            sab_read_by_due,sab_dopened,sab_dclosed, sab_cost, sab_cost_notes,
            sab_organisation, sab_unit, sab_clingroup, sab_directorate,
            sab_specialty, sab_loctype, sab_locactual
            from sab_link_contacts, contacts_main, sabs_main
            where sab_link_contacts.con_id = contacts_main.recordid
            and sab_link_contacts.sab_id = :sab_id
            and sab_link_contacts.sab_id = sabs_main.recordid";

    $pdoParameters = array('sab_id' => $sab_id);

    if ($SingleConID != "")
    {
        $sql .= ' and sab_link_contacts.con_id = :con_id';
        $pdoParameters['con_id'] = $SingleConID;
    }

    $contacts = DatixDBQuery::PDO_fetch_all($sql, $pdoParameters);

    require_once 'Source/libs/Email.php';

    $blockedEmails = array ();

    $contactsByType['FIO'] = array();
    $contactsByType['NOTIFY'] = array();
    $contactsByType['REMIND'] = array();

    $userFactory = new \src\users\model\UserModelFactory();
    $contactFactory = new \src\contacts\model\ContactModelFactory();
    foreach ($contacts as $row)
    {
        if ($_POST["checkbox_include_" . $row['con_id']] == "on" || $SingleConID != "")
        {
            if ($row["link_type"] == "I")
            {
                $emailableObject = $userFactory->getMapper()->findById($row['con_id']);

                if ($emailableObject == null)
                {
                    $emailableObject = $contactFactory->getMapper()->find($row['con_id']);
                }

                $contactsByType['FIO'][] = $emailableObject;
                $contactLinkID['FIO'][$row['con_id']] = $row['link_id'];
            }
            else
            {
                $sql = 'select recordid from sab_history
                                    where sab_id = :sab_id and con_id = :con_id
                                    and his_type = :his_type';

                $histId = DatixDBQuery::PDO_fetch($sql, array('sab_id' => $sab_id, 'con_id' => $row['con_id'], 'his_type' => 'NOTIFY'), PDO::FETCH_COLUMN);

                $emailableObject = $userFactory->getMapper()->findById($row['con_id']);

                if ($emailableObject == null)
                {
                    $emailableObject = $contactFactory->getMapper()->find($row['con_id']);
                }

                if (!$histId)
                {
                    $contactsByType['NOTIFY'][] = $emailableObject;
                    $contactLinkID['NOTIFY'][$row['con_id']] = $row['link_id'];
                }
                else
                {
                    $contactsByType['REMIND'][] = $emailableObject;
                    $contactLinkID['REMIND'][$row['con_id']] = $row['link_id'];
                }
            }
        }
    }

    if ($_SESSION["Globals"]["SABS_E_MAIL_ADDRESS"] != "")
    {
        $fromUser = new \src\users\model\User();
        $fromUser->con_email = $_SESSION["Globals"]["SABS_E_MAIL_ADDRESS"];
    }
    else
    {
        $userFactory = new \src\users\model\UserModelFactory();
        $fromUser = $userFactory->getMapper()->findById($_SESSION['contact_login_id']);
    }

    //Send notification emails
    list($subject, $body, $isHTML) = getSABSEmailContents('NOTIFY', $sab);

    $emailSenderNotify = \src\email\EmailSenderFactory::createEmailSender('SAB', 'NOTIFY');
    $emailSenderNotify->setTemplate(new \src\email\EmailCustomTemplate($subject, $body, $isHTML));

    foreach ($contactsByType['NOTIFY'] as $recipient)
    {
        $emailSenderNotify->addRecipient($recipient);
    }
    $emailSenderNotify->setFromUser($fromUser);

    // We need to pass the data array to sendEmails function to be able to store the recordid in the history table
    $emailSenderNotify->sendEmails($sab);
    $NumProcessed['Total'] += count($contactsByType['NOTIFY']);
    $NumProcessed['Success'] += count($emailSenderNotify->getSuccessful());
    $NumProcessed['Fail'] += count($contactsByType['NOTIFY']) - count($emailSenderNotify->getSuccessful());

    foreach ($emailSenderNotify->getSuccessful() as $recipientEmailAddress => $recipient)
    {
        AddToSABHistory($contactLinkID['NOTIFY'][$recipient->recordid], $sab_id, $recipient->recordid, $recipientEmailAddress, 'NOTIFY', $subject, $body);
    }

    if ($Progress)
    {
        $Progress->SetProgressBar(($NumProcessed['Total'] / $NumStaff) * 100);
    }

    //Send reminder emails
    list($subject, $body, $isHTML) = getSABSEmailContents('REMIND', $sab);

    $emailSenderRemind = \src\email\EmailSenderFactory::createEmailSender('SAB', 'REMIND');
    $emailSenderRemind->setTemplate(new \src\email\EmailCustomTemplate($subject, $body, $isHTML));

    foreach ($contactsByType['REMIND'] as $recipient)
    {
        $emailSenderRemind->addRecipient($recipient);
    }
    $emailSenderRemind->setFromUser($fromUser);

    // We need to pass the data array to sendEmails function to be able to store the recordid in the history table
    $emailSenderRemind->sendEmails($sab);
    $NumProcessed['Total'] += count($contactsByType['REMIND']);
    $NumProcessed['Success'] += count($emailSenderRemind->getSuccessful());
    $NumProcessed['Fail'] += count($contactsByType['REMIND']) - count($emailSenderRemind->getSuccessful());

    foreach ($emailSenderRemind->getSuccessful() as $recipientEmailAddress => $recipient)
    {
        AddToSABHistory($contactLinkID['REMIND'][$recipient->recordid],$sab_id, $recipient->recordid, $recipientEmailAddress, 'REMIND', $subject, $body);
    }

    if ($Progress)
    {
        $Progress->SetProgressBar(($NumProcessed['Total'] / $NumStaff) * 100);
    }

    //Send information emails
    list($subject, $body, $isHTML) = getSABSEmailContents('FIO', $sab);

    $emailSenderFIO = \src\email\EmailSenderFactory::createEmailSender('SAB', 'FIO');
    $emailSenderFIO->setTemplate(new \src\email\EmailCustomTemplate($subject, $body, $isHTML));

    foreach ($contactsByType['FIO'] as $recipient)
    {
        $emailSenderFIO->addRecipient($recipient);
    }
    $emailSenderFIO->setFromUser($fromUser);

    // We need to pass the data array to sendEmails function to be able to store the recordid in the history table
    $emailSenderFIO->sendEmails($sab);
    $NumProcessed['Total'] += count($contactsByType['FIO']);
    $NumProcessed['Success'] += count($emailSenderFIO->getSuccessful());
    $NumProcessed['Fail'] += count($contactsByType['FIO']) - count($emailSenderFIO->getSuccessful());

    foreach ($emailSenderFIO->getSuccessful() as $recipientEmailAddress => $recipient)
    {
        AddToSABHistory($contactLinkID['FIO'][$recipient->recordid], $sab_id, $recipient->recordid, $recipientEmailAddress, 'FIO', $subject, $body);
    }

    if ($Progress)
    {
        $Progress->SetProgressBar(($NumProcessed['Total'] / $NumStaff) * 100);
    }

    foreach (array_merge($emailSenderFIO->getFailedDomainValidation(), $emailSenderNotify->getFailedDomainValidation(), $emailSenderRemind->getFailedDomainValidation()) as $failedEmailAddress => $recipient)
    {
        $blockedEmails[] = $failedEmailAddress;
    }
    if (!empty($blockedEmails))
    {
        $text = sprintf(_tk('emails_not_set_domain_not_permitted'),join(', ', $blockedEmails));
            
        echo '
        <script>
            jQuery(document).ready(function() {
                alert ("'.$text.'");
            });
        </script>';
        ob_flush();
        flush();
    }
    
    if ($Progress)
    {
        echo '<script>
document.getElementById("waiting").style.display="none";
</script>';
        ob_flush();
        flush();
    }

    return $NumProcessed;
}

function getSABSEmailContents($type, $sab)
{
    //global used in email templates - do not delete
    global $scripturl;

    $data = $sab; //to allow users to use $data for all email templates.

    // Always include this file first.  The User file can override
    // any settings in it.
    include 'Source/SABS/SABSEmailText.php';

    // Need to include this for function getUserEmailTextFile()
    require_once 'Source/libs/Email.php';

    if ($EmailTextFile = getUserEmailTextFile('SAB'))
    {
        include($EmailTextFile);
    }

    $subject = '';
    $body = '';

    switch($type)
    {
        case 'NOTIFY':
            $subject = $sab["notify_eml_subject"] ?: $EmailText["NOTIFY"]["Subject"];
            $body = $sab["notify_eml_body"] ?: $EmailText["NOTIFY"]["Body"];
            break;
        case 'REMIND':
            $subject = $sab["remind_eml_subject"] ?: $EmailText["REMIND"]["Subject"];
            $body = $sab["remind_eml_body"] ?: $EmailText["REMIND"]["Body"];
            break;
        case 'FIO':
            $subject = $sab["fio_eml_subject"] ?: $EmailText["FIO"]["Subject"];
            $body = $sab["fio_eml_body"] ?: $EmailText["FIO"]["Body"];
            break;
    }

    return array($subject, $body, ($EmailText[$type]['HTML'] || $EmailText[$type]['html']));
}

function AddToSABHistory($link_id, $sab_id, $con_id, $con_email, $his_type, $his_subject, $his_body)
{
    $result = DatixDBQuery::PDO_build_and_insert('sab_history',
        array(
            'link_id' => $link_id,
            'sab_id' => $sab_id,
            'con_id' => $con_id,
            'his_email' => $con_email,
            'his_datetime' => date('Y-m-d H:i:s'),
            'his_type' => $his_type,
            'his_subject' => $his_subject,
            'his_body' => $his_body,
        )
    );

    if (!$result)
    {
        fatal_error("Cannot insert history record");
    }
}

function ShowEmailedSABS($sab = "", $Warning = "", $EmailsSent = "")
{
	global $dtxtitle, $person_action, $scripturl, $yySetLocation,
		$dtxtitle;

    $sab_reference = Sanitize::SanitizeString($sab["sab_reference"]);
    $sab_number = Sanitize::SanitizeInt($sab["recordid"]);

	if ($sab_number == "")
	{
		$yySetLocation = "$scripturl";
		redirectexit();
	}

    $EmailsSentText = 'E-mails sent: ' . $EmailsSent['Success'];

    if ($EmailsSent['Fail'] > 0)
    {
        $EmailsSentText = $EmailsSentText . "<br />E-mails not sent: " . $EmailsSent['Fail'];

    }

	$dtxtitle = _tk('SABSNameTitle') . ' Reference $sab_reference';
	template_header();

?>
<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <tr>
    	<td class="titlebg">
        <b><?php echo _tk('SABSNameTitle'); ?> Reference <?= $sab_reference ?></b>
    	</td>
    </tr>
    <tr>
    	<td class="windowbg2"><b>
<?php
	if ($Warning)
		echo $Warning . "<br /><br />";
?>
            <?= $EmailsSentText ?>
            </b>
    	</td>
    </tr>
    <form method="post" name="frmSaveSABS" action="<?= "$scripturl?action=list&amp;module=SAB&amp;table=main"?>">
        <input type="hidden" name="rbWhat" value="">
        <input type="hidden" name="recordid" value="<?= $sab_number ?>">
    <tr>
    	<td class="titlebg" align = "center">
        <input TYPE="button" VALUE="Back to <?php echo _tk('SABSName'); ?>" name="btnSABS" onClick="SendTo('<?= "$scripturl?action=sabs&recordid=$sab_number" ?>');">
        <input TYPE="button" VALUE="List <?php echo _tk('SABSNames'); ?>" name="btnListSABS" onClick="SendTo('<?= "$scripturl?action=list&amp;module=SAB&amp;table=main" ?>');">
        </td>
    </tr>
    </form>
</table>
<?php
	footer();
	obExit();
}

function EmailDetailsSection($email, $FormAction) {

	$CTable = new FormTable();
	$FieldObj = new FormField();
	$CTable->MakeTitleRow('<b>E-mail details</b>');

	if ($error)
		$CTable->Contents .= '<font color="red"><b>' . $error["message"] . '</b></font><br /><br />';

	$CTable->MakeRow("$error[eml_subject]<b>Subject</b>", $FieldObj->MakeInputField('eml_subject', 70, 70, $email["eml_subject"], ""));
    $CTable->MakeRow("$error[eml_body]<b>Body</b>", $FieldObj->MakeTextAreaField('eml_body', 7, 70, 254, $email["eml_body"]));

	$CTable->MakeTable();

    echo '
        <tr>
            <td>' . $CTable->GetFormTable() . '</td>
        </tr>
    ';
}


function SABSEmailPreviewSection($sab, $FormType)
{
    global $scripturl, $MinifierDisabled;

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    if($FormType == 'Locked')
    {
        $FormType = 'Edit'; //This section cannot be locked.
    }

    $SabID = $sab["recordid"];

    include('Source/SABS/SABSEmailText.php');

    // Need to include this for function getUserEmailTextFile()
    require_once 'Source/libs/Email.php';

    if ($EmailTextFile = getUserEmailTextFile('SAB'))
    {
        include($EmailTextFile);
    }

	$CTable = new FormTable($FormType, 'SAB');
	$FieldObj = new FormField($FormType);

	if ($error)
    {
		$CTable->Contents .= '<font color="red"><b>' . $error["message"] . '</b></font><br /><br />';
    }

    //The next section of code seems very confused: TODO: Refactor this.
    if(GetParm('EMT_SAB_NOT'))
    {
        $sql = "SELECT recordid FROM email_templates WHERE recordid='".GetParm('EMT_SAB_NOT')."'";
        $row_not = db_fetch_array(db_query($sql));
    }
    if(!$row_not["recordid"])
    {
        $sql = "SELECT recordid FROM email_templates WHERE emt_module='INC' AND emt_type='EMT_SAB_NOT'";
        $row_not = db_fetch_array(db_query($sql));
    }

    if(GetParm('EMT_SAB_REM'))
    {
        $sql = "SELECT recordid FROM email_templates WHERE recordid='".GetParm('EMT_SAB_REM')."'";
        $row_rem = db_fetch_array(db_query($sql));
    }
    if(!$row_rem["recordid"])
    {
        $sql = "SELECT recordid FROM email_templates WHERE emt_module='INC' AND emt_type='EMT_SAB_REM'";
        $row_rem = db_fetch_array(db_query($sql));
    }

    if(bYN(GetParm('EMAIL_TEMPLATES','N')) && $row_not["recordid"])
    {
        require_once 'Source/libs/EmailTemplateClass.php';

        $INCFeedbackTemplate = new NewEmailTemplate($row_not["recordid"]);

        $INCFeedbackTemplate->ConstructEmailToSend($SabID);

        $EmailText["Notify"]["Subject"] = $INCFeedbackTemplate->EmailSubject;
        $EmailText["Notify"]["Body"] = $INCFeedbackTemplate->EmailBody;

        $CTable->Contents .= '<script language="javascript" src="email' . $addMinExtension . '.js"></script>';
    }

    if(bYN(GetParm('EMAIL_TEMPLATES','N')) && $row_rem["recordid"])
    {
        require_once 'Source/libs/EmailTemplateClass.php';

        $INCFeedbackTemplate = new NewEmailTemplate($row_rem["recordid"]);

        $INCFeedbackTemplate->ConstructEmailToSend($SabID);

        $EmailText["Reminder"]["Subject"] = $INCFeedbackTemplate->EmailSubject;
        $EmailText["Reminder"]["Body"] = $INCFeedbackTemplate->EmailBody;

    }

    if(bYN(GetParm('EMAIL_TEMPLATES','N')))
    {
        $TemplateList = array();
        $CTable->Contents .= '<script language="javascript" src="email' . $addMinExtension . '.js"></script>';

        $sql = "SELECT recordid, emt_name FROM email_templates WHERE emt_module='SAB' AND emt_type='EMT_SAB_NOT'";
        $result = db_query($sql);
        while($templaterow = db_fetch_array($result))
        {
            $TemplateList[$templaterow['recordid']] = $templaterow['emt_name'];
        }

        if(!empty($TemplateList))
        {
            $field = Forms_SelectFieldFactory::createSelectField('not_template', 'SAB', $row["recordid"], $FormType);
            $field->setCustomCodes($TemplateList);
            $field->setOnChangeExtra('changePreviewedEmail(\'not_template\', \'SAB\', '.$SabID.', \'notify_eml_body\', \'notify_eml_subject\')');
            $field->setSuppressCodeDisplay();
            $CTable->MakeRow("<b>Notification Email Template</b>", $field);
        }
    }

    $CTable->MakeRow("$error[eml_subject]<b>Action notification subject</b>", $FieldObj->MakeInputField('notify_eml_subject', 70, 254, $EmailText['NOTIFY']['Subject'], "", false));
    $CTable->MakeRow("$error[eml_body]<b>Action notification body</b>", $FieldObj->MakeTextAreaField('notify_eml_body', 20, 70, 0, $EmailText['NOTIFY']['Body'],true, false));

    if(bYN(GetParm('EMAIL_TEMPLATES','N')))
    {
        $TemplateList = array();
        $sql = "SELECT recordid, emt_name FROM email_templates WHERE emt_module='SAB' AND emt_type='EMT_SAB_REM'";
        $result = db_query($sql);
        while($templaterow = db_fetch_array($result))
        {
            $TemplateList[$templaterow['recordid']] = $templaterow['emt_name'];
        }

        if(!empty($TemplateList))
        {
            $field = Forms_SelectFieldFactory::createSelectField('rem_template', 'SAB', $row["recordid"], $FormType);
            $field->setCustomCodes($TemplateList);
            $field->setOnChangeExtra('changePreviewedEmail(\'rem_template\', \'SAB\', '.$SabID.', \'remind_eml_body\', \'remind_eml_subject\')');
            $field->setSuppressCodeDisplay();
            $CTable->MakeRow("<b>Reminder Email Template</b>", $field);
        }
    }

    $CTable->MakeRow("$error[eml_subject]<b>Action reminder subject</b>", $FieldObj->MakeInputField('remind_eml_subject', 70, 254, $EmailText['REMIND']['Subject'], "", false));
    $CTable->MakeRow("$error[eml_body]<b>Action reminder body</b>", $FieldObj->MakeTextAreaField('remind_eml_body', 20, 70, 0, $EmailText['REMIND']['Body'], true, false));

    $CTable->MakeRow("$error[eml_subject]<b>For information subject</b>", $FieldObj->MakeInputField('fio_eml_subject', 70, 254, $EmailText['FIO']['Subject'], "", false));
    $CTable->MakeRow("$error[eml_body]<b>For information body</b>", $FieldObj->MakeTextAreaField('fio_eml_body', 20, 70, 0, $EmailText['FIO']['Body'], true, false));


    $Contents = $CTable->GetFormTable();

    return $Contents;
}
?>
