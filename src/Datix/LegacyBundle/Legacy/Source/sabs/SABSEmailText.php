<?php
if(GetParm("FMT_DATE_WEB") == "US")
    $data_fmt= 'm/d/Y';
else
    $data_fmt = 'd/m/Y';


// register link added if DIF_2_REGISTER set to Y
$EmailText_register_bottom_link = "
If you have not already registered, please do so at the following link:
$scripturl?action=register
";    
    
    
    
$EmailText["NOTIFY"]["Subject"] = "New $sab[sab_source] $txt[SABSNameTitle] $sab[sab_reference]";

$EmailText["NOTIFY"]["Body"] = "A $txt[SABSName] has been issued by $sab[sab_source].  The details are:

Title: $sab[sab_title]
Reference: $sab[sab_reference]
Issued by: $sab[sab_source]
Date issued: " . ($sab["sab_issued_date"] ? date($data_fmt, strtotime($sab["sab_issued_date"])) : "" ) . "

Description:

$sab[sab_descr]

Please go to $scripturl?action=sabs&recordid=$sab[recordid] to view and respond.

";

if (bYN(GetParm("DIF_2_REGISTER","N")))
{
	$EmailText["NOTIFY"]["Body"] .= $EmailText_register_bottom_link;
}



$EmailText["REMIND"]["Subject"] = "$sab[sab_source] $txt[SABSNameTitle] $sab[sab_reference]: Reminder";

$EmailText["REMIND"]["Body"] = "This is a reminder of the $txt[SABSName] that was issued by $sab[sab_source] on $sab[sab_issued_date].  The details are:

Title: $sab[sab_title]
Reference: $sab[sab_reference]
Issued by: $sab[sab_source]
Date issued: " . ($sab["sab_issued_date"] ? date($data_fmt, strtotime($sab["sab_issued_date"])) : "" ) . "

Description:

$sab[sab_descr]

Please go to $scripturl?action=sabs&recordid=$sab[recordid] to view and respond.

";

if (bYN(GetParm("DIF_2_REGISTER","N")))
{
	$EmailText["REMIND"]["Body"] .= $EmailText_register_bottom_link;
}


$EmailText["FIO"]["Subject"] = "New $sab[sab_source] $txt[SABSNameTitle] $sab[sab_reference]";

$EmailText["FIO"]["Body"] = "A $txt[SABSName] has been issued by $sab[sab_source].  The details are:

Title: $sab[sab_title]
Reference: $sab[sab_reference]
Issued by: $sab[sab_source]
Date issued: " . ($sab["sab_issued_date"] ? date($data_fmt, strtotime($sab["sab_issued_date"])) : "" ) . "

Description:

$sab[sab_descr]

Please go to $scripturl?action=sabs&recordid=$sab[recordid] to view. No action is required.

";


if (bYN(GetParm("DIF_2_REGISTER","N")))
{
	$EmailText["FIO"]["Body"] .= $EmailText_register_bottom_link;
}



?>
