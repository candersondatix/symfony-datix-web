<?php
$GLOBALS["FormTitle"] = "Safety Alert Response Form";
$GLOBALS["HideFields"] = array (
  'sab_dstart' => true,
  'sab_dend' => true,
  'sab_cost' => true,
  'sab_cost_notes' => true,
  'sab_read_by_due' => true,
  'sab_dclosed' => true,
  'location' => true,
  'contacts_type_S' => true,
  'contacts_type_I' => true,
  'contacts_type_N' => true,
  'linked_records' => true,
  'notepad' => true,
  'equipment' => true,
  'sab_history' => true,
  'sabs_email_preview' => true,
  'action_chains' => true,
);

$GLOBALS["ReadOnlyFields"] = array (
  'name' => true,
  'description' => true,
  'location' => true,
  'orphanedudfs' => true,
  'linked_records' => true,
  'notepad' => true,
);

$GLOBALS['NewPanels'] = array(
    'contacts_type_S' => true,
    'contacts_type_I' => true,
    'contacts_type_N' => true,
    'linked_records' => true,
    'notepad' => true,
    'progress_notes' => true,
    'sab_history' => true,
    'linked_actions' => true,
    'sabs_email_preview' => true,
    'word' => true,
);


?>
