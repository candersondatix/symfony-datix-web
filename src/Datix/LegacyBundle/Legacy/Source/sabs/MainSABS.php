<?php

// Still need this function rather than defining in the form definition file
// because this section needs to be read/write while all the other sections
// are read-only.
function SectionCurrentResponse($sab, $FormType)
{
    $FormType = "edit";

    $Table = new FormTable($FormType, 'SAB');

	$FieldObj = new FormField($FormType);

    if ($_GET["print"])
    {
        $FieldObj->SetFieldMode("Print");
    }

    $ReadonlyFieldObj = new FormField("Print");

    $ReadonlyFieldObj->MakeDateField("link_read_date", $sab["link_read_date"]);
    $Table->MakeRow('<b>Date read</b>', $ReadonlyFieldObj);
	$Table->MakeRow('<b>Response Type</b>', Forms_SelectFieldFactory::createSelectField('link_rsp_type', 'SAB', $sab["link_rsp_type"], $FormType));
    $FieldObj->MakeDateField("link_rsp_date", $sab["link_rsp_date"]);
    $Table->MakeRow('<b>Date of response</b>', $FieldObj);
    $FieldObj->MakeTextAreaField('link_comments', 7, 70, 0, $sab["link_comments"]);
    $Table->MakeRow('<b>Comments</b>', $FieldObj);

	echo $Table->GetFormTable();
}

function ListDocuments($sab, $FormType)
{
	global $scripturl, $txt;

    $doc = $sab["doc"];

	$AdminUser = $_SESSION["AdminUser"];
	$SABPerms = $_SESSION["Globals"]["SAB_PERMS"];

    $LinkPerms = (!$_GET["print"]);
    $print = ($_GET["print"] == 1);
    $SabID = $sab["recordid"];

echo'
<li name="documents_row" id="documents_row">

<table class="gridlines" cellspacing="1" cellpadding="4" width="99%" align="center" border="0">
';
	if (!$doc)
    {
        echo '
<tr>
	<td class="windowbg2" colspan="4">
	<b>No attachments saved.</b>
	</td>
</tr>';
    }
	else
	{
		foreach ($doc[""] as $document)
		{
            if ($document["wlk_web_link"])
            {
            echo'
<tr>

	<td class="windowbg2" valign="left">';
        if (!$print && $FormType != 'ReadOnly')
        {
            echo '<a href="JavaScript:void(0)" onclick="
                javascript:
                var link = \'' . addslashes($document["wlk_web_link"]) . '\';
                if(!link.match(\'http://\') && !link.match(\'https://\'))
                    link = \'http://\' + link;

                window.open(link);
                ">';
        }
        echo htmlspecialchars($document["wlk_descr"]);
        if (!$print && $FormType != 'ReadOnly')
        {
            echo ' <img align="bottom" alt="" src="Images/external.png" border="0"/></a>';
        }

    if ($LinkPerms && $FormType != 'Print' && $FormType != 'ReadOnly' && $SABPerms == 'SAB2')
    {
        echo '</td>';

        echo '<td class="windowbg2" valign="right" border="0">';
        echo '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=linkweblink&amp;module=SAB&amp;link_id='
	    . Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;wlk_id=' . $document["doc_id"]  . '&amp;formtype=' . $FormType . '\');}" > [edit]</a>';
    }

            echo '
	</td>

</tr>';
            }
            else
            {
            echo'
<tr>
	<td class="windowbg2" valign="middle">';
            if (!$print && $FormType != 'ReadOnly')
            {
				echo '<a href="' . $scripturl . "?action=downloaddocument&amp;module=SAB&amp;recordid=$document[recordid]&amp;link_id=$SabID&amp;main_table=1";

                if ($document["main_recordid"] != "")
                {
                    echo '&amp;main_recordid=' . $document["main_recordid"];
                }

                if ($document["recordid"] != "")
                {

                }

                echo '">';
            }

            echo Escape::EscapeEntities($document["doc_notes"]);

                if (!$print && $FormType != 'ReadOnly')
                {
                    echo "</a>";
                }

    if ($LinkPerms && $FormType != 'Print' && $FormType != 'ReadOnly' && $SABPerms == 'SAB2')
    {
        echo '</td>';

        echo '<td class="windowbg2" width="5%" valign="right" border="0">';
        echo '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=editdocument&amp;module=SAB&amp;link_id='
	    . Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;recordid=' . $document["recordid"]  . '&amp;main_table=1\');}"> [edit]</a>';
    }

			echo '
	</td>

</tr>';       }
		}
	}

    if ($LinkPerms && $FormType != 'Print' && $FormType != 'ReadOnly' && $SABPerms == 'SAB2')
		echo '
<tr>
	<td class="titlebg" colspan="4">
	<b>New attachment</b>
	</td>
</tr>
<tr>
	<td class="windowbg2" colspan="4" valign="middle">
	<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=editdocument&amp;module=SAB&amp;link_id='
	. Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;main_table=1\');}"><img alt="" src="Images/adddoc16.png" border="0" /><b> Add a new attachment to this ' . $txt["SABSName"] . '</b></a>
	</td>
</tr>
<tr>
    <td class="windowbg2" colspan="4" valign="middle">
	<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=linkweblink&amp;module=SAB&amp;link_id='
	. Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;formtype=' . $FormType . '\');}"><img alt="" src="Images/weblink16.png" border="0" /><b> Add a new web link to this ' . $txt["SABSName"] . '</b></a>
	</td>
</tr>
';
	echo '</table>
    </li>';

}

function ListHistory($sab, $FormType)
{
	global $scripturl;

    $TypeArray = array("REMIND"=>"Action reminder",
                        "NOTIFY"=>"Action notification",
                        "FAIL"=>"Email sending failed",
                        "FIO"=>"Information notification");

	$AdminUser = $_SESSION["AdminUser"];
	$SABPerms = $_SESSION["Globals"]["SAB_PERMS"];

    $LinkPerms = (!$_GET["print"] && $FormType != 'ReadOnly');
    $print = ($_GET["print"] == 1 || $FormType == 'ReadOnly');
    $SabID = $sab["recordid"];

    $sql = 'SELECT SAB_HISTORY.recordid AS his_id, con_jobtitle,
        con_title, con_forenames, con_surname, his_email, his_datetime, his_type
        FROM  SAB_HISTORY, CONTACTS_MAIN
        WHERE  CONTACTS_MAIN.RECORDID = SAB_HISTORY.CON_ID
	        AND SAB_HISTORY.SAB_ID = :sab_id
        ORDER BY HIS_DATETIME DESC';

    $his[""] = DatixDBQuery::PDO_fetch_all($sql, array('sab_id' => $SabID));

    $numrecords = count($his[""]);
    $listdisplay = GetParm("LISTING_DISPLAY_NUM", 20);

    $start = max(intval($_GET["recordlist"]),0);
    $end = min($start + $listdisplay , $numrecords);

    if($print)
    {
        echo '<table width="100%" cellpadding="0" cellspacing="0">';
    }

    echo'
<tr name="sab_history_row" id="sab_history_row">
<td class="windowbg2">
<table class="titlebg" cellspacing="1" cellpadding="0" width="100%" align="center" border="0">
<tr><td class="titlebg">
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
';
    if ($his)
    {
        if (!$_GET['print'])
        {
        echo '
<tr><td class="windowbg2" colspan="4">
    <b>'.$numrecords._tk('sabs_history_record_found').($start+1).'-'.($end).'</b>
    </td>
</tr>';
            if (($start != 0) || ($end != $numrecords))
            {
                echo '
<tr>';
            if ($start != 0)
                echo '
    <td class="windowbg2" colspan="'.($end!=$numrecords?2:4).'" align="left">
    <b><a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=sabs&recordid='.$SabID.'&recordlist='.($start-$listdisplay).'&panel=sab_history\');}">Previous Page</a></b>
    </td>';
            if ($end != $numrecords)
                echo '
    <td class="windowbg2" colspan="'.($start!=0?2:4).'" align="right">
    <b><a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=sabs&recordid='.$SabID.'&recordlist='.($end).'&panel=sab_history\');}">Next Page</a></b>
    </td>';
             echo '
</tr>';
            }

        }
        else
        {
             $start = 0;
             $end = $numrecords ;
        }
    }

echo '
<tr id="SABhistory" name="SABhistory" '.($_GET["print"]?'style="display: none"':"").'>
    <td class="windowbg">
    <b>Name</b>
    </td>
    <td class="windowbg">
    <b>Email sent to</b>
    </td>
    <td class="windowbg">
    <b>Date/Time</b>
    </td>
    <td class="windowbg">
    <b>Type</b>
    </td>
</tr>
';

    if (!$his)
        echo '
<tr id="SABhistory" name="SABhistory" '.($_GET["print"]?'style="display: none"':"").'">
    <td class="windowbg2" colspan="4">
    <b>No history records.</b>
    </td>
</tr>';
    else
    {
        for ($i = intval($start); $i < $end; $i++)
        {
            $history = $his[""][$i];

            switch (GetParm('STAFF_NAME_DROPDOWN'))
            {
                case 'B':
                    $con_name = $history["con_jobtitle"] . ' - ' . $history["con_title"] . " " . $history["con_forenames"] . " " . $history["con_surname"];
                    break;
                case 'A':
                    $con_name = $history["con_title"] . " " . $history["con_forenames"] . " " . $history["con_surname"] . ' - ' . $history["con_jobtitle"];
                    break;
                default:
                    $con_name = $history["con_title"] . " " . $history["con_forenames"] . " " . $history["con_surname"];
                    break;
            }

            echo'
            <tr id="SABhistory" name="SABhistory">
            <td class="windowbg2" valign="middle">'
            . (!$print ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=sabshistory&amp;sab_id=' . Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;his_id=' . $history["his_id"]  . '&amp;formtype=' . $FormType . '\');}"> ' : '')
            . $con_name
            . (!$print ? '</a>' : '')
            . '</td>
            <td class="windowbg2">'
            . (!$print ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=sabshistory&amp;sab_id='. Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;his_id=' . $history["his_id"]  . '&amp;formtype=' . $FormType . '\');}"> ' : '')
            . $history["his_email"]
            . (!$print ? '</a>' : '')
            . '</td>
            <td class="windowbg2">'
            . (!$print ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=sabshistory&amp;sab_id=' . Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;his_id=' . $history["his_id"]  . '&amp;formtype=' . $FormType . '\');}"> '  : '')
            . $history["his_datetime"]
            . (!$print ? '</a>' : '')
            . '</td>
            <td class="windowbg2">'
            . (!$print ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=sabshistory&amp;sab_id=' . Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;his_id=' . $history["his_id"]  . '&amp;formtype=' . $FormType . '\');}"> ' : '')
            . $TypeArray[$history["his_type"]]
            . (!$print ? '</a>' : '')
            . '</td>
            </tr>';
        }
    }

    if ($his)
    {
        if (($start != 0) || ($end != $numrecords))
            echo '
<tr>';
        if ($start != 0)
            echo '
<td class="windowbg2" colspan="'.($end!=$numrecords?2:4).'" align="left">
<b><a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=sabs&recordid='.$SabID.'&recordlist='.($start-$listdisplay).'&panel=sab_history\');}">Previous Page</a></b>
</td>';
        if ($end != $numrecords)
            echo '
<td class="windowbg2" colspan="'.($start!=0?2:4).'" align="right">
<b><a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=sabs&recordid='.$SabID.'&recordlist='.($end).'&panel=sab_history\');}">Next Page</a></b>
</td>';
        if (($start != '') || ($end != $numrecords))
            echo '
</tr>';

    }
    echo '</table></td></tr></table></td></tr>';

    if($print)
    {
        echo '</table>';
    }
}

function ListContacts($sab, $FormType = "Main")
{
    if($FormType == 'Print')
    {
        ?><table width="100%" cellpadding="0" cellspacing="0"><?php
    }

    ListContactType($sab, $FormType, "N", "Contacts", false, "contact");

    if($FormType == 'Print')
    {
        ?></table><?php
    }
}

function ListForActionBy($sab, $FormType = "Main")
{
    if($FormType == 'Print')
    {
    ?><table width="100%" cellpadding="0" cellspacing="0"><?php
    }

    ListContactType($sab, $FormType, "S", "Recipients: For Action By", true, "recipient");

    if($FormType == 'Print')
    {
    ?></table><?php
    }
}

function ListForInfoOnly($sab, $FormType = "Main")
{
    if($FormType == 'Print')
    {
        ?><table width="100%" cellpadding="0" cellspacing="0"><?php
    }

    ListContactType($sab, $FormType, "I", "Recipients: For Information Only", true, "recipient");

    if($FormType == 'Print')
    {
    ?></table><?php
    }
}

function ListContactType($sab, $FormType = "Main", $ConType, $Header, $EmailCheckBox = false, $ConItemName = "contact")
{
	global $scripturl, $txt, $ListingDesigns, $ModuleDefs;

	$SABPerms = $_SESSION["Globals"]["SAB_PERMS"];
    $LinkOwnerId = $_SESSION["contact_login_id"];

    if ($EmailCheckBox)
    {
        $ColSpan = 8;
    }
    else
    {
        $ColSpan = 7;
    }

    $LinkPerms = (!$_GET["print"] && $FormType != 'ReadOnly');

    $SabID = $sab["recordid"];

    if ($ConType == "I")
    {
        $his_type = "'FIO','NOTIFY'";
    }
    else
    {
        $his_type = "'NOTIFY'";
    }

    if (CanSeeModule('CON'))
    {
        $sql = "
            SELECT
                link_type, " . implode(', ', $ModuleDefs['CON']['FIELD_ARRAY']) . ", link_role, link_status,
                sab_link_contacts.recordid AS rsp_id, sab_link_contacts.recordid AS link_recordid,
                contacts_main.recordid, sab_link_contacts.sab_id, sab_link_contacts.con_id,
                sab_link_contacts.link_comments, sab_link_contacts.link_rsp_type, sab_link_contacts.link_rsp_date,
                link_read_ackn, link_read_date, sab_link_contacts.link_datetime, his_type, his_datetime
            FROM
                contacts_main, sab_link_contacts
                LEFT OUTER JOIN sab_history ON sab_link_contacts.con_id = sab_history.con_id AND
                sab_link_contacts.sab_id = sab_history.sab_id AND sab_history.his_type IN ($his_type)
            WHERE
                sab_link_contacts.sab_id = :sab_id AND
                contacts_main.recordid = sab_link_contacts.con_id AND
                sab_link_contacts.link_type = :link_type";

        if ($SABPerms != "SAB2")
        {
            $sql .= " AND sab_link_contacts.link_owner_id = $LinkOwnerId";
        }
        else
        {
            $sql .= " AND sab_link_contacts.link_owner_id IS NULL";
        }

        $securityWhere = MakeSecurityWhereClause('', 'CON');

        if ($securityWhere != '')
        {
            $sql .= ' AND '.$securityWhere;
        }

        $sql .= " ORDER BY his_datetime ASC";

        $contacts = DatixDBQuery::PDO_fetch_all($sql, array('sab_id' => $SabID, 'link_type' => $ConType));
    }
    else
    {
        $contacts = [];
    }

	foreach ($contacts as $con1)
    {
        if (!isset($con[$con1["link_type"]][$con1["con_id"]]))
        {
		    $con[$con1["link_type"]][$con1["con_id"]] = $con1;
        }
    }

	echo '
<tr name="contacts_type_'.$ConType.'_row" id="contacts_type_'.$ConType.'_row">
<td class="windowbg2">
<table class="titlebg" width="100%" cellspacing="1" cellpadding="0" style="background-color: white;">

';

    if ($con)
    {
        usort($con["$ConType"], 'CompareContacts');

        foreach ($con["$ConType"] as $contact)
        {
            if ($ConType == "N")
            {
                $Contacts[] = $contact;
            }
            elseif ($contact["link_read_date"] && $contact["link_rsp_type"] && $ConType == "S")
            {
                $ContactReadAndResp[] = $contact;
            }
            elseif ($contact["link_read_date"] && !$contact["link_rsp_type"] && $ConType == "S")
            {
                $ContactReadNotResp[] = $contact;
            }
            elseif ($contact["his_type"] == "NOTIFY" && !$contact["link_read_date"] && $ConType == "S")
            {
                $ContactSentNotRead[] = $contact;
            }
            elseif (($contact["his_type"] == "NOTIFY" || $contact["his_type"] == "FIO") && $ConType == "I")
            {
                $ContactSent[] = $contact;
            }
            elseif (($contact["his_type"] != "NOTIFY" && $contact["his_type"] != "FIO") && ($ConType == "S" || $ConType == "I"))
            {
                $ContactNotSent[] = $contact;
            }
        }
    }

    if ($ConType == "S" || $ConType == "I")
    {
        DoContactSection('Not yet sent', $ConType, $SabID , $ContactNotSent, $ColSpan, $EmailCheckBox, $FormType, '', $ListingDesigns['contacts_type_'.$ConType]['not']);
    }

    if ($ConType == "I")
    {
        $IncludeColsList = array('his_datetime' => array('width' => 10, 'title' => 'Date Sent', 'type' => 'date'), 'link_read_date' => array('width' => 10, 'title' => 'Date Read', 'type' => 'date'),'link_rsp_type' => array('width' => 10, 'title' => 'Response Type', 'type' => 'ff_select'), 'link_comments' => array('width' => 20, 'title' => 'Comments', 'type' => 'text'));
        DoContactSection('Sent alert', $ConType, $SabID , $ContactSent, $ColSpan, $EmailCheckBox, $FormType, $IncludeColsList, $ListingDesigns['contacts_type_'.$ConType]['sent']);

    }

    if ($ConType == "S")
    {
        $IncludeColsList = array('his_datetime' => array('width' => 10, 'title' => 'Date Sent', 'type' => 'date'));
        DoContactSection('Sent but not yet read', $ConType, $SabID , $ContactSentNotRead, $ColSpan, $EmailCheckBox, $FormType, $IncludeColsList, $ListingDesigns['contacts_type_'.$ConType]['sent']);
        $IncludeColsList = array('his_datetime' => array('width' => 10, 'title' => 'Date Sent', 'type' => 'date'), 'link_read_date' => array('width' => 10, 'title' => 'Date Read', 'type' => 'date'));
        DoContactSection('Read but not yet responded', $ConType, $SabID , $ContactReadNotResp, $ColSpan, $EmailCheckBox, $FormType, $IncludeColsList, $ListingDesigns['contacts_type_'.$ConType]['read']);
        $IncludeColsList = array('his_datetime' => array('width' => 10, 'title' => 'Date Sent', 'type' => 'date'), 'link_read_date' => array('width' => 10, 'title' => 'Date Read', 'type' => 'date'),'link_rsp_type' => array('width' => 10, 'title' => 'Response Type', 'type' => 'ff_select'), 'link_comments' => array('width' => 20, 'title' => 'Comments', 'type' => 'text'));
        DoContactSection('Read and responded', $ConType, $SabID , $ContactReadAndResp, $ColSpan, $EmailCheckBox, $FormType, $IncludeColsList, $ListingDesigns['contacts_type_'.$ConType]['resp']);
    }

    if ($ConType == "N")
    {
        DoContactSection('Contacts', $ConType, $SabID, $Contacts, $ColSpan, $EmailCheckBox, $FormType, '', $ListingDesigns['contacts_type_'.$ConType]['contacts']);
    }

    if ($EmailCheckBox && !$_GET["print"] && $FormType != 'ReadOnly')
    {
        echo '<tr>
            <table width="100%" cellpadding="1">
                <tr>
                    <td class="windowbg2" align="left">
                        <input type="submit" value="'.$txt['email_selected_recipients'].'" class="send_email_btn" onclick="if(CheckChange()){if(CheckContactSelected()){document.forms[0].rbWhat.value=\'Email\';submitClicked=true;} else {alert(\''. $txt["email_select_a_contact"].'\')}}" name="btnEmailSABS"/>
                    </td>
                    <td class="windowbg2" align="right">
                        <input type="submit" value="'.$txt['unlink_selected_recipients'].'" class="send_email_btn" onclick="if(CheckChange()){if(CheckContactSelected()){
                    if(confirm(\''.$txt['confirm_unlink_contacts_sab'].'\')) {
                        document.forms[0].rbWhat.value=\'Delete\';
                        submitClicked=true;
                    };document.forms[0].rbWhat.value=\'BatchUnlink\';document.forms[0].currentpanel.value=\'contacts_type_'. $ConType . '\'} else {alert(\''. $txt["unlink_select_a_contact"].'\')}}" name="btnBatchUnlink"/>
                    </td>
                </tr>
            </table>
        </tr>';
    }

//<input type="button" value="Check All" onclick="CheckAll(this, \'email_include_' . $ConType . '\')" name="btnCheckAll"/>
    if ($LinkPerms && $FormType != 'Print' && $FormType != 'ReadOnly')
    {
	    echo '
<tr>
<td colspan="' . $ColSpan . '">
<table class="windowbg2" border="0" cellspacing="1" cellpadding="4" width="100%">
<tr>
	<td class="titlebg">
	<b>' . $txt['sab_new_'.$ConItemName.'s'] . '</b>
	</td>
</tr>
<tr>
	<td class="windowbg2" width="100%">
    <a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=sabslinkcontact&amp;sab_id='
    . Sanitize::SanitizeInt($SabID) . '&amp;formtype=' . $FormType . '&amp;linktype=' . $ConType .'\');}"><b>'.$txt['sab_add_'.$ConItemName].'</b></a>
    </td>
</tr>';
    }
if ($FormType!="Print" && $FormType != 'ReadOnly')
    echo '<tr>
    <td class="windowbg2" width="100%">
	<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=contactssearch&amp;from_module=SAB&amp;sab_id='
	. Sanitize::SanitizeInt($SabID) . '&amp;formtype=' . $FormType . '&amp;linktype=' . $ConType . '\');}"><b>'.$txt['sab_search_'.$ConItemName].'</b></a>
	</td>
</tr>';
if ($FormType!="Print" && $FormType != 'ReadOnly')
    echo '<tr>
    <td class="windowbg2" width="100%">
	<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . '?action=sabslinkdistribution&amp;sab_id='
	. Sanitize::SanitizeInt($SabID) . '&amp;formtype=' . $FormType . '&amp;linktype=' . $ConType . '\');}"><b>'.$txt['sab_add_dst_'.$ConItemName].'</b></a>
	</td>
</tr>';

	echo '</td></tr></table></td></tr>';

    if (!$_GET['print'] && $FormType != 'ReadOnly')  // I don't know why this is needed, but the page stucture falls apart a bit without it.
    {
        echo '</table>';
    }
}

function DoContactSection($Header, $ConType, $SabID, $con, $ColSpan, $EmailCheckBox, $FormType = "Main", $IncludeColsList, $ListingID = null)
{
    $Div = $Header;

    if ($_GET["print"] || $FormType == 'ReadOnly')
    {
        $EmailCheckBox = false;
        $ColSpan--;
    }

    echo'
<tr height="20">
	<td class="titlebg" colspan="' . $ColSpan . '">
    <a href="Javascript:ToggleTwistyExpand(\'' . $ConType . '_' . $Div . '\', \'twisty_' . $ConType . '_' . $Div . '\',\'Images/expand.gif\', \'Images/collapse.gif\')">
    <img hspace="2" id="twisty_'. $ConType . '_' . $Div . '" src="Images/expand.gif" alt="+" border="0" /></a>
    <b>' . $Header . ' (' . count($con) . ')</b>
	</td>
</tr>
<tr>
<td>
<div id="' . $ConType . '_' . $Div .'" style="display:none">
';

    require_once 'Source/libs/ListingClass.php';
    $Listing = new Listing('CON', $ListingID);
    $Listing->LinkModule = 'SAB';
    $Listing->LoadColumnsFromDB();
    $Listing->EmptyMessage = 'No contacts';

    if ($EmailCheckBox)
    {
        $Listing->Checkbox = true;
        $Listing->CheckboxType = $ConType.'_'.$Div;
        $Listing->CheckboxIDField = 'con_id';
    }

    if ($IncludeColsList)
    {
        $Listing->AddAdditionalColumns($IncludeColsList);
    }

    $Listing->LoadData($con);

    echo $Listing->GetListingHTML($FormType);

    echo '</div>';
}

function ListActions($actions)
{
	global $scripturl, $txt;

	$SABPerms = GetParm("SAB_PERMS");
    $print = ($_GET["print"] == 1);

	echo'
<tr>
    <td>
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <tr>
    	<td class="titlebg" colspan="8"><b>Actions</b></td>
    </tr>';

    if ($savemessage = $_GET["savemessage"])
    {
        echo'
        <tr>
        	<td class="windowbg" colspan="8"><font color="red"><b>'.$txt['action_saved'].'</b></font></td>
        </tr>';
    }

    echo'
    <tr>
    	<td class="windowbg" width="10%"><b>Priority</b></td>
    	<td class="windowbg"><b>Type</b></td>
    	<td class="windowbg"><b>Description</b></td>
    	<td class="windowbg"><b>Responsible</b></td>
    	<td class="windowbg"><b>Start</b></td>
    	<td class="windowbg"><b>Due</b></td>
    	<td class="windowbg"><b>Done</b></td>
    	<td class="windowbg"><b>Cost</b></td>
    </tr>
';

	if (!$actions)
		echo '
    <tr>
    	<td class="rowtitlebg" colspan="8">
    	<b>No actions.</b>
    	</td>
    </tr>';
	else
	{
		foreach ($actions as $action)
		{
			echo'
<tr>
    <td class="rowtitlebg">'
    . (!$print ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . "?action=action&amp;module=SAB&amp;recordid=$action[recordid]&amp;frommainrecord=1" . '\');}">' : '')
	. code_descr("SAB", "act_priority", $action["act_priority"])
    . (!$print ? '</a>' : '')
	. '</td>
	<td class="rowtitlebg">'
	. (!$print ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . "?action=action&amp;module=SAB&amp;recordid=$action[recordid]&amp;frommainrecord=1" . '\');}">' : '')
	. $action["act_type"]
    . (!$print ? '</a>' : '')
	. '</td>
	<td class="rowtitlebg">'
	. (!$print ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl . "?action=action&amp;module=SAB&amp;recordid=$action[recordid]&amp;frommainrecord=1" . '\');}">' : '')
	. $action["act_descr"]
    . (!$print ? '</a>' : '')
	. '</td>
	<td class="rowtitlebg">
	' . code_descr("SAB", "act_to_inits", $action["act_to_inits"])
	. '
	</td>
	<td class="rowtitlebg">
	' . date("d/m/Y", strtotime($action["act_dstart"]))
	. '
	</td>
	<td class="rowtitlebg">
	' . date("d/m/Y", strtotime($action["act_ddue"]))
	. '
	</td>
	<td class="rowtitlebg">
	' . date("d/m/Y", strtotime($action["act_ddone"]))
	. '
	</td>
	<td class="rowtitlebg" align="right">';

    if (GetParm("COUNTRY") == "CANADA" || GetParm("COUNTRY") == "USA")
    {
        echo '$';
    }
    else
    {
        echo '?';
    }

	echo number_format($action["act_cost"], 2, '.', ',')
	. '
	</td>
</tr>';
		}
	}

    if (!($_GET["print"] == 1))
    {
	    echo '
<tr>
	<td class="titlebg" colspan="8">
	    <b>'.$txt['new_actions'].'</b>
	</td>
</tr>
<tr>
	<td class="rowtitlebg" colspan="8">
	<a href="Javascript:if(CheckChange()){SendTo(\'' . "$scripturl?action=newaction&amp;module=SAB&amp;act_cas_id=" . Sanitize::SanitizeInt($_GET['recordid']) . "&amp;frommainrecord=1" . '\');}"><b>Create a new action for this ' . $txt["SABSName"] . '</b></a>
	</td>
</tr>';
    }
	echo '
</table>
    </td>
</tr>';

}

function EmailPreviewSection($sab, $FormType)
{
    require_once 'Source/SABS/SABSEmail.php';
    echo SABSEmailPreviewSection($sab, $FormType);
}

?>
