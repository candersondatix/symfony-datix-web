<?php
$GLOBALS["FormTitle"] = "Safety Alerts Form";
$GLOBALS["MandatoryFields"] = array (
  "sab_title" => "details",

);

$GLOBALS["HideFields"] = array (
  'sab_subtype' => true,
  'sab_read_by_due' => true,
  "location" => true,
  'sab_organisation' => true,
  'sab_unit' => true,
  'sab_clingroup' => true,
  'sab_directorate' => true,
  'sab_specialty' => true,
  'sab_loctype' => true,
  'sab_locactual' => true,
  'progress_notes' => true,
  'action_chains' => true,
);

$GLOBALS['NewPanels'] = array(
    'contacts_type_S' => true,
    'contacts_type_I' => true,
    'contacts_type_N' => true,
    'linked_records' => true,
    'notepad' => true,
    'progress_notes' => true,
    'sab_history' => true,
    'linked_actions' => true,
    'sabs_email_preview' => true,
    'word' => true,
    'action_chains' => true,
);
?>
