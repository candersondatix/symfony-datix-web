<?php

$list_columns_standard = array(
    "sab_reference" => array(
        'width' => '4'),
    "sab_title" => array(
        'width' => '15'),
    "sab_dstart_due" => array(
        'width' => '6'),
    "sab_dend_due" => array(
        'width' => '6'),
    "sab_action_type" => array(
        'width' => '7'),
);

$list_columns_mandatory = array("sab_reference");


?>
