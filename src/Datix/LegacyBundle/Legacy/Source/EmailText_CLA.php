<?php

$EmailText["Acknowledge"]["Subject"] = "Acknowledgment of claim record submission";

$EmailText["Acknowledge"]["Body"] = "You reported a claim using the Datix claim report form.
The claim has been given ID $data[recordid].\n
We would like to thank you for taking the time to report this claim.\n
The Datix administrators.";

$EmailText["Notify"]["Subject"] = "Datix Claim Report Number $data[recordid]";

$EmailText["Notify"]["Body"] = "A claim record has been submitted via the Datix web form.

The details are:

Form number: $data[cla_ourref]

Description:

$data[cla_synopsis]

Please go to $scripturl?action=record&module=CLA&recordid=$data[recordid] to view and approve it.
";

$EmailText["Feedback"]["Subject"] = "DatixWeb feedback message";

$message = "This is a feedback message from $_SESSION[fullname]. Claim form reference is ";
$message .= $data['cla_ourref'];
$message .= ".\nThe feedback is: \n\n\n";
$message .= "Please go to $scripturl?action=record&module=CLA&recordid=$data[recordid] to view the record.";

$EmailText["Feedback"]["Body"] = $message;

$EmailText['NewHandler']['Subject'] = "You are now the handler for Datix "._tk('CLAName')." $data[recordid]";
$EmailText['NewHandler']['Body'] = "You have been assigned as the handler for Datix "._tk('CLAName')." $data[recordid].

The details are:

Form number: $data[recordid]

Description:

$data[cla_synopsis]

Please go to $scripturl?action=record&module=CLA&recordid=$data[recordid] to view it.
";

$EmailText['NewInvestigator']['Subject'] = "You are now an investigator for Datix "._tk('CLAName')." $data[recordid]";
$EmailText['NewInvestigator']['Body'] = "You have been assigned as an investigator for Datix "._tk('CLAName')." $data[recordid].

The details are:

Form number: $data[recordid]

Description:

$data[cla_synopsis]

Please go to $scripturl?action=record&module=CLA&recordid=$data[recordid] to view it.
";
?>
