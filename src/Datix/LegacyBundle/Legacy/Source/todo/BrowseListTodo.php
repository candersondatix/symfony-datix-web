<?php

// ==========================================================================//
// Initialise conditions                                                     //
// ==========================================================================//

$_SESSION["ReadOnlyMode"] = false;

$ListType = $_GET["listtype"];

// ==========================================================================//
// Build where clause according to access level, permission, timescales      //
// and criteria specific to the type of listing                              //
// ==========================================================================//

if ($ListType == "overdue")
{
    $ListTitleExtra = _tk('overdue');
}
else if ($ListType == "duetoday")
{
    $ListTitleExtra = _tk('due_today');
}
else if ($ListType == "duethisweek")
{
    $ListTitleExtra = _tk('due_this_week');
}
else if ($ListType == "duethismonth")
{
    $ListTitleExtra = _tk('due_this_month');
}
else
{
    $ListTitleExtra = _tk('all');
}

$ListTitle = _tk('mod_todo_title'). " - " .$ListTitleExtra;

$orderby = $_SESSION["LIST"]["TOD"]["ORDERBY"];
$order = $_SESSION["LIST"]["TOD"]["ORDER"];

if (!$orderby){
    $orderby = "tod_due";
    $order = "DESC";
}

$listnum = $listnum2;

if($_POST["listnum"])
    $listnum = Sanitize::SanitizeInt($_POST["listnum"]);

// ==========================================================================//
// Build the URL to be used when clicking on a record                        //
// ==========================================================================//

$timescales_url = $scripturl . '?action=list&amp;module=TOD&amp;whichdate='.$WhichDate.'&amp;listtype='.$ListType.(($_GET["timescale"]=="range" || ($_REQUEST["from"] && $_REQUEST["to"])) ? "&amp;from=$_REQUEST[from]&amp;to=$_REQUEST[to]" : "")
.($_GET["timescale"]=="overdue" ? "&amp;timescale=overdue" : "");

//$record_url = "$scripturl?action=todo&fromsearch=1";


// ==========================================================================//
// Columns to be displayed                                                   //
// ==========================================================================//

require_once 'Source/libs/ListingClass.php';
//$list_columns = Listings_ModuleListingDesign::GetListingColumns('TOD');
$list_columns = array(
    "recordid" => array(
        'width' => '4'),
    "tod_module" => array(
        'width' => '6'),
    "tod_descr" => array(
        'width' => '20'),
    "tod_type" => array(
        'width' => '7'),
    //"tod_mod" => array(
   //     'width' => '0'),
    "tod_due" => array(
        'width' => '7'),
);

$col_info_extra['colrename'] = 'ID';

if (is_array($list_columns))
{
    foreach ($list_columns as $col_name => $col_info)
    {
        if(is_array($list_columns_extra[$col_name])){
            if(!array_key_exists("condition", $list_columns_extra[$col_name]) || $list_columns_extra[$col_name]["condition"]){
                $selectfields[$col_name] = $col_name;
            }
            else{
                unset($list_columns[$col_name]);
            }
            if(array_key_exists("prefix", $list_columns_extra[$col_name]))
                $col_info_extra['prefix'] = $list_columns_extra[$col_name]["prefix"];
            if(array_key_exists("dataalign", $list_columns_extra[$col_name]))
                $col_info_extra['dataalign'] = $list_columns_extra[$col_name]["dataalign"];
        }
        else
            $selectfields[$col_name] = $col_name;
    }
}
//Hidden field required in select, but not shown in listing
$selectfields["tod_mod"] = "tod_mod";

$list_columns_count = count($list_columns);

$licensed_modules = array();

foreach (['ACT', 'INC', 'COM', 'RAM', 'CQO', 'CQS', 'CQP'] as $moduleToInclude)
{
    if(ModIsLicensed($moduleToInclude))
    {
        $licensed_modules[] = $moduleToInclude;
    }
}

$ToDoList = new ToDo_List($licensed_modules, $ListType);
$dbtable = $ToDoList->getToDoSQL();

$list_request = GetSQLResultsTimeScale($selectfields, $dbtable, $order, $orderby,
            '1=1', $listnum, $listdisplay, $listnum2, $listnumall, $sqla, null, $module);

