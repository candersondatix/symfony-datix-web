<?php

$EmailText["Acknowledge"]["Subject"] = "Acknowledgment of incident report submission";

$EmailText["Acknowledge"]["Body"] = "You reported an incident on $data[inc_dreported] using the Datix incident report form.
The incident has been given number $data[inc_ourref].\n
We would like to thank you for taking the time to report this incident.\n
The Datix administrators.";

$EmailText["Notify"]["Subject"] = "Datix Incident Report Number $data[inc_ourref]";

$EmailText["Notify"]["Body"] = "An incident report has been submitted via the DATIX web form.

The details are:

Form number: $data[inc_ourref]

Description:

$data[inc_notes]

Please go to $scripturl?action=incident&recordid=$data[recordid] to view and approve it.
";

$EmailText["Feedback"]["Subject"] = "DatixWeb feedback message";

$message = "This is a feedback message from $_SESSION[fullname]. Incident form reference is ";
$message .= $data['inc_ourref'];
$message .= ".\nThe feedback is: \n\n";
$message .= "Please go to $GLOBALS[scripturl]?action=incident&recordid=$data[recordid] to view the incident";

$EmailText["Feedback"]["Body"] = $message;

// Registration and reminder e-mails

$EmailText["RegisterNotify"]["Subject"] = "DatixWeb Registration Request";

$EmailText["RegisterNotify"]["Body"] = "A user has submitted a registration request for DatixWeb.  Details are:

Name: $_POST[title] $_POST[forenames] $_POST[surname]
Organisation: $_POST[organisation]
Email address: $_POST[email]

You will need to log in at $scripturl and approve their application.";

$EmailText["RegisterUser"]["Subject"] = "Your DatixWeb Registration Request";
$EmailText["RegisterUser"]["Body"] = "This is an automated message.  Your request to register for DatixWeb has been sent to the DatixWeb administrator.  Once your application has been processed, you will receive a further e-mail with details of how to access the service.

";

$EmailText["RegistrationApproved"]["Subject"] = "Your DatixWeb Registration Request";
$EmailText["RegistrationApproved"]["Body"] = "Your registration for DatixWeb has been approved.  You may log in at $scripturl with the user name $data[login] and the password that you chose when you registered.

If you have forgotten your password, please click on the 'Forgotten password?' link under the Password box on the login screen, or go to $scripturl?action=reset_password&user=$data[login] .  An e-mail will be sent to you with instructions on how to reset your password.

";

$EmailText["RegistrationRejected"]["Subject"] = "Your DatixWeb Registration Request";
$EmailText["RegistrationRejected"]["Body"] = "Your request to register for DatixWeb has not been approved.

The reason given is $data[reason]";

$EmailText['NewHandler']['Subject'] = "You are now handler for Datix "._tk('INCName')." $data[inc_ourref]";
$EmailText['NewHandler']['Body'] = "You have been assigned as handler for Datix "._tk('INCName')." $data[inc_ourref].

The details are:

Form number: $data[inc_ourref]

Description:

$data[inc_notes]

Please go to $scripturl?action=incident&recordid=$data[recordid] to view it.
";

$EmailText['NewInvestigator']['Subject'] = "You are now an investigator for Datix "._tk('INCName')." $data[inc_ourref]";
$EmailText['NewInvestigator']['Body'] = "You have been assigned as an investigator for Datix "._tk('INCName')." $data[inc_ourref].

The details are:

Form number: $data[inc_ourref]

Description:

$data[inc_notes]

Please go to $scripturl?action=incident&recordid=$data[recordid] to view it.
";

$EmailText['Overdue']['Subject'] = 'You have '.$data['overdue_num'].' overdue incidents to review';
$EmailText['Overdue']['Body'] = 'The following incidents are overdue for review.  Click on the links to access each record.

'.$data['overdue_list_html'];

$EmailText["UpdatedRecord"]["Subject"] = "Datix "._tk('INCNameTitle')." Number $data[inc_ourref] has been updated";

$EmailText["UpdatedRecord"]["Body"] = "An "._tk('INCName')." has been updated via the DATIX web form.

The details are:

Form number: $data[inc_ourref]

Description:

".$data["inc_notes"]."

Please go to $scripturl?action=incident&recordid=$data[recordid] to view it.
";
?>
