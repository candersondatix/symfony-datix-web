<?php

use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

// Function EmailNewInvestigators() is called when a record is saved, but
// before the record is saved to the incidents_main table. It checks to see
// if the inc_investigator field has been edited and, if it has, sends an
// e-mail of type NewInvestigator to each new member of staff that has been
// added to this field.
function EmailNewInvestigators($data, $module)
{
    global $ModuleDefs;

    if(!isset($ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS']))
    {
        return;
    }

    // Check for staff to e-mail if the Investigators field has been edited
    // and is not empty.
    if ($data['CHANGED-'.$ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS']] && $data[$ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS']] != '')
    {
        require_once 'Source/libs/Email.php';

        if($data[$ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS']])
        {
            $Investigators = explode(' ', $data[$ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS']]);
        }

        // Get the value of inc_investigator already in the database, i.e.
        // the old value.
        $OriginalInvestigator = DatixDBQuery::PDO_fetch('SELECT '.$ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS'].' FROM '.$ModuleDefs[$module]['TABLE'].' WHERE recordid = :recordid', array('recordid' => $data['recordid']), PDO::FETCH_COLUMN);

        if($OriginalInvestigator)
        {
            $OriginalInvestigators = explode(' ', $OriginalInvestigator);
        }

        // If the field was empty, e-mail everyone in the new list of
        // investigators
        if (empty($OriginalInvestigators))
        {
            $EmailInvestigators = $Investigators;
        }
        else
        {
            // Loop through the list of investigators, adding only those
            // which were not in the old list to the array of staff to be
            // e-mailed.
            foreach($Investigators as $Inv)
            {
                if (!in_array($Inv, $OriginalInvestigators))
                {
                    $EmailInvestigators[] = $Inv;
                }
            }
        }

        if ($EmailInvestigators != '')
        {
            $Factory = new UserModelFactory();
            // Loop through the array, sending e-mails to all those members
            // who have an e-mail address. Also, add all the User entity
            // properties to the $data array so things like fullname
            // can be used in the e-mail template if required.
            foreach ($EmailInvestigators as $Initials)
            {
                $StaffDetails = $Factory->getMapper()->findByInitials($Initials);

                if ($StaffDetails->isEmailable())
                {
                    Registry::getInstance()->getLogger()->logEmail('E-mail new Investigator added to record ' . $data['recordid'] . ' in module ' . $module . '.');

                    $emailSender = EmailSenderFactory::createEmailSender($module, 'NewInvestigator');
                    $emailSender->addRecipient($StaffDetails);
                    $emailSender->sendEmails(array_merge($StaffDetails->getVars(), $data));
                }
            }
        }
    }
}

// Function EmailNewHandler() is called when an incident is saved, but
// before the record is saved to the incidents_main table. It checks to see
// if the inc_mgr (normally called Handler) field has been edited and, if it
// has, sends an e-mail of type NewHandler to the new member of staff that has
// been added to this field.
function EmailNewHandler($data, $module)
{
    global $ModuleDefs;

    // Don't bother sending an e-mail if the field is empty
    if ($data['CHANGED-'.$ModuleDefs[$module]['FIELD_NAMES']['HANDLER']] && $data[$ModuleDefs[$module]['FIELD_NAMES']['HANDLER']] != '')
    {
        require_once 'Source/libs/Email.php';

        $Factory = new UserModelFactory();
        $StaffDetails = $Factory->getMapper()->findByInitials($data[$ModuleDefs[$module]['FIELD_NAMES']['HANDLER']]);

        if ($StaffDetails->isEmailable())
        {
            Registry::getInstance()->getLogger()->logEmail('Emailing new handler on record ' . $data['recordid'] . ' in module ' . $module . '.');

            $emailSender = EmailSenderFactory::createEmailSender($module, 'NewHandler');
            $emailSender->addRecipient($StaffDetails);
            $emailSender->sendEmails(array_merge($StaffDetails->getVars(), $data));
        }
    }
}

?>