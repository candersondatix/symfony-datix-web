<?php

$GLOBALS['FormTitle'] = 'Datix Level 2 Form';

$GLOBALS["ExpandSections"] = array (
  'rep_approved' =>
  array (
    0 =>
    array (
      'section' => 'rejection',
      'alerttext' => 'Please complete the \'Details of rejection\' section before saving this form.',
      'values' =>
      array (
        0 => 'REJECT',
      ),
    ),
  ),
);

?>