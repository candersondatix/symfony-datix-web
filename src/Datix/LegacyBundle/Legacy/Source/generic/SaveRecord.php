<?php

function SaveRecord()
{
    global $scripturl, $yySetLocation, $Perms, $SectionVisibility;
    global $dtxdebug, $ButtonDefs, $ModuleDefs, $AccessLvlDefs, $FieldDefs, $HideFields;

    session_start();

    $Module        = Sanitize::getModule($_GET['module']);
    $ModuleTable   = $ModuleDefs[$Module]['TABLE'];
    $Perms         = GetParm($ModuleDefs[$Module]['PERM_GLOBAL']);
    $ModuleActions = $ModuleDefs[$Module]['EXTRA_FORM_ACTIONS'];

    if (!is_array($_POST) || empty($_POST))
    {
        $form_action = "Cancel";
    }
    else
    {
        $form_action = $_POST["rbWhat"];
    }

    Unlock($Module, $_POST["recordid"]);

    switch ($form_action)
    {
        case "Cancel":
        case BTN_CANCEL:
            if($_POST['fromsearch'])
            {
                $yySetLocation = $_SESSION["LAST_PAGE"];
            }
            elseif ($_POST['from_report'])
            {
            	$yySetLocation = $scripturl.'?action=list&module='. $Module .'&listtype=search&from_report=1';
            }
            else
            {
                if($ModuleDefs[$Module]['MODULE_GROUP'])
                {
                    $yySetLocation = "$scripturl?module=".$ModuleDefs[$Module]['MODULE_GROUP'];
                }
                else
                {
                    $yySetLocation = "$scripturl?module=$Module";
                }

                if ($_POST["form_id"])
                {
                    $yySetLocation .= '&form_id=' . $_POST["form_id"];
                }
            }
            redirectexit();
        case "ShowLevel1Values":
            $yySetLocation = $scripturl . "?action=record&recordid=$_POST[recordid]&show_level1_values=1";
            redirectexit();
        case "ShowAudit":
            $yySetLocation = $scripturl . "?action=record&recordid=$_POST[recordid]&full_audit=1";
            redirectexit();
        case "SaveDesign":
        case "ClearDesign":
        case "CancelDesign":
            require_once "Source/AdminSetup.php";
            SaveDesignSettings();
            break;
        case "Search":
            $yySetLocation = $scripturl . '?action=doselection';
            redirectexit();
            break;
        case "BackToListing":
            $yySetLocation = 'app.php?action=reportdesigner';
            redirectexit();
            break;
        default:
            // default case to catch any custom module actions
            if (is_array($ModuleActions) && array_key_exists($form_action, $ModuleActions))
            {
                require_once $ModuleActions[$form_action]['sourceFile'];
                $ModuleActions[$form_action]['callback']();
            }

            break;
    }

    $HoldingForm = ($_POST["holding_form"] == 1);
    $recordid = Sanitize::SanitizeInt($_POST["recordid"]);

    if ($ModuleDefs[$Module]['USES_APPROVAL_STATUSES'] !== false)
    {
        $approved = GetValidApprovalStatusValueFromPOST(array('module' => $Module));
    }

    GetSectionVisibility($Module, ($HoldingForm? 1 : 2), $_POST, $_POST['link_module']);
    BlankOutPostValues($Module, ($HoldingForm? 1 : 2), $_POST['link_module']);

    $ValidationErrors = validateData($Module);

    $ContactError     = ValidateLinkedContactData($_POST, $Module);

    /*We need to ensure that fields that are not visible on save are not validated (otherwise you end
    up with validation messages you cannot see. The best way of doing this would be to have GetSectionVisibility()
    and BlankOutPostValues() run for each linked contact form. That's going to be pretty risky to force in now,
    so in the short term I'm doing a manual check of whether the field is visible here.
    TODO: make GetSectionVisibility and BlankOutPostValues generic enough that they can run on subforms within level 1 forms.
    */
    $ContactError = \src\generic\controllers\MainRecordController::RemoveHiddenContactValidation($ContactError, new \src\framework\controller\Request());

    $ValidationErrors = array_merge($ValidationErrors, $ContactError);

    if ($_POST["max_doc_suffix"]) //documents linked on level 1
    {
        require_once "Source/libs/Documents.php";
        $DocError = ValidateLinkedDocumentData($_POST);
        $ValidationErrors = array_merge($ValidationErrors, $DocError);
    }

    if($ValidationErrors)
    {
        $error['Validation'] = $ValidationErrors;
    }

    if (!empty($error))
    {
        $data = Sanitize::SanitizeStringArray($_POST);
        $data["error"]        = $error;
        $data["rep_approved"] = $approved;
        $data["submit_stage"] = $submit_stage;

        // Workaround to convert dates to SQL format
        // TODO: This needs to be removed when refactored
        $ModuleDateFields = array_merge(GetAllFieldsByType($Module, 'date'), getAllUdfFieldsByType('D', '', $data));

        foreach ($ModuleDateFields as $Date)
        {
            if (array_key_exists($Date, $data) && $data[$Date] != '')
            {
                $data[$Date] = UserDateToSQLDate($data[$Date]);
                //some extra field dates look directly at the post value, so need to blank it out here.
                unset($_POST[$Date]);
            }
        }

        if ($HoldingForm)
        {
            require_once "Source/generic/MainRecord.php";
            ShowForm($data, '',1);
        }
        else
        {
            require_once "Source/generic/MainRecord.php";
            ShowForm($data);
        }
        obExit();
    }

    if(bYN(GetParm('AUTO_POPULATE_REC_NAME_'.$Module, 'N')))
    {
        if($ModuleDefs[$Module]['RECORD_NAME_FROM_CONTACT'])
        {
            $NumContacts = max($_POST["contact_max_suffix"],$_POST["increp_contacts"]);

            for($i = 1; $i<= $NumContacts; $i++)
            {
                if($Module == 'COM' && $_POST["lcom_iscomplpat_$i"] == 'Y')
                {
                    $ChangeName = ($ModuleDefs[$Module]['FIELD_NAMES']['NAME'] && !$_POST[$ModuleDefs[$Module]['FIELD_NAMES']['NAME']] && $_POST["con_surname_$i"] && $_POST["link_type_$i"] == 'C');
                }
                else
                {
                    $ChangeName = ($ModuleDefs[$Module]['FIELD_NAMES']['NAME'] && !$_POST[$ModuleDefs[$Module]['FIELD_NAMES']['NAME']] && $_POST["con_surname_$i"] && $_POST["link_type_$i"] == $ModuleDefs[$Module]['RECORD_NAME_FROM_CONTACT']);
                }

                if ($ChangeName && !ContactHidden($_POST, $i))
                {
                    $_POST[$ModuleDefs[$Module]['FIELD_NAMES']['NAME']] = \UnicodeString::substr(\UnicodeString::strtoupper(Sanitize::SanitizeRaw($_POST["con_surname_$i"]) . ($_POST["con_forenames_$i"] ? " " . Sanitize::SanitizeRaw($_POST["con_forenames_$i"]) : "")),0,32);
                }
            }
        }
    }

    if (!$recordid)
    {
        $newRecord = true;
        if ($FieldDefs[$Module]['recordid']["IdentityCol"] == true)
        {
            $sql = 'INSERT INTO ' . $ModuleTable . ' (createdby) VALUES (:createdby)';
            $recordid = DatixDBQuery::PDO_insert($sql, array('createdby' => $_SESSION["initials"]));
        }
        else
        {
            $recordid = GetNextRecordID($ModuleTable, true, 'recordid', $_SESSION["initials"]);
        }
        if(!$_SESSION['logged_in'])
        {
            $_SESSION[$Module]['LOGGEDOUTRECORDHASH'] = getRecordHash($recordid);
        }
    }
    else
    {
        $newRecord = false;
        DoFullAudit($Module, $ModuleTable, $recordid);
    }

    $data = $_POST;

    $data["rep_approved"] = $approved;

    $Prefix = GetParm($Module.'_1_REF_PREFIX');
    if ($Prefix && $HoldingForm && $data[$ModuleDefs[$Module]['FIELD_NAMES']['REF']] == '')
    {
        $data[$ModuleDefs[$Module]['FIELD_NAMES']['REF']] = $Prefix.$recordid;
    }

    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => $Module, 'level' => ($HoldingForm? 1 : 2), 'parent_module' => $_POST['link_module']));
    $FormDesign->LoadFormDesignIntoGlobals();

    $data = ProcessNewCodeFieldValues ($Module, $data, $FieldDefs);
    $data = ParseSaveData(array('module' => $Module, 'data' => $data));

    require_once 'source/generic/RootCauses.php';
    $data = ParseRootCausesGeneric(array('module' => $Module, 'data' => $data));

    // Send notifications for changed handler and investigator
    // BEFORE the record is updated (needs to check old values).
    if (!$newRecord && bYN(GetParm('STAFF_CHANGE_EMAIL_'.$Module, 'N')))
    {
        $data['recordid'] = $recordid; //meeded so recordid can appear in email.
        require_once 'source/generic/EmailChangedStaff.php';
        EmailNewHandler($data, $Module);
        EmailNewInvestigators($data, $Module);
    }

    if(is_array($ModuleDefs[$Module]['EMAIL_AFTER_SAVE_FUNCTIONS']))
    {
        foreach ($ModuleDefs[$Module]['EMAIL_AFTER_SAVE_FUNCTIONS'] as $Function)
        {
            $data['recordid'] = $recordid; //needed so recordid can appear in email.
            require_once 'source/generic_modules/'.$Module.'/ModuleFunctions.php';
            $Function($data);
        }
    }

    if(is_array($ModuleDefs[$Module]['BEFORE_SAVE_FUNCTIONS']))
    {
        foreach ($ModuleDefs[$Module]['BEFORE_SAVE_FUNCTIONS'] as $Function)
        {
            $data['recordid'] = $recordid;
            require_once 'source/generic_modules/'.$Module.'/ModuleFunctions.php';
            $data = $Function($data);
        }
    }

    //SaveRejectionSection($module, $data, $approved, $form_action);

    $sql = "UPDATE $ModuleTable SET ";

    $FieldArray = $ModuleDefs[$Module]['FIELD_ARRAY'];

    if($ModuleDefs[$Module]['LINKED_MODULE'])
    {
        $FieldArray[] = $ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'][$_POST['link_module']];
        $data[$ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'][$_POST['link_module']]] = $_POST['main_recordid'];
    }

    $sql .= GeneratePDOSQLFromArrays(array(
                        'FieldArray' => $FieldArray,
                        'DataArray' => $data,
                        'Module' => $Module,
                        'end_comma' => true
                        ), $PDOParamsArray);

    $PDOParamsArray["updatedby"] = $_SESSION["initials"];
    $PDOParamsArray["updateid"] = GensUpdateID($_POST["updateid"]);
    $PDOParamsArray["updateddate"] = date('d-M-Y H:i:s');

    $sql .= " updatedby = :updatedby, updateid = :updateid, updateddate = :updateddate";

    $PDOParamsArray["where_recordid"] = $recordid;
    $PDOParamsArray["postedupdateid"] = $_POST["updateid"];
    $sql .= " WHERE recordid = :where_recordid and (updateid = :postedupdateid OR updateid IS NULL)";

    $result = PDO_query($sql, $PDOParamsArray);

    if (!$result)
    {
        $error = "An error has occurred when trying to save the record.  Please report the following to the Datix administrator: $sql";
    }

    if (!empty($error))
    {
        SaveError($error);
    }

    $data["recordid"] = $recordid;

    //Save UDFs
    require_once "Source/libs/UDF.php";
    SaveUDFs($recordid, GetModIDFromShortName($Module));

   // $data["pal_ourref"] = GetParm("PAL_1_REF_PREFIX") . $recordid;

    //Save notepad
    require_once "Source/libs/notepad.php";
    $error = SaveNotes(array('id_field' => $ModuleDefs[$Module]['FK'], 'id' => $recordid, 'notes' => $data['notes'], 'new' => $newRecord));

    //Save Progress Notes
    require_once "Source/libs/progressnotes.php";
    saveProgressNotes($Module, $data);

    //Save Rejection Notes
    if(bYN(GetParm("REJECT_REASON",'Y')) && $data['rep_approved'] == 'REJECT')
    {
        require_once "Source/libs/Reasons.php";
        SaveReason($Module, $recordid, $data);
    }

    //save contacts attached on level1.
    require_once "Source/contacts/SaveContact.php";
    SaveContacts(array('main_recordid' => $recordid, 'module' => $Module, 'formlevel' => ($HoldingForm? 1 : 2)));

    //Save linked records
    if(is_array($ModuleDefs[$Module]['LINKED_RECORDS']))
    {
        foreach($ModuleDefs[$Module]['LINKED_RECORDS'] as $Type => $aDetails)
        {
            $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => $Module, 'level' => ($HoldingForm? 1 : 2)));
            //IncludeCurrentFormDesign($Module, ($HoldingForm? 1 : 2));
            if(!$FormDesign->HideFields[$aDetails['section']])
            {
                $aDetails['module'] = $Module;
                $aDetails['main_recordid'] = $recordid;
                $error = SaveLinkedRecords($aDetails);
            }
        }
    }

    // custom functions for saving additional data
    if(is_array($ModuleDefs[$Module]['EXTRA_SAVE_INCLUDES']))
    {
        foreach($ModuleDefs[$Module]['EXTRA_SAVE_INCLUDES'] as $includeFile)
        {
            require_once $includeFile;
        }
    }
    if(is_array($ModuleDefs[$Module]['EXTRA_SAVE_FUNCTIONS']))
    {
        foreach($ModuleDefs[$Module]['EXTRA_SAVE_FUNCTIONS'] as $function)
        {
            $function($data);
        }
    }

    if (ModIsLicensed("HOT"))
    {
        require_once "Source/generic_modules/HOT/ModuleFunctions.php";
        InsertIntoHotspotQueue($Module, $recordid);
    }

    if($ModuleDefs[$Module]['LINKED_DOCUMENTS'])
    {
        if ($SectionVisibility['documents'] || $SectionVisibility['extra_document'])
        {
            //save documents attached on level1.
            require_once "Source/libs/Documents.php";
            SaveDocsFromLevel1($recordid, $Module);
        }
    }

    //Adding a new record that is not represented in the session recordlist object can cause
    //problems with the session checkboxes (since they can't point to a record that didn't exist
    //when the object was created, so we reset the selection here.
    if($newRecord && isset($_SESSION[$Module]['RECORDLIST']))
    {
        unset($_SESSION[$Module]['RECORDLIST']);
    }

    if(isset($ModuleDefs[$Module]['LOCATION_FIELD_PREFIX']))
    {
        // $sendEmailLocationChange is used for sending emails (not to reporter/handler/manager) when the location of a record is changed
        $sendEmailLocationChange = false;
        $modPre = $ModuleDefs[$Module]['LOCATION_FIELD_PREFIX'];
        foreach(array('CHANGED-'.$modPre.'organisation',
                    'CHANGED-'.$modPre.'unit',
                    'CHANGED-'.$modPre.'clingroup',
                    'CHANGED-'.$modPre.'directorate',
                    'CHANGED-'.$modPre.'specialty',
                    'CHANGED-'.$modPre.'loctype',
                    'CHANGED-'.$modPre.'locactual') as $field)
        {
            if($data[$field] == '1')
            {
                $sendEmailLocationChange = true;
            }
        }
    }

    // e-mail the handler of the record when:
    // - submitting a risk without logging in
    // - submitting a risk by a logged in RISK1 user
    // $approved is hardcoded, could be set in 'email sending' or 'module' settings or similar
    if((bYN(GetParm($ModuleDefs[$Module]['SHOW_EMAIL_GLOBAL'],'N')) && $HoldingForm) || $sendEmailLocationChange)
    {
        require_once "Source/libs/Email.php";
        //We need to send "POST" rather than $ram data here, to avoid sending dates in the wrong format.

        // Workaround to convert dates to SQL format
        // TODO: This needs to be removed when refactored
        $DateFields = GetAllFieldsByType($Module, 'date');
        foreach ($DateFields as $Date)
        {
            if (array_key_exists($Date, $_POST) && $_POST[$Date] != '')
            {
                $_POST[$Date] = UserDateToSQLDate($_POST[$Date]);
            }
        }
        $_POST['recordid'] = $recordid;
        $Output = SendEmails(array(
            'module'      => $Module,
            'data'        => Sanitize::SanitizeStringArray($_POST),
            'progressbar' => $Progress,
            'from'        => $data['rep_approved_old'],
            'to'          => $data['rep_approved'],
            'perms'       => ($Perms? $Perms: 'NONE'),
            'level'       => $data['formlevel'],
            'groupsOnly'  => $sendEmailLocationChange));
    }

    if ($ModuleDefs[$Module]['LINKED_CONTACTS'] !== false)
    {
        $UnapprovedContactArray = GetUnapprovedContacts(array('module' => $Module, 'recordid' => $recordid));
        //discount unapproved contacts if they are hidden, since the user will not be able to do anything about them.
        if (is_array($UnapprovedContactArray))
        {
            IncludeCurrentFormDesign($Module, 2);
            foreach ($UnapprovedContactArray as $key => $ContactDetails)
            {
                if ($GLOBALS['HideFields']['contacts_type_'.$ContactDetails['link_type']])
                {
                    unset($UnapprovedContactArray[$key]);
                }
            }
        }
    }

    // This was moved here because when we have unapproved contacts in a record the actions chains were not being attached
    if (bYN(GetParm('WEB_TRIGGERS', 'N')))
    {
        require_once 'Source/libs/Triggers.php';
        ExecuteTriggers($recordid, $data, $ModuleDefs[$Module]['MOD_ID']);
    }

    // Update record last updated
    if ($Module != 'PAY' || (isset($_GET['from_parent_record']) && $_GET['from_parent_record'] == '1') ||
        (isset($_POST['fromparent']) && $_POST['fromparent'] == '1'))
    {
        $loader = new src\framework\controller\Loader();
        $controller = $loader->getController([
            'controller' => 'src\\generic\\controllers\\RecordController'
        ]);
        $controller->setRequestParameter('module', $data['link_module']);
        $controller->setRequestParameter('recordId', $data['main_recordid']);
        echo $controller->doAction('updateRecordLastUpdated');
    }

    if ($form_action == 'QBE')
    {
        // redirect to appropriate search form to create a Query By Example
        $yySetLocation = $scripturl;
        if (isset($ModuleDefs[$_POST['qbe_search_module']]['SEARCH_URL']))
        {
            $yySetLocation .= "?" . $ModuleDefs[$_POST['qbe_search_module']]['SEARCH_URL'] . '&module=' . $_POST['qbe_search_module'] .
                '&qbe_recordid=' . $recordid . '&qbe_return_module=' . $Module;
        }
        redirectexit();
    }
    if(CheckUnapprovedContactRedirect(array('from' => $_POST['rep_approved_old'], 'to' => $_POST['rep_approved'], 'module' => $Module, 'access_level' => $Perms))
     && count($UnapprovedContactArray) > 0) // Warn for unapproved contacts
    {
        DoUnapprovedContactRedirect(array('module' => $Module, 'recordid' => $recordid, 'contact_array' => $UnapprovedContactArray));
    }
    else if ($ModuleDefs[$Module]['LINKED_MODULE'] && !isset($ModuleDefs[$Module]['SAVERECORD_POST_SAVE']))
    {
        if(!$_GET['from_parent_record'])
        {
            AddSessionMessage('INFO', _tk('record_saved'));
            $yySetLocation = getRecordURL(array('module' => $Module, 'recordid' => $recordid));
        }
        else
        {
            AddSectionMessage($ModuleDefs[$Module]['LINKED_MODULE']['panel'], 'INFO', _tk("linked_{$Module}_saved"));
 	 	 	$yySetLocation = $scripturl.'?action='.$ModuleDefs[$data['link_module']]['ACTION'].'&recordid='.$data[$ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'][$data['link_module']]].'&panel='.$ModuleDefs[$Module]['LINKED_MODULE']['panel'];
        }
        
        redirectexit();
    }
    else if ($ModuleDefs[$Module]['SAVERECORD_POST_SAVE'])
    {
        $callback = $ModuleDefs[$Module]['SAVERECORD_POST_SAVE'];
        if (is_array($callback) && method_exists($callback[0], $callback[1]))
        {
            call_user_func($callback, $recordid);
        }
        else if (is_string($callback) && function_exists($callback))
        {
            call_user_func($callback, $recordid);
        }
        else if (is_callable($callback)) // for lambdas
        {
            $callback($recordid);
        }
    }
    else
    {
        $loader = new src\framework\controller\Loader();
        $controller = $loader->getController(array('controller' => 'src\generic\controllers\SaveRecordTemplateController'));
        $controller->setRequestParameter('aParams', array(
            'module' => $Module,
            'data' => $data,
            'message' => $Output,
            'form_id' => $_REQUEST['form_id']
        ));
        echo $controller->doAction('ShowSaveRecord');
    }
    obExit();
}

/**
* Calls module-specific data validation functions (if any).
*
* @global array  $ModuleDefs
*
* @param  string $module      The module code.
*
* @return array  $error       Any generated error messages.
*/
function validateData($module)
{
    global $ModuleDefs;

    $error = ValidatePostedDates($module);

    $MoneyError = ValidatePostedMoney($module);

    if (is_array($MoneyError))
    {
        $error = array_merge($error, $MoneyError);
    }

    if ($ModuleDefs[$module]['DATA_VALIDATION_INCLUDES'])
    {
        foreach ($ModuleDefs[$module]['DATA_VALIDATION_INCLUDES'] as $includeFile)
        {
            require_once $includeFile;
        }
    }

    if ($ModuleDefs[$module]['DATA_VALIDATION_FUNCTIONS'])
    {
        foreach ($ModuleDefs[$module]['DATA_VALIDATION_FUNCTIONS'] as $function)
        {
            $newError = $function();
            if (is_array($newError))
            {
                $error = array_merge($error, $newError);
            }
        }
    }

    return $error;
}

