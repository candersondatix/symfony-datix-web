<?php
use src\security\Escaper;

function NewRecord()
{
    $Level = ($_GET['level'] ? Sanitize::SanitizeInt($_GET['level']) : 1);

    if($Level == 2)
    {
        LoggedIn();
    }
    else
    {
        if(isset($_REQUEST['form_id']))
        {
            if(FormIDExists($_REQUEST['form_id'], $_GET['module']))
            {
                $_SESSION['form_id'][$_GET['module']][$Level] = Sanitize::SanitizeInt($_REQUEST['form_id']);
            }
            else
            {
                $IncorrectFormID = true;
            }
        }
    }

    if (isset($_GET['main_recordid']) && $_GET['main_recordid'])
    {
        $data = getAssessmentTemplateData(Sanitize::SanitizeString($_GET['main_recordid']));
    }

    if($IncorrectFormID)
    {
        fatal_error("There is no form with the specified ID.");
    }
    else
    {
        $ReporterArray = SetUpDefaultReporter();
        if(!empty($ReporterArray))
        {
            $data['con']['R'][] = $ReporterArray;
        }
        ShowForm($data,'',$Level);
    }

    obExit();
}

function ShowForm($data = "", $form_action = "edit", $level = 2, $sideMenuModule = '')
{
    global $dtxtitle, $scripturl, $dtxdebug, $dtxtitle;
    global $Show_all_section, $FormType;
    global $formlevel, $FormTitle, $FormTitleDescr, $ModuleDefs, $AccessLvlDefs, $JSFunctions, $MinifierDisabled;

    $formlevel = $level;

    $Module = Sanitize::getModule($_GET['module']);
    $ModuleTable = $ModuleDefs[$Module]['TABLE'];

    if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && $form_action != 'search')
    {
        require_once 'Source/security/SecurityBase.php';
        $Perms = GetUserHighestAccessLvlForRecord($ModuleDefs[$Module]['PERM_GLOBAL'], $Module, $data['recordid']);
    }
    else
    {
        $Perms = GetParm($ModuleDefs[$Module]['PERM_GLOBAL']);
    }

    $LoggedIn = isset($_SESSION["logged_in"]);

    if (isset($ModuleDefs[$Module]['CUSTOM_DATA_TRANSFORM']))
    {
        require_once $ModuleDefs[$Module]['CUSTOM_DATA_TRANSFORM']['file'];
        $data = $ModuleDefs[$Module]['CUSTOM_DATA_TRANSFORM']['function']($data);
    }

    SetUpFormTypeAndApproval($Module, $data, $FormType, $form_action, $Perms);

    CheckForRecordLocks($Module, $data, $FormType, $sLockMessage);

    if($data['rep_approved'] == 'REJECT' && bYN(GetParm("REJECT_REASON",'Y')))
    {
        $data = \src\reasons\controllers\ReasonsController::GetReasonsData($Module, $data['recordid'], $data, $FormType);
    }

    // Use this if you don't want to specify the link as a LINKED_MODULE
    if (isset($_REQUEST['parent_module']) && $_REQUEST['parent_module'] != '' && isset($_REQUEST['linktype']) && $_REQUEST['linktype'] != '')
    {
        $linkModule = Sanitize::getModule($_REQUEST['parent_module']);
        $linkType = Sanitize::SanitizeString($_REQUEST['linktype']);
    }
    elseif ($ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'])
    {
        list($linkModule, $mainRecordid) = getParentRecordInfo($Module);
        $linkType = $ModuleDefs[$Module]['TABLE'];
    }

    // Load form settings
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => $Module, 'level' => $level, 'form_type' => $FormType, 'link_type' => $linkType, 'parent_module' => $linkModule));
    //$FormDesign->LoadFormDesignIntoGlobals();

    //These fields have special functionality, they are effectively computed fields when the related date is set.
    if ($Module == 'CLA')
    {
        if ($data['indem_dclosed'] != null)
        {
            $FormDesign->ReadOnlyFields['fin_indemnity_reserve'] = true;
        }
        if ($data['expen_dclosed'] != null)
        {
            $FormDesign->ReadOnlyFields['fin_expenses_reserve'] = true;
        }
    }

    if($LoggedIn && $_GET["action"] != "addnew")
    {
        $FormDesign->UnsetDefaultValues();
    }

    SetUpApprovalArrays($Module, $data, $CurrentApproveObj, $ApproveObj, $FormType, $FormDesign, '', $Perms);

    $FormFile = GetBasicFormFileName($Module, $level);
    include $FormFile;

    $Table = new FormTable($FormType, $Module, $FormDesign);

    //needed because some complaints field names are duplicated on subforms
    if($ModuleDefs[$Module]['IDENTIFY_TABLE'])
    {
        $Table->ffTable = $ModuleDefs[$Module]['TABLE'];
    }

    $Table->MakeForm($FormArray, $data, $Module);

    $dtxtitle = $FormDesign->FormTitle;

    $FormTitle = $FormDesign->FormTitle.GetRejectedSuffix($data, $Module, $ModuleDefs[$Module]['FIELD_NAMES']['HANDLER'], $data["rea_con_name"]);

    $title_suffix = \src\framework\controller\TemplateController::getTitleSuffix($Module, $data, !empty($FormTitle));

    if($FormType == 'Search')
    {
        $FormTitle .= _tk('search_for_records');
    }

    getPageTitleHTML(array(
        'title' => $FormTitle,
        'record_info' => $title_suffix,
        'subtitle' => $FormDesign->FormTitleDescr,
        'image' => ($FormType == 'Search' ? 'images/icons/icon_'.$Module.'_search.png' : 'images/icons/icon_'.$Module.'_new.png'),
        'module' => $Module
    ));

    $ButtonGroup = new ButtonGroup();

    $haystack = strtolower($_SERVER['HTTP_REFERER']);
    $needles = array(
        'action=incident&module=inc',
        'action=record&module=cla',
        'action=record&module=pay'
    );
    $strposa = function($haystack, $needles ) {
        foreach($needles as $needle) {
            if(false !== strpos($haystack, $needle)) {
                return true;
            }
        }
        return false;
    };
    if($strposa($haystack, $needles))
    {
        $parsedUrl = parse_url($haystack);
        parse_str($parsedUrl['query'], $query);
        $ButtonGroup->PopulateWithStandardFormButtons(array(
            'module' => $Module,
            'formtype' => $FormType,
            'data' => $data,
            'form_id' => $Module . 'form',
            'level' => $level,
            'link_module' => $linkModule,
            'main_recordid' => $mainRecordid,
            'cancel' => array('query_string' => array(
                'action' => $query['action'],
                'module' => strtoupper($query['module']),
                'fromsearch' => $query['fromsearch'],
                'recordid' => $query['recordid']
            ))
        ));
    }
    else
    {
        $ButtonGroup->PopulateWithStandardFormButtons(array(
            'module' => $Module,
            'formtype' => $FormType,
            'data' => $data,
            'form_id' => $Module . 'form',
            'level' => $level,
            'link_module' => $linkModule,
            'main_recordid' => $mainRecordid
        ));
    }
    //$ButtonGroup->AddNavigationButtons(new RecordSet($Module), $data['recordid']);
    if($FormType != 'Search' && $FormType != 'Print')
    {
        if($_GET['from_parent_record'])
        {
            $ButtonGroup->AddModuleSpecificNavigationButtons($Module, $data['recordid']);
        }
        else if($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) && !empty($_SESSION[$Module]['RECORDLIST']) && (!isset($ModuleDefs[$Module]['NO_NAV_ARROWS']) || $ModuleDefs[$Module]['NO_NAV_ARROWS'] != true))
        {
            $CurrentIndex = $_SESSION[$Module]['RECORDLIST']->getRecordIndex(array('recordid' => $data['recordid']));

            if ($CurrentIndex !== false)
            {
                $ButtonGroup->AddNavigationButtons($_SESSION[$Module]['RECORDLIST'], $data['recordid'], $Module);
            }
        }
    }

    if (!in_array($FormType, array('Search', 'Print', 'ReadOnly')) && is_array($ModuleDefs[$Module]['EXTRA_FORM_ACTIONS'])) // prevent the custom actions from being displayed unless the form is editable. This may need to be customisable in the future
    {
        foreach($ModuleDefs[$Module]['EXTRA_FORM_ACTIONS'] as $action => $btn) {
            if ($btn['condition']) {

                $ButtonGroup->AddButton(array(
                    'id'      => $btn['title'].'_btn',
                    'name'    => $btn['title'],
                    'label'   => _tk($btn['title']),
                    'onclick' => $btn['js'],
                    'action'  => $btn['action']
                ));
            }
        }
    }

    if($data['error']['Validation'])
    {
        $ErrorFields = array_keys($data['error']['Validation']);
        $ErrorField = $ErrorFields[0];
    }

    // If we are in "Print" mode, we don't want to display the menu
    if ($FormType != "Print" && $LoggedIn)
    {
        GetSideMenuHTML(array('module' => $Module, 'menumodule' => $sideMenuModule, 'table' => $Table, 'buttons'=>$ButtonGroup, 'error_field' => $ErrorField));
        template_header();
    }
    else
    {
        template_header('','','','',new Template(array('no_menu' => true)));
    }

    // TODO: this should be removed when this is refactored into a controller
    ?><script type="text/javascript">
        globals.FormType = '<?php echo $FormType; ?>';
        globals.printSections = <?php echo json_encode($Table->printSections); ?>;
    </script><?php

    // include any custom javascript libraries
    if ($ModuleDefs[$Module]['CUSTOM_JS'])
    {
        echo $ModuleDefs[$Module]['CUSTOM_JS'];
    }

    if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && ! empty($data['recordid']) && ($FormType != "Print" && $_GET['print'] != 1))
    {
        echo '<script type="text/javascript" src="src/generic/js/panelData' . ($MinifierDisabled ? '' : '.min') . '.js"></script>
';
    }

    echo '<script language="JavaScript" type="text/javascript">
    var submitClicked = false;
    var module = "' . $Module . '";
    ';

    if($FormType != "Print" && $FormType != "Search" && $FormType != "ReadOnly")
    {
        echo 'AlertAfterChange = true;
';
    }

    if ($Module == 'CLA')
    {
        if (GetParm('RESERVE_REASON_CLA', 'N') == 'O' || GetParm('RESERVE_REASON_CLA', 'N') == 'M')
        {
            ?>

            function showReasonForChangeField($target) {

                var targetId = $target.attr('id'),
                originalValue  = $target.data('originalValue'),
                currentValue = $target.val(),
                $parent = $target.closest('.field_input_div');

                if($parent.find('.reason-container').length == 0 && currentValue != originalValue) {
                    $parent.append('<div class="reason-container" style="margin: 10px 0 0;"><div style="margin: 0 0 5px;"><input type="hidden" value="1" id="show_field_reason_'+targetId+'" name="show_field_reason_'+targetId+'"><label for="reason_'+targetId+'"><?php echo (GetParm('RESERVE_REASON_CLA', 'N') == 'M') ? '<img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" />' : ''; ?>Reason for change</label><div style="display:none" id="errreason_'+targetId+'" class="field_error">You must enter a value in this field</div></div><div style="margin: 0 0 5px"><textarea id="reason_'+targetId+'" name="reason_'+targetId+'" style="width: 400px; height: 50px;"></textarea></div></div>');
                }
                else if(currentValue != originalValue) {

                    $parent.find('.reason-container').show();
                }
                else {

                    $parent.find('.reason-container').hide();
                }
            }

            function setOriginalValue($target) {

                if($target.length && ! $target.data('originalValue')) {

                    $target.data('originalValue', $target.val());
                }
            }

            jQuery(function(){

                jQuery(window).on('load', function(){

                    setOriginalValue(jQuery('#fin_indemnity_reserve'));
                    setOriginalValue(jQuery('#fin_expenses_reserve'));
                });

                jQuery('#fin_indemnity_reserve').on('keyup', function(){

                    showReasonForChangeField(jQuery(this));
                });

                jQuery('#fin_expenses_reserve').on('keyup', function(){

                    showReasonForChangeField(jQuery(this));
                });
            });

            <?php if (GetParm('RESERVE_REASON_CLA', 'N') == 'M') : ?>
            mandatoryArray.push(new Array("reason_fin_indemnity_reserve", "finance", "<?php echo GetFieldLabel('fin_indemnity_reserve', 'Indemnity incurred'); ?> <?php echo _tk('reason_for_change'); ?>"));
            mandatoryArray.push(new Array("reason_fin_expenses_reserve", "finance", "<?php echo GetFieldLabel('fin_expenses_reserve', 'Expenses incurred'); ?> <?php echo _tk('reason_for_change'); ?>"));
        <?php endif; ?>
        <?php
        }
    }

    echo '</script>
';

    if($FormType == 'New' || $FormType == 'Edit')
    {
        echo '<script type="text/javascript" src="src/generic/js/checkExisting' . ($MinifierDisabled ? '' : '.min') . '.js"></script>';

        if($Module == 'CLA' && $formlevel == 2) {

            echo '<script type="text/javascript" src="src/generic/js/showReserveChanges' . ($MinifierDisabled ? '' : '.min') . '.js"></script>';
        }
    }

    if ($FormType == "Print")
    {
        ?><script type="text/javascript" src="src/generic/js/print<?php $MinifierDisabled ? '' : '.min' ?>.js"></script><?php
    }

    if ($FormType != "Print" && $FormType != "Search")
    {
        $StatusesNoMandatory = GetStatusesNoMandatory(
            array(
                'module' => $Module,
                'level' => $Perms,
                'from' => $data['rep_approved']));

        if (is_array($StatusesNoMandatory) && !empty($StatusesNoMandatory))
        {
            $JSFunctions[] = 'StatusesNoMandatory = [\''.implode("','",$StatusesNoMandatory).'\'];';
        }

        $JSFunctions[] = MakeJavaScriptValidation($Module, $FormDesign);
    }

    echo '
<form method="post" id="'.$Module.'form" name="'.$Module.'form" enctype="multipart/form-data" action="'
        . $scripturl . '?action='
        . ($FormType == "Search" ? 'doselection' : 'saverecord')
        . ($_GET['from_parent_record'] ? '&from_parent_record=1' : '')
        . '&module='.$Module.'" onsubmit="return(submitClicked';

    if ($FormType != "Print" && $FormType != "Search" && $FormType != "ReadOnly")
    {
        echo ' && validateOnSubmit()';
    }

    echo ')">
<input type="hidden" name="recordid" id="recordid" value="' . $data['recordid'] . '" />
<input type="hidden" id="form-panel" name="panel" value="' . ($data['panel'] ?: '') . '" />
<input type="hidden" name="form_id" value="' . $FormDesign->ID . '" />
<input type="hidden" name="updateid" value="' . $data['updateid'] . '" />
<input type="hidden" name="submit_stage" value="' . $data['submit_stage'] . '" />
<input type="hidden" name="rep_approved_old" value="' . ($data['rep_approved_old'] ? $data['rep_approved_old'] : ($data['rep_approved'] ? $data['rep_approved'] : 'NEW')) . '" />
<input type="hidden" name="fromlisting" value="' . Escaper::escapeForHTMLParameter($_GET['fromlisting']) . '" />
<input type="hidden" name="qbe_recordid" value="' . Escaper::escapeForHTMLParameter($_GET['qbe_recordid']) . '" />
<input type="hidden" name="qbe_return_module" value="' . Escaper::escapeForHTMLParameter($_GET['qbe_return_module']) . '" />
<input type="hidden" name="fromsearch" value="' . Escaper::escapeForHTMLParameter($_REQUEST['fromsearch']) . '" />
<input type="hidden" name="from_report" value="' . Escaper::escapeForHTMLParameter($_GET['from_report']) . '" />
<input type="hidden" name="overdue" value="' . Escaper::escapeForHTMLParameter($_GET['overdue']) . '" />
<input type="hidden" name="fromparent" value="'.(isset($_REQUEST['link_module']) && isset($_REQUEST['main_recordid']) ? '1' : '0').'" />
';

    echo '
    <input type="hidden" name="formlevel" id="formlevel" value="'.$formlevel.'" />';

    if($formlevel == 1)
    {
        echo '
    <input type="hidden" name="holding_form" value="1" />';
    }

    if($ModuleDefs[$Module]['LINKED_MODULE'])
    {
        echo '
    <input type="hidden" name="link_module" id="link_module" value="'.Sanitize::SanitizeString($linkModule).'" />
    <input type="hidden" name="main_recordid" id="main_recordid" value="'.Sanitize::SanitizeInt($mainRecordid).'" />
    ';
    }

// we need to ensure that rep_approved is always passed, since it lets us know where to save the
// record. If it is not left to the user, we need to make sure there is a default value set.
    /*if($GLOBALS['HideFields']['rep_approved'] && $FormType != 'Search')
    {
        $value = getDefaultRepApprovedValue(array(
            'data' => $data,
            'perms' => $Perms,
            'module' => $Module
            ));

        echo '
        <input type="hidden" id="rep_approved" name="rep_approved" value="'.$value.'" />';
    } */

    if ($data['error'])
    {
        echo '<div class="error_div">Some of the information you entered is missing or incorrect.  Please correct the fields marked below and try again.</div>';
    }

    if ($data['error']['message'])
    {
        echo '<div class="error_div">'.$data['error']['message'].'</div>';
    }

    echo GetSessionMessages();

    if($sLockMessage)
    {
        echo '
        <div class="lock_message">
            <div id="LockMessage">'.$sLockMessage.'</div>
            <div id="userLockMessage"></div>
        </div>';
    }

    //Get breadcrumbs
    if ($ModuleDefs[$Module]["BREADCRUMBS"] === true && $FormType != 'Search')
    {
        require_once 'Source/generic_modules/'.$Module.'/ModuleFunctions.php';
        GetBreadCrumbs($data);
    }

    // Display the sections
    $Table->MakeTable();
    echo $Table->GetFormTable();

    echo '
        <input type="hidden" id="printAfterSubmit" name="printAfterSubmit" value="0" />
        <input type="hidden" id="rbWhat" name="rbWhat" value="Save">';

    echo $ButtonGroup->getHTML();

    echo '
</form>';

    if ($FormType != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $Table, $ErrorField);
    }

    footer();

    obExit();
}

function ListContacts($data, $con, $ReadOnly = false, $FormType)
{
    require_once 'Source/contacts/GenericContactForm.php';
    ListContactsSection(array(
        'data' => $data,
        'module' => Sanitize::getModule($_GET['module']),
        'formtype' => $FormType
    ));
}

function ShowTempLevel1Record()
{
    global $ModuleDefs;

    $FormType = 'Print';

    $Module = Sanitize::getModule($_GET['module']);
    $recordid = Sanitize::SanitizeInt($_GET['recordid']);

    $sql = "SELECT recordid, ".implode(', ',$ModuleDefs[$Module]['FIELD_ARRAY']).",
        updateid, updatedby FROM ".$ModuleDefs[$Module]['TABLE']." WHERE recordid = :recordid";

    $data = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

    $extra_data = GetExtraData($Module, $recordid);

    $data = array_merge($data, $extra_data);

    $LinkedContactFunction = ($ModuleDefs[$Module]['GET_LINKED_CONTACTS_FUNCTION'] ? $ModuleDefs[$Module]['GET_LINKED_CONTACTS_FUNCTION'] : 'GetLinkedContacts');

    $data['con'] = $LinkedContactFunction(array('recordid' => $recordid, 'module' => $Module, 'formlevel' => 1));

    $data['temp_record'] = true;

    // we can remove this once additional information fields become real fields for all modules.
    if ($Module != 'INC')
    {
        if (is_array($data['con']))
        {
            foreach ($data['con'] as $type => $links)
            {
                if ($ModuleDefs[$Module]['LEVEL1_CON_OPTIONS'][$type]['TriggerField'])
                {
                    $data[$ModuleDefs[$Module]['LEVEL1_CON_OPTIONS'][$type]['TriggerField']] = true;
                }
            }
        }
    }

    $data["notes"] = GetNotepad($recordid, $Module);

    $data["show_document"] = DatixDBQuery::PDO_fetch('SELECT COUNT(*) FROM documents_main WHERE '.$ModuleDefs[$Module]['FK'].' = '.$recordid, array(), PDO::FETCH_COLUMN) ? 'Y' : 'N';

    ShowForm($data, $form_action, 1);
}

// Displays a record from $ModuleTable
function ShowMainRecord($recordid, $message = "", $sideMenuModule = '')
{
    // Shows a record from the main table ($ModuleTable) and displays it using the PAL2 form
    global $scripturl, $ModuleDefs, $FormArray;

    if(!(!$_SESSION['logged_in'] && $_GET['submitandprint'] && HashesMatch($_GET['module'],$_GET['recordid'])))
    {
        LoggedIn();
    }
    else
    {
        ShowTempLevel1Record();
        obExit();
    }

    $Module = Sanitize::getModule($_GET['module']);

    //select from view if it exists
    $ModuleTable = ($ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE']);

    $FieldsToSelect = implode(', ',($ModuleDefs[$Module]['VIEW_FIELD_ARRAY'] ? $ModuleDefs[$Module]['VIEW_FIELD_ARRAY'] : $ModuleDefs[$Module]['FIELD_ARRAY']));

    // Override this global for Accreditation Criteria because it's a special case
    if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && $Module != 'AQU')
    {
        require_once 'Source/security/SecurityBase.php';

        //I don't think this is needed. TODO: try removing this in 12.3
        if(!$recordid)
        {
            $recordid = $_GET['recordid'];
        }
        $Perms = GetUserHighestAccessLvlForRecord($ModuleDefs[$Module]['PERM_GLOBAL'], $Module, $recordid);
    }
    else
    {
        $Perms = GetParm($ModuleDefs[$Module]['PERM_GLOBAL']);
    }


    $ShowLevel1Values = $_GET["show_level1_values"];

    $SelectString = "SELECT recordid" 
        . (("" != $FieldsToSelect && null != $FieldsToSelect) ? ", $FieldsToSelect," : ",")
        . "updateid, updatedby";

    $Where[] = "recordid = $recordid";

    $WhereClause = MakeSecurityWhereClause($Where, $Module, $_SESSION["initials"]);

    $sql = "$SelectString FROM $ModuleTable WHERE $WhereClause";

    $data = DatixDBQuery::PDO_fetch($sql);

    //---------------------------------------------------------------//
    if (isset($ModuleDefs[$Module]['CUSTOM_ACCESS_LEVEL_FUNCTION']) && !empty($ModuleDefs[$Module]['CUSTOM_ACCESS_LEVEL_FUNCTION']))
    {
        $AccessFlag = $ModuleDefs[$Module]['CUSTOM_ACCESS_LEVEL_FUNCTION']($Module, $data['recordid']);
    }
    else
    {
        // Access based on DB table link_access_approvalstatus
        $AccessFlag = GetAccessFlag($Module, $Perms, $data['rep_approved']);
    }

    if($AccessFlag == 'R')
    {
        $form_action = "ReadOnly";
    }

    if (!$data || !$Perms || ($AccessFlag == '' && $data['rep_approved'] != '') || !ModIsLicensed($Module))
    {
        CheckRecordNotFound(array('module' => $Module, 'recordid' => $recordid));
    }
    //---------------------------------------------------------------//

    $data[$ModuleDefs[$Module]['FK']] = $data['recordid'];

    if($FormType != 'Search')
    {
        $extra_data = GetExtraData($Module, $recordid);
    }

    // fetch generic linked records
    if (is_array($ModuleDefs[$Module]['LINKED_RECORDS']))
    {
        foreach ($ModuleDefs[$Module]['LINKED_RECORDS'] as $link)
        {
            if ($link['useGenericGet'])
            {
                $extra_data = array_merge($extra_data, getLinkedRecords($recordid, $link));
            }
        }
    }

    $data = array_merge($data, $extra_data);

    // Check if we need to show original values from PAL1
    if ($data && $ShowLevel1Values)
    {
        $sql = "$SelectString FROM ".$ModuleDefs[$Module]['AUDIT_TABLE']." WHERE main_recordid = :recordid and submit_stage = 2 and rep_approved = 'N'";

        $OldValues = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));
        if (!empty($OldValues))
        {
            $data["old_values"] = $OldValues;
        }
    }

    if ($ModuleDefs[$Module]['LINKED_CONTACTS'] !== false)
    {
        $LinkedContactFunction = ($ModuleDefs[$Module]['GET_LINKED_CONTACTS_FUNCTION'] ? $ModuleDefs[$Module]['GET_LINKED_CONTACTS_FUNCTION'] : 'GetLinkedContacts');
        $data['con'] = $LinkedContactFunction(array('recordid' => $recordid, 'module' => $Module, 'formlevel' => $FormLevel));
    }

    if ($ModuleDefs[$Module]['NOTEPAD'] !== false)
    {
        $data["notes"] = GetNotepad($recordid, $Module);
    }

    // Check if we need to show full audit trail
    if ($data && $_GET["full_audit"])
    {
        $FullAudit = GetFullAudit(array('Module' => $Module, 'recordid' => $recordid));

        if ($FullAudit)
        {
            $data["full_audit"] = $FullAudit;
        }
    }

    if($data['rep_approved'])
    {
        $FormLevel = GetFormLevel($Module, $Perms, $data['rep_approved']);
    }
    else
    {
        $FormLevel = 2;
    }

    if($FormLevel == 1)
    {
        //need to populate show_xxx fields
        $data = PopulateLevel1FormFields(array('module' => $Module, 'data' => $data));
    }

    AuditOpenRecord($Module, $recordid, "");

    ShowForm($data, $form_action, $FormLevel, $sideMenuModule);

    obExit();
}

function RecordPermissionsGeneric($data, $formAction, $module)
{
    global $ModuleDefs;

    require_once 'Source/security/SecurityRecordPerms.php';
    MakeRecordSecPanel(
        $module,
        $ModuleDefs[$module]['TABLE'],
        $data['recordid'],
        $formAction
    );
}

/**
 * Generic function for retrieving linked record data.
 *
 * @param  int      $recordid                      The ID of the main record.
 * @param  string   $link['type']                  The type of linked record.
 * @param  string   $link['table']                 The name of the link table.
 * @param  string   $link['recordid_field']        The field name for the linked record's unique id.
 * @param  array    $link['basic_form']['Rows']    The fields for this linked record.
 * @param  string   $link['main_recordid_label']   The field name for the main record's unique id.
 * @param  boolean  $link['save_listorder']        Whether the ordering of the linked records has been saved.
 *
 * @return array    $data                          The linked record data.
 */
function getLinkedRecords($recordid, $link)
{
    $suffix = 1;
    $data = array();
    $orderBy = $link['save_listorder'] ? 'listorder' : $link['recordid_field'] ;

    $sql = 'SELECT ' . implode(', ', $link['basic_form']['Rows']) . '
            FROM ' . $link['table'] . '
            WHERE ' . $link['main_recordid_label'] . ' = :recordid
            ORDER BY ' . $orderBy;

    $LinkedRecords = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $recordid));

    foreach ($LinkedRecords as $row)
    {
        foreach ($link['basic_form']['Rows'] as $field)
        {
            $data[$field . '_' . $suffix] = $row[$field];
        }
        $suffix++;
    }

    if ($suffix > 1)
    {
        $data[$link['type'] . '_max_suffix'] = $suffix;
    }

    return $data;
}

/**
 * Retrieves information (module and id) about the parent record of linked records. Need to use $_REQUEST, because we might be returning to the page after an error with the info in the $_POST rather than the $_GET
 *
 * @global array  $ModuleDefs
 *
 * @param  string $module     The link module code.
 *
 * @return array  $values     An array where the first element is the parent module and the second element is the parent record id.
 */
function getParentRecordInfo($module)
{
    global $ModuleDefs;

    $values = array(null, null);
    if (isset($_REQUEST['link_module']) && isset($_REQUEST['main_recordid']))
    {
        // link info has been passed on the query string
        $values = array($_REQUEST['link_module'], $_REQUEST['main_recordid']);
    }
    elseif (isset($_REQUEST['recordid']))
    {
        // need to fetch link info from DB
        $sql = 'SELECT
               ' . implode(',', $ModuleDefs[$module]['LINKED_MODULE']['parent_ids']) . '
                FROM
               ' . $ModuleDefs[$module]['TABLE'] . '
                WHERE
                    recordid = :recordid';
        $PDOParams = array('recordid' => $_REQUEST['recordid']);
        $mainIds = DatixDBQuery::PDO_fetch($sql, $PDOParams);

        foreach ($mainIds as $id => $value)
        {
            if ($value > 0)
            {
                $values = array(array_search($id, $ModuleDefs[$module]['LINKED_MODULE']['parent_ids']), $value);
                break;
            }
        }
    }

    return $values;
}
