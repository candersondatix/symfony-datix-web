<?php
/**
* Handle Causal Factors related sections.
*
*/

/**
* Check if there are any Causal factors for a record and set inc_causal_factors_linked to Y/N
*
* @param int $recordid Recordid of module record
* @param string $module Module short name
* @return mixed array
*/
function GetYNCausalFactors($recordid, $module)
{
    global $ModuleDefs;
    
    $ModKey = $ModuleDefs[$module]['FK'];
    
    $sql =
        "SELECT count(*) as NUM_CAUSAL_FACTORS_LINKED
        FROM causal_factors
        WHERE causal_factors." . $ModKey . " = :recordid";

    $row = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

    if($row['NUM_CAUSAL_FACTORS_LINKED'] == 0)
    {
        return array(\UnicodeString::strtolower($module) . '_causal_factors_linked' => 'N');
    }
    else
    {
        return array(\UnicodeString::strtolower($module) . '_causal_factors_linked' => 'Y');
    }
}

?>