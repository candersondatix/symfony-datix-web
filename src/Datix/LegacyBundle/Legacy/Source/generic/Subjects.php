<?php

function SavePrimarySubject($aParams)
{
    global $ModuleDefs;

    $sql = 'SELECT TOP 1 '.implode(', ',$aParams['basic_form']['Rows']).' from '.$aParams['table'].' WHERE '.$aParams['main_recordid_label'].' = '.$aParams['main_recordid'].' ORDER BY listorder ASC, '.$aParams['recordid_field'].' ASC';

    $row = DatixDBQuery::PDO_fetch($sql);

    if(is_array($ModuleDefs[$aParams['module']]['PRIMARY_SUBJECT_MAPPINGS']))
    {
        foreach($aParams['basic_form']['Rows'] as $Field)
        {
            if($ModuleDefs[$aParams['module']]['PRIMARY_SUBJECT_MAPPINGS'][$Field])
            {
                $NewFields[] = $ModuleDefs[$aParams['module']]['PRIMARY_SUBJECT_MAPPINGS'][$Field];
                $NewValues[$ModuleDefs[$aParams['module']]['PRIMARY_SUBJECT_MAPPINGS'][$Field]] = EscapeQuotes($row[$Field], true);
            }
        }
    }

    $sql = 'UPDATE '.$ModuleDefs[$aParams['module']]['TABLE'].' SET '.

    GenerateSQLFromArrays(array(
        'FieldArray' => $NewFields,
        'DataArray' => $NewValues,
        'Module' => $aParams['module']
        )).

        ' WHERE recordid = '.$aParams['main_recordid'];

    DatixDBQuery::PDO_query($sql);
}

function GetSubjectSectionHTML($aParams)
{
    global $JSFunctions, $formlevel;

    if($formlevel == null)
    {
        $formlevel = ($_POST['form_level'] ?: 1);
    }
    $SavedFormDesignSettings = saveFormDesignSettings();
    unsetFormDesignSettings();

    $AJAX = $aParams['ajax'];

    if($aParams['formtype'] == 'Search')
    {
        $aParams['suffix'] = null;
    }

    if($AJAX)
    {
        $formlevel = $aParams['level'];
    }

    $aSubjectRows = getBasicSubjectForm($aParams);

    $subjectsFormDesign = Forms_FormDesign::GetFormDesign(array('module' => $aParams['module'], 'level' => $formlevel, 'form_type' => $aParams['formtype']));
    $subjectsFormDesign = ModifyFormDesignForSubjects($subjectsFormDesign);

    if ($aParams['suffix'])
    {
        $subjectsFormDesign->AddSuffixToFormDesign($aParams['suffix']);
        $aParams['data'] = AddSuffixToData($aParams['data'], $aParams['suffix'], getSubjectFieldsForSave(array('module' => $aParams['module'])));
    }

    $oSubjectTable = new FormTable($aParams['formtype'], $aParams['module'], $subjectsFormDesign);
    $oSubjectTable->ffTable = 'COMSUB';
    $oSubjectTable->MakeForm($aSubjectRows, $aParams['data'], $aParams['module'], array('dynamic_section' => true));

    $html = '';

    if(!($AJAX) && $aParams['formtype'] != 'Print')
    {
        $html .= '<div id="'.$aParams['subject_name'].'_section_div_'.$aParams['suffix'].'">';
    }

    $html .= $oSubjectTable->GetFormTable();
    $html .= '<input type="hidden" name="' . $aParams['subject_name'] . '_link_id_' . $aParams['suffix'] .'" id="' . $aParams['subject_name'] . '_link_id_' . $aParams['suffix'] .'" value="' . $aParams['data']['subject_id'] .'" />';

    if($aParams['formtype'] != 'Print' && $aParams['formtype'] != 'ReadOnly' && $aParams['formtype'] != 'Search')
    {
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        $html .= '
            <div id="copy_subject_button_div">
                <ol>
                    <li class="new_windowbg">
                        <input type="button" id="copy_subject_section" value="Copy Subject" onclick="CopySectionToForm(\''.$aParams['subject_name'].'\',\'\',$(\''.$aParams['subject_name'].'_section_div_'.$aParams['suffix'].'\'),\''.$aParams['module'].'\','.$aParams['suffix'].', true, '.$spellChecker.', '.(is_numeric($_REQUEST['form_id']) ? Sanitize::SanitizeInt($_REQUEST['form_id']) : 'null').');">
                    </li>
                </ol>
            </div>';
    }

    if(!($AJAX) && $aParams['formtype'] != 'Print')
    {
        $html .= '</div>';
    }

    loadFormDesignSettings($SavedFormDesignSettings);

    return $html;
}

function getSubjectFieldsForSave($aParams)
{
    $RowList = GetSubjectRowList($aParams);
    $RowList[] = 'listorder';

    return array('Rows' => $RowList);
}

function GetSubjectRowList($aParams)
{
    global $ModuleDefs;

    $RowList = $ModuleDefs[$aParams['module']]['LINKED_RECORDS'][$ModuleDefs[$aParams['module']]['SUBJECT_TYPE']]['basic_form']['Rows'];

    return $RowList;
}

function getBasicSubjectForm($aParams)
{
    global $ModuleDefs;

    $aParams['subject_name'] = $ModuleDefs[$aParams['module']]['SUBJECT_TYPE'];
    $RowList = GetSubjectRowList($aParams);

    if($aParams['rows'])
    {
        return array('Rows' => $RowList);
    }
    else
    {
        return array(
            "Parameters" => array("Suffix" => $aParams['suffix']),
            "subject".($aParams['suffix'] ? '_'.$aParams['suffix'] : '') => array(
                "Title" => 'Subject',
                "OrderField" => array('id'=>$aParams['subject_name'].'_listorder_'.$aParams['suffix'], 'value'=>$aParams['data']['listorder']),
                "ClearSectionOption" => $aParams['clearsection'],
                "DeleteSectionOption" => !$aParams['clearsection'],
                'MandatorySection' => 'subject',
                "ContactSuffix" => $aParams['suffix'],
                "DynamicSectionType" => $aParams['subject_name'],
                "Rows" => $RowList,
                'ContainedIn' => 'subject',
                'FieldFormatsTable' => $aParams['module'] . 'SUB'
                ));
    }

}

/**
* @desc Called from {@link GetExtraData()} for Complaints module. Populates com_subjects_linked field with correct data based
* on whether there are subjects linked or not. May be replaced in future when com_subjects_linked becomes a proper db field.
*
* @param int $recordid The recordid of the current record.
*
* @return array Array containing data value for com_subjects_linked.
*/
function GetYNCOMSubjects($recordid)
{
    $sql =
        'SELECT COMPL_MAIN.RECORDID, count(*) as NUM_SUBJECTS_LINKED
        FROM COMPL_MAIN
        LEFT JOIN
        VW_COMPL_SUBJ_WEB
        ON COMPL_MAIN.RECORDID = VW_COMPL_SUBJ_WEB.COM_ID
        WHERE COMPL_MAIN.RECORDID = :recordid
        AND VW_COMPL_SUBJ_WEB.RECORDID IS NOT NULL
        GROUP BY COMPL_MAIN.RECORDID';

    $row = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

    if(!isset($row['NUM_SUBJECTS_LINKED']))
    {
        $SubjRow = array('com_subjects_linked' => 'N');
    }
    else
    {
        $SubjRow = array('com_subjects_linked' => 'Y');
    }

    return $SubjRow;
}

function ModifyFormDesignForSubjects(Forms_FormDesign $design)
{
    if (is_array($design->ExpandSections))
    {
        $NewArray = array();
        foreach ($design->ExpandSections as $FieldName => $SectionArray)
        {
            foreach ($SectionArray as $SectionID => $Details)
            {
                if ($Details['section'] != 'subject')
                {
                    $NewArray[$FieldName][] = $Details;
                }
            }
        }

        $design->ExpandSections = $NewArray;
    }

    // remove extra sections/extra fields
    unset($design->ExtraSections);
    unset($design->ExtraFields);

    return $design;
}
