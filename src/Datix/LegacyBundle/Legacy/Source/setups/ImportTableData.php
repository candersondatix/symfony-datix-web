<?php

/**
 * Imports the data from the uploaded load file.
 */
function importTableData()
{
    if (isset($_GET['ccs2']) && $_GET['ccs2'] != '')
    {
        $_SESSION['ccs2_verified'] = Sanitize::SanitizeString($_GET['ccs2']);
    }
    else
    {
        unset($_SESSION['ccs2_verified']);
    }
    
    try 
    {
        $importer = new Import_LoadFile(new Files_CSVLoadFile($_FILES['importfile']['tmp_name']), new DatixDBQuery(''));
        $importer->importData();               
    }
    catch (FileNotFoundException $e)
    {
        importTableDataExceptionHandler($e->getMessage());           
    }
    catch (FileHeaderNotFoundException $e)
    {
        importTableDataExceptionHandler($e->getMessage());            
    }
    catch (DatixDBQueryException $e)
    {
        importTableDataExceptionHandler(_tk('load_file_error').$e->getMessage());
    }
    catch (Exception $e)
    {
        if ($e->getMessage() == 'CCS2')
        {
            CCS2ExceptionHandler($e->getMessage());
        }
        else
        {
            importTableDataExceptionHandler($e->getMessage());
        }
    }    
    
    importExtraTransformAfterLoad();
}

/**
* Handles exceptions that may be thrown when importing table data.
* 
* @param string $error
*/
function importTableDataExceptionHandler($error)
{
    echo $error;
    obExit();    
}

function CCS2ExceptionHandler($error)
{
    echo $error;
    obExit();
}

function importExtraTransformAfterLoad()
{
    if (bYN(GetParm('RISK_MATRIX', 'N', true)))
    {
        $sql = "UPDATE field_formats SET fmt_code_table = 'code_inc_conseq' 
                WHERE fmt_field IN ('ram_consequence', 'ram_after_conseq', 'ram_cur_conseq')";
        
        DatixDBQuery::PDO_query($sql);
        
        $sql = "UPDATE field_directory SET fdr_code_table = 'code_inc_conseq' 
                WHERE fdr_name IN ('ram_consequence', 'ram_after_conseq', 'ram_cur_conseq')";
        
        DatixDBQuery::PDO_query($sql); 
             
        $sql = "UPDATE field_formats SET fmt_code_table = 'code_inc_likeli' 
                WHERE fmt_field IN ('ram_likelihood', 'ram_after_likeli', 'ram_cur_likeli')";
        
        DatixDBQuery::PDO_query($sql);
        
        $sql = "UPDATE field_directory SET fdr_code_table = 'code_inc_likeli' 
                WHERE fdr_name IN ('ram_likelihood', 'ram_after_likeli', 'ram_cur_likeli')";
        
        DatixDBQuery::PDO_query($sql);
    }
    else
    {
        $sql = "UPDATE field_formats SET fmt_code_table = 'code_ra_conseq' 
                WHERE fmt_field IN ('ram_consequence', 'ram_after_conseq', 'ram_cur_conseq')";
        
        DatixDBQuery::PDO_query($sql);
        
        $sql = "UPDATE field_directory SET fdr_code_table = 'code_ra_conseq' 
                WHERE fdr_name IN ('ram_consequence', 'ram_after_conseq', 'ram_cur_conseq')";
        
        DatixDBQuery::PDO_query($sql); 
             
        $sql = "UPDATE field_formats SET fmt_code_table = 'code_ra_likeli' 
                WHERE fmt_field IN ('ram_likelihood', 'ram_after_likeli', 'ram_cur_likeli')";
        
        DatixDBQuery::PDO_query($sql);
        
        $sql = "UPDATE field_directory SET fdr_code_table = 'code_ra_likeli' 
                WHERE fdr_name IN ('ram_likelihood', 'ram_after_likeli', 'ram_cur_likeli')";
        
        DatixDBQuery::PDO_query($sql); 
    }
}

?>