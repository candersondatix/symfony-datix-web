<?php

/**
* Auto-populates the risk matrix mappings table with all possible cominations of consequence/likelihood
*/
function generateRiskMatrix()
{
    $consequences = DatixDBQuery::PDO_fetch_all('SELECT code FROM code_inc_conseq', array(), PDO::FETCH_COLUMN);
    $likelihoods = DatixDBQuery::PDO_fetch_all('SELECT code FROM code_inc_likeli', array(), PDO::FETCH_COLUMN);

    foreach ($consequences as $consequence)
    {
        foreach ($likelihoods as $likelihood)
        {
            if (DatixDBQuery::PDO_fetch('SELECT COUNT(*) AS count FROM ratings_map WHERE consequence = :consequence AND likelihood = :likelihood', array('consequence' => $consequence, 'likelihood' => $likelihood), PDO::FETCH_COLUMN) == 0)
            {
                // insert combination if it doesn't already exist
                DatixDBQuery::PDO_query('INSERT INTO ratings_map (consequence, likelihood) VALUES (:consequence, :likelihood)', array('consequence' => $consequence, 'likelihood' => $likelihood));
            }
        }
    }
}