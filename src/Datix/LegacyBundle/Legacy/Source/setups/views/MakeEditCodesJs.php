<?php // prints out inline JS for the Rico LiveGrid ?>
<?php if (is_array($Parents)): ?>
	<?php foreach ($Parents as $i => $parentfieldname): ?>
		<script type="text/javascript">
			function CreateCodeList_<?php echo $parentfieldname ?>(ControlIndex, normal) {
				GlobalCodeSelectControls[ControlIndex].create({
					module: '<?php echo $module ?>',
					field: '<?php echo $parentfieldname ?>',
					fieldname: '<?php echo $code_table. "_" . $i ?>',
					multilistbox: true,
					table: '<?php echo $fmt_table ?>',
					returnCodeOnly: true
					<?php if ($codefieldname == 'inc_clintype' && $parentfieldname == 'inc_clin_detail'): ?>,showNumbers: true<?php endif ?>
				}); 
			}
		</script>
	<?php endforeach ?>
<?php endif ?>
<?php foreach ($FieldSetupMappings->SetupMappings as $index => $fsmap): ?>
    <?php $mapping_index = $i + 4 + $index; ?>
    <?php if (in_array($fsmap['fsm_setup_col_type'], array("MS","MSE"))): ?>
	<?php
		$select_sql = $fsmap['fsm_setup_col_select_sql'];   //'select cod_code as value ,cod_descr as description from code_npsa_types';
        $select_sql = str_replace("'", "\'", $select_sql);
        $select_sql = str_replace("+", "%2B", $select_sql);
	?>
		<script type="text/javascript">
			function CreateCodeList_<?php echo $fsmap['fsm_setup_col'] ?>(ControlIndex, normal) {
                GlobalCodeSelectControls[ControlIndex].create({
                    field: '<?php echo $fsmap['fsm_setup_col'] ?>',
                    fieldname: '<?php echo $code_table . "_" . $mapping_index ?>',
                    select_sql: '<?php echo $select_sql ?>',
                    overrideTitle: '<?php echo $fsmap['fsm_setup_col_title'] ?>', 
                    allow_dups: true,     
                    multilistbox: true,
                    returnCodeOnly: true
                    <?php if ($codefieldname == 'inc_clintype' && $fsmap['fsm_setup_col'] == 'cod_npsa'): ?>,showNumbers: true<?php endif ?>
                    }); 
			}
		</script>
    <?php endif ?>
<?php endforeach ?>