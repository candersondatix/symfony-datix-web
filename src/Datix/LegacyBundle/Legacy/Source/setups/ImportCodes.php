<?php

/**
 * @desc Function to get the Accreditation module that a field belongs to
 *
 * @param array $FieldDefsExtra Array with fields definition for every modules
 * @param string $FieldName Name of the field
 * @return string $AccreditationModule The accreditation module to which $FieldName belongs to or the accreditation
 * module by default
 */
function getAccreditationModule($FieldDefsExtra, $FieldName)
{
    $AccreditationModules = array('ATM', 'ATQ', 'ATI', 'AMO', 'AQU');

    foreach ($AccreditationModules as $AccreditationModule)
    {
        if (!empty($FieldDefsExtra[$AccreditationModule][$FieldName]))
        {
            return $AccreditationModule;
        }
    }

    return 'ACR';
}

?>
