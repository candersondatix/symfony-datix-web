<?php
function ShowComboLinkForm()
{
    global $scripturl;

    if (!$_SESSION["AdminUser"])
    {
        $yySetLocation = $scripturl;
        redirectexit();
    }

    getPageTitleHTML(array(
         'title' => _tk('combo_linking'),
         'module' => 'ADM'
         ));

    GetSideMenuHTML(array('module' => 'ADM'));

    template_header();

    $FormArray = array (
          "Parameters" => array("Panels" => "True", "Condition" => false),
          "name" => array("Title" => _tk('combo_linking'),
                "Function" => "MakeEditComboLinks",
                "Rows" => array()),
    );

    $ComboLinksTable = new FormTable();
    $ComboLinksTable->MakeForm($FormArray, $data, $module);

    $ComboLinksTable->MakeTable();
    echo $ComboLinksTable->GetFormTable();

    obExit();
}


function DefineFields()
{
    global $oForm, $oDB, $ModuleDefs;

    list($module, $table) = getSubFormInfo();

    $oForm->AddSort('cmb_child', 'ASC');
    $oForm->AddSort('recordid', 'ASC');
    $oForm->ConfirmDeleteColumn();
    $oForm->overrideKeys['RECORDID'] = true;
    $oForm->options["FilterLocation"] = -1;
    $oForm->options["onSubmit"] = 'validate()';

    $oForm->AddEntryField('recordid', '', 'H', GetNextRecordID('combo_links'));
    $oForm->AddEntryField("cmb_module", "cmb_module", "H", $module);
    $oForm->AddEntryField("cmb_table", "cmb_table", "H", $table);
    $oForm->AddEntryField("cmb_form_id", "cmb_form_id", "H", $_GET['subform']);

    $oForm->AddEntryFieldW('cmb_child', 'Child Field', 'S', '', 200);
    $oForm->CurrentField['required'] = true;

    $CCS2Clause = ((isset($ModuleDefs[$module]['CCS2_FIELDS']) && sizeof($ModuleDefs[$module]['CCS2_FIELDS']) > 0) ? 'AND field_formats.fmt_field NOT IN (\'' . implode("', '", $ModuleDefs[$module]['CCS2_FIELDS']) . '\')' : '');

    if (IsCentrallyAdminSys())
    {
        if (IsCentralAdmin())
        {
            $ChildFieldSql = 'SELECT fmt_field, fmt_title FROM field_formats WHERE fmt_module = \''.$module.'\' AND fmt_table = \''.$table.'\' AND fmt_data_type = \'C\' '.$ModuleDefs[$module]['COMBO_LINK_WHERE'].' ' . $CCS2Clause . ' ORDER BY fmt_title';
        }
        else
        {
            $ChildFieldSql = 'SELECT fmt_field, fmt_title FROM field_formats LEFT OUTER JOIN combo_links ON field_formats.fmt_field = combo_links.cmb_child WHERE fmt_module = \''.$module.'\' AND fmt_table = \''.$table.'\' AND fmt_data_type = \'C\' '.$ModuleDefs[$module]['COMBO_LINK_WHERE'].' AND (combo_links.cmb_locked != \'Y\' OR combo_links.cmb_locked IS NULL) ' . $CCS2Clause . ' ORDER BY fmt_title';
        }
    }
    else
    {
        $ChildFieldSql = 'SELECT fmt_field, fmt_title FROM field_formats WHERE fmt_module = \''.$module.'\' AND fmt_table = \''.$table.'\' AND fmt_data_type = \'C\' '.$ModuleDefs[$module]['COMBO_LINK_WHERE'].' ' . $CCS2Clause . ' ORDER BY fmt_title';
    }

    $oForm->CurrentField['SelectSql'] = $ChildFieldSql;
    $oForm->CurrentField["filterUI"] = 't';

    $oForm->AddCalculatedField('SELECT fmt_title FROM field_formats f WHERE t.cmb_child = f.fmt_field AND t.cmb_table = f.fmt_table', 'Child Field Title');
    $oForm->CurrentField["width"] = 200;
    $oForm->CurrentField["filterUI"] = 't';

    $oForm->AddEntryFieldW('cmb_parent', 'Parent Field', 'S', '', 200);
    $oForm->CurrentField['required'] = true;

    if (IsCentrallyAdminSys())
    {
        if (IsCentralAdmin())
        {
            $ParentFieldSql = 'SELECT fmt_field, fmt_title FROM field_formats WHERE fmt_module = \''.$module.'\' AND fmt_table = \''.$table.'\' AND fmt_data_type = \'C\' '.$ModuleDefs[$module]['COMBO_LINK_WHERE'].' ORDER BY fmt_title';
        }
        else
        {
            $ParentFieldSql = 'SELECT fmt_field, fmt_title FROM field_formats LEFT OUTER JOIN combo_links ON field_formats.fmt_field = combo_links.cmb_parent WHERE fmt_module = \''.$module.'\' AND fmt_table = \''.$table.'\' AND fmt_data_type = \'C\' '.$ModuleDefs[$module]['COMBO_LINK_WHERE'].' AND (combo_links.cmb_locked != \'Y\' OR combo_links.cmb_locked IS NULL) ORDER BY fmt_title';
        }
    }
    else
    {
        $ParentFieldSql = 'SELECT fmt_field, fmt_title FROM field_formats WHERE fmt_module = \''.$module.'\' AND fmt_table = \''.$table.'\' AND fmt_data_type = \'C\' '.$ModuleDefs[$module]['COMBO_LINK_WHERE'].' ORDER BY fmt_title';
    }

    $oForm->CurrentField['SelectSql'] = $ParentFieldSql;
    $oForm->CurrentField["filterUI"] = 't';

    $oForm->AddCalculatedField('SELECT fmt_title FROM field_formats f WHERE t.cmb_parent = f.fmt_field AND t.cmb_table = f.fmt_table', 'Parent Field Title');
    $oForm->CurrentField["width"] = 200;
    $oForm->CurrentField["filterUI"] = 't';

    if (IsCentrallyAdminSys())
    {
        $oForm->AddEntryFieldW('CMB_LOCKED', 'Locked?', 'S', '', 50);
        $oForm->CurrentField['SelectValues'] = 'Y,N';

        if (!IsCentralAdmin())
        {
            $oForm->CurrentField['ReadOnly'] = true;
        }
    }

    $oForm->DisplayPage();
}

/**
* Retrieves module and table information based on the subform ID when we're inserting new combo links.
*
* @return array The module/table info.
*/
function getSubFormInfo()
{
    if ($_GET['subform'])
    {
        list($module, $table) = DatixDBQuery::PDO_fetch('SELECT sfm_module, sfm_form FROM subforms WHERE sfm_form_id = :subform', array('subform' => $_GET['subform']), PDO::FETCH_NUM);
    }
    else
    {
        $module = '';
        $table = '';
    }
    return array($module, $table);
}

function IsComboLinkLocked()
{
    if ($_GET['recordid'])
    {
        $recordId = Sanitize::SanitizeInt($_GET['recordid']);

        if (IsCentrallyAdminSys())
        {
            if (IsCentralAdmin())
            {
                echo 'N';
            }
            else
            {
                $sql = '
                    SELECT
                        cmb_locked
                    FROM
                        combo_links
                    WHERE
                        recordid = :recordid
                ';

                $result = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordId), PDO::FETCH_COLUMN);

                switch ($result)
                {
                    case 'Y':
                    case 'N':
                        echo $result;
                        break;
                    default:
                        echo 'N';
                        break;
                }
            }
        }
        else
        {
            echo 'N';
        }
    }
    else
    {
        echo 'N';
    }

    obExit();
}
?>