<?php

function ExportProfile()
{
    $Profile = new ImportProfile(new DatixDBQuery(''), $_GET['recordid']);
    $Profile->OutputAsXML();
}

function DoImportProfile()
{
    global $InterfaceErrorMsg;

    if (!$_FILES['userfile']['name'])
    {
        AddSessionMessage('ERROR', 'Please choose a file to import');
        $loader = new src\framework\controller\Loader();
        $controller = $loader->getController(array(
            'controller' => 'src\excelimport\controllers\ImportImportProfileTemplateController',
        ));
        echo $controller->doAction('importimportprofile');
        obExit();
    }

    include_once 'soap/interface.php';

    if (!empty($_FILES["userfile"]["tmp_name"]))
    {
        $RawXMLData = file_get_contents ( $_FILES["userfile"]["tmp_name"]);

        $RawXMLData = '<?xml version="1.0" encoding="UTF-8"?><DATIXDATA>'.str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $RawXMLData).'</DATIXDATA>';
    }
    else
    {
        $ImportResult === false;
        AddSessionMessage('ERROR', 'No import file selected');
        $loader = new src\framework\controller\Loader();
        $controller = $loader->getController(array(
            'controller' => 'src\excelimport\controllers\ImportImportProfileTemplateController',
        ));
        echo $controller->doAction('importimportprofile');
        obExit();
    }

    $XSDFile = "soap/insertrecord.xsd";
    $ImportResult =  DoImportXMLdata($RawXMLData, $XSDFile);

    if ($ImportResult === true)
    {
       AddSessionMessage('INFO', 'Import successful.');
    }
    else
    {
        $ErrorMessage = 'Could not import XML data. ';

        if (is_string($InterfaceErrorMsg))
        {
           $ErrorMessage .= $InterfaceErrorMsg;
        }
        if (is_string($InterfaceErrorMsg->message))
        {
           $ErrorMessage .= $InterfaceErrorMsg->message;
        }

        AddSessionMessage('ERROR', $ErrorMessage);
    }

    $loader = new src\framework\controller\Loader();
    $controller = $loader->getController(array(
        'controller' => 'src\excelimport\controllers\ImportImportProfileTemplateController',
    ));
    echo $controller->doAction('importimportprofile');
    obExit();
}

function ColumnOptionsSection($Data, $FormType, $Module)
{
    echo '<li>';

    require_once "rico/applib.php";
    require_once "rico/plugins/php/ricoLiveGridForms.php";

    if (OpenGridForm("", 'excel_import', 'exc_profile = '.$Data['exp_profile_id'], 'rw', 'main'))
    {
        DefineFields($Data);
    }
    else
    {
      echo 'open failed';
    }

    CloseApp();

    echo '</li>';
}

function DefineFields($Data)
{
    global $oForm, $oDB, $JSFunctions;

    $oForm->options['visibleRows'] = 8;
    $oForm->overrideKeys['RECORDID'] = true;

    //for some reason if this is "hidden", the save function starts changing the recordids on save.
    $oForm->AddEntryField("recordid", 'ID', "RC", '');
    $oForm->CurrentField['recordid_table'] = 'excel_import';
    $oForm->CurrentField['ReadOnly'] = true;
    $oForm->CurrentField["visible"] = false;
    $oForm->CurrentField["ForceHide"] = true;

    $oForm->AddEntryField("exc_profile", 'Profile ID', "H", $Data['exp_profile_id']);
    $oForm->AddEntryField("exc_form", 'Table', "H", $Data['exp_profile_form']);

    if ($Data['exp_profile_type'] == 'CSV')
    {
        $oForm->AddEntryFieldW("exc_col_letter", 'Column Number', "B", "",100);
    }
    else
    {
        $oForm->AddEntryFieldW("exc_col_letter", 'Excel Column', "B", "",100);
    }

    $oForm->CurrentField["required"] = true;

    $oForm->AddCalculatedField('SELECT fdr_label FROM field_directory f WHERE fdr_table = t.exc_form AND t.exc_dest_field = f.fdr_name', 'Field Title');
    $oForm->CurrentField["width"] = 150;
    $oForm->CurrentField["filterUI"] = 't';

    $oForm->AddEntryFieldW("exc_dest_field", 'Destination Field', "S", "",150);
    $oForm->CurrentField["required"] = true;
    $oForm->CurrentField['SelectSql'] = 'SELECT fdr_name, fdr_label FROM field_directory WHERE fdr_table = \''.$Data['exp_profile_form'].'\' AND fdr_name NOT IN (\'fullname\', \'fullname2\', \'con_browse_name\', \'con_browse_order\', \'sta_lastloigin_dt\', \'sta_lastlogin_time\') ORDER BY fdr_label';

    $oForm->AddEntryFieldW("exc_id_field", 'Key Field', "S", "",80);
    $oForm->CurrentField["SelectValues"]='Y,N';

    $oForm->AddEntryFieldW("exc_import_order", 'Order', "T", "",70);

    $oForm->AddEntryFieldW("exc_ignore_nulls", 'Ignore Nulls?', "S", "",90);
    $oForm->CurrentField["SelectValues"]='Y,N';

    $oForm->AddSort("exc_import_order", "ASC");

    if ($Data['exp_profile_type'] == 'CSV')
    {
        $oForm->AddSort("CAST(exc_col_letter AS int)", "ASC");
    }
    else
    {
        $oForm->AddSort("exc_col_letter", "ASC");
    }

    $oForm->ExtraMenuItems[] = array('Label' => 'Transform', 'Action' => 'ShowTransform', 'Enabled' => true);

    $oForm->DisplayPage();
}

function ColumnMappingSection($Data, $FormType, $Module)
{
    echo '<li>';

    $DBColumns = DatixDBQuery::PDO_fetch_all('SELECT recordid, exc_dest_field FROM excel_import WHERE exc_profile = :profile', array('profile' => $Data['exp_profile_id']));

    require_once "rico/applib.php";
    require_once "rico/plugins/php/ricoLiveGridForms.php";

    foreach ($DBColumns as $DBData)
    {
        $DBData = array_merge($DBData,$Data);

        try
        {
            $FieldDef = new Fields_Field($DBData['exc_dest_field']);
        }
        catch (Exception $e)
        {
            continue;
        }

        $recordid = $DBData['recordid'];

        echo '<div id="mapping-'.$recordid.'" class="mapping-div">';

        if (in_array($FieldDef->getFieldType(), array('L', 'S', 'C')))
        {
            if (OpenGridForm("", 'excel_data_trans', 'edt_import_field_id = '.$recordid, 'rw', $recordid))
            {
                DefineMappingFields($DBData, $recordid);
            }
            else
            {
              echo 'open failed';
            }

            CloseApp();

        }
        else
        {
            echo '<div class="padded_div">Transformations are not permitted for this field type.</div>';
        }

        echo '</div>';
    }

    echo '</li>';
}

function DefineMappingFields($Data, $field_id)
{
    global $oForm, $oDB, $JSFunctions, $mappingGridIDs;

    $oForm->options['visibleRows'] = 8;
    $oForm->overrideKeys['RECORDID'] = true;

    $oForm->AddEntryField("recordid", 'ID', "RC", '');
    $oForm->CurrentField['recordid_table'] = 'excel_data_trans';
    $oForm->CurrentField['ReadOnly'] = true;
    $oForm->CurrentField["visible"] = false;
    $oForm->CurrentField["ForceHide"] = true;

    $oForm->AddEntryField("edt_profile_id", 'Profile ID', "H", $Data['exp_profile_id']);
    $oForm->CurrentField["required"] = true;

    $oForm->AddEntryField("edt_import_field_id", 'Field ID', "H", $field_id);
    $oForm->CurrentField["required"] = true;

    $oForm->AddEntryFieldW("edt_old_string_val", 'Excel/CSV Value', "T", "",100);
    $oForm->CurrentField["required"] = true;

    $oForm->AddEntryFieldW("edt_new_string_val", 'Datix Value', "T", "",100);
    $oForm->CurrentField["required"] = true;

    $oForm->AddSort("recordid", "ASC");

    //we need to store the id of this element so that we can resize it when we later show it.
    $mappingGridIDs[] = $oForm->gridID;

    $oForm->DisplayPage();
}

?>