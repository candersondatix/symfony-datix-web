<?php

/**
 * Will return any coded fields for a given module that don't appear in the field format or field directory tables (because they are not fields)
 * The returned coded-fields will follow the same format as the return values from the SQL query in CodeSetupList function
 * This function is not ideal and should be factored-out, but it is easier to understand than some twisted SQL to get codes like causal groups out of the DB
 *
 * @param string $module A module code
 * @return array An array of coded fields. Each field will contain the following keys:
 *  - fdr_code_table
 *  - frd_label
 *  - fdr_name
 *  - fdr_table
 */
function getHardCodedFields($module) {

    global $scripturl;

	$codes = array(
		'INC' => array(
			array(
				'fdr_code_table' => '!RCG',
				'fdr_label'      => 'Causal groups',
				'fdr_name'       => 'RCG',
				'fdr_table'      => ''
			),
            array(
                'fdr_code_table' => 'ratings_map',
                'fdr_label'      => _tk('risk_grading'),
                'url'            => $scripturl . '?action=riskmatrixsetup'
            )
		),
        'CLA' => array(
            array(
                'fdr_code_table' => 'claims_insurer_excess',
                'fdr_label'      => _tk('claims_insurer_excess'),
                'url'            => $scripturl . '?action=excesssetup'
            )
        ),
        'CON' => array(
            array(
                'fdr_code_table' => 'code_inc_injury',
                'fdr_label'      => 'Injury',
                'fdr_name'       => 'inc_injury',
                'fdr_table'      => 'inc_injuries'
            ),
            array(
                'fdr_code_table' => 'code_inc_bodypart',
                'fdr_label'      => 'Body Part',
                'fdr_name'       => 'inc_bodypart',
                'fdr_table'      => 'inc_injuries'
            ),
        )

	);

	if (isset($codes[$module])) {
		return $codes[$module];
	}

	return array();
}

/**
 * Sorts a code-field result set by a given field and in a given order
 * This function could be re-factored to be made more usable
 *
 * @param array $array The result set to sort
 * @param string $orderby The key used to sort the members of $array
 * @param string $order The order to sort the array in. Should be either ASC or DESC
 * @return array The sorted result set
 */
function sortCodeFieldListing(array $array, $orderby = 'fdr_label', $order = 'ASC') {

    //Foreach setup, assign a "orderby" attribute with the value specified by the $orderby column.
    //This "orderby" attribute is used in the sortOrderCompare() function to sort the array list.
    foreach($array as $i => $Setup)
    {
        $array[$i]["orderby"] = $Setup[$orderby];
    }
	usort($array, "sortOrderCompare");

	if ($order == 'DESC') {
		$array = array_reverse($array);
	}

	return $array;
}

function sortOrderCompare($a, $b)
{
    return \UnicodeString::strcasecmp($a['orderby'], $b['orderby']);
}

?>
