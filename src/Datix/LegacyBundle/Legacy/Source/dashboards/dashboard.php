<?php

use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\framework\query\Query;
use src\framework\query\Where;
use src\framework\query\FieldCollection;
use src\users\model\User;

/**
* @desc Puts together an array of dashboard information for all the dashboards a user owns, along with any published to their user, group
* or profile settings by an administrator.
*
* @return array An array of dashboard information (recordid and dashboard name, as an array).
*/
function GetUserDashboards($Parameters = array())
{
    $DashboardArray = array();
    $DashboardIDArray = array();

    //Get user's own dashboards
    $sql = '
        SELECT
            recordid,
            dash_name
        FROM
            reports_dashboards
        WHERE
            dash_owner_id = '.$_SESSION["contact_login_id"].'
        ORDER BY
            recordid
    ';

    $UserDashboards = DatixDBQuery::PDO_fetch_all($sql);

    foreach ($UserDashboards as $UserDashboard)
    {
        $UserDashboard['editable'] = true;
        $DashboardArray[] = $UserDashboard;
        $DashboardIDArray[] = $UserDashboard['recordid'];
    }

    if (!$Parameters['own_only'])
    {
        //get dashboards assigned to the user's profile
        $sql = '
            SELECT
                sta_profile
            FROM
                contacts_main
            WHERE
                recordid = '.$_SESSION["contact_login_id"];

        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row['sta_profile'])
        {
            $sql = '
                SELECT
                    reports_dashboards.recordid,
                    reports_dashboards.dash_name
                FROM
                    reports_dashboards,
                    reports_dashboards_links
                WHERE
                    dash_link_type = \'PROFIL\'
                    AND
                    dash_link_id = '.$row['sta_profile'].'
                    AND
                    reports_dashboards.recordid = reports_dashboards_links.dash_id
            ';

            $ProfileDashboards = DatixDBQuery::PDO_fetch_all($sql);

            foreach ($ProfileDashboards as $ProfileDashboard)
            {
                if (!in_array($ProfileDashboard['recordid'] ,$DashboardIDArray, true))
                {
                    $DashboardArray[] = $ProfileDashboard;
                    $DashboardIDArray[] = $ProfileDashboard['recordid'];
                }
            }
        }

        //get dashboards assigned to the user's groups
        require_once 'Source/security/SecurityBase.php';

        $UserGroups = GetUserSecurityGroups($_SESSION["contact_login_id"]);

        if (!empty($UserGroups))
        {
            $sql = '
                SELECT
                    reports_dashboards.recordid,
                    reports_dashboards.dash_name
                FROM
                    reports_dashboards,
                    reports_dashboards_links
                WHERE
                    dash_link_type = \'GROUP\'
                    AND
                    dash_link_id IN ('.implode(', ', $UserGroups).')
                    AND
                    reports_dashboards.recordid = reports_dashboards_links.dash_id
            ';

            $GroupDashboards = DatixDBQuery::PDO_fetch_all($sql);

            foreach ($GroupDashboards as $GroupDashboard)
            {
                if (!in_array($GroupDashboard['recordid'], $DashboardIDArray, true))
                {
                    $DashboardArray[] = $GroupDashboard;
                    $DashboardIDArray[] = $GroupDashboard['recordid'];
                }
            }
        }

        //get dashboards attached to the user.
        $sql = '
            SELECT
                reports_dashboards.recordid,
                reports_dashboards.dash_name
            FROM
                reports_dashboards,
                reports_dashboards_links
            WHERE
                dash_link_type = \'USER\'
                AND
                dash_link_id = '.$_SESSION["contact_login_id"].'
                AND
                reports_dashboards.recordid = reports_dashboards_links.dash_id
        ';

        $UserDashboards = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($UserDashboards as $UserDashboard)
        {
            if (!in_array($UserDashboard['recordid'], $DashboardIDArray, true))
            {
                $DashboardArray[] = $UserDashboard;
                $DashboardIDArray[] = $UserDashboard['recordid'];
            }
        }
    }

    return $DashboardArray;
}

//the functions below all call dashboard methods from ajax calls, and are not really unit testable.
// ---------------------
//@codeCoverageIgnoreStart
// ---------------------

/**
* @desc Called from ajax - sets up the objects needed to retrieve the contents of the dashboard widgets. This
* will happen on dashboard load and whenever individual widgets are refreshed. Outputs the html results ready to
* be returned to the browser.
*/
function GetWidgetContents()
{
    session_write_close();
    require_once 'Source/dashboards/widgetclass.php';

    $Widget = new Widget(Sanitize::SanitizeInt($_GET['id']));

    //we need a dummy dashboard here mainly because of the iframe requirements, though it may also be useful elsewhere.
    //we can simplify this if it is causing performance issues.
    require_once 'Source/dashboards/dashboardclass.php';

    $Dashboard = new Dashboard();
    $Dashboard->SetRecordid(Sanitize::SanitizeInt($_GET['dbid']));
    $Dashboard->InitialiseDashboard(Sanitize::SanitizeInt($_GET['width']), Sanitize::SanitizeInt($_GET['height']));

    try
    {
        $Widget->InitialiseWidget($Dashboard);
    }
    catch(ReportNotFoundException $exception)
    {
        echo '<div>'.$exception->getMessage().'</div>';
        obExit();
    }

    //we are only reloading the contents of the body of the widget, not the whole widget, so we don't need to re-load the header etc.
    echo $Widget->GetWidgetBodyHTML();
}

/**
* @desc Called from ajax - sets up the object needed to delete a widget from a dashboard.
*/
function DeleteWidget()
{
    require_once 'Source/dashboards/widgetclass.php';

    $Widget = new Widget(Sanitize::SanitizeInt($_GET['id']));
    $Widget->DeleteWidget();
}

/**
* @desc Called from ajax - sets up the object needed to get the edit form for a given widget
*/
function EditWidget()
{
    require_once 'Source/dashboards/widgetclass.php';
    require_once 'Source/dashboards/dashboardclass.php';

    $Widget = new Widget(Sanitize::SanitizeInt($_GET['id']));
    $Widget->InitialiseWidget(new Dashboard());

    echo $Widget->GetEditForm();
}

/**
* @desc Called from ajax - sets up the object needed to get the edit form for a given dashboard
*/
function EditDashboard()
{
    require_once 'Source/dashboards/dashboardclass.php';

    $Dashboard = new Dashboard();
    $Dashboard->SetRecordid(Sanitize::SanitizeInt($_GET['id']));
    $Dashboard->InitialiseDashboard(0,0);

    echo $Dashboard->GetEditForm();
    echo getJSFunctions();
}

/**
* @desc Called from ajax when widgets are moved - sets up the object needed to save the new positions of the widgets.
*/
function SetWidgetOrders()
{
    require_once 'Source/dashboards/dashboardclass.php';

    $Dashboard = new Dashboard();
    $Dashboard->SetRecordid(Sanitize::SanitizeInt($_GET['id']));

    //the new widget order is calculated on the client and sent as a delimited list with widgets
    //separated by commas and columns by semicolons.
    foreach (explode('-',Sanitize::SanitizeString($_GET['order'])) as $col => $Orders)
    {
        $OrderArray[$col] = explode(',',$Orders);
    }

    $Dashboard->SaveWidgetOrders($OrderArray);
}

/**
* @desc Called from ajax. Adds the current report to my reports
* Only works for crosstabs atm.
*/
function AddCurrentToMyReports()
{
    if ($_GET['title'])
    {
        $_SESSION['CurrentReport']->package_title = $_GET['title'];
    }

    if ($_SESSION['CurrentReport']->WhereClausePrompt && $_GET['use_values'] == 'PROMPT')
    {
        $_SESSION['CurrentReport']->WhereClause = $_SESSION['CurrentReport']->WhereClausePrompt;
    }
    elseif ($_SESSION['CurrentReport']->OriginalWhereClause)
    {
        $_SESSION['CurrentReport']->WhereClause = $_SESSION['CurrentReport']->OriginalWhereClause;
    }

    if (!$_SESSION['CurrentReport']->rep_id) // make sure we've already saved a packaged report here.
    {
        $_SESSION['CurrentReport']->SaveToMyReports();
    }
}

/**
* @desc Called from ajax. Creates a new dashboard and sets it as the default for this user in preparation for the dashboard page to be reloaded.
*/
function CreateNewDashboard()
{
    require_once 'Source/dashboards/dashboardclass.php';

    $Dashboard = new Dashboard();
    $Dashboard->SaveAsNewDashboard();

    $JSONdata['success'] = true;
    $JSONdata['dash_id'] = $Dashboard->Recordid;

    echo json_encode($JSONdata);
}

/**
* @desc Called from ajax. Sets a particular dashboard as the default one.
*/
function SetDefaultDashboard()
{
    require_once 'Source/dashboards/dashboardclass.php';

    $Dashboard = new Dashboard();
    $Dashboard->SetRecordid(Sanitize::SanitizeInt($_GET['id']));
    $Dashboard->SetAsDefault();
}

/**
* @desc Gets the id of the dashboard that has been set by the current user as their default.
*/
function GetDefaultDashboard()
{
    return GetParm('DEFAULT_DASHBOARD', 0);
}

/**
* @desc Gets the id of the dashboard that should be displayed for the current user.
*/
function GetCurrentDashboard()
{
    $UserDashboards = GetUserDashboards();

    foreach ($UserDashboards as $dashboard)
    {
        $ValidDashboardIDs[] = $dashboard['recordid'];
    }

    if (is_numeric($_GET['dash_id']) && in_array($_GET['dash_id'], $ValidDashboardIDs)) //manually set default
    {
        return Sanitize::SanitizeInt($_GET['dash_id']);
    }

    $GlobalDefault = GetDefaultDashboard();

    if ($GlobalDefault && in_array($GlobalDefault, $ValidDashboardIDs))
    {
        return $GlobalDefault;
    }

    //just pick the first dashboard in the list.
    $sql = '
        SELECT
            TOP 1 recordid
        FROM
            reports_dashboards
        WHERE
            dash_owner_id = :login_id
        ORDER BY
            recordid ASC
    ';

    $row = DatixDBQuery::PDO_fetch($sql, array('login_id' => $_SESSION["contact_login_id"]));

    return $row['recordid'];
}

/**
* @desc Called from ajax. Updates the dashboard database record with values input by the user.
*/
function UpdateDashboardSettings()
{
    require_once 'Source/dashboards/dashboardclass.php';

    $Dashboard = new Dashboard();
    $Dashboard->SetRecordid(Sanitize::SanitizeInt($_GET['id']));

    $Dashboard->Data['dash_name'] = strip_tags($_GET['dash_name_'.$_GET['id']]);
    $Dashboard->Data['dash_graphs_per_row'] = Sanitize::SanitizeString($_GET['dash_graphs_per_row_'.$_GET['id']]);

    if ($_GET['dash_default_'.$_GET['id']] == '1')
    {
        $Dashboard->SetAsDefault();
    }

    if (is_array($_GET['dash_profiles_'.$_GET['id']]))
    {
        $Dashboard->LinkedProfiles = $_GET['dash_profiles_'.$_GET['id']];
    }

    if (is_array($_GET['dash_groups_'.$_GET['id']]))
    {
        $Dashboard->LinkedGroups = $_GET['dash_groups_'.$_GET['id']];
    }

    if (is_array($_GET['dash_users_'.$_GET['id']]))
    {
        $Dashboard->LinkedUsers = $_GET['dash_users_'.$_GET['id']];
    }

    $Dashboard->UpdateDashboardRecord();

    $JSONdata['dash_id'] = $Dashboard->Recordid;

    echo json_encode($JSONdata);
}

/**
* @desc Called from ajax. Deletes a dashboard and returns success or failure to the front end via json.
*/
function DeleteDashboard()
{
    require_once 'Source/dashboards/dashboardclass.php';

    $Dashboard = new Dashboard();
    $Dashboard->SetRecordid(Sanitize::SanitizeInt($_GET['id']));
    $success = $Dashboard->DeleteDashboard();

    $JSONdata['success'] = $success;
    $JSONdata['id'] = Sanitize::SanitizeInt($_GET['id']);

    echo json_encode($JSONdata);
}

/**
* Gets the HTML for one of the thumbnails in the left hand widget bar.
*
* @param array $packagedReport The packaged report data.
*
* @return string HTML representing an individual report to be placed in the sidebar.
*/
function GetWidgetPanelReportThumbnailHTML(array $packagedReport)
{
    $HTML = '<div class="report_thumb_list">';

    $ImgSrc = '';

    switch ($packagedReport['type'])
    {
        case src\reports\model\report\Report::CROSSTAB:
            $ImgSrc = 'crosstab.jpg';
            $Alt = _tk('crosstab_report');
            break;
        case src\reports\model\report\Report::LISTING:
            $ImgSrc = 'listing.jpg';
            $Alt = _tk('listing_report');
            break;
        case src\reports\model\report\Report::BAR:
            $ImgSrc = 'bar.jpg';
            $Alt = _tk('bar_chart');
            break;
        case src\reports\model\report\Report::LINE:
            $ImgSrc = 'line.jpg';
            $Alt = _tk('line_graph');
            break;
        case src\reports\model\report\Report::SPC_C_CHART:
        case src\reports\model\report\Report::SPC_I_CHART:
        case src\reports\model\report\Report::SPC_MOVING_RANGE:
        case src\reports\model\report\Report::SPC_RUN_CHART:
            $ImgSrc = 'spc.jpg';
            $Alt = _tk('spc_chart');
            break;
        case src\reports\model\report\Report::PIE:
            $ImgSrc = 'pie.jpg';
            $Alt = _tk('pie_chart');
            break;
        case src\reports\model\report\Report::GAUGE:
            $ImgSrc = 'gauge.jpg';
            $Alt = _tk('speedometer');
            break;
        case src\reports\model\report\Report::TRAFFIC:
            $ImgSrc = 'traffic_light.jpg';
            $Alt = _tk('traffic_light_chart');
            break;
        case src\reports\model\report\Report::PARETO:
            $ImgSrc = 'pareto.jpg';
            $Alt = _tk('pareto_graph');
            break;
    }

    if ($ImgSrc)
    {
        $HTML .= '<div><img src="Images/report_thumbnails/'.$ImgSrc.'" title="'.$Alt.'" alt="'.$Alt.'" /></div>';
    }
    
    $HTML .= '<div>' . ($packagedReport['name'] ? htmlspecialchars($packagedReport['name']) : '&lt;untitled&gt;') . '</div>';
    $HTML .= '<div onclick="DashboardObjects[CurrentDashboard].AddToDashboard('.$packagedReport['recordid'].')"><a href="javascript:void(0)"><b>Add to dashboard</b></a></div>';

    $HTML .= '</div>';

    return $HTML;
}

/**
* @desc Gets the HTML for the widget side panel.
*
* @return string HTML representing the side panel containing a list of widgets that can be added to the dashboard
*
* [no unit test because this is an HTML-construction function, and so there is no clear output to check for]
*/
function MakeWidgetPanelHTML()
{
    $user = $_SESSION['CurrentUser'];
    if (!($user instanceof User))
    {
        throw new CurrentUserNotFoundException();
    }
    
    // ------------------------------------------------------------------------------------------------
    // TODO: the commented block below is the way we should be going with this, however
    //       the model doesn't currently allow us to fetch data for nested entities in one hit,
    //       so it generates an additional DB query for each packaged report in order to access the 
    //       report design object so we can inspect the report type.  This is very inefficient and leads
    //       to a several-fold increase in execution time when rendering the side panel, so for now 
    //       we'll manually query the DB here for the sake of performance, pending the appropriate 
    //       model improvements.
    // ------------------------------------------------------------------------------------------------
    
    /*
    $factory = new PackagedReportModelFactory();
    
    $orderBy = ['web_packaged_reports.name'];
    
    $myReports = $factory->getMyReportsCollection((new Query)->where( ['web_reports.owner' => $user->initials] )->orderBy($orderBy));
    
    $where = new Where();
    $where->add((new FieldCollection)->field('web_reports.owner')->notEq($user->initials));
    
    $adminReports = $factory->getMyReportsCollection((new Query)->where($where)->orderBy($orderBy));
    */

    list($select, $where, $orderBy, $params) = (new PackagedReportModelFactory)->getMyReportsSQL();
    
    $myReportsSql = $select . $where . ' AND web_reports.owner = :owner ' . $orderBy;
    $myReports = DatixDBQuery::PDO_fetch_all($myReportsSql, array_merge($params, ['owner' => $user->initials]));
    
    $adminReportsSql = $select . $where . ' AND (web_reports.owner != :owner OR web_reports.owner IS NULL) ' . $orderBy;
    $adminReports = DatixDBQuery::PDO_fetch_all($adminReportsSql, array_merge($params, ['owner' => $user->initials]));
    
    $HTML .= '
    <ul class="widgettabNavigation" style="background-color:none">
    <li class="dashboard_tab-selected widget_sidebar_tab dashboard_tab" id="user-reports-tab" ><a><span class="dashboard_title dashboard_tab_contents" onclick="jQuery(\'.user-reports\').show();jQuery(\'.admin-reports\').hide();jQuery(\'.widget_sidebar_tab\').removeClass(\'dashboard_tab-selected\');jQuery(\'#user-reports-tab\').addClass(\'dashboard_tab-selected\')">' . _tk('widget_sidebar_my_reports') . '</span></a></li>
    <li class="widget_sidebar_tab dashboard_tab" id="admin-reports-tab" ><a><span class="dashboard_title dashboard_tab_contents" onclick="jQuery(\'.user-reports\').hide();jQuery(\'.admin-reports\').show();jQuery(\'.widget_sidebar_tab\').removeClass(\'dashboard_tab-selected\');jQuery(\'#admin-reports-tab\').addClass(\'dashboard_tab-selected\')">' . _tk('widget_sidebar_other_reports') . '</span></a></li>
    </ul>';

    $HTML .= '<div class="user-reports">';

    foreach ($myReports as $report)
    {
        $HTML .= GetWidgetPanelReportThumbnailHTML($report);
    }

    $HTML .= '</div>';

    $HTML .= '<div class="admin-reports" style="display:none">';

    foreach ($adminReports as $report)
    {
        $HTML .= GetWidgetPanelReportThumbnailHTML($report);
    }

    $HTML .= '</div>';

    AddSidePanel($HTML);
}

function AddSidePanel($Contents)
{
    global $dtxside_panel, $JSFunctions;

    $dtxside_panel = '
    <a href="#" class="hidden-tab">hidden</a>
    <div class="hidden-panel" style="height: 250px;">
        <div id="form-wrap">
            <div class="hiddenFormDiv">
                <div class="review-options">'.
        $Contents.'
                </div>
            </div>
        </div>
    </div>';

    $JSFunctions[] = '
jQuery(function() {
    var hiddenTab = {
        speed:900,
        containerWidth:jQuery(\'.hidden-panel\').outerWidth(),
        containerHeight:jQuery(\'.hidden-panel\').outerHeight(),
        tabWidth:jQuery(\'.hidden-tab\').outerWidth(),
        init:function(){
            //jQuery(\'.hidden-panel\').css(\'height\',\'2000px\');
            jQuery(\'a.hidden-tab\').click(function(event){
                if (jQuery(\'.hidden-panel\').hasClass(\'open\')) {
                    jQuery(\'.hidden-panel\').animate({left:\'-\' + hiddenTab.containerWidth}, hiddenTab.speed)
                    .removeClass(\'open\');
                    jQuery(\'.hidden-tab\').removeClass(\'hidden-tab-open\');
                } else {
                    jQuery(\'.hidden-panel\').animate({left:\'0\'},  hiddenTab.speed)
                    .addClass(\'open\');
                    jQuery(\'.hidden-tab\').addClass(\'hidden-tab-open\');
                }
                event.preventDefault();
            });
        }
    };
    hiddenTab.init();
});';
}

// ---------------------
//@codeCoverageIgnoreEnd
// ---------------------