<?php

use src\reports\model\report\Report;
use src\reports\model\packagedreport\PackagedReport;
use src\framework\query\QueryFactory;

/**
* @desc Class to represent a widget.
*/
class Widget
{
    var $Recordid;

    var $Type;
    var $Title;
    var $Data;
    var $ContentId;
    var $Report;

    var $DashboardObj;

    /**
    * @desc Basic constructor - seperate from the initialise method in case we
    * want to use a method without calling a load of uneccesary setup functions.
    *
    * @param int $Recordid The recordid of the record that this object represents in reports_dash_links
    */
    function __construct($Recordid)
    {
        $this->Recordid = $Recordid;
    }

    /**
    * @desc Populates parameters from database and passed values.
    *
    * @param obj $DashboardObj Reference to the dashboard object in which this widget is contained. A bit messy, but we want to be able to get "parent" parameters from the dashboard object
    */
    function InitialiseWidget(&$DashboardObj)
    {
        $sql = 'SELECT recordid, dlink_dash_id, dlink_title, dlink_package_id, dlink_row_limit, dlink_col_limit, dlink_collapse_headings FROM reports_dash_links WHERE recordid = '.$this->Recordid;

        $data = DatixDBQuery::PDO_fetch($sql);

        $this->Data = $data;

        $this->Title = $data['dlink_title'];
        $this->ContentId = $data['dlink_package_id'];

        $this->DashboardObj = $DashboardObj;

        if ($this->ContentId)
        {
            $factory = new \src\reports\model\packagedreport\PackagedReportModelFactory();
            $packagedReport = $factory->getMapper()->find($this->ContentId);

            if ($packagedReport->name)
            {
                $this->Title = $packagedReport->name;
            }
        }

        if (!$this->Title)
        {
            $this->Title = '&lt;untitled&gt;';
        }

        $this->Report = $packagedReport;
    }

    /**
    * @desc Checks whether the currently logged in user is the creator of the report held by this widget
    *
    * @return bool True if the current user is the creator, false otherwise.
    */
    function CurrentUserIsOwner()
    {
        return ($this->Report->createdby == $_SESSION["initials"]);
    }

    // ---------------------
    //@codeCoverageIgnoreStart
    // ---------------------
    /**
    * @desc Constructs the HTML for the frame of the widget and returns it.
    *
    * @return string The widget HTML.
    */
    function GetWidgetHTML()
    {
        $height = 'auto';

        if ($this->Report->report instanceof Report)
        {
            $report_type_class = false;

            // Change the widget height depending on the report type
            switch ($this->Report->report->getReportType())
            {
                case Report::TRAFFIC:
                    $height = '130px';
                    break;
                case Report::LISTING:
                    $height = '600px';
                    break;
                default:
                    $height = '300px';
            }

            $spc_charts = array(
                Report::SPC_C_CHART,
                Report::SPC_I_CHART,
                Report::SPC_MOVING_RANGE,
                Report::SPC_RUN_CHART
            );

            if(in_array($this->Report->report->getReportType(), $spc_charts))
            {
                $report_type_class = 'spc_chart';
            }
            else
            {
                $report_type_class = \UnicodeString::strtolower(preg_replace('#[\s-]#i', '_', $this->Report->report->getTypeName()));
            }
        }

        $HTML = '<div class="widget' . ($report_type_class ? ' ' . $report_type_class : '') . '" id="widget_'.$this->Recordid.'">
            <div class="widget_header" id="widget_header_'.$this->Recordid.'">

            <span style="display:block; width:90%; margin: 0 auto;" class="widget_title" id="widget_title_'.$this->Recordid.'">'.htmlspecialchars($this->Title).'</span>';

        $HTML .= $this->GetSettingsMenuHTML();

        $HTML .= '
            </div>
            <div class="widget-viewport">
                <div class="widget_wait" id="widget_wait_'.$this->Recordid.'">
                '._tk('dashboard_please_wait').'
                </div>
                <div class="widget_body" id="widget_body_'.$this->Recordid.'" style="height: '.$height.';">';

            //use AJAX to populate the body of the widget.
            //TODO this could be done by sending the various parameters as an array of each widget through to JS using the asset manager and then using that to initiate each widget in one place rather than rendering each on on the page, as that's icky.
            $HTML .= '
                <script language="JavaScript" type="text/javascript">
                    jQuery(function(){

                        var newjQueryWidget = jQuery(\'#widget_'.$this->Recordid.'\').datixWidget({

                            id: ' . ($this->Recordid ?: 'null') . ',
                            reportId: ' . ($this->Report->recordid ?: 'null') . ',
                            dashboardId: ' . ($this->DashboardObj->Recordid ?: 'null') . ',
                            title: "' . addslashes(htmlspecialchars($this->Title)) . '",
                            editable: ' . ($this->widgetIsEditable() ? 'true' : 'false') . '
                        });

                        DashboardObjects['.$this->DashboardObj->Recordid.'].addWidget(newjQueryWidget);
                    });
                </script>
            ';

        //We could use this line instead if we didn't want to use ajax for population.
//         $HTML .= $this->GetWidgetBodyHTML();

        $HTML .= '</div></div></div>';

        return $HTML;
    }
    
    /**
     * Whether or not the widget is "editable" (i.e. the user has access to the "Save" button on the Edit Widget form).
     * 
     * @return boolean
     */
    protected function widgetIsEditable()
    {
        if (!$this->CurrentUserIsOwner())
        {
            return false;
        }
        
        if ($this->Report->report instanceof Report && $this->Report->report->getReportType() == Report::LISTING)
        {
            return false;
        }
        
        return true;
    }

    function GetSettingsMenuHTML()
    {
        if ($this->Report instanceof PackagedReport)
        {
            $reportType = $this->Report->report->getReportType();

            switch($reportType)
            {
                case Report::CROSSTAB:
                    $exportMode = 'crosstab';
                    break;
                case Report::LISTING:
                    $exportMode = 'listing';
                    break;
                default:
                    $exportMode = 'graph';
                    break;
            }

            $ExportButtonLink = "app.php?action=httprequest&amp;type=getExportFormHTML&amp;exportmode=$exportMode&recordid={$this->Report->recordid}";

            // We need to pass to function exportReport both the reportId and the widgetId because when we are
            // exporting to PDF we use the widgetId and when we are exporting to Excel we use the reportId.
            // TODO: Make a consistent way of exporting to PDF and Excel from the dashboard and from the full screen views
            $exportJS = "var buttons = new Array();
                             var rArray = new Array();
                             buttons[0]={'value':'"._tk('btn_export')."','onclick':'exportReport(".$reportType.",".$this->Report->recordid.",".$this->Recordid.");'};
                             buttons[1]={'value':'"._tk('btn_cancel')."','onclick':'GetFloatingDiv(\'exportoptions\').CloseFloatingControl();'};

                             PopupDivFromURL('exportoptions', 'Export', '$ExportButtonLink', '', buttons, '');";

            $HTML =
                '<span class="settings_link">
                    <ul class="dropdown">
                        <li><img alt="'._tk('dashboard_settings').'" src="Images/wrench.png">
                            <ul class="sub_menu">';

            if ($this->DashboardObj->CurrentUserIsOwner() && GetParm('DAS_PERMS') == 'DAS_FULL')
            {
                $HTML .= '<li onClick="WidgetObjects['.$this->Recordid.'].Edit(); jQuery(this).trigger(\'mouseleave\');">'._tk('dashboard_menu_edit').'</li>';
            }

            $HTML .= '
                            <li onclick="WidgetObjects['.$this->Recordid.'].Initialise(); jQuery(this).trigger(\'mouseleave\');">'._tk('dashboard_menu_refresh').'</li>';

            //don't want to give this option when looking at other user's dashboards.
            if ($this->DashboardObj->CurrentUserIsOwner() && GetParm('DAS_PERMS') == 'DAS_FULL')
            {
                $HTML .= '<li onclick="WidgetObjects['.$this->Recordid.'].Delete(); jQuery(this).trigger(\'mouseleave\');">'._tk('dashboard_menu_delete').'</li>';
            }

            if($reportType != Report::TRAFFIC && !\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
            {
                $HTML .= '<li onclick="'.$exportJS.'">'._tk('dashboard_menu_export').'</li>';
            }

            //don't want to give this option when looking at other user's dashboards.
            if ($this->DashboardObj->CurrentUserIsOwner() && GetParm('DAS_PERMS') == 'DAS_FULL')
            {
                //Get user's own dashboards
                if (empty($_SESSION['dashboards.user_dashboards']))
                {
                    $sql = 'SELECT recordid, dash_name FROM reports_dashboards WHERE dash_owner_id = '.$_SESSION["contact_login_id"];
                    $_SESSION['dashboards.user_dashboards'] = DatixDBQuery::PDO_fetch_all($sql);
                }

                $UserDashboards = $_SESSION['dashboards.user_dashboards'];

                if (count($UserDashboards) > 1)
                {
                    $HTML .= '<li>'._tk('dashboard_menu_moveto').'
                                    <ul>';

                    foreach ($UserDashboards as $UserDashboard)
                    {
                        if ($UserDashboard['recordid'] != $this->DashboardObj->Recordid)
                        {
                            $HTML .= '<li onclick="WidgetObjects['.$this->Recordid.'].Move('.$UserDashboard['recordid'].'); jQuery(this).trigger(\'mouseleave\');">'.$UserDashboard['dash_name'].'</li>';
                        }
                    }

                    $HTML .= '</ul>
                            </li>';
                }

            }

            $HTML .= '
                            </ul>
                        </li>
                    </ul>
                </span>';
        }
        // Give ability to delete an item that has no associated package report if user is owner and has full permissions
        elseif($this->Report == null && $this->DashboardObj->CurrentUserIsOwner() && GetParm('DAS_PERMS') == 'DAS_FULL')
        {
            $HTML = '
<span class="settings_link">
    <ul class="dropdown">
        <li><img alt="'._tk('dashboard_settings').'" src="Images/wrench.png">
            <ul class="sub_menu">
                <li onclick="WidgetObjects['.$this->Recordid.'].Delete()">'._tk('dashboard_menu_delete').'</li>
            </ul>
        </li>
    </ul>
</span>';
        }

        return $HTML;
    }

    /**
    * @desc Constructs the HTML for the contents of the widget (with potential for different types of content here).
    *
    * @return string The HTML of the body of the message.
    */
    function GetWidgetBodyHTML()
    {
        global $scripturl;

        $ModuleDefs = \src\framework\registry\Registry::getInstance()->getModuleDefs();

        $HTML       = '';
        $reportType = $this->Report instanceof PackagedReport ? $this->Report->report->getReportType() : '';

        // Isolate the lazy load of packaged report query object and query where object.
        // We need to access these properties because they are lazy loaded on the packaged report class.
        // TODO: Remove this when we are refactoring the packaged report class to load upfront the lazy loaded properties.
        $this->Report->query;
        $this->Report->query->where;

        switch ($reportType)
        {
            case src\reports\model\report\Report::LISTING:
                require_once 'Source/reports.php';

                // We need to store the title of the report on the session to be added to the stream to export to PDF
                $_SESSION['listing_title'] = $this->Report->report->title;

                $loader = new src\framework\controller\Loader();
                $controller = $loader->getController(array('controller' => 'src\\reports\\controllers\\ReportController'));
                $controller->setRequestParameter('packagedReport', $this->Report);
                $controller->setRequestParameter('context', \src\reports\controllers\ReportWriter::DASHBOARD);

                $HTML = $controller->doAction('displayReport');

                // Restructuring Session variables for Exporting from Dashboard
                $ListingReport = $_SESSION['listingreportstream'];

                if (!is_array($_SESSION['listingreportstream']))
                {
                    $_SESSION['listingreportstream'] = null;
                }

                $_SESSION['listingreportstream_dashboard'][$this->Recordid] = serialize($ListingReport);

                $crosstabtablePHPExcel = $_SESSION['crosstabtablePHPExcel'];

                if (!is_array($_SESSION['crosstabtablePHPExcel']))
                {
                    $_SESSION['crosstabtablePHPExcel'] = null;
                }

                $_SESSION['crosstabtablePHPExcel'][$this->Recordid] = $crosstabtablePHPExcel;

                break;

            default: //standard content: a packaged graphical report.
                if (!$this->Report->report)
                {
                    $HTML .= 'Report not found.';
                }
                elseif ($this->Report->query->hasAtPrompt())
                {
                    $HTML.= '<div style="text-anchor: middle; font-family: Verdana; font-size: 10px; text-align: center; cursor: pointer; width: auto; height: 150px; padding-top: 150px; color: #666666;" onclick="SendTo(\''.$scripturl.'?action=reportdesigner&designer_state=closed&from_my_reports=1&recordid='.$this->ContentId.'\');">No data to display.</div>';
                }
                else
                {
                    $isCrosstab = ($reportType == src\reports\model\report\Report::CROSSTAB ? true : false);

                    if ($isCrosstab)
                    {
                        $HTML .= '<div style="cursor:pointer" onclick="SendTo(\''.$scripturl.'?action=reportdesigner&designer_state=closed&recordid='.$this->ContentId.'&from_dashboard='.$this->DashboardObj->Recordid.'\');">';
                    }

                    $loader = new src\framework\controller\Loader();
                    $controller = $loader->getController(
                        array('controller' => 'src\\reports\\controllers\\ReportController')
                    );
                    $controller->setRequestParameter('recordid', $this->ContentId);

                    if ($isCrosstab)
                    {
                        $controller->setRequestParameter('isCrosstab', true);
                    }

                    $controller->setRequestParameter('context', \src\reports\controllers\ReportWriter::DASHBOARD);

                    $HTML .= $controller->doAction('displayReport');

                    if ($isCrosstab)
                    {
                        $HTML .= '</div>';
                    }
                }
                break;
        }

        return $HTML;
    }

    /**
    * @desc Provides an HTML form allowing the user to edit aspects of the widget (and attached report).
    */
    function GetEditForm()
    {
        require_once 'Source/libs/FormClasses.php';

        $HTML .= '<ol style="border:#094367 1px solid">';

        $HTML .= '<form id="designReport">';

        $HTML .= '<input type="hidden" id="module" value="'.$this->Report->report->module.'">';

        $loader = new \src\framework\controller\Loader();

        $controller = $loader->getController(
            array('controller' => 'src\\reports\\controllers\\ReportDesignFormController')
        );

        $controller->setRequestParameter('context', 'dashboard');
        $controller->setRequestParameter('module', $this->Report->report->module);
        $controller->setRequestParameter('packagedReport', $this->Report);

        $HTML .= $controller->doAction('reportDesignerForm', true);

        $HTML .= '</form>';

        $HTML .= '</ol>';

        return $HTML;

    }
    // ---------------------
    //@codeCoverageIgnoreEnd
    // ---------------------

    /**
    * @desc Deletes this widget from the database (and hence the dashboard).
    */
    function DeleteWidget()
    {
        $sql = 'DELETE FROM reports_dash_links WHERE recordid = '.$this->Recordid;
        DatixDBQuery::PDO_query($sql);
    }
}

