<?php

/**
* @desc Class to represent a dashboard.
*/
class Dashboard
{
    var $Recordid;
    var $Data;
    var $NumCols;
    var $ScreenWidth;
    var $ScreenHeight;
    var $ColumnWidgets;
    var $EditMode = false; //bool: if true then the logged in user can edit aspects of this dashboard.
    var $WidgetArray = array();

    var $LinkedProfiles = array(); //array of profiles, the members of which will see this dashboard automatically.
    var $LinkedGroups = array(); //array of groups, the members of which will see this dashboard automatically.
    var $LinkedUsers = array(); //array of users who will see this dashboard automatically.

    var $ColWidth = 0; //width in pixels of the columns on this dashboard - depends on screen size and number of columns.

    var $Hidden; //bool: if true, then this dashboard should not be displayed by default on pageload

    /**
    * @desc Basic constructor - seperate from the initialise method in case we
    * want to use a method without calling a load of uneccesary setup functions.
    */
    function __construct()
    {
        //set some default values for this dashboard.
        $this->Data['dash_owner_id'] = $_SESSION["contact_login_id"];
    }

    /**
    * @desc Sets the recordid of the dashboard. If not passed, just picks up the default dashboard for this user.
    *
    * @param int $Recordid The recordid of the record that this object represents in reports_dashboards
    */
    function SetRecordid($Recordid = null)
    {
        $this->Recordid = $Recordid;

        if ($this->Recordid)
        {
            $sql = 'SELECT recordid, dash_owner_id, dash_name, dash_graphs_per_row
                    FROM reports_dashboards WHERE recordid = '.$this->Recordid;

            $result = DatixDBQuery::PDO_fetch($sql);

            if (!empty($result))
            {
                $this->Data = $result;
            }

        }
        else if ($_SESSION["contact_login_id"])
        {
            $sql = 'SELECT recordid, dash_owner_id, dash_name, dash_graphs_per_row
                    FROM reports_dashboards WHERE dash_owner_id = '.$_SESSION["contact_login_id"];

            $result = DatixDBQuery::PDO_fetch($sql);

            if (!empty($result))
            {
                $this->Data = $result;
                $this->Recordid = $this->Data['recordid'];
            }
        }

        $this->NumCols = FirstNonNull(array($this->Data['dash_graphs_per_row'], GetParm('DASH_GRAPHS_PER_ROW', '3')));
    }

    /**
    * @desc Populates parameters from database and passed values. Calls function to initialise widgets.
    *
    * @param int $ScreenWidth The width of the user's screen.
    * @param int $ScreenHeight  The height of the user's screen.
    */
    function InitialiseDashboard($ScreenWidth, $ScreenHeight)
    {
        global $scripturl;

        $this->ScreenWidth = $ScreenWidth;
        $this->ScreenHeight = $ScreenHeight;

        if ($this->CurrentUserIsOwner() && $this->CurrentUserCanAdd())
        {
            $this->EditMode = true; //current user can edit this dashboard.
        }

        if (!$this->Recordid)
        {
            $this->SetRecordid();
        }

        $LinkedItems = DatixDBQuery::PDO_fetch_all('SELECT dash_link_type, dash_link_id FROM reports_dashboards_links WHERE dash_id = :recordid', array('recordid' => $this->Recordid));

        $Groups = array();
        $Profiles = array();
        $Users = array();
        foreach ($LinkedItems as $row)
        {
            switch ($row['dash_link_type'])
            {
                case 'GROUP':
                    $this->LinkedGroups[] = $row['dash_link_id'];
                    break;

                case 'PROFIL':
                    $this->LinkedProfiles[] = $row['dash_link_id'];
                    break;

                case 'USER':
                    $this->LinkedUsers[] = $row['dash_link_id'];
                    break;
            }
        }

        $this->InitialiseWidgets();
    }

    /**
    * @desc Selects all widgets linked to this dashboard and sorts them into columns according to the dlink_column field. The
    * widgets are stored in the correct order and column in the parameter "ColumnWidgets"
    */
    function InitialiseWidgets()
    {
        $WidgetArray = DatixDBQuery::PDO_fetch_all('SELECT reports_dash_links.recordid, web_reports.module, dlink_column FROM reports_dash_links LEFT JOIN web_packaged_reports ON web_packaged_reports.recordid = dlink_package_id LEFT JOIN web_reports ON web_reports.recordid = web_packaged_reports.web_report_id WHERE dlink_dash_id = :recordid ORDER BY dlink_order', array('recordid' => $this->Recordid));

        foreach ($WidgetArray as $row)
        {
            if ($row['module'] && !GetAccessLevel($row['module']))
            {
                //user doesn't have access to the module - don't show this graph
                continue;
            }

            //by default, will go in the first column if no column set, or if in a column outside of the current dashboard settings.
            if (!$row["dlink_column"] || $row["dlink_column"] >= $this->NumCols)
            {
                $row["dlink_column"] = 0;
            }

            require_once 'Source/dashboards/widgetclass.php';
            $this->ColumnWidgets[$row["dlink_column"]][] = new Widget($row["recordid"]);
            $this->WidgetArray[$row["recordid"]] = new Widget($row["recordid"]);
        }
    }

// ---------------------
//@codeCoverageIgnoreStart
// ---------------------
    /**
    * @desc Constructs the HTML for the dashboard and returns it.
    *
    * @return string The HTML representing this dashboard.
    */
    function GetDashboardHTML()
    {
        global $ModuleDefs, $scripturl;

        $HTML = '<div class="dashboard" id="dashboard_'.$this->Recordid.'"';

        if ($this->Hidden)
        {
            $HTML .= ' style="display:none"';
        }

        $HTML .= '>';

        for ($i=0; $i<$this->NumCols; $i++)
        {
            $HTML .= $this->GetColumnHTML($i);
        }

        $HTML .= '<div id="empty_dashboard_message_'.$this->Recordid.'" class="empty_dashboard_message"'.(!empty($this->WidgetArray) ? ' style="display:none"' : '').'>This dashboard is empty.</div>';

        //needed to prevent dashboard collapsing because columns are floated.
        $HTML .= '<div style="clear:both;"></div>';

        $HTML .= '<div class="dashboard_options_link_row">';

        $HTML .= '<span class="dashboard_options_link default_dashboard_link" id="default_dashboard_link_'.$this->Recordid.'" onclick="DashboardObjects['.$this->Recordid.'].SetAsDefault()" '.(GetParm('DEFAULT_DASHBOARD', 0) != $this->Recordid ? '' : 'style="display:none"').'>Set as default dashboard</span>';

        if ($this->EditMode)
        {
            $HTML .= '<span id="dashboard_options_'.$this->Recordid.'" class="dashboard_options_link" onclick="disableDashboardOptions(\'' . $this->Recordid . '\'); DashboardObjects['.$this->Recordid.'].Edit();">Options</span>';
        }

        $HTML .= '<span id="dashboard_help" class="dashboard_options_link"><a href="'.$scripturl.'?action=helpfinder&module=DAS" target="_blank">'. _tk('help_alt') .'</a></span>';

        $HTML .= '</div>';

        return $HTML;
    }

    /**
    * @desc Constructs the HTML for the options section of the dashboard and returns it.
    *
    * @return string The HTML representing the options section of this dashboard.
    */
    function GetEditForm()
    {
        require_once 'Source/libs/FormClasses.php';

        $HTML .= '<form id="dashboard_options_form_'.htmlspecialchars($this->Recordid).'"><ol style="border:#094367 1px solid">';

        $HTML .= GetDivFieldHTML('Dashboard name', '<input type="text" id="dash_name_'.htmlspecialchars($this->Recordid).'" name="dash_name_'.htmlspecialchars($this->Recordid).'" maxlength="128" size="44" value="'.htmlspecialchars($this->Data['dash_name']).'">');
        $HTML .= GetDivFieldHTML('Graphs per row', '<input type="text" id="dash_graphs_per_row_'.htmlspecialchars($this->Recordid).'" name="dash_graphs_per_row_'.htmlspecialchars($this->Recordid).'" value="'.htmlspecialchars($this->Data['dash_graphs_per_row']).'" maxlength="1" size="1">');

        if (IsFullAdmin())
        {
            $ProfileList = array();
            $ProfileArray = DatixDBQuery::PDO_fetch_all('SELECT recordid, pfl_name FROM profiles');
            foreach ($ProfileArray as $row)
            {
                $ProfileList[$row['recordid']] = $row['pfl_name'];
            }

            $field = Forms_SelectFieldFactory::createSelectField('dash_profiles_'.$this->Recordid, 'DAS', $this->LinkedProfiles, '', true);
            $field->setCustomCodes($ProfileList);
            $field->setSuppressCodeDisplay();
            $field->setIgnoreMaxLength();
            $HTML .= GetDivFieldHTML('Profiles', $field->getField());

            $GroupList = array();
            $GroupArray = DatixDBQuery::PDO_fetch_all('SELECT recordid, grp_code FROM sec_groups');
            foreach ($GroupArray as $row)
            {
                $GroupList[$row['recordid']] = $row['grp_code'];
            }

            $field = Forms_SelectFieldFactory::createSelectField('dash_groups_'.$this->Recordid, 'DAS', $this->LinkedGroups, '', true);
            $field->setSelectFunction('getSecurityGroups');
            $field->setCustomCodes($GroupList);
            $field->setSuppressCodeDisplay();
            $field->setIgnoreMaxLength();
            $HTML .= GetDivFieldHTML('Groups', $field->getField());

            $UserList = array();
            $sql = GetContactListSQLByAccessLevel(array(
                    'module' => 'DAS',
                    'levels' => array('DAS_FULL', 'DAS_READ_ONLY')
                    ));
            $UserArray = DatixDBQuery::PDO_fetch_all($sql);
            foreach ($UserArray as $row)
            {
                $UserList[$row['recordid']] = FormatContactNameForList(array('data' => $row));
            }

            $field = Forms_SelectFieldFactory::createSelectField('dash_users_'.$this->Recordid, 'DAS', $this->LinkedUsers, '', true);
            $field->setCustomCodes($UserList);
            $field->setSuppressCodeDisplay();
            $field->setIgnoreMaxLength();
            $HTML .= GetDivFieldHTML('Users', $field->getField());
        }

        $HTML .= '</ol></form>';

        return $HTML;
    }
// ---------------------
//@codeCoverageIgnoreEnd
// ---------------------

    /**
    * @desc Takes the information currently set in the dashboard object and updates the database with it. Sets values in the main dashboard table (reports_dashboards) and
    * in the dashboards publishing table (reports_dashboards_links)
    */
    function UpdateDashboardRecord()
    {
        $Dash_graphs_per_row = (intval($this->Data['dash_graphs_per_row']) == 0 ? null : intval($this->Data['dash_graphs_per_row']));

        DatixDBQuery::PDO_query('UPDATE reports_dashboards SET dash_name = :dash_name, dash_graphs_per_row = :dash_graphs_per_row WHERE recordid = :recordid', array('dash_name' => $this->Data['dash_name'], 'dash_graphs_per_row' => $Dash_graphs_per_row, 'recordid' => $this->Recordid));

        DatixDBQuery::PDO_query('DELETE FROM reports_dashboards_links WHERE dash_id = :recordid', array('recordid' => $this->Recordid));

        foreach ($this->LinkedProfiles as $ProfileID)
        {
            DatixDBQuery::PDO_build_and_insert('reports_dashboards_links', array('dash_id' => $this->Recordid, 'dash_link_type' => 'PROFIL', 'dash_link_id' => $ProfileID));
        }
        foreach ($this->LinkedGroups as $GroupID)
        {
            DatixDBQuery::PDO_build_and_insert('reports_dashboards_links', array('dash_id' => $this->Recordid, 'dash_link_type' => 'GROUP', 'dash_link_id' => $GroupID));
        }
        foreach ($this->LinkedUsers as $UserID)
        {
            DatixDBQuery::PDO_build_and_insert('reports_dashboards_links', array('dash_id' => $this->Recordid, 'dash_link_type' => 'USER', 'dash_link_id' => $UserID));
        }
    }

// ---------------------
//@codeCoverageIgnoreStart
// ---------------------
    /**
    * @desc Constructs the HTML for a given column and returns it.
    *
    * @param int $ColumnId The column to be represented - indicated by its key in the ColumnWidgets parameter
    *
    * @return string The HTML representing this dashboard.
    */
    function GetColumnHTML($ColumnId)
    {
        $HTML = '<div class="column" id="column_'.$this->Recordid.'_'.$ColumnId.'" style="width: 32%">';

        if (is_array($this->ColumnWidgets[$ColumnId]))
        {
            foreach ($this->ColumnWidgets[$ColumnId] as $Widget)
            {
                try
                {
                    $Widget->InitialiseWidget($this);
                    $HTML .= $Widget->GetWidgetHTML();
                }
                catch(ReportNotFoundException $exception)
                {
                    $HTML .= '<div>'.$exception->getMessage().'</div>';
                }

            }
        }

        $HTML .= '</div>';

        return $HTML;
    }
// ---------------------
//@codeCoverageIgnoreEnd
// ---------------------

    /**
    * @desc Takes an array of widget ids and saves the order and columns back to the reports_dash_links table
    *
    * @param array $Orders An array of widget ids in the form $Orders[column][order] = widget id.
    */
    function SaveWidgetOrders($Orders)
    {
        foreach ($Orders as $Column => $OrderArray)
        {
            foreach ($OrderArray as $Order => $WidgetId)
            {
                if ($WidgetId)
                {
                    $sql = 'UPDATE reports_dash_links SET dlink_order = '.$Order.', dlink_column = '.$Column.' WHERE recordid = '.$WidgetId.' and dlink_dash_id = '.$this->Recordid;
                    DatixDBQuery::PDO_query($sql);
                }
            }
        }
    }

    /**
    * @desc Saves a new default dashboard for the current user.
    */
    function SaveAsNewDashboard()
    {
        //make sure we have a name
        if (!$this->Data['dash_name'])
        {
            $this->Data['dash_name'] = 'Dashboard for user #'. $this->Data['dash_owner_id'];
        }

        $this->Recordid = DatixDBQuery::PDO_build_and_insert('reports_dashboards', array('dash_owner_id' => $this->Data['dash_owner_id'], 'dash_name' => $this->Data['dash_name']));
    }

    /**
    * @desc Saves a new widget onto the current dashboard.
    *
    * @param array $Params Array of parameters
    * @param string $Params['report_id'] The id of the contents of the widget being saved - to go into the dlink_package_id field
    */
    function AddWidget($Params)
    {
        //Deal with situation where user does not have a dashboard yet:
        if (!$this->Recordid)
        {
            $this->SaveAsNewDashboard();
        }

        //check user can add to this dashboard.
        if (!$this->CurrentUserIsOwner())
        {
            return array('success' => false, 'message' => 'You do not have permission to edit this dashboard');
        }

        $sql = 'SELECT COALESCE(MAX(dlink_order)+1, 1) as dlink_order FROM reports_dash_links
                WHERE dlink_dash_id = '.$this->Recordid;

        $report = DatixDBQuery::PDO_fetch($sql);

        //check this widget does not already exist.
        $sql = 'SELECT count(*) as check_dash_link FROM reports_dash_links
                WHERE dlink_dash_id = '.$this->Recordid.'
                AND dlink_package_id = '.$Params['report_id'];

        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row["check_dash_link"] != 0)
        {
            return array('success' => false, 'message' => 'Dashboard already contains that report');
        }

        $sql = 'INSERT INTO reports_dash_links (dlink_dash_id, dlink_package_id, dlink_title, dlink_order)
                VALUES ('.$this->Recordid.', '.$Params['report_id'].', \'Widget not yet named\', '.$report['dlink_order'].')';

        $WidgetID = DatixDBQuery::PDO_build_and_insert('reports_dash_links', array('dlink_dash_id' => $this->Recordid, 'dlink_package_id' => $Params['report_id'], 'dlink_title' => ($Params['report_name'] ? $Params['report_name'] : '&lt;untitled&gt'), 'dlink_order' => $report['dlink_order'], 'dlink_column' => 0));

        if ($WidgetID)
        {
            require_once 'Source/dashboards/widgetclass.php';
            $Widget = new Widget($WidgetID);
            $Widget->InitialiseWidget($this);
            $this->ColumnWidgets[0][] = $Widget;
            $this->WidgetArray[$WidgetID] = $Widget;
            return array('success' => true, 'widget_id' => $WidgetID);
        }
        else
        {
            return array('success' => false, 'message' => 'Dashboard widget could not be added to the database');
        }
    }

    /**
    * @desc Removes the "default" flag from the user's current dashboard and sets this one as default in its place.
    */
    function SetAsDefault()
    {
        SetUserParm($_SESSION['login'], 'DEFAULT_DASHBOARD', $this->Recordid);
        $_SESSION['Globals']['DEFAULT_DASHBOARD'] = $this->Recordid;
    }

    /**
    * @desc Checks whether the currently logged in user is the owner of this dashboard.
    *
    * @return bool True if the current user is the owner, false otherwise.
    */
    function CurrentUserIsOwner()
    {
        return ($this->Data['dash_owner_id'] == $_SESSION["contact_login_id"]);
    }

    function currentUserCanAdd()
    {
        return $_SESSION['CurrentUser']->canAddRecords('DAS', \src\framework\registry\Registry::getInstance());
    }

    /**
    * @desc Deletes the dashboard from the database and unlinks any widgets that were on it.
    */
    function DeleteDashboard()
    {
        DatixDBQuery::static_beginTransaction(); // in case of a failure we don't want to end up with orphan data.

        //get rid of widgets
        $sql = 'DELETE FROM reports_dash_links WHERE dlink_dash_id = '.$this->Recordid;
        $result = DatixDBQuery::PDO_query($sql);

        if ($result !== false)
        {
            $sql = 'DELETE FROM reports_dashboards_links WHERE dash_id = '.$this->Recordid;
            $result = DatixDBQuery::PDO_query($sql);
        }

        if ($result !== false)
        {
            $sql = 'DELETE FROM reports_dashboards WHERE recordid = '.$this->Recordid;
            $result = DatixDBQuery::PDO_query($sql);
        }

        //todo - reassign default dashboard

        if ($result === false)
        {
            DatixDBQuery::static_rollBack();
            return false;
        }

        DatixDBQuery::static_commit();
        return true;
    }

    /**
    * Sets the column width of this dashboard based on the screen width/height
    */
    public function setColWidth()
    {
        $this->ColWidth = ($this->ScreenWidth - (60 + (10 * $this->NumCols))) / $this->NumCols; //designed by trial and error, rather than logic - which is why it doesn't work very well
    }

}
?>