<?php
use src\security\Escaper;

function ReportsAdminAction()
{
    global $yySetLocation, $scripturl;

    $form_action = Sanitize::SanitizeString($_REQUEST['form_action']);
    $rep_id = Sanitize::SanitizeInt($_REQUEST['rep_id']);
    $module = Sanitize::SanitizeString($_REQUEST['module']);

    $showExcluded = ($_REQUEST['show_excluded'] !== null ? Sanitize::SanitizeString($_REQUEST['show_excluded']) : $_SESSION['reports_administration:show_excluded']);

    // Default value is hide forms/fields
    if (null === $showExcluded)
    {
        $showExcluded = '0';
    }

    $_SESSION['reports_administration:show_excluded'] = $showExcluded;

    if($rep_id && !$module)
    {
        $report = (new \src\reports\model\report\ReportModelFactory)->getMapper()->findByBaseListingReportID($rep_id);
        $module = $report->module;
    }

    switch($form_action)
    {
        case 'edit':
            ReportsAdminEdit($rep_id, $module);
            break;
        case 'delete':
            ReportsAdminDelete($rep_id, $module);
            break;
        case 'new':
            ReportsAdminNew($module);
            break;
        case 'cancel':
            $yySetLocation = "$scripturl?module=ADM";
            redirectexit();
            break;
        default:
            //error
            $yySetLocation = "$scripturl?module=ADM";
            redirectexit();
            break;
    }
}

function ReportsAdminEdit($rep_id, $module)
{
    ReportsAdminForm($rep_id, $module, 'edit');
}

function ReportsAdminNew($module)
{
    ReportsAdminForm('', $module, 'new');
}

function ReportsAdminForm($rep_id, $module, $form_action, $error = '', $rep = array())
{
    global $scripturl;

    LoggedIn();

    getPageTitleHTML(array(
         'title' => _tk('reports_administration_title'),
         'module' => $module
         ));

    $ButtonGroup = new ButtonGroup();
    $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'onclick' => 'selectAllMultiCodes();document.forms[0].rbWhat.value=\'save\';document.frmReportDocument.submit()', 'action' => 'SAVE'));
    $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'if(confirm(\'Are you sure you want to cancel?\')){javascript:document.forms[0].rbWhat.value=\'cancel\';document.frmReportDocument.submit()}', 'action' => 'CANCEL'));

    if ($rep_id)
    {
        $ButtonGroup->AddButton(array('id' => 'btnDelete', 'name' => 'btnDelete', 'label' => _tk('delete_base_report'), 'onclick' => 'deleteBaseReport(); return false', 'action' => 'DELETE'));
        $ButtonGroup->AddButton(array('id' => 'btnExport', 'name' => 'btnExport', 'label' => _tk('export_report_design'), 'onclick' => 'SendTo(\'' . $scripturl . '?action=exportabasereport&rep_id=' . urlencode($rep_id) . '\');', 'action' => 'BACK', 'icon' => 'images/icons/toolbar-export.png'));
    }

    GetSideMenuHTML(array('module' => 'ADM', 'buttons' => $ButtonGroup, 'action' => 'reportsadminaction'));

    template_header();

    if($rep_id)
    {
        //////////////////
        // Get report
        //////////////////
        $sql = "SELECT wr.title AS rep_name, wr.merge_repeated_values, re.recordid, re.rep_format, re.rep_module, re.rep_type, re.rep_file, re.updateid,
                       re.rep_fields, re.rep_subtype
                FROM web_reports AS wr
                JOIN reports_extra AS re
                ON wr.base_listing_report = re.recordid
                WHERE re.recordid = $rep_id";

        $request = db_query($sql);
        if (($rep = db_fetch_array($request)) == "")
            fatal_error("Report number $rep_id does not exist");
    }

    if($error)
    {
		$rep['error'] = true;
        $rep['rep_type'] = $_POST['rep_type'];
        $rep['rep_type_code'] = $_POST['rep_type_code'];
        $rep['rep_name'] = $_POST['rep_name'];
    }

    $FormField = new FormField();
    $FormField->MakeDivCheckbox('showadditional', '', $showAdditional, 'showadditional', true);

    $FormArray = array (
          "Parameters" => array("Panels" => "True", "Condition" => false),
          "name" => array(
              "Title" => _tk('listing_report_settings_title'),
              "MenuTitle" => "Reports admin",
              "NewPanel" => true,
              "Function" => "MakeReportsAdminEditPanel",
              "Rows" => array()
          ),
          "view" => array(
              "Title" => _tk('listing_report_view_settings_title'),
              "Function" => "MakeReportsViewAdminEditPanel",
              "Rows" => array()
          ),
          "additional" => array(
              "Title" => 'Additional options',
              "Function" => "MakeAdditionalOptionsPanel",
              "Rows" => array()
          ),
          "packages" => array(
              "Function" => "MakePackagedReportsList",
              "NotModes" => array("new"),
              "Rows" => array()
          )
    );

    echo '
        <form enctype="multipart/form-data" method="post" name="frmReportDocument" id="frmReportDocument" id="frmReportDocument" action="'
          . $scripturl . '?action=reportsadmineditsave">
            <input type="hidden" id="form_action" name="form_action" value="' . Escape::EscapeEntities($form_action) . '" />
            <input type="hidden" id="rep_type_code" name="rep_type_code" value="R" />';


    $RepTable = new FormTable($form_action);
    $RepTable->MakeForm($FormArray, $rep, $module);

    $RepTable->MakeTable();
    echo $RepTable->GetFormTable();

    echo $ButtonGroup->getHTML();

    echo '</form>';

    if (!$_GET["print"])
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $RepTable);
    }

    footer();

    ?>

<script type="text/javascript">

function deleteBaseReport()
{
    if(confirm('Delete Base Report?\nPlease confirm you wish to delete this report'))
	{
		$('rbWhat').value = 'delete';
        $('frmReportDocument').submit();
	}
}

</script>

<?php
    obExit();

}

function ReportsAdminSaveAction()
{
    global $scripturl, $yySetLocation;

    $module = $_POST['module'];

    if ($_POST["rbWhat"] == 'cancel')
    {
        $yySetLocation = "$scripturl?action=listlistingreports&module=$module";
        redirectexit();
    }
    elseif ($_POST["rbWhat"] == 'delete')
    {
        ReportsAdminDelete();
    }
    elseif ($_POST["rbWhat"] == 'deletepackage')
    {
        ReportsAdminDeletePackaged();
    }
    else
    {
        ReportsAdminSave();
    }

    $yySetLocation = "$scripturl?action=listlistingreports&module=$module";
    redirectexit();
}

function ReportsAdminSave()
{
    global $scripturl, $yySetLocation;

    $rep_id = Sanitize::SanitizeInt($_POST["rep_id"]);
    $module = Sanitize::SanitizeString($_POST['module']);
    $rep_type = Sanitize::SanitizeString($_POST['rep_type']);
    $rep_type_code = Sanitize::SanitizeString($_POST['rep_type_code']);
    $rep_table = Sanitize::SanitizeString($_POST['rep_table']);
    $form_action = Sanitize::SanitizeString($_POST['form_action']);
    $rep = QuotePostArray(Sanitize::SanitizeRawArray($_POST));
    $CustomRepUploadFileName = '';

    $ErrorMark = '<font size="3" color="red"><b>*</b></font>';

    if ($_POST["rep_name"] == "")
    {
        AddSessionMessage('ERROR', 'You must enter a report name.');
        $error = true;
    }

    if ($_POST["rep_type_code"] == "")
    {
        AddSessionMessage('ERROR', 'You must select the report type.');
        $error = true;
    }

    if ($_FILES["userfile"]["name"] == "" && $_POST["rep_type"] == "CW" && !$rep_id)
    {
        AddSessionMessage('ERROR', 'You must select a file to upload.');
        $error = true;
    }

    if ($_FILES["userfile"]["name"] != "" && $_POST["rep_type"] == "CW" && !$rep_id)
    {
        $ext = \UnicodeString::strtolower(\UnicodeString::strrchr($_FILES["userfile"]["name"],'.'));
        if($ext != ".rpt")
        {
            AddSessionMessage('ERROR', 'The file must be a Crystal report template (.rpt).');
            $error = true;
        }
    }

    if ($_POST["limittotop"] == 'Y' && !is_numeric($_POST["limittotopnum"]))
    {
        AddSessionMessage('ERROR', 'Limit to top must be a numeric value');
        $error = true;
    }

    if ($_POST["rows"] == '' && $_POST["rep_type"] && $_POST["rep_type"] != "CW"  && $_POST["rep_type"] != "R" && $_POST["rep_type"] != "GW_GAUGE")
    {
        AddSessionMessage('ERROR', 'You must select rows for the report ');
        $error = true;
    }

    if ($_POST["listing_fields_list"] == '' && $_POST["rep_type"] == "R")
    {
        AddSessionMessage('ERROR', 'You must select fields for the report ');
        $error = true;
    }

    if ($error)
    {
        ReportsAdminForm($rep_id, $module, $form_action, $error, $rep);
        obExit();
    }

    if ($rep_id == "")
    {
        $rep_id = GetNextRecordID("reports_extra", true);
        $NewRecord = true;
    }

    if($_FILES["userfile"]["name"] != "")
    {
        $CustomRepUploadFileName = 'CUSTOMREP' . $rep_id . '.rpt';
        GetParms($_SESSION["user"]);
        $ToFile = Sanitize::SanitizeFilePath(GetParm('CUSTOM_REP_PATH', '', true) . "\\" . $CustomRepUploadFileName);

        if ($_FILES["userfile"]["size"] == "0" || !@move_uploaded_file( $_FILES["userfile"]["tmp_name"], $ToFile))
        {
            fatal_error("Cannot copy file to server.  Please contact your IT department.");
        }
    }
    
    $rep['merge_repeated_values'] = $rep['merge_repeated_values'] == 'Y' ? 1 : 0;

    // Update the web_reports table title so that titles work consistently with graphical reports
    $sql = "UPDATE web_reports
        SET
        title = N'$rep[rep_name]',
        merge_repeated_values = :merge_repeated_values
        WHERE base_listing_report = :rep_id
        ";

    DatixDBQuery::PDO_query($sql, array('merge_repeated_values' => $rep['merge_repeated_values'], 'rep_id' => $rep_id));

    $sql = "UPDATE reports_extra
        SET
        rep_type = '$rep_type',
        rep_name = N'$rep[rep_name]',
        rep_module = '$module',
        rep_dupdated = '" . date('Y-m-d H:i:s') . "',
        rep_updatedby = '$_SESSION[initials]',
        ";

    if ($rep_type_code == 'GW_SPC')
    {
        $sql .= "rep_subtype = '$rep[rep_subtype]', ";
    }

    if($rep_type == 'SC' || $rep_type == 'SS')
    {
        $sql .= "rep_option = 255, ";
    }

    if ($NewRecord)
    {
        $sql .= "rep_createdby = '$_SESSION[initials]',
                 rep_dcreated = '" . date('Y-m-d H:i:s') . "',
                 rep_private = 'N',
                 rep_app_show = 'WEB', 
                 rep_format = 'L|Arial,10,Y|Arial,10,Y|Arial,10,Y|2.54,2.54,2.54,2.54|50.00|0.00|0.00', ";
    }

    ////////////////////////////////////////////////////
    // set rep_fields
    ////////////////////////////////////////////////////

    if ($rep['rows'] != "" || $rep['columns'] != "")
    {
        if (!$NewRecord)
        {
            $sql_rep_fields = "SELECT rep_fields
                FROM reports_extra
                WHERE recordid = $rep_id";
            $result_rep_fields = db_query($sql_rep_fields);
            $row = db_fetch_array($result_rep_fields);
            $ReportFields = explode(',', $row['rep_fields']);
            $RowParts = explode(';', $ReportFields[0]);
            $ColumnParts = explode(';', $ReportFields[1]);
            // $rep_fields_parts[2] is the group by information
        }

        if ($rep['rows'] != "")
        {
            $RowParts[0] = $rep['rows'];
            $RowParts[1] = $rep_table;
            $ReportFields[0] = implode(';', $RowParts);

        }

        if ($rep['columns'] != "")
        {
            $ColumnParts[0] = $rep['columns'];
            $ColumnParts[1] = $rep_table;
            $ReportFields[1] = implode(';', $ColumnParts);
        }

        $rep_fields = implode(',', $ReportFields);

        $sql .= "rep_fields = '$rep_fields', ";
    }

    if ($CustomRepUploadFileName)
    {
        $sql .= "rep_file = '".$CustomRepUploadFileName."', ";
    }

    $sql .= " updateid = :updateid_new
        WHERE recordid = :rep_id
        AND (updateid = :updateid OR updateid IS NULL)";

    if (!DatixDBQuery::PDO_query($sql, array("updateid_new" =>  GensUpdateID(Sanitize::SanitizeString($_POST["updateid"])),
                                            "rep_id" => $rep_id,
                                            "updateid" => $_POST["updateid"])))
    {
        fatal_error("Could not save report" . $sql);
    }
    else
    {
        $message = "Report saved.";
    }

    if ($rep_type_code == 'R') //save listing report fields
    {
        $sql = 'DELETE FROM reports_formats WHERE rpf_rep_id = :rep_id';
        DatixDBQuery::PDO_query($sql, array('rep_id' => $rep_id));

        if (is_array($_POST['listing_fields_list']))
        {
            //If Standards module, work out appropriate view to use for fields included.
            if ($module == "STN")
            {
                $STNView = AutoFixSTNFieldViews($_POST['listing_fields_list']);
            }

            foreach ($_POST['listing_fields_list'] as $Order => $Field)
            {
                $FieldDetails = explode('|', $Field); //fields are passed as form.field

                //Check if standards module and if we need to use special views for reporting.
                if ($module == "STN" && !empty($STNView))
                {
                    if ($FieldDetails[1] == 'library_main.recordid')
                    {
                        $FieldDetails[0] = 'STNEVI';
                        $FieldDetails[1] = 'lib_id';
                    }

                    if ($FieldDetails[0] == 'standard_elements' || $FieldDetails[0] == 'standard_prompts' || $FieldDetails[0] == 'library_main'
                        || $FieldDetails[0] == 'STNEE' || $FieldDetails[0] == 'STNEPE' || $FieldDetails[0] == 'STNEP'
                        || $FieldDetails[0] == 'STNELE'|| $FieldDetails[0] == 'STNPRO' || $FieldDetails[0] == 'STNEVI')
                    {
                        $FieldDetails[0] = $STNView;
                    }
                }

                if ($FieldDetails[0] == 'UDF')
                {
                    $FieldInfo = new Fields_ExtraField(intval(str_replace('U0_', '', $FieldDetails[1]))); //UDF data is passed as U0_<id>
                    $FieldFormat['fmt_data_type'] = $FieldInfo->getFieldType();
                    $Title = $FieldInfo->getLabel();
                }
                else
                {
                    $FieldFormat = getFieldFormat($FieldDetails[1],$module,$FieldDetails[0]);
                    $FieldTable = Labels_FieldLabel::getTableViaSubform($FieldDetails[1], $FieldDetails[0]);
                    //This is kind of a mess now, since we need fmt_table for duplicate fields in different subforms. Needs a refactor once we get away from fformats.
                    $Title = Labels_FieldLabel::GetFieldLabel($FieldDetails[1], $FieldFormat['fmt_title'], $FieldTable, $module, false, $FieldFormat['fmt_table']);

                    if ($module == 'PAY')
                    {
                        $FieldDetails[0] = 'vw_payments';
                    }
                }

                switch ($FieldFormat['fmt_data_type'])
                {
                    case 'C':
                        $Width = 2;
                        $Description = 'Y';
                        break;
                    case 'T':
                        $Width = 4;
                        $Description = 'Y';
                        break;
                    case 'Y':
                        $Width = 1.5;
                        $Description = 'Y';
                        break;
                    case 'L':
                        $Width = 7;
                        $Description = 'N';
                        break;
                    case 'D':
                        $Width = 1.5;
                        $Description = 'N';
                        break;
                    default:
                        $Width = 2;
                        $Description = 'N';
                        break;
                }

                DatixDBQuery::PDO_build_and_insert('reports_formats',
                    array('rpf_rep_id' => $rep_id,
                    'rpf_field' => $FieldDetails[1],
                    'rpf_title' => $Title,
                    'rpf_form' => $FieldDetails[0],
                    'rpf_order' => $Order,
                    'rpf_module' => $module,
                    'rpf_section' => 'DETAIL',
                    'rpf_descr' => $Description,
                    'rpf_data_type' => $FieldFormat['fmt_data_type'],
                    'rpf_login' => $_SESSION['login'],
                    'rpf_total' => 'N',
                    'rpf_width' => $Width));
            }

            if (is_array($_POST['listing_orders_list']))
            {
                foreach ($_POST['listing_orders_list'] as $Order => $Field)
                {
                    $FieldDetails = explode('|', $Field); //fields are passed as form.field

                    $FieldFormat = getFieldFormat($FieldDetails[1],$module,$FieldDetails[0]);

                    if ($module == 'PAY')
                    {
                        $FieldDetails[0] = 'vw_payments';
                    }

                    DatixDBQuery::PDO_build_and_insert('reports_formats',
                        array('rpf_rep_id' => $rep_id,
                        'rpf_field' => $FieldDetails[1],
                        'rpf_title' => GetColumnLabel($FieldDetails[1]),
                        'rpf_form' => $FieldDetails[0],
                        'rpf_order' => $Order,
                        'rpf_module' => $module,
                        'rpf_section' => 'SORT',
                        'rpf_descr' => 'N',
                        'rpf_data_type' => $FieldFormat['fmt_data_type'],
                        'rpf_login' => $_SESSION['login'],
                        'rpf_total' => 'N',
                        'rpf_width' => 1,
                        ));
                }
            }

            if (is_array($_POST['listing_groups_list']))
            {
                foreach ($_POST['listing_groups_list'] as $Order => $Field)
                {
                    $FieldDetails = explode('|', $Field); //fields are passed as form.field

                    $FieldFormat = getFieldFormat($FieldDetails[1],$module,$FieldDetails[0]);

                    if ($module == 'PAY')
                    {
                        $FieldDetails[0] = 'vw_payments';
                    }

                    DatixDBQuery::PDO_build_and_insert('reports_formats',
                        array('rpf_rep_id' => $rep_id,
                        'rpf_field' => $FieldDetails[1],
                        'rpf_title' => GetColumnLabel($FieldDetails[1]),
                        'rpf_form' => $FieldDetails[0],
                        'rpf_order' => $Order,
                        'rpf_module' => $module,
                        'rpf_section' => 'GROUP',
                        'rpf_descr' => 'N',
                        'rpf_data_type' => $FieldFormat['fmt_data_type'],
                        'rpf_login' => $_SESSION['login'],
                        'rpf_total' => 'N',
                        'rpf_width' => 1,
                        ));
                }
            }
        }

        // Save group by additional options for claims
        if ($module == 'CLA')
        {
            $noGroupBys = count($_POST['listing_groups_list']);
            
            if ($_POST['cla_dclaim_checkbox'] == 'on')
            {
                DatixDBQuery::PDO_build_and_insert('reports_formats', array(
                        'rpf_rep_id'      => $rep_id,
                        'rpf_field'       => 'cla_dclaim',
                        'rpf_title'       => GetColumnLabel('cla_dclaim'),
                        'rpf_form'        => 'claims_main',
                        'rpf_order'       => $noGroupBys++,
                        'rpf_module'      => 'CLA',
                        'rpf_section'     => 'GROUP',
                        'rpf_descr'       => 'N',
                        'rpf_data_type'   => 'D',
                        'rpf_login'       => $_SESSION['login'],
                        'rpf_total'       => 'N',
                        'rpf_width'       => 1,
                        'rpf_date_format' => $_POST['cla_dclaim_group_by_options']
                    )
                );
            }

            if ($_POST['pol_start_date_checkbox'] == 'on')
            {
                DatixDBQuery::PDO_build_and_insert('reports_formats', array(
                        'rpf_rep_id'      => $rep_id,
                        'rpf_field'       => 'start_date',
                        'rpf_title'       => GetColumnLabel('start_date'),
                        'rpf_form'        => 'CLAPOL',
                        'rpf_order'       => $noGroupBys++,
                        'rpf_module'      => 'CLA',
                        'rpf_section'     => 'GROUP',
                        'rpf_descr'       => 'N',
                        'rpf_data_type'   => 'D',
                        'rpf_login'       => $_SESSION['login'],
                        'rpf_total'       => 'N',
                        'rpf_width'       => 1,
                        'rpf_date_format' => $_POST['pol_start_date_group_by_options']
                    )
                );
            }
        }
    }

    //need to check that there is a linked web_reports record
    $web_report_id = DatixDBQuery::PDO_fetch('SELECT recordid FROM web_reports WHERE base_listing_report = :base_listing_report', array('base_listing_report' => $rep_id), \PDO::FETCH_COLUMN);

    if (!$web_report_id)
    {
        $report = (new \src\reports\model\report\ReportModelFactory)->getEntityFactory()->createObject(
            array(
                'title' => $_POST["rep_name"],
                'base_listing_report' => $rep_id,
                'module' => $module,
                'type' => \src\reports\model\report\Report::LISTING,
                'merge_repeated_values' => $rep['merge_repeated_values']
            ));

        (new \src\reports\model\report\ReportModelFactory)->getMapper()->save($report);

        $web_report_id = $report->recordid;
    }

    $yySetLocation = "$scripturl?action=reportsadminaction&rep_id=$rep_id&module=$module&form_action=edit";

    if ($message)
    {
        $yySetLocation .= "&message=$message";
    }

    AddSessionMessage('INFO', 'Report saved.');

    redirectexit();

}

function ReportsAdminDelete()
{
    global $scripturl, $yySetLocation;

    LoggedIn();

    $rep_id = Sanitize::SanitizeInt($_POST['rep_id']);
    $module = Sanitize::SanitizeString($_POST['module']);

    db_query("DELETE FROM report_parameters WHERE rpar_report_id = $rep_id");
    $sql = "DELETE FROM reports_extra WHERE recordid = $rep_id";

    if (!db_query($sql))
    {
        fatal_error("Could not delete report");
    }
    else
    {
        //The listing report should be linked to a web_report record, so we need to clean that record up here too.
        $web_report = (new \src\reports\model\report\ReportModelFactory)->getMapper()->findByBaseListingReportID($rep_id);

        if ($web_report instanceof \src\reports\model\report\Report)
        {
            (new \src\reports\model\report\ReportModelFactory)->getMapper()->delete($web_report);
        }

        AddSessionMessage('INFO', 'Report has been deleted.');
    }

    $yySetLocation = "$scripturl?action=listlistingreports&module=$module";
    redirectexit();
}

function GetReportFileDirectory($filename)
{
    GetParms($_SESSION["user"]);
    $doc_dir = GetParm('CUSTOM_REP_PATH', '', true) . "\\" . $filename;

    return $doc_dir;
}

function GetListingReportFieldsArray($ListingFields, $Module, $GroupBySubform = false)
{
    global $ModuleDefs, $FieldDefsExtra;

    $ListingFieldsArray = array();

    foreach ($ListingFields as $ListingField)
    {
        if ($ListingField['subform'] == 'UDF')
        {
            $udfField = new Fields_ExtraField(intval(str_replace('U0_', '', $ListingField['field'])));
            $ColName = $udfField->getLabel();
        }
        else if ($Module == 'CLA' && \UnicodeString::substr($ListingField['field'], 0, 9) == 'pay_type_')
        {
            $ColName = Labels_FieldLabel::GetFieldLabel($ListingField['field']);
        }
        else if (in_array($Module, array('CQO', 'CQP', 'CQS')) && preg_match('/tier_(\d)/u', $ListingField['field']))
        {
            include_once 'Source/classes/Listings/CQCListing.php';

            $ListingObj = new CQCListing($Module);
            $ColName = $ListingObj->GetColumnLabel($ListingField['field'],'');
        }
        else
        {
            $ColName = Labels_FieldLabel::getFieldFormatsLabelByForm($ListingField['field'], $ListingField['subform']);
        }

        if (isset($FieldDefsExtra[$Module][$ListingField['field']]['SubmoduleSuffix']))
        {
            $ColName .= ' '.$FieldDefsExtra[$Module][$ListingField['field']]['SubmoduleSuffix'];
        }

        if ($GroupBySubform)
        {
            $ListingFieldsArray[$ListingField['subform']][$ListingField['subform'].'|'.$ListingField['field']] = $ColName;
        }
        else
        {
            $ListingFieldsArray[$ListingField['subform'].'|'.$ListingField['field']] = $ColName;
        }
    }

    return $ListingFieldsArray;
}

/**
 * THIS IS STILL USED TO CREATE LISTING REPORTS, BUT NOTHING ELSE
 */
function MakeReportsAdminEditPanel($rep, $FormType, $module)
{
    global $ModuleDefs, $FieldDefs, $FieldDefsExtra, $MinifierDisabled;

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $ReportType = array('R' => 'Listing');

    $showExcluded = ($_SESSION['reports_administration:show_excluded'] !== null ? $_SESSION['reports_administration:show_excluded'] : '0');

    ?>
    <script language="javascript" type="text/javascript">

            AlertAfterChange = true;

            function oc(a)
            {
                var o = {};

                for(var i = 0; i < a.length; i++)
                {
                    o[a[i]] = '';
                }

                return o;
            }
    </script>


<?php

    if ($module == 'PAY' || $module == 'LOC' || !$ModuleDefs[$module]['VIEW'])
    {
        $fformats_table = $ModuleDefs[$module]['TABLE'];
    }
    else
    {
        $fformats_table = $ModuleDefs[$module]['VIEW'];
    }

    //////////////////////////////////////////////////////
    // Get reportable fields
    //////////////////////////////////////////////////////
    $sql = "SELECT fmt_field, fmt_new_label, fmt_data_type
        FROM vw_field_formats
        WHERE fmt_module = '$module'
        AND fmt_table = '$fformats_table'
        AND fmt_data_type in ('C','D','T')
        AND fmt_field NOT IN ('".implode("','",getNewHardCodedReportExcludedFields())."')
    ";

    if ($showExcluded == '0')
    {
        $sql .= "
            AND fmt_table+'.'+fmt_field NOT IN
            (SELECT rex_field FROM report_exclusions
                WHERE rex_module = '$module'
            )
        ";
    }

    $sql .= "
        UNION
        SELECT ('U0_' + CAST(udf_fields.recordid AS VARCHAR(32))) as fmt_field, fld_name as fmt_new_label,
        fld_type as fmt_data_type
        FROM udf_fields
        LEFT JOIN udf_mod_link ON udf_mod_link.uml_id = udf_fields.recordid
        WHERE fld_type in ('C','D','T','Y','M','N') AND (udf_mod_link.uml_module = '$module' OR udf_mod_link.uml_module IS NULL OR udf_mod_link.uml_module = '')
    ";

    if ($showExcluded == '0')
    {
        $sql .= "
            AND '$fformats_table.'+('UDF_' + CAST(udf_fields.recordid AS VARCHAR(32))) NOT IN
            (SELECT rex_field FROM report_exclusions
                WHERE rex_module = '$module')
            ORDER BY 2 ASC
        ";
    }

    $result = db_query($sql);

    while ($row = db_fetch_array($result))
    {
        if (!$FieldDefs[$module][$row["fmt_field"]]['BlockFromReports'])
        {
            $Fields[$row["fmt_field"]] = Labels_FieldLabel::GetFieldLabel($row["fmt_field"], $row["fmt_new_label"], $fformats_table).((isset($FieldDefsExtra[$module][$row["fmt_field"]]['SubmoduleSuffix'])) ? ' '.$FieldDefsExtra[$module][$row["fmt_field"]]['SubmoduleSuffix'] : '');

            if ($row["fmt_data_type"] == 'D')
            {
                $datefields[] = "fieldName == '$row[fmt_field]'";
            }
        }
    }

    $fnIsDateField = '
function isDateField(fieldName) {';

    if (is_array($datefields))
    {
        $fnIsDateField .= '
        if (' . implode(' || ', $datefields) . ')
        {
            return true;
        }';
    }

    $fnIsDateField .= '
    return false;
}   ';

    echo '
<script language="JavaScript" type="text/javascript">';
    echo $fnIsDateField;
    echo '
function showDateOptions(selectelement, divelement, dateoptionselement) {
    if (isDateField(selectelement.value))
    {
        divelement.style.display = "";
    }
    else
    {
        divelement.style.display = "none";
        dateoptionselement.value = "";
    }
}
</script>';

echo '
<input type="hidden" id="rep_id" name="rep_id" value="' . Sanitize::SanitizeInt($rep['recordid']) . '" />
<input type="hidden" id="module" name="module" value="' . Escape::EscapeEntities($module) . '" />
<input type="hidden" id="rep_type" name="rep_type" value="R" />
<input type="hidden" id="updateid" name="updateid" value="' . Escape::EscapeEntities($rep['updateid']) . '" />
<input type="hidden" id="rbWhat" name="rbWhat" value="Save" />
<input type="hidden" id="rep_table" name="rep_table" value="' . Escape::EscapeEntities($fformats_table) . '" />';

    $FormField = new FormField();

    $FormField->MakeInputField('rep_name', 70, 256, $rep["rep_name"], "");
    echo GetDivFieldHTML('Name:', $FormField->GetField());

    if ($rep['CHANGED-rep_type_code'] == 1 && $rep['error'] == true && $FormType == 'new')
    {
    	$RptTypeCode = $rep['rep_type_code'];
    }

//    $mode = ($RptTypeCode != '' && $FormType != 'new') ? 'ReadOnly' : '';
//    $field = Forms_SelectFieldFactory::createSelectField('rep_type_code', $module, $RptTypeCode, $mode);
//    $field->setCustomCodes($ReportType);
//    $field->setOnChangeExtra('initDropdown(jQuery("#rep_type_code_title"));showTypeSection(jQuery("#rep_type_code_title").data("datixselect").field.val(), true)');
//    $field->setSuppressCodeDisplay();
//    echo GetDivFieldHTML('Report type:', $field->getField());

    if ($rep["rep_file"] != "")
    {
        $FormField->MakeCustomField($rep["rep_file"]);
        echo GetDivFieldHTML('Template file:', $FormField->GetField(),'','','rep_file_row');

        $FormField->MakeCustomField('<input name="userfile" type="file"  size="50"/>');
        echo GetDivFieldHTML('Import new template file (.rpt):', $FormField->GetField(),'','','userfile_row');
    }
    else
    {
        $FormField->MakeCustomField('<input name="userfile" type="file"  size="50"/>');
        echo GetDivFieldHTML('Template file to import (.rpt):', $FormField->GetField(),'','','userfile_row',true);
    }

    if ($module == 'PAY')
    {
        $fformats_table = $ModuleDefs[$module]['TABLE'];
    }
    else
    {
        $fformats_table = ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']);
    }
    //////////////////////////////////////////////////////
    // Get listing reportable fields
    //////////////////////////////////////////////////////
    $sql = "SELECT fmt_field as field, fmt_table as subform, sfm_title
        FROM vw_field_formats, subforms
        WHERE fmt_module = '$module'
        AND fmt_table+'.'+fmt_field NOT IN ('".implode("','",getNewHardCodedReportExcludedFields())."')
    ";

    if ($showExcluded == '0')
    {
        $sql .= "
            AND fmt_table+'.'+fmt_field NOT IN
            (SELECT rex_field FROM report_exclusions WHERE rex_module = '$module')
        ";
    }

    $sql .= "
        AND fmt_table = sfm_form
        AND sfm_module = fmt_module AND sfm_type like '%A%'
        UNION
        SELECT ('U0_' + CAST(udf_fields.recordid AS VARCHAR(32))) as field, 'UDF' as subform, 'Extra Fields' as sfm_title
        FROM udf_fields
        LEFT JOIN udf_mod_link ON udf_mod_link.uml_id = udf_fields.recordid
        WHERE (udf_mod_link.uml_module = '$module' OR udf_mod_link.uml_module IS NULL OR udf_mod_link.uml_module = '')
    ";

    if ($showExcluded == '0')
    {
        $sql .= "
            AND '$fformats_table.'+('UDF_' + CAST(udf_fields.recordid AS VARCHAR(32))) NOT IN
            (SELECT rex_field FROM report_exclusions
            WHERE rex_module = '$module')
        ";
    }

    $sql .= "ORDER BY 2 ASC";

    $UnsortedListingFields = DatixDBQuery::PDO_fetch_all($sql);

    if (in_array($module, array('CQO', 'CQP', 'CQS')))
    {
        $Tiers = DatixDBQuery::PDO_fetch_all('SELECT lti_depth FROM location_tiers', array(), PDO::FETCH_COLUMN);

        foreach ($Tiers as $TierNumber)
        {
            $UnsortedListingFields[] = array('field' => 'tier_'.$TierNumber, 'subform' => $ModuleDefs[$module]['VIEW'], 'sfm_title' => $ModuleDefs[$module]['NAME']);
        }
    }

    $CurrentListingFields = array();

    if ($rep['recordid'])
    {
        $sql = 'SELECT rpf_form as subform, rpf_field as field FROM reports_formats
                WHERE rpf_rep_id = :rep_id
                AND rpf_section = \'DETAIL\'
                ORDER BY rpf_order';
        $ListingFieldResults = DatixDBQuery::PDO_fetch_all($sql, array('rep_id' => $rep['recordid']));

        //payments needs to be saved with a table of "vw_payments", but we need to change it back here to ensure we don't get duplicate
        //entries in the designer.
        if ($module == 'PAY')
        {
            foreach ($ListingFieldResults as $key => $details)
            {
                // Remove pay_vat RC field from Datixweb reports
                if ($details['field'] == 'pay_vat')
                {
                    unset($ListingFieldResults[$key]);
                    continue;
                }

                if ($details['subform'] == 'vw_payments')
                {
                    $ListingFieldResults[$key]['subform'] = 'payments';
                }
            }
        }

        $CurrentListingFields = GetListingReportFieldsArray($ListingFieldResults, $module);
    }
    else
    {
        if (is_array($rep["listing_fields_list"]))
        {
            foreach ($rep["listing_fields_list"] as $i => $key)
            {
                $field_detail = explode("|", $key);
                $ListingFieldResults[] = array("subform" => $field_detail[0], "field" => $field_detail[1]);
            }

            $CurrentListingFields = GetListingReportFieldsArray($ListingFieldResults, $module);
        }
    }

    $Subforms = array();

    foreach ($UnsortedListingFields as $ListingField)
    {
        $Subforms[$ListingField['subform']] = $ListingField['sfm_title'];
    }

    $ListingFields = GetListingReportFieldsArray($UnsortedListingFields, $module, true);

    if ($module == 'CLA')
    {
        $PaymentTypes = DatixDBQuery::PDO_fetch_all('SELECT code, description from code_fin_type', array(), PDO::FETCH_KEY_PAIR);

        foreach ($PaymentTypes as $code => $description)
        {
            $ListingFields['claims_main']['claims_main|pay_type_'.$code] = $description.' (Payment Summary)';
        }

        $ListingFields['claims_main']['claims_main|pay_type_PAYTOTAL'] = 'Total (Payment Summary)';
    }

    //payment fields are stored against the table, rather than the view
    $DefaultSubform = ($module == 'PAY' || $module == 'LOC' ? $ModuleDefs[$module]['TABLE'] : ($ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE']));

    $field = Forms_SelectFieldFactory::createSelectField('subforms', $module, $DefaultSubform, '');
    $field->setCustomCodes($Subforms);
    $field->setOnChangeExtra('jQuery(\'.subform_field_row\').hide();jQuery(\'#\'+jQuery(\'#subforms\').val()+\'_fields_row\').show()');
    $field->setSuppressCodeDisplay();

    echo '
    <li id="listing_field_select_row">
    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    ';

    echo '
    <tr>
    <td class="windowbg2" align="left" colspan="3">
    <b>Select fields to use on the report:</b>
    </td>
    </tr>';

    echo '
    <tr>
    <td class="windowbg2" align="left">
    <div><div style="float:left; padding-top:3px;">Subform:&nbsp;</div><div style="float:left"> '.$field->getField().'</div></div>
    </td>
    <td class="windowbg2" align="left">
    </td>
    <td class="windowbg2" align="left">
    Report fields:
    </td>
    </tr>
    <tr>
        <td class="windowbg2" align="center">
        ';

    foreach ($ListingFields as $Subform => $ListingFieldArray)
    {
        echo '
        <div id="'.$Subform.'_fields_row" class="subform_field_row" '.($Subform != $DefaultSubform ? 'style="display:none"' : '').'>
        <select id="'.$Subform.'_fields_list" multiple="multiple" size="12" style="width: 300px">';

        asort($ListingFieldArray);

        foreach ($ListingFieldArray as $Code => $Val)
        {
            $FieldName = explode('|', $Code);

            if (!array_key_exists($Code, $CurrentListingFields) && !$FieldDefs[$module][$FieldName[1]]['BlockFromReports'])
            {
                echo '<option value="' . $Code . '" source="'.$Subform.'">'.$Val.'</option>';
            }
        }

        echo '</select></div>';
    }

    echo '
        </td>
        <td class="windowbg2" align="center">
        <input type="button" value="Add     >>>"
            onclick="jQuery(\'#\'+jQuery(\'#subforms\').val()+\'_fields_list option:selected\').appendTo(\'#listing_fields_list\').attr(\'selected\', false);" /><br /><br />
        <input type="button" value="<<<Remove"
            onclick="var source_list_name = jQuery(\'#listing_fields_list option:selected\').attr(\'source\')+\'_fields_list\';jQuery(\'#listing_fields_list option:selected\').appendTo(\'#\'+source_list_name).attr(\'selected\', false); sortListBox(getElementById(source_list_name))"  />
        </td>
        <td class="windowbg2" align="center">
        <table>
        <tr>
        <td>
        <img src="Images/up_windowbg2.gif" border="0" onclick="moveUp(\'listing_fields_list\');"/> <br />
        <img src="Images/collapse_windowbg2.gif" border="0" onclick="moveDown(\'listing_fields_list\');"/>
        </td>
        <td>
        <select id="listing_fields_list" name="listing_fields_list[]" multiple="multiple" size="12" style="width: 300px">';
        foreach ($CurrentListingFields as $Code => $Val)
        {
            $CodeSplit = explode('|', $Code);
            echo '<option value="' . $Code . '" source="'.$CodeSplit[0].'">'.$Val.'</option>';
        }
        echo'
        </select>
        </td>
        </tr>
        </table>
        </td>
    </tr>';

    echo '
    </table>
    </li>';
}

function MakeReportsViewAdminEditPanel($rep)
{
    $FormField = new FormField();
    
    // default merge repeated values to on for new listing report designs
    if (empty($rep))
    {
        $rep['merge_repeated_values'] = true;
    }

    $boolCheckValue = filter_var($rep['merge_repeated_values'], FILTER_VALIDATE_BOOLEAN);

    $FormField->MakeDivCheckbox('merge_repeated_values', '', $boolCheckValue, 'merge_repeated_values', true);
    echo GetDivFieldHTML(_tk('merge_repeated_values'), $FormField->GetField());
}

function MakeAdditionalOptionsPanel($rep)
{
    global $ModuleDefs;

    $module = Sanitize::getModule($_REQUEST['module']);

    $showExcluded = ($_SESSION['reports_administration:show_excluded'] !== null ? $_SESSION['reports_administration:show_excluded'] : '0');

    if($rep['recordid'] && !$module)
    {
        $report = (new \src\reports\model\report\ReportModelFactory)->getMapper()->findByBaseListingReportID($rep['recordid']);
        $module = $report->module;
    }

    //////////////////////////////////////////////////////
    // Get fields to order by
    // Can't currently group or order by UDFS
    // Added exclusion of fields cla_dclaim (Claim date) and start_date (Coverage start date)
    //////////////////////////////////////////////////////
    list ($groupBySubforms, $groupByListingFields) = getListingFields($module, $showExcluded, false);

    $CurrentListingGroupFields = array();

    if ($rep['recordid'])
    {
        $sql = 'SELECT rpf_form as subform, rpf_field as field FROM reports_formats
                WHERE rpf_rep_id = :rep_id
                AND rpf_section = \'GROUP\'
                ORDER BY rpf_order';
        $ListingFieldResults = DatixDBQuery::PDO_fetch_all($sql, array('rep_id' => $rep['recordid']));

        $CurrentListingGroupFields = GetListingReportFieldsArray($ListingFieldResults, $module);
    }
    else
    {
        $rep['merge_repeated_values'] = true;
    }

    $field = Forms_SelectFieldFactory::createSelectField('groups_subforms', $module, (($module == 'PAY' || $module == 'LOC' || !$ModuleDefs[$module]['VIEW']) ? $ModuleDefs[$module]['TABLE'] : $ModuleDefs[$module]['VIEW']), '');
    $field->setCustomCodes($groupBySubforms);
    $field->setOnChangeExtra('jQuery(\'.subform_group_row\').hide();jQuery(\'#\'+jQuery(\'#groups_subforms\').val()+\'_groups_row\').show()');
    $field->setSuppressCodeDisplay();

    echo '
    <li id="listing_group_select_row">
    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    ';

    echo '
    <tr>
    <td class="windowbg2" align="left" colspan="3">
    <b>Select fields to group by:</b>
    </td>
    </tr>';

    echo '
    <tr>
    <td class="windowbg2" align="left">
    <div><div style="float:left; padding-top:3px;">Subform:&nbsp;</div><div style="float:left"> '.$field->getField().'</div></div>
    </td>
    <td class="windowbg2" align="left">
    </td>
    <td class="windowbg2" align="left">
    Current group columns:
    </td>
    </tr>
    <tr>
        <td class="windowbg2" align="center">
        ';

    foreach ($groupByListingFields as $Subform => $ListingFieldArray)
    {
        echo '
        <div id="'.$Subform.'_groups_row" class="subform_group_row" '.($Subform != (($module == 'PAY' || $module == 'LOC' || !$ModuleDefs[$module]['VIEW']) ? $ModuleDefs[$module]['TABLE'] : $ModuleDefs[$module]['VIEW']) ? 'style="display:none"' : '').'>
        <select id="'.$Subform.'_groups_list" multiple="multiple" size="12" style="width: 300px">';

        asort($ListingFieldArray);

        foreach ($ListingFieldArray as $Code => $Val)
        {
            if (!array_key_exists($Code, $CurrentListingGroupFields))
            {
                echo '<option value="' . $Code . '" source="'.$Subform.'">'.$Val.'</option>';
            }
        }

        echo '</select></div>';

        $FirstSubformDone = true;
    }

    echo '
        </td>
        <td class="windowbg2" align="center">
        <input type="button" value="Add     >>>"
            onclick="jQuery(\'#\'+jQuery(\'#groups_subforms\').val()+\'_groups_list option:selected\').appendTo(\'#listing_groups_list\').attr(\'selected\', false);" /><br /><br />
        <input type="button" value="<<<Remove"
           onclick="jQuery(\'#listing_groups_list option:selected\').appendTo(\'#\'+jQuery(\'#groups_subforms\').val()+\'_groups_list\').attr(\'selected\', false); sortListBox(getElementById(jQuery(\'#groups_subforms\').val()+\'_groups_list\'))"
        </td>
        <td class="windowbg2" align="center">
        <table>
        <tr>
        <td>
        <img src="Images/up_windowbg2.gif" border="0" onclick="moveUp(\'listing_groups_list\');"/> <br />
        <img src="Images/collapse_windowbg2.gif" border="0" onclick="moveDown(\'listing_groups_list\');"/>
        </td>
        <td>
        <select id="listing_groups_list" name="listing_groups_list[]" multiple="multiple" size="12" style="width: 300px">';

    foreach ($CurrentListingGroupFields as $Code => $Val)
    {
        $SplitCode = explode('|', $Code);

        if ($SplitCode[1] != 'cla_dclaim' && $SplitCode[1] != 'start_date')
        {
            echo '<option value="' . $Code . '" source="' . $SplitCode[0] . '">' . $Val . '</option>';
        }
    }

        echo'
        </select>
        </td>
        </tr>
        </table>
        </td>
    </tr>';

    echo '
    </table>
    </li>';

    if ($module == 'CLA')
    {
        $claimDateCheckbox  = '';
        $policyDateCheckbox = '';
        $claimDateFormat    = '';
        $policyDateFormat   = '';

        if (array_key_exists('claims_main|cla_dclaim', $CurrentListingGroupFields) === true)
        {
            $claimDateCheckbox = 'checked="checked"';

            $sql = '
                SELECT
                    rpf_date_format
                FROM
                    reports_formats
                WHERE
                    rpf_rep_id = :rep_id AND
                    rpf_section = \'GROUP\' AND
                    rpf_field = \'cla_dclaim\'
            ';

            $claimDateFormat = DatixDBQuery::PDO_fetch($sql, array('rep_id' => $rep['recordid']));
        }

        if (array_key_exists('CLAPOL|start_date', $CurrentListingGroupFields) === true)
        {
            $policyDateCheckbox = 'checked="checked"';

            $sql = '
                SELECT
                    rpf_date_format
                FROM
                    reports_formats
                WHERE
                    rpf_rep_id = :rep_id AND
                    rpf_section = \'GROUP\' AND
                    rpf_field = \'start_date\'
            ';

            $policyDateFormat = DatixDBQuery::PDO_fetch($sql, array('rep_id' => $rep['recordid']));
        }

        $field1 = Forms_SelectFieldFactory::createSelectField('cla_dclaim_group_by_options', 'CLA', $claimDateFormat['rpf_date_format'], '');
        $field1->setCustomCodes(array(
            \src\reports\model\report\Report::MONTH_AND_YEAR => _tk('month_and_year'),
            \src\reports\model\report\Report::YEAR           => _tk('year'),
            \src\reports\model\report\Report::FINANCIAL_YEAR => _tk('financial_year')
        ));
        $field1->setSuppressCodeDisplay();

        $field2 = Forms_SelectFieldFactory::createSelectField('pol_start_date_group_by_options', 'POL', $policyDateFormat['rpf_date_format'], '');
        $field2->setCustomCodes(array(
            \src\reports\model\report\Report::MONTH_AND_YEAR => _tk('month_and_year'),
            \src\reports\model\report\Report::YEAR           => _tk('year'),
            \src\reports\model\report\Report::FINANCIAL_YEAR => _tk('financial_year')
        ));
        $field2->setSuppressCodeDisplay();

        echo '
            <li id="listing_group_select_row">
                <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
                    <tr>
                        <td class="windowbg2" align="left" colspan="3"><b>Additional group options for Claims:</b></td>
                    </tr>
                    <tr>
                        <td class="windowbg2" align="left">
                            <input type="checkbox" id="cla_dclaim_checkbox" name="cla_dclaim_checkbox" class="listing_checkbox" '.$claimDateCheckbox.'>&nbsp;'.GetFieldLabel('cla_dclaim').'
                        </td>
                        <td class="windowbg2" align="left">
                            '.$field1->getField().'
                        </td>
                    </tr>
                    <tr>
                        <td class="windowbg2" align="left">
                            <input type="checkbox" id="pol_start_date_checkbox" name="pol_start_date_checkbox" class="listing_checkbox" '.$policyDateCheckbox.'>&nbsp;'.GetFieldLabel('start_date').'
                        </td>
                        <td class="windowbg2" align="left">
                            '.$field2->getField().'
                        </td>
                    </tr>
                </table>
            </li>
        ';
    }

    list ($orderBySubforms, $orderByListingFields) = getListingFields($module, $showExcluded);

    $CurrentListingOrderFields = array();

    if ($rep['recordid'])
    {
        $sql = 'SELECT rpf_form as subform, rpf_field as field FROM reports_formats
                WHERE rpf_rep_id = :rep_id
                AND rpf_section = \'SORT\'
                ORDER BY rpf_order';
        $ListingFieldResults = DatixDBQuery::PDO_fetch_all($sql, array('rep_id' => $rep['recordid']));

        $CurrentListingOrderFields = GetListingReportFieldsArray($ListingFieldResults, $module);
    }

    echo '
    <li id="listing_order_select_row" >
    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    ';

    echo '
    <tr>
    <td class="windowbg2" align="left" colspan="3">
    <b>Select fields to order by:</b>
    </td>
    </tr>';

    echo '
    <tr>
    <td class="windowbg2" align="left">
    <div></div>
    </td>
    <td class="windowbg2" align="left">
    </td>
    <td class="windowbg2" align="left">
    Current sort columns:
    </td>
    </tr>
    <tr>
        <td class="windowbg2" align="center">
        ';

        echo '
    <div id="listing_orders_row">
    <select id="listing_orders_list_source" multiple="multiple" size="12" style="width: 300px">';

        if ($module == 'PAY' || $module == 'LOC' || !$ModuleDefs[$module]['VIEW'])
        {
            $Table = $ModuleDefs[$module]['TABLE'];
        }
        else
        {
            $Table = $ModuleDefs[$module]['VIEW'];
        }

        if (isset($orderByListingFields[$Table]))
        {
            asort($orderByListingFields[$Table]);

            foreach ($orderByListingFields[$Table] as $Code => $Val)
            {
                if (!array_key_exists($Code, $CurrentListingOrderFields))
                {
                    echo '<option value="' . $Code . '">'.$Val.'</option>';
                }
            }
        }

        echo '</select></div>';

    echo '
        </td>
        <td class="windowbg2" align="center">
        <input type="button" value="Add     >>>"
            onclick="jQuery(\'#listing_orders_list_source option:selected\').appendTo(\'#listing_orders_list\').attr(\'selected\', false);" /><br /><br />
        <input type="button" value="<<<Remove"
            onclick="jQuery(\'#listing_orders_list option:selected\').appendTo(\'#listing_orders_list_source\').attr(\'selected\', false); sortListBox(getElementById(\'listing_orders_list_source\'));" />
        </td>
        <td class="windowbg2" align="center">
        <table>
        <tr>
        <td>
        <img src="Images/up_windowbg2.gif" border="0" onclick="moveUp(\'listing_orders_list\');"/> <br />
        <img src="Images/collapse_windowbg2.gif" border="0" onclick="moveDown(\'listing_orders_list\');"/>
        </td>
        <td>
        <select id="listing_orders_list" name="listing_orders_list[]" multiple="multiple" size="12" style="width: 300px">';

    foreach ($CurrentListingOrderFields as $Code => $Val)
    {
        echo '<option value="' . $Code . '">'.$Val.'</option>';
    }

        echo'
        </select>
        </td>
        </tr>
        </table>
        </td>
    </tr>';

    echo '
    </table>
    </li>';
}

function MakePackagedReportsList($rep, $FormType, $module)
{
    echo '<div class="new_titlebg"><div class="title_text_wrapper"><div class="section_title_group"><div class="section_title">'. _tk('packaged_reports_title') .'</div></div></div>';

    $web_report_id = DatixDBQuery::PDO_fetch('SELECT recordid FROM web_reports WHERE base_listing_report = :base_listing_report', array('base_listing_report' => $rep['recordid']), \PDO::FETCH_COLUMN);

    echo '
        <div class="title_rhs_container">
            <input type="button" id="btnNewPackage" name="btnNewPackage" value="'. _tk('new_packaged_report') .'" onClick="SendTo(\'app.php?action=designapackagedreport&web_report_id='.$web_report_id.'&module='.$module.'\');">
        </div>';

    echo '
        </div>';

    if ($web_report_id)
    {
        $sql = "SELECT web_packaged_reports.recordid, [name]
                FROM web_packaged_reports
                WHERE web_report_id = $web_report_id";

        $resultArray = DatixDBQuery::PDO_fetch_all($sql);
    }

    if (count($resultArray) == 0)
    {
        echo '<div class="page_title_background"><b>'. _tk('no_packaged_reports') .'</b></div>';
    }
    else
    {
        echo '<input type="hidden" name="rep_pack_id" value="" />';
        echo '
        <table class="gridlines" cellspacing="1" cellpadding="4" width="99%" align="center" border="0">
        <tr>
            <td class="windowbg" width="50%" align="left"><b>'. _tk('package_report_header_name') .'</b></td>
            <td class="windowbg" width="5%" align="center"></td>
            <td class="windowbg" width="5%" align="center"></td>
        </tr>   ';

        foreach ($resultArray as $row)
        {
            echo '
            <tr>
                <td class="windowbg2" width="50%" align="left"><a href="app.php?action=designapackagedreport&recordid='.$row['recordid'].'">'.$row["name"].'</a></td>
                <td class="windowbg2" width="5%" align="center"><a href="app.php?action=designapackagedreport&recordid='.$row['recordid'].'">[edit]</a></td>
                <td class="windowbg2" width="5%" align="center"><a href="javascript:if(confirm(\''. _tk('delete_packaged_report_q') .'\')){SendTo(\'app.php?action=deleteapackagedreport&recordid='.$row['recordid'].'\')}"> [delete]</a></td>
            </tr>   ';
        }

        echo '</table>';
    }
}

function RecordPermissionsREP($rep, $FormAction, $Module = "REP")
{
    include_once 'Source/security/SecurityRecordPerms.php';
    MakeRecordSecPanel('REP', 'reports_packaged', $rep['rep_pack_id'], $FormAction, 'both');
}

function PackagedReport()
{
    $rep_id = Sanitize::SanitizeInt($_GET['rep_id']);
    $rep_pack_id = Sanitize::SanitizeInt($_GET['rep_pack_id']);

    if ($rep_pack_id != "")
    {
        $FormType = 'edit';
        $sql = "SELECT recordid as rep_pack_id, rep_pack_name, rep_pack_where, updateid, rep_pack_tables, rep_pack_override_sec
            FROM reports_packaged
            WHERE recordid = $rep_pack_id";

        $result = db_query($sql);
        $rep = db_fetch_array($result);
        $rep['rep_id'] = $rep_id;
    }
    else
    {
        $FormType = 'new';

        if ($rep_id != "")
        {
            $sql = "SELECT recordid as rep_id, rep_name
                FROM reports_extra
                WHERE recordid = $rep_id";

            $result = db_query($sql);
            $rep = db_fetch_array($result);
        }
    }

    PackagedReportForm($rep, $FormType, Sanitize::getModule($_GET['module']), $error);
}

function PackagedReportForm($rep, $FormType, $module, $error)
{
    global $dtxtitle, $yySetLocation, $scripturl;

    LoggedIn();

    $dtxtitle = _tk('reports_administration_title');

    getPageTitleHTML(array(
         'title' => $dtxtitle,
         'module' => $module
         ));

    $ButtonGroup = new ButtonGroup();
    $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'onclick' => 'document.forms[0].rbWhat.value=\'save\';document.frmPackagedReportDocument.submit()', 'action' => 'SAVE'));
    $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'document.forms[0].rbWhat.value=\'cancel\';document.frmPackagedReportDocument.submit()', 'action' => 'CANCEL'));

    if ($rep['rep_pack_id'])
    {
        $ButtonGroup->AddButton(array('id' => 'btnDelete', 'name' => 'btnDelete', 'label' => _tk('delete_packaged_report'), 'onclick' => 'if(confirm(\''. _tk('delete_packaged_report_q') .'\')){document.forms[0].rbWhat.value=\'delete\';document.frmPackagedReportDocument.submit();}', 'action' => 'DELETE'));
    }

    GetSideMenuHTML(array('module' => 'ADM', 'buttons' => $ButtonGroup));

    template_header();

    $form_action = $FormType;
    $rep_pack_id = $rep['rep_pack_id'];
    $colspan = 4;

    $FormArray = array (
        "Parameters" => array("Panels" => "True", "Condition" => false),
        "name" => array(
            "Title" => _tk('packaged_report_settings_title'),
            "MenuTitle" => _tk('packaged_report_admin_menu_title'),
            "NewPanel" => true,
            "Function" => "MakePackagedReportsAdminEditPanel",
            "Rows" => array()
        ),
        "permissions" => array(
            "Title" => "Permissions",
            "NewPanel" => false,
            "NoFieldAdditions" => true,
            "Function" => "RecordPermissionsREP",
            "Condition" => $rep_pack_id,
            "NotModes" => array("new"),
            "Rows" => array()
        )
    );

    $RepTable = new FormTable($form_action);
    $RepTable->MakeForm($FormArray, $rep, $module);

    echo '
    <form enctype="multipart/form-data" method="post" id="frmPackagedReportDocument" name="frmPackagedReportDocument" action="'
            . $scripturl . '?action=packagedreportsadmineditsave">
    <input type="hidden" id="form_action" name="form_action" value="' . Escape::EscapeEntities($form_action) . '" />
    <input type="hidden" id="rep_pack_id" name="rep_pack_id" value="' . Escape::EscapeEntities($rep_pack_id) . '" />
    <input type="hidden" id="rep_id" name="rep_id" value="' . Escape::EscapeEntities($rep['rep_id']) . '" />
    <input type="hidden" id="rbWhat" name="rbWhat" value="" />
    <input type="hidden" id="module" name="module" value="'.Escape::EscapeEntities($module).'" />
    <input type="hidden" id="rep_pack_tables" name="rep_pack_tables" value="'.Escape::EscapeEntities($rep['rep_pack_tables']).'" />
    <input type="hidden" id="updateid" name="updateid" value="'.Escape::EscapeEntities($rep['updateid']).'" />';

    if ($error || $_GET['message'])
    {
        echo '<div class="form_error_wrapper">';

        if ($error)
        {
            echo '<div class="form_error_title">'. _tk('form_errors') .'</div>';

            foreach ($error as $thisError)
            {
                echo '<div class="form_error">'.$thisError.'</div>';
            }
        }

        echo '
                <div class="form_error">'.Escaper::escapeForHTML($_GET['message']).'</div>
            </div>
        ';
    }

    // Display the sections
    $RepTable->MakeTable();
    echo $RepTable->GetFormTable();

    echo $ButtonGroup->getHTML();

    echo '</form>';

    footer();

    obExit();
}

function MakePackagedReportsAdminEditPanel($rep, $FormType, $module)
{
    $FormField = new FormField();
    ?>
    <script language="javascript" type="text/javascript">
        function getQueryString(query_id, module)
        {
            new Ajax.Request
            (
                scripturl + '?action=httprequest&type=getQueryString&responsetype=json',
                {
                    method: 'post',
                    postBody:
                        '&query_id=' + query_id + '&module=' + module,
                    onSuccess: function(r)
                    {
                        var data = r.responseText.evalJSON();
                        $('rep_pack_where').value = data[0];
                        $('rep_pack_tables').value = data[1];
                    },
                    onFailure: function()
                    {
                        alert('Error retrieving query...');
                    }
                }

            );
        }

        AlertAfterChange = true;
    </script>
    <?php
    if ($rep['rep_pack_name'] == "")
    {
        $rep['rep_pack_name'] = $rep['rep_name'];
    }

    $ModuleWhere = $_SESSION[$module]["WHERE"];
    $QBE = (isset($_SESSION['packaged_report']) && $_SESSION['packaged_report']['success'] == true && $_SESSION['packaged_report']['rep_pack_id'] == $rep['rep_pack_id']);
    unset($_SESSION['packaged_report']);

    if (\UnicodeString::strpos($ModuleWhere, 'rep_approved') === false && $module == "INC")
    {
        $aiFlags = explode(",", GetParm("AI_FLAGS", "Y,I,C"));

        if (!$aiFlags)
        {
            $aiFlags = array("Y", "I", "C");
        }

        foreach ($aiFlags as $flag)
        {
            $flags[] = "'$flag'";
        }

        $aiFlags = implode(",", $flags);
    }

    $FormField->MakeInputField('rep_pack_name', 70, 256,$rep["rep_pack_name"], "");
    echo GetDivFieldHTML('Name:', $FormField->GetField());

    include_once 'Source/libs/QueriesBase.php';
    $saved_queries = get_saved_queries($module);

    if (!$saved_queries)
    {
        $saved_queries = array();
    }

    $field = Forms_SelectFieldFactory::createSelectField('rep_query', $module, '', '');
    $field->setCustomCodes($saved_queries);
    $field->setOnChangeExtra('initDropdown(jQuery("#rep_query_title"));getQueryString(jQuery("#rep_query_title").data("datixselect").field.val(), "'.$module.'")');
    $field->setSuppressCodeDisplay();
    echo GetDivFieldHTML(_tk('query_colon'), $field->getField());

    $FormField->MakeTextAreaField('rep_pack_where', 7, 70, '', ($QBE ? $ModuleWhere : $rep['rep_pack_where']), false);
    echo GetDivFieldHTML('SQL where clause:', $FormField->GetField());

    $SearchField = '
        <input type="hidden" name="searchwhere" id="searchwhere" value="' . Escape::EscapeEntities($ModuleWhere) . '" />
        <input type="hidden" name="CHANGED-rep_pack_where" id="CHANGED-rep_pack_where" value="0" />
        <input type="button" value="Use current search criteria"' .
            ' onclick="javascript:document.getElementById(\'rep_pack_where\').value = document.getElementById(\'searchwhere\').value;setChanged(\'CHANGED-rep_pack_where\');">&nbsp;
        <input type="submit" value="Define criteria using query by example" onclick="document.forms[0].rbWhat.value=\'search\';" />';

    if($QBE)
    {
        echo '
            <script language="javascript" type="text/javascript">
                setChanged(\'CHANGED-rep_pack_where\');
            </script>
        ';
    }

    $FormField->MakeCustomField($SearchField);
    echo GetDivFieldHTML('', $FormField->GetField());

    echo '<li class="new_windowbg field_div"><div style="float:left;padding:6px;width:25%;"><b>Override user security?</b><br />Set this option if you want the report viewed by users to include records to which they do not have access. Note: Setting this option will disable record links in the report</div>';
    $field = $FormField->MakeDivYesNo('rep_pack_override_sec', '', $rep['rep_pack_override_sec']);
    echo '<div style="width:70%;float:left;padding:4px">'.$field->GetField().'</div></li>';
}

function PackagedReportsAdminSaveAction()
{
    global $scripturl, $yySetLocation;

    $rep_pack = QuotePostArray($_POST);

    if ($_POST["rbWhat"] == 'cancel')
    {
        $yySetLocation = "$scripturl?action=reportsadminaction&rep_id=$rep_pack[rep_id]&module=$rep_pack[module]&form_action=edit";
        redirectexit();
    }
    elseif ($_POST["rbWhat"] == 'delete')
    {
        ReportsAdminDeletePackaged();
    }
    elseif ($_POST["rbWhat"] == 'save')
    {
        ReportsAdminSavePackaged();
    }
    elseif ($_POST["rbWhat"] == 'search')
    {
        ReportsAdminSavePackaged();
    }

    $yySetLocation = "$scripturl?action=listlistingreports";
    redirectexit();
}

function ReportsAdminSavePackaged()
{
    global $scripturl, $yySetLocation, $ModuleDefs;

    $rep_pack = Sanitize::SanitizeRawArray($_POST);
    $rep_pack_id = $rep_pack['rep_pack_id'];
    $module = $rep_pack['module'];

    $error = array();

    if ($rep_pack['rep_pack_name'] == "")
    {
        $error[] = "You must enter a report name.";
    }

    $Table = $rep_pack['rep_pack_tables'];

    if($Table == "")
    {
        $Table = $ModuleDefs[$module]['TABLE'];
    }

    $where = Sanitize::SanitizeRaw($_POST['rep_pack_where']);

    if ($where != "" && !ValidateWhereClause($where, $module))
    {
        $error[] = "'SQL where clause' contains a syntax error";
    }

    if ($error)
    {
        $rep_pack['rep_pack_where'] = $where;
        PackagedReportForm($rep_pack, $form_action, $module, $error);
        obExit();
    }

    if (!$rep_pack_id)
    {
        $rep_pack_id = GetNextRecordID("reports_packaged", true);
        $NewRecord = true;
    }

    $sql = "UPDATE reports_packaged
        SET
        rep_id = $rep_pack[rep_id],
        rep_pack_name = N'" . EscapeQuotes($rep_pack['rep_pack_name']) . "',
        rep_pack_where = '" . EscapeQuotes($rep_pack['rep_pack_where']) . "',
        rep_pack_tables = '" . EscapeQuotes($rep_pack['rep_pack_tables']) . "',
        rep_pack_dupdated = '" . date('Y-m-d H:i:s') . "',
        rep_pack_updatedby = '$_SESSION[initials]',
        rep_pack_override_sec = '" . EscapeQuotes($rep_pack['rep_pack_override_sec']) . "',
        ";

    if ($NewRecord)
    {
        $sql .= "rep_pack_createdby = '$_SESSION[initials]',
                 rep_pack_dcreated = '" . date('Y-m-d H:i:s') . "',";
    }

    $sql .= " updateid = '" . GensUpdateID($_POST["updateid"]) . "'
        WHERE recordid = $rep_pack_id";

    if (!db_query($sql))
    {
        fatal_error("Could not save packaged report" . $sql);
    }
    else
    {
        if ($_POST['rbWhat'] != 'search')
        {
            AddSessionMessage('INFO', _tk('packaged_report_saved'));
        }

        include_once 'Source/security/SecurityRecordPerms.php';

        $aParam = array(
            'module' =>'REP',
            'table' => 'reports_packaged',
            'recordid' => $rep_pack_id
        );

        $UserAccessList = GetRecordAccessUserList($aParam);
        $GroupAccessList = GetRecordAccessGroupList($aParam);
        $SomeoneLinked = false;

        if (is_array($UserAccessList))
        {
            if (count($UserAccessList) > 0)
            {
                $SomeoneLinked = true;
            }
        }

        if (is_array($GroupAccessList))
        {
            if (count($GroupAccessList) > 0)
            {
                $SomeoneLinked = true;
            }
        }

        if ($SomeoneLinked === false && $_POST['rbWhat'] != 'search')
        {
            AddSessionMessage('INFO', _tk('packaged_report_saved_permission_warning'));
        }
    }

    if ($_POST['rbWhat'] == 'search')
    {
        $_SESSION['packaged_report']['rep_id'] = $rep_pack['rep_id'];
        $_SESSION['packaged_report']['rep_pack_id'] = $rep_pack_id;
        $_SESSION['packaged_report']['module'] = $module;

        $yySetLocation = $scripturl;

        if (isset($ModuleDefs[$module]['SEARCH_URL']))
        {
            $yySetLocation .= "?" . $ModuleDefs[$module]['SEARCH_URL'] . '&module='.$module;
        }
        else if ($ModuleDefs[$module]['GENERIC'])
        {
            $yySetLocation .= '?action=search&module='.$module;
        }
    }
    else
    {
        $yySetLocation = "$scripturl?action=reportpackage&rep_id=$rep_pack[rep_id]&rep_pack_id=$rep_pack_id&module=$rep_pack[module]";
    }

    redirectexit();
}

function ReportsAdminDeletePackaged()
{
    global $scripturl, $yySetLocation;

    LoggedIn();

    $rep_pack = QuotePostArray($_POST);

    $sql = "DELETE FROM reports_packaged WHERE recordid = :recordid";

    if (!DatixDBQuery::PDO_query($sql, array("recordid" => $rep_pack["rep_pack_id"])))
    {
        fatal_error("Could not delete packaged report");
    }
    else
    {
        $message = "Packaged report has been deleted.";
    }

    $yySetLocation = "$scripturl?action=reportsadminaction&rep_id=$rep_pack[rep_id]&module=$rep_pack[module]&form_action=edit";

    if ($message)
    {
        $yySetLocation .= "&message=$message";
    }

    redirectexit();
}

function ExportReportDesign()
{
    global $ClientFolder;

    $recordid = Sanitize::SanitizeInt($_GET["rep_id"]);

    include_once "$ClientFolder/DATIXConfig.php";

    $client = new SoapClient("$ClientFolder/datixinterface.wsdl", array("trace"=> 1,"exceptions"=> 0));
    $report_xml = ($client->getReport($_SESSION["login"], "", $_SESSION["session_id"], "recordid=$recordid"));

    header("Content-Type: text/xml");
    header("Content-Disposition: attachment; filename=DATIXWebReportDesign_$recordid.xml");
    header("Content-Encoding: none");
    echo Sanitize::SanitizeRaw($report_xml);
    exit;
}
/**
* Automatically calculate which Standars view to use based on fields selected in a report
*
* @param string array $listing_fields_list Array of fields included in reports with subform details
*
* return string Standards View subform code.
*/
function AutoFixSTNFieldViews($listing_fields_list)
{
    $bELE = false;
    $bPRO = false;
    $bEVI = false;
    $STNView = "";

    foreach ($listing_fields_list as $Order => $Field)
    {
        $FieldDetails = explode('|', $Field);

        if ($FieldDetails[1] == 'library_main.recordid')
        {
            $FieldDetails[0] = 'STNEVI';
            $FieldDetails[1] = 'lib_id';
        }

        switch ($FieldDetails[0])
        {
            case "standard_elements":
            case "STNELE":
                 $bELE = true;
                 break;
            case "standard_prompts":
            case "STNPRO":
                 $bPRO = true;
                 break;
            case "library_main":
            case "STNEVI":
                 $bEVI = true;
                 break;
        }
    }

    if ($bELE == true && $bPRO == true && $bEVI == true)
    {
        $STNView = 'STNEPE';
    }
    else if ($bELE == true && $bPRO == true && $bEVI == false)
    {
        $STNView = 'STNEP';
    }
    else if ($bELE == true && $bPRO == false && $bEVI == true)
    {
        $STNView = 'STNEE';
    }
    else if ($bELE == TRUE && $bPRO == false && $bEVI == false)
    {
        $STNView = 'STNELE';
    }
    else if ($bELE == false && $bPRO == true && $bEVI == false)
    {
        $STNView = 'STNPRO';
    }
    else if ($bELE == false && $bPRO == false && $bEVI == true)
    {
        $STNView = 'STNEVI';
    }
    // There is no direct link from prompts to standards (only via elements), so re-use STNEPE view
    else if ($bELE == false && $bPRO == true && $bEVI == true)
    {
        $STNView = 'STNEPE';
    }

    return $STNView;
}

/**
 * @param string $module The module we are currently designing a report for
 * @param string $showExcluded if '0', we should not show fields that have been excluded in "Reporting Field Setup"
 * @param bool $includeNumericFields Whether or not numeric fields should be included in the set to be returned.
 * These fields are used for ordering but not grouping.
 */
function getListingFields($module, $showExcluded, $orderByFields = true)
{
    //The types of fields that can be used when ordering or grouping are coded, multicoded and date fields.
    $fieldTypes = array('C', 'D', 'T');

    if ($orderByFields)
    {
        $fieldTypes[] = 'N';
    }

    //////////////////////////////////////////////////////
    // Get fields to order by
    // Can't currently order by UDFS
    // Added exclusion of fields cla_dclaim (Claim date) and start_date (Coverage start date)
    //////////////////////////////////////////////////////
    $sql = "SELECT fmt_field as field, fmt_table as subform, sfm_title
        FROM vw_field_formats, subforms
        WHERE fmt_module = '$module'
        AND fmt_data_type in ('".implode("','", $fieldTypes)."')
    ";

    //These fields have been excluded in "Reporting Field Setup"
    if ('0' == $showExcluded)
    {
        $sql .= "
            AND fmt_field NOT IN
            (SELECT rex_field FROM report_exclusions)
        ";
    }

    $sql .= "
        AND fmt_table = sfm_form";

    if(!$orderByFields)
    {
        $sql .= "
            AND fmt_field NOT IN ('cla_dclaim', 'start_date')";
    }

    $sql .= "
        ORDER BY 2 ASC";

    $ListingFields = DatixDBQuery::PDO_fetch_all($sql);

    //We also need to generate a list of subforms for the returned fields to allow them
    //to be grouped on the page.
    $Subforms = array();
    foreach ($ListingFields as $ListingField)
    {
        $Subforms[$ListingField['subform']] = $ListingField['sfm_title'];
    }

    return array($Subforms, GetListingReportFieldsArray($ListingFields, $module, true));
}

?>