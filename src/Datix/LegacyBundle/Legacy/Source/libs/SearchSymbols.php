<?php
function GetAtTodayDate($WhereClause)
{
    $nStrSearchStartPos = \UnicodeString::stripos( $WhereClause, "@TODAY");
    $nStrSearchPos = $nStrSearchStartPos + 6 ;

    $sChar = "";
    $sCharTemp = "";
    $bEndQuoteNotFound = FALSE;
    $Today = getdate();
    while ($sCharTemp != "'" && $nStrSearchPos < \UnicodeString::strlen( $WhereClause ))
    {
        $sCharTemp = "";
        $sCharTemp = \UnicodeString::substr($WhereClause, $nStrSearchPos, 1);

        if ($sCharTemp == "+" || $sCharTemp == "-" || $sCharTemp >= "0" && $sCharTemp <= "9")
        {
            $sChar = $sChar . $sCharTemp;
        }
        else if ($sCharTemp == " " || $sCharTemp == "'")
        {
        }
        else
        {
            $bEndQuoteNotFound = TRUE;
            break;
        }

        $nStrSearchPos = $nStrSearchPos + 1;
    }

    if ($nStrSearchPos == \UnicodeString::strlen( $WhereClause ) && $sCharTemp != "'")
    {
        $bEndQuoteNotFound = TRUE;
    }

    if ($bEndQuoteNotFound)
    {
        $SearchDate = mktime(0, 0, 0, $Today["mon"], $Today["mday"] + $sChar, $Today["year"]);
        $SearchDateTime = strftime ( "%Y-%m-%d 00:00:00", $SearchDate );
        $WhereClause = \UnicodeString::substr_replace ( $WhereClause, $SearchDateTime, $nStrSearchStartPos, $nStrSearchPos );
    }
    else
    {
        $sStrLeft = \UnicodeString::substr( $WhereClause, 0, $nStrSearchStartPos );
        $sStrRight = \UnicodeString::substr( $WhereClause, $nStrSearchPos - 1, \UnicodeString::strlen( $WhereClause ) );

        $SearchDate = mktime(0, 0, 0, $Today["mon"], $Today["mday"] + $sChar, $Today["year"]);
        $SearchDateTime = strftime ( "%Y-%m-%d 00:00:00", $SearchDate );

        $WhereClause = $sStrLeft . $SearchDateTime . $sStrRight;
    }
    return $WhereClause;
}

function GetLastWeekDateRange($dtToday, &$dtStart, &$dtEnd)
{
    $nWeekStartDay = GetParm("WEEK_START_DAY", "2") - 1;
    $nWeekCurrentDay = $dtToday["wday"];

    If ($nWeekCurrentDay >= $nWeekStartDay)
    {
        $dtEnd = mktime(0, 0, 0, $dtToday["mon"], $dtToday["mday"] - ($nWeekCurrentDay - $nWeekStartDay) - 1, $dtToday["year"]);
    }
    else
    {
        $dtEnd = mktime(0, 0, 0, $dtToday["mon"], $dtToday["mday"] - ($nWeekStartDay - $nWeekCurrentDay) - 2, $dtToday["year"]);
    }
    $dtStartDate = getdate($dtEnd);
    $dtStart = mktime(0, 0, 0, $dtStartDate["mon"], $dtStartDate["mday"] - 6, $dtStartDate["year"]);
}

function TranslateDateRangeCom($sWhere, $sCommand, $dtStart, $dtEnd)
{
    $StartDateTime = strftime ( "%Y-%m-%d 00:00:00", $dtStart );
    $EndDateTime =  strftime ( "%Y-%m-%d 23:59:59", $dtEnd );

    preg_match_all("/(CAST\( FLOOR\( CAST\([ ])([^ ()]*)([ ]AS FLOAT \) \) AS DATETIME\))[ ]?[><=]*[ ]?[\'\"]+($sCommand)[\'\"]+/iu", $sWhere, $Matches);

    foreach($Matches[0] as $id => $Match)
    {
        $sFieldName = $Matches[1][$id] . $Matches[2][$id] . $Matches[3][$id];

        $sWhere = str_replace($Match, $sFieldName . " >= '$StartDateTime' AND " . $sFieldName . " <= '$EndDateTime'", $sWhere);
    }

    preg_match_all("/([^ ()]*)[ ]?[><=]*[ ]?[\'\"]+($sCommand)[\'\"]+/iu", $sWhere, $Matches);

    foreach($Matches[0] as $id => $Match)
    {
        $sFieldName = $Matches[1][$id];

        if($sFieldName != 'like') // need to deal with cases where this has been put into a non-date field.
        {
            $sWhere = str_replace($Match, $sFieldName . " >= '$StartDateTime' AND " . $sFieldName . " <= '$EndDateTime'", $sWhere);
        }
    }

    return $sWhere;
}

function GetLastMonthDateRange($dtToday, &$dtStart, &$dtEnd)
{

    $dtEnd = mktime(0, 0, 0, $dtToday["mon"], 0, $dtToday["year"]);
    $dtStart = mktime(0, 0, 0, $dtToday["mon"] - 1 , 1, $dtToday["year"]);
}

function GetLastQuarterDateRange($dtToday, &$dtStart, &$dtEnd)
{

    if ($dtToday["mon"] >=10)
    {
        $QtrStartMonth = 7;
        $QtrYear = $dtToday["year"];
    }
    else if ($dtToday["mon"] >=7)
    {
        $QtrStartMonth = 4;
        $QtrYear = $dtToday["year"];
    }
    else if ($dtToday["mon"] >=4)
    {
        $QtrStartMonth = 1;
        $QtrYear = $dtToday["year"];
    }
    else if ($dtToday["mon"] >=1)
    {
        $QtrStartMonth = 10;
        $QtrYear = $dtToday["year"] - 1;
    }

    $dtStart = mktime(0, 0, 0, $QtrStartMonth, 1, $QtrYear);
    $dtEnd = mktime(0, 0, 0, $QtrStartMonth + 3, 0, $QtrYear);
}

function GetLastYearDateRange($dtToday, &$dtStart, &$dtEnd)
{
    $dtStart = mktime(0, 0, 0, 1, 1, $dtToday["year"] - 1);
    $dtEnd = mktime(0, 0, 0, 12, 31, $dtToday["year"] - 1);
}

function GetWeekDateRange($dtToday, &$dtStart, &$dtEnd)
{
    $nWeekStartDay = GetParm("WEEK_START_DAY", "2") - 1; // we count sunday as "1", where php counts it as "0"
    $nWeekCurrentDay = $dtToday['wday'];

    If ($nWeekCurrentDay >= $nWeekStartDay)
    {
        $dtStart = mktime(0, 0, 0, $dtToday["mon"], $dtToday["mday"] - ($nWeekCurrentDay - $nWeekStartDay), $dtToday["year"]);
    }
    else
    {
        $dtStart = mktime(0, 0, 0, $dtToday["mon"], $dtToday["mday"] - (7 - $nWeekStartDay + $nWeekCurrentDay), $dtToday["year"]);
    }
    $dtEndDate = getdate($dtStart);
    $dtEnd = mktime(0, 0, 0, $dtEndDate["mon"], $dtEndDate["mday"] + 6, $dtEndDate["year"]);
}

function GetMonthDateRange($dtToday, &$dtStart, &$dtEnd)
{
    $dtStart = mktime(0, 0, 0, $dtToday["mon"] , 1, $dtToday["year"]);
    $dtEnd = mktime(0, 0, 0, $dtToday["mon"] + 1, 0, $dtToday["year"]);
}

function GetQuarterDateRange($dtToday, &$dtStart, &$dtEnd)
{

    if ($dtToday["mon"] >=10)
    {
        $QtrStartMonth = 10;
    }
    else if ($dtToday["mon"] >=7)
    {
        $QtrStartMonth = 7;
    }
    else if ($dtToday["mon"] >=4)
    {
        $QtrStartMonth = 4;
    }
    else if ($dtToday["mon"] >=1)
    {
        $QtrStartMonth = 1;
    }

    $dtStart = mktime(0, 0, 0, $QtrStartMonth, 1, $dtToday["year"]);
    $dtEnd = mktime(0, 0, 0, $QtrStartMonth + 3, 0,$dtToday["year"]);
}

function GetYearDateRange($dtToday, &$dtStart, &$dtEnd)
{
    $dtStart = mktime(0, 0, 0, 1, 1, $dtToday["year"]);
    $dtEnd = mktime(0, 0, 0, 12, 31, $dtToday["year"]);
}

/**
* Get date range for the financial year.
* @param array $dtToday The date used to determine which financial year to return (not necessarily today).
* @param int $dtStart Start date, passed by reference and set within this function.
* @param int $dtEnd End date, passed by reference and set within this function.
*/
function GetFinYearDateRange($dtToday, &$dtStart, &$dtEnd)
{
    $nFinMonthStart = GetParm("FINYEAR_START_MONTH", "4");

    if ($dtToday['mon'] < $nFinMonthStart)
    {
        // The current financial year started last calendar year
        $dtToday["year"]--;
    }

    $dtStart = mktime(0, 0, 0, $nFinMonthStart, 1, $dtToday["year"]);
    $dtEnd = mktime(0, 0, 0, $nFinMonthStart + 12, 0, $dtToday["year"]);
}

function TranslateConCode($sWhere, $initials, $module = "")
{
    global $FieldDefsExtra;

    $CurrentSymbol = '';
    $sSymbol = "@USER_";
    $nTagPos = \UnicodeString::stripos( $sWhere, $sSymbol);

    while ($nTagPos !== FALSE)
    {
        $CurrentCharacter = $sWhere[$nTagPos];

        while (!empty($CurrentCharacter))
        {
            if (preg_match("/^(\s)|(\))|(\')|(,)$/u", $CurrentCharacter))
            {
                break;
            }
            else
            {
                $CurrentSymbol.=  $CurrentCharacter;
                $nTagPos = $nTagPos + 1;
                $CurrentCharacter = $sWhere[$nTagPos];
            }
        }

        //Get contact data
        $CurrentField = \UnicodeString::strtolower(\UnicodeString::str_ireplace("@USER_", "", $CurrentSymbol));

        if (!array_key_exists($CurrentField, $FieldDefsExtra["CON"]))
        {
            $CurrentData["$CurrentField"] = "[INVALID USER SYMBOL/COLUMN: " . $CurrentField . ']';
        }
        else if ($initials != null)
        {
            $sql = "SELECT CAST($CurrentField AS TEXT) AS $CurrentField
                FROM contacts_main
                WHERE initials = '$initials'";
                $result = db_query($sql);
                $CurrentData = db_fetch_array($result);
        }
        else
        {
            //if initials are blank, we are validating @user_con codes and so this gives us the right data
            $CurrentData = array();
        }
        
        if(!(\UnicodeString::strpos($sWhere, 'dbo.fn_UnderLocationHierarchy') === false) || !(\UnicodeString::strpos($sWhere, 'DBO.FN_UNDERLOCATIONHIERARCHY') === false))
        {
        	return \UnicodeString::str_ireplace($CurrentSymbol, $CurrentData[0], $sWhere);
        }        

        if ($CurrentData["$CurrentField"] == '')
        {
            //preg_match("/([A-Za-z_.]*)[\s]*(like|LIKE|=)[\s]*('" . $CurrentSymbol . "'|" . $CurrentSymbol . ")/i",$sWhere,$Field);
            $sWhere = preg_replace("/([A-Za-z_.]*)[\s]*(like|not like|=)[\s]*('" . $CurrentSymbol . "'|" . $CurrentSymbol . ")/iu", "(1=2)", $sWhere);
        }
        else if ($FieldDefsExtra["CON"][$CurrentField]["Type"] == "multilistbox" && $CurrentData["$CurrentField"] != "")
        {
            $InList = array();
            $FieldParts = explode(" ", $CurrentData["$CurrentField"]);
            //Try and get the field name of the source field in order to determine whether it is a multicode
            preg_match("/(?P<leftfield>\w+)[\s]*(like|LIKE|=)[\s]*'" . $CurrentSymbol . "'/iu", $sWhere, $matches);

            if (\UnicodeString::strtolower($matches["leftfield"]) == 'udv_string')
            {
                preg_match("/field_id = ([0-9]+) AND/iu", $sWhere, $udfmatches);
                $udf = new Fields_ExtraField($udfmatches[1]);
                if ($udf->getFieldType() == 'T')
                {
                    $udfmulticode = true;
                }
            }

            if ($FieldDefsExtra[$module][\UnicodeString::strtolower($matches["leftfield"])]["Type"] == "multilistbox" || $udfmulticode)
            {
                if (count($FieldParts) > 0)
                {
                    foreach ($FieldParts as $code)
                    {
                        $codewhere = \UnicodeString::str_ireplace($CurrentSymbol, $code, $sWhere);
                        $InList[] = "(" . $codewhere . ")";
                    }
                }
                $sWhere = implode(" OR ", $InList);
            }
            else
            {
                if (count($FieldParts) > 0)
                {
                    foreach ($FieldParts as $code)
                    {
                        $InList[] = "'" . $code . "'";
                    }
                }

                $CurrentData["$CurrentField"] = implode(",", $InList);
                $CurrentDataAutoSQL= " IN (" . implode(",", $InList) . ")";
                $CurrentSymbolAutoSQL[] = "/[\s]*(like|LIKE|=)[\s]*'" . $CurrentSymbol . "'/iu";
                $sWhere = preg_replace($CurrentSymbolAutoSQL, $CurrentDataAutoSQL, $sWhere, -1, $count);
                
                if ($count == 0)
                {
                    throw new \Exception('@user_con search and replace failed.');
                }
                
                $CurrentSymbol = "'" . $CurrentSymbol . "'";
            }
        }

        $sWhere = \UnicodeString::str_ireplace($CurrentSymbol, $CurrentData["$CurrentField"], $sWhere);
        $nTagPos = \UnicodeString::stripos( $sWhere, $sSymbol );
        $CurrentSymbol = "";
    }
    return $sWhere;
}

?>
