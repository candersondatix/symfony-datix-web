<?php

use src\framework\registry\Registry;
use src\email\EmailSenderFactory;

function GetRecipients()
{
    global $ModuleDefs;

    $module = $_GET['module'];

    if ($module == 'INC')
    {
        $OverdueStatuses = getOverdueStatuses(array('module' => 'INC'));
        $StatusesToEmail = array_intersect(explode('|',$_GET['statuses']), array_flip($OverdueStatuses));

        SetGlobal('OVERDUE_EMAIL_STATUS', implode(' ',explode('|',Sanitize::SanitizeString($_GET['statuses']))));
        SetGlobal('OVERDUE_EMAIL_USERS', implode(' ',explode('|',Sanitize::SanitizeString($_GET['users']))));
    }
    else if ($module == 'COM')
    {
        foreach ($ModuleDefs['COM']['HARD_CODED_LISTINGS'] as $key => $listingDef)
        {
            if (isset($listingDef['OverdueWhere']))
            {
                $OverdueStatuses[$key] = $listingDef['Title'];
                $OverdueWhere[$key] = translateStaticAtCodes($listingDef['OverdueWhere']);
            }
        }

        $StatusesToEmail = array_intersect(explode('|',$_GET['statuses']), array_flip($OverdueStatuses));

        SetGlobal('OVERDUE_EMAIL_STATUS_COM', implode(' ',explode('|',Sanitize::SanitizeString($_GET['statuses']))));
        SetGlobal('OVERDUE_EMAIL_USERS_COM', implode(' ',explode('|',Sanitize::SanitizeString($_GET['users']))));
    }

    $ContactsToEmail = array();
    $JSONdata = array('complete' => 1);
    
    if(is_array($StatusesToEmail) && !empty($StatusesToEmail))
    {
        foreach($StatusesToEmail as $Status)
        {
            if ($module == 'INC')
            {
                $overduesql = getOverdueSQL('INC', $Status);
            
                $sql = 'SELECT recordid, inc_head, inc_mgr, inc_investigator FROM incidents_main '.$overduesql['join'].' WHERE rep_approved = \''.$Status.'\''.($overduesql['where'] ? ' AND '.$overduesql['where'] : '');
            }
            else if ($module == 'COM')
            {
                if ($OverdueWhere[$Status])
                {
                    $sql = 'SELECT recordid, com_head, com_mgr, com_investigator FROM compl_main WHERE '.$OverdueWhere[$Status];
                }
            }
            
            $result = db_query($sql);
                    
            while($row = db_fetch_array($result))
            {
                $SpecificContactsToEmail = getContactsToOverdueEmail($row, $_GET['users'], $module);
                
                foreach($SpecificContactsToEmail as $Contact)
                {
                    $ContactsToEmail[$Contact][] = $row['recordid'];
                }
            }
        }    
    }
        
    $sql = 'SELECT initials, con_email FROM contacts_main WHERE initials IN(\''.implode('\', \'',array_keys($ContactsToEmail)).'\')';
    $result = db_query($sql);
    
    while($row = db_fetch_array($result))
    {
        $ContactEmailAddresses[$row['initials']] = $row['con_email'];   
    }
        
    foreach($ContactsToEmail as $Initials => $Records)
    {
        if($ContactEmailAddresses[$Initials])
        {
            $JSONdata['contacts'][] = array('email' => $ContactEmailAddresses[$Initials], 'records' => $Records);
        }
    }
    
    //encode and return json data...
    echo json_encode($JSONdata);

}

function SendOverdueEmails()
{
    $Records = array_unique(explode('|', Sanitize::SanitizeString($_GET['records'])));

    $Result = SendOverdueEmailToContact(Sanitize::SanitizeString($_GET['email']), $Records, Sanitize::getModule($_GET['module']));
     
    $JSONdata['html'] = '<div style="padding-bottom:5px">'.$Result.'</div>';    
    $JSONdata['get'] = Sanitize::SanitizeStringArray($_GET);

    //encode and return json data...
    echo json_encode($JSONdata);
}

function SendOverdueEmailToContact($email, $records, $module)
{
    global $scripturl, $txt;
    
    $data['overdue_list_html'] = '';

    sort($records, SORT_NUMERIC);

    foreach($records as $recordid)
    {
        if ($module == 'INC')
        {
            $data['overdue_list_html'] .= $scripturl.'?action=incident&recordid='.$recordid."
";
        }
        else if ($module == 'COM')
        {
            $data['overdue_list_html'] .= $scripturl.'?action=record&module=COM&recordid='.$recordid."
";
        }
    }
    
    $data['overdue_num'] = count($records);

    require_once 'Source/libs/Email.php';
    Registry::getInstance()->getLogger()->logEmail('Sending Overdue notification e-mail to '. $email.' concerning '.$data['overdue_num'].' overdue '._tk('INCNames').'.');

    $emailSender = EmailSenderFactory::createEmailSender($module, 'Overdue');
    $emailSender->addRecipientEmail($email);
    $Success = $emailSender->sendEmails($data);

    if($Success)
    {
        $failedDomainValidation = $emailSender->getFailedDomainValidation();
        if (!empty($failedDomainValidation))
        {
            $Result = '<div>Sent an email to '.implode(',',array_keys($emailSender->getSuccessful())).' concerning '.$data['overdue_num'].' overdue '._tk($module."Names").'. '.sprintf(_tk('emails_not_set_domain_not_permitted'), implode(',',array_keys($emailSender->getFailedDomainValidation()))).'.</div>';
        }
        else
        {
            $Result = '<div>Sent an email to '.$email.' concerning '.$data['overdue_num'].' overdue '._tk($module."Names").'.</div>';
        }
    }
    else
    {
        $failedDomainValidation = $emailSender->getFailedDomainValidation();
        if (!empty($failedDomainValidation) && $failedDomainValidation[$email])
        {
            $Result = '<div>'.sprintf(_tk('emails_not_set_domain_not_permitted'), $email).'</div>';
        }
        else
        {
            $Result = '<div>Attempted <b>unsuccessfully</b> to send an email to '.$email.' concerning '.$data['overdue_num'].' overdue '._tk($module."Names").'.</div>';
        }
    }
    
    return $Result;
}

function getContactsToOverdueEmail($data, $users, $module)
{
    global $ModuleDefs;

    $SpecificContactsToEmail = array();
    
    $TypesToEmail = explode('|',$users);

    if(is_array($TypesToEmail) && !empty($TypesToEmail))
    {
        foreach($TypesToEmail as $Type)
        {
            switch($Type)
            {
                case 'MGR':
                    if($data[$ModuleDefs[$module]['FIELD_NAMES']['MANAGER']])
                    {
                        $SpecificContactsToEmail[] = $data[$ModuleDefs[$module]['FIELD_NAMES']['MANAGER']];
                    }
                break;
                case 'HDLR':
                    if($data[$ModuleDefs[$module]['FIELD_NAMES']['HANDLER']])
                    {
                        $SpecificContactsToEmail[] = $data[$ModuleDefs[$module]['FIELD_NAMES']['HANDLER']];
                    }
                break;
                
                case 'INV':
                    if(!empty($data[$ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS']]))
                    {
                        foreach(explode(' ', $data[$ModuleDefs[$module]['FIELD_NAMES']['INVESTIGATORS']]) as $Investigator)
                        {
                            $SpecificContactsToEmail[] = $Investigator;
                        }    
                    }
                break;
            }
        }
    }
    
    return array_unique($SpecificContactsToEmail);
}