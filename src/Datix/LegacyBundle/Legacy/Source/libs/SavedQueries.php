<?php

use src\users\model\UserModelFactory;
use src\savedqueries\model\SavedQuery;
use src\savedqueries\model\SavedQueryModelFactory;
use src\framework\query\Where;
use src\framework\registry\Registry;
use src\security\Escaper;

function NewSavedQuery()
{
    $FormAction = Sanitize::SanitizeString($_GET["form_action"]);

    $loader = new \src\framework\controller\Loader();
    $controller = $loader->getController(
        array('controller' => 'src\\savedqueries\\controllers\\ShowSavedQueriesTemplateController')
    );
    $controller->setRequestParameter('formAction', $FormAction);
    echo $controller->doAction('showSavedQueryForm');
    obExit();
}

function SaveQuery()
{
    global $scripturl, $yySetLocation;

    if ($_POST["rbWhat"] == 'cancel')
    {

        if ($_POST["form_action"]== 'new')
        {
            if ($_POST["module"] == 'SAB')
            {
                $yySetLocation = "$scripturl?action=list&module=SAB&table=main";
            }
            elseif ($_POST["module"] == 'CON')
            {
                $yySetLocation = "$scripturl?action=listcontacts&module=CON";
            }
            else
            {
                $yySetLocation = "$scripturl?action=list&module=". $_POST["module"] . "&listtype=search";
            }
        }
        else
        {
            $yySetLocation = "$scripturl?action=savedqueries&module=". $_POST["module"];
        }

	    redirectexit();
    }
    elseif ($_POST["rbWhat"] == 'delete')
    {
        DeleteQuery();
    }
    else
    {
        SaveQueryToMain();
    }
}

function SaveQueryToMain()
{
	global $scripturl, $yySetLocation, $txt, $ModuleDefs;

	LoggedIn();

    $qry_id = Sanitize::SanitizeInt($_POST['qry_recordid']);
    $sq_name = $_POST['sq_name'];

	$ErrorMark = '<font size="3" color="red"><b>*</b></font>';

    if ($_POST["sq_name"] == "")
	{
        AddValidationMessage('sq_name', 'You must enter a name for the query.');
		$error = true;
	}
    else
    {

        if ($_POST["qry_recordid"])
        {
            $sql = "SELECT recordid as qry_recordid, sq_name, sq_where, sq_tables, sq_module, sq_user_id
                  FROM queries
                  WHERE recordid = :recordid";
            $qry_original = DatixDBQuery::PDO_fetch($sql, ['recordid' => $_POST['qry_recordid']]);
        }

        if ($qry_original["sq_name"] != $sq_name)
        {
            $sql = "select sq_name from queries where sq_name = :sq_name";
            $qrycheckexists = DatixDBQuery::PDO_fetch($sql, ['sq_name' => $sq_name]);

            if ($qrycheckexists)
            {
                AddValidationMessage('sq_name', 'The name provided already exists. Please use another name.');
                $error = true;
            }
        }
    }

    $qry = $_POST;

    if (!ValidateWhereClause($_POST['sq_where'], Sanitize::getModule($qry['module'])))
    {
        AddValidationMessage('sq_where', 'The \'WHERE\' statement provided contains a syntax error.');
        $error = true;
    }

	if ($_POST["qry_type"] == "" || $_POST["qry_type"] == "ALL")
	{
        $user_id = null;
	}
    else
    {
        $user_id = $_SESSION['contact_login_id'];
    }

    if ($error)
	{
        AddSessionMessage('ERROR', $txt["form_errors"]);
        $loader = new \src\framework\controller\Loader();
        $controller = $loader->getController(
            array('controller' => 'src\\savedqueries\\controllers\\ShowSavedQueriesTemplateController')
        );
        $controller->setRequestParameter('error', $error);
        echo $controller->doAction('showSavedQueryForm');
		obExit();
	}

    if ($qry["table"] == "holding")
    {
        $qry_type = "WH";
    }
    else
    {
        $qry_type = "W";
    }

    $qrycheck = "";

    if ($qry_id)
    {
        $sql = "select sq_name, sq_where from queries where recordid = :recordid";
        $qrycheck = DatixDBQuery::PDO_fetch($sql, ['recordid' => $qry_id]);
    }

    $PDOParams = array(
        'sq_name_w' => $sq_name,
        'user_id'   => $user_id,
        'module'    => $qry['module'],
        'sq_tables' => $qry['sq_tables'],
        'sq_where'  => $qry['sq_where'],
        'qry_type'  => $qry_type
    );

    if (!$qrycheck)
    {
        if ($ModuleDefs[$qry['module']]['LISTING_FILTERS'])
        {
            // the format of the filter is saved as a serialized array in the sq_order field
            $sql = "INSERT INTO queries (
               sq_name, sq_user_id, sq_module, sq_tables, sq_where, sq_type, sq_orderby)
               VALUES (
               :sq_name_w, :user_id, :module, :sq_tables, :sq_where, :qry_type, :sq_orderby)";

            $PDOParams['sq_orderby'] = serialize($_SESSION[$qry['module']]['FILTER']);
        }
        else
        {
            $sql = "INSERT INTO queries (
               sq_name, sq_user_id, sq_module, sq_tables, sq_where, sq_type)
               VALUES (
               :sq_name_w, :user_id, :module, :sq_tables, :sq_where, :qry_type)";
        }
        
        $query_id = DatixDBQuery::PDO_insert($sql, $PDOParams);
    }
    else
    {
        $sql = "UPDATE queries SET
               sq_name = :sq_name_w,
               sq_user_id = :user_id,
               sq_module = :module,
               sq_tables = :sq_tables,
		       sq_where = :sq_where,
               sq_type = :qry_type
               WHERE
               recordid = :recordid";

        $PDOParams['recordid'] = $qry['qry_recordid'];
        
        DatixDBQuery::pdo_query($sql, $PDOParams);
    }
    
	if ($_POST['qry_recordid'] == '')
	{
	    $factory = new SavedQueryModelFactory();
	    
	    if ($_POST['sq_where'] == $_SESSION[$_POST['module']]['WHERE'])
	    {
	        // The where clause has not been altered, so we can attempt to persist a SavedQuery properly.
    	    if ($_SESSION[$_POST['module']]['DRILL_SAVED_QUERY'] instanceof SavedQuery)
    	    {
    	        // The current search has been procuced by drilling into a report.
    	        $savedQuery = $_SESSION[$_POST['module']]['DRILL_SAVED_QUERY'];
    	        
    	        if ($savedQuery->where_string != '')
    	        {
    	            // The original saved query used by the report was an "old" query, and so will comprise of a where_string 
    	            // representing the original query and a Where object representing the drill-in criteria.  In this situation we have to 
    	            // save the criteria as a string, so we overwrite the where_string with the posted value, which is the string representation of
    	            // the old query plus the additional drill-in criteria (the Where object is removed in this process).
    	            $savedQuery->where_string = $_POST['sq_where'];
    	        }
    	        
    	        $savedQuery->name = $_POST['sq_name'];
    	        $savedQuery->user_id = $user_id;
    	        $savedQuery->old_query_id = $query_id;
    	    }
    	    elseif ($_SESSION[$_POST['module']]['NEW_WHERE'] instanceof Where)
    	    {
    	        $queryData = [
        	        'module'       => $_POST['module'],
        	        'name'         => $_POST['sq_name'],
        	        'user_id'      => $user_id,
        	        'where'        => $_SESSION[$_POST['module']]['NEW_WHERE'],
        	        'old_query_id' => $query_id
        	    ];
        	    
        	    $savedQuery = $factory->getEntityFactory()->createObject($queryData);
    	    }
            else
            {
                saveWhereAsString($user_id, $query_id, $factory);
            }
	    }
	    else
	    {
            saveWhereAsString($user_id, $query_id, $factory);
	    }
	    
	    if (isset($savedQuery))
	    {
	        $factory->getMapper()->save($savedQuery);
	    }
	}
	else
	{
	    // Attempt to find a "new" SavedQuery linked to the "old" one.
	    $factory = new SavedQueryModelFactory();
	    $savedQuery = $factory->getMapper()->findByOldQueryID($_POST['qry_recordid']);
	    
	    if ($savedQuery !== null)
	    {
	        // Update basic properties.
	        $savedQuery->name = $_POST['sq_name'];
	        $savedQuery->user_id = $user_id;
	        
    	    if ($qrycheck['sq_where'] != $_POST['sq_where'])
    	    {
        	    // The where clause has been manually edited, so we have to save it as a string.
        	    $savedQuery->where_string = $_POST['sq_where'];
    }

    	    $factory->getMapper()->save($savedQuery);
	    }
	}

    AddSessionMessage('INFO', sprintf(_tk('query_save_message'), $sq_name));

    if ($_POST["form_action"] == 'new')
    {
        if ($qry["module"] == "CON")
        {
            $yySetLocation = "$scripturl?action=listcontacts&module=CONfromsearch=1";
        }
        else
        {
            $yySetLocation = "$scripturl?action=list&module=". $qry["module"] . "&table=" . $qry["table"] . "&fromsearch=1&listtype=search";
        }
    }
    else
    {
        $yySetLocation = "$scripturl?action=savedqueries&module=". $_POST["module"];
    }

    redirectexit();
}

function SavedQueryDetailsSection($qry, $FormAction, $module)
{
    global $ModuleDefs;

	$CTable = new FormTable();
	$FieldObj = new FormField();
	$CTable->MakeTitleRow('<b>Query details</b>');

    $CTable->MakeRow("<label for=\"sq_name\"><b>Save as</b></label>".GetValidationErrors($qry, 'sq_name'), $FieldObj->MakeInputField('sq_name', 50, 50, $qry["sq_name"], ""));

    $Factory = new UserModelFactory();
    $User = $Factory->getMapper()->findByInitials($_SESSION["initials"]);

    require_once 'Source/security/SecurityBase.php';
    $Perms = ParsePermString($User->permission);

    $allowSetup = $Perms[$module]["disallow_setup"] === false;
    
    if (($allowSetup || $_SESSION["AdminUser"]))
    {
        if ($ModuleDefs[$module]['LISTING_FILTERS'])
        {
            $CTable->Contents .= '<input type="hidden" name="sq_where" value="'.Sanitize::SanitizeRaw($qry["sq_where"]).'" />';
        }
        else
        {
            $CTable->MakeRow("<label for=\"sq_where\"><b>Where clause</b></label>".GetValidationErrors($qry, 'sq_where'), $FieldObj->MakeTextAreaField('sq_where', 7, 70, 0, $qry["sq_where"], false));
        }

        $field = Forms_SelectFieldFactory::createSelectField('qry_type', $module, ($qry["qry_type"] == '' ? 'ALL' : $qry["qry_type"]), '');
        $field->setCustomCodes($qry["linktypes"]);
        $field->setSuppressCodeDisplay();
        $CTable->MakeRow("<label for=\"qry_type_title\"><b>Query type</b></label>".GetValidationErrors($qry, 'qry_type'), $field);
    }
    else
    {
        $CTable->MakeRow("<label for=\"qry_type\"><b>Query type</b></label>".GetValidationErrors($qry, 'qry_type'), $FieldObj->MakeCustomField("Accessible to you only<input type=\"hidden\" name=\"qry_type\" id=\"qry_type\" value=\"USER\"><input type=\"hidden\" name=\"sq_where\" id=\"sq_where\" value=\"".$qry["sq_where"]."\">"));
    }

	$CTable->MakeTable();

    echo '
        <tr>
            <td>' . $CTable->GetFormTable() . '</td>
        </tr>
    ';
}

function DeleteQuery()
{
	global $scripturl, $yySetLocation;

	LoggedIn();

    $query_recordid = Sanitize::SanitizeInt($_POST["qry_recordid"]);
    $AdminUser      = $_SESSION['AdminUser'];
    $module         = Sanitize::SanitizeString($_POST['module']);

    $Factory = new UserModelFactory();
    $User = $Factory->getMapper()->findByInitials($_SESSION['initials']);

    require_once 'Source/security/SecurityBase.php';
    $Perms = ParsePermString($User->permission);

    $allowSetup = ($Perms[$module]['disallow_setup'] === false);

    if ($query_recordid)
    {
        // Is this user the owner of the query?
        $sql = 'SELECT 1 FROM QUERIES WHERE recordid = :recordid AND sq_user_id = (SELECT recordid FROM staff WHERE initials = :initials)';

        $isOwner = (\DatixDBQuery::PDO_fetch($sql, [
            'recordid' => Sanitize::SanitizeInt($_POST['qry_recordid']),
            'initials' => $User->initials
        ], \PDO::FETCH_COLUMN) == '1');

        if ($AdminUser || $allowSetup || $isOwner)
        {
            $sql = "DELETE FROM queries
                   WHERE
                   recordid = :recordid";

            if (!\DatixDBQuery::PDO_query($sql, ['recordid' => $query_recordid]))
            {
                fatal_error(_tk('query_delete_error') . $sql);
            }
        }
    }

    $factory    = new SavedQueryModelFactory();
    $mapper     = $factory->getMapper();
    $savedQuery = $mapper->findByOldQueryID($_POST['qry_recordid']);
    
    if ($savedQuery !== null && ($AdminUser || $allowSetup || ($User->initials == $savedQuery->createdby)))
    {
        $factory->getMapper()->delete($savedQuery);
    }
    else
    {
        fatal_error(_tk('query_delete_error'));
    }

    $module = Sanitize::SanitizeString($_POST['module']);
    $pinnedGlobal = GetParm($module.'_SAVED_QUERIES', false);

    if ($pinnedGlobal != '')
    {
        $pinnedQueries = explode(' ', $pinnedGlobal);

        foreach ($pinnedQueries as $id => $queryId)
        {
            if ($queryId == $query_recordid)
            {
                unset($pinnedQueries[$id]);
            }
        }

        $pinnedQueries = implode(' ', Sanitize::SanitizeStringArray($pinnedQueries));

        SetGlobal($module.'_SAVED_QUERIES', $pinnedQueries, true);
    }

    AddSessionMessage('INFO', _tk('query_delete_message'));
    
    $yySetLocation = "$scripturl?action=savedqueries&module=". Sanitize::getModule($_POST["module"]);
	redirectexit();
}

function SavedQueriesSection($qry, $FormAction)
{
    require_once 'Source/libs/QueriesBase.php';
    $saved_queries = get_saved_queries(Sanitize::getModule($_GET["module"]));

    if (!$saved_queries)
    {
        $saved_queries = array();
    }

	$CTable = new FormTable();
	$CTable->MakeTitleRow('<b>'._tk('saved_queries').'</b>');

	if ($error)
    {
        $CTable->Contents .= '<font color="red"><b>' . $error["message"] . '</b></font><br /><br />';
    }

    $field = Forms_SelectFieldFactory::createSelectField('qry_recordid', $_GET["module"], $qry["qry_recordid"], '');
    $field->setCustomCodes($saved_queries);
    $field->setSuppressCodeDisplay();
	$CTable->MakeRow("<label for=\"qry_recordid_title\"><b>"._tk('query')."</b></label>".GetValidationErrors($qry, 'qry_recordid'), $field);

	$CTable->MakeTable();

    echo '
        <tr>
            <td>' . $CTable->GetFormTable() . '</td>
        </tr>
    ';
}

function CheckForPrompt($Query, $Module)
{
    if ($Query)
    {
        // make sure carriage returns are removed from where clause as these can interfere with the pattern matching
        $Query = preg_replace("/[\n\r]/u", " ", $Query);

        //Match udf fields using groups
        preg_match_all("/((CAST\( FLOOR\( CAST\([ ])([^ ()]*)([ ]AS FLOAT \) \) AS DATETIME\))|udf_values WHERE field_id = (\d+) AND group_id = (\d+) AND mod_id = (\d+) AND( | CAST\( FLOOR\( CAST\( | \()(udv_string|udv_date)( | AS FLOAT \) \) AS DATETIME\) )|([^ ()]*))[ ]?([><=]*|like)[ ]?[\'\"]+(@PROMPT|@prompt)[\'\"]+/iu", $Query, $MatchArrayWithGroups);

        //Match udf fields searched without groups
        preg_match_all("/((CAST\( FLOOR\( CAST\([ ])([^ ()]*)([ ]AS FLOAT \) \) AS DATETIME\))|udf_values WHERE field_id = (\d+) AND mod_id = (\d+) AND( | CAST\( FLOOR\( CAST\( | \()(udv_string|udv_date)( | AS FLOAT \) \) AS DATETIME\) )|([^ ()]*))[ ]?([><=]*|like)[ ]?[\'\"]+(@PROMPT|@prompt)[\'\"]+/iu", $Query, $MatchArrayNoGroups);

        //Process and Merge results for udf fields searched on with and without groups. This could be optimized, but safest option for now that works.
        $TmpMatchArray = array_merge(ProcessPromptMatch($Module, $MatchArrayWithGroups, true), ProcessPromptMatch($Module, $MatchArrayNoGroups, false));
        $DupCheckList = array();

        foreach ($TmpMatchArray as $MatchFieldData)
        {
            if (!in_array($MatchFieldData['field'], $DupCheckList))
            {
                $MatchArray[] = $MatchFieldData;
                $DupCheckList[] = $MatchFieldData['field'];
            }
        }

        return $MatchArray;
    }

    return false;
}

function ProcessPromptMatch($Module, $Matches, $CheckWithGroups)
{
    global $FieldDefsExtra;

    $MatchArray = array();

    if (!empty($Matches))
    {
        foreach ($Matches[3] as $id => $Field)
        {
            $TempArray = array();
            $isUDF = false;

            if ($Field == '')
            {
                if ($Matches[5][$id] != '')
                {
                    if ($CheckWithGroups)
                    {
                        $Field = $Matches[9][$id];
                    }
                    else
                    {
                        $Field = $Matches[8][$id];
                    }

                    $isUDF = true;
                }
                else
                {
                    $Field = $Matches[1][$id];
                    $Matches[3][$id] = $Matches[1][$id];
                    $isUDF = false;
                }
            }

            if ($isUDF)
            {
                // build UDF field name (need to look up group (any group for this field) and type)
                // UDF_<Type>_<GroupID>_<FieldID>
                $FieldID = $Matches[5][$id];

                if (!$CheckWithGroups)
                {
                    $sql = "SELECT fld_type FROM udf_fields WHERE recordid = $FieldID";
                }
                else
                {
                    $GroupID = $Matches[6][$id];

                    if ($GroupID == 0)
                    {
                        $sql = "SELECT fld_type, 0 as group_id FROM udf_fields WHERE recordid = $FieldID";
                    }
                    else
                    {
                        $sql = "SELECT uf.fld_type, ul.group_id
                            FROM udf_fields uf
                            INNER JOIN udf_links ul ON uf.recordid = ul.field_id
                            WHERE uf.recordid = '$FieldID'";
                    }
                }

                $result = db_query($sql);

                if ($row = db_fetch_array($result))
                {
                    if (in_array($row['fld_type'], array('D', 'C', 'T', 'Y')))
                    {
                        if ($CheckWithGroups)
                        {
                            $TempArray['field'] = 'UDF_'.$row['fld_type'].'_'.$GroupID.'_'.$FieldID;
                        }
                        else
                        {
                            $TempArray['field'] = "UDF_$row[fld_type]_$row[group_id]_$FieldID";
                        }

                        $TempArray['type'] = $row['fld_type'];
                        $TempArray['matchstring'] = $Matches[0][$id];

                        // Keep the original match string to be used later to replace in the where string the @prompt codes
                        $TempArray['originalmatchstring'] = $Matches[0][$id];

                        if ($TempArray['type'] == 'T')
                        {
                            if (mb_substr_count($TempArray['matchstring'], 'prompt'))
                            {
                                $promptReplace = '@prompt';
                            }
                            else
                            {
                                $promptReplace = '@PROMPT';
                            }

                            $TempArray['matchstring'] .= " OR $Field like '$promptReplace %' OR $Field like '% $promptReplace' OR $Field like '% $promptReplace %'";
                        }

                        $MatchArray[] = $TempArray;
                    }
                }
            }
            else
            {
                $RealFieldName = RemoveTableFromFieldName($Field);

                if (!empty($FieldDefsExtra[$Module][$RealFieldName]) && in_array($FieldDefsExtra[$Module][$RealFieldName]['Type'], array('date', 'ff_select', 'multilistbox', 'yesno')))
                {
                    $TempArray['field'] = $Field;
                    $TempArray['type'] = $FieldDefsExtra[$Module][$RealFieldName]['Type'];
                    $TempArray['matchstring'] = $Matches[0][$id];

                    // Keep the original match string to be used later to replace in the where string the @prompt codes
                    $TempArray['originalmatchstring'] = $Matches[0][$id];

                    if ($TempArray['type'] == 'multilistbox')
                    {
                        if (mb_substr_count($TempArray['matchstring'], 'prompt'))
                        {
                            $promptReplace = '@prompt';
                        }
                        else
                        {
                            $promptReplace = '@PROMPT';
                        }

                        $TempArray['matchstring'] .= " OR $Field like '$promptReplace %' OR $Field like '% $promptReplace' OR $Field like '% $promptReplace %'";
                    }

                    $MatchArray[] = $TempArray;
                }
            }
        }
    }

    return $MatchArray;
}

function GetPromptData($aParams = array())
{
    global $yySetLocation, $dtxtitle, $scripturl;

    $params = array();

    foreach ($_GET as $key => $value)
    {
    	$params[$key] = Sanitize::SanitizeString($value);
    }

    $ListUrlString = ArrayToUrlString($params);

    $qry_recordid = is_numeric($params['query']) ? (int) $params['query'] : "";

    if ($qry_recordid)
    {
        $sql = "SELECT sq_name FROM queries
                WHERE recordid = ".$qry_recordid;
        $request = db_query($sql);
        $row = db_fetch_array($request);
        $dtxtitle = $row['sq_name'];
    }
    else
    {
        $dtxtitle = 'Selection criteria';
    }

    if (!$_GET['module'])
    {
        $yySetLocation = "$scripturl?".$ListUrlString;
        redirectexit();
    }

    getPageTitleHTML(array(
        'title' => $dtxtitle,
        'module' => $_GET['module']
    ));

    GetSideMenuHTML(array('module' => $_GET['module']));

    template_header();

    echo '
    <form enctype="multipart/form-data" method="post" name="frmDocument" id="frmDocument" action="'
        . $scripturl . '?'.$ListUrlString.'">
        <input type="hidden" id="qry_recordid" name="qry_recordid" value="'.$qry_recordid.'" />';

    $Query = $_SESSION[$_GET['module']]["PROMPT"]["ORIGINAL_WHERE"];

    $MatchArray = CheckForPrompt($Query, $aParams['module']);

    foreach ($MatchArray as $Match)
    {
        $MatchedFields[] = RemoveTableFromFieldName($Match['field']);
    }

    foreach (Sanitize::SanitizeRawArray($_POST) as $key => $val)
    {
        if (!in_array($key, $MatchedFields) && !in_array(str_replace('_start','',$key), $MatchedFields) && !in_array(str_replace('_end','',$key), $MatchedFields))
        {
            echo '<input type="hidden" id="'.Escape::EscapeEntities($key).'" name="'.Escape::EscapeEntities($key).'" value="'.Escape::EscapeEntities($val).'" />';
        }
    }

    if (empty($MatchArray))
    {
        echo '
            <div>
                <b>Redirecting...</b>
            </div>
            <script language="Javascript">$("frmDocument").submit()</script>';
    }
    else
    {
        echo '
                <div class="external_border">
                    <div class="internal_border">
                        <a name="datix-content" id="datix-content"></a>
                        <ol>
                            <li name="prompt_title_row" id="prompt_title_row" class="section_title_row">
                                <h2>
                                    <div class="section_title_group">
                                        <div class="section_title">In order to proceed with your request, you must provide further information.  Please complete the fields below and press Continue:</div>
                                    </div>
                                </h2>
                            </li>';

        foreach ($MatchedFields as $Field)
        {
            echo getPromptFields($Field);
        }

        echo '
                        </ol>';

    echo '
                        <div class="button_wrapper">
                            <input type="hidden" name="promptsubmitted" value="1" />
                            <input type="button" name="confirmDates" value="Continue" onClick="document.forms[0].submit()"/>';

    if ($_GET['action'] == 'reports2')
    {
        $predefined = ($_GET['form'] == 'predefined');
        echo '              <input type="button" name="cancelRecordButton" value="'._tk('btn_cancel').'" onClick="SendTo(\''.$scripturl.'?action=listmyreports&module='.Sanitize::getModule($_REQUEST['module']).($predefined ? '&form=predefined' : "").'\');" />';
    }
    else
    {
        echo '              <input type="button" name="cancelRecordButton" value="'._tk('btn_cancel').'" onClick="SendTo(\''.$scripturl.'?module='.Sanitize::getModule($_GET['module']).'\');" />';
    }

    echo '
                        </div>
                    </div>
    </form>';

    }

    footer();
    obExit();
}

function getPromptFields($Field)
{
    global $FieldDefsExtra;

    $Module = Sanitize::getModule($_GET['module']);

    if (\UnicodeString::substr($Field, 0, 3) == 'udf')
    {
        $RealFieldName = \UnicodeString::strtoupper($Field);
        $Type = 'UDF';
        $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => $Module, 'level' => 2, 'form_type' => 'Search'));
    }
    else
    {
        $RealFieldName = RemoveTableFromFieldName($Field);
        $Type = $FieldDefsExtra[$Module][$RealFieldName]['Type'];
        $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => $Module, 'level' => 2, 'form_type' => 'Search'));
    }

    $FormDesign->LoadFormDesignIntoGlobals();

    $label = GetFormFieldLabel($RealFieldName, '', $Module);

    if ($label == '')
    {
        // need a better way to accurately determine which module each of these fields is in
        $label = GetFormFieldLabel($RealFieldName);
    }

    switch($Type)
    {
        case 'date':
            $StartDate = new FormField('Search');
            $StartDate->MakeDateField($RealFieldName.'_start',Sanitize::SanitizeString($_POST[$RealFieldName.'_start']));

            $EndDate = new FormField('Search');
            $EndDate->MakeDateField($RealFieldName.'_end',Sanitize::SanitizeString($_POST[$RealFieldName.'_end']));

            $HTML = '<li class="field_div" name="_row" id="_row"  style="">
                         <div class="field_label_div" style="width:25%">'.$label.'</div>
                         <div class="field_input_div" style="width:70%">&nbsp; from: '.$StartDate->Field.'&nbsp; to: '.$EndDate->Field.'</div>
                     </li>';
            break;
        case 'ff_select':
        case 'yesno':
        case 'multilistbox':
            if ($Field == 'rep_approved')
            {
                unset($GLOBALS['HideFields']['rep_approved']); // this will affect the display of the approval status field, otherwise.
                SetUpApprovalArrays($Module, $_POST, $CurrentApproveObj, $DropdownBox, 'Search', $FormDesign);
            }
            else
            {
                $DropdownBox = Forms_SelectFieldFactory::createSelectField($RealFieldName, Sanitize::SanitizeString($_GET['module']), Sanitize::SanitizeString($_POST[$RealFieldName]), 'Search');
            }

            $HTML = '<li class="field_div" name="_row" id="_row"  style="">
                         <div class="field_label_div" style="width:25%">'.$label.'</div>
                         <div class="field_input_div" style="width:70%">'.$DropdownBox->getField() .'</div>
                     </li>';
            break;
        case 'UDF':
            $UDFArray = explode("_", $RealFieldName);
            $sql = "SELECT fld_name FROM udf_fields WHERE recordid = $UDFArray[3]";
            $row = db_fetch_array(db_query($sql));

            if ($UDFArray[1] == 'D')
            {
                $StartDate = new FormField('Search');
                $StartDate->MakeDateField($RealFieldName.'_start',Sanitize::SanitizeString($_POST[$RealFieldName.'_start']));

                $EndDate = new FormField('Search');
                $EndDate->MakeDateField($RealFieldName.'_end',Sanitize::SanitizeString($_POST[$RealFieldName.'_end']));

                $HTML = '<li class="field_div" name="_row" id="_row"  style="">
                            <div class="field_label_div" style="width:25%">'.$label.'</div>
                            <div class="field_input_div" style="width:70%">&nbsp; from: '.$StartDate->Field.'&nbsp; to: '.$EndDate->Field.'</div>
                         </li>';
            }
            else
            {
                $Form = new FormTable();
                $Form->FormMode = 'Search';
                $UDFField = $Form->MakeUDFField($RealFieldName, $UDFArray[3], $UDFArray[1], '', $label);
                $Form->MakeRow($label, $UDFField);

                $HTML = $Form->Contents;
            }

            break;
    }

    return $HTML;
}

function DoPromptSection($aParams)
{
    global $scripturl, $yySetLocation, $ModuleDefs;

    $sModule = $aParams["module"];
    $bReport = $aParams["report"];

    $MatchArray = CheckForPrompt($_SESSION[$sModule]["PROMPT"]["ORIGINAL_WHERE"],$sModule);

    if (!empty($MatchArray))
    {
        //because the "original where" value gets changed too often, we need another variable to track the where clause containing @prompt
        $_SESSION[$sModule]["PROMPT"]["CURRENT_QUERY_ORIGINAL_WHERE"] = $_SESSION[$sModule]["PROMPT"]["ORIGINAL_WHERE"];

        $_GET['module'] = $sModule;

        if(is_array($_SESSION[$sModule]["PROMPT"]['VALUES']) && !empty($_SESSION[$sModule]["PROMPT"]['VALUES']))
        {
            //if at least one value is set, we can use the previous values.
            foreach ($_SESSION[$sModule]["PROMPT"]['VALUES'] as $Value)
            {
                if (is_array($Value))
                {
                    //dates will have a "start" and "end" entry
                    foreach ($Value as $ValDate)
                    {
                        if ($ValDate != '' && $ValDate != 'NULL')
                        {
                            $ValueSet = true;
                        }
                    }
                }
                elseif ($Value != '')
                {
                    $ValueSet = true;
                }
            }
        }

        if (!isset($_POST['promptsubmitted']) && !$ValueSet && !$_SESSION[$sModule]['PROMPT']['PROMPTSUBMITTED'])
        {
            $PromptComplete = false;
        }
        else
        {
            $PromptComplete = true;
        }

        if ($PromptComplete)
        {
            $_SESSION[$sModule]["PROMPT"]["NEW_WHERE"] = replaceAtPrompts($_SESSION[$sModule]["PROMPT"]["ORIGINAL_WHERE"], $sModule, $MatchArray);
        }

        if (!$PromptComplete)
        {
            $_GET["module"] = $sModule;
            GetPromptData($aParams);
        }
    }
    else
    {
        $_SESSION[$sModule]["PROMPT"]["NEW_WHERE"] = '';
    }
}

/**
 * Replaces occurances of @prompt within a where string.
 * 
 * Factored out of DoPromptSection() so I can reuse this logic in Query::replaceAtPrompts().
 * There seems to be a lot of duplicated logic between this function and ReplacePrompts().  I'm not sure why this is,
 * but we should attempt to refactor this dupicated logic out.
 * 
 * @param string $whereString The where clause.
 * @param string $module      The module context.
 * @param array  $MatchArray  An array of @prompt matches.
 * 
 * @return string $whereString The where clause with the @prompts replaced.
 */
function replaceAtPrompts($whereString, $module, array $MatchArray = array())
{
    $ModuleDefs = Registry::getInstance()->getModuleDefs();
    $table = $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE'];
    
    if (empty($MatchArray))
    {
        $MatchArray = CheckForPrompt($whereString, $module);
    }

    if (!empty($MatchArray))
    {
            foreach ($MatchArray as $Match)
            {
                $Field = $Match['field'];
                $Type = $Match['type'];

                if (\UnicodeString::substr($Field, 0, 3) == 'UDF')
                {
                    $RealFieldName = $Field;
                    $newPromptName = $Field;
                    $Field = ($Type == 'D' ? 'udv_date' : 'udv_string');
                    $isUDF = true;
                    
                    // ensure a group ID is always part of the field name when caching "new" prompt values
                    $udfParts = explode('_', $newPromptName);
                    if (count($udfParts) == 4 && $udfParts[2] == '')
                    {
                        $udfParts[2] = '0';
                        $newPromptName = implode('_', $udfParts);
                    }
                    $newPromptName = \UnicodeString::strtolower($newPromptName);
                }
                else
                {
                    $RealFieldName = RemoveTableFromFieldName($Field);
                    $newPromptName = \UnicodeString::strtolower($ModuleDefs[$module]['TABLE'].'.'.$RealFieldName);
                    $isUDF = false;
                }

                if (($Type == 'D' || $Type == 'date') && empty($_POST[$RealFieldName.'_start']) && empty($_POST[$RealFieldName.'_end']))
                {
                    $_POST[$RealFieldName.'_start'] = $_SESSION[$module]["PROMPT"]["VALUES"][$RealFieldName]['start'];
                    $_POST[$RealFieldName.'_end'] = $_SESSION[$module]["PROMPT"]["VALUES"][$RealFieldName]['end'];
                }
                elseif (!($Type == 'D' || $Type == 'date') && empty($_POST[$RealFieldName]))
                {
                    $_POST[$RealFieldName] = $_SESSION[$module]["PROMPT"]['VALUES'][$RealFieldName];
                }

                switch ($Type)
                {
                    case 'date':
                    case 'D':
                        if (mb_substr_count($_POST[$RealFieldName.'_start'], '@')>0 || mb_substr_count($_POST[$RealFieldName.'_end'], '@')>0)
                        {
                            $PromptComplete = false;
                            AddSessionMessage('ERROR', 'Error with \''.GetFieldLabel($RealFieldName).'\' field : You may not use \'@\' codes in @PROMPT fields');
                        }
                        else
                        {
                            $start = UserDateToSQLDate(Sanitize::SanitizeString($_POST[$RealFieldName.'_start']));
                            $end = UserDateToSQLDate(Sanitize::SanitizeString($_POST[$RealFieldName.'_end']));

                            if ($start == '@DATE_ERROR')
                            {
                                $PromptComplete = false;
                                AddMangledSearchError($RealFieldName, Sanitize::SanitizeString($_POST[$RealFieldName.'_start']));
                                break;
                            }

                            if ($end == '@DATE_ERROR')
                            {
                                $PromptComplete = false;
                                AddMangledSearchError($RealFieldName, Sanitize::SanitizeString($_POST[$RealFieldName.'_end']));
                                break;
                            }

                            $_SESSION[$module]["PROMPT"]["VALUES"][$RealFieldName]['start'] = Sanitize::SanitizeString($_POST[$RealFieldName.'_start']);
                            $_SESSION[$module]["PROMPT"]["VALUES"][$RealFieldName]['end'] = Sanitize::SanitizeString($_POST[$RealFieldName.'_end']);

                            $_SESSION['NEW_PROMPT_VALUES'][$newPromptName]['start'] = Sanitize::SanitizeString($_POST[$RealFieldName.'_start']);
                            $_SESSION['NEW_PROMPT_VALUES'][$newPromptName]['end'] = Sanitize::SanitizeString($_POST[$RealFieldName.'_end']);

                            $FieldWithCasting = 'CAST( FLOOR( CAST( '.$Field.' AS FLOAT ) ) AS DATETIME)';

                            $NewWhere[] = '1=1';

                            if ($_POST[$RealFieldName.'_start'] && $_POST[$RealFieldName.'_start'] != 'NULL')
                            {
                                $NewWhere[] = '('.$FieldWithCasting.' >= \''.UserDateToSQLDate(Sanitize::SanitizeString($_POST[$RealFieldName.'_start'])).'\')';
                            }

                            if ($_POST[$RealFieldName.'_end'] && $_POST[$RealFieldName.'_end'] != 'NULL')
                            {
                                $NewWhere[] = '('.$FieldWithCasting.' <= \''.UserDateToSQLDate(Sanitize::SanitizeString($_POST[$RealFieldName.'_end'])).'\')';
                            }

                            if ($isUDF)
                            {
                                $replacement = implode(' AND ',$NewWhere);
                                if ($replacement != '1=1')
                                {
                                    // we need to retain the beginning portion of the matched string
                                    // which contains the udf_values field_id and mod_id
                                    $ReplaceWith = preg_replace("/CAST.*/u", $replacement, $Match['matchstring']);
                                }
                                else
                                {
                                    $ReplaceWith = '1=1';
                                    $aux = str_replace("'", "\'", $Match['matchstring']);
                                    $aux = str_replace("(", "\(", $aux);
                                    $aux = str_replace(")", "\)", $aux);

                                    $pattern = '/\('.$table.'\.recordid IN \(SELECT cas_id FROM '.$aux.'\){2}/iu';

                                    if (preg_match($pattern, $whereString, $result) !== false)
                                    {
                                        $Match['matchstring'] = $result[0];
                                    }
                                }
                            }
                            else
                            {
                                $ReplaceWith = implode(' AND ',$NewWhere);
                            }

                            unset($NewWhere);
                            $whereString = str_replace($Match['matchstring'], $ReplaceWith, $whereString);
                        }
                        break;
                    
                    case 'ff_select':
                    case 'C':
                    case 'Y':
                    case 'yesno':
                        if (mb_substr_count($_POST[$RealFieldName], '@')>0)
                        {
                            $PromptComplete = false;
                            AddSessionMessage('ERROR', 'Error with \''.GetFieldLabel($RealFieldName).'\' field : You may not use \'@\' codes in @PROMPT fields');
                        }
                        else
                        {
                            if ($_POST[$RealFieldName])
                            {
                                if ($isUDF)
                                {
                                    $replacement = $Field.' IN (\''.implode('\',\'',explode('|',Sanitize::SanitizeString($_POST[$RealFieldName]))).'\')';
                                    $ReplaceWith = preg_replace("/$Field.*/u", $replacement, $Match['matchstring']);
                                }
                                else
                                {
                                    $ReplaceWith = '('.$Field.' IN (\''.implode('\',\'',explode('|',Sanitize::SanitizeString($_POST[$RealFieldName]))).'\'))';
                                }
                            }
                            else
                            {
                                if ($isUDF)
                                {
                                    $ReplaceWith = '1=1';
                                    $aux = str_replace("'", "\'", $Match['matchstring']);
                                    $aux = str_replace("(", "\(", $aux);

                                    $pattern = '/\('.$table.'\.recordid IN \(SELECT cas_id FROM '.$aux.'\){2}/iu';

                                    if (preg_match($pattern, $whereString, $result) !== false)
                                    {
                                        $Match['matchstring'] = $result[0];
                                    }
                                }
                                else
                                {
                                    $ReplaceWith = '1=1';
                                }
                            }

                            $whereString = str_replace($Match['matchstring'], $ReplaceWith, $whereString);
                            $_SESSION[$module]["PROMPT"]["VALUES"][$RealFieldName] = Sanitize::SanitizeString($_POST[$RealFieldName]);

                            if ($newPromptName == $ModuleDefs[$module]['TABLE'].'.'.$RealFieldName)
                            {
                                $_SESSION['NEW_PROMPT_VALUES'][$newPromptName] = Sanitize::SanitizeString($_POST[$RealFieldName]);
                            }
                            else
                            {
                                $_SESSION['NEW_PROMPT_VALUES'][$ModuleDefs[$module]['TABLE'].'.'.$newPromptName] = Sanitize::SanitizeString($_POST[$RealFieldName]);
                            }
                        }
                        break;

                    case 'multilistbox':
                    case 'T':
                        if (mb_substr_count($_POST[$RealFieldName], '@')>0)
                        {
                            $PromptComplete = false;
                            AddSessionMessage('ERROR', 'Error with \''.GetFieldLabel($RealFieldName).'\' field : You may not use \'@\' codes in @PROMPT fields');
                        }
                        else
                        {
                            if ($_POST[$RealFieldName])
                            {
                                $searchArray = explode('|',Sanitize::SanitizeString($_POST[$RealFieldName]));
                                $replacement = "";
                                
                                foreach ($searchArray as $value)
                                {
                                    $replacement .= "$Field = '$value' OR $Field like '$value %' OR $Field like '% $value' OR $Field like '% $value %' OR ";
                                }
                                
                                $replacement = \UnicodeString::substr_replace($replacement, "", -4);
                            }
                            else
                            {
                                $replacement = '1=1';
                            }
                            
                            if ($isUDF)
                            {
                                if ($replacement != '1=1')
                                {
                                    $ReplaceWith = preg_replace("/$Field.*/u", $replacement, $Match['matchstring']);
                                }
                                else
                                {
                                    $ReplaceWith = '1=1';
                                    $aux = str_replace("'", "\'", $Match['matchstring']);
                                    $aux = str_replace("(", "\(", $aux);

                                    $pattern = '/\('.$table.'\.recordid IN \(SELECT cas_id FROM '.$aux.'\){3}/iu';

                                    if (preg_match($pattern, $whereString, $result) !== false)
                                    {
                                        $Match['matchstring'] = $result[0];
                                    }

                                    //This use case is not compatible with the originalmatchstring code, I guess probably
                                    //because this is earlier code and is solving the same problem in a different way.
                                    $Match['originalmatchstring'] = null;
                                }
                            }
                            else
                            {
                                $ReplaceWith = $replacement;
                            }

                            if (substr_count($whereString, $Match['matchstring']) == 0 && isset($Match['originalmatchstring']) && $Match['originalmatchstring'] != '')
                            {
                                $whereString = str_replace($Match['originalmatchstring'], $ReplaceWith, $whereString);
                            }
                            else
                            {
                                $whereString = str_replace($Match['matchstring'], $ReplaceWith, $whereString);
                            }

                            $_SESSION[$module]["PROMPT"]["VALUES"][$RealFieldName] = Sanitize::SanitizeString($_POST[$RealFieldName]);
                            $_SESSION['NEW_PROMPT_VALUES'][$ModuleDefs[$module]['TABLE'].'.'.$newPromptName] = Sanitize::SanitizeString($_POST[$RealFieldName]);
                        }
                        break;
                }
            }
    }

    return $whereString;
}

//Replaces @prompt values in a where clause from the session
function ReplacePrompts($WhereClause, $Module, $bSQLTest = false)
{
    $MatchArray = CheckForPrompt($WhereClause,$Module);

    if (!empty($MatchArray))
    {
        foreach($MatchArray as $Match)
        {
            $Field = $Match['field'];
            $Type = $Match['type'];

            if (\UnicodeString::substr($Field, 0, 3) == 'UDF')
            {
                $RealFieldName = $Field;
                $Field = ($Type == 'D' ? 'udv_date' : 'udv_string');
                $isUDF = true;
            }
            else
            {
                $RealFieldName = RemoveTableFromFieldName($Field);
                $isUDF = false;
            }

            if ($_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName] != '' || $bSQLTest)
            {
                switch ($Type)
                {
                    case 'date':
                    case 'D':
                        if ($bSQLTest)
                        {
                            if ($isUDF)
                            {
                                // we need to retain the beginning portion of the matched string
                                // which contains the udf_values field_id and mod_id
                                $replacement = '('.$Field.' >= \'\')';
                                $ReplaceWith = preg_replace("/CAST.*/u", $replacement, $Match['matchstring']);

                                $WhereClause = str_replace($Match['matchstring'], $ReplaceWith, $WhereClause);
                            }
                            else
                            {
                                $WhereClause = str_replace($Match['matchstring'], '('.$Field.' >= \'\')', $WhereClause);
                            }
                        }
                        else
                        {
                            $FieldWithCasting = 'CAST( FLOOR( CAST( '.$Field.' AS FLOAT ) ) AS DATETIME)';
                            $NewWhere[] = '1=1';

                            if (!empty($_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]['start']) && $_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]['start'] != 'NULL')
                            {
                                $NewWhere[] = '('.$FieldWithCasting.' >= \''.UserDateToSQLDate($_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]['start']).'\')';
                            }


                            if (!empty($_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]['end']) && $_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]['end'] != 'NULL')
                            {
                                $NewWhere[] = '('.$FieldWithCasting.' <= \''.UserDateToSQLDate($_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]['end']).'\')';
                            }

                            if ($isUDF)
                            {
                                // we need to retain the beginning portion of the matched string
                                // which contains the udf_values field_id and mod_id
                                $replacement = implode(' AND ',$NewWhere);
                                $ReplaceWith = preg_replace("/CAST.*/u", $replacement, $Match['matchstring']);
                            }
                            else
                            {
                                $ReplaceWith = implode(' AND ',$NewWhere);
                            }

                            unset($NewWhere);
                            $WhereClause = str_replace($Match['matchstring'], $ReplaceWith, $WhereClause);

                        }

                        break;
                    case 'ff_select':
                    case 'C':
                    case 'Y':
                        if ($bSQLTest)
                        {
                            if ($isUDF)
                            {
                                $replacement = '('.$Field.' IN (\'\'))';
                                $ReplaceWith = preg_replace("/$Field.*/u", $replacement, $Match['matchstring']);

                                $WhereClause = str_replace($Match['matchstring'], $ReplaceWith, $WhereClause);
                            }
                            else
                            {
                                $WhereClause = str_replace($Match['matchstring'], '('.$Field.' IN (\'\'))', $WhereClause);
                            }
                        }
                        else
                        {
                            if ($isUDF)
                            {
                                $replacement = $Field.' IN (\''.implode('\',\'',explode('|',$_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName])).'\')';
                                $ReplaceWith = preg_replace("/$Field.*/u", $replacement, $Match['matchstring']);

                                $WhereClause = str_replace($Match['matchstring'], $ReplaceWith, $WhereClause);
                            }
                            else
                            {
                                $WhereClause = str_replace($Match['matchstring'], '('.$Field.' IN (\''.str_replace('|', "', '", $_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]).'\'))', $WhereClause);
                            }
                        }

                        break;
                    case 'multilistbox':
                    case 'T':
                        if ($bSQLTest)
                        {
                            if ($isUDF)
                            {
                                $replacement = '('.$Field.' IN (\'\'))';
                                $ReplaceWith = preg_replace("/$Field.*/u", $replacement, $Match['matchstring']);

                                $WhereClause = str_replace($Match['matchstring'], $ReplaceWith, $WhereClause);
                            }
                            else
                            {
                                $WhereClause = str_replace($Match['matchstring'], '('.$Field.' IN (\'\'))', $WhereClause);
                            }
                        }
                        else
                        {
                            $searchArray = explode('|', $_SESSION[$Module]['PROMPT']['VALUES'][$RealFieldName]);
                            $replacement = "";

                            foreach ($searchArray as $value)
                            {
                                $replacement .= "$Field = '$value' OR $Field like '$value %' OR $Field like '% $value' OR $Field like '% $value %' OR ";
                            }

                            $replacement = \UnicodeString::substr_replace($replacement, "", -4);

                            if ($isUDF)
                            {
                                $ReplaceWith = preg_replace("/$Field.*/u", $replacement, $Match['matchstring']);
                            }
                            else
                            {
                                $ReplaceWith = $replacement;
                            }

                            $WhereClause = str_replace($Match['matchstring'], $ReplaceWith, $WhereClause);
                        }

                        break;
                }
            }
        }
    }

    return $WhereClause;
}

/**
 * Function that gets all saved queries that are accessible to everyone.
 *
 * @param string $module The module to which the saved queries belong to.
 *
 * @return array $savedQueries A list of saved queries.
 */
function getSavedQueriesAccessibleToEveryone($module)
{
    $savedQueries = [];

    $sql = '
        SELECT
            recordid, sq_name
        FROM
            queries
        WHERE
            sq_module = :sq_module AND
            sq_user_id IS NULL AND
            (sq_type != :sq_type OR sq_type IS NULL)
        ORDER BY
            sq_name
    ';

    $result = DatixDBQuery::PDO_fetch_all($sql, [
        'sq_module' => $module,
        'sq_type'   => 'UI'
    ]);

    foreach ($result as $row)
    {
        $savedQueries[$row['recordid']] = $row['sq_name'];
    }

    return $savedQueries;
}

/**
 * Saves a query the old-fashion way as a where string instead of a where object.
 *
 * @param string $user_id                 The id of the user that is saving the query.
 * @param string $query_id                The id of the query.
 * @param SavedQueryModelFactory $factory The factory.
 *
 * @return \src\framework\model\Entity    The saved query object.
 */
function saveWhereAsString($user_id, $query_id, SavedQueryModelFactory $factory)
{
    // Bah, we'll have to save the where clause as a string then.  How old-fashioned.
    $queryData = [
        'module'       => $_POST['module'],
        'name'         => $_POST['sq_name'],
        'user_id'      => $user_id,
        'where_string' => $_POST['sq_where'],
        'old_query_id' => $query_id
    ];

    $savedQuery = $factory->getEntityFactory()->createObject($queryData);

    return $savedQuery;
}

?>
