<?php

class ConfigSender
{
   // var $ForceSend = true;
    
    /**
    * @desc Checks whether it is time to send data to Datix, and if so, returns a javascript instruction
    * that will trigger an ajax call to send the data.
    * 
    * @return string Javascript function.
    */
    function CheckSendToDatix()
    {
        if($_SESSION['AdminUser'] && $this->TimeToSend())
        {
            //set the time that we began processing the data sending
            SetGlobal('CONFIG_SENDING_BEGUN', time());
            $_SESSION['Globals']['CONFIG_SENDING_BEGUN'] = time();
            
            //return javascript that will make ajax call.
            return '
            jQuery(document).ready(function(){    
                jQuery.get(\''.$scripturl.'?action=httprequest&type=sendconfigtodatix\');
            });
            '; 
        }        
    }
    
    /**
    * @desc Checks whether it is appropriate to send data to Datix, based on the time elapsed since the last
    * successful transfer.
    * 
    * @return bool True if a new transfer is due, false if not.
    */
    function TimeToSend()
    {
        //CONFIG_SENDING_BEGUN will be non-zero if a transfer has begun but not finished. If this is more than 10 minutes ago,
        //we assume that it has failed and authorise another to be sent.
        $LastSendingBegun = GetParm('CONFIG_SENDING_BEGUN', 0);
        
        if($LastSendingBegun != 0)
        {
            if(time() - $LastSendingBegun < 600)
            {
                //sending has been triggered less than ten minutes ago - don't re-trigger it yet in case it is still being processed.
                return false;
            } 
        }

        if($this->ForceSend)
        {
            return true;    
        }
        
        //LAST_CONFIG_SENT tracks the last time a transfer was completed. If it has been less than a month since the last transfer, there is no need to re-send it.
        $LastSent = GetParm('LAST_CONFIG_SENT', 0);
        
        if($LastSent != 0)
        {
            if(time() - $LastSent < 2629743) //once a month
            {
                return false;
            } 
        }
        
        return true;
    }

    /**
    * @desc Pulls together the data to transfer, converts in to xml and pushes it through a web service to the central datix server.
    */
    function SendToDatix()
    {
        $XMLDoc = new DOMDocument();
        
        $XMLNode = $XMLDoc->createElement('xml');
        
        $HeaderNode = $XMLDoc->createElement('header');
        $HeaderNode->appendChild($XMLDoc->createElement('date', FormatDateVal(date("Y-m-d H:i:s.000"))));
        $HeaderNode->appendChild($XMLDoc->createElement('time', date("H:i:s")));
        
        $Client = GetParm('CLIENT');
        if($Client == 'DATIX') 
        { 
            $Client .= $_SESSION['login']; 
        }
        $HeaderNode->appendChild($XMLDoc->createElement('client', htmlspecialchars($Client)));
        
        $XMLNode->appendChild($HeaderNode);
        
        //All application global values
        $GlobalNode = $XMLDoc->createElement('globals');
        
        foreach($_SESSION['Globals'] as $GlobalName => $GlobalVal)
        {
            $Global = $XMLDoc->createElement('global', htmlspecialchars($GlobalVal));
            $Global->setAttribute('name', htmlspecialchars($GlobalName));
            $GlobalNode->appendChild($Global);
        }
        
        $XMLNode->appendChild($GlobalNode);

        //All php server variables
        $ServerVarsNode = $XMLDoc->createElement('servervars');   
        
        foreach($_SERVER as $ServerVarName => $ServerVarVal)
        {
            $ServerVar = $XMLDoc->createElement('servervar', htmlspecialchars($ServerVarVal));
            $ServerVar->setAttribute('name', htmlspecialchars($ServerVarName));
            $ServerVarsNode->appendChild($ServerVar);
        }
        
        $XMLNode->appendChild($ServerVarsNode);

        //All php ini settings
        $INISettingsNode = $XMLDoc->createElement('inisettings');   
        
        $inisettings = ini_get_all();
        
        foreach($inisettings as $ININame => $INIVals)
        {
            $INISetting = $XMLDoc->createElement('inisetting', htmlspecialchars($INIVals['global_value']));
            $INISetting->setAttribute('name', htmlspecialchars($ININame));
            $INISettingsNode->appendChild($INISetting);
        }
        
        $XMLNode->appendChild($INISettingsNode);
        
        $XMLDoc->appendChild($XMLNode);
        
        //connect to central datix server and call service "addClientConfigData"
        $server = new SoapClient('Source/libs/ConfigTracking/datix_central_server_interface.wsdl', array("trace"=> 1,"exceptions"=> 0));
        $ServerResponse = ($server->addClientConfigData($XMLDoc->saveXML()));
        //server response is in the format array(bool success, string message);

        if($ServerResponse['Result']) //config send sucessfully - we don't need to do it again for another month.
        {
            SetGlobal('LAST_CONFIG_SENT', time());
            $_SESSION['Globals']['LAST_CONFIG_SENT'] = time();
        }
        
        //permit the system to send another config if this one has failed.
        SetGlobal('CONFIG_SENDING_BEGUN', 0);
        $_SESSION['Globals']['CONFIG_SENDING_BEGUN'] = 0;
    }
}

?>
