<?php
  
    /**
    * @desc Called from ajax. Sends config to datix.
    */
    function SendToDatix()
    {
        require_once 'Source/libs/ConfigTracking/ConfigSenderClass.php';
        $ConfigSender = new ConfigSender();      
        $ConfigSender->SendToDatix();
    }
?>
