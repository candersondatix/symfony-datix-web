<?php

// function
function GetRiskGradingMatrix($paramArray)
{
    $ram = $paramArray[0];
    $FormAction = $paramArray[1];
    $module = $paramArray[3];

    if($module == 'COM')
    {
        $RiskRow = 'grading';
    }
    else
    {
        $RiskRow = $paramArray[2];
    }

    $returnArray = GetConsLikeli($paramArray);
    $consequence = $returnArray[0];
    $likelihood = $returnArray[1];

    $content = "
    <script language='javascript' type='text/javascript'>

    function getRatingLevel(module, consequence, likelihood, divid, riskrow)
    {
        new Ajax.Updater
        (
            divid,
            scripturl+'?action=httprequest&type=getratinglevel',
            {
                parameters:
                {
                    module : module,
                    consequence: consequence,
                    likelihood: likelihood,
                    riskrow: riskrow
                }
            }
        );
    }

    </script>
    ";

    if($module == 'COM' || $module == 'CLA')
    {
        $content .= '
            <input type="hidden" name="'.\UnicodeString::strtolower($module).'_consequence" id="'.\UnicodeString::strtolower($module).'_consequence" value="'.$ram[\UnicodeString::strtolower($module)."_consequence"].'" />
            <input type="hidden" name="'.\UnicodeString::strtolower($module).'_likelihood" id="'.\UnicodeString::strtolower($module).'_likelihood" value="'.$ram[\UnicodeString::strtolower($module)."_likelihood"].'" />
            <input type="hidden" name="CHANGED-'.\UnicodeString::strtolower($module).'_consequence" id="CHANGED-'.\UnicodeString::strtolower($module).'_consequence"/>
            <input type="hidden" name="CHANGED-'.\UnicodeString::strtolower($module).'_likelihood" id="CHANGED-'.\UnicodeString::strtolower($module).'_likelihood"/>';
    }
    else if($module == 'INC')
    {
        if ($RiskRow == 'initial')
        {
            $content .= '
                <input type="hidden" name="inc_consequence_initial" id="inc_consequence_initial" value="'.$ram["inc_consequence_initial"].'" />
                <input type="hidden" name="inc_likelihood_initial" id="inc_likelihood_initial" value="'.$ram["inc_likelihood_initial"].'" />
                <input type="hidden" name="CHANGED-inc_consequence_initial" id="CHANGED-inc_consequence_initial"/>
                <input type="hidden" name="CHANGED-inc_likelihood_initial" id="CHANGED-inc_likelihood_initial"/>';
        }
        if ($RiskRow == 'grading')
        {
            $content .= '
                <input type="hidden" name="inc_consequence" id="inc_consequence" value="'.$ram["inc_consequence"].'" />
                <input type="hidden" name="inc_likelihood" id="inc_likelihood" value="'.$ram["inc_likelihood"].'" />
                <input type="hidden" name="CHANGED-inc_consequence" id="CHANGED-inc_consequence"/>
                <input type="hidden" name="CHANGED-inc_likelihood" id="CHANGED-inc_likelihood"/>';
        }
    }
    elseif($module == 'RAM')
    {
        if ($RiskRow == 'initial')
        {
            $content .= '
                <input type="hidden" name="ram_consequence" id="ram_consequence" value="'.$ram["ram_consequence"].'" />
                <input type="hidden" name="ram_likelihood" id="ram_likelihood" value="'.$ram["ram_likelihood"].'" />
                <input type="hidden" name="CHANGED-ram_consequence" id="CHANGED-ram_consequence"/>
                <input type="hidden" name="CHANGED-ram_likelihood" id="CHANGED-ram_likelihood"/>';
        }
        if ($RiskRow == 'current')
        {
            $content .= '
                <input type="hidden" name="ram_cur_conseq" id="ram_cur_conseq" value="'.$ram["ram_cur_conseq"].'" />
                <input type="hidden" name="ram_cur_likeli" id="ram_cur_likeli" value="'.$ram["ram_cur_likeli"].'" />
                <input type="hidden" name="CHANGED-ram_cur_conseq" id="CHANGED-ram_cur_conseq"/>
                <input type="hidden" name="CHANGED-ram_cur_likeli" id="CHANGED-ram_cur_likeli"/>';
        }
        if ($RiskRow == 'target')
        {
            $content .= '
                <input type="hidden" name="ram_after_conseq" id="ram_after_conseq" value="'.$ram["ram_after_conseq"].'" />
                <input type="hidden" name="ram_after_likeli" id="ram_after_likeli" value="'.$ram["ram_after_likeli"].'" />
                <input type="hidden" name="CHANGED-ram_after_conseq" id="CHANGED-ram_after_conseq"/>
                <input type="hidden" name="CHANGED-ram_after_likeli" id="CHANGED-ram_after_likeli"/>';
        }
    }



    $content .= '<table class="formbg" width="100%" cellspacing="1" cellpadding="4" align="left" border="0">';

    if (is_array($consequence))
    {
        $colspan = count($consequence);
    }

    $content .= '<tr>
                    <td width="25%"></td>
                    <th class="risk_matrix_title" colspan="'.$colspan.'">';

    if ($module == 'COM' || $module == 'CLA')
    {
        $ConsFieldName = \UnicodeString::strtolower($module)."_consequence";
        $ConsTitle = Labels_FieldLabel::GetFieldLabel($ConsFieldName, _tk('consequence'));
    }
    elseif ($module == 'INC')
    {
        if ($RiskRow == 'initial')
        {
            $ConsFieldName = 'inc_consequence_initial';
        }

        if ($RiskRow == 'grading')
        {
            $ConsFieldName = 'inc_consequence';
        }

        $ConsTitle = Labels_FieldLabel::GetFieldLabel($ConsFieldName, _tk('consequence'));
    }
    elseif ($module == 'RAM')
    {
        if ($RiskRow == 'initial')
        {
            $ConsFieldName = 'ram_consequence';
        }

        if ($RiskRow == 'current')
        {
            $ConsFieldName = 'ram_cur_conseq';
        }

        if ($RiskRow == 'target')
        {
            $ConsFieldName = 'ram_after_conseq';
        }

        $ConsTitle = Labels_FieldLabel::GetFieldLabel($ConsFieldName, _tk('consequence'));
    }

    $content .= $ConsTitle;
    $content .= '   </th>
                </tr>
                <tr>
                    <th class="risk_matrix_title">';

    if ($module == 'COM' || $module == 'CLA')
    {
        $LikeFieldName = \UnicodeString::strtolower($module)."_likelihood";
        $LikeTitle .= Labels_FieldLabel::GetFieldLabel($LikeFieldName, _tk('likelihood'));
    }
    elseif ($module == 'INC')
    {
        if ($RiskRow == 'initial')
        {
            $LikeFieldName = 'inc_likelihood_initial';
        }

        if ($RiskRow == 'grading')
        {
            $LikeFieldName = 'inc_likelihood';
        }

        $LikeTitle .= Labels_FieldLabel::GetFieldLabel($LikeFieldName, _tk('likelihood'));
    }
    elseif ($module == 'RAM')
    {
        if ($RiskRow == 'initial')
        {
            $LikeFieldName = 'ram_likelihood';
        }

        if ($RiskRow == 'current')
        {
            $LikeFieldName = 'ram_cur_likeli';
        }

        if ($RiskRow == 'target')
        {
            $LikeFieldName = 'ram_after_likeli';
        }

        $LikeTitle = Labels_FieldLabel::GetFieldLabel($LikeFieldName, _tk('likelihood'));
    }

    $content .= $LikeTitle;
    $content .= '</th>';

    if (is_array($consequence))
    {
        foreach ($consequence as $code => $consarray)
        {
            $content .= '<th class="risk_matrix_header">'.$consarray['description'].'</th>';
        }
    }

    $content .= '</tr>';

    $gradingrow = GetGradingValues($module, $consequence, $likelihood);

    $width = floor(75/$colspan);

    if ($module == 'COM' || $module == 'CLA')
    {
        $LevelFieldName = \UnicodeString::strtolower($module).'_grade';
        $LevelTitle = Labels_FieldLabel::GetFieldLabel(\UnicodeString::strtolower($module)."_grade", _tk('grade'));
    }
    elseif ($module == 'INC')
    {
        if ($RiskRow == 'grading')
        {
            $LevelFieldName = 'inc_grade';
            $LevelTitle = Labels_FieldLabel::GetFieldLabel("inc_grade", _tk('grade'));
        }

        if ($RiskRow == 'initial')
        {
            $LevelFieldName = 'inc_grade_initial';
            $LevelTitle = Labels_FieldLabel::GetFieldLabel("inc_grade_initial", _tk('grade'));
        }
    }
    elseif ($module == 'RAM')
    {
        if ($RiskRow == 'initial')
        {
            $RatingFieldName = 'ram_rating';
            $LevelFieldName = 'ram_level';
            $RatingTitle = Labels_FieldLabel::GetFieldLabel("ram_rating", _tk('rating'));
            $LevelTitle = Labels_FieldLabel::GetFieldLabel("ram_level", _tk('level'));
        }

        if ($RiskRow == 'current')
        {
            $RatingFieldName = 'ram_cur_rating';
            $LevelFieldName = 'ram_cur_level';
            $RatingTitle = Labels_FieldLabel::GetFieldLabel("ram_cur_rating", _tk('rating'));
            $LevelTitle = Labels_FieldLabel::GetFieldLabel("ram_cur_level", _tk('level'));
        }

        if ($RiskRow == 'target')
        {
            $RatingFieldName = 'ram_after_rating';
            $LevelFieldName = 'ram_after_level';
            $RatingTitle = Labels_FieldLabel::GetFieldLabel("ram_after_rating", _tk('rating'));
            $LevelTitle = Labels_FieldLabel::GetFieldLabel("ram_after_level", _tk('level'));
        }
    }

    if (is_array($likelihood))
    {
        foreach ($likelihood as $likecode => $likeliarray)
        {
            $content .= '<tr><th class="risk_matrix_header">'.$likeliarray['description'].'</th>';

            foreach ($consequence as $conscode => $consarray)
            {
                $colour = $gradingrow[$conscode][$likecode] ? $gradingrow[$conscode][$likecode] : '';
                $content .= '<td class="windowbg2" style="'.($colour ? 'background-color:#'.$colour.'' : '') .';" align="center" width="'.$width.'%">';

                $content .= '<input type="radio" name="radio_'.$RiskRow.'" value="radio_'.$conscode.'_'.$likecode.'" onclick="getRatingLevel(\''.$module.'\', \''.$conscode.'\', \''.$likecode.'\', \'div_radio_'.$RiskRow.'\', \''.$RiskRow.'\');';
                $content .= '';

                if ($module == 'COM' || $module == 'CLA')
                {
                    $content .= " $('".\UnicodeString::strtolower($module)."_consequence').value='$conscode';setChanged('".\UnicodeString::strtolower($module)."_consequence');";
                    $content .= " $('".\UnicodeString::strtolower($module)."_likelihood').value='$likecode';setChanged('".\UnicodeString::strtolower($module)."_likelihood');";
                    $content .= '"';
                    $content .= ($ram[\UnicodeString::strtolower($module)."_consequence"] == (string)$conscode && $ram[\UnicodeString::strtolower($module)."_likelihood"] == (string)$likecode ? ' checked' : (($FormAction == "ReadOnly" || $FormAction == "Print") ? 'disabled' : ''));
                }
                elseif ($module == 'INC')
                {
                    if ($RiskRow == 'initial')
                    {
                        $content .= " $('inc_consequence_initial').value='$conscode';setChanged('inc_consequence_initial');";
                        $content .= " $('inc_likelihood_initial').value='$likecode';setChanged('inc_likelihood_initial');";
                        $content .= '"';
                        $content .= ($ram["inc_consequence_initial"] == (string)$conscode && $ram["inc_likelihood_initial"] == (string)$likecode ? ' checked' : (($FormAction == "ReadOnly" || $FormAction == "Print") ? 'disabled' : ''));
                    }

                    if ($RiskRow == 'grading')
                    {
                        $content .= " $('inc_consequence').value='$conscode';setChanged('inc_consequence');";
                        $content .= " $('inc_likelihood').value='$likecode';setChanged('inc_likelihood');";
                        $content .= '"';
                        $content .= ($ram["inc_consequence"] == (string)$conscode && $ram["inc_likelihood"] == (string)$likecode ? ' checked' : (($FormAction == "ReadOnly" || $FormAction == "Print") ? 'disabled' : ''));
                    }
                }
                elseif ($module == 'RAM')
                {
                    if ($RiskRow == 'initial')
                    {
                        $content .= " $('ram_consequence').value='$conscode';setChanged('ram_consequence');";
                        $content .= " $('ram_likelihood').value='$likecode';setChanged('ram_likelihood');";
                        $content .= '"';
                        $content .= ($ram["ram_consequence"] == (string)$conscode && $ram["ram_likelihood"] == (string)$likecode ? ' checked' : (($FormAction == "ReadOnly" || $FormAction == "Print") ? 'disabled' : ''));
                    }

                    if ($RiskRow == 'current')
                    {
                        $content .= " $('ram_cur_conseq').value='$conscode';setChanged('ram_cur_conseq');";
                        $content .= " $('ram_cur_likeli').value='$likecode';setChanged('ram_cur_likeli');";
                        $content .= '"';
                        $content .= ($ram["ram_cur_conseq"] == (string)$conscode && $ram["ram_cur_likeli"] == (string)$likecode ? ' checked' : (($FormAction == "ReadOnly" || $FormAction == "Print") ? 'disabled' : ''));
                    }

                    if ($RiskRow == 'target')
                    {
                        $content .= " $('ram_after_conseq').value='$conscode';setChanged('ram_after_conseq');";
                        $content .= " $('ram_after_likeli').value='$likecode';setChanged('ram_after_likeli');";
                        $content .= '"';
                        $content .= ($ram["ram_after_conseq"] == (string)$conscode && $ram["ram_after_likeli"] == (string)$likecode ? ' checked' : (($FormAction == "ReadOnly" || $FormAction == "Print") ? 'disabled' : ''));
                    }
                }

                $content .= '>';

                $content .= '</td>';
            }

            $content .= '</tr>';
        }
    }

    $content .= '<tr>
                 <td class="titlebg"></td>
                 <td class="titlebg" colspan="'.$colspan.'" align="center" id="div_radio_'.$RiskRow.'">';

    if (bYN(GetParm('RISK_MATRIX', 'N')) || $module == 'INC'  || $module == 'COM' || $module == 'CLA')
    {
        $sql = "
            SELECT
                code, description, lower_rating, upper_rating, cod_web_colour
            FROM
                code_inc_grades
            ORDER BY
                listorder
        ";
    }
    else
    {
        $sql = "
            SELECT
                code, description, lower_rating, upper_rating, cod_web_colour
            FROM
                code_ra_levels
            ORDER BY
                cod_listorder
        ";
    }

    $result = DatixDBQuery::PDO_fetch_all($sql);

    foreach ($result as $row)
    {
        $levels[$row['code']] = $row;
    }

    $bgColor = '';

    if ($module == 'RAM')
    {
        $content .= '<b>'.$RatingTitle.': </b><input type="text" name="'.$RatingFieldName.'"'.' id="'.$RatingFieldName.'" size="10" value="'.$ram[$RatingFieldName].'" readonly="readonly"/>';
        $content .= '<input type="hidden" name="CHANGED-'.$RatingFieldName.'" id="CHANGED-'.$RatingFieldName.'"/>';

        if ($levels[$ram[$LevelFieldName]]['cod_web_colour'])
        {
            $bgColor = 'style="background-color: #'.$levels[$ram[$LevelFieldName]]['cod_web_colour'].'"';
        }
        else
        {
            foreach ($levels as $level)
            {
                $ratingFieldNameExists = ($ram[$RatingFieldName] !== null);
                $lowerRatingExists     = ($level['lower_rating'] !== null);
                $upperRatingExists     = ($level['upper_rating'] !== null);
                $isRatingBetweenLevels = ($ram[$RatingFieldName] >= $level['lower_rating'] && $ram[$RatingFieldName] <= $level['upper_rating']);

                if ($ratingFieldNameExists && $lowerRatingExists && $upperRatingExists && $isRatingBetweenLevels)
                {
                    $bgColor = 'style="background-color: #' . $level['cod_web_colour'] . '"';
                    break;
                }
            }
        }
    }
    else
    {
        $bgColor = 'style="background-color: #'.$levels[$ram[$LevelFieldName]]['cod_web_colour'].'"';
    }

    $content .= ' <b>'.$LevelTitle.': </b><input type="hidden" name="'.$LevelFieldName.'"'.' id="'.$LevelFieldName.'" value="'.$ram[$LevelFieldName].'" />
            <input type="text" name="'.$LevelFieldName.'_Descr" id="'.$LevelFieldName.'_Descr" size="20" value="'.$levels[$ram[$LevelFieldName]]["description"].'" readonly="readonly" '.$bgColor.' />
            <input type="hidden" name="CHANGED-'.$LevelFieldName.'" id="CHANGED-'.$LevelFieldName.'" />';

    $content .= '</td>';
    $content .= '</tr>';
    $content .= '</table>';

    if ($_GET['full_audit'])
    {
        if ($module == 'INC' || $module == 'COM' || $module == 'CLA')
        {
            $content .= GetAuditLine(array(
                'module' => $module,
                'fieldName' => $RiskRow,
                'recordid' => $ram['recordid'],
                'fields' => array(
                    Labels_FieldLabel::GetFieldLabel(\UnicodeString::strtolower($module)."_likelihood", _tk('likelihood')) => \UnicodeString::strtolower($module).'_likelihood'.($RiskRow != 'grading' ? '_'.$RiskRow : ''),
                    Labels_FieldLabel::GetFieldLabel(\UnicodeString::strtolower($module)."_consequence", _tk('consequence')) => \UnicodeString::strtolower($module).'_consequence'.($RiskRow != 'grading' ? '_'.$RiskRow : ''),
                    Labels_FieldLabel::GetFieldLabel(\UnicodeString::strtolower($module)."_grade", _tk('grade')) => \UnicodeString::strtolower($module).'_grade'.($RiskRow != 'grading' ? '_'.$RiskRow : ''),
                    ),
                ));
        }
        if ($module == 'RAM')
        {
            $content .= GetAuditLine(array(
                'module' => $module,
                'fieldName' => $RiskRow,
                'recordid' => $ram['recordid'],
                'fields' => array(
                    $LikeTitle => $LikeFieldName,
                    $ConsTitle => $ConsFieldName,
                    $RatingTitle => $RatingFieldName,
                    $LevelTitle => $LevelFieldName,
                    ),
                ));
        }
    }

    return $content;
}

function GetGradingValues($module, $consequence, $likelihood)
{
    $riskMatrix = (bYN(GetParm('RISK_MATRIX', 'N')) || $module == 'INC' || $module == 'COM' || $module == 'CLA');
    $colours    = [];

    // Get risk level map colour information
    if ($riskMatrix)
    {
        $sql = '
            SELECT
                rating as lvl, cod_web_colour, consequence, likelihood, code, description
            FROM
                code_inc_grades, ratings_map
            WHERE
                code_inc_grades.code = ratings_map.rating
        ';

        $result = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($result as $colour)
        {
            $colours[$colour['consequence']][$colour['likelihood']] = $colour['cod_web_colour'];
        }
    }
    else
    {
        $sql = '
            SELECT
                lower_rating, upper_rating, code, description, cod_web_colour
            FROM
                code_ra_levels
            WHERE
                lower_rating IS NOT NULL AND
                upper_rating IS NOT NULL
            ORDER BY
                lower_rating, upper_rating
        ';

        $levelMap = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($likelihood as $likelihoodCode => $likelihoodArray)
        {
            foreach ($consequence as $consequenceCode => $consequenceArray)
            {
                $ratingValue = $consequenceCode * $likelihoodCode;

                foreach ($levelMap as $level)
                {
                    if ($ratingValue >= $level['lower_rating'] && $ratingValue <= $level['upper_rating'])
                    {
                        $colours[$likelihoodCode][$consequenceCode] = $level['cod_web_colour'];
                        break;
                    }
                }
            }
        }
    }

    return $colours;
}

function GetConsLikeli($paramArray)
{
    $ram = $paramArray[0];
    $module = $paramArray[3];
    $riskMatrix = (bYN(GetParm('RISK_MATRIX', 'N')) || $module == 'INC' || $module == 'COM' || $module == 'CLA');

    $sql = 'SELECT code, description, listorder FROM ' . ($riskMatrix ? 'code_inc_conseq' : 'code_ra_conseq');
    if ($FormAction != "Search")
    {
        if($module == 'INC' || $module == 'COM' || $module == 'CLA')
        {
            $sql .= ' WHERE cod_priv_level is null or cod_priv_level = \'\' or cod_priv_level = \'Y\'
                or code = \''.$ram["inc_consequence"].'\'';
        }
        elseif($module == 'RAM')
        {
            $sql .= ' WHERE cod_priv_level is null or cod_priv_level = \'\' or cod_priv_level = \'Y\'
                or code = \''.$ram["ram_consequence"].'\'
                or code = \''.$ram["ram_cur_conseq"].'\'
                or code = \''.$ram["ram_after_conseq"].'\'';
        }
    }

    $sql .= ' ORDER BY listorder ASC';

    $result = db_query($sql);

    while ($row = db_fetch_array($result))
    {
        $consequence[$row["code"]] = $row;
    }

    $sql = 'SELECT code, description, listorder FROM ' . ($riskMatrix ? 'code_inc_likeli' : 'code_ra_likeli');
    if ($FormAction != "Search")
    {
        if($module == 'INC' || $module == 'COM' || $module == 'CLA')
        {
            $sql .= ' WHERE cod_priv_level is null or cod_priv_level = \'\' or cod_priv_level = \'Y\'
                or code = \''.$ram["inc_likelihood"].'\'';
        }
        elseif($module == 'RAM')
        {
            $sql .= ' WHERE cod_priv_level is null or cod_priv_level = \'\' or cod_priv_level = \'Y\'
                or code = \''.$ram["ram_likelihood"].'\'
                or code = \''.$ram["ram_cur_likeli"].'\'
                or code = \''.$ram["ram_after_likeli"].'\'';
        }
    }
    $sql .= ' ORDER BY listorder DESC';

    $result = db_query($sql);

    while ($row = db_fetch_array($result))
    {
        $likelihood[$row["code"]] = $row;
    }

    $returnArray[0] = $consequence;
    $returnArray[1] = $likelihood;

    return  $returnArray;
}

function GetRatingLevel()
{
    $module      = Sanitize::SanitizeString($_REQUEST["module"]);
    $consequence = Sanitize::SanitizeString($_REQUEST["consequence"]);
    $likelihood  = Sanitize::SanitizeString($_REQUEST["likelihood"]);
    $riskRow     = Sanitize::SanitizeString($_REQUEST["riskrow"]);

    $riskMatrix = (bYN(GetParm('RISK_MATRIX', 'N')) || $module == 'INC' || $module == 'COM' || $module == 'CLA');

    if ($riskMatrix)
    {
        // Level
        $sql = 'SELECT rating as lvl, colour, consequence, likelihood, code, description, cod_web_colour
        FROM code_inc_grades, ratings_map
        WHERE code_inc_grades.code = ratings_map.rating
        AND ratings_map.consequence = \''.$consequence.'\'
        AND ratings_map.likelihood = \''.$likelihood.'\'';

        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row)
        {
            $levelDescr = $row['description'];
            $levelValue = $row['code'];
            $bgColor = 'style="background-color: #'.$row['cod_web_colour'].'"';
        }

        // Rating
        $sql = 'SELECT listorder FROM code_inc_conseq WHERE code = \''.$consequence.'\'';
        $consrow = DatixDBQuery::PDO_fetch($sql);

        if ($consrow)
        {
            $consRating = $consrow['listorder'];
        }

        $sql = 'SELECT listorder FROM code_inc_likeli WHERE code = \''.$likelihood.'\'';
        $likerow = DatixDBQuery::PDO_fetch($sql);

        if ($likerow)
        {
            $likeRating = $likerow['listorder'];
        }

        if ($module == 'COM' || $module == 'CLA')
        {
            $levelFieldName = \UnicodeString::strtolower($module).'_grade';
            $levelTitle = Labels_FieldLabel::GetFieldLabel(\UnicodeString::strtolower($module)."_grade", _tk('grade'));
        }
        elseif ($module == 'INC')
        {
            if ($riskRow == 'initial')
            {
                $levelFieldName = \UnicodeString::strtolower($module).'_grade_initial';
                $levelTitle = Labels_FieldLabel::GetFieldLabel(\UnicodeString::strtolower($module)."_grade_initial", _tk('grade'));
            }

            if ($riskRow == 'grading')
            {
                $levelFieldName = \UnicodeString::strtolower($module).'_grade';
                $levelTitle = Labels_FieldLabel::GetFieldLabel(\UnicodeString::strtolower($module)."_grade", _tk('grade'));
            }
        }
        elseif ($module = 'RAM')
        {
            if ($riskRow == 'initial')
            {
                $ratingFieldName = 'ram_rating';
                $levelFieldName = 'ram_level';
                $ratingTitle = Labels_FieldLabel::GetFieldLabel("ram_rating", _tk('rating'));
                $levelTitle = Labels_FieldLabel::GetFieldLabel("ram_level", _tk('level'));
            }
            elseif ($riskRow == 'current')
            {
                $ratingFieldName = 'ram_cur_rating';
                $levelFieldName = 'ram_cur_level';
                $ratingTitle = Labels_FieldLabel::GetFieldLabel("ram_cur_rating", _tk('rating'));
                $levelTitle = Labels_FieldLabel::GetFieldLabel("ram_cur_level", _tk('level'));
            }
            elseif ($riskRow == 'target')
            {
                $ratingFieldName = 'ram_after_rating';
                $levelFieldName = 'ram_after_level';
                $ratingTitle = Labels_FieldLabel::GetFieldLabel("ram_after_rating", _tk('rating'));
                $levelTitle = Labels_FieldLabel::GetFieldLabel("ram_after_level", _tk('level'));
            }
        }

        if ($module == 'RAM')
        {
            $content .= '<b>'. $ratingTitle . '</b>: <input type="text" name="'.$ratingFieldName.'"'.' id="'.$ratingFieldName.'" size="10" value="'.($consRating * $likeRating).'" readonly="readonly"/>';
            $content .= '<input type="hidden" name="CHANGED-'.$ratingFieldName.'" id="CHANGED-'.$ratingFieldName.'" value="1"/>';
        }

        $content .= ' <b>' . $levelTitle . '</b>: <input type="hidden" name="'.$levelFieldName.'"'.' id="'.$levelFieldName.'" value="'.$levelValue.'" />
                <input type="text" name="'.$levelFieldName.'_Descr" id="'.$levelFieldName.'_Descr" size="20" value="'.$levelDescr.'" readonly="readonly" '.$bgColor.' />
                <input type="hidden" name="CHANGED-'.$levelFieldName.'" id="CHANGED-'.$levelFieldName.'" value="1" />';

        // setting value="1" here means these will always audit, even if the value hasn't technically changed, but otherwise we'll have to start passing the current values around messily.
    }
    else
    {
        $sql = '
            SELECT
                lower_rating, upper_rating, code, description, cod_web_colour
            FROM
                code_ra_levels
            WHERE
                lower_rating IS NOT NULL AND
                upper_rating IS NOT NULL
            ORDER BY
                lower_rating, upper_rating
        ';

        $levelMap = DatixDBQuery::PDO_fetch_all($sql);

        $consequence = (int) $consequence;
        $likelihood  = (int) $likelihood;

        $ratingValue = $consequence * $likelihood;

        foreach ($levelMap as $level)
        {
            if ($ratingValue >= $level['lower_rating'] && $ratingValue <= $level['upper_rating'])
            {
                $levelValue = $level['code'];
                $levelDescr = $level['description'];
                $bgColor    = $level['cod_web_colour'];
                break;
            }
        }

        if ($riskRow == 'initial')
        {
            $ratingFieldName = 'ram_rating';
            $levelFieldName = 'ram_level';
            $ratingTitle = Labels_FieldLabel::GetFieldLabel("ram_rating", _tk('rating'));
            $levelTitle = Labels_FieldLabel::GetFieldLabel("ram_level", _tk('level'));
        }
        elseif ($riskRow == 'current')
        {
            $ratingFieldName = 'ram_cur_rating';
            $levelFieldName = 'ram_cur_level';
            $ratingTitle = Labels_FieldLabel::GetFieldLabel("ram_cur_rating", _tk('rating'));
            $levelTitle = Labels_FieldLabel::GetFieldLabel("ram_cur_level", _tk('level'));
        }
        elseif ($riskRow == 'target')
        {
            $ratingFieldName = 'ram_after_rating';
            $levelFieldName = 'ram_after_level';
            $ratingTitle = Labels_FieldLabel::GetFieldLabel("ram_after_rating", _tk('rating'));
            $levelTitle = Labels_FieldLabel::GetFieldLabel("ram_after_level", _tk('level'));
        }

        $content = '
            <b>'.$ratingTitle.'</b>: <input type="text" name="'.$ratingFieldName.'"'.' id="'.$ratingFieldName.'" size="10" value="'.$ratingValue.'" readonly="readonly" />
            <input type="hidden" name="CHANGED-'.$ratingFieldName.'" id="CHANGED-'.$ratingFieldName.'" value="1" />
            <b>'.$levelTitle.'</b>: <input type="hidden" name="'.$levelFieldName.'"'.' id="'.$levelFieldName.'" value="'.$levelValue.'" />
            <input type="text" name="'.$levelFieldName.'_Descr" id="'.$levelFieldName.'_Descr" size="20" value="'.$levelDescr.'" readonly="readonly" style="background-color: #'.$bgColor.';" />
            <input type="hidden" name="CHANGED-'.$levelFieldName.'" id="CHANGED-'.$levelFieldName.'" value="1" />
        ';
    }

    echo $content;
}

 /**
 * Prints out full-audit lines for non-standard risk matrix type fields.
 *
 * Getting the html in is a slight hack, but it looks similar to other implementations for the moment.
 * We could think about refactoring the way that this is done in the future.
 *
 * @param string $aParams['module'] The current module
 * @param string $aParams['recordid'] The current recordid
 * @param string $aParams['fieldName'] The parent field for which the audit is being run.
 * @param array $aParams['fields'] An array of the fields to be checked for in the audit table.
 *
 * @returns string The audit line including HTML formatting and structure.
 */
function GetAuditLine($aParams)
{
    global $ModuleDefs, $FieldDefs;

    $FullAudit = GetRiskRatingAudit($aParams);
    if (is_array($FullAudit))
    {
        foreach ($FullAudit as $Audit)	
        {
            //hacks the audit row into the form - there's almost certainly a better way of doing this.
            $contents .= '
            </div></li>
            <li class="field_div" id="aud_' . $aParams['fieldName'] .'">
                <div class="field_label_div" style="width: 25%;">
                '. code_descr($aParams['module'], $ModuleDefs[$aParams['module']]['FIELD_NAMES']['HANDLER'], $Audit["aud_login"]) . " " . FormatDateVal($Audit["aud_date"], true).
            '</div>
            <div class="field_input_div" style="width: 70%;">';

            foreach($aParams['fields'] as $FieldName => $Field)
            {
                if($Audit[$Field])
                {
                    if($FieldDefs[$aParams['module']][$Field]['Type'] == 'ff_select' || $FieldDefs[$aParams['module']][$Field]['Type'] == 'multicode')
                    {
                        $contents .= $FieldName.': '.code_descr_Grading($aParams['module'], $Field, $Audit[$Field]).'<br />';
                    }
                    else
                    {
                        $contents .= $FieldName.': '.$Audit[$Field].'<br />';
                    }
                }
                else
                {
                    $contents .= $FieldName.': &lt;no value&gt;<br />';
                }
            }
        }
    }

    return $contents;
}

 /**
 * Translates non-standard risk grading codes
 *
 * Sometimes the risk grading fields will use incident codelists, so under those circumstances we might need to translate
 * incident codes in risk fields. If it is not a field from this area, the normal code_descr() function is called
 *
 * @param string $module The current module
 * @param string $field The current field
 * @param string $value The code to be translated.
 *
 * @returns string The translated description of the code.
 */
function code_descr_Grading($module, $field, $value)
{
    if($module == 'RAM' && bYN(GetParm('RISK_MATRIX', 'N')))
    {
        if($field == 'ram_consequence' || $field == 'ram_cur_conseq' || $field == 'ram_after_conseq')
        {
            $sql = 'SELECT description FROM code_inc_conseq WHERE code = \''.$value.'\'';
        }
        if($field == 'ram_likelihood' || $field == 'ram_cur_likeli' || $field == 'ram_after_likeli')
        {
            $sql = 'SELECT description FROM code_inc_likeli WHERE code = \''.$value.'\'';
        }
        if($field == 'ram_level' || $field == 'ram_cur_level' || $field == 'ram_after_level')
        {
            $sql = 'SELECT description FROM code_inc_grades WHERE code = \''.$value.'\'';
        }

        if($sql)
        {
            $row = db_fetch_array(db_query($sql));
            return $row['description'];
        }
    }

    return code_descr($module, $field, $value);
}

 /**
 * Gets full audit data for risk rating fields for both incidents and risks from database and
 * collates the data by date to be read by GetAuditLine()
 *
 * @param string $aParams['module'] The current module
 * @param string $aParams['recordid'] The current recordid
 * @param array $aParams['fields'] An array of the fields to be checked for in the audit table.
 *
 * @returns array An array of audit data, keyed by date.
 */
function GetRiskRatingAudit($aParams)
{
    $fieldWhere = [];
    $rcFieldWhere = [];
    foreach($aParams['fields'] as $Field)
    {
        $fieldWhere[] = "aud_action = 'WEB:$Field'";
        $rcFieldWhere[] = "aud_detail like '%$Field =%'";
        $CurrentData[$Field] = '';
    }

    $fieldWhereString = implode(' OR ', $fieldWhere);
    $rcFieldWhereString = implode(' OR ', $rcFieldWhere);

    //aud_date_split is used to distinguish between updates made in the same minute, since php cannot distinuish these.
    $sql = 'SELECT aud_login, CAST(aud_date as float) as aud_date_split, aud_date, aud_action, aud_detail
        FROM full_audit
        WHERE aud_module = :aud_module
        AND aud_record = :aud_record
        AND ('.$fieldWhereString.')'.
        ($aParams['include_rich_client_data'] ? ' OR (aud_action = \'UPDATE\' AND ('.$rcFieldWhereString.'))' : '').
        ' ORDER BY aud_date ASC';

    $resultArray = DatixDBQuery::PDO_fetch_all($sql, array('aud_module' => $aParams['module'], 'aud_record' => $aParams['recordid']));

    $containsData = false;
    //Loops through audit rows and pulls out changes made at the same moment in batches.
    foreach($resultArray as $row)
    {
        if($CurrentDate == '')
        {
            $CurrentDate = $row['aud_date_split'];
            $CurrentData['aud_date'] = $row['aud_date'];
        }

        if($CurrentDate != $row['aud_date_split'])
        {
            if ($containsData)
            {
                $FullAudit[] = $CurrentData;
            }
            $CurrentDate = $row['aud_date_split'];
            $CurrentData['aud_date'] = $row['aud_date'];
        }

        $containsData = false;

        if ($row['aud_action'] == 'UPDATE') //data from the Rich Client
        {
            foreach($aParams['fields'] as $Field)
            {
                //Data from the RC is stored as part of a string of the form Field = Val, Field2 = Val, etc.
                if (preg_match_all('/'.$Field.' = ([^, ]*)(,|$)/iu', $row['aud_detail'], $matches) !== 0)
                {
                    if ($matches[1][0])
                    {
                        $containsData = true;
                        $CurrentData[$Field] = $matches[1][0];
                    }
                }
            }
        }
        else
        {
            $containsData = true;
            $CurrentData[str_replace('WEB:', '', $row['aud_action'])] = $row['aud_detail'];
        }

        $CurrentData['aud_login'] = $row['aud_login'];
    }
    if(count($resultArray) > 0)
    {
        $FullAudit[] = $CurrentData;
    }
    return $FullAudit;
}

/**
* Creates the risk grading section when global WEB_RISK_MATRIX = N
*
* @global array  $JSFunctions
*
* @param  string $paramArray[0]  The form data.
* @param  string $paramArray[1]  The form mode.
* @param  string $paramArray[3]  The module.
*
* @return string $section        The section HTML.
*/
function getRiskGradingFields($paramArray)
{
    global $JSFunctions;

    $module         = $paramArray[3];
    $RiskRow        = $paramArray[2];
    $consFieldName  = \UnicodeString::strtolower($module) . '_consequence';
    $likeFieldName  = \UnicodeString::strtolower($module) . '_likelihood';
    $levelFieldName = \UnicodeString::strtolower($module) . '_grade';
    $consFieldNameAlt  = \UnicodeString::strtolower($module) . '_consequence'.($RiskRow != 'grading' ? '_'.$RiskRow : '');
    $likeFieldNameAlt  = \UnicodeString::strtolower($module) . '_likelihood'.($RiskRow != 'grading' ? '_'.$RiskRow : '');
    $levelFieldNameAlt = \UnicodeString::strtolower($module) . '_grade'.($RiskRow != 'grading' ? '_'.$RiskRow : '');
    $consValue      = $paramArray[0][$consFieldNameAlt];
    $likeValue      = $paramArray[0][$likeFieldNameAlt];
    $levelValue     = $paramArray[0][$levelFieldNameAlt];
    $consTitle      = Labels_FieldLabel::GetFieldLabel($consFieldName, _tk('consequence'));
    $likeTitle      = Labels_FieldLabel::GetFieldLabel($likeFieldName, _tk('likelihood'));
    $levelTitle     = Labels_FieldLabel::GetFieldLabel($levelFieldName, _tk('grade'));
    $searchMode     = ($paramArray[1] == "Search");
    $onChange       = $searchMode ? '' : 'set'.$levelFieldNameAlt.'(false)';

    // create consequence and likelihood fields
    $consField = Forms_SelectFieldFactory::createSelectField($consFieldName, $module, $consValue, $paramArray[1], false,'','','',($RiskRow != 'grading' ? $RiskRow : ''));
    $consField->setOnChangeExtra($onChange);
    $likeField = Forms_SelectFieldFactory::createSelectField($likeFieldName, $module, $likeValue, $paramArray[1], false, '','','',($RiskRow != 'grading' ? $RiskRow : ''));
    $likeField->setOnChangeExtra($onChange);

    if ($searchMode)
    {
        // treat grade as a standard field
        $levelField = Forms_SelectFieldFactory::createSelectField($levelFieldName, $module, $levelValue, $paramArray[1], false, '','','',($RiskRow != 'grading' ? $RiskRow : ''));
    }
    else
    {
        // the grade field is populated on the basis of the values currently contained in the consequence/likelihood fields
        $class = ($paramArray[1] == 'ReadOnly' || $paramArray[1] == 'Print') ? 'style="padding:3px"' : 'class="codefield"';

        $levelFieldContents = '<input type="hidden" name="'.$levelFieldNameAlt.'" id="'.$levelFieldNameAlt.'" value="'.$levelValue.'" />
                    <div '.$class.' id="'.$levelFieldNameAlt.'_Descr">'.code_descr($module, $levelFieldName, $levelValue).'</div>';
        $levelField = new FormField($paramArray[1]);
        $levelField->MakeCustomField($levelFieldContents);
    }

    // build section
    $section = '
        <div style="clear:both">
            <div id="risk_grading_titles">
                <div style="height:26px">
                ' . $consTitle . ':&nbsp;
                </div>
                <div style="height:26px">
                ' . $likeTitle . ':&nbsp;
                </div>
                <div style="height:26px">
                ' . $levelTitle . ':&nbsp;
                </div>
            </div>
            <div id="risk_grading_fields">
                <div style="height:26px">
                ' . $consField->GetField() . '
                </div>
                <div style="height:26px">
                ' . $likeField->GetField() . '
                </div>
                <div style="height:26px">
                ' . $levelField->GetField() . '
                    <input type="hidden" name="CHANGED-'.$levelFieldNameAlt.'" id="CHANGED-'.$levelFieldNameAlt.'" />
                </div>
            </div>
        </div>';

    if ($_GET['full_audit'])
    {
         $section .= GetAuditLine(array(
            'module'    => $module,
            'fieldName' => \UnicodeString::strtolower($module) . '_grading',
            'recordid'  => $paramArray[0]['recordid'],
            'fields'    => array(	
                $likeTitle  => $likeFieldNameAlt,
                $consTitle  => $consFieldNameAlt,
                $levelTitle => $levelFieldNameAlt
            ),
         ));	
    }

    	    
    if (!$searchMode)
    {
        // construct javascript used to update grade field
        $sql = 'SELECT
                    r.consequence, r.likelihood, r.rating,
                    c.description, c.cod_web_colour
                FROM
                    ratings_map r
                INNER JOIN
                    code_inc_grades c ON c.code = r.rating';

        $gradeMap = PDO_fetch_all($sql);

        $javascript = 'var gradeMap = new Array();';
        foreach ($gradeMap as $row)
        {
            $javascript .= '
            gradeMap["'.$row['consequence'].'-'.$row['likelihood'].'"] = {"rating" : "'.$row['rating'].'", "description" : "'.$row['description'].'", "colour" : "#'.$row['cod_web_colour'].'"};';
        }

        $javascript .= '
            function set'.$levelFieldNameAlt.'(onInitialLoad)
            {
                var consValue = jQuery("#'.$consFieldNameAlt.'").val();
                var likeValue = jQuery("#'.$likeFieldNameAlt.'").val();
                if (consValue != "" && likeValue != "" && gradeMap[consValue+"-"+likeValue])
                {
                    jQuery("#'.$levelFieldNameAlt.'").val(gradeMap[consValue+"-"+likeValue]["rating"]);
                    jQuery("#'.$levelFieldNameAlt.'_Descr").html(gradeMap[consValue+"-"+likeValue]["description"]);
                    jQuery("#'.$levelFieldNameAlt.'_Descr").css("background-color", gradeMap[consValue+"-"+likeValue]["colour"]);
                }
                else
                {
                    jQuery("#'.$levelFieldNameAlt.'").val("");
                    jQuery("#'.$levelFieldNameAlt.'_Descr").html("");
                    ' . (($paramArray[1] == 'ReadOnly' || $paramArray[1] == 'Print') ? '' : 'jQuery("#'.$levelFieldNameAlt.'_Descr").css("background-color", "#FFFFFF");') . '
                }

                if (!onInitialLoad)
                {
                    jQuery("#CHANGED-'.$levelFieldNameAlt.'").val("1");
                }

            }
            jQuery(function(){set'.$levelFieldNameAlt.'(true);});';
        $JSFunctions[] = $javascript;
    }
    return $section;
}
?>
