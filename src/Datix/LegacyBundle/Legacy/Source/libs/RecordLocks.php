<?php

function CheckLock()
{
    $SQL = "SELECT lck_active FROM record_locks WHERE lck_sessionid = '".$_SESSION["session_id"]."'";

    $data = DatixDBQuery::PDO_fetch($SQL);

    $JSONdata['success'] = true;
    $JSONdata['lock_active'] = ($data["lck_active"] == 'Y');

    echo json_encode($JSONdata);
}

function CheckLockRecord()
{
    $recordArray = array('incident', 'risk', 'record', 'standard', 'element', 'prompt', 'evidence', 'sabs', 'editcontact', 'action', 'asset');

    if(in_array($_GET["action"], $recordArray))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function ResetTimer()
{
    if(bYN(GetParm("RECORD_LOCKING"), "N"))
    {
        if($_GET["timertype"] == "SES")
        {
            DatixDBQuery::CallStoredProcedure(array(
            "procedure" => "SetSessionActive",
            "parameters" => array(
                array("@session_id", $_SESSION["session_id"], SQLINT4),
                array("@lock_id", NULL, SQLINT4)
                )
            ));
        }
        else
        {
            DatixDBQuery::CallStoredProcedure(array(
            "procedure" => "SetSessionActive",
            "parameters" => array(
                array("@session_id", NULL, SQLINT4),
                array("@lock_id", $_GET["id"], SQLINT4)
                )
            ));
        }
    }
}

function UnlockRecord_ajax()
{
    UnlockRecord($_GET);
}

function UnlockRecord($aParams)
{
    if(bYN(GetParm("RECORD_LOCKING","N")) && (($aParams["table"] && $aParams["link_id"]) || $aParams["lock_id"]))
    {
        if(!$aParams["lock_id"])
        {
        	$params = array(
        		'lck_table' => $aParams["table"],
        		'lck_linkid' => $aParams["link_id"],
        		'lck_sessionid' => $_SESSION["session_id"]
        	);
            $sSQL = "SELECT recordid FROM record_locks"; 
            $sSQL .= " WHERE lck_table = :lck_table"; 
            $sSQL .= " AND lck_linkid = :lck_linkid";
            $sSQL .= " AND lck_sessionid= :lck_sessionid";

    		$row = PDO_fetch($sSQL, $params);
            $aParams["lock_id"] = $row["recordid"];
        }

        DatixDBQuery::CallStoredProcedure(
            array(
                "procedure" => "UnlockRecord",
                "parameters" => array(
                    array("@session_id", $_SESSION["session_id"], SQLINT4),
                    array("@lock_id", $aParams["lock_id"], SQLINT4)
                    )
                )
            );
    }
}

function LockRecord($aParams)
{
    $oResult = DatixDBQuery::CallStoredProcedure(
        array(
            "procedure" => "LockRecord",
            "parameters" => array(
                array("@link_id", $aParams["link_id"], SQLINT4),
                array("@table", $aParams["table"], SQLVARCHAR),
                array("@session_id", $_SESSION["session_id"], SQLINT4)
            )
        )
    );
        
    // process the resulting queries with the sp
    if (is_array($oResult) && sizeof($oResult) > 2)
    {
    	$res_offset = sizeof($oResult) - 2;
    	$data_offset = sizeof($oResult) - 1;
    	
	    if ($oResult[$res_offset][0] === "0")
	    {
	        //$sLockMessage = "You have locked this record for editing. Other users will only be able to access this record in read-only mode until you navigate away from it.";
	        $_SESSION["Record_Locks"]["Current_Lock"] = Sanitize::SanitizeString($oResult[$data_offset][0]);
	    }
        else
	    {
	        $aParams['lock_message'] = 'This record has been locked for editing by <B>'. Sanitize::SanitizeString($oResult[$data_offset][0]) .'</B>.<br/>Until this user is finished with the record, you will only be able to access it in read-only mode.';
	        $aParams['formtype'] = "Locked";
	    }
    }
    
    return $aParams;
}
