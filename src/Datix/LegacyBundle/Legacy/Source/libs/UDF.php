<?php

function SaveUDFs($RecordID, $module, $Suffix = '')
{
	foreach ($_POST as $Field => $Value)
	{
        $Parts = explode("_", $Field);
        $PDOParamsArray = array();
        $PDOUpdateParams = array();

        if (($Parts[0] != "SelectUDF" && $Parts[0] != "UDF" && $Parts[0] != 'CHANGED-UDF') || $Parts[5] == 'disabled')
        {
            continue; // Get next field
        }

        if ($Parts[0] == 'CHANGED-UDF')
        {
            // if no data has been passed for this field, it is empty and needs to be cleared out.
            if ($_POST[$Field] == '1' && $_POST[str_replace("CHANGED-UDF","UDF",$Field)] == "")
            {
                $Parts[0] = "UDF";
                $Value = null;
            }
            else
            {
                continue;
            }
        }

        if ($Parts[0]=="SelectUDF")
        {
            if (!$_POST[str_replace("SelectUDF_","UDF_",$Field)])
            {
                $_POST[str_replace("SelectUDF_","UDF_",$Field)] = "";
                $Parts[0] = "UDF";
            }
            else
            {
                continue;
            }
        }

		if ($Parts[0] == "UDF")
		{
			$UDFType = $Parts[1];
            $GroupID = $Parts[2];
            $FieldID = $Parts[3];
			$UDFSuffix = $Parts[4];

            $PDOParamsArray["field_id"] = $FieldID;

            if (isset($GroupID))
            {
                $PDOParamsArray["group_id"] = $GroupID;
            }

            $PDOParamsArray["mod_id"] = $module;
            $PDOParamsArray["cas_id"] = $RecordID;

            if (($Suffix && $UDFSuffix == $Suffix) || (!$Suffix && !$UDFSuffix))
            {
                if ($Value === '' || !isset($Value))
                {
                    // Delete extra field value
                    $sql =
                    'DELETE FROM udf_values
                    WHERE field_id = :field_id
                        AND mod_id = :mod_id
                        AND cas_id = :cas_id'.
                        (isset($GroupID) ? ' and group_id = :group_id' : '');

                    PDO_query($sql, $PDOParamsArray);

                    continue; // Get next field
                }

			    switch ($UDFType)
			    {
				    case "S":
				    case "Y":
				    case "C":
					    $ValueField = "udv_string";
					    break;
                    case "T":
                        $ValueField = "udv_string";

                        if (is_array($Value))
                        {
                            $Value =  implode(" ", $Value);
                        }

                        // We cannot use here empty() or !$Value because if the string is "0" it will be changed to ""
                        if ($Value == '')
                        {
                            $Value = "";
                        }

                        break;
				    case "D":
					    $ValueField = "udv_date";
					    $Value = UserDateToSQLDate($Value);
					    break;
				    case "N":
					    $ValueField = "udv_number";

                        if ($Value == "" || str_replace(" ","",$Value) == "") // make value null if string empty or just spaces.
                        {
                            $Value = null;
                        }
                        else
                        {
                            $Value = str_replace(',', '', $Value);
                        }

                        if (!is_numeric($Value))
                        {
                            $Value = null;
                        }

					    break;
                    case "M":
                        $ValueField = "udv_money";

                        if ($Value == "")
                        {
                            $Value = null;
                        }
                        else
                        {
                            $Value = preg_replace('/[^\.0-9]/u', '', $Value);
                        }

                        break;
                    case "L":
					    $ValueField = "udv_text";
					    break;
			    }

			    // Check to see if value exists - if so, update it
			    // Otherwise, insert it.
			    $sql = "SELECT $ValueField
			    FROM udf_values
			    WHERE field_id = :field_id
			    AND mod_id = :mod_id
			    AND cas_id = :cas_id".
                (isset($GroupID) ? ' AND group_id = :group_id' : '');

                $Query = new DatixDBQuery($sql);
                $request = $Query->prepareAndExecute($PDOParamsArray);

                $PDOUpdateParams["field_id"] = $FieldID;
                $PDOUpdateParams["mod_id"] = $module;
                $PDOUpdateParams["cas_id"] = $RecordID;

                if (isset($GroupID))
                {
                    $PDOUpdateParams["group_id"] = $GroupID;
                }

                $PDOUpdateParams["value"] = $Value;

			    if ($row = $Query->fetch())
			    {
				    $sql = "UPDATE udf_values
				    SET $ValueField = :value
				    WHERE field_id = :field_id
				    AND mod_id = :mod_id
				    AND cas_id = :cas_id".
                    (isset($GroupID) ? ' AND group_id = :group_id' : '');
			    }
			    else
			    {
				    $sql = "INSERT INTO udf_values
				    (mod_id, cas_id, group_id, field_id, $ValueField)
				    VALUES
				    (:mod_id, :cas_id, ".(isset($GroupID) ? ':group_id' : '0').", :field_id, :value)";
			    }

                PDO_query($sql, $PDOUpdateParams);
            }
		}
    }
}

// AddUDFRow() generates the html for a row containing a udf field within a search form.
// Called via AJAX dynamically or from PHP during pageload

function AddUDFRow($aParams = "")
{
    require_once 'Source/libs/FormClasses.php';

    if ($aParams)
    {
        $sMod = $aParams["mod"];
        $sId = $aParams["id"];
    }
    else
    {
        $sMod = Sanitize::getModule($_GET["mod"]);
        $sId = Sanitize::SanitizeInt($_GET["id"]);
    }

    $SELECTParams["recordid"] = $sId;
    $sql = "SELECT fld_name, fld_type, recordid FROM UDF_FIELDS WHERE recordid = :recordid ORDER BY fld_name";
    $Query = new DatixDBQuery($sql);
    $request = $Query->prepareAndExecute($SELECTParams);
    $row = $Query->fetch();

    $sType = $row["fld_type"];

    $sHTML =
        '<table width="100%" cellspacing="0">
            <tr class="fmd_field_container">
                <td class="fmd_field_label">
                    <div><b>'
                        .$row['fld_name'].
                    '</b></div>
                </td>
                <td class="fmd_field">
                    <div>'.
                    getUDFFieldHTML($sMod, '', $sId, $sType).
                    '</div>
                </td>
            </tr>
        </table>';

    if ($aParams)
    {
        return $sHTML;
    }
    else
    {
        echo $sHTML;
    }

}

function getUDFFieldHTML($sMod, $sGrp, $sId, $sType)
{
    global $scripturl;

    $FieldName = htmlspecialchars("UDF_".$sType."_".$sGrp."_".$sId);

    switch ($sType)
    {
        case "D": // date field
            $FieldObj = new FormField('Search');
            $FieldObj->MakeDateField($FieldName,'');

            return $FieldObj->Field;
            break;
        case "M": // money
            return '<input type="text" name="'.$FieldName.'" id="'.$FieldName.'" size="10" value="" onchange="setChanged(\''.$FieldName.'\')" /><input type="hidden" name="CHANGED-'.$FieldName.'" id="CHANGED-'.$FieldName.'" />';
            break;
        case "N": // number
            return '<input type="text" maxlength="32" size="15" name="'.$FieldName.'" value="" onchange="setChanged(\''.$FieldName.'\')" /><input type="hidden" name="CHANGED-'.$FieldName.'" id="CHANGED-'.$FieldName.'" />';
            break;
        case "S": // string field
            return '<input type="text" size="15" name="'.$FieldName.'" value="" onchange="setChanged(\''.$FieldName.'\')" /><input type="hidden" name="CHANGED-'.$FieldName.'" id="CHANGED-'.$FieldName.'" />';
            break;
        case "L": // text field
            return '<textarea name="'.$FieldName.'" id="'.$FieldName.'" cols="70" rows="10"></textarea><input type="hidden" name="CHANGED-'.$FieldName.'" id="CHANGED-'.$FieldName.'" />';
            break;
        case "Y": // yes/no field
            return '<select name="'.$FieldName.'" value="" onchange="setChanged(\''.$FieldName.'\')" /><option value="">Choose</option><option value="Y">Yes</option><option value="N">No</option></select><input type="hidden" name="CHANGED-'.$FieldName.'" id="CHANGED-'.$FieldName.'" />';
            break;
        default:  // dropdowns
            $FieldObj = getUDFDropdownObj($sMod, $sId, $sGrp, $sType,"", "Search");

			return $FieldObj->GetField();
	}
}

function getUDFDropdownObj($sMod, $sId, $sGrp, $sType, $sValue, $Mode)
{
    $FieldName = "UDF_".$sType."_".$sGrp."_".$sId;

    $FieldObj = Forms_SelectFieldFactory::createSelectField($FieldName, $sMod, $sValue, $Mode, ($sType == "T"));
    return $FieldObj;
}

?>