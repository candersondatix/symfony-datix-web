<?php

use src\framework\registry\Registry;
use src\email\EmailSenderFactory;
use src\email\EmailCustomTemplate;
use src\contacts\model\ContactModelFactory;

/**
 * @desc Returns all contacts who have emailing enabled and an email address but are not in a security group.
 *
 * @param array $aParams Array of parameters
 * @param string $aParams['module'] Current module
 * @param string $aParams['values'] Additional values to select from the db
 * @param string $aParams['where'] Where clause to append with an AND
 * @param string $aParams['from'] Approval status code that record was at before save
 * @param string $aParams['to'] Approval status code record is being saved to.
 *
 * @return array Array of contacts to email
 */
function GetContactsToEmail($aParams)
{
    $module = $aParams['module'];
    $values = $aParams['values'];
    $where = $aParams['where'];

    $contacts = array();

    $email_param = array(
        "INC" => "DIF_STA_EMAIL_LOCS",
        "RAM" => "RISK_STA_EMAIL_LOCS",
        "PAL" => "PAL_STA_EMAIL_LOCS",
        "COM" => "COM_STA_EMAIL_LOCS",
        "CLA" => "CLA_STA_EMAIL_LOCS"
    );

    if (!array_key_exists($module, $email_param))
    {
        return null;
    }

    // Only select contacts/users who are not also member of any security group of any type
    $sql = "select distinct contacts_main.recordid as con_id" . ($values ? ", $values" : "") . "
	from contacts_main
	join user_parms p on contacts_main.login = p.login
	where
	con_email != '' AND con_email is not null
	and p.parameter = '" . $email_param[$module] . "'
	and p.parmvalue = 'Y'
	AND NOT EXISTS (
		SELECT * FROM sec_staff_group
		JOIN sec_groups ON sec_staff_group.grp_id = sec_groups.recordid
		WHERE sec_staff_group.con_id = contacts_main.recordid
	)
    AND
    (
        contacts_main.sta_profile IS NULL
        OR contacts_main.sta_profile = ''
        OR
        NOT EXISTS (
            SELECT * FROM link_profile_group
            WHERE lpg_profile = contacts_main.sta_profile
        )
    )";

    if ($where)
    {
        $sql .= " AND ({$where})";
    }

    $contactList = DatixDBQuery::PDO_fetch_all($sql, array());

    foreach ($contactList as $contact)
    {
        $contacts = $contacts + array($contact['con_id'] => $contact);
    }

    return $contacts;
}

/**
 * @desc Returns all contacts who have emailing enabled and an email address but are not in a security group.
 *
 * @param array $aParams Array of parameters
 *
 * @return array Array of groups to email
 */
function GetGroupsToEmail($aParams)
{
    $group_type = $aParams['group_type'] ?: GRP_TYPE_EMAIL;

    $sql = 'SELECT recordid from sec_groups
            where sec_groups.grp_type = (grp_type | ' . $group_type . ')';

    $groups = DatixDBQuery::PDO_fetch_all($sql, array(), PDO::FETCH_COLUMN);

    return $groups;
}

/**
* @desc Organises the sending of emails to reporter, handler and other users when a record is saved
*
* @param array $aParams Array of parameters
* @param string $aParams['module'] Current module
* @param string $aParams['from'] Approval status code that record was at before save
* @param string $aParams['to'] Approval status code record is being saved to.
* @param string $aParams['perms'] Permission level of current user.
* @param string $aParams['falseSend'] if this is active no emails are actually sent.
*
* @return string HTML output to show to the user
*/
function SendEmails($aParams)
{
    global $ModuleDefs, $txt;

    if($aParams['to'] == 'STCL')
    {
        return;
    }

    $SentList = [];

    if(isset($aParams['notList']))
    {
        $SentList = $aParams['notList'];
    }
    $Progress = $aParams['progressbar'];

    Registry::getInstance()->getLogger()->logEmail('SendEmails() called with parameters: '. var_export($aParams, true));

    $sql = '
        SELECT
            apac_email_reporter, apac_email_handler
        FROM
            approval_action
        WHERE
            apac_from = :apac_from
            AND
            apac_to = :apac_to
            AND
            access_level = :access_level
            AND
            module = :module
            AND
            apac_workflow = :apac_workflow';

    $row = DatixDBQuery::PDO_fetch($sql,
        array(
            'apac_from' => $aParams['from'],
            'apac_to' => $aParams['to'],
            'access_level' => $aParams['perms'],
            'module' => $aParams['module'],
            'apac_workflow' => GetWorkflowID($aParams['module'])
        )
    );

    $Errors           = array();
    $SuccessfulEmails = array();

    if ((bYN($row['apac_email_reporter']) && ($aParams['level'] == 1
        && bYN(GetParm($ModuleDefs[$aParams['module']]['EMAIL_REPORTER_GLOBAL'])))
        || ($aParams['level'] == 2 && bYN(GetParm('REP_EMAIL_'.$aParams['module'].'_2')))) && $aParams['falseSend'] != true)
    {
        SendEmailToReporter($aParams, $Errors, $SuccessfulEmails);
    }

    if ((bYN($row['apac_email_handler']) && ($aParams['level'] == 1
        && bYN(GetParm($ModuleDefs[$aParams['module']]['EMAIL_HANDLER_GLOBAL']))
        || ($aParams['level'] == 2
            && in_array('HNDLR', explode(' ', GetParm('WHO_EMAIL_'.$aParams['module'].'_2')))))) && $aParams['falseSend'] != true)
    {
        $handlerSent = SendEmailToHandler($aParams, $Errors, $SuccessfulEmails);

        if ($handlerSent)
        {
            $SentList[] = $handlerSent;
        }
    }

    //assume handler and manager get emailed at the same stages
    if ((bYN($row['apac_email_handler']) && ($aParams['level'] == 2
        && in_array('MNGR', explode(' ', GetParm('WHO_EMAIL_'.$aParams['module'].'_2'))))) && $aParams['falseSend'] != true)
    {
        $managerSent = SendEmailToManager($aParams, $Errors, $SuccessfulEmails);

        if ($managerSent)
        {
            $SentList[] = $managerSent;
        }
    }

    SendNotificationEmails($emailSender, $aParams, $Errors, $SuccessfulEmails, $SentList, $aParams['emailType']);

    if ($Progress)
    {
        echo '<script>document.getElementById("waiting").style.display="none";</script>';
        ob_flush();
        flush();
    }

    if (!empty($SuccessfulEmails))
    {
        $Output = '
        <div class="padded_wrapper">' . _tk("email_sent_confirm") . '</div>
        <div class="padded_wrapper"><div>'.implode('</div><div>', array_unique($SuccessfulEmails)).'</div></div>';
    }

    foreach ($emailSender->getFailedDomainValidation() as $recipient)
    {
        $blockedEmails[] = $recipient->fullname;
    }

    if (!empty($blockedEmails))
    {
        $blocked = '<div class="padded_wrapper"><div>'.implode('</div><div>', $blockedEmails).
            '</div></div>';
        $message = sprintf(_tk("emails_not_set_domain_not_permitted"), $blocked);
        $Output .= '
            <hr/>
            <div class="padded_wrapper">' . $message . '</div>';
    }

    if (!empty($Errors))
    {
        $Output .= '
        <div class="padded_wrapper form_error"><div>'.implode('</div><div>', $Errors).'</div></div>';
    }

    if (!$aParams['falseSend'])
    {
        return $Output;
    }
    else
    {
        return $SuccessfulEmails;
    }
}

/**
* @desc Takes a list of groups and decides wheich of their contacts should be emailed, returning this list as an array
*
* @param array $aParams Array of parameters
* @param string $aParams['module'] Current module
* @param string $aParams['from'] Approval status code that record was at before save
* @param string $aParams['to'] Approval status code record is being saved to.
* @param string $aParams['perms'] Permission level of current user.
* @param string $aParams['groups'] Array of groups to check.
*
* @return array Contacts to email
*/
function GetContactsToEmailFromGroups($aParams)
{
    global $ModuleDefs, $userParmCache;
    require_once "Source/security/SecurityBase.php";

    $contactIdsToEmail = array();

    $groups = $aParams['groups'];
    $module = $aParams['module'];
    $data = $aParams['data'];
    if (!isset($data['recordid'] ) && isset($aParams['recordid']))
    {
        $data['recordid'] = $aParams['recordid'];
    }

    $Table = $ModuleDefs[$module]['TABLE'];

    if (is_array($groups) && !empty($groups))
    {
        // Loop through the groups, checking the WHERE clauses match the incident.
        foreach ($groups as $grp_id)
        {
            \src\framework\registry\Registry::getInstance()->getLogger()->logEmail('Processing group '.$grp_id.' to decide whether members should be emailed.');

            // We need to reset this where clause here otherwise other security groups will use previously values
            $joinGroupWhere = '';

            $group_where = MakeGroupSecurityWhereClause($module, $grp_id, "", "", true);

            if ($group_where == '')
            {
                \src\framework\registry\Registry::getInstance()->getLogger()->logEmail('Group '.$grp_id.' does not have a where clause, so no emails are being sent.');
                continue; //the group does not have a where clause - no point in continuing.
            }

            // We can check the group clause at this point unless it has @USER codes, in which case it will need to
            // be checked on a user level.
            $RequireUserCheck = (mb_substr_count($group_where, '@USER') > 0 || mb_substr_count($group_where, '@user') > 0);

            if (!$RequireUserCheck)
            {
                $group_where = TranslateWhereCom($group_where);

                $sql = "SELECT recordid FROM $Table
                WHERE recordid = $data[recordid] AND $group_where";

                $resultArray = DatixDBQuery::PDO_fetch_all($sql);

                if (count($resultArray) == 0)
                {
                    \src\framework\registry\Registry::getInstance()->getLogger()->logEmail('Group '.$grp_id.' does not match the record that was submitted, so no emails are being sent.');
                    continue;  //the group does not match this record - no point in continuing.
                }
            }

            //We need to process every contact, in case there are contact-specific options set
            $contactIdsToEmailFromThisGroup = GetSecurityGroupUsers($grp_id);

            if (empty($contactIdsToEmailFromThisGroup))
            {
                \src\framework\registry\Registry::getInstance()->getLogger()->logEmail('Group '.$grp_id.' contains no contacts, so no emails are being sent.');
                continue; //the group contains no contacts - no point in continuing
            }

            $contactsRestrictedByOwnOnly = array();
            if (!bYN(GetParm('EMAIL_IGNORE_OWN_ONLY', 'Y')))
            {
                $contactsRestrictedByOwnOnly = getContactsRestrictedByOwnOnly($contactIdsToEmailFromThisGroup, $module);
            }

            if ($RequireUserCheck)
            {
                $joinGroupWhere = TranslateConCodeToJoin($group_where);
            }

            if (!empty($contactsRestrictedByOwnOnly))
            {
                if ($joinGroupWhere)
                {
                    $joinGroupWhere .= ' AND ';
                }
                else
                {
                    $joinGroupWhere = '';
                }

                $joinGroupWhere .= '
                        (((contacts_main.recordid IN ('.implode(', ', $contactsRestrictedByOwnOnly).')
                        AND ('.$ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . ' = contacts_main.initials'
                    . ' OR ' . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . ' IS NULL '
                    . ' OR ' . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . ' = \'\')))
                            OR
                        (contacts_main.recordid NOT IN ('.implode(', ', $contactsRestrictedByOwnOnly).')))
                            ';
            }

            if ($joinGroupWhere)
            {
                $query = (new \src\contacts\model\ContactModelFactory)->getQueryFactory()->getQuery();
                $query->select(['contacts_main.recordid']);
                $query->join($ModuleDefs[$module]['TABLE'], null, 'RIGHT', $joinGroupWhere);
                $query->setWhereStrings(['contacts_main.recordid IN ('.implode(',', $contactIdsToEmailFromThisGroup).') AND '.$ModuleDefs[$module]['TABLE'].'.recordid = '.$data['recordid']]);
                $query->orderBy(['contacts_main.recordid']);

                list($statement, $parameters) = (new \src\framework\query\SqlWriter)->writeStatement($query);
                $contactIdsToEmailFromThisGroup = DatixDBQuery::PDO_fetch_all($statement, $parameters, PDO::FETCH_COLUMN);
            }

            if (Registry::getInstance()->getLogger()->emailsToBeLogged())
            {
                if (empty($contactIdsToEmailFromThisGroup))
                {
                    \src\framework\registry\Registry::getInstance()->getLogger()->LogEmail('No contacts will be emailed due to membership of group ' . $grp_id);
                }
                else
                {
                    foreach ($contactIdsToEmailFromThisGroup as $con_id)
                    {
                        \src\framework\registry\Registry::getInstance()->getLogger()->LogEmail('Contact ' . $con_id . ' will be emailed due to membership of group ' . $grp_id);
                    }
                }
            }

            $contactIdsToEmail = array_merge($contactIdsToEmail, $contactIdsToEmailFromThisGroup);
        }
    }

    return $contactIdsToEmail;
}

function getContactsRestrictedByOwnOnly($sourceList, $module)
{
    $ModuleDefs = Registry::getInstance()->getModuleDefs();

    //we need to add the logic around OWN_ONLY that happens in MakeToplevelSecurityWhereClause(), but fit it in with the join.
    //First get all contacts with OWN_ONLY set.
    $ownOnlyContacts = GetAllContactsWithOwnOnlySet($module);

    $noOwnOnlyAccessLevels = array(
        'INC' => array('RM', 'DIF1'),
        'PAL' => array('RM', 'PAL1'),
        'RAM' => array('RM', 'RISK1'),
        'COM' => array('COM2', 'COM1'),
        'ACT' => array(),
    );

    $contacts_to_check = array();

    //Select set of users for which we need to check that they meet the own-only restriction.
    foreach ($sourceList as $contactRecordid)
    {
        if (in_array($contactRecordid, $ownOnlyContacts))
        {
            $userAccessLevels = GetUserAccessLevels($contactRecordid);
            if ($userAccessLevels[$ModuleDefs[$module]['PERM_GLOBAL']] && !in_array($userAccessLevels[$ModuleDefs[$module]['PERM_GLOBAL']], $noOwnOnlyAccessLevels))
            {
                $contacts_to_check[] = $contactRecordid;
            }
        }
    }

    return $contacts_to_check;
}

/**
 * @param string $where
 * @return string
 *
 * Removes @USER_xxx codes from a where clause and replaces them with references to contacts_main.xxx fields
 */
function TranslateConCodeToJoin($where)
{
    $where = translateStaticAtCodes($where);

    $fieldDefs = Registry::getInstance()->getFieldDefs();

    preg_match_all('/\'% @user_([^ \']+)\'/ui', $where, $matches);

    if (!empty($matches))
    {
        foreach ($matches[0] as $key => $match)
        {
            $where = UnicodeString::str_ireplace($match, '\'% \' + contacts_main.'.$matches[1][$key], $where);
        }
    }

    preg_match_all('/\'@user_([^ \']+) %\'/ui', $where, $matches);

    if (!empty($matches))
    {
        foreach ($matches[0] as $key => $match)
        {
            $where = UnicodeString::str_ireplace($match, 'contacts_main.'.$matches[1][$key].' + \' %\'', $where);
        }
    }

    preg_match_all('/\'% @user_([^ \']+) %\'/ui', $where, $matches);

    if (!empty($matches))
    {
        foreach ($matches[0] as $key => $match)
        {
            $where = UnicodeString::str_ireplace($match, '\'% \' + contacts_main.'.$matches[1][$key].' + \' %\'', $where);
        }
    }
	
    //See if there are any '@user codes' in the query
    preg_match_all('/\'@user_([^ \']+)\'/ui', $where, $matches);
	//If there are any @user codes in the query, then replace them with their corresponding field name
    if (!empty($matches))
    {
        foreach ($matches[0] as $key => $match)
        {
            //If the field name in the $fieldDefs object says that the field is of type MULTICODE, then try to generate a
            //more complicated replacement pattern, otherwise just use the @user codes and replace it with it's corresponding field name
            if ($fieldDefs['contacts_main.'.$matches[1][$key]]->getType() == \src\system\database\FieldInterface::MULTICODE)
            {
                //Attempt to extract any 'multicodes' from the where clause, so we can formulate a more complex replacement string
            	preg_match("/([\.A-Za-z_]+)[\s]*(not like|like|!=|=)[\s]*'@user_" . $matches[1][$key] . "'/iu", $where, $multicodeMatches);

                if(!empty($multicodeMatches))
                {
                    //If either of the fields are empty, a join should never be created.
                    $replacement = '(
                        contacts_main.'.$matches[1][$key].' != \'\'
                        AND contacts_main.'.$matches[1][$key].' IS NOT NULL
                        AND '.$multicodeMatches[1].' != \'\'
                        AND '.$multicodeMatches[1].' IS NOT NULL
                        AND
                        (';

                    if (in_array($multicodeMatches[2], array('not like', 'NOT LIKE', '!=')))
                    {
                        //When either the contacts or the record have blank values, there should be no match.
                        $replacement .= '
                        contacts_main.'.$matches[1][$key].' NOT LIKE \'% \' + '.$multicodeMatches[1].' + \' %\' AND
                        contacts_main.'.$matches[1][$key].' NOT LIKE '.$multicodeMatches[1].' + \' %\' AND
                        contacts_main.'.$matches[1][$key].' NOT LIKE \'% \' + '.$multicodeMatches[1].' AND
                        contacts_main.'.$matches[1][$key].' != '.$multicodeMatches[1];

                    }
                    else
                    {
                        $replacement .= '
                        contacts_main.'.$matches[1][$key].' LIKE \'% \' + '.$multicodeMatches[1].' + \' %\' OR
                        contacts_main.'.$matches[1][$key].' LIKE '.$multicodeMatches[1].' + \' %\' OR
                        contacts_main.'.$matches[1][$key].' LIKE \'% \' + '.$multicodeMatches[1].' OR
                        contacts_main.'.$matches[1][$key].' = '.$multicodeMatches[1];
                    }

                    $replacement .= '))';

	                $where = UnicodeString::str_ireplace($multicodeMatches[0], $replacement, $where);
            	}

            }
            else
            {
                $where = UnicodeString::str_ireplace($match, 'contacts_main.'.$matches[1][$key], $where);
            }
        }
    }

    return $where;
}

/**
 * @param $module
 * @return array
 *
 * Returns all contacts where a GetParm check of XXX_OWN_ONLY would return Y
 */
function GetAllContactsWithOwnOnlySet($module)
{
    $globalArray =
        array(
            'INC' => 'DIF_OWN_ONLY',
            'PAL' => 'PAL_OWN_ONLY',
            'RAM' => 'RISK_OWN_ONLY',
            'COM' => 'COM_OWN_ONLY',
            'ACT' => 'ACT_OWN_ONLY',
        );

    return DatixDBQuery::PDO_fetch_all('SELECT recordid FROM GetContactsByParameterValue(:parameter, :value)', array('parameter' => $globalArray[$module], 'value' => 'Y'), PDO::FETCH_COLUMN);
}

/**
* @desc Appends the "Top Level" security where clause to an existing contact where clause.
*
* @param string $module Current module.
* @param string $con_id Id of current contact.
* @param string $contact_where Current contact where clause
* @param bool $email Whether this is being done in relation to an email - needed within MakeToplevelSecurityWhereClause().
*
* @return string The full contact where clause
*/
function AddTopLevelSecClause($module, $con_id, $contact_where, $email = false)
{
    require_once 'Source/security/SecurityBase.php';

    $toplevelWhere = MakeToplevelSecurityWhereClause($module, $con_id, $email);

    if ($toplevelWhere)
    {
        if ($contact_where)
        {
            $contact_where = '('.implode('AND', array($contact_where, $toplevelWhere)).')';
        }
        else
        {
            $contact_where = $toplevelWhere;
        }
    }

    return $contact_where;
}

/**
* @desc Gets the handler's email address and sends an email notifying them of the submission of a record.
*
* @param array $aParams Array of parameters
* @param array $aParams['data'] Data array describing current record.
* @param string $aParams['module'] Current module.
* @param bool $aParams['progressbar'] True if there is a progressbar to update, false otherwise.
* @param array $Errors Reference to variable tracking current errors. NOT CURRENTLY USED IN THIS FUNCTION
* @param array $SuccessfulEmails Reference to variable tracking emails sent.
* @param array $ExcludeList List of people to ignore because they have already been sent emails.
*/
function GetContactsToEmailFromUsers($aParams)
{
    global $ModuleDefs;

    $module = $aParams['module'];
    $data = $aParams['data'];
    $todayStr = GetTodaysDate();
    $contacts = array();

    $aParams['values'] = 'initials, con_email, con_jobtitle, fullname, contacts_main.login as login';
    $aParams['where'] = "con_dod IS NULL AND (sta_daccessstart is null or sta_daccessstart <= '$todayStr') AND (sta_daccessend is null or sta_daccessend >= '$todayStr')";

    // Get all contacts whose email option is switched on
    $contacts_tmp = GetContactsToEmail($aParams);

    if (is_array($contacts_tmp) && !empty($contacts_tmp))
    {
        // For each user with e-mailing enabled, check if their security WHERE clause matches the incident.
        foreach ($contacts_tmp as $con_id => $values)
        {
            $contact_where = MakeUserSecurityWhereClause($module, $con_id, "", "", true);
            $contact_where = AddTopLevelSecClause($module, $con_id, $contact_where, true);

            if(!isset($data))
            {
                $data['recordid'] =  $aParams['recordid'];
            }

            $sql = "SELECT recordid AS con_id
                    FROM ".$ModuleDefs[$module]['TABLE']."
                    WHERE recordid = $data[recordid]";

            if ($contact_where)
            {
                $sql .= " AND $contact_where";
            }

            $request = db_query($sql);

            if ($row = db_fetch_array($request))
            {
                if (!array_key_exists($con_id, $contacts))
                {
                    $contacts[] = $con_id;
                }
            }
        }
    }

    return $contacts;
}

/**
* @desc Gets the handler's email address and sends an email notifying them of the submission of a record.
*
* @param array $aParams Array of parameters
* @param array $aParams['data'] Data array describing current record.
* @param string $aParams['module'] Current module.
* @param bool $aParams['progressbar'] True if there is a progressbar to update, false otherwise.
* @param string $aParams['falseSend'] if this is active no emails are actually sent.
* @param array $Errors Reference to variable tracking current errors. NOT CURRENTLY USED IN THIS FUNCTION
* @param array $SuccessfulEmails Reference to variable tracking emails sent.
* @param array $ExcludeList List of people to ignore because they have already been sent emails.
* @param array $emailType the type of email to send: 'UPDATE' or 'NEW'.
*/
function SendNotificationEmails(&$emailSender, $aParams, &$Errors, &$SuccessfulEmails, $ExcludeList = array(), $emailType = 'NEW')
{
    $Progress = $aParams['progressbar'];

    // Get all groups with email on
    $aParams['groups'] = GetGroupsToEmail($aParams);

    $contactsFromGroups = GetContactsToEmailFromGroups($aParams);

    Registry::getInstance()->getLogger()->logEmail('Calculating who to email. Based on groups, sending emails to '.
        (count($contactsFromGroups) > 0 ? 'IDs ' . implode(', ', $contactsFromGroups) : 'no-one'));

    if (count($contactsFromGroups) > 0)
    {
        $DenyParams = $aParams;
        $DenyParams['group_type'] = GRP_TYPE_DENY_ACCESS;
        $DenyParams['groups'] = GetGroupsToEmail($DenyParams);

        $contactsIdsFromDenyGroups = GetContactsToEmailFromGroups($DenyParams);

        if (!empty($contactsIdsFromDenyGroups))
        {
            $contactsFromGroups = array_diff($contactsFromGroups, $contactsIdsFromDenyGroups);

            if (Registry::getInstance()->getLogger()->emailsToBeLogged())
            {
                foreach ($contactsIdsFromDenyGroups as $deniedContactId)
                {
                    if (in_array($deniedContactId, $contactsFromGroups))
                    {
                        Registry::getInstance()->getLogger()->logEmail('Calculating who to email. Based on being in a deny group, preventing email being sent to contact with id ' . $deniedContactId);
                    }
                }
            }
        }
    }

    $contactsFromUsers = GetContactsToEmailFromUsers($aParams);

    if (Registry::getInstance()->getLogger()->emailsToBeLogged())
    {
        Registry::getInstance()->getLogger()->logEmail('Calculating who to email. Based on user settings, sending emails to'.
            (count($contactsFromUsers)>0 ?
                ': '.implode(', ', array_map(function($userDetails){ return $userDetails['con_email'].'('.$userDetails['fullname'].')'.'('.$userDetails['con_id'].')';}, $contactsFromUsers))
                : ' no-one'));
    }

    $contactsToEmail = array_merge($contactsFromGroups, $contactsFromUsers);

    if (!empty($aParams['notList']))
    {
        $contactsToEmail = array_diff($contactsToEmail, array_keys($aParams['notList']));
    }

    if($emailType == 'UPDATE')
    {
        $emailSender = EmailSenderFactory::createEmailSender($aParams['module'], 'UpdatedRecord');
    }
    else
    {
        $emailSender = EmailSenderFactory::createEmailSender($aParams['module'], 'Notify');
    }

    $contactsToEmailMinusExcluded = array_diff($contactsToEmail, $ExcludeList);
    $contactsToStillBeExcluded = array_intersect($contactsToEmail, $ExcludeList);

    try
    {
        $migrateDateString = Registry::getInstance()->getParm('RECORD_UPDATE_EMAIL_DATETIME');
        $migrateDateIsValid = $migrateDateString != '' && preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}(\.\d{3})?/', $migrateDateString) === 1;

        new \DateTime($migrateDateString);
        $insertIntoRecordUpdateEmailLog = bYN(GetParm('RECORD_UPDATE_EMAIL', 'N')) && $migrateDateIsValid;
    }
    catch (\Exception $e)
    {
        $insertIntoRecordUpdateEmailLog = false;
    }

    if (count($contactsToEmailMinusExcluded))
    {
        if ($Progress)
        {
            if (!isset($Progress->CurrentPercent))
            {
                $Progress->SetProgressBar(0);
            }

            // Get count of e-mails to be sent
            $progress_full = 100 - $Progress->CurrentPercent;
            $progress_increment = $progress_full/(count($contactsToEmail));
        }

        if (Registry::getInstance()->getLogger()->emailsToBeLogged())
        {
            foreach ($contactsToEmailMinusExcluded as $contactId)
            {
                Registry::getInstance()->getLogger()->logEmail('Sending notification e-mail to contact with ID ' . $contactId . ' when saving ' . $aParams['module'] . ' with ID ' . $aParams['data']['recordid']);
            }
        }
        if ($aParams['falseSend'] != true)
        {
            $query = (new \src\contacts\model\ContactModelFactory)->getQueryFactory()->getQuery();
            $query->setWhereStrings(['contacts_main.recordid IN ('.implode(',', $contactsToEmailMinusExcluded).')']);
            $query->orderBy(['contacts_main.recordid']);

            $recipientCollection = (new ContactModelFactory)->getCollection();
            $recipientCollection->setQuery($query);

            $emailSender->addRecipients($recipientCollection);
            $emailSender->sendEmails($aParams['data']);

            foreach ($emailSender->getSuccessful() as $recipient)
            {
                $SuccessfulEmails[$recipient->recordid] = $recipient->con_jobtitle.' '.$recipient->fullname;
            }

            if ($insertIntoRecordUpdateEmailLog)
            {
                insertIntoRecordUpdateEmailLog(array_merge($contactsToEmailMinusExcluded, $contactsToStillBeExcluded), $aParams['data']['recordid']);
            }
        }
        else
        {
            $SuccessfulEmails = $contactsToEmailMinusExcluded;
        }

        if ($Progress)
        {
            $Progress->IncrProgressBar($progress_increment);
        }
    }
    elseif ($insertIntoRecordUpdateEmailLog)
    {
        insertIntoRecordUpdateEmailLog($contactsToStillBeExcluded, $aParams['data']['recordid']);
    }
}

/**
* @desc Gets the handler's email address and sends an email notifying them of the submission of a record.
*
* @param array $aParams Array of parameters
* @param array $aParams['data'] Data array describing current record.
* @param string $aParams['module'] Current module.
* @param array $Errors Reference to variable tracking current errors. NOT CURRENTLY USED IN THIS FUNCTION
* @param array $SuccessfulEmails Reference to variable tracking emails sent.
*/
function SendEmailToHandler($aParams, &$Errors, &$SuccessfulEmails)
{
    global $ModuleDefs;

    // in future, we'll need to work out which template to use here.

    $ManagerInitials = $aParams['data'][$ModuleDefs[$aParams['module']]['FIELD_NAMES']['HANDLER']];

    if ($ManagerInitials)
    {
        $todayStr = GetTodaysDate();

        $sql = 'SELECT recordid as con_id, con_email, con_jobtitle, fullname, login
            FROM contacts_main
            WHERE initials = :initials
            AND con_email != \'\' AND con_email is not null AND con_dod IS NULL
            AND (sta_daccessstart is null or sta_daccessstart <= :sta_daccessstart)
            AND (sta_daccessend is null or sta_daccessend >= :sta_daccessend)';

        $row = DatixDBQuery::PDO_fetch($sql, array('initials' => $ManagerInitials, 'sta_daccessstart' => $todayStr, 'sta_daccessend' => $todayStr));

        $contactFactory = new \src\contacts\model\ContactModelFactory();

        if (!empty($row))
        {
            Registry::getInstance()->getLogger()->logEmail('Sending notification e-mail to handler: '. $row['fullname'].' ('.$row['con_email'].')' . ' when saving ' . $ModuleDefs[$aParams['module']]['REC_NAME'] . ' with ID '. $aParams['data']['recordid']);

            $recipient = $contactFactory->getMapper()->find($row['con_id'], true);

            $emailSender = EmailSenderFactory::createEmailSender($aParams['module'], 'Notify');
            $emailSender->addRecipient($recipient);
            $emailsent = $emailSender->sendEmails($aParams['data']);

            if ($emailsent == 1)
            {
                $SuccessfulEmails[$row['con_id']] = $row['con_jobtitle'].' '.$row['fullname'];
            }
        }
    }

    return $row['con_id'];
}

/**
* @desc Gets the manager's email address and sends an email notifying them of the submission of a record.
*
* @param array $aParams Array of parameters
* @param array $aParams['data'] Data array describing current record.
* @param string $aParams['module'] Current module.
* @param array $Errors Reference to variable tracking current errors. NOT CURRENTLY USED IN THIS FUNCTION
* @param array $SuccessfulEmails Reference to variable tracking emails sent.
*/
function SendEmailToManager($aParams, &$Errors, &$SuccessfulEmails)
{
    global $ModuleDefs;

    // in future, we'll need to work out which template to use here.

    $ManagerInitials = $aParams['data'][$ModuleDefs[$aParams['module']]['FIELD_NAMES']['MANAGER']];

    if ($ManagerInitials)
    {
        $todayStr = GetTodaysDate();

        $sql = 'SELECT recordid as con_id, con_email, con_jobtitle, fullname, login
            FROM contacts_main
            WHERE initials = :initials
            AND con_email != \'\' AND con_email is not null AND con_dod IS NULL
            AND (sta_daccessstart is null or sta_daccessstart <= :sta_daccessstart)
            AND (sta_daccessend is null or sta_daccessend >= :sta_daccessend)';

        $row = DatixDBQuery::PDO_fetch($sql, array('initials' => $ManagerInitials, 'sta_daccessstart' => $todayStr, 'sta_daccessend' => $todayStr));

        $contactFactory = new \src\contacts\model\ContactModelFactory();

        if (!empty($row))
        {
            Registry::getInstance()->getLogger()->logEmail('Sending notification e-mail to manager: '. $row['fullname'].' ('.$row['con_email'].')' . ' when saving ' . $ModuleDefs[$aParams['module']]['REC_NAME'] . ' with ID '. $aParams['data']['recordid']);

            $recipient = $contactFactory->getMapper()->find($row['con_id'], true);

            $emailSender = EmailSenderFactory::createEmailSender($aParams['module'], 'Notify');
            $emailSender->addRecipient($recipient);
            $emailsent = $emailSender->sendEmails($aParams['data']);

            if ($emailsent == 1)
            {
                $SuccessfulEmails[$row['con_id']] = $row['con_jobtitle'].' '.$row['fullname'];
            }
        }
    }

    return $row['con_id'];
}

function SendProgressEmailToReporters($aParams, $reportersToEmail)
{
    $ModuleDefs = Registry::getInstance()->getModuleDefs();

    foreach ($reportersToEmail as $reporterId)
    {
        $reporter = (new ContactModelFactory)->getMapper()->find($reporterId, true);

        Registry::getInstance()->getLogger()->logEmail('Sending progress update notification e-mail to reporter: '. $reporter->con_forenames .' '. $reporter->con_surname .' ('.$reporter->con_email.')' . ' when saving ' . $ModuleDefs[$aParams['module']]['REC_NAME'] . ' with ID '. $aParams['data']['recordid']);

        $emailSender = EmailSenderFactory::createEmailSender($aParams['module'], 'REPPROGRESS', GetParm('EMT_INC_PRO'));
        $emailSender->addRecipient($reporter);
        $emailSender->sendEmails($aParams['data']);
    }
}

/**
* @desc Gets the reporters email address (from the posted data or from the DB) and sends an email acknowledging submission of a record.
*
* @param array $aParams Array of parameters
* @param array $aParams['data'] Data array describing current record.
* @param string $aParams['module'] Current module.
* @param array $Errors Reference to variable tracking current errors. NOT CURRENTLY USED IN THIS FUNCTION
* @param array $SuccessfulEmails Reference to variable tracking emails sent. NOT CURRENTLY USED IN THIS FUNCTION
*/
function SendEmailToReporter($aParams, &$Errors, &$SuccessfulEmails)
{
    global $ModuleDefs;

    // in future, we'll need to work out which template to use here.

    $LinkedContactFunction = ($ModuleDefs[$aParams['module']]['GET_LINKED_CONTACTS_FUNCTION'] ? $ModuleDefs[$aParams['module']]['GET_LINKED_CONTACTS_FUNCTION'] : 'GetLinkedContacts');
    $contacts = $LinkedContactFunction(array('recordid' => $aParams['data']['recordid'], 'module' => $aParams['module'], 'formlevel' => 1), true);

    if (is_array($contacts['R']))
    {
        foreach ($contacts['R'] as $OtherContact)
        {
            if ($OtherContact['link_role'] == GetParm('REPORTER_ROLE', 'REP'))
            {
                $reporter = (new ContactModelFactory)->getMapper()->find($OtherContact['recordid']);
            }
        }
    }

    if (!$reporter)
    {
        if ($aParams['data']["inc_rep_email"])
        {
            Registry::getInstance()->getLogger()->logEmail('Sending acknowledgement e-mail to reporter: '. $aParams['data']["inc_repname"].' ('.$aParams['data']["inc_rep_email"].')' . ' when saving ' . $ModuleDefs[$aParams['module']]['REC_NAME'] . ' with ID '. $aParams['data']['recordid']);

            $emailSender = EmailSenderFactory::createEmailSender($aParams['module'], 'Acknowledge');
            $emailSender->addRecipientEmail($aParams['data']["inc_rep_email"]);
            $emailSender->sendEmails($aParams['data']);
        }
    }
    else
    {
        Registry::getInstance()->getLogger()->logEmail('Sending acknowledgement e-mail to reporter: '. $reporter->fullname.' ('.$reporter->con_email.')' . ' when saving ' . $ModuleDefs[$aParams['module']]['REC_NAME'] . ' with ID '. $aParams['data']['recordid']);

        $emailSender = EmailSenderFactory::createEmailSender($aParams['module'], 'Acknowledge');
        $emailSender->addRecipient($reporter);
        $emailSender->sendEmails($aParams['data']);
    }
}

/**
* @desc Returns the name of the user-defined e-mail template file for the specified module using the following logic:
*
* If the global $UserEmailTextFile is an array, the element whose key is the module code will be used as the filename.
* If $UserEmailTextFile is a string, its value will be used as the name of the file.
* If $UserEmailTextFile is not set, or it is an array but there is no entry for the module, the name of the file will be UserEmailText_<module>.php
* If the filename does not exist, the name of the file will be UserEmailText.php
*
* @param string $module The current module
*
* @return string The name of the file if it exists, an empty string if not.
*/
function GetUserEmailTextFile($module)
{
    global $ClientFolder, $UserEmailTextFile;

    $DefaultFile = "UserEmailText_$module.php";

    if (is_array($UserEmailTextFile) && $UserEmailTextFile[$module])
	{
        $EmailTextFile = $UserEmailTextFile[$module];
	}
    elseif ($UserEmailTextFile)
    {
        $EmailTextFile = $UserEmailTextFile;
	}
    else
    {
        $EmailTextFile = $DefaultFile;
    }

    $EmailTextFile = $ClientFolder . '/' . $EmailTextFile;

    if (!file_exists($EmailTextFile))
    {
        $EmailTextFile = $ClientFolder . '/UserEmailText.php';

        if (!file_exists($EmailTextFile))
        {
            $EmailTextFile = '';
        }
    }

    return $EmailTextFile;
}

/**
* @desc Called via AJAX. Takes a module and a recordid and sends appropriate emails as though that record had just been logged.
*/
function EmailAlertsService()
{
    global $dtxdebug, $ModuleDefs, $FieldDefs;

    $module = Sanitize::getModule($_POST["module"]);
    $recordid = Sanitize::SanitizeInt($_POST["recordid"]);
    $permissions = GetParm($ModuleDefs[$module]['PERM_GLOBAL']);

    $old_approval = ($_POST['old_approval'] ? Sanitize::SanitizeString($_POST['old_approval']) : 'NEW');
    $new_approval = Sanitize::SanitizeString($_POST['approval']);
    $perms = ($permissions? $permissions: 'NONE');

    $data = GetRecordData(array('module' => $module, 'recordid' => $recordid));

    SendEmails(array(
        'module' => $module,
        'data' => $data,
        'from' => $old_approval,
        'to' => $new_approval,
        'perms' => $perms,
        'level' => Sanitize::SanitizeInt($_POST['formlevel']),
        'emailType' => $old_approval == 'NEW' ? 'NEW' : 'UPDATE'
    ));
}

/**
* @desc Called via AJAX. Takes information posted from the feedback form and sends emails as appropriate, returning
* errors/success messages to the page via an echo.
*/
function SendFeedback()
{
    $module           = Sanitize::getModule($_POST['module']);
    $recordid         = Sanitize::SanitizeInt($_POST['recordid']);

    $fbk_to_conid = array();
    $fbk_gab_conid = array();

    if ($_POST["to"] != '')
    {
        $fbk_to_conid = explode(",", Sanitize::SanitizeString($_POST["to"]));
    }

    if ($_POST['fbk_gab'] != '')
    {
        $fbk_gab_conid = explode(',', Sanitize::SanitizeString($_POST['fbk_gab']));
    }

    $EmailErrors = '';

    // Need to strip quotes from post before mailing.
    $fbk_body_noquotes    = StripPostQuotes($_POST["body"]);
    $fbk_subject_noquotes = StripPostQuotes($_POST["subject"]);
    $fbk_html = $_POST['is_html'] == "true" ? true : false;

    $emailSender = EmailSenderFactory::createEmailSender($module, 'FBK_M');
    //TODO: need to get the HTML param from the template and use it to set isHTML for EmailCustomTemplate
    $emailSender->setTemplate(new EmailCustomTemplate($fbk_subject_noquotes, $fbk_body_noquotes, $fbk_html));

    $userFactory = new \src\users\model\UserModelFactory();
    $userFrom = $userFactory->getMapper()->findById($_SESSION["contact_login_id"], true);

    $emailSender->setFromUser($userFrom);

    if (is_array(array_merge($fbk_gab_conid, $fbk_to_conid)))
    {
        foreach (array_merge($fbk_gab_conid, $fbk_to_conid) as $conid)
        {
            if ($conid)
            {
                if (is_numeric($conid))
                {
                    $findRecipient = $userFactory->getMapper()->findById($conid, true);
                    if ($findRecipient == null)
                    {
                        $contactFactory = new \src\contacts\model\ContactModelFactory();
                        $findRecipient = $contactFactory->getMapper()->find($conid, true);
                    }
                    $emailSender->addRecipient($findRecipient);
                }
                else //else: it is an email address of a reporter who is not linked as a contact
                {
                    $emailSender->addRecipientEmail($conid);
                }
            }
        }
    }

    if($_POST["fbk_email"] != '')
    {
        foreach (explode(",", $_POST["fbk_email"]) as $con_email)
        {
            $emailSender->addRecipientEmail(Sanitize::SanitizeEmail($con_email));
        }
    }

    $emailSender->sendEmails(array('recordid' => $recordid));

    $successfulEmails = $emailSender->getSuccessful();
    if (!empty($successfulEmails))
    {
        $EmailResults = '<div><img src="Images/SendMail.png" /></div><div>'._tk('feedback_sent_full').'</div>';

        foreach ($successfulEmails as $recipientEmailAddress => $recipient)
        {
            $EmailResults .= '<div>'.$recipientEmailAddress.'</div>';
        }
    }

    $failedDomainValidation = $emailSender->getFailedDomainValidation();

    if (!empty($failedDomainValidation))
    {
        $failedDomainEmails = array ();
        foreach ($failedDomainValidation as $failedEmailAddress => $recipient)
        {
            $failedDomainEmails[] = $failedEmailAddress;
        }

        $EmailErrors[] = sprintf(_tk('emails_not_set_domain_not_permitted'), implode(', ', $failedDomainEmails));
    }

    $otherFailedEmails = array_merge($emailSender->getFailedEmailStructureValidation(), $emailSender->getFailedOther());

    if (!empty($otherFailedEmails))
    {
        foreach ($otherFailedEmails as $failedEmailAddress => $recipient)
        {
            $EmailErrors[] = 'There was an error sending the feedback to ' . $failedEmailAddress;
        }
    }

    if(!empty($EmailErrors))
    {
        $emailErrorText = '<div><img src="Images/Error_24.png" /></div>';
        foreach ($EmailErrors as $EmailError)
        {
            $emailErrorText .= '<div>'.$EmailError.'</div>';
        }
    }
    
    echo $EmailResults.$emailErrorText;
}

function insertIntoRecordUpdateEmailLog(array $contactsToExclude, $inc_id)
{
    $sql = 'DELETE FROM contact_email_history WHERE inc_id = :inc_id';
    \DatixDBQuery::PDO_query($sql, array('inc_id' => $inc_id));

    if (count($contactsToExclude) > 0)
    {
        $sql = "INSERT INTO contact_email_history (inc_id, con_id)\n";
        foreach ($contactsToExclude as $key => $con_id)
        {
            $sql .= "SELECT " . $inc_id . ", " . $con_id . " \n";

            if ($key + 1 < count($contactsToExclude))
            {
                $sql .= "UNION ALL \n";
            }
        }
        \DatixDBQuery::PDO_insert($sql);
    }
}
