<?php
use src\framework\registry\Registry;

/**
* @desc Wraps PDO classes, allowing us to perform our own SQL logging/error catching.
*/
class DatixDBQuery
{
    public $PDO; // PDO connection object
    var $PDOStatement; // PDOStatement object that is the basis for anything we will do here.
    public $sql; // sql string we are dealing with here

    /**
    * The actual query run (i.e. when all parameters have been bound)
    *
    * @var string
    */
    protected $rawQuery;
    
    /**
     * @var boolean
     */
    public $writeErrorsToFile;

    public function __sleep()
    {
        unset($this->PDO);
    }

    public function __wakeup()
    {
        self::__construct($this->sql);
    }

    /**
    * Constructor for DatixDBQuery class.
    *
    * Sets up global PDO connection object if it doesn't already exist and assigns SQL string to member variable.
    */
    function __construct($sql = '')
    {
        global $PDO, $Database, $ServerName, $UserName, $Password, $WindowsAuthentication;

        if (empty($PDO))
        {
            $version = explode('.', phpversion());
            if ($version[1] >= 3 || $WindowsAuthentication || $MicrosoftDrivers)
            {
                $PDO = new PDO('sqlsrv:Database='.$Database.';Server='.$ServerName, $UserName, $Password);
                $PDO->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
            }
            else
            {
                $PDO = new PDO('mssql:dbname='.$Database.';host='.$ServerName, $UserName, $Password);
            }

            // ensure SQL SERVER text fields aren't truncated to 4096 bytes
            $PDO->query('SET TEXTSIZE 2147483647');
        }

        $this->PDO = $PDO;
        $this->sql = $sql;
        $this->writeErrorsToFile = true;
    }

    /**
    * @desc Wrapper for PDO::prepare. Takes the sql statement stores in the $sql variable and stores the
    * resulting PDOStatement object in the variable $PDOStatement
    */
    public function prepare()
    {
        $this->PDOStatement = $this->PDO->prepare($this->sql);
    }


    /**
     * @desc allows you bind a single params to the PDO query
     * useful for when executing stored proceedures, also provides custom conversion from MSSQL types to PDO
     */
    public function bind($parameter, &$variable, $data_type = null, $length = null)
    {
    	$this->PDOStatement->bindParam($parameter, $variable, $data_type, $length);
    }

    /**
     * @desc execute a stored proceedure to retrieve results values
     * need to be consumed by iteration on the new sqlsrv driver.
     * Data bindings must be by using $this->bind and cannot be passed as options
     * @return boolean true if stored proceedure executed successfully
     */
	public function executeStoredProcedure()
	{
		$res = $this->PDOStatement->execute();

		// You MUST consume to receive the result set using the sqlsrv driver (stupid huh!)
		if ($res)
		{
			$rows = array();
	   		do
	   		{
	   			$rows[] = $this->PDOStatement->fetchAll(PDO::FETCH_COLUMN);
			} while ($this->PDOStatement->nextRowset());
			return $rows;
		}

		return $res;
	}


    /**
    * @desc Wrapper for PDOStatement:execute - includes normal datix error-catching code
    *
    * @param array $options replacements for keys within the sql statment provided as kay-value pairs.
    *
    * @return bool Result of the sql query - true if successful, false if an error occured.
    */
    public function execute($options = array(), $suppress_errors = false, $suppress_logging = false)
    {
        global $dtxdebug, $query_counter, $logLevel;

        $this->setRawQuery($options);

        if($logLevel == 100 || $logLevel == 200)
        {
            $logger = \src\framework\registry\Registry::getInstance()->getLogger();
            $logger->logInfoCategory('Query: ' . $this->PDOStatement->queryString . "\n\rParameters: " . var_export($options, true), \src\logger\DatixLogger::QUERYSTRINGS);
        }

        if(bYN(GetParm('LOG_QUERIES', 'N')) && !$suppress_logging) //used for client-site debugging.
        {
            foreach($options as $key => $val)
            {
                $paramstring .= $key.':'.$val.' | ';
            }
            LogQuery($this->PDOStatement->queryString, $paramstring);
        }

        $MaxRetries = GetParm('INSERT_RETRY', '100');
        $Retry = 0;
        $result = false;
        $start = microtime(true);

        while (!$result && $Retry <= $MaxRetries)   //Work around if deadlock occurs. System will re-try a re-execute SQL statements.
        {
            if (!$result = $this->PDOStatement->execute($options))
            {
                if (!$suppress_errors && $Retry  >= $MaxRetries) //Query has failed and we've run out of retries
                {
                    //debugbreak();
                    $this->handleSqlError($options);
                }
            }
            $Retry++;
        }
        
        $end = microtime(true) - $start;
        
        if ($end > 5)
        {
            $logger = \src\framework\registry\Registry::getInstance()->getLogger();
            $logger->logInfoCategory('Long-running query ('.$end.' secs): '.$this->PDOStatement->queryString."\n\rParameters: ".var_export($options, true), \src\logger\DatixLogger::LONGQUERIES);
        }

        if ($dtxdebug)
        {
            $query_counter++;
        }

        return $result;
    }

    /**
    * @desc Shortcut when no further action is needed between preparing and executing - this function
    * performs both functions, taking the same parameter and returning the same result as execute().
    *
    * @param array $options replacements for keys within the sql statment provided as kay-value pairs.
    *
    * @return bool Result of the sql query - true if successful, false if an error occured.
    */
    public function prepareAndExecute(array $options = array(), $suppress_logging = false)
    {
        $this->prepare();
        return $this->execute($options, false, $suppress_logging);
    }

    /**
    * @desc Gets the next result from the most recently executed query.
    *
    * @param $fetchsStyle variable describing the structure in which the results should be provided. - documentation here: http://www.php.net/manual/en/pdostatement.fetch.php
    *
    * @return array An array of the values selected from the database. Array format determined by $fetchStyle parameter.
    */
    public function fetch($fetchStyle = PDO::FETCH_ASSOC)
    // $result_type:
    // MSSQL_ASSOC - stores the data in the associative indices of the result array
    // MSSQL_NUM - stores the data in the numeric indices of the result array
    // MSSQL_BOTH - stores the data both in the numeric and the associative indices of the result array
    {
        return $this->PDOStatement->fetch($fetchStyle);
    }

    /**
    * @desc Gets all results from the most recently executed query.
    *
    * @param $fetchsStyle variable describing the structure in which the results should be provided. - documentation here: http://www.php.net/manual/en/pdostatement.fetch.php
    *
    * @return array An array of the values selected from the database. Array format determined by $fetchStyle parameter.
    */
    public function fetchAll($fetchStyle = PDO::FETCH_ASSOC)
    // $result_type:
    // MSSQL_ASSOC - stores the data in the associative indices of the result array
    // MSSQL_NUM - stores the data in the numeric indices of the result array
    // MSSQL_BOTH - stores the data both in the numeric and the associative indices of the result array
    {
        return $this->PDOStatement->fetchAll($fetchStyle);
    }

    /**
    * Gets the number of rows affected by the last SQL statment.
    *
    * @return int Number of rows affected.
    */
    public function getRowsAffected()
    {
        return $this->PDOStatement->rowCount();
    }

    /**
    * Inserts a record into an identity table.
    *
    * @param  array  $options  Replacements for keys within the sql statment provided as kay-value pairs.
    *
    * @return string $id       The ID of the inserted record.
    */
    public function insert(array $options = array())
    {
        global $dbtype;

        switch ($dbtype)
        {
            case "mssql":
                $this->sql .= ';SELECT SCOPE_IDENTITY() AS \'identity\'';
                break;

            default:
                $this->sql .= ';SELECT SCOPE_IDENTITY() AS \'identity\'';
                break;
        }

        $this->prepareAndExecute($options);
        $this->PDOStatement->nextRowset();
        $id = $this->fetch(PDO::FETCH_COLUMN);

        // remove unwanted characters from returned integer (",00", ".00" etc)
        $id = preg_replace('/[,.]0+$/u', '', $id);
        $id = preg_replace('/[,.]/u', '', $id);

        return Sanitize::SanitizeInt($id);
    }

    /**
    * Begins a transaction.
    */
    public function beginTransaction()
    {
        if (!$this->PDO->inTransaction)
        {
            $this->PDO->inTransaction = $this->PDO->beginTransaction();
        }
    }

    /**
    * Rolls back a transaction.
    */
    public function rollBack()
    {
        if ($this->PDO->inTransaction)
        {
            $this->PDO->rollback();
            $this->PDO->inTransaction = false;
        }
    }

    /**
    * Commits a transaction.
    */
    public function commit()
    {
        if ($this->PDO->inTransaction)
        {
            $this->PDO->commit();
            $this->PDO->inTransaction = false;
        }
    }

    /**
    * Setter for the SQL query.
    */
    public function setSQL($sql)
    {
        $this->sql = $sql;
    }

    /**
    * Handles SQL errors by logging them and printing an appropriate message to the screen.
    */
    private function handleSqlError($options)
    {
        global $dtxdebug;

        $errorInfo = $this->PDOStatement->errorInfo();
        if($errorInfo[0] == 'HY093')
        {
            $errorInfo[2] = 'Incorrect number of bound parameters.  SQL: '.$this->sql.'; Parameters: '.json_encode($options);
        }
        
        if ($this->writeErrorsToFile)
        {
            $this->WriteToErrorLog($errorInfo[2], '');
        }

        if ($dtxdebug)
        {
            $message = "Error: " . $errorInfo[2] . "  <br><br>SQL statement: " . $this->rawQuery . "\n";
        }
        else
        {
            $message = _tk('sql_error');
        }

        throw new DatixDBQueryException($message);
    }

    /**
    * @desc saves an xml error report to SQLErrors.xml containing all information passed in the parameters.
    * Identical to the subs.php function {@link logErrorReport()}
    *
    * @param array $aParameters Array of parameters
    * @param string $aParameters["message"] The message returned by SQL Server along with the error code.
    * @param string $aParameters["details"] Any error details added by datix.
    * @param string $aParameters["sql"] The SQL that was run.
    */
    private function WriteToErrorLog($message, $details)
    {
        global $ClientFolder;

        // check for SYSTEM_ERROR_LOGGING is set to yes
        $systemErrorLogging = getParm('SYSTEM_ERROR_LOGGING', 'Y');
        if ($systemErrorLogging != 'Y')
        {
        	return;
        }

        if(file_exists($ClientFolder."/SQLErrors.xml"))
        {
            $XMLDoc = new DOMDocument();
            $XMLDoc->load($ClientFolder."/SQLErrors.xml");
        }
        else //we need to create the header rows for a new error file
        {
            $BasicXML = getErrorXMLHeaders();

            $BasicXML .= '<errors></errors>';

            $XMLDoc = new DOMDocument();
            $XMLDoc->loadXML($BasicXML);
        }

        $NewErrorXML = $XMLDoc->createElement('error');
        $NewErrorXML->appendChild($XMLDoc->createElement('date', FormatDateVal(date("Y-m-d H:i:s.000"))));
        $NewErrorXML->appendChild($XMLDoc->createElement('time', date("H:i:s")));
        $NewErrorXML->appendChild($XMLDoc->createElement('message', htmlfriendly($message)));
        $NewErrorXML->appendChild($XMLDoc->createElement('details', htmlfriendly($details)));
        $NewErrorXML->appendChild($XMLDoc->createElement('sql', htmlfriendly($this->sql)));
        $NewErrorXML->appendChild($XMLDoc->createElement('contact_id', $_SESSION['contact_login_id']));
        $NewErrorXML->appendChild($XMLDoc->createElement('contact', htmlfriendly($_SESSION["fullname"])));
        $NewErrorXML->appendChild($XMLDoc->createElement('contact_initials', htmlfriendly($_SESSION["initials"])));
        $NewErrorXML->appendChild($XMLDoc->createElement('page', htmlfriendly($_SERVER['PHP_SELF'] . "?" . getGetString())));

        $XMLDoc->getElementsByTagName('errors')->item(0)->appendChild($NewErrorXML); //add child to "errors" tag.

        $XMLDoc->save($ClientFolder."/SQLErrors.xml");

        //notifies administrators that an error has been logged.
        SetGlobal('UNREAD_ERRORS', '1');
    }

    /**
    * Creates a DatixDBQuery object and prepares and executes SQL passed to it. Replacement for db_query().
    *
    * @param  string $sql  SQL statement.
    * @param  array  $PDOParamsArray   Array containing key/values for SQL variables.
    *
    * @return bool Successful execution
    */
    static function PDO_query($sql, $PDOParamsArray = array(), $query = null)
    {
        if(!$query)
        {
            $query = new self('');
        }

        $query->setSQL($sql);
        $result = $query->prepareAndExecute($PDOParamsArray);
        unset($query);
        return $result;
    }

    /**
    * Fetches complete result set for SQL SELECT with given variables.
    *
    * @param  string $sql  SQL statement.
    * @param  array  $PDOParamsArray   Array containing key/values for SQL variables.
    * @param string $fetchStyle Controls the contents of the returned array as documented in PDOStatement::fetch().
    *
    * @return array Complete result set for SQL SELECT with given variables.
    */
    static function PDO_fetch_all($sql, $PDOParamsArray = array(), $fetchStyle = PDO::FETCH_ASSOC, $query = null)
    {
        if(!$query)
        {
            $query = new self('');
        }

        $query->setSQL($sql);
        $query->prepareAndExecute($PDOParamsArray);
        return $query->fetchAll($fetchStyle);
    }

    /**
    * Fetches a single row from a result set for SQL SELECT with given variables.
    *
    * @param  string  $sql             SQL statement.
    * @param  array   $PDOParamsArray  Array containing key/values for SQL variables.
    * @param  string  $fetchStyle      Controls the contents of the returned array as documented in PDOStatement::fetch().
    *
    * @return array                    Complete result set for SQL SELECT with given variables.
    */
    static function PDO_fetch($sql, $PDOParamsArray = array(), $fetchStyle = PDO::FETCH_ASSOC, $query = null)
    {
        if(!$query)
        {
            $query = new self('');
        }

        $query->setSQL($sql);
        $query->prepareAndExecute($PDOParamsArray);
        $row = $query->fetch($fetchStyle);
        unset($query);

        return $row;
    }

    /**
    * Instantiates a DatixDBQuery object and uses it to insert a record.
    *
    * @param  string  $sql             SQL statement.
    * @param  array   $PDOParamsArray  Array containing key/values for SQL variables.
    *
    * @return string                   The insert ID.
    */
    static function PDO_insert($sql, $PDOParamsArray = array())
    {
        $query = new self($sql);
        $id = $query->insert($PDOParamsArray);
        unset($query);
        return $id;
    }

    /**
    * Instantiates a DatixDBQuery object and uses it to insert a record.
    *
    * @param  string  $table           The table we're inserting into.
    * @param  array   $PDOParamsArray  Array containing key/values for SQL variables.
    *
    * @return string                   The insert ID.
    */
    static function PDO_build_and_insert($table, $PDOParamsArray = array())
    {
        $fields = array_keys($PDOParamsArray);
        $query = new self('INSERT INTO ' . $table . ' (' . implode(',', $fields) . ') VALUES (' . implode(',', array_map('addColon', $fields)) . ')');
        $id = $query->insert($PDOParamsArray);
        unset($query);
        return $id;
    }

    /**
    * Begins a transaction.
    */
    public static function static_beginTransaction()
    {
        $query = new self('');
        $query->beginTransaction();
    }

    /**
    * Rolls back a transaction.
    */
    public static function static_rollBack()
    {
        $query = new self('');
        $query->rollback();
    }

    /**
    * Commits a transaction.
    */
    public static function static_commit()
    {
        $query = new self('');
        $query->commit();
    }

    /**
    * @desc Generates the sql and parameter array for an "IN" statement, based on an array
    */
    public static function getINforPDO(array $Values)
    {
        $ParameterName = 'param';
        $ParameterCount = 0;

        $Parameters = array();

        foreach($Values as $Value)
        {
            $SQL[] = ':'.$ParameterName.$ParameterCount;
            $Parameters[$ParameterName.$ParameterCount] = $Value;

            $ParameterCount++;
        }

        $returnArray['SQL'] = 'IN ('.implode(', ', $SQL).')';
        $returnArray['Parameters'] = $Parameters;

        return $returnArray;
    }


	public static function CallStoredProcedure($aParams)
	{
		$queryParams = array();
		for ($x = 0; $x < sizeof($aParams["parameters"]); $x++)
		{
			$queryParams[] = str_replace('@', ':', $aParams["parameters"][$x][0]);
		}
		$query = 'EXECUTE ' . $aParams["procedure"] . ' ' . implode(', ', $queryParams);
		$dbQuery = new self($query);
		$dbQuery->prepare();

		// Bind all available parameters
		for ($x = 0; $x < sizeof($aParams["parameters"]); $x++)
		{
	    	// Ensure all @'s are replaced with :
	    	$parameter = $aParams["parameters"][$x][0];
	    	if (\UnicodeString::strstr($parameter, '@') !== false)
	    	{
	    		$parameter = str_replace('@', ':', $parameter);
	    	}
	    	if (\UnicodeString::substr($parameter, 0, 1) != ':')
	    	{
	    		$parameter = ':' . $parameter;
	    	}

			//Convert old mssql data types over to PDO PARAM TYPES
			switch ($aParams["parameters"][$x][2])
			{
				// SQLTEXT, SQLVARCHAR, SQLCHAR, SQLINT1, SQLINT2, SQLINT4, SQLBIT, SQLFLT4, SQLFLT8, SQLFLTN
				case 'SQLTEXT':
				case 'SQLVARCHAR':
				case 'SQLCHAR':
					$data_type = PDO::PARAM_STR;
					break;
				case 'SQLINT1':
				case 'SQLINT2':
				case 'SQLINT4':
				case 'SQLBIT':
				case 'SQLFLT4':
				case 'SQLFLT8':
				case 'SQLFLTN':
					$data_type = PDO::PARAM_INT;
					break;
				default:
					$data_type = PDO::PARAM_STR;
					break;
	    	}

	    	// If is_output pass a parameter length to PDO:bind();
	    	$length = ($aParams["parameters"][$x][3]) ? 999 : null;

	    	// Must pass the variable that came it, they're passed by reference and populated by PDO
	    	$dbQuery->bind($parameter, $aParams["parameters"][$x][1], $data_type, $length);
	    }

	    $res = $dbQuery->executeStoredProcedure();
	    return $res;
	}

    /**
    * Builds the actual query as it would be run on the DB.  Useful for debugging.
    *
    * @param array $options Parameters to bind to the query
    */
    protected function setRawQuery(array $options)
    {
        $this->rawQuery = $this->sql;
        foreach ($options as $param => $value)
        {
            $value = EscapeQuotes($value);
            $this->rawQuery = preg_replace('/'.($param{0} == ':' ? $param : ":".$param).'(?![_a-z0-9])/', (is_null($options[$param]) ? 'NULL' : "'".$value."'"), $this->rawQuery);
        }
    }

    /**
     * Places quotes around the input string and escapes special characters.
     *
     * @param string $var The string to be quoted.
     *
     * @return string
     */
    public static function quote($var)
    {
        $query = new self();
        return $query->PDO->quote($var);
    }
}
