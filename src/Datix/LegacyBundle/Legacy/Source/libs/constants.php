<?php

// Module constants
DEFINE("MOD_COUNT",40); // Number of modules
DEFINE("MOD_CLAIMS",1); // Claims module
DEFINE("MOD_COMPLAINTS",2); // Complaints module
DEFINE("MOD_INCIDENTS",3); // Incidents module
DEFINE("MOD_CONTACTS",4); // Contacts module
DEFINE("MOD_RISKREGISTER",5); // Risk register module
DEFINE("MOD_ASSESS",6); // Controls assurance module
DEFINE("MOD_ACTIONS",7); // Actions module
DEFINE("MOD_PALS",8); // PALS module
DEFINE("MOD_TRAINING",9); // Training module
DEFINE("MOD_ADMIN",10); // Admin (dummy) module
DEFINE("MOD_RFI",11); // RFI module
DEFINE("MOD_TIME",12); // Time module
DEFINE("MOD_STANDARDS",16); // Standards module
DEFINE("MOD_LIBRARY",17); // Library module
DEFINE("MOD_ELEMENTS",18); // Elements module (Standards)
DEFINE("MOD_PROMPTS",19); // Prompts module (Standards)
DEFINE("MOD_INQUESTS",20); // Inquest module
DEFINE("MOD_DIARY",21); // Diary module
DEFINE("MOD_SABS",22); // SABS module
DEFINE("MOD_COSTS",23); // Assets (Equipment) module
DEFINE("MOD_ASSETS",24); // Assets (Equipment) module
DEFINE("MOD_DISTRIBUTION",25); // Distribution lists
DEFINE("MOD_MESSAGES",26); // Secure messages
DEFINE("MOD_CIV",27); // Clinical outcomes
DEFINE("MOD_PERFORMANCE",28); // Performance module
DEFINE("MOD_INDICATORS",29); // Performance indicators module
DEFINE("MOD_DASHBOARD",30); // Dashboard
DEFINE("MOD_MEDICATIONS",31); // Medications module
DEFINE("MOD_HOTSPOTAGENTS",32); // Hotspot agents module
DEFINE("MOD_HOTSPOTS",33); // Hotspot agents module
DEFINE("MOD_CQC_OUTCOMES",34); // Medications module
DEFINE("MOD_CQC_PROMPTS",35); // Hotspot agents module
DEFINE("MOD_CQC_SUBPROMPTS",36); // Hotspot agents module
DEFINE("MOD_PAYMENTS",37); // Payments
DEFINE("MOD_TODO",38); // To Do List
DEFINE("MOD_LOCATIONS",39); // Locations
DEFINE("MOD_ASSESSMENT_MODULE_TEMPLATES",40); // Assessment Module templates module
DEFINE("MOD_ASSESSMENT_QUESTION_TEMPLATES",41); // Assessment Question Templates module
DEFINE("MOD_ASSESSMENT_MODULE_TEMPLATE_INSTANCES",42); // Assessment Module Template Instances module
DEFINE("MOD_ASSESSMENT_MODULES",43); // Assessment Modules module
DEFINE("MOD_ASSESSMENT_QUESTIONS",44); // Assessment Questions module
DEFINE("MOD_POLICIES",45); //Policies module (claims)
DEFINE("MOD_ORGANISATIONS",46); //Organisations module

// DATIX Globals Defaults
DEFINE("LOGIN_TRY",3);
DEFINE("PWD_EXPIRY_DAYS",90);
DEFINE("PWD_MIN_LENGTH",6);
DEFINE("PWD_UNIQUE","Y");
DEFINE("PWD_LOWERCASE",0);
DEFINE("PWD_UPPERCASE",0);
DEFINE("PWD_NUMBERS",0);
DEFINE("PWD_SPECIAL",0);
DEFINE("VAT_RATE", 17.5);

// Licence validation error messages
DEFINE("LIC_ERR_INVALID","Invalid DatixWeb licence key.");
DEFINE("LIC_ERR_EXPIRED","The DatixWeb software licence has expired.");
DEFINE("LIC_ERR_KEY_NOTSET","The DatixWeb licence key has not been set.");
DEFINE("LIC_ERR_CLIENT_NOTSET","The client name has not been set.");

// LDAP error messages
DEFINE("LDAP_ERR_CONNECT_FAILED","Could not connect to active directory server.");
DEFINE("LDAP_ERR_BIND_FAILED","Invalid user credentials to bind to active directory server.");
DEFINE("LDAP_ERR_INVALID_USER","Invalid user credentials.");
DEFINE("LDAP_ERR_NO_ACCESS","You do not have permission to access DatixWeb.");
DEFINE("LDAP_ERR_INVALID_MAPPING","Invalid mapping or no mapping between the active directory group(s) the user belongs to and any of the Datix groups.");

// LDAP Constants
DEFINE("LDAP_SUCCESS", 0x00);
DEFINE("LDAP_OPERATIONS_ERROR", 0x01);
DEFINE("LDAP_PROTOCOL_ERROR", 0x02);
DEFINE("LDAP_TIMELIMIT_EXCEEDED", 0x03);
DEFINE("LDAP_SIZELIMIT_EXCEEDED", 0x04);
DEFINE("LDAP_COMPARE_FALSE", 0x05);
DEFINE("LDAP_COMPARE_TRUE", 0x06);
DEFINE("LDAP_AUTH_METHOD_NOT_SUPPORTED", 0x07);
DEFINE("LDAP_STRONG_AUTH_REQUIRED", 0x08);
// Not used in LDAPv3
DEFINE("LDAP_PARTIAL_RESULTS", 0x09);
// Next 5 new in LDAPv3
DEFINE("LDAP_REFERRAL", 0x0a);
DEFINE("LDAP_ADMINLIMIT_EXCEEDED", 0x0b);
DEFINE("LDAP_UNAVAILABLE_CRITICAL_EXTENSION", 0x0c);
DEFINE("LDAP_CONFIDENTIALITY_REQUIRED", 0x0d);
DEFINE("LDAP_SASL_BIND_INPROGRESS", 0x0e);
DEFINE("LDAP_NO_SUCH_ATTRIBUTE", 0x10);
DEFINE("LDAP_UNDEFINED_TYPE", 0x11);
DEFINE("LDAP_INAPPROPRIATE_MATCHING", 0x12);
DEFINE("LDAP_CONSTRAINT_VIOLATION", 0x13);
DEFINE("LDAP_TYPE_OR_VALUE_EXISTS", 0x14);
DEFINE("LDAP_INVALID_SYNTAX", 0x15);
DEFINE("LDAP_NO_SUCH_OBJECT", 0x20);
DEFINE("LDAP_ALIAS_PROBLEM", 0x21);
DEFINE("LDAP_INVALID_DN_SYNTAX", 0x22);
// Next two not used in LDAPv3
DEFINE("LDAP_IS_LEAF", 0x23);
DEFINE("LDAP_ALIAS_DEREF_PROBLEM", 0x24);
DEFINE("LDAP_INAPPROPRIATE_AUTH", 0x30);
DEFINE("LDAP_INVALID_CREDENTIALS", 0x31);
DEFINE("LDAP_INSUFFICIENT_ACCESS", 0x32);
DEFINE("LDAP_BUSY", 0x33);
DEFINE("LDAP_UNAVAILABLE", 0x34);
DEFINE("LDAP_UNWILLING_TO_PERFORM", 0x35);
DEFINE("LDAP_LOOP_DETECT", 0x36);
DEFINE("LDAP_SORT_CONTROL_MISSING", 0x3C);
DEFINE("LDAP_INDEX_RANGE_ERROR", 0x3D);
DEFINE("LDAP_NAMING_VIOLATION", 0x40);
DEFINE("LDAP_OBJECT_CLASS_VIOLATION", 0x41);
DEFINE("LDAP_NOT_ALLOWED_ON_NONLEAF", 0x42);
DEFINE("LDAP_NOT_ALLOWED_ON_RDN", 0x43);
DEFINE("LDAP_ALREADY_EXISTS", 0x44);
DEFINE("LDAP_NO_OBJECT_CLASS_MODS", 0x45);
DEFINE("LDAP_RESULTS_TOO_LARGE", 0x46);
// Next two for LDAPv3
DEFINE("LDAP_AFFECTS_MULTIPLE_DSAS", 0x47);
DEFINE("LDAP_OTHER", 0x50);
// Used by some APIs
DEFINE("LDAP_SERVER_DOWN", 0x51);
DEFINE("LDAP_LOCAL_ERROR", 0x52);
DEFINE("LDAP_ENCODING_ERROR", 0x53);
DEFINE("LDAP_DECODING_ERROR", 0x54);
DEFINE("LDAP_TIMEOUT", 0x55);
DEFINE("LDAP_AUTH_UNKNOWN", 0x56);
DEFINE("LDAP_FILTER_ERROR", 0x57);
DEFINE("LDAP_USER_CANCELLED", 0x58);
DEFINE("LDAP_PARAM_ERROR", 0x59);
DEFINE("LDAP_NO_MEMORY", 0x5a);
// Preliminary LDAPv3 codes
DEFINE("LDAP_CONNECT_ERROR", 0x5b);
DEFINE("LDAP_NOT_SUPPORTED", 0x5c);
DEFINE("LDAP_CONTROL_NOT_FOUND", 0x5d);
DEFINE("LDAP_NO_RESULTS_RETURNED", 0x5e);
DEFINE("LDAP_MORE_RESULTS_TO_RETURN", 0x5f);
DEFINE("LDAP_CLIENT_LOOP", 0x60);
DEFINE("LDAP_REFERRAL_LIMIT_EXCEEDED", 0x61);
// Incidents permission levels
DEFINE("DIF1", 1);              //DIF1 input only
DEFINE("DIF2_READ_ONLY", 2);    //DIF2 read-only access
DEFINE("DIF2_NO_APPROVAL", 3);  //DIF2 access only - no review of DIF1 forms
DEFINE("DIF2", 4);              //DIF2 access and review of DIF1 forms
// Risks permission levels
DEFINE("RISK1", 1);              //RISK1 input only
DEFINE("RISK2_READ_ONLY", 2);    //RISK2 read-only access
DEFINE("RISK2_NO_APPROVAL", 3);  //RISK2 access only - no review of RISK2 forms
DEFINE("RISK2", 4);              //RISK2 access and review of RISK1 forms
// PALS permission levels
DEFINE("PAL1", 1);              //PAL1 input only
DEFINE("PAL2_READ_ONLY", 2);    //PAL2 read-only access
DEFINE("PAL2_NO_APPROVAL", 3);  //PAL2 access only - no review of PAL1 forms
DEFINE("PAL2", 4);              //PAL2 access and review of PAL1 forms
// PALS permission levels
DEFINE("CLA1", 1);              //CLA1 input only
DEFINE("CLA2_READ_ONLY", 2);    //CLA2 read-only access
DEFINE("CLA2_NO_APPROVAL", 3);  //CLA2 access only - no review of CLA1 forms
DEFINE("CLA2", 4);              //CLA2 access and review of CLA1 forms
// SABS permission levels
DEFINE("SAB1", 1);              //Response only
DEFINE("SAB2", 2);              //Full access

DEFINE("RM", 5);                //Final approval of DIF2, RISK2, PAL2, CLA1

// Constants definitions for form buttons
DEFINE("BTN_SUBMIT", 1);
DEFINE("BTN_PENDING", 2);
DEFINE("BTN_SAVE", 4);
DEFINE("BTN_APPROVE", 8);
DEFINE("BTN_CANCEL", 16);
DEFINE("BTN_SEARCH", 32);
DEFINE("BTN_RESTORE", 64);
DEFINE("BTN_FINISH", 128);
DEFINE("BTN_REJECT", 256);
DEFINE("BTN_SUBMIT_APPROVE", 512); // Used when RISK2 user submits a new risk for final approval.
DEFINE("BTN_SAVE_APPROVE", 1024); // Used when RISK2 open a finally approved risk and saves it.
DEFINE("BTN_CONTINUE", 2048);

// Security group types - Those are bitwise values!!
DEFINE("GRP_TYPE_ACCESS", 1);
DEFINE("GRP_TYPE_EMAIL", 2);
DEFINE("GRP_TYPE_DENY_ACCESS", 4);

//Other
DEFINE("DROPDOWN_WIDTH_DEFAULT", 300); //default dropdown width

// fix for ionCube PHP 5.3 encoder
if (!defined(MSSQL_BOTH))
{
    DEFINE("MSSQL_BOTH", 3);
}
?>
