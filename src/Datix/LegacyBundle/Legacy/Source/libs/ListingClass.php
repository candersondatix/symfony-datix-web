<?php

class Listing
{
    var $Module; //module the listing belongs to
    var $LinkModule; //module the records in the listing are linked to (usually the module the listing appears in).
    var $ListingId; //id of the listing
    var $Columns; //array of column details
    var $Title; // the name of the form on the listings listing page
    var $Data; // the data to display in this listing;
    var $ReadOnly = false; // controls whether the design can be edited.
    var $Sortable = false; //controls whether the listing will allow for sorting by columns.
    var $EmptyMessage = 'No records to display.'; //message to appear if there are no items to be displayed in the listing.
    //var $ListingURLSuffix = ''; //additional string to add to the end of all linked urls displayed in this listing.
    var $Checkbox = false; // Flag to display checkboxes
    var $CheckboxType; // Additional type field to cater for more than one section using checkboxes e.g. different contact type sections
    var $CheckboxIDField = "recordid"; // Data field used to uniquly identify individual checkbox with checkbox_include_ prefix.

    var $OverrideLinkType; // Link type to use instead of the link type provided in the data. Needed because of the weird data structure in complaints.
    var $ExtraParameters; // Extra parameters to be stored against the listing
    var $WhereClause; //Where clause to use when retrieving data for the listing
    var $PDOParamArray = array(); //parameters to be inserted into the where clause (optional)
    var $OrderBy; //Orderby clause to use when retrieving data for the listing

    /**
    * @desc Constructor - sets a couple of basic variables.
    *
    * @param string $Module The module this listing represents
    * @param int $ListingId The recordid of the listing in the database
    */
    function __construct($Module, $ListingId = null)
    {
        $this->Module = Sanitize::getModule($Module);
        $this->ListingId = $ListingId;
    }

    /**
    * @desc Loads column definitions from the database into this object.
    *
    * @param int $ListingId the recordid of the listing in the database, in case it has not already been provided.
    */
    function LoadColumnsFromDB($ListingId = null)
    {
        if (isset($ListingId))
        {
            $this->ListingId = $ListingId;
        }
        if (!isset($this->ListingId))
        {
            $this->ListingId = Listing::GetDefaultListing($this->Module);
        }

        if ($this->ListingId != 0)
        {
            //this could almost certainly sit in memory, since it is going to change very infrequently
            if (!\src\framework\registry\Registry::getInstance()->getSessionCache()->exists('listing-design.modules-for-listings'))
            {
                //need to check that the listing given is for the appropriate module
                $ListingModules = DatixDBQuery::PDO_fetch_all('SELECT recordid, lst_module FROM WEB_LISTING_DESIGNS', array(), PDO::FETCH_KEY_PAIR);
                \src\framework\registry\Registry::getInstance()->getSessionCache()->set('listing-design.modules-for-listings', $ListingModules);
            }

            $ListingModule = \src\framework\registry\Registry::getInstance()->getSessionCache()->get('listing-design.modules-for-listings')[$this->ListingId];
        }

        if ($this->ListingId == 0 || $ListingModule != $this->Module) //use the default design - not stored in the database.
        {
            $this->LoadFromDefaultFile();
        }
        else
        {
            $sql = 'SELECT lcl_field, lcl_width FROM WEB_LISTING_COLUMNS WHERE listing_id = :listing_id ORDER BY LCL_order';

            $Columns = DatixDBQuery::PDO_fetch_all($sql, array('listing_id' => $this->ListingId));

            foreach ($Columns as $ColumnDetails)
            {
                $this->Columns[$ColumnDetails['lcl_field']] = array('width' => $ColumnDetails['lcl_width']);
            }
        }
    }

    /**
    * @desc Loads column definitions directly from an array
    *
    * @param array $Columns Array of columns.
    */
    function LoadColumnsFromArray($Columns)
    {
        $this->Columns = $Columns;
    }

    /**
    * @desc Determines whether the listing represented by this object is the listing to be used by default in the system.
    *
    * @return bool true if this listing is the default, false otherwise.
    */
    function isDefault()
    {
        return (System_Globals::GetCurrentValue_GlobalOnly($this->Module.'_LISTING_ID', 0) == $this->ListingId);
    }

    /**
    * @desc Loads columns from the default listing file - if this listing is then displayed in the listing designer,
    * it will be read-only, since you can't change the datix default listing.
    */
    function LoadFromDefaultFile()
    {
        $ColSettingsFile = $this->GetDefaultFile();

        include $ColSettingsFile;

        $this->Columns = $list_columns_standard;
        //$this->ReadOnly = true;
    }

    function GetDefaultFile()
    {
        global $ModuleDefs;

        if ($ModuleDefs[$this->Module]['GENERIC'])
        {
            if (file_exists('Source/generic_modules/'.$ModuleDefs[$this->Module]['GENERIC_FOLDER'].'/StandardListSettings.php'))
            {
                $SettingsFilename = 'Source/generic_modules/'.$ModuleDefs[$this->Module]['GENERIC_FOLDER'].'/StandardListSettings.php';
            }
            else
            {
                $SettingsFilename = 'Source/generic/StandardListSettings.php';
            }
        }
        else
        {
            $SettingsFilename = "{$ModuleDefs[$this->Module][LIBPATH]}/Standard{$this->Module}ListSettings.php";
        }

        return $SettingsFilename;
    }

    /**
    * @desc Loads listing data into this object from an array.
    *
    * @param array $DataArray Array of data.
    */
    function LoadData($DataArray)
    {
        $this->Data = $DataArray;
    }

    /**
    * @desc Constructs a select statement and collects record data for this listing from the appropriate table.
    */
    function LoadDataFromDB()
    {
        global $ModuleDefs;
        $sql = 'SELECT recordid, '.implode(', ', array_keys($this->Columns)).' FROM '.$ModuleDefs[$this->Module]['TABLE'].($this->WhereClause ? ' WHERE '.$this->WhereClause : '').($this->OrderBy ? ' ORDER BY '.$this->OrderBy : '');

        $this->Data = DatixDBQuery::PDO_fetch_all($sql, $this->PDOParamArray);
    }

    /**
    * @desc Constructs the url to link the user to a particular record referenced in this listing.
    *
    * @param array $datarow Array of data from this record
    *
    * @return string The url of the record.
    */
    function getRecordUrl($datarow)
    {
        global $ModuleDefs, $scripturl;

        if ($this->LinkModule) //linking to a linked record.
        {
            if($this->Module == 'CON' && $this->LinkModule == 'SAB')
            {
                //exception because SABS contacts are non-standard.
                return $scripturl.'?action=sabslinkcontact&con_id='.$datarow['con_id'].'&sab_id='.$datarow['sab_id'].'&formtype=Edit'.($_REQUEST['fromsearch'] ? '&fromsearch=1' : '');
            }
            else
            {
                // return $scripturl.'?action='.$ModuleDefs[$this->Module]['ACTION'].'&module='.$this->Module.'&link_recordid='.$datarow['link_recordid'];
                return $scripturl.'?action='.$ModuleDefs[$this->Module]['LINK_ACTION'].'&module='.$this->LinkModule.'&link_recordid='.$datarow['link_recordid'].'&link_type='.($this->OverrideLinkType ? $this->OverrideLinkType : $datarow['link_type']).'&main_recordid=' . $datarow[$ModuleDefs[$this->LinkModule]['FK']].($_REQUEST['fromsearch'] ? '&fromsearch=1' : '');
            }
        }
        else
        {
            return $scripturl.'?action='.$ModuleDefs[$this->Module]['ACTION'].'&module='.$this->Module.'&'.FirstNonNull(array($ModuleDefs[$this->Module]['URL_RECORDID'], 'recordid')).'='.$datarow['recordid'].($_REQUEST['fromsearch'] ? '&fromsearch=1' : '');
        }
    }

    /**
    * @desc Appends additional (probably hard coded) columns to the end of the existing column list.
    */
    function AddAdditionalColumns($AdditionalColumns)
    {
        $this->Columns = SafelyMergeArrays(array($this->Columns, $AdditionalColumns));
    }

    /**
    * @desc Constructs HTML representation of the listing as a table. Currently using copy-and-pasted code from browselist,
    * this will expand and refine in time.
    *
    * @param string $formType The current form mode.
    *
    * @return string HTML representation of the listing.
    *
    * @codeCoverageIgnoreStart
    * No unit test, since it deals with constructing HTML (which will, in this case, evolve regularly in the future),
    * rather than providing any testable return value.
    */
    function GetListingHTML($formType = 'Edit')
    {
        global $FieldDefs, $FieldDefsExtra, $scripturl, $ModuleDefs;

        //Check if Read only override already set.
        if ($this->ReadOnly !== true)
        {
            $this->setFormAccess($formType);
        }

        if (count($this->Columns) == 0)
        {
            return '<div class="padded_div"><b>This listing has no columns. Please contact your Datix administrator</b></div>';
        }

        if (count($this->Data) == 0)
        {
            return '<div class="padded_div windowbg2"><b>'.$this->EmptyMessage.'</b></div>';
        }

        $HTML = '<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';

        $HTML .= '<tr name="element_section_row" id="element_section_row" class="tableHeader head2">'; //title row

        if ($this->Checkbox)
        {
             $HTML .= '<th class="windowbg" width="1%">
                <input type="checkbox" id = "check_all_checkbox" name="check_all_checkbox" onclick="ToggleCheckAll(\'checkbox_id_' . $this->CheckboxType . '\', this.checked)"/>
            </th>';
        }

        foreach ($this->Columns as $col_field => $col_info)
        {
            $HTML .= '<th class="windowbg"' . ($col_info['width'] ? 'width="' . $col_info['width'] . '%"' : '') . '>';

            if ($FieldDefs[$this->Module][$col_field]['Type'] != 'textarea' && $this->Sortable && !$col_info['custom'])
            {
                $HTML .= '<a href="Javascript:sortList(\'' . $col_field . '\');">';
            }
            if ($col_field == "mod_title")
            {
                $currentCols["mod_title"] = "Module";
            }
            elseif ($col_field == "recordid")
            {
                if ($col_info_extra['colrename'])
                    $currentCols[$col_field] = $col_info_extra['colrename'];
                else
                    $currentCols[$col_field] = $this->GetColumnLabel($col_field, $FieldDefsExtra[$this->Module][$col_field]["Title"]);
            }
            elseif($col_info['title'])
            {
                $currentCols[$col_field] = $col_info['title'];
            }
            else
            {
                $currentCols[$col_field] = $this->GetColumnLabel($col_field, $FieldDefsExtra[$this->Module][$col_field]["Title"]);
            }

            $HTML .= $currentCols[$col_field];

            if ($FieldDefs[$this->Module][$col_field]['Type'] != 'textarea' && $this->Sortable && !$col_info['custom'])
            {
                $HTML .= '</a>';
            }

            $HTML .= '</th>';
        }

        $HTML .= '</tr>';

        foreach ($this->Data as $row)
        {
            $record_url = $this->getRecordUrl($row);

            $HTML .= '<tr class="listing-row">';

            if ($this->Checkbox)
            {
                $HTML .= '<td class="windowbg2" width="1%">
                        <input type="checkbox" id="checkbox_id_' . $this->CheckboxType . '" class="list_checkbox_'.$this->CheckboxType.'" name="checkbox_include_' . Escape::EscapeEntities($row[$this->CheckboxIDField]) . '" onclick="" />
                        </td>';
            }

            foreach ($this->Columns as $col_field => $col_info )
            {
                if($col_info['type'])
                {
                    $FieldType = $col_info['type'];
                }
                else
                {
                    $FieldType = $FieldDefsExtra[$this->Module][$col_field]['Type'];
                }

                // special columns
                if ($col_field == 'ram_cur_level' || $col_field == 'ram_after_level' || $col_field == 'ram_level')
                {
                    if ($col_field == "ram_cur_level")
                    {
                        $col = $row["ram_cur_level"];
                    }
                    if ($col_field == "ram_after_level")
                    {
                        $col = $row["ram_after_level"];
                    }
                    if ($col_field == "ram_level")
                    {
                        $col = $row["ram_level"];
                    }

                    if (bYN(GetParm('RISK_MATRIX', 'N')))
                    {
                        $sql = "SELECT description, cod_web_colour FROM code_inc_grades WHERE code = '$col'";
                    }
                    else
                    {
                        $sql = "SELECT description, cod_web_colour FROM code_ra_levels WHERE code = '$col'";
                    }

                    $request2 = db_query($sql);
                    $level = db_fetch_array($request2);

                    if (!isset($level['description']))
                    {
                        $level['description'] = $col;
                    }

                    if ($level["cod_web_colour"])
                    {
                        $colour = $codeinfo["cod_web_colour"];
                    }

                    $HTML .= '<td align="left" '.(empty($colour) ? 'class="windowbg2">' : 'bgcolor="#'.$colour.'">').$level["description"].'</td>';
                }
                elseif ($col_field == 'inc_grade')
                {
                    // Get grade map information
                    $sql = "SELECT description, cod_web_colour FROM code_inc_grades WHERE code = '{$row['inc_grade']}'";

                    $result = db_query($sql);

                    if ($incgrade = db_fetch_array($result))
                    {
                        $HTML .= '<td align="left" ' . ($incgrade["cod_web_colour"] ? 'bgcolor="#' . $incgrade["cod_web_colour"] . '"' : "") . '>' . $incgrade["description"] . '</td>';
                    }
                    else
                    {
                        $HTML .= '<td align="left" class="rowfieldbg">' . $row['inc_grade'] . ' </td>';
                    }
                }
                // regular columns
                else
                {
                    if ($FieldType == 'ff_select' || $FieldType == 'multilistbox')
                    {
                        // Array for fields that doesn't fall under the following if statement rule when getting code info
                        $excludeFields = array(
                            'link_rsp_type'
                        );

                        if($this->Module == 'CON' && \UnicodeString::substr($col_field, 0, 5) == 'link_' && !in_array($col_field, $excludeFields))
                        {
                            //hack needed because we store link fields under 'INC' rather than 'CON'
                            if ($FieldType == 'ff_select')
                            {
                                $codeinfo = get_code_info('INC', $col_field, $row[$col_field]);
                            }
                            else if ($FieldType == 'multilistbox')
                            {
                                 $codeinfo['description'] = GetCodeDescriptions('INC', $col_field, $row[$col_field], '', ', ');
                            }
                        }
                        else
                        {
                            if ($FieldType == 'ff_select')
                            {
                                $codeinfo = get_code_info($this->Module, $col_field, $row[$col_field]);
                            }
                            else if ($FieldType == 'multilistbox')
                            {
                                 $codeinfo['description'] = GetCodeDescriptions($this->Module, $col_field, $row[$col_field], '', ', ');
                            }
                        }

                        $colour = '';

                        if ($codeinfo["cod_web_colour"])
                        {
                            $colour = $codeinfo["cod_web_colour"];
                        }

                        if ($colour)
                        {
                            $HTML .= '<td valign="left" style="background-color:#' . $colour. '"';
                        }
                        else
                        {
                            $HTML .= '<td class="windowbg2" valign="top"';
                        }
                    }
                    else
                    {
                        $HTML .= '<td class="windowbg2" valign="top"';
                    }

                    if ($col_info_extra['dataalign'])
                         $HTML .= ' align="' . $col_info_extra['dataalign'] . '"';

                    $HTML .= '>';

                    if ($BoldRowField && $row[$BoldRowField] == $BoldRowConditionValue)
                    {
                        $HTML .= '<b>';
                    }

                    if ($row[$col_field] && !$this->ReadOnly && !$col_info['custom'])
                    {
                        $HTML .= '<a href="javascript:if(CheckChange()){SendTo(\''.$record_url.'\');}" >';
                    }

                    if(!$col_info['custom'])
                    {
                        $row[$col_field] = htmlspecialchars($row[$col_field]);
                    }
                    if ($col_field == 'recordid')
                    {
                        if ($col_info_extra['prefix'])
                        {
                            $HTML .= $col_info_extra['prefix'];
                        }
                        $HTML .= $row[$col_field];
                    }
                    elseif ($col_field == "act_module")
                    {
                        $HTML .= $ModuleDefs[$row[$col_field]]["NAME"];
                    }
                    else if ($col_field == 'rep_approved')
                    {
                        $HTML .= FirstNonNull(array(_tk('approval_status_'.$this->Module.'_'.$row[$col_field]), $codeinfo['description']));
                    }
                    elseif ($FieldType == 'ff_select' || $FieldType == 'multilistbox')
                    {
                        //echo code_descr($this->Module, $col_field, $row[$col_field]);
                        $HTML .= $codeinfo['description'];
                    }
                    elseif ($FieldType == 'yesno')
                    {
                        $HTML .= code_descr($this->Module, $col_field, $row[$col_field]);
                    }
                    elseif ($FieldType == 'date')
                    {
                        $HTML .= FormatDateVal($row[$col_field]);
                    }
                    elseif ($FieldType == 'money')
                    {
                        $HTML .= FormatMoneyVal($row[$col_field]);
                    }
                    /*elseif ($FieldDefs[$this->Module][$col_field]['Type'] == 'textarea')
                    {
                        echo htmlspecialchars($row[$col_field]);
                    } */
                    elseif ($FieldType == 'time')
                    {
                        if (\UnicodeString::strlen($row[$col_field]) == 4)
                            $row[$col_field] = $row[$col_field]{0} . $row[$col_field]{1} . ":" . $row[$col_field]{2} . $row[$col_field]{3};

                        $HTML .= $row[$col_field];
                    }
                    else
                    {
                        $HTML .= $row[$col_field];
                    }

                    if ($row[$col_field] && !$this->ReadOnly && !$col_info['custom'])
                    {
                        $HTML .= '</a>';
                    }

                    if ($BoldRow)
                    {
                        $HTML .= '</b>';
                    }
                    $HTML .= '</td>';
                }
            }
            $HTML .= '</tr>';
        }

        $HTML .= '</table>';

        return $HTML;
    }

    /**
    * @desc Similar to the listing HTML generated when viewing a listing, this generates HTML for use in the listing designer,
    * representing a preview of the columns, with fields to set their widths.
    *
    * @return string HTML representing a preview of the listing.
    * No unit test, since it deals with constructing HTML rather than providing any testable return value.
    */
    function GetListingPreviewHTML()
    {
        global $FieldDefs, $FieldDefsExtra, $ModuleDefs;

        $HTML = '<table id="listing_preview_table" class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';

        $HTML .= '<tr name="element_section_row" id="element_section_row">'; //title row

        $customFields = getCustomFields($this->Module, true);

        if (is_array($this->Columns))
        {
            foreach ($this->Columns as $col_field => $col_info)
            {
                $HTML .= '<th id="col_heading_'.$col_field.'" class="windowbg"' . ($col_info['width'] ? ' width="' . $col_info['width'] . '%"' : '') . '>';

                if ($col_field == "mod_title")
                {
                    $currentCols["mod_title"] = "Module";
                }
                elseif ($col_field == "recordid")
                {
                    if ($col_info_extra['colrename'])
                    {
                        $currentCols[$col_field] = $col_info_extra['colrename'];
                    }
                    else
                    {
                        $currentCols[$col_field] = Labels_FieldLabel::GetFieldLabel($col_field,$FieldDefsExtra[$this->Module][$col_field]["Title"], $ModuleDefs[$this->Module]['VIEW'] ?: $ModuleDefs[$this->Module]['TABLE'], $this->Module).($FieldDefsExtra[$this->Module][$col_field]['SubmoduleSuffix'] ? ' '.$FieldDefsExtra[$this->Module][$col_field]['SubmoduleSuffix'] : '');
                    }
                }
                else if (isset($customFields[$col_field]))
                {
                    $currentCols[$col_field] = $customFields[$col_field];
                }
                else
                {
                    $currentCols[$col_field] = Labels_FieldLabel::GetFieldLabel($col_field,$FieldDefsExtra[$this->Module][$col_field]["Title"], $ModuleDefs[$this->Module]['VIEW'] ?: $ModuleDefs[$this->Module]['TABLE'], $this->Module).($FieldDefsExtra[$this->Module][$col_field]['SubmoduleSuffix'] ? ' '.$FieldDefsExtra[$this->Module][$col_field]['SubmoduleSuffix'] : '');
                }
                $HTML .= '<b>' . $currentCols[$col_field] . '</b>';

                $HTML .= '</th>';
            }

            $HTML .= '</tr>';
            $HTML .= '<tr>';

            foreach ($this->Columns as $col_field => $col_info)
            {
                //$HTML .= '<th class="windowbg"' . ($col_info['width'] ? 'width="' . $col_info['width'] . '%"' : '') . '>';

                if(floatval($col_info['width']) - intval($col_info['width']) == 0)
                {
                    $col_info['width'] = intval($col_info['width']);
                }

                if ($this->ReadOnly)
                {
                    $HTML .= '<td class="windowbg" style="text-align:center">'.$col_info['width'].'%</td>';
                }
                else
                {
                    $HTML .= '<td class="windowbg" style="text-align:center"><input id="col_width_'.$col_field.'" name="col_width_'.$col_field.'" type="string" size="2" maxlength="3" value="'.$col_info['width'].'" onchange="jQuery(\'#col_heading_'.$col_field.'\').width(this.value.toString()+\'%\')"></td>';
                }

                //$HTML .= '</th>';
            }
        }

        $HTML .= '</tr>';

        $HTML .= '</table>';

        return $HTML;

    }

    // @codeCoverageIgnoreEnd

    /**
    * @desc Deletes the current listing design from the database.
    */
    function DeleteFromDB()
    {
        DatixDBQuery::PDO_query('DELETE FROM WEB_LISTING_COLUMNS WHERE listing_id = :listing_id', array('listing_id' => $this->ListingId));
        DatixDBQuery::PDO_query('DELETE FROM WEB_LISTING_DESIGNS WHERE recordid = :listing_id', array('listing_id' => $this->ListingId));

        if ($this->isDefault()) //we need to reassign the default design to one that will still exist.
        {
            SetGlobal($this->Module.'_LISTING_ID', 0);
            $_SESSION["Globals"][$_GET['global']] = $_GET['value']; //make sure the change is picked up straight away.
        }
    }

    /**
    * @desc Saves the current listing design to the database.
    */
    function SaveToDB()
    {
        if (!$this->ListingId)
        {
            $this->ListingId = DatixDBQuery::PDO_build_and_insert('WEB_LISTING_DESIGNS',
                array('lst_title' => $this->Title, 'lst_module' => $this->Module, 'createddate' => GetTodaysDate(), 'createdby' => $_SESSION['initials']));
        }
        else
        {
            DatixDBQuery::PDO_query('UPDATE WEB_LISTING_DESIGNS SET updateddate = :updateddate, updatedby = :updatedby WHERE recordid = :recordid',
                array('recordid' => $this->ListingId, 'updateddate' => GetTodaysDate(), 'updatedby' => $_SESSION['initials']));
        }

        DatixDBQuery::PDO_query('DELETE FROM WEB_LISTING_COLUMNS WHERE listing_id = :listing_id', array('listing_id' => $this->ListingId));

        $InsertColumnQuery = new DatixDBQuery('INSERT INTO WEB_LISTING_COLUMNS (listing_id, LCL_field, LCL_width, LCL_order, updatedby, updateddate) VALUES (:listing_id, :field, :width, :order, :updatedby, :updateddate)');
        $InsertColumnQuery->prepare();

        $Order = 0;

        if (is_array($this->Columns))
        {
            foreach ($this->Columns as $ColField => $ColOptions)
            {
                if($ColOptions['width'] == '') //otherwise we get an error trying to put a blank string into an integer field.
                {
                    $ColOptions['width'] = 0.0;
                }

                $InsertColumnQuery->execute(array('listing_id' => $this->ListingId, 'field' => $ColField, 'width' => $ColOptions['width'], 'order' => $Order++, 'updatedby' => $_SESSION['initials'], 'updateddate' => date('Y-m-d H:i:s')));
            }
        }
    }

    /**
    * Set the listing's ReadOnly property based on the current permissions/form type.
    *
    * @global array $ModuleDefs
    *
    * @param string $formType The current form type.
    */
    protected function setFormAccess($formType)
    {
        global $ModuleDefs;

        if(GetParm($ModuleDefs[$this->Module]['PERM_GLOBAL']) == '' || $formType == 'Print' || $formType == 'ReadOnly')
        {
            $this->ReadOnly = true;
        }
    }

    /**
    * @desc Loads the default listing from the database and returns its columns as an array.
    *
    * @param string $module The module listing to return
    *
    * @return array An array of columns
    */
    static function GetListingColumns($module)
    {
        $Listing = new Listing($module, Listing::GetDefaultListing($module));
        $Listing->LoadColumnsFromDB();

        return $Listing->Columns;
    }

    /**
    * @desc Returns the default listing for a given module.
    *
    * @return int The id of the default listing for this module.
    */
    static function GetDefaultListing($module)
    {
        return System_Globals::GetCurrentValue($module.'_LISTING_ID', 0);
    }

    /**
    * @desc Returns the default listing for a given module, ignoring user and profile parameters.
    *
    * @return int The id of the default listing for this module.
    */
    static function GetGlobalDefaultListing($module)
    {
        return System_Globals::GetCurrentValue_GlobalOnly($module.'_LISTING_ID', 0);
    }
    
    /**
    * Retrieves the label for a listing column header.
    * 
    * @param string $FieldName    The name of the field (in the DB).
    * @param string $DefaultTitle The label to use if no other is found.
    * @param bool   $FullName     Whether or no to use the "long" name from field_formats/field_directory (as opposed to the short one in FieldDefs).
    * @param string $table        The table (DB) the field belongs to.
    * 
    * @return string
    */
    public function GetColumnLabel($FieldName, $DefaultTitle = "", $FullName = false, $table = '', $module='')
    {
        return Labels_FieldLabel::GetFieldLabel($FieldName, $DefaultTitle, $table, $module, $FullName);
    }
}

