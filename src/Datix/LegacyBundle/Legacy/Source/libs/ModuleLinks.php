<?php

use src\security\Escaper;

function LinkSection($rec, $FormType, $mod, $errSource="")
{
    global $scripturl, $ModuleDefs, $FieldDefs, $ListingDesigns;

    $openMod = $_SESSION["ModuleLinks"]["openMod"];
    if($_POST['openMod'])
    {
       $openMod = Sanitize::SanitizeString($_POST['openMod']);
       $_SESSION["ModuleLinks"]["openMod"] = Sanitize::SanitizeString($_POST['openMod']);
    }

    $link_id = $rec["recordid"];
    //$table = ($_GET['table']?$_GET['table']:'main');
    $table = 'main';

    $modArray = getModLinksArray($mod);

    $LinkFromField = $ModuleDefs[$mod]['FK'];
    $MainTable = $ModuleDefs[$mod]['TABLE'];

    $ReadOnlyMarker = ($FormType=="ReadOnly" || $FormType=="Print" || $FormType=="Locked" || $_GET["print"]);

    foreach($modArray as $Module => $ModuleName)
    {
        if (CanSeeModule($Module))
        {
            require_once 'Source/libs/ListingClass.php';
            $ListingArray[$Module] = new Listing($Module);
            $ListingArray[$Module]->LoadColumnsFromDB($ListingDesigns['linked_records'][$Module]);

            $ListingCols = array_keys($ListingArray[$Module]->Columns);

            foreach($ListingCols as $col_id => $ListingField)
            {
                if(preg_match('/tier_(\d)/u', $ListingField))
                {
                    //The below code is hard-coded and can't support tier_... fields yet.
                    unset($ListingCols[$col_id]);
                }

                if (!isset($FieldDefs[$Module][$ListingField]))
                {
                    unset($ListingCols[$col_id]);
                }
            }

            if(!in_array('recordid', $ListingCols))
            {
                $ListingCols[] = 'recordid';
            }

            //links are stored in a different table depending on whether they are within the same module or not.
            if($Module == $mod)
            {
                $recordSql = 'SELECT '.implode(', ', $ListingCols).', link_notes FROM '.($ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE']).', links
                WHERE ((LNK_ID1=:current_recordid AND LNK_MOD1=:current_module AND LNK_ID2=recordid AND LNK_MOD2=:target_module) OR (LNK_ID2=:current_recordid2 AND LNK_MOD2=:current_module2 AND LNK_ID1=recordid AND LNK_MOD1=:target_module2))';

                $securityWhere = MakeSecurityWhereClause('', $Module, $_SESSION['initials']);

                if ($securityWhere != '')
                {
                    $recordSql .= ' AND '.$securityWhere;
                }

                $recordSql .= 'ORDER BY '.($ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE']).'.recordid';

                $linkArray[$Module] = DatixDBQuery::PDO_fetch_all($recordSql, array('current_recordid' => $link_id, 'current_recordid2' => $link_id, 'current_module' => $mod, 'current_module2' => $mod, 'target_module' => $Module, 'target_module2' => $Module));
            }
            else
            {
                $recordSql = 'SELECT '.implode(', ', $ListingCols).', link_notes FROM '.($ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE']).', link_modules
                WHERE (recordid = '.$ModuleDefs[$Module]['FK'].' AND '.$ModuleDefs[$mod]['FK'].' = :current_recordid)';

                $securityWhere = MakeSecurityWhereClause('', $Module, $_SESSION['initials']);

                if ($securityWhere != '')
                {
                    $recordSql .= ' AND '.$securityWhere;
                }

                $recordSql .= 'ORDER BY '.($ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE']).'.recordid';

                $linkArray[$Module] = DatixDBQuery::PDO_fetch_all($recordSql, array('current_recordid' => $link_id));
            }
        }
    }

    // Risks can additionallu be linked through a seperate table.
    if($mod == "RAM")
    {
        $risklinkssql = "SELECT triggers_links.cas_id, modules.mod_module FROM triggers_links, triggers_main, modules WHERE triggers_main.TRG_LINK_CAS_ID='$link_id'
                            AND triggers_links.trigger_id = triggers_main.recordid AND triggers_links.mod_id=modules.mod_id";

        $request = db_query($risklinkssql);
        while ($trigger = db_fetch_array($request))
        {
            $linkArray_trigger[$trigger["mod_module"]][]=$trigger["cas_id"];
        }
    }
    else
    {
        $risklinkssql = "SELECT triggers_main.TRG_LINK_CAS_ID FROM triggers_links, triggers_main, modules WHERE triggers_links.cas_id='$link_id'
                            AND triggers_links.trigger_id = triggers_main.recordid AND triggers_links.mod_id=modules.mod_id AND modules.mod_module='$mod'";

        $request = db_query($risklinkssql);
        while ($trigger = db_fetch_array($request))
        {
            if($trigger["TRG_LINK_CAS_ID"] != '')
            {
                $linkArray_trigger["RAM"][]=$trigger["TRG_LINK_CAS_ID"];
            }
        }
    }

    foreach($modArray as $Module => $ModuleName)
    {
        if (CanSeeModule($Module))
        {
            require_once 'Source/libs/ListingClass.php';

            if(in_array($Module, array('CQO', 'CQP', 'CQS')))
            {
                require_once 'Source/classes/Listings/CQCListing.php';
                $ListingArray[$Module] = new CQCListing($Module);
            }
            else
            {
                $ListingArray[$Module] = new Listing($Module);
            }
            $ListingArray[$Module]->LoadColumnsFromDB($ListingDesigns['linked_records'][$Module]);

            if(in_array($Module, array('CQO', 'CQP', 'CQS'))) {
                foreach ($linkArray[$Module] as $id => $Record) {
                    foreach ($ListingArray[$Module]->Columns as $field => $fieldInfo) {
                        if (preg_match('/tier_(\d)/u', $field)) {
                            $linkArray[$Module][$id][$field] = array_shift($ListingArray[$Module]->getNodeAtTier($field, $Record['recordid']));
                        }
                    }
                }
            }

            $ListingCols = array_keys($ListingArray[$Module]->Columns);

            if(!in_array('recordid', $ListingCols))
            {
                $ListingCols[] = 'recordid';
            }

            if(is_array($linkArray_trigger[$Module]) && !empty($linkArray_trigger[$Module]))
            {
                $recordSql = 'SELECT ' . implode(', ', $ListingCols) . ' FROM '.$ModuleDefs[$Module]['TABLE'] . ' WHERE recordid in (';

                $recordSql .= implode(',',$linkArray_trigger[$Module]);

                $recordSql .= ') AND recordid!='.$link_id.' ORDER BY recordid';

                $request = db_query($recordSql);

                while ($recordtemp = db_fetch_array($request))
                {
                    $recordtemp["no_edit"]=1;
                    $linkArray[$Module][] = $recordtemp;
                }
            }

            // load UDF fields
            $udfFields = [];
            $recordIDs = [];
            foreach ($ListingCols as $name) {
                if (substr ($name, 0, 4) == 'UDF_') {
                    $parts = explode('_', $name);
                    $udfFields[] = $parts[1];
                }
            }
            foreach ($linkArray[$Module] as $link) {
                $recordIDs[] = $link['recordid'];
            }

            if (!empty($udfFields))
            {
                $udfValues = getUDFData ($Module,$recordIDs,$udfFields);
                foreach ($udfValues as $recordid => $values) {
                    foreach ($values as $field => $fieldValue) {
                        // look for the entry with the correct recordid
                        foreach ($linkArray[$Module] as $index => $row) {
                            if ($row['recordid'] == $recordid) {
                                $linkArray[$Module][$index]['UDF_'.$field] = $fieldValue;
                            }
                        }
                    }
                }
            }
        }

        // build html listing
        if(!empty($linkArray[$Module]))
        {
            $LinksExist = true;

            echo '
            <li class="new_titlebg" name="linked_records_row" id="linked_records_row">
                <a href="javascript:expandRow(\''.$Module.'row\',this)"><img src="Images/expand.gif" alt="+" border="0"/></a>
                <b>Linked '.$ModuleDefs[$Module]["REC_NAME_PLURAL"].' ('.count($linkArray[$Module]).')</b>
            </li>
            <li name="'.$Module.'row" id="'.$Module.'row" class="new_windowbg">';

            if (!$ReadOnlyMarker && GetParm($ModuleDefs[$Module]['PERM_GLOBAL']) != '')
            {
                foreach($linkArray[$Module] as $key => $data)
                {
                    if(!$linkArray[$Module][$key]['no_edit'])
                    {
                        $linkArray[$Module][$key]['edit'] = '<a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=editlinkedrecord&currentID='.$link_id.'&currentMod='.$mod.'&editID='.$linkArray[$Module][$key]['recordid'].'&editMod='.$Module.'\');}">[edit]</a>';
                    }
                }
            }

            $ListingArray[$Module]->LoadData($linkArray[$Module]);
            $ListingArray[$Module]->AddAdditionalColumns(array('link_notes' => array('width' => 10, 'title' => 'Link Notes'), 'edit' => array('width' => 5, 'custom' => true)));
            echo $ListingArray[$Module]->GetListingHTML($FormType);

            echo '</li>';
        }
    }

    if(!$LinksExist)
    {
            echo '
        <li name="linked_records_row" id="linked_records_row" class="section_link_row">
            <b>No Linked Records.</b>
        </li>
    ';
    }
    echo '
    <input type="hidden" name="edit" id="edit" value="">
    <script language="javascript">
   function expandRow(id, image)
   {
        var rows = document.getElementsByName(id);

        if(rows[0].style.display=="none")
        {
            image.src="Images/collapse.gif";
        }
        else
        {
            image.src="Images/expand.gif";
        }
        for(var i=0;i<rows.length;i++)
        {
            if(rows[i].style.display=="none")
            {
                rows[i].style.display = "";
            }
            else
            {
                rows[i].style.display = "none";
            }
        }

   }
    </script>
    ';

    $total = 0;
    foreach($modArray as $Module => $ModuleName)
    {
        $total += count($linkArray[$Module]);
    }

   if(!$ReadOnlyMarker)
   {
        echo '
            <li class="new_link_row">
                <a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=editlinkedrecord&currentID='.$link_id.'&currentMod='.$mod.'\');}"><b>Link a'. ($total == 0 ? '' : 'nother'). ' record.</b></a>
                <input type="hidden" id="currentMod" name="currentMod" value="'.Escape::EscapeEntities($mod).'">
                <input type="hidden" id="currentID" name="currentID" value="'.Sanitize::SanitizeInt($link_id).'">
                <input type="hidden" id="postedaction" name="postedaction" value="action='.Escape::EscapeEntities($ModuleDefs[$mod]["ACTION"]).'">
                <input type="hidden" name="editMod" id="editMod" value="">
                <input type="hidden" name="editID" id="editID" value="">
                <input type="hidden" name="openMod" id="openMod" value="'.Escaper::escapeForHTMLParameter($_POST['openMod']).'">
            </li>
        ';
   }
}

function SaveLink($current_id, $current_mod, $new_id, $new_mod, $linkNotes)
{
    global $yySetLocation, $scripturl, $ModuleDefs;

    if($current_mod == $new_mod)
    {
        $sql = "UPDATE links SET link_notes = :linkNotes WHERE (LNK_MOD1 = :current_mod AND LNK_MOD2 = :new_mod AND LNK_ID1 = :current_id AND LNK_ID2 = :new_id)
        OR (LNK_MOD2 = :current_mod2 AND LNK_MOD1 = :new_mod2 AND LNK_ID2 = :current_id2 AND LNK_ID1 = :new_id2)";

        $pdoParams = array(
            'linkNotes' => $linkNotes,
            'current_mod' => $current_mod,
            'current_mod2' => $current_mod,
            'new_mod' => $new_mod,
            'new_mod2' => $new_mod,
            'current_id' => $current_id,
            'current_id2' => $current_id,
            'new_id' => $new_id,
            'new_id2' => $new_id
        );
    }
    else
    {
        $sql = "UPDATE link_modules SET link_notes = :linkNotes WHERE ".$current_mod."_ID = :current_id AND ".$new_mod."_ID = :new_id";

        $pdoParams = array(
            'linkNotes' => $linkNotes,
            'current_id' => $current_id,
            'new_id' => $new_id
        );
    }

    DatixDBQuery::PDO_query($sql, $pdoParams);

    $yySetLocation = $scripturl . "?action=".$ModuleDefs[$current_mod]['ACTION']."&recordid=$current_id&panel=linked_records";
    redirectexit();

}

function RemoveLink($current_id,$current_mod, $linked_id,$linked_mod)
{
    global $yySetLocation, $scripturl, $ModuleDefs;

    if($current_mod == $linked_mod)
    {
        $pdoParams = array(
            'current_mod' => $current_mod,
            'linked_mod' => $linked_mod,
            'current_id' => $current_id,
            'linked_id' => $linked_id
        );

        $sql = "DELETE FROM links WHERE (LNK_MOD1 = :current_mod AND LNK_MOD2 = :linked_mod AND LNK_ID1 = :current_id AND LNK_ID2 = :linked_id)";
        DatixDBQuery::PDO_query($sql, $pdoParams);

        $sql = "DELETE FROM links WHERE (LNK_MOD2 = :current_mod AND LNK_MOD1 = :linked_mod AND LNK_ID2 = :current_id AND LNK_ID1 = :linked_id)";
        DatixDBQuery::PDO_query($sql, $pdoParams);
    }
    else
    {
        $pdoParams = array(
            'current_id' => $current_id,
            'linked_id' => $linked_id
        );

    	$sql = 'DELETE FROM link_modules WHERE '
		    . $ModuleDefs[$current_mod]['FK'] . " = :current_id AND ".$ModuleDefs[$linked_mod]['FK'] . " = :linked_id";
        DatixDBQuery::PDO_query($sql, $pdoParams);
    }

    $yySetLocation = $scripturl . "?action=".$ModuleDefs[$current_mod]['ACTION']."&recordid=$current_id&panel=linked_records";
    redirectexit();

}
function InsertLink($current_id,$current_mod, $linked_id,$linked_mod, $linkNotes)
{
    global $yySetLocation, $scripturl, $ModuleDefs;

    if($current_mod == $linked_mod && $current_id == $linked_id)
    {
        AddSessionMessage('ERROR', 'You cannot link a record to itself');

        $yySetLocation = $scripturl . "?action=editlinkedrecord&currentID=$current_id&currentMod=$current_mod";
        redirectexit();
    }

    if($current_mod == $linked_mod)
    {
        //check link does not already exist.
        $sqlcheck = "SELECT count(*) as num FROM links WHERE (LNK_MOD1 = :current_mod AND LNK_MOD2 = :linked_mod AND LNK_ID1 = :current_id AND LNK_ID2 = :linked_id)
        OR (LNK_MOD2 = :current_mod2 AND LNK_MOD1 = :linked_mod2 AND LNK_ID2 = :current_id2 AND LNK_ID1 = :linked_id2)";

        $pdoParams = array(
            'current_mod' => $current_mod,
            'current_mod2' => $current_mod,
            'linked_mod' => $linked_mod,
            'linked_mod2' => $linked_mod,
            'current_id' => $current_id,
            'current_id2' => $current_id,
            'linked_id' => $linked_id,
            'linked_id2' => $linked_id
        );
    }
    else
    {
        //check link does not already exist.
        $sqlcheck = "SELECT count(*) as num FROM link_modules WHERE ".$ModuleDefs[$current_mod]['FK'] . " = :current_id AND " . $ModuleDefs[$linked_mod]['FK'] . " = :linked_id";

        $pdoParams = array(
            'current_id' => $current_id,
            'linked_id' => $linked_id
        );
    }

     if (DatixDBQuery::PDO_fetch($sqlcheck, $pdoParams, PDO::FETCH_COLUMN)) //already exists
     {
         AddSessionMessage('ERROR', 'A link to that record already exists.');
         $yySetLocation = $scripturl . "?action=editlinkedrecord&currentID=$current_id&currentMod=$current_mod";
         redirectexit();
     }

    //check record to link to exists.
    $sqlcheck = "SELECT count(*) as num FROM ".$ModuleDefs[$linked_mod]["TABLE"]." WHERE recordid = :linked_id";
    $pdoParams = array('linked_id' => $linked_id);

    if(!DatixDBQuery::PDO_fetch($sqlcheck, $pdoParams, PDO::FETCH_COLUMN)) //doesn't exist
    {
        AddSessionMessage('ERROR', 'Target record does not exist.');
        $yySetLocation = $scripturl . "?action=editlinkedrecord&currentID=$current_id&currentMod=$current_mod";
        redirectexit();
    }

     //insert link
    if($current_mod == $linked_mod)
    {
        $sql = "INSERT INTO links (LNK_MOD1, LNK_ID1, LNK_MOD2, LNK_ID2, LINK_NOTES)
        VALUES (:current_mod, :current_id, :linked_mod, :linked_id, :linkNotes)";

        $pdoParams = array(
            'current_mod' => $current_mod,
            'current_id' => $current_id,
            'linked_mod' => $linked_mod,
            'linked_id' => $linked_id,
            'linkNotes' => $linkNotes,
        );
    }
    else
    {
        $sql = "INSERT INTO link_modules ( ".$ModuleDefs[$current_mod]['FK'] . ', ' . $ModuleDefs[$linked_mod]['FK'].", LINK_NOTES)
        VALUES (:current_id, :linked_id, :linkNotes)";

        $pdoParams = array(
            'current_id' => $current_id,
            'linked_id' => $linked_id,
            'linkNotes' => $linkNotes,
        );
    }

    DatixDBQuery::PDO_query($sql, $pdoParams);

    $yySetLocation = $scripturl . "?action=".$ModuleDefs[$current_mod]['ACTION']."&recordid=$current_id&panel=linked_records";
    redirectexit();
}

function getModLinksArray($CurrentModule = '')
{
    global $ModuleDefs;

    $LinksArray = array('INC', 'RAM', 'CLA', 'SAB', 'PAL', 'COM', 'HOT', 'STN', 'CQO', 'CQP', 'CQS'); //list of modules for which links can be made.

    $ModArray = getModArray();

    foreach($ModArray as $code => $ModuleName)
    {
        if(!in_array($code, $LinksArray))
        {
            unset($ModArray[$code]);
        }

        if($CurrentModule && is_array($ModuleDefs[$CurrentModule]['BLOCK_LINKS']) && in_array($code, $ModuleDefs[$CurrentModule]['BLOCK_LINKS']))
        {
            unset($ModArray[$code]);
        }
    }

    return $ModArray;
}

?>
