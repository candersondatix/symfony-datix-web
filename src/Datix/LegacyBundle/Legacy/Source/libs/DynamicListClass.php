<?php
class DynamicList
{
    var $aButtons;
    
    var $sDivID;
    var $sDivContents;

    var $sType;
    var $sDelete;
    var $bUnique;
    var $bChoose;

    var $sListId;
    var $aListEntries;
    
    function DynamicList($aParams)
    {
        $this->aButtons = $aParams["buttons"];
        $this->sType = $aParams["type"];
        
        $this->sDelete = $aParams["delete"];
        $this->bUnique = ($aParams["unique"] != '');
        $this->bChoose = ($aParams["choose"] != '');
        
        $this->sListId = $aParams["listid"];
        $this->aListEntries = $aParams["list"];
        
        $this->sDivID = $aParams["divid"];
        $this->sDivContents = $aParams["divcontents"];
        
    }
    
    function GetListHeaderHTML()
    {
        $sHTML = "<div>";
        
        $sHTML .= "<span><select id='".$this->sListId."' name='".$this->sListId."'>";
                                 
        if($this->bChoose)
        {
            $sHTML .= "<option value=''>Choose</option>";
        }

        if(is_array($this->aListEntries))
        {
            foreach($this->aListEntries as $sKey => $sValue)
            {
                $sHTML .= "<option value='$sKey'>".$sValue."</option>";
            }
        }
        
        $sHTML .= "</select></span>";
        
        if($this->sDelete)
        {
            $JSOptions["delete"] = $this->sDelete;
        }
        if($this->bUnique)
        {
            $JSOptions["unique"] = "true";
        }
        else
        {
            $JSOptions["unique"] = "false";
        }
        
        foreach($JSOptions as $key => $value)
        {
            $JSOptionString[] = "$key : \"$value\"";     
        }
        
        foreach($this->aButtons as $sName => $aJSFunction)
        {
            if(is_array($aJSFunction["jsparams"]))
            {
                foreach($aJSFunction["jsparams"] as $sKey => $sVal)
                {
                    $aJSParams[] = "$sKey : \"$sVal\"";
                }    
            }
            
            $sHTML .= "<input type='button' value='$sName' onClick='if($(\"".$this->sListId."\").length > 0 && $(\"".$this->sListId."\").options[$(\"".$this->sListId."\").selectedIndex].value != \"\"){this.disabled=true;populateDynamicList(\"".$aJSFunction["jsfunction"]."\", \"".$this->sDivID."\", \"".$this->sListId."\", \"".$this->sType."\",{".implode(", ",$JSOptionString).(is_array($aJSParams) ? ", jsparams : {".implode(", ",$aJSParams)."}" : "")."});RemoveListEntry($(\"".$this->sListId."\"));this.disabled=false;}' />";
        }
                
        $sHTML .= "</div>";
        
        return $sHTML;
        
    }
    
    function GetDyanmicDivHTML()
    {
        $sHTML = "<div id='".$this->sDivID."'>".$this->sDivContents."</div>";  

        return $sHTML;        
    }
     
     
    
    
    
}
?>
