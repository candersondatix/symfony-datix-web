<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */

class ProgressBar
{
    var $CurrentPercent;

    var $CurrentText;

    function InitProgressBar()
    {
        $this->CurrentPercent = 0;
        
        $Progress = '
<table class="bordercolor" cellspacing="1" cellpadding="0" width="100%" align="center" border="0">
        <tr>
          <td>
            <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0"><tr>';
            
        for ($i = 0; $i < 100; $i++)
        {
            $Progress .= '<td bgcolor="white" width="1%"><div id="progress' . $i . '">&nbsp;</div></td>';
        }
        
        $Progress .= '</tr></table></td></tr></table>';
        $Progress .= '<div id="progress_text"></div>';
        return $Progress;
    }

    function SetProgressBar($Percent)
    {
        $Percent = round($Percent);
        
        if ($Percent >= 100)
        {
            $Percent = 100;
        }
        
        if ($this->CurrentPercent != $Percent)
        {
            $this->CurrentPercent = $Percent;
            for ($i = 0; $i < $Percent; $i++)
            {
                echo '<script language="JavaScript" type="text/javascript">document.getElementById("progress' . $i . '").style.backgroundColor = "#6394bd";</script>';
            }
            ob_flush();
            flush();   
        }
    }

    function IncrProgressBar($Percent)
    {
        $Percent = round($Percent);
        for ($i = $this->CurrentPercent; $i < (($this->CurrentPercent + $Percent) >= 100 ? 100 : ($this->CurrentPercent + $Percent)); $i++)
            echo '<script language="JavaScript" type="text/javascript">document.getElementById("progress' . $i . '").style.backgroundColor = "#6394bd";</script>';
        ob_flush();
        flush();
        $this->CurrentPercent += $Percent;
    }
    
    function ChangeProgressText($id, $text)
    {
        echo '<script language="JavaScript" type="text/javascript">';
        if(!isset($this->CurrentText[$id]))
        {
            $this->CurrentText[$id] = $text;
            echo 'var newdiv = document.createElement(\'div\');newdiv.id=\'progress_bar_'.$id.'\';setClass(newdiv, \'progress_text\');$(\'progress_text\').appendChild(newdiv);';
        }
        echo '$(\'progress_bar_'.$id.'\').innerHTML = \''.$text.'\'';
        echo '</script>';
        
        ob_flush();
        flush();
    }
}
?>