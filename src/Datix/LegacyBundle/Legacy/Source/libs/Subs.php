<?php

use src\users\model\UserModelFactory;
use src\framework\registry\Registry;
use src\framework\controller\TemplateController;

$yyheaderdone = 0;    // this is used to make the fatal error thing work better

$tabIndex = array(); // global variable for storing tabindex values, starts as 2 as 1 is used for the skip navigation link in the header, see template.php

trait HttpRequestUtil
{
    public function getHeaders()
    {

    }
}

/**
 * Function to get the next available tabIndex value and increment the global variable
 *
 * @return int returns value for tabindex
 */

function getTabIndexValue($start = 'main')
{
    global $tabIndex;

    if( ! $tabIndex[$start])
    {
        $tabIndex[$start] = ($start == 'main' ? 2 : ($start > $tabIndex['main'] ? $start : $tabIndex['main']));
    }

    $output = $tabIndex[$start];

    $tabIndex[$start] = $tabIndex[$start] + 1;

    return $output;
}

/**
* @desc Called at the bottom of every page - constructs the band along the bottom of the page containing the
* copyright information and any additional contents from template.php
*
* Takes no parameters and returns nothing, but echos HTML instead.
*/
function footer(\src\framework\controller\TemplateController $instance = null)
{
    // Warning: These global variables are referenced via variable variables,
    // so please do not remove them
    global $dtxtitle, $dtxmenu, $dtxtemplate, $yycopyin, $dtxtemplatemain, $dtxcopyright, $Development, $modSettings,
           $time_start, $txt, $dtxlogo_bottomright, $dtxlogo_bottomcentre, $dtxlogo_bottomleft, $JSFunctions,
           $scripturl, $dtxscreen_footer, $dtxprint_footer, $dtx_system_js_bottom, $dtx_system_click_block_div;

    $dtxextra_javascript = getJSFunctions();

    for ($i = $dtxtemplatemain; $i < sizeof($dtxtemplate); $i++)
    {
        $curline = $dtxtemplate[$i];

        if (!$yycopyin && \UnicodeString::strstr($curline,'<datix copyright>'))
        {
            $yycopyin = 1;
        }

        $tags = array();

        while (preg_match("/<datix\s+(\w+)>/u", $curline, $tags))
        {
            $temp = "dtx$tags[1]";

            if (function_exists($temp))
            {
                ob_start();
                $temp();
                $str = ob_get_contents();
                $curline = str_replace("<datix $tags[1]>", $str, $curline);
                ob_end_clean();
            }
            else
            {
                $curline = str_replace("<datix $tags[1]>", $$temp, $curline);
            }
        }

        echo "$curline\n";
    }
    if ($yycopyin == 0)
    {
        echo '
            <div align="center">
                <font size="5">
                    <b>
                        Sorry, the copyright tag &lt;datix copyright&gt; must be in the template.
                        <br />
                        Please notify this ' . "forum's" . ' administrator that this site is using an ILLEGAL copy of Datix!
                    </b>
                </font>
            </div>
        ';
    }

    // Form Design globals don't seem to be picked up in the Help popup for some reason,
    // so need to store the Help Texts in the session:
    if (is_array($_SESSION["HelpTexts"]) && is_array($GLOBALS["HelpTexts"]))
    {
        $_SESSION["HelpTexts"] = array_merge($_SESSION["HelpTexts"],$GLOBALS["HelpTexts"]);
    }
    elseif (is_array($GLOBALS["HelpTexts"]))
    {
        $_SESSION["HelpTexts"] = $GLOBALS["HelpTexts"];
    }
}

function PopulateTemplateStrings(\src\framework\controller\TemplateController $instance = null)
{
    global $dtx_system_css, $dtx_system_js_top, $dtx_system_js_bottom, $dtxuser_css_file, $dtx_system_click_block_div,
           $dtx_system_banner_error_div, $dtx_system_click_block_js, $dtx_system_accessibility_anchor,
           $ClientSpecificPrefix, $scripturl, $MinifierDisabled;

    $registry = src\framework\registry\Registry::getInstance();

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    if($instance instanceof TemplateController)
    {
        $dtx_system_css .= $instance->buildCss();
    }
    else
    {
        $dtx_system_css .= '
        <link rel="stylesheet" type="text/css" href="css/global' . $addMinExtension . '.css" media="screen"/>
        ' . ($registry->getParm('ENHANCED_ACCESSIBILITY', 'N') ? '<link rel="stylesheet" type="text/css" href="css/accessibility' . $addMinExtension . '.css" media="screen"/>' : '' ) . '
        <link rel="stylesheet" type="text/css" href="css/nav' . $addMinExtension . '.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="src/login/css/login' . $addMinExtension . '.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="src/dashboard/css/dashboard' . $addMinExtension . '.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="css/dropdown_popups' . $addMinExtension . '.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="css/crosstab' . $addMinExtension . '.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="css/reporting' . $addMinExtension . '.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="css/IE6' . $addMinExtension . '.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom' . $addMinExtension . '.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/jquery.datixweb' . $addMinExtension . '.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="js_functions/qTip/jquery.qtip.min.css" media="screen" />
        ';

        if($_GET['print'] == 1)
        {
            $dtx_system_css .= '<link rel="stylesheet" type="text/css" href="css/print' . $addMinExtension . '.css" media="print" />';
        }

        if (file_exists($dtxuser_css_file))
        {
            $dtx_system_css .= '
                <!--USER CSS FILE : -->
                <link rel="stylesheet" type="text/css" href="<datix user_css_file>" media="screen" />
            ';
        }

        if ($instance !== null && !$instance->hasHeader())
        {
            $dtx_system_css .= '
                <link rel="stylesheet" type="text/css" href="css/template_css/no-header'.$addMinExtension.'.css" media="screen" />
            ';
        }
    }

    $dtx_system_css .= '
        <link rel="SHORTCUT ICON" href="Images/datix.ico" />';


    if($instance instanceof TemplateController)
    {
        /**
         * Output required javascript to the page
         */
        $dtx_system_js_top .= $instance->buildJs();
        $dtx_system_js_bottom .= $instance->buildJs('bottom');
    }
    else
    {
        $dtx_system_js_top .= '<script language="javascript" type="text/javascript">var globals = {}; globals.deviceDetect = ' . json_encode(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->getDetectValues()) . '</script>
        <script type="text/javascript" src="js_functions/jquery/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="js_functions/jquery/jquery-ui-1.10.3.custom' . $addMinExtension . '.js"></script>
        <script type="text/javascript" src="js_functions/lib/jquery.navigation' . $addMinExtension . '.js"></script>
        <script type="text/javascript" src="js_functions/jquery/jquery.cookie' . $addMinExtension . '.js"></script>
        <script>jQuery.noConflict();</script>
        <script language="javascript" type="text/javascript" src="js_functions/common' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/Listing' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/listbox' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/timerClass' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/DynamicList' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript">
            var scripturl="'.$ClientSpecificPrefix.$scripturl.'";
        </script>
        <script language="javascript" type="text/javascript" src="js_functions/ie6compat' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/email' . $addMinExtension . '.js"></script>
        <script type="text/javascript" src="src/generic/js/modernizr.datix.min.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/jquery/jquery.ready' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/prototype' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/scriptaculous/scriptaculous.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/CodeSelectionCtrl' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="src/generic/js/limitTextareaChars' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/accessibility' . $addMinExtension . '.js"></script>
        <script language="javascript" type="text/javascript" src="js_functions/qTip/jquery.qtip.min.js"></script>
        ';

        if (bYN(GetParm('WEB_SPELLCHECKER', 'N')) && $_GET['print'] != 1)
        {
            $dtx_system_js_top .= '
            <script src="thirdpartylibs/phpspellcheck/include.js" type="text/javascript"></script>
        ';
        }

        if (!isset($_SESSION['FlashAvailable']))
        {
            $dtx_system_js_top .= '<script language="javascript" type="text/javascript" src="js_functions/AdobeFlashDetection' . $addMinExtension . '.js"></script>';
        }

        // Script to bind the calendar to date fields
        if (GetParm('FMT_DATE_WEB') == 'US')
        {
            $date_fmt= 'mm/dd/yy';
        }
        else
        {
            $date_fmt = 'dd/mm/yy';
        }

        /**
         * doCreateCalender will only be generated if we're not in a controller context, but initialiseCalendars() will
         * always be available, so we use that function to feed back to this function if neccessary. It's not great, but
         * hopefully gets round the problem of needing to trigger two different functions depending on the context.
         * Ideally this will be removed entirely when we're not having to balance both controller and non controller contexts
         */
        $dtx_system_js_top .= '
        <script language="javascript" type="text/javascript">
            function doCreateCalendar() {

                createCalendar(jQuery(\'.date\'), "'.GetParm('CALENDAR_ONCLICK', 'N').'", "'.$date_fmt.'", '.GetParm('WEEK_START_DAY', 2).', '.GetParm('CALENDAR_WEEKEND', 1).');
            }

            jQuery(function() {

    			initialiseCalendars();
            });
        </script>
';

        if (bYN(GetParm('CSRF_PREVENTION','N')))
        {
            $dtx_system_js_top .= '
                    <script language="javascript">
                        var token = \''.CSRFGuard::getCurrentToken().'\';
                        var suppressClientSideCSRF = false;
                    </script>
                    <script language="javascript" type="text/javascript" src="js_functions/tokenizer' . $addMinExtension . '.js"></script>
                ';
        }
        else
        {
            $dtx_system_js_top .= '<script language="javascript">
                        var token = \'\';
                </script>
                ';
        }

        $dtx_system_js_bottom = '
            <script language="javascript" type="text/javascript" src="js_functions/xml' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/FloatingWindowClass' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/jquery/jquery.class' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/encoder' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/dropdowns' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/ContactMatchCtrl' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/OrganisationMatchCtrl' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/MedicationMatchCtrl' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/EquipmentMatchCtrl' . $addMinExtension . '.js"></script>
            <script type="text/javascript" src="js_functions/colorpicker/js/colorpicker' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/export' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/contactSearch' . $addMinExtension . '.js"></script>
            <script language="javascript" type="text/javascript" src="js_functions/organisationSearch' . $addMinExtension . '.js"></script>
';
    }

    // Puts a temporary barrier in front of the page while loading completes.
    // Barrier is removed when page is fully loaded.
    if (bYN(GetParm('BLOCK_LOADING_CLICKS', 'Y')))
    {
        $dtx_system_click_block_div = '<div id="page-loading-block" class="page-loading-block"></div>';

        //Onload we want to remove the page-load buffer, allowing users to interact
        $dtx_system_click_block_js = '
            <script language="javascript" type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery(\'#page-loading-block\').hide();
                });
            </script>
        ';
    }
}

function template_header_nopadding(\src\framework\controller\TemplateController $instance = null)
{
    template_header('','',false,"UTF-8",new Template($instance instanceof TemplateController ? '' : array('no_padding' => true)), $instance);
}

function template_header_nomenu(\src\framework\controller\TemplateController $instance = null)
{
    template_header('','',false,"UTF-8",new Template($instance instanceof TemplateController ? '' : array('no_menu' => true)), $instance);
}

function template_header_nomenu_nopadding(\src\framework\controller\TemplateController $instance = null)
        {
    template_header('','',false,"UTF-8",new Template($instance instanceof TemplateController ? '' : array('no_menu' => true, 'no_padding' => true)), $instance);
}

/**
* @desc Called at the top of every page - constructs the band along the top of the page containing the
* currently logged in user, the top menu and any logos. Takes no parameters and echos HTML.
*
* @return obj A ProgressBar object
*/
function template_header($WaitTitle = "", $WaitSubTitle = "", $ProgressBar = false, $Charset = "UTF-8",
                         $Template = null, \src\framework\controller\TemplateController $instance = null)
{
    // Warning: These global variables are referenced via variable variables,
    // so please do not remove them
     global $dtxtemplate, $dtxtitle, $dtxmenu, $dtxsub_menu, $menusep, $db_prefix, $scripturl, $helpfile, $yyheaderdone,
           $yycopyin, $dtxtemplatemain, $user_css_file, $imagesdir, $NoMainMenu, $txt, $dtxdebug, $dtxwait,
           $dtxlogo_topleft, $dtxlogo_topcentre, $dtxlogo_topright, $dtxuser_css_file, $dtxheading, $dtxleft_menu,
           $dtx_system_css, $dtx_system_js_top, $dtx_system_accessibility_anchor, $dtxside_panel,
           $dtx_system_click_block_div, $dtx_system_banner_error_div, $MinifierDisabled, $JSFunctions, $dtx_system_js_bottom;

    $dtxtitle = StripHTML($dtxtitle); //strip HTML from title (needed for form designs)

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    if ($yyheaderdone)
    {
        return;
    }

    PopulateTemplateStrings($instance);

    // print stuff to prevent cacheing of pages
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header('Content-Type: text/html; charset=utf-8');
    header("Pragma: no-cache");

    SetLogos();

    // TODO - this probably shouldn't go here as js is set in PopulateTemplateStrings()
    // Javascript to insert in the header for the  dynamic dropdown sizing
    $dtxheader_js = '';

    $dtxheader_js .= '
        <script language="JavaScript" type="text/javascript">
            function checkMultiWidth(multiListBox)
            {
    ';

    if (bYN(GetParm("ALLOW_DYNAMIC_SIZING",'Y')))
    {
        $dtxheader_js .= '
            if (multiListBox.length == 0)
            {
                multiListBox.style.width="300px";
            }
            else if (multiListBox.style.width != "auto" && multiListBox.length != 0)
            {
                multiListBox.style.width="auto";
            }

            if (multiListBox.offsetWidth < 300)
            {
                multiListBox.style.width="300px";
            }

            return true;
        ';
    }
    else
    {
        $dtxheader_js .= ' return true; ';
    }

    $dtxheader_js .= '
        }
        </script>
    ';

    // only valid form IDs are appended to links
    if (isset($_REQUEST["form_id"]) && is_numeric($_REQUEST["form_id"]))
    {
        $formID = Sanitize::SanitizeInt($_REQUEST['form_id']);
    }

    //setup globals for menus in header bar
    $dtxsub_menu = GetSubMenuHTML($formID);
    $dtxmenu = GetMenuHTML($formID);

    // Only turn on JavaScript logout timer if
    // the DIF_TIMER_MINS global is not set to -1
    // OR we are on the DIF1 form (i.e. not "logged_in") and the
    // "No timeout on DIF1" global is NOT set.
    // Also do not want to turn on the timer if we are on the logout page.
    $dtxtimeouthead = '
        <script language="JavaScript" type="text/javascript"><!--
            if (document.layers) {
    ';

    if ($_SESSION["logged_in"])
    {
      $dtxtimeouthead .= '  window.captureEvents(Event.MOUSEMOVE);';
    }
    else
    {
    $dtxtimeouthead .= '  window.captureEvents(Event.CLICK, Event.KEYPRESS);';
    }

    //By default the rediect URL will be taken from this global
    $redirecturl = $scripturl;

    //We wish to redirect to the home page of the module the user is currently in,
    //however sometimes a non standard module will not follow standard URL conventions.
    //In these cases we need to deduce the module from the current action
    $actionToModule = ['editcontact' => 'CON',
        'asset' => 'AST',
        'sabs' => 'SAB',
        'standard' => 'STN'];

    if (isset($_GET['module'])) {
        $redirecturl = 'index.php?module=' . $_GET['module'];
    } elseif (isset($_GET['action']) && isset($actionToModule[$_GET['action']])) {
        $redirecturl = 'index.php?module=' . $actionToModule[$_GET['action']];
    }

    $dtxtimeouthead .= '
}

function timedOut(sType)
{
    if (sType == "REC")
    {
        datixRecordTimer.Unlock();

        SendTo(\'' . $redirecturl .'\');
    }
    else if (sType == "LOGGED_OUT")
    {
        SendTo(\'' . $scripturl .'?'.urlencode(getGetString()).'\');
    }
    else
    {

        SendTo(\'' . $scripturl . '?action=logout&timed_out=Y'
        . (isset($formID) ? 'form_id=' . $formID : "")
        . '\');
    }
}
';
    require_once "Source/libs/RecordLocks.php";

    $LockTimeout = GetParm("RECORDLOCK_TIMEOUT",10);

    //Check timeout is not zero, otherwise continuous AJAX calls are made.
    if ($LockTimeout == 0)
    {
        $LockTimeout = 10;
    }

    $bRecordTimer = (CheckLockRecord() && $LockTimeout != -1 && bYN(GetParm("RECORD_LOCKING","N")));
    $bSessionTimer = (GetParm("DIF_TIMER_MINS",10) != -1);

    if ($bRecordTimer)
    {
        $dtxtimeouthead .= '
            var datixRecordTimer = new Timer('.$LockTimeout.',"REC", "datixRecordTimer", "'.$_SESSION["Record_Locks"]["Current_Lock"].'");
        ';
    }

    if ($bSessionTimer)
    {
        if (!$_SESSION["logged_in"] && $_GET['action'] == '' && !bYN(GetParm("DIF_1_NO_TIMEOUT","N")))
        {
            $dtxtimeouthead .= '
                var datixSessionTimer = new Timer('.GetParm("DIF_TIMER_MINS",10).',"LOGGED_OUT", "datixSessionTimer");
            ';
        }
        elseif ($_SESSION["logged_in"])
        {
            $dtxtimeouthead .= '
                var datixSessionTimer = new Timer('.GetParm("DIF_TIMER_MINS",10).',"SES", "datixSessionTimer");
            ';
        }
    }

    $dtxtimeouthead .= '
    //-->
    </script>';

    if (!$_SESSION["logged_in"] && $_GET['action'] == '' && !bYN(GetParm("DIF_1_NO_TIMEOUT","N")))
    {
        $dtxonload = ' onmousemove="'.($bSessionTimer ? 'datixSessionTimer.Reset();' : '').'" onkeypress="'.($bSessionTimer ? 'datixSessionTimer.Reset();' : '').'"';
    }
    elseif ($_SESSION["logged_in"])
    {
        $dtxonload = 'onload="'.($bSessionTimer ? 'datixSessionTimer.Reset();' : '').($bRecordTimer ? 'datixRecordTimer.Reset();' : '' ).'" onmousemove="'.($bSessionTimer ? 'datixSessionTimer.Reset();' : '').($bRecordTimer ? 'datixRecordTimer.Reset();' : '' ).'"';
    }

    // If first_login is set, display the login message and then
    // unset first_login.
    if (bYN(GetParm("DIF_LOGIN_MSG","N")) && $_SESSION["first_login"])
    {
        $LoginMessages = GetLoginMessages();
    }

    if (bYN(GetParm('DISPLAY_LAST_LOGIN', 'N')) && $_SESSION["first_login"])
    {
        $LoginMessages[] = GetLastLoginMessage();
    }

    if (!empty($LoginMessages))
    {
        foreach ($LoginMessages as $LoginMessage)
        {
            $LoginMessage = str_replace('"', '\"', $LoginMessage);
            $LoginMessage = str_replace("\r\n", "\\n", $LoginMessage);
            $dtxtimeouthead .= '<script language="javascript" type="text/javascript">
OldAlert("' . $LoginMessage . '");
</script>';
        }
    }

    // Displays and alert when the admin email address is not set.
    if ($_SESSION['AdminUser'] === true && GetParm('DIF_ADMIN_EMAIL') == '' && $_SESSION['first_login'])
    {
        $dtxtimeouthead .= '
            <script type="text/javascript">
                OldAlert("The admin email address is blank. This must be populated if emails are needed to be sent from within the application.");
            </script>
        ';
    }

    // Make sure we don't blank this out if we are about to get a reload
    if (!(bYN(GetParm('DETECT_TIMEZONES', 'N')) && !isset($_SESSION['Timezone'])))
    {
        $_SESSION["first_login"] = "";
    }

    if ($WaitTitle)
    {
        $dtxwait = '<div id="waiting">
        <table class="bordercolor" cellspacing="0" cellpadding="0" width="90%" align="center" bgcolor="#6394bd" border="0">
  <tr>
    <td>
      <table class="bordercolor" cellspacing="1" cellpadding="0" width="100%" align="center" bgcolor="#6394bd" border="0">
        <tr>
          <td>
            <table cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#ffffff" border="0">
              <tr>
                <td valign="top" width="100%">
<table class="windowbg2" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
<tr>
    <td class="titlebg">
    ' . $WaitTitle . '
    </td>
    </tr>
    <tr>
    <td class="windowbg">
    ' . $WaitSubTitle . '
    </td>
    </tr>';
        if ($ProgressBar)
        {
            $dtxwait .= '
        <tr>
        <td>
        <table class="windowbg2" cellspacing="1" cellpadding="4" width="50%" align="center" border="0">
            <tr>
            <td>';
            require_once "Source/libs/ProgressBar.php";

            $Progress = new ProgressBar();
            $dtxwait .= $Progress->InitProgressBar();
            $dtxwait .= '</td>
        </tr>
        </table>
        </td>
        </tr>';
        }
        $dtxwait .= '
    </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</div>';
    }

    if (!$Template)
    {
        $Template = new Template();
    }

    // Opens template.php and replaces <datix> tags with the contents of variables
    // which may have been set in DatixConfig.
    $dtxtemplate = $Template->getTemplateHTML();

    if (!sizeof($dtxtemplate))
    {
        die("Cannot open template file: $templateFile");
    }

    $yyboardname = $mbname;
    $time = 0;

    $dtxtime = date("j F Y", time());

    // display their username if they haven't set their real name yet.
    if (!$NoMainMenu && $_SESSION["fullname"] && $_SESSION["logged_in"] === TRUE)
    {
        $dtxuname = $_SESSION["fullname"];

        if ($dtxdebug)
        {
            $dtxuname .= ' ' . _tk('timeout') . GetParm('DIF_TIMER_MINS', '10');
        }

        $dtxuname = '<div id="uname">'.$dtxuname.'</div>';
    }

    $yycopyin = 0;

    for ($dtxtemplatemain = 0; $dtxtemplatemain < sizeof($dtxtemplate); $dtxtemplatemain++)
    {
        $curline = $dtxtemplate[$dtxtemplatemain];

        if (\UnicodeString::strstr($curline,'<datix main>'))
        {
            $dtxtemplatemain++;
            break;
        }

        if (!$yycopyin && \UnicodeString::strstr($curline, '<datix copyright>'))
        {
            $yycopyin = 1;
        }

        $tags = array();

        while (preg_match("/<datix\s+(\w+)>/u", $curline, $tags))
        {
            $temp = "dtx$tags[1]";

            if (function_exists($temp))
            {
                ob_start();
                $temp();
                $str = ob_get_contents();
                $curline = preg_replace("/<datix\s+$tags[1]>/u", $str, $curline);
                ob_end_clean();
            }
            else
            {
                $curline = preg_replace("/<datix\s+$tags[1]>/u", $GLOBALS[$temp] ?: $$temp, $curline);
            }
        }

        echo "$curline\n";
    }

    echo GetSessionMessages();

    //HACK: This is a terrible way of handing this and should be removed asap.
    if (!empty($_SESSION['ajaxEmailsToSend']))
    {
        $dtx_system_js_bottom .= '
            <script language="javascript" type="text/javascript" src="js_functions/EmailAlerts' . $addMinExtension . '.js"></script>';

        foreach ($_SESSION['ajaxEmailsToSend'] as $emailScript)
        {
            $JSFunctions[] = $emailScript;
        }

        unset($_SESSION['ajaxEmailsToSend']);
    }

    if ($dtxwait)
    {
        ob_flush();
        flush();
    }

    $yyheaderdone = 1;

    if ($ProgressBar)
    {
        return $Progress;
    }
}

/**
* @desc Gets the HTML for the dropdown menus in the header bar.
*
* This is hard coded at the moment (in the same manner as the old Main Menu), but could be designed in the future.
*/
function GetMenuHTML($formID)
{
    global $NoMainMenu, $ModuleDefs, $scripturl;

    $menuHTML = '';
    $menuArray = array();

    // If DIF_NOMAINMENU is set, do not display the main menu
    // if not logged in only.
    // If $NoMainMenu or "print" are set, do not display the main menu at all.
    if ($NoMainMenu || (bYN(GetParm("DIF_NOMAINMENU", "N")) && !$_SESSION["logged_in"]) || $_GET["print"] == 1)
    {
        return '';
    }
    else
    {
        if ($_SESSION['logged_in'])
        {
            $modules = ['INC','RAM','PAL','COM','CLA','SAB','STN'];

            foreach ($modules as $Module)
            {
                if (ModIsLicensed($Module) && CanSeeModule($Module))
                {
                    $menuArray[$Module] = array('label' => $ModuleDefs[$Module]['NAME']);
                    $menuArray[$Module]['contents'] = GetMenuItems(array('module' => $Module));
                }
            }

            //CQC works slightly differently as it is three modules combined into one.
            if (ModIsLicensed('CQO') && (CanSeeModule('CQO') || CanSeeModule('CQP') || CanSeeModule('CQS')))
            {
                $menuArray['CQO'] = array('label' => $ModuleDefs['CQO']['MENU_NAME']);
                $menuArray['CQO']['contents'] = GetMenuItems(array('module' => 'CQO'));
            }

            //Assessment also works slightly differently...
            if (ModIsLicensed('AMO') && (CanSeeModule('AMO') || CanSeeModule('AQU') || CanSeeModule('ATI') ||
                CanSeeModule('ATM') || CanSeeModule('ATQ')))
            {
                $menuArray['AMO'] = array('label' => $ModuleDefs['AMO']['MENU_NAME']);
                $menuArray['AMO']['contents'] = GetMenuItems(array('module' => 'AMO'));
            }

            $menuHTML.= '<ul class="topnav">';

            if (empty($menuArray))
            {
                $menuHTML.= '<li>.</li>'; //hack to stop the name of the current user losing its position.
            }

            foreach ($menuArray as $module => $details)
            {
                $menuHTML.= '<li><a href="javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=home&module='.$module.'\');}" tabindex="' . getTabIndexValue() . '">'._t($details['label']).'</a>';

                if (is_array($details['contents']))
                {
                    $menuHTML.= '<ul class="subnav">';

                    foreach ($details['contents'] as $subentries)
                    {
                        if (isset($subentries['menu_name']))
                        {
                            if (displaySectionHeading($subentries['items']))
                            {
                                $menuHTML.= '
                                    <li>
                                        <div>' . $subentries['menu_name'] . '</div>
                                        <ul style="padding-left: 10px;">
                                ';

                                foreach ($subentries['items'] as $Items)
                                {
                                    $menuHTML.= buildMenuItems($Items);
                                }

                                $menuHTML.= '</ul></li>';
                            }
                        }
                        else
                        {
                            $menuHTML.= buildMenuItems($subentries);
                        }
                    }

                    $menuHTML.= '</ul>';
                }

                $menuHTML.= '</li>';
            }

            $menuHTML.= '</ul>';
        }
    }

    return $menuHTML;
}

function buildMenuItems($Item)
{
    global $scripturl;

    $html = '';

    if ($Item['condition'] !== false)
    {
        $html.= '<li>';

        if ($Item['onclick'])
        {
            $html.= '<a href="#" onclick="if(CheckChange()){'.$Item['onclick'].'}" tabindex="' . getTabIndexValue() . '">';
        }
        else
        {
            $html.= '<a href="javascript:if(CheckChange()){SendTo(\''.($Item['external_link'] ? '' : $scripturl.'?').$Item['link'].'\', \'' . ($Item['new_window'] ? '_blank' : '') . '\');}" tabindex="' . getTabIndexValue() . '">';
        }

        $html.= '<img src="'.$Item['icon'].'" alt="'.$Item['alt'].'" /> '.$Item['label'].'</a></li>';
    }

    return $html;
}

/**
 * Function that decides if a section header is displayed
 *
 * @param array $Items Collection of items that appear inside the section
 * @return bool
 */
function displaySectionHeading($Items)
{
    $display = false;

    foreach ($Items as $Item)
    {
        if ($Item['condition'] === true)
        {
            $display = true;
            break;
        }
    }

    return $display;
}

function GetGenericMenuItems($Parameters)
{
    global $ModuleDefs;

    $canGenerateRecords = bYN(GetParm("ENABLE_GENERATE","N"));

    if (!$_SESSION['CurrentUser']->canSeeModule($Parameters['module']))
    {
        return array();
    }

    // Only do this if the user can generate records
    if($canGenerateRecords)
    {
        $recordid = $_GET["recordid"] ? (int) $_GET["recordid"]: null;

        if($recordid)
        {
            $sql = "SELECT count(*) as num FROM ".$ModuleDefs[$Parameters['module']]['TABLE']." WHERE ".MakeSecurityWhereClause("recordid = $recordid", $Parameters['module'], $_SESSION["initials"]);

            $recordAccess = (DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN) == '1');
        }
        else
        {
            $recordAccess = false;
        }

        // Used to count how many modules the user can generate records against
        $generateModules = array();
        $excludeArray = array();
        $keepArray = array();

        foreach (array_keys(getModArray()) as $checkMod)
        {
            $genModPerms = GetParm($ModuleDefs[$checkMod]['PERM_GLOBAL']);

            if (!is_array($ModuleDefs[$Parameters['module']]['GENERATE_MAPPINGS'][$checkMod]) || empty($ModuleDefs[$checkMod]['LEVEL1_PERMS']) || empty($genModPerms))
            {
                $excludeArray[] = $checkMod;  //modules not to be shown
            }
            else
            {
                $keepArray[] = $checkMod;  //modules to be shown
            }
        }
        if(is_array($keepArray))
        {
            foreach($keepArray as $checkMod)
            {
                //get the available approval status's that the user has perms to 'generate to'
                $codesList = GetLevelsTo($checkMod, GetAccessLevel($checkMod), 'NEW');
                if(is_array($codesList) && !empty($codesList))
                {
                    //if there are codes continue,
                    $generateModules[] = $checkMod;
                }
                else
                {
                    //otherwise there are no available status's and exclude this module
                    $excludeArray[] = $checkMod;
                }

            }
        }
    }

    $Perms = GetParm($ModuleDefs[$Parameters['module']]['PERM_GLOBAL']);

    //New record
    if (!$ModuleDefs[$Parameters['module']]['NO_REP_APPROVED'])
    {
        if ($_SESSION['CurrentUser']->canAddRecords($Parameters['module']))
        {
            if (bYN(GetParm('NEW_RECORD_LVL1_'.$Parameters['module'], 'N')))  //always use level 1 form for new records
            {
                $menuArray[] = array(
                    'label' => _tk('add_a_new').' '.$ModuleDefs[$Parameters['module']]['REC_NAME'],
                    'icon' => 'images/icon_plus.gif',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'link' => 'action=addnew&level=1&module='.$Parameters['module'],
                    'sprite_id' => 1
                );
            }
            elseif ((in_array($Perms, $ModuleDefs[$Parameters['module']]['LEVEL1_PERMS']) || $Perms == "") &&
                !$ModuleDefs[$Parameters['module']]['NO_LEVEL1'])
            {
                $menuArray[] = array(
                    'label' => _tk('add_a_new').' '.$ModuleDefs[$Parameters['module']]['REC_NAME'],
                    'icon' => 'images/icon_plus.gif',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'link' => 'action=addnew&level=1&module='.$Parameters['module'],
                    'sprite_id' => 1
                );
            }
            else
            {
                $menuArray[] = array(
                    'label' => _tk('add_a_new').' '.$ModuleDefs[$Parameters['module']]['REC_NAME'],
                    'icon' => 'images/icon_plus.gif',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'link' => 'action=addnew&level=2&module='.$Parameters['module'],
                    'sprite_id' => 1
                );
            }
        }

        if ($canGenerateRecords && $recordAccess && count($generateModules) > 0 && $Parameters['module'] == $_GET['module'] &&
                (is_array($ModuleDefs[$Parameters['module']]['GENERATE_MAPPINGS'])))
            {
                    $menuArray[] = array(
                        'label' => _tk('generate'),
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=generaterecord&amp;module='.$Parameters['module'].'&recordid='.$recordid,
                        'sprite_id' => 1
                    );
                }
            }
    else
    {
        if (is_array($ModuleDefs[$Parameters['module']]['ADD_NEW_RECORD_LEVELS']) &&
            count(array_intersect($ModuleDefs[$Parameters['module']]['ADD_NEW_RECORD_LEVELS'],
                $_SESSION['CurrentUser']->getAccessLevels($Parameters['module']))) > 0)
        {
            if ($ModuleDefs[$Parameters['module']]['GENERIC'])
            {
                // Always use level 1 form for new records
                if (bYN(GetParm('NEW_RECORD_LVL1_'.$Parameters['module'], 'N')))
                {
                    $menuArray[] = array(
                        'label' => _tk('add_a_new').' '.$ModuleDefs[$Parameters['module']]['REC_NAME'],
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=addnew&level=1&module='.$Parameters['module'],
                        'sprite_id' => 1
                    );
                }
                elseif (!$ModuleDefs[$Parameters['module']]['NO_LEVEL1'] &&
                    $ModuleDefs[$Parameters['module']]['LEVEL1_PERMS'] &&
                    (in_array($Perms, $ModuleDefs[$Parameters['module']]['LEVEL1_PERMS']) || $Perms == ""))
                {
                    $menuArray[] = array(
                        'label' => _tk('add_a_new').' '.$ModuleDefs[$Parameters['module']]['REC_NAME'],
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=addnew&level=1&module='.$Parameters['module'],
                        'sprite_id' => 1
                    );
                }
                else
                {
                    $menuArray[] = array(
                        'label' => _tk('add_a_new').' '.$ModuleDefs[$Parameters['module']]['REC_NAME'],
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=addnew&level=2&module='.$Parameters['module'],
                        'sprite_id' => 1
                    );
                }
            }
            else
            {
                $menuArray[] = array(
                    'label' => _tk('add_a_new').' '.$ModuleDefs[$Parameters['module']]['REC_NAME'],
                    'icon' => 'images/icon_plus.gif',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'link' => 'action='.$ModuleDefs[$Parameters['module']]['NEW_RECORD_ACTION'],
                    'sprite_id' => 1
                );
            }
        }

        if ($canGenerateRecords && $recordAccess && count($generateModules) > 0 && $Parameters['module'] == $_GET['module'] &&
                (is_array($ModuleDefs[$Parameters['module']]['GENERATE_MAPPINGS'])))
            {
                    $menuArray[] = array(
                        'label' => _tk('generate'),
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=generaterecord&amp;module=' . $Parameters['module'] . '&recordid=' . $recordid,
                        'sprite_id' => 1
                    );
                }

        if (!$ModuleDefs[$Parameters['module']]['INPUT_ONLY_LEVELS'] ||
            !in_array($Perms, $ModuleDefs[$Parameters['module']]['INPUT_ONLY_LEVELS']))
        {
            //There will be no "status" section, so we need to include the hard coded listings here.
            if (is_array($ModuleDefs[$Parameters['module']]['HARD_CODED_LISTINGS']))
            {
                foreach ($ModuleDefs[$Parameters['module']]['HARD_CODED_LISTINGS'] as $Listing => $Details)
                {
                    if ($Details['Condition'] !== false)
                	{
                    	$menuArray[] = array(
                            'label' => $Details['Link'],
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&amp;module='.$Parameters['module'].'&amp;listtype='.$Listing,
                            'sprite_id' => 2
                        );
                	}
                }
            }
        }
    }

    if (!$ModuleDefs[$Parameters['module']]['INPUT_ONLY_LEVELS'] ||
        !in_array($Perms, $ModuleDefs[$Parameters['module']]['INPUT_ONLY_LEVELS']))
    {
        if (GetUserFormLevel($Parameters['module'], $Perms) == 2)
        {
            $menuArray[] = array(
                'label' => _tk('my_reports'),
                'icon' => 'images/icon_reports.gif',
                'icon_blue' => 'images/icon_reports_blue.gif',
                'link' => 'action=listmyreports&module='.$Parameters['module'].'&form=predefined',
                'sprite_id' => 12,
                'condition' => (!$ModuleDefs[$Parameters['module']]['NO_REPORTING'])
            );
            $menuArray[] = array(
                'label' => _tk('design_report'),
                'icon' => 'images/icon_design_reports.gif',
                'icon_blue' => 'images/icon_design_reports_blue.gif',
                'link' => 'action=reportdesigner&blank=1&module='.$Parameters['module'],
                'sprite_id' => 23,
                'condition' => (!$ModuleDefs[$Parameters['module']]['NO_REPORTING'] &&
                    bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')))
            );
        }
    }

    $menuArray[] = array(
        'label' => _tk('new_search'),
        'icon' => 'images/icon_search.gif',
        'icon_blue' => 'images/icon_search_blue.gif',
        'link' => 'action=search&module='.$Parameters['module'],
        'sprite_id' => 24
    );
    $menuArray[] = array(
        'label' => _tk('saved_queries'),
        'icon' => 'images/icon_save.gif',
        'icon_blue' => 'images/icon_save_blue.gif',
        'link' => 'action=savedqueries&module='.$Parameters['module'],
        'sprite_id' => 25,
        'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
    );


    if (in_array($Parameters['module'], array('COM', 'CLA', 'PAL', 'DST', 'HOT', 'HSA', 'LIB', 'PAY', 'POL')))
    {
        $menuArray[] = array(
            'label' => _tk('help_alt'),
            'icon' => 'images/icon_help.gif',
            'icon_blue' => 'images/icon_help_blue.gif',
            'link' => '?action=helpfinder&module='.$Parameters['module'],
            'sprite_id' => 51,
            'new_window' => true,
            'external_link' => true,
            'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
        );
    }

    return $menuArray;
}

function GetMenuItems($Parameters)
{
    global $ModuleDefs, $DatixView;

    $menuArray = array();
    $homeScreen = (isset($_GET['action']) && $_GET['action'] == 'home') ? true : false;
    $canGenerateRecords = bYN(GetParm("ENABLE_GENERATE","N"));

    if($canGenerateRecords)
    {
        $generateModules = array();
        $ExcludeArray = array();
        $KeepArray = array();

        foreach (array_keys(getModArray()) as $CheckMod)
        {
            $GenModPerms = GetParm($ModuleDefs[$CheckMod]['PERM_GLOBAL']);

            if (!is_array($ModuleDefs[$Parameters['module']]['GENERATE_MAPPINGS'][$CheckMod]) || empty($ModuleDefs[$CheckMod]['LEVEL1_PERMS']) || empty($GenModPerms))
            {
                $ExcludeArray[] = $CheckMod;  //modules not to be shown
            }
            else
            {
                $KeepArray[] = $CheckMod;  //modules to be shown
            }
        }
        if(is_array($KeepArray))
        {
            foreach($KeepArray as $CheckMod)
            {
                //get the available approval status's that the user has perms to 'generate to'
                $codesList = GetLevelsTo($CheckMod, GetAccessLevel($CheckMod), 'NEW');
                if(is_array($codesList) && !empty($codesList))
                {
                    //if there are codes continue,
                    $generateModules[] = $CheckMod;
                }
                else
                {
                    //otherwise there are no available status's and exclude this module
                    $ExcludeArray[] = $CheckMod;
                }

            }
        }
    }

    if ($ModuleDefs[$Parameters['module']]['GENERIC'] &&
        !($ModuleDefs[$Parameters['module']]['NO_REP_APPROVED'] &&
            !$ModuleDefs[$Parameters['module']]['HARD_CODED_LISTINGS']))
    {
        $menuArray = GetGenericMenuItems($Parameters);
    }
    else
    {
        switch ($Parameters['module'])
        {
            case 'INC':

                // Get the record ID for the incident if supplied
                $inc_id = ($_GET['recordid'] ? (int) $_GET["recordid"] : null);

                // If supplied check for recordAccess and set var otherwise set as false
                if ($inc_id)
                {
                    //need to check we have access to the record before allowing generation.
                    $sql = "SELECT count(*) as num FROM ".$ModuleDefs[$Parameters['module']]['TABLE']." WHERE ".MakeSecurityWhereClause("recordid = $inc_id", $Parameters['module'], $_SESSION["initials"]);
                    $recordAccess = (DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN) == '1');
                }
                else
                {
                    $recordAccess = false;
                }

                // If the user can add records
                if ($_SESSION['CurrentUser']->canAddRecords('INC'))
                {
                    if (bYN(GetParm('NEW_RECORD_LVL1_INC', 'N')))  //always use DIF1 for new incidents
                    {
                        $menuArray[] = array(
                            'label' => _tk('add_new_incident'),
                            'icon' => 'images/icon_plus.gif',
                            'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=newdif1&module=INC',
                            'sprite_id' => 1
                        );
                    }
                    elseif (GetAccessLevel('INC') == "DIF1" || GetAccessLevel('INC') == "")
                    {
                        $menuArray[] = array(
                            'label' => _tk('add_new_incident'),
                            'icon' => 'images/icon_plus.gif',
                            'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=newdif1&module=INC',
                            'sprite_id' => 1
                        );
                    }
                    else
                    {
                        $menuArray[] = array(
                            'label' => _tk('add_new_incident'),
                            'icon' => 'images/icon_plus.gif',
                            'icon_blue' => 'images/icon_plus_blue.gif',
                            'link' => 'action=addnewincident',
                            'sprite_id' => 1
                        );
                    }

                    $IncidentCopy = bYN(GetParm("COPY_INCIDENTS","N"));
                    $inc_id = ($_GET['recordid'] ? (int) $_GET["recordid"] : null);

                    if ($inc_id)
                    {
                        //need to check we have access to the record before allowing generation.
                        $sql = "SELECT count(*) as num FROM ".$ModuleDefs[$Parameters['module']]['TABLE']." WHERE ".MakeSecurityWhereClause("recordid = $inc_id", $Parameters['module'], $_SESSION["initials"]);
                        $recordAccess = (DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN) == '1');
                    }

                    if ($IncidentCopy && ! $homeScreen && ((is_null($inc_id) && $_SESSION['INC']["WHERE"] != '') || ($inc_id && $recordAccess)))
                    {
                        $link = 'action=copyrecord&amp;module=INC'.($inc_id && $recordAccess ? '&amp;recordid='.$inc_id : '');
                        $menuArray[] = array(
                            'label' => _tk('copy'),
                            'icon' => 'images/icon_plus.gif',
                            'icon_blue' => 'images/icon_plus_blue.gif',
                            'link' => $link,
                            'sprite_id' => 1
                        );
                    }
                }

                // Check the user can generate record, has record access, has modules they can generate to and there are
                // mappings for the module
                if ($canGenerateRecords && $recordAccess && count($generateModules) > 0 && $Parameters['module'] == $_GET['module'] &&
                    (is_array($ModuleDefs[$Parameters['module']]['GENERATE_MAPPINGS'])))
                {
                    $menuArray[] = array(
                        'label' => _tk('generate'),
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=generaterecord&amp;module='.$Parameters['module'].'&recordid='.$inc_id,
                        'sprite_id' => 1
                    );
                }

                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=listmyreports&module=INC&form=predefined',
                    'sprite_id' => 12,
                    'condition' => (GetAccessLevel('INC') != 'DIF1')
                );
                $menuArray[] = array(
                    'label' => _tk('design_report'),
                    'icon' => 'images/icon_design_reports.gif',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'link' => 'action=reportdesigner&blank=1&module=INC',
                    'sprite_id' => 23,
                    'condition' => (GetAccessLevel('INC') != 'DIF1') && bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y'))
                );
                $menuArray[] = array(
                    'label' => _tk('new_search'),
                    'icon' => 'images/icon_search.gif',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'link' => 'action=incidentssearch',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk('saved_queries'),
                    'icon' => 'images/icon_save.gif',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'link' => 'action=savedqueries&module=INC',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
                );
                $menuArray[] = array(
                    'label' => _tk('show_staff_responsibilities'),
                    'icon' => 'images/icon_staff.gif',
                    'icon_blue' => 'images/icon_staff_blue.gif',
                    'link' => 'action=staffresponsibilities',
                    'sprite_id' => 2,
                    'condition' => IsFullAdmin()
                );
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=INC',
                    'new_window' => true,
                    'sprite_id' => 51,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'RAM':

                // Get the record ID for the incident if supplied
                $ram_id = ($_GET['recordid'] ? (int) $_GET['recordid'] : null);

                // If supplied check for recordAccess and set var otherwise set as false
                if ($ram_id)
                {
                    //need to check we have access to the record before allowing generation.
                    $sql = "SELECT count(*) as num FROM ".$ModuleDefs[$Parameters['module']]['TABLE']." WHERE ".MakeSecurityWhereClause("recordid = $ram_id", $Parameters['module'], $_SESSION["initials"]);
                    $recordAccess = (DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN) == '1');
                }
                else
                {
                    $recordAccess = false;
                }

                if ($_SESSION['CurrentUser']->canAddRecords('RAM'))
                {
                        if (bYN(GetParm('NEW_RECORD_LVL1_RAM', 'N')))  //always use RISK1 for new risks
                        {
                            $menuArray[] = array(
                                'label' => _tk('add_new_risk'),
                                'icon' => 'images/icon_plus.gif',
                                'icon_blue' => 'images/icon_plus_blue.gif',
                                'link' => 'action=newrisk1',
                                'sprite_id' => 1
                            );
                        }
                        elseif (GetAccessLevel('RAM') == "RISK1" || GetAccessLevel('RAM') == "")
                        {
                            $menuArray[] = array(
                                'label' => _tk('add_new_risk'),
                                'icon' => 'images/icon_plus.gif',
                                'icon_blue' => 'images/icon_plus_blue.gif',
                                'link' => 'action=newrisk1',
                                'sprite_id' => 1
                            );
                        }
                        else
                        {
                            $menuArray[] = array(
                                'label' => _tk('add_new_risk'),
                                'icon' => 'images/icon_plus.gif',
                                'icon_blue' => 'images/icon_plus_blue.gif',
                                'link' => 'action=newrisk2',
                                'sprite_id' => 1
                            );
                        }

                    $RiskCopy = bYN(GetParm('COPY_RISKS', 'N'));

                    if ($RiskCopy && !$homeScreen && ((is_null($ram_id) && $_SESSION['RAM']["WHERE"] != '') || ($ram_id && $recordAccess)))
                        {
                        $link = 'action=copyrecord&amp;module=RAM'.($ram_id && $recordAccess ? '&amp;recordid='.$ram_id : '');
                                $menuArray[] = array(
                            'label' => _tk('copy'),
                                    'icon' => 'images/icon_plus.gif',
                                    'icon_blue' => 'images/icon_plus_blue.gif',
                            'link' => $link,
                                    'sprite_id' => 1
                                );
                            }
                        }

                // Check the user can generate record, has record access, has modules they can generate to and there are
                // mappings for the module
                if ($canGenerateRecords && $recordAccess && count($generateModules) > 0 && $Parameters['module'] == $_GET['module'] &&
                    (is_array($ModuleDefs[$Parameters['module']]['GENERATE_MAPPINGS'])))
                        {
                            $menuArray[] = array(
                        'label' => _tk('generate'),
                                'icon' => 'images/icon_plus.gif',
                                'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=generaterecord&amp;module='.$Parameters['module'].'&recordid='.$ram_id,
                                'sprite_id' => 1
                            );
                        }

                $menuArray[] = array(
                    'label' => _tk('assurance_framework'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=assurancelist',
                    'sprite_id' => 1,
                    'condition' => GetAccessLevel('RAM') != 'RISK1' && bYN(GetParm("ASSURANCE_FRAMEWORK", "Y"))
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=listmyreports&module=RAM&form=predefined',
                    'sprite_id' => 12,
                    'condition' => GetAccessLevel('RAM') != 'RISK1'
                );
                $menuArray[] = array(
                    'label' => _tk('design_report'),
                    'icon' => 'images/icon_design_reports.gif',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'link' => 'action=reportdesigner&blank=1&module=RAM',
                    'sprite_id' => 23,
                    'condition' => (GetAccessLevel('RAM') != 'RISK1') && bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y'))
                );
                $menuArray[] = array(
                    'label' => _tk('new_search'),
                    'icon' => 'images/icon_search.gif',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'link' => 'action=risksearch',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk('saved_queries'),
                    'icon' => 'images/icon_save.gif',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'link' => 'action=savedqueries&module=RAM',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
                );
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=RAM',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'SAB':
                if (in_array('SAB2', $_SESSION['CurrentUser']->getAccessLevels('SAB')) || in_array('RM', $_SESSION['CurrentUser']->getAccessLevels('SAB')))
                {
                    $menuArray[] = array(
                        'label' => _tk('add_new_sab'),
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => 'action=addnewsabs',
                        'sprite_id' => 1
                    );
                }
                else
                {
                    $menuArray[] = array(
                        'label' => _tk('list_sab_response'),
                        'icon' => 'images/icon_reports.gif',
                        'icon_blue' => 'images/icon_list_blue.gif',
                        'link' => 'action=list&amp;module=SAB&amp;table=main&amp;listtype=mysabsresponse',
                        'sprite_id' => 2
                    );
                    $menuArray[] = array(
                        'label' => _tk('list_all_sab'),
                        'icon' => 'images/icon_reports.gif',
                        'icon_blue' => 'images/icon_list_blue.gif',
                        'link' => 'action=list&amp;module=SAB&amp;table=main&amp;listtype=mysabs',
                        'sprite_id' => 2
                    );
                }

                $menuArray[] = array(
                    'label' => _tk('list_all_open_sab'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=list&amp;module=SAB&amp;table=main&amp;listtype=open',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('list_all_closed_sab'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=list&amp;module=SAB&amp;table=main&amp;listtype=closed',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=listmyreports&module=SAB&form=predefined',
                    'sprite_id' => 12
                );
                $menuArray[] = array(
                    'label' => _tk('design_report'),
                    'icon' => 'images/icon_design_reports.gif',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'link' => 'action=reportdesigner&blank=1&module=SAB',
                    'sprite_id' => 23,
                    'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y'))
                );
                $menuArray[] = array(
                    'label' => _tk('new_search'),
                    'icon' => 'images/icon_search.gif',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'link' => 'action=sabssearch',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk('saved_queries'),
                    'icon' => 'images/icon_save.gif',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'link' => 'action=savedqueries&module=SAB',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
                );

                if (in_array('SAB2', $_SESSION['CurrentUser']->getAccessLevels('SAB')) || in_array('RM', $_SESSION['CurrentUser']->getAccessLevels('SAB')))
                {
                    $overdue_action_underway = CountSABS("sab_dstart is null and sab_dclosed is null", true,
                        'sab_dstart_due');

                    if ($overdue_action_underway > 0)
                    {
                        if ($overdue_action_underway > 1)
                        {
                            $label = _tk('there_are') . ' ' . $overdue_action_underway . ' ' . _tk("SABSNames") . _tk('deadline_overdue');
                        }
                        else
                        {
                            $label = _tk('there_is') . ' ' . $overdue_action_underway . ' ' . _tk("SABSName") . _tk('deadline_overdue');
                        }

                        $menuArray[] = array(
                            'label' => $label,
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&amp;module=SAB&amp;table=main&amp;overdue=1&amp;overduetype=underway',
                            'sprite_id' => 2
                        );
                    }

                    $overdue_action_completed = CountSABS("sab_dend is null and sab_dclosed is null", true,
                        'sab_dend_due');

                    if ($overdue_action_completed > 0)
                    {
                        if ($overdue_action_completed > 1)
                        {
                            $label = _tk('there_are') . ' ' . $overdue_action_completed . ' ' . _tk("SABSNames") . _tk('action_overdue');
                        }
                        else
                        {
                            $label = _tk('there_is') . ' ' . $overdue_action_completed . ' ' . _tk("SABSName") . _tk('action_overdue');
                        }

                        $menuArray[] = array(
                            'label' => $label,
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&amp;module=SAB&amp;table=main&amp;overdue=1&amp;overduetype=completed',
                            'sprite_id' => 2
                        );
                    }

                    $overdue_read = CountSABS(
                        "sabs_main.recordid IN (SELECT sab_id FROM sab_link_contacts, contacts_main
                        WHERE sab_link_contacts.con_id = contacts_main.recordid AND link_type = 'S'
                        AND LINK_READ_DATE is null
                        AND sab_link_contacts.link_owner_id " .
                            (GetAccessLevel('SAB') == "SAB2" ? "IS NULL" : $_SESSION["contact_login_id"]) .
                        ") and sab_dclosed is null", true, 'sab_read_by_due');

                    if ($overdue_read > 0)
                    {
                        $menuArray[] = array(
                            'label' => _tk('there_are') . ' ' . $overdue_read . ' ' . _tk("SABSNames") . _tk('read_overdue'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&amp;module=SAB&amp;table=main&amp;overdue=1&amp;overduetype=readby',
                            'sprite_id' => 2
                        );
                    }

                    $menuArray[] = array(
                        'label' => _tk('help_alt'),
                        'icon' => 'images/icon_help.gif',
                        'icon_blue' => 'images/icon_help_blue.gif',
                        'link' => '?action=helpfinder&module=SAB',
                        'sprite_id' => 51,
                        'new_window' => true,
                        'external_link' => true,
                        'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                    );
                }

                break;
            case 'STN':
                $menuArray[] = array(
                    'label' => _tk('add_new_standard'),
                    'icon' => 'images/icon_plus.gif',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'link' => 'action=newstandard',
                    'sprite_id' => 1,
                    'condition' => (in_array('STN_FULL', $_SESSION['CurrentUser']->getAccessLevels('STN')) && bYN(GetParm("STN_ADD_NEW", "Y")))
                );

                $StandardsCopy = bYN(GetParm('COPY_STANDARDS', 'N'));
                $stn_id = ($_GET['recordid'] ? (int) $_GET['recordid'] : null);

                if ($stn_id)
                {
                    //need to check we have access to the record before allowing generation.
                    $sql = "SELECT count(*) as num FROM ".$ModuleDefs[$Parameters['module']]['TABLE']." WHERE ".MakeSecurityWhereClause("recordid = $stn_id", $Parameters['module'], $_SESSION["initials"]);
                    $recordAccess = (DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN) == '1');
                }

                if ((($_SESSION['STN']['WHERE'] != '') || ($stn_id && $recordAccess)) && $StandardsCopy && $homeScreen === false)
                {
                    $link = 'action=copyrecord&amp;module=STN'.($stn_id && $recordAccess ? '&amp;recordid='.$stn_id : '');
                    $menuArray[] = array(
                        'label' => _tk('copy'),
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => $link,
                        'sprite_id' => 1
                    );
                }

                $menuArray[] = array(
                    'label' => _tk('list_all_standards'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=liststandards',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('list_my_elements'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=listmyelements',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('list_pending_elements'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=listpendingelements',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('list_my_pending_elements'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=listmypendingelements',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=listmyreports&module=STN&form=predefined',
                    'sprite_id' => 12
                );
                $menuArray[] = array(
                    'label' => _tk("design_report"),
                    'icon' => 'images/icon_design_reports.gif',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'link' => 'action=reportdesigner&blank=1&module=STN',
                    'sprite_id' => 23,
                    'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y'))
                );
                $menuArray[] = array(
                    'label' => _tk("new_search"),
                    'icon' => 'images/icon_search.gif',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'link' => 'action=search&module=STN',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk("saved_queries"),
                    'icon' => 'images/icon_save.gif',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'link' => 'action=savedqueries&module=STN',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
                );

                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=STN',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'CQO':
            case 'CQP':
            case 'CQS':
            case 'LOC':
                $menuArray[] = array(
                    'label' => _tk('list_locations'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=list&module=LOC&listtype=all',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('cqc_list_outcomes'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=list&module=CQO&listtype=all',
                    'sprite_id' => 2,
                    'condition' => CanSeeModule('CQO')
                );
                $menuArray[] = array(
                    'label' => _tk('cqc_list_prompts'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=list&module=CQP&listtype=all',
                    'sprite_id' => 2,
                    'condition' => CanSeeModule('CQP')
                );
                $menuArray[] = array(
                    'label' => _tk('cqc_list_subprompts'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'link' => 'action=list&module=CQS&listtype=all',
                    'sprite_id' => 2,
                    'condition' => CanSeeModule('CQS')
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports').' '._tk('cqc_outcomes'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=listmyreports&module=CQO&form=predefined',
                    'sprite_id' => 12,
                    'condition' => CanSeeModule('CQO')
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports').' '._tk('cqc_prompts'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=listmyreports&module=CQP&form=predefined',
                    'sprite_id' => 12,
                    'condition' => CanSeeModule('CQP')
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports').' '._tk('cqc_subprompts'),
                    'icon' => 'images/icon_reports.gif',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'link' => 'action=listmyreports&module=CQS&form=predefined',
                    'sprite_id' => 12,
                    'condition' => CanSeeModule('CQS')
                );
                $menuArray[] = array(
                    'label' => _tk("design_report").' '._tk('cqc_outcomes'),
                    'icon' => 'images/icon_design_reports.gif',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'link' => 'action=reportdesigner&blank=1&module=CQO',
                    'sprite_id' => 23,
                    'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')) && CanSeeModule('CQO')
                );
                $menuArray[] = array(
                    'label' => _tk("design_report").' '._tk('cqc_prompts'),
                    'icon' => 'images/icon_design_reports.gif',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'link' => 'action=reportdesigner&blank=1&module=CQP',
                    'sprite_id' => 23,
                    'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')) && CanSeeModule('CQP')
                );
                $menuArray[] = array(
                    'label' => _tk("design_report").' '._tk('cqc_subprompts'),
                    'icon' => 'images/icon_design_reports.gif',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'link' => 'action=reportdesigner&blank=1&module=CQS',
                    'sprite_id' => 23,
                    'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')) && CanSeeModule('CQS')
                );
                $menuArray[] = array(
                    'label' => _tk('cqc_outcome').' '._tk('saved_queries'),
                    'icon' => 'images/icon_save.gif',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'link' => 'action=savedqueries&module=CQO',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('CQO')
                );
                $menuArray[] = array(
                    'label' => _tk('cqc_prompt').' '._tk('saved_queries'),
                    'icon' => 'images/icon_save.gif',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'link' => 'action=savedqueries&module=CQP',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('CQP')
                );
                $menuArray[] = array(
                    'label' => _tk('cqc_subprompt').' '._tk('saved_queries'),
                    'icon' => 'images/icon_save.gif',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'link' => 'action=savedqueries&module=CQS',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('CQS')
                );
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=CQO',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'CON':
                $menuArray[] = array(
                    'label' => _tk('create_new_contact'),
                    'link' => 'action=newcontact',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'sprite_id' => 1,
                    'condition' => $_SESSION['CurrentUser']->notOnlyAccessLevel('CON', 'CON_READ_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk('list_all_contacts'),
                    'link' => 'action=listcontacts',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'sprite_id' => 2,
                    'condition' => CanListContacts()
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'link' => 'action=listmyreports&module=CON&form=predefined',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'sprite_id' => 12,
                    'condition' => CanListContacts()
                );
                $menuArray[] = array(
                    'label' => _tk("design_report"),
                    'link' => 'action=reportdesigner&blank=1&module=CON',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'sprite_id' => 23,
                    'condition' => (CanListContacts() && bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')))
                );
                $menuArray[] = array(
                    'label' => _tk("new_search"),
                    'link' => 'action=contactssearch',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'sprite_id' => 24,
                    'condition' => CanListContacts()
                );
                $menuArray[] = array(
                    'label' => _tk("saved_queries"),
                    'link' => 'action=savedqueries&module=CON',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'sprite_id' => 25,
                    'condition' => (CanListContacts() && bYN(GetParm("SAVED_QUERIES", "Y")))
                );
                if ($_SESSION[$Parameters['module']]['SEARCHLISTURL'] == null)
                {
                    $menuArray[] = array(
                        'label' => _tk("manage_duplicates"),
                        'link' => 'service=duplicatesearch&event='.(empty($_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS']) ? 'collectparameters' : 'performsearch'),
                        'icon_blue' => 'images/icon_search_blue.gif',
                        'sprite_id' => 24,
                        'condition' => (CanListContacts() && (IsFullAdmin() || bYN(GetParm('CON_ALLOW_MERGE_DUPLICATES', 'N'))))
                    );
                }
                else
                {
                    $manageDuplicates = array(
                        'label' => _tk("manage_duplicates"),
                        'link' => 'service=duplicatesearch&event='.(empty($_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS']) ? 'collectparameters' : 'performsearch'),
                        'icon_blue' => 'images/icon_search_blue.gif',
                        'sprite_id' => 24,
                        'condition' => (CanListContacts() && (IsFullAdmin() || bYN(GetParm('CON_ALLOW_MERGE_DUPLICATES', 'N'))))
                    );
                }
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=CON',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'ACT':
                $OverdueActions = CountActions(true);

                $menuArray[] = array(
                    'label' => _tk('list_all_actions'),
                    'link' => 'action=list&module=ACT&listtype=all',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => 'There '.($OverdueActions == 1 ? 'is': 'are').' '.$OverdueActions.' overdue '.($OverdueActions == 1 ? 'Action': 'Actions'),
                    'link' => 'action=list&module=ACT&overdue=1',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'sprite_id' => 2,
                    'condition' => $OverdueActions
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'link' => 'action=listmyreports&module=ACT&form=predefined',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'sprite_id' => 12,
                    'condition' => (GetAccessLevel('ACT') != 'ACT_INPUT_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk("design_report"),
                    'link' => 'action=reportdesigner&blank=1&module=ACT',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'sprite_id' => 23,
                    'condition' => ($_SESSION['CurrentUser']->notOnlyAccessLevel('ACT', 'ACT_INPUT_ONLY') && bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')))
                );
                $menuArray[] = array(
                    'label' => _tk("new_search"),
                    'link' => 'action=actionssearch',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk("saved_queries"),
                    'link' => 'action=savedqueries&module=ACT',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
                );
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=ACT',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'AST':
                $menuArray[] = array(
                    'label' => _tk('add_new_asset'),
                    'link' => 'action=addnewasset',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'sprite_id' => 1,
                    'condition' => (GetAccessLevel('AST') != 'AST_READ_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk("list_all_equipment"),
                    'link' => 'action=list&module=AST&listtype=all',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'sprite_id' => 2,
                    'condition' => (GetAccessLevel('AST') != 'AST_INPUT_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'link' => 'action=listmyreports&module=AST&form=predefined',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'sprite_id' => 12,
                    'condition' => (GetAccessLevel('AST') != 'AST_INPUT_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk("design_report"),
                    'link' => 'action=reportdesigner&blank=1&module=AST',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'sprite_id' => 23,
                    'condition' => (GetAccessLevel('AST') != 'AST_INPUT_ONLY') && bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y'))
                );
                $menuArray[] = array(
                    'label' => _tk("new_search"),
                    'link' => 'action=assetssearch',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk("saved_queries"),
                    'link' => 'action=savedqueries&module=AST',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
                );
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=AST',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'MED':
                $menuArray[] = array(
                    'label' => _tk('add_new_medication'),
                    'link' => 'action=addnewmedication',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'sprite_id' => 1,
                    'condition' => !bYN(GetParm('DMD_ENABLED', 'N')) && $_SESSION['CurrentUser']->notOnlyAccessLevel('MED', 'MED_READ_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk("import_dmd_data"),
                    'link' => 'action=dmdimportform',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'sprite_id' => 11,
                    'condition' => bYN(GetParm('DMD_ENABLED', 'N'))
                );
                $menuArray[] = array(
                    'label' => _tk("list_all_medication"),
                    'link' => 'action=list&module=MED&listtype=all',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'sprite_id' => 2
                );
                $menuArray[] = array(
                    'label' => _tk('my_reports'),
                    'link' => 'action=listmyreports&module=MED&form=predefined',
                    'icon_blue' => 'images/icon_reports_blue.gif',
                    'sprite_id' => 12
                );
                $menuArray[] = array(
                    'label' => _tk("design_report"),
                    'link' => 'action=reportdesigner&blank=1&module=MED',
                    'icon_blue' => 'images/icon_design_reports_blue.gif',
                    'sprite_id' => 23,
                    'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y'))
                );
                $menuArray[] = array(
                    'label' => _tk("new_search"),
                    'link' => 'action=medicationssearch',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk("saved_queries"),
                    'link' => 'action=savedqueries&module=MED',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'sprite_id' => 25,
                    'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
                );
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=MED',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
            case 'ACR':
            case 'ATM':
            case 'ATQ':
            case 'ATI':
            case 'AQU':
            case 'AMO': // fall through to "assessment modules" module
                $menuArray[] = array(
                    'label' => _tk('add_new_assessment_template'),
                    'link' => 'action=addnew&level=2&module=ATM',
                    'icon' => 'images/icon_plus.gif',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'sprite_id' => 1,
                    'condition' => bYN(GetParm('ATM_MQ_TEMP', 'N')) && CanSeeModule('ATM')
                );
                $menuArray[] = array(
                    'menu_name' => _tk('listings'),
                    'sprite_id' => 2,
                    'items' => array(
                        1 => array(
                            'label' => _tk('LOCNamesTitle'),
                            'lhsm_label' => _tk('list_locations'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&module=LOC&listtype=all&sidemenu=ACR',
                            'display_always' => true,
                            'condition' => CanSeeModule('LOC')
                        ),
                        2 => array(
                            'label' => _tk("ATMNamesTitle"),
                            'lhsm_label' => _tk('amo_list_assessment_templates'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&module=ATM',
                            'condition' => CanSeeModule('ATM')
                        ),
                        3 => array(
                            'label' => _tk("ATQNamesTitle"),
                            'lhsm_label' => _tk('amo_list_criterion_templates'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&module=ATQ',
                            'condition' => CanSeeModule('ATQ')
                        ),
                        4 => array(
                            'label' => _tk("ATINamesTitle"),
                            'lhsm_label' => _tk('amo_list_assigned_assessments'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&module=ATI',
                            'condition' => CanSeeModule('ATI')
                        ),
                        5 => array(
                            'label' => _tk("AMONamesTitle"),
                            'lhsm_label' => _tk('amo_list_assessments'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&module=AMO',
                            'condition' => CanSeeModule('AMO')
                        ),
                        6 => array(
                            'label' => _tk("AQUNamesTitle"),
                            'lhsm_label' => _tk('amo_list_criteria'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_list_blue.gif',
                            'link' => 'action=list&module=AQU',
                            'condition' => CanSeeModule('AQU')
                        )
                    )
                );
                $menuArray[] = array(
                    'menu_name' => _tk('my_reports'),
                    'sprite_id' => 12,
                    'items' => array(
                        1 => array(
                            'label' => _tk('ATINamesTitle'),
                            'lhsm_label' => _tk('my_reports') . ' ' . _tk('amo_assigned_assessments'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_reports_blue.gif',
                            'link' => 'action=listmyreports&module=ATI&form=predefined',
                            'condition' => CanSeeModule('ATI')
                        ),
                        2 => array(
                            'label' => _tk('AMONamesTitle'),
                            'lhsm_label' => _tk('my_reports') . ' ' . _tk('amo_assessments'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_reports_blue.gif',
                            'link' => 'action=listmyreports&module=AMO&form=predefined',
                            'condition' => CanSeeModule('AMO')
                        ),
                        3 => array(
                            'label' => _tk('AQUNamesTitle'),
                            'lhsm_label' => _tk('my_reports') . ' ' . _tk('amo_criteria'),
                            'icon' => 'images/icon_reports.gif',
                            'icon_blue' => 'images/icon_reports_blue.gif',
                            'link' => 'action=listmyreports&module=AQU&form=predefined',
                            'condition' => CanSeeModule('AQU')
                        )
                    )
                );

                $menuArray[] = array(
                    'menu_name' => _tk('design_report'),
                    'sprite_id' => 23,
                    'items' => array(
                        1 => array(
                            'label' => _tk('ATINamesTitle'),
                            'lhsm_label' => _tk('design_report') . ' ' . _tk('amo_assigned_assessments'),
                            'icon' => 'images/icon_design_reports.gif',
                            'icon_blue' => 'images/icon_design_reports_blue.gif',
                            'link' => 'action=reportdesigner&blank=1&module=ATI',
                            'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')) && CanSeeModule('ATI')
                        ),
                        2 => array(
                            'label' => _tk('AMONamesTitle'),
                            'lhsm_label' => _tk('design_report') . ' ' . _tk('amo_assessments'),
                            'icon' => 'images/icon_design_reports.gif',
                            'icon_blue' => 'images/icon_design_reports_blue.gif',
                            'link' => 'action=reportdesigner&blank=1&module=AMO',
                            'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')) && CanSeeModule('AMO')
                        ),
                        3 => array(
                            'label' => _tk('AQUNamesTitle'),
                            'lhsm_label' => _tk('design_report') . ' ' . _tk('amo_criteria'),
                            'icon' => 'images/icon_design_reports.gif',
                            'icon_blue' => 'images/icon_design_reports_blue.gif',
                            'link' => 'action=reportdesigner&blank=1&module=AQU',
                            'condition' => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')) && CanSeeModule('AQU')
                        )
                    )
                );
                $menuArray[] = array(
                    'menu_name' => _tk('saved_queries'),
                    'sprite_id' => 25,
                    'items' => array(
                        1 => array(
                            'label' => _tk('ATMNamesTitle'),
                            'lhsm_label' => _tk('amo_assessment_template') . ' ' . _tk('saved_queries'),
                            'icon' => 'images/icon_save.gif',
                            'icon_blue' => 'images/icon_save_blue.gif',
                            'link' => 'action=savedqueries&module=ATM',
                            'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('ATM')
                        ),
                        2 => array(
                            'label' => _tk('ATQNamesTitle'),
                            'lhsm_label' => _tk('amo_criterion_template') . ' ' . _tk('saved_queries'),
                            'icon' => 'images/icon_save.gif',
                            'icon_blue' => 'images/icon_save_blue.gif',
                            'link' => 'action=savedqueries&module=ATQ',
                            'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('ATQ')
                        ),
                        3 => array(
                            'label' => _tk('ATINamesTitle'),
                            'lhsm_label' => _tk('amo_assigned_assessment') . ' ' . _tk('saved_queries'),
                            'icon' => 'images/icon_save.gif',
                            'icon_blue' => 'images/icon_save_blue.gif',
                            'link' => 'action=savedqueries&module=ATI',
                            'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('ATI')
                        ),
                        4 => array(
                            'label' => _tk('AMONamesTitle'),
                            'lhsm_label' => _tk('amo_assessment') . ' ' . _tk('saved_queries'),
                            'icon' => 'images/icon_save.gif',
                            'icon_blue' => 'images/icon_save_blue.gif',
                            'link' => 'action=savedqueries&module=AMO',
                            'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('AMO')
                        ),
                        5 => array(
                            'label' => _tk('AQUNamesTitle'),
                            'lhsm_label' => _tk('amo_criterion') . ' ' . _tk('saved_queries'),
                            'icon' => 'images/icon_save.gif',
                            'icon_blue' => 'images/icon_save_blue.gif',
                            'link' => 'action=savedqueries&module=AQU',
                            'condition' => (bYN(GetParm("SAVED_QUERIES", "Y"))) && CanSeeModule('AQU')
                        )
                    )
                );

                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=ATM',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
			case 'POL':
				$menuArray[] = array(
					'label' => _tk('add_new_policy'),
					'link' => 'action=addnew&level=2&module=POL',
					'icon_blue' => 'images/icon_plus_blue.gif',
					'sprite_id' => 1,
					'condition' => (GetAccessLevel('POL') != 'POL_READ_ONLY')
				);
				$menuArray[] = array(
					'label' => _tk("list_all_policy"),
					'link' => 'action=list&module=POL&listtype=all',
					'icon_blue' => 'images/icon_list_blue.gif',
					'sprite_id' => 2,
					'condition' => (GetAccessLevel('POL') != 'POL_INPUT_ONLY')
				);
				$menuArray[] = array(
					'label' => _tk('my_reports'),
					'link' => 'action=listmyreports&module=POL&form=predefined',
					'icon_blue' => 'images/icon_reports_blue.gif',
					'sprite_id' => 12,
					'condition' => (GetAccessLevel('POL') != 'POL_INPUT_ONLY')
				);
				$menuArray[] = array(
					'label' => _tk("design_report"),
					'link' => 'action=reportdesigner&blank=1&module=POL',
					'icon_blue' => 'images/icon_design_reports_blue.gif',
					'sprite_id' => 23,
					'condition' => (GetAccessLevel('POL') != 'POL_INPUT_ONLY') && bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y'))
				);
				$menuArray[] = array(
					'label' => _tk("new_search"),
					'link' => 'action=search&module=POL',
					'icon_blue' => 'images/icon_search_blue.gif',
					'sprite_id' => 24
				);
				$menuArray[] = array(
					'label' => _tk("saved_queries"),
					'link' => 'action=savedqueries&module=POL',
					'icon_blue' => 'images/icon_save_blue.gif',
					'sprite_id' => 25,
					'condition' => (bYN(GetParm("SAVED_QUERIES", "Y")))
				);

                $PolicyCopy = bYN(GetParm('COPY_POL', 'N'));
                $pol_id = ($_GET['recordid'] ? (int) $_GET['recordid'] : null);

                if ($pol_id)
                {
                    //need to check we have access to the record before allowing generation.
                    $sql = "SELECT count(*) as num FROM ".$ModuleDefs[$Parameters['module']]['TABLE']." WHERE ".MakeSecurityWhereClause("recordid = $pol_id", $Parameters['module'], $_SESSION["initials"]);
                    $recordAccess = (DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN) == '1');
                }

                if ($PolicyCopy && $homeScreen === false && ((is_null($pol_id) && $_SESSION['POL']["WHERE"] != '') || ($pol_id && $recordAccess)))
                {
                    $link = 'action=copyrecord&amp;module=POL'.($pol_id && $recordAccess ? '&amp;recordid='.$pol_id : '');
                    $menuArray[] = array(
                        'label' => _tk('copy'),
                        'icon' => 'images/icon_plus.gif',
                        'icon_blue' => 'images/icon_plus_blue.gif',
                        'link' => $link,
                        'sprite_id' => 1
                    );
                }
				break;
            case 'ORG':
                $menuArray[] = array(
                    'label' => _tk('add_new_organisation'),
                    'link' => 'action=addnew&level=2&module=ORG',
                    'icon_blue' => 'images/icon_plus_blue.gif',
                    'sprite_id' => 1,
                    'condition' => (GetAccessLevel('ORG') != 'ORG_READ_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk('list_all_organisations'),
                    'link' => 'action=list&module=ORG&listtype=all',
                    'icon_blue' => 'images/icon_list_blue.gif',
                    'sprite_id' => 2,
                    'condition' => (GetAccessLevel('ORG') != 'ORG_INPUT_ONLY')
                );
                $menuArray[] = array(
                    'label' => _tk("new_search"),
                    'link' => 'action=search&module=ORG',
                    'icon_blue' => 'images/icon_search_blue.gif',
                    'sprite_id' => 24
                );
                $menuArray[] = array(
                    'label' => _tk("saved_queries"),
                    'link' => 'action=savedqueries&module=ORG',
                    'icon_blue' => 'images/icon_save_blue.gif',
                    'sprite_id' => 25,
                    'condition' => (CanListContacts() && bYN(GetParm("SAVED_QUERIES", "Y")))
                );
                $menuArray[] = array(
                    'label' => _tk('help_alt'),
                    'icon' => 'images/icon_help.gif',
                    'icon_blue' => 'images/icon_help_blue.gif',
                    'link' => '?action=helpfinder&module=ORG',
                    'sprite_id' => 51,
                    'new_window' => true,
                    'external_link' => true,
                    'condition' => (bYN(GetParm('SHOW_MODULE_HELP', 'Y')))
                );
                break;
        }
    }

    // pop the help item from the menu
    // to add it back later
    if (end($menuArray)['label'] == _tk('help_alt'))
    {
        $helpItem = array_pop ($menuArray);
    }


    if ($_SESSION[$Parameters['module']]['SEARCHLISTURL'] != '')
    {
        if (!$ModuleDefs[$Parameters['module']]['LISTING_FILTERS'])
        {
            $menuArray[] = array(
                'label' => _tk('list_search'),
                'link' => $_SESSION[$Parameters['module']]['SEARCHLISTURL'],
                'external_link' => true,
                'icon_blue' => 'images/icon_search_blue.gif',
                'icon' => 'images/icon_search.gif',
                'sprite_id' => 24
            );
            $menuArray[] = array(
                'label' => _tk('clear_search'),
                'link' => 'action=clearsearch&amp;module='.$Parameters['module'],
                'icon_blue' => 'images/icon_search_blue.gif',
                'icon' => 'images/icon_search.gif',
                'sprite_id' => 24
            );
            if ($manageDuplicates != null)
            {
                $menuArray[] = $manageDuplicates;
            }

        }

        switch ($Parameters['module'])
        {
            case 'INC':
                if (bYN(GetParm('SHOW_NPSA_SETTINGS', 'Y')) && $_SESSION["AdminUser"] == true && $homeScreen === false)
                {
                    $menuArray[] = array(
                        'label' => 'Export to NPSA',
                        'icon' => 'images/icon_npsa_export.gif',
                        'icon_blue' => 'images/icon_npsa_export_blue.gif',
                        'onclick' => 'exportFile(\'app.php?action=npsaexport\', \'displayExportError(\\\''.str_replace('\'', '\\\'', _tk('npsa_export_error_title')).'\\\')\')',
                        'sprite_id' => 39
                    );
                }

                if (bYN(GetParm('SHOW_CFSMS_SETTINGS', 'Y'))&& $_SESSION["AdminUser"] == true && $homeScreen === false)
                {
                    $menuArray[] = array(
                        'label' => 'Export to CFSMS',
                        'icon' => 'images/icon_cfsms_export.gif',
                        'icon_blue' => 'images/icon_cfsms_export_blue.gif',
                        'onclick' => 'exportFile(\'index.php?service=export&event=cfsms\', \'setSirsTrustGlobal()\')',
                        'sprite_id' => 40
                    );
                }

                break;
            case 'COM':
                if (GetParm('COUNTRY', 'ENGLAND') == 'SCOTLAND' || bYN(GetParm('ENABLE_ISD', 'N')) == 'Y')
                {
                    $menuArray[] = array(
                        'label' => 'Export to ISD',
                        'icon' => 'images/icon_isd_export.gif',
                        'icon_blue' => 'images/icon_isd_export_blue.gif',
                        'onclick' => 'isdExport(\''.str_replace('\'', '\\\'', _tk('isd_export_title')).'\')',
                        'sprite_id' => 45
                    );
                }

                break;
        }
    }

    // add help back in the last position
    if (!empty($helpItem)) {
        array_push ($menuArray, $helpItem);
    }

    return $menuArray;
}

function CountActions($Overdue = true)
{
    if ($Overdue)
    {
        $ActionsWhere[] = "act_ddone is null and act_ddue is not null and act_ddue < '" . date('Y-m-d') . "'";
    }

    $WhereClause = MakeSecurityWhereClause($ActionsWhere, "ACT", $_SESSION["initials"]);

    $moduleWhere = "mod_module is null or mod_module IN ('" . implode("', '", getActionLinkModules()) . "')";

    $sql = "SELECT count(*) as act_count FROM ca_actions, modules
                WHERE ca_actions.ACT_MODULE = modules.mod_module ";

    if($moduleWhere != "")
    {
        $sql .= " AND ($moduleWhere)";
    }
    if ($WhereClause != "")
    {
        $sql .= " AND $WhereClause";
    }

    $actionCount = \DatixDBQuery::PDO_fetch($sql, array(), \PDO::FETCH_COLUMN);

    if($actionCount)
    {
        return $actionCount;
    }

    return 0;
}

function CountSABS($where = "", $Overdue = false, $OverdueField = "")
{
    if ($where)
    {
        if (!is_array($where))
        {
            $SABSWhere[] = $where;
        }
        else
        {
            $SABSWhere = $where;
        }
    }

    if ($Overdue)
    {
        $SABSWhere[] = $OverdueField . " < '" . date('Y-m-d') . "'";
    }

    $WhereClause = MakeSecurityWhereClause($SABSWhere, "SAB", $_SESSION["initials"]);

    $sql = "SELECT count(*) as sabs_count FROM sabs_main";

    if ($WhereClause != "")
    {
        $sql .= " WHERE " . $WhereClause;
    }

    $sabCount = \DatixDBQuery::PDO_fetch($sql, array(), \PDO::FETCH_COLUMN);

    if($sabCount)
    {
        return $sabCount;
    }

    return 0;
}

function GetSideMenuHTML($Parameters)
{
    global $dtxleft_menu;

    $LeftMenu = new LeftMenu($Parameters);
    $LeftMenu->ExtraLinks = $Parameters['extra_links'];
    $MenuHTML = $LeftMenu->GetHTML();

    if ($Parameters['floating_menu'] !== false && ($Parameters['table'] || $Parameters['panels'] ||
        $Parameters['formarray'] || $Parameters['buttons']))
    {
        $FloatingMenuHTML = $LeftMenu->GetFloatingMenuHTML();
    }

    $dtxleft_menu = $MenuHTML.$FloatingMenuHTML;
}

function GetSubMenuHTML($formID)
{
    global $NoMainMenu, $scripturl, $ModuleDefs;

    $SubMenuItems = array();

    // If DIF_NOMAINMENU is set, do not display the main menu
    // if not logged in only.
    // If $NoMainMenu or "print" are set, do not display the main menu at all.
    if ($NoMainMenu || (bYN(GetParm("DIF_NOMAINMENU", "N")) && !$_SESSION["logged_in"]) || $_GET["print"] == 1)
    {
        return '';
    }
    else
    {
        if ($_SESSION["logged_in"])
        {
            //set up sub-menu for logged in screens
            foreach (array('TOD', 'DAS', 'ACT', 'CON', 'ORG', 'AST', 'DST', 'LIB', 'HOT', 'HSA', 'MED', 'POL', 'PAY', 'ADM') as $module)
            {
                if (ModIsLicensed($module) && CanSeeModule($module))
                {
                    if ($module == 'DAS')
                    {
                        $SubMenuItems[] = '<a href="javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=dashboard\');}" tabindex="'.getTabIndexValue().'">'._tk('my_dashboard').'</a>';
                    }
                    elseif ($module == 'TOD')
                    {
                    	if(ModIsLicensed('ACT') || ModIsLicensed('INC') || ModIsLicensed('COM') || ModIsLicensed('RAM'))
                    	{
                        	$SubMenuItems[] = '<a href="javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=list&module=TOD\');}" tabindex="'.getTabIndexValue().'">'._t($ModuleDefs[$module]['NAME']).'</a>';
                    	}
                    }
                    else
                    {
                        $SubMenuItems[] = '<a href="javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=home&module='.$module.'\');}" tabindex="'.getTabIndexValue().'">'._t($ModuleDefs[$module]['NAME']).'</a>';
                    }
                }
            }

            $SubMenuItems[] = '<a href="#" onclick="if(CheckChange()){SendTo(\''.$scripturl.'?action=logout\');}" tabindex="'.getTabIndexValue().'">'._tk('top_menu_logout').'</a>';
        }
        else
        {
            if (!bYN(GetParm("DIF_NO_OPEN")))
            {
                $NewFormLink = $scripturl.(isset($formID) ? '?form_id=' . $formID : '');

                if (isset($_REQUEST["module"]))
                {
                    if (isset($formID))
                    {
                        $NewFormLink .= '&amp;module=' . htmlspecialchars($_REQUEST["module"]);
                    }
                    else
                    {
                        $NewFormLink .= '?module=' . htmlspecialchars($_REQUEST["module"]);
                    }
                }

                $SubMenuItems[] = '<a href="'.$NewFormLink.'" tabindex="'.getTabIndexValue().'">'._tk('top_menu_new_form').'</a>';
            }

            $LoginLink = $scripturl.'?action=login'.(isset($formID) ? '&amp;form_id=' . $formID : '');
            $SubMenuItems[] = '<a href="'.$LoginLink.'" tabindex="'.getTabIndexValue().'">'._tk('top_menu_login').'</a>';

            if (bYN(GetParm("DIF_2_REGISTER")))
            {
                $RegisterLink = $scripturl.'?action=register'.(isset($formID) ? '&amp;form_id=' . $formID : '');
                $SubMenuItems[] = '<a href="'.$RegisterLink.'" tabindex="'.getTabIndexValue().'">'._tk('top_menu_register').'</a>';
            }

        }
    }

    if (!empty($SubMenuItems))
    {
        return '<ul><li>'.implode('</li><li>', $SubMenuItems).'</li></ul>';
    }

    return '';
}

/**
* @desc ensures that client-defined logo is pushed into the proper global and wrapped in <img> tags.
*/
function SetLogos()
{
    if (!$GLOBALS["logo_topright"])
    {
        //default datix image
        $GLOBALS["dtxlogo_topright"] = '<img class="colour-logo" src="images/datix-logo.gif" alt="Datix Logo" /><img class="grey-logo" src="images/datix_gray-logo.gif" alt="Datix Logo" style="display:none;" />';
    }
    else
    {
        $GLOBALS["dtxlogo_topright"] = '<img alt="" src="'.$GLOBALS['logo_topright'].'" />';
    }
}

/**
* @desc Used to send the script process to a url before anything has been output.
*/
function redirectexit()
{
    global $yySetLocation;

    if (bYN(GetParm('CSRF_PREVENTION','N'))) {
        // add CSRF prevention token to url
        $query = parse_url($yySetLocation, PHP_URL_QUERY);
        $yySetLocation .= ($query ? '&' : '?').'token='.\CSRFGuard::getCurrentToken();
    }

    header('Location: ' . Sanitize::SanitizeURL(str_replace(' ', '%20', $yySetLocation)));
    obExit();
}

function obExit()
{
    \src\logger\DatixLogger::logScriptExecutionLength();
    
    if ($GLOBALS['profiler'] instanceof src\framework\profiler\Profiler)
    {
        echo $GLOBALS['profiler']->stopAndPublish();
    }
    //symfony changes by CA
    exit();
}

/**
* @desc Maps a code to a description for a given coded field.
*
* @global array $FieldDefs
*
* @param string $module The module that the field belongs to.
* @param string $field The name of the coded field.
* @param string $value The code in question.
* @param string $table The table that the field is in (if required).
*
* @return string The description of the code.
*/
function code_descr($module, $field, $value, $table = "", $EscapeData = true)
{
    global $FieldDefs;

    if ($value == "")
    {
        return "";
    }

    if ($FieldDefs[$module][$field]['Type'] == 'yesno')
    {
        if ($value=='Y')
        {
            return 'Yes';
        }

        if ($value=='N')
        {
            return 'No';
        }
    }

    if ($_SESSION['CachedValues']['cod_descr'][$module][$field][$value][($table ? $table : 'T')] &&
        !$GLOBALS['devNoCache'])
    {
        $CodeDescr = $_SESSION['CachedValues']['cod_descr'][$module][$field][$value][($table ? $table : 'T')];
    }
    else
    {
        if (is_array($FieldDefs[$module][$field]['fformat']))
        {
            // the format is stored in FieldDefs, rather than the DB, which allows you to change fformats on-the-fly
            $row = $FieldDefs[$module][$field]['fformat'];
        }
        else
        {
            $row = getFieldFormat($field, $module, $table);
        }

        $code_table = $row["fmt_code_table"];
        $code_where = \UnicodeString::rtrim($row["fmt_code_where"]);

        if ($FieldDefs[$module][$field]['FieldList'])
        {
            // a custom field type where the codes are fields themselves
            $row["description"] = Labels_FieldLabel::GetFieldLabel($value);
        }
        elseif ($code_table{0} == '!')
        {
            $code_type = \UnicodeString::substr($code_table, 1);

            $sql = "
                SELECT
                    cod_descr AS description
                FROM
                    code_types
                WHERE
                    cod_code = :val
                    AND
                    cod_type = '$code_type'
            ";

            $code_ok = true;
        }
        elseif (\UnicodeString::rtrim($row["fmt_code_descr"]) != "" && \UnicodeString::rtrim($code_table) != "" &&
            \UnicodeString::rtrim($row["fmt_code_field"]) != "")
        {
            $sql = "SELECT";

            if ($code_table == "staff" || $code_table == "vw_staff_combos")
            {
                //Need to set as staff table even though already set as vw_staff_combo,
                //since vw_staff_combos does not include closed or excluded contacts, but we still need
                //to retieve a contacts name.
                $code_table = "staff";

                if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
                {
                    $coalesce_sql = "
                        coalesce(sta_surname, '') + ', ' + coalesce(sta_title, '')  + ' ' + coalesce(sta_forenames, '')
                    ";
                }
                else
                {
                    $coalesce_sql = "
                        coalesce(sta_title, '') + ' ' + coalesce(sta_forenames, '')  + ' ' + coalesce(sta_surname, '')
                    ";
                }

                if (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'A')
                {
                    $coalesce_sql .= " + ' - ' + coalesce(jobtitle, '')";
                }
                elseif (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'B')
                {
                    $coalesce_sql = " coalesce(jobtitle, '') + ' - ' + " . $coalesce_sql;
                }

                $sql .= $coalesce_sql . " AS description";

            }
            else
            {
                $sql .= " $row[fmt_code_descr] AS description";
            }

            $sql .= " FROM $code_table WHERE $row[fmt_code_field] = :val";

            if (!empty($code_where))
            {
                $sql .= " AND " . $code_where;
            }

            $code_ok = true;
        }

        $fmt_field = $row['fmt_field'];
        if ($code_ok)
        {
            $row = DatixDBQuery::PDO_fetch($sql, array("val" => $value));
            
            // handle relabelled approval statuses
            if( $fmt_field == 'rep_approved' && ( $new_desc = _tk( 'approval_status_'.$module.'_'.$value ) ) )
            {
            	$row['description'] = $new_desc;
            	unset( $new_desc );
        }
        }

        if ($row["description"] == "")
        {
            $CodeDescr = $value;
        }
        else
        {
            $CodeDescr = $row["description"];
        }

        $_SESSION['CachedValues']['cod_descr'][$module][$field][$value][($table ? $table : 'T')] = $CodeDescr;
    }

    return ($EscapeData === true ? Escape::EscapeEntities($CodeDescr): $CodeDescr);
}

/**
* @desc Gets the description of a coded value and the colour it should appear. Caches values to prevent multiple
 * identical queries.
*
* @param string $module The current module
* @param string $field The coded field
* @param string $value The code
*
* @return array An array containing the description and colour of the code.
*/
function get_code_info($module, $field, $value, $table = '')
{
    if ($value == '')
    {
        return '';
    }

    if ($_SESSION['CachedValues']['cod_info'][$module][$table][$field][$value])
    {
        return $_SESSION['CachedValues']['cod_info'][$module][$table][$field][$value];
    }
    else
    {
        $row = getFieldFormat($field, $module, $table);

        $code_table = $row["fmt_code_table"];
        $code_where = $row["fmt_code_where"];

        if ($code_table{0} == '!')
        {
            $code_type = \UnicodeString::substr($code_table, 1);

            $sql = "
                SELECT
                    cod_descr AS description,
                    cod_colour as colour,
                    cod_web_colour,
                    cod_parent,
                    cod_parent2
                FROM
                    code_types
                WHERE
                    cod_code = :value
                    AND
                    cod_type = '$code_type'
            ";

            $code_ok = true;
        }
        elseif ($row["fmt_code_descr"] != "" && $code_table != "" && $row["fmt_code_field"] != "")
        {
            if ($code_table == "vw_staff_combos")
            {
                $code_table = "staff";
                $row['fmt_code_field'] = str_replace('vw_staff_combos', 'staff', $row['fmt_code_field']);
            }

            $sql = "
                SELECT
                    $row[fmt_code_descr] AS description".
                    (bYN($row['fmt_custom_code']) || $code_table == 'staff' ? '' : ',
                    cod_colour as colour,
                    cod_web_colour,
                    cod_parent,
                    cod_parent2')."
                FROM
                    $code_table
                WHERE
                    $row[fmt_code_field] = :value
            ";

            $code_ok = true;

            if (!empty($code_where))
            {
                $sql .= " AND " . $code_where;
            }
        }

        $fmt_field = $row['fmt_field'];
        if ($code_ok)
        {
            $values = explode(' ', $value);

            if (sizeof($values) > 1)
            {
                foreach ($values as $val)
                {
                    $result = PDO_fetch($sql, array('value' => $val));

                    if ($result)
                    {
                        $row['description'].= $result['description'] . ', ';
                    }
                }

                $row['description'] = \UnicodeString::substr($row['description'], 0, -2);
            }
            else
            {
                $row = PDO_fetch($sql, array("value" => $value));
            }
            
            // handle relabelled approval statuses
            if( $fmt_field == 'rep_approved' && ( $new_desc = _tk( 'approval_status_'.$module.'_'.$value ) ) )
            {
            	$row['description'] = $new_desc;
            	unset( $new_desc );
        }
        }

        if ($row == "")
        {
            $row["description"] = $value;
        }

        $_SESSION['CachedValues']['cod_info'][$module][$table][$field][$value] = $row;
    }

    // basic data satinization for output
    $row['cod_parent'] = Sanitize::SanitizeRaw($row['cod_parent']);
    $row['cod_parent2'] = Sanitize::SanitizeRaw($row['cod_parent2']);
    $row['description'] = Sanitize::SanitizeRaw($row['description']);
    $row['colour'] = filter_var( $row['colour'], FILTER_SANITIZE_NUMBER_INT );
    $row['cod_web_colour'] = filter_var( $row['cod_web_colour'], FILTER_SANITIZE_SPECIAL_CHARS );
    return $row;
}

/**
* @desc Gets the maximum value saved in a numeric field in a given table. By default this will look for the
 * recordid field.
*
* @param string $table The table to look in.
* @param string $field The field to check.
*
* @return int The maximum value held in this field accross all the records in the table. -1 if something
 * has gone wrong.
*/
function GetMaxRecordID($table, $field = "recordid")
{
    $sql = "SELECT MAX($field) as maxid FROM $table";
    $row = PDO_fetch($sql);

    return $row["maxid"];
}

/**
* @desc An updateid field acts as a marker to ensure that other people's changes are not overwritten and is
* changed on every update. This function defines the progression of ids by converting one in a sequence to the next
*
* @param string $sUpdateId The old updateid value
*
* @return string The new updateid value to save
*/
function GensUpdateID($sUpdateID)
{
    if ($sUpdateID == "")
    {
        $nHb = 0;
        $nLb = 0;
    }
    else
    {
        $nHb = ord($sUpdateID{0});
        $nLb = ord($sUpdateID{1});
    }

    $nLb = IncUpdateIDChar($nLb);

    if ($nLb == 48)
    {
        $nHb = IncUpdateIDChar($nHb);
    }

    if ($nHb == 0)
    {
        $tmp = chr($nLb);
    }
    else
    {
        $tmp = chr($nHb) . chr($nLb);
    }

    return $tmp;
}

// Only ever called by GensUpdateID() to increment one character of the ID
function IncUpdateIDChar($nUID)
{
    $nUID++;

    if ($nUID < 48)
    {
        $nUID = 49;
    }
    elseif ($nUID > 57 && $nUID < 65)
    {
        $nUID = 65;
    }
    elseif ($nUID > 90 && $nUID < 97)
    {
        $nUID = 97;
    }
    elseif ($nUID > 122)
    {
        $nUID = 48;
    }

    return $nUID;
}

/**
* @desc Retrieves the next recordid from the specified table and has the option to also insert a blank record
 * with this ID.
*
* @param string $table The table in question
* @param bool $insert True if a blank record should be inserted, false otherwise
* @param string $field The primary index (the value that gets returned).
* @param string $createdby The initials of the person who is logged in when this record is created.
*
* @return int The primary index of the new record
*/
function GetNextRecordID($table, $insert = false, $field = "recordid", $createdby = "")
{
    $db = new DatixDBQuery();

    // Create new row in controls table if one does not already exist.
    // The value to be inserted should be the maximum recordid that
    // already exists in the table, if any.
    $sql = "SELECT currentid, updateid FROM controls WHERE tablename = :tablename";
    if (!$row = $db->PDO_fetch($sql, array("tablename" => $table)))
    {
        $sql = "INSERT INTO controls (tablename, currentid)
                VALUES (:tablename, " . (GetMaxRecordID($table, $field) + 1) . ")";
        $db->PDO_query($sql, array("tablename" => $table));
    }

    // get recordid and updateid from control table
    $sql = "SELECT currentid, updateid FROM controls WHERE tablename = :tablename";
    $row = $db->PDO_fetch($sql, array("tablename" => $table));
    $updateid = $row["updateid"];

    // get maximum recordid from actual table
    $maxTableId = GetMaxRecordID($table, $field);

    // ...and use the biggest one
    $nID = max ($row['currentid'], $maxTableId) + 1;

    $newUpdateId = GensUpdateID($updateid);

    // update controls table
    $sql = "UPDATE controls SET currentid = $nID, updateid = '" . $newUpdateId .
            "' WHERE tablename = :tablename AND ";

    if ($updateid == "") {
        $sql = $sql . "(updateid IS NULL OR updateid = '')";
    } else {
        $sql = $sql . "updateid = '$updateid'";
    }

    $db->PDO_query($sql, array("tablename" => $table));


    // insert blank record
    if ($insert)
    {
        $fields = "$field";
        $values = "$nID";

        if ($createdby != "")
        {
            $fields .= ', createdby';
            $values .= ",'$createdby'";
        }

        $sql = "INSERT INTO $table ($fields) VALUES ($values)";

        // do not write to SQLErrors if we're attempting to retry failed inserts unless we've reached our retry limit
        $db->writeErrorsToFile = 1;

        $db->setSQL($sql);
        $db->prepareAndExecute();
    }

    return Sanitize::SanitizeInt($nID);
}

/**
* @desc Takes a date string (seperated by . / or - and converts it into SQL Server format. Also zeros the time.
*
* @param string $datestring The date in a string format
*
* @return string The date in SQL Server format.
*/
function UserDateToSQLDate($datestring)
{
    if ($datestring == '')
    {
        return null;
    }

    if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $datestring) == 1)
    {
        $datestring = $datestring . ' 00:00:00.000';
    }

    if (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}(.\d{3})?/', $datestring) == 1)
    {
        //date is already in the correct format
        return $datestring;
    }


    $US = GetParm('FMT_DATE_WEB') == 'US';

    if ($US)
    {
        list($month, $day, $year) = explode('/', $datestring);

        if ($day == '' || $month == '' || $year == '')
        {
            list($month, $day, $year) = explode('.', $datestring);
        }

        if ($day == '' || $month == '' || $year == '')
        {
            list($month, $day, $year) = explode('-', $datestring);
        }
    }
    else
    {
        list($day, $month, $year) = explode('/', $datestring);

        if ($day == '' || $month == '' || $year == '')
        {
            list($day, $month, $year) = explode('.', $datestring);
        }

        if ($day == '' || $month == '' || $year == '')
        {
            list($day, $month, $year) = explode('-', $datestring);
        }
    }

    if (!is_numeric($day) ||  !is_numeric($month) || !is_numeric($year) || checkdate($month, $day, $year) === false)
    {
        throw new InvalidDataException('Invalid date provided.');
    }

    return "$year-$month-$day 00:00:00.000";
}

/**
 * Converts a string into a Unix timestamp. Wrapper around PHP's strtotime function
 * Will replace slashes in $dateString if the FMT_DATE_WEB global isn't set to US
 *
 * @param string $dateString The date to convert
 * @return int|false
 */
function DateStrToUnixTimestamp($dateString)
{
    $US = (GetParm('FMT_DATE_WEB') == 'US');

    if (!$US)
    {
        $dateString = str_replace('/', '-', $dateString);
    }

    return strtotime($dateString);
}

/**
 * @param $Date Date in sql server format (yyyy-mm-dd hh:mm:ss.nnn). If not provided, takes todays date
 *
 * This function should only be used to convert from a storage format to a display format.
 */
function FormatDateVal($SQLDate, $ReturnTime = false)
{
    if ($SQLDate == '')
    {
        return ''; //date('Y-m-d h:i:s.000')
    }

    if (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}(\.\d{3})?/', $SQLDate) !== 1)
    {
        $Timestamp = strtotime($SQLDate);

        $year = date('Y', $Timestamp);

        //we want to use this but it causes too many errors, so we'll need to replace it iduring development of a future version:
        throw new InvalidDataException('Date provided is not in a valid format');

        //workaround for when we can't match the date format, but it looks valid. TODO: Remove if we've got rid of all exception errors by 12.3 release
/*        if($Timestamp == false && preg_match('/\d{2}\/\d{2}\/\d{4}/', $SQLDate))
        {
            return $SQLDate;
        }*/
        }
    else
    {

        list($date, $time) = explode(" ", $SQLDate);
        list($year, $month, $day) = explode("-", $date);

        // Create a timestamp the system can understand
        // Use 1972 since it is supported by php daet functions (unlike pre-1901 dates)
        $Timestamp = strtotime("1972-$month-$day $time");
    }

    if ($Timestamp === false) //invalid date
    {
        throw new InvalidDataException('Date provided is not valid');
    }

    if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
    {
        $Format = "$year-m-d";
    }
    elseif (GetParm("FMT_DATE_WEB") == 'US')
    {
        $Format = "m/d/$year";
    }
    else
    {
        $Format = "d/m/$year";
    }

    if ($ReturnTime)
    {
        $Format .= ' H:i:s';
    }

    return date($Format, $Timestamp);
}

/**
* @desc Calls the appropriate function (based on db type) to get the number of records affected by the last query.
*
* @return int The number of records affected by the last query
*/
function affected_rows($result = 0)
{
    if ($result)
    {
        return $result->rowCount();
    }
    else
    {
        return 0;
    }
}

function success_message($error, $title = '')
{
    global $yyheaderdone, $dtxtitle;

    if (!$yyheaderdone)
    {
        if ($title)
        {
            $dtxtitle = $title;

            getPageTitleHTML(array(
                 'title' => $title,
                 ));
        }

        template_header_nomenu();
    }

    echo '<div style="padding:10px">'.htmlspecialchars($error).'</div>';

    footer();
    obExit();
}

/**
* @desc Gets the current version number from the db and returns it (also stores it in a global).
* Used to ensure that we don't accidentally change versions while maintaining our session and for comparing
 * versions when migrating.
*
* @return string The version number as <mainversion>.<patchlevel>
*/
function GetVersion()
{
    return '14.0';
}

/**
* @desc called when something goes wrong - interrupts the flow of the script to display a page with
* an error message.
*
* @param string $error The error message to display
* @param string $title The title to give the page.
*
* @codeCoverageIgnoreStart
* No unit test because there is no real testable output.
*/
function fatal_error($error, $title = 'Error', $module = 'ADM', $cssClass = 'error_div')
{
    global $yyheaderdone, $dtxtitle;

    $LoggedIn = (isset($_SESSION["logged_in"]));

    if (!$yyheaderdone)
    {
        $dtxtitle = $title;

        GetPageTitleHTML(array(
            'title' => $dtxtitle,
            'module' => $module
        ));

        if ($LoggedIn)
        {
            GetSideMenuHTML(array('module' => $module, 'error' => true));
            template_header();
        }
        else
        {
            template_header_nomenu();
        }
    }

    if (is_array($error))
    {
        foreach ($error as $value)
        {
            echo '
            <div class="' . $cssClass . '"><b>' . Sanitize::SanitizeRaw($value) . '</b></div>';
        }
    }
    else
    {
        echo '
        <div class="' . $cssClass . '"><b>' . Sanitize::SanitizeRaw($error) . '</b></div>';
    }

    footer();
    obExit();
}
// @codeCoverageIgnoreEnd

/**
* @desc Calls a stored procedure to update the last time a user session was active with the current time.
*
* @return bool True if there is a valid session for this user (or sessions are not used) and false if a valid
 * session was not found.
*/
function SetSessionActive()
{
    if (bYN(GetParm("RECORD_LOCKING"), "N") && isset($_SESSION["logged_in"]))
    {
		if (empty($_SESSION["session_id"]))
		{
			return false;
		}

        $sql = 'SELECT count(*) as total FROM SESSIONS WHERE RECORDID = :recordid';
        $result = DatixDBQuery::PDO_fetch($sql, array(':recordid' => $_SESSION["session_id"]));
        $ValidSession = ($result['total'] > 0);

        DatixDBQuery::CallStoredProcedure(array(
            "procedure" => "SetSessionActive",
            "parameters" => array(
                array("@session_id", $_SESSION["session_id"], SQLINT4),
                array("@lock_id", null, SQLINT4)
            )
        ));

		return $ValidSession;
    }

    return true;
}

/**
* @desc Updates the current session if sessions are used and then redirects the user to the login page if their
 * session has expired
* (or doesn't exist) or if they are not logged in. Used to protect internal pages from non-logged-in users.
*/
function LoggedIn()
{
    global $scripturl;

    $ValidSession = SetSessionActive();

    // Goes to the login page if the user is not logged in
    // or the user is logged in but has gone to a different URL!
    if ((!isset($_SESSION["logged_in"]) || strtolower($_SESSION["scripturl"]) != strtolower($scripturl))
        || (bYN(GetParm("RECORD_LOCKING"), "N") && !$ValidSession))
    {
        // log reason for automatic logout to assist diagnosis of instances where this occurs in error
        if (!isset($_SESSION["logged_in"]))
        {
            $reason = 'No session found';
        }
        else if (strtolower($_SESSION["scripturl"]) != strtolower($scripturl))
        {
            $reason = 'Session URL "'.strtolower($_SESSION["scripturl"]).'" does not match actual URL "'.strtolower($scripturl).'"';
        }
        else if (bYN(GetParm("RECORD_LOCKING"), "N") && !$ValidSession)
        {
            $reason = 'No Datix session found';
        }
        
        Registry::getInstance()->getLogger()->logDebug('Automatic logout. Reason: '.$reason);
        
        $loader = new \src\framework\controller\Loader();

        $controller = $loader->getController(
            array('controller' => 'src\\login\\controllers\\LoginTemplateController')
        );
        echo $controller->doAction('login');
        obExit();
    }
}

function FixColour($OldColour)
{
    $HexColour = dechex($OldColour);
    $NewColour = str_pad($HexColour, 6, "0", STR_PAD_LEFT);

    $part1 = substr($NewColour, 0, 2);
    $part2 = substr($NewColour, 2, 2);
    $part3 = substr($NewColour, 4, 2);

    $NewColour = $part3 . $part2 . $part1;

    return $NewColour;
}

/**
* @desc Turns a space-separated string into a comma-separated list of quoted strings suitable for use as the IN
 * part of a WHERE clause
* Can possibly be refactored out as is only used once.
*
* @param string $Values Could be an array or a string - contains values to be concatenated.
*
* @return string Comma seperated string of quoted values.
*/
function MakeInClause($Values)
{
    if (!$Values)
    {
        return "";
    }

    if (!is_array($Values))
    {
        $ValueArray = explode(' ', $Values);
    }
    else
    {
        $ValueArray = $Values;
    }

    foreach ($ValueArray as $Val)
    {
        if ($Val)
        {
            $SepArray[] = "'$Val'";
        }
    }

    $NewString = implode(',', $SepArray);

    return $NewString;
}

/**
* @desc Takes a post variable name and sets a global parameter (in the globals table) from it.
*
* @param string $PostName The name of the parameter to be set (also defines the name of the $_POST variable
 * to retrieve it from)
*/
function SetGlobalFromPost($PostName)
{
    $PostValue = $_POST[$PostName];

    // Special case for array - space separate it
    if (is_array($PostValue))
    {
        $PostValue = implode(" ", $PostValue);
    }

    // This is a checkbox
    if ($PostValue == "on")
    {
        $PostValue = "Y";
    }

    SetGlobal(Sanitize::SanitizeRaw($PostName), Sanitize::SanitizeRaw($PostValue));
}

/**
* @desc Called from ajax.
* Collects $_GET values and uses them to call {@link SetGlobal} or {@link SetUserParm()} depending on whether
 * we are setting user or global parameters.
*
* @codeCoverageIgnoreStart
* No unit test, since this function just covers two other functions which already have unit tests.
*/
function SetGlobalAJAX()
{
    if ($_GET['user'])
    {
        SetUserParm($_GET['user'], $_GET['global'], $_GET['value']);
    }
    else
    {
        SetGlobal($_GET['global'], $_GET['value']);

        if ($_GET['update_session'] == 1)
        {
            $_SESSION["Globals"][$_GET['global']] = $_GET['value'];

            if (!isset($_SESSION['GlobalObj']))
            {
                System_Globals::InitialiseSessionObject();
            }

            $_SESSION['GlobalObj']->setGlobalValue($_GET['global'], $_GET['value']);
        }
    }
}

/**
* @desc Called from ajax.
* Collects $_GET values and uses them to set SESSION variables.
*
* @codeCoverageIgnoreStart
*/
function SetSessionAJAX()
{
    // CB 2010-11-02 - we need to be very careful which variables we configure this way
    // to prevent malicious users manipulating the session
    $whitelist = array('FlashAvailable', 'Timezone');

    if (in_array($_GET['var'], $whitelist))
    {
        if ($_GET['bool'] == 'true')
        {
            $_SESSION[$_GET['var']] = (bool)$_GET['value'];
        }
        else
        {
            $_SESSION[$_GET['var']] = $_GET['value'];
        }
    }
}
// @codeCoverageIgnoreEnd

/**
* @desc Takes a global/value pair and sets them in the globals table, removing any previous setting.
*
* @param string $sGlobalName The name of the parameter to be set
* @param string $sGlobalValue The value of the global
* @param bool $UpdateSession If true, the session will be updated (so the user doesn't need to log out), useful
 * when setting globals automatically
*/
function SetGlobal($sGlobalName, $sGlobalValue, $UpdateSession = false)
{
    if ($sGlobalName != "")
    {
        // Delete the parameter.
        $sql = "DELETE FROM globals WHERE parameter = :parameter";
        PDO_query($sql, array("parameter" => $sGlobalName));

        if (is_array($sGlobalValue))
        {
            $sGlobalValue = implode(" ", $sGlobalValue);
        }

        $sql = "INSERT INTO globals
            (parameter, parmvalue)
            VALUES
            (:parameter, :paramvalue)";

        PDO_query($sql, array("parameter" => $sGlobalName, "paramvalue" => $sGlobalValue));

        if ($UpdateSession)
        {
            $_SESSION['Globals'][$sGlobalName] = $sGlobalValue;
        }
    }
}

/**
* @desc Adds a user parameter to the database
*
* @param string $login The login of the user to whom this parameter is attached
* @param string $parm The name of the parameter being changed
* @param string $parmvalue The value of the parameter to save
*/
function SetUserParm($login, $parm, $parmvalue)
{
    if ($parmvalue === null)
    {
        // the field has been hidden on the form, so don't touch the data
        return;
    }
    
    // Delete any existing parameter value.
    $sql = "DELETE FROM user_parms
        WHERE (parameter = :parameter
        OR parameter LIKE :paramlike)
        AND login = :login";
    PDO_query($sql, array("parameter" => $parm, "paramlike" => $parm . ":%", "login" => $login));

    if ($parmvalue == "")
    {
        //rather than saving a blank value, we can just leave the table empty
        return;
    }

    if ($parmvalue == "on")
    {
        $parmvalue = "Y";
    }

    // Need to cope with parameters where there are more than 254
    // characters.  To do this, split into chunks of 254 and then
    // call them PARM, PARM:1, PARM:2, etc.
    // Not sure whether this is used, so no unit test yet: @codeCoverageIgnoreStart
    $Parms = chunk_split($parmvalue, 254, '|');
    $ParmArray = explode('|', $Parms);

    $i = 1;
    $parmname = $parm;

    foreach ($ParmArray as $Parmpart)
    {
        if ($Parmpart != "")
        {
            $sql = "INSERT INTO user_parms
            (login, parameter, parmvalue)
            VALUES
            (:login, :parameter, :parmvalue)";

            PDO_query($sql, array("login" => $login, "parameter" => $parmname, "parmvalue" => $Parmpart ));
        }

        $parmname = $parm . ":$i";
        $i++;
    }
    //@codeCoverageIgnoreEnd
}

function GetParms($username = "", $setSessionGlobals = true)
{
    global $AccessLvlDefs;
    // User parameters override global parameters,

    // Get global parameters
    $sql = "SELECT parameter, parmvalue as [value] FROM globals";
    $globals = PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);

    if ($username)
    {
        //Get profile parameters
        $sql = '
            SELECT lpp_parameter as parameter, lpp_value as [value]
            FROM link_profile_param, contacts_main
            WHERE link_profile_param.lpp_profile = contacts_main.sta_profile
            AND contacts_main.login = :username';
        $result = PDO_fetch_all($sql, array('username' => $username));

        foreach ($result as $glob)
        {
            $globals[$glob["parameter"]] = $glob["value"];
        }

        // Get user parameters
        $sql = 'SELECT parameter, parmvalue as [value] FROM user_parms WHERE login = :username';
        $result = PDO_fetch_all($sql, array('username' => $username));

        foreach ($result as $glob)
        {
            $globals[$glob["parameter"]] = $glob["value"];
        }
    }

    // if $setSessionGlobals is set (which the default)
    // then also initialise $_SESSION["Globals"]
    // usefull to only get the settings for a given user without setting the session
    if ($setSessionGlobals)
    {
        // Global parameters also includes the access levels of the currently logged on user
        // get hold of those before resetting the globals array
        if (isset($_SESSION["Globals"]))
        {
            $UserAccessLvls = array();
            $AccessLvls = array_keys($AccessLvlDefs);

            foreach ($AccessLvls as $AccessLvl)
            {
                if (array_key_exists($AccessLvl, $_SESSION["Globals"]) && isset($_SESSION["Globals"][$AccessLvl]))
                {
                    $UserAccessLvls[$AccessLvl] = $_SESSION["Globals"][$AccessLvl];
                }
            }

            unset($_SESSION["Globals"]);
        }

        $_SESSION["Globals"] = $globals;

        if (is_array($UserAccessLvls) && !empty($UserAccessLvls))
        {
            $_SESSION["Globals"] = array_merge($_SESSION["Globals"], $UserAccessLvls);
        }

        System_Globals::InitialiseSessionObject();
    }

    return $globals;
}

function FetchUserParm($login, $parm)
{
    $sql = "SELECT parmvalue FROM user_parms
        WHERE (parameter = :parameter
        OR parameter LIKE :paramlike)
        AND login = :login
        ORDER BY parameter ASC";

    $result = PDO_fetch_all($sql, array("parameter" => $parm, "paramlike" => $parm . ":%", "login" => $login));

    foreach ($result as $row)
    {
        $ParmValue .= $row["parmvalue"];
    }

    return $ParmValue;
}

define("SUB_SETUP", 15);
define("SUB_WHERE", 16);

/**
* @desc Gets a list of module codes, keyed by the ids (contrast with {@link GetModuleCodeIdList()}).
 * Includes a caching system to prevent huge numbers of queries.
*
* @return array Array of module codes.
*/
function GetModuleIdCodeList()
{
    if ($_SESSION["ModuleCache"])
    {
        $Modules = array_flip($_SESSION["ModuleCache"]);  // need to flip because modulecache stores mod_code => mod_id
    }
    else
    {
        // Get list of module codes-id: array('mod_id' => 'mod_code')
        $sql = 'SELECT mod_id, mod_module FROM modules UNION SELECT ' . MOD_ADMIN . ' , \'ADM\'';
        $Modules = PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);
        $_SESSION["ModuleCache"] = array_flip($Modules);
    }

    return $Modules;
}

/**
* @desc Saves an audit of any sql run (excluding the sql used to perfom this query audit)
*
* @param string $query The sql text to audit.
* @param string $parameters Any PDO parameters passed along with the query
*/
function LogQuery($query, $parameters = '')
{
    global $PDO, $Database, $ServerName, $UserName, $Password;

    if (empty($PDO))
    {
        $PDO = new PDO('mssql:dbname='.$Database.';host='.$ServerName, $UserName, $Password);
    }

    $sql = '
        INSERT INTO
            QUERY_LOG
            (login, timestamp, query, parameters)
        VALUES
            (:login, :timestamp, :query, :parameters)
    ';

    $Statement = $PDO->prepare($sql);
    $Statement->execute(array(
        'login' => $_SESSION['login'],
        'timestamp' => time(),
        'query' => $query,
        'parameters' => $parameters
    ));
}

/**
 * @deprecated Use DatixDBQuery instead
 */
function db_query($sql, $suppress_errors = false)
{
    //we need to use PDO here to prevent transaction conflicts between the two connection methods.
    $sql = Sanitize::SanitizeRaw($sql);
    $query = new DatixDBQuery($sql);
    $query->prepare();
    $result = $query->execute(array(), $suppress_errors);
    $PDOStatement = $query->PDOStatement;
    unset($query);

    //sometimes we want to return bool, and sometimes a result set - we should clean up these parts
    // of the code as we find them

    if ($result)
    {
        return $PDOStatement;
    }
    else
    {
        return $result;
    }
}

/**
* @desc Called when a user accesses the error log file - resets the "new errors" counter to zero and the
 * "last checked" global to
* the current timestamp to reset the message on the main menu.
*/
function ErrorLogRead()
{
    SetGlobal('UNREAD_ERRORS', '0');
    SetGlobal('ERROR_LOG_LAST_CHECKED', time());
}

/**
* @desc Interprets the SQLErrors.xml file using xsl and returns an HTML formatted error report.
*
* @param array $aParams Array of parameters
* @param array $aParams[applyxslt] xsl file to use when interpreting xml
* @param array $Errors Reference to variable to store any internal errors that occur.
*
* @return string HTML code for error report.
*/
function GetErrorLogContents($aParams, &$Errors)
{
    global $ClientFolder;

    if (file_exists($ClientFolder . "/SQLErrors.xml"))
    {
        $xml = new DOMDocument;
        $xml->load($ClientFolder . "/SQLErrors.xml");

        if ($aParams['applyxslt'])
        {
            if (!extension_loaded('xsl'))
            {
                $Errors[] = "There was an error initialising xslt transformations. Please contact Datix support to check that your php.ini file is correctly set up.";
            }
            else
            {
                $xsl = new DOMDocument;
                $xsl->load("xsl_files/errorlog.xsl");

                // Configure the transformer
                $oProcessor = new XSLTProcessor;
                $oProcessor->importStyleSheet($xsl); // attach the xsl rules

                $XSLResult = $oProcessor->transformToXML($xml);
            }

            return $XSLResult;
        }
        else
        {
            return $xml->saveXML();
        }
    }

    return '';
}

/**
* @desc Coverts the current $_GET array of url parameters into a string that can be used in a hyperlink or redirect.
*
* @return string String of the form key=val&key2=val2&....
*/
function getGetString($exclude = array())
{
    if (!empty($_GET))
    {
        foreach ($_GET as $key => $val)
        {
            if (!in_array($key, $exclude))
            {
                $aGetString[] = Sanitize::SanitizeString($key)."=".Sanitize::SanitizeString($val);
            }
        }

        return implode("&",$aGetString);
    }
}

/**
* @desc saves an xml error report to SQLErrors.xml containing all information passed in the parameters.
*
* @param array $aParameters Array of parameters
* @param string $aParameters["message"] The message returned by SQL Server along with the error code.
* @param string $aParameters["details"] Any error details added by datix.
* @param string $aParameters["sql"] The SQL that was run.
*/
function logErrorReport($aParameters)
{
    global $ClientFolder;

    if (file_exists($ClientFolder."/SQLErrors.xml"))
    {
        $XMLDoc = new DOMDocument();
        $XMLDoc->load($ClientFolder."/SQLErrors.xml");
    }
    else //we need to create the header rows for a new error file
    {
        $BasicXML = getErrorXMLHeaders();

        $BasicXML .= '<errors></errors>';

        $XMLDoc = new DOMDocument();
        $XMLDoc->loadXML($BasicXML);
    }

    $NewErrorXML = $XMLDoc->createElement('error');
    $NewErrorXML->appendChild($XMLDoc->createElement('date', FormatDateVal(date("Y-m-d H:i:s.000"))));
    $NewErrorXML->appendChild($XMLDoc->createElement('time', date("H:i:s")));
    $NewErrorXML->appendChild($XMLDoc->createElement('message', htmlfriendly($aParameters["message"])));
    $NewErrorXML->appendChild($XMLDoc->createElement('details', htmlfriendly($aParameters["details"])));
    $NewErrorXML->appendChild($XMLDoc->createElement('sql', htmlfriendly($aParameters["sql"])));
    $NewErrorXML->appendChild($XMLDoc->createElement('contact_id', $_SESSION['contact_login_id']));
    $NewErrorXML->appendChild($XMLDoc->createElement('contact', htmlfriendly($_SESSION["fullname"])));
    $NewErrorXML->appendChild($XMLDoc->createElement('contact_initials', htmlfriendly($_SESSION["initials"])));
    $NewErrorXML->appendChild($XMLDoc->createElement('page', htmlfriendly($_SERVER['PHP_SELF']."?".getGetString())));

    $XMLDoc->getElementsByTagName('errors')->item(0)->appendChild($NewErrorXML); //add child to "errors" tag.

    $XMLDoc->save($ClientFolder."/SQLErrors.xml");

    //notifies administrators that an error has been logged.
    SetGlobal('UNREAD_ERRORS', '1');
}

function getErrorXMLHeaders()
{
    $ErrorStr = '<!DOCTYPE errors [';
    $ErrorStr .= '<!ELEMENT date (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT time (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT message (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT details (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT sql (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT contact_id (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT contact (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT contact_initials (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT page (#PCDATA)>';
    $ErrorStr .= '<!ELEMENT error ((date, time, message, details?, sql, contact_id, contact, contact_initials, page))>';

    $ErrorStr .= GetEntityCodes();

    $ErrorStr .= ']>';

    return $ErrorStr;
}

/**
 * @deprecated Use DatixDBQuery instead
 */
function db_fetch_array($result, $result_type = MSSQL_BOTH)
// $result_type:
// MSSQL_ASSOC - stores the data in the associative indices of the result array
// MSSQL_NUM - stores the data in the numeric indices of the result array
// MSSQL_BOTH - stores the data both in the numeric and the associative indices of the result array
{
    global $dbtype;

    if (gettype($result) == 'resource') //old mssql query result - used by stored procedures
    {
        $result_array = @mssql_fetch_array($result, $result_type);
        // Need to strip off trailing characters because of
        // broken behaviour in PHP > 4.3.3
        if ($result_array)
        {
            $return_array = array_map("rtrim", $result_array);
        }


        return $return_array;
    }
    else //new pdo connection
    {
        $result_array = $result->fetch();

        if ($dbtype == "mssql" && $result_array)
        {
            $result_array = array_map("rtrim", $result_array);
        }

        return $result_array;
    }
}

// Subtracts $Days days from $Date and returns the new date as a Date object.
function SubtractDays($Days, $Date = "")
{
    require_once 'Date.php';

    $Span = new Date_Span("$Days", "%d");

    if ($Date == "")
    {
        $MyDate = new Date;
    }
    else
    {
        $MyDate = $Date;
    }

    $MyDate->subtractSpan($Span);

    return $MyDate;
}

/**
 * Retrieves the label for a listing column header
 * Legacy wrapper function which should be remove once all places are changed to use the function call below instead.
 * @deprecated Replace with Labels_FieldLabel::GetFieldLabel
 */
function GetColumnLabel($FieldName, $DefaultTitle = "", $FullName = false, $table = '', $module = '')
{
    return Labels_FieldLabel::GetFieldLabel($FieldName, $DefaultTitle, $table, $module, $FullName);
}

/**
 * Retrieves the label for a field
 * Legacy wrapper function which should be remove once all places are changed to use the function call below instead.
 * @deprecated Replace with Labels_FieldLabel::GetFieldLabel
 */
function GetFieldLabel($FieldName, $DefaultTitle = "")
{
    return Labels_FieldLabel::GetFieldLabel($FieldName, $DefaultTitle);
}

/**
 * Legacy wrapper function which should be remove once all places are changed to use the function call below instead.
 * @deprecated Replace with Labels_FormLabel::GetFormFieldLabel
 */
function GetFormFieldLabel($FieldName, $DefaultTitle = "", $module = '', $FormFieldName = '', $table = '')
{
   return Labels_FormLabel::GetFormFieldLabel($FieldName, $DefaultTitle, $module, $FormFieldName, $table);
}

function GetStatusesNoMandatory($aParams, $multiPerms = false)
{
    $queryFactory = new src\framework\query\QueryFactory();
    $query = $queryFactory->getQuery();
    $query->select(['approval_action.apac_to'])
        ->from('approval_action');

    $fieldCollection = new src\framework\query\FieldCollection();
    $fieldCollection->field('approval_action.apac_from')->eq($aParams['from'] ? $aParams['from'] : 'NEW')
                    ->field('approval_action.module')->eq($aParams['module'])
        ->field('approval_action.apac_ignore_mandatory')->eq('Y')
        ->field('approval_action.apac_workflow')->eq(GetWorkflowID($aParams['module']));

    if($multiPerms)
    {
        $fieldCollection->field('approval_action.access_level')->in($aParams['level']);
    }
    else
    {
        $fieldCollection->field('approval_action.access_level')->eq($aParams['level']);
}

    $where = new src\framework\query\Where();
    $where->add($fieldCollection);
    $query->where($where);

    list($sql, $parameters) = $queryFactory->getSqlWriter()->writeStatement($query);
    $db = new \DatixDBQuery();
    $db->setSQL($sql);
    $db->prepareAndExecute($parameters);
    $results = $db->fetchAll();

    return $results[0];
    }

/**
 * Removes the last bit of an UDF formatted field name, if numeric (ie: "UDF_3_5" returns "UDF_3")
 * @param string $FieldName
 * @return string  
 */
function RemoveSuffix($FieldName)
{
    return preg_replace ('/_\d+$/', '', $FieldName);
}

function RemoveSuffixUDF($FieldName)
{
    $NameArray = explode('_', $FieldName);

    if (count($NameArray) == 5)
    {
        unset($NameArray[4]);
    }

    return implode('_', $NameArray);
}

/**
* @desc Sets up javascript function to enforce client-side mandatory field validation.
*
* @param string $module The current module
* @param obj $FormDesign The Form Design object to refer to.
*
* @return string Javascript code to be inserted into the page.
*/
function MakeJavaScriptValidation($module = '', $FormDesign = '', $FieldNames = array())
{
    global $MandatoryFields, $formlevel, $FieldDefs, $ModuleDefs;

    $recordid = is_numeric($_GET['recordid']) ? Sanitize::SanitizeInt($_GET['recordid']) : "";

    if ($FormDesign)
    {
        $MandatoryFields = $FormDesign->MandatoryFields;
    }

    if (is_array($MandatoryFields))
    {
        foreach ($MandatoryFields as $Name => $Div)
        {
            $fieldModule = getModuleFromField($Name);

            if ($fieldModule && is_array($FieldDefs[$fieldModule][$Name]) &&
                $FieldDefs[$fieldModule][$Name]['MandatoryField'])
            {
                $MandatoryName = $FieldDefs[$fieldModule][$Name]['MandatoryField'];
            }
            else
            {
                $MandatoryName = $Name;
            }

            $SectionSuffixArray = array('contact_reporter'=>3, 'assailant'=>4, 'police_officer'=>5);

            if (isset($SectionSuffixArray[$Div]))
            {
                $FormFieldName = $Name;
                $Name = RemoveSuffix($Name);
                $Suffix = $SectionSuffixArray[$Div];
            }

            $FieldName = ($FormDesign->UserLabels[$Name] ? $FormDesign->UserLabels[$Name] :
                GetFormFieldLabel($Name, '', $fieldModule, $FormFieldName, $ModuleDefs[$fieldModule]['TABLE']));

            if ((($Name == 'inc_mgr' && !$FormDesign->FieldLabels['inc_mgr']) ||
                ($Name == 'com_mgr' && !$FormDesign->FieldLabels['com_mgr'] ) ||
                ($Name == 'pal_handler' && !$FormDesign->FieldLabels['pal_handler'] )) && !$_SESSION['logged_in'])
            {
                //hack because we have no way at the moment of dealing with a field that
                //appears in two places with different names.
                $FieldName = 'Your Manager';
            }

            if (!$FieldName)
            {
                $FieldName = $FieldNames[$Name];
            }

            $Function .= 'mandatoryArray.push(new Array("'.$MandatoryName.'","'.$Div.'","'.htmlspecialchars($FieldName).'"));
            ';

            //Special case for reasons for rejection section:
            //Normally mandatory fields are ignored when saving rejected incidents, but in this case they need to be enforced.
            //If we can find a non-hard-coded way of setting this up, we should do it.
            if (in_array($Name, array('rea_code', 'rea_text')))
            {
                $Function .= 'mandatoryArraySpecial[\'REJECT\'].push(new Array("'.$MandatoryName.'","'.$Div.'","'.GetFormFieldLabel($Name, '', $fieldModule, $FormFieldName).'"));
                ';
            }
        }
    }

    $Function .= '
function validateOnSubmit() {
    var elem;
    var errs = 0;

    if (!submitClicked)
    {
        return false;
    }

    checkDatixSelects();

    if(document.forms[0].rbWhat)
    {
        if (document.forms[0].rbWhat.value == "Cancel") return true;
        if (document.forms[0].rbWhat.value == "BackToListing") return true;
        if (document.forms[0].rbWhat.value == 16) return true;
        if (document.forms[0].rbWhat.value == "Delete") return true;
    }
    ';

    if (in_array($module,array('INC','RAM','PAL','COM','CON','HOT'))) //modules with rep_approved flag
    {
        $Function .= '
        if ($(\'rep_approved\') && $(\'rep_approved\').value && StatusesNoMandatory && $(\'rep_approved\').value in oc(StatusesNoMandatory))
        {
            disableAllButtons();
            displayLoadPopup();

            if (mandatoryArraySpecial[$(\'rep_approved\').value])
            {
                mandatoryArray = mandatoryArraySpecial[$(\'rep_approved\').value];
            }
            else
            {
            ';

            if ($recordid && $module)
            {
                $Function .= '

                if (FeedbackValidationNeeded === true && '.($recordid && $module ? 'true' : 'false').')
                {
                    return DoFeedbackValidation(\''.$module.'\','.$recordid.');
                }
                else
                {
                    return true;
                }';
            }
            else
            {
                $Function .= ' return true;';
            }

        $Function .= '
            }
        }
        ';
    }

       $Function .= '
        var errorFields = new Array();

        mandatoryArray = removeDuplicateMandatoryFields();

        var radios = jQuery(\'[type=radio]\');

        for(var i=0; i< mandatoryArray.length; i++)
        {
            var fieldname  = mandatoryArray[i][0];
            var divName    = mandatoryArray[i][1];
            var fieldEmpty = true;

            if(!jQuery(\'#\'+fieldname))
            {
                fieldEmpty = false;
            }

            var fieldElement = jQuery(\'#\'+fieldname);
            var fieldValue = fieldElement.length ? jQuery.trim(fieldElement.val()) : \'\';

            if(fieldElement.length && fieldElement.is(\'select\') && fieldElement.attr(\'multiple\'))
            {
                fieldEmpty = (jQuery(\'#\'+fieldname+\' option\').size() == 0);
            }
            else if(radios.filter(\'\\\'[name="\'+fieldname+\'"]\\\'\').length > 0)
            {
                // radio buttons are selected by name (not id)
                fieldElement = jQuery(\'input[name=\'+fieldname+\']:checked\');
       			fieldValue = fieldElement.val();

                if (fieldValue !== undefined)
                {
                    fieldEmpty = (fieldValue.length == 0);
            }
            }
            else
            {
                var fieldEmpty = (fieldElement.length && fieldValue=="");
            }

            if (fieldEmpty && $("CURRENT_"+fieldname) && $("CURRENT_"+fieldname).value != "")
            {
                // special case for timestamp textarea fields
                fieldEmpty = false;
            }

            if (!fieldEmpty && jQuery("#err"+fieldname).length)
            {
                jQuery("#err"+fieldname).hide()
            }

            var divVisible = ($(divName) && isVisible($(divName))) || ($(divName+"_contained_in") && jQuery("#" + $(divName+"_contained_in").value)[0].style.display != "none");

            //workaround for sections that are shown/hidden as groups e.g. subjects/issues.
            if(jQuery("#"+divName+"_contained_in").length)
            {
                var divInvisibleButShown = ('.($formlevel == 2 || $formlevel == '' ? 'true' : 'false').' && !isVisible($(jQuery(divName+"_contained_in").val())) && $("show_section_"+jQuery(divName+"_contained_in").val()) && $("show_section_"+jQuery(divName+"_contained_in").val()).value == "1");
            }
            else
            {
                var divInvisibleButShown = ('.($formlevel == 2 || $formlevel == '' ? 'true' : 'false').' && !isVisible($(divName)) && $("show_section_"+divName) && $("show_section_"+divName).value == "1");
            }

            var fieldVisible              = ($("show_field_"+fieldname) && $("show_field_"+fieldname).value == "1");
            var fieldInvisibleButShown    = (!isVisible($(fieldname)) && $("show_field_"+fieldname) && $("show_field_"+fieldname).value == "1");
            var fieldInvisibleButConstant = (!isVisible($(fieldname)) && !$("show_field_"+fieldname));
            var fieldValidateEmpty        = validateEmpty (fieldValue, "err"+fieldname, divName, errs);

            if (
                fieldEmpty && (divVisible || divInvisibleButShown)
                && (fieldVisible || (divInvisibleButShown && (fieldInvisibleButShown || fieldInvisibleButConstant)))
                && (!fieldValidateEmpty)
               )
            {
                if (errs==0) {
                    moveScreenToField(fieldname, divName);
                }
                errs += 1;

                // Make sure that HTML tags doesn\'t affect the alert
                errorFields.push(jQuery("<div/>").html(mandatoryArray[i][2]).text());
            }
        }
        ';

    $Function .= '
    if (errs >= 1)
    {
        var error = txt["alert_message"] + "<br/> <br/>";
        error += errorFields.join("<br />");
        hideLoadPopup();
        enableAllButtons();
        alert(error);
    }
    ';

    $Function .= '
    if (errs == 0) {
    disableAllButtons();
    displayLoadPopup(txt[\'email_merge_wait_msg\']);
    }';

    if ($recordid && $module)
    {
        $Function .= '
        if (FeedbackValidationNeeded === true && (errs == 0))
        {
            return DoFeedbackValidation(\''.$module.'\','.$recordid.');
        }
        ';
    }

    $Function .= '
    return (errs == 0);
    }';

    return $Function;
}

function GetFormsFilename($Module)
{
    global $UserIncidentFormsFile, $ClientFolder, $ModuleDefs;

    if ($UserIncidentFormsFile)
    {
        $FormsFileName = $UserIncidentFormsFile;
    }
    else
    {
        if ($Module == 'DIF1')
        {
            $Module = 'INC';
        }

        if ($ModuleDefs[$Module]['FORM_DESIGN_ARRAY_FILE'])
        {
            $FormsFileName = $ClientFolder . '/'. $ModuleDefs[$Module]['FORM_DESIGN_ARRAY_FILE'];
        }
    }

    return $FormsFileName;
}

function GetMessages($Field)
{
    switch ($Field)
    {
        case 'LoginMessage':
            return GetLoginMessages();
            break;

        case 'UsageAgreementMessage':
            return GetUsageAgreementMessages();
            break;
    }
}

function GetLastLoginMessage()
{
    $pastLoginEvents = DatixDBQuery::PDO_fetch_all('SELECT lau_date, lau_description FROM login_audit WHERE lau_login = :lau_login ORDER BY lau_date DESC', array('lau_login' => $_SESSION['CurrentUser']->login));

    $lastSuccessfulEvent = null;
    $currentLoginEvent = null;
    $i = 0;

    while (empty($lastSuccessfulEvent))
    {
    	// when a user sets password for first time at log in
    	if( !isset( $pastLoginEvents[$i] ) )
    	{
    		break;
    	}
    	
        $event = $pastLoginEvents[$i];

        $date = new \DateTime($event['lau_date']);
        $event['Time'] = $date->format('H:i:s');

        if (preg_match('/WEB (.*): User logged in/', $event['lau_description'], $matches) != 0)
        {
            $event['IP'] = $matches[1];
            if (!$currentLoginEvent)
            {
                $currentLoginEvent = $event;
            }
            else
            {
                $lastSuccessfulEvent = $event;
            }
        }
        else if (preg_match('/WEB (.*): Login failed/', $event['lau_description'], $matches) != 0)
        {
            $event['IP'] = $matches[1];
            $failedEvents[] = $event;
        }

        $i++;
    }

    if ($lastSuccessfulEvent)
    {
    $message = "
Last successful login attempt
Date: ".FormatDateVal($lastSuccessfulEvent['lau_date'])." Time: ".$lastSuccessfulEvent['Time']." IP: ".$lastSuccessfulEvent['IP']."

";
    }
    else
    {
        $message = "
Last successful login attempt
<This is your first successful login attempt>

";
    }

    if (!empty($failedEvents))
    {
        $message .= "Unsuccessful login attempts";

        krsort($failedEvents);
        foreach ($failedEvents as $event)
        {
            $message .= "
Date: ".FormatDateVal($event['lau_date'])." Time: ".$event['Time']." IP: ".$event['IP'];

        }
    }
    else
    {
        $message .= "Unsuccessful login attempts
<There have been no unsuccessful login attempts since your last successful login>";
    }

    $message .= "

Current login
Date: ".FormatDateVal($currentLoginEvent['lau_date'])." Time: ".$currentLoginEvent['Time']." IP: ".$currentLoginEvent['IP'];

    return $message;
}

function GetLoginMessages()
{
    $LoginMessages = array();
    $LoginMessageFile = GetLoginMessageFile();

    if (file_exists($LoginMessageFile))
    {
        include("$LoginMessageFile");
    }
    else
    {
        $LoginMessages = array(_t("You are now being logged in to Datix. If you are not an authorised user, please log out immediately."));
    }

    if (is_array($LoginMessages) && !empty($LoginMessages))
    {
        return $LoginMessages;
    }
    else
    {
        return array($LoginMessage);
    }
}

function GetUsageAgreementMessages()
{
    $LoginMessageFile = GetLoginMessageFile();

    if (file_exists($LoginMessageFile))
    {
        include("$LoginMessageFile");
    }

    if (is_array($UsageAgreementMessages) && !empty($UsageAgreementMessages))
    {
        return $UsageAgreementMessages;
    }
    else
    {
        return array($UsageAgreementMessages);
    }
}

function GetLoginMessageFile()
{
    global $UserLoginMessageFile, $ClientFolder;

    if ($UserLoginMessageFile)
    {
        $LoginMessageFileName = $UserLoginMessageFile;
    }
    else
    {
        $LoginMessageFileName = $ClientFolder . "/UserLoginMessage.php";
    }

    return $LoginMessageFileName;
}

/**
* Function adds escape characters to a string depending on whether they are SQLServer ('') or SQLBase (\').
*
* @param string $InsertUpdate string to be altered.
* @param bool $GPC TRUE by default. Means that the values of php.ini settings magic_quotes_sybase
 * and magic_quotes_gpc are taken
* into account when working out whether to insert extra quotes.  Set it to  FALSE if you are escaping
 * a string which has not come from a GET, POST or cookie.
*
* @return string the $InsertUpdate sting with added quotes.
*/
function EscapeQuotes($InsertUpdate, $GPC = true)
{
    if (ini_get('magic_quotes_sybase') && ini_get('magic_quotes_gpc') && $GPC)
    {
        $Quoted = $InsertUpdate;
    }
    elseif (ini_get('magic_quotes_gpc') && $GPC)
    {
        $Quoted = str_replace("\\'", "''", $InsertUpdate);
        $Quoted = str_replace('\"', '"', $Quoted);
    }
    else
    {
        $Quoted = str_replace("'", "''", $InsertUpdate);
    }

    return $Quoted;
}

/**
* Function adds escape characters to a string in preparation for being written to a file.
*
* @param string $InsertUpdate string to be altered.
*
* @return string the $InsertUpdate sting with added quotes.
*/
function EscapeQuotestoFile($InsertUpdate)
{
    if (ini_get('magic_quotes_sybase') && ini_get('magic_quotes_gpc'))
    {
        $Quoted = str_replace("''", "'", $InsertUpdate);
        $Quoted = str_replace('"', '\"', $Quoted);
    }
    elseif (ini_get('magic_quotes_gpc'))
    {
        $Quoted = str_replace("\'", "'", $InsertUpdate);
    }
    else
    {
        $Quoted = str_replace('"', '\"', $InsertUpdate);
    }

    return $Quoted;
}

/**
* Function adds escape characters to each member of a Posted array depending on whether they are
 * SQLServer ('') or SQLBase (\').
*
* @param array $PostArray array to be altered.
* @param bool $GPC TRUE by default. Means that the values of php.ini settings magic_quotes_sybase
 * and magic_quotes_gpc are taken
* into account when working out whether to insert extra quotes.  Set it to  FALSE if you are escaping
 * a string which has not come from a GET, POST or cookie.
*
* @return array the $PostArray array with added quotes.
*/
function QuotePostArray($PostArray, $GPC = true)
{
    $QuotedArray = array();

    $PostArray = Sanitize::SanitizeRawArray($PostArray);

    foreach ($PostArray as $Name => $Value)
    {
        $QuotedArray[$Name] = EscapeQuotes($Value, $GPC);
    }

    return $QuotedArray;
}

/**
* Strips quotes from a string.  Takes into account the same php.ini settings as {@link EscapeQuotes()}
*
* @param string $InsertUpdate string to be altered.
* @param bool $GPC TRUE by default. Means that the values of php.ini settings magic_quotes_sybase
 * and magic_quotes_gpc are taken
* into account when working out whether to insert extra quotes.  Set it to  FALSE if you are escaping
 * a string which has not come from a GET, POST or cookie.
*
* @return string the $InsertUpdate string with quotes removed.
*/
function StripPostQuotes($InsertUpdate, $GPC = true)
{
    if (ini_get('magic_quotes_sybase') && ini_get('magic_quotes_gpc') && $GPC)
    {
        $Quoted = str_replace("''", "'", $InsertUpdate);
    }
    elseif (ini_get('magic_quotes_gpc') && $GPC)
    {
        $Quoted = str_replace("\\'", "'", $InsertUpdate);
        $Quoted = str_replace('\"', '"', $Quoted);
    }
    else
    {
        $Quoted = $InsertUpdate;
    }

    return $Quoted;
}

/**
* Strips quotes from all members of an array by calling {@link StripPostQuotes()}
*
* @param array $PostArray array to be altered.
*
* @return array the $InsertUpdate array with quotes removed.
*/
function StripPostQuotesArray($PostArray)
{
    foreach ($PostArray as $Name => $Value)
    {
        $QuotedArray["$Name"] = StripPostQuotes($Value);
    }

    return $QuotedArray;
}

/**
* Inserts records into the full_audit table depending on the values of the $_POST variables
*
* @param string $Module Three-letter module code
* @param string $MainTable The table to be audited
* @param string $recordid The recordid of the record to be audited
* @param array $FieldValueArray An array of ($FieldName => $Value)If this array is set, only those $FieldNames
* it contains will be included in the audit.
* @param string $WhereClause If set, is used to identify the record for e.g. link tables.
 * Example: "con_id = 3 AND inc_id = 5"
*
* @return array the $InsertUpdate array with quotes removed.
*/
function DoFullAudit($Module, $MainTable, $recordid, $FieldValueArray = '', $WhereClause = '', &$DateTime = null)
{
    global $ModuleDefs;

    $AuditDate = date('Y-m-d H:i:s');
    $DateTime = $AuditDate;
    // If the $FieldValueArray is set, use this to determine which fields to
    // audit.  This is so we can audit forms where two tables are on the same
    // page, e.g. linked contacts.
    if ($FieldValueArray)
    {
        foreach ($FieldValueArray as $FieldName => $FieldNewValue)
        {
            if ($_POST["CHANGED-$FieldName"] != '')
            {
                $ChangedArray[] = $FieldName;
            }
        }
    }
    else
    {
        // Find fields which have changed.  The "CHANGED-<fieldname>"
        // input field will contain a value if this is the case.
        foreach ($_POST as $Name => $Value)
        {
            $Name = Sanitize::SanitizeString($Name);
            $Value = Sanitize::SanitizeString($Value);

            $Field = explode('-', $Name);

            if ($Field[0] == "CHANGED" && $Value)
            {
                $UDFTest = explode('_', $Field[1]);

                if ($UDFTest[0]=="SelectUDF")
                {
                    if (!$_POST[str_replace("SelectUDF_","UDF_",$Field)])
                    {
                        $_POST[str_replace("SelectUDF_","UDF_",$Field)] = "";
                        $UDFTest[0] = "UDF";
                        $Field[1] = str_replace("SelectUDF_","UDF_",$Field);
                    }
                }

                if ($UDFTest[0]=="UDF")
                {
                    $UDFChangedArray[] = $Field[1];
                }
                else
                {
                    $ChangedArray[] = $Field[1];
                }
            }
        }
    }

    // Now select old values from udf_values
    if ($UDFChangedArray)
    {
        foreach ($UDFChangedArray as $Changed)
        {
            $udfArray = explode("_",$Changed);

            $udftype = $udfArray[1];
            $groupid = $udfArray[2];
            $fieldid = $udfArray[3];

            switch($udftype)
            {
                case "M":
                    $ColName = "UDV_MONEY";
                    break;
                case "D":
                    $ColName = "UDV_DATE";
                    break;
                case "N":
                    $ColName = "UDV_NUMBER";
                    break;
                case "L":
                    $ColName = "UDV_TEXT";
                    break;
                default:
                    $ColName = "UDV_STRING";
                    break;
            }

            $sql = "SELECT $ColName
                FROM UDF_VALUES
                WHERE group_id = :group_id
                AND field_id = :field_id
                AND mod_id = :mod_id
                AND cas_id = :cas_id";
            $sql_module = ($Module == 'ADM' && $MainTable == 'contacts_main') ? 'CON' : $Module;
            $row = PDO_fetch($sql, array(
                "group_id" => $groupid,
                "field_id" => $fieldid,
                "mod_id" => ModuleCodeToID($sql_module),
                "cas_id" => $recordid
            ));

            if (!is_array($row))
            {
                $row[$ColName] = "";
            }

            $sql = "
                INSERT INTO
                    full_audit
                    (aud_module, aud_record, aud_login, aud_date, aud_action, aud_detail)
                VALUES
                    (:aud_module, :aud_record, :aud_login, :aud_date, :aud_action, :aud_detail)
            ";

            PDO_query($sql, array(
                "aud_module" => $Module,
                "aud_record" => $recordid,
                "aud_login" => $_SESSION["initials"],
                "aud_date" => $AuditDate,
                "aud_action" => "WEB:" .$Changed,
                "aud_detail" => $row[$ColName]
            ));
        }
    }

    // Now select old values from main table
    if ($ChangedArray)
    {
        // remove fields from $ChangedArray which are not present in $MainTable
        if (is_array($ModuleDefs[$Module]['FIELD_ARRAY']) && empty($FieldValueArray))
        {
            foreach ($ChangedArray as $key => $field)
            {
                if (!in_array($field, $ModuleDefs[$Module]['FIELD_ARRAY']))
                {
                    unset($ChangedArray[$key]);
                }
            }
        }
        // Otherwise injury/bodypart fields are still included here and cause sql errors.
        elseif ($MainTable == 'link_contacts')
        {
            foreach ($ChangedArray as $key => $field)
            {
                if (!in_array($field, $ModuleDefs['CON']['LINKED_FIELD_ARRAY']))
                {
                    unset($ChangedArray[$key]);
                }
            }
        }
        // Otherwise injury/bodypart fields are still included here and cause sql errors.
        elseif ($MainTable == 'contacts_main')
        {
            foreach ($ChangedArray as $key => $field)
            {
                if (!in_array($field, $ModuleDefs['CON']['FIELD_ARRAY']))
                {
                    unset($ChangedArray[$key]);
                }
            }
        }

        if (!empty($ChangedArray))
        {
            $SelectList = implode($ChangedArray, ', ');
            $sql = "SELECT $SelectList
                FROM $MainTable
                WHERE ";

            if ($WhereClause)
            {
                $sql .= $WhereClause;
                $PDOParams = array();
            }
            else
            {
                $sql .= "recordid = :recordid";
                $PDOParams["recordid"] = $recordid;
            }

            $row = PDO_fetch($sql, $PDOParams);

            // Insert a new row into full_audit for each
            // value which has changed.  Note that this is
            // different behaviour from the full audit in the
            // main application, but allows each changed value
            // to be shown separately for each field.
            foreach ($ChangedArray as $Changed)
            {
                if ($Changed == "rep_approved")
                {
                    $sValue = $_POST[$Changed];

                    if ($Module == 'INC')
                    {
                        //duplicate rep_approved audit table to improve speed of overdue calculations in incidents.
                        $sql = 'INSERT INTO inc_status_audit ([RECORDID], [LOGIN], [DATE], [STATUS]) VALUES (:recordid, :login, :date, :status)';
                        DatixDBQuery::PDO_insert($sql, array('recordid' => $recordid, 'login' => $_SESSION["initials"], 'date' => $AuditDate, 'status' => $sValue));
                    }
                }
                else
                {
                    $sValue = $row[$Changed];
                }

                $sql = "INSERT INTO full_audit
                    (aud_module, aud_record,
                    aud_login, aud_date, aud_action,
                    aud_detail)
                    VALUES
                    (:aud_module, :aud_record, :aud_login, :aud_date, :aud_action, :aud_detail)";
                PDO_query($sql, array(
                    "aud_module" => $Module,
                    "aud_record" => $recordid,
                    "aud_login" => $_SESSION["initials"],
                    "aud_date" => $AuditDate,
                    "aud_action" => "WEB:" .$Changed,
                    "aud_detail" => $sValue
                ));
            }
        }
    }

    // Do full audit for last_updated field
    if (in_array($Module, ['INC', 'COM', 'CLA', 'PAL', 'RAM']))
    {
        $sql = 'SELECT ' . strtolower($Module) . '_last_updated FROM ' . $MainTable . ' WHERE recordid = :recordid';
        $audDetail = \DatixDBQuery::PDO_fetch($sql, ['recordid' => $recordid], \PDO::FETCH_COLUMN);

        $sql = '
            INSERT INTO full_audit (
                aud_module, aud_record, aud_login, aud_date, aud_action, aud_detail
            )
            VALUES (
                :aud_module, :aud_record, :aud_login, :aud_date, :aud_action, :aud_detail
            )
        ';

        \DatixDBQuery::PDO_query($sql, [
            'aud_module' => $Module,
            'aud_record' => $recordid,
            'aud_login' => $_SESSION['initials'],
            'aud_date' => $AuditDate,
            'aud_action' => 'WEB:' . strtolower($Module) . '_last_updated',
            'aud_detail' => $audDetail
        ]);
    }
}

/**
* @desc Retuns a list of modules that it is possible to link actions to and which are licensed.
*
* @returns array An array of module codes.
*/
function getActionLinkModules()
{
    $LinkModules = array(
        'INC', 'PAL', 'CLA', 'COM', 'RAM', 'SAB', 'AST', 'STN', 'ELE', 'PRO', 'HOT', 'CQO', 'CQP', 'CQS', 'AMO', 'AQU'
    );

    foreach ($LinkModules as $Module)
    {
        if (ModIsLicensed($Module))
        {
            $FinalLinkModules[] = $Module;
        }
    }

    return $FinalLinkModules;
}

/**
* @desc Grabs the global value that applies to a particular user. If no specific value is found,
 * then just returns the global using {@link GetParm()}
*
* @param string $login The login of the user
* @param string $Glob The name of the global
* @param string $Default The value to return if no other is found in the session or db
*
* @return string The global value
*/
function GetUserParm($login, $param, $default = '')
{
    if ($login && $param)
    {
        if (isset($_SESSION['UserParms'][$login][$param]) && !$GLOBALS['devNoCache'])
        {
            return $_SESSION['UserParms'][$login][$param];
        }
        else
        {
            $sql = "SELECT parmvalue
                    FROM user_parms
                    WHERE parameter like :parameter and login like :login";
            $value = PDO_fetch($sql, array("parameter" => $param, "login" => $login), PDO::FETCH_COLUMN);
    
            if ($value == '')
            {
                $sql = "SELECT lpp_value
                        FROM link_profile_param
                        JOIN staff ON sta_profile = lpp_profile
                        WHERE lpp_parameter like :parameter and login like :login";
                $value = PDO_fetch($sql, array("parameter" => $param, "login" => $login), PDO::FETCH_COLUMN);
            }
    
            if ($value == '')
            {
                /*
                 * We need to force the fetch from the DB because when we reach here we want live data
                 * or the default value passed into this function.
                 */
                 $value = GetParm($param, $default, true);
            }
            
            $_SESSION['UserParms'][$login][$param] = $value;
        }
    }

    return $value;
}

/**
* @desc Grabs a global value, either from the session, the database or from a provided default.
*
* @param string $Glob The name of the global
* @param string $Default The value to return if no other is found in the session or db
* @param bool $bGetLive Whether to check the db or not - if true will check the db, otherwise will rely on the session.
*
* @return string The global value
*/
function GetParm($Glob, $Default = "", $bGetLive = false)
{
    if ($bGetLive)
    {
        $sql = "SELECT parmvalue
                FROM globals
                WHERE parameter like :glob";
        $GlobValue = DatixDBQuery::PDO_fetch($sql, array('glob' => $Glob), PDO::FETCH_COLUMN);
    }
    else
    {
        if (!$Glob || !isset($_SESSION["Globals"][$Glob]))
        {
            return $Default;
        }

        $GlobValue = $_SESSION["Globals"][$Glob];
    }


    if ($GlobValue != "")
    {
        return $GlobValue;
    }
    else
    {
        return $Default;
    }
}

/**
* Checks whether a field is a location field or not - used to validate whether the automated
 * combo-link backfill is appropriate
*/
function IsLocationField($FieldName, $Module)
{
    if ($Module == 'PAL')
    {
        $ModuleString = '(pal|psu)';
    }
    elseif ($Module == 'COM')
    {
        $ModuleString = '(com|csu)';
    }
    else
    {
        $ModuleString = \UnicodeString::strtolower($Module);
    }

    return (preg_match('/^'.$ModuleString.'_(organisation|directorate|specialty|unit|clingroup|loctype|locactual|location)(_\d)?$/u', $FieldName) > 0);
}

/**
* @desc Translates a yes/no value into a boolean
*
* @param string $Parm The value to be translated ("Y" or "N")
*
* @return bool true if passed "Y" or "y" false otherwise.
*/
function bYN($Parm)
{
    if ($Parm == "Y" || $Parm == "y")
    {
        return true;
    }
    else
    {
        return false;
    }

}

/**
* @desc Takes a field name and calls the appropriate MakeFieldWhere function depending on whether
* it is a UDF or not.
*
* @param string $FieldName The name of the field
* @param string $value The value of the field
* @param string $module The module this field belongs to.
* @param string $table The table this field can be found in.
*
* @return string A where clause tailored to this field.
*/
function CheckMakeFieldWhere($FieldName, $value, $module, $table = '')
{
    global $ModuleDefs;

    if (!$table)
    {
        $table = ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']);
    }

    $explodeArray = explode("_",$FieldName);

    if ($explodeArray[0] == "UDF")
    {
        if (MakeUDFFieldWhere($explodeArray, $value, $ModuleDefs[$module]['MOD_ID'], $FieldWhere) === false)
        {
            return false;
        }
    }
    else
    {
        if (MakeFieldWhere($module, $FieldName, $value, $FieldWhere, $table) == false)
        {
            return false;
        }
    }

    return $FieldWhere;
}

/**
 * Append to an SQL 'where' statement. Pass the field and the value that's going to search the field, and generate
 * part of the where statement for it. 
 *
 * @param string $Module          Module the field belongs to - to retrieve field definition
 * @param string $name            The name of the field
 * @param string $value			  The content of the field
 * @param string $search          The where clause returned
 * @param string $tableprefix     Table prefix of column (optional) 
 * @param array  $PDOPlaceholders [Not sure what this is - CA]
 *
 * @return bool Was the amendment to the 'where statement' made correctly. This can fail if the provided  
 *              information was improperly formatted. E.g. searching for a date 30/11/2014 in U.S. system.
 *
 * @throws InvalidDataException
 */
function MakeFieldWhere(
    $Module, // Module the field belongs to - to retrieve field definition
    $name, // The name of the field
    $value, // The content of the field
    &$search, // The where clause returned
    $tableprefix = "",
    $PDOPlaceholders = array()
    )
{
    global $FieldDefs, $FieldDefsExtra, $ModuleDefs;

    $search = '';

    $value = trim($value);

    $parts = explode("_", $name);
    $isUDF = ($parts[0] == 'udv');

    if($parts[2] == '1' && $parts[3] == '1')
    {
        $name = substr($name, 0, strlen($name)-2);
    }

    if ($value == '' || !$isUDF && (!isset($FieldDefsExtra[$Module][$name])))
    {
        // Field not defined or blank - skip field
        return true;
    }

    if ($isUDF)
    {
        $FieldType = $parts[1];
    }
    else
    {
        $FieldType = $FieldDefsExtra[$Module][$name]['Type'];
    }

    if (is_array($value))
    {
        $value = implode('|', $value);
    }

    // Check whether the field contains multiple values
    $sepPipePos = \UnicodeString::strpos($value, '|');
    $sepAmpPos = \UnicodeString::strpos($value, '&');

    if ($FieldType == 'money' && !($sepAmpPos || $sepPipePos))
    {
        $value = preg_replace("/[^0-9\.\<\>=!?]/u","",$value);
    }

    if ($FieldType == 'date')
    {
        if ($tableprefix != "" && \UnicodeString::strpos($name, ".") === false)
        {
            $datename = $name;
            $name = $tableprefix . "." . $name;
        }

        // picks up just the date part, ignoring the time. Otherwise we get problems with '=' matches.
        $name = 'CAST( FLOOR( CAST( '.$name.' AS FLOAT ) ) AS DATETIME)';
    }

    if ($FieldType == 'checkbox')
    {
        //checkboxes are basically yes/no fields and pass their values as Y and N
        $FieldType = 'yesno';
    }

    $valueTmp = $value;
    $FieldWhere = '';

    $multiDataTypes = array('multilistbox');
    $strDataTypes = array(
        'string',
        'textarea',
        'ff_select',
        'yesno',
        'grading',
        'combobox',
        'string_search',
        'email',
        'formfield',
        'time',
        'text'
    );
    $rangeDataTypes = array('date', 'number', 'money');

    if ($sepPipePos !== false || $sepAmpPos !== false)
    {
        $search = '';
        $sepPrev = '';
        $valueStr = $valueTmp;

        while (\UnicodeString::strlen($valueStr) > 0)
        {
            // Get the position and type of the first separator
            $sepPipePos = \UnicodeString::strpos($valueStr, '|');
            $sepAmpPos = \UnicodeString::strpos($valueStr, '&');

            if ($sepPipePos !== false || $sepAmpPos !== false)
            {
                if ($sepPipePos !== false && $sepAmpPos !== false)
                {
                    $sepPos = ($sepPipePos < $sepAmpPos ? $sepPipePos : $sepAmpPos);
                    $sep = ($sepPipePos < $sepAmpPos ? '|' : '&');
                }
                elseif ($sepPipePos !== false)
                {
                    $sepPos = $sepPipePos;
                    $sep = '|';
                }
                else
                {
                    // ($sepAmpPos !== false)
                    $sepPos = $sepAmpPos;
                    $sep = '&';
                }
            }
            else
            {
                $sepPos = false;
                $sep = '';
            }

            // Extract the value on the left of the separator (if there is one) and build where clause
            if ($sepPos !== false)
            {
                $valueStrLeft = \UnicodeString::substr($valueStr, 0, $sepPos);
            }
            else
            {
                // No more separators this will be the last pass,
                // use the whole of the remaining string
                $valueStrLeft = $valueStr;
            }

            //If date, then need to pass MakeFieldWhere the real field name rather than the CAST value
            // in order to handle AND and OR symbols.
            if ($FieldType == 'date')
            {
                if($isUDF)
                {
                    $name = 'udv_date';
                }
                else
                {
                  $name = $datename;
            }
            }

            if (MakeFieldWhere($Module, $name, $valueStrLeft, $FieldWhere, $tableprefix) === false)
            {
                return false;
            }

            $searchWhereTmp = $FieldWhere;

            if ($search != '' && $sepPrev != '')
            {
                $search .= ($sepPrev == '&' ? ' AND ' : ' OR ');
            }

            $search .= $searchWhereTmp;

            if ($sepPos !== false)
            {
                $valueStr = \UnicodeString::substr($valueStr, $sepPos+1);
                $sepPrev = $sep;
            }
            else
            {
                $valueStr = '';
                $sepPrev = '';
            }
        }

        $search = "($search)";
        return true;
    }

    if ($isUDF)
    {
        if ($FieldType == 'multilistbox') //multicode vals are stored in udv_string.
        {
            $name = 'udv_string';
        }
    }

    if ($tableprefix != "" && \UnicodeString::strpos($name, ".") === false)
    {
        $name = $tableprefix . "." . $name;
    }

    // checking for null and not null values
    if (\UnicodeString::strtolower($valueTmp) == 'is not null' || $valueTmp == '==')
    {
        if ($FieldType == 'date')
        {
            if (empty($datename))
            {
                $datename = $name;
            }

            $search = "$name is not null and $datename != ''";
        }
        elseif (in_array($FieldType, $strDataTypes))
        {
            $search = "$name is not null and $name not like ''";
        }
        else
        {
            $search = "$name is not null";
        }

        return true;
    }
    elseif (\UnicodeString::strtolower($valueTmp) == 'is null' || $valueTmp == '=')
    {
        if (in_array($FieldType, $strDataTypes))
        {
            $search = "($name is null or $name like '')";
        }
        else
        {
            $search = "$name is null";
        }

        return true;
    }

    if (($FieldType == "number" || $FieldType == "money") && preg_match('/[^ -<>=:.,\$£!0-9]/u', $value) > 0 &&
        !in_array($value, $PDOPlaceholders) && \UnicodeString::stripos($value, '@USER_') === false)
    {
        AddSessionMessage('ERROR', 'An invalid character was used in the field "'.GetFormFieldLabel($name,'',$Module,'').'"');
        return false;
    }

    if ($FieldType == "money" && $value === '')
    {
        AddSessionMessage('ERROR', 'An invalid character was used in the field "'.GetFormFieldLabel($name,'',$Module,'').'"');
        return false;
    }

    $value = str_replace("'", "''", $value); //single to double singles

    // searching on Date and numeric ranges
    if (in_array($FieldType, $rangeDataTypes) && \UnicodeString::strpos($value, ':') !== false &&
        !in_array($value, $PDOPlaceholders))
    {
        $valueparts = explode(":", $value);

        if (!$valueparts[0] && !$valueparts[1])
        {
            return false;
        }

        $valueparts[0] = \UnicodeString::trim($valueparts[0]);
        $valueparts[1] = \UnicodeString::trim($valueparts[1]);

        if ($valueparts[0] && $valueparts[1])
        {
            if ($FieldType == "date")
            {
                if (!validStringDate($valueparts[0]) || !validStringDate($valueparts[1]))
                {
                    AddSessionMessage('ERROR', 'One or both of the dates entered as part of a date range in the field "'.GetFormFieldLabel($datename,'',$Module,'').'" is invalid');
                    return false;
                }

                $value1 = '\''.UserDateToSQLDate($valueparts[0]).'\'';
                $value2 = '\''.UserDateToSQLDate($valueparts[1]).'\'';

                if ($value1 == "NULL" || $value2 == "NULL") //one or both of the dates entered is invalid
                {
                    AddSessionMessage('ERROR', 'One or both of the dates entered as part of a date range in the field "'.GetFormFieldLabel($datename,'',$Module,'').'" is invalid');
                    return false;
                }
            }
            else
            {
                $value1 = $valueparts[0];
                $value2 = $valueparts[1];
            }

            $search = "($name BETWEEN $value1 AND $value2)";

            return true;
        }
        else
        {
            if ($FieldType == "date")
            {
                if (!validStringDate($valueparts[0]))
                {
                    AddSessionMessage('ERROR', 'The date entered in the field "'.GetFormFieldLabel($datename,'',$Module,'').'" is invalid');
                    return false;
                }

                $value = '\''.UserDateToSQLDate($valueparts[($valueparts[0] ? 0 : 1)]).'\'';
            }
            else
            {
                $value = $valueparts[($valueparts[0] ? 0 : 1)];
            }

            $search = $name . ($valueparts[0] ? '>=' : '<=') . $value;
            return true;
        }
    }

    // get first and second chars to check the type of comparison
    $chars = \UnicodeString::str_split($value);

    if (( $chars[1] == '=' || $chars[1] == '>' ) && ($chars[0] == '<' or $chars[0] == '>' or $chars[0] == '!'))
    {
        $value = \UnicodeString::trim(\UnicodeString::substr($value, 2, \UnicodeString::strlen($value)-2));

        if ($FieldType == 'date' && ((\UnicodeString::strpos($value, "@") !== false &&
                    !in_array(\UnicodeString::strtoupper($value), getAtCodeList()) && \UnicodeString::strpos($value, "@TODAY") !== false) ||
                (\UnicodeString::strpos($value, "@") === false && !validStringDate($value))))
        {
            AddSessionMessage('ERROR', 'The date entered in the field "'.GetFormFieldLabel($datename,'',$Module,'').'" is invalid');
            return false;
        }
		
        // Provide NOT LIKE search facility - eg: !=AA*
        if ($chars[0] == '!' and $chars[1] == '=' and \UnicodeString::strpos($value, '*') !== false)
        {
            if (!in_array($FieldType, $strDataTypes))
            {
                return false;
            }

            $search = "$name not like " . QuoteData($value, $FieldType);
            return true;
        }
        else if ($chars[0] == '!' and $chars[1] == '=')
        {
            if ($FieldType == 'multilistbox')
            {
                $search = " NOT ($name = '".$value."' OR $name LIKE '".$value." %' OR $name LIKE '% ".$value."' OR $name LIKE '% ".$value." %')";
                return true;
            }
            else
            {
                $search = "($name {$chars[0]}{$chars[1]} " . QuoteData($value, $FieldType) . " OR $name IS NULL)";
                return true;
            }
        }
        else
        {
            $search = "$name {$chars[0]}{$chars[1]} " . QuoteData($value, $FieldType);
            return true;
        }
    }
    elseif ($chars[0] == '>' || $chars[0] == '<' || $chars[0] == '=')
    {
        $value = \UnicodeString::trim(\UnicodeString::substr($value, 1, \UnicodeString::strlen($value)-1));

        //If the field being searched is a date field, then we need to ensure it's formatted correctly before
        //plugging it into the SQL query
        if ($FieldType == 'date' && ((\UnicodeString::strpos($value, "@") !== false &&
            !in_array(\UnicodeString::strtoupper($value), getAtCodeList()) && \UnicodeString::strpos($value, "@TODAY") !== false) ||
            (\UnicodeString::strpos($value, "@") === false && !validStringDate($value))))
        {
            AddSessionMessage('ERROR', 'The date entered in the field "'.GetFormFieldLabel($datename,'',$Module,'').'" is invalid');
            return false;
        }

        $search = "$name {$chars[0]} " . QuoteData($value, $FieldType);

        return true;
    }
    if ($FieldType == 'time')
    {
        if (in_array($value, $PDOPlaceholders))
        {
            $value2 = $value;
            $search = "$name LIKE $value OR $name LIKE $value2";
        }
        else
        {
            $value = str_replace(':','',$value);
            $value2 = substr($value, 0, 2).':'.substr($value, 2);
            $search = "$name LIKE ".QuoteData($value, $FieldType)." OR $name LIKE ".QuoteData($value2, $FieldType);
        }
        return true;
    }
    else
    {
        if (in_array($value, $PDOPlaceholders))
        {
            if (in_array($FieldType, $strDataTypes))
            {
                $search = "$name LIKE $value";
            }
            else
            {
                $search = "$name = $value";
            }
        }
        elseif (in_array($FieldType, $strDataTypes))
        {
            $search = "$name like " . QuoteData($value, $FieldType);
        }
        elseif ($FieldType == 'multilistbox')
        {
            // searching on multicoded fields
            $search = "($name = '$value'";
            $search .= " OR $name like '$value %'";
            $search .= " OR $name like '% $value'";
            $search .= " OR $name like '% $value %')";
        }
        else
        {
            if ($FieldType == 'date' && ((\UnicodeString::strpos($value, "@") !== false &&
                !in_array(\UnicodeString::strtoupper($value), getAtCodeList()) && \UnicodeString::strpos($value, "@TODAY") !== false) ||
                (\UnicodeString::strpos($value, "@") === false && !validStringDate($value))))
            {
                AddSessionMessage('ERROR', 'The date entered in the field "'.GetFormFieldLabel($datename,'',$Module,'').'" is invalid');
                return false;
            }

            $search = "$name = " . QuoteData($value, $FieldType);
        }
        return true;
    }

    return false;
}

function validStringDate($date)
{
    if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
    {
        return (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/u', $date));
    }
    elseif (GetParm('FMT_DATE_WEB') == 'US')
    {
        return (preg_match('/^(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])\/[0-9]{4}$/u', $date));
    }
    else
    {
        return (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/u', $date));
    }
}

function getAtCodeList()
{
    return array(
        '@TODAY', '@LASTWEEK', '@LASTMONTH', '@LASTQUARTER', '@LASTYEAR', '@WEEK', '@MONTH', '@QUARTER',
        '@YEAR', '@FINYEAR', '@LASTFINYEAR', '@PROMPT', '@USER_INITIALS'
    );
}

function CheckForCorrectAtCodes($WhereClause)
{
    global $FieldDefs;

    preg_match_all('/([<>=\' -]|^)(@[^<>=\' -]+)([<>=\' -]|$)/u', $WhereClause, $Matches);

    foreach ($Matches[2] as $Match)
    {
        if (!in_array(\UnicodeString::strtoupper($Match), getAtCodeList()))
        {
            $ConCodeCheck = \UnicodeString::str_ireplace("@USER_", "", $Match);

            if (!array_key_exists(\UnicodeString::strtolower($ConCodeCheck), $FieldDefs["CON"]))
            {
                return false;
            }
        }
    }

    return true;
}

function AddMangledSearchError($name, $value)
{
    AddSessionMessage('ERROR', 'There was a problem with the value entered in the \''.GetFormFieldLabel($name).'\' field. The system does not understand what \''.$value.'\' means');
}

/**
* Formats and surrounds a given value with quotes (if necessary).
*
* Used when building WHERE clauses e.g. when searching.
*
* @param  string  $value     The data value.
* @param  string  $datatype  The data type.
*
* @return string  $value     The value having been reformatted and quoted (if necessary).
*/
function QuoteData($value, $datatype)
{
    $strDataTypes = array(
        'string', 'textarea', 'ff_select', 'yesno', 'grading', 'combobox', 'string_search', 'multilistbox', 'email',
        'time', 'text'
    );

    $datatimeDataTypes = array('date');
    $strAtSigns = getAtCodeList();

    if (in_array($datatype, $strDataTypes))
    {
        if (\UnicodeString::strlen($value) > 0)
        {
            $value = '\'' . str_replace(array('*', '?'), array('%', '_'), $value) . '\'';
        }
        else
        {
            $value = '\'\'';
        }

        return $value;
    }
    elseif (in_array($datatype, $datatimeDataTypes))
    {
        if ($value[0] == '@')
        {
            foreach ($strAtSigns as $AtSign)
            {
                if (\UnicodeString::stripos($value, $AtSign)!== false)
                {
                    return '\'' . $value . '\'';
                }
            }
        }

        return '\''.UserDateToSQLDate($value).'\'';
    }
    else
    {
        return $value;
    }
}

function GetSQLResultsTimeScale($selectfields = "", $dbtable = "", $order = "ASC", $orderby = "",
                $WhereClause = "", $listnum, &$listdisplay, &$listnum2, &$listnumall,
                &$sqla, $join = null, $module = '')
{
    global $ModuleDefs, $FieldDefs, $dtxdebug;

    require_once 'Source/libs/SavedQueries.php';

    $WhereClause = ReplacePrompts($WhereClause, $_GET['module']);

    $_SESSION[$_GET['module']]["PROMPT"]["ORIGINAL_JOIN"] = $join;
    $_SESSION[$_GET['module']]["PROMPT"]["ORIGINAL_WHERE"] = $WhereClause;

    DoPromptSection(array('module' => $_GET['module']));

    if ($_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"])
    {
        $WhereClause = $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"];
        $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"] = '';
    }

    if ($module)
    {
        foreach ($selectfields as $key => $field)
        {
            if (!isset($FieldDefs[$module][$field]))
            {
                $selectfields[$key] = 'null as \''.$field.'\'';
            }
        }
    }

    if (!in_array('recordid', $selectfields))
    {
        $selectfields[] = 'recordid';
    }

    if ($orderby)
    {
        if (!is_array($orderby))
        {
            $orderby_fields = explode(',', $orderby);
        }
        else
        {
            $orderby_fields = $orderby;
        }

        foreach ($orderby_fields as $orderby_field)
        {
            $orderby_field = \UnicodeString::trim($orderby_field);

            if (!in_array($orderby_field, $selectfields))
            {
                $selectfields[] = $orderby_field;
            }
        }
    }

    if (is_array($selectfields))
    {
        $selectfields = implode(", ", $selectfields);
    }

    if (is_array($dbtable))
    {
        $dbtable = implode(", ", $dbtable);
    }

    $listdisplay = GetParm("LISTING_DISPLAY_NUM", 20);

    if (!$listnum)
    {
        $listnum = $listdisplay;
    }

    // Counting records
    if (!empty($WhereClause) || !in_array($module, array('LOC', 'ATM', 'ATQ', 'ATI', 'AMO', 'AQU')))
    {
        $sqlnum = "SELECT count(*) as listnumall
               FROM $dbtable
               $join
               " . (!empty($WhereClause) ? "WHERE $WhereClause" : '');

        $requestnum = db_query($sqlnum,true);

        if ($requestnum === false)
        {
            fatal_error(_tk("search_errors").($dtxdebug ? $sqlnum : ''));
        }

        if ($row = db_fetch_array($requestnum))
        {
            $listnumall =  $row["listnumall"];
        }
        else
        {
            $listnumall = 0;
        }
    }
    else
    {
        $listnumall = 0;
    }

    $intop = Sanitize::SanitizeInt($listnum);

    if (($listnum) <= $listnumall)
    {
        $outtop = $listdisplay;
    }
    else
    {
        $outtop = ($listdisplay-($listnum-$listnumall));
        $intop = $listnumall;

        if ($outtop < 0)
        {
            $outtop = $listdisplay;
            $intop = $listdisplay;
            $listnum = $listdisplay;
        }
    }

    if ($order == "ASC")
    {
        $sorttype_a = "ASC";
        $sorttype_b = "DESC";
    }
    else
    {
        $sorttype_b = "ASC";
        $sorttype_a = "DESC";
    }

    if ($orderby && $orderby != "recordid")
    {
        $sqlsortfielda = "$orderby $sorttype_a,";
        $sqlsortfieldb = "$orderby $sorttype_b,";
    }

    $sqla = "SELECT $selectfields
            FROM ( ";
    $sqla .= " SELECT TOP $outtop $selectfields
            FROM ( ";
    $sqla .= " SELECT TOP $intop $selectfields
            FROM $dbtable
            $join
            ";

    if ($WhereClause)
    {
        $sqla .= " WHERE " . $WhereClause;
    }

    $sqla .= " ORDER BY $sqlsortfielda recordid $sorttype_a";
    $sqla .= ") q ORDER BY $sqlsortfieldb recordid $sorttype_b) r ORDER BY $sqlsortfielda recordid $sorttype_a";

    $request = db_query($sqla);
    $listnum2 = $listnum;
    return $request;
}

/**
* @desc Counts the number of records linked to a main record - uses views for this.
*
* @param string $linkTable The name of the view holding the link information
* @param string $recordId The recordid of the main record
* @param string $keyColumn The name of the column holding the recordid of the main record
* @param string $countColumn The name of the column holding the number of records found
*
* @return int The number of linked records.
*/
function CountLinkedRecords($linkTable, $recordId, $keyColumn, $countColumn)
{
    $count = 0;

    $sql = "SELECT $countColumn FROM $linkTable WHERE $keyColumn = :recordid";

    if ($row = PDO_fetch($sql, array("recordid" => $recordId)))
    {
        $count = $row[$countColumn];
    }

    return $count;
}

/**
* @desc Returns the contents of the field_formats table for a particular field.
*
* @param string $field The name of the field to be retrieved
* @param string $module The module this field is in
* @param string $table The table this field is in
*
* @return array Record data from the field_formats table.
*/
function getFieldFormat(
    $field,         // field_formats.fmt_module
    $module = '',   // field_formats.fmt_field
    $table = ''     // field_formats.fmt_table
    )
{
    global $ModuleDefs;

    if (!$field || $module == '')
    {
        return null;
    }

    $field = \UnicodeString::strtolower($field);
    $table = \UnicodeString::strtolower($table);

    if ($module == 'ADM')
    {
        $module = 'CON';
    }

    if ($_SESSION["ff_loaded"][$module] !== true)
    {
        InitFieldFormats($module);
    }

    //hack for payment summary screen
    if ($module == 'CLA' && \UnicodeString::substr($field, 0, 9) == 'pay_type_')
    {
        if (!isset($_SESSION["fformats"][$module][$table][$field]))
        {
            $ExplodeField = explode('_', $field);
            $ColName = DatixDBQuery::PDO_fetch('SELECT description from code_fin_type WHERE code = :code', array(
                'code' => $ExplodeField[2]
            ), PDO::FETCH_COLUMN).' (Payment summary)';

            $_SESSION["fformats"][$module][$table][$field] = array(
                'fmt_module' => 'CLA',
                'fmt_title' => $ColName,
                'fmt_data_type' => 'M'
            );
        }

        return $_SESSION["fformats"][$module][$table][$field];
    }

    if ($table == '' && is_array($_SESSION["fformats"][$module]))
    {
        foreach ($_SESSION["fformats"][$module] as $table => $fields)
        {
            if (!empty($_SESSION["fformats"][$module][$table][$field]))
            {
                break;
            }
        }
    }

    return $_SESSION["fformats"][$module][$table][$field];
}

/**
* @desc Called if field formats have not yet been cached in the session - collects them all
 * and saves in a session variable.
*
* @param string $module The module to be loaded.
*/
function InitFieldFormats($module = '')
{
    $sql = "SELECT * FROM field_formats WHERE fmt_module = :module";

    $result = DatixDBQuery::PDO_fetch_all($sql, array("module" => $module));

    foreach ($result as $fformats)
    {
        if (is_array($fformats) && !empty($fformats))
        {
            $fformats = array_change_key_case($fformats, CASE_LOWER);
        }

        $_SESSION["fformats"][$module][\UnicodeString::strtolower($fformats['fmt_table'])][\UnicodeString::strtolower($fformats['fmt_field'])] = $fformats;
    }

    $_SESSION["ff_loaded"][$module] = true;
}

function SendEmail($to = "", $subject = "", $body = "", $filearray)
{
	if (empty($to))
	{
		// No email address has been defined
		return false;
	}

    if (is_array($filearray))
    {
        foreach ($filearray as $farray)
        {
            $handle = fopen($farray["source"], 'rb');

            if (filesize($farray["source"]) > 0)
            {
                $f_contents[$farray["name"]] = fread($handle, filesize($farray["source"]));
            }

            $f_contents[$farray["name"]] = chunk_split(base64_encode($f_contents[$farray["name"]]));
            fclose($handle);
        }
    }

    $AdminEmail = $_SESSION["Globals"]["DIF_ADMIN_EMAIL"];
    $from = "From: $AdminEmail";

    $eol = "\r\n";
    $headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">".$eol;
    $headers .= "X-Mailer: PHP v".phpversion().$eol;
    $mime_boundary=hash("sha512",time());
    $headers .= 'MIME-Version: 1.0'.$eol;
    $headers .= "Content-Type: multipart/related; boundary=\"".$mime_boundary."\"".$eol;
    $headers .= $from;
    $body .= $eol.$eol;

    // body
    $msg .=  "--".$mime_boundary.$eol.
               "Content-Type: text/plain; charset='utf-8'".$eol .
            "Content-Transfer-Encoding: 7bit".$eol.$eol;
    $msg .= "$body ".$eol.$eol;

    // attachments
    if (is_array($filearray))
    {
        foreach ($filearray as $farray)
        {
            $msg .= "--".$mime_boundary.$eol;
            $msg .= "Content-Type: application/txt; name=\"".$farray["name"]."\"".$eol;
            $msg .= "Content-Transfer-Encoding: base64".$eol;
            $msg .= "Content-Disposition: attachment; filename=\"".$farray["name"]."\"".$eol.$eol;
            $msg .= $f_contents[$farray["name"]].$eol.$eol;
        }
    }

    $msg .= "--".$mime_boundary."--".$eol.$eol;

    if (!@\UnicodeString::mail($to, $subject, $msg, $headers))
    {
        fatal_error("There was a system error when trying to send the e-mail.  This may be because of firewall settings on the server, or an incorrectly configured php.ini.<br />".
        ($php_errormsg ? "<br />The error was:<br /><br />$php_errormsg" : ''));
    }
}

/**
* @desc MakeUDFFieldWhere puts a where clause into $udf, by hijacking the MakeFieldWhere
*  function. The where clause checks both that the value in the udf_values table is appropriate
*  and that the udf field would appear on the record.
*
* @param array $explodeArray The udf field name split by '_' into an array.
* @param string $value The value this field holds.
* @param string $table The table to search in the module we are looking at.
* @param int $module_num The module we are searching in
* @param string $search Reference to a variable which will be populated by the where clause.
*
*/
function MakeUDFFieldWhere(
    $explodeArray,
    $value,
    $module_num,
    &$search // The where clause returned
    )
{
    global $ModuleDefs;

    if ($value == '')
    {
        return true;
    }

    $module = ModuleIDToCode($module_num);
    $table = ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']);

    $fieldid = $explodeArray[3];
    $groupid = $explodeArray[2];
    $Type = $explodeArray[1];

    $FieldNames = array(
        'Y' => 'udv_string',
        'D' => 'udv_date',
        'S' => 'udv_string',
        'N' => 'udv_number',
        'M' => 'udv_money',
        'T' => 'udv_multilistbox',
        'L' => 'udv_text'
    );

    if (is_array($value))
    {
        $value = implode("|", $value);
    }

    if (!isset($FieldNames[$Type]))
    {
        $Type = 'S';
    }

    $valid = MakeFieldWhere($module, $FieldNames[$Type], ($value == '=' ? '==' : $value), $FieldWhere);

    if ($value == '=')
    {
        $search = 'NOT (('. $table . '.recordid IN (SELECT cas_id FROM udf_values WHERE field_id = '.$fieldid.($groupid !== '' ? ' AND group_id = '.$groupid : '').' AND mod_id = '. $module_num .' AND '.$FieldWhere.')))';
    }
    else
    {
        $search = '('. $table . '.recordid IN (SELECT cas_id FROM udf_values WHERE field_id = '.$fieldid.($groupid !== '' ? ' AND group_id = '.$groupid : '').' AND mod_id = '. $module_num .' AND '.$FieldWhere.'))';
    }

    return $valid;
}

/**
 * @param $date
 *
 * Checks whether a date is provided in the correct format for processing within the application
 */
function ValidateSQLServerDate($date)
{
    if (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{3}/', $date) !== 1)
    {
        throw new InvalidDataException('Date provided is not in a valid format');
    }

    return true;
}

/**
* @desc Takes a date of birth and calculates the age of someone born on that date at a time given by a second date.
*
* @param datetime $dob The person's date of birth
* @param datetime $date The date in question
*
* @return bool/int false if there is an error, otherwise returns the person's age at the date specified.
*/
function getAgeAtDate($dob, $date)
{
    try
    {
        ValidateSQLServerDate($dob);
        ValidateSQLServerDate($date);
    }
    catch (InvalidDataException $e)
    {
        return false;
    }

    $dobday = date('d', strtotime($dob));
    $dobmonth = date('m', strtotime($dob));
    $dobyear = date('Y', strtotime($dob));

    $dateday = date('d', strtotime($date));
    $datemonth = date('m', strtotime($date));
    $dateyear = date('Y', strtotime($date));

    if ($dobyear &&  $dobmonth && $dobday && $dateyear && $datemonth && $dateday)
    {
        $age = intval($dateyear) - intval($dobyear);

        if (intval($datemonth) < intval($dobmonth) && $age > 0)
        {
            $age--;
        }
        elseif (intval($datemonth) == intval($dobmonth))
        {
            if (intval($dateday) < intval($dobday) && $age >0)
            {
                $age--;
            }
        }
        if ($age < 0)
        {
            $age = 0;
        }
        return $age;
    }

    return false;
}

/**
* @desc Retrieved the fld_code_like value from udf_fields for a given field id. Uses caching to prevent multiple calls.
*
* @param int $FieldId The recordid of the udf field in question
*
* @return string The value of the fld_code_like field for the given field id.
* This value is the name of the main-table field this extra field is linked to for coding purposes.
*/
function GetFieldCodeLike($FieldID)
{
    if (!isset($_SESSION['CachedValues']['fld_code_like']))
    {
        $result = DatixDBQuery::PDO_fetch_all ("SELECT recordid, fld_code_like FROM udf_fields", array(), PDO::FETCH_KEY_PAIR);

        $_SESSION['CachedValues']['fld_code_like'] = $result;
    }

    return $_SESSION['CachedValues']['fld_code_like'][$FieldID];
}

/**
* @desc code_descr_udf works similarly to {@link code_descr()}, but retrieves the description of the udf code.
*
* @param int $fieldid The id of the udf
* @param string $code The code that we want to match to a description
* @param string $module The module that we are in (needed when the udf is linked to the codes of a standard field).
*
* @return string The description of the code if one is found, otherwise the code itself
*/
function code_descr_udf($fieldid, $code, $module='', $escape = true)
{
    if ($code=='Y')
    {
        return 'Yes';
    }

    if ($code=='N')
    {
        return 'No';
    }

    if ($code == "" || $fieldid=="")
    {
        return $code;
    }

    $fld_code_like = GetFieldCodeLike($fieldid);

    if ($fld_code_like)
    {
        $module = getModuleFromField($fld_code_like);
        return code_descr($module, $fld_code_like, $code, '', $escape);
    }

    $sql = "SELECT UDC_DESCRIPTION FROM udf_codes WHERE field_id = :field_id AND udc_code = :udc_code";

    $row = PDO_fetch($sql, array("field_id" => $fieldid, "udc_code" => $code));

    if ($row["UDC_DESCRIPTION"])
    {
        return $row["UDC_DESCRIPTION"];
    }
    else
    {
        return $code;
    }
}

// Returns the name of the UserSettings file for the specified linked
// contacts form.
function GetContactFormSettings($LinkType, $FormLevel = "2", $MainModule = 'INC', $MainModuleFormLevel = "",
                                $FormType = "")
{
    global $ClientFolder, $FormDesigns;

    // Main module form level can be passed optionally in order to use a different level contact form to the main form
    // e.g. SAB1 users use level 2 contact forms.
    if ($MainModuleFormLevel == "")
    {
        $MainModuleFormLevel = $FormLevel;
    }

    $Return = IncludeCurrentFormDesign($MainModule, $MainModuleFormLevel, "", $FormType);

    if ($MainModule != 'CON')
    {
        if ($FormLevel == 1)
        {
            $FilePrefix = 'CON1Settings';
        }
        else
        {
            $FilePrefix = 'CONSettings';
        }

        if (intval($FormDesigns['contacts_type_'.$LinkType][$LinkType]))
        {
            $UserSettingsFilename = 'User'.$FilePrefix.'_'.intval($FormDesigns['contacts_type_'.$LinkType][$LinkType]).'.php';
        }
        else
        {
            $UserSettingsFilename = 'User'.$FilePrefix.'.php';
        }

        if (file_exists($ClientFolder . '/' . $UserSettingsFilename))
        {
            $SettingsFilename = $ClientFolder . '/' . $UserSettingsFilename;
        }
        else
        {
            $SettingsFilename = 'Source/contacts/Standard' . $MainModule . '_CON_Settings.php';

            if (!file_exists($SettingsFilename))
            {
                $SettingsFilename = 'Source/contacts/Standard'.$FilePrefix.'.php';
            }
        }
    }
    else
    {
        $SettingsFilename = $Return['sSettingsFilename'];

        if (!file_exists($SettingsFilename))
        {
            $SettingsFilename = 'Source/contacts/Standard' . $MainModule . '_CON_Settings.php';

            if (!file_exists($SettingsFilename))
            {
                $SettingsFilename = 'Source/contacts/Standard'.$FilePrefix.'.php';
            }
        }
    }

    unsetFormDesignSettings();

    return $SettingsFilename;
}

// Returns the name of the UserSettings file for the specified linked
// equipment form.
function GetAssetFormSettings($FormLevel = 2)
{
    global $ClientFolder, $EquipmentForms, $FormTitle, $FormTitleDescr;

    IncludeCurrentFormDesign('INC', $FormLevel);

    if ($FormLevel == 1)
    {
        $FilePrefix = 'AST1Settings';
    }
    else
    {
        $FilePrefix = 'ASTSettings';
    }

    if (intval($EquipmentForms))
    {
        $UserSettingsFilename = 'User'.$FilePrefix.'_' . intval($EquipmentForms) . '.php';
    }
    else
    {
        $UserSettingsFilename = 'User'.$FilePrefix.'.php';
    }

    if (file_exists($ClientFolder . '/' . $UserSettingsFilename))
    {
        $SettingsFilename = $ClientFolder . '/' . $UserSettingsFilename;
    }
    else
    {
        $SettingsFilename = 'Source/assets/Standard'.$FilePrefix.'.php';
    }

    unsetFormDesignSettings();

    return $SettingsFilename;
}

/**
* @desc Runs through all the form design globals and unsets them. Used when switching between form designs
* such as when we have contact form designs embedded in incidents ones.
*/
function unsetFormDesignSettings()
{
    foreach (Forms_FormDesign::getFormDesignGlobals() as $GlobalName)
    {
        unset($GLOBALS[$GlobalName]);
    }
}

/**
* @desc Puts all form design globals in an array and returns it to be stored for late use.
* Used when switching between form designs such as when we have contact form designs embedded in incidents ones.
*
* @return array Array of form design global names and their values.
*/
function saveFormDesignSettings()
{
    foreach (Forms_FormDesign::getFormDesignGlobals() as $GlobalName)
    {
        $Settings[$GlobalName] = $GLOBALS[$GlobalName];
    }

    return $Settings;
}

/**
* @desc Loads new form design global values from a passed array.
*
* @param array $newGlobals Array of global names and values to be loaded into the session.
*/
function loadFormDesignSettings($newGlobals)
{
    foreach ($newGlobals as $key => $value)
    {
        $GLOBALS[$key] = $value;
    }
}

function GetContactLinkValues($recordid)
{
    $con["linkedincnum"] = CountLinkedRecords("con_links_inc_all", $recordid, "con_id", "con_inc_count");
    //$con["linkedramnum"] = CountLinkedRecords("con_links_ram", $_GET["con_id"], "con_id", "con_ram_count");
    $con["linkedpalnum"] = CountLinkedRecords("con_links_pal", $recordid, "con_id", "con_pal_count");
    $con["linkedcomnum"] = CountLinkedRecords("con_links_com", $recordid, "con_id", "con_com_count");
    $con["linkedclanum"] = CountLinkedRecords("con_links_cla", $recordid, "con_id", "con_cla_count");

    //no view for risks yet, so will need to make do with a hard coded statement:
    $sql = 'SELECT CONTACTS_MAIN.RECORDID AS con_id, COUNT(RA_MAIN.RECORDID) AS con_ram_count
            FROM
            RA_MAIN INNER JOIN
            LINK_CONTACTS ON RA_MAIN.RECORDID = LINK_CONTACTS.RAM_ID RIGHT OUTER JOIN
            CONTACTS_MAIN ON LINK_CONTACTS.CON_ID = CONTACTS_MAIN.RECORDID
            WHERE CONTACTS_MAIN.RECORDID = :recordid
            GROUP BY CONTACTS_MAIN.RECORDID';

    $row = PDO_fetch($sql, array("recordid" => $recordid));

    $con["linkedramnum"] = $row['con_ram_count'];

    return $con;
}

/**
* @desc Takes user initials and finds the recordid of the contact associated with them. Caches values to
 * prevent repeated queries.
*
* @param string $initials The initials of the contacts.
*
* @return int The recordid of the contact record in the contacts_main table.
*/
function GetConIdFromInitials($initials)
{
    //cache results to prevent lots of queries.
    if ($_SESSION['ConIdFromInitials'][$initials])
    {
        return $_SESSION['ConIdFromInitials'][$initials];
    }
    else
    {
        $sql = "select recordid as con_id from contacts_main where initials = :initials";

        $row = PDO_fetch($sql, array("initials" => $initials));

        $_SESSION['ConIdFromInitials'][$initials] = $row['con_id'];
    }

    return $row['con_id'];
}

/*
 * @param $currentWhere     Existing where clause to which the security where clause will be appended to.
 * @param $module           Module code, default 'INC'.
 * @param $initials         User's initials, default empty string.
 * @param $tableSearch      The table name to be substituted e.g. incidents_main, default empty string.
 * @param $tableReplace     The replacement table name e.g. incidents_report, default empty string
 * @param $email            Used to specify whether the where clause is to be built for the purpose of sending e-mails.
 *                          The use of the main application WHERE clause will depend upon the the value of
 *                          DIF_1_EMAIL_SELECT instead of RISK_WHERE_OVERRIDE.
 *                          DIF_1_EMAIL_SELECT = 'Y' use main app WHERE clause when e-mailling
 *                          DIF_WHERE_OVERRIDE = 'N' use main app WHERE clause when filtering records. default value is false.
 * @param $useCache         By default the system will use the cached WHERE clause if it has already been generated.
 *                          This parameter can be set to false to force the system to rebuild the WHERE clause from the
 *                          information stored in the database, like on the security settings screens. default value true.
 * @param $parent_module    Parent module, default value empty string.
 * @param $recursiveHistory To prevent infinite loops in CQC modules. default value is empty array.
 */
function MakeSecurityWhereClause($currentWhere, $module = "INC", $initials = "", $tableSearch = "", $tableReplace = "",
                                 $email = false, $useCache = true, $parent_module = "", $recursiveHistory = array(), $recordid = null)
{
    global $ModuleDefs;

    //This makes sure that $useCache is not used if devNoCache is on
    $useCache = ($useCache && !$GLOBALS['devNoCache']);

    //Sets the initials as the current user if not set
    $initials = ($initials == "") ? $_SESSION["initials"] : $initials;

    require_once 'Source/security/SecurityBase.php';

    if ($row['con_id'] = GetConIdFromInitials($initials))
    {
        // for performance, if we are building the where clause for the currently logged on user
        // re-use values stored in session vars
        if ($useCache === true && $email === false && $_SESSION["logged_in"] &&
            $_SESSION["contact_login_id"] == $row['con_id']
            && isset($_SESSION["permission_where"][$module]))
        {
            $permissionWhere = $_SESSION["permission_where"][$module];
        }
        else
        {
            // Check wether to use security groups or user location/type settings
            $groups = GetUserSecurityGroups($row['con_id']);

            if (count($groups) > 0)
            {
                // Get the e-mail or record access security groups depending on the type of WHERE clause to be built
                $groups = GetUserSecurityGroups($row['con_id'], ($email ? GRP_TYPE_EMAIL : GRP_TYPE_ACCESS));

                if (count($groups) > 0)
                {
                    foreach ($groups as $grp_id)
                    {
                        $groupWhere = MakeGroupSecurityWhereClause($module, $grp_id, $tableSearch, $tableReplace, $email);

                        if (!empty($groupWhere))
                        {
                            $where[] = $groupWhere;
                        }
                    }
                }
                else
                {
                    $where[] = "1=2";
                }

                //Get Deny access security groups and WHERE clauses
                $groups = GetUserSecurityGroups($row['con_id'], GRP_TYPE_DENY_ACCESS);

                if (count($groups) > 0)
                {
                    foreach ($groups as $grp_id)
                    {
                        $groupWhere = MakeGroupSecurityWhereClause($module, $grp_id, $tableSearch, $tableReplace, $email);

                        if (!empty($groupWhere))
                        {
                            $denywhere[] = $groupWhere;
                        }
                    }
                }
            }
            else
            {
                $userWhere = MakeUserSecurityWhereClause($module, $row['con_id'], $tableSearch, $tableReplace, $email);

                if ($userWhere)
                {
                    $where[] = $userWhere;
                }
            }

            /* Make record level WHERE clause */
            /* Record level security should only be applied if a group WHERE clause */
            /* and/or a user WHERE clause is present otherwise the user might end up with restricted access */
            /*exception is Reporting where the record level security is restrictive and must be applied all the time.*/
            if (!$email)
            {
                if ($module == 'REP' || $module == 'TEM')
                {
                    $RecordWhere = MakeRecordWherePermsClause($module, $ModuleDefs[$module]['TABLE'], $row['con_id'], $parent_module);

                    if ($RecordWhere)
                    {
                        $where[] = $RecordWhere;
                    }
                    else
                    {
                        $where[] = '1=2';
                    }
                }
                elseif (bYN(GetParm('SEC_RECORD_PERMS', 'N')))
                {
                    if (!empty($where))
                    {
                        $RecordWhere = MakeRecordWherePermsClause($module, $ModuleDefs[$module]['TABLE'], $row['con_id'], $parent_module);

                        if ($RecordWhere)
                        {
                            $where[] = $RecordWhere;
                        }
                    }
                }
            }

            if (!empty($where))
            {
                $permissionPreWhere[] = '('.implode(' OR ', $where).')';
            }

            if (!(count($groups) > 0 && bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS', 'N'))))
            {
                // Make top level WHERE clause
                // "Restrict managers to approving and viewing their own incidents" setting has to be applied
                // on top of user and group security settings (AND)
                $toplevelWhere = MakeToplevelSecurityWhereClause($module, $row['con_id'], false, $recordid);
            }

            if (isset($toplevelWhere) && $toplevelWhere)
            {
                $permissionPreWhere[] = $toplevelWhere;
            }

            if (!empty($permissionPreWhere))
            {
                $permissionWhere = implode(' AND ', $permissionPreWhere);
            }

            // Add module-specific custom security before deny group exceptions.
            // $recursivehistory prevents this being called multiple times.
            if (!in_array($module, $recursiveHistory))
            {
                $recursiveHistory[] = $module;
                $moduleWhereClauses = getModuleWhereClause($module, $connector, $recursiveHistory);

                if (!empty($moduleWhereClauses['OR']))
                {
                    $permissionWhere = $permissionWhere . ($permissionWhere ? " OR " : "") . '(' . implode(') OR (', $moduleWhereClauses['OR']) . ')';
                }

                if (!empty($moduleWhereClauses['AND']))
                {
                    $permissionWhere = ($permissionWhere ? "($permissionWhere) AND " : "") . '(' . implode(') AND (', $moduleWhereClauses['AND']) . ')';
                }
            }

            if (!empty($denywhere))
            {
                $permissionDenyWhere = implode(' AND ', $denywhere);

                if (!empty($permissionWhere))
                {
                    $permissionWhere = "({$permissionWhere}) AND ({$permissionDenyWhere})";
                }
                else
                {
                    $permissionWhere = "({$permissionDenyWhere})";
                }
            }
        }
    }

    if ($email === false && $_SESSION["logged_in"] && $_SESSION["contact_login_id"] == $row['con_id'] && $useCache)
    {
        $_SESSION["permission_where"][$module] = $permissionWhere;
    }

    if (is_array($currentWhere) && !empty($currentWhere))
    {
        $returnWhere = implode(' AND ', $currentWhere);
    }
    elseif ($currentWhere)
    {
        $returnWhere = $currentWhere;
    }

    if ($returnWhere)
    {
        $returnWhere = '('.$returnWhere.')';
    }

    if ($permissionWhere)
    {
        $permissionWhere = "({$permissionWhere})";
    }

    $returnWhere = $returnWhere . (($returnWhere && $permissionWhere) ? " AND " : "") . ($permissionWhere ? '('.$permissionWhere.')' : '');

    $returnWhere = TranslateWhereCom($returnWhere, "", $module);

    if ($returnWhere)
    {
        // Also replace any occurrences of the table specified
        if ($tableSearch && $tableReplace && \UnicodeString::strtoupper($tableSearch) <> \UnicodeString::strtoupper($tableReplace))
        {
            $returnWhere = str_ireplace($tableSearch . ".", $tableReplace . ".", $returnWhere);
        }
    }

    return $returnWhere;
}

/**
* @desc Checks whether a user can only see incidents for which they are the handler. Checks XXX_OWN_ONLY global, which
* only works for users who are not final approvers or level 1 submitters.
*
* @param string $module The current module
* @param string $login The login of the user to check
* @param string $userParm The access level of the user to check
*
* @return bool True if the user can only see records for which they are the handler,
 * false if they have no such restriction.
*/
function RestrictToOwnOnly($module, $login, $userParm)
{
    global $ModuleDefs;

    if (($module == "INC" && bYN(GetUserParm($login, "DIF_OWN_ONLY")) && $userParm != "RM" && $userParm != "DIF1"
        || $module == "PAL" && bYN(GetUserParm($login, "PAL_OWN_ONLY")) && $userParm != "RM" && $userParm != "PAL1"
        || $module == "RAM" && bYN(GetUserParm($login, "RISK_OWN_ONLY")) && $userParm != "RM" && $userParm != "RISK1"
        || $module == "COM" && bYN(GetUserParm($login, "COM_OWN_ONLY")) && $userParm != "COM2" && $userParm != "COM1"
        || $module == "ACT" && bYN(GetUserParm($login, "ACT_OWN_ONLY")))
        && $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'])
    {
        return true;
    }

    return false;
}

function MakeUserSecurityWhereClause(
    $module,            // module code
    $con_id,            // user's contact ID
    $tableSearch = "",  // the table name to be substituted e.g. incidents_main
    $tableReplace = "", // the replacement table name e.g. incidents_report
    $email = false      // Used to specify whether the where clause is to be built for the purpose of sending e-mails
                        // in which case the use of the main application WHERE clause will depend upon the
                        // the value of DIF_1_EMAIL_SELECT instead of RISK_WHERE_OVERRIDE
                        // DIF_1_EMAIL_SELECT = 'Y' use main app WHERE clause when e-mailling
                        // DIF_WHERE_OVERRIDE = 'N' use main app WHERE clause when filtering records
    )
{
    global $ModuleDefs;

    $userParameters = GetUserParameters($con_id);

    $modParams = array(
        "RAM" => array(
            "handler" => "ram_handler",
            "email_select" => bYN($userParameters["RISK_1_EMAIL_SELECT"]),
            "where_override" => bYN($userParameters["RISK_WHERE_OVERRIDE"])
        ),
        "PAL" => array(
            "handler" => "pal_handler",
            "email_select" => bYN($userParameters["PAL_1_EMAIL_SELECT"]),
            "where_override" => bYN($userParameters["PAL_WHERE_OVERRIDE"])
        ),
        "COM" => array(
            "handler" => "com_mgr",
            "email_select" => bYN($userParameters["COM_1_EMAIL_SELECT"]),
            "where_override" => bYN($userParameters["COM_WHERE_OVERRIDE"])
        ),
        "INC" => array(
            "handler" => "inc_mgr",
            "email_select" => bYN($userParameters["DIF_1_EMAIL_SELECT"]),
            "where_override" => bYN($userParameters["DIF_WHERE_OVERRIDE"])
        ),
        "ACT" => array(
            "handler" => "act_to_inits",
            "email_select" => false,
            "where_override" => false
        ),
        "HOT" => array(
            "handler" => "hot_handler",
            "email_select" => false,
            "where_override" => false
        )
    );

    if (!array_key_exists($module, $modParams))
    {
        // Defaults for other modules
        $modParams[$module] = array(
            "handler" => null,
            "email_select" => false,
            "where_override" => false
            );
    }

    $where = array();

/*====================================================================================================

If building where clause for restricting access to records:

    If *_WHERE_OVERRIDE = Y :
        > If it is set use the WHERE clause from main application INSTEAD of the DATIXWeb location and type settings
        > Use the DATIXWeb location and type settings if the main application WHERE clause is not set

    If *_WHERE_OVERRIDE = N :
        > Both the DATIXWeb location and type settings AND the main application WHERE clause
        > Users will only have access to records which are defined by BOTH the DATIXWeb location and type settings
            AND the main application WHERE clause

    If *_OWN_ONLY = Y - On top of applying the WHERE clause rules above:
        > Users will have access to records where they are the handler
        > Users will also have access to records where the handler is blank

If building where clause for sending email notifications:

    If *_1_EMAIL_SELECT = Y - Apply the same rule as when restricting access to records:

    If *_1_EMAIL_SELECT = N :
        > Only use the DATIXWeb location and type settings
        > Do not use the main application WHERE clause.

//==================================================================================================*/

    if ($email === false || $email === true && $modParams[$module]['email_select'] === true)
    {
        // Get main application WHERE clause
        $sql = "SELECT permission from contacts_main where recordid = :recordid";
        $row = PDO_fetch($sql, array("recordid" => $con_id));

        require_once 'Source/security/SecurityBase.php';
        $permission = ParsePermString($row["permission"]);
        $permissionWhere = $permission[$module]["where"];

        if ($permissionWhere)
        {
            // Strip out anything which excludes unapproved incidents.
            $permissionWhere = str_replace("and (incidents_main.rep_approved != 'N' or incidents_main.rep_approved is null)", "", $permissionWhere);
            $permissionWhere = str_replace("(incidents_main.rep_approved != 'N' or incidents_main.rep_approved is null)", "", $permissionWhere);
            $where[] = '(' . $permissionWhere . ')';
        }
    }

    if (
        ($email === false || $email === true && $modParams[$module]['email_select'] === true)
        && ($modParams[$module]['where_override'] === true && !$permissionWhere ||
        $modParams[$module]['where_override'] === false)
        || ($email === true && $modParams[$module]['email_select'] === false)
    )
    {
        $sql =
        "select parameter, value from
        (select p.parameter as parameter, p.parmvalue as [value], c.recordid as con_id from
        user_parms p
        join contacts_main c on p.login = c.login
        union select 'INC_ORGANISATION' as parameter, sta_orgcode as [value], recordid as con_id
        from contacts_main where sta_orgcode is not null and sta_orgcode != ''
        union select 'INC_CLINGROUP' as parameter, sta_clingroup as [value] , recordid as con_id
        from contacts_main where sta_clingroup is not null and sta_clingroup != ''
        union select 'INC_UNIT' as parameter, sta_unit as [value] , recordid as con_id
        from contacts_main where sta_unit is not null and sta_unit != ''
        union select 'INC_DIRECTORATE' as parameter, sta_directorate as [value] , recordid as con_id
        from contacts_main where sta_directorate is not null and sta_directorate != ''
        union select 'INC_SPECIALTY' as parameter, sta_specialty as [value] , recordid as con_id
        from contacts_main where sta_specialty is not null and sta_specialty != ''
        ) as user_parameters
        where con_id = :con_id
        AND (parameter like '{$module}_ORGANISATION'
        OR parameter like '{$module}_UNIT'
        OR parameter like '{$module}_CLINGROUP'
        OR parameter like '{$module}_DIRECTORATE'
        OR parameter like '{$module}_SPECIALTY'
        OR parameter like '{$module}_LOCTYPE'
        OR parameter like '{$module}_LOCATION'
        OR parameter like '{$module}_LOCACTUAL'
        OR parameter like 'DIF_TYPECAT_PERMS' AND '{$module}' = 'INC')";

        $request = PDO_fetch_all($sql, array("con_id" => $con_id));

        foreach ($request as $row)
        {
            if ($row['value'])
            {
                if ($row['parameter'] == "DIF_TYPECAT_PERMS")
                {
                    $TypeCatArray = explode(":", $row['value']);

                    foreach ($TypeCatArray as $TypeCat)
                    {
                        if ($TypeCat != "" && $TypeCat != " ")
                        {
                            $TC2 = explode(" ", $TypeCat);
                            $TCWhere = "inc_type = '" . $TC2[0] . "'";
                            if ($TC2[1] != "")
                                $TCWhere .= " AND inc_category = '{$TC2[1]}'";
                            $TypeCatWheres[] = $TCWhere;
                        }
                    }

                    if ($TypeCatWheres)
                    {
                        $where[] = "(" . implode(" OR ", $TypeCatWheres) . ")";
                    }
                }
                else
                {
                    $Table = GetTableForField(\UnicodeString::strtolower($row[parameter]), $module);

                    $where[] = "($Table.$row[parameter] IN (" . MakeInClause($row['value']) . ")"
                    . " OR $Table.$row[parameter] IS NULL OR $Table.$row[parameter] = '')";
                }
            }
        }
    }

    if (!empty($where))
    {
        $returnWhere = implode(' AND ', $where);
    }

    if ($returnWhere)
    {
        $returnWhere = \UnicodeString::strtoupper($returnWhere);
        $tableSearch = \UnicodeString::strtoupper($tableSearch);
        $tableReplace = \UnicodeString::strtoupper($tableReplace);

        // Also replace any occurrences of the table specified
        if ($tableSearch && $tableReplace && $tableSearch <> $tableReplace)
        {
            $returnWhere = str_replace($tableSearch . ".", $tableReplace . ".", $returnWhere);
        }
    }

    return $returnWhere;
}

function GetUserParameters($con_id)
{
    // Retrieve global parameters using a user's contact ID
    // User parameters override global parameters,

    //check for cached values.
    if ($_SESSION['UserParameters'][$con_id])
    {
        return $_SESSION['UserParameters'][$con_id];
    }

    $globals = array();

    // Get global parameters
    $sql = "SELECT parameter, parmvalue as [value] FROM globals";
    $globals = PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);

    // Get user parameters
    $sql = "SELECT user_parms.parameter, user_parms.parmvalue as [value]
    FROM user_parms
    join contacts_main on user_parms.login = contacts_main.login
    WHERE contacts_main.recordid = :con_id";
    $result = PDO_fetch_all($sql, array("con_id" => $con_id));

    foreach ($result as $row)
    {
        $globals[$row["parameter"]] = $row["value"];
    }

    $_SESSION['UserParameters'][$con_id] = $globals;

    return $globals;
}

function printField($parameterArray)
{
    $data = $parameterArray['data'];
    $FormType = $parameterArray['formtype'];
    $field = $parameterArray['field'];

    switch ($field)
    {
        case "link_complpat":
            return '
    <script language="JavaScript" type="text/javascript">
        function checkHideComplPat(el)
        {
            //log(el.parentNode.parentNode.parentNode.parentNode);

            if (el.options[el.selectedIndex].value!=\'Y\')
            {
                el.parentNode.parentNode.parentNode.parentNode.rows[(el.parentNode.parentNode.rowIndex)+1].style.display="";
                document.getElementById(\'person_affected\').style.display = "";
            }
            else
            {
                el.parentNode.parentNode.parentNode.parentNode.rows[(el.parentNode.parentNode.rowIndex)+1].style.display="none";
                document.getElementById(\'person_affected\').style.display = "none";
            }
        }
    </script>

    <select name="link_complpat_1" id="link_complpat_1" onchange="setChanged(\'link_complpat_1\');checkHideComplPat(this)" >
    <option value="" selected="selected">Choose</option>
    <option value="Y">Yes</option>
    <option value="N">No</option>
    </select><input type="hidden" name="CHANGED-link_complpat_1" id="CHANGED-link_complpat_1" />';
            break;
        case "link_patrelation":
            $return_html = '
            <select name="link_patrelation_1" id="link_patrelation_1" >
            ';

            $sql = "SELECT description FROM code_relationship";

            $result = db_query($sql);
            while ($row=db_fetch_array($result))
            {
                $return_html .= '<option value="'.$row["description"].'">'.$row["description"].'</option>
                ';
            }
            $return_html .= '</select>';

            return $return_html;
            break;
    }
}

function GetPagedResults (
    $table,                 // The database table from which to select the data
    $columns,               // The columns to be select from the table
    $where = "",            // An optional WHERE clause to restrict the number of
                            // records returned
    $sortcolumns,           // The columns on which to sort the data returned
    $sortorder = "ASC",     // The sort order for the specified columns
    $pageNumber = 1,        // The page number to be displayed
    &$recordsPerPage,       // The number of records to be displayed on each page
                            // Can be set to custom value, otherwise
                            // it takes the value from LISTING_DISPLAY_NUM
                            // It will then be set to the actual number of records returned
    &$totalRecordCount,     // The overral number of records, including all possible pages,
                            // for display purposes
    &$actualRecordCount,    // The actual number of records returned for the page,
                            // useful for last page if less than $recordsPerPage is returned
    &$sql                   // The overral SQL statement for debug purposes
    )
{
    require_once 'Source/libs/SavedQueries.php';
    $_SESSION[$_GET['module']]["PROMPT"]["ORIGINAL_WHERE"] = $where;

    DoPromptSection(array('module' => Sanitize::getModule($_GET['module'])));

    if ($_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"])
    {
        $where = $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"];
        $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"] = '';
    }

    if (!$recordsPerPage)
    {
        $recordsPerPage = GetParm("LISTING_DISPLAY_NUM", 20);
    }

    if ($pageNumber == 0)
    {
        $pageNumber = 1;
    }

    if (!is_array($columns))
    {
        $columns = str_replace(" ", "", $columns);
        $columns = explode(",", $columns);
    }

    if (!is_array($sortcolumns))
    {
        $sortcolumns = str_replace(" ", "", $sortcolumns);
        $sortcolumns = explode(",", $sortcolumns);
    }

    // Make sure that any sort column is also selected
    foreach (array_diff($sortcolumns, $columns) as $sortCol)
    {
        if (!isset($columns[$sortCol]))
        {
            $columns[$sortCol] = $sortCol;
        }
    }

    $sortorder = \UnicodeString::strtoupper($sortorder);
    $columns = implode(", ", $columns);
    $sortcolumns_rev = implode(($sortorder == "ASC" ? " DESC," : " ASC,"), $sortcolumns) . ($sortorder == "ASC" ? " DESC" : " ASC");
    $sortcolumns = implode(" $sortorder,", $sortcolumns) . " $sortorder";

    // Get overal number of records
    $sql = "SELECT count(*) as [count] FROM $table" . ($where ? " WHERE $where" : "");
    $request = db_query($sql);
    $row = db_fetch_array($request);
    $totalRecordCount = $row['count'];

    // Create most inner SELECT
    $sql = "SELECT TOP " . ($recordsPerPage*$pageNumber > $totalRecordCount ? $totalRecordCount : $recordsPerPage*$pageNumber)
    . " $columns FROM $table"
    . ($where ? " WHERE $where" : "")
    . " ORDER BY $sortcolumns";

    // Create SELECT to requested range of records
    // and nest previous SELECT

    $sql = "SELECT TOP " . ($recordsPerPage*$pageNumber > $totalRecordCount ? ($totalRecordCount - $recordsPerPage*($pageNumber-1)) : $recordsPerPage)
    . " $columns FROM ($sql) AS [i]"
    . " ORDER BY $sortcolumns_rev";

    // Create outer SELECT to reverse the data in the correct sort order
    $sql = "SELECT $columns FROM ($sql) AS [j] ORDER BY $sortcolumns";

    $request = db_query($sql);

    $actualRecordCount = count($request->fetchAll());

    return db_query($sql);
}

function GetPostedContacts($Data, $Type = '')
{
    $TotalContacts = $Data['contact_max_suffix'] + 1;

    for ($i = 1; $i < $TotalContacts; $i++ )
    {
        if ($Type == $Data['link_type_'.$i] || $Type == '')
        {
            $ContactSuffixes[] = $i;
        }
    }

    return $ContactSuffixes;
}

function GetTotalNumContacts($Data)
{
    $Total = 0;

    if (is_array($Data['con']))
    {
        foreach ($Data['con'] as $contacttype => $contactlist)
        {
            $Total += count($contactlist);
        }
    }

    return $Total;
}

function MakeDynamicContactSection($aParams)
{
    global $MaxSuffixField, $ModuleDefs, $MaxSuffix;

    $Data   = $aParams['data']; //need to check whether there are multiple sections to put into the form.
    $module = $aParams['module'];
    $ContactsToShow = array(); // initialise variables!

    $aParams['contacttype'] = filter_var($aParams['contacttype'], FILTER_SANITIZE_STRING);

    if (!empty($Data['con']) && !$Data['temp_record'])
    {
        $NumToShow = count($Data['con'][$aParams['contacttype']]);

        if (!$MaxSuffixField)
        {
            echo '<input type="hidden" name="max_suffix" id="max_suffix" value="'.(GetTotalNumContacts($Data)+9).'" />';
        }

        if (!$NumToShow)
        {
            if ($aParams['FormType'] != 'Print' && $aParams['FormType'] != 'ReadOnly')
            {
                $NumToShow = 1;
            }
        }
    }
    else
    {
        if ($Data['contact_max_suffix'])
        {
            $ContactsToShow = GetPostedContacts($Data, $aParams['contacttype']);
            $aParams['suffixpresent'] = true; //prevent additional suffix being added.
            $NumToShow = count($ContactsToShow);
        }
        elseif ($Data['temp_record'])
        {
            $ContactsToShow = $Data['con'][$aParams['contacttype']];
            $NumToShow = count($ContactsToShow);
        }

        if (!$NumToShow)
        {
            $ContactsToShow[] = $aParams['suffix'];

            if ($aParams['FormType'] != 'Print' && $aParams['FormType'] != 'ReadOnly')
            {
                $NumToShow = 1;
            }
        }

        if (!$MaxSuffixField)
        {
            echo '<input type="hidden" name="max_suffix" id="max_suffix" value="'.($Data['max_suffix'] ? Sanitize::SanitizeInt($Data['max_suffix']) : '9').'" />';
        }
    }

    if ($MaxSuffix)
    {
        $MaxSuffix += $NumToShow;
    }
    else
    {
        $MaxSuffix = 6;
    }

    for ($i=0; $i<$NumToShow; $i++)
    {
        if (($_SESSION['logged_in'] && !isset($Data['error']) && !isset($Data['dif1_snapshot'])) || $Data['temp_record'])
        {
            $aParams['data'] = Sanitize::SanitizeString($Data['con'][$aParams['contacttype']][$i]);

            if ($i != 0 || !$aParams['suffix']) //first contact should take the default suffix
            {
                $aParams['suffix'] = $i + $MaxSuffix;
            }
        }
        else
        {
            if (!empty($ContactsToShow[$i]))
            {
                $aParams['suffix'] = $ContactsToShow[$i];
            }
        }

        if ($i == 0)
        {
            $aParams['clear'] = true;
        }

        echo '

        <div id="contact_section_div_'.Sanitize::SanitizeString($aParams['suffix']).'">';

        get_contact_section($aParams);

        echo '</di>';
    }

    if ($aParams['FormType'] != 'ReadOnly' && $aParams['FormType'] != 'Print' &&
        ($NumToShow < $ModuleDefs[$module]['LEVEL1_CON_OPTIONS'][$aParams['contacttype']]['Max'] ||
            $ModuleDefs[$module]['LEVEL1_CON_OPTIONS'][$aParams['contacttype']]['Max'] == 0) )
    {
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        echo '
            <li class="new_windowbg" id="add_another_'.Sanitize::SanitizeString($aParams['contacttype']).'_button_list">
                <input type="button" id="contact_section_'.Sanitize::SanitizeString($aParams['contacttype']).'" value="'._tk('btn_add_another').'" onclick="AddSectionToForm(\'contact\', \''.Sanitize::SanitizeString($aParams['contacttype']).'\', $(\'add_another_'.Sanitize::SanitizeString($aParams['contacttype']).'_button_list\'), \''.$module.'\', \'\', false, '.$spellChecker.', '.(is_numeric($_REQUEST['form_id']) ? Sanitize::SanitizeInt($_REQUEST['form_id']) : 'null').');" />
            </li>';
    }

    if (!$MaxSuffixField)
    {
        echo '<script language="JavaScript" type="text/javascript">dif1_section_suffix[\'contact\'] = 10</script>';
        $MaxSuffixField = true;
    }
}

function MakeDynamicDocumentSection($aParams)
{
    $Data = $aParams['data']; //need to check whether there are multiple sections to put into the form.

    if ($Data['document_max_suffix'])
    {
        $NumToShow = $Data['document_max_suffix'];
    }

    if (!$NumToShow)
    {
        $NumToShow = 1;
    }

    echo '<input type="hidden" name="max_doc_suffix" id="max_doc_suffix" value="'.$NumToShow.'" />';

    for ($i = 1; $i <= $NumToShow; $i++)
    {
        if (($i == 1) || ($Data['doc_type_'.$i] && $Data['doc_notes_'.$i]))
        {
            $aParams['suffix'] = $i;

            if ($i == 1)
            {
                $aParams['clear'] = true;
            }

            echo '<div id="document_section_div_'.$aParams['suffix'].'">';

            get_document_section($aParams);

            echo '</div>';
        }
    }

    $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
    
    echo '
        <li class="new_windowbg" id="add_another_document_button_list">
            <script language="javascript">dif1_section_suffix[\'document\'] = ' . (($Data['document_max_suffix']) ? Sanitize::SanitizeString($Data['document_max_suffix']) : 2) . '</script>
            <input type="button" id="documents" value="'._tk('btn_add_another').'" onclick="AddSectionToForm(\'document\', \'\', $(\'add_another_document_button_list\'), \''.$aParams['module'].'\', \'\', false, '.$spellChecker.', null);">
        </li>';
}

function get_contact_section($aParams = array())
{
    global $ClientFolder, $FormDesigns, $ModuleDefs, $JSFunctions, $FieldDefs;

    require_once 'Source/libs/FormClasses.php';
    $FormType = $aParams['FormType'];
    $data = $aParams['data'];
    $AJAX = (empty($aParams));

    if ($AJAX)
    {
        $aParams = $_POST;
    }

    $Module = Sanitize::SanitizeString($aParams['module']);
    $Type   = Sanitize::SanitizeString($aParams["contacttype"]);
    $Suffix = Sanitize::SanitizeString($aParams["suffix"]);
    $clear  = Sanitize::SanitizeString($aParams["clear"]);

    if ($Suffix && !$aParams['suffixpresent'] && !empty($data))
    {
        $Rows = getContactLinkFields($Type);

        $OtherRows = array(
            'recordid', 'link_recordid', 'link_exists', 'con_id', 'updateid','show_injury',
            'lcom_iscomplpat', 'lcom_primary'
        );

        $Rows = array_merge($Rows, $OtherRows, $ModuleDefs['CON']['FIELD_ARRAY']);

        $data = AddSuffixToData($data, $Suffix, array('Rows'=>$Rows));
    }

    $OptionArray = $ModuleDefs[$Module]['LEVEL1_CON_OPTIONS'];

    $Attributes = $OptionArray[$Type];

    $SectionName = 'contact_'.$Suffix;

    if (empty($data['recordid_'.$Suffix]))
    {
        $data['con_dopened_'.$Suffix] = date('Y-m-d H:i:s.000');
    }

    $contactFormDesign = Forms_FormDesign::GetFormDesign(array(
        'module' => 'CON',
        'level' => Sanitize::SanitizeString($aParams['level']),
        'parent_level' => Sanitize::SanitizeString($aParams['level']),
        'parent_module' => $Module,
        'link_type' => $Type
    ));
    $contactFormDesign->AddSuffixToFormDesign($Suffix, $ModuleDefs[$Module]['LEVEL1_CON_OPTIONS'][$Type]['Title']);

    $contactTable = new FormTable(Sanitize::SanitizeString($aParams['FormType']), 'CON', $contactFormDesign);

    $Title = '<div>'._t($Attributes["Title"]).'</div>';

    if ($aParams['FormType'] != 'ReadOnly' && $aParams['FormType'] != 'Print')
    {
        if (!$clear)
        {
            $RightHandLink['onclick'] = 'var DivToDelete=$(\'contact_section_div_'.$Suffix.'\');DivToDelete.parentNode.removeChild(DivToDelete)';
            $RightHandLink['text'] = _tk('delete_section');
        }
        else
        {
            if (is_numeric($_REQUEST['form_id']))
            {
                $parentFormId = Sanitize::SanitizeInt($_REQUEST['form_id']);
            }
            elseif (is_numeric($_REQUEST['parent_form_id']))
            {
                $parentFormId = Sanitize::SanitizeInt($_REQUEST['parent_form_id']);
            }
            else
            {
                $parentFormId = '';
            }

            $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
            $RightHandLink['onclick'] = 'ReplaceSection(\''.$Module.'\','.$Suffix.', \'contact\', \''.$Type.'\', '.$spellChecker.', \''.$parentFormId.'\')';
            $RightHandLink['text'] = _tk('clear_section');
        }
    }

    $GLOBALS["CON"]["form_id"] = GetParm($Attributes["Global"]);

    require_once 'Source/incidents/Injuries.php';

    $Role = $Attributes["Role"];
    $Show = $Attributes["Show"];

    include "Source/contacts/BasicForm_Simple.php";

    //set link role
    if ($Role)
    {
        $FormArray[$SectionName]["LinkRole"] = $Role;
    }

    if (bYN(GetParm('CON_PAS_CHECK', 'N')))
    {
        if (is_array($GLOBALS["ContactMatch"]))
        {
            // Add external contact search button to fields.
            $ConMatchFields = array_keys($contactFormDesign->ContactMatch);

            foreach ($ConMatchFields as $FieldName)
            {
                if ($contactFormDesign->ContactMatch[$FieldName])
                {
                    $FieldName = \UnicodeString::substr($FieldName, 0, (\UnicodeString::strlen($FieldName) - \UnicodeString::strlen("_" . $Suffix)));
                    $contactTable->FieldDefs[$FieldName]['OriginalType'] = $FieldDefs['CON']["$FieldName"]["Type"];
                    $contactTable->FieldDefs["$FieldName"]["Type"] = "string_con_search";
                }
            }
        }
    }

    $contactTable->MakeForm($FormArray, $data, $Module, array('dynamic_section' => true));

    $returnHTML = $contactTable->GetFormTable();

    $returnHTML .= '<input type="hidden" id="rep_approved_'.$Suffix.'" name="rep_approved_'.$Suffix.'" value="'.\src\security\Escaper::escapeForHTMLParameter($data['rep_approved_'.$Suffix]).'" />';
    $returnHTML .= '<input type="hidden" id="link_recordid_'.$Suffix.'" name="link_recordid_'.$Suffix.'" value="'.\src\security\Escaper::escapeForHTMLParameter($data['link_recordid_'.$Suffix]).'" />';
    $returnHTML .= '<input type="hidden" id="con_id_'.$Suffix.'" name="con_id_'.$Suffix.'" value="'.\src\security\Escaper::escapeForHTMLParameter($data['con_id_'.$Suffix]).'" />';
    $returnHTML .= '<input type="hidden" id="updateid_'.$Suffix.'" name="updateid_'.$Suffix.'" value="'.\src\security\Escaper::escapeForHTMLParameter($data['updateid_'.$Suffix]).'" />';

    if ($Attributes["DivName"])
    {
        $returnHTML .= '<input type="hidden" id="contact_div_name_'.$Suffix.'" name="contact_div_name_'.$Suffix.'" value="'.$Attributes["DivName"].'" />';
    }

    if ($data['link_exists_'.$Suffix])
    {
        $returnHTML .= '<input type="hidden" id="link_exists_'.$Suffix.'" name="link_exists_'.$Suffix.'" value="1" />';
    }

    echo $returnHTML;

    unset($GLOBALS["CON"]["form_id"]);
}

function StoreContactLabels()
{
    //Store contact labels in session for later use in dropdown popups
    if (!is_array($_SESSION["CONTACTLABELS"]))
    {
        $_SESSION['CONTACTLABELS'] = array();
    }

    if (is_array($GLOBALS["UserLabels"]))
    {
        $_SESSION['CONTACTLABELS'] = array_merge($_SESSION['CONTACTLABELS'], $GLOBALS["UserLabels"]);
    }
}

function RemoveUDFGroups()
{
    if (is_array($GLOBALS["OrderSections"]) && is_array($GLOBALS["DIF1UDFGroups"]))
    {
        foreach ($GLOBALS["DIF1UDFGroups"] as $groupid => $group)
        {
            foreach ($GLOBALS["OrderSections"] as $id => $section)
            {
                if ($section == "UDFGROUP".$group)
                {
                    unset($GLOBALS["OrderSections"][$id]);
                }
            }
        }
    }

    unset($GLOBALS["DIF1UDFGroups"]);
}

function CompareContacts($contactA, $contactB)
{
    $contactA["con_surname"] = \UnicodeString::strtoupper($contactA["con_surname"]);
    $contactB["con_surname"] = \UnicodeString::strtoupper($contactB["con_surname"]);
    $contactA["con_forenames"] = \UnicodeString::strtoupper($contactA["con_forenames"]);
    $contactB["con_forenames"] = \UnicodeString::strtoupper($contactB["con_forenames"]);
    $contactA["con_title"] = \UnicodeString::strtoupper($contactA["con_title"]);
    $contactB["con_title"] = \UnicodeString::strtoupper($contactB["con_title"]);

    if ($contactA["con_surname"] == $contactB["con_surname"])
    {
        if ($contactA["con_forenames"] == $contactB["con_forenames"])
        {
            if ($contactA["con_title"] == $contactB["con_title"])
            {
                return 0;
            }
            elseif ($contactA["con_title"] < $contactB["con_title"])
            {
                return -1;
            }
        }
        elseif ($contactA["con_forenames"] < $contactB["con_forenames"])
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
    elseif ($contactA["con_surname"] < $contactB["con_surname"])
    {
        return -1;
    }
    else
    {
        return 1;
    }
}

function get_document_section($aParams = array())
{
    global $JSFunctions;

    $AJAX = (empty($aParams));

    if ($AJAX)
    {
        $aParams = Sanitize::SanitizeRawArray($_POST);
    }

    $Suffix = $aParams["suffix"];
    $data = $aParams['data'];
    $clear = $aParams['clear'];

    $documentTable = new FormTable('', $aParams['module']);
    $FieldObj = new FormField();

    $Title = '<div class="section_title_group">New Document</div>';

    if ($aParams['FormType'] != 'ReadOnly' && $aParams['FormType'] != 'Print')
    {
        if (!$clear)
        {
            $Title .= '<div class="title_rhs_container"><div class="section_title_right_hand_link"
                onclick="
                    var DivToDelete=$(\'document_section_div_'.$Suffix.'\');DivToDelete.parentNode.removeChild(DivToDelete)"
                >'._tk('delete_section').'</div></div>';
        }
        else
        {
            if (is_numeric($_REQUEST['form_id']))
            {
                $parentFormId = Sanitize::SanitizeInt($_REQUEST['form_id']);
            }
            elseif (is_numeric($_REQUEST['parent_form_id']))
            {
                $parentFormId = Sanitize::SanitizeInt($_REQUEST['parent_form_id']);
            }
            else
            {
                $parentFormId = '';
            }

            $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
            $Title .= '<div class="title_rhs_container"><div class="section_title_right_hand_link"
                onclick="
                    ReplaceSection(\''.$Module.'\','.$Suffix.', \'document\', \'\', '.$spellChecker.', \''.$parentFormId.'\')"
                >'._tk('clear_section').'</div></div>';
        }
    }

    $documentTable->MakeTitleRow('<b>'.$Title.'</b>');

    // get link types for "Link as" drop down
    $sql = "SELECT code, description FROM code_doc_type ORDER BY description";
    $LinkTypes = PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);

    $LinkTitle = '<label for="doc_type_'.$Suffix.'_title"><img src="images/Warning.gif" alt="Warning" /> '._tk('doc_link_as').GetValidationErrors($data, "doc_type_".$Suffix).
        '<div id="errdoc_type_'.$Suffix.'" style="display:none"><font color="red">' . _tk("mandatory_err") . '</font></div></label><input type="hidden" id="show_field_doc_type_'.$Suffix.'" value="1" />';

    $field = Forms_SelectFieldFactory::createSelectField('doc_type_'.$Suffix, $Module, Sanitize::SanitizeString($data['doc_type_'.$Suffix]), '');
    $field->setCustomCodes($LinkTypes);
    $documentTable->MakeRow($LinkTitle, $field);

    $DescTitle = '<label for="doc_notes_' . $Suffix . '"><img src="images/Warning.gif" alt="Warning" /> '._tk('doc_description').GetValidationErrors($data, "doc_notes_".$Suffix).
        '<div id="errdoc_notes_'.$Suffix.'" style="display:none"><font color="red">' . _tk("mandatory_err") . '</font></div></label><input type="hidden" id="show_field_doc_notes_'.$Suffix.'" value="1" />';
    $documentTable->MakeRow($DescTitle, $FieldObj->MakeInputField('doc_notes_'.$Suffix, 50, 50, Sanitize::SanitizeString($data["doc_notes_".$Suffix]), ""));

    $UploadTitle = '<label for="userfile_'.$Suffix.'"><img src="images/Warning.gif" alt="Warning" /> '._tk('doc_attach_this_file').GetValidationErrors($data, "userfile_".$Suffix).
        '<div id="erruserfile_'.$Suffix.'" style="display:none"><font color="red">' . _tk("mandatory_err") . '</font></div></label><input type="hidden" id="show_field_userfile_'.$Suffix.'" value="1" />';
    $UploadField  = '<input contentEditable="false" name="userfile_'.$Suffix.'" id="userfile_'.$Suffix.'" type="file" size="50" />'.
        ($_FILES["userfile_".$Suffix]['name'] ? '<div><i>(You will need to re-select this file. The file was '.$_FILES["userfile_".$Suffix]['name'].')</i></div>' : '');

    $documentTable->MakeRow($UploadTitle, $FieldObj->MakeCustomField($UploadField));

    $documentTable->MakeTable();

    if ($aParams['FormType'] != 'ReadOnly' && $aParams['FormType'] != 'Print')
    {
        $JSFunctions[] = '
            mandatoryArray.push(new Array("doc_type_'.$Suffix.'","document_section_div_'.$Suffix.'","'._tk('doc_link').' (Document '.$Suffix.')"));
            mandatoryArray.push(new Array("doc_notes_'.$Suffix.'","document_section_div_'.$Suffix.'","'._tk('doc_desc').' (Document '.$Suffix.')"));
            mandatoryArray.push(new Array("userfile_'.$Suffix.'","document_section_div_'.$Suffix.'","'._tk('doc_attach').' (Document '.$Suffix.')"));
                       ';
    }

    echo $documentTable->GetFormTable();
}

/**
* @desc Turns a module id number into a three character module code (the inverse of {@link ModuleCodeToID()})
*
* @param int $mod_id The module id
*
* @return string The module code.
*/
function ModuleIDToCode($mod_id)
{
    $ModuleIdCodeList = GetModuleIdCodeList();
    return $ModuleIdCodeList[$mod_id];
}

/**
* @desc Turns a module code into a module id number (the inverse of {@link ModuleIDToCode()})
*
* @param string $mod_code The module code
*
* @return int The module id.
*/
function ModuleCodeToID($mod_code)
{
    $ModuleCodeIdList = GetModuleCodeIdList();
    return $ModuleCodeIdList[$mod_code];
}

/**
* @desc Returns a string of JavaScript to jump to the specified panel or section of a form.
*
* @param bool $Show_all_section 'Y' if all panels/sections are displayed on the one page, 'N' if panels are used
* @param string $panel The id of the panel to display.
* @param obj $Table The FormTable object describing the form.
*/
function JavascriptPanelSelect($Show_all_section, $panel, $Table, $Field = '')
{
    global $FieldSectionReference;

    if ($Field)
    {
        $panel = $FieldSectionReference[$Field];
    }

    // If the table does not have panels, we just want to return.
    if (!$Table->HasPanels())
    {
        return;
    }

    if ($panel)
    {
        if (bYN($Show_all_section))
        {
            $MySection = $panel;
        }
        else
        {
            // We need to check if this is a panel or a section
            $Panels = $Table->GetPanelArray();

            if ($Panels[$panel])
            {
                $MyPanel = $panel;
            }
            else //try to find the panel this section is in.
            {
                $MySection = $panel;

                foreach ($Panels as $panel_id => $sections)
                {
                    if (in_array($panel, $sections))
                    {
                        $MyPanel = $panel_id;
                    }
                }
            }
        }
    }

    if (!$MyPanel && count($Table->PanelArray) > 0)
    {
        $MyPanel = $Table->GetFirstPanel();
    }

    $HTML = '';

    if ($MyPanel)
    {
        $HTML .= '<script language="javascript" type="text/javascript">showFormPanel(\'panel-' . Escape::EscapeEntities($MyPanel) . '\')</script>';
    }

    if ($MySection)
    {
        $HTML .= '<script language="javascript" type="text/javascript">window.location.hash=\'' . Escape::EscapeEntities($MySection) . '\'</script>';
    }

    return $HTML;
}

/**
* @desc Gets a list of module ids, keyed by the codes (contrast with {@link GetModuleIdCodeList()}).
 * Includes a caching system to prevent huge numbers of queries.
* @return array Array of module ids.
*/
function GetModuleCodeIdList()
{
	if ($_SESSION["ModuleCache"])
	{
		$Modules = $_SESSION["ModuleCache"];  // need to flip because modulecache stores mod_code => mod_id
	}
	else
	{
        require_once 'constants.php';

        // Get list of module codes-id: array('mod_code' => 'mod_id')
        $sql = "SELECT mod_module, mod_id FROM modules union select 'ADM'," . MOD_ADMIN;
        $Modules = PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);
        $_SESSION["ModuleCache"] = $Modules;
	}

	return $Modules;
}

function CreatePermString($ModulePerms)
{
    if (empty($ModulePerms) || !is_array($ModulePerms))
    {
        return '';
    }

    $PermString = '';
    $PermArray = array();

    $Modules = GetModuleCodeIdList();

    $PermNos = Array(
        "ALL" => 0,
        "READ" => 1,
        "WRITE" => 2,
        "NONE" =>3
    );

    foreach ($ModulePerms as $ModStr => $ModPerms)
    {
        if (empty($ModPerms) || !is_array($ModPerms))
        {
            continue;
        }

        if (isset($ModPerms['form']) && is_array($ModPerms['form']))
        {
            foreach ($ModPerms['form'] as $SubformId => $PermValue)
            {
                $PermArray[] = ($Modules[$ModStr] * 256 + $SubformId) . ";" . $PermNos[$PermValue];
            }
        }

        if (isset($ModPerms['disallow_setup']) && $ModPerms['disallow_setup'] = true)
        {
            $PermArray[] = ($Modules[$ModStr] * 256 + SUB_SETUP) . ";" . $PermNos[$PermValue];
        }

        if (isset($ModPerms['secgroup']) && $ModPerms['secgroup'] != '' ||
            isset($ModPerms['seclevel']) && $ModPerms['seclevel'] != '' ||
            isset($ModPerms['where']) && $ModPerms['where'] != '')
        {
            $PermArray[] =
            ($Modules[$ModStr] * 256 + SUB_WHERE) .
            ";" . $ModPerms['secgroup'] .
            ";" . $ModPerms['seclevel'] .
            ";" . $ModPerms['where'];
        }
    }

    if (empty($PermArray))
    {
        return '';
    }

    $PermString = implode('|', $PermArray);

    return $PermString;
}

function translateStaticAtCodes($WhereClause)
{
    require_once 'Date.php';
    require_once 'SearchSymbols.php';

    while (preg_match("/@TODAY[^a-zA-Z0-9]/iu", $WhereClause) > 0)
    {
        $WhereClause = GetAtTodayDate($WhereClause);
    }

    if (\UnicodeString::stripos($WhereClause, "@LASTWEEK") !== false)
    {
        GetLastWeekDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@LASTWEEK', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@LASTMONTH") !== false)
    {
        GetLastMonthDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@LASTMONTH', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@LASTQUARTER") !== false)
    {
        GetLastQuarterDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@LASTQUARTER', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@LASTYEAR") !== false)
    {
        GetLastYearDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@LASTYEAR', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@WEEK") !== false)
    {
        GetWeekDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@WEEK', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@MONTH") !== false)
    {
        GetMonthDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@MONTH', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@QUARTER") !== false)
    {
        GetQuarterDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@QUARTER', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@YEAR") !== false)
    {
        GetYearDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@YEAR', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@FINYEAR") !== false)
    {
        GetFinYearDateRange( getdate(), $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@FINYEAR', $dtStart, $dtEnd );
    }

    if (\UnicodeString::stripos($WhereClause, "@LASTFINYEAR") !== false)
    {
        $dtToday = getdate();
        $dtToday["year"] = $dtToday["year"] - 1;
        GetFinYearDateRange($dtToday, $dtStart, $dtEnd );
        $WhereClause = TranslateDateRangeCom( $WhereClause, '@LASTFINYEAR', $dtStart, $dtEnd );
    }

    return $WhereClause;
}


function TranslateWhereCom($WhereClause, $Initials = "", $module = "", $allowNull = false)
{
    if ($Initials == "" && !$allowNull)
    {
        $Initials = $_SESSION["initials"];
    }

    $WhereClause = \UnicodeString::str_ireplace("@USER_INITIALS", $Initials, $WhereClause);

    $WhereClause = translateStaticAtCodes($WhereClause);

    if (\UnicodeString::stripos($WhereClause, "@USER_") !== false)
    {
        $WhereClause = TranslateConCode($WhereClause, $Initials, $module);
    }

    return $WhereClause;
}

/**
 * Only works for Level 2 forms
 * @deprecated deprecated since version 11.0
 */
function IncludeCurrentFormDesign($module = 'INC', $formlevel = 2, $FormID = null, $FormType = '')
{
    $FormDesign = Forms_FormDesign::GetFormDesign(array(
        'module' => $module,
        'level' => $formlevel,
        'id' => $FormID,
        'form_type' => $FormType
    ));
    $FormDesign->LoadFormDesignIntoGlobals();

    $aReturn['sSettingsFilename'] = $FormDesign->getFileName();

    return $aReturn;
}

function GetRecordLevelSecurityIds($module, $table, $con_id)
{
    $sql =
        "SELECT link_id
        FROM sec_record_permissions
        WHERE
            module = :module
            AND tablename = :tablename
            AND con_id = :con_id";

    $Where = '';

    $RecordIDs = PDO_fetch_all($sql, array(
        "module" => $module,
        "tablename" => $table,
        "con_id" => $con_id
    ), PDO::FETCH_COLUMN);

    /*
    "Packaged reports are accessible to all users by default. To restrict access,
    identify in the Permissions section the specific users / groups who should have access to the report."
    For this reason, add the packaged reports which do not have any permissions set to the RecordIDs collection. */
    if ($module == 'REP')
    {
        $sql =
           "SELECT recordid
            FROM reports_packaged
            WHERE
                recordid not in (
                    select link_id
                    from sec_record_permissions
                    where module = 'REP'
                )
            AND
                recordid not in (
                    select link_id
                    from sec_group_record_permissions
                    where module = 'REP'
                )";

        $rows = PDO_fetch_all($sql);

        foreach ($rows as $row)
        {
            $RecordIDs[] = $row['recordid'];
        }
    }

    ////////////////////////////////////////
    // same for document templates as above?
    //
    ////////////////////////////////////////
    if ($module == 'TEM')
    {
        $sql =
           "SELECT recordid
            FROM templates_main tm
            WHERE
                recordid not in (
                    select link_id
                    from sec_record_permissions
                    where module = 'TEM'
                )
            AND
                recordid not in (
                    select link_id
                    from sec_group_record_permissions
                    where module = 'TEM'
                )
             ";

        $rows = PDO_fetch_all($sql);

        foreach ($rows as $row)
        {
            $RecordIDs[] = $row['recordid'];
        }

    }

    require_once 'Source/security/SecurityBase.php';
    $groups = GetUserSecurityGroups($con_id);

    if (count($groups)>0)
    {
        foreach ($groups as $groupid)
        {
            $sql =
                "SELECT link_id
                FROM sec_group_record_permissions
                WHERE
                    module = :module
                    AND tablename = :tablename
                    AND group_id = :group_id";

            $rows = PDO_fetch_all($sql, array("module" => $module, "tablename" => $table, "group_id" => $groupid));
            $Where = '';

            foreach ($rows as $row)
            {
                $RecordIDs[] = $row['link_id'];
            }
        }
    }

    return $RecordIDs;
}

function MakeRecordWherePermsClause($module, $table, $con_id, $parent_module)
{
    $RecordIDs = GetRecordLevelSecurityIds($module, $table, $con_id);

    if (is_array($RecordIDs))
    {
        $RecordIDs = implode(', ',$RecordIDs);
    }

    if ($RecordIDs != '')
    {
        $Where = "{$table}.recordid in ({$RecordIDs})";
    }

    return $Where;
}


function GetPHPVersion()
{
    $PHPArray = explode('.',PHP_VERSION);
    return intval($PHPArray[0]);
}

/**
 * Legacy wrapper function which should be remove once all places are changed to use the function call below instead.
 * @deprecated Replace with Labels_UDFLabel::GetUDFFieldLabel 
 */
function GetUDFFieldLabel($id)
{
    return Labels_UDFLabel::GetUDFFieldLabel($id);
}

function AuditLogin($username, $comments)
{
    $sql = "
        INSERT INTO
            login_audit
            (lau_login, lau_date, lau_description)
        VALUES
            (:lau_login, :lau_date, :lau_description)
    ";

    PDO_query($sql, array(
        "lau_login" => $username,
        "lau_date" => date("Y-m-d H:i:s"),
        "lau_description" => "WEB $_SERVER[REMOTE_ADDR]: $comments"
    ));
}

function ArrayToSelect($aParams)
{
    $sID = $aParams['id'];
    $aOptionArray = $aParams['options'];
    $CurrentValue = $aParams['value'];
    $disabled = ($aParams['disabled'] === true ? 'disabled=disabled' : '');

    $sSelect = '<select id="'.$sID.'" name="'.$sID.'" '.$disabled.'>';

    if (is_array($aOptionArray))
    {
        foreach ($aOptionArray as $value => $text)
        {
            // Make sure we don't accidentally have null = ''
            if ($CurrentValue === $value || ($CurrentValue != NULL && $CurrentValue == $value))
            {
                $sSelect .= '<option value="'.$value.'" selected="selected">'.$text.'</option>';
            }
            else
            {
                $sSelect .= '<option value="'.$value.'">'.$text.'</option>';
            }
        }
    }

    $sSelect .= '</select>';

    return $sSelect;
}

/**
* @desc Takes an array of variables and returns the first one that is not empty
*
* @param array $aChoices Set of variables to check for values.
*
* @return string Contents of the first variable to contain a value.
*/
function FirstNonNull($aChoices)
{
    if (!empty($aChoices))
    {
        foreach ($aChoices as $Choice)
        {
            if ($Choice != null && $Choice !== 0 && $Choice != '' && !(is_array($Choice) && empty($Choice)))
            {
                return $Choice;
            }
        }
    }

    return null;
}

/**
* @desc Makes an entry in the full_audit log, but only if FULL_AUDIT_OPEN is Y. Used to log when a record was opened
*
* @param string $Module Code of the current module
* @param int $RecordID Recordid of the current record
* @param string $Comments Comments to include in the audit
*/
function AuditOpenRecord($Module, $RecordID, $Comments)
{
    if (bYN(GetParm("FULL_AUDIT_OPEN", "N")))
    {
        InsertFullAuditRecord($Module, $RecordID, 'OPEN', $Comments);
    }
}

/**
* @desc Inserts an entry into the full_audit table, logging an action, a timestamp and the initials
 * of the logged in user.
*
* @param string $Module Code of the current module
* @param int $RecordID Recordid of the current record
* @param string $Action Code describing what is being audited
* @param string $Comments Comments to include in the audit
*/
function InsertFullAuditRecord($Module, $RecordID, $Action, $Comments)
{
    $AuditDate = date('Y-m-d H:i:s');

    $InsertArray = array(
        'aud_module' => $Module,
        'aud_record' => $RecordID,
        'aud_login' => $_SESSION['initials'],
        'aud_date' => $AuditDate,
        'aud_action' => $Action,
        'aud_detail' => 'WEB '.$_SERVER['REMOTE_ADDR'].' '.$Comments
    );

    DatixDBQuery::PDO_build_and_insert('full_audit', $InsertArray);
}

/**
* @desc Inserts an entry into the full_audit table for a contact link.
*
* @param string $Module Code of the current module
* @param int $RecordID Recordid of the main record
* @param string $ContactID Recordid of the contact record
* @param string $Type link_type code for this contact link
*/
function AuditContactLink($Module, $RecordID, $ContactID, $Type)
{
    InsertFullAuditRecord($Module, $RecordID, 'LINK', "Contact ID: $ContactID, Type: $Type");
}

/**
* @desc Inserts an entry into the full_audit table for a contact unlink action.
*
* @param string $Module Code of the current module
* @param int $RecordID Recordid of the main record
* @param string $ContactID Recordid of the contact record
*/
function AuditContactUnlink($Module, $RecordID, $ContactID)
{
    InsertFullAuditRecord($Module, $RecordID, 'UNLINK', "Contact ID: $ContactID");
}

/**
* @desc Inserts an entry into the full_audit table for a contact rejection action.
*
* @param string $Module Code of the current module
* @param int $RecordID Recordid of the main record
* @param string $ContactID Recordid of the contact record
*/
function AuditContactReject($Module, $RecordID, $ContactID)
{
    InsertFullAuditRecord($Module, $RecordID, 'REJECT_CONTACT', "Contact ID: $ContactID");
}

/**
* @desc Converts an array into a string of the form key1=val1&key2=val2&...
* Used to pass potentially large arrays in the url.
*
* @param array $Array The array to convert
*
* @return string URL-style string of keys and values.
*/
function ArrayToUrlString($Array)
{
    // TODO: Replace this method with php's native http_build_query()
    
    if (is_array($Array))
    {
        foreach ($Array as $key => $val)
        {
            $PairArray[] = $key.'='.$val;
        }

        return implode('&', $PairArray);
    }
    else
    {
        return '';
    }
}

/**
* @desc Returns array of approval status code/description pairs for a given module.
*
* @param string $Module The module in question.
*
* @return array Array of approval status code/description pairs.
*/
function GetLevelFieldLabels($Module)
{
    $sql =  'SELECT code, description, cod_web_colour
             FROM code_approval_status
             WHERE module like :module
             AND workflow = :workflowid';

    $result = PDO_fetch_all(
        $sql,
        array(
            'module' => $Module,
            'workflowid' => GetWorkflowID($Module)
        )
    );

    $FieldLabels = array();

    foreach ($result as $row)
    {
        $FieldLabels[$row['code']]['description'] = FirstNonNull(array(
            _tk('approval_status_'.$Module.'_'.$row['code']),
            _t($row['description'])
        ));
        $FieldLabels[$row['code']]['colour'] = $row['cod_web_colour'];
    }

    return $FieldLabels;
}

/**
* @desc Determines whether or not a user can save a record from its current location
 * (includes saving back to the same status).
*
* @param array $aParams Array of parameters.
* @param string $aParams['module'] The current module.
* @param array $aParams['data'] Data array for the current record.
*
* @return bool true if this record can be saved, false if not.
*/
function CanMoveRecord($aParams)
{
    global $ModuleDefs;

    $Levels = GetLevelsTo($aParams['module'], GetParm($ModuleDefs[$aParams['module']]['PERM_GLOBAL']), $aParams['data']['rep_approved']);

    if (empty($Levels))
    {
        return false;
    }

    return true;
}

/**
* @desc Determines the approval statuses that a record can be moved to from the one it is currently at.
*
* @param string $Module The current module.
* @param string $ModPerm The set of access levels the user has on this module.
* @param string $CurrLevel The approval status the record is currently at.
*
* @return array An array of code/description pairs for the statuses the record can be moved to.
*/
function GetLevelsTo($Module, $ModPerm, $CurrLevel)
{
    $LevelLabels = array();

    if ($ModPerm == '')
    {
        $ModPerm = array('NONE');
    }
    else if (!is_array($ModPerm))
    {
        $ModPerm = array($ModPerm);
    }

    $CurrLevel = ($CurrLevel ? $CurrLevel : 'NEW');

    $sql = '
        SELECT code, description as descr, cod_web_colour
        FROM code_approval_status
        JOIN approval_action
        ON
            code_approval_status.code = approval_action.apac_to
        AND
            code_approval_status.module = approval_action.module
        AND
            code_approval_status.workflow = approval_action.apac_workflow
        WHERE
            approval_action.access_level in (\''.implode('\', \'', $ModPerm).'\')
        AND
            approval_action.module like :module
        AND
            approval_action.apac_from like :apac_from
        AND
            approval_action.apac_workflow = :apac_workflow
        ORDER BY code_approval_status.cod_listorder';

    $result = PDO_fetch_all(
        $sql,
        array(
            'module' => $Module,
            'apac_from' => $CurrLevel,
            'apac_workflow' => GetWorkflowID($Module)
        )
    );

    foreach ($result as $row)
    {
        $LevelLabels[$row['code']]['description'] = FirstNonNull(array(
            _tk('approval_status_'.$Module.'_'.$row['code']),
            $row['descr']
        ));
        $LevelLabels[$row['code']]['colour'] = $row['cod_web_colour'];
    }

    if ($CurrLevel != 'REJECT' && (($Module == 'INC' && !bYN(GetParm('DIF_SHOW_REJECT_BTN','Y'))) ||
        ($Module == 'CON' && !bYN(GetParm('CON_SHOW_REJECT_BTN','Y')))))
    {
        unset($LevelLabels['REJECT']);
    }

    return $LevelLabels;
}

/**
* @desc Determines the approval statuses that a user with a given access level is able to view.
*
* @param string $Module The current module.
* @param string $ModPerm The access level the user has on this module.
*
* @return array An array of code/description pairs for the statuses the user can see.
*/
function GetLevelstoView($Module, $ModPerm)
{
    $LevelLabels = array();

    $sql = "
        SELECT code_approval_status.code as code, code_approval_status.description as descr, cod_web_colour
        FROM code_approval_status
        JOIN link_access_approvalstatus
            ON
              code_approval_status.code = link_access_approvalstatus.code
            AND
              code_approval_status.module = link_access_approvalstatus.module
            AND
              code_approval_status.workflow = link_access_approvalstatus.las_workflow
        WHERE
              code_approval_status.module like '$Module'
            AND
              link_access_approvalstatus.access_level = '$ModPerm'
            AND
              code_approval_status.workflow = ".GetWorkflowID($Module)."
        ORDER BY code_approval_status.cod_listorder";

    $result = db_query($sql);

    while ($row = db_fetch_array($result))
    {
        $LevelLabels[$row['code']]['description'] = FirstNonNull(array(
            _tk('approval_status_'.$Module.'_'.$row['code']),
            $row['descr']
        ));
        $LevelLabels[$row['code']]['colour'] = $row['cod_web_colour'];
    }

    return $LevelLabels;
}

/**
* @desc Gets the name of an approval status.
*
* @param string $Module The module this level belongs to
* @param string $Level The code for the level in question.
*
* @return string The user-facing name of this approval status.
*/
function GetApprovalStatusDescription($Module, $Level)
{
    $sql =  "SELECT code, description
             FROM code_approval_status
             WHERE module like '$Module'
             AND code= '$Level'";

    $row = db_fetch_array(db_query($sql));

    return FirstNonNull(array(_tk('approval_status_'.$Module.'_'.$row['code']), $row['description']));
}

/**
* @desc Determines the type of access that a user has for a particular approval status
 * (if approval statuses are used) in a particular module.
*
* @global array $ModuleDefs
*
* @param string $Module The current module.
* @param string $ModPerm The access level the user has on this module.
* @param string $CurrLevel The approval status the record is currently at.
*
* @return string 'E' if the user has permission to edit the record, 'R' if they have read-only access
 * and '' if something has gone wrong.
*/
function GetAccessFlag ($Module, $ModPerm, $CurrLevel)
{
    global $ModuleDefs;

    if ($ModuleDefs[$Module]['USES_APPROVAL_STATUSES'] !== false)
    {
        $sql = '
        SELECT access
        FROM link_access_approvalstatus lal
        JOIN code_approval_status apst
        ON
          apst.code = lal.code
        AND
          apst.module = lal.module
        AND
          apst.workflow = lal.las_workflow
        WHERE
          lal.access_level like :ModPerm
        AND
          lal.code like :CurrLevel
        AND
          lal.module like :Module
        AND
          lal.las_workflow = :WorkflowID';

        $PDOParamsArray = array(
            'ModPerm'    => $ModPerm,
            'Module'     => $Module,
            'WorkflowID' => GetWorkflowID($Module),
            'CurrLevel' => $CurrLevel
        );

    }
    else
    {
        $sql = 'SELECT acl_access FROM access_levels WHERE acl_module = :Module AND acl_code = :ModPerm';

        $PDOParamsArray = array('ModPerm' => $ModPerm, 'Module' => $Module);
    }

    $access = DatixDBQuery::PDO_fetch($sql, $PDOParamsArray, PDO::FETCH_COLUMN);
    
    if ($Module != '' && $ModPerm != '' && $CurrLevel != '' && $access == '')
    {
        // commenting this out for now since in certain circumstances we "legitimately" end up here
        //throw new src\security\exceptions\SecurityException('Unable to determine access for the following criteria - Module: '.$Module.'; ModPerm: '.$ModPerm.'; CurrLevel: '.$CurrLevel);
    }
    
    return $access;
}

/**
 * @desc Determines the type of access that a user has for a criterion based on the status of the
 * Assigned Assessment Template and the Assessment instance.
 *
 * @param string $Module The current module.
 * @param string $CriterionId The ID of the Criterion to display.
 * @return string 'E' if the user has permission to edit the record or 'R' if they have read-only access.
 */
function GetCriterionAccessLevel($Module, $CriterionId)
{
    // Get current Assigned Assessment Template and Assessment Instance statuses for selected Criterion
    $sql = '
        SELECT
            asm_questions.adm_rep_approved AS amo_rep_approved,
            asm_templates.rep_approved AS ati_rep_approved
        FROM
            asm_questions,
            asm_templates
        WHERE
            asm_questions.asm_template_instance_id = asm_templates.asm_template_instance_id
            AND
            asm_question_id = :asm_question_id
    ';

    $Status = DatixDBQuery::PDO_fetch($sql, array('asm_question_id' => $CriterionId));

    return GetInstancesAccessLevel($Module, $Status);
}

/**
 * @desc Determines the type of access that a user has for an assessment instance based on the status of the
 * Assigned Assessment Template and the Assessment instance.
 *
 * @param string $Module The current module.
 * @param string $AssessmentId The ID of the Assessment instance to display.
 * @return string 'E' if the user has permission to edit the record or 'R' if they have read-only access.
 */
function GetAssessmentAccessLevel($Module, $AssessmentId)
{
    // Get current Assigned Assessment Template and Assessment Instance statuses for selected Assessment instance
    $sql = '
        SELECT
            asm_templates.rep_approved AS ati_rep_approved,
            asm_modules.rep_approved AS amo_rep_approved
        FROM
            asm_templates,
            asm_modules
        WHERE
            asm_modules.asm_template_instance_id = asm_templates.asm_template_instance_id
            AND
            asm_modules.recordid = :recordid
    ';

    $Status = DatixDBQuery::PDO_fetch($sql, array('recordid' => $AssessmentId));

    return GetInstancesAccessLevel($Module, $Status);
}

function GetInstancesAccessLevel($Module, $Status)
{
    switch ($Status['ati_rep_approved'])
    {
        case 'OPEN':
            switch ($Status['amo_rep_approved'])
            {
                case 'OPEN':
                    return 'E';
                    break;
                case 'SUBMIT':
                    // Get user access level and remove module name
                    $UserAccessLevel = \UnicodeString::substr(GetAccessLevel($Module), 4);

                    if ($UserAccessLevel == 'FULL')
                    {
                        return 'E';
                    }
                    else
                    {
                        return 'R';
                    }

                    break;
            }

            break;
        case 'SUBMIT':
            switch ($Status['amo_rep_approved'])
            {
                case 'OPEN':
                    AddSessionMessage('INFO', _tk('ASM_READONLY_SUBMITTED_INFO'));
                    return 'R';
                    break;
                case 'SUBMIT':
                    return 'R';
                    break;
            }

            break;
        case 'REVIEW':
            switch ($Status['amo_rep_approved'])
            {
                case 'OPEN':
                    AddSessionMessage('INFO', _tk('ASM_READONLY_REVIEWED_INFO'));
                    return 'R';
                    break;
                case 'SUBMIT':
                    return 'R';
                    break;
            }

            break;
    }
}

/**
* @desc Generic function. Picks up an array of functions from AppVars and runs them all,
 * taking arrays of data they return and returning a merged array of them all. Used to load in extra
 * non-standard data after all main table/normal linked data has been collected
*
* @param string $module The current module.
* @param int $recordid The current recordid.
*
* @return array Array of additional data.
*/
function GetExtraData($module, $recordid)
{
    global $ModuleDefs;

    $ExtraDataArray = array();

    if (is_array($ModuleDefs[$module]['EXTRA_RECORD_DATA_FUNCTION_INCLUDES']))
    {
        foreach ($ModuleDefs[$module]['EXTRA_RECORD_DATA_FUNCTION_INCLUDES'] as $ExtraDataFunctionInclude)
        {
            require_once $ExtraDataFunctionInclude;
        }
    }

    if (is_array($ModuleDefs[$module]['EXTRA_RECORD_DATA_FUNCTIONS']))
    {
        foreach ($ModuleDefs[$module]['EXTRA_RECORD_DATA_FUNCTIONS'] as $ExtraDataFunctions)
        {
            $ExtraDataFunctionArray = $ExtraDataFunctions($recordid, $module);

            if (is_array($ExtraDataFunctionArray))
            {
                $ExtraDataArray = array_merge($ExtraDataFunctions($recordid, $module), $ExtraDataArray);
            }
        }
    }

    return $ExtraDataArray;
}

/**
* @desc Loops through a list of provided field names and calls {@link CheckMultiListValue()} for each.
 * Returns an array of values ready to be used in a SQL update statement.
*
* @param array $aFields Array of fields to modify.
* @param array $aData Data array.
*
* @return array Array of field/value pairs ready for input into database.
*/
function CheckMultiListsFromArray($aFields, $aData)
{
    $aValues = array();

    foreach ($aFields as $sFieldName)
    {
        if (isset($aData[$sFieldName]) || $aData["CHANGED-".$sFieldName] == "1")
        {
            $FieldVal = CheckMultiListValue($sFieldName, $aData);
            $aValues[$sFieldName] = $FieldVal;
        }
    }

    return $aValues;
}

/**
* @desc Called by {@link CheckMultiListsFromArray()} to prepare multicode field data for
 * input into a SQL update string.
*
* @param string $sName Field in question.
* @param array $aData Data array.
*
* @return string Representation of the multicode value in string form, ready for insertion.
*/
function CheckMultiListValue($sName, array $aData)
{
    if (is_array($aData[$sName]))
    {
        return implode(" ", $aData[$sName]);
    }
    elseif ($aData[$sName] != '')//read only/default hidden value
    {
        return $aData[$sName];
    }

    if (!is_array($aData[$sName]) && isset($aData[$sName]))
    {
        return null;
    }
}

/**
* @desc Loops through a list of provided field names and calls {@link CheckDateValue()} for each.
 * Returns an array of values ready to be used in a SQL update statement.
*
* @param array $aFields Array of fields to modify.
* @param array $aData Data array.
*
* @return array Array of field/value pairs ready for input into database.
*/
function CheckDatesFromArray($aFields, $aData)
{
    $aValues = array();

    foreach ($aFields as $sFieldName)
    {
        $Value = CheckDateValue($sFieldName, $aData);

        if (isset($aData[$sFieldName]))
        {
            $aValues[$sFieldName] = $Value;
        }
    }

    return $aValues;
}

/**
* @desc Called by {@link CheckDatesFromArray()} to prepare date field data for input into a SQL update string.
*
* @param string $sName Field in question.
* @param array $aData Data array.
*
* @return string Representation of the date value in string form, ready for insertion.
*/
function CheckDateValue($sName, $aData)
{
    if (isset($aData[$sName]) && $aData[$sName] != null)
    {
        return UserDateToSQLDate($aData[$sName]);
    }
    elseif (isset($aData[$sName]))
    {
        return null;
    }
}

/**
* @desc Loops through a list of provided field names and calls {@link CheckNumberValue()} for each.
 * Returns an array of values ready to be used in a SQL update statement.
*
* @param array $aFields Array of fields to modify.
* @param array $aData Data array.
*
* @return array Array of field/value pairs ready for input into database.
*/
function CheckNumbersFromArray($aFields, $aData)
{
    $aValues = array();

    foreach ($aFields as $sFieldName)
    {
        $Value = CheckNumberValue($sFieldName, $aData);

        if (isset($aData[$sFieldName]))
        {
            $aValues[$sFieldName] = $Value;
        }
    }

    return $aValues;
}

/**
* @desc Called by {@link CheckNumbersFromArray()} to prepare number field data for input into a SQL update string.
*
* @param string $sName Field in question.
* @param array $aData Data array.
*
* @return string Representation of the number value in string form, ready for insertion.
*/
function CheckNumberValue($sName, $aData)
{
    if ($aData[$sName] !== null && $aData[$sName] !== '')
    {
        return intval($aData[$sName]);
    }
    elseif (isset($aData[$sName]))
    {
        return null;
    }
}

/**
* @desc Loops through a list of provided field names and calls {@link CheckDecimalValue()} for each.
 * Returns an array of values ready to be used in a SQL update statement.
*
* @param array $aFields Array of fields to modify.
* @param array $aData Data array.
*
* @return array Array of field/value pairs ready for input into database.
*/
function CheckDecimalsFromArray($aFields, $aData)
{
    $aValues = array();

    foreach ($aFields as $sFieldName)
    {
        $Value = CheckDecimalValue($sFieldName, $aData);

        if (isset($aData[$sFieldName]))
        {
            $aValues[$sFieldName] = $Value;
        }
    }

    return $aValues;
}

/**
* @desc Called by {@link CheckDecimalsFromArray()} to prepare decimal field data for input into a SQL update string.
*
* @param string $sName Field in question.
* @param array $aData Data array.
*
* @return string Representation of the decimal value in string form, ready for insertion.
*/
function CheckDecimalValue($sName, $aData)
{
    if ($aData[$sName] != null && $aData[$sName] != '')
    {
        return floatval($aData[$sName]);
    }
    elseif (isset($aData[$sName]))
    {
        return null;
    }
}

/**
* @desc Loops through a list of provided field names and calls {@link CheckMoneyValue()} for each.
 * Returns an array of values ready to be used in a SQL update statement.
*
* @param array $aFields Array of fields to modify.
* @param array $aData Data array.
*
* @return array Array of field/value pairs ready for input into database.
*/
function CheckMoneyFromArray($aFields, $aData)
{
    $aValues = array();

    foreach ($aFields as $sFieldName)
    {
        $Value = CheckMoneyValue($sFieldName, $aData);

        if (isset($aData[$sFieldName]))
        {
            $aValues[$sFieldName] = $Value;
        }
    }

    return $aValues;
}

/**
* Checks all textarea fields on the form to see if they have been set up
* as Timestamp fields and reformats posted values accordingly.
*
* @global array  $TimestampFields
*
* @param  array  $aFields  The textarea fields on the form.
* @param  array  $aSuffix  Identifier for linked data.
* @param  array  $aData    The submitted form data.
*
* @return array  $aValues  The textarea contents with any necessary modifications.
*/
function CheckTextAreaFromArray($aFields, $aSuffix, $aData, $FormDesign = null)
{
    global $TimestampFields, $FieldDefs;

    if ($FormDesign)
    {
        $TimestampFields = $FormDesign->TimestampFields;
    }

    $aValues = array();

    $user = $_SESSION["logged_in"] ? PDO_fetch('SELECT (con_forenames + \' \' + con_surname) AS name FROM contacts_main WHERE initials = \''.$_SESSION['initials'].'\'', array(), PDO::FETCH_COLUMN) : 'the reporter';

    $date = GetParm('FMT_DATE_WEB') == 'US' ? date('m/d/Y H:i:s') : date('d/m/Y H:i:s');

    foreach ($aFields as $sFieldName)
    {
        if ($TimestampFields[$sFieldName] && ! $FieldDefs[$_GET['module']][$sFieldName]['MaxLength'])
        {
            if ($aData[$sFieldName.$aSuffix] != '')
            {
                if (!isLevelOneForm() && is_null($aData['CURRENT_'.$sFieldName.$aSuffix]))
                {
                    // prevent field being date-stamped if read-only
                    $newValue = $aData[$sFieldName.$aSuffix];
                }
                else
                {
                    $newValue = '['.$date.' '.$user.'] '.$aData[$sFieldName.$aSuffix]
                        ."\r\n".$aData['CURRENT_'.$sFieldName.$aSuffix];
                }
            }
            else
            {
                $newValue = $aData['CURRENT_'.$sFieldName.$aSuffix];
            }

            if (\UnicodeString::substr($sFieldName, 0, 4) == 'UDF_')
            {
                // we need to modify the $_POST array, since UDFs are saved using $_POST directly
                $_POST[$sFieldName.$aSuffix] = $newValue;
            }
            else
            {
                $aValues[$sFieldName.$aSuffix] = $newValue;
            }
        }
    }

    return $aValues;
}

/**
* @desc Called by {@link CheckMoneyFromArray()} to prepare money field data for input into a SQL update string.
*
* @param string $sName Field in question.
* @param array $aData Data array.
*
* @return string Representation of the money value in string form, ready for insertion.
*/
function CheckMoneyValue($sName, $aData)
{
    if ($aData[$sName] != null && $aData[$sName] != '')
    {
        return floatval(preg_replace("/[^0-9\.-]/u","",$aData[$sName]));
    }
    elseif (isset($aData[$sName]))
    {
        return null;
    }
}

/**
* @desc Generates an update-style SQL string (key=value, key2=value2...) from arrays of fields and data.
*
* @param array $aParams Array of parameters.
* @param string $aParams['Module'] The current module.
* @param int $aParams['Suffix'] Any suffix present on the data.
* @param array $aParams['FieldArray'] Array of fields to include in the statement.
* @param array $aParams['DataArray'] Array of data from which to draw the values in the statement.
* @param bool $aParams['end_comma'] If false, the statement will not end in a comma,
 * to allow where clause information to be appended. - this can probably be refactored out at some point
*
* @return string String containing key/value pairs formated for a SQL update statement.
*/
function GenerateSQLFromArrays($aParams)
{
    global $FieldDefs;

    $Module = $aParams['Module'];

    if ($aParams['Suffix'])
    {
        $SuffixString = '_'.$aParams['Suffix'];
    }

    $sqlArray = array();

    if (is_array($aParams["FieldArray"]) && is_array($aParams["DataArray"]))
    {
        foreach ($aParams["FieldArray"] as $FieldKey)
        {
            if (array_key_exists($FieldKey.$SuffixString, $aParams["DataArray"]))
            {
                $Value = $aParams['DataArray'][$FieldKey.$SuffixString];

                if (($FieldDefs[$Module][$FieldKey]['Type'] == 'number' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'money' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'date' ||
                    $FieldDefs[$Module][$FieldKey]['NullZeros'] == true) &&
                    $Value == '')
                {
                    $Value = 'NULL';
                }

                if ($FieldDefs[$Module][$FieldKey]['Type'] == 'number' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'money' ||
                    (($FieldDefs[$Module][$FieldKey]['Type'] == 'date' ||
                        $FieldDefs[$Module][$FieldKey]['NullZeros']) && $Value == 'NULL'))
                {
                    if ($Value != '')
                    {
                        $sqlArray[] = $FieldKey.' = '.$Value;
                    }
                }
                else
                {
                    $sqlArray[] = $FieldKey.' = \''.$Value.'\'';
                }
            }
        }
    }

    if (!empty($sqlArray))
    {
        if ($aParams['end_comma'])
        {
            return join(', ',$sqlArray).', ';
        }
        else
        {
            return join(', ',$sqlArray);
        }
    }

}

/**
* @desc Generates an update-style SQL string (key=parameter1, key2=parameter2...) from arrays of fields
 * and data for PDO connection method.
* (Similar to old GenerateSQLFromArrays but for new PDO method of connection replacement and additionally returns
 * in receive paramater array $PDOParamsArray the key/value pairs for each variable used in the SQL statement)
*
* @param array $aParams Array of parameters.
* @param string $aParams['Module'] The current module.
* @param int $aParams['Suffix'] Any suffix present on the data.
* @param array $aParams['FieldArray'] Array of fields to include in the statement.
* @param array $aParams['DataArray'] Array of data from which to draw the values in the statement.
* @param bool $aParams['end_comma'] If false, the statement will not end in a comma, to allow where clause
 * information to be appended. - this can probably be refactored out at some point
* @param array $PDOParamsArray recieve array parameter for key/value pairs
* @return string String containing key/value pairs formated for a SQL update statement.
*/
function GeneratePDOSQLFromArrays($aParams, &$PDOParamsArray)
{
    global $FieldDefs;

    $Module = $aParams['Module'];

    if ($aParams['Suffix'])
    {
        $SuffixString = '_'.$aParams['Suffix'];
    }

    $sqlArray = array();

    if (!$PDOParamsArray)
    {
        $PDOParamsArray = array();
    }

    if (is_array($aParams["FieldArray"]) && is_array($aParams["DataArray"]))
    {
        foreach ($aParams["FieldArray"] as $FieldKey)
        {
            if (array_key_exists($FieldKey.$SuffixString, $aParams["DataArray"]) && !$FieldDefs[$Module][$FieldKey]['Computed'])
            {
                $Value = $aParams['DataArray'][$FieldKey.$SuffixString];

                if ((($FieldDefs[$Module][$FieldKey]['Type'] == 'number' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'decimal' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'money') &&
                    ($Value === '' || $Value === 'NULL')) // 0 is a valid value for number and money
                    || (($FieldDefs[$Module][$FieldKey]['NullZeros'] == true ||
                        $FieldDefs[$Module][$FieldKey]['Type'] == 'date') && ($Value == '' || $Value === 'NULL')))
                {
                    $Value = null;
                }

                $FieldKeyParam = $FieldKey;

                $PDOParamsArray[$FieldKeyParam] = $Value;
                $sqlArray[] = $FieldKey.' = :'.$FieldKeyParam;
            }
        }
    }

    if (!empty($sqlArray))
    {
        if ($aParams['end_comma'])
        {
            return join(', ',$sqlArray).', ';
        }
        else
        {
            return join(', ',$sqlArray);
        }
    }
}

/**
* @desc Generates an insert-style SQL string (key, key2, ...) VALUES (value, value2, ...)
 * from arrays of fields and data.
*
* @param array $aParams Array of parameters.
* @param string $aParams['Module'] The current module.
* @param int $aParams['Suffix'] Any suffix present on the data.
* @param array $aParams['FieldArray'] Array of fields to include in the statement.
* @param array $aParams['DataArray'] Array of data from which to draw the values in the statement.
* @param array $aParams['Extras'] Custom key/value pairs to be added to the statement.
*
* @return string String containing key/value pairs formated for a SQL insert statement.
*/
function GenerateInsertSQLFromArrays($aParams)
{
    global $FieldDefs;

    if (is_array($aParams["FieldArray"]) && !empty($aParams["FieldArray"]) && is_array($aParams["DataArray"]) &&
        !empty($aParams["DataArray"]))
    {
        $Module = $aParams['Module'];

        if ($aParams['Suffix'])
        {
            $SuffixString = '_'.$aParams['Suffix'];
        }

        $sqlArray = array();
        $fieldArray = array();
        $valArray = array();

        foreach ($aParams["FieldArray"] as $FieldKey)
        {
            if (array_key_exists($FieldKey.$SuffixString, $aParams["DataArray"]))
            {
                IncludeFieldDefs();

                $Value = $aParams['DataArray'][$FieldKey.$SuffixString];

                if (($FieldDefs[$Module][$FieldKey]['Type'] == 'number' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'money' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'date') &&
                    $Value == '')
                {
                    $Value = 'NULL';
                }

                if ($FieldDefs[$Module][$FieldKey]['Type'] == 'number' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'money' ||
                    ($FieldDefs[$Module][$FieldKey]['Type'] == 'date' && $Value == 'NULL'))
                {
                    if ($Value != '')
                    {
                        $fieldArray[] = $FieldKey;
                        $valArray[] = $Value;
                    }
                }
                else
                {
                    $fieldArray[] = $FieldKey;
                    $valArray[] = '\''.$Value.'\'';
                }
            }
        }

        if (is_array($aParams['Extras']))
        {
            foreach ($aParams['Extras'] as $field => $val)
            {
                $fieldArray[] = $field;
                $valArray[] = $val;
            }
        }

        $sql = '('.join(', ',$fieldArray).') VALUES ('.join(', ',$valArray).')';
    }

    return $sql;
}

/**
 * @desc Generates an insert-style SQL string (key, key2, ...) VALUES (value, value2, ...) from arrays of fields
 * and data for PDO connection method.
 * (Similar to old {@link GenerateSQLFromArrays()} but for new PDO method of connection replacement and additionally
 * returns in receive paramater array $PDOParamsArray the key/value pairs for each variable used in the SQL statement).
 *
 * @param array $aParams Array of parameters.
 * @param string $aParams['Module'] The current module.
 * @param int $aParams['Suffix'] Any suffix present on the data.
 * @param array $aParams['FieldArray'] Array of fields to include in the statement.
 * @param array $aParams['DataArray'] Array of data from which to draw the values in the statement.
 * @param array $aParams['Extras'] Custom key/value pairs to be added to the statement.
 *
 * @return string String containing key/value pairs formated for a SQL insert statement.
*/
function GeneratePDOInsertSQLFromArrays($aParams, &$PDOParamsArray)
{
    global $FieldDefs;

    if (is_array($aParams["FieldArray"]) && !empty($aParams["FieldArray"]) && is_array($aParams["DataArray"]) &&
        !empty($aParams["DataArray"]))
    {
        $Module = $aParams['Module'];

        if ($aParams['Suffix'])
        {
            $SuffixString = '_'.$aParams['Suffix'];
        }
        $fieldArray = array();
        $valArray = array();

        foreach ($aParams["FieldArray"] as $FieldKey)
        {
            if (array_key_exists($FieldKey.$SuffixString, $aParams["DataArray"]) && $FieldDefs[$Module][$FieldKey]['CalculatedField'] != true)
            {
                IncludeFieldDefs();

                $Value = $aParams['DataArray'][$FieldKey.$SuffixString];

                if (($FieldDefs[$Module][$FieldKey]['Type'] == 'number' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'money' ||
                    $FieldDefs[$Module][$FieldKey]['Type'] == 'date') &&
                    ($Value === '' || $Value === 'NULL'))
                {
                    $Value = null;
                }

                $fieldArray[] = $FieldKey;
                $valArray[] = ':'.$FieldKey;
                $PDOParamsArray[$FieldKey] = $Value;
            }
        }

        if (is_array($aParams['Extras']))
        {
            foreach ($aParams['Extras'] as $field => $val)
            {
                $fieldArray[] = $field;
                $valArray[] = ':'.$field;
                $PDOParamsArray[$field] = $val;
            }
        }

        $sql = '('.join(', ',$fieldArray).') VALUES ('.join(', ',$valArray).')';
    }

    return $sql;
}

function ParseRootCauses($aParams)
{
    if (isset($_POST["numrootcauses"])) //otherwise no data sent - means readonly or hidden
    {
        // Create root causes list from radio buttons
        // only if global INC_INV_RC is NOT set.
        // Also, check to see if $_POST["inc_root_causes"] is set:
        // if it is, use this instead of trying the other values
        if ($_POST["inc_root_causes"])
        {
            $inc_root_cause_array = Sanitize::SanitizeRaw($_POST["inc_root_causes"]);
        }
        else
        {
            $NumRootCauses = $_POST["numrootcauses"];

            if (GetParm("INC_INV_RC", "N") == "N")
            {
                $cause_group = 1;

                while ($cause_group <= $NumRootCauses)
                {
                    if (($cause = Sanitize::SanitizeRaw($_POST["incrootcause_$cause_group"])) != "")
                    {
                        $inc_root_cause_array[] = $cause;
                    }

                    $cause_group ++;
                }
            }
            else
            {
                for ($i = 0; $i < $NumRootCauses; $i++)
                {
                    if ($RootCause = Sanitize::SanitizeRaw($_POST["incrootcause_$i"]))
                    {
                        $inc_root_cause_array[] = $RootCause;
                    }
                }
            }
        }

        if ($inc_root_cause_array)
        {
            $inc_root_causes = implode(" ", $inc_root_cause_array);
        }

        \UnicodeString::ltrim($inc_root_causes);

        if (!$_POST["inc_root_causes"])
        {
            $aParams['data']['inc_root_causes'] = $inc_root_causes;
        }
    }

    return $aParams['data'];
}

function getContactLinkFields ($link_type = '', $calculatedFields = false)
{
    global $ModuleDefs, $FieldDefs;

    if ($link_type == 'O') { // individual respondents
        if ($calculatedFields)
        {
            $tempArray = array();
            foreach ($ModuleDefs["CON"]["LINKED_FIELD_ARRAY_RESPONDENT"] as $field)
            {
                if (!$FieldDefs["CON"][$field]['CalculatedField'])
                {
                    $tempArray[] = $field;
                }
            }
            return $tempArray;
        }
        else
        {
            return $ModuleDefs["CON"]["LINKED_FIELD_ARRAY_RESPONDENT"];
        }
    }

    return $ModuleDefs["CON"]["LINKED_FIELD_ARRAY"];
}

function GetComplLinkData($aParams)
{
    $sql = '
        SELECT
            lcom_primary, lcom_ddueack, lcom_ddueact, lcom_ddueresp, lcom_dduehold, lcom_dduerepl,
            lcom_iscomplpat, lcom_dreceived, lcom_dack, lcom_dactioned, lcom_dresponse, lcom_dholding, lcom_dreplied
        FROM
            LINK_COMPL
        WHERE
            com_id = '.$aParams['data']['com_id'].'
            AND
            con_id = '.$aParams['data']['con_id'].'
            AND
            lcom_current = \'Y\'
    ';

    $row = db_fetch_array(db_query($sql));

    return $row;
}

/**
* @desc Called when a contact is unlinked from a complaint - cleans up the link_compl entry which would
 * otherwise be left orphaned.
*
* @param array $aParams Array of parameters
* @param array $aParams[data] Data array for the record just deleted
*
* @return obj The result of the SQL delete request
*/
function UnlinkComplLinkData($aParams)
{
    if ($aParams['data']['link_recordid'])
    {
        $sql = '
            SELECT
                link_type, lcom_iscomplpat
            FROM
                link_contacts, link_compl
            WHERE
                link_contacts.link_recordid = '.$aParams['data']['link_recordid'].'
                AND link_contacts.com_id = link_compl.com_id
                AND link_contacts.con_id = link_compl.con_id
        ';

        $row = db_fetch_array(db_query($sql));

        // We want to delete the linked complainant record, but only if the link we are removing is a complainant
        // and not just he same person linked as an Employee or Other Contact
        if ($row['link_type'] == 'C' || ($row['link_type'] == 'A' && $row['lcom_iscomplpat'] == 'Y'))
        {
            $sql = '
                DELETE FROM
                    LINK_COMPL
                WHERE
                    com_id = '.$aParams['data']['main_recordid'].'
                    AND con_id = '.$aParams['data']['con_recordid'];

            $result = db_query($sql);

            return $result;
        }
    }
}

/**
* @desc Deletes mutually exclusive contact links when a new contact is added to a complaint.
*
* @param array $aParams Array of parameters.
* @param array $aParams['data'] The data posted when linking the contact.
 * Contains 'main_recordid': the id of the complaint and 'con_recordid': the id of the contact .
*/
function DeleteDuplicateComplaintContacts($aParams)
{
    $contactId = $aParams['data']['con_recordid'.$aParams['suffixstring']];
    $linkType = $aParams['data']['real_link_type'.$aParams['suffixstring']];
    $complaintRecordid = $aParams['data']['main_recordid'] ?: $aParams['main_recordid'];

    if (!empty($contactId))
    {
        if ($linkType == 'C')
        {
            // Delete duplicate contacts linked as 'Person'
            $sql = '
                DELETE FROM link_contacts
                WHERE link_contacts.com_id = '.$complaintRecordid.'
                AND link_contacts.con_id = '.$contactId.'
                AND link_contacts.link_type = \'A\'
                AND NOT EXISTS(
                    SELECT con_id FROM link_compl
                    WHERE link_compl.com_id = '.$complaintRecordid.'
                    AND link_compl.con_id = '.$contactId.'
                    AND link_compl.lcom_iscomplpat = \'Y\'
                    )';

            $result = db_query($sql);
        }
        elseif ($linkType == 'A')
        {
            // update the link_type and lcom_iscomplpat for the complainant, rather than add a new link
            $sql = '
                SELECT link_compl.con_id, link_compl.recordid FROM link_contacts, link_compl
                WHERE link_compl.con_id = link_contacts.con_id
                AND link_compl.com_id = link_contacts.com_id
                AND link_contacts.com_id = :com_id
                AND link_compl.con_id = :con_id
                AND link_contacts.link_type = \'C\'
                AND link_compl.lcom_iscomplpat != \'Y\'';

            $PDOParamsArray = array(
                'com_id' => $complaintRecordid,
                'con_id' => $contactId
            );

            if ($result = PDO_fetch($sql, $PDOParamsArray))
            {
                PDO_query('
                    UPDATE
                        link_contacts
                    SET
                        link_type = \'A\'
                    WHERE
                        com_id = :com_id
                        AND
                        con_id = :con_id',
                    $PDOParamsArray);
                PDO_query('
                    UPDATE
                        link_compl
                    SET
                        lcom_iscomplpat = \'Y\'
                    WHERE
                        com_id = :com_id
                        AND
                        con_id = :con_id',
                    $PDOParamsArray);
            }
        }
    }
}

/**
* @desc Used when emailing via AJAX - returns a data array for a particular record.
*
* @param array $aParams Array of parameters
* @param string $aParams[module] Module to which the record belongs
* @param int $aParams[recordid] Id of the record in question
*
* @return array Data array for the record in question.
*/
function GetRecordData($aParams)
{
    global $ModuleDefs, $FieldDefs;

    $Fields = $ModuleDefs[$aParams['module']]['FIELD_ARRAY'];

    $Fields[] = 'recordid';

    $sql = '
        SELECT '
            . implode(',', $Fields) . '
        FROM '
            . $ModuleDefs[$aParams['module']]['TABLE'] . '
        WHERE
            recordid = :recordid
    ';

    return DatixDBQuery::PDO_fetch($sql, array('recordid' => $aParams['recordid']));

}

/**
* @desc Returns an array of audited changes to a particular record.
*
* @param array $aParams Array of parameters
* @param string $aParams[module] Module to which the record belongs
* @param int $aParams[recordid] Id of the record in question
*
* @return array Array of audited changes, keyed by field name.
*/
function GetFullAudit($aParams)
{
    $sql = "SELECT aud_login, aud_date, aud_action, aud_detail
        FROM full_audit
        WHERE aud_module = :aud_module
        AND aud_record = :aud_record
        AND aud_action LIKE 'WEB%'
        ORDER BY aud_date ASC";

    $resultArray = DatixDBQuery::PDO_fetch_all($sql, array(
        'aud_module' => $aParams['Module'],
        'aud_record' => $aParams['recordid']
    ));

    foreach ($resultArray as $row)
    {
        $Action = explode(":", $row["aud_action"]);
        $FieldName = $Action[1];
        $FullAudit[$FieldName][] = $row;
        $FullAudit["aud_detail"] = htmlspecialchars($FullAudit["aud_detail"]);
    }

     return $FullAudit;
}

/**
* @desc Return an array of contacts linked to a given record with approval status "Unapproved". Used to decide
* whether to return to the record after saving or not.
*
* @param array $aParams Array of parameters
* @param string $aParams[module] Module to which the record belongs
* @param int $aParams[recordid] Id of the record in question
*
* @return array List of unapproved contacts attached to the record.
*/
function GetUnapprovedContacts($aParams)
{
    global $ModuleDefs;

    $sql = '
        SELECT
            link_contacts.link_type, contacts_main.con_title, contacts_main.con_forenames, contacts_main.con_surname
        FROM
            link_contacts, contacts_main
        WHERE
            link_contacts.'.$ModuleDefs[$aParams['module']]['FK'].' = '.$aParams['recordid'].'
            AND link_contacts.con_id = contacts_main.recordid
            AND contacts_main.rep_approved = \'UN\'
    ';

    return DatixDBQuery::PDO_fetch_all($sql);
}

function CanListContacts()
{
    $CONPerms = GetParm('CON_PERMS');

    return ($CONPerms != "CON_INPUT_ONLY");
}

/**
* @desc Checks whether a combination of permissions and approval statuses has permission to see
 * contact information linked to a record.
* This access is decided by Datix at the moment, with no user configuration.
*
* @param string $Module The current module
* @param string $Perms The permission level in the current module
* @param string $ApprovalStatus The approval status of the main record in question
*
* @return bool True if contact information can be displayed, false otherwise.
*/
function CanSeeContacts($Module, $Perms, $ApprovalStatus)
{
    $sql = '
        SELECT
            las_restrict_contacts
        FROM
            link_access_approvalstatus
        WHERE
            access_level = :access_level
        AND
            module = :module
        AND
            code = :code
        AND
            las_workflow = :las_workflow
    ';

    $sqlParams = array(
        'access_level' => $Perms,
        'module' => $Module,
        'code' => $ApprovalStatus,
        'las_workflow' => GetWorkflowID($Module)
    );

    $row = DatixDBQuery::PDO_fetch($sql, $sqlParams);

    return !(bYN($row['las_restrict_contacts']));
}

/**
* @desc Shortcut function. Gets the access level for the currently logged in user for a particular module.
*
* @param string $module The module in question
*
* @return string The code for the access level that the current user has.
*/
function GetAccessLevel($module)
{
    global $ModuleDefs;

    return GetParm($ModuleDefs[$module]['PERM_GLOBAL']);
}

/**
 * @desc Shortcut function. Returns true if we are on a centrally administered system, false otherwise.
 *
 * @return bool true if the system is centrally administered, false otherwise.
 */
function IsCentrallyAdminSys()
{
    if (GetParm('CENTRAL_ADMINISTRATION', 'N') == 'L')
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @desc Shortcut function. Returns true if the user is a central administrator in a centrally administered system,
 * false otherwise. Put in a function so that if we ever change the admin requirements, we don't have
 * to change hundreds of places.
 *
 * @return bool true if the user is a central admin, false otherwise.
 */
function IsCentralAdmin()
{
    return (IsCentrallyAdminSys() && bYN(GetUserParm($_SESSION['login'], 'CENTRAL_ADMINISTRATOR', 'N')));
}

/**
* @desc Shortcut function. Returns true if the user is a full admin, false otherwise. Put in a function
* so that if we ever change the admin requirements, we don't have to change hundreds of places.
*
* @param bool $OrCentralAdmin If true, then will return whether the current user is a full admin OR a cenral admin.
 * If false, will only return whether they are a full admin. True by default.
*
* @return bool true if the user is a full admin, false otherwise.
*/
function IsFullAdmin($OrCentralAdmin = true)
{
    if ($OrCentralAdmin && IsCentralAdmin())
    {
        return true;
    }

    return $_SESSION["AdminUser"];
}

/**
* @desc Used to verify that a user is allowed to access a particular function or screen only available to full admins.
* If user is not permitted, sends them back to the main account screen.
*/
function CheckFullAdmin()
{
    global $scripturl, $yySetLocation;

    if (!IsFullAdmin())
    {
        $yySetLocation = $scripturl;
        redirectexit();
    }
}

/**
* @desc Checks whether the current user has user admin permissions.
*
* @param bool $OrFullAdmin If true, then will return whether the current user is a user admin OR a full admin.
 * If false, will only return whether they are a user admin. True by default.
*
* @return bool True if the user has user admin permissions, false otherwise.
*/
function IsSubAdmin($OrFullAdmin = true)
{
    if ($OrFullAdmin && IsFullAdmin())
    {
        return true;
    }
    elseif (GetAccessLevel('ADM') == 'ADM_USER_ADMIN')
    {
        return true;
    }

    return false;
}

/**
* @desc Shortcut function. Returns true if the user has setup permsisions on the module provided, false otherwise.
*
* @param string $module The module code we are checking.
*
* @return bool true if the user has setup permsisions, false otherwise.
*/
function HasSetupPermissions($module)
{
    //if user is a full admin, they can automatically set up all modules.
    if (IsFullAdmin())
    {
        return true;
    }

    //exceptions for Module Groups. This needs to be rewritten when
    //we have a more generic way of handling these.
    if ($module == 'STD')
    {
        $module = 'STN';
    }

    return bYN(GetParm($module.'_SETUP', "N"));
}

function MakeProfileSetupField($value, $FieldType = '')
{
    global $UserLabels;

    $sql = 'SELECT recordid, pfl_name FROM profiles ORDER BY pfl_name ASC';
    $result = db_query($sql);

    $Profiles = array();

    while ($row = db_fetch_array($result))
    {
        $Profiles[$row['recordid']] = $row['pfl_name'];
    }

    $Title = ($UserLabels['ADM_PROFILES'] ? $UserLabels['ADM_PROFILES'] : 'Profiles');

    $field = Forms_SelectFieldFactory::createSelectField('ADM_PROFILES', 'ADM', $value, $FieldType, true, $Title);
    $field->setCustomCodes($Profiles);
    $field->setSuppressCodeDisplay();
    $field->setIgnoreMaxLength();

    return $field->getField();
}

function MakeIncSavedQueriesSetupField($value, $FieldType = '', $formType)
{
    global $UserLabels;

    require_once 'Source/libs/SavedQueries.php';

    $savedQueries = getSavedQueriesAccessibleToEveryone('INC');

    if (!$savedQueries)
    {
        $savedQueries = array();
    }

    $Title = ($UserLabels['INC_SAVED_QUERIES'] ? $UserLabels['INC_SAVED_QUERIES'] : _tk('choose_saved_queries'));

    if ($formType != 'ReadOnly')
    {
        $field = Forms_SelectFieldFactory::createSelectField('INC_SAVED_QUERIES', 'INC', $value, $FieldType, true, $Title);
        $field->setCustomCodes($savedQueries);
        $field->setSuppressCodeDisplay();
        return $field->getField();
    }
    else
    {
        return null;
    }
}

function MakeProfileField($value, $FieldType = '')
{
    $ADMProfiles = GetParm('ADM_PROFILES');
    $Profiles = array();

    $sql = 'SELECT recordid, pfl_name FROM profiles';

    if ($ADMProfiles)
    {
        $RestrictedProfiles = explode(' ', $ADMProfiles);

        if (!empty($RestrictedProfiles))
        {
            $sql .= ' WHERE recordid IN ('.join(', ', $RestrictedProfiles).')';
        }
    }

    $sql.= ' ORDER BY pfl_name ASC';

    $result = db_query($sql);

    $Profiles = array();

    while ($row = db_fetch_array($result))
    {
        $Profiles[$row['recordid']] = $row['pfl_name'];
    }

    $field = Forms_SelectFieldFactory::createSelectField('sta_profile', '', $value, $FieldType);
    $field->setCustomCodes($Profiles);
    $field->setSuppressCodeDisplay();

    return $field->getField();
}

/**
* @desc returns a field object that represents a dropdown populated with form designs and with the name
* of the form design global for the current module.
*
* @param string $module The current module
* @param string $value The current value of this setting
* @param string $level The current form level
* @param string $mode the current field mode
* @param bool $OverrideFormDesignGlobal To use a custom form design
*
* @return object A field object representing the form design dropdown.
*/
function MakeFormDesignField($module, $value, $level = '', $mode = '', $OverrideFormDesignGlobal = false)
{
    global $ModuleDefs;

    $FormFileName = GetFormsFilename($module);

    if (file_exists($FormFileName))
    {
        include($FormFileName);
    }
    else
    {
        $FormFiles = "";
        $FormFiles2 = "";
    }

    $Lvl1Code = $ModuleDefs[$module]["FORMS"][1]['CODE'];
    $Lvl2Code = $ModuleDefs[$module]["FORMS"][2]['CODE'];

    if (!is_array($FormFiles))
    {
        if ($Lvl1Code != "")
        {
            $Lvl1CodeDisplay = GetFormCode(array('module' => $module, 'level' => 1));
            $FormFiles = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Lvl1CodeDisplay ? ' ('.$Lvl1CodeDisplay.')' : '')." form");
        }
    }

    if (!is_array($FormFiles2) && $Lvl2Code != "")
    {
        $Lvl2CodeDisplay = GetFormCode(array('module' => $module, 'level' => 2));
        $FormFiles2 = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Lvl2CodeDisplay ? ' ('.$Lvl2CodeDisplay.')' : '')." form");
        $sL2FormDesignGlobal = $Lvl2Code . "_DEFAULT";
    }

    if ($Lvl2Code != "" && $level != '1')
    {
        $sL2FormDesignGlobal = $Lvl2Code . "_DEFAULT";
        $aFormList = $FormFiles2;
    }
    elseif ($Lvl1Code != '' && $OverrideFormDesignGlobal && $module == 'INC')
    {
        $sL2FormDesignGlobal = $Lvl1Code . '_ONLY_FORM';
        $aFormList = $FormFiles;
    }
    elseif ($Lvl1Code != '')
    {
        $sL2FormDesignGlobal = $Lvl1Code . "_DEFAULT";
        $aFormList = $FormFiles;
    }

    $field = Forms_SelectFieldFactory::createSelectField($sL2FormDesignGlobal, $module, $value, $mode);
    $field->setCustomCodes($aFormList);
    $field->setSuppressCodeDisplay();

    return $field;
}

/**
* @desc returns a field object that represents a dropdown populated with form designs and with the name
* of the search form global for the current module.
*
* @param string $module The current module
* @param string $value The current value of this setting
* @param string $level The current form level
* @param string $mode the current field mode
*
* @return object A field object representing the form design dropdown.
*/
function MakeSearchFormDesignField($module, $value, $level = '2', $mode = '')
{
    global $ModuleDefs;

    $FormFileName = GetFormsFilename($module);

    if (file_exists($FormFileName))
    {
        include($FormFileName);
    }
    else
    {
        $FormFiles = "";
        $FormFiles2 = "";
    }

    $Lvl1Code = $ModuleDefs[$module]["FORMS"][1]['CODE'];
    $Lvl2Code = $ModuleDefs[$module]["FORMS"][2]['CODE'];

    if (!is_array($FormFiles2) && $Lvl2Code != "")
    {
        $Lvl2CodeDisplay = GetFormCode(array('module' => $module, 'level' => 2));
        $FormFiles2 = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Lvl2CodeDisplay ? ' ('.$Lvl2CodeDisplay.')' : '')." form");
        $sL2SearchFormDesignGlobal = $Lvl2Code . "_SEARCH_DEFAULT";
    }

    if ($Lvl2Code != "" && $level != '1')
    {
        $sL2SearchFormDesignGlobal = $Lvl2Code . "_SEARCH_DEFAULT";
        $aFormList = $FormFiles2;
    }
    elseif ($Lvl1Code != "")
    {
        if ($FormFiles == "")
        {
            $Lvl1CodeDisplay = GetFormCode(array('module' => $module, 'level' => 1));
            $FormFiles = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Lvl1CodeDisplay ? ' ('.$Lvl1CodeDisplay.')' : '')." form");
        }

        $sL2SearchFormDesignGlobal = $Lvl1Code . "_SEARCH_DEFAULT";
        $aFormList = $FormFiles;
    }

    $field = Forms_SelectFieldFactory::createSelectField($sL2SearchFormDesignGlobal, $module, $value, $mode);
    $field->setCustomCodes($aFormList);
    $field->setSuppressCodeDisplay();

    return $field;
}

/**
* @desc returns a field object that represents a dropdown populated with form designs and with the name
* of the search form global for the current module.
*
* @param string $module The current module
* @param string $value The current value of this setting
* @param string $level The current form level
* @param string $mode the current field mode
* @param string $name The name of the field.
*
* @return object A field object representing the form design dropdown.
*/
function MakeListingFormDesignField($module, $value, $level = '2', $mode = '', $name = '')
{
    if ($name == '')
    {
        $name = $module.'_LISTING_ID';
    }

    $field = Forms_SelectFieldFactory::createSelectField($name, $module, $value, $mode);
    $field->setCustomCodes(GetListOfListingDesigns(array('module' => $module)));
    $field->setSuppressCodeDisplay();

    return $field;
}

function GetTitleTable($aParams)
{
    $Section = $aParams['section'];
    $Module = $aParams['module'];

    $TitleDiv = '
        <div class="section_title_group">';

    if ($aParams['sectionarray']['TitleSuffix'])
    {
        $aParams['title'] .= $aParams['sectionarray']['TitleSuffix'];
    }

    $TitleDiv .= '
        <div class="section_title">'._t($aParams['title']).'</div>';

    if ($aParams['subtitle'])
    {
        $TitleDiv .= '
            <div class="section_subtitle">' . _t($aParams['subtitle']) . '</div>';
    }

    if ($aParams['sectionarray']['OrderField'] && $aParams['formtype'] != 'Search')
    {
        $TitleDiv .= '
        <div class="title_subelement">
            Order: ';

        if ($aParams['formtype'] == 'Print' || $aParams['formtype'] == 'ReadOnly')
        {
            $TitleDiv .= $aParams['sectionarray']['OrderField']['value'];
        }
        else
        {
            $TitleDiv .= '
            <input type="text" size="3" maxlength="3" name="'.$aParams['sectionarray']['OrderField']['id'].'" id="'.$aParams['sectionarray']['OrderField']['id'].'" value="'.$aParams['sectionarray']['OrderField']['value'].'" onKeyPress="return IsPositiveInteger(this, event)" title="order"/>';
        }

        $TitleDiv .= '</div>';
    }

    $TitleDiv .= '
        </div>';

    if ($aParams['twisty'])
    {
        $TwistyDiv = '
            <div class="title_rhs_container">
                <a class="toggle-trigger">
                    <img id=\'twisty_image_'.$Section.'\' src="Images/collapse.gif" alt="+" border="0"/>
                </a>
            </div>';

        $TitleRowHTML = '
            <div class="section_title_wrapper">'.
            $TitleDiv.
            $TwistyDiv.
            '</div>';
    }
    elseif (!empty($aParams['sectionarray']['right_hand_link']))
    {
        $DeleteDiv = '<span style="cursor:pointer" onclick="'.$aParams['sectionarray']['right_hand_link']['onclick'].'"><b>
            '.$aParams['sectionarray']['right_hand_link']['text'].'</b></span>';

        $DeleteDiv = '
            <div class="title_rhs_container">
                <div class="section_title_right_hand_link" onclick="'.$aParams['sectionarray']['right_hand_link']['onclick'].'">
                    '.$aParams['sectionarray']['right_hand_link']['text'].'
                </div>
            </div>';

        $TitleRowHTML = '
            <div class="section_title_wrapper">'.
            $TitleDiv.
            $DeleteDiv.
            '</div>';
    }
    elseif (!empty($aParams['sectionarray']['ClearSectionOption']) && $aParams['formtype'] != 'Print' &&
        $aParams['formtype'] != 'ReadOnly' && $aParams['formtype'] != 'Search')
    {
        $Suffix = $aParams['sectionarray']['ContactSuffix'];
        $Type = $aParams['sectionarray']['DynamicSectionType'];
        $Subtype = $aParams['sectionarray']['DynamicSectionSubtype'];
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';

        if (is_numeric($_REQUEST['form_id']))
        {
            $parentFormId = (int) $_REQUEST['form_id'];
        }
        elseif (is_numeric($_REQUEST['parent_form_id']))
        {
            $parentFormId = (int) $_REQUEST['parent_form_id'];
        }
        else
        {
            $parentFormId = '';
        }

        $DeleteDiv = '
            <div class="title_rhs_container">
                <div class="section_title_right_hand_link" onclick="ReplaceSection(\''.$Module.'\','.$Suffix.', \''.$Type.'\', \''.$Subtype.'\', '.$spellChecker.', \''.$parentFormId.'\')">
                    <b>'._tk('clear_section').'</b>
                </div>
            </div>';

        $TitleRowHTML = '
            <div class="section_title_wrapper">'.
            $TitleDiv.
            $DeleteDiv.
            '</div>';
    }
    elseif (!empty($aParams['sectionarray']['DeleteSectionOption']) && $aParams['formtype'] != 'Print' &&
        $aParams['formtype'] != 'ReadOnly')
    {
        $Suffix = $aParams['sectionarray']['ContactSuffix'];
        $Type = $aParams['sectionarray']['DynamicSectionType'];

        $DeleteDiv = '
            <div class="title_rhs_container">
                <div class="section_title_right_hand_link" onclick="var DivToDelete=$(\''.$Type.'_section_div_'.$Suffix.'\');DivToDelete.parentNode.removeChild(DivToDelete)">
                    <b>'._tk('delete_section').'</b>
                </div>
            </div>';

        $TitleRowHTML = '
            <div class="section_title_wrapper">'.
            $TitleDiv.
            $DeleteDiv.
            '</div>';
    }
    else
    {
        $TitleRowHTML = $TitleDiv;
    }

    return $TitleRowHTML;
}

/**
* @desc Inserts an array entry (or entries) before a particular key in an existing array, passed by reference.
*
* @param array $array The array to be amended.
* @param string $position The key before which the new entries shoud be placed.
* @param array $insert_array Array of entries to be inserted.
*/
function array_insert_datix(&$array, $position, $insert_array)
{
    if (!is_int($position))
    {
        $i = 0;

        foreach ($array as $key => $value)
        {
            if ($key == $position)
            {
                $position = $i;
                break;
            }

            $i++;
        }
    }

    $first_array = array_splice ($array, 0, $position);
    $array = array_merge ($first_array, $insert_array, $array);
}

/**
* @desc Basic file-writing function: opens a file, writes to it and closes it, printing an error if
* there is a problem.
*
* @param string $contents The data to be written to the file.
* @param string $path The location of the file.
*/
function writeToFile($contents, $path)
{
    $handle=@fopen(Sanitize::SanitizeFilePath($path), 'w+');

    if (!$handle || fwrite($handle, $contents) === false)
    {
        logErrorReport(array(
        'message' => 'Failed to write to file',
        'details' => 'When attempting to write to "'.$path.'", the system encountered an error.'));
    }

    if($handle)
    {
        fclose($handle);
    }
}

/**
* @desc Returns a list of form design names for a particular module and level, keyed by the form id.
*
* @param array $aParams Array of parameters.
* @param string $aParams['module'] The module in question.
* @param string $aParams['level'] The form level in question.
*
* @return array List of form designs keyed by form id.
*/
function GetListOfFormDesigns($aParams)
{
    global $ClientFolder, $ModuleDefs;

    if (file_exists($ClientFolder.'/'.$ModuleDefs[$aParams['module']]['FORM_DESIGN_ARRAY_FILE']))
    {
        include($ClientFolder.'/'.$ModuleDefs[$aParams['module']]['FORM_DESIGN_ARRAY_FILE']);

        if ($aParams['level'] == 1)
        {
            if (!empty($FormFiles))
            {
                $FormArray = $FormFiles;
            }
            else
            {
                $FormArray = array(0 => 'Default '.$ModuleDefs[$aParams['module']]['REC_NAME'].' Form');
            }
        }
        elseif ($aParams['level'] == 2)
        {
            if (!empty($FormFiles2))
            {
                $FormArray = $FormFiles2;
            }
            else
            {
                $FormArray = array(0 => 'Default '.$ModuleDefs[$aParams['module']]['REC_NAME'].' Form');
            }
        }
    }
    else
    {
        $FormArray = array(0 => 'Default '.$ModuleDefs[$aParams['module']]['REC_NAME'].' Form');
    }

    //When listing linked form designs we need to have a "None selected" option to allow users to fall back on globals.
    if (in_array($aParams['module'], array('ACT', 'ELE', 'PRO', 'CON', 'AST', 'PAY')))
    {
        $FormArray[null] = 'None Selected';
    }

    return $FormArray;
}

/**
* @desc Returns a list of listing design names for a particular module, keyed by the design recordid.
*
* @param array $aParams Array of parameters.
* @param string $aParams['module'] The module in question.
*
* @return array List of listing designs keyed by design recordid.
*/
function GetListOfListingDesigns($aParams)
{
    $sql ='SELECT recordid, LST_TITLE from WEB_LISTING_DESIGNS WHERE lst_module = :module';

    $ListingDesigns = DatixDBQuery::PDO_fetch_all($sql, array('module' => $aParams['module']));

    $ReturnArray[0] = 'Datix Listing Design Template';

    foreach ($ListingDesigns as $Design)
    {
        $ReturnArray[$Design['recordid']] = $Design['LST_TITLE'];
    }

    return $ReturnArray;
}

/**
* @desc Picks up validation messages from $_POST (via the $data array) and $_SESSION for a particular field
* and returns them wrapped in the appropriate HTML.
*
* @param array $Data array data for the current record.
* @param string $InputFieldName the field to check for validation errors.
*
* @return string HTML validation error to be inserted into the page.
*/
function GetValidationErrors($Data, $InputFieldName, $Parameters = array())
{
    if (is_array($Data["error"]["Validation"]))
    {
        $ErrorArray = $Data["error"]["Validation"][$InputFieldName];
    }

    if (!$ErrorArray && is_array($_SESSION['MESSAGES']['VALIDATION']))
    {
        $ErrorArray = $_SESSION['MESSAGES']['VALIDATION'][$InputFieldName];
        $_SESSION['MESSAGES']['VALIDATION'][$InputFieldName] = array();
    }

    $tag = ($Parameters['inline'] ? 'span' : 'div');

    if (!empty($ErrorArray))
    {
        if (is_array($ErrorArray))
        {
            foreach ($ErrorArray as $Error)
            {
                $ErrorText .= '<'.$tag.' class="field_error"><img src="images/Warning.gif" /> '
                    . $Error.
                  '</'.$tag.'>';
            }
        }
        else
        {
            $ErrorText = '<'.$tag.' class="field_error"><img src="images/Warning.gif" /> '
                        . $ErrorArray.
                      '</'.$tag.'>';
        }
    }

    return $ErrorText;
}

/**
* @desc Merges arrays even if the variables passed are not actually array type. Put in because in php v5
* array_merge required array-type parameters, where some of the parameters we were passing were null. Anything
* that is not an array will be ignored.
*
* @param array $aArrays array of "arrays" to be merged.
*
* @return array Merge of any arrays passed.
*/
function SafelyMergeArrays($aArrays)
{
    $TotalArray = array();

    foreach ($aArrays as $Array)
    {
        if (is_array($Array))
        {
            $TotalArray = array_merge($TotalArray, $Array);
        }
    }

    return $TotalArray;
}

function GetCodeDescriptions($module, $fieldname, $fielddata, $table = "", $Delimiter = "; ")
{
    $MultiCode = explode(" ", \UnicodeString::rtrim($fielddata));

    foreach ($MultiCode as $SingleCode)
    {
        $CodeDescriptions[] .= code_descr($module, $fieldname, $SingleCode, $table);
    }

    $DescriptionList = implode($Delimiter, $CodeDescriptions);
    return $DescriptionList;
}

/**
* @desc Gets the form code to use for a particular module and level (e.g. DIF1 for incidents level 1).
*
* @return string Module form code.
*/
function GetFormCode($aParams)
{
    global $ModuleDefs;

    if (!$ModuleDefs[$aParams['module']]["FORMS"][$aParams['level']]['HIDECODE'])
    {
        return $ModuleDefs[$aParams['module']]["FORMS"][$aParams['level']]['CODE'];
    }
}

/**
* @desc Gets contact data array for currently logged in user.
*
* @return array Contact data array for logged in user.
*/
function SetUpDefaultReporter()
{
    global $ModuleDefs;

    if ($_SESSION['initials'])
    {
        $sql = 'SELECT recordid, recordid AS con_id, '.
                implode(', ',$ModuleDefs['CON']['FIELD_ARRAY']).
                ' FROM contacts_main
                WHERE contacts_main.initials = \''.$_SESSION['initials'].'\'';
        $data = db_fetch_array(db_query($sql));
    }

    return $data;
}

/**
* @desc Gets contact data array for currently logged in user and (because this is called from incidents)
 * populates fields used with non-contact-form-reporter section.
*
* @return array Contact data array for logged in contact.
*/
function SetUpDefaultReporterINC()
{
    global $ModuleDefs;

    if ($_SESSION['initials'])
    {
        $sql = 'SELECT '.
            implode(', ',$ModuleDefs['CON']['FIELD_ARRAY']).
            ' FROM contacts_main
            WHERE contacts_main.initials = \''.$_SESSION['initials'].'\'';

        $con = db_fetch_array(db_query($sql));

        $inc['inc_repname'] = $con['con_forenames']
                                    . ($con['con_forenames'] ? ' ' : '' ) . $con['con_surname'];
        $inc['inc_rep_tel'] = $con['con_tel1'];
        $inc['inc_rep_email'] = $con['con_email'];

        $ReportedBy = $_SESSION["Globals"]["DIF_2_REPORTEDBY"];

        if ($ReportedBy == "")
        {
            $ReportedBy = "con_subtype";
        }

        $inc["inc_reportedby"] = code_descr("CON", $ReportedBy, $con[$ReportedBy]);

    }

    return $inc;
}

/**
* @desc Returns an array of all contacts linked to a particular record, keyed by their link_type.
*
* @param array $aParams Array of parameters
* @param int $aParams['recordid'] Record ID
* @param int $aParams['module'] Current module.
* @param int $aParams['formlevel'] Level of form (e.g. 1 for DIF1, 2 for DIF2).
 * @param bool $overrideSecurity Overrides the security applied to the where clause if we are coming from a logged out form.
*
* @return array Array of contact data keyed by link_type
*/
function GetLinkedContacts($aParams, $overrideSecurity = false)
{
    global $ModuleDefs;

    // If user doesn't have access to the module he should not see any data
    if ($overrideSecurity === false && !CanSeeModule('CON'))
    {
        return [];
    }

    $recordid  = $aParams['recordid'];
    $module    = $aParams['module'];
    $formlevel = $aParams['formlevel'];

    //  This section populates $con, which will be passed on holding the contacts info

    $sqlArray = array('contacts_main.recordid as recordid', 'contacts_main.recordid as con_id', 'link_contacts.link_recordid as link_recordid',
                      'contacts_main.updateid as updateid', $ModuleDefs[$module]['FK']);

    foreach ($ModuleDefs['CON']['FIELD_ARRAY'] as $ConField)
    {
        $sqlArray[] = 'contacts_main.'.$ConField.' as '.$ConField;
    }

    foreach (getContactLinkFields() as $ConField)
    {
        $sqlArray[] = 'link_contacts.'.$ConField.' as '.$ConField;
    }

    $sql = 'SELECT '.implode(' , ', $sqlArray)."
        FROM link_contacts, contacts_main
        WHERE link_contacts.con_id = contacts_main.recordid
        AND ".$ModuleDefs[$module]['FK']." = " . $recordid .'';

    // Apply security where clause to linked contacts so that users see the appropriate linked data
    if ($overrideSecurity === false)
    {
        $securityWhere = MakeSecurityWhereClause('', 'CON', $_SESSION['initials']);
    }

    if ($securityWhere != '')
    {
        $sql .= ' AND '.$securityWhere;
    }

    $request = db_query($sql);

    while ($con1 = db_fetch_array($request))
    {
        if ($module == 'INC')
        {
            require_once 'Source/incidents/Injuries.php';
            $con1 = GetLinkedInjuries(array('data' => $con1));
        }

        if ($formlevel == 1) //to split into 'fake' reporter section
        {
            if ($con1['link_type'] == 'N' && $con1['link_role'] == GetParm('REPORTER_ROLE', 'REP'))
            {
                $con1['link_type'] = 'R';
            }

            //Fake witness section
            if ($con1['link_type'] == 'N' && $con1['link_role'] == "WITN")
            {
                $con1['link_type'] = "W";
            }

            //Fake assailant section
            if ($con1['link_type'] == 'N' && $con1['link_role'] == "PERP")
            {
                $con1['link_type'] = "L";
            }
        }

        $con1["link_exists"] = true;

        $con[$con1['link_type']][] = $con1;
    }

    if($aParams['module'] == 'CLA')
    {
        //Get individual respondents
        $sqlArray = array('contacts_main.recordid as recordid', 'contacts_main.recordid as con_id',
            'link_respondents.recordid as link_recordid', 'contacts_main.updateid as updateid');
        foreach ($ModuleDefs['CON']['FIELD_ARRAY'] as $ConField)
        {
            $sqlArray[] = 'contacts_main.'.$ConField.' as '.$ConField;
        }
        foreach (array('link_role', 'link_type', 'link_notes', 'link_resp') as $ConField)
        {
            $sqlArray[] = 'link_respondents.'.$ConField.' as '.$ConField;
        }
        $sql = 'SELECT '.implode(' , ', $sqlArray)."
        FROM link_respondents, contacts_main
        WHERE link_respondents.con_id = contacts_main.recordid
        AND link_respondents.main_recordid = " . $recordid;
        $result = \DatixDBQuery::PDO_fetch_all($sql, array());
        foreach($result as $respondent)
        {
            $con['O'][] = $respondent;
        }

        //Get organisations
        $sqlArray = array('organisations_main.recordid as recordid', 'organisations_main.recordid as org_id',
            'link_respondents.recordid as link_recordid', 'organisations_main.updateid as updateid');
        foreach ($ModuleDefs['ORG']['FIELD_ARRAY'] as $OrgField)
        {
            $sqlArray[] = 'organisations_main.'.$OrgField.' as '.$OrgField;
        }
        foreach (array('link_role', 'link_type', 'link_notes', 'link_resp') as $OrgField)
        {
            $sqlArray[] = 'link_respondents.'.$OrgField.' as '.$OrgField;
        }
        $sql = 'SELECT '.implode(' , ', $sqlArray)."
        FROM link_respondents, organisations_main
        WHERE link_respondents.org_id = organisations_main.recordid
        AND link_respondents.main_recordid = " . $recordid;
        $result = \DatixDBQuery::PDO_fetch_all($sql, array());

        foreach($result as $respondent)
        {
            $con['G'][] = $respondent;
        }
    }

    return $con;
}

/**
* @desc Returns an array of all Equipment linked to a particular record, keyed by their link_type.
*
* @param array $aParams Array of parameters
* @param int $aParams['recordid'] Record ID
* @param int $aParams['module'] Current module.
* @param int $aParams['formlevel'] Level of form.
*
* @return array Array of contact data keyed by link_type
*/
function GetLinkedEquipment($aParams)
{
    global $ModuleDefs;

    $recordid = $aParams['recordid'];
    $module = $aParams['module'];
    $formlevel = $aParams['formlevel'];

    //  This section populates $ast, which will be passed on holding the equipment info

    $sqlArray[] = 'assets_main.recordid as recordid';
    $sqlArray[] = 'assets_main.recordid as ast_id';
    $sqlArray[] = 'link_assets.link_recordid as link_recordid';
    $sqlArray[] = 'link_assets.link_type as link_type';

    $sqlArray[] = 'assets_main.updateid as updateid';

    $sqlArray[] = $ModuleDefs[$module]['FK'];

    foreach ($ModuleDefs['AST']['FIELD_ARRAY'] as $AstField)
    {
        $sqlArray[] = 'assets_main.'.$AstField.' as '.$AstField;
    }

    foreach (\src\ast\controllers\AssetsController::getEquipmentLinkFields() as $AstField)
    {
        $sqlArray[] = 'link_assets.'.$AstField.' as '.$AstField;
    }

    $sql = 'SELECT '.implode(' , ', $sqlArray)."
        FROM link_assets, assets_main
        WHERE link_assets.ast_id = assets_main.recordid
        AND ".$ModuleDefs[$module]['FK']." = " . $recordid .'';

    $request = db_query($sql);

    while ($ast1 = db_fetch_array($request))
    {
        $ast1["link_exists"] = true;

        $ast[$ast1['link_type']][] = $ast1;
    }

    return $ast;
}

/**
* @desc Returns an array of all contacts linked to a particular complaint, keyed by their link_type.
*
* @param array $aParams Array of parameters
* @param int $aParams['recordid'] Complaint recordid
* @param int $aParams['formlevel'] Level of form (e.g. 1 for DIF1, 2 for DIF2).
 * * @param bool $overrideSecurity Overrides the security applied to the where clause if we are coming from a logged out form.
*
* @return array Array of contact data keyed by link_type
*/
function GetLinkedContactsForComplaints($aParams, $overrideSecurity = false)
{
    global $ModuleDefs;

    // If user doesn't have access to the module he should not see any data
    if ($overrideSecurity === false && !CanSeeModule('CON'))
    {
        return [];
    }

    $recordid = $aParams['recordid'];
    $module = 'COM';
    $formlevel = $aParams['formlevel'];

    $sqlArray[] = 'contacts_main.recordid as recordid';
    $sqlArray[] = 'contacts_main.recordid as con_id';
    $sqlArray[] = 'link_contacts.link_recordid as link_recordid';

    $sqlArray[] = 'contacts_main.updateid as updateid';

    $sqlArray[] = $ModuleDefs[$module]['FK'];

    foreach ($ModuleDefs['CON']['FIELD_ARRAY'] as $ConField)
    {
        $sqlArray[] = 'contacts_main.'.$ConField.' as '.$ConField;
    }

    foreach (getContactLinkFields() as $ConField)
    {
        $sqlArray[] = 'link_contacts.'.$ConField.' as '.$ConField;
    }

    $sql = 'SELECT '.implode(' , ', $sqlArray)."
        FROM link_contacts, contacts_main
        WHERE link_contacts.con_id = contacts_main.recordid
        AND ".$ModuleDefs[$module]['FK']." = " . $recordid;

    // Apply security where clause to linked contacts so that users see the appropriate linked data
    if ($overrideSecurity === false)
    {
        $securityWhere = MakeSecurityWhereClause('', 'CON', $_SESSION['initials']);
    }

    if ($securityWhere != '')
    {
        $sql .= ' AND '.$securityWhere;
    }

    $contacts = DatixDBQuery::PDO_fetch_all($sql);

    foreach ($contacts as $con1)
    {
        if ($formlevel == 1) //to split into 'fake' witness and reporter sections
        {
            if ($con1['link_type'] == 'N' && $con1['link_role'] == GetParm('REPORTER_ROLE', 'REP'))
            {
                $con1['link_type'] = 'R';
            }

            if ($con1['link_type'] == 'N' && $con1['link_role'] == 'WITN')
            {
                $con1['link_type'] = 'W';
            }
        }

        $con1["link_exists"] = true;

        $con[$con1['link_type']][] = $con1;
    }

    //If a contact is saved as a Person ('A') with lcom_iscomplpat = Y, they also need to appear
    // on the form as a complainant ('C')
    $sql = 'SELECT con_id, lcom_iscomplpat, lcom_primary
        FROM link_compl
        WHERE lcom_current = \'Y\' AND com_id = '.$recordid;

    $request = db_query($sql);

    while ($con1 = db_fetch_array($request))
    {
        if ($con1['lcom_iscomplpat'] == 'Y')
        {
            if (is_array($con['A']))
            {
                foreach ($con['A'] as $Person)
                {
                    if ($Person['recordid'] == $con1['con_id'])
                    {
                        $Person = array_merge($Person, $con1);
                        $con['C'][] = $Person;
                    }
                }
            }
        }
    }

    return $con;
}

/**
* @desc Gets the contents of the notepad field for a given record.
*
* @param int $recordid ID of current record
* @param string $module Current module
*
* @return string Contents of notepad field.
*/
function GetNotepad($recordid, $module)
{
    global $ModuleDefs;

    $sql = "SELECT notes FROM notepad WHERE ".$ModuleDefs[$module]['FK']." = $recordid";
    $request = db_query($sql);

    if ($row = db_fetch_array($request))
    {
        return $row["notes"];
    }
}

/**
* @desc Simple function used when loading records.
*
* @return bool true if this user has permission to edit the record, false if not.
*/
function CanEditRecord($aParams)
{
    return (GetEditPermissions($aParams) == 'E');
}

/**
* @desc Finds the read/write permissions for a user and a module.
*
* @param array $aParams Array of parameters
* @param string $aParams['perms'] Access level for user in question
* @param string $aParams['module'] Current module
* @param array $aParams['data'] Data array for current record
*
* @return string 'E' for editable, 'R' for read only
*/
function GetEditPermissions($aParams)
{
    $sql = 'SELECT access FROM link_access_approvalstatus WHERE
            access_level = \''.$aParams['perms'].'\' AND
            module = \''.$aParams['module'].'\' AND
            code = \''.$aParams['data']['rep_approved'].'\' AND
            las_workflow = '.GetWorkflowID($Module);

    $row = db_fetch_array(db_query($sql));

    return $row['access'];
}

/**
* @desc Simple function used when drawing contacts sections.
*
* @return bool true if this user has permission to link contacts to records, false otherwise.
*/
function CanLinkNewContacts()
{
    // "cascade permissions" mean that permissions for the main module cascade onto linked
    // modules. Since the user has permission to the main module, he has permission to the contacts linked.
    if (bYN(GetParm('CASCADE_PERMISSIONS', 'N')))
    {
        return true;
    }
    else
    {
        //otherwise, find the user's contact module permissions.
        $AccessFlag = GetAccessFlag('CON', GetAccessLevel('CON'), 'NEW');
        return ($AccessFlag == 'E');
    }
}

/**
* @desc Simple function used when drawing contacts sections.
*
* @return bool true if this user has permission to link contacts to records, false otherwise.
*/
function CanAccessContacts()
{
    // "cascade permissions" mean that permissions for the main module cascade onto linked
    // modules. Since the user has permission to the main module, he has permission to the contacts linked.
    if (bYN(GetParm('CASCADE_PERMISSIONS', 'Y')))
    {
        return true;
    }
    else
    {
        return (GetAccessLevel('CON') != 'CON_INPUT_ONLY' && GetAccessLevel('CON') != '');
    }
}

/**
* @desc Populates $CurrentApproveObj and $ApprovalObj, which are referred to in BasicFormXXX.php.
*
* @param string $Module Current module
* @param array $Data Data array for current record
* @param obj $CurrentApproveObj Reference to object to populate with field object for the current approval status field
* @param obj $ApproveObj Reference to object to populate with field object for the approval status field
* @param string $FormType Type of form (Print, ReadOnly, Design etc...)
* @param object $FormDesign Contains current form design settings.
* @param string $PermOverride Permissions to use if CASCADE_PERMISSIONS global is set (used in contacts).
*/
function SetUpApprovalArrays($Module, $Data, &$CurrentApproveObj, &$ApproveObj, $FormType = '',
                             $FormDesign = null, $PermOverride = '', $HighestAccessLevel = '')
{
    global $ModuleDefs, $AccessLvlDefs, $ExpandSections, $ExpandFields, $DefaultValues;

    if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS', 'N')) && $HighestAccessLevel)
    {
        $UserPerm = $HighestAccessLevel;
    }
    else
    {
        $UserPerm = GetParm($ModuleDefs[$Module]['PERM_GLOBAL']);
    }
    // "cascade permissions" mean that permissions for the main module cascade onto linked modules.
    $Perms = (bYN(GetParm('CASCADE_PERMISSIONS', 'Y')) && $PermOverride ? $PermOverride : $UserPerm);
    $CurrLevel = ($CurrLevel ? $CurrLevel : 'NEW');

    $ApprovalLabel = $FormDesign->UserLabels['rep_approved'];

    // The following code is normally executed in FormClasses under MakeForm, but that function
    // hasn't been called yet, so we have to put it in earlier for rep_approved.
    // This could maybe be removed later if this condition handling is improved/made more generic.

    // Reformat $ExpandSections
    if ($ExpandSections['rep_approved'])
    {
        foreach ($ExpandSections['rep_approved'] as $ExpandSectionKey => $ExpandSection)
        {
            // Make up the condition for displaying the section
            foreach ($ExpandSection["values"] as $ExpandSectionValues)
            {
                $ExpandStrings[] = 'parentValue == \'' . $ExpandSectionValues . '\'';
            }

            $ExpandString = implode('||', $ExpandStrings);
            $ExpandSections['rep_approved'][$ExpandSectionKey]["alertcondition"] = $ExpandString;
            $ExpandStrings = "";
        }
    }

    // Reformat $ExpandFields
    if ($ExpandFields['rep_approved'])
    {
        foreach ($ExpandFields['rep_approved'] as $ExpandFieldKey => $ExpandField)
        {
            // Make up the condition for displaying the section
            foreach ($ExpandField["values"] as $ExpandFieldValues)
            {
                $ExpandStrings[] = 'parentValue == \'' . $ExpandFieldValues . '\'';
            }

            $ExpandString = implode('||', $ExpandStrings);
            $ExpandFields['rep_approved'][$ExpandFieldKey]["alertcondition"] = $ExpandString;
            $ExpandStrings = "";
        }
    }

    ///-- end of Expand reformating

    //Current approval status field is always read only.
    $CurrentApproveArray = array();
    $CurrentApproveArray = GetLevelFieldLabels($Module);
    $CurrentApproveObj = Forms_SelectFieldFactory::createSelectField(
        'rep_approved_display',
        $Module,
        $Data['rep_approved_display'],
        'ReadOnly'
    );
    $CurrentApproveObj->setCustomCodes($CurrentApproveArray);

    if ($Data['rep_approved'] == 'REJECT' && $FormType != 'Search' &&
        CanMoveRecord(array('module' => $Module, 'data' => $Data, 'perms' => $Perms)))
    {
        $FieldType = 'Edit';
    }
    else
    {
        $FieldType = $FormType;
    }

    $ApproveObj = new FormField($FieldType);

    $ApproveArray = array();

    if ($FormDesign->HideFields['rep_approved'] && $FormType != 'Design')
    {
        if ($FormType != 'Search' && $FormDesign->DefaultValues['rep_approved'])
        {
            $ApproveObj->MakeCustomField('<input type="hidden" name="rep_approved" id="rep_approved" value="' . $FormDesign->DefaultValues['rep_approved'] . '" />');
        }
    }
    else
    {
        if ($FormType == 'Search')
        {
            if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')))
            {
                $unorderedApprovalStatuses = array();

                $accessLevels = $_SESSION['CurrentUser']->getAccessLevels($Module);

                foreach($accessLevels as $accessLevel)
                {
                    $unorderedApprovalStatuses = array_merge($unorderedApprovalStatuses, GetLevelstoView($Module, $accessLevel));
                }

                $sql = "
                        SELECT code_approval_status.code as code
                        FROM code_approval_status
                        JOIN link_access_approvalstatus
                            ON
                              code_approval_status.code = link_access_approvalstatus.code
                            AND
                              code_approval_status.module = link_access_approvalstatus.module
                            AND
                              code_approval_status.workflow = link_access_approvalstatus.las_workflow
                        WHERE
                              code_approval_status.module like '$Module'
                            AND
                              code_approval_status.workflow = ".GetWorkflowID($Module)."
                        ORDER BY code_approval_status.cod_listorder";

                $orderedApprovalStatuses = DatixDBQuery::PDO_fetch_all($sql,array(), PDO::FETCH_COLUMN);

                foreach ($orderedApprovalStatuses as $orderedApprovalStatusCode)
                {
                    if (isset($unorderedApprovalStatuses[$orderedApprovalStatusCode]))
                    {
                        $ApproveArray[$orderedApprovalStatusCode] = $unorderedApprovalStatuses[$orderedApprovalStatusCode];
                    }
                }
            }
            else
            {
                $ApproveArray = GetLevelstoView($Module, $Perms);
            }

            $ApprovalValue = $Data['rep_approved'];

            unset($ApproveArray['NEW']);
        }
        elseif ($FormType == 'Design')
        {
            $ApproveArray = GetLevelFieldLabels($Module);

            $ApprovalValue = $DefaultValues['rep_approved'];

            unset($ApproveArray['NEW']);
        }
        else
        {
            if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && $FormType == 'New')
            {
                $unorderedApprovalStatuses = array();

                $accessLevels = array();

                if ($_SESSION['CurrentUser'] instanceof \src\users\model\User)
                {
                    $accessLevels = $_SESSION['CurrentUser']->getAccessLevels($Module);
                }

                if (count($accessLevels) > 0)
                {
                    foreach ($accessLevels as $accessLevel)
                    {
                        $unorderedApprovalStatuses = array_merge($unorderedApprovalStatuses, GetLevelsTo($Module, $accessLevel, $Data['rep_approved_display']));
                    }

                    $sql = "
                        SELECT code_approval_status.code as code
                        FROM code_approval_status
                        JOIN link_access_approvalstatus
                            ON
                              code_approval_status.code = link_access_approvalstatus.code
                            AND
                              code_approval_status.module = link_access_approvalstatus.module
                            AND
                              code_approval_status.workflow = link_access_approvalstatus.las_workflow
                        WHERE
                              code_approval_status.module like '$Module'
                            AND
                              code_approval_status.workflow = ".GetWorkflowID($Module)."
                        ORDER BY code_approval_status.cod_listorder";

                    $orderedApprovalStatuses = DatixDBQuery::PDO_fetch_all($sql,array(), PDO::FETCH_COLUMN);

                    foreach ($orderedApprovalStatuses as $orderedApprovalStatusCode)
                    {
                        if (isset($unorderedApprovalStatuses[$orderedApprovalStatusCode]))
                        {
                            $ApproveArray[$orderedApprovalStatusCode] = $unorderedApprovalStatuses[$orderedApprovalStatusCode];
                        }
                    }
                }
                else
                {
                    $ApproveArray = GetLevelsTo($Module, 'NONE', $Data['rep_approved_display']);
                }
            }
            else
            {
            $ApproveArray = GetLevelsTo($Module, ($Perms ? $Perms : 'NONE'), $Data['rep_approved_display']);
            }

            if (isset($ApproveArray[$Data['rep_approved']]))
            {
                $ApprovalValue = $Data['rep_approved'];
            }

            if ($ApprovalValue == '' && $ApproveArray[$FormDesign->DefaultValues['rep_approved']])
            {
                $ApprovalValue = $FormDesign->DefaultValues['rep_approved'];
            }
            
            if ($ApprovalValue == '' && isset($DefaultValues['rep_approved']) )
            {
            	$ApprovalValue = $DefaultValues['rep_approved'];
            }
        }

        if (empty($ApproveArray)) //no permissions to save
        {
            $ApproveObj->MakeCustomField('No permissions to save record');
        }
        else
        {
            $ApproveObj = Forms_SelectFieldFactory::createSelectField(
                'rep_approved',
                $Module,
                $ApprovalValue,
                $FieldType,
                false,
                ($ApprovalLabel ? $ApprovalLabel :'Approval status after save')
            );
            $ApproveObj->setCustomCodes($ApproveArray);
        }
    }
}

/**
* @desc Returns basicform array for rep_approved field. This field has complex logic associated with it, and putting
* all the logic in the BasicForm file has become too confusing.
*
* @param array $aParams Array of parameters
* @param string $aParams[formtype] The current form state (e.g. New, Edit, Search, Design, ReadOnly, Print...)
* @param array $aParams[data] The current record data.
* @param string $aParams[module] The current module.
* @param string $aParams[perms] The access level of the logged in user.
* @param obj $aParams[approveobj] A FormField object representing the dropdown to be displayed.
*
* @returns array BasicForm array entry.
*/
function RepApprovedBasicFormArray($aParams)
{
    global $ModuleDefs;

    return array(
        'Name' => 'rep_approved',
        'Title' => 'Approval status' . ((($aParams['formtype'] != "Search" && $aParams['data']['recordid']) || $aParams['formtype'] == 'Design') ?  ' after save' : ''),
        'Type' => 'formfield',
        'Condition' => RepApprovedBasicFormArrayCondition($aParams),
        'IgnoreHidden' => true,
        'AlwaysMandatory' => true,
        'NoReadOnly' => true,
        'AllowDefault' => true,
        'ValueRequired' => true,
        'FormField' => $aParams['approveobj']
    );
}

/**
* @desc Runs through the logic to check whether the approval status dropdown should be displayed.
*
* @param array $aParams Array of parameters
* @param string $aParams[formtype] The current form state (e.g. New, Edit, Search, Design, ReadOnly, Print...)
* @param array $aParams[data] The current record data.
* @param string $aParams[module] The current module.
* @param obj $aParams[approveobj] A FormField object representing the dropdown to be displayed.
*
* @returns bool True if this field should be available for display, false if not.
*/
function RepApprovedBasicFormArrayCondition($aParams)
{
    if ($aParams['formtype'] == 'Design')
    {
        return true;
    }

    if ($aParams['formtype'] == 'Search')
    {
        return true;
    }

    if ($aParams['formtype'] == 'Print')
    {
        return false;
    }

    if ($aParams['formtype'] == 'ReadOnly' && $aParams['data']['rep_approved'] != 'REJECT')
    {
        return false;
    }

    if (!isset($aParams['approveobj']))
    {
        return false;
    }

    return true;
}

/**
* @desc Returns basicform array for rep_approved_display field. This field has complex logic associated with it,
 * and putting all the logic in the BasicForm file has become too confusing.
*
* @param array $aParams Array of parameters
* @param string $aParams[formtype] The current form state (e.g. New, Edit, Search, Design, ReadOnly, Print...)
* @param array $aParams[data] The current record data.
* @param string $aParams[module] The current module.
* @param string $aParams[perms] The access level of the logged in user.
* @param obj $aParams[currentapproveobj] A FormField object representing the text to be displayed.
*
* @returns array BasicForm array entry.
*/
function RepApprovedDisplayBasicFormArray($aParams)
{
    global $ModuleDefs, $AccessLvlDefs;

    return array(
        'Name' => 'rep_approved_display',
        'Title' => 'Current approval status',
        'Type' => 'formfield',
        'NoMandatory' => true,
        'NoReadOnly' => true,
        'Condition' => RepApprovedDisplayBasicFormArrayCondition($aParams),
        'FormField' => $aParams['currentapproveobj']
    );
}

/**
* @desc Runs through the logic to check whether the current approval status description should be displayed.
*
* @param array $aParams Array of parameters
* @param string $aParams[formtype] The current form state (e.g. New, Edit, Search, Design, ReadOnly, Print...)
* @param string $aParams[perms] The access level of the logged in user.
* @param string $aParams[module] The current module.
* @param obj $aParams[currentapproveobj] A FormField object representing the text to be displayed.
*
* @returns bool True if this field should be available for display, false if not.
*/
function RepApprovedDisplayBasicFormArrayCondition($aParams)
{
    global $ModuleDefs, $AccessLvlDefs;

    if ($aParams['formtype'] == 'Print')
    {
        return true;
    }

    if (!$_SESSION['logged_in'])
    {
        return false;
    }

    if ($aParams['formtype'] == 'Design')
    {
        return true;
    }

    if ($aParams['formtype'] == 'Search')
    {
        return false;
    }

    if (!isset($aParams['currentapproveobj']))
    {
        return false;
    }

    return true;
}

/**
* @desc Returns the suffix text for a rejected record to identify who rekected it.
*
* @param array $data Current record data
* @param string $module The current module.
* @param string $HandlerField The field to use to match the staff code of the contact who rejected the record.
* @param int $ReasonName The record id of the contact who rejected the record
*
* @returns string Text to display after the form name for a rejected record.
*/
function GetRejectedSuffix($data, $module, $HandlerField, $ReasonName)
{
    if ($data["rep_approved"] == 'REJECT' && bYN(GetParm("REJECT_REASON",'Y')) && $ReasonName)
    {
        $Suffix = " (" . _tk('rejected_by') . " " . $ReasonName . ")";
    }
    elseif ($data["rep_approved"] == 'REJECT')
    {
        if ($data["updatedby"])
        {
            $Suffix = " (" . _tk('rejected_by') .' '. code_descr($module, $HandlerField, $data["updatedby"], '', false) . ")";
        }
        else
        {
            $Suffix = " (rejected)";
        }
    }

    return $Suffix;
}

function SetUpFormTypeAndApproval($module, &$data, &$FormType, $form_action, $Perms)
{
    global $ModuleDefs;

    $AccessFlag = GetAccessFlag($module, $Perms, $data['rep_approved']);

    if ($AccessFlag == 'R')
    {
        $FormType = "ReadOnly";
    }
    elseif ($AccessFlag == 'E')
    {
        $FormType = "Edit";
    }

    if (empty($data['error']))
    {
        $data['rep_approved_display'] = $data['rep_approved'];
    }

    if (empty($data) || ($data['recordid'] == '' ))
    {
        $FormType = "New";
        $data['rep_approved_display'] = 'NEW';
    }

    if ($form_action == "search")
    {
        $FormType = "Search";
    }

    if ($form_action == "ReadOnly")
    {
        $FormType = "ReadOnly";
    }

    if ($_GET["print"] == 1 || $form_action == "Print")
    {
        $FormType = "Print";
    }
}
  /**
  * @desc Checks whether a given record is locked or not by trying to lock it.
   * Updates FormType and sLockMessage parameters with results.
  *
  * @param string $module The module we are checking
  * @param array $data The record data
  * @param string $FormType The current form state (e.g. New, Edit, ReadOnly, Search, Design, Print...)
  * @param string $sLockMessage The message to display at the top of the form.
  */
function CheckForRecordLocks($module, $data, &$FormType, &$sLockMessage)
{
    global $ModuleDefs;

    if ($data["recordid"] && $FormType != "Locked" && $FormType != "ReadOnly" && $FormType != "Print" &&
        $FormType != "Search" && $FormType != "Design" && bYN(GetParm("RECORD_LOCKING","N")))
    {
        require_once 'Source/libs/RecordLocks.php';
        $aReturn = LockRecord(array(
            'formtype' => $FormType,
            "link_id" => $data["recordid"],
            "table" => $ModuleDefs[$module]['TABLE']
        ));

        $sLockMessage = $aReturn["lock_message"];

        if ($aReturn["formtype"])
        {
            $FormType = $aReturn["formtype"];
        }
    }
}

function UnlockLinkedRecords($aParams)
{
    global $ModuleDefs;

    if (bYN(GetParm("RECORD_LOCKING","N")) && $aParams['main']['recordid'] && $aParams['link']['recordid'])
    {
        require_once 'Source/libs/RecordLocks.php';
        UnlockRecord(array(
            "table" => $ModuleDefs[$aParams['link']['module']]['TABLE'],
            "link_id" => $aParams['link']['recordid']
        ));

        if ($rbWhat != "cancel")
        {
            UnlockRecord(array(
                "table" => $ModuleDefs[$aParams['main']['module']]['TABLE'],
                "link_id" => $aParams['main']['recordid']
            ));
        }
    }
}

/**
* @desc Generic version of UnlockRecord - takes just the module and recordid and calls
 * the unlock record stored procedure
*
* @param string $module The current module
* @param int $recordid The ID of the record to be unlocked
*/
function Unlock($module, $recordid)
{
    global $ModuleDefs;

    if (bYN(GetParm("RECORD_LOCKING","N")) && $recordid)
    {
        require_once 'Source/libs/RecordLocks.php';
        UnlockRecord(array("table" => $ModuleDefs[$module]['TABLE'], "link_id"=> $recordid));
    }
}

/**
* @desc Loops through FieldDefs and picks out all fields for a given module and type - used when we
 * need to do particular actions on particular types of fields (e.g. validation when saving).
*
* @param string $module The current module
* @param string $type The type of fields to check for
* @param boolean $useFieldDefsExtra Whether to use FieldDefsExtra, or just FieldDefs
*
* @return array Array of fields that match the given type
*/
function GetAllFieldsByType($module, $type, $useFieldDefsExtra = true)
{
    $FieldDefs = $useFieldDefsExtra ? $GLOBALS['FieldDefsExtra'] : $GLOBALS['FieldDefs'];

    $FieldArray = array();

    if (is_array($FieldDefs[$module]))
    {
        foreach ($FieldDefs[$module] as $FieldName => $Properties)
        {
            if ($Properties['Type'] == $type)
            {
                $FieldArray[] = $FieldName;
            }
        }
    }

    return $FieldArray;
}

/**
* Loops through the current form fields and picks out UDFs of a certain type.
*
* @param string $type    The field type to check for.
* @param string $suffix  Identifier for linked data.
* @param string $data    The form data.
*
* @return array $fields  Array of fields that match the given type.
*/
function getAllUdfFieldsByType($type, $suffix, $data)
{
    $fields = array();

    foreach ($data as $field => $value)
    {
        if (\UnicodeString::substr($field, 0, 4) == 'UDF_')
        {
            $udfParts = explode('_', $field);

            if ($udfParts[1] == $type && ((!$suffix && !$udfParts[4]) || ('_'.$udfParts[4] == $suffix)))
            {
                $fields[] = $field;
            }
        }
    }

    if ($suffix)
    {
        $fields = array_map('RemoveSuffix', $fields);
    }

    return $fields;
}

// Returns a FormField object containing a dropdown list of all
// available managers.  Finds all of those whose perms are in PermArray
function MakeManagerDropdownGeneric($Module, $Data, $FieldMode, $PermArray, $ContactSuffix = "")
{
    global $ModuleDefs;

    if ($ContactSuffix)
    {
        $ContactSuffix = "_" . $ContactSuffix;
    }

    if (bYN(GetParm('STAFF_EMPL_FILTERS', 'N', true)))
    {
        $MgrField = $ModuleDefs[$Module]['FIELD_NAMES']['HANDLER'];

        $Parents = GetParents(array('module' => $Module, 'field' => $MgrField));

        foreach ($Parents as $Parent)
        {
            if ($Parent)
            {
                $StaffFields[] = $ModuleDefs[$Module][STAFF_EMPL_FILTER_MAPPINGS][$Parent];
            }
        }

        $ParentValues[0] = $inc[$StaffFields[0]];
        $ParentValues[1] = $inc[$StaffFields[1]];

        $FieldObj = Forms_SelectFieldFactory::createSelectField(
            $MgrField,
            $Module,
            $Data[$MgrField],
            $FieldMode,
            false,
            _tk('your_manager'),
            'your_manager',
            $Data['CHANGED-'.$MgrField],
            $ContactSuffix
        );
        $FieldObj->setParents(array($StaffFields[0], $StaffFields[1]));
    }
    else
    {

        $sql = "SELECT recordid, initials, fullname, con_jobtitle, con_forenames,
            con_surname, con_title
            FROM contacts_main
            WHERE con_email IS NOT NULL
            AND contacts_main.login IS NOT NULL
            AND con_dclosed is null
            AND initials IS NOT NULL AND initials <> ''
            AND (con_staff_include IS NULL OR con_staff_include != 'N')
            ";

        $conPermWhere = MakeSecurityWhereClause("", "CON", $_SESSION["initials"]);

        if ($conPermWhere)
        {
            $sql .= ' AND '.$conPermWhere;
        }

        $sql .= GetContactListOrderBy();

        $ContactArray = DatixDBQuery::PDO_fetch_all($sql);

        $MgrArray = array();

        foreach ($ContactArray as $row)
        {
            require_once 'Source/security/SecurityBase.php';
            $AccessLevels = GetUserAccessLevels($row['recordid']);

            if (in_array($AccessLevels[$ModuleDefs[$Module]['PERM_GLOBAL']],$PermArray))
            {
                $MgrArray[$row["initials"]] = FormatContactNameForList(array('data' => $row));
            }
        }

        // Value is stored under inc_mgr in database, so need to add suffix back on.
        if ($Module == 'INC' && !$Data["inc_mgr" . $ContactSuffix])
        {
            $Data["inc_mgr" . $ContactSuffix] = $Data["inc_mgr"];
        }

        $ManagerField = $ModuleDefs[$Module]['FIELD_NAMES']['HANDLER'] . $ContactSuffix;

        $FieldObj = Forms_SelectFieldFactory::createSelectField(
            $ManagerField,
            $Module,
            $Data[$ManagerField],
            $FieldMode,
            false,
            _tk('your_manager'),
            'your_manager',
            $Data['CHANGED-'.$MgrField],
            $ContactSuffix
        );
        $FieldObj->setCustomCodes($MgrArray);
    }

    return $FieldObj;
}

function getModuleFromField($TargetField)
{
    global $FieldDefs;

    foreach ($FieldDefs as $module => $aFields)
    {
        if (isset($aFields[$TargetField]))
        {
            return $module;
        }
    }

    return '';
}

function RemoveTableFromFieldName($Field)
{
    if (\UnicodeString::strpos($Field,'.') === false)
    {
        $RealFieldName = \UnicodeString::strtolower($Field);
    }
    else
    {
        $RealFieldName = \UnicodeString::strtolower(\UnicodeString::substr($Field, \UnicodeString::strpos($Field,'.') + 1));
    }

    return $RealFieldName;
}

//not an ideal function to have to use, but it gets rid of a lot of weird characters
//produced by copying from word into fields.
//function copied from php.net comments.
// FIXME: Get rid of this function and inline it where necessary
function htmlfriendly($var)
{
    return htmlspecialchars(stripslashes($var));
}

function GetEntityCodes()
{
    // TODO: This method should not be necessary with UTF-8 content. 
    // Leaving it here for now, just in case    
    $Response = '';

    $Response .= '<!ENTITY nbsp "&#160;">'; // no-break space = non-breaking space, U+00A0 ISOnum
    $Response .= '<!ENTITY iexcl "&#161;">'; // inverted exclamation mark, U+00A1 ISOnum
    $Response .= '<!ENTITY cent "&#162;">'; // cent sign, U+00A2 ISOnum
    $Response .= '<!ENTITY pound "&#163;">'; // pound sign, U+00A3 ISOnum
    $Response .= '<!ENTITY curren "&#164;">'; // currency sign, U+00A4 ISOnum
    $Response .= '<!ENTITY yen "&#165;">'; // yen sign = yuan sign, U+00A5 ISOnum
    $Response .= '<!ENTITY brvbar "&#166;">'; // broken bar = broken vertical bar, U+00A6 ISOnum
    $Response .= '<!ENTITY sect "&#167;">'; // section sign, U+00A7 ISOnum
    $Response .= '<!ENTITY uml "&#168;">'; // diaeresis = spacing diaeresis, U+00A8 ISOdia
    $Response .= '<!ENTITY copy "&#169;">'; // copyright sign, U+00A9 ISOnum
    $Response .= '<!ENTITY ordf "&#170;">'; // feminine ordinal indicator, U+00AA ISOnum
    $Response .= '<!ENTITY laquo "&#171;">'; // left-pointing double angle quotation mark = left pointing guillemet, U+00AB ISOnum
    $Response .= '<!ENTITY not "&#172;">'; // not sign, U+00AC ISOnum
    $Response .= '<!ENTITY shy "&#173;">'; // soft hyphen = discretionary hyphen, U+00AD ISOnum
    $Response .= '<!ENTITY reg "&#174;">'; // registered sign = registered trade mark sign, U+00AE ISOnum
    $Response .= '<!ENTITY macr "&#175;">'; // macron = spacing macron = overline = APL overbar, U+00AF ISOdia
    $Response .= '<!ENTITY deg "&#176;">'; // degree sign, U+00B0 ISOnum
    $Response .= '<!ENTITY plusmn "&#177;">'; // plus-minus sign = plus-or-minus sign, U+00B1 ISOnum
    $Response .= '<!ENTITY sup2 "&#178;">'; // superscript two = superscript digit two = squared, U+00B2 ISOnum
    $Response .= '<!ENTITY sup3 "&#179;">'; // superscript three = superscript digit three = cubed, U+00B3 ISOnum
    $Response .= '<!ENTITY acute "&#180;">'; // acute accent = spacing acute, U+00B4 ISOdia
    $Response .= '<!ENTITY micro "&#181;">'; // micro sign, U+00B5 ISOnum
    $Response .= '<!ENTITY para "&#182;">'; // pilcrow sign = paragraph sign, U+00B6 ISOnum
    $Response .= '<!ENTITY middot "&#183;">'; // middle dot = Georgian comma = Greek middle dot, U+00B7 ISOnum
    $Response .= '<!ENTITY cedil "&#184;">'; // cedilla = spacing cedilla, U+00B8 ISOdia
    $Response .= '<!ENTITY sup1 "&#185;">'; // superscript one = superscript digit one, U+00B9 ISOnum
    $Response .= '<!ENTITY ordm "&#186;">'; // masculine ordinal indicator, U+00BA ISOnum
    $Response .= '<!ENTITY raquo "&#187;">'; // right-pointing double angle quotation mark = right pointing guillemet, U+00BB ISOnum
    $Response .= '<!ENTITY frac14 "&#188;">'; // vulgar fraction one quarter = fraction one quarter, U+00BC ISOnum
    $Response .= '<!ENTITY frac12 "&#189;">'; // vulgar fraction one half = fraction one half, U+00BD ISOnum
    $Response .= '<!ENTITY frac34 "&#190;">'; // vulgar fraction three quarters = fraction three quarters, U+00BE ISOnum
    $Response .= '<!ENTITY iquest "&#191;">'; // inverted question mark = turned question mark, U+00BF ISOnum
    $Response .= '<!ENTITY Agrave "&#192;">'; // latin capital letter A with grave = latin capital letter A grave, U+00C0 ISOlat1
    $Response .= '<!ENTITY Aacute "&#193;">'; // latin capital letter A with acute, U+00C1 ISOlat1
    $Response .= '<!ENTITY Acirc "&#194;">'; // latin capital letter A with circumflex, U+00C2 ISOlat1
    $Response .= '<!ENTITY Atilde "&#195;">'; // latin capital letter A with tilde, U+00C3 ISOlat1
    $Response .= '<!ENTITY Auml "&#196;">'; // latin capital letter A with diaeresis, U+00C4 ISOlat1
    $Response .= '<!ENTITY Aring "&#197;">'; // latin capital letter A with ring above = latin capital letter A ring, U+00C5 ISOlat1
    $Response .= '<!ENTITY AElig "&#198;">'; // latin capital letter AE = latin capital ligature AE, U+00C6 ISOlat1
    $Response .= '<!ENTITY Ccedil "&#199;">'; // latin capital letter C with cedilla, U+00C7 ISOlat1
    $Response .= '<!ENTITY Egrave "&#200;">'; // latin capital letter E with grave, U+00C8 ISOlat1
    $Response .= '<!ENTITY Eacute "&#201;">'; // latin capital letter E with acute, U+00C9 ISOlat1
    $Response .= '<!ENTITY Ecirc "&#202;">'; // latin capital letter E with circumflex, U+00CA ISOlat1
    $Response .= '<!ENTITY Euml "&#203;">'; // latin capital letter E with diaeresis, U+00CB ISOlat1
    $Response .= '<!ENTITY Igrave "&#204;">'; // latin capital letter I with grave, U+00CC ISOlat1
    $Response .= '<!ENTITY Iacute "&#205;">'; // latin capital letter I with acute, U+00CD ISOlat1
    $Response .= '<!ENTITY Icirc "&#206;">'; // latin capital letter I with circumflex, U+00CE ISOlat1
    $Response .= '<!ENTITY Iuml "&#207;">'; // latin capital letter I with diaeresis, U+00CF ISOlat1
    $Response .= '<!ENTITY ETH "&#208;">'; // latin capital letter ETH, U+00D0 ISOlat1
    $Response .= '<!ENTITY Ntilde "&#209;">'; // latin capital letter N with tilde, U+00D1 ISOlat1
    $Response .= '<!ENTITY Ograve "&#210;">'; // latin capital letter O with grave, U+00D2 ISOlat1
    $Response .= '<!ENTITY Oacute "&#211;">'; // latin capital letter O with acute, U+00D3 ISOlat1
    $Response .= '<!ENTITY Ocirc "&#212;">'; // latin capital letter O with circumflex, U+00D4 ISOlat1
    $Response .= '<!ENTITY Otilde "&#213;">'; // latin capital letter O with tilde, U+00D5 ISOlat1
    $Response .= '<!ENTITY Ouml "&#214;">'; // latin capital letter O with diaeresis, U+00D6 ISOlat1
    $Response .= '<!ENTITY times "&#215;">'; // multiplication sign, U+00D7 ISOnum
    $Response .= '<!ENTITY Oslash "&#216;">'; // latin capital letter O with stroke = latin capital letter O slash, U+00D8 ISOlat1
    $Response .= '<!ENTITY Ugrave "&#217;">'; // latin capital letter U with grave, U+00D9 ISOlat1
    $Response .= '<!ENTITY Uacute "&#218;">'; // latin capital letter U with acute, U+00DA ISOlat1
    $Response .= '<!ENTITY Ucirc "&#219;">'; // latin capital letter U with circumflex, U+00DB ISOlat1
    $Response .= '<!ENTITY Uuml "&#220;">'; // latin capital letter U with diaeresis, U+00DC ISOlat1
    $Response .= '<!ENTITY Yacute "&#221;">'; // latin capital letter Y with acute, U+00DD ISOlat1
    $Response .= '<!ENTITY THORN "&#222;">'; // latin capital letter THORN, U+00DE ISOlat1
    $Response .= '<!ENTITY szlig "&#223;">'; // latin small letter sharp s = ess-zed, U+00DF ISOlat1
    $Response .= '<!ENTITY agrave "&#224;">'; // latin small letter a with grave = latin small letter a grave, U+00E0 ISOlat1
    $Response .= '<!ENTITY aacute "&#225;">'; // latin small letter a with acute, U+00E1 ISOlat1
    $Response .= '<!ENTITY acirc "&#226;">'; // latin small letter a with circumflex, U+00E2 ISOlat1
    $Response .= '<!ENTITY atilde "&#227;">'; // latin small letter a with tilde, U+00E3 ISOlat1
    $Response .= '<!ENTITY auml "&#228;">'; // latin small letter a with diaeresis, U+00E4 ISOlat1
    $Response .= '<!ENTITY aring "&#229;">'; // latin small letter a with ring above = latin small letter a ring,U+00E5 ISOlat1
    $Response .= '<!ENTITY aelig "&#230;">'; // latin small letter ae = latin small ligature ae, U+00E6 ISOlat1
    $Response .= '<!ENTITY ccedil "&#231;">'; // latin small letter c with cedilla, U+00E7 ISOlat1
    $Response .= '<!ENTITY egrave "&#232;">'; // latin small letter e with grave, U+00E8 ISOlat1
    $Response .= '<!ENTITY eacute "&#233;">'; // latin small letter e with acute, U+00E9 ISOlat1
    $Response .= '<!ENTITY ecirc "&#234;">'; // latin small letter e with circumflex, U+00EA ISOlat1
    $Response .= '<!ENTITY euml "&#235;">'; // latin small letter e with diaeresis, U+00EB ISOlat1
    $Response .= '<!ENTITY igrave "&#236;">'; // latin small letter i with grave, U+00EC ISOlat1
    $Response .= '<!ENTITY iacute "&#237;">'; // latin small letter i with acute, U+00ED ISOlat1
    $Response .= '<!ENTITY icirc "&#238;">'; // latin small letter i with circumflex, U+00EE ISOlat1
    $Response .= '<!ENTITY iuml "&#239;">'; // latin small letter i with diaeresis, U+00EF ISOlat1
    $Response .= '<!ENTITY eth "&#240;">'; // latin small letter eth, U+00F0 ISOlat1
    $Response .= '<!ENTITY ntilde "&#241;">'; // latin small letter n with tilde, U+00F1 ISOlat1
    $Response .= '<!ENTITY ograve "&#242;">'; // latin small letter o with grave, U+00F2 ISOlat1
    $Response .= '<!ENTITY oacute "&#243;">'; // latin small letter o with acute, U+00F3 ISOlat1
    $Response .= '<!ENTITY ocirc "&#244;">'; // latin small letter o with circumflex, U+00F4 ISOlat1
    $Response .= '<!ENTITY otilde "&#245;">'; // latin small letter o with tilde, U+00F5 ISOlat1
    $Response .= '<!ENTITY ouml "&#246;">'; // latin small letter o with diaeresis, U+00F6 ISOlat1
    $Response .= '<!ENTITY divide "&#247;">'; // division sign, U+00F7 ISOnum
    $Response .= '<!ENTITY oslash "&#248;">'; // latin small letter o with stroke, = latin small letter o slash, U+00F8 ISOlat1
    $Response .= '<!ENTITY ugrave "&#249;">'; // latin small letter u with grave, U+00F9 ISOlat1
    $Response .= '<!ENTITY uacute "&#250;">'; // latin small letter u with acute, U+00FA ISOlat1
    $Response .= '<!ENTITY ucirc "&#251;">'; // latin small letter u with circumflex, U+00FB ISOlat1
    $Response .= '<!ENTITY uuml "&#252;">'; // latin small letter u with diaeresis, U+00FC ISOlat1
    $Response .= '<!ENTITY yacute "&#253;">'; // latin small letter y with acute, U+00FD ISOlat1
    $Response .= '<!ENTITY thorn "&#254;">'; // latin small letter thorn, U+00FE ISOlat1
    $Response .= '<!ENTITY yuml "&#255;">'; // latin small letter y with diaeresis, U+00FF ISOlat1
    $Response .= '<!ENTITY Alpha "&#913;">'; // greek capital letter alpha, U+0391
    $Response .= '<!ENTITY Beta "&#914;">'; // greek capital letter beta, U+0392
    $Response .= '<!ENTITY Gamma "&#915;">'; // greek capital letter gamma, U+0393 ISOgrk3
    $Response .= '<!ENTITY Delta "&#916;">'; // greek capital letter delta, U+0394 ISOgrk3
    $Response .= '<!ENTITY Epsilon "&#917;">'; // greek capital letter epsilon, U+0395
    $Response .= '<!ENTITY Zeta "&#918;">'; // greek capital letter zeta, U+0396
    $Response .= '<!ENTITY Eta  "&#919;">'; // greek capital letter eta, U+0397
    $Response .= '<!ENTITY Theta "&#920;">'; // greek capital letter theta, U+0398 ISOgrk3
    $Response .= '<!ENTITY Iota "&#921;">'; // greek capital letter iota, U+0399
    $Response .= '<!ENTITY Kappa "&#922;">'; // greek capital letter kappa, U+039A
    $Response .= '<!ENTITY Lambda "&#923;">'; // greek capital letter lambda, U+039B ISOgrk3
    $Response .= '<!ENTITY Mu  "&#924;">'; // greek capital letter mu, U+039C
    $Response .= '<!ENTITY Nu  "&#925;">'; // greek capital letter nu, U+039D
    $Response .= '<!ENTITY Xi  "&#926;">'; // greek capital letter xi, U+039E ISOgrk3
    $Response .= '<!ENTITY Omicron "&#927;">'; // greek capital letter omicron, U+039F
    $Response .= '<!ENTITY Pi  "&#928;">'; // greek capital letter pi, U+03A0 ISOgrk3
    $Response .= '<!ENTITY Rho  "&#929;">'; // greek capital letter rho, U+03A1
    $Response .= '<!ENTITY Sigma "&#931;">'; // greek capital letter sigma, U+03A3 ISOgrk3
    $Response .= '<!ENTITY Tau  "&#932;">'; // greek capital letter tau, U+03A4
    $Response .= '<!ENTITY Upsilon "&#933;">'; // greek capital letter upsilon, U+03A5 ISOgrk3
    $Response .= '<!ENTITY Phi  "&#934;">'; // greek capital letter phi, U+03A6 ISOgrk3
    $Response .= '<!ENTITY Chi  "&#935;">'; // greek capital letter chi, U+03A7
    $Response .= '<!ENTITY Psi  "&#936;">'; // greek capital letter psi, U+03A8 ISOgrk3
    $Response .= '<!ENTITY Omega "&#937;">'; // greek capital letter omega, U+03A9 ISOgrk3
    $Response .= '<!ENTITY alpha "&#945;">'; // greek small letter alpha, U+03B1 ISOgrk3
    $Response .= '<!ENTITY beta "&#946;">'; // greek small letter beta, U+03B2 ISOgrk3
    $Response .= '<!ENTITY gamma "&#947;">'; // greek small letter gamma, U+03B3 ISOgrk3
    $Response .= '<!ENTITY delta "&#948;">'; // greek small letter delta, U+03B4 ISOgrk3
    $Response .= '<!ENTITY epsilon "&#949;">'; // greek small letter epsilon, U+03B5 ISOgrk3
    $Response .= '<!ENTITY zeta "&#950;">'; // greek small letter zeta, U+03B6 ISOgrk3
    $Response .= '<!ENTITY eta  "&#951;">'; // greek small letter eta, U+03B7 ISOgrk3
    $Response .= '<!ENTITY theta "&#952;">'; // greek small letter theta, U+03B8 ISOgrk3
    $Response .= '<!ENTITY iota "&#953;">'; // greek small letter iota, U+03B9 ISOgrk3
    $Response .= '<!ENTITY kappa "&#954;">'; // greek small letter kappa, U+03BA ISOgrk3
    $Response .= '<!ENTITY lambda "&#955;">'; // greek small letter lambda, U+03BB ISOgrk3
    $Response .= '<!ENTITY mu  "&#956;">'; // greek small letter mu, U+03BC ISOgrk3
    $Response .= '<!ENTITY nu  "&#957;">'; // greek small letter nu, U+03BD ISOgrk3
    $Response .= '<!ENTITY xi  "&#958;">'; // greek small letter xi, U+03BE ISOgrk3
    $Response .= '<!ENTITY omicron "&#959;">'; // greek small letter omicron, U+03BF NEW
    $Response .= '<!ENTITY pi  "&#960;">'; // greek small letter pi, U+03C0 ISOgrk3
    $Response .= '<!ENTITY rho  "&#961;">'; // greek small letter rho, U+03C1 ISOgrk3
    $Response .= '<!ENTITY sigmaf "&#962;">'; // greek small letter final sigma, U+03C2 ISOgrk3
    $Response .= '<!ENTITY sigma "&#963;">'; // greek small letter sigma, U+03C3 ISOgrk3
    $Response .= '<!ENTITY tau  "&#964;">'; // greek small letter tau, U+03C4 ISOgrk3
    $Response .= '<!ENTITY upsilon "&#965;">'; // greek small letter upsilon, U+03C5 ISOgrk3
    $Response .= '<!ENTITY phi  "&#966;">'; // greek small letter phi, U+03C6 ISOgrk3
    $Response .= '<!ENTITY chi  "&#967;">'; // greek small letter chi, U+03C7 ISOgrk3
    $Response .= '<!ENTITY psi  "&#968;">'; // greek small letter psi, U+03C8 ISOgrk3
    $Response .= '<!ENTITY omega "&#969;">'; // greek small letter omega, U+03C9 ISOgrk3
    $Response .= '<!ENTITY thetasym "&#977;">'; // greek small letter theta symbol, U+03D1 NEW
    $Response .= '<!ENTITY upsih "&#978;">'; // greek upsilon with hook symbol, U+03D2 NEW
    $Response .= '<!ENTITY piv  "&#982;">'; // greek pi symbol, U+03D6 ISOgrk3
    $Response .= '<!ENTITY bull "&#8226;">'; // bullet = black small circle, U+2022 ISOpub
    $Response .= '<!ENTITY hellip "&#8230;">'; // horizontal ellipsis = three dot leader, U+2026 ISOpub
    $Response .= '<!ENTITY prime "&#8242;">'; // prime = minutes = feet, U+2032 ISOtech
    $Response .= '<!ENTITY Prime "&#8243;">'; // double prime = seconds = inches,U+2033 ISOtech
    $Response .= '<!ENTITY oline "&#8254;">'; // overline = spacing overscore,U+203E NEW
    $Response .= '<!ENTITY frasl "&#8260;">'; // fraction slash, U+2044 NEW
    $Response .= '<!ENTITY weierp "&#8472;">'; // script capital P = power set = Weierstrass p, U+2118 ISOamso
    $Response .= '<!ENTITY image "&#8465;">'; // blackletter capital I = imaginary part, U+2111 ISOamso
    $Response .= '<!ENTITY real "&#8476;">'; // blackletter capital R = real part symbol, U+211C ISOamso
    $Response .= '<!ENTITY trade "&#8482;">'; // trade mark sign, U+2122 ISOnum
    $Response .= '<!ENTITY alefsym "&#8501;">'; // alef symbol = first transfinite cardinal, U+2135 NEW
    $Response .= '<!ENTITY larr "&#8592;">'; // leftwards arrow, U+2190 ISOnum
    $Response .= '<!ENTITY uarr "&#8593;">'; // upwards arrow, U+2191 ISOnum-->
    $Response .= '<!ENTITY rarr "&#8594;">'; // rightwards arrow, U+2192 ISOnum
    $Response .= '<!ENTITY darr "&#8595;">'; // downwards arrow, U+2193 ISOnum
    $Response .= '<!ENTITY harr "&#8596;">'; // left right arrow, U+2194 ISOamsa
    $Response .= '<!ENTITY crarr "&#8629;">'; // downwards arrow with corner leftwards = carriage return, U+21B5 NEW
    $Response .= '<!ENTITY lArr "&#8656;">'; // leftwards double arrow, U+21D0 ISOtech
    $Response .= '<!ENTITY uArr "&#8657;">'; // upwards double arrow, U+21D1 ISOamsa
    $Response .= '<!ENTITY rArr "&#8658;">'; // rightwards double arrow, U+21D2 ISOtech
    $Response .= '<!ENTITY dArr "&#8659;">'; // downwards double arrow, U+21D3 ISOamsa
    $Response .= '<!ENTITY hArr "&#8660;">'; // left right double arrow, U+21D4 ISOamsa
    $Response .= '<!ENTITY forall "&#8704;">'; // for all, U+2200 ISOtech
    $Response .= '<!ENTITY part "&#8706;">'; // partial differential, U+2202 ISOtech
    $Response .= '<!ENTITY exist "&#8707;">'; // there exists, U+2203 ISOtech
    $Response .= '<!ENTITY empty "&#8709;">'; // empty set = null set = diameter, U+2205 ISOamso
    $Response .= '<!ENTITY nabla "&#8711;">'; // nabla = backward difference, U+2207 ISOtech
    $Response .= '<!ENTITY isin "&#8712;">'; // element of, U+2208 ISOtech
    $Response .= '<!ENTITY notin "&#8713;">'; // not an element of, U+2209 ISOtech
    $Response .= '<!ENTITY ni  "&#8715;">'; // contains as member, U+220B ISOtech
    $Response .= '<!ENTITY prod "&#8719;">'; // n-ary product = product sign, U+220F ISOamsb
    $Response .= '<!ENTITY sum  "&#8721;">'; // n-ary sumation, U+2211 ISOamsb
    $Response .= '<!ENTITY minus "&#8722;">'; // minus sign, U+2212 ISOtech
    $Response .= '<!ENTITY lowast "&#8727;">'; // asterisk operator, U+2217 ISOtech
    $Response .= '<!ENTITY radic "&#8730;">'; // square root = radical sign, U+221A ISOtech
    $Response .= '<!ENTITY prop "&#8733;">'; // proportional to, U+221D ISOtech
    $Response .= '<!ENTITY infin "&#8734;">'; // infinity, U+221E ISOtech
    $Response .= '<!ENTITY ang  "&#8736;">'; // angle, U+2220 ISOamso
    $Response .= '<!ENTITY and  "&#8743;">'; // logical and = wedge, U+2227 ISOtech
    $Response .= '<!ENTITY or  "&#8744;">'; // logical or = vee, U+2228 ISOtech
    $Response .= '<!ENTITY cap  "&#8745;">'; // intersection = cap, U+2229 ISOtech
    $Response .= '<!ENTITY cup  "&#8746;">'; // union = cup, U+222A ISOtech
    $Response .= '<!ENTITY int  "&#8747;">'; // integral, U+222B ISOtech
    $Response .= '<!ENTITY there4 "&#8756;">'; // therefore, U+2234 ISOtech
    $Response .= '<!ENTITY sim  "&#8764;">'; // tilde operator = varies with = similar to, U+223C ISOtech
    $Response .= '<!ENTITY cong "&#8773;">'; // approximately equal to, U+2245 ISOtech
    $Response .= '<!ENTITY asymp "&#8776;">'; // almost equal to = asymptotic to, U+2248 ISOamsr
    $Response .= '<!ENTITY ne  "&#8800;">'; // not equal to, U+2260 ISOtech
    $Response .= '<!ENTITY equiv "&#8801;">'; // identical to, U+2261 ISOtech
    $Response .= '<!ENTITY le  "&#8804;">'; // less-than or equal to, U+2264 ISOtech
    $Response .= '<!ENTITY ge  "&#8805;">'; // greater-than or equal to, U+2265 ISOtech
    $Response .= '<!ENTITY sub  "&#8834;">'; // subset of, U+2282 ISOtech
    $Response .= '<!ENTITY sup  "&#8835;">'; // superset of, U+2283 ISOtech
    $Response .= '<!ENTITY nsub "&#8836;">'; // not a subset of, U+2284 ISOamsn
    $Response .= '<!ENTITY sube "&#8838;">'; // subset of or equal to, U+2286 ISOtech
    $Response .= '<!ENTITY supe "&#8839;">'; // superset of or equal to, U+2287 ISOtech
    $Response .= '<!ENTITY oplus "&#8853;">'; // circled plus = direct sum, U+2295 ISOamsb
    $Response .= '<!ENTITY otimes "&#8855;">'; // circled times = vector product, U+2297 ISOamsb
    $Response .= '<!ENTITY perp "&#8869;">'; // up tack = orthogonal to = perpendicular, U+22A5 ISOtech
    $Response .= '<!ENTITY sdot "&#8901;">'; // dot operator, U+22C5 ISOamsb
    $Response .= '<!ENTITY lceil "&#8968;">'; // left ceiling = apl upstile, U+2308 ISOamsc
    $Response .= '<!ENTITY rceil "&#8969;">'; // right ceiling, U+2309 ISOamsc
    $Response .= '<!ENTITY lfloor "&#8970;">'; // left floor = apl downstile, U+230A ISOamsc
    $Response .= '<!ENTITY rfloor "&#8971;">'; // right floor, U+230B ISOamsc
    $Response .= '<!ENTITY lang "&#9001;">'; // left-pointing angle bracket = bra, U+2329 ISOtech
    $Response .= '<!ENTITY rang "&#9002;">'; // right-pointing angle bracket = ket, U+232A ISOtech
    $Response .= '<!ENTITY loz  "&#9674;">'; // lozenge, U+25CA ISOpub
    $Response .= '<!ENTITY spades "&#9824;">'; // black spade suit, U+2660 ISOpub
    $Response .= '<!ENTITY clubs "&#9827;">'; // black club suit = shamrock, U+2663 ISOpub
    $Response .= '<!ENTITY hearts "&#9829;">'; // black heart suit = valentine, U+2665 ISOpub
    $Response .= '<!ENTITY diams "&#9830;">'; // black diamond suit, U+2666 ISOpub
    $Response .= '<!ENTITY OElig "&#338;">'; // latin capital ligature OE, U+0152 ISOlat2
    $Response .= '<!ENTITY oelig "&#339;">'; // latin small ligature oe, U+0153 ISOlat2
    $Response .= '<!ENTITY Scaron "&#352;">'; // latin capital letter S with caron, U+0160 ISOlat2
    $Response .= '<!ENTITY scaron "&#353;">'; // latin small letter s with caron, U+0161 ISOlat2
    $Response .= '<!ENTITY Yuml "&#376;">'; // latin capital letter Y with diaeresis, U+0178 ISOlat2
    $Response .= '<!ENTITY circ "&#710;">'; // modifier letter circumflex accent, U+02C6 ISOpub
    $Response .= '<!ENTITY tilde "&#732;">'; // small tilde, U+02DC ISOdia
    $Response .= '<!ENTITY ensp "&#8194;">'; // en space, U+2002 ISOpub
    $Response .= '<!ENTITY emsp "&#8195;">'; // em space, U+2003 ISOpub
    $Response .= '<!ENTITY thinsp "&#8201;">'; // thin space, U+2009 ISOpub
    $Response .= '<!ENTITY zwnj "&#8204;">'; // zero width non-joiner, U+200C NEW RFC 2070
    $Response .= '<!ENTITY zwj "&#8205;">'; // zero width joiner, U+200D NEW RFC 2070
    $Response .= '<!ENTITY lrm "&#8206;">'; // left-to-right mark, U+200E NEW RFC 2070
    $Response .= '<!ENTITY rlm "&#8207;">'; // right-to-left mark, U+200F NEW RFC 2070
    $Response .= '<!ENTITY ndash "&#8211;">'; // en dash, U+2013 ISOpub
    $Response .= '<!ENTITY mdash "&#8212;">'; // em dash, U+2014 ISOpub
    $Response .= '<!ENTITY lsquo "&#8216;">'; // left single quotation mark, U+2018 ISOnum
    $Response .= '<!ENTITY rsquo "&#8217;">'; // right single quotation mark, U+2019 ISOnum
    $Response .= '<!ENTITY sbquo "&#8218;">'; // single low-9 quotation mark, U+201A NEW
    $Response .= '<!ENTITY ldquo "&#8220;">'; // left double quotation mark, U+201C ISOnum
    $Response .= '<!ENTITY rdquo "&#8221;">'; // right double quotation mark, U+201D ISOnum
    $Response .= '<!ENTITY bdquo "&#8222;">'; // double low-9 quotation mark, U+201E NEW
    $Response .= '<!ENTITY dagger "&#8224;">'; // dagger, U+2020 ISOpub
    $Response .= '<!ENTITY Dagger "&#8225;">'; // double dagger, U+2021 ISOpub
    $Response .= '<!ENTITY permil "&#8240;">'; // per mille sign, U+2030 ISOtech
    $Response .= '<!ENTITY lsaquo "&#8249;">'; // single left-pointing angle quotation mark, U+2039 ISO proposed
    $Response .= '<!ENTITY rsaquo "&#8250;">'; // single right-pointing angle quotation mark, U+203A ISO proposed
    $Response .= '<!ENTITY euro "&#8364;">'; //euro sign, U+20AC NEW

    return $Response;
}

function AddUDFValueToDataArray($aParams, &$Data)
{
    global $ModuleDefs;

    if ($aParams['fieldname'])
    {
        $Parts = explode("_", $aParams['fieldname']);

        $UDFArray = $aParams['formobj']->GetUDFValue(
            $ModuleDefs[$aParams['module']]['MOD_ID'],
            $Data["recordid"],
            $Parts[2],
            $Parts[3]
        );

        $Data[$aParams['fieldname']] = $UDFArray["udv_string"];
    }
}

function GetCurrentUserFormLevel($Module)
{
    global $ModuleDefs;

    return GetUserFormLevel($Module, GetParm($ModuleDefs[$Module]['PERM_GLOBAL']));
}

function GetUserFormLevel($Module, $Perms)
{
    global $ModuleDefs;

    if ($Perms == '' || (is_array($ModuleDefs[$Module]['LEVEL1_PERMS']) &&
        in_array($Perms, $ModuleDefs[$Module]['LEVEL1_PERMS'])))
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

/**
*  Gets the type (calendar or working) and number of days after which an record is considered overdue.
*
*  Defaults to 14 days for overall settings.
*  Can also (optionally) return information based upon a specific approval status.
*  Caches database-based values to prevent repeated queries.
*  @param string $Module The code for this module.
*  @param string $approvalStatus The approval status code.
*  @return array $OverdueDays Contains overall and optionally approval status-specific overdue settings
 * (individual array elements described below):
*  <ul>
*  <li> int $OverdueDays['overall'] The overall number of days before a record is considered overdue.</li>
*  <li> string $OverdueDays['overall_type'] The method by which overall overdue status is calculated
 * (calendar or working days).</li>
*  <li> int $OverdueDays[$approvalStatus] The number of days specific to this approval stage before a record is
 * considered overdue.</li>
*  <li> string $OverdueDays[$approvalStatus.'_type'] The method by which this approval stage's overdue status is
 * calculated (calendar or working days).</li>
*  <li> string $OverdueDays['overdue_date_field'] A custom date field that can be specified for use when calculating
 * overdue days for this approval status
*  (instead of using dates from the audit table).</li>
*  </ul>
*/
function GetOverdueDays($Module = 'INC', $approvalStatus = '')
{
    global $ModuleDefs;

    $OverdueDays = array();
    $OverdueDays['overall'] = GetParm($ModuleDefs[$Module]['OVERDUE_DAY_GLOBAL'], 14);
    $OverdueDays['overall_type'] = GetParm($ModuleDefs[$Module]['OVERDUE_TYPE_GLOBAL'], 'C');

    if (!empty($approvalStatus))
    {
        //values cached in $_SESSION['CachedValues']['OverdueDays'][$Module][$approvalStatus][GetWorkflowID()]
        if (is_array($_SESSION['CachedValues']['OverdueDays'][$Module][$approvalStatus]) &&
            array_key_exists(GetWorkflowID($Module), $_SESSION['CachedValues']['OverdueDays'][$Module][$approvalStatus]))
        {
            $row = $_SESSION['CachedValues']['OverdueDays'][$Module][$approvalStatus][GetWorkflowID($Module)];
        }
        else
        {
            $result = db_query("SELECT t.tim_overdue_days, t.tim_type, cas.overdue_date_field
                                FROM timescales t
                                INNER JOIN code_approval_status cas
                                    ON cas.module = t.tim_module
                                    AND cas.code = t.tim_approval_status
                                WHERE tim_module = '$Module'
                                AND tim_approval_status = '$approvalStatus'
                                AND cas.workflow = ".GetWorkflowID($Module));

            $row = db_fetch_array($result);
            $_SESSION['CachedValues']['OverdueDays'][$Module][$approvalStatus][GetWorkflowID($Module)] = $row;
        }

        if ($row)
        {
            $OverdueDays[$approvalStatus] = $row['tim_overdue_days'];
            $OverdueDays[$approvalStatus.'_type'] = $row['tim_type'];
            $OverdueDays['overdue_date_field'] = $row['overdue_date_field'];
        }
        else
        {
            $OverdueDays[$approvalStatus] = -1;
        }
    }

    return $OverdueDays;
}

/**
* @desc Used when we need to find out the number of records that match a particular where clause.
*
* @param string $Module The current module
* @param string $where The where clause defining the set of records to check
* @param bool $Overdue If true, we need to add an additional filter to only find overdue records.
* @param string $approvalStatus Used to define the overdue dates if we are looking for overdue records in a
 * particular approval status.
*
* @returns int the number of records that match the criteria (0 if there are no matching records)
*/
function CountRecordsGeneric($Module, $where = "", $Overdue = false, $approvalStatus = '')
{
    global $ModuleDefs;

    if ($where)
    {
        if (!is_array($where))
        {
            $FullWhere[] = $where;
        }
        else
        {
            $FullWhere = $where;
        }
    }

    $sql = 'SELECT COUNT(*)
            FROM ' . $ModuleDefs[$Module]['TABLE'];

    if ($Overdue)
    {
        $overdueSQL = getOverdueSQL($Module, $approvalStatus);
        $sql .= $overdueSQL['join'];

        if ($overdueSQL['where'])
        {
            $FullWhere[] = $overdueSQL['where'];
        }
    }

    $WhereClause = MakeSecurityWhereClause($FullWhere, $Module, $_SESSION["initials"]);

    if ($WhereClause != "")
    {
        $sql .= " WHERE " . $WhereClause;
    }

    return (int) DatixDBQuery::PDO_fetch($sql, [], PDO::FETCH_COLUMN);
}

/**
* @desc Used when we don't need to know how many records exist that match a where clause (for which we would
 * use {@link CountRecordsGeneric()}), but when we just want to know that a record exists. This is thus a much
 * faster function when dealing with large numbers of records. Otherwise, this function mimics
 * {@link CountRecordsGeneric()} in its logic.
*
* @param string $Module The current module
* @param string $where The where clause defining the set of records to check
* @param bool $Overdue If true, we need to add an additional filter to only find overdue records.
* @param string $approvalStatus Used to define the overdue dates if we are looking for overdue records in a
 * particular approval status.
*
* @returns int 1 if records exist that match the criteria and 0 if they do not.
*/
function CheckRecordsExistGeneric($Module, $where = "", $Overdue = false, $approvalStatus = '')
{
    global $ModuleDefs;

    if ($where)
    {
        if (!is_array($where))
        {
            $FullWhere[] = $where;
        }
        else
        {
            $FullWhere = $where;
        }
    }

    $sql = 'SELECT '.$ModuleDefs[$Module]['TABLE'].'.recordid FROM ' . $ModuleDefs[$Module]['TABLE'];

    if ($Overdue)
    {
        $overdueSQL = getOverdueSQL($Module, $approvalStatus);
        $sql .= $overdueSQL['join'];
        $FullWhere[] = $overdueSQL['where'];
    }

    $WhereClause = MakeSecurityWhereClause($FullWhere, $Module, $_SESSION["initials"]);

    if ($WhereClause != "")
    {
        $sql .= " WHERE " . $WhereClause;
    }

    $sql = 'SELECT 1 as records_exist WHERE (EXISTS('.$sql.'))';

    $result = db_query($sql);

    if ($row = db_fetch_array($result))
    {
        return ($row["records_exist"] == '1');
    }

    return false;
}


/**
* @desc Called when a record cannot be found in the database - checks whether it exists or not and
* then treats it as a security issue or a non-existant record as appropriate.
*
* @param array $Parameters Array of parameters
* @param string $Parameters['module'] The module the record belongs to.
* @param int $Parameters['recordid'] The id of the record in question
*/
function CheckRecordNotFound($Parameters)
{
    global $ModuleDefs;

    //need to check whether record exists at all.
    $sql = '
        SELECT
            1 as records_exist
        WHERE (EXISTS(
            SELECT
                recordid
            FROM
                ' . ($ModuleDefs[$Parameters['module']]['VIEW'] ? $ModuleDefs[$Parameters['module']]['VIEW'] : $ModuleDefs[$Parameters['module']]['TABLE']). '
            WHERE
                recordid = '.$Parameters['recordid'].'
        ))
    ';

    $row = db_fetch_array(db_query($sql));

    $RecordExists = ($row['records_exist'] == '1');

    if ($RecordExists)
    {
        $Message = _tk('no_perms_view_'.$Parameters['module']);

        if (empty($Message))
        {
            $Message = _tk('no_perms_view');
        }

        AuditOpenRecord($Parameters['module'], $Parameters['recordid'], "Access denied by security WHERE clause");
    }
    else
    {
        //record has been deleted or never existed.
        $Message = _tk('not_found');
    }

    if ($dtxdebug)
    {
        $Message .= "\n$sql";
    }

    fatal_error($Message, "Information", $Parameters['module']);
}

function getContactTypeArray($Module)
{
    global $ModuleDefs;

    if (is_array($ModuleDefs[$Module]['LEVEL1_CON_OPTIONS']))
    {
        foreach ($ModuleDefs[$Module]['LEVEL1_CON_OPTIONS'] as $Type => $Details)
        {
            $returnArray[$Details['DivName']] = $Type;
        }
    }

    return $returnArray;
}

function CheckUseReporterFields()
{
    return (!bYN(GetParm("DIF_1_CONTACT_REP")));
}

function GetDaysArray()
{
    // returns array of days with keys corresponding to SQL server weekday keys
    // and ordered according to week_start_day global.

    $sql = 'Select @@DATEFIRST as StartDate';
    $row = db_fetch_array(db_query($sql));
    $StartDate = intval($row['StartDate']);

    $dayref = array(
        1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday',
        5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'
    );

    $days = array();

    $i = intval(GetParm("WEEK_START_DAY", "2",true)) - 2;
    $j = 1 - $StartDate + (intval(GetParm("WEEK_START_DAY", "2",true)) - 2);

    while (count($days)<7)
    {
        $days[mod_pos($j,7)+1] = array('value'=>(mod_pos($j,7))+1, 'title' => $dayref[(mod_pos($i,7))+1]);
        $i++;
        $j++;
    }

    return $days;
}

function mod_pos($k, $m)
{
    return ($k - abs($m) * floor($k/abs($m)));
}

function GetContactListSQLByAccessLevel($aParams)
{
    global $ModuleDefs;

    $aWhere = $aParams['where'];
    $aLevels = $aParams['levels'];
    $aNotLevels = $aParams['notlevels'];
    $table = $aParams['table'] ? $aParams['table'] : 'con';
    $Module = $aParams['module'];

    $AccessLevelWhere = '';

    if ($aLevels)
    {
        $AccessLevelWhere .= " AND sec_group_permissions.perm_value in ('".implode("', '",$aLevels)."') ";
    }

    if ($aNotLevels)
    {
        $AccessLevelWhere .= " AND
            sec_group_permissions.perm_value not in ('".implode("', '",$aNotLevels)."')
            AND sec_group_permissions.perm_value != ''";
    }

    $UserParmsWhere = '';

    if ($aLevels)
    {
        $UserParmsWhere .= " AND user_parms.parmvalue in ('".implode("', '",$aLevels)."') ";
    }

    if ($aNotLevels)
    {
        $UserParmsWhere .= " AND
            user_parms.parmvalue not in ('".implode("', '",$aNotLevels)."')
            AND user_parms.parmvalue != ''";
    }

    $NewWhere = "
    $table.recordid in ((SELECT sec_staff_group.con_id
                            FROM sec_group_permissions
                            JOIN sec_staff_group ON sec_group_permissions.grp_id = sec_staff_group.grp_id
                            WHERE sec_group_permissions.item_code = '".$ModuleDefs[$Module]['PERM_GLOBAL']."'"
                            . $AccessLevelWhere .
                            " )
                            UNION
                            (
                            SELECT contacts_main.recordid FROM contacts_main, profiles, link_profile_group
                                WHERE
                                contacts_main.sta_profile = profiles.recordid
                                AND profiles.recordid = lpg_profile AND lpg_group IN
                                    (SELECT grp_id FROM sec_group_permissions
                                    WHERE
                                    item_code = '".$ModuleDefs[$Module]['PERM_GLOBAL']."'" . $AccessLevelWhere . ")
                            ))
            OR
            (
                (SELECT count(*)
                    FROM sec_staff_group l
                    JOIN sec_groups g on l.grp_id = g.recordid
                    WHERE l.con_id = $table.recordid AND g.grp_type = (g.grp_type | 0)
                ) = 0
                AND
                $table.login in (SELECT user_parms.login
                                FROM user_parms
                                WHERE user_parms.parameter = '".$ModuleDefs[$Module]['PERM_GLOBAL']."'"
                                . $UserParmsWhere .
                                " )
            )";

    if ($aParams['return_where'])
    {
        return $NewWhere;
    }

    $aWhere[] = $NewWhere;

    $sql = "SELECT recordid, initials, fullname, con_jobtitle, con_forenames,
        con_surname, con_title
        FROM contacts_main con
        WHERE (".implode(') AND (',$aWhere).")".GetContactListOrderBy();

    return $sql;
}

function GetContactListOrderBy()
{
    switch (GetParm('STAFF_NAME_DROPDOWN', 'N'))
    {
        case 'N':
            if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
            {
                $OrderBy = ' ORDER BY con_surname, con_forenames';
            }
            else
            {
                $OrderBy = " ORDER BY con_title, con_surname, con_forenames";
            }

            break;
        case 'A':
            if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
            {
                $OrderBy = " ORDER BY con_surname, con_forenames, con_jobtitle";
            }
            else
            {
                $OrderBy = " ORDER BY con_title, con_forenames, con_surname, con_jobtitle";
            }

            break;
        case 'B':
            if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
            {
                $OrderBy = " ORDER BY con_jobtitle, con_surname, con_forenames";
            }
            else
            {
                $OrderBy = " ORDER BY con_jobtitle, con_title, con_forenames, con_surname";
            }

            break;
    }

    return $OrderBy;
}

function FormatContactNameForListSQL($aParams)
{
    if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
    {
        $ContactName = $aParams['data']['con_surname'].', '.$aParams['data']['con_title'].' '.$aParams['data']['con_forenames'];
    }
    else
    {
        $ContactName = $aParams['data']['con_title'].' '.$aParams['data']['con_forenames'].' '.$aParams['data']['con_surname'];
    }

    if (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'A')
    {
        $ContactName .= ' - '.$aParams['data']['con_jobtitle'];
    }
    elseif (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'B')
    {
        $ContactName = $aParams['data']['con_jobtitle'].' - '.$ContactName;
    }

    return $ContactName;
}

function FormatContactNameForList($aParams)
{
    if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
    {
        $ContactName = $aParams['data']['con_surname'].', '.$aParams['data']['con_title'].' '.$aParams['data']['con_forenames'];
    }
    else
    {
        $ContactName = $aParams['data']['con_title'].' '.$aParams['data']['con_forenames'].' '.$aParams['data']['con_surname'];
    }

    if (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'A')
    {
        $ContactName .= ' - '.$aParams['data']['con_jobtitle'];
    }
    elseif (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'B')
    {
        $ContactName = $aParams['data']['con_jobtitle'].' - '.$ContactName;
    }

    return $ContactName;
}

function GetSectionVisibility($module, $level, $data, $parentModule = null, $parentLevel = null)
{
    global $ModuleDefs, $SectionVisibility, $FieldDefs;

    $FormDesign = Forms_FormDesign::GetFormDesign(array(
        'module' => $module,
        'level' => $level,
        'parent_module' => $parentModule,
        'parent_level' => $parentLevel
    ));

    $FormFile = GetBasicFormFileName($module, $level);
    require_once $FormFile;

    //add user defined sections to the form array
    if (is_array($FormDesign->ExtraSections))
    {
        foreach ($FormDesign->ExtraSections as $SectionID => $SectionName)
        {
            $FormArray['section'.$SectionID] = array('Title' => 'User Defined Section '.$SectionID);
        }
    }

    $SectionActions = array();
    $SectionVisibility = array();

    if (is_array($FormDesign->ExpandSections))
    {
        foreach ($FormDesign->ExpandSections as $Field => $Actions)
        {
            foreach ($Actions as $Action)
            {
                $Action['field'] = $Field;
                $SectionActions[$Action['section']][] = $Action;
            }
        }
    }

    foreach ($FormArray as $Section => $SectionArray)
    {
        if ($Section != 'Parameters')
        {
            $SectionVisibility[$Section] = false;

            if (!empty($SectionActions[$Section]))
            {
                foreach ($SectionActions[$Section] as $SectionActionDetails)
                {
                    // Special case for fields that are displayed as checkboxes and trigger a section
                    // This is needed because the data arrives here as a string separated by spaces instead of an array
                    if ($FormDesign->DisplayAsCheckboxes[$SectionActionDetails['field']] === true)
                    {
                        $data[$SectionActionDetails['field']] = explode(' ', $data[$SectionActionDetails['field']]);
                    }

                    if (is_array($data[$SectionActionDetails['field']]))
                    {
                        foreach ($data[$SectionActionDetails['field']] as $value)
                        {
                            //the triggered section will only be shown if the action is active,
                            // (i.e. the triggering field is shown).
                            //otherwise, we risk saving data triggered by data that is not saved
                            if (in_array($value, $SectionActionDetails['values'], true) &&
                                $_POST['show_field_'.$SectionActionDetails['field']] == '1')
                            {
                                $SectionVisibility[$Section] = true;
                            }
                        }
                    }
                    else
                    {
                        if ($FieldDefs[$module][$SectionActionDetails['field']]['Type'] == 'checkbox')
                        {
                            if ($data[$SectionActionDetails['field']] == 'on')
                            {
                                $data[$SectionActionDetails['field']] = 'Y';
                            }

                            if ($data[$SectionActionDetails['field']] != 'Y')
                            {
                                $data[$SectionActionDetails['field']] = 'N';
                            }
                        }

                        //DW-10822 This is a special case for data to be saved in 2 scenarios:
                        //There is data in some section: *S*, that is triggered by some field: *F*
                        //Scenario 1, *F* is in a shown section, hard hidden and has a default value that is the trigger value for *S* (note: you are an idiot, just hard show the section)
                        //Scenario 2 *A* is visible and has the trigger value set in it.
                        if (in_array($data[$SectionActionDetails['field']], $SectionActionDetails['values'], true) &&
                            ($_POST['show_field_'.$SectionActionDetails['field']] == '1') || $FormDesign->HideFields[$SectionActionDetails['field']])
                        {
                            $SectionVisibility[$Section] = true;
                        }
                    }
                }
            }

            if ($SectionArray['Show'] && $SectionVisibility[$Section] !== true) // hard coded show/hide
            {
                if (!$GLOBALS['HideFields'][$SectionArray['Show']])
                {
                    if ($data[$SectionArray['Show']] == 'Y' || $data[$SectionArray['Show']] == 'on')
                    {
                        $SectionVisibility[$Section] = true;
                    }
                    elseif ($data[$SectionArray['Show']] == 'N' || $data[$SectionArray['Show']] == '')
                    {
                        $SectionVisibility[$Section] = false;
                    }
                }
                elseif (empty($SectionActions[$Section]))
                {
                    //the hard coded show/hide is hidden and there is no user-defined one to replace it
                    $SectionVisibility[$Section] = true;
                }
            }
            elseif (empty($SectionActions[$Section]))
            {
                //there is no show/hide toggle on this section
                $SectionVisibility[$Section] = true;
            }

            if ($FormDesign->HideFields[$Section] || ((array_key_exists("Condition", $SectionArray) &&
                $SectionArray["Condition"] === false)))
            {
                $SectionVisibility[$Section] = null;
            }
        }
    }
}

/**
 * Iterates $data looking for code fields with can-add param. If the value
 * matches !NEWCODE!, a new code is created and $data is changed accordingly.
 * @param $module
 * @param $data Post data
 * @param $FieldDefs
 * @return \Post $data modified
 */
function ProcessNewCodeFieldValues ($module, $data, $FieldDefs)
{
    $registry    = \src\framework\registry\Registry::getInstance();
    $moduleTable = $registry->getModuleDefs()[$module]['TABLE'];
    $defs        = $registry->getFieldDefs();

    foreach ($FieldDefs[$module] as $field => $params)
    {
        // code field with can-add option?
        if ($params['Type'] == 'ff_select' && !empty($params['data']['can-add']))
        {
            // new value?
            if (\UnicodeString::substr($data[$field], 0, 10) == '!NEWCODE!=')
            {
                // TODO: Add code to deal with UDF codes

                // find table
                $codeTable = $defs[$moduleTable.'.'.$field]->getCodeTable();

                // find next number
                $result = \DatixDBQuery::PDO_fetch('SELECT COALESCE(MAX(CAST(code AS INTEGER))+1,1) AS nextId FROM '.$codeTable.' WHERE ISNUMERIC(code) = 1');

                // create new code
                $description = \UnicodeString::substr ($data[$field], 10);
                $description = \UnicodeString::substr ($description, 0, 254); // truncate description
                \DatixDBQuery::PDO_insert("INSERT INTO {$codeTable} (code, description) VALUES (?, ?)", [$result['nextId'], $description]);
                $data[$field] = $result['nextId'];
            }
        }
    }

    return $data;
}
    /*
     *  This steals some code from BlankOutPostValues (Subs) to deal with medications which needs special
     *  functionality *sigh*. I've stripped out parts on the original function to the minimum requirements.
     *
     * @param $module           the module used for getting the form design
     * @param $level            the level of the form, used for getting the form design
     * @param $parentModule     the module of the parent form design, used for getting the form design
     * @param $parentLevel      the level of the parent form design, used for getting the form design
     * @param $request          the request object, which is also 'blanked out' if provided
     */
function BlankOutPostValues($module, $level, $parentModule = null, $parentLevel = null, src\framework\controller\Request $request = null)
{
    global $ModuleDefs, $SectionVisibility, $HideFields, $DefaultValues;

    $FormDesign = Forms_FormDesign::GetFormDesign(array(
        'module' => $module,
        'level' => $level,
        'parent_module' => $parentModule,
        'parent_level' => $parentLevel
    ));

    $FormFile = GetBasicFormFileName($module, $level);
    require $FormFile;

    //add user defined sections to the form array
    if (is_array($FormDesign->ExtraSections))
    {
        foreach ($FormDesign->ExtraSections as $SectionID => $SectionName)
        {
            $FormArray['section'.$SectionID] = array('Title' => 'User Defined Section '.$SectionID);
        }
    }

    //add user defined fields to the form array
    if (is_array($FormDesign->ExtraFields))
    {
        foreach ($FormDesign->ExtraFields as $Field => $SectionName)
        {
            list($FieldID, $GroupID) = explode('_', $Field);
            $ExtraField = new Fields_ExtraField($FieldID, $GroupID);
            $FormArray[$SectionName]['Rows'][] = $ExtraField->getName();
        }
    }

    if (is_array($FormDesign->MoveFieldsToSections))
    {
        foreach ($FormDesign->MoveFieldsToSections as $Field => $aDetails)
        {
            $aDetails['Field'] = $Field;
            $MoveFieldsByNewSection[$aDetails['New']][] = $aDetails;
        }
    }

    if (is_array($FormArray))
    {
        foreach ($FormArray as $Section => $SectionArray)
        {
            if ($Section != 'Parameters' && $Section != 'ram_control_grading' && $Section != 'grading')
            {
                if ($SectionVisibility[$Section] === false)
                {
                    if (is_array($SectionArray['Rows']))
                    {
                        foreach ($SectionArray['Rows'] as $Field)
                        {
                            if (is_array($Field))
                            {
                                $FieldName = $Field['Name'];
                            }
                            else //just a string.
                            {
                                $FieldName = $Field;
                            }

                            if ($FieldName)
                            {
                                if ($FieldName == 'recordid') //don't want to blank this out accidentally.
                                {
                                    continue;
                                }

                                if (!$FormDesign->MoveFieldsToSections[$FieldName])
                                {
                                    unset($_POST[$FieldName]);
                                    // in case we are dealing with extra field multicodes - otherwise the saveUDF
                                    // function will wipe out the value
                                    unset($_POST['Select'.$FieldName]);
                                    if(!is_null($request))
                                    {
                                        $request->unSetParameter($FieldName);
                                        $request->unSetParameter('Select'.$FieldName);
                                    }
                                }
                            }
                        }
                    }

                    if (is_array($MoveFieldsByNewSection[$Section]))
                    {
                        foreach ($MoveFieldsByNewSection[$Section] as $aDetails)
                        {
                            $FieldName = $aDetails['Field'];

                            if ($FieldName)
                            {
                                if (!$_POST['show_field_'.$FieldName])
                                {
                                    unset($_POST[$FieldName]);
                                    // in case we are dealing with extra field multicodes - otherwise the saveUDF
                                    // function will wipe out the value
                                    unset($_POST['Select'.$FieldName]);
                                    if(!is_null($request))
                                    {
                                        $request->unSetParameter($FieldName);
                                        $request->unSetParameter('Select'.$FieldName);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (is_array($SectionArray['Rows']))
                    {
                        foreach ($SectionArray['Rows'] as $Field)
                        {
                            if (is_array($Field))
                            {
                                $FieldName = $Field['Name'];
                            }
                            else //just a string.
                            {
                                $FieldName = $Field;
                            }

                            if ($FieldName == 'recordid') //don't want to blank this out accidentally.
                            {
                                continue;
                            }

                            if (!$FormDesign->MoveFieldsToSections[$FieldName] &&
                                (!($FormDesign->HideFields[$FieldName] &&
                                    isset($FormDesign->DefaultValues[$FieldName]))))
                            {
                                if (!$_POST['show_field_'.$FieldName])
                                {
                                    unset($_POST[$FieldName]);
                                    // in case we are dealing with extra field multicodes - otherwise the saveUDF
                                    // function will wipe out the value
                                    unset($_POST['Select'.$FieldName]);
                                    if(!is_null($request))
                                    {
                                        $request->unSetParameter($FieldName);
                                        $request->unSetParameter('Select'.$FieldName);
                                    }
                                }
                            }
                        }
                    }

                    if (is_array($MoveFieldsByNewSection[$Section]))
                    {
                        foreach ($MoveFieldsByNewSection[$Section] as $aDetails)
                        {
                            $FieldName = $aDetails['Field'];

                            if (!$_POST['show_field_'.$FieldName] && (!($FormDesign->HideFields[$FieldName] &&
                                isset($FormDesign->DefaultValues[$FieldName]))))
                            {
                                unset($_POST[$FieldName]);
                                // in case we are dealing with extra field multicodes - otherwise the saveUDF
                                // function will wipe out the value
                                unset($_POST['Select'.$FieldName]);
                                if(!is_null($request))
                                {
                                    $request->unSetParameter($FieldName);
                                    $request->unSetParameter('Select'.$FieldName);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function GetContactFullname($con_id)
{
    $sql = "SELECT fullname FROM contacts_main
        WHERE recordid = $con_id";
    $request = db_query($sql);

    $row = db_fetch_array($request);

    return $row["fullname"];
}

function GetBasicFormFileName($Module, $level)
{
    global $ModuleDefs;

    if ($ModuleDefs[$Module]['GENERIC'])
    {
        if (file_exists('Source/generic_modules/'.$ModuleDefs[$Module]['GENERIC_FOLDER'].'/BasicForm'.$level.'.php'))
        {
            $Return = 'Source/generic_modules/'.$ModuleDefs[$Module]['GENERIC_FOLDER'].'/BasicForm'.$level.'.php';
        }
        else
        {
            $Return = 'Source/generic/BasicForm'.$level.'.php';
        }
    }
    else
    {
        $Return = $ModuleDefs[$Module]['BASIC_FORM_FILES'][$level];
    }

    return $Return;
}

/**
* @desc Constructs the HTML wrapper for a field to be placed in a Datix form.
*
* @param string $Label The Field label to appear beside the field
* @param string $FieldHTML The HTML of the field itself
* @param int $Width The width in percent that the label should take up.
* @param string $RowId The name and id to give to the <li> object that represents the field row.
* @param bool $Hide True if this fieldrow should be hidden initially.
* @param bool $UseLists True if this fieldrow should use <li> rather than <div> tags.
* @param string $ExtraClasses Extra classes to add to the "class" attribute of the row.
*
* @return HTML for this fieldrow
*/
function GetDivFieldHTML($Label, $FieldHTML, $ExtraText='', $Width=25, $RowId = '', $Hide = false,
                         $UseLists = true, $ExtraClasses = '')
{
    if ($Width == '')
    {
        $Width = 25;
    }

    if ($UseLists)
    {
        $Field = '<li class="new_windowbg field_div '.$ExtraClasses.'"';

        if ($RowId)
        {
            $Field .= ' id="'.$RowId.'" name="'.$RowId.'"';
        }

        if ($Hide)
        {
            $Field .= ' style="display:none"';
        }

        $Field .= '>';
    }
    else
    {
        $Field = '<div class="new_windowbg field_div"';

        if ($RowId)
        {
            $Field .= ' id="'.$RowId.'" name="'.$RowId.'"';
        }

        if ($Hide)
        {
            $Field .= ' style="display:none"';
        }

        $Field .= ' >';
    }

    $Field .= '
        <div style="float:left;padding:6px;width:'.$Width.'%">';

    $Field .= '
            <div class="field_label">'.$Label.'</div>';

    if ($ExtraText)
    {
        $Field .= '
            <div class="field_extra_text">'.$ExtraText.'</div>';
    }

    $Field .= '
        </div>';

    $Field .= '
        <div style="width:'.(95-$Width).'%;float:left;padding:4px">
            '.$FieldHTML.'
        </div>';

    if ($UseLists)
    {
        $Field .= '</li>';
    }
    else
    {
        $Field .= '</div>';
    }

    return $Field;
}

function getRowList()
{
    require_once 'Source/libs/FormClasses.php';

    switch($_POST['type'])
    {
        case 'com_subject':
        case 'pal_subject':
            require_once 'Source/generic/Subjects.php';
            $Rows = getBasicSubjectForm(array('rows' => true, 'module' => $_POST['module']));
            break;
        case 'com_issue':
            require_once 'Source/generic_modules/COM/ModuleFunctions.php';
            $Rows = getBasicIssueForm(array('rows' => true, 'module' => $_POST['module']));
            break;
        case 'inc_causal_factor':
            $loader = new \src\framework\controller\Loader();
            $controller = $loader->getController(
                array('controller' => 'src\\causalfactors\\controllers\\CausalFactorsController')
            );
            $controller->setRequestParameter('aParams', array('rows' => true, 'module' => $_POST['module']));
            $Rows = $controller->doAction('getBasicCausalFactorForm');
            break;
    }

    echo implode(',', $Rows['Rows']);
}

/**
* Saves all linked records associated with a main record.
*
* Iterates through each instance on the form of this linked record type
* and calls {@link SaveLinkedRecord()} to save a record for each one.
*
* @param string $aParams['table'] The link table name.
* @param int $aParams['main_recordid_label'] The field name for the main record's unique id.
* @param int $aParams['main_recordid'] The value for the main record's unique id.
* @param int $_POST[$aParams['type'].'_max_suffix'] The number (+1) of linked records, used when iterating
 * through each linked record.
* @param boolean $aParams['useIdentity'] Specify that the link table uses an identity column for recordids.
*/
function SaveLinkedRecords($aParams)
{
    $SaveData = true;

    if ($aParams['save_controller'] && isset($_POST[$aParams['save_controller']]) &&
        !bYN($_POST[$aParams['save_controller']]))
    {
        $SaveData = false;
    }

    if ($SaveData)
    {
        $MaxSuffix = Sanitize::SanitizeString($_POST[$aParams['type'].'_max_suffix']);

        for ($i=0; $i<$MaxSuffix; $i++)
        {
            $aParams['suffix'] = $i;
            //Check for existing linked recordid in order to perform update rather than insert.
            $aParams['recordid'] = Sanitize::SanitizeInt($_POST[$aParams['type'] . "_link_id_" . $aParams['suffix']]);

            if (!RecordNotFilledIn($aParams))
            {
                if ($aParams['useIdentity'])
                {
                    $RecordIdsNoDelete[] = saveLinkedRecordIdentity($aParams);
                }
                else
                {
                    $RecordIdsNoDelete[] = SaveLinkedRecord($aParams);
                }
            }
        }
    }

    //Delete records which have been removed and make sure not to delete the ones that have been saved.
    $sql = 'DELETE FROM '.$aParams['table'].' WHERE '.$aParams['main_recordid_label'].' = :main_recordid';

    if (is_array($RecordIdsNoDelete))
    {
        $ListDeleteExclude = implode(",", $RecordIdsNoDelete);
        $sql .=' AND recordid NOT IN (' . $ListDeleteExclude . ')';
    }

    PDO_query($sql, array('main_recordid' => $aParams['main_recordid']));
}

/**
* Determines whether any of a given set of form fields have been filled in (i.e. are not empty).
*
* Used when saving linked records ({@link SaveLinkedRecords()}),
* checks to see if the submitted form holds any data for the linked record.
*
* @param string $aParams['suffix'] Identifier for this linked record.
* @param array  $aParams['basic_form']['Rows'] The fields for this linked record.
* @param array  $_POST Contains the submitted form data.
*
* @return boolean Whether or not any of the linked record fields have been filled in.
*/
function RecordNotFilledIn($aParams)
{
    global $FieldDefs;

    $Data = $_POST;
    $Suffix = $aParams['suffix'];
    $Rows = $aParams['basic_form']['Rows'];

    foreach ($Rows as $Row)
    {
        if ($Data[$Row.'_'.$Suffix] != '')
        {
            return false;
        }

        //If this is a text field, need to check there isn't any history data either/
        if ($FieldDefs[$aParams['module']][CheckFieldMappings($aParams['module'], $Row, true)]['Type'] == 'textarea')
        {
            if ($Data['CURRENT_'.$Row.'_'.$Suffix] != '')
            {
                return false;
            }
        }
    }

    return true;
}

/**
* Saves an individual linked record attached to a main record.
*
* Called by {@link SaveLinkedRecords()}.
*
* @global array $FieldDefs
*
* @param array $_POST Contains the submitted form data.
* @param string $aParams['table'] The link table name.
* @param string $aParams['real_table'] The "real table" name if $aParams['table'] is a view.
* @param string $aParams['recordid_field'] The field name for the linked record's unique id.
* @param int $aParams['recordid'] The value for the linked record's unique id.
* @param string $aParams['main_recordid_label'] The field name for the main record's unique id.
* @param int $aParams['main_recordid'] The value for the main record's unique id.
* @param boolean $aParams['save_listorder'] Whether or not the list order (order in which the record is displayed)
 * should be saved.
* @param string $aParams['type'] The type of linked record.
* @param int $aParams['suffix'] The ID (on the form) for this linked record.
* @param string $aParams['module'] The module code.
* @param array $aParams['basic_form']['Rows'] The fields for this linked record.
* @param array $aParams['RunAfterSave'] Provides a list of functions to run once the linked record is saved.
* @param array $aParams['RunAfterSaveIncludes'] Include file(s) for the RunAfterSave function(s).
*/
function SaveLinkedRecord($aParams)
{
    global $FieldDefs;

    $data = QuotePostArray(ParseSaveData(array(
        'module' => $aParams['module'],
        'data' => Sanitize::SanitizeRawArray($_POST),
        'suffix' => '_'.$aParams['suffix']
    )));

    // we need to use the "real table" if $aParams['table'] is a view,
    // so the correct table name is inserted into the controls table
    $table = array_key_exists('real_table', $aParams) ? $aParams['real_table'] : $aParams['table'];

    if (empty($aParams['recordid']))
    {
        $aParams['recordid'] = GetNextRecordid($table);

        $sql = '
            INSERT INTO
                '.$aParams['table'].'
                ('.$aParams['recordid_field'].','.$aParams['main_recordid_label'].')
            VALUES
                ('.$aParams['recordid'].','.$aParams['main_recordid'].')
        ';

        db_query($sql);
    }

    if ($aParams['save_listorder'] && isset($data[$aParams['type'].'_listorder_'.$aParams['suffix']]))
    {
        $Listorder = intval($data[$aParams['type'].'_listorder_'.$aParams['suffix']]);
        $UpdateArray[] = 'listorder = '.($Listorder == 0 ? 'NULL' : $Listorder);
    }
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => $aParams['module'], 'level' => $data['formlevel'], 'parent_module' =>  null));
    foreach ($aParams['basic_form']['Rows'] as $field)
    {
        if ($FieldDefs[$aParams['module']][$field]["DummyField"] != true &&
            ($data["show_field_" . $field . "_" .$aParams['suffix']] == "1" ||
                ($FormDesign->HideFields[$field] && $FormDesign->DefaultValues[$field])))
        {
            if (empty($data[$field.'_'.$aParams['suffix']]))
            {
                $UpdateArray[] = $field.' = null';
            }
            else
            {
                $UpdateArray[] = $field.' = \''.$data[$field.'_'.$aParams['suffix']].'\'';
            }
        }
    }

    if ($aParams['main_recordid'])
    {
        $UpdateArray[] = $aParams['main_recordid_label'].' = \''.$aParams['main_recordid'].'\'';
    }

    $sql = '
        UPDATE
            '.$aParams['table'].'
        SET
            '.implode(', ',$UpdateArray).'
        WHERE
            '.$aParams['recordid_field'].' = '.$aParams['recordid'];

    DatixDBQuery::PDO_query($sql);

    if (is_array($aParams['RunAfterSaveIncludes']))
    {
        foreach ($aParams['RunAfterSaveIncludes'] as $includes)
        {
            require_once $includes;
        }
    }

    if (is_array($aParams['RunAfterSave']))
    {
        foreach ($aParams['RunAfterSave'] as $function)
        {
            eval($function.'($aParams);');
        }
    }

    return $aParams['recordid'];
}

/**
* Saves a record to a link table which uses an identity column for the recordid.
*
* Similar to {@link SaveLinkedRecords()}, but without the need to create the recordid manually.
*
* @global array $FieldDefs
*
* @param array $_POST Contains the submitted form data.
* @param string $aParams['table'] The link table name.
* @param string $aParams['recordid_field'] The field name for the linked record's unique id.
* @param int $aParams['recordid'] The value for the linked record's unique id.
* @param string $aParams['main_recordid_label'] The field name for the main record's unique id.
* @param int $aParams['main_recordid'] The value for the main record's unique id.
* @param boolean $aParams['save_listorder'] Whether or not the list order (order in which the record is displayed)
 * should be saved.
* @param string $aParams['type'] The type of linked record.
* @param int $aParams['suffix'] The ID (on the form) for this linked record.
* @param array $aParams['basic_form']['Rows'] The fields for this linked record.
* @param string $aParams['module'] The module code.
* @param array $aParams['RunAfterSave'] Provides a list of functions to run once the linked record is saved.
* @param array $aParams['RunAfterSaveIncludes'] Include file(s) for the RunAfterSave function(s).
*/
function saveLinkedRecordIdentity($aParams)
{
    global $FieldDefs;

    // construct values array to hold the data being inserted into the table
    $values = array();
    $data = ParseSaveData(array(
        'module' => $aParams['module'],
        'data' => Sanitize::SanitizeRawArray($_POST),
        'suffix' => '_'.$aParams['suffix']
    ));

    if ($aParams['save_listorder'] && $data[$aParams['type'] . '_listorder_' . $aParams['suffix']])
    {
        $values['listorder'] = $data[$aParams['type'] . '_listorder_' . $aParams['suffix']];
    }
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => $aParams['module'], 'level' => $data['formlevel'], 'parent_module' =>  null));
    foreach ($aParams['basic_form']['Rows'] as $field)
    {
        if ($FieldDefs[$aParams['module']][$field]['DummyField'] != true &&
            ($data["show_field_" . $field . "_" .$aParams['suffix']] == "1" ||
            ($FormDesign->HideFields[$field] && $FormDesign->DefaultValues[$field])))
        {
            if ($FieldDefs[$aParams['module']][$field]['Type'] == 'multilistbox' &&
                is_array($data[$field . '_' . $aParams['suffix']]))
            {
                $values[$field] = implode(' ', $data[$field . '_' . $aParams['suffix']]);
            }
            else
            {
                $values[$field] = $data[$field . '_' . $aParams['suffix']];
            }
        }
    }

    if ($aParams['main_recordid'])
    {
        $values[$aParams['main_recordid_label']] = $aParams['main_recordid'];
    }

    if (empty($aParams['recordid']))
    {
        // insert a new record
        $sql = 'INSERT INTO ' . $aParams['table'] . ' ';
        $sql .= GeneratePDOInsertSQLFromArrays(
            array(
                'Module' => $aParams['module'],
                'FieldArray' => array_keys($values),
                'DataArray' => $values
            ),
            $PDOParamsArray
        );
        $aParams['recordid'] = DatixDBQuery::PDO_insert($sql, $PDOParamsArray);
    }
    else
    {
        // update an existing record
        $sql = 'UPDATE ' . $aParams['table'] . ' SET ';
        $sql .= GeneratePDOSQLFromArrays(
            array(
                'Module' => $aParams['module'],
                'FieldArray' => array_keys($values),
                'DataArray' => $values
            ),
            $PDOParamsArray
        );
        $sql .= ' WHERE ' . $aParams['recordid_field'] . ' = :' . $aParams['recordid_field'];
        $PDOParamsArray[$aParams['recordid_field']] = $aParams['recordid'];
        DatixDBQuery::PDO_query($sql, $PDOParamsArray);
    }

    if (is_array($aParams['RunAfterSaveIncludes']))
    {
        foreach ($aParams['RunAfterSaveIncludes'] as $includes)
        {
            require_once $includes;
        }
    }

    if (is_array($aParams['RunAfterSave']))
    {
        foreach ($aParams['RunAfterSave'] as $function)
        {
            eval($function . '($aParams);');
        }
    }

    return $aParams['recordid'];
}

function CheckFieldMappings($Module, $Field, $Reverse = false)
{
    global $ModuleDefs;

    if (is_array($ModuleDefs[$Module]['FIELD_MAPPINGS']))
    {
        if ($Reverse)
        {
            $ReverseFieldMapping = array_flip($ModuleDefs[$Module]['FIELD_MAPPINGS']);

            if ($ReverseFieldMapping[$Field])
            {
                return $ReverseFieldMapping[$Field];
            }
        }
        elseif ($ModuleDefs[$Module]['FIELD_MAPPINGS'][$Field])
        {
            return $ModuleDefs[$Module]['FIELD_MAPPINGS'][$Field];
        }
    }

    return $Field;
}

function AddSuffixToData($Data, $Suffix, $Rows)
{
    $NewData = $Data;

    foreach ($Rows['Rows'] as $Field)
    {
        $NewData[$Field.'_'.$Suffix] = $Data[$Field];
        unset($NewData[$Field]);
    }

    return $NewData;
}

function AddSuffixToFormDesign($aParams)
{
    global $ModuleDefs;

    $FileName = $aParams['filename'];
    $Suffix = $aParams['suffix'];
    $Module = $aParams['module'];

    if (file_exists($FileName))
    {
        include($FileName);

        if ($aParams['modifyforsubjects'])
        {
            ModifyFormDesignForSubjects();
        }

        if ($aParams['modifyforissues'])
        {
            ModifyFormDesignForIssues();
        }

        if ($aParams['modifyformedications'])
        {
            ModifyFormDesignForMedications();
        }

        if ($Suffix)
        {
            foreach (array("HideFields", "UserExtraText", 'DefaultValues', 'UserLabels', 'ContactMatch', 'ReadOnlyFields', 'OrganisationMatch') as $Key)
            {
                if (is_array($GLOBALS[$Key]))
                {
                    $NewArray = array();

                    foreach ($GLOBALS[$Key] as $FieldName => $value)
                    {
                        unset($GLOBALS[$Key][$FieldName]);
                        $NewArray[$FieldName."_".$Suffix] = $value;
                    }

                    $GLOBALS[$Key] = $NewArray;
                }
            }

            if (is_array($GLOBALS['ExpandSections']))
            {
                $NewArray = array();

                foreach ($GLOBALS['ExpandSections'] as $FieldName => $SectionArray)
                {
                    unset($GLOBALS['ExpandSections'][$FieldName]);

                    $NewArray[$FieldName."_".$Suffix] = $SectionArray;

                    foreach ($SectionArray as $index => $value)
                    {
                        if ($value['section'])
                        {
                            $NewArray[$FieldName."_".$Suffix][$index]['section'] = $value['section'].'_'.$Suffix;
                        }
                    }
                }

                $GLOBALS['ExpandSections'] = $NewArray;
            }

            if (is_array($GLOBALS['ExpandFields']))
            {
                $NewArray = array();

                foreach ($GLOBALS['ExpandFields'] as $FieldName => $FieldArray)
                {
                    unset($GLOBALS['ExpandSections'][$FieldName]);

                    $NewArray[$FieldName."_".$Suffix] = $FieldArray;

                    foreach ($FieldArray as $index => $value)
                    {
                        if ($value['field'])
                        {
                            $NewArray[$FieldName."_".$Suffix][$index]['field'] = $value['field'].'_'.$Suffix;
                        }
                    }
                }

                $GLOBALS['ExpandFields'] = $NewArray;
            }

            if (is_array($GLOBALS["HelpTexts"]))
            {
                $NewArray = array();

                foreach ($GLOBALS["HelpTexts"] as $FieldName => $value)
                {
                    unset($GLOBALS["HelpTexts"][$FieldName]);
                    $NewArray[$FieldName."_".$Suffix] = $value;
                }

                $GLOBALS["HelpTexts"] = $NewArray;
                $_SESSION['HelpTexts'] = array_merge((is_array($_SESSION['HelpTexts']) ? $_SESSION['HelpTexts'] : array()) , $NewArray);
            }

            if (is_array($GLOBALS["MandatoryFields"]))
            {
                $NewArray = array();

                foreach ($GLOBALS["MandatoryFields"] as $FieldName => $value)
                {
                    $FieldnameWithSuffix = $FieldName.($Suffix ? "_".$Suffix : '');
                    unset($GLOBALS["MandatoryFields"][$FieldName]);
                    $NewArray[$FieldnameWithSuffix] = $value;

                    // If rows are provided, make sure we are returning appropriate mandatory fields.
                    if (!(is_array($aIssueRows['Rows']) && !in_array($FieldName, $aIssueRows['Rows'])))
                    {
                        $mandatoryJSChanges .= 'if (mandatoryArray){mandatoryArray.push(new Array("'.$FieldName."_".$Suffix.'","'.$value.(!$aParams['no_section_suffix'] ? "_".$Suffix : '').'","'.

                        \UnicodeString::str_ireplace('"', '\"', Labels_FormLabel::GetFormFieldLabel($FieldName, "", $Module, $FieldnameWithSuffix)).

                        ($Suffix > 0 ? ' ('.($aParams['type']).')' : '').'"))};
                        ';
                    }
                }

                $GLOBALS["MandatoryFields"] = $NewArray;
            }

            if (is_array($GLOBALS["FieldOrders"]))
            {
                $NewArray = array();

                foreach ($GLOBALS["FieldOrders"] as $id => $Section)
                {
                    foreach ($Section as $value => $FieldName)
                    {
                        unset($GLOBALS["FieldOrders"][$id][$value]);
                        $NewArray[$id."_".$Suffix][$value] = $FieldName."_".$Suffix;
                    }
                }

                $GLOBALS["FieldOrders"] = $NewArray;
            }

            //Store contact labels in session for later use in dropdown popups
            if (!is_array($_SESSION["CONTACTLABELS"]))
            {
                $_SESSION['CONTACTLABELS'] = array();
            }
            if (is_array($GLOBALS["UserLabels"]))
            {
                $_SESSION['CONTACTLABELS'] = array_merge($_SESSION['CONTACTLABELS'], $GLOBALS["UserLabels"]);
            }

        }
    }

    return $mandatoryJSChanges;
}

use Source\classes\Filters\Container;

function AddDynamicSection()
{
    require_once 'Source/libs/FormClasses.php';
    $SavedFormDesignSettings = saveFormDesignSettings();
    unsetFormDesignSettings();

    $data = Sanitize::SanitizeRawArray($_POST);
    $type = RemoveSuffix(Sanitize::SanitizeString($_POST['type']));
    $module = Sanitize::getModule($_POST['module']);
    $level = Sanitize::SanitizeString($_POST['level']);
    $suffix = Sanitize::SanitizeString($_POST['suffix']);
    $clear = Sanitize::SanitizeString($_POST['clearsection']);

    switch($type)
    {
        case 'pal_subject':
        case 'com_subject':
            require_once 'Source/generic/Subjects.php';
            echo getSubjectSectionHTML(array(
                'module'=>$module,
                'data'=>$data,
                'ajax'=>true,
                'formtype'=>'New',
                'level'=>$level,
                'suffix' => $suffix,
                'clearsection' => $clear,
                'subject_name' => $type
            ));
            break;
        case 'com_issue':
            require_once 'Source/generic_modules/COM/ModuleFunctions.php';
            echo getIssueSectionHTML(array(
                'module' => $module,
                'data' => $data,
                'ajax' => true,
                'formtype' => 'New',
                'level' => $level,
                'suffix' => $suffix,
                'clearsection' => $clear,
                'issue_name' => $type
            ));
            break;
        case 'inc_causal_factor':
            $loader = new \src\framework\controller\Loader();
            $controller = $loader->getController(
                array('controller' => 'src\\causalfactors\\controllers\\CausalFactorsController')
            );
            $controller->setRequestParameter('aParams', array(
                'module' => $module,
                'data' => $data,
                'ajax' => true,
                'formtype' => 'New',
                'level' => $level,
                'suffix' => $suffix,
                'clearsection' => $clear,
                'causal_factor_name' => $type
            ));
            echo $controller->doAction('getCausalFactorsSectionHTML');
            break;
        case 'cla_causal_factor':
            $loader = new \src\framework\controller\Loader();
            $controller = $loader->getController(
                array('controller' => 'src\\causalfactors\\controllers\\CausalFactorsController')
            );
            $controller->setRequestParameter('aParams', array(
                'module' => $module,
                'data' => $data,
                'ajax' => true,
                'formtype' => 'New',
                'level' => $level,
                'suffix' => $suffix,
                'clearsection' => $clear,
                'causal_factor_name' => $type
            ));
            echo $controller->doAction('getCausalFactorsSectionHTML');
            break;
        case 'inc_medication':
            require_once 'Source/medications/MainMedications.php';
            echo getMedicationSectionHTML(array(
                'module'=>$module,
                'data'=>$data,
                'ajax'=>true,
                'formtype'=>'New',
                'level'=>$level,
                'suffix' => $suffix,
                'clearsection' => $clear,
                'medication_name' => $type
            ));
            break;
        case 'document':
            echo get_document_section(array(
            	'data'=> $data,
            	'ajax'=>true,
            	'formtype'=>'New',
            	'level'=> $level,
            	'suffix' => $suffix,
            	'clear' => $clear
            ));
            break;
        case 'contact':
            echo get_contact_section(array(
            	'data'=> $data,
            	'ajax'=>true,
            	'formtype'=>'New',
            	'level'=> $level,
            	'suffix' => $suffix,
            	'module' => $module,
            	'contacttype' => Sanitize::SanitizeString($_POST['subtype']),
            	'clear' => $clear,
                'FormType' => 'New'
            ));
            break;
        case 'equipment':
            require_once 'Source/incidents/TrustProperty.php';
            echo get_equipment_section(array(
                'data'=>$data,
                'ajax'=>true,
                'FormType'=>'New',
                'level'=>$level,
                'suffix' => $suffix,
                'module' => $module,
                'clear' => $clear
            ));
            break;
        case 'condition':
            require_once 'Source/generic_modules/HSA/FormFunctions.php';
            getFormSection('', '', '', array('sectionFunction' => 'getConditionSection', 'suffix' => $suffix));
            break;
        case 'recipients':
            require_once 'Source/generic_modules/HSA/FormFunctions.php';
            getFormSection('', '', '', array('sectionFunction' => 'getRecipientsSection', 'suffix' => $suffix));
            break;
        case 'recipient_conditions':
            require_once 'Source/generic_modules/HSA/FormFunctions.php';
            getFormSection('', '', '', array('sectionFunction' => 'getRecipientConditionSection', 'suffix' => $suffix));
            break;
        case 'step':
            $loader = new src\framework\controller\Loader();
            $controller = $loader->getController(array(
                'controller' => 'src\\admin\\actionchains\\controllers\\ActionChainStepsController'
            ));
            echo $controller->doAction('getStep');
            break;
        case 'filter':
            $filter = Container::getFilter($module, false);
            echo $filter->addAnother($suffix, $module);
            break;
        case 'organisation':
            $loader = new \src\framework\controller\Loader();
            $controller = $loader->getController(
                array('controller' => 'src\\respondents\\controllers\\RespondentsController')
            );
            $controller->setRequestParameter('aParams', array(
                'data'=> $data,
                'ajax'=>true,
                'formtype'=>'New',
                'level'=> $level,
                'suffix' => $suffix,
                'module' => $module,
                'organisationType' => Sanitize::SanitizeString($_POST['subtype']),
                'clear' => $clear,
                'FormType' => 'New'
            ));
            echo $controller->doAction('getOrganisationSection');
            break;
    }

    // Script to bind the calendar to date fields
    echo '
        <script language="javascript" type="text/javascript">
            initialiseCalendars();
        </script>
    ';

    loadFormDesignSettings($SavedFormDesignSettings);
}

function getJSFunctions()
{
    global $JSFunctions;

    $JSFunctionString = '';

    if (is_array($JSFunctions) && $_GET['print'] != 1)
    {
        $JSFunctionString .= '<script language="JavaScript" type="text/javascript">
        ';

        foreach ($JSFunctions as $Function)
        {
            $JSFunctionString .= $Function.'
            ';
        }

        $JSFunctionString .= '</script>
        ';
    }

    return $JSFunctionString;
}

function echoJSFunctions()
{
    echo getJSFunctions();
}

function GetContactArray($aData, $SuffixString = '')
{
    global $ModuleDefs;

    foreach ($ModuleDefs['CON']['FIELD_ARRAY'] as $field)
    {
        if (array_key_exists($field.$SuffixString, $aData))
        {
            $ContactArray[$field] = $aData[$field.$SuffixString];
        }
    }

    return $ContactArray;
}

function PreSaveLinkContactForComplaints($aParams)
{
    global $ModuleDefs;

    $lcom_iscomplpat = $_POST['lcom_iscomplpat'.$aParams['suffixstring']];

    if (!isset($lcom_iscomplpat) && $_REQUEST['con_recordid']  && $aParams['main_recordid'])
    {
        //we don't have a posted value for lcom_iscomplpat, so we need to get it from the database.
        if ($_POST['from_contact_match'])
        {
            if ($_REQUEST['contact_match_link_id'])
            {
                //the new contact doesn't have linked data yet, so we need to pick it out of the old contact record.
                $sql = '
                    SELECT lcom_iscomplpat FROM link_compl, link_contacts
                    WHERE link_contacts.com_id = link_compl.com_id
                    AND link_contacts.con_id = link_compl.con_id
                    AND link_contacts.LINK_RECORDID = :contact_match_link_id';
                $row = DatixDBQuery::PDO_fetch($sql, array(
                    "contact_match_link_id" => $_REQUEST['contact_match_link_id']
                ));

                $lcom_iscomplpat = $row['lcom_iscomplpat'];
            }
            else
            {
                // There is no old contact, and no data posted, so we have to assume that there is no value
                // for lcom_iscomplpat.
                $lcom_iscomplpat = 'N';
            }
        }
        else
        {
            // The contact is not being matched, so we can just use the con_recordid value to get the data.
            $sql = 'SELECT lcom_iscomplpat FROM link_compl WHERE com_id = :com_id AND con_id = :con_id';
            $row = DatixDBQuery::PDO_fetch($sql, array(
                "com_id" => $aParams['main_recordid'],
                "con_id" => Sanitize::SanitizeInt($_REQUEST['con_recordid'])
            ));
            $lcom_iscomplpat = $row['lcom_iscomplpat'];
        }
    }

    if ($lcom_iscomplpat == 'Y' && $_POST['link_type'.$aParams['suffixstring']] == 'C')
    {
        $_POST['link_type'.$aParams['suffixstring']] = 'A';
        $_POST['real_link_type'.$aParams['suffixstring']] = 'C';
    }
    else
    {
        $_POST['real_link_type'.$aParams['suffixstring']] = $_POST['link_type'.$aParams['suffixstring']];
    }
}

 /**
 * @desc Normally contacts can be linked multiple times with different link_type values, but this is not
  * always the case. Called when a contact is linked to a record, this function checks whether these types of
  * contacts have a mutually exclusive relationship with any other types of contact. If so, then instances of this
  * contact being linked as those types are removed.
 *
 * @param array $aParams Array of parameters:
 * @param array $aParams['types'] Array of contact link_type codes that conflict with the new contact.
 * @param int $aParams['recordid'] ID of the contact record that has just been linked
 * @param int $aParams['main_recordid'] ID of the main record that the contact has been linked to.
 *
 */
function DeleteDuplicateContacts($aParams)
{
    global $ModuleDefs;

    if (!empty($aParams['types']))
    {
        $RecordIds = array();

        // need to collect up the IDs of all the contacts we're deleting in case there are additional details to clean up
        $sql = '
            SELECT
                con_id
            FROM
                link_contacts
            WHERE
                '.$ModuleDefs[$aParams['module']]['FK'].' = '.$aParams['main_recordid'].
                ' AND CON_ID = '.$aParams['recordid'].'
                AND link_type IN (\''.implode('\',\'', $aParams['types']).'\')';

        $result = db_query($sql);

        while ($row = db_fetch_array($result))
        {
            $ContactIds[] = $row['con_id'];
        }

        // delete the contact links
        $sql = '
            DELETE FROM
                link_contacts
            WHERE
                '.$ModuleDefs[$aParams['module']]['FK'].' = '.$aParams['main_recordid'].
                ' AND CON_ID = '.$aParams['recordid'].'
                AND link_type IN (\''.implode('\',\'', $aParams['types']).'\')';

        db_query($sql);

        //if we're in complaints and removing complainants, we'll need to clear out the link_compl table too.
        if ($aParams['module'] == 'COM' && !empty($ContactIds) && in_array('C', $aParams['types']))
        {
            $sql = '
                DELETE FROM
                    link_compl
                WHERE
                    con_id IN ('.implode(', ', $ContactIds).')
                    AND com_id = '.$aParams['main_recordid'];

            db_query($sql);
        }
    }
}

/**
* @desc Finds any contacts that cannot be linked to a particular record with a particular type since such a link
 * already exists.
*
* @param array $aParams Array of parameters.
* @param int $aParams['main_recordid'] The id of the record the contacts will be linked to.
* @param string $aParams['link_type'] The link_type value to check for.
* @param string $aParams['module'] The current module.
*
* @returns array An array of the contact record ids that cannot be re-linked.*
*/
function GetDisallowedContacts($aParams)
{
    global $ModuleDefs;

    //we ignore "other contacts" for now, since sometimes people will be linked with different roles.
    if ($aParams['link_type'] && $aParams['link_type'] != 'N' && $aParams['module'] && $aParams['main_recordid'])
    {
        //exception for complainants in complaints, since they are sometimes linked with link_type 'A'
        if ($aParams['module'] == 'COM' && $aParams['link_type'] == 'C')
        {
            $sql = '
                SELECT
                    con_id
                FROM
                    link_contacts
                WHERE
                    link_type = \''.$aParams['link_type'].'\'
                    AND '.$ModuleDefs[$aParams['module']]['FK'].' = '.$aParams['main_recordid'].'
            UNION
                SELECT link_contacts.con_id FROM link_contacts, link_compl
                WHERE link_contacts.con_id = link_compl.con_id
                AND link_contacts.com_id = link_compl.com_id
                AND link_contacts.link_type = \'A\'
                AND link_contacts.com_id = '.$aParams['main_recordid'].'
                AND link_compl.lcom_iscomplpat = \'Y\'';
        }
        elseif ($aParams['module'] == 'CLA' && $aParams['link_type'] == 'O')
        {
            // Gets individuals that are already linked as respondents
            $sql = '
                SELECT
                    con_id
                FROM
                    link_respondents
                WHERE
                    link_type = \'CON\' AND main_recordid = '.$aParams['main_recordid'];
        }
        else
        {
            $sql = '
                SELECT
                    con_id
                FROM
                    link_contacts
                WHERE
                    link_type = \''.$aParams['link_type'].'\'
                    AND '.$ModuleDefs[$aParams['module']]['FK'].' = '.$aParams['main_recordid'];
        }

        $result = db_query($sql);

        while ($row = db_fetch_array($result))
        {
            $resultSet[] = $row['con_id'];
        }

        return $resultSet;
    }
}
/**
 * @desc Finds any equipment that cannot be linked to a particular record with a particular type since such a link
 * already exists.
 *
 * @param int $mainRecordID ID of the main record to check links against.
 * @param string $module The module of the record being checked e.g. INC.
 *
 * @returns array An array of the equipment record ids that cannot be re-linked.*
 */
function GetDisallowedEquipment($mainRecordID, $module)
{
	global $ModuleDefs;
	
	$sql = 'SELECT
				AST_ID
			FROM
				LINK_ASSETS
			WHERE
				'.$ModuleDefs[$module]['FK'].' = '.$mainRecordID;
	
	$result = db_query($sql);
	
	while ($row = db_fetch_array($result))
	{
		$resultSet[] = $row['AST_ID'];
	}
	
	return $resultSet;
}

/**
* Validates contact form data.
*
* Calls various validation functions which each in turn validate a specific field/field type
* and returns any error messages generated.
* @param array $aParams['data'] The data posted on the form.
* @param int $aParams['suffix'] The identifier for this contact on the form.
* @return array $error Any error messages generated from the validation checks.
*/
function ValidateContactData($aParams)
{
    $DateError = ValidatePostedDates('CON', $aParams['suffix'], true);
    $MoneyError = ValidatePostedMoney('CON', $aParams['suffix']);

    $error = array_merge($DateError, $MoneyError);
    $error = ValidateNHSNumber($aParams, $error);
    return $error;
}

function ValidateInjuryData($aParams)
{
    $error = array();

    if (is_array($aParams['data']['injury'.$aParams['suffixstring']]))
    {
        foreach ($aParams['data']['injury'.$aParams['suffixstring']] as $id => $injury)
        {
            if ($injury == '' && $aParams['data']['bodypart'.$aParams['suffixstring']][$id] != '')
            {
                $error['injury'.$aParams['suffixstring']] = 'Please enter the injury that this body part relates to.';
            }
        }

    }

    return $error;
}

function MergeContact(&$ContactFromUser)
{
    global $FieldDefs;

    if ($ContactFromUser['con_number'] && $ContactFromUser['con_surname'] && bYN(GetParm('CONTACT_AUTO_APPROVE', 'Y')))
    {
        $sql = 'SELECT recordid, con_number, con_nhsno, con_police_number,
        con_surname, con_title, con_forenames,  con_gender, con_dob,
        con_dod, con_address,con_postcode, con_subtype, con_type,
        con_tel1, con_tel2, con_email,  con_jobtitle,
        con_ethnicity, con_disability, con_language, con_name, con_fax,
        con_orgcode , con_clingroup , con_directorate, con_specialty,
        con_unit, con_loctype, con_locactual,
        con_empl_grade , con_payroll, con_dopened , con_dclosed,
        con_notes, con_organisation
        FROM contacts_main
        WHERE con_number = :con_number
        AND con_surname = :con_surname
        AND rep_approved = :rep_approved
        ';

        $ContactFromDB = DatixDBQuery::PDO_fetch_all($sql, array(
            'con_number' => $ContactFromUser['con_number'],
            'con_surname' => $ContactFromUser['con_surname'],
            'rep_approved' => 'FA'
        ));

        if (!count($ContactFromDB) == 1) //only auto-match if there is a single contact record returned.
        {
            return false;
        }
        else
        {
            $ContactFromDB = $ContactFromDB[0];
        }

        foreach ($ContactFromDB as $key => $val)
        {
            if (!is_numeric($key))
            {
                if ($FieldDefs['CON'][$key]['Type'] == 'date')
                {
                    if ($val == null)
                    {
                        $ContactFromDB[$key] = 'NULL';
                    }
                    else
                    {
                        $val = FormatDateVal($val);
                    }
                }

                if ($ContactFromUser[$key] != '' && strtolower($ContactFromUser[$key]) != strtolower($val) && $val != '')
                {
                    if ($key != 'con_dopened') //con_dopened will always be different- don't let that stop the merge.
                    {
                        //non-null data present in DB conflicts with non-null data from user
                        return false;
                    }
                }
                elseif ($ContactFromUser[$key] == '' && $val != '')
                {
                    //non-conflicting data from DB - we don't actually need to re-save this.
                    if ($key == 'recordid')
                    {
                        $ContactToReturn[$key] = $ContactFromDB[$key];
                    }
                }
                elseif ($ContactFromUser[$key] != '' && $val == '')
                {
                    if($FieldDefs['CON'][$key]['Type'] == 'date')
                    {
                        $ContactToReturn[$key] = UserDateToSQLDate($ContactFromUser[$key]);
                    }
                    else
                    {
                        $ContactToReturn[$key] = $ContactFromUser[$key];
                    }
                }
            }
        }

        // can't just edit contactfromuser throughout in case it turns out not to match after it has been changed.
        $ContactFromUser = $ContactToReturn;

        $ContactFromUser['rep_approved'] = 'FA';

        return true;
    }

    return false;
}

function runExtraRecordDataFunctions($aParams)
{
    global $ModuleDefs;

    if (is_array($ModuleDefs[$aParams['module']]['EXTRA_RECORD_DATA_FUNCTION_INCLUDES']))
    {
        foreach ($ModuleDefs[$aParams['module']]['EXTRA_RECORD_DATA_FUNCTION_INCLUDES'] as $ExtraDataFunctionInclude)
        {
            require_once $ExtraDataFunctionInclude;
        }
    }

    if (is_array($ModuleDefs[$aParams['module']]['EXTRA_RECORD_DATA_SAVE_FUNCTIONS']))
    {
        foreach ($ModuleDefs[$aParams['module']]['EXTRA_RECORD_DATA_SAVE_FUNCTIONS'] as $Details)
        {
            eval('$Condition = ('.$Details['Condition'].');');

            if ($Condition)
            {
                $Details['Function']($aParams);
            }
        }
    }
}

/**
 * Calculates the number of calendar days that exist within a span of working days.
 * 
 * e.g. how many calendar days exist within the span of today + 10 working days.
 * 
 * @param int      $numDays The number of working days.
 * @param DateTime $from    The start date.
 * @param boolean  $reverse Whether we're counting backwards from the start date.
 * 
 * @return int $totalDays The number of calendar days.
 */
function CalculateWorkingDays($numDays, \DateTime $from = null, $reverse = false)
{
    $totalDays   = 0;
    $workingDays = 0;
    
    if ($from === null)
    {
        $currentDate = new DateTime();
    }
    else
    {
        $currentDate = clone $from;
    }
    
    // TODO the condition below will need to change to deal with
    //      countries where the weekend is not Sat/Sun (e.g. Saudi)
    if ($reverse && in_array($currentDate->format('w'), range(1,5))) // mon-fri
    {
        // count the from date as the first working day if we're counting backwards
        $workingDays = 1;
    }
    
    while ($workingDays < $numDays)
    {
        $currentDate->modify(($reverse ? '-' : '+').'1 day');

        if (!isHoliday($currentDate))
        {
            $workingDays++;
        }
        
        $totalDays++;
    }

    return $totalDays;
}

/**
 * Determines whether the provided date is a weekend or a public holiday.
 * 
 * @param \DateTime $date     The date we're interested in.
 * @param  Registry $registry The application registry, used to retrieve holiday definitions.
 * 
 * @return boolean
 */
function isHoliday(\DateTime $date, src\framework\registry\Registry $registry = null)
{
    if (in_array($date->format('w'), array(6,0))) // sat/sun
    {
        // TODO this will need to take locale into account to deal with
        //      countries where the weekend is not Sat/Sun (e.g. Saudi)
        return true;
    }
    else
    {
        $registry = $registry ?: src\framework\registry\Registry::getInstance();
        $holidays = $registry->getHolidays();
        
        return in_array($date->format('Y-m-d 00:00:00.000'), $holidays);
    }
}

function EnsureUniquePrimary($aParams)
{
    global $ModuleDefs;

    $sql = '
        UPDATE
            LINK_COMPL
        SET
            lcom_primary = \'N\'
        WHERE
            '.$ModuleDefs[$aParams['module']]['FK'].' = '.$aParams['main_recordid'].'
            AND con_id != '.$aParams['recordid'];

    db_query($sql);
}

function ContactHidden($Data, $Suffix, $Module = 'INC')
{
    global $SectionVisibility, $ModuleDefs;

    if ($Suffix >=6)
    {
        return !$SectionVisibility[$Data['contact_div_name_'.$Suffix]];
    }
    else
    {
        //loop through basicform - only ever needed for level1 forms
        include(GetBasicFormFileName($Module, 1));

        foreach ($FormArray as $section => $details)
        {
            if ($details['Special'] == 'DynamicContact' && $details['suffix'] && $details['suffix'] == $Suffix)
            {
                return !$SectionVisibility[$section];
            }
        }
    }

    return false;
}

/**
* Checks that the transfer of approval status is legitimate by querying the approval_action table.
*
* @param  string  $aParams['to']      The new approval status.
* @param  string  $aParams['from']    The old approval status.
* @param  string  $aParams['module']  The module code.
* @param  string  $aParams['perm']    The user permissions.
*
* @return boolean                     Whether or not the transfer is legitimate.
*/
function checkApprovalStatusTransferLegitimate($aParams)
{
    if (!is_array($aParams['perm']))
    {
        $aParams['perm'] = array($aParams['perm']);
    }

    $sql = 'SELECT count(*) as num
            FROM approval_action
            WHERE apac_to = :apac_to
            AND apac_from = :apac_from
            AND module = :module
            AND access_level in (\''.implode('\',\'', $aParams['perm']).'\')
            AND apac_workflow = :apac_workflow';

    $result = PDO_fetch(
        $sql,
        array(
            'apac_to' => $aParams['to'],
            'apac_from' => $aParams['from'],
            'module' => $aParams['module'],
            'apac_workflow' => GetWorkflowID($aParams['module'])
        ),
        PDO::FETCH_COLUMN
    );

    return (intval($result) > 0);
}

function getDefaultRepApprovedValue($aParams)
{
    if ($aParams['perms'] == '')
    {
        $aParams['perms'] = array('NONE');
    }
    else if (!is_array($aParams['perms']))
    {
        $aParams['perms'] = array($aParams['perms']);
    }

    $CurrentPerms = $aParams['perms'];

    if ($GLOBALS['DefaultValues']['rep_approved'] && $aParams['data']['rep_approved_old'] == '')
    {
        if (checkApprovalStatusTransferLegitimate(array(
        'to' => $GLOBALS['DefaultValues']['rep_approved'],
        'from' => ($aParams['data']['rep_approved_old'] ? $aParams['data']['rep_approved_old'] : 'NEW'),
        'perm' => $CurrentPerms,
        'module' => $aParams['module'],
        )))
        {
            $value = $GLOBALS['DefaultValues']['rep_approved'];
        }
    }

    if ($value == '' && $aParams['data']['rep_approved_old'])
    {
        if (checkApprovalStatusTransferLegitimate(array(
        'to' => $aParams['data']['rep_approved_old'],
        'from' => $aParams['data']['rep_approved_old'],
        'perm' => $CurrentPerms,
        'module' => $aParams['module'],
        )))
        {
            $value = $aParams['data']['rep_approved_old'];
        }
    }

    if ($value == '')
    {
        $ApproveArray = GetLevelsTo($aParams['module'], $CurrentPerms, 'NEW');

        if (count($ApproveArray))
        {
            foreach ($ApproveArray as $status => $desc)
            {
                if (!$value)
                {
                    $value = $status;
                }
            }
        }
    }

    if (!$value && $aParams['data']['module'] != 'SAB')
    {
        AddSessionMessage('ERROR', 'Warning: You may not be able to save this record due to a problem with the approval status workflow setup');
    }

    return $value;
}

/**
* @desc Checks whether the approval status change being made according to the $_POST
* array is valid or not. Returns a valid target approval status.
*
* @param array $aParams Array of Parameters
* @param string $aParams['module'] The current module
*
* @return string The code for a valid approval status.
*/
function GetValidApprovalStatusValueFromPOST($aParams)
{
    /*
    * rep_approved should always be passed in $_POST when saving a record. This lets us know where to save the record.
    * We need to check the value passed is valid before saving.
    */
    if (isset($_POST['rep_approved']) && $_POST['rep_approved'] != ''
        && checkApprovalStatusTransferLegitimate(array(
            'to' => Sanitize::SanitizeString($_POST['rep_approved']),
            'from' => Sanitize::SanitizeString($_POST['rep_approved_old']),
            'perm' => ($_SESSION['CurrentUser']? $_SESSION['CurrentUser']->getAccessLevels($aParams['module']) : 'NONE'),
            'module' => $aParams['module']
        )))
    {
        //valid approval status transition
        return Sanitize::SanitizeString($_POST['rep_approved']);
    }
    else
    {
        //invalid or missing value - needs to be set automatically;
        return getDefaultRepApprovedValue(array(
            'data' => Sanitize::SanitizeRawArray($_POST),
            'perms' => ($_SESSION['CurrentUser']? $_SESSION['CurrentUser']->getAccessLevels($aParams['module']) : 'NONE'),
            'module' => $aParams['module']
        ));
    }
}

function ContactNotFilledIn($Data, $Suffix)
{
    return  ($Data["con_title_$Suffix"] == ""
            && $Data["con_forenames_$Suffix"] == ""
            && $Data["con_surname_$Suffix"] == ""
            && $Data["con_gender_$Suffix"] == ""
            && $Data["con_dob_$Suffix"] == ""
            && $Data["con_dod_$Suffix"] == ""
            && $Data["con_address_$Suffix"] == ""
            && $Data["con_postcode_$Suffix"] == ""
            && $Data["con_tel1_$Suffix"] == ""
            && $Data["con_jobtitle_$Suffix"] == ""
            && $Data["con_email_$Suffix"] == ""
            && $Data["con_ethnicity_$Suffix"] == ""
            && $Data["con_disability_$Suffix"] == ""
            && $Data["con_language_$Suffix"] == ""
            && $Data["con_number_$Suffix"] == ""
            && $Data["con_nhsno_$Suffix"] == ""
            && $Data["link_notes_$Suffix"] == "");
}

function CreateWarningDlg($WarningMessage, $align = "center")
{
     echo '<script language="javascript" type="text/javascript">
            window.onload = function TriggerAlert()
            {
                AlertWindow = AddNewFloatingDiv(\'alert\');

    message = \'<div style="width:600px;text-align:' . $align . '"><b>';

    if (is_array($WarningMessage))
    {
        foreach ($WarningMessage as $Message)
        {
            echo Escape::EscapeEntities($Message) . '<br>';
        }
    }
    else
    {
         echo Escape::EscapeEntities($Message);
    }

    echo '</b><br><br></div>\';

    AlertWindow.setContents(message);
    AlertWindow.setTitle("Warning");

    var buttonHTML = \'\';
    var buttons = new Array();
    buttons.push({\'value\':\'Close\',\'onclick\':"var div = GetFloatingDiv(\'alert\');div.CloseFloatingControl()"});

    for(var i=0; i<buttons.length; i++)
    {
        buttonHTML += \'<input type="button" value="\'+buttons[i].value+\'" onclick="\'+buttons[i].onclick+\'" />\';
    }

    AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);

    AlertWindow.display();
          }

        </script>';
}

function IncludeFieldDefs()
{
    global $FieldDefs, $FieldDefsExtra, $devNoCache;

    if (empty($_SESSION["FieldDefs"]) || $devNoCache)
    {
        require_once 'Source/FieldDefs.php';
        $_SESSION["FieldDefs"] = $FieldDefs;
        $_SESSION["FieldDefsExtra"] = $FieldDefsExtra;
    }
    else
    {
        $FieldDefs = $_SESSION["FieldDefs"];
        $FieldDefsExtra = $_SESSION["FieldDefsExtra"];
    }
}

function ValidatePostedDates($module, $suffix = '', $conlink = false)
{
    global $FieldDefs;

    if ($suffix)
    {
        $SuffixText = '_'.$suffix;
    }

    $error = array();

    require_once 'Date.php';
    $Today = new \DateTime();
    if (bYN(GetParm('DETECT_TIMEZONES', 'N')) && isset($_SESSION['Timezone']))
    {
        $Today->setTimezone(new \DateTimeZone('UTC'));
    }

    $Today = new \Date($Today->format('Y-m-d H:i:s'));

    if (bYN(GetParm('DETECT_TIMEZONES', 'N')) && isset($_SESSION['Timezone']))
    {
        $Today->setTZbyID('Etc/GMT');
        $Today->convertTZbyID('Etc/GMT'.($_SESSION['Timezone'] < 0 ? '' : '+').$_SESSION['Timezone']);
    }

    $Today->setSecond(0);
    $Today->setMinute(0);
    $Today->setHour(0);

    $Dates = GetAllFieldsByType($module, 'date');

    //this is an unapologetic hack until we can put the link details into the 'CON' area of fielddefs.
    if ($conlink)
    {
        $LinkedDates = GetAllFieldsByType('INC', 'date');

        foreach ($LinkedDates as $id => $Date)
        {
            if (\UnicodeString::substr($Date, 0, 5) != 'link_')
            {
                unset($LinkedDates[$id]);
            }
        }
    }

    $Dates = SafelyMergeArrays(array($Dates, $LinkedDates));

    foreach ($Dates as $Field)
    {
        //this is an unapologetic hack until we can put the link details into the 'CON' area of fielddefs.
        $mod = $module;

        if ($conlink && \UnicodeString::substr($Field, 0, 5) == 'link_')
        {
            $mod = 'INC';
        }

        if ($FieldDefs[$mod][$Field]['NotFuture'])
        {
            // date cannot be in the future.
            if ($_POST[$Field.$SuffixText] != "")
            {
                $Date = new Date(UserDateToSQLDate($_POST[$Field.$SuffixText]));

                if (bYN(GetParm('DETECT_TIMEZONES', 'N')) && isset($_SESSION['Timezone']))
                {
                    $Date->setTZbyID('Etc/GMT'.($_SESSION['Timezone'] < 0 ? '' : '+').$_SESSION['Timezone']);
                }

                if ($Today->Compare($Date, $Today) == 1)
                {
                    $error[$Field.$SuffixText] = _tk("date_in_future_err");
                }
            }
        }

        if (is_array($FieldDefs[$mod][$Field]['NotEarlierThan']))
        {
            foreach ($FieldDefs[$mod][$Field]['NotEarlierThan'] as $EarlyDateField)
            {
                //Check closed date not before open date
                if ($_POST[$EarlyDateField.$SuffixText] != "" && $_POST[$Field.$SuffixText]!="")
                {
                    $EarlyDate = new Date(UserDateToSQLDate($_POST[$EarlyDateField.$SuffixText]));
                    $CheckDate = new Date(UserDateToSQLDate($_POST[$Field.$SuffixText]));

                    if (bYN(GetParm('DETECT_TIMEZONES', 'N')) && isset($_SESSION['Timezone']))
                    {
                        $EarlyDate->setTZbyID('Etc/GMT'.($_SESSION['Timezone'] < 0 ? '' : '+').$_SESSION['Timezone']);
                        $CheckDate->setTZbyID('Etc/GMT'.($_SESSION['Timezone'] < 0 ? '' : '+').$_SESSION['Timezone']);
                    }

                    if ($Today->Compare($CheckDate, $EarlyDate) == -1)
                    {
                        $error[$Field.$SuffixText] = _tk("date_earlier_than_err") . ' '  . GetFormFieldLabel($EarlyDateField, '', $mod);
                        $error[$EarlyDateField.$SuffixText] = _tk("date_later_than_err") . ' '  . GetFormFieldLabel($Field, '', $mod);
                    }
                }
            }
        }
    }

    return $error;
}

function ValidatePostedTimes($module, $suffix = '')
{
    global $FieldDefs;

    if ($suffix)
    {
        $SuffixText = '_'.$suffix;
    }

    $error = array();

    $Times = GetAllFieldsByType($module, 'time');

    foreach ($Times as $Field)
    {
        if ($FieldDefs[$module][$Field]['NotFuture'])
        {
            // time cannot be in the future.
            if ($_POST[$Field.$SuffixText] != "")
            {
                $Time = str_replace(':','',$_POST[$Field.$SuffixText]);
                $CurrentTime = date('Hi', time());

                if (intval($Time) > intval($CurrentTime))
                {
                    $error[$Field.$SuffixText] = _tk("time_in_future_err");
                }
            }
        }

        if (is_array($FieldDefs[$module][$Field]['NotEarlierThan']))
        {
            foreach ($FieldDefs[$module][$Field]['NotEarlierThan'] as $EarlyTimeField)
            {
                //Check time not before another time
                if ($_POST[$EarlyTimeField.$SuffixText] != "" && $_POST[$Field.$SuffixText]!="")
                {
                    $EarlyTime = str_replace(':','',$_POST[$EarlyTimeField.$SuffixText]);
                    $CheckTime = str_replace(':','',$_POST[$Field.$SuffixText]);

                    if (intval($EarlyTime) > intval($CheckTime))
                    {
                        $error[$Field.$SuffixText] = _tk("time_earlier_than_err") . ' '  . GetFormFieldLabel($EarlyTimeField, '', $module);
                    }
                }
            }
        }
    }

    return $error;
}

function ValidatePostedMoney($module, $suffix = '')
{
    global $FieldDefs;

    $SuffixText = '';
    if ($suffix)
    {
        $SuffixText = '_'.$suffix;
    }

    $error = array();

    $moneyFields = array_merge(getAllUdfFieldsByType('M', $SuffixText, $_POST), GetAllFieldsByType($module, 'money'));

    foreach ($moneyFields as $Field)
    {
        //These pay_type_ fields don't need validation. They're not real fields
        if(preg_match('/pay_type_/', $Field))
        {
            continue;
        }

        if (!$FieldDefs[$module][$Field]['Computed'])
        {
            if ($_POST[$Field.$SuffixText] &&
                !preg_match('/^-?('.preg_quote(GetParm('CURRENCY_CHAR', '£')).')?-?[0-9,\.]+$/u', $_POST[$Field.$SuffixText]))
            {
                $error[$Field.$SuffixText] = 'Invalid character found';
            }

            if (floatval(preg_replace('/[^0-9\.\-]/', '', $_POST[$Field.$SuffixText])) > 99999999999.99)
            {
                $error[$Field.$SuffixText] = _tk('large_money_err');
            }
        }
    }

    return $error;
}

function FormIDExists($ID, $module, $level = 1)
{
    $Filename = GetFormsFilename($module);

    if (file_exists($Filename))
    {
        include($Filename);

        if ($level == 1)
        {
            $FormArray = $FormFiles;
        }
        else
        {
            $FormArray = $FormFiles2;
        }

        if (isset($FormArray[$ID]))
        {
            return true;
        }

        return false;
    }
    else
    {
        return ($ID == 0);
    }
}

function RecordPermissions($data, $FormAction, $Module = "INC", $ExtraParams)
{
    require_once 'Source/security/SecurityRecordPerms.php';
    MakeRecordSecPanel('INC', 'incidents_main', $data['recordid'], $FormAction, $ExtraParams['type']);
}

/**
 * @desc Determines which buttons should be displayed at the base of the form, either using the workflows
 * (if approval buttons are switched on) or using a default set of buttons.
 *
 * @param array $aParams Array of parameters
 * @param string $aParams['module'] The current module
 * @param string $aParams['formtype'] The type of form being processed (Search, Design, Edit etc.)
 * @param array $aParams['data'] The record data
 *
 * @returns string HTML code describing buttons.
 */
function GetFormButtonsHTML($aParams)
{
    global $ModuleDefs, $OnSubmitJS;

    $aParams['perms'] = GetParm($ModuleDefs[$aParams['module']]['PERM_GLOBAL']);

    $HTML = '';

    if ($aParams['data']['rep_approved'] == 'REJECT' && $aParams['formtype'] != 'Search' &&
        (CanEditRecord($aParams) || CanMoveRecord($aParams)))
    {
        // not an ideal solution, but some proper work needs to be done on re-structuring the
        // formtype/formmode options available, since they are not currently particularly useful.
        $aParams['formtype'] = 'Edit';
    }

    switch ($aParams['formtype'])
    {
        case 'ReadOnly':
        case 'Locked':
            $HTML = '<input type="button" value="' . _tk('btn_cancel') . '" border="10" class="button" name="btnCancel" id="btnCancel"'
                . ' onClick="submitClicked=true;selectAllMultiCodes();document.forms[0].rbWhat.value=\'Cancel\';this.form.submit();">';
            break;
        case 'Search':
            $HTML = '<input type="button" value="' . _tk('btn_search') . '" border="10" class="button" name="btnSave" id="btnSave"'
                . ' onClick="submitClicked=true;selectAllMultiCodes();document.forms[0].rbWhat.value=\'Search\';this.form.submit();">'.

                '<input type="button" value="' . _tk('btn_cancel') . '" border="10" class="button" name="btnCancel" id="btnCancel"'
                . ' onClick="submitClicked=true;selectAllMultiCodes();document.forms[0].rbWhat.value=\'Cancel\';this.form.submit();">';
            break;
        case 'Print': // no buttons
            break;
        default:
            $HTML = '<input type="button" value="' . _tk('btn_save') . '" border="10" class="button" name="btnSave" id="btnSave"'
                . ' onClick="submitClicked=true;selectAllMultiCodes();if(validateOnSubmit() && RemoveDisabledFields())
            { document.forms[0].rbWhat.value=\'Save\';writeSuffixLimits(); ';

            if ($OnSubmitJS['btnSave'])
            {
                $HTML .= $OnSubmitJS['btnSave'];
            }

            $HTML .= ' this.form.submit();}">&nbsp;';

            if (!bYN(GetParm('DIF_1_NO_PRINT')) && !$_SESSION['logged_in'] && !\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
            {
                $HTML .= '<input type="button" value="'
                    . _tk('btn_submit_and_print_incident') . '" class="button" onclick="Javascript:$(\'printAfterSubmit\').value = 1;$(\'rbWhat\').value = \'Save\'; submitClicked=true;selectAllMultiCodes();if(validateOnSubmit()){writeSuffixLimits();this.form.submit();}" name="btnSubmitPrint" />&nbsp;&nbsp;';
            }

            $HTML .= '<input type="button" value="' . _tk('btn_cancel') . '" border="10" class="button" name="btnCancel" id="btnCancel"'
                . ' onClick="'.getConfirmCancelJavascript().'">';
            break;
    }

    if ($_GET['fromlisting'] == 1)
    {
        $HTML .= '<input type="button" value="'._tk('btn_back_to_report').'" border="10" class="button" name="btnBack" id="btnBack"'
            . ' onClick="submitClicked=true;selectAllMultiCodes();document.forms[0].rbWhat.value=\'BackToListing\';this.form.submit();">';
    }

    return $HTML;
}

/**
 * Renders any messages registered in the session as HTML and empties the message array
 * Duplicate messages are removed
 *
 * @return string Rendered HTML
 */
function GetSessionMessages()
{
    $HTML = '';

    if (is_array($_SESSION['MESSAGES']['ERROR']))
    {
        foreach (array_unique($_SESSION['MESSAGES']['ERROR']) as $Message)
        {
            $HTML .= '<div class="error_div">'.$Message.'</div>';
        }
    }

    if (is_array($_SESSION['MESSAGES']['INFO']))
    {
        foreach (array_unique($_SESSION['MESSAGES']['INFO']) as $Message)
        {
            $HTML .= '<div class="info_div">'.$Message.'</div>';
        }
    }

    if (is_array($_SESSION['MESSAGES']['POPUP']))
    {
        foreach (array_unique($_SESSION['MESSAGES']['POPUP']) as $Message)
        {
            $HTML .= '<script> jQuery(document).ready(function() { alert("'.$Message.'"); }); </script>';
        }
    }

    $_SESSION['MESSAGES']['ERROR'] = array();
    $_SESSION['MESSAGES']['INFO']  = array();
    $_SESSION['MESSAGES']['POPUP'] = array();

    return $HTML;
}

/**
 * Registers a message in the user session
 *
 * @param string $Type Either 'ERROR' or 'INFO'
 * @param string $Message The message
 * @return void
 */
function AddSessionMessage($Type, $Message)
{
    $_SESSION['MESSAGES'][$Type][] = $Message;
}

function AddValidationMessage($Field, $Message)
{
    $_SESSION['MESSAGES']['VALIDATION'][$Field][] = $Message;
}

function AddSectionMessage($Section, $Type, $Message)
{
    $_SESSION['MESSAGES']['SECTION'][$Section][$Type][] = $Message;
}

function GetSectionMessages($Section)
{
    $HTML = '';

    if (is_array($_SESSION['MESSAGES']['SECTION'][$Section]['ERROR']))
    {
        foreach ($_SESSION['MESSAGES']['SECTION'][$Section]['ERROR'] as $Message)
        {
            $HTML .= '<div class="error_div">'.$Message.'</div>';
        }
    }

    if (is_array($_SESSION['MESSAGES']['SECTION'][$Section]['INFO']))
    {
        foreach ($_SESSION['MESSAGES']['SECTION'][$Section]['INFO'] as $Message)
        {
            $HTML .= '<div class="info_div">'.$Message.'</div>';
        }
    }

    $_SESSION['MESSAGES']['SECTION'][$Section]['ERROR'] = array();
    $_SESSION['MESSAGES']['SECTION'][$Section]['INFO'] = array();

    return $HTML;
}

function ValidateWhereClause($Where, $module)
{
    global $ModuleDefs;

    // @PROMPT won't work on the test search,but should be fine on the real one.
    $Where = \UnicodeString::str_ireplace('@prompt', '', $Where);
    $Where = TranslateWhereCom($Where, "", $module, true);

//    try
//    {
//        $Where = TranslateWhereCom($Where, "", $module);
//    }
//    catch (Exception $e)
//    {
//        return false;
//    }

    //select from view if it exists
    $ModuleTable = ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']);

    $TestWHEREClause = "SELECT TOP 1 * FROM " . $ModuleTable . ($Where ? " WHERE " . $Where : '');

    return db_query($TestWHEREClause, true);
}

function ParseSaveData($aParams)
{
    global $FieldDefs;

    if (is_array($aParams['data']))
    {
        $aDateValues = CheckDatesFromArray(
            AddSuffixToFields(GetAllFieldsByType($aParams['module'], 'date'), $aParams['suffix']),
            $aParams['data']
        );
        $aMultiListValues = CheckMultiListsFromArray(
            AddSuffixToFields(GetAllFieldsByType($aParams['module'], 'multilistbox'), $aParams['suffix']),
            $aParams['data']
        );
        $aNumberValues = CheckNumbersFromArray(
            AddSuffixToFields(GetAllFieldsByType($aParams['module'], 'number'), $aParams['suffix']),
            $aParams['data']
        );
        $aDecimalValues = CheckDecimalsFromArray(
            AddSuffixToFields(GetAllFieldsByType($aParams['module'], 'decimal'), $aParams['suffix']),
            $aParams['data']
        );
        $aMoneyValues = CheckMoneyFromArray(
            AddSuffixToFields(GetAllFieldsByType($aParams['module'], 'money'), $aParams['suffix']),
            $aParams['data']
        );
        $aTextAreaValues = CheckTextAreaFromArray(
            array_merge(
                GetAllFieldsByType($aParams['module'], 'textarea'),
                getAllUdfFieldsByType('L', $aParams['suffix'], $aParams['data'])
            ),
            $aParams['suffix'],
            $aParams['data'],
            $aParams['form_design']
        );
        $aCheckboxesValues = CheckCheckboxesFromArray(
            AddSuffixToFields(GetAllFieldsByType($aParams['module'], 'checkbox'), $aParams['suffix']),
            $aParams['data'],
            $aParams['form_design']
        );

        $aParams['data'] = array_merge(
            $aParams['data'], $aMultiListValues, $aDateValues, $aNumberValues, $aDecimalValues, $aMoneyValues,
            $aTextAreaValues, $aCheckboxesValues
        );

        foreach ($FieldDefs[$aParams['module']] as $field => $details)
        {
            if ($details['UpperCase'] && isset($aParams['data'][$field]))
            {
                $aParams['data'][$field] = \UnicodeString::strtoupper($aParams['data'][$field]);
            }
        }
    }

    return $aParams['data'];
}

function GenericRejectionArray($module, $data)
{
    return array(
        'Condition' => (bYN(GetParm("REJECT_REASON",'Y'))),
        'Title' => "Details of Rejection",
        'NotModes' => array('New','Search'),
        'NoReadOnly' => true,
        "Rows" => array(
            array(
                'Name' => 'rea_dlogged',
                'Title' => 'Date Rejected',
                'ReadOnly' => true,
                'Type' => 'date',
                'Module' => $module
            ),
            array(
                'Name' => 'rea_con_name',
                'Title' => 'Rejected by',
                'ReadOnly' => true,
                'Type' => 'string',
                'Module' => $module
            ),
            array(
                'Name' => 'rea_code',
                'Title' => 'Reason for rejection',
                'EditableWhenRejected' => $data['ReasonSectionEditable'],
                'Type' => 'ff_select',
                'NoReadOnly' => true,
                'Module' => 'INC'
            ),
            array(
                'Name' => 'rea_text',
                'Title' => 'Further Details',
                'EditableWhenRejected' => $data['ReasonSectionEditable'],
                'Type' => 'textarea',
                "Rows" => 10,
                "Columns" => 70,
                'NoReadOnly' => true,
                'Module' => $module
            )
        )
    );
}

function GetComplaintDateFields()
{
    return array(
        'lcom_dreceived',
        'lcom_ddueack',
        'lcom_ddueact',
        'lcom_ddueresp',
        'lcom_dduehold',
        'lcom_dduerepl',
        'lcom_dack',
        'lcom_dactioned',
        'lcom_dresponse',
        'lcom_dholding',
        'lcom_dreplied',
        'lcom_dreopened'
    );
}

function TimeStampToSQLDateStr($time)
{
    if (GetParm("FMT_DATE_WEB") == "US")
    {
        $str = date('m/d/Y', $time);
    }
    else
    {
        $str = date('d/m/Y', $time);
    }

    return UserDateToSQLDate($str);
}

/**
 * Returns a SQL Server-formatted string representing today's date in the current timezone.
 * 
 * @param boolean $withoutTime Whether or not to omit the hours/mins/secs.
 * 
 * @return string
 */
function GetTodaysDate($withoutTime = false)
{
    require_once 'Date.php';
    $TodaysDate = new Date();

    if (bYN(GetParm('DETECT_TIMEZONES', 'N')) && isset($_SESSION['Timezone']))
    {
        $TodaysDate->convertTZbyID('Etc/GMT'.($_SESSION['Timezone'] < 0 ? '' : '+').$_SESSION['Timezone']);
    }

    $format = $withoutTime ? '%Y-%m-%d 00:00:00.000' : '%Y-%m-%d %H:%M:%S.000';

    return $TodaysDate->format($format);
}

/**
* @desc Returns a boolean describing whether or not the given module is licensed.
*
* @param string $module The module to check.
*
* @returns bool true if the module is licensed, false otherwise.
*/
function ModIsLicensed($module)
{
    global $ModuleDefs;

    switch($module)
    {
        case 'ACT':
        case 'TOD':
            $Licensed = (
                   $_SESSION["licensedModules"][MOD_INCIDENTS]
                || $_SESSION["licensedModules"][MOD_PALS]
                || $_SESSION["licensedModules"][MOD_RISKREGISTER]
                || $_SESSION["licensedModules"][MOD_CLAIMS]
                || $_SESSION["licensedModules"][MOD_COMPLAINTS]
                || $_SESSION["licensedModules"][MOD_SABS]
                || $_SESSION["licensedModules"][MOD_STANDARDS]
                || $_SESSION["licensedModules"][MOD_HOTSPOTS]
                || $_SESSION["licensedModules"][MOD_CQC_OUTCOMES]
                || $_SESSION["licensedModules"][MOD_CQC_PROMPTS]
                || $_SESSION["licensedModules"][MOD_CQC_SUBPROMPTS]
            );
            break;
        case 'CON':
            $Licensed = (
                $_SESSION["licensedModules"][MOD_INCIDENTS]
                    || $_SESSION["licensedModules"][MOD_PALS]
                    || $_SESSION["licensedModules"][MOD_RISKREGISTER]
                    || $_SESSION["licensedModules"][MOD_CLAIMS]
                    || $_SESSION["licensedModules"][MOD_COMPLAINTS]
                    || $_SESSION["licensedModules"][MOD_SABS]
            );
            break;
        case 'AST':
            $Licensed = (
                $_SESSION["licensedModules"][MOD_INCIDENTS]
                || $_SESSION["licensedModules"][MOD_RISKREGISTER]
            );
            break;
        case 'MED':
            $Licensed = ($_SESSION["licensedModules"][MOD_INCIDENTS] && bYN(GetParm('MULTI_MEDICATIONS', 'N')));
            break;
        case 'DST':
            $Licensed = ($_SESSION["licensedModules"][MOD_SABS]);
            break;
        case 'HSA':
            $Licensed = ($_SESSION["licensedModules"][MOD_HOTSPOTS]);
            break;
        case 'HOT':
            $Licensed = ($_SESSION["licensedModules"][MOD_HOTSPOTS]);
            break;
        case 'ELE':
        case 'PRO':
            $Licensed = ($_SESSION["licensedModules"][MOD_STANDARDS]);
            break;
        case 'CQC':
        case 'CQP':
        case 'CQS':
            $Licensed = ($_SESSION["licensedModules"][MOD_CQC_OUTCOMES]);
            break;
        case 'ACR':
        case 'ATM':
        case 'ATQ':
        case 'ATI':
        case 'AQU':
            $Licensed = ($_SESSION["licensedModules"][MOD_ASSESSMENT_MODULES]);
            break;
        case 'LIB':
            $Licensed = (($_SESSION["licensedModules"][MOD_LIBRARY]) ||
                ($_SESSION["licensedModules"][MOD_STANDARDS]) || ($_SESSION["licensedModules"][MOD_CQC_OUTCOMES]));
            break;
        case 'PAY':
            $Licensed = ($_SESSION["licensedModules"][MOD_CLAIMS] ||
                $_SESSION["licensedModules"][MOD_INCIDENTS] || $_SESSION["licensedModules"][MOD_COMPLAINTS]);
            break;
        case 'ADM':
            $Licensed = true;
            break;
        case 'LOC':
            $Licensed = ($_SESSION["licensedModules"][MOD_CQC_OUTCOMES] ||
                $_SESSION["licensedModules"][MOD_ASSESSMENT_MODULES]);
            break;
        case 'POL':
        case 'ORG':
            $Licensed = ($_SESSION["licensedModules"][MOD_CLAIMS]);
            break;
        default:
            $Licensed = $_SESSION["licensedModules"][$ModuleDefs[$module]['MOD_ID']];
            break;
    }

    return $Licensed;
}

function CanSeeModule($module)
{
    global $ModuleDefs;

    switch($module)
    {
        case 'ACT':
            $HasPerms = (GetParm($ModuleDefs[$module]['PERM_GLOBAL']) != '' &&
                GetParm($ModuleDefs[$module]['PERM_GLOBAL']) != 'ACT_INPUT_ONLY');
            break;
        case 'MED':
            $HasPerms = (GetParm($ModuleDefs[$module]['PERM_GLOBAL']) != '' && bYN(GetParm('MULTI_MEDICATIONS', 'N')));
            break;
        case 'ADM':
            //Don't show admin if not permitted or on a tablet device
            $HasPerms = (!(bYN(GetParm('WEB_NETWORK_LOGIN')) && !bYN(GetParm('RECORD_LOCKING')) && !IsSubAdmin()) && !\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet());
            break;
        default:
            $HasPerms = (GetParm($ModuleDefs[$module]['PERM_GLOBAL']) != '');
            break;
    }

    return $HasPerms;
}
/*
 * This is the function used for populating the session data that says what modules
 * are licensed
 */
function PutLicenceIntoSession()
{
    $_SESSION["licensedModules"] = getDatixWebLicensedModulesByNumber();
    $_SESSION["licensedModules"][MOD_ORGANISATIONS] = true;
}
/*
 * @return  array representing which modules are licensed, the modules referenced by number
 *          with a true or false value
 */
function getDatixWebLicensedModulesByNumber()
{
    global $yySetLocation, $scripturl;

    // check licensing - begin
    require_once 'externals/keygen/DatixLicenceKeyFactory.php';
    require_once 'externals/keygen/DatixLicenceKey120.php';
    require_once 'externals/keygen/DatixLicenceKey121.php';
    require_once 'externals/keygen/DatixLicenceKey122.php';
    require_once 'externals/keygen/DatixLicenceKey.php';
    require_once 'externals/keygen/DatixLicenceKey114.php';

    $LicenceKey = DatixLicenceKeyFactory::createLicenceKey(GetParm('LICENCE_KEY_WEB', "", true));
    $LicenceKey->SetLicenceKey(GetParm('LICENCE_KEY_WEB', "", true));
    $LicenceKey->SetClientName(GetParm('CLIENT', "", true));

    while (!$LicenceKey->validateLicenceKey())
    {
        AddSessionMessage('ERROR', _tk('err_licence_invalid'));
        $yySetLocation = $scripturl . '?action=licence&saveok=false&licenceOnLogin=true&invalid=1';
        redirectexit();
    }

    // check licensing - end
    $LicenceKey->DecodeKey();

    if ($LicenceKey->keyHasExpired())
    {
        AddSessionMessage('ERROR', _tk('err_licence_expired'));
        $yySetLocation = $scripturl . '?action=licence&saveok=false&licenceOnLogin=true&invalid=1';
        redirectexit();
    }

    if ($LicenceKey->validateVersion())
    {
        AddSessionMessage('ERROR', 'This licence key is only valid for version '.DatixLicenceKey::GetVersionLabel($LicenceKey->getVersion()));
        $yySetLocation = $scripturl . '?action=licence&saveok=false&licenceOnLogin=true&invalid=1';
        redirectexit();
    }

    return $LicenceKey->GetLicencedModules();
}

/*
 * @return  array representing which modules are licensed, the modules referenced by module code
 *          (ie 'INC') with a true or false value
 */
function getDatixWebLicensedModulesByModuleCode()
{
    $returnArray = array();
    $sql = 'SELECT mod_id, mod_module
            FROM modules';
    $moduleList = \DatixDBQuery::PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);

    $licensedModules = getDatixWebLicensedModulesByNumber();
    foreach ($licensedModules as $key => $val)
    {
        $returnArray[$moduleList[$key]] = $val;
    }

    return $returnArray;
}

function getModArray($Exclude = array(), $groupModules = false)
{
    global $ModuleDefs, $ModuleListOrder;

    $modOrder = array();
    $modArray = array();

    $Listorder = array_flip($ModuleListOrder);

    foreach ($ModuleDefs as $Module => $Details)
    {
        if (ModIsLicensed($Module) && !$Details['DUMMY_MODULE'] && !in_array($Module, $Exclude))
        {
            if ($groupModules && $Details['MODULE_GROUP'])
            {
                $Code = $Details['MODULE_GROUP'];
                $Name = GetModuleGroupName($Details['MODULE_GROUP']);

                //Grouped module should take list position of first module in group where possible
                if (!isset($Listorder[$Code]))
                {
                    if (isset($Listorder[$Details['CODE']]))
                    {
                        $Listorder[$Code] = $Listorder[$Details['CODE']];
                    }
                    else
                    {
                        $Listorder[$Code] = max($Listorder) + 1;
                    }
                }
            }
            else
            {
                $Code = $Details['CODE'];
                $Name = $Details['NAME'];

                if (!isset($Listorder[$Code]))
                {
                    $Listorder[$Code] = max($Listorder) + 1;
                }
            }

            $modArray[$Code] = $Name;
            $modOrder[$Code] = $Listorder[$Code];
        }
    }

    array_multisort($modOrder, $modArray);

    return $modArray;
}

/**
 * Some modules are collected into groups. This function defines those groups.
 * In the future this could be factored out into a metadata file/table, but there's not
 * obvious place for that right now.
 *
 * @return array Array of module group codes and names
 */
function GetModuleGroups()
{
    $ModuleGroups = array(
        'ACR' => _tk('accreditation'),
        'CQC' => _tk('CQCNamesTitle'),
        'STD' => _tk('STNNamesTitle')
    );

    return $ModuleGroups;
}

/**
 * Some modules are collected into groups. This function accesses the names for those groups.
 *
 * @param $moduleGroup the code for the module group being referenced (defined in AppVars)
 * @return the human readable name for the group of modules.
 */
function GetModuleGroupName($moduleGroup)
{
    $ModuleGroups = GetModuleGroups();

    return $ModuleGroups[$moduleGroup];
}

function getModuleDropdown($aParams)
{
    global $ModuleDefs;

    if (empty($aParams['not']) || !is_array($aParams))
    {
        $aParams['not'] = array();
    }

    $ModArray = getModArray(array(), $aParams['includeModuleGroups']);

    if (is_array($aParams['add']))
    {
        $ModArray = array_merge($aParams['add'], $ModArray);
    }

    if (!in_array($aParams['current'], array_keys($ModArray)))
    {
        $aParams['current'] = '';
    }

    foreach ($ModArray as $code => $Name)
    {
        if ($aParams['use_menu_names'] && $ModuleDefs[$code]['MENU_NAME'])
        {
            $Name = $ModuleDefs[$code]['MENU_NAME'];
        }

        if (in_array($code, $aParams['not']))
        {
            unset($ModArray[$code]);
        }
    }

    if ($aParams['include_choose'])
    {
        $TempModArray[''] = _tk('choose');
        $ModArray = array_merge($TempModArray, $ModArray);
    }

    $value = $aParams['current'] ?: array_keys($ModArray)[0];
    
    $field = Forms_SelectFieldFactory::createSelectField($aParams['name'], 'ADM', $value, '');
    $field->setCustomCodes($ModArray);
    $field->setSuppressCodeDisplay();
    
    if ($aParams['onchange'] != '')
    {
        $field->setOnChangeExtra($aParams['onchange']);
    }
    $field->setAddChangedFlag($aParams['setChangedFlag']);
    return $field->getField(false);
}

function getSecuritySettingJS($aParams = array())
{
    global $JSFunctions;

    $JS = '
            var currentModule = new Array();

            var ModuleList = [\''.implode('\', \'', array_merge(array_keys(getModArray()), array('ALL_MODULES', 'ACR'))).'\'];

            window.onload = function showFirstModuleSettings()
            {';

    if (!$aParams['not_parameters'])
    {
        $JS .= '
                ModuleList.each(function(s) {
                    if ($(s + \'_parameters\'))
                    {
                        $(s + \'_parameters\').style.display = "none";
                    }

                    if ($(s + \'_notifications\'))
                    {
                        $(s + \'_notifications\').style.display = "none";
                    }
                });

                if ($("parametersModule"))
                {
                    currentModule[\'parameters\'] = $("parametersModule").value;
                    $(currentModule[\'parameters\'] + \'_parameters\').style.display = "block";
                }';
    }

    $JS .= '
                try {
                    currentModule[\'security_settings\'] = document.getElementById("securitySettingsModule").value;
                    document.getElementById(currentModule[\'security_settings\'] + \'_security_settings\').style.display = "block";
                }
                catch(e) {
                    // Do nothing
                }
            }';

    $JSFunctions[] = $JS;

    $JSFunctions[] = '
            function showModuleSection(module, sectionType)
            {
                if (currentModule[sectionType] != module)
                {
                    document.getElementById(currentModule[sectionType] + \'_\' + sectionType).style.display = "none";

                    if (currentModule[sectionType] == "ACR")
                    {
                        if (document.getElementById(currentModule[sectionType] + \'_notifications\'))
                        {
                            document.getElementById(currentModule[sectionType] + \'_notifications\').style.display = "none";
                        }
                    }
                }

                var div = document.getElementById(module + \'_\' + sectionType);
                div.style.display = "block";
                currentModule[sectionType] = module;

                if (module == "ACR")
                {
                    if (document.getElementById(module + \'_notifications\'))
                    {
                        document.getElementById(module + \'_notifications\').style.display = "block";
                    }
                }
            }';
}

function getUserSecTitleDropdown()
{
    $ExcludeArray = array('DST', 'DAS', 'ELE', 'PRO', 'CQP', 'CQS', 'ATM', 'ATQ', 'ATI', 'AQU', 'AMO', 'TOD', 'ORG');

    if (!IsFullAdmin())
    {
        $ExcludeArray[] = 'ADM';
    }

    //This should be refactored to use module groups.
    $AdditionalArray = array('ALL_MODULES' => 'All Modules', 'ACR' => _tk('accreditation'));

    $HTML = getModuleDropdown(array(
        'name' => 'parametersModule',
        'onchange' => 'showModuleSection(jQuery(\'#parametersModule\').val(), \'parameters\')',
        'current' => 'ALL',
        'not' => $ExcludeArray,
        'add' => $AdditionalArray,
        'use_menu_names' => true
    ));

    getSecuritySettingJS();

    return $HTML;
}

/**
* @desc Finds the appropriate comparison field for a given module and calls {@link getAgeAtDate} to work out the
 * age of the person at the date of a given record.
*
* @param string $module The current module
* @param string $mainrecid The recordid of the record in question
* @param string $dob The date of birth of the person in question
*
* @return int The person's age at the given date.
*
*/
function getAgeAtDateForModule($module, $mainrecid, $dob, $link_age)
{
    global $ModuleDefs;

    if (!empty($ModuleDefs[$module]['AGE_AT_DATE']))
    {
        $sql = "SELECT " . $ModuleDefs[$module]['AGE_AT_DATE'] .
            " FROM " . $ModuleDefs[$module]['TABLE'] .
            " WHERE recordid = :recordid";

        $baseDate = DatixDBQuery::PDO_fetch($sql, array('recordid' => $mainrecid), PDO::FETCH_COLUMN);

        if (!empty($baseDate) && !empty($dob))
        {
            $link_age = getAgeAtDate($dob,$baseDate);
        }
    }

    return $link_age;
}

function getAgeBand($link_age)
{
    $sql = 'SELECT TOP 1 code FROM code_age_band WHERE lower_limit <= :age1 AND upper_limit >= :age2';
    return DatixDBQuery::PDO_fetch($sql, array('age1' => $link_age, 'age2' => $link_age), PDO::FETCH_COLUMN);
}

function GetFormLevel($Module, $Perms, $ApprovalStatus)
{
    $sql = 'SELECT las_form_level FROM link_access_approvalstatus WHERE
            access_level = \''.$Perms.'\' AND
            module = \''.$Module.'\' AND
            code = \''.$ApprovalStatus.'\' AND
            las_workflow = '.GetWorkflowID($Module);

    $row = db_fetch_array(db_query($sql));

    return ($row['las_form_level'] ? $row['las_form_level'] : 2);
}

function GetActiveStaffWhereClause()
{
    require_once 'Date.php';

    $TodaysDate = new Date;
    $todayStr = $TodaysDate->getDate();

    $Where = "
        (sta_dclosed is null OR sta_dclosed > '$todayStr')
        AND (sta_dopened is null OR sta_dopened < '$todayStr')";

    return $Where;
}

function GetReportsAdminMenuArray()
{
    return array(
        array('label' => 'Design reports', 'link' => 'action=reportsadminlist'),
        array('label' => 'Reporting fields setup', 'link' => 'action=excludefields'),
    );
}

function getPageTitleHTML($aParams, $addAsHeading = true)
{
    global $ModuleDefs, $dtxheading, $JSFunctions;

    $HTML = '
        <div class="heading">
            <div><h1>';

    if ($aParams['image'] && file_exists($aParams['image']))
    {
        $HTML .= '
            <img align="middle" alt="" src="'.$aParams['image'].'" border="0" style="float:left;" />';
    }
    elseif ($aParams['module'] && $ModuleDefs[$aParams['module']]['ICON'])
    {
        $HTML .= '
            <img align="middle" alt="" src="Images/'.$ModuleDefs[$aParams['module']]['ICON'].'" border="0" style="float:left;" />';
    }

    $HTML .= '
            <span style="padding:1px">'.$aParams['title'].'</span></h1>';

    if ($aParams['subtitle'])
    {
        $HTML .= '
            <div class="page_subtitle">'.$aParams['subtitle'].'</div>';
    }

    if ($aParams['record_info'])
    {
        $HTML .= '
            <div class="clearfix"></div>
            <div id="record-info">'.$aParams['record_info'].'</div>';
    }

    $HTML .= '</div>';

    //Add "flag" option
    if ($_SESSION['CON']['RECORDLIST'] && $_SESSION['CON']['DUPLICATE_SEARCH'] && $aParams['formtype'] != 'Print')
    {
        $CurrentIndex = $_SESSION['CON']['RECORDLIST']->getRecordIndex(array(
            'recordid' => $aParams['data']['recordid']
        ));

        if ($CurrentIndex !== false)
        {
            $HTML .= '<div class="flag-btn">Flag as duplicate';

            $HTML .= '
                <img align="middle" style="cursor:pointer" src="Images/flag_filled_record.png" id="flag_filled" onclick="UnFlagRecord(\'CON\', '.$aParams['data']['recordid'].');jQuery(\'#flag_filled\').hide();jQuery(\'#flag_unfilled\').show();">
                <img align="middle" style="cursor:pointer" src="Images/flag_unfilled_record.png" id="flag_unfilled" onclick="FlagRecord(\'CON\', '.$aParams['data']['recordid'].');jQuery(\'#flag_unfilled\').hide();jQuery(\'#flag_filled\').show();">';

            if ($_SESSION['CON']['RECORDLIST']->FlaggedRecords[$CurrentIndex])
            {
                $JSFunctions[] = 'jQuery(\'#flag_unfilled\').hide();';
            }
            else
            {
                $JSFunctions[] = 'jQuery(\'#flag_filled\').hide();';
            }

            $HTML .= '</div>';
        }
    }

    $HTML .= '
            <div class="clearfix"></div>
        </div>';

    if($addAsHeading){ $dtxheading = $HTML; }

    return $HTML;
}

function unique_filename($xtn = ".tmp")
{
    // explode the IP of the remote client into four parts
    $ipbits = explode(".", Sanitize::SanitizeRaw($_SERVER["REMOTE_ADDR"]));
    // Get both seconds and microseconds parts of the time
    list($usec, $sec) = explode(" ",microtime());

    // Fudge the time we just got to create two 16 bit words
    $usec = (integer) ($usec * 65536);
    $sec = ((integer) $sec) & 0xFFFF;

    // Convert the remote client's IP into a 32 bit
    // hex number then tag on the time.
    // Result of this operation looks like this xxxxxxxx-xxxx-xxxx
    $uid = sprintf("%08x-%04x-%04x",($ipbits[0] << 24)
         | ($ipbits[1] << 16)
         | ($ipbits[2] << 8)
         | $ipbits[3], $sec, $usec);

    // Tag on the extension and return the filename
    return $uid.$xtn;
}

function checkGeneralMigrationNeeded()
{
    if (skipMigration())
    {
        return false;
    }
    
    if (CompareVersions(GetVersion(), GetParm('FORM_MIGRATE_VERSION')))
    {
        //the current version is later than the last version migrated.
        return true;
    }

    return false;
}

/**
 * Defines specific revisions which should not run the form migrate.
 * 
 * @return boolean
 */
function skipMigration()
{
    $skip = array(
        array('from' => '12.1', 'to' => '12.1.1')    
    );
    
    foreach ($skip as $revision)
    {
        if ($revision['from'] == GetParm('FORM_MIGRATE_VERSION') && $revision['to'] == GetVersion())
        {
            return true;
        }
    }

    return false;
}

//compares two version numbers of the form x.x.x.x...
//if versionA is a later version than versionB, returns true, otherwise false
function CompareVersions($VersionA, $VersionB)
{
    $VersionAArray = explode('.', $VersionA);
    $VersionBArray = explode('.', $VersionB);

    while(count($VersionAArray) > count($VersionBArray))
    {
        $VersionBArray[] = "0";
    }
    while(count($VersionBArray) > count($VersionAArray))
    {
        $VersionAArray[] = "0";
    }

    foreach ($VersionAArray as $index => $number)
    {
        if (intval($VersionAArray[$index]) > intval($VersionBArray[$index]))
        {
            return true;
        }

        if (intval($VersionAArray[$index]) < intval($VersionBArray[$index]))
        {
            return false;
        }
    }

    return false;
}

function getRecordHash($recordid)
{
    return hash("sha512", 'LEVEL1RECORDID'.$recordid);
}

function HashesMatch($module, $recordid)
{
    return ($_SESSION[$module]['LOGGEDOUTRECORDHASH'] == getRecordHash($recordid));
}

function PopulateLevel1FormFields($aParams)
{
    $data = $aParams['data'];
    $module = $aParams['module'];

    if ($module == 'INC')
    {
        if ($data["show_equipment"] == "")
        {
            $data["show_equipment"] = ($data["inc_eqpt_type"] != "" || $data["inc_manufacturer"] != "" || $data["inc_serialno"] != "" || $data["inc_defect"] != "");
        }

        if ($data["show_medication"] == "")
        {
            $data["show_medication"] = ($data["inc_med_stage"] != "" || $data["inc_med_error"] != "" || $data["inc_med_drug"] != "" || $data["inc_med_drug_rt"] != "" || $data["inc_med_form"] != "" || $data["inc_med_form_rt"] != "" || $data["inc_med_dose"] != "" || $data["inc_med_dose_rt"] != "" || $data["inc_med_route"] != "" || $data["inc_med_route_rt"] != "");
        }

        if ($data["show_pars"] == "")
        {
            $data["show_pars"] = ($data["inc_agg_issues"] != "" || $data["inc_user_action"] != "" || $data["inc_pol_crime_no"] != "" || $data["inc_pol_called"] != "" || $data["inc_pol_call_time"] != "" || $data["inc_pol_attend"] != "" || $data["inc_pol_att_time"] != "" || $data["inc_pol_action"] != "" || $data["inc_pars_pri_type"] != "" || $data["inc_pars_sec_type"] != "" || $data["inc_pars_clinical"] != "" || $data["inc_pars_address"] != "" || $data["inc_postcode"] != "");
        }

        if ($data["show_person"] == "")
        {
            $data["show_person"] = (!empty($data['con']['A']));
        }

        if ($data["show_witness"] == "")
        {
            $data["show_witness"] = (!empty($data['con']['W']));
        }

        if ($data["show_employee"] == "")
        {
            $data["show_employee"] = (!empty($data['con']['E']));
        }

        //logic for this section is slightly different, since police, assailant etc may have ended up in here on save
        //so might need to be shown even if unticked on form
        $data["show_other_contacts"] = (!empty($data['con']['N']));

        if ($data["inc_report_npsa"] == "")
        {
            $data["inc_report_npsa"] = ($data["inc_n_effect"] != "" || $data["inc_n_patharm"] != "" || $data["inc_n_nearmiss"] != "" ||$data["inc_n_actmin"] != "" || $data["inc_n_paedward"] != "" || $data["inc_n_paedspec"] != "" || $data["inc_dnpsa"] != "");
        }

        if ($data["show_assailant"] == "")
        {
            $data["show_assailant"] = ($data["con_title_4"] != "" || $data["con_forenames_4"] != "" || $data["con_surname_4"] != "" || $data["con_nhsno_4"] != "" || $data["con_dob_4"] != "" || $data["con_address_4"] != "" || $data["con_postcode_4"] != "" || $data["con_tel1_4"] != "" || $data["con_subtype_4"] != "" || $data["con_gender_4"] != "" || $data["con_number_4"] != "" || $data["con_jobtitle_4"] != "" || $data["con_ethnicity_4"] != "");
        }
    }
    elseif ($module=='RAM')
    {
        if ($data["show_contacts_RAM"] == "")
        {
            $data["show_contacts_RAM"] = (!empty($data['con']['N']));
        }
    }
    elseif ($module=='PAL')
    {
        if ($data["show_other_contacts"] == "")
        {
            $data["show_other_contacts"] = (!empty($data['con']['N']));
        }
    }
    elseif ($module == 'CLA')
    {
        if ($data["show_employee"] == "")
        {
            $data["show_employee"] = (!empty($data['con']['E']));
        }

        if ($data["show_other_contacts"] == "")
        {
            $data["show_other_contacts"] = (!empty($data['con']['N']));
        }
    }

    return $data;
}

function GetDefaultFromTo($string)
{
    $FromToStrings = explode(',', $string);
    $FinalArray = array();

    if (is_array($FromToStrings))
    {
        foreach ($FromToStrings as $FromToString)
        {
            $FromToArray = explode('-', $FromToString);
            $FinalArray[] = array('from'=>$FromToArray[0], 'to' => $FromToArray[1]);
        }
    }

    return $FinalArray;
}

function ParseHTMLTableForExport($html, $output_format, $pdf = null)
{
    libxml_use_internal_errors(true); // required to prevent IIS throwing a wobbly on some installations when loading invalid HTML markup
    
    $html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>'.$html.'</html>';

    $doc = new DOMDocument('1.0', 'utf-8');
    @$doc->loadHTML($html);
    $telements = $doc->getElementsByTagName('table');

    // remove attributes from tables
    foreach ($telements as $telement)
    {
        $telement->removeAttribute('class');
        $telement->removeAttribute('border');
        $telement->removeAttribute('width');
        $telement->removeAttribute('bgcolor');
        $telement->setAttribute('border', '1');

        if ($output_format == 'pdf')
        {
            $telement->removeAttribute('width');
            $telement->setAttribute('width', $pdf->width);
        }
    }

    if ($_REQUEST['exportmode'] == 'listing')
    {
        // deal with cell widths
        $thelements = $doc->getElementsByTagName('th');
        $lineWidth = 0;

        foreach ($thelements as $thelement)
        {
            if ($thelement->getAttribute('excelwidth') != '')
            {
                $lineWidth += $thelement->getAttribute('excelwidth');
            }
        }

        foreach ($thelements as $thelement)
        {
            if ($_REQUEST['report'] == 'report6' || $_REQUEST['report'] == 'report5')
            {
                $thelement->setAttribute('bgcolor', '#d2e4fc');
            }
            else
            {
                $thelement->setAttribute('bgcolor', '#162D54');

                if ($output_format == 'pdf')
                {
                    $thelement->setAttribute('color', '#FFFFFF');
                }
                else
                {
                    $thelement->setAttribute('style', 'color: #FFFFFF;');
                }
            }

            $excelwidth = $thelement->getAttribute('excelwidth');

            if ($output_format == 'excel' && $excelwidth != '' && $excelwidth > 0)
            {
                $thelement->setAttribute('width', round($excelwidth * 10 / $lineWidth * 100 + 20));
                $thelement->removeAttribute('excelwidth');
            }
        }
    }

    $tdelements = $doc->getElementsByTagName('td');

    foreach ($tdelements as $tdelement)
    {
        // some colouring
        if ($_REQUEST['exportmode']=='listing')
        {
            if ($tdelement->getAttribute('class')=='titlebg2')
            {
                $tdelement->setAttribute('bgcolor', '#6E94B7');

                if ($output_format == 'pdf')
                {
                    $tdelement->setAttribute('color', '#FFFFFF');
                }
                else
                {
                    $tdelement->setAttribute('style', 'color: #FFFFFF;');
                }
            }
        }

        if ($_REQUEST['exportmode']=='crosstab' && ($tdelement->getAttribute('class')=='crosstab header' ||
            $tdelement->getAttribute('class')=='crosstab header top-header'))
        {
            $tdelement->setAttribute('bgcolor', '#162D54');

            if ($output_format == 'pdf')
            {
                $tdelement->setAttribute('color', '#FFFFFF');
            }
            else
            {
                $felement = $tdelement->firstChild;

                if ($felement != null)
                {
                    $fvalue = $felement->textContent;
                    $fontelement = $doc->createElement("font");
                    $fontelement->setAttribute('color', '#FFFFFF');
                    $newelement = $doc->createTextNode($fvalue);
                    $fontelement->appendChild($newelement);
                    $tdelement->replaceChild($fontelement, $felement);
                }
            }
        }

        if ($_REQUEST['exportmode']=='crosstab' && $tdelement->getAttribute('class')=='crosstab total data')
        {
            $tdelement->setAttribute('bgcolor', '#afc6db');
        }

        $tdelement->removeAttribute('onmouseover');
        $tdelement->removeAttribute('onmouseout');
        $tdelement->removeAttribute('onclick');
        $tdelement->removeAttribute('onClick');

        if ($tdelement->hasChildNodes())
        {
            $aelement = $tdelement->firstChild;

            if ($aelement->nodeName == 'a')
            {
                // This deals with every type of content that we have in cells specially progress notes
                $children = $aelement->childNodes;

                $newelement = $doc->createElement('span');

                foreach ($children as $child)
                {
                    if ($child->nodeName == 'br')
                    {
                        $newelement->appendChild($doc->createElement('br'));
                    }
                    else
                    {
                        $newelement->appendChild($doc->createTextNode($child->nodeValue));
                    }
                }

                $tdelement->replaceChild($newelement, $aelement);
            }
        }

        if ($output_format == 'pdf')
        {
            // truncate text to 1024 chars
            foreach ($tdelement->childNodes as $node)
            {
                if ($node->nodeName == '#text' && \UnicodeString::strlen($node->nodeValue) > 1024)
                {
                    $node->nodeValue = \UnicodeString::substr($node->nodeValue, 0, 1024) . '...';
                }
            }
        }
    }

    return $doc->saveHTML();
}

function OutputPDFTable($html, $mode, $orientation, $paperSize, PDFTable $pdf)
{
    // PDFTable doesn't seem to like th elements, so convert to td
    $html = str_replace('<th', '<td', $html);
    $html = str_replace('</th>', '</td>', $html);
    //Remove blue background on PDF printouts
    $html = str_replace('bgcolor="#E3EFFF"', '', $html);

    //Fix encoded symbols
    $html = str_replace('&amp;', '&', $html);

    // Replace UTF-8 pound code for ASCII because the font used to generate the PDF doesn't support UTF-8
    $html = str_replace('£', chr(163), $html);

    if ($mode == 'crosstab')
    {
        // Remove portion of javascript from the PDF output
        // This is being added in the WriteCrosstabReport.php view file
        $html = substr($html, 0, strpos($html, '<script'));
        $html .= '</body></html>';
    }

    $doc = new DOMDocument('1.0', 'utf-8');
    @$doc->loadHTML($html);

    $body = $doc->documentElement->getElementsByTagName('body')->item(0);
    $reportTable = $doc->getElementById($mode . 'table');

    // get table attributes
    $cellspacing = $reportTable->getAttribute('cellspacing');
    $cellpadding = $reportTable->getAttribute('cellpadding');
    $border = $reportTable->getAttribute('border');
    $width = $reportTable->getAttribute('width');
    $id = $mode . 'table';

    // count the number of rows in the report table
    $totalRows = 0;
    foreach ($reportTable->childNodes as $node)
    {
        if ($node->nodeName == 'tr')
        {
            $totalRows++;
        }
    }

    // count the number of columns in the report table
    $totalCols = 0;

    foreach ($reportTable->firstChild->childNodes as $node)
    {
        if ($node->nodeName == 'td')
        {
            $totalCols++;
        }
    }
    
    $pdf->AddPage($orientation, $paperSize);
    $pdf->htmltable($html);

    return $pdf;
}

function AddSuffixToFields($aFields, $suffix)
{
    $aFieldsWithSuffix = array();

    foreach ($aFields as $sFieldName)
    {
        if ($suffix != "")
        {
            $sFieldName = $sFieldName . $suffix;
        }

        $aFieldsWithSuffix[] = $sFieldName;
    }

    return $aFieldsWithSuffix;
}

function StripNonAlphabet($String)
{
    return preg_replace('/[^a-zA-Z]/u','',$String);
}

//very basic code to strip out HTML tags - we may need to extend this in the future if more complex situations arise.
function StripHTML($String)
{
    return preg_replace('/\<[^\<\>]+\>/u', '', $String);
}

function ConstructInitials($aParams)
{
    if (!$aParams['data'])
    {
        $sql = 'SELECT con_forenames, con_surname, initials FROM contacts_main WHERE recordid = :recordid';
        $aParams['data'] = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $aParams['recordid']));
    }

    $Names = explode(' ', $aParams['data']['con_forenames']);
    array_splice($Names, 3); //make sure the array does not end up too long for the db
    $Names[] = $aParams['data']['con_surname'];

    $Initials = $aParams['data']['initials'];

    if (!$Initials)
    {
        foreach ($Names as $Name)
        {
            $Name = StripNonAlphabet($Name);
            $Initials .= \UnicodeString::strtoupper(\UnicodeString::substr($Name,0,1));
        }
    }

    $UnusedInitials = false;
    $InitialSuffix = 1;
    $FinalInitials = $Initials;

    while ($FinalInitials == '' || !$UnusedInitials)
    {
        if (CheckUniqueInitials($FinalInitials))
        {
            $UnusedInitials = true;
        }
        else
        {
            $FinalInitials = $Initials.$InitialSuffix;
            $InitialSuffix++;
        }
    }

    return $FinalInitials;
}

function CheckUniqueInitials($Initials)
{
    if (!$Initials)
    {
        return false;
    }

    $sql = 'SELECT count(*) as num FROM contacts_main WHERE initials = :initials';

    $matches = DatixDBQuery::PDO_fetch($sql, array('initials' => $Initials));

    return ($matches['num'] == 0);
}

function GetUserAndDomain($LoginAndDomain)
{
    $LoginArray = explode('\\', $LoginAndDomain);

    if ($LoginArray[1] != '')
    {
        $Domain = $LoginArray[0];
        $User = $LoginArray[1];
    }
    else
    {
        $User = $LoginArray[0];
    }

    return array('user'=>$User, 'domain' => $Domain);
}

// GetLoginMessageControl() generate the HTML for the control used when designing
// the alert messages to display when logging in.
function GetLoginMessageControl($Field, $ButtonText)
{
    $Messages = GetMessages($Field);

    $HTML = '<div id="'.$Field.'Div">';

    foreach ($Messages as $id => $Message)
    {
        $FieldName = $Field.'_'.$id;
        $HTML .= '<div><textarea rows="6" cols="60" id="'.$FieldName.'" name="'.$FieldName.'" >'.$Message.'</textarea></div>';
    }

    $HTML .= '</div>
    <input type="hidden" id="max'.$Field.'Id" name="max'.$Field.'Id" value="'.(count($Messages)-1).'" />
    <div><input id="NewMessageBtn" name="NewMessageBtn" type="button" value="'.$ButtonText.'" onclick="AddNewMessageBox(\''.$Field.'\')" /></div>';

    return $HTML;
}

/**
* @desc Checks the database and file versions against the current session. If either differ, then we need to
* clear the session out, since values stored there may be out of date.
*/
function CheckSessionVersion()
{
    global $dtxversion;

    $SessionReset = false;

    if (!isset($_SESSION['DATIX_FILE_VERSION']) || $_SESSION['DATIX_FILE_VERSION'] !== $dtxversion)
    {
        $SessionReset = true;
    }

    $Version = GetVersion();

    if (!isset($_SESSION['DATIX_DATABASE_VERSION']) || $_SESSION['DATIX_DATABASE_VERSION'] !== $Version)
    {
        $SessionReset = true;
    }

    if ($SessionReset)
    {
        session_start();

        // save token before destroying session
        $csrf_token = \CSRFGuard::getCurrentToken();

        session_unset();

        // now restore it
        \CSRFGuard::setToken($csrf_token);

        $_SESSION['DATIX_DATABASE_VERSION'] = $Version;
        $_SESSION['DATIX_FILE_VERSION'] = $dtxversion;
    }
}

// If SSO global is set, checks that the SSO configuration file contains the
// correct information.
function ValidateSSO()
{
    if (SSOConfigError())
    {
        template_header();
        echo '<div class="info_div">'._tk('sso_error_message').'</div>';
        footer();
        obExit();
    }
}

function SSOConfigError()
{
    global $UserSSOSettingsFile;

    if (bYN(GetParm('VERIFY_SSO_LOGIN', 'N')) && bYN(GetParm('WEB_NETWORK_LOGIN','N')))
    {
        if (!$UserSSOSettingsFile || !file_exists($UserSSOSettingsFile))
        {
            $ConfigError = true;
        }
        else
        {
            include($UserSSOSettingsFile);

            $UserAndDomain = GetUserAndDomain($LoginAndDomain);

            if ($UserAndDomain['user'] == '' || $UserArray['UserID'] == '')
            {
                $ConfigError = true;
            }
        }
    }

    return $ConfigError;
}

function SetFormSession(&$session_form, &$lastsession)
{
    if (empty($_SESSION["lastsession"]))
    {
        $_SESSION["lastsession"] = array();
    }

    $lastsession = date('d-m-Y H:i:s');
    $session_form = uniqid();
    $_SESSION["lastsession"][$session_form] = $lastsession;
}

function ResetFormSession()
{
    unset($_SESSION["lastsession"][$_POST["session_form"]]);

    if (count($_SESSION["lastsession"]) == 0)
    {
        //Always need one entry to prevent refresh re-submit. Empty variable still submits due to failsafe
        // if php session deleted.
        $_SESSION["lastsession"][$_POST["session_form"]] = date('d-M-Y H:i:s');
    }
}

function CheckForModuleExtraWhere($module)
{
    $SystemModuleWhereClause = "";

    switch ($module)
    {
        case "ACT":
            // Only list actions for modules which are licensed
            $LicModules = getActionLinkModules();

            if (empty($LicModules) === false)
            {
                $SystemModuleWhereClause = "(act_module IN ('" . implode("', '", getActionLinkModules()) . "'))";
            }

            break;
    }

    return $SystemModuleWhereClause;
}

function ReplaceAllEntityCodes($xmltext)
{
    return htmlspecialchars($xmltext, ENT_QUOTES);
}
/*
perform a sort on a 2D array
you can optionally specify the key to sort on and the sort method
*/
function sort2d($arrIn, $index = null, $sortMethod = 'asort')
{
   $arrTemp = array();
   $arrOut = array();

   foreach ($arrIn as $key => $value)
   {
       reset($value);
       $arrTemp[$key] = is_null($index) ? current($value) : $value[$index];
   }

   $sortMethod($arrTemp);

   foreach ($arrTemp as $key => $value)
   {
       $arrOut[$key] = $arrIn[$key];
   }

   return $arrOut;
}

/**
* @desc Sanitises the $_GET array, ensuring that there are no SQL-injections being attempted.
* At the moment we only check recordid and module, though we will clearly need to expand this to other values
* in future versions.
*/
function ValidateURL()
{
    $_GET['recordid'] = ValidateValue(array('value' => $_GET['recordid'], 'type' => 'int'));
    $_GET['module'] = ValidateValue(array('value' => $_GET['module']));
}

/**
* @desc Takes a value and expected type and returns a sanitised version of the value without any unexpected
* data.
*
* @param array $aParams Array of parameters
* @param string $aParams[value] The value to be checked
* @param string $aParams[type] Type of input expected (int => number, string => full text allowed)
*
* @return string Validated input
*/
function ValidateValue($aParams)
{
    if ($aParams['value'])
    {
        if ($aParams['type'] == 'string') //no validation to do - everything is allowed
        {
            return $aParams['value'];
        }
        if ($aParams['type'] == 'int')
        {
            return preg_replace('/[^0-9]/u', '', $aParams['value']);
        }

        //default: get rid of spaces, hyphens and quotes
        return preg_replace('/[ \-\'\"]/u', '', $aParams['value']);
    }
}

function CalculateDaysDiff($date1, $date2)
{
    $DiffDays = 0;

    if (GetParm("FMT_DATE_WEB") == "US")
    {
        list($date1month, $date1day, $date1year) = explode("/", $date1);
        list($date2month, $date2day, $date2year) = explode("/", $date2);
    }
    else
    {
        list($date1day, $date1month, $date1year) = explode("/", $date1);
        list($date2day, $date2month, $date2year) = explode("/", $date2);
    }

    $date1 = mktime(0, 0, 0, $date1month, $date1day, $date1year);
    $date2 = mktime(0, 0, 0, $date2month, $date2day, $date2year);

    $difference = $date2 - $date1;

    if ($difference < 0)
    {
        $difference = 0;
    }

    $DiffDays = floor($difference/60/60/24);
    return $DiffDays;
}

// allows us to access certain labels from javascript. If this becomes too unweildy, we may need to make
// it more general or work out another way of doing it.
function AddCustomTextToJSGlobal()
{
    global $JSFunctions;

    $txtValuesToTransfer = array(
        'email_address_missing_err',
        'select_word_template_err',
        'fbk_email_check_recipients',
        'fbk_email_check_amend',
        'btn_send_and_save',
        'btn_save_without_sending',
        'btn_feedback_cancel',
        'profile_clear_check',
        'ajax_wait_msg',
        'save_wait_msg',
        'import_table_data',
        'isd_validate_year',
        'isd_validate_edition',
        'isd_export_error_title',
        'validate_step_numbers_error_msg',
        'locked',
        'unlocked',
        'confirm_or_cancel',
        'alert_title',
        'alert_message'
    );

    $JS = '';

    foreach ($txtValuesToTransfer as $txtVal)
    {
        $JS .= 'txt[\''.$txtVal.'\'] = \''.str_replace('\'', '\\\'', _tk($txtVal)).'\';
        ';
    }

    $JSFunctions[] = $JS;
}

/**
* Adds selected global parameters to a global JavaScript array,
* so they are accessible in JS functions.
*
* @global array $JSFunctions
*/
function addGlobalsToJSGlobal()
{
    global $JSFunctions;

    $globalValuesToTransfer = array(
        'AJAX_WAIT_MSG'                 => getParm('AJAX_WAIT_MSG', 'N'),
        'AJAX_WAIT_TIME'                => getParm('AJAX_WAIT_TIME', '20'),
        'COMBO_LINK_IN_SEARCH'          => getParm('COMBO_LINK_IN_SEARCH', 'N'),
        'WORD_MERGE_TEMP_FILE_LOCATION' => getParm('WORD_MERGE_TEMP_FILE_LOCATION', 'c:\\windows\\temp'),
        'MinifierDisabled'              => ($GLOBALS['MinifierDisabled'] ? 'true' : 'false'),
        'WEB_SPELLCHECKER'              => (getParm('WEB_SPELLCHECKER', 'N') == 'Y' ? 'true' : 'false'),
        'CSRF_PREVENTION'               => GetParm('CSRF_PREVENTION', 'N'),
        'dateFormatCode'                => GetParm('FMT_DATE_WEB'),
        'deviceDetect'                  => json_encode(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->getDetectValues()),
        'currencyChar'                  => GetParm('CURRENCY_CHAR', '£')
    );

    $JS = '';

    foreach ($globalValuesToTransfer as $global => $globalVal)
    {
        $JS .= "globals['" . $global . "'] = " . ($globalVal == 'true' || $globalVal == 'false' || $global == 'deviceDetect' ? $globalVal : "'" . $globalVal . "'")  .";
        ";
    }

    $JSFunctions[] = $JS;
}

/**
* @desc Checks posted user data for validity. Used in SecurityUsers.php for users and Login.php for registrations.
*
* @return bool true if errors are found, false otherwise
*/
function ValidateUserData()
{
    $UserAction = $_POST["user_action"];
    $Initials = $_POST["initials"];
    $Login = $_POST['login'];

    if ($UserAction != 'register')
    {
        if ($Initials == "")
        {
            AddValidationMessage('initials', 'Initials cannot be blank');
            $ValidationError = true;
        }

        if ($_POST["login"] == "")
        {
            AddValidationMessage('login', 'Login name cannot be blank');
            $ValidationError = true;
        }

    }

    require_once 'Date.php';

    if ($_POST["sta_daccessstart"] && $_POST["sta_daccessend"])
    {
        if (Date_Calc::IsDateGreater($_POST["sta_daccessstart"], $_POST["sta_daccessend"]) == 1)
        {
            AddValidationMessage('sta_daccessend', 'The Access end date cannot be earlier than the Access start date');
            $ValidationError = true;
        };
    }

    if ($Initials && preg_match('/[\[\]\*\?\|\&\\\"\'\!\<\>\=\-\+\%\(\)\,\:@~#\ `\/]/u', $Initials))
    {
        AddValidationMessage('initials', _tk('invalid_char_initials_err'));
        $ValidationError = true;
    }

    if (UnicodeString::strlen($_POST["login"]) && \UnicodeString::trim($_POST["login"]) == '')
    {
        AddValidationMessage('login', 'Invalid character in login name.');
        $ValidationError = true;
    }

    // Exclude characters not permitted in windows usernames.
    // Old expression: (!preg_match("/^([0-9A-Za-z-]{0,})([\\\\]{0,1})([0-9A-Za-z#%,-\.:=?@^_������������������������������]+)$/", $username))
    if ($_POST["login"] &&
        !preg_match("/^([0-9A-Za-z-_]{0,})([\\\\]{0,1})([^\"\\\:\;\|\=\,\+\*\?\<\>\(\)]+)$/u", $_POST["login"]))
    {
        AddValidationMessage('login', 'Invalid character in login name.');
        $ValidationError = true;
    }

    //If validation errors have been spotted so far, we don't want to run any sql queries with this data,
    // just in case there are apostrophes etc.
    if (!$ValidationError && $_POST['initials'] != '' && ($UserAction == "new" || $UserAction == 'register'))
    {
        $Factory = new UserModelFactory();
        $User = $Factory->getMapper()->findByInitials($Initials);

        if ($User != null)
        {
            AddValidationMessage('initials', 'Initials ' . $Initials . ' are already in use.');
            $ValidationError = true;

            $_POST["initials"] = '';
        }
    }

    if (!$ValidationError && $_POST['login'] != '')
    {
        // Check that login name does not already exist
        $sql = "SELECT login FROM contacts_main WHERE login = :login";
        $PDOParam = array("login" => $_POST['login']);

        if ($_POST['con_id'])
        {
            $sql .= " AND recordid <> :con_id";
            $PDOParam["con_id"] = Sanitize::SanitizeInt($_POST["con_id"]);
        }

        $row = DatixDBQuery::PDO_fetch($sql, $PDOParam);

        if (!empty($row))
        {
            AddValidationMessage('login', 'Login name '.Escape::EscapeEntities($_POST['login']).' is already in use.');
            $ValidationError = true;

            $_POST["login"] = '';
        }
    }

    if ($_POST["password"] && $_POST["login"] && $_POST["password"] == $_POST["login"])
    {
        AddValidationMessage('password', 'Login name cannot be used as password.');
        $ValidationError = true;
        $_POST["password"] = "";
        $_POST["retype_password"] = "";
    }
    if ($_POST["password"] != "" && $_POST["password"] != $_POST["retype_password"])
    {
        AddValidationMessage('retype_password', 'Password and retyped passwords do not match.');
        $ValidationError = true;
        $_POST["password"] = "";
        $_POST["retype_password"] = "";
    }

    if (!$ValidationError && $_POST["password"] != "")
    {
        require_once 'Source/security/SecurityBase.php';

        if (!checkNewPassword($_POST["login"], $_POST["password"], $pwdError))
        {
            AddValidationMessage('password', $pwdError['message']);
            $ValidationError = true;
            $_POST["password"] = "";
            $_POST["retype_password"] = "";
        }
    }

    if ($ValidationError)
    {
        AddSessionMessage('ERROR', 'Some of the information you entered is missing or incorrect.  Please correct the fields marked below and try again.');
    }

    return $ValidationError;
}

function validateUserPasswordFromPost()
{
    if ($_POST["password"] && $_POST["login"] && $_POST["password"] == $_POST["login"])
    {
        AddValidationMessage('password', 'Login name cannot be used as password.');
        $ValidationError = true;
        $_POST["password"] = "";
        $_POST["retype_password"] = "";
    }
    if ($_POST["password"] != "" && $_POST["password"] != $_POST["retype_password"])
    {
        AddValidationMessage('retype_password', 'Password and retyped passwords do not match.');
        $ValidationError = true;
        $_POST["password"] = "";
        $_POST["retype_password"] = "";
    }

    if (!$ValidationError && $_POST["password"] != "")
    {
        require_once 'Source/security/SecurityBase.php';

        if (!checkNewPassword($_POST["login"], $_POST["password"], $pwdError))
        {
            AddValidationMessage('password', $pwdError['message']);
            $ValidationError = true;
            $_POST["password"] = "";
            $_POST["retype_password"] = "";
        }
    }

    return $ValidationError;
}

function GetPageTopDivs($aParams)
{
    global $ModuleDefs, $scripturl;

    $Module = $aParams['module'];

    $HTML = '';

    $HTML .= getPageTitleHTML($aParams['pagetitle']);

    if ($aParams['data']['error'])
    {
        $HTML .= '
            <div class="form_error">
                The form you submitted contained the following errors.  Please correct them and try again.
            </div>
        ';
        $HTML .= '<div class="form_error">'.$aParams['data']['error']['message'].'</div>';
    }

    // If we are in "Print" mode, we don't want to display the menu
    if ($aParams['formtype'] != "Print" && isset($_SESSION["logged_in"]) && !$aParams['no_menu'])
    {
        $HTML .= GetSideMenuHTML(array('module' => 'INC', 'table' => $IncTable));

        $HTML .= '
            <div class="col form-wrapper with-menu">';
    }
    elseif (!$aParams['no_padding'])
    {
        $HTML .= '
            <div class="col form-wrapper">';
    }

    $HTML .= '
        <div class="content">
            <fieldset>
            ';

    $HTML .= GetSessionMessages();

    if ($aParams['lockmessage'])
    {
        $HTML .= '
        <div class="lock_message">
            <div id="LockMessage">'.$aParams['lockmessage'].'</div>
            <div id="userLockMessage"></div>
        </div>';
    }

    $HTML .= '  <div class="external_border">
    ';

    return $HTML;
}

/**
 * Creates the Overdue days section in the DATIXWeb Configuration screen.
 *
 * Currently only called for the incidents module, but
 * available to be used for other modules.
 * @param string $module The code for this module
 * @return string $section
 */
function GetOverdueDaysSection($module)
{
    $daysField = new FormField();
    $daysField->MakeInputField(($module == 'INC' ? 'DIF' : $module).'_OVERDUE_DAYS', 5, 3, GetParm(($module == 'INC' ? 'DIF' : $module).'_OVERDUE_DAYS', 14));
    $typeField = Forms_SelectFieldFactory::createSelectField(($module == 'INC' ? 'DIF' : $module).'_OVERDUE_TYPE', $module, GetParm(($module == 'INC' ? 'DIF' : $module).'_OVERDUE_TYPE'), '');
    $typeField->setCustomCodes(array('C' => 'Calendar days', 'W' => 'Working Days'));
    $typeField->setSuppressCodeDisplay();

    $section = '
    <div style="clear:both;">
        <div style="float:left; width:40%; font-weight:bold;">Stage</div>
        <div style="float:left; width:15%; font-weight:bold;">Days</div>
        <div style="float:left; width:45%; font-weight:bold;">Type</div>
    </div><br /><br />
    <div style="clear:both;">
        <div style="float:left; width:40%;">
            <label id="LABEL_'.($module == 'INC' ? 'DIF' : $module).'_OVERDUE_DAYS" style="margin-top" for="'.($module == 'INC' ? 'DIF' : $module).'_OVERDUE_DAYS">Overall</label>
        </div>
        <div style="float:left; width:15%;">' . $daysField->GetField() . '</div>
        <div style="float:left; width:45%;">' . $typeField->getField() . '</div>
    </div><br /><br /><br />';

    $sql = 'SELECT cas.code, cas.description, t.tim_overdue_days, t.tim_type
            FROM code_approval_status cas
            LEFT JOIN timescales t
                ON cas.module = t.tim_module
                AND cas.code = t.tim_approval_status
            WHERE cas.module = :module
            AND cas.can_be_overdue = 1
            AND cas.workflow = :workflow
            ORDER BY cas.cod_listorder';

    $result = DatixDBQuery::PDO_fetch_all($sql, array('module' => $module, 'workflow' => GetWorkflowID($module)));

    foreach ($result as $row)
    {
        if ($row['tim_overdue_days'] == '')
        {
            $row['tim_overdue_days'] = -1;
        }

        $daysField->MakeInputField($module.'_'.$row['code'].'_OVERDUE_DAYS', 5, 3, $row['tim_overdue_days']);
        $typeField = Forms_SelectFieldFactory::createSelectField($module.'_'.$row['code'].'_OVERDUE_TYPE', $module, $row['tim_type'], '');
        $typeField->setCustomCodes(array('C' => 'Calendar days', 'W' => 'Working Days'));
        $typeField->setSuppressCodeDisplay();

        $section .= '
        <div style="clear:both;">
            <div style="float:left; width:40%;">
                <label id="LABEL_'.$module.'_'.$row['code'].'_OVERDUE_DAYS" for="'.$module.'_'.$row['code'].'_OVERDUE_DAYS">' . $row['description'] . '</label>
            </div>
            <div style="float:left; width:15%;">' . $daysField->GetField() . '</div>
            <div style="float:left; width:45%;">' . $typeField->getField() . '</div>
        </div><br /><br />';
    }

    return $section;
}

// ---------------------
//@codeCoverageIgnoreStart
// ---------------------
/**
* @desc Puts together the HTML for a dropdown containing user access levels.
*
* @param obj $fieldObj reference to a general FormField object to use (I don't know that we actually need this
 * if we're returning the HTML anyway)
* @param string $paramName The id and name to give to the dropdown.
* @param string $module The module to retrueve access levels for.
* @param string $currentvalue The currently selected value.
*
* @return string The HTML representing the dropdown field.
*/
function MakeAccessLevelField(&$fieldObj, $paramName, $module, $currentvalue, $returnObj = false) {

    global $AccessLvlDefs;

    $accessLevels = array();

    $paramDefs = $AccessLvlDefs[$paramName];

    if (is_array($paramDefs))
    {
        foreach ($paramDefs as $value => $valueInfo)
        {
            $accessLevels[$value] = $valueInfo['title'];
        }
    }

    $field = Forms_SelectFieldFactory::createSelectField($paramName, $module, $currentvalue, $fieldObj->FieldMode);
    $field->setCustomCodes($accessLevels);
    $field->setSuppressCodeDisplay();

    if ($returnObj)
    {
        return $field;
    }

    return $field->getField();
}

//Used for email templates
function StripXMLTagWrapper($text)
{
    if (\UnicodeString::strpos($text, '<xml>') == 0)
    {
        $text = \UnicodeString::substr($text, 5, \UnicodeString::strlen($text) - 11);
    }

    return $text;
}

//Used for email templates
function AddXMLTagWrapper($text)
{
    return '<xml>'.$text.'</xml>';
}
// ---------------------
//@codeCoverageIgnoreEnd
// ---------------------

/**
* @desc Checks whether the current form design is formatted correctly or not. If the form is set
* up in such a way as to allow the form to be saved without a rep_approved value, the user is warned.
* Doesn't return anything, but stores errors in the session.
*/
function ValidateFormDesign()
{
    global $HideFields, $DefaultValues, $MoveFieldsToSections, $ExpandFields, $ExpandSections;

    $rep_approved_section = GetDefaultRepApprovedSection(array(
        'module' => $_GET['module'],
        'level' => ($_GET['formlevel'] ? $_GET['formlevel'] : 1)
    ));

    if ($rep_approved_section)
    {
        $rep_approved_section_hard_hide = false;
        $rep_approved_section_soft_hide = false;
        $rep_approved_field_hard_hide = false;
        $rep_approved_field_soft_hide = false;

        if (isset($MoveFieldsToSections['rep_approved']))
        {
            $rep_approved_section = $MoveFieldsToSections['rep_approved']['New'];
        }

        if ($HideFields['rep_approved'] && !$DefaultValues['rep_approved'])
        {
            $rep_approved_field_hard_hide = true;
        }

        if ($HideFields[$rep_approved_section])
        {
            $rep_approved_section_hard_hide = true;
        }

        if (is_array($ExpandSections))
        {
            foreach ($ExpandSections as $field => $actions)
            {
                if ($rep_approved_section_hard_hide)
                {
                    break;
                }

                foreach ($actions as $details)
                {
                    if ($details['section'] == $rep_approved_section)
                    {
                        $rep_approved_section_soft_hide = true;
                        break;
                    }
                }
            }
        }

        if (is_array($ExpandFields))
        {
            foreach ($ExpandFields as $field => $actions)
            {
                if ($rep_approved_field_hard_hide)
                {
                    break;
                }

                foreach ($actions as $details)
                {
                    if ($details['field'] == 'rep_approved')
                    {
                        $rep_approved_field_soft_hide = true;
                        break;
                    }
                }
            }
        }

        if ($rep_approved_field_hard_hide)
        {
            AddSessionMessage('INFO', _tk('rep_approved_field_hard_hide'));
        }
        elseif ($rep_approved_section_hard_hide)
        {
            AddSessionMessage('INFO', _tk('rep_approved_section_hard_hide'));
        }
        elseif ($rep_approved_section_soft_hide)
        {
            AddSessionMessage('INFO', _tk('rep_approved_section_soft_hide'));
        }
        elseif ($rep_approved_field_soft_hide)
        {
            AddSessionMessage('INFO', _tk('rep_approved_field_soft_hide'));
        }
    }
}

// ---------------------
//@codeCoverageIgnoreStart
// ---------------------

 /**
 * Returns the id of the section where the rep_approved field is found by default
 *
 * Used when validating the form design, since we need to know where this field is, if not specified by the user
 *
 * @param string $aParams['module'] The current module
 * @param string $aParams['level'] e.g. 1 for DIF1, 2 for DIF2
 *
 * @returns string Section ID
 */
function GetDefaultRepApprovedSection($aParams)
{
    if ($aParams['level'] == 1)
    {
        switch ($aParams['module'])
        {
            case 'RAM':
                return 'header';
                break;
            case 'INC':
            case 'PAL':
            case 'COM':
                return 'details';
                break;
        }
    }
    else
    {
        switch ($aParams['module'])
        {
            case 'INC':
                return 'name';
                break;
            case 'CON':
                return 'contact';
                break;
            case 'RAM':
            case 'PAL':
            case 'COM':
            case 'HOT':
                return 'header';
                break;
        }
    }
}

// ---------------------
//@codeCoverageIgnoreEnd
// ---------------------

/**
 * Creates join/where clauses when building a SQL statement to query for overdue records.
 *
 * Does not generate a fully-formed SQL statement.
 * The join clause is returned with the INNER JOIN prefix,
 * however the where clause does not include the WHERE declaration (to enable flexibility).
 *
 * @param string   $module         The code for this module.
 * @param string   $approvalStatus The code for this approval status.
 * @param DateTime $onDate         The date we're using to check the overdue status.  Defaults to now.
 * @param bool     $ignoreOverall  Flagged in situations where we want to ignore the overall overdue setting completely
 *                                 when determining whether or not a record is overdue for a particular approval status (used in the To Do List).
 *
 * @return array $sqlArray The join/where clauses.
 */
function getOverdueSQL($module, $approvalStatus, \DateTime $baseOnDate = null, $ignoreOverall = false)
{
    $ModuleDefs = src\framework\registry\Registry::getInstance()->getModuleDefs();
    $sqlArray   = array();
    $where      = array();
    
    $comparison = '<='; // changes to '<' when we're using working days to handle records that have been modified on weekends

    $Statuses = getOverdueStatuses(array('module' => $module));

    if (!empty($approvalStatus) && empty($Statuses[$approvalStatus])) //cannot be overdue.
    {
        $sqlArray['where'] = '1=2';
        return $sqlArray;
    }
    
    $OverdueDays = GetOverdueDays($module, $approvalStatus);
    
    if ($baseOnDate === null)
    {
        $baseOnDate = new \DateTime();
    }

    $onDate = clone $baseOnDate;

    if (!empty($approvalStatus) && $OverdueDays[$approvalStatus] >= 0)
    {
        if ($OverdueDays[$approvalStatus.'_type'] == 'W')
        {
            // we're calculating the overdue date based on working days
            $OverdueDays[$approvalStatus] = CalculateWorkingDays($OverdueDays[$approvalStatus], $onDate, true);
            $comparison = '<';
        }
        
        $MyDate = $onDate->modify('-'.((int) $OverdueDays[$approvalStatus]).' day')
                         ->format('Y-m-d');

        if (empty($OverdueDays['overdue_date_field'])) 
        {
            // we could eventually allow users to decide whether the overdue date is calculated from
            // the first time a record reaches an approval status, or the most recent time.
            // e.g. $minMax = $OverdueDays['overdue_calculated_from'] == 'earliest' ? 'MIN' : 'MAX';
            // for now we always look for the most recent date a record reached its current approval status,
            // effectively resetting the clock if a record is moved back to a previous status
            $minMax = 'MAX';

            if($module = 'INC')
            {
                $join = '
                    LEFT JOIN
                    (
                        SELECT inc_status_audit.recordid as aud_recordid, DATEADD(day, DATEDIFF(day, \'20000101\', ' . $minMax . '([date])), \'20000101\') AS auditdate
                        FROM inc_status_audit
                        WHERE [status] = \''.$approvalStatus.'\'
                        GROUP BY inc_status_audit.recordid
                    ) AS fa' . $suffix . '
                    ON fa' . $suffix . '.aud_recordid = ' . $ModuleDefs[$module]['TABLE'] . '.recordid
                    ';
            }
            else
            {
                $join = '
                    LEFT JOIN
                    (
                        SELECT aud_record, DATEADD(day, DATEDIFF(day, \'20000101\', ' . $minMax . '(aud_date)), \'20000101\') AS auditdate
                        FROM full_audit
                        WHERE aud_module = \'' . $module . '\'
                        AND aud_action = \'WEB:rep_approved\'
                        AND CAST(aud_detail AS varchar(6)) = \'' . $approvalStatus . '\'
                        GROUP BY aud_record
                    ) AS fa'.$suffix.'
                    ON fa'.$suffix.'.aud_record = ' . $ModuleDefs[$module]['TABLE'] . '.recordid
                    ';
            }

            // in addition to checking the audit date, we need to check
            // against the OVERDUE_CHECK_FIELD in cases where this record has
            // no full_audit entry (i.e. this record has never been updated)
            $where[] = '(fa'.$suffix.'.auditdate '. $comparison .' \'' . $MyDate . '\' OR (fa'.$suffix.'.auditdate IS NULL AND ' . $ModuleDefs[$module]['TABLE'] . '.' . $ModuleDefs[$module]['OVERDUE_CHECK_FIELD'].' '. $comparison .' \'' . $MyDate . '\'))';
        }
        else
        {
            // we've specified a field to calculate the overdue date from for this approval status
            $where[] = $ModuleDefs[$module]['TABLE'] . '.' . $OverdueDays['overdue_date_field'] . ' '. $comparison .' \'' . $MyDate . '\'';
        }
    }

    if ($OverdueDays['overall'] >= 0 && !$ignoreOverall)
    {
        $onDate = clone $baseOnDate;

        // we need to take the overall timescale into consideration
        if ($OverdueDays['overall_type'] == 'W')
        {
            // we're calculating the overdue date based on working days
            $OverdueDays['overall'] = CalculateWorkingDays($OverdueDays['overall'], $onDate, true);
            $comparison = '<';
        }

        $MyDate = $onDate->modify('-'.((int) $OverdueDays['overall']).' day')
                         ->format('Y-m-d');
        
        $where[] = $ModuleDefs[$module]['TABLE'] . '.' . $ModuleDefs[$module]['OVERDUE_CHECK_FIELD'].' '. $comparison .' \'' . $MyDate . '\'';
    }

    $sqlArray['join'] = $join;

    if (!empty($where))
    {
        $sqlArray['where'] = '(' . implode(' OR ', $where) . ')';
    }
    else
    {
        $sqlArray['where'] = '';
    }

    return $sqlArray;
}

/**
* Determines whether overdue functionality is enabled for this module.
*
* @param string $module The module code.
*
* @return boolean Whether or not overdue functionality is enabled.
*/
function overdueSet($module)
{
    global $ModuleDefs;

    // first check for an overall overdue setting
    if (GetParm($ModuleDefs[$module]['OVERDUE_DAY_GLOBAL'], 14) == -1)
    {
        // check for approval status-specific overdue settings
        $result = db_query("SELECT tim_overdue_days FROM timescales WHERE tim_module = '$module'");

        while ($row = db_fetch_array($result))
        {
            if ($row['tim_overdue_days'] >= 0)
            {
                return true;
            }
        }

        return false;
    }
    else
    {
        return true;
    }
}

/**
* Fetches an array of codes and descriptions for approval statuses that can become overdue.
*
* @param string $aParams['module'] The module code.
* @param boolean $aParams['db_only'] If true returns all possible overdue codes, regardless of whether they've
 * been activated.
*
* @return array $approvalStatuses An array where the keys/values are codes/descriptions respectively.
*/
function getOverdueStatuses($aParams)
{
    global $ModuleDefs;

    $approvalStatuses = array();
    $result = db_query('SELECT cas.code as code, cas.description as description, t.tim_overdue_days as days
                        FROM code_approval_status cas
                        LEFT JOIN timescales t
                            ON cas.module = t.tim_module
                            AND cas.code = t.tim_approval_status
                        WHERE module = \''.$aParams['module'].'\'
                        AND cas.workflow = '.GetWorkflowID($aParams['module']).'
                        AND can_be_overdue = 1');

    while ($row = db_fetch_array($result))
    {
        if ($aParams['db_only'])
        {
            $approvalStatuses[$row['code']] = FirstNonNull(array(
                _tk('approval_status_'.$aParams['module'].'_'.$row['code']),
                $row['description']
            ));
        }
        elseif (GetParm($ModuleDefs[$aParams['module']]['OVERDUE_DAY_GLOBAL'], 14) != -1 || $row['days'] != -1)
        {
            $approvalStatuses[$row['code']] = FirstNonNull(array(
                _tk('approval_status_'.$aParams['module'].'_'.$row['code']),
                $row['description']
            ));
        }
    }

    return $approvalStatuses;
}

// ---------------------
//@codeCoverageIgnoreStart
// ---------------------

 /**
 * Adds a custom help bubble for a non-standard form.
 *
 * Used in e.g. DatixWeb configuration where there is no form design
 *
 * @param string $aParams['field'] The id of the field to which this help is linked
 * @param string $aParams['title'] The name of the field (for the title bar)
 * @param string $aParams['help'] The content of the help box (stored in session until needed).
 *
 * @returns string The HTML code for the actual help link
 */
function AddHelpBubble($aParams)
{
    global $scripturl;

    $_SESSION["CustomHelpTexts"][$aParams['field']] = $aParams['help'];

    return '
        <a href="javascript:PopupDivFromURL(\'help\', \''._tk('datix_help_title').$aParams['title'].'\', \'' . $scripturl . '?action=fieldhelp&amp;module=ADM&amp;level=2&amp;field='.$aParams['field'].'\', \'\', \'\', \'300px\')">
            <img id="'.$aParams['field'].'_help_image" src="images/Help.gif" style="cursor:pointer" alt="Help ('.$aParams['title'].')"  style="border:0" />
        </a>';
}

function getConfigurationParameters()
{
    return array(
        // Actions
        'ACT_OWN_ONLY',
        'ACT_DEFAULT',
        'ACT_LISTING_ID',
        'ACT_SEARCH_DEFAULT',
        'ACT_SHOW_AUDIT',
        'ACT_SETUP',
        'DELETE_ACT_CHAIN',
        'ACT_DELETE_DOCS',

        // All modules
        'LOGIN_DEFAULT_MODULE',
        'LOGOUT_DEFAULT_MODULE',
        'CUSTOM_REPORT_BUILDER',
        'ADDITIONAL_REPORT_OPTIONS',
        'PROG_NOTES_EDIT',
        'ENABLE_GENERATE',
        'ENABLE_BATCH_DELETE',
        'ENABLE_BATCH_UPDATE',

        // Admin
        'FULL_ADMIN',
        'ADM_DEFAULT',
        'ADM_LISTING_ID',
        'ADM_PROFILES',
        'ADM_SHOW_AUDIT',
        'ADM_GROUP_SETUP',
        'ADM_VIEW_OWN_USER',
        'ADM_NO_ADMIN_REPORTS',

        // Contacts
        'CON_DEFAULT',
        'CON_LISTING_ID',
        'CON_SEARCH_DEFAULT',
        'CON_SHOW_REJECT_BTN',
        'CON_SETUP',
        'CON_ALLOW_MERGE_DUPLICATES',

        // Equipment/Assets
        'AST_DEFAULT',
        'AST_LISTING_ID',
        'AST_SEARCH_DEFAULT',
        'AST_SETUP',
        'AST_DELETE_DOCS',

        // Medications
        'MED_DEFAULT',
        'MED_LISTING_ID',
        'MED_SEARCH_DEFAULT',
        'MED_SETUP',

        // Safety Alerts
        'SAB1_DEFAULT',
        'SAB2_DEFAULT',
        'SAB_LISTING_ID',
        'SAB1_SEARCH_DEFAULT',
        'SAB_SETUP',
        'SAB_DELETE_DOCS',

        // PALS
        'PAL2_DEFAULT',
        'PAL_LISTING_ID',
        'PAL2_SEARCH_DEFAULT',
        'PAL_OWN_ONLY',
        'PAL_SHOW_AUDIT',
        'PAL_SETUP',
        'PAL_DELETE_DOCS',

        // COM
        'COM2_DEFAULT',
        'COM_LISTING_ID',
        'COM2_SEARCH_DEFAULT',
        'COM_OWN_ONLY',
        'COM_SHOW_AUDIT',
        'COM_SETUP',
        'COM_DELETE_DOCS',
        'COM_SHOW_ADD_NEW',

        // Claims
        'CLAIM1_DEFAULT',
        'CLAIM2_DEFAULT',
        'CLA_LISTING_ID',
        'CLAIM2_SEARCH_DEFAULT',
        'CLA_OWN_ONLY',
        'CLA_SHOW_AUDIT',
        'CLA_SETUP',
        'CLA_DELETE_DOCS',

        //Payments
        'PAY2_SEARCH_DEFAULT',
        'DELETE_PAY',

        // Incidents
        'DIF_SHOW_REJECT_BTN',
        'DIF2_HIDE_CONTACTS',
        'USER_GRANT_ACCESS',
        'DIF_OWN_ONLY',
        'INC_SHOW_AUDIT',
        'DIF1_ONLY_FORM',
        'DIF2_DEFAULT',
        'INC_LISTING_ID',
        'DIF2_SEARCH_DEFAULT',
        'INC_SETUP',
        'COPY_INCIDENTS',
        'INC_DELETE_DOCS',
        'INC_SAVED_QUERIES_HOME_SCREEN',
        'INC_SAVED_QUERIES',

        // Risk register
        'RISK_OWN_ONLY',
        'RISK2_DEFAULT',
        'RAM_LISTING_ID',
        'RISK2_SEARCH_DEFAULT',
        'RAM_SHOW_AUDIT',
        'RAM_SETUP',
        'COPY_RISKS',
        'RAM_DELETE_DOCS',
    	
    	// Policy
    	'POL2_DEFAULT',
    	'POL_LISTING_ID',
    	'POL2_SEARCH_DEFAULT',
    	'POL_OWN_ONLY',
    	'POL_SHOW_AUDIT',
    	'POL_SETUP',
    	'COPY_POL',
    	'POL_DELETE_DOCS',

        // HSA
        'HSA2_DEFAULT',
        'HSA_LISTING_ID',
        'HSA2_SEARCH_DEFAULT',

        // HOT
        'HOT2_DEFAULT',
        'HOT_LISTING_ID',
        'HOT2_SEARCH_DEFAULT',
        'HOT_DELETE_DOCS',

        //Standards
        'STN2_DEFAULT',
        'STN_LISTING_ID',
        'STN2_SEARCH_DEFAULT',
        'STN_ADD_NEW',
        'STN_SETUP',
        'COPY_STANDARDS',
        'STN_DELETE_DOCS',

        // Locations
        'LOC_DEFAULT',

        //Library
        'LIB_SETUP',
        'LIB_DELETE_DOCS',

        // CQC Standards
        'CQO_LISTING_ID',
        'CQP_LISTING_ID',
        'CQS_LISTING_ID',
        'LOC_LISTING_ID',
        'CQO_DEFAULT',
        'CQP_DEFAULT',
        'CQS_DEFAULT',

        //Accreditation
        'ATQ_DEFAULT',
        'ATQ_LISTING_ID',
        'ATI_DEFAULT',
        'ATI_LISTING_ID',
        'ATM_DEFAULT',
        'ATM_LISTING_ID',
        'AQU_DEFAULT',
        'AQU_LISTING_ID',
        'AMO_DEFAULT',
        'AMO_LISTING_ID',
        'ATM_MQ_TEMP',
        'ATM_AAT',
        'ATM_CAI',
        'ATI_STAFF_EMAIL',
        'ATI_LOC_EMAIL',
        'ATM_STAFF_EMAIL',
        'ASM_MANAGE_CYCLES',
        'ASM_CREATE_INSTANCES_EMAIL',
        'AMO_SUBMIT_INSTANCE_EMAIL',
        'AMO_STAFF_EMAIL',
        'ATI_REVIEWED_EMAIL'
    );
}
// ---------------------
//@codeCoverageIgnoreEnd
// ---------------------

 /**
 * Gets ID of current workflow
 *
 * Very simple at the moment, but separated in case we need to add more layers here. If necessary, this can be
  * folded back into the main code body. Returns the appropriate workflow for the module, or 0 if this module
  * does not use workflows.
 *
 * @param string The module we are looking at.
 *
 * @returns int The ID of the current workflow.
 */
function GetWorkflowID($module)
{
    global $ModuleDefs;

    if ($ModuleDefs[$module]['USE_WORKFLOWS'])
    {
        return GetParm($ModuleDefs[$module]['WORKFLOW_GLOBAL'], $ModuleDefs[$module]['DEFAULT_WORKFLOW']);
    }

    return 0;
}

// ---------------------
//@codeCoverageIgnoreStart
// ---------------------

// used by SaveXXX.php to print out errors when saving (can probably be replaced in the future)
function SaveError($error)
{
    global $yyheaderdone;

    if ($yyheaderdone)
    {
        echo '<script language="JavaScript" type="text/javascript">
document.getElementById("waiting").style.display="none";
</script>';
    }

    fatal_error($error);
    template_header();
    echo "<b>". htmlspecialchars($error) . "</b>";
    obExit();
}

// ---------------------
//@codeCoverageIgnoreEnd
// ---------------------

/**
* @desc Zeros the time portion of a date, so time can be disregarded in date comparisons.
*
* @param array $date Array of date, containing 'day', 'month', 'year'.
*
* @returns object A date object containing the date with a time of 00:00:00
*/
function removeTimeFromDate($date)
{
    $date = date_parse($date);
    $date = date('Y-m-d H:i:s', mktime(0, 0, 0, $date['month'], $date['day'], $date['year']));
    return $date;
}

/**
 * Processes values in an array with the htmlentities function.
 *
 * This function reurns an array after the htmlentities function has been applied to all values in the array.
 * Can be useful for processing data which needs to be displayed without any HTML being applied to the output.
 * @param string array $ValuesToProssess The source array
 * @return string array
 */
function ProcessHTMLEntities($ValuesToProssess)
{
    $ProcessedHTMLEntities = array();

    foreach ($ValuesToProssess as $key => $value)
    {
        if (!is_array($value))
        {
            $ProcessedHTMLEntities[$key] = \src\security\Escaper::escapeForHTML($value);
        }
        else
        {
            //Recursively process values in sub-arrays
            $ProcessedHTMLEntities[$key] = ProcessHTMLEntities($value);
        }
    }

    return $ProcessedHTMLEntities;
}

/**
* @desc Adds javascript instructions to disable fields on the DIF1 form, based on the fact that they
* were previously matched with db records.
*
* @param string $InputFieldName The id of the field to be disabled.
* @param array $FieldDef Array of field parameters
*/
function AddJSDisableScript($InputFieldName, $FieldDef)
{
    global $JSFunctions;

    switch ($FieldDef["Type"])
    {
        case 'ff_select':
        case 'multilistbox':
            $JSFunctions[] = '
                jQuery(document).ready(function()
                {
                    if (jQuery("input[id='.$InputFieldName.'_title]")length)
                    {
                        jQuery("input[id='.$InputFieldName.'_title]").disable();
                    }
                });';
            break;
        default:
            $JSFunctions[] = '
                jQuery(document).ready(function()
                {
                    if (jQuery("#'.$InputFieldName.'").length)
                    {
                        jQuery("#'.$InputFieldName.'").attr("disabled", true);
                        AddHiddenField($("'.$InputFieldName.'"));
                        if (jQuery("#img_cal_'.$InputFieldName.'").length)
                        {
                            jQuery("#img_cal_'.$InputFieldName.'").css("visibility", "hidden");
                        }
                    }
                });';
            break;
    }

    $JSFunctions[] = '
        jQuery(document).ready(function()
        {
            if (jQuery("#check_btn_'.$InputFieldName.'").length)
            {
                jQuery("#check_btn_'.$InputFieldName.'").attr("disabled", true);
            }
            AddCustomHiddenField("'.$InputFieldName.'_disabled", "'.$InputFieldName.'_disabled", "1");
        });';
}

/**
* @desc Takes a module and a field and finds the parents for that field.
*
* @param array $aParams Array of parameters
* @param string $aParams['module'] The current module
* @param string $aParams['field'] The field in question
*
* @return array An array containing up to two parents as array(parent1, parent2)
*/
function GetParents($aParams)
{
    global $ModuleDefs;

    $Parent1 = '';
    $Parent2 = '';

    if ($_SESSION['CachedValues']['ChildParent'][$aParams['module']][$aParams['field']])
    {
        $Parent1 = $_SESSION['CachedValues']['ChildParent'][$aParams['module']][$aParams['field']][0];

        if ($_SESSION['CachedValues']['ChildParent'][$aParams['module']][$aParams['field']][1] &&
            $_SESSION['CachedValues']['ChildParent'][$aParams['module']][$aParams['field']][1] !=
                $_SESSION['CachedValues']['ChildParent'][$aParams['module']][$aParams['field']][0])
        {
            $Parent2 = $_SESSION['CachedValues']['ChildParent'][$aParams['module']][$aParams['field']][1];
        }
    }

    return array($Parent1, $Parent2);
}

/**
* @desc Takes a module and a field and finds the children for that field.
*
* @param array $aParams Array of parameters
* @param string $aParams['module'] The current module
* @param string $aParams['field'] The field in question
*
* @return array An array containing an unlimited number of children as array(child1, child2, ...)
*/
function GetChildren($aParams)
{
    global $ModuleDefs;

    $children = array();

    if (!empty($_SESSION['CachedValues']['ParentChild'][$aParams['module']][$aParams['field']]))
    {
        $children = array_unique($_SESSION['CachedValues']['ParentChild'][$aParams['module']][$aParams['field']]);
    }

    return $children;
}

//@codeCoverageIgnoreStart

/**
* @desc constructs a header to be used in place of {@link template_header()} when we don't want the full datix
 * banner top and bottom.
*
* @return string The HTML describing the page header.
*/
function getBasicHeader()
{
    global $dtxtitle, $MinifierDisabled;

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    return '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html>
            <head>
                <title>'.$dtxtitle.'</title>
                <link rel="stylesheet" type="text/css" href="css/global' . $addMinExtension . '.css" media="screen"/>
                <link rel="stylesheet" type="text/css" href="css/nav' . $addMinExtension . '.css" media="screen"/>
                <link rel="stylesheet" type="text/css" href="css/login' . $addMinExtension . '.css" media="screen"/>
                <link rel="stylesheet" type="text/css" href="css/dashboard' . $addMinExtension . '.css" media="screen"/>
                <link rel="stylesheet" type="text/css" href="css/dropdown_popups' . $addMinExtension . '.css" media="screen"/>
                <link rel="stylesheet" type="text/css" href="css/crosstab' . $addMinExtension . '.css" media="screen"/>
            </head>
            <body>';
}

/**
* @desc constructs a footer to be used in place of {@link footer()} when we don't want the full datix banner top
 * and bottom.
*
* @return string The HTML describing the page footer.
*/
function getBasicFooter()
{
    return '
            </body>
        </html>';
}

//@codeCoverageIgnoreEnd

/**
* @desc Gets the url of a given record, ready for a redirect.
*
* @param array $Parameters Array of parameters
* @param int $Parameters['recordid'] The id of the record in question
* @param int $Parameters['module'] The module the record belongs to
* @param int $Parameters['module'] The panel to direct to
*
* @return string The url of the record in question.
*/
function getRecordURL($Parameters)
{
    global $scripturl, $ModuleDefs;

    if ($Parameters['module'] == '' || $Parameters['recordid'] == '')
    {
        return '';
    }

    $URL = $scripturl . '?';

    if ($ModuleDefs[$Parameters['module']]['GENERIC'] &&
        $ModuleDefs[$Parameters['module']]['MAIN_URL_OVERRIDE'] != true)
    {
        $URL .= 'action=record&module='.$Parameters['module'];
    }
    elseif ($Parameters['module'] == 'AST' && $Parameters['linkMode'] == true)
    {
    	$URL .= 'action=linkequipment';
    }
    elseif ($ModuleDefs[$Parameters['module']]['MAIN_URL'])
    {
        $URL .= $ModuleDefs[$Parameters['module']]['MAIN_URL'];
    }
    else
    {
        $URL .= 'action='.$ModuleDefs[$Parameters['module']]['ACTION'];
    }

    $URL .= '&'.($ModuleDefs[$Parameters['module']]['PK'] ?: 'recordid').'=' . $Parameters['recordid'];

    if ($Parameters['panel'])
    {
        $URL .= '&panel=' . $Parameters['panel'];
    }

    return $URL;
}

/**
* @desc Gets an array of fields which should not be appearing in the reporting setup. This is
* a hard coded hack because some "user" fields are classed as "contact" fields and so are available
* for contact reporting as soon as they are added to field formats.
*
* @return array Array of fields to exclude.
*/
function GetHardCodedReportExcludedFields()
{
    return array(
        'con_staff_include',
        'sta_lockout_dt',
        'sta_pwd_change',
        'sta_daccessend',
        'sta_daccessstart',
        'sta_user_type',
        'sab_issued_time',
        'act_model',
        'pay_vat',

        'act_chain_id',
        'act_chain_instance',
        'act_step_no',
        'act_start_after_step',
        'act_start_after_days_no',
        'act_due_days_no',
        'act_reminder_days_no',
        'link_primary',
        'updateddate'
    );
}

/**
* Validates an NHS number.
*
* Validation can be toggled on/off with global 'VALID_NHSNO'.
* Determines whether this contact has a valid NHS number (using {@link ValidNHSNumber()})
* and produces an error message if not.
* @global array of user-definable text.
* @param array $aParams['data'] The data posted on the form.
* @param array $_POST The data posted on the form, used if $aParams['data'] is empty.
* @param int $aParams['suffix'] The identifier for this contact on the form.
* @param array $error The current error array as created by previous contacts validation.
* @return array $error Contains any error information resulting from invalid data.
*/
function ValidateNHSNumber($aParams, $error)
{
    if (!is_array($aParams['data']))
    {
        $aParams['data'] = $_POST;
    }

    $field = isset($aParams['suffix']) ? 'con_nhsno_'.$aParams['suffix'] : 'con_nhsno';

    if (bYN(GetParm('VALID_NHSNO', 'Y')) && array_key_exists($field, $aParams['data']) &&
        $aParams['data'][$field] != '')
    {
        if (!ValidNHSNumber($aParams['data'][$field]))
        {
            $error[$field] = _tk('invalid_nhs_num_err');
        }
    }

    return $error;
}

/**
* Determines whether or not a given NHS number is valid.
*
* @param string $sNHSNo The NHS number
* @return boolean Whether or not the number is valid.
*/
function ValidNHSNumber($sNHSNo)
{
    $sNHSNo = str_replace(' ', '', $sNHSNo);

    if (\UnicodeString::strlen($sNHSNo) != 10)
    {
        return false;
    }

    $nNHSIndex = 0;
    $nCount10to2 = 10;
    $nNHSCheckDigit = 0;

    // Step 1 - multiply each of the first nine digits by a weighting factor and Step 2 - add the results
    // of each multiplication together
    while ($nNHSIndex < 9)
    {
        $nNHSCheckDigit += \UnicodeString::substr($sNHSNo, $nNHSIndex, 1) * $nCount10to2;
        $nNHSIndex++;
        $nCount10to2--;
    }

    // Step 3 - divide the total by 11 and establish the remainder
    $nNHSCheckDigit %= 11;

    // Step 4 - subtract the remainder from 11 to give the check digit
    $nNHSCheckDigit = 11 - $nNHSCheckDigit;

    // Check for exceptions. There are two occasions where the check digit calculation process
    // must be modified slightly:
    // if the result of step 4 is 11 then a check digit of 0 is used
    // if the result of step 4 is 10 then the number is invalid and not used.
    if ($nNHSCheckDigit == 11)
    {
        $nNHSCheckDigit = 0;
    }

    // See if Check Digit matches
    if (\UnicodeString::substr($sNHSNo, 9, 1) == $nNHSCheckDigit && $nNHSCheckDigit != 10)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* @desc Checks whether the user should be prompted about unapproved contacts, based
* on the current and previous values of rep_approved
*/
function CheckUnapprovedContactRedirect($Parameters)
{
    $sql = '
        SELECT
            apac_check_contacts
        FROM
            approval_action
        WHERE
            apac_from = :from
            AND apac_to = :to
            AND module = :module
            AND access_level = :access_level
    ';

    $CheckContacts = DatixDBQuery::PDO_fetch($sql, array(
        'from' => $Parameters['from'],
        'to' => $Parameters['to'],
        'module' => $Parameters['module'],
        'access_level' => $Parameters['access_level']
    ), PDO::FETCH_COLUMN);

    return ($CheckContacts == 'Y');
}

function DoUnapprovedContactRedirect($Parameters)
{
    $moduleDefs = Registry::getInstance()->getModuleDefs();
    $module     = $Parameters['module'];
    $formLevel  = $_POST['formlevel'];

    $UnapprovedContactsByType = array();

    foreach ($Parameters['contact_array'] as $Contact)
    {
        $UnapprovedContactsByType[$Contact['link_type']][] = $Contact;
    }

    AddSessionMessage('INFO', _tk($module.'_'.$formLevel.'_record_saved'));

    if (CanSeeModule('CON'))
    {
        $UnapprovedMessage = _tk('contacts_require_approval').'<br/><br/>';

        foreach ($UnapprovedContactsByType as $Type => $Contacts)
        {
            AddSectionMessage('contacts_type_'.$Type, 'INFO', _tk('link_contact_before_approve'));

            $UnapprovedMessage .= '<div><b>'.$moduleDefs[$Parameters['module']]['CONTACTTYPES'][$Type]['Plural'].':</b></div>';

            foreach ($Contacts as $Contact)
            {
                $UnapprovedMessage .= '<div>'.htmlspecialchars($Contact['con_title']).' '.htmlspecialchars($Contact['con_forenames']).' '.htmlspecialchars($Contact['con_surname']).'</div>';
            }
        }
    }

    if (count($UnapprovedContactsByType) > 1) //need to work out which section to redirect to
    {
        IncludeCurrentFormDesign($Parameters['module'], 2);

        if (is_array($GLOBALS['OrderSections']))
        {
            foreach ($GLOBALS['OrderSections'] as $Order => $Section)
            {
                if (mb_substr_count($Section, 'contacts_type_') != 0)
                {
                    $ContactSectionOrder[str_replace('contacts_type_', '', $Section)] = $Order;
                }
            }
        }

        foreach ($UnapprovedContactsByType as $Type => $Contacts)
        {
            if ($RedirectType == null || $ContactSectionOrder[$RedirectType] > $ContactSectionOrder[$Type])
            {
                $RedirectType = $Type;
            }
        }
    }
    else
    {
        $RedirectType = $Type;
    }

    if ($UnapprovedMessage)
    {
        AddSessionMessage('INFO', $UnapprovedMessage);
    }

    $yySetLocation = getRecordURL($Parameters).'&panel=contacts_type_'.$RedirectType.($_POST['fromsearch'] == 1 ? '&fromsearch=1' : '').'&token='.CSRFGuard::getCurrentToken();
    /*
    We can use this code if we ever want to show the "please wait"
    dialogue on the DIF2 form, as headers will already have been sent.*/
    echo "<script language='JavaScript' type='text/javascript'>window.location = '$yySetLocation';</script>";
    obExit();
}

function GetModulesWithNoFormDesign()
{
     return array('DAS', 'TOD');
}

/**
* Called from ajax when cascading parent values upwards.
*/
function GetParentValue()
{
    //Suffix manipulation needed for dealing with parenting on e.g. subjects forms.
    $NameArray = explode('_', $_GET['field']);
    if (is_numeric($NameArray[count($NameArray)-1]))
    {
        $SuffixString = '_'.$NameArray[count($NameArray)-1];
    }

    $parents = GetParents(array('module' => $_GET['module'], 'field' => RemoveSuffix($_GET['field'])));
    $SelectedCodeInfo = get_code_info(
        $_GET['module'],
        CheckFieldMappings($_GET['module'], RemoveSuffix($_GET['field']), true),
        $_GET['value']
    );
    $ParentCodeInfo = get_code_info(
        $_GET['module'],
        CheckFieldMappings($_GET['module'], $parents[0], true),
        $SelectedCodeInfo['cod_parent']
    );

    $parent_test = explode(' ', $SelectedCodeInfo['cod_parent']);
    $JSONdata['multiparent'] = count($parent_test);
    $JSONdata['value'] = $SelectedCodeInfo['cod_parent'];
    $JSONdata['description'] = $ParentCodeInfo['description'];
    $JSONdata['colour'] = $ParentCodeInfo['cod_web_colour'];
    $JSONdata['fieldname'] = $parents[0].$SuffixString;
    echo json_encode($JSONdata);
}

/**
* Creates a DatixDBQuery object and prepares and executes SQL passed to it. Replacement for db_query().
* N.B. Only wrapper for DatixDBQuery static method.
*
* @deprecated Replace with DatixDBQuery::PDO_query
* @param  string $sql  SQL statement.
* @param  array  $PDOParamsArray   Array containing key/values for SQL variables.
*
* @return bool Successful execution
*/
function PDO_query($sql, $PDOParamsArray = array())
{
    return DatixDBQuery::PDO_query($sql, $PDOParamsArray);
}

/**
* Fetches complete result set for SQL SELECT with given variables.
* N.B. Only wrapper for DatixDBQuery static method
*
* @deprecated Replace with DatixDBQuery::::PDO_fetch_all
* @param  string $sql  SQL statement.
* @param  array  $PDOParamsArray   Array containing key/values for SQL variables.
* @param string $fetchStyle Controls the contents of the returned array as documented in PDOStatement::fetch().
*
* @return array Complete result set for SQL SELECT with given variables.
*/
function PDO_fetch_all($sql, $PDOParamsArray = array(), $fetchStyle = PDO::FETCH_ASSOC)
{
    return DatixDBQuery::PDO_fetch_all($sql, $PDOParamsArray, $fetchStyle);
}

/**
* Fetches a single row from a result set for SQL SELECT with given variables.
* N.B. Only wrapper for DatixDBQuery static method. 
*
* @deprecated Replace with DatixDBQuery::PDO_fetch
* @param  string  $sql             SQL statement.
* @param  array   $PDOParamsArray  Array containing key/values for SQL variables.
* @param  string  $fetchStyle      Controls the contents of the returned array as documented in PDOStatement::fetch().
*
* @return array                    Complete result set for SQL SELECT with given variables.
*/
function PDO_fetch($sql, $PDOParamsArray = array(), $fetchStyle = PDO::FETCH_ASSOC)
{
    return DatixDBQuery::PDO_fetch($sql, $PDOParamsArray, $fetchStyle);
}

/**
* Prefixes a value with a colon character.
*
* Useful when building SQL string where we're binding parameters with PDO.
*
* @param  string $value The original value.
*
* @return string The value prefixed with a colon.
*/
function addColon($value)
{
    return ':' . $value;
}

/**
* Generic function to provide messages when not logged in
*
* @param  string $Message Message.
* @param  string $Title Option title.
* @param  string $Message Type of Message.
*
*/
function SimpleMessage($Message, $Title = "", $MessageType = "INFO")
{
    AddSessionMessage('INFO', $Message);

    template_header();

    echo getPageTitleHTML(array('title' => $Title));

    echo GetSessionMessages();

    footer();
}

function ModArrayCanLinkDocs()
{
    return array('INC', 'RAM', 'PAL', 'COM', 'STN', 'LIB', 'SAB', 'AST', 'HOT');
}

/**
* @desc Escapes slashes and quotes. Used when the string will be inside both a double and single quoted string.
*/
function HTMLEscape($string)
{
    return str_replace(array('\\', "'", '"'), array('\\\\', "\'", '&quot;'), $string);
}

/**
* Determines whether or not we're on a level one form.
*
* @return boolean
*/
function isLevelOneForm()
{
    return (!$_SESSION["logged_in"] || $_GET['action'] == 'newdif1' || $_GET['level'] == '1');
}

/**
* Returns Javascript used to confirm form cancellation.
*
* @return string The javascript for confirming form cancellation.
*/
function getConfirmCancelJavascript($formID = '')
{
    return 'if(confirm(\''.addslashes(_tk('confirm_or_cancel')).'\')){'.
                'if ('.($formID ? $formID : 'this.form').'.rbWhat != null){'.($formID ? $formID : 'this.form').'.rbWhat.value = \'Cancel\';} submitClicked = true; '.($formID ? $formID : 'this.form').'.submit(); }';
}

function HasPrimaryComplLinkData($com_id)
{
    if (!empty($com_id))
    {
        $sql = "SELECT count(*) as PrimaryComplainantCheck
                FROM LINK_COMPL WHERE com_id = $com_id AND lcom_current = 'Y' AND lcom_primary = 'Y'";

        $row = DatixDBQuery::PDO_fetch($sql);
    }

    if ($row["PrimaryComplainantCheck"] > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
* Clears any search related SESSION data for a specified module
*
* @param string $Module The short name for the module to reset.
*/
function ClearSearchSession($Module)
{
    $_SESSION[$Module]["WHERE"] = "";
    $_SESSION[$Module]["DUPLICATE_SEARCH"] = "";
    $_SESSION["PROMPT"]["PROMPT_WHERE"] = "";
    $_SESSION[$Module]["WHERETABLE"] = "";
    $_SESSION[$Module]["SEARCHLISTURL"] = "";
    $_SESSION[$Module]["RECORDLIST"] = null;

    ClearAtPromptSession($Module);

    $_SESSION[$Module]["current_drillwhere"] = "";
    
    $_SESSION[$Module]['NEW_WHERE'] = null;
    $_SESSION[$Module]['DRILL_QUERY'] = null;
    $_SESSION[$Module]['list_type'] = null;
}

function ClearAtPromptSession($Module)
{
    global $ModuleDefs;

    $_SESSION[$Module]["PROMPT"]["PROMPT_WHERE"] = '';
    $_SESSION[$Module]["PROMPT"]["NEW_WHERE"] = '';
    $_SESSION[$Module]["PROMPT"]["ORIGINAL_WHERE"] = '';
    $_SESSION[$Module]["PROMPT"]["CURRENT_QUERY_ORIGINAL_WHERE"] = '';
    $_SESSION[$Module]['PROMPT']['PROMPTSUBMITTED'] = null;
    $_SESSION[$Module]['PROMPT']['VALUES'] = array();

    foreach ($_SESSION['NEW_PROMPT_VALUES'] as $field => $values)
    {
        $fieldParts = explode('.', $field);
        if ($fieldParts[0] == $ModuleDefs[$Module]['TABLE'])
        {
            unset($_SESSION['NEW_PROMPT_VALUES'][$field]);
        }
}
}

/**
* Temporary function required for retrieving the table from the field directory for a field
* by checking against the main module table fields, otherwise assuming only a maximum of one other duplicate
 * entry for the field.
* E.g. inc_injury can be found in the incidents_main and inc_injuries.
* This function should be deprecated once all parts of the system has been converted to use field_directory and
 * form design stores the table information.
*
* @param string Field to find table for
* @param sting Module for field.
*
* @return string Table for field.
*/
function GetTableForField($Field, $Module)
{
    global $ModuleDefs;

    if ((is_array($ModuleDefs[$Module]["FIELD_MAPPINGS"]) &&
        array_flip($ModuleDefs[$Module]["FIELD_MAPPINGS"])[$Field]))
    {
        $Field = array_flip($ModuleDefs[$Module]["FIELD_MAPPINGS"])[$Field];
    }

    if ((is_array($ModuleDefs[$Module]["VIEW_FIELD_ARRAY"]) &&
        in_array($Field, $ModuleDefs[$Module]["VIEW_FIELD_ARRAY"])))
    {
        $Table = $ModuleDefs[$Module]["VIEW"];
    }
    elseif ((is_array($ModuleDefs[$Module]["FIELD_ARRAY"]) &&
        in_array($Field, $ModuleDefs[$Module]["FIELD_ARRAY"])) || $Field == 'recordid')
    {
        $Table = $ModuleDefs[$Module]["TABLE"];
    }
    else
    {
        //Cache this result in the session
        if (!isset($_SESSION['TableForFields']))
        {
            $sql = 'SELECT fdr_name, fdr_table FROM field_directory WHERE fdr_table != :Table';
            $PDOParams = array('Table' => $ModuleDefs[$Module]["TABLE"]);

            $Rows = DatixDBQuery::PDO_fetch_all($sql, $PDOParams);

            foreach ($Rows as $Row)
            {
                if (!isset($_SESSION['TableForFields'][$Row['fdr_name']]))
                {
                    $_SESSION['TableForFields'][$Row['fdr_name']] = $Row['fdr_table'];
                }
            }
        }

        $Table = $_SESSION['TableForFields'][$Field];

    }

    return $Table;
}

function GetTableViewForField($Field, $Module)
{
    global $ModuleDefs;

    if ($ModuleDefs[$Module]["VIEW"])
    {
        $Table = $ModuleDefs[$Module]["VIEW"];
    }
    else
    {
        $Table = GetTableForField($Field, $Module);
    }

    return $Table;
}

function ValidateLinkedContactData($Data, $module)
{
    $MaxLinkedContacts = max($Data["contact_max_suffix"],$Data["increp_contacts"]);

    $error = array();

    if ($MaxLinkedContacts == "")
    {
        return array();
    }

    // increp_contacts contains the number of additional contacts on the form.
    // Additional contacts are called e.g. con_surname_1, i.e. starting at 1

    //For incidents this used to be set to a minumum of 6. Changed to ensure reporter (3) is validated, and not sure
    //why 1-5 should have been ignored previously.
    $ContactNumber = 1;

    while ($ContactNumber <= $MaxLinkedContacts)
    {
        if (!ContactHidden($Data, $ContactNumber, $module) && !ContactNotFilledIn($Data, $ContactNumber))
        {
            $NewErrors = ValidateContactData(array('data' => $Data, 'suffix' => $ContactNumber));
            $injuryErrors =  ValidateInjuryData(array('data' => $Data, 'suffixstring' => '_'.$ContactNumber));

            $error = array_merge($error, $NewErrors, $injuryErrors);
        }

        $ContactNumber++;
    }

    return $error;
}

/**
* returns custom where clauses to be tacked on to security for different modules
*/

function getModuleWhereClause($module, &$connector, $recursiveHistory)
{
    global $ModuleDefs;

    $WhereClauses = array();

    switch ($module)
    {
        case 'DST':
            $WhereClauses['AND'][] = "((dst_private != 'Y' OR dst_private IS NULL OR dst_private = '') OR (dst_owner = ". $_SESSION["contact_login_id"] . " OR dst_owner IS NULL))";
            break;
        case 'LOC':
            $LOCClause = MakeSecurityWhereClause('', 'LOC','','','','','','',$recursiveHistory);

            // The following condition matches (the security group where clause) against
            // the current location OR any of its parents.
            // fn_UnderLocationHierarchy is a function used to look for children instead, so
            // they should not be used together.

            if ($LOCClause && stripos ($LOCClause, 'fn_UnderLocationHierarchy') === FALSE)
            {
                $WhereClauses['OR'][] = "(vw_locations_main.recordid IN (SELECT loc_main_link.recordid from vw_locations_main JOIN vw_locations_main loc_main_link on loc_main_link.lft>=vw_locations_main.lft AND loc_main_link.rght<=vw_locations_main.rght WHERE ".$LOCClause."))";
            }

            if (!IsFullAdmin())
            {
                $WhereClauses['AND'][] = 'vw_locations_main.loc_active != \'N\' OR vw_locations_main.loc_active IS NULL';
            }
            break;
        case 'CQO':
            if (!in_array('CQP', $recursiveHistory))
            {
                $CQPClause = MakeSecurityWhereClause('', 'CQP','','','','','','',$recursiveHistory);
            }

            if (!in_array('LOC', $recursiveHistory))
            {
                $LOCClause = MakeSecurityWhereClause('', 'LOC','','','','','','',$recursiveHistory);
            }

            if (GetAccessLevel('CQP') && $CQPClause)
            {
                $Where = "(cqc_outcomes.recordid IN (SELECT cqc_outcome_id from cqc_prompts WHERE ".$CQPClause."))";
            }

            if (GetAccessLevel('LOC') && $LOCClause)
            {
                $Where .= (GetAccessLevel('CQP') && $CQPClause ? " OR " : "")."(cqc_outcomes.cdo_location IN (SELECT vw_locations_main.recordid from vw_locations_main WHERE ".$LOCClause."))";
            }

            if ($Where != '')
            {
                $WhereClauses['OR'][] = $Where;
            }

            $WhereClauses['AND'][] = 'cqc_outcomes.cdo_location NOT IN (SELECT vw_locations_main.recordid from vw_locations_main WHERE vw_locations_main.loc_active = \'N\')';
            break;
        case 'CQP':
            if (!in_array('CQO', $recursiveHistory))
            {
                $CQOClause = MakeSecurityWhereClause('', 'CQO','','','','','','',$recursiveHistory);
            }

            if (!in_array('CQS', $recursiveHistory))
            {
                $CQSClause = MakeSecurityWhereClause('', 'CQS','','','','','','',$recursiveHistory);
            }

            if (GetAccessLevel('CQO') && $CQOClause)
            {
                $Where = "(cqc_prompts.cqc_outcome_id IN (SELECT recordid FROM cqc_outcomes WHERE ".$CQOClause."))";
            }

            if (GetAccessLevel('CQS') && $CQSClause)
            {
                $Where .= (GetAccessLevel('CQO') && $CQOClause ? " OR " : "")."(cqc_prompts.recordid IN (SELECT cqc_prompt_id from cqc_subprompts WHERE ".$CQSClause."))";
            }

            if ($Where != '')
            {
                $WhereClauses['OR'][] = $Where;
            }

            $WhereClauses['AND'][] = 'cqc_prompts.cqc_outcome_id NOT IN (SELECT recordid FROM cqc_outcomes WHERE cdo_location IN (SELECT vw_locations_main.recordid from vw_locations_main WHERE vw_locations_main.loc_active = \'N\'))';
            break;
        case 'CQS':
            if (!in_array('CQP', $recursiveHistory))
            {
                $CQPClause = MakeSecurityWhereClause('', 'CQP','','','','','','',$recursiveHistory);
            }

            if (GetAccessLevel('CQP') && $CQPClause)
            {
                $WhereClauses['OR'][] = "(cqc_subprompts.cqc_prompt_id IN (SELECT recordid FROM cqc_prompts WHERE ".$CQPClause."))";
            }

            $WhereClauses['AND'][] = 'cqc_subprompts.cqc_prompt_id NOT IN (SELECT recordid FROM cqc_prompts WHERE cqc_prompts.cqc_outcome_id IN (SELECT recordid FROM cqc_outcomes WHERE cdo_location IN (SELECT vw_locations_main.recordid from vw_locations_main WHERE vw_locations_main.loc_active = \'N\')))';
            break;
        case 'AQU':
            if (!in_array('AMO', $recursiveHistory))
            {
                $AMOClause = MakeSecurityWhereClause('', 'AMO', '', '', '', '', '', '', $recursiveHistory);
            }

            if (GetAccessLevel('AMO') && $AMOClause)
            {
                $WhereClauses['OR'][] = "(asm_questions.asm_module_id IN (SELECT recordid FROM asm_modules WHERE " . $AMOClause . "))";
            }
            break;
        case 'ACT':
            $LicModules = getActionLinkModules();
            if (!empty($LicModules))
            {
            	$WhereClauses['AND'][] = "(act_module IN ('" . implode("', '", $LicModules) . "'))";
            }
            break;
    }

    if (!empty($ModuleDefs[$module]['LINKED_MODULE']['parent_ids']))
    {
        $WhereIds = array ();

        foreach ($ModuleDefs[$module]['LINKED_MODULE']['parent_ids'] as $id)
        {
            $WhereIds[] = $id . ' > 0';
        }

        $WhereClauses['AND'][] = '(' . implode(' OR ', $WhereIds) . ')';
    }

    return $WhereClauses;
}

/**
* Determines whether the current user has permission to delete a generic record (only used in payments atm)
*/
function CanDeleteRecord($Module)
{
    return (in_array($Module, array('PAY')) && CanSeeModule($Module) && bYN(GetParm('DELETE_'.$Module, 'N')));
}

/**
* Returns a field containing fields which should be available to sum data over.
*/
function GetNumericReportField($Parameters)
{
    $module = $Parameters['module'];
    $fformats_table = $Parameters['fformats_table'];
    $name = $Parameters['name'];
    $rep = $Parameters['data'];

    $NumericExclusions = array(
        'inc_dreported-inc_dincident',
        'inc_dsched-inc_dincident',
        'inc_dsched-inc_dopened',
        'seclevel',
        'ram_parent',
        'com_dclosed-com_dreceived',
        'pal_dclosed-pal_dreceived',
        'asm_module_template_id',
        'asm_template_instance_id',
        'asm_question_template_id',
        'asm_module_id',
    );

    // Get reportable numeric fields (money and number)
    $sql = "SELECT fmt_field, fmt_new_label
        FROM vw_field_formats
        WHERE fmt_module = :module1
        AND fmt_table = :ff_table1
        AND
            (fmt_data_type in ('M','N'))
        AND fmt_field NOT IN
        (SELECT rex_field FROM report_exclusions
            WHERE rex_module = :module2
        AND rex_fieldset = 0)
        AND fmt_field NOT IN ('".implode("','",GetHardCodedReportExcludedFields())."')
        AND fmt_field NOT IN ('".implode("','",$NumericExclusions)."')
        AND fmt_field NOT LIKE '%.recordid%'
        AND fmt_field NOT LIKE 'recordid'
        UNION
        SELECT ('UDF_' + CAST(udf_fields.recordid AS VARCHAR(32))) as fmt_field, fld_name as fmt_new_label
        FROM udf_fields
        LEFT JOIN udf_mod_link ON udf_mod_link.uml_id = udf_fields.recordid
        WHERE
        fld_type in ('M','N')
        AND (udf_mod_link.uml_module = :module3 OR udf_mod_link.uml_module IS NULL OR udf_mod_link.uml_module = '')
        AND ('UDF_' + CAST(udf_fields.recordid AS VARCHAR(32))) NOT IN
        (SELECT rex_field FROM report_exclusions
            WHERE rex_module = :module4
        AND rex_fieldset = 0)
        ORDER BY 2 ASC";

    $SumFields = PDO_fetch_all($sql, array(
        'module1' => $module,
        'module2' => $module,
        'module3' => $module,
        'module4' => $module,
        'ff_table1' => $fformats_table,
        'ff_table2' => $fformats_table,
        'ff_table3' => $fformats_table
    ), PDO::FETCH_KEY_PAIR);

    //If we're in claims, get payment summary fields
    if ($module == 'CLA')
    {
        $PaymentTypes = DatixDBQuery::PDO_fetch_all('SELECT code, description from code_fin_type');

        foreach($PaymentTypes as $PaymentType)
        {
            $SumFields['pay_type_'.$PaymentType['code']] = $PaymentType['description'].' (Payment Summary)';
        }

        $SumFields['pay_type_PAYTOTAL'] = 'Total (Payment Summary)';
    }

    $SumField = Forms_SelectFieldFactory::createSelectField($name, 'ADM', $rep[$name], '', false, 'Field to sum');
    $SumField->setSuppressCodeDisplay();
    $SumField->setCustomCodes($SumFields);

    return $SumField;
}

/**
* @desc Formats a money value, adding currency, commas and correct decimal places.
*/
function FormatMoneyVal($value, $escape = true)
{
    if (!isset($value) || !is_numeric($value))
    {
        return '';
    }
    if ($escape)
    {
        if ($value >= 0)
        {
            $value = htmlspecialchars(GetParm('CURRENCY_CHAR', '£'), ENT_COMPAT | ENT_HTML401).number_format($value, 2);
        }
        else
        {
            $value = '-'.htmlspecialchars(GetParm('CURRENCY_CHAR', '£'), ENT_COMPAT | ENT_HTML401).number_format(floatval($value*-1),2);
        }
    }
    else
    {
        if ($value >= 0)
        {
            $value = GetParm('CURRENCY_CHAR', '£').number_format($value, 2);
        }
        else
        {
            $value = '-'.GetParm('CURRENCY_CHAR', '£').number_format(floatval($value*-1),2);
        }
    }

    return $value;
}

/**
* @desc Translation shortcut function
*/
function _t($text)
{
    return System_Translate::getTranslator()->translate(array('text' => $text));
}

/**
* @desc Key Translation shortcut function
*/
function _tk($key)
{
    return System_Translate::getTranslator()->translate(array('key' => $key));
}

/**
* Simply retrieves data from a table given it's ID, ID field and table
*
* @param int $ID ID of record
* @param string $FieldDataToGet field containing data to retrieve
* @param string $Table table name
* @param string $ID_field ID field name within table.
* @return mixed data contained in $FieldDataToGet field
*/
function getFieldDataFromID($ID, $FieldDataToGet, $Table, $ID_field = 'recordid')
{
    $sql = "SELECT " . $FieldDataToGet . " FROM $Table WHERE $ID_field = :id";

    $row = DatixDBQuery::PDO_fetch($sql, array("id" => $ID));
    return $row[$FieldDataToGet];
}

/**
 * Add a popup to warn the user that a change was made if he tries to navigate away from the form
 *
 * @param $href The URL to navigate to
 * @return string The validation string
 */
function addCheckChangeValidation($href)
{
    return "javascript:if(CheckChange()){SendTo('" . $href . "');}";
}

/**
 * @param $recordid
 * @return array Human-readable name and parent id for the location with id provided as a parameter.
 */
function getUpperTierInfo($recordid)
{
    /**
     * TODO: When memory caching introduced, use this.
     */
    //$memoryCache = \src\framework\registry\Registry::getInstance()->getMemoryCache();
    //$upperTierInfo = $memoryCache->get('location_hierarchy:parenting:'.$locArray['parent_id']);
    $upperTierInfo = $_SESSION['location_hierarchy:parenting:'.$recordid];

    if (empty($upperTierInfo))
    {
        $sql = '
                SELECT
                    recordid,
                    loc_name,
                    parent_id
                FROM
                    vw_locations_main
                WHERE
                    recordid = :parent_id
            ';

        $upperTierInfo = DatixDBQuery::PDO_fetch($sql, array('parent_id' => $recordid));

        $_SESSION['location_hierarchy:parenting:'.$recordid] = $upperTierInfo;
        //$memoryCache->set('location_hierarchy:parenting:'.$locArray['parent_id'], $upperTierInfo);
    }

    return $upperTierInfo;
}

/**
 * Add upper tier info to a set of locations
 *
 * e.g: Organisation 1 > Site 1 > Unit 1
 *
 * @param $locationsArray The set of locations
 * @return mixed The same set of locations with upper tier info per location
 */
function addUpperTierInfo($locationsArray)
{
    foreach ($locationsArray as $id => $locArray)
    {
        if ($locArray['parent_id'])
        {
            $locationsArray[$id]['upperTierInfo'] = array();

            $upperTierInfo = getUpperTierInfo($locArray['parent_id']);

            array_push($locationsArray[$id]['upperTierInfo'], $upperTierInfo['loc_name']);

            while ($upperTierInfo['parent_id'])
            {
                $upperTierInfo = getUpperTierInfo($upperTierInfo['parent_id']);

                array_push($locationsArray[$id]['upperTierInfo'], $upperTierInfo['loc_name']);
            }
        }

        $locName = '';

        if (!empty($locationsArray[$id]['upperTierInfo']))
        {
            $locationsArray[$id]['upperTierInfo'] = array_reverse($locationsArray[$id]['upperTierInfo']);

            foreach ($locationsArray[$id]['upperTierInfo'] as $info)
            {
                $locName.= $info . ' > ';
            }
        }

        $locationsArray[$id]['loc_name'] = $locName . $locationsArray[$id]['loc_name'];
        unset($locationsArray[$id]['upperTierInfo']);
    }

    return $locationsArray;
}

/**
* For a location id, finds the full parentage and presents it as a > delimited string
*
* @param int $recordid The id of the location
* @return mixed
*/
function getLocationParentage($recordid)
{
    if ($_GET['action'] == 'saverecord')
    {
        // suppress this function for performance reasons when saving the record
        return;
    }

    $hier = array_pop(addUpperTierInfo([getUpperTierInfo($recordid)]));

    return $hier['loc_name'];
}

function GetAdmYearPromptText()
{
    $AdmYear = Forms_SelectFieldFactory::createSelectField('adm_year', 'ATI', '', '');

    echo '<ol>'.GetDivFieldHTML('Assessment period', $AdmYear->getField()).'</ol>';
}

/**
 * Helper function
 *
 * Helper function to generate the select part of the SQL query (field names selected with AS key-values)
 * used to gather data from database based on merge codes.
 *
 * @param string $outer_glue The character string used for the outer glue when imploding the array to string.
 * (it's a single quote)
 * @param array $array Array which contains the fields as values and and their aliases as keys to be selected
 * in the SQL query.
 *
 * @return string The select part of the SQL query.
 */
function implode_select_as_key($outer_glue, $array)
{
    $output = array();

    foreach($array as $key => $item)
    {
        $output[] = $item . ' as [' . $key . ']';
    }

    return implode($outer_glue, $output);
}

/**
 * Function to create multi select Field for locations, used in accredition module
 * 
 * @param array  $data The existing record data
 * @param string $mode The mode the field is in (e.g. ReadOnly)
 *
 * @return string The HTML markup for the additional locations field
 */
function getLocationControl($data, $FormMode = '', $FormDesign = null)
{
    if ($_GET['action'] == 'saverecord')
    {
        // suppress this function for performance reasons when saving the record
        return;
    }
    
	$FieldMode = $FormMode;

    //is the field marked as read-only?
    if ($FormDesign->ReadOnlyFields['ati_location_additional'] == true)
    {
        $FieldMode = 'ReadOnly';
    }

    //is the section marked as read-only?
    if (($FormDesign->MoveFieldsToSections['ati_location_additional'] &&
        $FormDesign->ReadOnlyFields[$FormDesign->MoveFieldsToSections['ati_location_additional']['New']])
        || (!$FormDesign->MoveFieldsToSections['ati_location_additional'] && $FormDesign->ReadOnlyFields['module']))
    {
        $FieldMode = 'ReadOnly';
    }

    $activeWhere = "
	(loc_active != 'N' OR  loc_active is null)
	AND
	(loc_active_inherit != 'N' OR  loc_active_inherit is null)";

    if (isset($data['ati_location']))
    {
	    $activeWhere .= "AND vw_locations_main.recordid != ".$data['ati_location'];
    }

    $DropdownLocations = getUserLocations($activeWhere);
    $userLocations = getUserLocations();

    // unset locations
    $ValidatedLocations = locationRestriction($userLocations, $data['ati_location_additional']);

    // proper format for next function
    $values = implode(' ', array_keys($ValidatedLocations));
    $LocationSelect = Forms_SelectFieldFactory::createSelectField(
        'ati_location_additional',
        'ATI',
        $values,
        $FieldMode,
        true,
        '',
        'module'
    );
    
    // Filter locations that are not descendants of the Assigned Assessment Template
    if (isset($data['recordid']) && $_GET['module'] == 'ATI')
    {
    	$AssignedAssessmentId = array();
    	$AssignedAssessmentId[] = $data['recordid'];
    	
    	foreach ($DropdownLocations as $LocationId => $LocationLabel)
    	{
    		if (ASM_Template::showLocationIfIsDescendant($AssignedAssessmentId, $LocationId) == false)
    		{
    			unset($DropdownLocations[$LocationId]);
    		}
    	}
    	
    	if (is_array($DropdownLocations))
    	{
    		$LocationSelect->setCustomCodes($DropdownLocations);
    	}
    }

    $LocationSelect->setSuppressCodeDisplay();
    $LocationSelect->setAddChangedFlag(false);

    return $LocationSelect->getField();
}

/**
 * Function to create multi select Field for locations, used in Admin for User record
 *
 * @return SelectField
 */
function getLocationsControlAdmin($name,$value,$module,$mode = '')
{
    $activeWhere = "
    (loc_active != 'N' OR  loc_active is null)
    AND
    (loc_active_inherit != 'N' OR  loc_active_inherit is null)
    ";

    $DropdownLocations = getUserLocations($activeWhere);
    $userLocations = getUserLocations();

    // unset locations
    $ValidatedLocations = locationRestriction($userLocations, $value);

    // proper format for next function
    $values = implode(' ', array_keys($ValidatedLocations));

    $LocationSelect = Forms_SelectFieldFactory::createSelectField($name, $module, $values, $mode, true);

    if (is_array($DropdownLocations))
    {
        $LocationSelect->setCustomCodes($DropdownLocations);
    }

    $LocationSelect->setSuppressCodeDisplay();
    $LocationSelect->setAddChangedFlag(false);

    return $LocationSelect->getField();
}

/**
 * Function to validate that is not possible to simultaneously select locations at multiple tiers when generating
 * instances from an Assigned Assessment Template
 *
 * @param $data $_POST array
 * @return bool
 */
function validateMultipleTiers($locations)
{
	if ($locations)
	{
	    $sql = '
	        SELECT
	            loc_tier_depth
	        FROM
	            vw_locations_main
	        WHERE
	            recordid in (' . implode(', ', $locations) . ')
	        GROUP BY
	            loc_tier_depth
	    ';

	    $nodes = DatixDBQuery::PDO_fetch_all($sql, array());

	    return (sizeof($nodes) > 1 ? false : true);
	}
    else
    {
		return false;
	}
}

/**
* @desc Function to get specific user locations with $sql where clouse
*
* @param string $sql security where clouse
* @param array $initials for which user
* @return array $codes locations
*/
function getUserLocations($sql = "", $initials = null)
{
	if (!$initials)
	{
		$initials = $_SESSION['initials'];
	}

	$codes = array();

	$LocSecWhere = MakeSecurityWhereClause($sql, 'LOC', $initials);

    $sql2 = 'SELECT recordid FROM vw_locations_main'.($LocSecWhere ? ' WHERE '.$LocSecWhere : '');
	$Locations = DatixDBQuery::PDO_fetch_all($sql2, array(), PDO::FETCH_COLUMN);

	foreach ($Locations as $LocationID)
	{
		$codes[$LocationID] = getLocationParentage($LocationID);
	}

    return $codes;
}

/**
* @desc Function to validate intersection between user locations that have acces to and current locations
*
* @param array|string$userLocations locations that user have access to
* @param array|string $currentlocations Current locarions from DB
* @return array intersection between user locations that have acces to and current locations
*/
function locationRestriction($userLocations, $currentLocations)
{
	$codes = array();

	if (!is_array($currentLocations))
	{
		$currentLocations = explode(' ', $currentLocations);
	}

	foreach ($currentLocations as $LocationID)
	{
		$codes[$LocationID] = getLocationParentage($LocationID);
	}

	// choosing only those locations that user have access
	$validatedValues = array_intersect_key($userLocations, $codes);

	return $validatedValues;
}

/**
* @desc Function adds suppressed locations that user have got no access to
*
* @param array|string $newLocations Locations currently populated from the form
* @param array|string $currentlocations Current locarions from DB
* @return array $newLocations $newLocations + suppressed locations
*/
function AddRestrictedLocations($newLocations, $currentlocations)
{
	if ((is_array($currentlocations) && count($currentlocations) == 0) || !$currentlocations)
	{ // for performace
		return $newLocations;
	}

	if (!is_array($newLocations))
	{
		$newLocations = explode(' ', $newLocations); // format [] => id
	}

	if (!is_array($currentlocations))
	{
		$currentlocations = explode(' ', $currentlocations); // format [] => id
	}

	// check which locations user have access to
	$userLocations = getUserLocations(); // format [id] => name

	// check which locations have beed suppressed
	$validatedLocations = locationRestriction($userLocations, $currentlocations); // format [id] => name
	$validatedLocations = array_keys($validatedLocations);
	$hiddenLocations = array_diff($currentlocations, $validatedLocations);

	// add those locations
	$newLocations = array_merge($newLocations, $hiddenLocations);

	// proper format for next function
	$newLocations = implode(' ', $newLocations);

	return $newLocations;
}

/**
 * @desc Function that returns Assessment Template data based on a passed recordid
 *
 * @param $recordid The id of the Assessment Template
 * @return array Data of the selected Assessment Template
 */
function getAssessmentTemplateData($recordid)
{
    $sql = '
        SELECT
            atm_ref,
            atm_version,
            atm_category,
            atm_title,
            atm_description,
            atm_notes,
            atm_staff
        FROM
            asm_template_modules
        WHERE
            recordid = :recordid
    ';

    return DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));
}

/**
 * @desc Function that shows the correct sub-module suffix
 *
 * @param $fieldName The name of the field (e.g. 'ati_staff')
 * @param $fieldLabel The label of the field (e.g. 'Staff (Assigned Assessment Templates)')
 * @param $module
 * @return mixed|string
 */
function displayCorrectSubModuleSuffix($fieldName, $fieldLabel, $module)
{
    global $FieldDefs, $ModuleDefs;

    if (preg_match('/^.*\((\D*)\)/iu', $fieldLabel, $result))
    {
        if (isset($FieldDefs[$module][$fieldName]['SubmoduleSuffix']))
        {
            $submoduleSuffix = \UnicodeString::substr($FieldDefs[$module][$fieldName]['SubmoduleSuffix'], 0, -1);
            $submoduleSuffix = \UnicodeString::substr($submoduleSuffix, 1);

            if (strcmp($submoduleSuffix, $module) != 0)
            {
                $fieldLabel = str_replace($result[1], $submoduleSuffix, $fieldLabel);
            }
        }
        else
        {
            if (strcmp($result[1], $ModuleDefs[$module]['NAME']) == 0)
            {
                $fieldLabel = str_replace($result[1], '', $fieldLabel);
                $fieldLabel = \UnicodeString::substr($fieldLabel, 0, -2);
            }
        }
    }

    return $fieldLabel;
}

/**
 * @desc Function that returns the modules to exclude for Actions and Action Chains
 *
 * @param string $module actions/action_chains
 * @return array $ExcludeArray Array of modules that doesn't allow creation of Actions or Action Chains
 */
function getExcludeArray($module)
{
    global $ModuleDefs;

    switch ($module)
    {
        case 'actions':
            $IncludeArray = $ModuleDefs['ACT']['CAN_LINK_ACTIONS'];
            break;
        case 'action_chains':
            $IncludeArray = $ModuleDefs['ACT']['CAN_LINK_ACTIONS_CHAINS'];
            break;
    }

    $ExcludeArray = array();
    $AllModules = getModArray();

    foreach($AllModules as $ModKey => $ModLabel)
    {
        if (!in_array($ModKey, $IncludeArray))
        {
            $ExcludeArray[] = $ModKey;
        }
    }

    return $ExcludeArray;
}

/**
 * @desc Gets the module that should be displayed by default when a choice of modules is available
 * (e.g. Level 1, form design)
 * (normally this will be INC, but some clients may not have this module licenced).
 *
 * @param array $aParams Array of parameters.
 * @param string $aParams['default'] The value specified by the user, which will override the normal
 * ordering if it is valid.
 *
 * @return string The module code for the module for which the form should be displayed.
 */
function GetDefaultModule($aParams = array())
{
    global $ModuleDefs, $ModuleListOrder;

    if ($aParams['default'] && ModIsLicensed($aParams['default']))
    {
        //There are different requirements for logged in and logged out scenarios.
        if ($_SESSION['logged_in'])
        {
            if (($aParams['default'] == 'CQC' || $ModuleDefs[$aParams['default']]['MODULE_GROUP'] == 'CQC') &&
                (CanSeeModule('CQO') || CanSeeModule('CQP') || CanSeeModule('CQS')))
            {
                return $aParams['default'];
            }

            if (($aParams['default'] == 'ACR' || $ModuleDefs[$aParams['default']]['MODULE_GROUP'] == 'ACR') &&
                (CanSeeModule('AMO') || CanSeeModule('AQU') || CanSeeModule('ATI') || CanSeeModule('ATM') ||
                    CanSeeModule('ATQ')))
            {
                return $aParams['default'];
            }

            if (CanSeeModule($aParams['default']))
            {
                return $aParams['default'];
            }
        }

        if (!$_SESSION['logged_in'] && !bYN(GetParm($ModuleDefs[$aParams['default']]['NO_LEVEL1_GLOBAL'])))
        {
            return $aParams['default'];
        }
    }

    if (!$_SESSION['logged_in'] && bYN(GetParm($ModuleDefs[$aParams['default']]['NO_LEVEL1_GLOBAL'])))
    {
        return null;
    }


    //no valid default provided.
    foreach ($ModuleListOrder as $Module)
    {
        if (ModIsLicensed($Module))
        {
            //There are different requirements for logged in and logged out scenarios.
            if ($_SESSION['logged_in'] && CanSeeModule($Module) && (!$aParams['needs_homescreen'] ||
                $ModuleDefs[$Module]['HAS_HOMESCREEN'] !== false))
            {
                return $Module;
            }
            elseif (!$_SESSION['logged_in'] && !bYN(GetParm($ModuleDefs[$Module]['NO_LEVEL1_GLOBAL'])))
            {
                return $Module;
            }
        }
    }

    return '';
}

/**
 * @desc Function that parses form data and decides if a checkbox should be checked/unchecked
 *
 * @param array $aFields Array of fields to modify.
 * @param array $aData Data array.
 *
 * @return array Form checkboxes checked/unchecked
 */
function CheckCheckboxesFromArray($aFields, $aData, $FormDesign = null)
{
    global $MandatoryFields;

    if ($FormDesign)
    {
        $MandatoryFields = $FormDesign->MandatoryFields;
    }

    $aValues = array();

    foreach ($aFields as $sFieldName)
    {
        // mandatory and read only checkboxes are yes/no fields, so ignore
        // read only fields are hard to detect (they might move, and either the field or section could be read only), so we use the CHANGED- flag for now
        if (!($MandatoryFields[$sFieldName] || $aData['CHANGED-'.$sFieldName] != '1'))
        {
            if (isset($aData[$sFieldName]))
            {
                $aValues[$sFieldName] = 'Y';
            }
            else if ($aData['show_field_'.$sFieldName] == 1)
            {
                $aValues[$sFieldName] = 'N';
            }   
        }
    }

    return $aValues;
}

/**
 * @param $module
 * @param $recordids
 * @param $udf_field_ids
 * @return array
 * Rubbish procedural solution to enable udf display in listings by pulling the data out only for the records that are required,
 * rather than risking messing up the core listing query. Used in e.g. BrowseList.php when we need to have the value formatted for display.
 */
function getUDFDisplayData($module, $recordids, $udf_field_ids)
{
    $udfData = array();

    if (empty($recordids) || empty($udf_field_ids))
    {
        return array();
    }

    $udfRawData = \DatixDBQuery::PDO_fetch_all(
        'SELECT cas_id, field_id, fld_type, fld_format, udv_string, udv_text, udv_number, udv_money, udv_date FROM udf_values
            LEFT JOIN udf_fields ON udf_fields.recordid = udf_values.field_id
            WHERE field_id IN ('.implode (',', $udf_field_ids).')
                    AND udf_values.cas_id IN ('.implode (',', $recordids).')
                    AND udf_values.mod_id = :mod_id AND group_id = 0',
        array('mod_id' => ModuleCodeToID($module)));

    foreach ($udfRawData as $udfRow)
    {
        $value = '';
        switch ($udfRow["fld_type"])
        {
            case "S":
                $value = $udfRow['udv_string'];
                break;
            case "Y":
            case "C":
                $value = code_descr_udf($udfRow['field_id'], $udfRow['udv_string'], $module);
                break;
            case "T":
                $multiCodes = array();
                foreach (explode(' ', $udfRow['udv_string']) as $multiCode)
                {
                    $multiCodes[] = code_descr_udf($udfRow['field_id'], $multiCode, $module);
                }
                $value = implode(', ', $multiCodes);
                break;
            case "D":
                $value = FormatDateVal($udfRow['udv_date']);
                break;
            case "N":
                //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                if ($udfRow['fld_format'] != "")
                {
                    $value = GuptaFormatEmulate($udfRow["udv_number"], $udfRow['fld_format']);
                }
                else
                {
                    $value = number_format(floatval($udfRow["udv_number"]), 2, '.', '');
                }
                break;
            case "M":
                $value = FormatMoneyVal($udfRow['udv_money']);
                break;
            case "L":
                $value = $udfRow['udv_text'];
                break;
        }

        $udfData[$udfRow['cas_id']][$udfRow['field_id']] = $value;
    }

    return $udfData;
}

/**
 * @param $module
 * @param $recordids
 * @param $udf_field_ids
 * @return array
 * Rubbish procedural solution to enable udf display in listings by pulling the data out only for the records that are required,
 * rather than risking messing up the core listing query. Used when we are going to use ListingDisplayClass to format for display,
 * so just returns the value.
 */
function getUDFData($module, $recordids, $udf_field_ids)
{
    $udfData = array();

    if (empty($recordids) || empty($udf_field_ids))
    {
        return array();
    }

    $udfRawData = \DatixDBQuery::PDO_fetch_all(
        'SELECT cas_id, field_id, fld_type, fld_format, udv_string, udv_text, udv_number, udv_money, udv_date FROM udf_values
            LEFT JOIN udf_fields ON udf_fields.recordid = udf_values.field_id
            WHERE field_id IN ('.implode (',', $udf_field_ids).')
                    AND udf_values.cas_id IN ('.implode (',', $recordids).')
                    AND udf_values.mod_id = :mod_id AND group_id = 0',
        array('mod_id' => ModuleCodeToID($module)));

    foreach ($udfRawData as $udfRow)
    {
        $value = '';
        switch ($udfRow["fld_type"])
        {
            case "S":
            case "Y":
            case "C":
            case "T":
                $value = $udfRow['udv_string'];
                break;
            case "D":
                $value = $udfRow['udv_date'];
                break;
            case "N":
                $value = $udfRow["udv_number"];
                break;
            case "M":
                $value = $udfRow['udv_money'];
                break;
            case "L":
                $value = $udfRow['udv_text'];
                break;
        }

        $udfData[$udfRow['cas_id']][$udfRow['field_id']] = $value;
    }

    return $udfData;
}


/*
 * Given a table, works out the most logical "module" for the record to belong to.
 * Will be needed more and more as we move towards classifying data by table.
 * Will probably need some hard-coded entries going forwards.
 */
function getModuleFromTable($table)
{
    global $ModuleDefs;

    if (preg_match('/^link_contacts_([A-Za-z]{3})_.$/', $table))
    {
        return 'CON';
    }

    foreach ($ModuleDefs as $Module => $Details)
    {
        if ($Details['TABLE'] == $table || $Details['VIEW'] == $table)
        {
            return $Module;
        }
    }
}

function CanUseCodeTags()
{
    return (bYN(GetParm('CCS2_INC', 'N')) || bYN(GetParm('CCS2_CLA', 'N')));
}

/**
 * @desc
 * makes the visual indication with locking control for central admin
 * @param string $InputFieldName
 * @param string $type
 * @param string $Div
 * @param array $LockFieldAtribs
 * @param JSON $dependency 
 * 
 * @return HTML img lock field
 */
function makeLockField($InputFieldName, $type, $Div, $LockFieldAtribs, $dependency = '{}')
{
	
	if (IsCentrallyAdminSys())
	{
        if ($LockFieldAtribs["$InputFieldName"]["$type"])
        {
            $val = 1;
            $icon = 'lock.png';
            $label = _tk('locked');
        }
        else
        {
            $val = 0;
            $icon = 'lock_open.png';
            $label = _tk('unlocked');
        }
        
        $id_ref = $type. '-' . $Div . '-' . $InputFieldName;
        
	    if (IsCentralAdmin())
        {
	        return ' <img style="margin: 2px; " OnClick="javascript:changeLock(\'lock-' . $id_ref . '\',this,' . $dependency . ');" id="lock-'.$id_ref . '-img" src="Images/icons/' . $icon . '" alt="'.$label.'" title="'.$label.'"/>
	        <input type="hidden" name="lock-'.$id_ref . '" id="lock-'.$id_ref . '" value="'. $val . '" />';
	    }
        elseif ($val)
        {
            return ' <img style="margin: 2px; " src="Images/icons/' . $icon . '" alt="'.$label.'" title="'.$label.'"><input type="hidden" name="lock-'.$id_ref . '" id="lock-'.$id_ref . '" value="'. $val . '" />';
        }
	}
}

/**
 * Returns the module id based on the module short name
 *
 * @param string $ModShortName The module short name (e.g. 'INC')
 * @return int $_SESSION["ModuleCache"][$ModShortName] Module id
 */
function GetModIDFromShortName($ModShortName)
{
    $ModShortName = $ModShortName == 'ADM' ? 'CON' : $ModShortName;

    if (!isset($_SESSION["ModuleCache"][$ModShortName]))
    {
        $sql = "
            SELECT
                mod_id
            FROM
                modules
            WHERE
                mod_module = :mod_module
        ";

        $row = DatixDBQuery::PDO_fetch($sql, array("mod_module" => $ModShortName));
        $_SESSION["ModuleCache"][$ModShortName] = $row["mod_id"];
    }

    return $_SESSION["ModuleCache"][$ModShortName];
}

/**
 * Simple debug function with nice, legiable output.
 *
 * @author gszucs
 * @return void
 */
function debug()
{
	global $dtxdebug;

	if (!$dtxdebug)
	{
		return;
	}

	echo '<pre style="background-color: #EFFBFB; font-size: 12px; margin: 5px; padding: 10px; border: 1px solid; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">';
	call_user_func_array('var_dump',func_get_args());
	echo '</pre>';
}

function getExtraFieldMapLink($module, $fieldName)
{
    global $scripturl;

    $html = '';
    $showLink = false;

    if ($fieldName)
    {
        require_once 'Source/libs/SelectFunctions.php';
        $_POST['module']['module'] = $module;
        $fieldList = getMapCodeFieldsList();

        foreach ($fieldList as $code => $description)
        {
            if ($code == $fieldName)
            {
                $showLink = true;
                break;
            }
        }

        if ($showLink)
        {
            $sql = 'SELECT fdr_code_table, fdr_table FROM field_directory WHERE fdr_name = :fdr_name';
            $result = DatixDBQuery::PDO_fetch($sql, ['fdr_name' => $fieldName]);

            $html = '
                <a href="'.$scripturl.'?action=codesetupsaction&code='.$result['fdr_code_table'].'&module='.$module.'&codefieldname='.$fieldName.'&fdr_table='.$result['fdr_table'].'&form_action=readonly&mapped_code=1&recordid='.$_GET['recordid'].'">
                    <b>'._tk('show_mapped_code_setup').'</b>
                </a>
            ';
        }
    }

    return $html;
}

/**
 * Function used to get hard coded excluded fields for listing reports.
 *
 * @return array
 */
function getNewHardCodedReportExcludedFields()
{
    return [
        'con_staff_include',
        'sta_pwd_change',
        'sta_user_type',
        'sab_issued_time',
        'act_model',
        'pay_vat',
        'act_chain_id',
        'act_chain_instance',
        'act_step_no',
        'act_start_after_step',
        'act_start_after_days_no',
        'act_due_days_no',
        'act_reminder_days_no'
    ];
}

/**
* @desc Populates $txt from language files, caching in session for future pageloads. Also replaces <datix> tags
* with the appropriate strings.
*/
function getLanguageArray()
{
    global $txt, $LanguageExtraFile, $LanguageFile, $DatixView;

    if (empty($_SESSION['txt']) || $GLOBALS['devNoCache'])
    {
        if (isset($LanguageFile))
        {
            require_once($LanguageFile);
        }
        else
        {
            require_once("english.php");
        }

        if (isset($LanguageExtraFile))
        {
            require_once($LanguageExtraFile);
        }

        foreach ($txt as $code => $text)
        {
            while (preg_match("/<datix\s+(\w+)>/", $txt[$code], $tags))
            {
                $txt[$code] = preg_replace("/<datix\s+$tags[1]>/",$txt[$tags[1]],$txt[$code]);
            }
        }

        mergeGlobalsIntoLanguageArray();

        $_SESSION['txt'] = $txt;
    }
    else
    {
        $txt = $_SESSION['txt'];
    }

    $DatixView->txt = $txt; //load english.php replacements into view class
}

/**
* @desc Replaces <global> tags in language files with the contents of the appropriate global.
*/
function mergeGlobalsIntoLanguageArray()
{
    global $txt;

    if (!empty($txt))
    {
        foreach ($txt as $code => $text)
        {
            while (preg_match("/<global\s+(\w+)>/", $txt[$code], $tags))
            {
                $txt[$code] = preg_replace("/<global\s+$tags[1]>/",GetParm($tags[1]),$txt[$code]);
            }
        }
    }
}
