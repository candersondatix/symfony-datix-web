<?php

use src\system\database\FieldInterface;


/**
* Contains methods for creating/displaying a variety of form field types.
*/
class FormField
{
    var $Field;
    var $DropdownWidth;
    var $FieldMode;	// Search, New, Update or Print
    var $Audit; // true or false
    var $extraRow; // additional custom output after the main field row
    var $isTablet;

    // fmt_table entry in field_formats belonging to this field (needed when name and module are not enough).
    var $ffTable;

    /**
    * FormField constructor.
    *
    * Instantiates the object and sets field mode and width.
    *
    * @param string $FieldModeParm  The field mode (New/Print/ReadOnly/Search/Locked).
    */
    function FormField($FieldModeParm = "New")
    {
        $this->FieldMode = $FieldModeParm;
        $this->DropdownWidth = DROPDOWN_WIDTH_DEFAULT;
        $this->isTablet = \src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet();
    }

    /**
    * Sets the field width.
    *
    * Defaults to unit "px".
    *
    * @param mixed  $DropdownWidthParm  The width value.
    * @param string $DropdownWidthUnit  The width unit.
    */
    function SetDropdownWidth($DropdownWidthParm, $DropdownWidthUnit = "px")
    {
        $this->DropdownWidth = $DropdownWidthParm;
        $this->DropdownWidthUnit = $DropdownWidthUnit;
    }

    /**
    * Gets the (HTML) string which comprises the field itself and all associated components.
    *
    * @return string $this->Field.
    */
    function GetField()
    {
        return $this->Field;
    }

    /**
    * Sets the field mode (New/Print/ReadOnly/Search/Locked).
    */
    function SetFieldMode($Mode)
    {
        $this->FieldMode = $Mode;
    }

    /**
    * Creates a dropdown field.
    *
    * Returns one of two types of field (standard or pop up) by checking global "DROPDOWN_POPUP"
    * and subsequently calling the appropriate dropdown creation method.
    *
    * @param string $module The code for this module.
    * @param string $field The field name/id.
    * @param string $current The current value for this field.
    * @param string $parent The name/id of the first parent (combo-linked) field.
    * @param string $child The name/id of the first child (combo-linked) field.
    * @param string $parent2 The name/id of the second parent (combo-linked) field.
    * @param string $child2 The name/id of the second child (combo-linked) field.
    * @param string $AltFieldName Overrides the standard field name; can be used when e.g. using a suffix
    *                             to identify multiple instances of the same field type (linked data).
    * @param boolean $ShowCodes Display codes as well as descriptions.
    * @param string $NPSA The code table column to use for NPSA mapping.
    * @param mixed $DefaultHidden If set the field is hidden and set to the default value.
    * @param mixed $OldCodes If true cod_priv_level code table column is used to filter out old codes.
    *                        Can alternatively be used to pass a where clause to filter out old codes.
    * @param string $ContactSuffix Unique identifier for this linked contact record.
    * @param string $onchangeExtra A custom action for this field's onchange event.
    * @param boolean $MultiListbox Whether or not this field is a multi-select list (DropdownPopup only).
    * @param string $customTitle Custom field title (DropdownPopup only).
    * @param string $mainModule Holds e.g. "INC" for link fields, since this is the module they are stored
    *                           under in fieldformats.
    *
    * @return object    $this               The form field.
    */
    function MakeDropdown(
        $module,
        $field,
        $current = "",
        $parent = "",
        $child = "",
        $parent2 = "",
        $child2 = "",
        $AltFieldName = "",
        $ShowCodes = false,
        $NPSA = "",
        $DefaultHidden = "",
        $OldCodes = false,
        $ContactSuffix = "",
        $onchangeExtra = "",
        $MultiListbox = false,
        $customTitle = '',
        $mainModule = ''
        )
    {
        if (bYN(GetParm("DROPDOWN_POPUP", "N")))
        {
            $this->MakeDropdownPopup(
                array(
                    "module" => $module,
                    "field" => $field,
                    "current" => $current,
                    "parent" => $parent,
                    "child" => $child,
                    "parent2" => $parent2,
                    "child2" => $child2,
                    "altfieldname" => $AltFieldName,
                    "showcodes" => $ShowCodes,
                    "defaulthidden" => $DefaultHidden,
                    "contactsuffix" => $ContactSuffix,
                    "onchangeextra" => $onchangeExtra,
                    "multilistbox" => $MultiListbox,
                    'customtitle' => $customTitle
                )
            );
        }
        else
        {
            $this->MakeDropdownStd(
                $module,
                $field,
                $current,
                $parent,
                $child,
                $parent2,
                $child2,
                $AltFieldName,
                $ShowCodes,
                $NPSA,
                $DefaultHidden,
                $OldCodes,
                $ContactSuffix,
                $onchangeExtra,
                $mainModule
            );
        }

        return $this;
    }

    /**
    * Creates a standard select field.
    *
    * @param string $module The code for this module.
    * @param string $field The field name/id.
    * @param string $current The current value for this field.
    * @param string $parent The name/id of the first parent (combo-linked) field.
    * @param string $child The name/id of the first child (combo-linked) field.
    * @param string $parent2 The name/id of the second parent (combo-linked) field.
    * @param string $child2 The name/id of the second child (combo-linked) field.
    * @param string $AltFieldName Overrides the standard field name; can be used when e.g. using a suffix
    *                             to identify multiple instances of the same field type (linked data).
    * @param boolean $ShowCodes Display codes as well as descriptions.
    * @param string $NPSA The code table column to use for NPSA mapping.
    * @param mixed $DefaultHidden If set the field is hidden and set to the default value.
    * @param mixed $OldCodes If true cod_priv_level code table column is used to filter out old codes.
    *                        Can alternatively be used to pass a where clause to filter out old codes.
    * @param string $ContactSuffix Unique identifier for this linked contact record.
    * @param string $onchangeExtra A custom action for this field's onchange event.
    * @param string $mainModule Holds e.g. "INC" for link fields, since this is the module they are stored
    *                           under in fieldformats.
    *
    * @return object    $this The form field.
    */
    function MakeDropdownStd(
        $module,
        $field,
        $current = "",
        $parent = "",
        $child = "",
        $parent2 = "",
        $child2 = "",
        $AltFieldName = "",
        $ShowCodes = false,
        $NPSA = "",
        $DefaultHidden = "",
        $OldCodes = false,
        $nContactSuffix = "",
        $onchangeExtra = "",
        $mainModule = '')
    {
        global $FieldDefs;
        // If $DefaultHidden is set, the field should be hidden and set to the
        // default value.  We need to set any dependencies too.

        $codeFieldName = $FieldDefs[$module][$field]['FieldFormatsName'] ? $FieldDefs[$module][$field]['FieldFormatsName'] : $field;

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly")
        {
            $this->Field = htmlspecialchars(code_descr($module, $codeFieldName, $current));
            $this->Field .= '<input type="hidden" name="' . $field . '" id="' . $field . '" value="' . $current . '"/>';
            return $this;
        }

        if (array_key_exists('WEB_SHOW_CODE', $_SESSION['Globals']) && $_SESSION['Globals']['WEB_SHOW_CODE'] == 'Y')
        {
            $ShowCodes = true;
        }

        if ($this->DropdownWidth > 0)
        {
            $StyleArray[] = 'width: '.$this->DropdownWidth.($this->DropdownWidthUnit ? $this->DropdownWidthUnit : 'px');
        }

        if ($DefaultHidden)
        {
            $StyleArray[] = "display: none";
        }

        $DoComboLinks = (!($this->FieldMode == "Search" && !bYN(GetParm('COMBO_LINK_IN_SEARCH', 'Y'))));

        if ($DoComboLinks)
        {
            // Get parent fields according to combolinks - You can only have a maximum of two parents
            $sql = "SELECT cmb_parent
                    FROM combo_links
                    WHERE cmb_child = '$codeFieldName' AND cmb_module = '$module'
                    ORDER BY recordid";
            $result = db_query($sql);

            if ($row = db_fetch_array($result))
            {
                if ($row["cmb_parent"])
                {
                    $ComboLinksParents[0]= $row["cmb_parent"];
                }

                if ($row = db_fetch_array($result))
                {
                    if ($row["cmb_parent"])
                    {
                        $ComboLinksParents[1]= $row["cmb_parent"];
                    }
                }
            }

	        // Get child fields according to combolinks
	        $sql = "SELECT cmb_child
			        FROM combo_links
			        WHERE cmb_parent = '$codeFieldName'
                    AND cmb_module = '$module'";
	        $result = db_query($sql);

	        while ($row = db_fetch_array($result))
            {
		        $ChildFF = getFieldFormat($row["cmb_child"], $module);

		        if ($ChildFF["fmt_data_type"] == "C" && $ChildFF["fmt_data_length"] == "254") //Multicode field.
		        {
			        $ComboLinksChildren[$row["cmb_child"]] = "Select".$row["cmb_child"];
		        }
		        else
		        {
			        $ComboLinksChildren[$row["cmb_child"]] = $row["cmb_child"];
		        }
	        }

	        // Also get the default children from the field_formats table as the
	        // combo links supplement rather than override these.
	        $sql = "SELECT fmt_field
			        FROM field_formats
			        WHERE fmt_module = '$mainModule'
			        AND (fmt_code_parent = '$codeFieldName'
			        OR fmt_code_parent2 = '$codeFieldName')";
	        $result = db_query($sql);

	        while ($row = db_fetch_array($result))
            {
		        $ComboLinksChildren[$row["fmt_field"]] = $row["fmt_field"];
            }
        }


	    $sql = "
	        SELECT
	            fmt_code_table, fmt_code_field, fmt_code_descr, fmt_code_order, fmt_code_parent,
	            fmt_code_parent2, fmt_code_where
	    	FROM
	    	    field_formats
	        WHERE
	            fmt_module ='$module'
	            AND fmt_field = '$codeFieldName'
        ";

	    $result = db_query($sql);

	    // Should only return one row.
	    if ($row = db_fetch_array($result))
	    {
	    	if ($DoComboLinks)
            {
                // Set the default parents from the field_format table
		        // Only use the field formats default parents if not overwridden by combolinks retrieved earlier
		        if ($row["fmt_code_parent"] && !isset($ComboLinksParents[0]))
                {
			        $ComboLinksParents[0] = $row["fmt_code_parent"];
                }

		        if ($row["fmt_code_parent2"] && !isset($ComboLinksParents[1]))
                {
			        $ComboLinksParents[1] = $row["fmt_code_parent2"];
                }
            }

		    $code_table = $row["fmt_code_table"];

		    if ($code_table{0} == '!')
		    {

			    $code_type = \UnicodeString::substr($code_table, 1);

			    $code_field = "cod_code";
			    $code_descr = "cod_descr";
			    $code_table = "code_types";

			    $sql = "
			        SELECT
			            cod_code, cod_descr, convert(text, cod_parent) as cod_parent,
			            convert(text, cod_parent2) as cod_parent2";

			    if ($NPSA != "")
                {
				    $sql .= ", $NPSA";
                }

			    if ($this->FieldMode == "Search")
                {
			        $sql .= "
				        FROM code_types
				        WHERE cod_type = '" . $code_type . "'
                        AND (cod_priv_level != 'X' OR cod_priv_level is NULL)
				        ORDER BY cod_listorder, cod_descr";
                }
			    else
                {
			        $sql .= "
				        FROM code_types
				        WHERE cod_type = '" . $code_type . "'
				        AND ((cod_priv_level <> 'N' AND cod_priv_level != 'X')
                        OR cod_priv_level IS NULL"
				        . ($current ? " OR $code_field = '$current'" : "") . ")
				        ORDER BY cod_listorder, cod_descr";
                }

		    }
		    else
		    {
			    $code_field = $row["fmt_code_field"];
			    $code_descr = $row["fmt_code_descr"];

			    $sql = "SELECT $row[fmt_code_field], $row[fmt_code_descr]";

			    if ($row["fmt_code_parent"] != "" || $ComboLinksParents != "")
                {
				    $sql .= ", convert(text, cod_parent) as cod_parent";
                }

			    if ($parent2 != "" || $ComboLinksParents)
                {
				    $sql .= ", convert(text, cod_parent2) as cod_parent2";
                }

			    if ($NPSA != "")
                {
				    $sql .= ", $NPSA";
                }

			    if ($code_table == "vw_staff_combos")
                {
				    $sql .= "
					    , sta_surname, sta_forenames, sta_title, jobtitle
					    FROM
					        $code_table
                        left join
                            code_parents on cop_field = '$codeFieldName' AND cop_code = initials";
                }
			    else
                {
				    $sql .= "
					    FROM $code_table";
                }


			    // If $OldCodes is true, use the standard cod_priv_level
			    // functionality to switch off old codes.  Otherwise,
			    // if it is not empty, use the supplied string as the WHERE
			    // clause.
                $Where = array();

			    if ($OldCodes === true && $this->FieldMode == "Search")
                {
                    $Where[] = "(cod_priv_level != 'X' OR cod_priv_level IS NULL)";
                }
			    elseif ($OldCodes === true && $code_table != "vw_staff_combos")
                {
                    $Where[] = "
                        ((cod_priv_level != 'N' AND cod_priv_level != 'X')
                        OR cod_priv_level IS NULL". ($current ? " OR $code_field = '$current'" : "") . ")";
                }
			    elseif ($OldCodes && $code_table != "vw_staff_combos")
                {
                    $Where[] = $OldCodes;
                }
			    elseif ($code_table == "vw_staff_combos")
			    {
                    $today = getdate();
                    $todayStr = "$today[year]-$today[mon]-$today[mday] 00:00:00";

				    $Where[] = "
				        (((cod_priv_level != 'N' AND cod_priv_level != 'X')
				        OR cod_priv_level IS NULL) AND ".GetActiveStaffWhereClause().')';

                    // need to filter staff according to their permissions on level one forms
                    if (!$_SESSION["logged_in"] || $_GET['action'] == 'newdif1' || $_GET['level'] == '1')
                    {
                        $levels = $module == 'INC' ? array('DIF2', 'RM') : array($module.'2', 'RM');
                        $Where[] = '(' . getContactListSQLByAccessLevel(
                            array(
                                'module' => $module,
                                'levels' => $levels,
                                'table' => $code_table,
                                'return_where' => true,
                            )
                        ) . ')';
                    }

				    $conPermWhere = MakeSecurityWhereClause("", "CON", $_SESSION["initials"]);

				    if ($conPermWhere)
                    {
					    $Where[] = "vw_staff_combos.recordid in
						    (SELECT recordid FROM contacts_main
						    WHERE $conPermWhere)";
			        }
			    }

                if (!empty($row['fmt_code_where']))
                {
                    $Where[] = $row['fmt_code_where'];
                }

                if (!empty($Where))
                {
                    $sql .= ' WHERE ' . implode(' AND ', $Where);
                }

			    if ($row["fmt_code_order"] != "" && $row["fmt_code_order"] != " " )
                {
				    $sql .= " ORDER BY $row[fmt_code_order] ASC";
                }
			    elseif ($code_table == "vw_staff_combos")
                {
                    if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
                    {
                        $sql .= " ORDER BY sta_surname, sta_forenames, sta_title ASC";
                    }
                    else
                    {
                        $sql .= " ORDER BY fullname";
                    }
                }
			    else
                {
				    $sql .= " ORDER BY cod_listorder, $code_table.$code_descr ASC";
		        }
		    }

		    $fieldname = $field;

		    if ($AltFieldName != "")
            {
			    $fieldname = $AltFieldName;
            }
		    elseif ($nContactSuffix)
		    {
			    $ContactSuffix = "_" . $nContactSuffix;
			    $fieldname .= $ContactSuffix;
		    }

		    if ($StyleArray)
		    {
			    $StyleString = implode(";", $StyleArray);
			    $StyleString = ' style="' . $StyleString . '"';
		    }

		    $output = '<select name="' . $fieldname . '" id="' . $fieldname
			    . '"' . $StyleString;

		    // Don't set combo links if we have altered the field name as a one-off
		    if ($ComboLinksChildren != "" && $AltFieldName == "")
            {
			    $children = $ComboLinksChildren;
            }
		    elseif ($AltFieldName == '')
		    {
			    if ($child != "")
                {
				    $children[] = $child;
                }

			    if ($child2 != "")
                {
				    $children[] = $child2;
		        }
		    }

		    // Likewise with parent links
		    if ($ComboLinksParents != "" && $AltFieldName == "")
            {
			    $parents = $ComboLinksParents;
            }
		    elseif ($AltFieldName == '')
		    {
			    if ($parent != "")
                {
				    $parents[] = $parent;
                }

			    if ($parent2 != "")
                {
				    $parents[] = $parent2;
		        }
		    }

		    if ($children != "")
		    {
			    $ParentSelected = "";

			    foreach ($children as $NewChild)
                {
				    $ParentSelected .= 'if (document.forms[0].' . $NewChild . $ContactSuffix .
				    ') {if(typeof ' . $NewChild . $ContactSuffix . 'Selected == \'function\'){' .
                    $NewChild . $ContactSuffix . 'Selected(false);}}';
		        }
		    }

		    // Only output the "onChange" if the parent isn't hidden.
		    if (!$DefaultHidden)
		    {
                $output .= ' onchange="FieldChanged(\''.$fieldname.'\');';

			    if ($ParentSelected)
                {
				    $output .= 'setChanged(\'' . $fieldname . '\');'
					    . $ParentSelected;
                }
			    else
                {
				    $output .= 'setChanged(\'' . $fieldname . '\');';
                }

                if ($onchangeExtra)
                {
                    $output .= $onchangeExtra;
                }

			    $output .= '"';
		    }

		    $output .= '>';

		    if ($DefaultHidden)
            {
			    $current = $DefaultHidden;
            }

		    // Remember, a child can have a maximum of two parents!
		    // If there are two parents, we can't do anything until the second
		    // parent contains a value, so we need to just return if it doesn't.
		    if ($parents != "")
		    {
			    $options = "
    function ${fieldname}Selected(ignore_parent){
	    var currentValue = document.forms[0].$fieldname.value;
	    for (var i = document.forms[0].$fieldname.length; i >= 0; i--){
		    document.forms[0].$fieldname.options[i] = null;
	    }
    ";
		        // Add the "choose" (empty) option
		        $options .= "document.forms[0].$fieldname.options[document.forms[0].$fieldname.length] = new Option('" . _tk('choose') . "','');\n";

		        $options .= "var parent = document.forms[0]." . $parents[0] . $ContactSuffix . ";\n";

		        // If the other parent does not contain a value, we can't really do anything
		        if ($parents[1])
                {
				    $options .= "var parent2 = document.forms[0]." . $parents[1]
					    . $ContactSuffix . ";\n"
					    . "if(parent2){
                        var parentArray = getValuesArray('".$parents[1] . $ContactSuffix."');
                        var currentOption2 = parentArray[0]; \n"
					    . "}\n";
                }

			    $options .= "if(parent){
                    var parentArray = getValuesArray('".$parents[0] . $ContactSuffix."');
                    var currentOption = parentArray[0]}";
		    }

		    $selectedRow = -1;
		    $result = db_query($sql);
		    $current_exists = false;

		    while (($row = db_fetch_array($result)) || ($current && $current_exists === false))
		    {
			    $i++;

                if (!$row && $current && $current_exists === false)
			    {
				    if (array_key_exists('WEB_SHOW_CODE', $_SESSION['Globals']) &&
                        $_SESSION['Globals']['WEB_SHOW_CODE'] == 'Y')
				    {
					    $row[$code_field] = $current;

					    if ($code_field != $code_descr)
                        {
						    $row[$code_descr] = _tk('sqb_deleted_sqb');
				        }
				    }
				    else
				    {
					    $row[$code_field] = $current;
					    $row[$code_descr] = "$current " . _tk('sqb_deleted_sqb');
				    }

				    $current_exists = true;
			    }

			    if ($current != "" && $row[$code_field] == $current)
			    {
				    $current_exists = true;
			    }

			    // Get array of NPSA mapping codes if required
			    $NPSACodes = "";

			    if ($NPSA != "" && $row[$NPSA] != "")
                {
				    $NPSACodes = explode(" ", $row[$NPSA]);
                }

                if ($code_table == "vw_staff_combos")
                {
                    if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
                    {
                        $row[$code_descr] = $row["sta_surname"].', '.$row["sta_forenames"].' '.$row["sta_title"];
                    }

                    if (GetParm('STAFF_NAME_DROPDOWN') != 'N')
                    {
                        switch (GetParm('STAFF_NAME_DROPDOWN'))
                        {
                            case 'B':
                                $row[$code_descr] = $row["jobtitle"] . ' - ' . $row[$code_descr];
                                break;
                            case 'A':
                            default:
                                $row[$code_descr] .= ' - ' . $row["jobtitle"];
                                break;
                        }
                    }
                }

                $row[$code_descr] = str_replace("'", "\\'", $row[$code_descr]);

			    if ($parents != "")
			    {
				    // Blank cod_parent means it applies to all
				    if ($row["cod_parent"] != "" || $row["cod_parent2"] != "")
				    {
					    // Need to deal with the case when a code has multiple parents
					    $parent_codes = explode(" ", $row["cod_parent"]);
					    $parent2_codes = "";

					    if ($parents[1] != "")
                        {
						    $parent2_codes = explode(" ", $row["cod_parent2"]);
                        }

					    // Only need to do the looping thing if we are showing the
					    // NPSA codes (i.e. need different combinations of parents
					    // and children to be unique
					    if ($NPSA == "")
					    {
						    $ParentOrs = "";
						    $ParentOrString = "";
						    $Parent2OrString = "";
						    $ParentOrStrings = "";

						    foreach ($parent_codes as $par)
						    {
							    if ($par)
                                {
								    $ParentOrs[] = "currentOption=='$par'";
						        }
						    }

						    if ($ParentOrs)
                            {
							    $ParentOrStrings[] = '(!parent||currentOption==\'\'||'.implode("||", $ParentOrs) . ')';
                            }

						    // Now do the same for parent2!
						    $Parent2Ors = "";

						    if ($parents[1] && $parent2_codes)
						    {
							    foreach ($parent2_codes as $par2)
							    {
								    if ($par2)
                                    {
									    $Parent2Ors[] = "currentOption2 == '$par2'";
							        }
							    }

							    if ($Parent2Ors)
                                {
								    $ParentOrStrings[] = '(!parent2||currentOption2==\'\'||' . implode("||", $Parent2Ors) . ')';
						        }
						    }

						    $options .= "if(";

						    if ($ParentOrStrings)
                            {
							    $options .= implode("&&", $ParentOrStrings);
                            }
						    else
                            {
							    $options .= "true";
                            }

						    $options .= "||ignore_parent==true&&'$row[$code_field]'=='$current'";

						    $options .= "){";
						    $options .= "document.forms[0].$fieldname.options[document.forms[0].$fieldname.length] = new Option('"
							    . $row[$code_descr];

						    if ($ShowCodes)
                            {
							    $options .= ': ' . $row[$code_field];
                            }

						    $options .= "','$row[$code_field]');\n";

						    // Set current option selected
						    // using Javascript
						    if ($row["$code_field"] == $current)
						    {
							    $options .= "document.forms[0].$fieldname.selectedIndex = document.forms[0].$fieldname.length - 1;\n";
						    }

						    $options .= "}\n";
					    }
					    else
					    {
						    $ParentCount = 0;

						    foreach ($parent_codes as $par)
						    {
							    $options .= "if(!parent||currentOption=='" . $par . "'";

							    // If there is a second parent field, it can only contain a single code.
							    if ($parents[1] != "")
                                {
								    $options .=
				    "&&document.forms[0]." . $parents[1] . $ContactSuffix . ".options[document.forms[0]." . $parents[1] . $ContactSuffix . ".selectedIndex].value == '" . $parent2_codes[0] . "'";
                                }

							    $options .= "||ignore_parent==true&&'$row[$code_field]'=='$current'";

							    $options .= "){";

							    $options .= "document.forms[0].$fieldname.options[document.forms[0].$fieldname.length]=new Option('"
						    . $row[$code_descr];

							    if ($ShowCodes)
                                {
								    $options .= ': ' . $row[$code_field];
                                }

							    // Add the NPSA mapping code to the description
							    if ($NPSACodes != "")
                                {
								    $options .= ' - ' . $NPSACodes[$ParentCount];
                                }

							    $options .= "','$row[$code_field]');\n";

							    // Set current option selected
							    // using Javascript
							    if ($row["$code_field"] == $current)
							    {
								    $options .= "document.forms[0].$fieldname.selectedIndex=document.forms[0].$fieldname.length - 1;\n";
							    }

							    $options .= "}\n";
							    $ParentCount++;
						    }
					    }

				    }
				    else // Parent field empty - applies to all
				    {
					    $options .= "document.forms[0].$fieldname.options[document.forms[0].$fieldname.length]=new Option('"
					    . $row[$code_descr];

					    if ($ShowCodes && $code_field != $code_descr)
                        {
						    $options .= ': ' . $row[$code_field];
                        }

					    if ($NPSACodes != "")
                        {
						    $options .= ' - ' . $NPSACodes[0];
                        }

					    $options .= "','$row[$code_field]');\n";

					    if ($row["$code_field"] == $current)
					    {
						    $options .= "document.forms[0].$fieldname.selectedIndex=document.forms[0].$fieldname.length - 1;\n";
					    }
				    }
			    }
			    else
			    {
				    $Descr = $row[$code_descr];

				    if ($ShowCodes)
                    {
					    $Descr .= ': ' . $row[$code_field];
                    }

				    if ($NPSACodes != "")
                    {
					    $Descr .= ' - ' . $NPSACodes[0];
                    }

				    $options .= '<option value="' . htmlspecialchars($row[$code_field], ENT_QUOTES) . '"';

				    if ($current != "" && $row[$code_field] == $current)
                    {
					    $options .= ' selected="selected"';
                    }

				    $options .= '>';
				    $options .= htmlspecialchars($Descr, ENT_QUOTES) . '</option>';
			    }
		    }

		    $output .= '<option value=""';

		    if (!$current)
            {
			    $output .= ' selected="selected"';
            }

		    $output .= '>' . _tk('choose') . '</option>';

		    if (!$parents)
            {
			    $output .= $options . '</select>';
            }
		    else
		    {
			    // Check whether the current selection was valid for the refreshed list
			    $options .= "if (currentValue != '') {";
			    $options .= "if (document.forms[0]." . $fieldname . ".options.item(currentValue)) {";
			    $options .= "document.forms[0]." . $fieldname . ".value = currentValue;}";
			    $options .= "else {document.forms[0]." . $fieldname . ".value = '';}}";
                $options .= "\n}\n";

			    $output .= "\n</select>\n<script language=\"JavaScript\" type=\"text/javascript\">\n";
			    $output .= $options;
			    $output .= "\n {$fieldname}Selected(true);\n";
			    $output .= "\n</script>";
		    }

		    // Call JavaScript child population function if $DefaultHidden is set
		    if ($DefaultHidden && $ParentSelected)
            {
			    $output .= '<script language="JavaScript" type="text/javascript">
    ' . $ParentSelected . '
    </script>';
	        }
	    }
	    else // in case of there is no field format definition for this field
	    {
		    $output = '<select name="' . $fieldname . '" id="' . $fieldname . '">';
		    $output .= '<option value=""';

		    if (!$current)
            {
			    $output .= ' selected="selected"';
            }

		    $output .= '>Choose</option>';
		    $output .= $options . '</select>';
	    }

	    $this->Field = $output;
	    return $this;
	}

    /**
    * Creates a new-style pop up select field.
    *
    * @global array
    * @global array
    * @global array
    *
    * @param string $aParams["module"] The code for this module.
    * @param string $aParams["field"] The field name/id.
    * @param string $aParams["current"] The current value for this field.
    * @param string $aParams["parent"] The name/id of the first parent (combo-linked) field.
    * @param string $aParams["child"] The name/id of the first child (combo-linked) field.
    * @param string $aParams["parent2"] The name/id of the second parent (combo-linked) field.
    * @param string $aParams["child2"] The name/id of the second child (combo-linked) field.
    * @param string $aParams["altfieldname"] Overrides the standard field name; can be used when e.g. using a suffix.
    *                                        to identify multiple instances of the same field type (linked data).
    * @param boolean $aParams["showcodes"] Display codes as well as descriptions.
    * @param string $aParams["NPSA"] The code table column to use for NPSA mapping.
    * @param mixed $aParams["defaulthidden"] If set the field is hidden and set to the default value
    *                                       (not sure if this parameter is used anymore).
    * @param string $aParams["contactsuffix"] Unique identifier for this linked contact record.
    * @param string $aParams["onchangeextra"] A custom action for this field's onchange event.
    * @param boolean $aParams["multilistbox"] Whether or not this field is a multi-select list.
    * @param boolean $aParams["freetext"] Allows text to be typed in the control which is not a code in the list.
    * @param string $aParams["customarray"] A custom data array which can be used to build the code list
    *                                       (Used in {@link MakeSelectFromArray()}).
    * @param string $aParams["customtitle"] A custom title for the pop up control.
    * @param boolean $aParams["nochoose"] Whether or not "Choose..." is used as the first value in code list.
    * @param string $aParams["selectfunction"] A function used to retrieve codes from the DB on-the-fly, rather than
    *                                          using pre-built Javascript arrays.
    *                                          Functions are defined in Source/libs/SelectFunctions.php
    * @param array $aParams["selectfunctionparameters"] Parameters for select function, passed as POST variables in
    *                                                   AJAX call.
    * @param boolean $aParams["noparentsuffix"] Parent field should not use the suffix id
    *                                           (when parent field is outside the dynamic section).
    * @param boolean $aParams["noparent2suffix"] Parent2 field should not use the suffix id
    *                                           (when parent field is outside the dynamic section).
    * @param string $aParams["mimicfield"] Enables this field to mimic another. Can be used to dynamically change
    *                                       the codes available to this field.
    * @param string $aParams["mimicmodule"] The module for the field we're mimicking.
    *
    * @return object $this The form field.
    */
	function MakeDropdownPopup($aParams)
    {
		$module = $aParams["module"];
		$field = $aParams["field"];
		$current = $aParams["current"];
		$parent = $aParams["parent"];
		$child = $aParams["child"];
		$parent2 = $aParams["parent2"];
		$child2 = $aParams["child2"];
		$AltFieldName = $aParams["altfieldname"];
		$ShowCodes = ($aParams["showcodes"] ? $aParams["showcodes"] : false);
		$DefaultHidden = $aParams["defaulthidden"];
		$nContactSuffix = $aParams["contactsuffix"];
		$onchangeExtra = $aParams["onchangeextra"];
		$MultiListbox = ($aParams["multilistbox"] ? $aParams["multilistbox"] : false);
		$FreeText = ($aParams["freetext"] ? $aParams["freetext"] : false);
        $customArray = $aParams["customarray"]; //Used in MakeSelectFromArray
        $customTitle = $aParams["customtitle"];
        $NoChoose = ($aParams["nochoose"] ? $aParams["nochoose"] : false);
        $selectFunction = $aParams["selectfunction"]; // custom function used to retrieve codes
        $selectFunctionParameters = $aParams["selectfunctionparameters"];
        $noParentSuffix = $aParams["noparentsuffix"];
        $noParent2Suffix = $aParams["noparent2suffix"];
        $mimicField = $aParams["mimicfield"];
        $mimicModule = $aParams["mimicmodule"];

		global $FieldDefs, $JSFunctions;

		$ShowCode = (bYN(GetParm('WEB_SHOW_CODE', 'N')));
		$ReadOnly = ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked");
		$displayValues = '';

		$fieldname = $field;

		if ($AltFieldName != "")
        {
			$fieldname = $AltFieldName;
		}
		elseif ($nContactSuffix)
        {
			$ContactSuffix = "_" . $nContactSuffix;
			$fieldname .= $ContactSuffix;
		}

        if ($FieldDefs[$module][$field]['FieldFormatsName'])
        {
            $CodeFieldName = $FieldDefs[$module][$field]['FieldFormatsName'];
        }
        else
        {
            $CodeFieldName = $field;
        }

        // we need to retrieve code descriptions based on the mimicked field
        $descriptionField = $mimicField ? $mimicField : $CodeFieldName;
        $descriptionModule = $mimicModule ? $mimicModule : $module;

        // CB 2010-03-19: Is $DefaultHidden used anymore?  There's no reference to it in FieldDefs.
        //                If it is, what's the point in adding a CreateCodeList function for this field, as below?
        //                This code should be stripped out if no longer in use.
		if ($DefaultHidden)
        {
			$this->Field = '<input type="hidden" name="' . $fieldname . '" id="' . $fieldname . '" value="' . $DefaultHidden . '"/>';
            $this->AddCreateCodeListFunc(
                array(
                    'fieldname' => $fieldname,
                    'module' => $module,
                    'codefieldname' => $CodeFieldName,
                   // 'fieldname' => $fieldname,
                    'contactsuffix' => $ContactSuffix,
                    'multilistbox' => $MultiListbox,
                    'freetext' => $FreeText,
                    'parent' => $parent,
                    'parent2' => $parent2,
                    'child' => $child,
                    'child2' => $child2,
                    'customarray' => $customArray,
                    'customtitle' => $customTitle,
                    'selectfunction' => $selectFunction,
                    'parameterstring' => $parameterString,
                    'nochoose' => $NoChoose,
            ));
			return $this;
		}

		if ($current != '')
        {
            if ($mimicField)
            {
                $isUDF = (\UnicodeString::substr($mimicField, 0, \UnicodeString::strpos($mimicField, '_')) == 'UDF');
            }
            else
            {
                $isUDF = (\UnicodeString::substr($field, 0, \UnicodeString::strpos($field, '_')) == 'UDF');
            }

			if ($isUDF)
            {
				// Format in edit mode:   UDF_<Type>_<GroupID>_<FieldID> / UDF_<FieldID>
				$udf_parts = $mimicField ? explode('_', $mimicField) : explode('_', $field);
                $field_id = $udf_parts[3] ? $udf_parts[3] : $udf_parts[1];
			}
		}

        if ($MultiListbox)
        {
            $output = '<input type="hidden" name="'.$fieldname.'" id="'.$fieldname.'" value="" />';
        }

        if (is_array($customArray))
        {
            foreach ($customArray as $code => $name)
            {
                // don't put any html codes in here - the codes need to be transferred into XML.
                $overrideArray[$code] = '\''.str_replace('\'','\\\'',$code).'\': \''.str_replace('\'','\\\'', htmlfriendly($name)).'\'';
            }
        }

		if ($MultiListbox && $current != '')
		{
            if (!is_array($current))
            {
                $currentValues = explode(' ', $current);
            }
            else
            {
                $currentValues = $current;
            }

			foreach ($currentValues as $val)
			{
				if (!empty($customArray))
                {
                    $description = Escape::EscapeEntities($customArray[$val]);
                }
                else
                {
                    if ($isUDF)
                    {
					    $description = code_descr_udf($field_id, $val, $module);
                    }
				    else
                    {
					    $description = code_descr($descriptionModule, $descriptionField, $val);
                    }
                }

				$description = $description;

				if ($ReadOnly)
				{
					$displayValue .= ($displayValue == '' ?  '' : '<br/>').($description == '' ? $val : $description);
				}
				else
				{
					$displayValue .=
						'<option value="' . $val . '">' .
						$description .
						($ShowCode && $description != $current ? ': ' . $val : '') .
						'</option>';
				}
			}
		}
		elseif ($current != '')
		{
            if (!empty($customArray))
            {
                $displayValue = Escape::EscapeEntities($customArray[$current]);
            }
            else
            {
                if ($isUDF)
                {
				    $description = code_descr_udf($field_id, $current, $module);
                }
			    else
                {
				    $description = code_descr($descriptionModule, $descriptionField, $current);
                }

                $displayValue = $description . ($ShowCode && $description != $current && !$ReadOnly ? ': ' . $current : '');
            }
		}

		if ($ReadOnly)
		{
            if ($FreeText)
            {
                $displayValue = $current;
            }

			$this->Field = '<div id="' . $fieldname . '_title">' . $displayValue . '</div>';
            $this->Field .= '<input type="hidden" name="' . $fieldname . '" id="' . $fieldname . '" value="' . $current . '"/>';
			return $this;
		}

        // set up parameters for custom select function
        if (!empty($selectFunction) && is_array($selectFunctionParameters) && !empty($selectFunctionParameters))
        {
            $parameterString = 'var '.$CodeFieldName.'SelectFunctionParameters = new Array();';

            foreach ($selectFunctionParameters as $key => $value)
            {
                $parameterString .= $CodeFieldName.'SelectFunctionParameters["' . $key . '"] = "' . htmlspecialchars($value) . '";';
            }

            $JSFunctions[] = $parameterString;
        }

       /*
        maybe put this in in a later version - potentially high impact.

        $this->AddCreateCodeListFunc(
            array(
                'fieldname' => $fieldname,
                'module' => $module,
                'codefieldname' => $CodeFieldName,
                'fieldname' => $fieldname,
                'contactsuffix' => $ContactSuffix,
                'multilistbox' => $MultiListbox,
                'freetext' => $FreeText,
                'parent' => $parent,
                'parent2' => $parent2,
                'child' => $child,
                'child2' => $child2,
                'customarray' => $customArray,
                'customtitle' => $customTitle,
                'selectfunction' => $selectFunction,
                'parameterstring' => $parameterString,
                'nochoose' => $NoChoose,
        ));*/

        $customTitle = preg_replace('/<[^<>]+>/u', '', $customTitle); //get rid of HTML in title

        if (!$parent) //if no overriding parent
        {
            $cachedParents = GetParents(array('module' => $module, 'field' => $field, 'table' => $this->ffTable));

            if ($cachedParents[0])
            {
                $parent = $cachedParents[0];

                if ($cachedParents[1])
                {
                    $parent2 = $cachedParents[1];
                }
            }
        }

        if (!$child) //if no overriding child
        {
            $cachedChildren = GetChildren(array('module' => $module, 'field' => $fieldname));

            if ($cachedChildren[0])
            {
                $child = $cachedChildren[0];

                if ($cachedChildren[1])
                {
                    $child2 = $cachedChildren[1];
                }
            }
        }

        if ($parent && !$noParentSuffix)
        {
            $parent  .= $ContactSuffix;
        }

        if ($parent2 && !$noParent2Suffix)
        {
            $parent2 .= $ContactSuffix;
        }

        $JSFunctions[] = 'function CreateCodeList_'.$fieldname.'(ControlIndex, normal) {'
            .'  GlobalCodeSelectControls[ControlIndex].create({' .
                "module: '$module', " .
                "field: '$CodeFieldName', " .
                "fieldname: '$fieldname', " .
                "contactsuffix: '$ContactSuffix', " .
                "multilistbox: " . ($MultiListbox && $this->FieldMode != "Search"? "true" : "false" ) . ", " .
                "mandatory: " . ($GLOBALS['MandatoryFields'][$fieldname] ? "true" : "false" ) . ", " .
                "fieldmode: '" . $this->FieldMode . "', " .
                "freetext: " . ($FreeText ? "true" : "false" ) .
                ($parent != "" ? ", parent1: '$parent'" : "" ) .
                ($parent != "" && $parent2 != "" ? ", parent2: '$parent2'" : "" ) .
                ($child != "" ? ", child1: '$child$ContactSuffix'" : "" ) .
                ($child != "" && $child2 != "" ? ", child2: '$child2$ContactSuffix'" : "" ) .
                (is_array($customArray) ? ", overrideArray: true" : "" ) .
                ($customTitle != "" ? ", overrideTitle: '".str_replace("'", "\\'", htmlspecialchars($customTitle))."'" : "" ) .
                (!empty($selectFunction) ? ", selectFunction: '$selectFunction'" : "" ) .
                (isset($parameterString) ? ", selectFunctionParameters: ".$CodeFieldName."SelectFunctionParameters" : "" ) .
                ' , xml: ' . (bYN(GetParm('XML_CODELISTS', 'N')) ? 'true' : 'false') .
                ' , nochoose: ' . ($NoChoose ? 'true' : 'false') .
                ' , childcheck: ' . (bYN(GetParm('RETAIN_COMBO_CHILDREN', 'Y')) ? 'true' : 'false') .
                ' , codecheck: !normal}); }';

        $output .= '<div class="code_field_wrapper">';

        if (is_array($customArray) )
        {
            $JSFunctions[] = 'overrideArray["'.$fieldname.'"] = new Array();'.(!empty($customArray) ? 'overrideArray["'.$fieldname.'"] = {'.(!empty($customArray) ? implode(',',$overrideArray) : '').'}' : '');
        }

		if ($this->FieldMode == "Search" || $FreeText == true)
		{
			$output .= '<input class="codefield" type="text" name="' . $fieldname . '" id="' . $fieldname . '" value="' . $current . '" />';
		}
		elseif ($MultiListbox == true)
		{
			$output .= '<select name="' . $fieldname .
				'[]" multiple="multiple" size="5" id="' . $fieldname .
				'" class="multilistbox_select_box" style="width: '.DROPDOWN_WIDTH_DEFAULT.'px" ondblclick="GlobalCodeSelectControls[$(\'' . $fieldname . '\').ctrlidx].removeItemFromMultiListbox($(\'' . $fieldname . '\'));">';
			$output .= $displayValue;
			$output .= '</select>';
		}
		else
		{
			$output .= '<div id="' . $fieldname . '_title" class="codefield">' . $displayValue . '</div>';
		}

		$output .= '</div>';

		if ($MultiListbox && $this->FieldMode != "Search" )
		{
			$output .= '<div class="multilistbox_button_wrapper">';

			$output .= '<div>';
			$output .= '<a href="javascript:CreateCodeList(\''.$fieldname.'\', true)"><img id="img_add_' . $fieldname . '" name="img_add_' . $fieldname . '" src="Images/' .
				'CodeList16_Add_n.gif' .
				'" border="0" width="16" height="16" class="mutlilistbox_image" ' .
				' onmouseover="this.src=\'Images/CodeList16_Add_h.gif\'" onmouseout="this.src=\'Images/CodeList16_Add_n.gif\'"' .
				' alt="' . _tk('alt_picklist_add') . '" title="' . _tk('alt_picklist_add') . '" />';
			$output .= '</a>';
			$output .= '</div>';

			$output .= '<div class="multilistbox_delete_button_wrapper">';
			$output .= '<a href="javascript:removeSelectedItemsFromMultiListbox(\'' . $fieldname . '\');">';
			$output .= '<img id="img_del_' . $fieldname . '" name="img_del_' . $fieldname . '" src="Images/' .
				'CodeList16_Del_n.gif' .
				'" border="0" width="16" height="16" class="mutlilistbox_image"' .
				' onmouseover="this.src=\'Images/CodeList16_Del_h.gif\'" onmouseout="this.src=\'Images/CodeList16_Del_n.gif\'"' .
				' alt="' . _tk('alt_picklist_delete') . '" title="' . _tk('alt_picklist_delete') . '" />';
			$output .= '</a>';
			$output .= '</div>';
		}
		else
		{
			$output .= '<div class="code_field_button_wrapper">';
			$output .= '<a href="javascript:CreateCodeList(\''.$fieldname.'\',true)"><img id="img_add_' . $fieldname . '" name="img_add_' . $fieldname . '" src="Images/' .
				($FreeText == true ? 'magnifier2.gif' : 'DATIX_Dropdown_n.gif') .
				'" border="0" width="21" height="20" class="dropdown_button_image"' .
				($FreeText == true ? '' : ' onmouseover="this.src=\'Images/DATIX_Dropdown_h.gif\'" onmouseout="this.src=\'Images/DATIX_Dropdown_n.gif\'"') .
				' alt="' . _tk('alt_dropdown') . '" title="' . _tk('alt_dropdown') . '" />';
			$output .= '</a>';
        }


		if ($this->FieldMode != "Search" && $FreeText == false && $MultiListbox == false)
		{
			$output .= '<input class="codefield" type="hidden" name="' . $fieldname . '" id="' . $fieldname . '" value="' . $current . '" />';
		}

        if ($onchangeExtra)
		{
            $JSFunctions[] = "function OnChange_Extra_{$fieldname}(){" . $onchangeExtra . "}";
		}

		$output .= '</div>';

		$this->Field = $output;
		return $this;
    }

    /**
    * Generates a javascript function used to create a pop up code list (code selection control) for a specified field.
    *
    * The function is added to the global $JSFunctions array, whose elements are printed out at the end of the page.
    *
    * @global array
    * @global boolean $MandatoryFields[$aParams['fieldname']] Indicates a mandatory field on the form.
    *
    * @param string $aParams['module'] The code for the current module for which the control is created.
    * @param string $aParams['codefieldname'] The database field name.
    * @param string $aParams['fieldname'] The name to be given to the HTML element storing the code.
    * @param string $aParams['contactsuffix'] The contact suffix from the form design contact section.
    * @param boolean $aParams['multilistbox'] Indicates a multiple or single selection listbox.
    * @param string $this->FieldMode The field mode (New/Print/ReadOnly/Search/Locked).
    * @param boolean $aParams['freetext'] Allows text to be typed in the control which is not a code in the list.
    * @param string $aParams['parent'] The name/id of the first parent (combo-linked) field.
    * @param string $aParams['parent2'] The name/id of the second parent (combo-linked) field.
    * @param string $aParams['child'] The name/id of the first child (combo-linked) field.
    * @param string $aParams['child2'] The name/id of the second child (combo-linked) field.
    * @param array $aParams['customarray'] A custom data array which can be used to build the code list
    *                                      (Used in {@link MakeSelectFromArray()}).
    * @param string $aParams['customtitle'] A custom title for the pop up control.
    * @param string $aParams['selectfunction'] A function used to retrieve codes from the DB on-the-fly,
    *                                           rather than using pre-built Javascript arrays.
    *                                          Functions are defined in Source/libs/SelectFunctions.php
    * @param string $aParams['parameterstring'] Parameters for select function, passed as POST variables in AJAX call.
    * @param boolean $aParams['nochoose'] Whether or not "Choose..." is used as the first value in code list.
    * @param string $_SESSION['Globals']['RETAIN_COMBO_CHILDREN'] If this is set to Y, when a value is changed
    *                                                             on a form, a child value will blank out only if it
    *                                                             is no longer valid. If it is set to N, the child
    *                                                             will blank out regardless of the parent value.
    */
    function AddCreateCodeListFunc($aParams)
    {
        global $JSFunctions;

        $JSFunctions[] = 'function CreateCodeList_'.$aParams['fieldname'].'(ControlIndex, normal) {'
            .'  GlobalCodeSelectControls[ControlIndex].create({' .
                "module: '".$aParams['module']."', " .
                "field: '".$aParams['codefieldname']."', " .
                "fieldname: '".$aParams['fieldname']."', " .
                "contactsuffix: '".$aParams['contactsuffix']."', " .
                "multilistbox: " . ($aParams['multilistbox'] && $this->FieldMode != "Search"? "true" : "false" ) . ", " .
                "mandatory: " . ($GLOBALS['MandatoryFields'][$aParams['fieldname']] ? "true" : "false" ) . ", " .
                "fieldmode: '" . $this->FieldMode . "', " .
                "freetext: " . ($aParams['freetext'] ? "true" : "false" ) .
                ($aParams['parent'] != "" ? ", parent1: '".$aParams['parent'].$aParams['contactsuffix']."'" : "" ) .
                ($aParams['parent'] != "" && $aParams['parent2'] != "" ? ", parent2: '".$aParams['parent2'].$aParams['contactsuffix']."'" : "" ) .
                ($aParams['child'] != "" ? ", child1: '".$aParams['child'].$aParams['contactsuffix']."'" : "" ) .
                ($aParams['child'] != "" && $aParams['child2'] != "" ? ", child2: '".$aParams['child2'].$aParams['contactsuffix']."'" : "" ) .
                (!empty($aParams['customarray']) ? ", overrideArray: true" : "" ) .
                ($aParams['customtitle'] != "" ? ", overrideTitle: '".str_replace("'", "\\'", htmlspecialchars($aParams['customtitle']))."'" : "" ) .
                (!empty($aParams['selectfunction']) ? ", selectFunction: '".$aParams['selectfunction']."'" : "" ) .
                (isset($aParams['parameterstring']) ? ", selectFunctionParameters: ".$aParams['codefieldname']."SelectFunctionParameters" : "" ) .
                ' , xml: ' . (bYN(GetParm('XML_CODELISTS', 'N')) ? 'true' : 'false') .
                ' , nochoose: ' . ($aParams['nochoose'] ? 'true' : 'false') .
                ' , childcheck: ' . (bYN(GetParm('RETAIN_COMBO_CHILDREN', 'Y')) ? 'true' : 'false') .
                ' , codecheck: !normal}); }';
    }

    /**
    * Creates a Yes/No dropdown field.
    *
    * @param  string    $Name           The field name/id.
    * @param  string    $Module         The module code.
    * @param  string    $Value          The current value for this field.
    * @param  string    $Mode           The mode the form is currently in.
    * @param  string    $ExtraChange    A custom action for this field's onchange event.
    * @param  string    $Title          A custom title for the pop up control.
    * @param  mixed     $DefaultHidden  If set the field is hidden and set to the default value.
    *
    * @return object    $this           The form field.
    */
    function MakeYesNoSelect($Name, $Module, $Value = "", $Mode = '', $ExtraChange="", $Title="", $DefaultHidden = '', $changedValue = '', $YNArray = array())
    {
        if (empty($YNArray))
        {
            $YNArray = Array('Y' => _tk('yes'), 'N' => _tk('no'));
        }

        if (\UnicodeString::strpos($Name, 'Audit_') === 0)
        {
            // audit not incorporated into new functionality yet
            return $this->MakeSelectFromArray($Name, $YNArray, $Value, '', $DefaultHidden, $ExtraChange, $Title);
        }
        else
        {
            $field = Forms_SelectFieldFactory::createSelectField($Name, $Module, $Value, $Mode, false, $Title, '', $changedValue);
            $field->setCustomCodes($YNArray);
            $field->setOnChangeExtra($ExtraChange);
            $field->setDefaultHidden($DefaultHidden);
            $field->setSuppressCodeDisplay();
            return $field;
        }
	}

    function MakeMultiList($aParams)
    {
        if (bYN(GetParm('DROPDOWN_POPUP', 'N')))
        {
            $this->MakeDropdownPopup($aParams);
        }
        else
        {
            $this->MakeMultiListbox($aParams['module'], $aParams['field'], $aParams['current'], '', $aParams['altfieldname'], $aParams['customarray'], $aParams['title']);
        }
    }

    /* Function makes a multiple-selection listbox.  The combo element is
       populated from the code table indicated in the field_formats
       entry for the $FieldName parameter.
       The values for the list box can be either an array of codes
       or a space-separated string.
       If $CodesArray is not null, this is used to populate the combo
       instead.
    */
	function MakeMultiListBox($Module, $FieldName, $FieldValue = "", $FormType = "", $AltFieldName = "",
                              $CodesArray = "", $Title = '')
    {
        if ($AltFieldName != "")
        {
            $FormFieldName = $AltFieldName;
        }
        else
        {
            $FormFieldName = $FieldName;
        }

		if ($this->FormMode == "Print" || $FormType == "Print" || $this->FieldMode == "Print" ||
            $this->FormMode == "ReadOnly" || $FormType == "ReadOnly" || $this->FieldMode == "ReadOnly")
		{
			if ($FieldValue != "")
			{
				if (!is_array($FieldValue))
                {
                    $Fields = explode(" ", $FieldValue);
                }
                else
                {
                    $Fields = $FieldValue;
                }

				if ($CodesArray)
				{
					foreach ($Fields as $Code)
                    {
                        $DescrArray[] = $CodesArray[$Code];
                    }
				}
				else
				{
                    foreach ($Fields as $Code)
                    {
                        $DescrArray[] = code_descr($Module, $FieldName, $Code);
                    }
                }

				$output .= implode("<br />", $DescrArray);
            }

            $output .= '<input type="hidden" name="' . $FieldName . '" value="' . $FieldValue . '">';
            $this->Field = $output;
            return $this;
        }
        else
        {
            $output = '<select name="' . $FormFieldName . '[]" multiple="multiple" size="5" id="'
            . $FormFieldName . '" class="multilistbox_select_box" style="width: '.DROPDOWN_WIDTH_DEFAULT.'px" >';

            if ($FieldValue != "")
            {
                if (!is_array($FieldValue))
                {
                    $Fields = explode(" ", $FieldValue);
                }
                else
                {
                    $Fields = $FieldValue;
                }

                // $ could be either a field name or an
                // array of code => description.  If it is an array, we
                // need to populate the list box in a slightly different way.
                foreach ($Fields as $Code)
                {
                    $output .= '<option value="' . $Code . '">';

                    if (!$CodesArray)
                    {
                        $output .= code_descr($Module, $FieldName, $Code);
                    }
                    else
                    {
                        $output .= $CodesArray[$Code];
                    }

                    $output .= '</option>';
                }
            }

            // Get parent/child fields
            $sql = "SELECT cmb_parent
                    FROM combo_links
                    WHERE cmb_child = '$FieldName' AND cmb_module = '$Module'
                    ORDER BY recordid";
            $result = db_query($sql);

            while ($row = db_fetch_array($result))
            {
                $ComboLinksParents[]= $row["cmb_parent"];
            }

            $DropdownField = new FormField();

            if ($CodesArray)
            {
                $DropdownField->MakeSelectFromArray("Select" . $FormFieldName, $CodesArray,'','','','',$Title,'',true);
            }
            else
            {
                $DropdownField->MakeDropdownStd($Module, $FieldName, "", $ComboLinksParents[0], "", $ComboLinksParents[1], "", "Select" . $FormFieldName, "", "", "", true);
            }

            $output .= '</select>&nbsp;
                <input type="button" value="' . _tk('btn_delete') . '" onclick="deleteSelected(document.getElementById(\'' . $FormFieldName . '\'))" />
                <br />
                ' . $DropdownField->GetField()
            . '&nbsp;<input type="button" name="btnAdd" value="'
            . _tk('btn_add_to_table') . '" onclick="addCode(document.getElementById(\'Select' . $FormFieldName . '\')'
            . ', document.getElementById(\'' . $FormFieldName . '\'))" />';

            $this->Field = $output;
            return $this;
        }
    }

    /*
    Function makes a multiple-selection listbox.
    The entries in the list box are added from an input field
    The values for the list box can be either an array of codes
    or a space-separated string.
    If $CodesArray is not null, this is used to populate the combo
    instead.
    e.g.
		$FieldObj->MakeMultiListbox("INC", "inc_loctype", $loctypes, "", "loctypes");
    */
	function MakeInputToMultiListBox($FieldName, $FieldValue = "", $FormType = "", $AltFieldName = "")
    {
        if ($AltFieldName != "")
        {
            $FormFieldName = $AltFieldName;
        }
        else
        {
            $FormFieldName = $FieldName;
        }

        if ($this->FormMode == "Print" || $FormType == "Print" || $this->FieldMode == "Print"
          || $this->FormMode == "ReadOnly" || $FormType == "ReadOnly" || $this->FieldMode == "ReadOnly")
		{
            if (!is_array($FieldValue))
            {
                $Fields[] = $FieldValue;
            }
            else
            {
                $Fields = $FieldValue;
            }

            $output .= implode("<br />", $Fields);
            $output .= '<input type="hidden" name="' . $FormFieldName . '" value="' . $FieldValue . '">';

            $this->Field = $output;
            return $this;
         }

        $output = '<select name="' . $FormFieldName . '[]" multiple size="5" id="'
        . $FormFieldName . '" class="multilistbox_select_box"  style="width: '.DROPDOWN_WIDTH_DEFAULT.'px">
    ';

        if ($FieldValue != "")
        {
            if (!is_array($FieldValue))
            {
                $Fields[] = $FieldValue;
            }
            else
            {
                $Fields = $FieldValue;
            }

            // $FieldName parameter could be either a field name or an
            // array of code => description.  If it is an array, we
            // need to populate the list box in a slightly different way.
            foreach ($Fields as $Value)
            {
                $output .= '<option value="' . $Value . '">';
                $output .= $Value;
                $output .= '</option>';
            }
        }

        $FieldObj = new FormField();
        $FieldObj->MakeInputField($FieldName . "Input", 47, 254, "");

        $output .= '</select>&nbsp;
            <input type="button" value="' . _tk('btn_delete') . '" onclick="deleteSelected(document.getElementById(\'' . $FormFieldName . '\'))" />
            <br />
            ' . $FieldObj->GetField()
            . '&nbsp;<input type="button" name="btnAdd" value="'
            . _tk('btn_add_to_table') . '"'
            . ' onclick="addInputToListBox(document.forms[0].' . $FieldName . "Input" . ', document.getElementById(\'' . $FormFieldName . '\'))" />';
        $this->Field = $output;
        return $this;
    }
	// end of MakeInputToMultiListBox()

    function MakeEmailField($FieldName, $Width, $MaxLength, $FieldValue = "", $onblurExtra = "", $singleEmail = false)
    {
        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked")
        {
            $this->Field = htmlspecialchars($FieldValue) . '<input type="hidden" name="' . $FieldName . '" value="' . $FieldValue . '" />';
            return $this;
        }

        if($this->isTablet)
        {
            $this->Field = '<input type="email" autocapitalize="none" autocorrect="off" name="' . $FieldName . '" id="' . $FieldName . '" size="' . $Width . '"';
        }
        else
        {
            $this->Field = '<input type="text" name="' . $FieldName . '" id="' . $FieldName . '" size="' . $Width . '"';
        }

        if ($this->FormMode != "Search")
        {
            $this->Field .= ' maxlength="' . $MaxLength . '" value="' . $FieldValue .'"';
        }
        $onBlurJS = null;
        if (bYN(GetParm('VALIDATE_EMAIL','Y')) && $this->FieldMode != 'Search')
        {
            if (!$singleEmail)
            {
                $onBlurJS .= 'Javascript:isEmail(this, false);';
            }
            else
            {
                $onBlurJS .= 'Javascript:isSingleEmail(this, false);';
            }

            $this->Field .= ' onchange="setChanged(\'' . $FieldName . '\')"';
        }

        if(!empty($onblurExtra))
        {
            $onBlurJS .= $onblurExtra;
        }

        if(!empty($onBlurJS))
        {
            $this->Field .= ' onblur="' . $onBlurJS . '"';
        }

        $this->Field .= ' />';
        return $this;
    }

    function MakeDateField($FieldName, $FieldValue = "", $FieldDef = null)
    {
        $onblurExtra = $FieldDef['onBlurExtra'] ?: "";
        $onchangeExtra = $FieldDef['onChangeExtra'] ?: "";
        $DefaultHidden = $FieldDef['DefaultHidden'] ?: false;
        $NotFuture = $FieldDef['NotFuture'] ?: false;
        $NoValidation = $FieldDef['NoValidation'] ?: false;
        $data = "";

        if ($this->FieldMode == "New" && $DefaultHidden)
        {
           $FieldValue = $DefaultHidden;
        }

        if ($FieldValue == "TODAY")
        {
            $FieldValue = GetTodaysDate(true);
        }

        if ($this->FieldMode == "New" && $DefaultHidden)
        {
            $this->Field .= '<input type="hidden" name="' . $FieldName . '" id="' . $FieldName . '" value="' . FormatDateVal($FieldValue) . '"/>';
            return $this;
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked")
        {
            $FieldValue = UserDateToSQLDate($FieldValue);
            $this->Field = FormatDateVal($FieldValue);
            $this->Field .= '<input type="hidden" name="' . $FieldName . '" id="' . $FieldName . '" value="' . FormatDateVal($FieldValue) . '"/>';
            return $this;
        }

        if($NotFuture == true)
        {
            $data = ' data-not-future="true"';

            $max = date('Y-m-d');
        }

        if($this->isTablet)
        {
            $output = '<input type="date" ';
        }
        else
        {
            $output = '<input type="text" ';
        }

        if ($this->FieldMode != "Search")
        {
            $FieldValue = UserDateToSQLDate($FieldValue);

            if( ! $this->isTablet){ $output .= 'maxlength="10" '; }
            if($NoValidation == true)
            {
                $output .= 'onblur="'.$onblurExtra.'" ';
            }
            else
            {
                $output .= 'onblur="Javascript:isDate(document.getElementById(\''
                . $FieldName . '\'));'.$onblurExtra.'" ';
            }

            $output .= 'value="'
            . FormatDateVal($FieldValue) . '" ';

			$output .= 'onchange="setChanged(\'' . $FieldName . '\');'.$onchangeExtra.'" ';
        }
        elseif($FieldValue)
        {
            $output .= 'value="'
                .$FieldValue. '" ';
        }

        $output .= ' name="' . $FieldName . '" id="' . $FieldName . '" size="15" class="date"' . $data . ($max && $this->isTablet ? ' max="' . $max . '"' : '') . ' />';

        $this->Field = $output;
        return $this;
    }

    function MakeDateTimeField($FieldName, $FieldValue = "", $onblurExtra = "")
    {
        $DateTimeValue = '\''.UserDateToSQLDate($FieldValue).'\'';

        if ($DateTimeValue === null)
        {
            $DateTimeValue = "";
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly")
        {
            $this->Field = $DateTimeValue . '<input type="hidden" name="' . $FieldName . '" value="' . $DateTimeValue . '" />';
            return $this;
        }

        if($this->isTablet)
        {
            $output = '<input type="datetime" ';
        }
        else
        {
            $output = '<input type="text" ';
        }

        if ($this->FieldMode != "Search")
        {
            $output .= 'maxlength="10" onblur="Javascript:is'
                . (GetParm("FMT_DATE_WEB") == "US" ? 'American' : '')
                . 'Date(document.forms[0].'
                . $FieldName . ');'.$onblurExtra.'" value="'
                . $DateTimeValue . '" onchange="setChanged(\'' . $FieldName . '\');"';
        }

        $output .= ' name="' . $FieldName . '" id="' . $FieldName . '" size="15" />';

        // CB 2010-11-15: the calendar library referenced below has been removed due to concerns over XSS vulnerabilities,
        // however this function doesn't appear to be called anywhere (we don't have any fields defined as datetime).
        // We need to be wary of this if this field type is ever reinstated.
        $CalendarScript = "calendar.php";

        if (GetParm("FMT_DATE_WEB") == "US")
        {
            $CalendarScript = "am$CalendarScript";
        }

        $output .= '<img id=\'img_cal_'. $FieldName . '\' class="cal" src="Images/calendar.gif" border="0" alt="Calendar" onclick="popupwin=window.open(\'' . $CalendarScript . '?field='
            . $FieldName . '\', \'cal\', \'dependent=yes, width=190, height=250, screenX=200, screenY=300, titlebar=yes\');popupwin.focus(); setChanged(\'' . $FieldName . '\')" />';

        $this->Field = $output;
        return $this;
    }

    function MakeDivCheckbox($Name, $Div = "", $Value = false, $id = "", $Generic = false, $additionalAttributes = '')
    {
        if ($id == "")
        {
            $id = $Name;
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly")
        {
            if ($Value === true || $Value === "Y" || $Value === 1 || $Value == "on")
            {
                $output .= _tk('yes');
            }
            else
            {
                $output .= _tk('no');
            }

            $output .= '<input type="hidden" name="' . $Name . '" value="' . $Value . '" />';
        }
        else
        {
            $output = '<input type="checkbox" name="' . $Name . '" id="' . $id . '" ' . $additionalAttributes;

            if ($Value === true || $Value === "Y" || $Value === 1 || $Value == "on")
            {
                $output .= ' checked="checked"';
            }

            if ($Generic)
            {
                $output .= ' onclick="FieldChanged(\'' . $Name . '\');"';
            }
            else
            {
                $output .= ' onclick="expandIt(\'' . $Div . '\',this);"';
            }

            $output .= ' value=\'Y\' />';
        }

        $this->Field = $output;
        return $this;
    }

    function MakeDivYesNo($Name, $Div = "", $Value, $DefaultHidden = '', $onClick = '')
    {
        $field = Forms_SelectFieldFactory::createSelectField($Name, '', $Value, $this->FieldMode);
        $field->setCustomCodes(array('Y' => _tk('yes'), 'N' => _tk('no')));
        $field->setSuppressCodeDisplay();

        if ($DefaultHidden != '')
        {
            $field->setDefaultHidden($DefaultHidden);
        }

        if ($Div != '')
        {
            $field->setOnChangeExtra('expandItYN(\''.$Div.'\',this); setChanged(\''.$Name.'\')');
        }

        if ($onClick != '')
        {
            $field->setOnChangeExtra($onClick);
        }

        return $field;
    }

    /**
    * Allows creation of a form field object with custom contents.
    *
    * @param string $Contents The contents (HTML) of the field.
    *
    * @return object $this The FormField object.
    */
    function MakeCustomField($Contents)
    {
        $this->Field = $Contents;
        return $this;
    }

    function MakeTextAreaField($Name, $Rows, $Cols, $MaxLength, $Contents, $Spellcheck = true, $CheckChanged = true,
                               $additionalAttributes = '', $title = '', $currentValue = '', $Suffix = '', $onBlurJS = '')
    {
        global $TimestampFields, $FieldDefs;

        if ($Suffix)
        {
            $Name = $Name . '_' . $Suffix;
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked")
        {
            $this->Field = nl2br(htmlspecialchars($Contents)) . '<input type="hidden" name="' . $Name . '" value="' . htmlspecialchars($Contents) . '" />';
            return $this;
        }

        $this->Field = '<textarea name="' . $Name . '" id="' . $Name . '" cols="' . $Cols
        . '" rows="' . $Rows . '" ' . $additionalAttributes;

        if($this->isTablet)
        {
            $this->Field .= ' autocapitalize="sentences"';
        }

        if($Spellcheck)
        {
            $this->Field .= ' class="spellcheck"';
        }

        if ($this->FieldMode != "Search" && $CheckChanged === true)
        {
            $this->Field .= ' onchange="setChanged(\'' . $Name . '\')"';
        }

        if($onBlurJS != '' && ($this->FieldMode != "Search" && $this->FieldMode != "Print" && $this->FieldMode != "ReadOnly"))
        {
            $this->Field .= ' onblur="'.$onBlurJS.'"';
        }

        if ($MaxLength)
        {
            $this->Field .= 'data-max-length="' . $MaxLength . '"';
        }

        $this->Field .= '>';

        if ($this->FieldMode != "Search" && ($TimestampFields[$Name] || $TimestampFields[RemoveSuffix($Name)]) &&
            !isLevelOneForm() && ! $FieldDefs[$_GET['module']][$Name]['MaxLength'])
        {
            if (isset($currentValue) &&
                str_replace(array("\r\n", "\n", "\r"), '', $Contents) != str_replace(array("\r\n", "\n", "\r"), '', $currentValue))
            {
                // there was an error when saving the form, so we need to populate the text area with the correct pre-save value
                $this->Field .= htmlspecialchars($Contents);
                $historyText = $currentValue;
            }
            else
            {
                $historyText = $Contents;
            }

            $extraRowField = new FormField();
            $extraRowField->Field = nl2br(htmlspecialchars($historyText)).'<input type="hidden" id="CURRENT_' . $Name . '" name="CURRENT_' . $Name . '" value="' . htmlspecialchars($historyText) . '" />';
            $this->extraRow = array('title' => '<span class="field_label">'.Labels_FormLabel::GetFormFieldLabel($Name,$title).' history</span>', 'field' => $extraRowField);
        }
        else
        {
            $this->Field .= htmlspecialchars($Contents);
        }

        $this->Field .= '</textarea>';

        return $this;
    }

    /**
    * Creates a text area field with an 'Edit' link to the right.
    *
    * The field is displayed as read-only by default until the edit link (if available to this user) is clicked.
    *
    * @param  string $name The field name.
    * @param  mixed $rows The value for the textarea "rows" attribute.
    * @param  mixed $cols The value for the textarea "cols" attribute.
    * @param  mixed $maxLength The maximum character length for this field.
    * @param  string $contents The field contents.
    * @param  boolean $spellcheck Whether or not the spellchecker should be loaded for this field (when in edit mode).
    * @param  boolean $checkChanged Whether or not to set this field as changed upon edit.
    * @param  boolean $allowEdit Whether or not this user has permission to edit this field.
    * @param  string $onEditExtra Any additional javascript to execute when edit is clicked.
    *
    * @return object $this The form field.
    */
    function MakeTextAreaFieldWithEdit($name, $rows, $cols, $maxLength, $contents, $spellcheck = true,
                                       $checkChanged = true, $allowEdit = false, $onEditExtra = '')
    {
        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked")
        {
            $this->Field = nl2br(htmlspecialchars($contents)) . '<input type="hidden" name="' . $name . '" value="' . htmlspecialchars($contents) . '" />';
            return $this;
        }

        $spellcheck = $spellcheck && $this->FieldMode != "Search" && $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y";
        $additionalAttributes = 'style="display:none';

        if ($spellcheck)
        {
             $additionalAttributes .= '; width: ' . ($cols * 6) . 'px; height: ' . ($rows * 11) . 'px';
        }

        $additionalAttributes .= '"';
        $this->MakeTextAreaField($name, $rows, $cols, $maxLength, $contents, false, $checkChanged, $additionalAttributes);

        $field .= '
            <div>
                <div id="'.$name.'_readonly">'.nl2br(htmlspecialchars($contents)).'</div>
                '.$this->Field.'
            </div>';

        if ($allowEdit)
        {
            $field .= '
                <div id="'.$name.'_editlink" style="margin-top:10px; font-weight:bold; font-style:italic">
                    <a href="javascript:void(0)" onclick="editTextArea(\''.$name.'\', '.($spellcheck ? 'true' : 'false').');'.$onEditExtra.'">edit</a>
                </div>';
        }

        $this->Field = $field;
        return $this;
    }

    function MakeInputField($Name, $Width, $MaxLength, $Contents, $FieldDef=array(), $CheckChanged = true)
    {
        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked")
        {
            $this->Field = htmlspecialchars($Contents) . '<input type="hidden" name="' . $Name . '" value="' . htmlspecialchars($Contents) . '" />';
            return $this;
        }

        $this->Field = '<input ';

        if ($FieldDef['Password'])
        {
            $this->Field .= 'type="password" ';
        }
        else
        {
            $this->Field .= 'type="text" ';
        }

        if ( ! $FieldDef['Password'] && $Name != 'login' && $this->isTablet)
        {
            $this->Field .=  ' autocapitalize="' . ($FieldDef['UpperCase'] ? 'character' : 'sentences') . '"';
        }

        $this->Field .= 'name="' . $Name . '" id="' . $Name . '" size="' . $Width . '"';

        if( ! empty($FieldDef['data']))
        {
            foreach($FieldDef['data'] as $key => $val)
            {
                if(($key != 'duplicate-check' && $key != 'can-add') ||
                    ($key == 'duplicate-check' && isset($_SESSION["logged_in"]) && bYN(GetParm('CLA_SHOW_DUPLICATES', 'N'))) ||
                    ($key == 'can-add' && bYN(GetParm('PAY_ADD_NEW_RECIPIENT', 'N')))
                )
                {
                    $this->Field .= 'data-' . $key . '="' . $val . '"';
                }
            }
        }

        if ($this->FieldMode != "Search")
        {
            $this->Field .= ' maxlength="' . $MaxLength . '" value="'.htmlspecialchars($Contents).'"';

            if ($CheckChanged === true)
            {
                $this->Field .= ' onchange="setChanged(\'' . $Name . '\')"';
            }

            if ($FieldDef['eventExtra'])
            {
                $this->Field .= ' ' . $FieldDef['eventExtra'] . ' ';
            }
        }

        if ($FieldDef['UpperCase'])
        {
            $this->Field .= ' style="text-transform:uppercase"';
        }

        if($FieldDef['Title'])
        {
            $this->Field .= ' title="' . $FieldDef['Title'] . '"';
        }

        $this->Field .= '/>';

        return $this;
    }

	function MakeInputFieldSearch($Name, $Width, $MaxLength, $Contents, $FieldDef = array(), $module = "")
    {
        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly")
        {
            $this->Field = htmlspecialchars($Contents) . '<input type="hidden" name="' . $Name . '" value="' . htmlspecialchars($Contents) . '" />';
            return $this;
        }

        $this->Field = '<input type="text" name="' . $Name . '" id="' . $Name . '" '.($Width ? 'size="' . $Width . '"' : '');

        if ($this->FormMode != "Search")
        {
            $this->Field .= ' maxlength="' . $MaxLength
                . '" value="' . htmlspecialchars($Contents) .'" onchange="setChanged(\'' . $Name . '\')"';
        }

        if ($FieldDef['UpperCase'])
        {
            $this->Field .= ' style="text-transform:uppercase"';
        }

        $this->Field .= ' />';

		return $this;
    }

    function MakeInputFieldMedSearch($Name, $Width, $MaxLength, $Contents, $FieldDef = array(), $module = "",
                                     $MedicationSuffix = '')
    {
        global $ModuleDefs;

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly")
        {
            $this->Field = htmlspecialchars($Contents) . '<input type="hidden" name="' . $Name . '" value="' . htmlspecialchars($Contents) . '" />';
            return $this;
        }

        $this->Field = '<input type="text" name="' . $Name . '" id="' . $Name . '" size="' . $Width
        . '"';

        if ($this->FormMode != "Search")
        {
            $this->Field .= ' maxlength="' . $MaxLength
                . '" value="' . htmlspecialchars($Contents) .'" onchange="setChanged(\'' . $Name . '\')"';
        }

        if ($FieldDef['UpperCase'])
        {
            $this->Field .= ' style="text-transform:uppercase"';
        }

        $this->Field .= ' />';

        $this->Field .= '&nbsp;<input type="button" value="Search" id="check_btn_' . $Name . '"
            onclick="javascript:';

        $SearchMappingType = $FieldDef["SearchMappingType"];
        $fieldlist = GetParm('MED_CHK_FIELDS', 'med_name,med_class,med_brand,med_manufacturer');
        $fieldlist .= ",$SearchMappingType";
        $med_search_fields = explode ( ",", $fieldlist);

        foreach ($med_search_fields as $key => $med_field)
        {
                $med_search_fields[$key] = $ModuleDefs["MED"]["SEARCH_FIELD_MAPPING"][$SearchMappingType][$med_field];
        }

        foreach ($med_search_fields as $key => $med_field)
        {
            if (is_array($GLOBALS['HideFields']) &&
                array_key_exists($med_field . '_' . $MedicationSuffix, $GLOBALS['HideFields']))
            {
                unset($med_search_fields[$key]);
            }
            else
            {
                if ($med_field != $SearchMappingType)
                {
                    $FieldValueChecks[] = 'document.getElementById(\'' . $med_field . '_' . $MedicationSuffix . '\').value != \'\'';
                    $alertmsg .= '<br />' . GetFormFieldLabel($med_field);
                }

                $CtrlFields .= $med_field . ": '" . $med_field . '_' . $MedicationSuffix . "',";
            }
        }

        $CtrlFields .= $med_field . ": '" . $Name . "',";
        $FieldValueChecks[] = 'document.getElementById(\'' .$Name . '\').value.length > 3';
        $this->Field .= "if (" . implode(" || ", $FieldValueChecks) . ")";

        $this->Field .= '
        {
            MedicationSelectionCtrl.create({' .
            $CtrlFields .
            "module: '$module', " .
            "fieldname: '$Name', " .
            "suffix: '$MedicationSuffix', " .
            "searchmappingtype: '$SearchMappingType', " .
            "fieldlist: '" . implode(",", $med_search_fields) . "'" .
            ($_GET['form_id'] ? ", parentformid: ".$_GET['form_id'] : '').
            '});
        }
        else
        {
            alert(\''.sprintf(_tk('min_character_error'), '<br />' . $alertmsg). '\');
        }"
        />';

        return $this;
    }

    function MakeDecimalField($Name, $Value, $Width = 13, $MaxLength = 0, $MaxValue = "", $MinValue = "")
    {
        //same as MakeNumberField, except need to strip off trailing zeros after the dp.
        if ($Value != '')
        {
            $Value = floatval($Value);
        }

        $this->MakeNumberField($Name, $Value, $Width, $MaxLength, $MaxValue, $MinValue);
    }


    function MakeNumberField($Name, $Value, $Width = 13, $MaxLength = 0, $MaxValue = "", $MinValue = "",
                             $disabled = false, $OnBlurCustom = '', $AlwaysShowBox = false)
    {
        if (!$Width)
        {
            $Width = 13;
        }

        //new php db driver strips leading zeros
        if (\UnicodeString::substr($Value, 0, 1) == '.')
        {
            $Value = '0'.$Value;
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked")
        {
            if ($AlwaysShowBox)
            {
                $this->Field = '
                    <input type="text" value="'. $Value . '" disabled="disabled" size="' . $Width . '">
                    <input type="hidden" name="' . $Name . '" value="' . $Value . '" />';
            }
            else
            {
                $this->Field = $Value . '<input type="hidden" name="' . $Name . '" value="' . $Value . '" />';
            }

            return $this;
        }

        if ($this->FieldMode != "Search" && !$MaxLength)
        {
            $MaxLength = 9; //allowing integers greater than 9 digits could cause sql errors id they exceed int32 range.
        }

        $this->Field = '<input type="text" name="' . $Name;

        $this->Field .= '" id="' . $Name . '" size="' . $Width . '" '.($MaxLength ? 'maxlength="'. $MaxLength .'" ' : '').'value="' . $Value . '"';

        if ($this->FieldMode != "Search")
        {
            if (is_numeric($MaxValue))
            {
                $OnBlurJS = "CheckMax(this, $MaxValue);";
            }
            else
            {
                $OnBlurJS = "CheckMax(this, 99999999999.99);";
            }

            if (is_numeric($MinValue))
            {
                $OnBlurMinJS = "CheckMin(this, $MinValue);";
            }
            else
            {
                $OnBlurMinJS = "CheckMin(this, -99999999999.99);";
            }

            $this->Field .= ' onblur="CheckNumber(\''.$Name.'\');'.$OnBlurJS.$OnBlurMinJS.$OnBlurCustom.'" onchange="setChanged(\''.$Name.'\')" '.($disabled ? 'disabled="disabled"' : '');

        }

        $this->Field .= ' />';
        return $this;
    }

    /**
     * A field for holding values which represent an amount of money in different currencies
     * It's specific functionaity includes specific evaluation logic for money in javascript
     *
     * @param string $Name           Name of the field
     * @param string $Value          Value displayed in the field
     * @param int    $Width          HTML style attribute value
     * @param string $MaxLength      Max length value of text field
     * @param string $MaxValue       Largest integer the monetary value is allowed to be
     * @param string $MinValue       Smallest integer the monetary value is allowed to be
     * @param string $ParentFormMode Money fields are evaluated in js differently when in a Search or an Edit form
     *
     * @return $this
     */
    function MakeMoneyField($Name, $Value, $Width = 13, $MaxLength = "", $MaxValue = "", $MinValue = "", $ParentFormMode = "")
    {
        if (!$Width)
        {
            $Width = 13;
        }

        //new php db driver strips leading zeros
        if (\UnicodeString::substr($Value, 0, 1) == '.')
        {
            $Value = '0'.$Value;
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $this->FieldMode == "Locked")
        {
            if (is_numeric($Value))
            {
                //format money value for display
                $Value = FormatMoneyVal($Value);
            }

            $this->Field = ' <input type="text" name="' . $Name
                            . '" id="' . $Name . '" size="' . $Width . '" value="' . $Value . '" readonly="readonly" style="text-align:right;background-color:#D0D0D0" />';
            return $this;
        }

        if ($this->FieldMode != 'Search')
        {
            if (is_numeric($MaxValue))
            {
                $OnBlurJS = "CheckMax(this, $MaxValue);";
            }
            else
            {
                $OnBlurJS = "CheckMax(this, 99999999999.99);";
            }

            if (is_numeric($MinValue))
            {
                $OnBlurMinJS = "CheckMin(this, $MinValue);";
            }
            else
            {
                $OnBlurMinJS = "CheckMin(this, -99999999999.99);";
            }
        }

        if (is_numeric($Value))
        {
            //format money value for display
            $Value = FormatMoneyVal($Value);
        }

        $this->Field = ' <input type="text" name="' . $Name;

        $this->Field .= '" id="' . $Name . '" onchange="setChanged(\'' . $Name . '\')" size="' . $Width . '" '.($MaxLength ? 'maxlength="'. $MaxLength .'" ' : '').'value="' . $Value . '"';

        //Money fields are evaluated in js differently when in a search or edit page
        //This is a string representation of a boolean value, to be passed to the string representation of js below
        //For obvious reasons, don't treat it like a boolean in this php code!
        $allowSearchSymbols = ($ParentFormMode === 'Search') ? ('true') : ('false');

        $this->Field .= ' onblur="CheckMoney(jQuery(this), ' .  $allowSearchSymbols . ');' . $OnBlurJS . $OnBlurMinJS . '"';

        $this->Field .= ' style="text-align:right" />';
        return $this;
    }

    function MakeTimeField($Name, $Value)
    {
        if (\UnicodeString::strlen($Value) == 4)
        {
            $Value = $Value{0} . $Value{1} . ":" . $Value{2} . $Value{3};
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly")
        {
            $this->Field = $Value . '<input type="hidden" name="' . $Name . '" value="' . $Value . '" />';
            return $this;
        }

        if($this->isTablet)
        {
            $this->Field = '<input type="time" name="' . $Name;
        }
        else
        {
            $this->Field = '<input type="text" name="' . $Name;
        }

        $this->Field .= '" id="' . $Name . '"'. ($this->FieldMode!="Search" ? ' size="5" maxlength="5" onblur="Javascript:isTime(document.forms[0].' . $Name . ')"' : '').' value="' . $Value . '" onchange="setChanged(\'' . $Name . '\')" />';

        return $this;
    }

    // Makes a SELECT element from an array of [value, text].
    // Parameter Extra is for adding e.g. onchange events
    function MakeSelectFromArray($Name, $SelectArray, $Value = "", $Extra = "",
        $DefaultHidden = false, $onchangeExtra = "", $Title = "", $NoChoose = false, $NoPopup=false,
        $MultiListBox=false, $Module='', $width='')
    {
        if (bYN(GetParm("DROPDOWN_POPUP", "N")) && !$NoPopup)
        {
            $this->MakeDropdownPopup(
                array(
                    "module" => $Module,
                    "field" => $Name,
                    "current" => $Value,
                    'onchangeextra' => $onchangeExtra,
                    'defaulthidden' => $DefaultHidden,
                    "customarray" => $SelectArray,
                    "customtitle" => $Title,
                    "nochoose" => $NoChoose,
                    'multilistbox' => $MultiListBox,
                    'defaulthidden' => $DefaultHidden
                )
            );
        }
        else
        {
            if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $DefaultHidden)
            {
                $this->Field = $SelectArray[$Value];
                $this->Field .= '<input type="hidden" name="' . $Name
                    . '" id="' . $Name . '" value="' . ($DefaultHidden ? $DefaultHidden : $Value) . '" />';
                //$this->Field = $SelectArray[$Value];
                return $this;
            }

            if ($MultiListBox)
            {
                $this->MakeMultiListBox($Module, $Name, $Value, $this->FieldMode,'',$SelectArray);
                return $this;
            }

            $onChange = 'FieldChanged(\'' . $Name . '\');';

            $onChange .= $onchangeExtra;

            $this->Field = '<select name="' . $Name . '" id="' . $Name
                . '" onchange="'. $onChange . '" '
                . ($width ? 'style="width:'.$width.'px" ' : '')
                . ($MultiListBox ? 'multiple="multiple" ' : '')
                . $Extra . '>';

            if (!$NoChoose)
            {
                $this->Field .= '<option value=""';
                if ($Value == "")
                    $this->Field .= ' selected="selected"';
                $this->Field .= '>' . _tk('choose') . '</option>';
            }

            if (is_array($SelectArray))
            {
                foreach ($SelectArray As $Code => $Text)
                {
                    $this->Field .= '<option value="' . $Code . '"';

                    if ((string)$Code === (string)$Value)
                    {
                        $this->Field .= ' selected="selected"';
                    }

                    $this->Field .= '>' . $Text . '</option>';
                }
            }

            $this->Field .= '</select>';
        }

        return $this;
    }

    function MakeRatingFields($paramArray)
    {
        require_once 'Source/libs/RiskRatingBase.php';

        if (bYN(GetParm('WEB_RISK_MATRIX', 'N')) && $paramArray[1] != 'Search')
        {
            return GetRiskGradingMatrix($paramArray);
        }
        else
        {
            return getRiskGradingFields($paramArray);
        }
    }

     function ConcatFields($Field1, $Field2)
     {
         $Text1 = $Field1->GetField();
         $Text2 = $Field2->GetField();

         $this->Field = $Text1 . $Text2;
         return $this;
     }

     function MakeChangedFlagField($Name, $CurrentValue = '')
     {
         $this->Field = '<input type="hidden" name="CHANGED-'
            . $Name . '" id="CHANGED-' . $Name . '" value="' . $CurrentValue . '" />';
         return $this;
	 }

     // not fully implemented - will currently only work when explicitly passing in an array of codes
    function MakeRadioButtons($Name, $CurrentValue = '', $Extra = '',
        $DefaultHidden = false, $onchangeExtra = '', $Module = '', $CodesArray = '', $suffix = '')
    {
        if (is_array($CodesArray) && !empty($CodesArray))
        {
            //build the radio buttons using the array of codes passed to the function
            $useCustomCodes = true;
        }

        if ($suffix != '')
        {
            $Name .= '_' . $suffix;
        }

        if ($this->FieldMode == "Print" || $this->FieldMode == "ReadOnly" || $DefaultHidden)
        {
            if ($useCustomCodes)
            {
                $this->Field = htmlspecialchars($CodesArray[$CurrentValue]);
            }
            else
            {
                $this->Field = htmlspecialchars(code_descr($Module, $FieldName, $CurrentValue));
            }

            $this->Field .= '<input type="hidden" name="' . $Name
                . '" id="' . $Name . '" value="' . ($DefaultHidden ? $DefaultHidden : htmlspecialchars($CurrentValue)) . '" />';
            return $this;
        }

        $onChange = 'FieldChanged(\'' . $Name . '\');';
        $onChange .= $onchangeExtra;

        if (!$useCustomCodes)
        {
            // get codes from DB
            // function GetCodeListSQL() in CodeSelectionCtrl.php?
        }

        $this->Field = '';

        foreach ($CodesArray as $code => $description)
        {
            $this->Field .= '<div>
                <input type="radio" name="' . $Name . '" id="' . $Name . '_' . $code
                . '" value = "' . $code . '" onchange="'. $onChange . '" '
                . ($CurrentValue == $code ? 'checked="checked" ' : '') . $Extra . '>'
                . '<label for="' . $Name . '_' . $code .'" id="' . $Name . '_' . $code .'_label">' . $description . '</label>
                </div>';
        }
    }

    /**
    * Determines whether or not a form field should should have additional advanced design
    * options, e.g. default value, dynamic sizing, actions.
    *
    * @param string  $FieldName                 The field name/id.
    * @param string  $FieldDef["Type"]          Definitions for this field (type etc).
    * @param string  $FieldDef["DataType"]      For UDFs, e.g. 'C', 'Y', 'T'.
    * @param boolean $FieldDef['AllowDefault']  Special condition to allow advanced design options by default.
    *
    * @return boolean Whether or not this field should have these options available.
    */
    function FieldHasAdvancedOptions($FieldName, $FieldDef)
    {
        // special fields - actions etc don't work.
        if (in_array($FieldName, array('link_patrelation', 'inc_med_drug', 'inc_med_drug_rt')))
        {
            return false;
        }

        // CB 2010-03-17: Needs to be deprecated when development done for form actions on non-coded fields (see DW-770)
        if ($FieldName == 'inc_reportedby')
        {
            if (bYN(getParm('REPORTEDBY_ACTIONS', 'N')))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        if (in_array($FieldDef["Type"], array('ff_select', 'multilistbox', 'multicode', 'yesno', 'checkbox')))
        {
            return true;
        }

        if ($FieldDef["Type"] == 'udf' && in_array($FieldDef["DataType"], array('C', 'Y', 'T')))
        {
            return true;
        }

        if ($FieldDef['AllowDefault'])
        {
            return true;
        }

        return false;
    }
}


class FormTable
{
    var $Contents;
    var $TitleWidth;
    var $UDFHasValue;
    var $FormMode;
    var $FieldDefs;
    var $FieldDefsExtra;
    var $PanelArray;
    var $Module;
    var $printSections;

    var $ffTable; //identifies the fmt_table value that fields on this form will have.

    var $CurrentSection; //tracks the section being processed so that the table rows can be tagged correctly.
    var $CurrentSectionEmpty; //false until a field in the section has a value, then true.

    protected $FormDesign; //The form design object that this form should refer to for design parameters.

    private $FormArray;
    
    /**
     * Used in situations where we're creating a link record (e.g. when linking contacts)
     * to determine whether or not the link already exists.
     * 
     * @var boolean
     */
    protected $newLink;

    private $ExpandSections = array();
    
    /**
     * A list of extra fields used to populate the
     * "Add extra field to this section" dropdown.
     * 
     * @var array
     */
    protected $extraFields;

    /**
     * @var Caches the extra field values for this form to avoid repeated db calls.
     */
    protected $extraFieldValues;

    function FormTable($FormModeParm = '', $FormModule = '', $FormDesign = null)
    {
        global $FieldDefs, $FieldDefsExtra;

        $this->FormMode = $FormModeParm;
        $this->TitleWidth = 25;
        $this->Module = $FormModule;

        if ($FormDesign)
        {
            //need to push the form design parameters into globals for any code that hasn't been converted.
            $this->FormDesign = $FormDesign;
            $FormDesign->LoadFormDesignIntoGlobals();
        }
        else
        {
            //need to construct a fake formdesign object. This code can be removed once we are no longer using globals.
            $this->FormDesign = new Forms_FormDesign();
            $this->FormDesign->PopulateFromCurrentDesignGlobals();
        }

        if ($FormModule)
        {
            $this->FieldDefs = $FieldDefsExtra[$FormModule];
        }
        else
        {
            // Field defs are in the format $FieldDefs["Module code"][Fields];
            // We need to transform it so that the module code is part of the
            // field element itself.
            foreach ($FieldDefsExtra as $Module => $Fields)
            {
                foreach ($Fields as $FieldName => $FieldDef)
                {
                    //if we know the module from the url, nothing should overwrite
                    //fields defined for this module.
                    if (!$FormModule || ($FormModule && !($this->FieldDefs[$FieldName]["Module"] == $FormModule)))
                    {
                        $this->FieldDefs[$FieldName] = $FieldDef;
                    }
                }
            }
        }

        $_SESSION["FieldsSetup"] = array(); //reset tracker of current field/section links.
    }

    /**
     * Setter for newLink
     * 
     * @param boolean $newLink
     */
    public function setNewLink($newLink)
    {
        $this->newLink = (bool) $newLink;
    }
    
    function GetFormTable()
    {

        return $this->Contents;
    }

    // Creates the $PanelArray array.
    private function GetCurrentRecordPanels($Data)
    {
        global $HideFields, $OrderSections, $NewPanels, $ModuleDefs;

        $MyFormMode = $this->FormMode;

        if ($this->FormArray['Parameters']['Suffix'])
        {
            $SuffixString = '_'.$this->FormArray['Parameters']['Suffix'];
        }

        if ($this->FormDesign->OrderSections)
        {
            ksort($this->FormDesign->OrderSections);

            foreach ($this->FormDesign->OrderSections as $OrderNo => $SectionName)
            {
                $SectionName .= $SuffixString;

                // Don't include the section if "Condition" exists and
                // evaluates to false, or if we are not in design mode and
                // the user has chosen to hide the section or if the section
                // does not appear in FormArray.
                if (($MyFormMode != "Design" && $this->FormDesign->HideFields[$SectionName]) ||
                    empty($this->FormArray[$SectionName]) ||
                    (is_array($this->FormArray[$SectionName]) &&
                        array_key_exists("Condition", $this->FormArray[$SectionName])
                        && !$this->FormArray[$SectionName]["Condition"]) ||
                    (is_array($this->FormArray[$SectionName]) &&
                        array_key_exists("NotModes", $this->FormArray[$SectionName]) &&
                        in_array($MyFormMode, $this->FormArray[$SectionName]["NotModes"])))
                {
                    continue;
                }

                // Start a new panel.  Only do so if the global array $NewPanels
                // has been set (in user-defined form design) or, if this is
                // not set, if the form parameter "NewPanel" is set to true.
                // The first section in a form should always be a new panel,
                // so we need to take this into account too (that's the reason
                // for the first condition, $Panels == '').
                if ($Panels == ''
                    || ($this->FormArray['Parameters']['Panels'] && ((!$this->FormDesign->NewPanels &&
                        $this->FormArray[$SectionName]["NewPanel"])
                       || $this->FormDesign->NewPanels[$SectionName])))
                {
                    $CurrentPanel = $SectionName;
                }

                $Panels[$CurrentPanel][] = $SectionName;
                $SectionsFound[$SectionName] = true;
            }
        }

        // Now get all unordered sections
        foreach ($this->FormArray as $SectionName => $SectionDetails)
        {
            if ($SectionsFound[$SectionName]
                ||($this->FormMode != "Design" && $this->FormDesign->HideFields[$SectionName])
                || (array_key_exists("Condition", $this->FormArray[$SectionName])
                    && !$this->FormArray[$SectionName]["Condition"])
                || (array_key_exists("NotModes", $this->FormArray[$SectionName])
                        && in_array($MyFormMode, $this->FormArray[$SectionName]["NotModes"])))
            {
                continue;
            }

            if ($Panels == '' || ($this->FormArray['Parameters']['Panels'] && ((!$this->FormDesign->NewPanels &&
                $SectionDetails["NewPanel"]) || $this->FormDesign->NewPanels[$SectionName])))
            {
                $CurrentPanel = $SectionName;
            }

            $Panels[$CurrentPanel][] = $SectionName;
        }

        $this->PanelArray = $Panels;
        return $Panels;
    }

    // Returns the array of panels created by GetCurrentRecordPanels()
    public function GetPanelArray()
    {
        return $this->PanelArray;
    }

    // Returns true if the form array has panels
    public function HasPanels()
    {
        return $this->FormArray['Parameters']['Panels'];
    }

    // Returns the name of the first panel in the list
    public function GetFirstPanel()
    {
        // Need to do this so we don't change the array pointer
        $Keys = array_keys($this->PanelArray);
        return $Keys[0];
    }

    // Returns the form array used to create the table
    public function GetFormArray()
    {
        return $this->FormArray;
    }

    // Sets the form array used to create the table
    public function SetFormArray($FormArray)
    {
        $this->FormArray = $FormArray;
    }

    function MakeTitleRow($Title, $tdClass = "section_title_row", $UseLists = true, $SectionName = '')
    {
        if ($UseLists)
        {
            $output = '
                <li name="'.$this->CurrentSection.'_title_row" id="'.$this->CurrentSection.'_title_row" class="'.$tdClass.'">
                    ' . $Title;

            if ($this->FormMode == 'Design' && sizeof($this->FormArray[$this->CurrentSection]['Rows']) > 0)
            {
                switch ($this->Module)
                {
                    case 'INC':
                        $Exceptions = array('multi_medication_record', 'causal_factor');
                        break;
                    case 'RAM':
                        $Exceptions = array('ass_ram');
                        break;
                    case 'PAL':
                        $Exceptions = array('subject');
                        break;
                    case 'COM':
                        $Exceptions = array('subject', 'issue');
                        break;
                    case 'CLA':
                        $Exceptions = array('causal_factor');
                        break;
                    case 'SAB':
                        $Exceptions = array('notepad');
                        break;
                    case 'CON':
                        $Exceptions = array('injury_section', 'property_section', 'injury');
                        break;
                    case 'MED':
                        $Exceptions = array('details');
                        break;
                    case 'ADM':
                        $Exceptions = array(
                            'profile', 'ALL_MODULES_parameters', 'ACT_parameters', 'INC_parameters', 'RAM_parameters',
                            'PAL_parameters', 'COM_parameters', 'CLA_parameters', 'PAY_parameters', 'CON_parameters',
                            'AST_parameters', 'MED_parameters', 'SAB_parameters', 'STN_parameters', 'LOC_parameters',
                            'LIB_parameters', 'HSA_parameters', 'HOT_parameters', 'CQO_parameters', 'ATM_parameters',
                            'ATM_notifications', 'ADM_parameters'
                        );
                        break;
                }

                $this->ExpandSections[] = $this->CurrentSection;
            }

            $output.= '
                </li>
            ';
        }
        else
        {
            $output = '
        <div name="'.$this->CurrentSection.'_title_row" id="'.$this->CurrentSection.'_title_row" class="'.$tdClass.'">
            ' . $Title . '
        </div>
        ';
        }

        $this->Contents .= $output;
    }

	function MakeRow($Title, $Field, $TitleWidth = false, $DefaultHidden = false, $ExtraParameters="",
                     $UseLists = true, $contactSearchBtn = '')
    {
        global $ModuleDefs;

        // ExtraParameters holds an array of other parameters passed.
		$RowClass = ($ExtraParameters["RowClass"]?$ExtraParameters["RowClass"]:"windowbg2");

        $Width = !empty($TitleWidth) ? $TitleWidth : $this->TitleWidth;

		if ($DefaultHidden)
        {
            $output = '<div>' . $Field->GetField() . '</div>';
        }
        else
        {
            if ($UseLists)
            {
                $output = '
                <li class="field_div" name="'.$this->CurrentSection.'_row" id="'.$ExtraParameters['dbfield'].'_row" '.($ExtraParameters['hidden'] && $this->FormMode != 'Design' ? 'style="display:none"' : '').'>';
            }
            else
            {
                $output = '
                <div class="field_div" name="'.$this->CurrentSection.'_row" id="'.$ExtraParameters['dbfield'].'_row" '.($ExtraParameters['hidden'] && $this->FormMode != 'Design' ? 'style="display:none"' : '').'>';
            }

            $output .= '
                <div class="field_label_div" style="width:'.$Width.'%">
                '. $Title. '
                </div>';

            $output .= '<div class="field_input_div" style="width:'.(95-$Width).'%; word-wrap: break-word;">'. $Field->GetField();

            if (!empty($contactSearchBtn))
            {
                $output .= $contactSearchBtn;
            }

            $output .= '</div>';

            //If we are searching, and this is a coded field with a tag set attached, we need to give the user the option
            //to search on tags.
            if ($this->FormMode == 'Search' && CanUseCodeTags() && is_a($Field, 'Forms_SelectField'))
            {
                $sql = '
                    SELECT
                        [group], name
                    FROM
                        code_tag_linked_fields
                    LEFT JOIN
                        code_tag_groups on code_tag_groups.recordid = code_tag_linked_fields.[group]
                    WHERE
                        field = :field
                        AND [table] = :table
                ';

                $TagGroups = DatixDBQuery::PDO_fetch_all($sql, array(
                    'field' => $Field->getName(),
                    'table' => $ModuleDefs[$Field->getModule()]['TABLE']
                ), PDO::FETCH_KEY_PAIR);

                foreach ($TagGroups as $GroupID => $GroupName)
                {
                    $sql = 'SELECT recordid, name FROM code_tags WHERE group_id = :group';
                    $Tags = DatixDBQuery::PDO_fetch_all($sql, array('group' => $GroupID), PDO::FETCH_KEY_PAIR);

                    if ($_GET['searchtype'] == 'lastsearch')
                    {
                        $tagValue = $_SESSION[$Field->getModule()]["LAST_SEARCH"]['searchtaggroup_'.$GroupID.'_'.$Field->getName()];
                    }

                    $TagSearchField = Forms_SelectFieldFactory::createSelectField('searchtaggroup_'.$GroupID.'_'.$Field->getName(), $Field->getModule(), $tagValue, $this->FormMode);
                    $TagSearchField->setCustomCodes($Tags);

                    $output .= '
                        <div class="field_label_div" style="width:'.$Width.'%">
                        &nbsp;&nbsp;Tags: '. $GroupName. '
                        </div>';

                    $output .= '<div class="field_input_div" style="width:'.(95-$Width).'%; word-wrap: break-word;">'. $TagSearchField->GetField().'</div>';
                }
            }

            if ($UseLists)
            {
                $output .= '</li>';
            }
            else
            {
                $output .= '</div>';
            }
        }

        $this->Contents .= $output;
    }

    function MakeTable()
    {
        $output = '
            <a name="datix-content" id="datix-content"></a>
            <div class="external_border">
                <div class="internal_border">
                    <ol>
                ' . $this->Contents . '
                    </ol>
                </div>
            </div>
        ';
        $this->Contents = $output;
    }

    function MakeSection($SectionName, $SectionDisplay)
    {
        $output = '<div id="' . $SectionName . '"';

        if (!$SectionDisplay)
        {
            $output .= ' style="display:none"';
        }

        $output .= '>';

        $this->Contents = $output . $this->Contents . '</div>';
    }

    // Gets an extra field group for the specified record in the specified module
    function MakeOrphanedUDFGroup($Mod, $RecordID, $Data)
    {
        /*
        - Display all the extra field which have had values stored against them when the incident was submitted
        */
        $UDFFields = Fields_ExtraField::getOrphanedUDFs($Mod, $RecordID, $this->FormDesign);

        foreach ($UDFFields as $row)
        {
            $UDFValues = array(
                'udv_string' => $row['udv_string'],
                'udv_number' => $row['udv_number'],
                'udv_date' => $row['udv_date'],
                'udv_money' => $row['udv_money'],
                'udv_text' => $row['udv_text'],
            );

            $UDFProperties = array(
                'fld_name' => $row['fld_name'],
                'grp_id' => $row['grp_id'],
                'fld_id' => $row['fld_id'],
                'fld_type' => $row['fld_type'],
                'fld_format' => $row['fld_format']
            );

            $this->MakeUDFRow($UDFProperties, $UDFValues, array('full_audit' => $Data['full_audit']));
            $this->UDFHasValues = true;
        }
    }

    // Function takes the name of a UDF and a group and
    // returns an array containing a table row and a value array.
    function MakeUDFRowByName($UDFGroupName, $UDFFieldName, $RecordID = 0)
    {

        $sql = "SELECT udf_fields.recordid AS fld_id, fld_name,
            udf_groups.recordid as grp_id, fld_type, fld_format
            FROM udf_fields, udf_groups, udf_links
            WHERE
            fld_name = '$UDFFieldName'
            AND grp_descr = '$UDFGroupName'
            AND udf_fields.recordid = udf_links.field_id
            AND udf_groups.recordid = udf_links.group_id
            AND udf_groups.mod_id = 3";

        $result = db_query($sql);

        if ($row = db_fetch_array($result))
        {
            if ($RecordID != 0)
            {
                $ValueRow = $this->GetUDFValue(3, $RecordID, $row["grp_id"], $row["fld_id"]);
            }

            $this->MakeUDFRow($row, $ValueRow);

            if ($ValueRow != "")
            {
                $this->UDFHasValues = true;
            }
        }
    }

    function MakeUDFRow($UDF, $Values, $Data = "")
    {
        $RowTitle = '<b>'.$UDF[fld_name].'</b>';
        $RowType = $UDF["fld_type"];

        $FieldName = "UDF_{$RowType}_$UDF[grp_id]_$UDF[fld_id]";
        $FieldObj = $this->MakeUDFField($FieldName, $UDF["fld_id"],
            $UDF["fld_type"], $Values, $UDF['fld_name'], $UDF['fld_format']);

        if ($FieldObj instanceOf FormField && $this->FormMode != "Search" && $this->FormMode != "Print" &&
            $this->FormMode != "ReadOnly")
        {
            $ChangedField = new FormField;
            $ChangedField->MakeChangedFlagField($FieldName);
            $FieldObj->ConcatFields($FieldObj, $ChangedField);
        }

        $this->MakeRow($RowTitle, $FieldObj, $this->TitleWidth);

        if (($FullAudit = $Data["full_audit"])
            && $FullAudit[$FieldName])
        {
            $AuditObj = new FormField();

            foreach ($FullAudit[$FieldName] as $Audit)
            {
                if ($UDF["fld_type"] == 'C' || $UDF["fld_type"] == 'T' || $UDF["fld_type"] == 'Y')
                {
                    $DetailArray = explode(" ",$Audit["aud_detail"]);
                }
                else
                {
                    $DetailArray = array($Audit["aud_detail"]);
                }

                foreach ($DetailArray as $i => $Detail)
                {
                    if ($FieldDef['Type'] == 'ff_select' || $FieldDef['Type'] == 'multilistbox' ||
                        $InputFieldName == 'rep_approved')
                    {
                        $DetailArray[$i] = code_descr($FieldDef['Module'],$InputFieldName,$Detail);
                    }
                    else
                    {
                        $DetailArray[$i] = $Detail;
                    }

                    $DetailArray[$i] = nl2br(htmlspecialchars($DetailArray[$i]));

                    if ($DetailArray[$i] == "")
                    {
                        $DetailArray[$i] = "&lt;no value&gt;";
                    }

                }

                $display = implode(", " , $DetailArray);

                $this->MakeRow("&nbsp;&nbsp;&nbsp;" . code_descr("INC", "inc_mgr", $Audit["aud_login"]) . " " . FormatDateVal($Audit["aud_date"], true),
                    $AuditObj->MakeCustomField($display));

            }
        }
    }

    // Function only makes the field part of the UDF row.
	function MakeUDFField($FieldName, $FieldID, $FieldType, $Values, $Title='', $Format = '', $changedValue = '',
                          $formSection = '', $errorReload = false, $maxLength = null, $mode = '')
    {
        global $HideFields, $ReadOnlyFields, $DefaultValues, $TextareaMaxChars;

        $HasValue = false;

        // When we reach this point the FieldName has the section appended, so we need this magic to determine the field name.
        $ExplodedFieldName = explode('_', $FieldName);
        $FieldNameWithoutSection = $ExplodedFieldName[0].'_'.$ExplodedFieldName[1].'_'.$ExplodedFieldName[2].'_'.$ExplodedFieldName[3];

        if (!empty($Values) && is_array($Values))
        {
            foreach ($Values as $Value)
            {
                if ($Value != '')
                {
                    $HasValue = true;
                    break;
				}
            }
        }

        $FieldValue = '';

        if ($_POST[$FieldName] || ($this->FormDesign->DefaultValues[$FieldName] != '' && $this->FormMode == 'New'))
        {
            $HasValue = true;
        }

        if ($this->FormMode != 'Search' && is_array($this->FormDesign->ReadOnlyFields) &&
            array_key_exists($FieldName, $this->FormDesign->ReadOnlyFields))
        {
            $FieldObj = new FormField('ReadOnly');
            $mode = 'ReadOnly';
        }
        else
        {
            $FieldObj = new FormField($this->FormMode);
            $mode = $this->FormMode;
        }

        if ($this->FormDesign->HideFields[$FieldName] && $this->FormDesign->DefaultValues[$FieldName] &&
            $this->FormMode == 'New')
        {
            //total hack: we should be letting even default values fall through to the Make...Field functions,
            // but since we're not we have to have this to allow the TODAY code to work correctly.
            if ($FieldType == 'D' && $this->FormDesign->DefaultValues[$FieldName] == "TODAY")
            {
                $this->FormDesign->DefaultValues[$FieldName] = FormatDateVal(date('Y-m-d H:i:s.000'));
            }

            $FieldObj->Field = '<input type="hidden" name="' . $FieldName . '" id="' . $FieldName . '" value="' . $this->FormDesign->DefaultValues[$FieldName] . '"/>';
            return $FieldObj;
        }

        switch ($FieldType)
        {
            case "S":
            case FieldInterface::STRING:
                if ($HasValue === true)
                {
                    $FieldValue = FirstNonNull(array($_POST["$FieldName"], $Values["udv_string"], ($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                $FieldObj->MakeInputField($FieldName, 30, $maxLength ?: 254, $FieldValue);
                break;
            case "Y":
            case FieldInterface::YESNO:
                if ($HasValue === true)
                {
                    $FieldValue = FirstNonNull(array($_POST["$FieldName"], $Values["udv_string"], ($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }
				
                if ($this->FormDesign->DisplayAsRadioButtons[$FieldNameWithoutSection] && $mode != 'Search')
                {
                    $field = Forms_RadioButtonsFieldFactory::createField ($FieldName, $module, $Values["udv_string"], $mode, $Title, $this->CurrentSection, $changedValue, '', isset($this->FormDesign->MandatoryFields[$FieldName]));
                }
                else
                {
                    $field = $FieldObj->MakeYesNoSelect($FieldName, $module, $FieldValue, $FieldObj->FieldMode, $output, $Title, '', $changedValue);
                }

                break;
            case "D":
            case FieldInterface::DATE:
                if ($HasValue === true)
                {
                    $FieldValue = FirstNonNull(array($_POST["$FieldName"], $Values["udv_date"], ($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                $FieldObj->MakeDateField($FieldName, $FieldValue);
                break;
            case "C":
            case FieldInterface::CODE:
				if ($HasValue === true)
                {
                    $FieldValue = FirstNonNull(array($_POST["$FieldName"], $Values["udv_string"], ($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                $fld_code_like = GetFieldCodeLike($FieldID);

                if ($fld_code_like)
                {
                    $module = getModuleFromField($fld_code_like);
                }

                if ($this->FormDesign->DisplayAsRadioButtons[$FieldNameWithoutSection] && $mode != 'Search')
                {
                    $field = Forms_RadioButtonsFieldFactory::createField ($FieldName, $module, $Values["udv_string"], $mode, $Title, $this->CurrentSection, $changedValue, '', isset($this->FormDesign->MandatoryFields[$FieldName]));
                }
                else
                {
                    $field = Forms_SelectFieldFactory::createSelectField($FieldName, $module, $FieldValue, $mode, false, $Title, $formSection, $changedValue);
                }

				break;
            case "N":
            case FieldInterface::NUMBER:
                if ($HasValue === true)
                {
                    //Don't try and format if we are reloading after error
                    if (!$errorReload)
                    {
                        //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                        if ($Format != "")
                        {
                            $FormattedNumber = GuptaFormatEmulate($Values["udv_number"], $Format);
                        }
                        else
                        {
                            $FormattedNumber = number_format(floatval($Values["udv_number"]), 2, '.', '');
                        }
                    }
                    else
                    {
                        $FormattedNumber = $Values["udv_number"];
                    }

                    $FieldValue = FirstNonNull(array($FormattedNumber, ($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                $FieldObj->MakeNumberField($FieldName, $FieldValue);
                break;
			case "M":
			case FieldInterface::MONEY:
                if ($HasValue === true)
                {
                    //Don't try and format if we are reloading after error
                    if (!$errorReload)
                    {
                        //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                        if ($Format != "")
                        {
                            $FormattedNumber = GuptaFormatEmulate($Values["udv_money"], $Format);
                        }
                        else
                        {
                            $FormattedNumber = number_format(floatval($Values["udv_money"]), 2, '.', '');
                        }
                    }
                    else
                    {
                        $FormattedNumber = $Values["udv_money"];
                    }

                    $FieldValue = FirstNonNull(array($FormattedNumber, ($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                $FieldObj->MakeMoneyField($FieldName, $FieldValue);
                break;
            case "T":
            case FieldInterface::MULTICODE:
                if ($HasValue === true)
                {
                    if (is_array($_POST["$FieldName"]))
                    {
                        $sPostStr = implode(" ",$_POST["$FieldName"]);
                    }
                    else
                    {
                        $sPostStr = '';
                    }

                    $FieldValue = FirstNonNull(array($sPostStr, $Values["udv_string"],($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                if ($this->FormDesign->DisplayAsCheckboxes[$FieldNameWithoutSection] && $mode != 'Search')
                {
                    $field = Forms_MultipleCheckboxesFieldFactory::createField ($FieldName, $module, $Values["udv_string"], $mode, $Title, $this->CurrentSection, $changedValue);
                }
                else
                {
                    $field = Forms_SelectFieldFactory::createSelectField($FieldName, $module, $FieldValue, $mode, true, $Title, $formSection, $changedValue);
                }

                break;
            case "E": // email field
                if ($HasValue === true)
                {
                    $FieldValue = FirstNonNull(array($_POST["$FieldName"], $Values["udv_string"],($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                $FieldObj->MakeEmailField($FieldName, 30, 64, $FieldValue);
                break;
            case "L":
            case FieldInterface::TEXT:
                if ($HasValue === true)
                {
                    $FieldValue = FirstNonNull(array($_POST["$FieldName"], $Values["udv_text"], ($this->FormMode == 'New' ? $this->FormDesign->DefaultValues[$FieldName] : null)));
                }

                $FieldObj->MakeTextAreaField($FieldName, 7, 70, $TextareaMaxChars[$FieldName], $FieldValue, true, true, '', $Title, $_POST['CURRENT_'.$FieldName]);
                break;
        }

        return isset($field) ? $field : $FieldObj;
    }

    function MakeUDFGroupByName($UDFGroupName, $RecordID = 0)
    {
        $sql = "SELECT recordid
                FROM udf_groups
                WHERE grp_descr = '$UDFGroupName'
                AND mod_id = 3";
        $result = db_query($sql);

        if ($row = db_fetch_array($result))
        {
            return $this->MakeUDFGroupByID($row["recordid"], $RecordID);
        }
    }

    // Function takes the name of a UDF group and a recordid and returns an
    // array of ("Contents" => table rows, "HasValues" => true if contains any values)
    function MakeUDFGroupByID($UDFGroupID, $RecordID = 0, $nModuleId = 3)
    {
        $sql = "SELECT udf_fields.recordid AS fld_id, fld_name, fld_type, fld_format
                FROM udf_fields, udf_links
                WHERE
                udf_fields.recordid = udf_links.field_id
                AND udf_links.group_id = $UDFGroupID
                ORDER BY udf_links.listorder";

        $result = db_query($sql);

        while ($row = db_fetch_array($result))
        {
            $row["grp_id"] = $UDFGroupID;

            if ($RecordID != 0)
            {
                $ValueRow = $this->GetUDFValue($nModuleId, $RecordID, $UDFGroupID,  $row["fld_id"]);
            }

            if ($ValueRow != "")
            {
                $this->UDFHasValues = true;
            }

            $this->MakeUDFRow($row, $ValueRow);
        }
    }

    function getExtraFieldValues($recordid, $module)
    {
        $moduleId = GetModIDFromShortName($module);

        $sql = "
            SELECT
                field_id,
                group_id,
                udv_string,
                udv_number,
                udv_date,
                udv_money,
                udv_text
            FROM
                udf_values
            WHERE
                udf_values.mod_id = ? AND
                udf_values.cas_id = ?
            ";

        $rawExtraFieldValues = DatixDBQuery::PDO_fetch_all($sql, [$moduleId, $recordid]);

        $extraFieldValues = array();

        foreach ($rawExtraFieldValues as $resultSet)
        {
            $extraFieldValues[$recordid][$moduleId][$resultSet['field_id']][$resultSet['group_id']] =
                array('udv_string' => $resultSet['udv_string'], 'udv_number' => $resultSet['udv_number'], 'udv_date' => $resultSet['udv_date'], 'udv_money' => $resultSet['udv_money'], 'udv_text' => $resultSet['udv_text']);
        }

        return $extraFieldValues;
    }

    function GetUDFValue($ModID, $CasID, $GroupID, $FieldID)
    {
        static $cache;

        if (!$CasID || !$FieldID)
        {
            return '';
        }

        if ($CasID != '' && $ModID != '' && $FieldID != '' && $GroupID != '' && isset($this->extraFieldValues))
        {
            if (isset($this->extraFieldValues[$CasID][$ModID][$FieldID][$GroupID]))
            {
                return $this->extraFieldValues[$CasID][$ModID][$FieldID][$GroupID];
            }
            else
            {
                return array('udv_string' => null, 'udv_number' => null, 'udv_date' => null, 'udv_money' => null, 'udv_text' => null);
            }
        }

        if (isset($cache[$ModID][$GroupID][$CasID]))
        {
            return $cache[$ModID][$GroupID][$CasID][$FieldID];
        }

        // For debugging: $Contents .= MakeRow("SQL", $sql);
        // Get the UDF value
        $sql = "
            SELECT
                udv_string,
                udv_number,
                udv_date,
                udv_money,
                udv_text,
                field_id
            FROM
                udf_values
            WHERE
                " . (($ModID != '') ? "mod_id = :mod_id AND" : '') . "
                cas_id = :cas_id
                ".(isset($GroupID) ? "AND group_id = :group_id" : '')."
                AND field_id = :field_id";

        $PDOParams = array(
            'cas_id'   => $CasID,
            'field_id' => $FieldID
        );

        if ($ModID != '')
        {
            $PDOParams['mod_id'] = $ModID;
        }

        if ($GroupID != '')
        {
            $PDOParams['group_id'] = $GroupID;
        }

        $result = DatixDBQuery::PDO_fetch_all($sql, $PDOParams);

        while ($row = array_shift($result))
        {
            $row_field_id = $row['field_id'];
            unset ($row['field_id']);

            $cache[$ModID][$GroupID][$CasID][$row_field_id] = $row;
        }

        return $cache[$ModID][$GroupID][$CasID][$FieldID];
    }

    // Takes an array of UDF Group IDs and creates a table from all of them.
    // This is used to display UDF groups on the DIF1 form from the
    // $DIF1UDFGroups global variable set in the UserDIF1Settings file.
    function MakeUDFGroupsFromIDs($GroupIDs, $recordid = "", $ModID)
    {
        if ($GroupIDs == "")
        {
            return "";
        }

        $this->Contents .= '<div class="form_table">';

        if (is_array($GroupIDs))
        {
            $GroupIDArray = $GroupIDs;
        }
        else
        {
            $GroupIDArray = explode(" ", $GroupIDs);
        }

        foreach ($GroupIDArray as $GroupID)
        {
            $sql = "SELECT grp_descr
                    FROM udf_groups
                    WHERE recordid = $GroupID
                    AND mod_id = $ModID";
            $result = db_query($sql);

            if ($row = db_fetch_array($result))
            {
                $this->MakeTitleRow("<b>$row[grp_descr]</b>");
                $this->MakeUDFGroupByID($GroupID, $recordid);
            }
        }

        $this->Contents .= '</div>';
        return $this;
    }

    function MakeUDFGroupArray($GroupID, $RecordID, $Suffix = '')
    {
        global $ExpandSections;

        if (isset($_SESSION["udf_groups"][$GroupID]))
        {
            $row = $_SESSION["udf_groups"][$GroupID];
        }
        else
        {
            $sql = "SELECT grp_descr, udf_groups.mod_id, mod_module as mod_name
                    FROM udf_groups, modules
                    WHERE recordid = :recordid
                    AND modules.mod_id=udf_groups.mod_id";


            $row = DatixDBQuery::PDO_fetch($sql, array('recordid' => $GroupID));
            $_SESSION["udf_groups"][$GroupID] = $row;
        }

        if ($this->FormMode == "Design")
        {
            $row["grp_descr"] = _tk('extra_field_group') . ": $row[grp_descr]";
        }

        $UDFGroupArray = array ("Type" => "udfgroup",
                    "ContactSuffix" => $Suffix,
                    "GroupID" => Sanitize::SanitizeInt($GroupID),
                    "RecordID" => Sanitize::SanitizeInt($RecordID),
                    "Title" => $row["grp_descr"]);

        $ModID = $row["mod_id"];
        $ModName = $row["mod_name"];


        if (isset($_SESSION["udf_group_fields"][$GroupID]))
        {
            $result = $_SESSION["udf_group_fields"][$GroupID];
        }
        else
        {
            $sql = "SELECT udf_fields.recordid AS fld_id, fld_name, fld_type
                    FROM udf_fields, udf_links
                    WHERE
                    udf_fields.recordid = udf_links.field_id
                    AND udf_links.group_id = $GroupID
                    ORDER BY udf_links.listorder";

            $result = DatixDBQuery::PDO_fetch_all($sql);
            $_SESSION["udf_group_fields"][$GroupID] = $result;
        }

        foreach ($result as $row )
        {
            $UDFName = "UDF_$row[fld_type]_${GroupID}_$row[fld_id]";
            $UDFNameSuffix = "UDF_$row[fld_type]_${GroupID}_$row[fld_id]".($Suffix ? '_'.$Suffix : '');
            $UDFGroupRows[$UDFName] = array("Type" => "udf",
                "Name" => $UDFName,
                "ID" => Sanitize::SanitizeInt($row["fld_id"]),
                "Title" => ($this->FormDesign->UserLabels[$UDFNameSuffix] != "" && $this->FormMode != 'Design' ? $this->FormDesign->UserLabels[$UDFNameSuffix] : $row["fld_name"]),
                "DataType" => $row["fld_type"],
                "GroupID" => Sanitize::SanitizeInt($GroupID),
                "ModID" => Sanitize::SanitizeInt($ModID),
                "Module" => Sanitize::SanitizeInt($ModName),
                "RecordID" => Sanitize::SanitizeInt($RecordID)
            );
        }

        $UDFGroupArray["Rows"] = $UDFGroupRows;

        return $UDFGroupArray;
    }

    private function MakeOrphanedUDFSection($SectionName, $Data, $Module)
    {
        global $HideFields, $ReadOnlyFields, $UserLabels, $UserExtraText;

        $FormType = $this->FormMode;

        if (is_array($this->FormDesign->ReadOnlyFields) &&
            array_key_exists('orphanedudfs', $this->FormDesign->ReadOnlyFields))
        {
            $FormType = 'ReadOnly';
        }

        if ($FormType != 'Search' && $Data["recordid"])
        {
            $ExtraFieldGroup = new FormTable($FormType, $Module);

            if ($this->FormDesign->HideFields[$SectionName])
            {
                return; //do not display the SectionExtraFields section, it is hidden
            }

            $ExtraFieldGroup->MakeOrphanedUDFGroup(
                $Module,
                $Data['recordid'],
                $Data);

            if ($ExtraFieldGroup->UDFHasValues)
            {
                echo $ExtraFieldGroup->GetFormTable();
            }
        }
    }

    function SetupShowConditionFunctions($aParams)
    {
        global $ExpandSections, $ExpandFields, $JSFunctions, $FieldDefs, $MoveFieldsToSections, $TimestampFields;

        $sectionActions = array();

        if ($this->FormArray['Parameters']['Suffix'])
        {
            $SuffixString = '_'.$this->FormArray['Parameters']['Suffix'];
        }

        $JS = '';

        if (is_array($this->FormDesign->ExpandFields))
        {
            foreach ($this->FormDesign->ExpandFields as $field => $Actions)
            {
                foreach ($Actions as $ActionDetails)
                {
                    $ActionDetails['triggerfield'] = $field;
                    $FieldConditions[$ActionDetails['field']][] = $ActionDetails;
                }
            }
        }

        if (is_array($this->FormDesign->ExpandSections))
        {
            foreach ($this->FormDesign->ExpandSections as $field => $Actions)
            {
                foreach ($Actions as $ActionDetails)
                {
                    $ActionDetails['triggerfield'] = $field;
                    $SectionConditions[$ActionDetails['section']][] = $ActionDetails;
                }
            }
        }

        if (is_array($this->FormDesign->MoveFieldsToSections))
        {
            foreach ($this->FormDesign->MoveFieldsToSections as $field => $Details)
            {
                $MovedFields[$Details['New']][] = $field;
            }
        }

        foreach ($this->FormArray as $Section => $SectionDetails)
        {
            if (isset($SectionDetails['Condition']) && $SectionDetails['Condition'] === false)
            {
                // skip this section if it's not displayed
                continue;
            }

            // If there is a hard-coded action on this section, ensure we take it into account.
            if ($this->FormArray[$Section]['Show'])
            {
                $SectionConditions[$Section][] = array(
                    'section' => $Section,
                    'values' => array('on', 'Y'),
                    'triggerfield' => $this->FormArray[$Section]['Show']
                    );
            }

            if (is_array($SectionConditions[$Section]) && !empty($SectionConditions[$Section]))
            {
                $CheckStateFunc = 'function CheckState_'.$Section.'() {
                ';
                $CheckStateFunc .= 'var actualValues, ShowThisSection = false;
                ';

                foreach ($SectionConditions[$Section] as $Details)
                {
                    if(($this->Module == 'COM' || $this->Module == 'INC') && ($this->FormDesign->Module == 'INC' || $this->FormDesign->Module == 'COM'))
                    {
                        $shouldAddRenderFields = true;
                    }
                    elseif($this->Module == 'CON' && $this->FormDesign->getLevel() == 1)
                    {
                        if($this->FormDesign->getParentModule() == 'COM' || $this->FormDesign->getParentModule() == 'INC')
                        {
                            $shouldAddRenderFields = true;
                        }
                        else
                        {
                            $shouldAddRenderFields = false;
                        }
                    }
                    else
                    {
                        $shouldAddRenderFields = false;
                    }

                    $CheckStateFunc .= "if(FieldIsAvailable('".$Details['triggerfield']."')){";

                    $CheckStateFunc .= "actualValues = getValuesArray('".$Details['triggerfield']."');
                    ";

                    $CheckStateFunc .= "if(actualValues.inArray(['".implode("', '",$Details['values'])."']))
                    {
                        ShowThisSection = true;
                        ".($shouldAddRenderFields ?
                            "renderFields('#".$Details['section']."_title_row',
                            ".json_encode(array(
                                'module' => $this->Module,
                                //probably, this should always use $this->FormDesign->Level, however this is a much lower risk change.
                                'form_level' => $GLOBALS['formlevel'] ?: $this->FormDesign->Level,
                                'section_name' => $Details['section'],
                                'form_id' => $this->FormDesign->ID,
                                'readonly' => null,
                                'form_type' => $this->FormMode,
                                'recordid' => $aParams['recordid']
                            )).", function(){ TriggerOnChangeSection('".$Details['section']."'); initialiseSpellchecker(); });" : "initialiseSpellchecker();")."}";
                    $CheckStateFunc .= "}";
                }

                $CheckStateFunc .= "if(ShowThisSection){ ShowSection('".$Section."'); } else { HideSection('".$Section."') }";

                $CheckStateFunc .= "}";

                $JSFunctions[] = $CheckStateFunc;
            }

            if (is_array($SectionDetails['Rows']))
            {
                $OnSectionChangeFunction = '';

                foreach ($SectionDetails['Rows'] as $Field)
                {
                    $isFieldAnArray = is_array($Field);

                    if ($isFieldAnArray)
                    {
                        $FieldName = $Field['Name'].$SuffixString;
                    }
                    else
                    {
                        $FieldName = $Field.$SuffixString;
                    }

                    if (!$this->FormDesign->MoveFieldsToSections[$FieldName] && (!$isFieldAnArray ||
                        !isset($Field['Condition']) || $Field['Condition']))
                    {
                        $OnSectionChangeFunction .= 'TriggerOnChange(\''.$FieldName.'\');';
                        $ConstructFieldArray[$FieldName] = $Section;
                    }
                }

                if (is_array($MovedFields[$Section]))
                {
                    foreach ($MovedFields[$Section] as $Field)
                    {
                        $OnSectionChangeFunction .= 'TriggerOnChange(\''.$Field.'\');';
                        $ConstructFieldArray[$Field] = $Section;
                    }
                }

                if ($OnSectionChangeFunction)
                {
                    $OnSectionChangeFunction = 'function OnSectionChange_'.$Section.'() {'.$OnSectionChangeFunction.'}';
                    $JSFunctions[] = $OnSectionChangeFunction;
                }

                foreach ($SectionDetails['Rows'] as $Field)
                {
                    if (is_array($Field))
                    {
                        $FieldDetails = SafelyMergeArrays(array($this->FieldDefs[$Field['Name']], $Field));
                        $FieldName = $Field['Name'].$SuffixString;
                    }
                    else
                    {
                        $FieldDetails = $this->FieldDefs[$Field];
                        $FieldName = $Field.$SuffixString;
                    }

                    if ($FieldName)
                    {
                        if (is_array($FieldConditions[$FieldName]))
                        {
                            $CheckStateFunc = 'function CheckState_'.$FieldName.'()
                            {
                                var actualValues, ShowThisField = false;
                            ';

                            foreach ($FieldConditions[$FieldName] as $Details)
                            {
                                $CheckStateFunc .= 'if(FieldIsAvailable(\''.$Details['triggerfield'].'\')){';
                                $CheckStateFunc .= 'actualValues = getValuesArray(\''.$Details['triggerfield'].'\');
                                ';
                                $CheckStateFunc .= 'if(actualValues.inArray([\''.implode('\',\'',$Details['values']).'\'])) { ShowThisField = true; }
                                ';
                                $CheckStateFunc .= '}';
                            }

                            $CheckStateFunc .= '
                                if(ShowThisField) { ShowFieldRow(\''.$FieldName.'\') } else { HideFieldRow(\''.$FieldName.'\') }';

                            if($FieldDefs[$this->Module][$FieldName]['MandatoryField'])
                            {
                                $CheckStateFunc .= '
                                if(ShowThisField) { ShowFieldRow(\''.$FieldDefs[$this->Module][$FieldName]['MandatoryField'].'\') } else { HideFieldRow(\''.$FieldDefs[$this->Module][$FieldName]['MandatoryField'].'\') }';
                            }

                            if ($TIMESTAMPFields[$FieldName])
                            {
                                $CheckStateFunc .= '
                                if(ShowThisField) { ShowFieldRow(\'CURRENT_'.$FieldName.'\') } else { HideFieldRow(\'CURRENT_'.$FieldName.'\') }';
                            }

                            $CheckStateFunc .= '}';

                            $JSFunctions[] = $CheckStateFunc;

                        }

                        $CheckStates = array();
                        $Messages = array();

                        if ($FieldDetails['Div']) //if there is a hard-coded action on this field, ensure we take it into account.
                        {
                            $CheckStates[] = $FieldDetails['Div'];
                        }

                        if (is_array($this->FormDesign->ExpandSections[$FieldName]))
                        {
                            foreach ($this->FormDesign->ExpandSections[$FieldName] as $ActionDetails)
                            {
                                if ($ActionDetails['section'] &&
                                    $this->FormArray[$ActionDetails['section']]['Condition'] !== false)
                                {
                                    $CheckStates[] = $ActionDetails['section'];
                                }

                                if ($ActionDetails['alerttext'])
                                {
                                    $Messages[] = $ActionDetails;
                                }
                            }
                        }

                        if (is_array($this->FormDesign->ExpandFields[$FieldName]))
                        {
                            foreach ($this->FormDesign->ExpandFields[$FieldName] as $ActionDetails)
                            {
                                if ($ActionDetails['field'])
                                {
                                    $CheckStates[] = $ActionDetails['field'];
                                }

                                if ($ActionDetails['alerttext'])
                                {
                                    $Messages[] = $ActionDetails;
                                }
                            }
                        }

                        if (!empty($CheckStates))
                        {
                            $CascadeFunc = 'function CascadeCheckState_'.$FieldName.'() {
                            ';

                            foreach ($CheckStates as $FunctionName)
                            {
                                $CascadeFunc .= 'if(typeof(CheckState_'.$FunctionName.') === \'function\'){CheckState_'.$FunctionName.'();}';
                            }

                            $CascadeFunc .= '}';

                            $JSFunctions[] = $CascadeFunc;
                        }

                        $OnChangeFunc = 'function OnChange_'.$FieldName.'() {
                            ';

                        if (!empty($Messages) && ($this->FormMode != 'Search' ||
                            !bYN(GetParm('NO_SEARCH_ALERTS', 'N'))))
                        {
                            foreach ($Messages as $Details)
                            {
                                $OnChangeFunc .= 'var actualValues = getValuesArray(\''.$FieldName.'\');';
                                $Details['alerttext'] = str_replace('\\', '\\\\\\\\',$Details['alerttext']);
                                $OnChangeFunc .= 'if(actualValues.inArray([\''.implode('\',\'',$Details['values']).'\'])) { alert(\''.str_replace('\'','\\\'',$Details['alerttext']).'\'); }';
                            }
                        }

                        $children = GetChildren(array(
                            'module' => $aParams['module'],
                            'field' => RemoveSuffix($FieldName)
                        ));

                        if (!empty($children[0]) || !empty($children[1]))
                        {
                            foreach ($children as $childField)
                            {
                                if ($childField != '')
                                {
                                    // Added check to make sure that a datixselect object has actually been stored
                                    // on the element. However it would be better to just to not attach this event
                                    // handler at all if the element is readonly. Not sure how to determine if a
                                    // child field is readonly at this point though
                                    $OnChangeFunc .= 'if(jQuery(\'#'.$childField.$SuffixString.'_title\').length){jQuery(\'#'.$childField.$SuffixString.'_title\').checkClearChildField('.(bYN(GetParm('RETAIN_COMBO_CHILDREN', 'Y')) ? 'true' : 'false').');}';
                                }
                            }
                        }

                        $parents = GetParents(array('module' => $aParams['module'], 'field' => RemoveSuffix($FieldName)));

                        if ($parents[0] != '' && $parents[1] == '' && bYN(GetParm('CASCADE_DATA_TO_PARENTS', 'N')) &&
                            IsLocationField($parents[0], $this->Module) && IsLocationField($FieldName, $this->Module))
                        {
                            $parentField = $parents[0];

                            $OnChangeFunc .= '
                                if (jQuery(\'#'.$FieldName.'_title\').length){jQuery(\'#'.$FieldName.'_title\').cascadeDataToParentField();}';
                        }

                        $OnChangeFunc .= '}';

                        $JSFunctions[] = $OnChangeFunc;
                    }
                }
            }
        }

        if (is_array($ConstructFieldArray))
        {
            foreach ($ConstructFieldArray as $Field => $Section)
            {
                $FieldArrays .= 'FieldSection[\''.$Field.'\'] = \''.$Section.'\';';
            }

            $JSFunctions[] = $FieldArrays;
        }
    }

    function setupFieldSectionReference()
    {
        global $FieldSectionReference, $MoveFieldsToSections;

        foreach ($this->FormArray as $Section => $details)
        {
            if (is_array($details['Rows']))
            {
                foreach ($details['Rows'] as $FieldRow)
                {
                    //Exception for rep_approved because it has a "false" condition when printed, but may
                    //still have form actions linked.
                    //TODO: can we just remove the "condition" check here altogether? Is it needed?
                    if (!is_array($FieldRow) || $FieldRow['Condition']!==false || $FieldRow['Name'] == 'rep_approved')
                    {
                        if (is_array($FieldRow))
                        {
                            $Field = $FieldRow['Name'];
                        }
                        else
                        {
                            $Field = $FieldRow;
                        }

                        if($details['ContactSuffix'])
                        {
                            $Field .= '_'.$details['ContactSuffix'];
                        }

                        if ($this->FormDesign->MoveFieldsToSections[$Field])
                        {
                            $FieldSectionReference[$Field] = $this->FormDesign->MoveFieldsToSections[$Field]['New'];
                        }
                        else
                        {
                            $FieldSectionReference[$Field] = $Section;
                        }
                    }
                }
            }
        }
    }

    function setupRecursionLimitation()
    {
        global $ExpandSections, $RecursionLimitations, $FieldSectionReference, $ExpandFields;

        if (is_array($this->FormDesign->ExpandSections))
        {
            foreach ($this->FormDesign->ExpandSections as $ExpandField => $Expandables)
            {
                foreach ($Expandables as $ExpandDetails)
                {
                    $RecursionLimitations['movement'][$ExpandField][] = $ExpandDetails['section'];
                }
            }
        }

        foreach ($FieldSectionReference as $Field => $Section)
        {
            $RecursionLimitations['actions'][$Field][] = $Section;
        }

        if (is_array($this->FormDesign->ExpandFields))
        {
            foreach ($this->FormDesign->ExpandFields as $ExpandField => $Expandables)
            {
                foreach ($Expandables as $ExpandDetails)
                {
                    $RecursionLimitations['field_actions'][$ExpandDetails['field']][] = $ExpandField;
                }
            }
        }
    }

    /**
    * @desc Loops through all sections and fields, running isSectionShown and isFieldShown to populate
    * $ShowHideSections and $ShowHideFields globals, which are then used when displaying the form.
    *
    * @param array $aParams Array of parameters
    * @param array $aParams['data'] Array of record data
    * @param string $aParams['module'] The current module
    *
    */
    function setupInitialFormVisibility($aParams)
    {
        global $ShowHideSections, $ShowHideFields;

        if ($this->FormArray['Parameters']['Suffix'])
        {
            $SuffixString = '_'.$this->FormArray['Parameters']['Suffix'];
        }

        foreach ($this->FormArray as $Section => $details)
        {
            $ShowHideSections[$Section] = $this->isSectionShown(array(
                'section' => $Section,
                'module' => $aParams['module'],
                'data' => $aParams['data']
            ));

            if (is_array($details['Rows']))
            {
                foreach ($details['Rows'] as $FieldRow)
                {
                    if (is_array($FieldRow))
                    {
                        $Field = $FieldRow['Name'];
                    }
                    else
                    {
                        $Field = $FieldRow;
                    }

                    $ShowHideFields[$Field] = $this->isFieldShown(array(
                        'field' => $Field.$SuffixString,
                        'data' => $aParams['data'],
                        'module' => $aParams['module']
                    ));
                }
            }
        }
    }

    function isSectionShown($aParams)
    {
        global $ShowHideSections, $ShowHideFields, $FieldSectionReference, $RecursiveFormDesignCheck, $ExpandSections,
               $DefaultValues, $HideFields, $ModuleDefs;

        // Used to check if the injury section is being generated for print so that it doesn't use the cached value for
        // each contact
        $notPrintInjurySection = ($this->FormMode != 'Print' || ($this->FormMode == 'Print' && $aParams['section'] != 'injury' && $aParams['module'] == 'CON'));

        if (!$aParams['section'])
        {
            return false;
        }
        elseif (preg_match('/multi_medication_record_/iu', $aParams['section'], $result))
        {
            return true;
        }
        elseif (isset($ShowHideSections[$aParams['section']]) && $notPrintInjurySection)
        {
            return $ShowHideSections[$aParams['section']];
        }
        elseif ($this->FormDesign->HideFields[$aParams['section']])
        {
            return false;
        }

        $RecursiveFormDesignCheck[] = $aParams['section'];

        $Section = $aParams['section'];
        $SectionArray = $aParams['sectionarray'];

        // Ensure default values are taken into account when deciding whether to show a section or not
        $aParams['data'] = SafelyMergeArrays(array($this->FormDesign->DefaultValues, $aParams['data']));

        // Need to find out if the display of this section is linked to
        // a custom dropdown field.
        if ($this->FormDesign->ExpandSections)
        {
            $ExpandChecks = array();

            foreach ($this->FormDesign->ExpandSections as $ExpandField => $ExpandValue)
            {
                foreach ($ExpandValue as $ExpandSection)
                {
                    if ($ExpandSection["section"] == $Section)
                    {
                        //set variables tracking whether there is a custom action on this section
                        $ExpandLogic['custom']['action'] = true;

                        // Otherwise we don't even need to check these cases
                        if (!$this->FormDesign->HideFields[$FieldSectionReference[$ExpandField]])
                        {
                            $FieldShown = $this->isFieldShown(array(
                                'field' => $ExpandField,
                                'data' => $aParams['data']
                            ));

                            // Action triggering its own section - just hide the section, since otherwise we get
                            // into a loop.
                            if (in_array($FieldSectionReference[$ExpandField], $RecursiveFormDesignCheck))
                            {
                                $ShowHideSections[$Section] = false;
                                $SectionShown = false;
                                $RecursiveFormDesign[$Section] = $ExpandField;
                            }
                            else
                            {
                                $SectionShown = $this->isSectionShown(array(
                                    'section' => $FieldSectionReference[$ExpandField],
                                    'data' => $aParams['data']
                                ));
                            }

                            //if the trigger field is hidden, the check auto-fails
                            if (($FieldShown || $this->FormDesign->DefaultValues[$ExpandField]) && $SectionShown)
                            {
                                $ExpandChecks[] = array(
                                    'field' => $ExpandField,
                                    'values' => $ExpandSection['values'],
                                    'condition' => $ExpandSection['alertcondition']);

                                if (\UnicodeString::substr($ExpandField,0,3) == "UDF" &&
                                    !$aParams['data'][$ExpandField])
                                {
                                    // This is a udf field, and so not contained in $Data
                                    // So we need to get hold of the value to test against.
                                    AddUDFValueToDataArray(array(
                                        'fieldname' => $ExpandField,
                                        'formobj' => $this,
                                        'module' => $aParams['module']
                                    ), $aParams['data']);
                                }

                                if ($this->FormDesign->DefaultValues[$ExpandField] &&
                                    !$aParams['data'][$ExpandField] && $this->FormMode == 'New')
                                {
                                    $aParams['data'][$ExpandField] = $this->FormDesign->DefaultValues[$ExpandField];
                                }
                            }
                        }
                    }
                }
            }

            foreach ($ExpandChecks as $ExpandCheck)
            {
                if (!empty($aParams['data'][$ExpandCheck['field']]))
                {
                    if ($aParams['data'][$ExpandCheck['field']] === true)
                    {
                        $aParams['data'][$ExpandCheck['field']] = 'Y';
                    }

                    if (is_string($aParams['data'][$ExpandCheck['field']]))
                    {
                        $MultiCodeParts = explode(" ", $aParams['data'][$ExpandCheck['field']]);
                    }
                    elseif (is_array($aParams['data'][$ExpandCheck['field']]))
                    {
                        $MultiCodeParts = $aParams['data'][$ExpandCheck['field']];
                    }

                    foreach ($MultiCodeParts as $index => $SingleCode)
                    {
                        if (in_array($SingleCode, $ExpandCheck['values']))
                        {
                            //the condition for this field is satisfied
                            $ExpandLogic['custom']['value'] = true;
                            break;
                        }
                    }
                }
            }
        }

        //set variables tracking whether there is a hard-coded action on this section
        if ($SectionArray["Show"])
        {
            if (!$this->FormDesign->HideFields[$SectionArray["Show"]])
            {
                $ExpandLogic['hardcoded']['action'] = true;

                if ($aParams['data'][$SectionArray["Show"]] == 'on' ||
                    $aParams['data'][$SectionArray["Show"]] == 'Y' || (!$aParams['data'][$SectionArray["Show"]] &&
                    $this->FormDesign->DefaultValues[$SectionArray["Show"]] == 'Y'))
                {
                    $ExpandLogic['hardcoded']['value'] = true;
                }
            }
        }

        //run logic to work out whether section should be displayed or not
        if (($ExpandLogic['hardcoded']['action'] && !$ExpandLogic['hardcoded']['value'] &&
            !$ExpandLogic['custom']['value']) || (!$ExpandLogic['hardcoded']['action'] &&
            $ExpandLogic['custom']['action'] && !$ExpandLogic['custom']['value']))
        {
            $DoExpand = false;
        }
        else
        {
            $DoExpand = true;
        }

        $RecursiveFormDesignCheck = array();
        $ShowHideSections[$Section] = $DoExpand;

        return $DoExpand;
    }

    function isFieldShown($aParams)
    {
        global $ShowHideFields, $RecursiveFormDesignCheck;

        if (!$aParams['field'])
        {
            return false;
        }
        elseif (isset($ShowHideFields[$aParams['field']]))
        {
            return $ShowHideFields[$aParams['field']];
        }
        elseif ($this->FormDesign->HideFields[$aParams['field']])
        {
            return false;
        }
        elseif (isset($this->FieldDefs[RemoveSuffix($aParams['field'])]['Show']))
        {
            return $this->FieldDefs[RemoveSuffix($aParams['field'])]['Show'];
        }

        $RecursiveFormDesignCheck[] = $aParams['field'];

        if ($this->FormDesign->ExpandFields)
        {
            foreach ($this->FormDesign->ExpandFields as $ExpandKey => $ExpandValue)
            {
                foreach ($ExpandValue as $ParentFieldDetails)
                {
                    if ($ParentFieldDetails["field"] == $aParams['field'])
                    {
                        //make sure we're passing all possible information in the new isFieldShown check
                        $NewParams = $aParams;
                        $NewParams['field'] = $ExpandKey;

                        //if the trigger field is hidden, the check auto-fails
                        if ($this->isFieldShown($NewParams) || $this->FormDesign->DefaultValues[$ExpandKey])
                        {
                            $ExpandChecks[] = array(
                                'field' => $ExpandKey,
                                'values' => $ParentFieldDetails['values']
                                );

                            if (\UnicodeString::substr($ExpandKey,0,3) == "UDF" && !$aParams['data'][$ExpandKey])
                            {
                                // This is a udf field, and so not contained in $Data
                                // So we need to get hold of the value to test against.
                                AddUDFValueToDataArray(array(
                                    'fieldname' => $ExpandKey,
                                    'formobj' => $this,
                                    'module' => $aParams['module']
                                ), $aParams['data']);
                            }

                            if ($this->FormDesign->DefaultValues[$ExpandKey] && !$aParams['data'][$ExpandKey])
                            {
                                $aParams['data'][$ExpandKey] = $this->FormDesign->DefaultValues[$ExpandKey];
                            }
                        }
                        else
                        {
                            $ExpandChecks[] = array(
                                'field' => $ExpandKey,
                                'fail' => true
                                );
                        }
                    }
                }
            }
        }

        $DoExpand = true;

        if (is_array($ExpandChecks))
        {
            $DoExpand = false;

            foreach ($ExpandChecks as $ExpandCheck)
            {
                if (!$ExpandCheck['fail'])
                {
                    if (!empty($aParams['data'][$ExpandCheck['field']]))
                    {
                        $MultiCodeParts = explode(" ", $aParams['data'][$ExpandCheck['field']]);

                        foreach ($MultiCodeParts as $index => $SingleCode)
                        {
                            if (in_array($SingleCode, $ExpandCheck['values']))
                            {
                                //the condition for this field is satisfied
                                $DoExpand = true;
                                break 2;
                            }
                        }
                    }
                }
            }
        }

        $ShowHideFields[$aParams['field']] = $DoExpand;

        return $DoExpand;
    }

    function MakeForm($InputFormArray, $Data, $Module, $ExtraParameters="")
    {
        global $OrderFields, $FormTitle, $DIF1UDFGroups, $OrderSections,
            $FormTitleDescr, $ExpandSections, $ExpandFields, $Show_all_section,
            $ModuleDefs, $Holding, $MoveFieldsToSections, $JSFunctions,
            $ShowHideSections, $ShowHideFields, $FieldSectionReference, $RecursionLimitations;

        if ($Data instanceof src\framework\model\Entity)
        {
            $Data = $Data->getVars();
        }
        elseif (!empty($Data) && !is_array($Data))
        {
            throw new \InvalidArgumentException('Form data must be provided as an Entity object or an array.');
        }
        
        $formlevel = ($_GET["formlevel"] ? $_GET["formlevel"] : 1);

        $DisabledHTML = ($this->FormDesign->ReadOnly ? "disabled='disabled'" : "");

        // Set the instance variable $FormArray.
        $this->FormArray = $InputFormArray;

        if ($this->FormArray['Parameters']['Suffix'])
        {
            $SuffixString = '_'.$this->FormArray['Parameters']['Suffix'];
        }

        //add user defined sections to the form array
        if (is_array($this->FormDesign->ExtraSections))
        {
            foreach ($this->FormDesign->ExtraSections as $SectionID => $SectionName)
            {
                $this->FormArray['section'.$SectionID.$SuffixString] = array(
                    'Title' => 'User Defined Section '.$SectionID,
                    'ID' => $SectionID,
                    'ExtraSection' => true,
                    'ContactSuffix' => $this->FormArray['Parameters']['Suffix']
                );
            }
        }

        //add user defined fields to the form array
        $this->addUDFFields($Data);

        if ($ExtraParameters['NoLinkFields'])
        {
            foreach ($this->FormArray as $SectionName => $SectionDetails)
            {
                if ($SectionDetails['LinkFields'])
                {
                    unset($this->FormArray[$SectionName]);
                }
            }
        }

        $this->CheckFormArrayKeys();

        if ($OrderFields)
        {
            $this->FormDesign->OrderSections = $OrderFields;
        }

        // If we are in Design mode, display a row asking the user for the title of the form.
        // Also make up the code for opening an expanding section setup window
        if ($this->FormMode == "Design")
        {
            $this->Contents .= '<li> <!-- List of Form Design options --><ol>';

            if ($ModuleDefs[$Module]["FORMS"][$formlevel]['CODE'] != "")
            {
                $sLevel2GlobalName = $ModuleDefs[$Module]["FORMS"][$formlevel]['CODE'] . "_DEFAULT";

                $Label = 'Use this design as the default form?';

                $FieldHTML = '<select name="use_design" id="use_design" ' . ($this->FormDesign->ReadOnly ? "disabled='disabled'" : "") . '>
                            <option value="Y"' . (GetParm($sLevel2GlobalName, '0', true) == $_GET["form_id"] ? " selected='selected'" : "") . '>Yes</option>
                            <option value="N"' . (GetParm($sLevel2GlobalName, '0', true) != $_GET["form_id"] ? " selected='selected'" : "") . '>No</option>
                        </select>';

                $this->Contents .= GetDivFieldHTML($Label, $FieldHTML);
            }

            // Display a text box for the title of the form.
            $Label = _tk('title_of_form');
            $IsFieldLocked = $this->FormDesign->IsFieldLocked('form', 'form_title', 'FIELD');
            $FieldHTML = '<textarea ' . ($this->isTablet ? ' autocapitalize="sentences"' : '') . 'cols="60" rows="2" name="form_title" id="form_title" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . '>' . $this->FormDesign->FormTitle . '</textarea>';
            $FieldHTML .= makeLockField('form_title', 'FIELD', 'form', $this->FormDesign->LockFieldAtribs);

            if ($IsFieldLocked)
            {
                $FieldHTML.= '<input type="hidden" name="form_title" id="form_title" value="' . $this->FormDesign->FormTitle . '">';
            }

            $this->Contents .= GetDivFieldHTML($Label, $FieldHTML);

            // Display a text box for the extra title text
            $Label = _tk('introductory_text');

            $IsFieldLocked = $this->FormDesign->IsFieldLocked('form', 'form_title_descr', 'FIELD');
            $FieldHTML = '<textarea ' . ($this->isTablet ? ' autocapitalize="sentences"' : '') . 'cols="60" rows="4" name="form_title_descr" id="form_title_descr" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . '>' . $this->FormDesign->FormTitleDescr . '</textarea>';
            $FieldHTML .= makeLockField('form_title_descr', 'FIELD', 'form', $this->FormDesign->LockFieldAtribs);

            if ($IsFieldLocked)
            {
                $FieldHTML.= '<input type="hidden" name="form_title_descr" id="form_title_descr" value="' . $this->FormDesign->FormTitleDescr . '">';
            }

            $this->Contents .= GetDivFieldHTML($Label, $FieldHTML);

            // If the form allows panels (i.e.
            // $FormArray['Parameters']['Panels'] is set), display a yes/no
            // dropdown for the "All tabs on one page" option.
            if ($this->FormArray['Parameters']['Panels'])
            {
                // Display a text box for the extra title text
                $Label = _tk('show_all_on_one_page');

                $FieldHTML = '<select name="show_all_section" ' . ($this->FormDesign->ReadOnly ? 'disabled="disabled"' : '') . '>
                        <option value="Y"' . ($this->FormDesign->Show_all_section == "Y" ? ' selected="selected"' : "") . '>' . _tk('yes') . '</option>
                        <option value="N"' . (($this->FormDesign->Show_all_section == "N"
                        || $this->FormDesign->Show_all_section == '') ? ' selected="selected"' : '') . '>' . _tk('no') . '</option>
                        </select>';

                $this->Contents .= GetDivFieldHTML($Label, $FieldHTML);
            }

            $this->Contents .= '</ol></li>';
        }

        // Add the "extra contacts" count field required from the Parameters
        // section.
          if ($FormParms = $this->FormArray["Parameters"])
        {
            if ($FormParms["ExtraContacts"])
            {
                $this->Contents .= '<input type="hidden" name="increp_contacts" value="' . $FormParms["ExtraContacts"] . '" />';
            }
        }

        // If $FormArray['Parameters']['Panels'] is set, stick it on the end
        // of $ExtraParameters so it can be passed down to MakeDivSection().
        // This is so we can work out whether or not to display the "New panel?"
        // checkbox in a section header in Design mode.
        if ($FormParms['Panels'])
        {
            $ExtraParameters['Panels'] = true;
        }

        // Add any UDF groups that the user has added to the
        // design into the $FormArray.
        if ($this->FormDesign->DIF1UDFGroups)
        {
            if ($FormParms['Suffix'])
            {
                $SuffixString = '_'.$FormParms['Suffix'];
            }

            $RecordID = $this->FormArray["udf".$SuffixString]["RecordID"];
            // If the recordid is not set in the form definitions, get it
            // from the data
            if (empty($RecordID))
            {
                $RecordID = Sanitize::SanitizeInt($Data["recordid"]);
            }

            if ($FormParms['Suffix'])
            {
                if (empty($RecordID))
                {
                    $RecordID = Sanitize::SanitizeInt($Data["recordid" . $SuffixString]);
                }
            }

            // Make up an array of UDF Groups.
            foreach ($this->FormDesign->DIF1UDFGroups as $Index => $GroupID)
            {
            	$GroupID = Sanitize::SanitizeInt($GroupID);

                if ($this->FormDesign->NewPanels["UDFGROUP$GroupID"])
                {
                    $this->FormArray["UDFGROUP$GroupID".$SuffixString] = $this->MakeUDFGroupArray($GroupID, $RecordID);
                }
                else
                {
                    array_insert_datix($this->FormArray, 'udf'.$SuffixString, array("UDFGROUP$GroupID".$SuffixString => $this->MakeUDFGroupArray($GroupID, $RecordID, $FormParms['Suffix'])));
                }
            }
        }

        if (!in_array($this->FormMode, ['Search', 'New', 'Design']))
        {
            $this->extraFieldValues = $this->getExtraFieldValues($Data['recordid'], $Module);
        }

        $this->setupFieldSectionReference();
        $this->setupInitialFormVisibility(array('data' => $Data, 'module' => $Module));

        if ($this->FormMode == 'Design')  // check for recursion
        {
             $this->setupRecursionLimitation();

             unset($ShowHideSections);
             unset($ShowHideFields);
             unset($FieldSectionReference);
        }

        if ($this->FormMode == "Design")
        {
            $_SESSION["SectionsSetup"] = array();
            $_SESSION["FieldAdditionsSectionsSetup"] = array();
            $_SESSION["RecursionLimitations"] = $RecursionLimitations;
            $_SESSION['LockedFieldsHIDE'] = array();
            
            
            // If a filed has locked HIDE attribute, it cannot be the target of a field action
            $LockedFieldsHIDE = array();
            
            if (is_array($this->FormDesign->LockFieldAtribs))
            {
                foreach ($this->FormDesign->LockFieldAtribs as $LockedFieldName => $Fields)
                {
                    if (is_array($Fields))
                    {
                        if ($Fields['ALLFIELD'] || $Fields['HIDE'])
                        {
                        	$LockedFieldsHIDE[] = $LockedFieldName;
                        }
                    }
                }
            }
            
			if ($LockedFieldsHIDE)
			{
				$_SESSION['LockedFieldsHIDE'] = $LockedFieldsHIDE;
			}
			
			if (IsCentrallyAdminSys() && is_array($this->FormDesign->ExpandSections))
			{
				$_SESSION['ExpandSections'] = $this->FormDesign->ExpandSections;
			}
			 
			
            
			// if a section has been locked, it cannont be the target of actions
			$LockedSections = array();

			if (IsCentrallyAdminSys() && !IsCentralAdmin())
			{
                foreach ($this->FormArray as $SectionName => $SectionAtribs)
                {
	            	if (isset($this->FormDesign->LockFieldAtribs[$SectionName]['SECTION']))
	            	{
	            		$LockedSections[] = $SectionName;
                    }
                }
			}

			
            //If a field has been locked, the section that field is in cannot be targeted by actions
            if (IsCentrallyAdminSys() && !IsCentralAdmin())
            {
                if (is_array($this->FormDesign->LockFieldAtribs))
                {
                    foreach ($this->FormDesign->LockFieldAtribs as $LockedFieldName => $Fields)
                    {
                        if (is_array($Fields))
                        {
                            if ($Fields['ALLFIELD'] || $Fields['HIDE'])
                            {
                                foreach ($this->FormArray as $SectionName => $SectionAtribs)
                                {
                                    if (isset($SectionAtribs['Rows']))
                                    {
                                        foreach ($SectionAtribs['Rows'] as $Field)
                                        {
                                            if (is_array($Field))
                                            {
                                                $FieldName = $Field['Name'];
                                            }
                                            else
                                            {
                                                $FieldName = $Field;
                                            }

                                            if ($FieldName == $LockedFieldName &&
                                                !isset($this->FormDesign->MoveFieldsToSections[$LockedFieldName]))
                                            {
                                                if (!in_array($SectionName, $LockedSections))
                                                {
                                                    $LockedSections[] = $SectionName;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Create an array of ($Section => $SectionTitle).  This is for the
            // setup of expanding sections.
            foreach ($this->FormArray as $Section => $SectionArray)
            {
                if ((!array_key_exists("Condition", $SectionArray) || $SectionArray["Condition"]) &&
                    $SectionArray["Title"] && !in_array($Section, $LockedSections))
                {
                    $_SESSION["SectionsSetup"][$Section] = $SectionArray["Title"];

                    if (!$SectionArray['NoFieldAdditions'])
                    {
                        $_SESSION["FieldAdditionsSectionsSetup"][$Section] = $SectionArray["Title"];
                    }
                }
            }
        }

        // Reformat $ExpandSections
        if ($this->FormDesign->ExpandSections)
        {
            foreach ($this->FormDesign->ExpandSections as $ExpandKey => $ExpandValue)
            {
                foreach ($ExpandValue as $ExpandSectionKey => $ExpandSection)
                {
                    if (\UnicodeString::substr($ExpandKey,0,3) == "UDF" &&
                        !$Data[$ExpandKey])
                    {
                        AddUDFValueToDataArray(array(
                            'fieldname' => $ExpandKey,
                            'formobj' => $this,
                            'module' => $Module
                        ), $Data);
                    }

                    // Make up the condition for displaying the section
                    foreach ($ExpandSection["values"] as $ExpandSectionValues)
                    {
                        $ExpandStrings[] = 'parentValue == \'' . $ExpandSectionValues . '\'';
                    }

                    $ExpandString = implode('||', $ExpandStrings);
                    $this->FormDesign->ExpandSections[$ExpandKey][$ExpandSectionKey]["alertcondition"] = $ExpandString;
                    $ExpandStrings = "";
                }
            }
        }

        // Reformat $ExpandFields
        if ($this->FormDesign->ExpandFields)
        {
            foreach ($this->FormDesign->ExpandFields as $ExpandKey => $ExpandValue)
            {
                foreach ($ExpandValue as $ExpandFieldKey => $ExpandField)
                {
                    if (\UnicodeString::substr($ExpandKey,0,3) == "UDF" &&
                        !$Data[$ExpandKey])
                    {
                        AddUDFValueToDataArray(array(
                            'fieldname' => $ExpandKey,
                            'formobj' => $this,
                            'module' => $Module
                        ), $Data);
                    }

                    // Make up the condition for displaying the section
                    foreach ($ExpandField["values"] as $ExpandFieldValues)
                    {
                        $ExpandStrings[] = 'parentValue == \'' . $ExpandFieldValues . '\'';
                    }

                    $ExpandString = implode('||', $ExpandStrings);
                    $this->FormDesign->ExpandFields[$ExpandKey][$ExpandFieldKey]["alertcondition"] = $ExpandString;
                    $ExpandStrings = "";
                }
            }
        }

        // Move fields between sections if the array which contains the
        // re-assignments is set.
        // This array is in the form of:
        // array('<FieldName>' => array ('Original' => '<original section that
        // the field was in>', 'New' => '<New section that the field will
        // appear in>'), ...)
        $this->MoveFieldsToSections();

        // We may need to put this condition back - I'm not entirely sure why I put it here in the first place.
        if ($this->FormMode != 'Design' /*&& !$ExtraParameters['dynamic_section']*/)
        {
            $JSFunctions[] = $this->SetupShowConditionFunctions(array('module' => $Module, 'recordid' => $Data['recordid']));
        }

        // Create the panels
        $this->GetCurrentRecordPanels($Data);

        $this->MakeCurrentRecordPanels($Data, $Module, $ExtraParameters);
    }

    // Outputs panels and the sections they contain, using $PanelArray
    // to determine these.
    private function MakeCurrentRecordPanels($Data, $Module, $ExtraParameters)
    {
        global $Show_all_section;

        // The panel class depends on whether $Show_all_section is set in the
        // form design.
        if (bYN($this->FormDesign->Show_all_section))
        {
            $PanelClass = "panelall";
        }
        else
        {
            $PanelClass = "panel";
        }

        if ($this->FormMode == 'Design')
        {
            // Build Expand all/Collapse all section
            $this->Contents .= '
                <li id="expand_collapse" class="section_title_row">
                    <div style="float: right;">
                        <a onclick="if(CheckChange()){ExpandAllSections();} return false;" href="#" class="expand_collapse"><b>'._tk('expand_all').'</b></a>
                        &nbsp;/&nbsp;
                        <a onclick="if(CheckChange()){CollapseAllSections();} return false;" href="#" class="expand_collapse"><b>'._tk('collapse_all').'</b></a>
                    </div>
                </li>
            ';
        }

        if (is_array($this->PanelArray))
        {
            foreach ($this->PanelArray as $PanelName => $PanelValues)
            {
                $currentPanel = $_GET['panel'];

                // Used to check if the panel being rendered should be shown as the current panel or if they should all
                // be shown
                $showPanel = (bYN($this->FormDesign->Show_all_section) || $currentPanel == $PanelName || (! $currentPanel && $this->GetFirstPanel() == $PanelName));

               // Only wrap in panels if this form can have panels,
               // indicated by the "Panels" parameter.
                if ($this->FormArray["Parameters"]["Panels"] && $this->FormMode != "Print" &&
                    $this->FormMode != "Design")
                {
                    $this->Contents .= '
                     <li id="panel-' . $PanelName . '" name="panel-'
                        . $PanelName . '" class="'
                        . $PanelClass . '"'
                        . ' style="display: ' . ($showPanel ? 'list-item' : 'none') . ';"'
                        . '>';

                    $this->Contents .= '

                    <!-- START OF PANEL "'.$PanelName.'" -->
                    <!-- LIST OF SECTIONS FOR PANEL "'.$PanelName.'" -->
                    <ol>
                        ';
                }

                // Output the contents of each section in the panel
                foreach ($PanelValues as $PanelNo => $SectionName)
                {
                    $this->MakeDivSection($SectionName, $this->FormArray[$SectionName], $Data, $Module, $ExtraParameters);
                }

                if ($this->FormArray["Parameters"]["Panels"] && $this->FormMode != "Print" &&
                    $this->FormMode != "Design")
                {
                    // Only wrap in panels if the form can have panels

                    $this->Contents .= '
                        </ol>
                        </li>
                        <!-- END OF PANEL "'.$PanelName.'" -->
                    ';
                }
            }
        }
    }

    function CheckSectionExpanded($aParams)
    {
        global $ExpandSections, $DefaultValues;

        $Section = $aParams['section'];
        $SectionArray = $aParams['sectionarray'];

        // Ensure defaultvalues are taken into account when deciding whether to show a section or not
        $aParams['data'] = SafelyMergeArrays(array($aParams['data'], $this->FormDesign->DefaultValues));

        // Need to find out if the display of this section is linked to
        // a custom dropdown field.
        if ($this->FormDesign->ExpandSections)
        {
            $ExpandChecks = array();

            foreach ($this->FormDesign->ExpandSections as $ExpandField => $ExpandValue)
            {
                foreach ($ExpandValue as $ExpandSection)
                {
                    if ($ExpandSection["section"] == $Section)
                    {
                        //set variables tracking whether there is a custom action on this section
                        $ExpandLogic['custom']['action'] = true;

                        $ExpandChecks[] = array(
                            'field' => $ExpandField,
                            'values' => $ExpandSection['values'],
                            'condition' => $ExpandSection['alertcondition']
                        );


                        if (\UnicodeString::substr($ExpandField,0,3) == "UDF")
                        {
                            // This is a udf field, and so not contained in $Data
                            // So we need to get hold of the value to test against.
                            $Parts = explode("_", $ExpandField);

                            $UDFArray = $this->GetUDFValue($ModuleDefs[$Module]['MOD_ID'], $Data["recordid"], $Parts[2], $Parts[3]);

                            $aParams['data'][$ExpandField] = $UDFArray["udv_string"];
                        }

                        if ($this->FormDesign->DefaultValues[$ExpandField] && !$Data[$ExpandField] &&
                            $this->FormMode == 'New')
                        {
                            $aParams['data'][$ExpandField] = $this->FormDesign->DefaultValues[$ExpandField];
                        }
                    }
                }
            }

            foreach ($ExpandChecks as $ExpandCheck)
            {
                if (!empty($aParams['data'][$ExpandCheck['field']]))
                {
                    if ($aParams['data'][$ExpandCheck['field']] === true)
                    {
                        $aParams['data'][$ExpandCheck['field']] = 'Y';
                    }

                    $MultiCodeParts = explode(" ", $aParams['data'][$ExpandCheck['field']]);

                    foreach ($MultiCodeParts as $index => $SingleCode)
                    {
                        if (in_array($SingleCode, $ExpandCheck['values']))
                        {
                            //the condition for this field is satisfied
                            $ExpandLogic['custom']['value'] = true;
                            break;
                        }
                    }
                }
            }
        }

        //set variables tracking whether there is a hard-coded action on this section
        if ($SectionArray["Show"])
        {
            if (!$this->FormDesign->HideFields[$SectionArray["Show"]])
            {
                $ExpandLogic['hardcoded']['action'] = true;

                if ($aParams['data'][$SectionArray["Show"]] == 'on' ||
                    $aParams['data'][$SectionArray["Show"]] == 'Y' || (!$aParams['data'][$SectionArray["Show"]] &&
                    $this->FormDesign->DefaultValues[$SectionArray["Show"]] == 'Y'))
                {
                    $ExpandLogic['hardcoded']['value'] = true;
                }
            }
        }

        //run logic to work out whether section should be displayed or not
        if (($ExpandLogic['hardcoded']['action'] && !$ExpandLogic['hardcoded']['value'] &&
            !$ExpandLogic['custom']['value']) || (!$ExpandLogic['hardcoded']['action'] &&
            $ExpandLogic['custom']['action'] && !$ExpandLogic['custom']['value']))
        {
            $DoExpand = false;
        }
        else
        {
            $DoExpand = true;
        }

        return $DoExpand;
    }

    // $Section is the name of the section, to be used as a DIV tag.
    // $SectionArray is the list of parameters for the section - see
    // Technical Knowledge for a full explanation.
    function MakeDivSection($Section, $SectionArray, $Data, $Module, $ExtraParameters="")
    {
        global $HideFields, $ReadOnlyFields, $DefaultValues, $OrderSections, $OrderFields, $DIF1UDFGroups,
            $UserLabels, $UserExtraText, $FieldOrders, $ExpandSections, $ExpandFields, $NewPanels, $FormDesigns,
               $EquipmentForms, $ListingDesigns, $ModuleDefs, $MandatoryFields, $DisplayAsCheckboxes;

        $DisabledHTML = ($this->FormDesign->ReadOnly ? "disabled='disabled'" : "");
        $Disabled = $this->FormDesign->ReadOnly;
        $IsSectionLocked = $this->FormDesign->IsFieldLocked($Section, $Section, 'SECTION');

        $UseLists = (isset($ExtraParameters['UseLists']) ? $ExtraParameters['UseLists'] : true);

        $this->CurrentSection = $Section;
        $this->CurrentSectionEmpty = true;

        // Need to find out if the display of this section is linked to
        // a dropdown field - whether hardcoded or custom.
        $DoExpand = $this->isSectionShown(array('section' => $Section, 'data' => $Data));

        if ($OrderFields)
        {
            $this->FormDesign->OrderSections = $OrderFields;
        }

        // "Condition" key determines whether the row is displayed at all.
        // Used for having global parameters control the display of sections.
        if (is_array($SectionArray) && array_key_exists("Condition", $SectionArray) && !$SectionArray["Condition"])
        {
            return;
        }

        // Global $HideFields contains user-definable hiding settings.
        if ($this->FormMode != "Design" && $this->FormDesign->HideFields[$Section])
        {
            return;
        }

        // In print mode, the form is static, so we can ignore currently hidden sections and fields.
        if ($this->FormMode == "Print" && isset($DoExpand) && $DoExpand === false)
        {
            return;
        }

        /**
         * This should get the module for the form so that it can be used to prefix the section id so that there aren't
         * any conflicts
         */
        $formModule = $this->FormDesign->Module ?: $Module;

        $section_id = ($this->FormMode == "Print" ? $formModule . '-' : '') . $Section;

        $this->Contents .= '<li id="' . $section_id . '" class="section_div" ';

        $this->printSections[] = $section_id;

        if (is_array($SectionArray['Rows']))
        {
            foreach ($SectionArray['Rows'] as $Row => $Details)
            {
                if (is_array($Details))
                {
                    if ($Details['AlwaysMandatory'] && !$this->FormDesign->HideFields[$Details['Name']])
                    {
                        $this->FormDesign->MandatoryFields[$Details['Name']] = $Section;
                        //temporary handler while we're moving accross to object based form designs:
                        $MandatoryFields[$Details['Name']] = $Section;
                    }
                }
            }
        }

        // Is this a show/hide section?  Always show if $FormMode is Design
        if ($this->FormMode != "Design" && isset($DoExpand) && $DoExpand === false)
        {
            $this->Contents .= ' style="display:none"';
        }

        $this->Contents .= '>
            <!-- START OF SECTION "'.$Section.'" -->

';
        $this->Contents .= GetSectionMessages($Section);

        $this->Contents .= '
        <!-- LIST OF ROWS FOR SECTION "'.$Section.'" -->
        <ol class="toggle-group">
';
        if ($this->FormMode == "Design" && $Section != "udf")
        {
            $DesignFields = $SectionArray["Title"];

            if ($DesignFields == "")
            {
                $DesignFields = _tk('section_name') . ' ' . $Section;
            }

            $DesignFields = '<div style="float:left;width:25%;padding:3px"><b>' . $DesignFields . '</b>.'.($SectionArray['FormDesignSubtitle'] ? ' ('.$SectionArray['FormDesignSubtitle'].')' : '');
            $DesignFields .= ' ' . makeLockField($this->CurrentSection, 'SECTION', $this->CurrentSection, $this->FormDesign->LockFieldAtribs);
            $DesignFields .= '</div>';

            // Hide section option
            $DesignFields .= '<div style="float:right;width:74%">';

            if (sizeof($this->FormArray[$this->CurrentSection]['Rows']))
            {
                $img = (isset($_SESSION['SECTIONEXPANDED'][$this->CurrentSection]) ? 'collapse.gif' : 'expand.gif');
                //TODO - removed renderFields() from onCLick and make jQuery event handler
                $DesignFields .= '
            <div class="title_rhs_container" style="padding: 5px;">
            <a class="toggle-trigger"' . ( ! isset($_SESSION['SECTIONEXPANDED'][$this->CurrentSection]) ? ' data-render="true"' : '') . ' onClick="renderFields(this, '
            ."{ 'module'        :'".$this->FormDesign->Module."',
                'form_level'    :'".$this->FormDesign->Level."',
                'section_name'  :'".$this->CurrentSection."',
                'form_id'       :'".$this->FormDesign->ID."',
                'readonly'      :'".$this->FormDesign->ReadOnly."',
                'form_type'     :'".$this->FormMode.'\'});">
            <img id=\'twisty_image_'.$this->CurrentSection.'\' src="Images/'.$img.'" alt="+" border="0"/>
            </a>
            </div>';
            
            }
            
            $DesignFields .= _tk('hide_section_q') . ' ';

            $DesignFields .= '<input type="checkbox" name="HIDE-' . $Section . '-' . $Section . '"';

            if ($this->FormDesign->HideFields[$Section])
            {
                $DesignFields .= ' checked="checked"';
            }
            
            $sectionAtrribsDisabled = ($IsSectionLocked || $this->FormDesign->HasSectionFullyLockedField($Section, $this->FormArray) || $this->FormDesign->HasSectionLockedHiddenFields($Section, $this->FormArray));
            
            if ($this->FormDesign->ReadOnly || $sectionAtrribsDisabled)
            {
                $DesignFields .= ' disabled="disabled"';
            }
            else
            {
                $DesignFields .= ' onchange="addInputHidden(\'HIDE-'.$Section.'-'.$Section.'\');jQuery(\'input[name=HIDE-'.$Section.'-'.$Section.']\').is(\':checked\') ? jQuery(\'input#HIDE-'.$Section.'-'.$Section.'\').val(\'on\') : jQuery(\'input#HIDE-'.$Section.'-'.$Section.'\').val(\'off\');"';
            }

            $DesignFields .= ' />&nbsp;&nbsp;';

            if ($sectionAtrribsDisabled && $this->FormDesign->HideFields[$Section])
            {
            	$DesignFields .= '<input type="hidden" name="HIDE-' . $Section . '-' . $Section . '" value="1"/>';
            }

            // Read-only option
            if (!$SectionArray['NoReadOnly'] && !$SectionArray['ReadOnly'])
            {
                $DesignFields .= _tk('read_only_section_q')
                    . ' ';
                $DesignFields .= '<input type="checkbox" name="READONLY-'
                    . $Section . '-' . $Section . '" onclick="UncheckMandatoryFields(this, \'' . $Section . '\')"';

                if ($this->FormDesign->ReadOnlyFields[$Section])
                {
                    $DesignFields .= ' checked="checked"';
                }

                if ($this->FormDesign->ReadOnly || $sectionAtrribsDisabled)
                {
                    $DesignFields .= ' disabled="disabled"';
                }
                else
                {
                    $DesignFields .= ' onchange="addInputHidden(\'READONLY-'.$Section.'-'.$Section.'\');jQuery(\'input[name=READONLY-'.$Section.'-'.$Section.']\').is(\':checked\') ? jQuery(\'input#READONLY-'.$Section.'-'.$Section.'\').val(\'on\') : jQuery(\'input#READONLY-'.$Section.'-'.$Section.'\').val(\'off\');"';
                }

                $DesignFields .= ' />&nbsp;&nbsp;';
                
                if ($sectionAtrribsDisabled && $this->FormDesign->ReadOnlyFields[$Section])
	            {
	            	$DesignFields .= '<input type="hidden" name="READONLY-' . $Section . '-' . $Section . '" value="1"/>';
	            }
            }

            // Set the Order field from the $OrderSections array
            // and the "New panel" field from the $NewPanels array
            $OrderObj = new FormField();

            if ($this->FormDesign->OrderSections)
            {
                $OrderNo = array_search($Section, $this->FormDesign->OrderSections);
            }

            $OrderObj->MakeNumberField("ORDER-$Section-$Section", $OrderNo, 5, 0, '', '', $Disabled);
            $DesignFields .= 'Order: ' . $OrderObj->GetField();
            $DesignFields .= "&nbsp;&nbsp;";

            // Only display the "New panel?" option if the form allows panels
            // in the first place.
            if ($ExtraParameters['Panels'] && !$SectionArray['NoNewPanel'])
            {
                $DesignFields .= _tk('new_panel_q') . ' ';
                $DesignFields .= '<input type="checkbox" ' . $DisabledHTML . ' name="NEWPANEL-'
                    . $Section . '-' . $Section . '"';
                // Check the NewPanel checkbox if 'NewPanel' is set in the
                // section array.  This is overridden if the NewPanels
                // form design global array is set.
				$NewPanelChecked = $this->FormDesign->NewPanels[$Section];
					
                if ($NewPanelChecked)
                {
                    $DesignFields .= ' checked="checked"';
                }

                $DesignFields .= ' onchange="addInputHidden(\'NEWPANEL-'.$Section.'-'.$Section.'\');jQuery(\'input[name=NEWPANEL-'.$Section.'-'.$Section.']\').is(\':checked\') ? jQuery(\'input#NEWPANEL-'.$Section.'-'.$Section.'\').val(\'on\') : jQuery(\'input#NEWPANEL-'.$Section.'-'.$Section.'\').val(\'off\');"';

                $DesignFields .= ' />&nbsp;&nbsp;';
            }

            $ContactTypeArray = getContactTypeArray($Module);

            $AdvancedOn = ($this->FormDesign->UserLabels[$Section] || $this->FormDesign->UserExtraText[$Section] ||
                        ($SectionArray['LinkedEquipment'] && $EquipmentForms)
                        );

            // Advanced option
            if (!$SectionArray["NoAdvanced"])
            {
                if (!$AdvancedOn)
                {
                   $DesignFields .= _tk('advanced_q') . ' <input type="checkbox" ' . $DisabledHTML . ' '
                                . ' onclick="expandIt(\'ADVDIV_'
                                . $Section . '\', this)" />';
                }
            }

            // Section Expanded variable
            $DesignFields .= '<input type="hidden" id="SECTIONEXPANDED-'.$Section.'-'.$Section.'" name="SECTIONEXPANDED-'.$Section.'-'.$Section.'" />';

            if ($SectionArray['ExtraSection'])
            {
                if (!empty($SectionArray['Rows']))
                {
                    $DesignFields .= '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsSectionLocked) ? 'disabled="disabled"' : '') . ' value="Delete Section" onClick="alert(\'You must remove all fields from this section before it can be deleted.\')">';
                }
                else
                {
                    $DesignFields .= '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsSectionLocked) ? 'disabled="disabled"' : '') . ' value="Delete Section" onClick="if(confirm(\'Are you sure you want to remove this section?\')){DeleteFormDesignSection('.$SectionArray['ID'].')}">';
                }
            }

            //advanced section contents:
            if (!$SectionArray["NoAdvanced"])
            {
                if (!$AdvancedOn)
                {
                    $DesignFields .= '<div id="ADVDIV_' . $Section
                                    . '" style="display:none">';
                }

                $DesignFields .= '<br />Section label: <input type="text" size="20" ' . (($this->FormDesign->ReadOnly || $IsSectionLocked) ? 'disabled="disabled"' : '') . ' name="'
                            . 'LABEL-' . $Section . '-' . $Section
                            . '" value="' . htmlspecialchars($this->FormDesign->UserLabels["$Section"])
                            . '" /><br /> ' . _tk('extra_text') . ' <br /><textarea ' . ($this->isTablet ? ' autocapitalize="sentences"' : '') . (($this->FormDesign->ReadOnly || $IsSectionLocked) ? 'disabled="disabled"' : '') . ' cols="60" rows="3" name="EXTRA-'
                            . $Section . '-' . $Section . '">'
                            . htmlspecialchars($this->FormDesign->UserExtraText["$Section"])
                            . '</textarea>';
                            
                if ($this->FormDesign->ReadOnly || $IsSectionLocked)
                {
                	$DesignFields .= '<input type="hidden" name="'
                            . 'LABEL-' . $Section . '-' . $Section
                            . '" value="' . htmlspecialchars($this->FormDesign->UserLabels["$Section"])
                            . '" /><input type="hidden" name="EXTRA-'
                            . $Section . '-' . $Section . '" value="' . htmlspecialchars($this->FormDesign->UserExtraText["$Section"])
                            . '">';
                            
                }

                if ($SectionArray['LinkedEquipment'])
                {
                    $DesignFields .= '<br /> ' . _tk('equip_form_select_text') . ' ' .
                        ArrayToSelect(array('id' => 'EQUIPFORM-'.$Section.'-'.$Section,
                            'options'=> GetListOfFormDesigns(array('level' => ($_REQUEST['formlevel'] ? $_REQUEST['formlevel'] : 1), 'module' => 'AST')),
                            'value' => $EquipmentForms));
                }

                if ($SectionArray['Listings'])
                {
                    foreach ($SectionArray['Listings'] as $Key => $ListingDetails)
                    {
                        $DesignFields .= '<br /> ' . ($ListingDetails['Label'] ? $ListingDetails['Label'] : _tk('listing_section_select_text')) . ' ' .
                            ArrayToSelect(array('id' => 'LISTINGDESIGN-'.$Section.'-'.$Key,
                                'options'=> GetListOfListingDesigns(array('module' => $ListingDetails['module'])),
                                'value' => $this->FormDesign->ListingDesigns[$Section][$Key]));
                    }
                }

                if ($SectionArray['ExtraSection'])
                {
                    $SectionNumber = str_replace('section', '', $Section);
                    $DesignFields .= '<input type="hidden" id="EXTRASECTIONS-'.$SectionNumber.'-'.$SectionNumber.'" name="EXTRASECTIONS-'.$SectionNumber.'-'.$SectionNumber.'" value="User Defined Section '.$SectionNumber.'">';
                }

                if ($SectionArray['LinkedForms'])
                {
                    foreach ($SectionArray['LinkedForms'] as $Type => $FormDetails)
                    {
                        //Check for form level override e.g. SAB1 and SAB2 both use level 2 contact forms.
                        $level = ($FormDetails['level'] ? $FormDetails['level'] : $_REQUEST['formlevel']);
                        $DesignFields .= '<br /> ' . ($FormDetails['Label'] ? $FormDetails['Label'] : _tk('form_section_select_text')) . ' ' .
                            ArrayToSelect(array(
                                'id'       => 'FORMDESIGN-'.$Section.'-'.$Type,
                                'options'  => GetListOfFormDesigns(array('level' => ($level ? $level : 1), 'module' => $FormDetails['module'])),
                                'value'    => $this->FormDesign->FormDesigns[$Section][$Type],
                                'disabled' => ($this->FormDesign->ReadOnly || $sectionAtrribsDisabled ? true : false)
                            ));
                    }
                }

                if (!$SectionArray['NoFieldAdditions'] && !$ModuleDefs[$Module]['NO_FIELD_ADDITIONS'] && !$this->FormDesign->ReadOnly)
                {
                    $DesignFields .= '<div>Add extra field to this section: <div class="clearfix"></div>';

                    $LocalUDFFields = array();

                    if (!empty($this->FormDesign->ExtraFields))
                    {
                        foreach ($this->FormDesign->ExtraFields as $UDFField => $FieldSection)
                        {
                            if ($Section == $FieldSection)
                            {
                                $LocalUDFFields[] = $UDFField;
                            }
                        }
                    }

                    $FieldSelector = Forms_SelectFieldFactory::createSelectField('EXTRAFIELDS-'.$Section.'-'.$Section, 'ADM', $LocalUDFFields, '', true);
                    
                    $FieldSelector->setCustomCodes($this->getExtrafieldList($Module));
                    $FieldSelector->setCustomIncludeListOption('jQuery("select[id^=EXTRAFIELDS]").each(function(){'.
                        'if (jQuery(this).find("option[value=\'"+code+"\']").length){result = false;return false;}'.
                    '});');
                    if (IsCentrallyAdminSys() && !IsCentralAdmin())
                    {
                    	if ($IsSectionLocked)
	                    {
	                    	$FieldSelector->lockedCodes = $this->getExtrafieldList($Module);
	                    }
                        else
                        {
	                    	$FieldSelector->setLockedCodes($this->FormDesign->LockFieldAtribs);
	                    }
                    }

                    $FieldSelector->setSuppressCodeDisplay();
                    $FieldSelector->setIgnoreMaxLength();

                    $DesignFields .= $FieldSelector->getField($IsSectionLocked);
                }

                if (!$AdvancedOn)
                {
                    $DesignFields .= '
                            </div>
                        </div>
                    ';
                }
            }
		}
        // Special case: if there is a "Table" element, just output this
        // as a table object
        if (is_array($SectionArray) && array_key_exists("Table", $SectionArray))
        {
            // If we are in design mode, don't output the table, just ask
            // the user if they want to hide it.
            if ($this->FormMode == "Design")
            {
                $this->Contents .= '<div class="form_table">';
                $this->MakeTitleRow($DesignFields);
                $this->Contents .= '</div>';
            }
            elseif ($SectionArray["Table"] != "")
            {
                $this->Contents .= $SectionArray["Table"]->GetFormTable();
            }
		}
        else
        {
            $this->Contents .= '
                <!-- TITLE ROW FOR SECTION "'.$Section.'" -->';

            // If this is not a Show/Hide section, we need to create a hidden
            // field called show_$Section to indicate that this section is
            // visible and needs to be included in the checking for
            // mandatory fields.  Don't need it in design mode, though.
            if ($this->FormMode != "Design")
            {
                //workaround for sections that are shown/hidden as groups e.g. subjects/issues.
                if ($SectionArray['ContainedIn'])
                {
                    $this->Contents .= '
                    <input type="hidden" name="'. $Section . '_contained_in" id="'. $Section . '_contained_in" value="' . $SectionArray['ContainedIn'] . '" />';
                }

                if ($DoExpand === false)
                {
                    $this->Contents .= '
                    <input type="hidden" name="show_section_'. $Section . '" id="show_section_'. $Section . '" value="0" />';
                }
                else
                {
                    $this->Contents .= '
                    <input type="hidden" name="show_section_'. $Section . '" id="show_section_'. $Section . '" value="1" />';
                }
            }

            if ($this->FormMode == "Design")
            {
                $this->MakeTitleRow($DesignFields, 'section_title_row', true, $Section);
            }
            elseif ($SectionArray['Special'] != 'UDFMainSection'
                && !$SectionArray['NoTitle']
                && $Title = ($this->FormDesign->UserLabels[$Section] ? $this->FormDesign->UserLabels[$Section]
                                : $SectionArray["Title"]))
            {
                $TitleRowHTML = GetTitleTable(array(
                    'section' => $Section,
                    'title' => $Title,
                    'subtitle' => $this->FormDesign->UserExtraText[$Section],
                    'twisty' => ($this->FormMode == 'Print'),
                    'sectionarray' => $SectionArray,
                    'module' => $Module,
                    'formtype' => $this->FormMode
                ));

                if($this->FormMode == "Print" && ! $SectionArray['ContainedIn'] && ( ! $ExtraParameters['parentForm'] || $Module == $ExtraParameters['parentForm']))
                {
                    $this->MakeTitleRow($TitleRowHTML, 'section_title_row');
                }
                else
                {
                    $this->MakeTitleRow($TitleRowHTML);
                }
            }

            $this->Contents .= '<li><ul class="toggle-target"' . ($SectionArray['HideSection'] === true ? ' style="display:none;"' : '') .
                ((($Module == 'COM' || $Module == 'INC') && $DoExpand && $this->FormMode != 'Design') || $_SESSION['SECTIONEXPANDED'][$Section] ? ' data-rendered="true"' : '') .'>';

            // If section is '$DoExpand' then expand it.
            // If section is stored on the SESSION as expanded, then expand it.
            // [DW-10803] If the module is COM or INC, and at level 1 then skip expanding it
            // $_SESSION['SECTIONEXPANDED'][$Section] should be part of the global if as an OR otherwise sections will not expand in the form design page
            if ($_SESSION['SECTIONEXPANDED'][$Section] == '1' || ($this->FormMode != 'Design' &&
                    ($DoExpand || !($Module == 'COM' || $Module == 'INC'))))
            {
                $this->RenderFields($Section, $Module, $Data, $UseLists);
            }

            $this->Contents .= '</ul></li>';
		}

        $this->Contents .= '
</ol></li>
<!-- END OF SECTION "' . $Section . '" -->
';
	}

    private function DoSpecialSection($SectionArray, $Module, $Data, $FormType)
    {
        $Special = $SectionArray['Special'];

        ob_start();

        switch ($Special)
        {
            case "LinkedContacts":
                if ($Data["recordid"] != "" || $Data["increp_recordid"] != "")
                {
                    ListContacts($Data, "", "", $FormType);
                }

                break;
            case "LinkedActions":
                if ($Data["recordid"] != "")
                {
                    //Need to allow actions for SAB1 users even though main form is readonly.
                    if ($Module == "SAB" && $_SESSION["Globals"]["SAB_PERMS"] == "SAB1")
                    {
                        $FormType = "";
                    }

                    $loader = new src\framework\controller\Loader();
                    $controller = $loader->getController(array(
                        'controller' => 'src\actions\controllers\ActionsController'
                    ));
                    $controller->setRequestParameter('module', $Module);
                    $controller->setRequestParameter('recordid', Sanitize::SanitizeInt($Data['recordid']));
                    $controller->setRequestParameter('FormType', $FormType);
                    echo $controller->doAction('ListLinkedActions');
                }

                break;
            case "LinkedEquipment":
                require_once 'Source/incidents/TrustProperty.php';
                MakePropertyPanel($Module, Sanitize::SanitizeInt($Data["recordid"]), $FormType);
                break;
            case "Feedback":
                require_once 'Source/libs/EmailHistory.php';
                SectionFeedbackToReporter($Module, $Data, $FormType);
                break;
            case "LinkedRecords":
                require_once 'Source/libs/ModuleLinks.php';
                LinkSection($Data, $FormType, $Module);
                break;
            case "ChildLinkedRecords":
                require_once 'Source/libs/ModuleLinks.php';
                ChildLinkSection($Data, $FormType, $Module);
                break;
            case "SectionInjuryDetails":
                require_once 'Source/incidents/Injuries.php';
                SectionInjuryDetails($Data, $FormType, $Module, $SectionArray['ContactSuffix'], 'injury_details', $SectionArray['LinkType']);
                break;
            case "SectionPropertyDetails":
                require_once 'Source/incidents/PersonalProperty.php';
                SectionPropertyDetails($Data, $FormType, $Module, $SectionArray['ContactSuffix'], 'injury_details');
                break;
            case "OrphanedUDFSection":
                $this->MakeOrphanedUDFSection($SectionName, $Data, $Module);
                break;
            case "DynamicContact":
                $aParams = $SectionArray;
                $aParams['data'] = $Data;
                $aParams['FormType'] = $FormType;
                $aParams['level'] = $this->FormDesign->Level;

                MakeDynamicContactSection($aParams);
                break;
            case "DynamicDocument":
                MakeDynamicDocumentSection(array('suffix'=>1, 'data'=>$Data, 'FormType' => $FormType, 'module' => $Module));
                break;
            case "DynamicEquipment":
                require_once 'Source/incidents/TrustProperty.php';

                $Parameters = $SectionArray;
                $Parameters['data'] = $Data;
                $Parameters['FormType'] = $FormType;
                $Parameters['level'] = $this->FormDesign->Level;
                $Parameters['module'] = $Module;
                $Parameters['suffix'] = 1;

                MakeDynamicEquipmentSection($Parameters);
                break;
            case "SectionWordMerge":
                require_once 'Source/MSWord/MSWordIntegrated.php';
                MSWordMain($Module, $Data, $FormType);
                break;
            case "ProgressNotes":
                $loader = new src\framework\controller\Loader();
                $controller = $loader->getController(array(
                    'controller' => 'src\progressnotes\controllers\ProgressNotesController'
                ));
                $controller->setRequestParameter('recordid', Sanitize::SanitizeInt($Data['recordid']));
                $controller->setRequestParameter('module', $Module);
                $controller->setRequestParameter('formType', $FormType);
                echo $controller->doAction('showProgressNotes');
                break;
            case "LinkedCQCPrompts":
                CQC_Outcome::LinkedPromptSection(Sanitize::SanitizeInt($Data['recordid']));
                break;
            case "LinkedCQCSubprompts":
                CQC_Prompt::LinkedSubpromptSection(Sanitize::SanitizeInt($Data['recordid']));
                break;
            case "LinkedASMQuestionTemplates":
                ASM_ModuleTemplate::LinkedQuestionSection(Sanitize::SanitizeInt($Data['recordid']), $FormType);
                break;
            case "LinkedASMQuestions":
                $loader = new src\framework\controller\Loader();
                $controller = $loader->getController(array(
                    'controller' => 'src\assessments\controllers\AssessmentController'
                ));
                $controller->setRequestParameter('recordid', Sanitize::SanitizeInt($Data['recordid']));
                echo $controller->doAction('LinkedQuestionSection');
                break;
            case 'LinkedATIQuestions':
                ATI_Module::LinkedQuestionSection(Sanitize::SanitizeInt($Data['recordid']));
                break;
        }

        $this->Contents .= ob_get_contents();
        ob_end_clean();
    }

    function FieldIsReadOnly($aParams)
    {
        global $ReadOnlyFields;

        if ($aParams['fielddef']['IgnoreReadOnly'])
        {
            return false;
        }

        if ($aParams['fielddef']['EditableWhenRejected'] && $aParams['data']['rep_approved'] == 'REJECT' &&
            !$_GET['print'] && (CanEditRecord($aParams) || CanMoveRecord($aParams)))
        {
            return false;
        }

        if ($this->FormMode == 'ReadOnly')
        {
            return true;
        }

        if ($this->FormMode == 'Locked')
        {
            return true;
        }

        if ($this->FormMode == 'Search')
        {
            return false;
        }

        if ($aParams['fielddef']['ReadOnly'] == true ||
            $aParams['extraparameters']['sectionarray']['ReadOnly'] == true)
        {
            //hard coded read-only overrides form design
            return true;
        }
        elseif (is_array($this->FormDesign->ReadOnlyFields) && array_key_exists($aParams['rowfield'].($aParams['extraparameters']['sectionarray']['ContactSuffix']? '_'.$aParams['extraparameters']['sectionarray']['ContactSuffix'] : ''), $this->FormDesign->ReadOnlyFields))
        {
            //form design settings
            return true;
        }
        elseif ($this->FormMode != 'New' &&
            $aParams['fielddef']['SetOnce'] && $aParams['data'][$aParams['rowfield']] != '' &&
            $aParams['data'][$aParams['rowfield'].'_NotSet'] != 1)
        {
            // Field is "SetOnce" and has a value
            return true;
        }

        return false;
    }

    // MakeFieldRow can optionally take a combo width
    // $Div is the ID of the DIV section that the row is inside.
    function MakeFieldRow($RowField, $Data, $OverrideTitle = "", $ContactSuffix = "", $DropdownWidth = "", $Div = "",
                          $ExtraParameters="", $MainModule="")
    {
        global $MandatoryFields, $ReadOnlyFields, $HideFields, $TimestampFields, $DisplayAsCheckboxes, $UserLabels, $UserExtraText,
               $DefaultValues, $FieldOrders, $FieldDefs, $ModuleDefs, $JSFunctions, $ExpandSections, $ExpandFields,
               $HelpTexts, $ContactMatch, $scripturl, $TextareaMaxChars, $formlevel, $DatixView, $DisplayAsRadioButtons;

        $DisabledHTML = ($this->FormDesign->ReadOnly ? "disabled='disabled'" : "");
        $Disabled = $this->FormDesign->ReadOnly;
        if(is_array($RowField))
        {
            if($FieldDefs[$MainModule][$RowField['Name']]['NoSearch'] === true && $this->FormMode == 'Search')
            {
                return null;
            }
        }
        else
        {
            if($FieldDefs[$MainModule][$RowField]['NoSearch'] === true && $this->FormMode == 'Search')
            {
                return null;
            }
        }

        // Appears to be necessary to prevent PHP5 giving an error:
        // Fatal error: Cannot use string offset as an array
        // a bit further on in this file when we attempt to reference
        // $Data["error"][$InputFieldName] and $Data is empty.
        if (!$Data)
        {
            $Data = array();
        }

        // RowField could itself be an array
        if (is_array($RowField))
        {
            if ($RowField['Type'] == 'udf')
            {
                $FieldDef = $RowField;
                $RowField = $FieldDef['Name'];

                if ($RowField["Title"] != "")
                {
                    $TitleSetInRowArray = true;
                }
            }
            else
            {
                // If the Name key is set, the contents of the
                // array should override the field definition.
                $FieldDef = $this->FieldDefs[$RowField["Name"]];
                // If the title has been set in this row array, set this
                // flag as we don't want it overridden by whatever is in
                // dat_columns.
                if ($RowField["Title"] != "")
                {
                    $TitleSetInRowArray = true;
                }
                // If it's not in the $FieldDefs array, just set $FieldDef
                // to $RowField.
                if (!$FieldDef)
                {
                    $FieldDef = $RowField;
                }
                else
                {
                    $FieldDef = array_merge($FieldDef, $RowField);
                }

                $RowField = $RowField["Name"];
            }
        }
        else
        {
            $FieldDef = $this->FieldDefs[$RowField];

            //If the field type is "array_select", then we need to pull out the right array
            //data for the module from FieldDefs, since different module may have different values for the field.
            if ($FieldDef["Type"] == "array_select" && $MainModule != "")
            {
                if (isset($FieldDefs[$MainModule][$RowField]["SelectArray"]))
                {
                    $FieldDef["SelectArray"] = $FieldDefs[$MainModule][$RowField]["SelectArray"];
                }
            }
        }

        // Only check vw_field_formats if the title hasn't been set in the row array
        $Type = $FieldDef["Type"];
        $Module = $FieldDef["Module"];

        $FFName = $FieldDef['FieldFormatsName'] ?: $RowField;

        if ($Type != "checkbox"
            //hacky exception for Respondent field (resp_id) in payments.
            //I don't know if there's a good reason that all formfields are excluded, so I don't want to change the overall logic here.
            //This can be solved properly when the system wide relabelling is moved into the web.
            && ($Type != "formfield" || $FFName == 'resp_id')
            && $Type != "custom")
		{
            //Temporary hack until form design stores table name in order to pass into function.
            /*  I have thrown in 'ContactsLinkTable' to differentiate 'link_contacts.link_role' from
             *  'link_respondents.link_role'. They are the same in all aspects other than the label. The value
             *  is set in BasicForm_Simple.php */
            $RowTable = $ExtraParameters['sectionarray']['ContactsLinkTable'] ?: GetTableForField($FFName, $MainModule);
            $FFTable = $ExtraParameters['sectionarray']['FieldFormatsTable'];

			$FieldDef["Title"] = Labels_FieldLabel::GetFieldLabel($FFName, $FieldDef["Title"], $RowTable, $Module, false, $FFTable);
        }

        // "Condition" determines whether the row is hidden.
        // User-definable row hiding is kept in $HideFields
        if (is_array($FieldDef) && array_key_exists("Condition", $FieldDef) && !$FieldDef["Condition"])
        {
            return;
        }

        if ($this->FieldIsReadOnly(array(
            'module' => $Module,
            'fielddef' => $FieldDef,
            'data' => $Data,
            'rowfield' => $RowField,
            'extraparameters' => $ExtraParameters)))
        {
            unset($this->FormDesign->MandatoryFields[$RowField]);
        }
        // Cater for SetOnce and ReadOnly fields, but only if we are NOT in
        // Design mode, otherwise 'Order:' fields come out read-only.
        if ($this->FormMode != 'Design')
        {
            if ($FieldDef['SetOnce'])
            {
                //make sure SetOnce carries over to validation-error pages ok.
                if ($Data[$RowField.'_NotSet'] == 1 || $Data[$RowField] == '')
                {
                    $notSetValue = 1;
                }
                else
                {
                    $notSetValue = 0;
                }

                $this->Contents .= '<input type="hidden" id="'.$RowField.'_NotSet" name="'.$RowField.'_NotSet" value="'.$notSetValue.'" />';
            }

            if (!$this->FormDesign->HideFields[$RowField] && $this->FieldIsReadOnly(array('module' => $Module, 'fielddef' => $FieldDef, 'data' => $Data, 'rowfield' => $RowField, 'extraparameters' => $ExtraParameters)))
            {
                $FieldObj = new FormField('ReadOnly');
                $mode = 'ReadOnly';

                if ($FieldDef['SetOnce'])
                {
                    $this->Contents .= '<input type="hidden" id="'.$RowField.'" name="'.$RowField.'" value="'.htmlspecialchars($Data[$RowField]).'" />';
                }
            }
            elseif ($this->FormMode == 'ReadOnly' && $FieldDef['IgnoreReadOnly'])
            {
                $FieldObj = new FormField('Edit');
                $mode = 'Edit';
            }
            else
            {
                $aParams = array(
                    'module' => $Module,
                    'data' => $Data,
                    'perms' => GetParm($ModuleDefs[$ModuleCodeToShow]['PERM_GLOBAL'])
                );

                if ($FieldDef['EditableWhenRejected'] && $Data['rep_approved'] == 'REJECT' && !$_GET['print'] &&
                    (CanEditRecord($aParams) || CanMoveRecord($aParams)))
                {
                    $FieldObj = new FormField('Edit');
                    $mode = 'Edit';
                }
                else
                {
                    $FieldObj = new FormField($this->FormMode);
                    $mode = $this->FormMode;
                }
            }
        }
        else
        {
            // Not sure we still need, this, as FormMode will always be
            // 'Design' if we get to this point, but I'm leaving it in just
            // in case.
            $FieldObj = new FormField($this->FormMode);
        }

        // Need to cater for contact suffixes
        $OriginalFieldName = $RowField;

        if ($FieldDef["AltFieldName"] != "")
        {
            $AltFieldName = $FieldDef["AltFieldName"];
        }
        elseif ($ContactSuffix != "")
        {
            $InputFieldName = $RowField . '_' . $ContactSuffix;
        }
        else
        {
            $InputFieldName = $RowField;
        }

        //If a field has been locked, it should not be the target of a field action
        if (!$this->FormDesign->LockFieldAtribs[$this->CurrentSection][$InputFieldName]["ALLFIELD"])
        {
            // for expanding field sections
            $_SESSION["FieldsSetup"][$this->CurrentSection][$InputFieldName] = $FieldDef["Title"];
        }

        if ($FieldDef["DropdownWidth"] != "")
        {
            $DropdownWidth = $FieldDef["DropdownWidth"];
        }

        // Set the width of the SELECT object if set
        if ($DropdownWidth != "")
        {
            $FieldObj->SetDropdownWidth($DropdownWidth);
        }

        // Does this field cause another section to expand?
        $ExpandSection = $this->FormDesign->ExpandSections[$InputFieldName];

        // Does this field cause another field to expand?
        $ExpandField = $this->FormDesign->ExpandFields[$InputFieldName];

        if ($this->FormMode == "Design")
        {
            // The onclick event in the Hide and Mandatory checkboxes
            // ensures that both Hide and Mandatory cannot be checked
            // at the same time.
            $PreventHide = $FieldDef["NoHide"];
            $PreventMandatory = ($FieldDef["AlwaysMandatory"] || $FieldDef["NoMandatory"] || $FieldDef["ReadOnly"] || $ExtraParameters['sectionarray']['ReadOnly']);
            $PreventReadOnly = ($FieldDef["NoReadOnly"] || $FieldDef["ReadOnly"] || $ExtraParameters['sectionarray']['ReadOnly']);

            if (!$PreventHide)
            {
                $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'HIDE');

                $DesignFields = _tk('hide_q') . ' <input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' name="HIDE-'
                                . $Div . '-' . $InputFieldName . '"';

                $DesignFields .= ' onclick="';

                if (!$PreventMandatory)
                {
                    $DesignFields .= 'addInputHidden(\'MANDATORY-'.$Div.'-'.$InputFieldName.'\'); jQuery(\'input#MANDATORY-'.$Div.'-'.$InputFieldName.'\').val(\'off\'); jQuery(\'[name=MANDATORY-' . $Div . '-' . $InputFieldName . ']:checkbox\').attr(\'checked\',false);';
                }

                if (!$PreventReadOnly)
                {
                    $DesignFields .= 'addInputHidden(\'READONLY-'.$Div.'-'.$InputFieldName.'\'); jQuery(\'input#READONLY-'.$Div.'-'.$InputFieldName.'\').val(\'off\'); jQuery(\'[name=READONLY-' . $Div . '-' . $InputFieldName . ']:checkbox\').attr(\'checked\',false);';
                }

                $DesignFields .= '"';

                if ($this->FormDesign->HideFields[$InputFieldName])
                {
                    $DesignFields .= " checked='checked' ";
                }

                if (!$IsFieldLocked)
                {
                    $DesignFields .= ' onchange="addInputHidden(\'HIDE-'.$Div.'-'.$InputFieldName.'\');jQuery(\'input[name=HIDE-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') ? jQuery(\'input#HIDE-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : jQuery(\'input#HIDE-'.$Div.'-'.$InputFieldName.'\').val(\'off\');"';
                }

                $DesignFields .= ' />';

                if ($IsFieldLocked && $this->FormDesign->HideFields[$InputFieldName])
                {
                    $DesignFields.= '<input type="hidden" name="HIDE-' . $Div . '-' . $InputFieldName . '" value="1"/>';
                }

                // controls for central administration locking
                $dependency = '{0:\'lock-READONLY-' . $Div . '-' . $InputFieldName . '\',1:\'lock-MANDATORY-' . $Div . '-' . $InputFieldName . '\'}';
                $DesignFields .= makeLockField($InputFieldName, 'HIDE', $Div, $this->FormDesign->LockFieldAtribs, $dependency);

                $DesignFields .= ' &nbsp;&nbsp;';
            }

            if (!$PreventReadOnly)
            {
                $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'READONLY');
                $DesignFields .= _tk('read_only_q') . ' <input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . '  name="READONLY-'
                                . $Div . '-' . $InputFieldName . '"';

                $DesignFields .= ' onclick="';

                if (!$PreventMandatory)
                {
                    $DesignFields .= 'addInputHidden(\'MANDATORY-'.$Div.'-'.$InputFieldName.'\'); jQuery(\'input#MANDATORY-'.$Div.'-'.$InputFieldName.'\').val(\'off\'); jQuery(\'[name=MANDATORY-' . $Div . '-' . $InputFieldName . ']:checkbox\').attr(\'checked\',false);';
                }

                if (!$PreventHide)
                {
                    $DesignFields .= 'addInputHidden(\'HIDE-'.$Div.'-'.$InputFieldName.'\'); jQuery(\'input#HIDE-'.$Div.'-'.$InputFieldName.'\').val(\'off\');  jQuery(\'[name=HIDE-' . $Div . '-' . $InputFieldName . ']:checkbox\').attr(\'checked\',false);';
                }

                $DesignFields .= '"';

                if ($this->FormDesign->ReadOnlyFields[$InputFieldName])
                {
                    $DesignFields .= " checked='checked' ";
                }

                if (!$IsFieldLocked)
                {
                    $DesignFields .= ' onchange="addInputHidden(\'READONLY-'.$Div.'-'.$InputFieldName.'\');jQuery(\'input[name=READONLY-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') ? jQuery(\'input#READONLY-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : jQuery(\'input#READONLY-'.$Div.'-'.$InputFieldName.'\').val(\'off\');"';
                }

                $DesignFields .= ' />';

                if ($IsFieldLocked && $this->FormDesign->ReadOnlyFields[$InputFieldName])
                {
                    $DesignFields.= '<input type="hidden" name="READONLY-' . $Div . '-' . $InputFieldName . '" value="1"/>';
                }

                // controls for central administration locking
                $dependency = '{0:\'lock-HIDE-' . $Div . '-' . $InputFieldName . '\',1:\'lock-MANDATORY-' . $Div . '-' . $InputFieldName . '\'}';
                $DesignFields .= makeLockField($InputFieldName, 'READONLY', $Div, $this->FormDesign->LockFieldAtribs, $dependency);
                
                $DesignFields .= ' &nbsp;&nbsp;';
            }

            if (!$PreventMandatory)
            {
                $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'MANDATORY');

                $DesignFields .= _tk('mandatory_q') . ' <input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' name="MANDATORY-'
                                . $Div . '-' . $InputFieldName
                                . '" ';

                $DesignFields .= ' onclick="';

                if (!$PreventReadOnly)
                {
                    $DesignFields .= 'addInputHidden(\'READONLY-'.$Div.'-'.$InputFieldName.'\'); jQuery(\'input#READONLY-'.$Div.'-'.$InputFieldName.'\').val(\'off\'); jQuery(\'[name=READONLY-' . $Div . '-' . $InputFieldName . ']:checkbox\').attr(\'checked\',false);';
                }

                if (!$PreventHide)
                {
                    $DesignFields .= 'addInputHidden(\'HIDE-'.$Div.'-'.$InputFieldName.'\'); jQuery(\'input#HIDE-'.$Div.'-'.$InputFieldName.'\').val(\'off\');  jQuery(\'[name=HIDE-' . $Div . '-' . $InputFieldName . ']:checkbox\').attr(\'checked\',false);';
                }

                $DesignFields .= '"';

                if ($this->FormDesign->MandatoryFields[$InputFieldName] != "")
                {
                    $DesignFields .= ' checked="checked"';
                }

                if (is_array($this->FormDesign->ReadOnlyFields) &&
                    array_key_exists($Div, $this->FormDesign->ReadOnlyFields))
                {
                    $DesignFields .= ' disabled="disabled"';
                }

                if (!$IsFieldLocked)
                {
                    $DesignFields .= ' onchange="addInputHidden(\'MANDATORY-'.$Div.'-'.$InputFieldName.'\');jQuery(\'input[name=MANDATORY-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') ? jQuery(\'input#MANDATORY-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : jQuery(\'input#MANDATORY-'.$Div.'-'.$InputFieldName.'\').val(\'off\');"';
                }

                $DesignFields .= ' />';

                if ($IsFieldLocked && $this->FormDesign->MandatoryFields[$InputFieldName] != "")
                {
                    $DesignFields.= '<input type="hidden" name="MANDATORY-' . $Div . '-' . $InputFieldName . '" value="1"/>';
                }

                // controls for central administration locking
                $dependency = '{0:\'lock-HIDE-' . $Div . '-' . $InputFieldName . '\',1:\'lock-READONLY-' . $Div . '-' . $InputFieldName . '\'}';
                $DesignFields .= makeLockField($InputFieldName, 'MANDATORY', $Div, $this->FormDesign->LockFieldAtribs, $dependency);
                
                $DesignFields .= ' &nbsp;&nbsp;';
            }

            // Output the order field
            if ($this->FormDesign->FieldOrders[$Div])
            {
                $Order = array_search($InputFieldName, $this->FormDesign->FieldOrders[$Div]);
            }

            if (!$FieldDef["NoOrder"] || $Order)
            {
                $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'FIELDORDER');

                $FieldObj->MakeNumberField("FIELDORDER-$Div-$InputFieldName",
                    $Order, 1, 0, '', '', ($this->FormDesign->ReadOnly || $IsFieldLocked));

                $DesignFields .= _tk('order') . ' ' . $FieldObj->GetField();

                if ($IsFieldLocked)
                {
                    $DesignFields.= '<input type="hidden" name="FIELDORDER-' . $Div . '-' . $InputFieldName
                        . '" value="' . $Order . '" />';
                }

                // controls for central administration locking
				$DesignFields .= makeLockField($InputFieldName, 'FIELDORDER', $Div, $this->FormDesign->LockFieldAtribs);

                $DesignFields .= '&nbsp;&nbsp;';
            }

            // Set the Advanced field to CHECKED if necessary
            $AdvancedOn = ($this->FormDesign->UserLabels[$InputFieldName]
                || $this->FormDesign->UserExtraText[$InputFieldName]
                || $this->FormDesign->DefaultValues[$InputFieldName]
                || $this->FormDesign->HelpTexts[$InputFieldName] || $ExpandSection
                || $this->FormDesign->TextareaMaxChars[$InputFieldName]
                || $ExpandSection
                || $ExpandField
                || ($this->FormDesign->ContactMatch[$InputFieldName] && bYN(GetParm('CON_PAS_CHECK', 'N')))
                || $DisplayAsCheckboxes[$InputFieldName]
                || $DisplayAsRadioButtons[$InputFieldName]
                || $TimestampFields[$InputFieldName]
                || ($this->FormDesign->OrganisationMatch[$InputFieldName]));

            if (!$FieldDef["NoAdvanced"])
            {
                if (!$AdvancedOn)
                {
                    $DesignFields .= _tk('advanced_q') . ' <input type="checkbox" '
                                    . ' onclick="expandIt(\'ADVDIV_'
                                    . $InputFieldName . '\', this)" />';
                }
            }

            if (!$FieldDef["NoAdvanced"])
            {
                if (!$AdvancedOn)
                {
                    $DesignFields .= '<div id="ADVDIV_' . $InputFieldName
                                    . '" style="display:none">';
                }

                $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'LABEL');
               
                $DesignFields .= '<br />' . _tk('field_label') . ' <input type="text" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' size="20" name="'
                            . 'LABEL-' . $Div . '-' . $InputFieldName
                            . '" value="' . htmlspecialchars($this->FormDesign->UserLabels["$InputFieldName"])
                            . '" />';

                if ($IsFieldLocked)
                {
                    $DesignFields.= '<input type="hidden" name="LABEL-' . $Div . '-' . $InputFieldName
                            . '" value="' . htmlspecialchars($this->FormDesign->UserLabels["$InputFieldName"]) . '" />';
                }
                            
                            
                // controls for central administration locking
                $DesignFields .= makeLockField($InputFieldName, 'LABEL', $Div, $this->FormDesign->LockFieldAtribs);

                if (!isset($this->FormArray[$Div]['ContactSuffix']) &&
                    !$ExtraParameters['sectionarray']['NoFieldRemoval'] && $Module != 'MED')
                {
                    $CurrentSection = ($this->FormDesign->MoveFieldsToSections[$RowField]['New'] ? $this->FormDesign->MoveFieldsToSections[$RowField]['New'] : $this->CurrentSection);

                    if ($FieldDef['Type'] != 'udf')
                    {
                        $OriginalSection = ($this->FormDesign->MoveFieldsToSections[$RowField]['Original'] ? $this->FormDesign->MoveFieldsToSections[$RowField]['Original'] : $this->CurrentSection);
                    }
                    else
                    {
                        $OriginalSection = $CurrentSection;
                    }

                    // Create the link for the Change Section button
                    $ChangeButtonLink = "$scripturl?action=httprequest&amp;type=changefieldsection";
                    $postBody = 'module=' . $FieldDef['Module'] . '&inputfield=' . $InputFieldName . '&section=' . $CurrentSection . '&originalsection=' . $OriginalSection
                                . ($FieldDef['Type'] == 'udf' ? '&udf=1' : '');

                    $ApplyOnClick = 'if(setReturns()){GetFloatingDiv(\\\'changefieldsection\\\').CloseFloatingControl();}';

                    $LockedSections = array();

                    if (IsCentrallyAdminSys() && !IsCentralAdmin())
                    {
                        foreach ($this->FormArray as $SectionName => $SectionAtribs)
                        {
                            if (isset($this->FormDesign->LockFieldAtribs[$SectionName]['SECTION']))
                            {
                                $LockedSections[] = $SectionName;
                            }
                        }
                    }

                    //Prevent field being restored to default section it has an action for.
                    if ($_SESSION['RecursionLimitations']['movement'][$RowField] &&
                        in_array($OriginalSection, $_SESSION['RecursionLimitations']['movement'][$RowField]))
                    {
                        $ResetOnClick = 'alert(\\\'Unable to move field. This field is related to an action in the default section. Please delete the action before moving the field.\\\');';
                    }
                    elseif (IsCentrallyAdminSys() && !IsCentralAdmin() && in_array($OriginalSection, $LockedSections))
                    {
                    	$ResetOnClick = 'alert(\\\'Unable to move field. This field is related to locked section.\\\');';
                    }
                    else
                    {
                        $ResetOnClick = 'returnToDefault();GetFloatingDiv(\\\'changefieldsection\\\').CloseFloatingControl();';
                    }

                    $ExpandDeleteWarning = false;

                    if(!bYN(GetParm('ALLOW_CROSS_SECTION_ACTIONS', 'N')))
                    {
                        if (is_array($this->FormDesign->ExpandFields))
                        {
                            foreach ($this->FormDesign->ExpandFields as $ExpandArrays)
                            {
                                foreach ($ExpandArrays as $ExpandArray)
                                {
                                    if ($ExpandArray['field'] == $RowField)
                                    {
                                        $ExpandDeleteWarning = true;
                                    }
                                }
                            }
                        }

                        if ($this->FormDesign->ExpandFields[$RowField] || $ExpandDeleteWarning)
                        {
                            $ApplyOnClick = 'if(confirm(\\\'Any field actions set on this field will be deleted when it is moved. Click OK to continue, or cancel to cancel\\\')){'.$ApplyOnClick.'}';
                            $ResetOnClick = 'if(confirm(\\\'Any field actions set on this field will be deleted when it is moved. Click OK to continue, or cancel to cancel\\\')){'.$ResetOnClick.'}';
                        }
                    }

                    $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'CHANGEDSECTION');
                    
                    // Create the input fields for the values
                    $DesignFields .= '<div style="padding:3px">
                    Move this field to another section?
                    <input type="hidden" name="NEWCHANGEDSECTION-'.$OriginalSection.'-' . $RowField . '" id = "NEWCHANGEDSECTION-'.$OriginalSection.'-' . $RowField . '" value="0">
                    <input type="hidden" name="CHANGEDSECTION-'.$OriginalSection.'-' . $RowField . '" id = "CHANGEDSECTION-'.$OriginalSection.'-' . $RowField . '"
                    value="' . $this->FormDesign->MoveFieldsToSections[$RowField]['New'] . '" />
                           <input type="button" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' onclick="
                        var buttons = new Array();
                        buttons[0]={\'value\':\''._tk('btn_apply').'\',\'onclick\':\''.$ApplyOnClick.'\'};
                        buttons[1]={\'value\':\''._tk('btn_cancel').'\',\'onclick\':\'GetFloatingDiv(\\\'changefieldsection\\\').CloseFloatingControl();\'};
                        '.($FieldDef['Type'] != 'udf' ?
                        'buttons[2]={\'value\':\'Return field to Default section\',\'onclick\':\''.$ResetOnClick.'\'};' : '').'
						
                        if (jQuery(\'#lock-HIDE-'.$OriginalSection.'-' . $RowField . '\').val() == 1)
                        {
                        	fielddisplay = \'&lockdisplay=1\';
                        }
                        else
                        {
                        	fielddisplay = \'&lockdisplay=0\';
                        }
                                                
                        PopupDivFromURL(\'changefieldsection\', \'Select New Section\', \''.$ChangeButtonLink.'\' + fielddisplay, \''.$postBody.'\', buttons, \'\');"'
                           . ' value="' . _tk('change_fieldsection_btn') . '" />';
                           
                    $DesignFields .= makeLockField($InputFieldName, 'CHANGEDSECTION', $Div, $this->FormDesign->LockFieldAtribs);
                    $DesignFields .= '</div>';
                }
                else
                {
                    $DesignFields .= '<br />';
                }

                if (!$FieldDef['NoExtraText'])
                {
                    $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'EXTRA');
                	
                    $DesignFields .= '' . _tk('extra_text') . ' <br /><textarea ' . ($this->isTablet ? ' autocapitalize="sentences"' : '')  . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' cols="60" rows="3" name="EXTRA-'
                                    . $Div . '-' . $InputFieldName . '">'
                                    . htmlspecialchars($this->FormDesign->UserExtraText["$InputFieldName"])
                                    . '</textarea>';

                    if ($IsFieldLocked)
                    {
                        $DesignFields.= '<input type="hidden" name="EXTRA-' . $Div . '-' . $InputFieldName . '" id="EXTRA-' . $Div . '-' . $InputFieldName . '" value="' . htmlspecialchars($this->FormDesign->UserExtraText["$InputFieldName"]) . '">';
                    }

	                // controls for central administration locking
	                $DesignFields .= makeLockField($InputFieldName, 'EXTRA', $Div, $this->FormDesign->LockFieldAtribs);
                }

                if (!$FieldDef['NoHelpText'])
                {
                	$IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'HELPTEXT');
                    $DesignFields .= '<br /> ' . _tk('help_text') . ' <br /><textarea ' . ($this->isTablet ? ' autocapitalize="sentences"' : '') . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' cols="60" rows="3" name="HELPTEXT-'
                                    . $Div . '-' . $InputFieldName . '">'
                                    . htmlspecialchars($this->FormDesign->HelpTexts[$InputFieldName])
                                    . '</textarea>';

                    if ($IsFieldLocked)
                    {
                        $DesignFields.= '<input type="hidden" name="HELPTEXT-' . $Div . '-' . $InputFieldName . '" id="HELPTEXT-' . $Div . '-' . $InputFieldName . '" value="' . htmlspecialchars($this->FormDesign->HelpTexts[$InputFieldName]) . '">';
                    }
                                    
                    // controls for central administration locking
                    $DesignFields .= makeLockField($InputFieldName, 'HELPTEXT', $Div, $this->FormDesign->LockFieldAtribs);
                }

                // display multilistbox as checkboxes?
                if ($FieldDef['Type'] == 'multilistbox' || ($FieldDef['Type'] == 'udf' && $FieldDef['DataType'] == 'T'))
                {
                    $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'DISPLAYASCHECKBOXES');
                    
                    $DesignFields .= '<br />' . _tk('display_as_checkboxes') .
                        '<input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' name="DISPLAYASCHECKBOXES-'.$Div.'-'.$InputFieldName.'"'.
                        ($DisplayAsCheckboxes[$InputFieldName] ? ' checked="checked"' : '').
                        ' onchange="addInputHidden(\'DISPLAYASCHECKBOXES-'.$Div.'-'.$InputFieldName.
                        '\');jQuery(\'input[name=DISPLAYASCHECKBOXES-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') '.
                        '? jQuery(\'input#DISPLAYASCHECKBOXES-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : '.
                        'jQuery(\'input#DISPLAYASCHECKBOXES-'.$Div.'-'.$InputFieldName.'\').val(\'off\');" />';
                }

                // display select as radio buttons?
                if (
                        $FieldDef['Type'] == 'ff_select' || ($FieldDef['Type'] == 'udf' && $FieldDef['DataType'] == 'C') ||
                        $FieldDef['Type'] == 'yesno' || ($FieldDef['Type'] == 'udf' && $FieldDef['DataType'] == 'Y')
                   )
                {
                    
                    $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'DISPLAYASRADIOBUTTONS');
                    
                    $DesignFields .= '<br />' . _tk('display_as_radio_buttons') . 
                        '<input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' name="DISPLAYASRADIOBUTTONS-'.$Div.'-'.$InputFieldName.'"'.
                        ($DisplayAsRadioButtons[$InputFieldName] ? ' checked="checked"' : '').
                        ' onchange="addInputHidden(\'DISPLAYASRADIOBUTTONS-'.$Div.'-'.$InputFieldName.
                        '\');jQuery(\'input[name=DISPLAYASRADIOBUTTONS-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') '.
                        '? jQuery(\'input#DISPLAYASRADIOBUTTONS-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : '.
                        'jQuery(\'input#DISPLAYASRADIOBUTTONS-'.$Div.'-'.$InputFieldName.'\').val(\'off\');" />';
                }
                
                if (($FieldDef['Type'] == 'textarea' || ($FieldDef['Type'] == 'udf' && $FieldDef['DataType'] == 'L')) && !$FieldDef['NoTimestamp'] && ! $FieldDef['MaxLength'])
                {
                    $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'TIMESTAMP');
                    
                    $DesignFields .= '<br />' . _tk('timestamp_q') . '<input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' name="TIMESTAMP-'.$Div.'-'.$InputFieldName.'"'.($TimestampFields[$InputFieldName] ? ' checked="checked"' : '').' onchange="addInputHidden(\'TIMESTAMP-'.$Div.'-'.$InputFieldName.'\');jQuery(\'input[name=TIMESTAMP-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') ? jQuery(\'input#TIMESTAMP-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : jQuery(\'input#TIMESTAMP-'.$Div.'-'.$InputFieldName.'\').val(\'off\');"    />';
                }

                if (bYN(GetParm('CON_PAS_CHECK', 'N')))
                {
                    $PASCheckFields = explode(',', GetParm('CON_PAS_CHK_FIELDS', 'con_surname,con_number'));

                    if (array_search ( $InputFieldName, $PASCheckFields) !== false && $this->Module == 'CON') //shouldn't appear on ADM form designs
                    {
                        $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'MATCH');

                        $DesignFields .= '<br /> ' . _tk('con_number_match_text') . '  <input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' ' . $DisabledHTML . ' name="MATCH-'
                            . $Div . '-' . $InputFieldName . '" '.($this->FormDesign->ContactMatch[$InputFieldName] ? 'checked="checked"' : '').' onchange="addInputHidden(\'MATCH-'.$Div.'-'.$InputFieldName.'\');jQuery(\'input[name=MATCH-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') ? jQuery(\'input#MATCH-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : jQuery(\'input#MATCH-'.$Div.'-'.$InputFieldName.'\').val(\'off\');"><br />';
                    }
                }

                $PASCheckFields = explode(',', GetParm('ORG_PAS_CHK_FIELDS', 'org_name'));

                if ($this->Module == 'ORG' && $this->getFormLevel() == 1 && in_array($InputFieldName, $PASCheckFields)) //shouldn't appear on ADM form designs
                {
                    $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'MATCH');

                    $DesignFields .= '<br /> ' . _tk('org_number_match_text') . '  <input type="checkbox" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' ' . $DisabledHTML . ' name="MATCHORG-'
                        . $Div . '-' . $InputFieldName . '" '.($this->FormDesign->OrganisationMatch[$InputFieldName] ? 'checked="checked"' : '').' onchange="addInputHidden(\'MATCHORG-'.$Div.'-'.$InputFieldName.'\');jQuery(\'input[name=MATCHORG-'.$Div.'-'.$InputFieldName.']\').is(\':checked\') ? jQuery(\'input#MATCHORG-'.$Div.'-'.$InputFieldName.'\').val(\'on\') : jQuery(\'input#MATCHORG-'.$Div.'-'.$InputFieldName.'\').val(\'off\');"><br />';

                }

                // Claims name field duplicate match listing
                if (bYN(GetParm('CLA_SHOW_DUPLICATES', 'N')) && $InputFieldName == 'cla_name')
                {
                    $DesignFields .= '<br /> '._tk('select_listing_for_duplicates').' '.
                        ArrayToSelect([
                            'id'      => 'LISTINGDESIGN-DUPLICATES-cla_name',
                            'options' => GetListOfListingDesigns(['module' => 'CLA']),
                            'value'   => $this->FormDesign->ListingDesigns['DUPLICATES']['cla_name']
                        ]);
                }

                // Show the "Maximum characters:" option if it's a textarea,
                // but only if 'MaxLength' isn't already defined.
                if (($FieldDef['Type'] == 'textarea' && !$FieldDef['MaxLength']) || ($FieldDef['Type'] == 'udf' && $FieldDef['DataType'] == 'L'))
                {
                    $ReadOnly = ($this->FormDesign->ReadOnly || $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'TEXTAREAMAXCHARS'));

                    $MaxLengthField = new FormField(($ReadOnly ? 'ReadOnly' : ''));
                    $MaxLengthField->MakeNumberField("TEXTAREAMAXCHARS-$Div-$InputFieldName",
                        $this->FormDesign->TextareaMaxChars[$InputFieldName], 5, 0, '', '', $Disabled, '', true);

                    $DesignFields .= '<br />Maximum number of characters: ' . $MaxLengthField->GetField();
                }
                elseif ($FieldObj->FieldHasAdvancedOptions($RowField, $FieldDef))
                {
                    if (!in_array($FieldDef["Type"],array('multilistbox', 'checkbox')) && !($FieldDef["Type"] == "udf" && ($FieldDef["DataType"]=="T")))
                    {
                        $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'DEF');
                        $fieldMode = ($IsFieldLocked ? 'ReadOnly' : 'Design');

                        if ($FieldDef["Type"] == "udf")
                        {
                            if ($FieldDef["DataType"]=="C")
                            {
                                require_once 'Source/libs/UDF.php';

                                $FieldObj = getUDFDropdownObj($FieldDef["Module"], $FieldDef["ID"], $FieldDef["GroupID"], $FieldDef["DataType"], $this->FormDesign->DefaultValues[$InputFieldName], "Edit");
                            }
                            elseif ($FieldDef["DataType"]=="Y")
                            {
                                $FieldObj = $FieldObj->MakeYesNoSelect($InputFieldName, $FieldDef["Module"], $this->FormDesign->DefaultValues[$InputFieldName], $fieldMode, '', $FieldDef["Title"]);
                            }
                        }
                        else
                        {
                            //rep_approved doesn't really work like a normal dropdown anymore - needs to filter on workflow etc. Better to deal with it separately.
                            if ($InputFieldName == 'rep_approved')
                            {
                                SetUpApprovalArrays($this->Module, $Data, $CurrentApproveObj, $FieldObj, $fieldMode);
                            }
                            elseif ($FieldDef["Type"] == "yesno")
                            {
                                $FieldObj = $FieldObj->MakeYesNoSelect($InputFieldName, $FieldDef["Module"], $this->FormDesign->DefaultValues[$InputFieldName], $fieldMode, '', $FieldDef["Title"]);
                            }
                            else
                            {
                                $FieldObj = Forms_SelectFieldFactory::createSelectField($OriginalFieldName, $FieldDef['Module'], $this->FormDesign->DefaultValues[$InputFieldName], $fieldMode, false);

                                if (isset($FieldDefs[$FieldDef['Module']][$OriginalFieldName]['FieldFormatsName']))
                                {
                                    $FieldObj->setFieldFormatsName($FieldDefs[$FieldDef['Module']][$OriginalFieldName]['FieldFormatsName']);
                                }
                            }
                        }

                        if (!$FieldDef['NoDefault'] && !$FieldDef['ReadOnly'])
                        {
                            if ($FieldObj instanceOf Forms_SelectField)
                            {
                                $FieldObj->setDisableCache();
                            }

                            $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'DEF');
                            
                            // Also need to set a dummy "marker" field to indicate that
                            // the field can accept a default value.
                            $DesignFields .= '<br /><div style="padding-bottom: 3px;">' . _tk('default_value') . ' </div>'
                                . $FieldObj->GetField(($this->FormDesign->ReadOnly || $IsFieldLocked)) . '<input type="hidden" name="DEF-'
                                . $Div . '-' . $InputFieldName . '" value="1" />';
                                
                            // controls for central administration locking
		                    $DesignFields .= makeLockField($InputFieldName, 'DEF', $Div, $this->FormDesign->LockFieldAtribs) . '<br />';

                            // define javascript function for clearing child default fields
                            $children = GetChildren(array('module' => $FieldDef['Module'], 'field' => $OriginalFieldName));

                            if(!empty($children[0]) || !empty($children[1]) || count($parents) == 1)
                            {
                                $OnChangeFunc = 'function OnChange_'.$OriginalFieldName.'() {
                                ';

                                foreach ($children as $childField)
                                {
                                    if ($childField != '')
                                    {
                                        $OnChangeFunc .= '
                                        if (jQuery(\'#'.$childField.'_title\').length){jQuery(\'#'.$childField.'_title\').checkClearChildField('.(bYN(GetParm('RETAIN_COMBO_CHILDREN', 'Y')) ? 'true' : 'false').');}';
                                    }
                                }

                                $OnChangeFunc .= '}';
                                $JSFunctions[] = $OnChangeFunc;
                            }
                        }
                    }

                    if (!$FieldDef['NoSectionActions'] && !$ExtraParameters['sectionarray']['NoSectionActions'] && $Module != 'MED')
                    {
                        $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'EXPANDSECTION');

                        ksort($ExpandSection);

                        // Show Expanding Sections controls
                        if ($ExpandSection)
                        {
                            foreach ($ExpandSection as $ESKey => $ESValues)
                            {
                                $SectionToExpand = $ESValues['section'];
                                $ExpandValues = $ESValues['values'];
                                $ExpandAlertText = htmlspecialchars($ESValues['alerttext']);
                                $ValuesString = "";

                                foreach ($ExpandValues as $EV)
                                {
                                    if ($FieldDef["Type"]=="udf")
                                    {
                                        $ValuesString .= '&nbsp;&nbsp;&nbsp;&nbsp;' . code_descr_udf($FieldDef["ID"],$EV, $_GET['module']) . '<br />';
                                    }
                                    else
                                    {
                                        $ValuesString .= '&nbsp;&nbsp;&nbsp;&nbsp;' . code_descr($FieldDef["Module"], $OriginalFieldName, $EV) . '<br />';
                                    }
                                }

                                if ($SectionToExpand)
                                {
                                    $DesignFields .= '<br /><br />'
                                        . _tk('display_section')
                                        . ' <i>'
                                        . ($_SESSION["SectionsSetup"][$SectionToExpand] ? $_SESSION["SectionsSetup"][$SectionToExpand] : $this->FormArray[$SectionToExpand]['Title'])
                                        . ' </i>';
                                }

                                if ($ExpandAlertText)
                                {
                                    $DesignFields .= '<br />'
                                        . _tk('show_popup_msg') . ' <i>'
                                        . $ExpandAlertText . '</i>';
                                }

                                if ($ExpandValues)
                                {
                                    $ExpandFieldsString = implode(' ', $ExpandValues);
                                }

                                $DesignFields .= '<br /> '. _tk('trigger_values')
                                    . '<br />' . $ValuesString;
                                // Create the link for the Change button
                                $ChangeButtonLink = "$scripturl?action=httprequest&amp;type=changeexpand";
                                $postBody = "targettype=section&amp;module=" . $this->FormDesign->Module . "&amp;inputfield=$InputFieldName&amp;fieldsection=".$this->CurrentSection."&amp;originalfield=$OriginalFieldName&amp;div=$Div&amp;section=$SectionToExpand&amp;fieldvalues=" . urlencode($ExpandFieldsString)
                            . '&amp;alerttext=' . urlencode($ExpandAlertText)
                            . "&amp;sectionindex=$ESKey".($FieldDef["Type"]=="udf"?"&udf=1":"") . '&formlevel=' . $this->FormDesign->getLevel() . '&formid=' . $this->FormDesign->ID;

                                // Create the input fields for the values
                                $DesignFields .= '
        <input type="hidden" name="EXPANDSECTION-'
                            . $ESKey . '-' . $InputFieldName
                            . '" id = "EXPANDSECTION-'
                            . $ESKey . '-' . $InputFieldName . '" value="'
                            . $SectionToExpand . '" /><input type="hidden" name="EXPANDVALUES-'
                           . $ESKey . '-' . $InputFieldName . '" id="EXPANDVALUES-'
                           . $ESKey . '-' . $InputFieldName . '" value="'
                           . $ExpandFieldsString . '" /><input type="hidden" name="EXPANDALERTTEXT-'
                            . $ESKey . '-' . $InputFieldName
                            . '" id = "EXPANDALERTTEXT-'
                            . $ESKey . '-' . $InputFieldName . '" value="'
                            . $ExpandAlertText . '" />'
                            . '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' onclick="
                        var buttons = new Array();
                        buttons[0]={\'value\':\''._tk('btn_apply').'\',\'onclick\':\'if(setReturns()){GetFloatingDiv(\\\'expandsection\\\').CloseFloatingControl();}\'};
                        buttons[1]={\'value\':\''._tk('btn_cancel').'\',\'onclick\':\'GetFloatingDiv(\\\'expandsection\\\').CloseFloatingControl();\'};

                        PopupDivFromURL(\'expandsection\', \'Expand Section Options\', \''.$ChangeButtonLink.'\', \''.$postBody.'\', buttons, \'\');"'
                           . ' value="' . _tk('change_actions_btn') . '" />';

                           // Create the link for the Delete button
                           $DesignFields .= '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' onclick="javascript:
                               if(confirm(\'' . _tk('delete_action_q')
                               . '\'))
                                         {
                                            document.getElementById(\'EXPANDSECTION-' . $ESKey . '-' . $InputFieldName . '\').value = \'\';
                                            document.getElementById(\'EXPANDALERTTEXT-' . $ESKey . '-' . $InputFieldName . '\').value = \'\';
                                            submitClicked = true;
                                            document.forms[0].btnSaveDesign.click();
                                         }"
                                             value="' . _tk('delete_action_btn')
                                                 . '" />';
                            }
                        }
                        else
                        {
                            $DesignFields .= '<br /><br />'
                                . _tk('no_sections_linked');
                        }

                        // Create input fields for new actions.
                        if (isset($ESKey))
                        {
                            $ESKey++;
                        }
                        else
                        {
                            $ESKey = 0;
                        }

                        // Create the link for the Add button
                        $AddButtonLink = "$scripturl?action=httprequest&amp;type=changeexpand";
                        $postBody = "targettype=section&amp;module=" . $this->FormDesign->Module . "&amp;inputfield=$InputFieldName&amp;fieldsection=".$this->CurrentSection
                                    ."&amp;originalfield=$OriginalFieldName&amp;div=$Div&amp;sectionindex=$ESKey".($FieldDef["Type"]=="udf"?"&udf=1":"")
                                    . '&formlevel=' . $this->FormDesign->getLevel() . '&formid=' . $this->FormDesign->ID;
                        $DesignFields .= '
        <input type="hidden" name="EXPANDSECTION-'
                            . $ESKey . '-' . $InputFieldName
                            . '" id = "EXPANDSECTION-'
                            . $ESKey . '-' . $InputFieldName
                            . '" /><input type="hidden" name="EXPANDVALUES-'
                           . $ESKey . '-' . $InputFieldName . '" id="EXPANDVALUES-'
                           . $ESKey . '-' . $InputFieldName
                           . '" /><input type="hidden" name="EXPANDALERTTEXT-'
                            . $ESKey . '-' . $InputFieldName
                            . '" id = "EXPANDALERTTEXT-'
                            . $ESKey . '-' . $InputFieldName
                            . '" />'
                            .
                        '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' onclick="
                        var buttons = new Array();
                        buttons[0]={\'value\':\''._tk('btn_apply').'\',\'onclick\':\'if(setReturns()){GetFloatingDiv(\\\'expandsection\\\').CloseFloatingControl();}\'};
                        buttons[1]={\'value\':\''._tk('btn_cancel').'\',\'onclick\':\'GetFloatingDiv(\\\'expandsection\\\').CloseFloatingControl();\'};

                        PopupDivFromURL(\'expandsection\', \'Expand Section Options\', \''.$AddButtonLink.'\', \''.$postBody.'\', buttons, \'\');"'
                           . ' value="' . _tk('add_action_btn') . '" />';

                        $DesignFields .= makeLockField($InputFieldName, 'EXPANDSECTION', $Div, $this->FormDesign->LockFieldAtribs);
                    }

                    // Show Expanding Fields controls
                    $IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'EXPANDFIELD');

                    if ($ExpandField)
                    {
                        ksort($ExpandField);

                        foreach ($ExpandField as $EFKey => $EFValues)
                        {
                            $FieldToExpand = $EFValues['field'];
                            $ExpandValues = $EFValues['values'];
                            $ExpandAlertText = htmlspecialchars($EFValues['alerttext']);
                            $ValuesString = "";

                            foreach ($ExpandValues as $EV)
                            {
                                if ($FieldDef["Type"]=="udf")
                                {
                                    $ValuesString .= '&nbsp;&nbsp;&nbsp;&nbsp;'
                                        . code_descr_udf($FieldDef["ID"],$EV, $_GET['module']) . '<br />';
                                }
                                else
                                {
                                    $ValuesString .= '&nbsp;&nbsp;&nbsp;&nbsp;'
                                        . code_descr($FieldDef["Module"],
                                            $OriginalFieldName, $EV) . '<br />';
                                }
                            }

							if ($FieldToExpand)
                            {
                                $OriginalName = GetFieldLabel($FieldToExpand);
                                $RelabelledName = GetFormFieldLabel($FieldToExpand,'',$Module);

								$DesignFields .= '<br /><br />'
                                    . _tk('display_field')
                                   . ' <i>'
                                . $OriginalName
                                . ($RelabelledName && $OriginalName != $RelabelledName ? ' (' . $RelabelledName . ')' : '')
                            . ' </i>';
                            }

                            if ($ExpandAlertText)
                            {
                                $DesignFields .= '<br />'
                                    . _tk('show_popup_msg') . ' <i>'
                                    . $ExpandAlertText . '</i>';
                            }

                            if ($ExpandValues)
                            {
                                $ExpandFieldsString = implode(' ', $ExpandValues);
                            }

                            $DesignFields .= '<br /> '. _tk('trigger_values')
                                . '<br />' . $ValuesString;
                            // Create the link for the Change button
                            $ChangeButtonLink = "$scripturl?action=httprequest&amp;type=changeexpand";
                            $postBody = "targettype=field&amp;module=". $this->FormDesign->Module."&amp;inputfield=$InputFieldName&amp;originalfield=$OriginalFieldName&amp;div=$Div&currentsection=".$this->CurrentSection."&amp;targetfield=$FieldToExpand&amp;fieldvalues=" . urlencode($ExpandFieldsString)
                                      . '&amp;alerttext=' . urlencode($ExpandAlertText)
                                      . "&amp;fieldindex=$EFKey".($FieldDef["Type"]=="udf"?"&udf=1":"");

                            // Create the input fields for the values
                            $DesignFields .= '
    <input type="hidden" name="EXPANDFIELD-'
                        . $EFKey . '-' . $InputFieldName
                        . '" id = "EXPANDFIELD-'
                        . $EFKey . '-' . $InputFieldName . '" value="'
                        . $FieldToExpand . '" /><input type="hidden" name="EXPANDFIELDVALUES-'
                       . $EFKey . '-' . $InputFieldName . '" id="EXPANDFIELDVALUES-'
                       . $EFKey . '-' . $InputFieldName . '" value="'
                       . $ExpandFieldsString . '" /><input type="hidden" name="EXPANDFIELDALERTTEXT-'
                        . $EFKey . '-' . $InputFieldName
                        . '" id = "EXPANDFIELDALERTTEXT-'
                        . $EFKey . '-' . $InputFieldName . '" value="'
                        . $ExpandAlertText . '" />'
                        . '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' onclick="
                    var buttons = new Array();
                    buttons[0]={\'value\':\''._tk('btn_apply').'\',\'onclick\':\'if(setReturns()){GetFloatingDiv(\\\'expandfield\\\').CloseFloatingControl();}\'};
                    buttons[1]={\'value\':\''._tk('btn_cancel').'\',\'onclick\':\'GetFloatingDiv(\\\'expandfield\\\').CloseFloatingControl();\'};

                    PopupDivFromURL(\'expandfield\', \'Expand Field Options\', \''.$ChangeButtonLink.'\', \'' . $postBody . '\', buttons, \'\');"'
                       . ' value="' . _tk('change_field_actions_btn') . '" />';

                       // Create the link for the Delete button
                       $DesignFields .= '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' onclick="javascript:
                           if(confirm(\'' . _tk('delete_action_q')
                           . '\'))
                                     {
                                        document.getElementById(\'EXPANDFIELD-' . $EFKey . '-' . $InputFieldName . '\').value = \'\';
                                        document.getElementById(\'EXPANDFIELDALERTTEXT-' . $EFKey . '-' . $InputFieldName . '\').value = \'\';
                                        document.forms[0].btnSaveDesign.click();
                                     }"
                                         value="' . _tk('delete_field_action_btn')
                                             . '" />';
                        }
                    }
                    else
                    {
                        $DesignFields .= '<br /><br />'
                            . _tk('no_fields_linked');
                    }

                    // Create input fields for new actions.
                    if (isset($EFKey))
                    {
                        $EFKey++;
                    }
                    else
                    {
                        $EFKey = 0;
                    }

                    // Create the link for the Add button
                    $AddButtonLink = "$scripturl?action=httprequest&amp;type=changeexpand";
                    $postBody = "targettype=field&amp;module=". $this->FormDesign->Module."&amp;inputfield=$InputFieldName&amp;originalfield=$OriginalFieldName&amp;div=$Div&amp;currentsection=".$this->CurrentSection."&amp;fieldindex=$EFKey".($FieldDef["Type"]=="udf"?"&udf=1":"");

                    $DesignFields .= '
    <input type="hidden" name="EXPANDFIELD-'
                        . $EFKey . '-' . $InputFieldName
                        . '" id = "EXPANDFIELD-'
                        . $EFKey . '-' . $InputFieldName
                        . '" /><input type="hidden" name="EXPANDFIELDVALUES-'
                       . $EFKey . '-' . $InputFieldName . '" id="EXPANDFIELDVALUES-'
                       . $EFKey . '-' . $InputFieldName
                       . '" /><input type="hidden" name="EXPANDFIELDALERTTEXT-'
                        . $EFKey . '-' . $InputFieldName
                        . '" id = "EXPANDFIELDALERTTEXT-'
                        . $EFKey . '-' . $InputFieldName
                        . '" />'
                        .
                    '<input type="button" ' . (($this->FormDesign->ReadOnly || $IsFieldLocked) ? 'disabled="disabled"' : '') . ' onclick="
                    var buttons = new Array();
                    buttons[0]={\'value\':\''._tk('btn_apply').'\',\'onclick\':\'if(setReturns()){GetFloatingDiv(\\\'expandfield\\\').CloseFloatingControl();}\'};
                    buttons[1]={\'value\':\''._tk('btn_cancel').'\',\'onclick\':\'GetFloatingDiv(\\\'expandfield\\\').CloseFloatingControl();\'};

                    PopupDivFromURL(\'expandfield\', \'Expand Field Options\', \''.$AddButtonLink.'\', \''.$postBody.'\', buttons, \'\');"'
                       . ' value="' . _tk('add_field_action_btn') . '" />';
                       
                    $DesignFields .= makeLockField($InputFieldName, 'EXPANDFIELD', $Div, $this->FormDesign->LockFieldAtribs);
                }

                // Need special case for array_select object, i.e. need
                // the default hidden dropdown.
                if ($FieldDef["Type"] == "array_select")
                {
                	$IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'DEF');
                   	$fieldMode = ($IsFieldLocked ? 'ReadOnly' : 'Design');
                	
                    $field = Forms_SelectFieldFactory::createSelectField($InputFieldName, $FieldDef['Module'], $this->FormDesign->DefaultValues[$InputFieldName], $fieldMode);
                    $field->setCustomCodes($FieldDef["SelectArray"]);
                                        
                    // Also need to set a dummy "marker" field to indicate that
                    // the field can accept a default value.
                    $DesignFields .= '<br /><div>' . _tk('default_value') . ' </div>'
                        . $field->getField(($this->FormDesign->ReadOnly || $IsFieldLocked)) . '<input type="hidden" name="DEF-'
                        . $Div . '-' . $InputFieldName . '" value="1" />';
                        
                    // controls for central administration locking
		            $DesignFields .= makeLockField($InputFieldName, 'DEF', $Div, $this->FormDesign->LockFieldAtribs);
                        
                }

                if ($FieldDef["Type"] == "date" || ($FieldDef["Type"] == "udf" && $FieldDef["DataType"]=="D"))
                {
                	$IsFieldLocked = $this->FormDesign->IsFieldLocked($this->CurrentSection, $InputFieldName, 'DEF');
                   	$fieldMode = ($IsFieldLocked ? 'ReadOnly' : 'Design');
                	
                    $field = Forms_SelectFieldFactory::createSelectField($InputFieldName, $FieldDef['Module'], $this->FormDesign->DefaultValues[$InputFieldName], $fieldMode);
                    $field->setCustomCodes(array("TODAY" => "Current date"));

                    // Also need to set a dummy "marker" field to indicate that
                    // the field can accept a default value.
                    if (!$FieldDef['NoDefault'])
                    {
                    	
                        $DesignFields .= '<br /><div>' . _tk('default_value') . ' </div>'
                            . $field->getField(($this->FormDesign->ReadOnly || $IsFieldLocked)) . '<input type="hidden" name="DEF-'
                            . $Div . '-' . $InputFieldName . '" value="1" />';
                            
                        // controls for central administration locking
                    	$DesignFields .= makeLockField($InputFieldName, 'DEF', $Div, $this->FormDesign->LockFieldAtribs);
                    }
                }

                if (!$AdvancedOn)
                {
                   $DesignFields .= '</div>';
                }
            }    // end of if(!$FieldDef["NoAdvanced"])

            if ($FieldObj instanceOf Forms_FormField)
            {
                $FieldObj = new FormField('Design');
            }

            $FieldObj->MakeCustomField($DesignFields);

            $FieldTitle = '<b>'.$FieldDef[Title].'</b>';

            $FieldTitle .= ' ' . makeLockField($InputFieldName, 'ALLFIELD', $Div, $this->FormDesign->LockFieldAtribs);

            $this->MakeRow($FieldTitle, $FieldObj, false, false, $ExtraParameters);

            return $this;
        }

        // Cannot be in design mode here.
        // $HideFields contains user-definable field hiding.
        // It works on the name of the field on the form (i.e. not the
        // database field name - this allows the same fields in different
        // sections to be distinguished.
        // There will be a different behaviour if a default value is set,
        // i.e. the field needs to be generated but hidden (except in search mode).

        //Put data from the database on the form - this should not be called if the form is new, because it can interfere with default fields.
        if (!($ExtraParameters['sectionarray']['LinkFields'] && $this->newLink))
        {
            if ($this->FormDesign->HideFields[$InputFieldName] && $Data[$InputFieldName] && !$FieldDef['ValueRequired'] && $this->FormMode != 'New')
            {
                //If there is data on the record that is hidden on the form, we need to have it available for parenting.
                //The "name" attribute is left blank so that the data is not posted.
                $this->Contents .= '<input type="hidden" id="' . $InputFieldName . '" value="' . htmlspecialchars($Data[$InputFieldName]) . '" />';
                return;
            }
            elseif ($this->FormDesign->HideFields[$InputFieldName] && (!$this->FormDesign->DefaultValues[$InputFieldName]  || $this->FormMode == "Search" || ($this->FormMode == "Edit" && !$FieldDef['ValueRequired'])))
            {
                //There is no need to have this field appear on the form, since it is hidden and has no value.
                return;
            }
        }

        $this->Contents .= '
            <!-- START OF FIELD "'.$RowField.'" -->';

        // Check Expandable fields.
        if ($this->FormMode != "Design" && !empty($InputFieldName) &&
            !$this->isFieldShown(array('field' => $InputFieldName)))
        {
            // In print mode, the form is static, so we can ignore currently hidden sections and fields.
            if ($this->FormMode == "Print")
            {
                return;
            }

            $ExtraParameters['hidden'] = true;
            $this->Contents .= '
			<input type="hidden" name="show_field_'. $InputFieldName . '" id="show_field_'. $InputFieldName . '" value="0" />';

            if(isset($FieldDef['MandatoryField']))
            {
                $this->Contents .= '
            <input type="hidden" name="show_field_'. $FieldDef['MandatoryField'] . '" id="show_field_'. $FieldDef['MandatoryField'] . '" value="0" />';
            }
        }
        else
        {
            $ExtraParameters['hidden'] = false;
            $this->Contents .= '
			<input type="hidden" name="show_field_'. $InputFieldName . '" id="show_field_'. $InputFieldName . '" value="1" />';

            if(isset($FieldDef['MandatoryField']))
            {
            	$this->Contents .= '
            <input type="hidden" name="show_field_'. $FieldDef['MandatoryField'] . '" id="show_field_'. $FieldDef['MandatoryField'] . '" value="1" />';
            }
        }

        $ExtraParameters['dbfield'] = $InputFieldName;

        if ($this->FormDesign->HideFields[$InputFieldName] && $this->FormDesign->DefaultValues[$InputFieldName])
        {
            $FieldDef["DefaultHidden"] = $this->FormDesign->DefaultValues[$InputFieldName];
        }
        elseif ($this->FormDesign->DefaultValues[$InputFieldName] &&
            !$Data[$InputFieldName] && $this->FormMode == 'New')
        {
            if ($FieldDefs[$this->Module][$InputFieldName]['Type'] == 'date' &&
                $this->FormDesign->DefaultValues[$InputFieldName] != 'TODAY')
            {
                $Data[$InputFieldName] = UserDateToSQLDate($this->FormDesign->DefaultValues[$InputFieldName]);
            }
            else
            {
                $Data[$InputFieldName] = $this->FormDesign->DefaultValues[$InputFieldName];
            }
        }
        
        $AuditObj = new FormField("ReadOnly");
        $OldValues = $Data["old_values"];

        if (!empty($OldValues) && $InputFieldName != 'recordid')
        {
            if (isset($OldValues[$InputFieldName]))
            {
                $OldValue = $OldValues[$InputFieldName];
                
                 if ($FieldDef['Type'] == 'udf')
                 {
                     $UDFData = $this->GetUDFValue($FieldDef["ModID"], $FieldDef["RecordID"], $FieldDef["GroupID"], $FieldDef["ID"]);

                     switch ($FieldDef['DataType'])
                     {
                         case 'D':
                             $Data[$InputFieldName] = $UDFData['udv_date'];
                             break;
                         case 'S':
                             $Data[$InputFieldName] = $UDFData['udv_string'];
                             break;
                         case 'N':
                             $Data[$InputFieldName] = $UDFData['udv_number'];
                             break;
                         case 'M':
                             $Data[$InputFieldName] = $UDFData['udv_money'];
                             break;
                         case 'L':
                             $Data[$InputFieldName] = $UDFData['udv_text'];
                             break;
                     }
                 }
                
                 if (!empty($OldValue) && $OldValue != $Data[$InputFieldName])
                 {
                    $AuditValue = $OldValue;
                 }
                 elseif ($OldValues[$InputFieldName] == '' && $OldValue != $Data[$InputFieldName])
                 {
                    $AuditValue = "<no value>";
                }
            }
        }

        if (($ContactSuffix || $Suffix) && $Data[$InputFieldName.'_disabled'] == '1' && $Data[$InputFieldName])
        {
            // we're loading a dynamic form on a level 1 record that has been matched with a database record,
            // so we need to disable any matched fields. This should only come into play after a validation page
            // reload. It's not ideal to put this here as a javascript action, but the alternative is changing all
            // of the fieldobj methods, which is pretty invasive and bug-prone. We can always move this inside those
            // methods in 10.2 or later, when we have time.
            AddJSDisableScript($InputFieldName, $FieldDef);
        }

        if ($UserTitle = $this->FormDesign->UserLabels[$InputFieldName])
        {
            $OverrideTitle = $UserTitle;
        }

        if ($OverrideTitle)
        {
            $Label = $OverrideTitle;
        }
        else
        {
            $Label = _t($FieldDef["Title"]);
        }

        $field = $this->_makeField(($FieldDef['Type'] == 'string_con_search' || $FieldDef['Type'] == 'string_org_search') ? $FieldDef['OriginalType'] : $FieldDef['Type'],
                                    $FieldDef, $OriginalFieldName, $Data,
                                    $mode, $ContactSuffix, $InputFieldName,
                                    $AltFieldName, $FieldObj, $AuditValue,
                                    $AuditObj, $Module, $Label);

        // $ErrorTitle includes <div> tag so that error text can be displayed
        // dynamically
        $ErrorTitle = '<div class="field_error" id="err' . ($FieldDef['MandatoryField'] ? $FieldDef['MandatoryField'] : $InputFieldName)
            . '"';

        if (!is_array($Data["error"][$InputFieldName]))
        {
            $ErrorTitle .= ' style="display:none">' . _tk('mandatory_err');
        }
        else
        {
            $errorMessage = $Data["error"][$InputFieldName]['message'] ? $Data["error"][$InputFieldName]['message'] : _tk('mandatory_err');
            $ErrorTitle .='>' . $errorMessage;
        }

        $ErrorTitle .= '</div>';

        // Set extra text if it has been set in the User Settings.
        if ($this->FormDesign->UserExtraText[$InputFieldName])
        {
            $TitleExtra = _t($this->FormDesign->UserExtraText[$InputFieldName]);
        }
        else
        {
            $TitleExtra = $FieldDef["TitleExtra"];
        }

        $ShowMandatoryFields = ($this->FormMode != "Search" && $this->FormMode != "Print"
                && ($this->FormMode != "ReadOnly" || ($this->FormMode=='ReadOnly' && $FieldDef['IgnoreReadOnly']) ||
                ($Data['rep_approved'] == 'REJECT' && $FieldDef['EditableWhenRejected'])) &&
            $this->FormMode != "Design");

        $Title = '';

        if(
            in_array($FieldDef['Type'], array('ff_select', 'multilistbox', 'yesno', 'string_search'))
            ||
            (isset($FieldDef['FormField']) && $FieldDef['FormField'] instanceof Forms_SelectField)
            ||
            $FieldDef['Name'] == 'act_module' // TODO: improve how this functions to capture more generic instances of this problem when a dropdown is sent as a formfield with full markup - this is a horrible way to get it to work properly for the actions module dropdown when searching.
        )
        {
            $label_id_suffix = '_title';
        }
        else
        {
            $label_id_suffix = '';
        }

        $Title .= '<label class="field_label" for="' . $InputFieldName . $label_id_suffix . '">';

        // Add a red asterisk at the beginning of the field title
        // if it is mandatory, but only do this if the form mode
        // is not Search, Print, ReadOnly or Design.
        if ($ShowMandatoryFields && $this->FormDesign->MandatoryFields[$InputFieldName])
        {
            $Title .= '<img src="images/Warning.gif" alt="'
                . _tk('mandatory_alt') . '" class="mandatory_image" /> ';
        }

        $Title .= $Label;

        if ($FieldDef['Type'] == 'date')
        {
            $Title .= '<span class="field_date_format"> (' . $_SESSION["DATE_FORMAT"] . ')</span>';
        }

        if ($FieldDef['Type'] == 'time')
        {
            $Title .= '<span class="field_time_format"> (hh:mm)</span>';
        }

        // Display the help icon if necessary
        if ($this->FormMode != "Design" && $this->FormMode != "Print"
            && $this->FormMode != "ReadOnly" && $this->FormDesign->HelpTexts[$InputFieldName])
        {
            $Label = str_replace('"', '&quot;',$Label);
            $Label = str_replace('\'', '\\\'',$Label);

            $Title .= '&nbsp;
            <a href="javascript:PopupDivFromURL(\'help\', \''._tk('datix_help_title').$Label.'\', \''
                . $scripturl . '?action=fieldhelp&amp;module=' . $this->Module . '&amp;field=' . $OriginalFieldName . '&amp;level=' . $this->FormDesign->Level
                . '&amp;parent_module=' . $this->FormDesign->getParentModule() . '&amp;link_type=' . $this->FormDesign->getLinkType() . '&amp;parent_level=' . $this->FormDesign->getParentLevel()
                . ($ContactSuffix ? '&amp;consuffix='.$ContactSuffix : '').'&amp;id='.Sanitize::SanitizeInt($this->FormDesign->GetID())
                . '\', \'\', [{\'value\':\'' . _tk('btn_help_close') . '\',\'onclick\':\'GetFloatingDiv(\\\'help\\\').CloseFloatingControl()\'}], \''.DROPDOWN_WIDTH_DEFAULT.'px\')">
            <img id="'.$InputFieldName.'_help_image" src="images/Help.gif" style="cursor:pointer;border:0" alt="' . _tk('help_alt') . ' ('.$Label.')" />
            </a>';
        }

        $Title .= $ErrorTitle;

        $ValidationErrors = GetValidationErrors($Data, $InputFieldName);

        if ($ValidationErrors)
        {
            $Title .= '<div class="field_error">'.$ValidationErrors.'</div>';
        }

        if ($TitleExtra)
        {
            $Title .= '<div class="field_extra_text">'.$TitleExtra.'</div>';
        }
        elseif(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet() && (in_array($InputFieldName, array('inc_med_drug', 'inc_med_drug_rt')) || preg_match('/imed_name_(admin|correct)_[0-9]+/i', $InputFieldName) > 0))
        {
            $Title .= '<div class="field_extra_text">Type at least 4 characters to start search</div>';
        }

        $Title .= '</label>';

        if ($field instanceOf FormField && $this->FormMode != "Search" && $this->FormMode != "Print"
            && $this->FormMode != "ReadOnly")
        {
            $ChangedField = new FormField;
            $ChangedField->MakeChangedFlagField($InputFieldName, $Data['CHANGED-' . $InputFieldName]);
            $field->ConcatFields($field, $ChangedField);
        }
        else if ($FieldObj instanceOf FormField && $this->FormMode != "Search" && $this->FormMode != "Print"
            && $this->FormMode != "ReadOnly")
        {
            $ChangedField = new FormField;
            $ChangedField->MakeChangedFlagField($InputFieldName, $Data['CHANGED-' . $InputFieldName]);
            $FieldObj->ConcatFields($FieldObj, $ChangedField);
        }

        $contactSearchFlds = $this->getSearchFields();
        $organisationSearchFlds = $this->getOrganisationSearchFields();

        if ($FieldDef['Type'] == 'string_con_search' && in_array($OriginalFieldName, $contactSearchFlds) &&
            !in_array($this->FormMode, array('Print', 'ReadOnly', 'Locked', 'Search')))
        {
            $FieldBtn = $this->addContactSearchBtn($InputFieldName, $Module, $ContactSuffix);

            if (!$this->_hasConSearchJs)
            {
                $this->MakeConSearchJs($InputFieldName, $FieldDef['Module'], $ContactSuffix);
            }
        }
        elseif ($FieldDef['Type'] == 'string_org_search' && in_array($OriginalFieldName, $organisationSearchFlds) &&
            !in_array($this->FormMode, array('Print', 'ReadOnly', 'Locked', 'Search')))
        {
            $FieldBtn = $this->addOrganisationSearchBtn($InputFieldName, $Module, $ContactSuffix);

            if (!$this->_hasOrgSearchJs)
            {
                $this->MakeOrgSearchJs($InputFieldName, $FieldDef['Module'], $ContactSuffix);
            }
        }
        elseif ($FieldDef['SubType'] == 'weblink' &&
            !in_array($this->FormMode, array('Print', 'ReadOnly', 'Locked', 'Search')))
        {
            $FieldBtn = $this->addWebLinkBtn($InputFieldName, $Module, $ContactSuffix);
        }
        else
        {
            $FieldBtn = '';
        }

        if (isset($field))
        {
            // new form field class structure
            $this->MakeRow("$Title", $field, 25, $FieldDef["DefaultHidden"], $ExtraParameters, true, $FieldBtn);
        }
        else
        {
            $this->MakeRow("$Title", $FieldObj, 25, $FieldDef["DefaultHidden"], $ExtraParameters, true, $FieldBtn);
        }

        if ($field->extraRow)
        {
            $this->MakeRow($field->extraRow['title'], $field->extraRow['field'], 25, $FieldDef["DefaultHidden"], array_merge($ExtraParameters, array('dbfield' => 'CURRENT_'.$ExtraParameters['dbfield'])));
        }

        if ($AuditValue)
        {
            $this->MakeRow("&nbsp;&nbsp;&nbsp;" . _tk('original_value'), $AuditObj, 25);
        }

        if (($FullAudit = $Data["full_audit"])
            && $FullAudit[$InputFieldName])
        {
            $AuditObj = new FormField();

            foreach ($FullAudit[$InputFieldName] as $Audit)
            {
                if ($FieldDef['Type'] == 'ff_select' || $FieldDef['Type'] == 'multilistbox' ||
                    $InputFieldName == 'rep_approved' || ($FieldDef['Type'] == 'udf' &&
                    $FieldDef['DataType'] == 'T'))
                {
                    $DetailArray = explode(" ",$Audit["aud_detail"]);
                }
                else
                {
                    $DetailArray = array($Audit["aud_detail"]);
                }

                foreach ($DetailArray as $i => $Detail)
                {
                    if ($FieldDef['Type'] == 'ff_select' || $FieldDef['Type'] == 'multilistbox')
                    {
                        $DetailArray[$i] = code_descr($FieldDef['Module'], $InputFieldName, $Detail, '', false);
                    }
                    elseif ($InputFieldName == 'rep_approved')
                    {
                    	$DetailArray[$i] = code_descr($FieldDef['FormField']->getModule(), $InputFieldName, $Detail, '', false);
                    }
                    elseif ($FieldDef['Type'] == 'date' || ($FieldDef['Type'] == 'udf' &&
                        $FieldDef['DataType'] == 'D') )
                    {
                        $DetailArray[$i] = FormatDateVal($Detail);
                    }
                    elseif ($FieldDef['Type'] == 'udf')
                    {
                        $DetailArray[$i] = code_descr_udf($FieldDef["ID"], $Detail);
                    }
                    else
                    {
                        $DetailArray[$i] =  $Detail;
                    }

                    $DetailArray[$i] = htmlspecialchars($DetailArray[$i]);

                    if ($DetailArray[$i] == "")
                    {
                        $DetailArray[$i] = "&lt;no value&gt;";
                    }
                }

                $display = implode(", " , $DetailArray);

                $this->MakeRow("&nbsp;&nbsp;&nbsp;" . code_descr("INC", "inc_mgr", $Audit[aud_login]) . " " . FormatDateVal($Audit["aud_date"], true),
                    $AuditObj->MakeCustomField($display));
            }
        }
    }

    function CheckFormArrayKeys()
    {
        $NewFormArray = array();

        if (is_array($this->FormArray))
        {
            foreach ($this->FormArray as $Key => $aDetails)
            {
                if ($aDetails['Condition'] && $aDetails["AltSectionKey"])
                {
                    $NewFormArray[$aDetails["AltSectionKey"]] = $aDetails;
                }
                else
                {
                    $NewFormArray[$Key] = $aDetails;
                }
            }
        }

        $this->FormArray = $NewFormArray;
    }

    /**
     * Creates a string of Javascript to be executed before searching for contacts
     * Javascript makes sure all required fields are not empty
     *
     * @return string
     */
    function MakeConSearchJs($Name, $module = '', $ContactSuffix = '')
    {
        $fieldlist = GetParm('CON_PAS_CHK_FIELDS', 'con_number,con_surname');
        $con_pas_search_fields = explode(',', $fieldlist);
        $return = '';

        $ctrlData = array(
            'module' => $module,
            'fieldname' => $Name,
            'suffix' => $ContactSuffix,
            'fieldlist' => implode(',', $con_pas_search_fields)
        );

        foreach ($con_pas_search_fields as $key => $con_field)
        {
            if (is_array($GLOBALS['HideFields']) &&
                array_key_exists($con_field . '_' . $ContactSuffix, $GLOBALS['HideFields']))
            {
                unset($con_pas_search_fields[$key]);
            }
            else
            {
                $key = $con_field . '_' . $ContactSuffix;
                $fields[$key] = GetFormFieldLabel($con_field);

                $ctrlData[$con_field] = $con_field . '_' . $ContactSuffix;
            }
        }

        $this->_hasConSearchJs = true;

        return $return;
    }

    /**
     * Creates a string of Javascript to be executed before searching for contacts
     * Javascript makes sure all required fields are not empty
     *
     * @param $Name
     * @param string $module
     * @param string $ContactSuffix
     * @return string
     */
    function MakeOrgSearchJs($Name, $module = '', $ContactSuffix = '')
    {
        $fieldlist = GetParm('ORG_PAS_CHK_FIELDS', 'org_name');
        $org_pas_search_fields = explode(',', $fieldlist);
        $return = '';

        $ctrlData = array(
            'module' => $module,
            'fieldname' => $Name,
            'suffix' => $ContactSuffix,
            'fieldlist' => implode(',', $org_pas_search_fields)
        );

        foreach ($org_pas_search_fields as $key => $org_field)
        {
            if (is_array($GLOBALS['HideFields']) &&
                array_key_exists($org_field . '_' . $ContactSuffix, $GLOBALS['HideFields']))
            {
                unset($org_pas_search_fields[$key]);
            }
            else
            {
                $key = $org_field . '_' . $ContactSuffix;
                $fields[$key] = GetFormFieldLabel($org_field);

                $ctrlData[$org_field] = $org_field . '_' . $ContactSuffix;
            }
        }

        $this->_hasOrgSearchJs = true;

        return $return;
    }

    function call_makeField($Type, $FieldDef, $OriginalFieldName, $Data, $mode, $ContactSuffix, $InputFieldName, $AltFieldName, $FieldObj, $AuditValue, $AuditObj, $Module, $Label = "")
    {
        $FieldObj = $FieldObj ?: new FormField();
        
        $field = $this->_makeField($Type, $FieldDef, $OriginalFieldName, $Data, $mode, $ContactSuffix, $InputFieldName, $AltFieldName, $FieldObj, $AuditValue, $AuditObj, $Module, $Label = "");

        if ($field instanceOf FormField && $this->FormMode != "Search" && $this->FormMode != "Print"
            && $this->FormMode != "ReadOnly")
        {
            $ChangedField = new FormField;
            $ChangedField->MakeChangedFlagField($InputFieldName, $Data['CHANGED-' . $InputFieldName]);
            $field->ConcatFields($field, $ChangedField);
        }

        return $field;
    }

    private function _makeField($Type, $FieldDef, $OriginalFieldName, $Data, $mode, $ContactSuffix, $InputFieldName,
                                $AltFieldName, $FieldObj, $AuditValue, $AuditObj, $Module, $Label = "")
    {
        switch ($Type)
    	{
    		case "ff_select":
    		case FieldInterface::CODE:
                if ($this->FormDesign->DisplayAsRadioButtons[$OriginalFieldName] && $mode != 'Search')
                {
                    $field = Forms_RadioButtonsFieldFactory::createField (
                        $OriginalFieldName, 
                        $FieldDef['Module'], 
                        $Data[$InputFieldName], 
                        $mode, 
                        $FieldDef["Title"], 
                        $this->CurrentSection, 
                        $Data['CHANGED-'.$InputFieldName], 
                        $ContactSuffix,
                        isset($this->FormDesign->MandatoryFields[$InputFieldName])
                    );
                } 
                else
                {
                    $field = Forms_SelectFieldFactory::createSelectField(
                        $OriginalFieldName,
                        $FieldDef['Module'],
                        $Data[$InputFieldName],
                        $mode,
                        false,
                        $FieldDef['Title'],
                        $this->CurrentSection,
                        $Data['CHANGED-'.$InputFieldName],
                        $ContactSuffix
                    );
                }

                if (isset($FieldDef['CustomCodes']))
                {
                    $field->setCustomCodes($FieldDef['CustomCodes']);
                }

                if ($FieldDef['SuppressCodeDisplay'])
                {
                    $field->setSuppressCodeDisplay();
                }

                if ($FieldDef["DefaultHidden"])
                {
                    $field->setDefaultHidden($FieldDef["DefaultHidden"]);
                }

                if ($FieldDef["FieldFormatsName"])
                {
                    $field->setFieldFormatsName($FieldDef["FieldFormatsName"]);
                }

                if ($FieldDef["OnChangeExtra"])
                {
                    $field->setOnChangeExtra($FieldDef["OnChangeExtra"]);
                }
                
    	        if ($FieldDef["SelectFunction"])
                {
                    $field->setSelectFunction($FieldDef["SelectFunction"][0], $FieldDef["SelectFunction"][1]);
                }

                // audit currently handled by old dropdown code
    			if ($AuditValue)
    			{
    			    $AuditObj->MakeDropdown(
    			        $FieldDef["Module"],
    			        $OriginalFieldName,
    				    $AuditValue,
    				    $FieldDef["Parent"],
    				    $FieldDef["Child"],
    				    $FieldDef["Parent2"],
    				    $FieldDef["Child2"],
    				    "Audit_" . $OriginalFieldName,
    				    $FieldDef["ShowCodes"],
    				    $FieldDef["NPSA"],
    				    $FieldDef["DefaultHidden"],
    				    $FieldDef["OldCodes"],
    				    $ContactSuffix
    				);
    			}

                if ( ! empty($FieldDef['data']) && $field instanceof Forms_FormField)
                {
                    $field->setData($FieldDef['data']);
                }

    			$DBField = true;
    			break;
            case "array_select":
                $field = Forms_SelectFieldFactory::createSelectField(
                    $OriginalFieldName, $FieldDef['Module'],
                    $Data[$InputFieldName],
                    $mode,
                    false,
                    $FieldDef['Title'],
                    $this->CurrentSection,
                    $Data['CHANGED-'.$InputFieldName],
                    $ContactSuffix
                );
                $field->setCustomCodes($FieldDef["SelectArray"]);

                if ($FieldDef["DefaultHidden"])
                {
                    $field->setDefaultHidden($FieldDef["DefaultHidden"]);
                }

                if ($FieldDef["OnChangeExtra"])
                {
                    $field->setOnChangeExtra($FieldDef["OnChangeExtra"]);
                }

                break;
    		case "multilistbox":
    		case FieldInterface::MULTICODE:
                if ($this->FormDesign->DisplayAsCheckboxes[$OriginalFieldName] && $mode != 'Search')
                {
                    // display as multiple checkboxes
                    $field = Forms_MultipleCheckboxesFieldFactory::createField(
                        $OriginalFieldName,
                        $FieldDef["Module"],
                        $Data[$InputFieldName],
                        $mode,
                        $FieldDef['Title'],
                        $this->CurrentSection,
                        $Data['CHANGED-'.$InputFieldName],
                        $ContactSuffix
                    );
                }
                else
                {
                    // display as multi select
                    $field = Forms_SelectFieldFactory::createSelectField(
                        $OriginalFieldName,
                        $FieldDef["Module"],
                        $Data[$InputFieldName],
                        $mode,
                        true,
                        $FieldDef['Title'],
                        $this->CurrentSection,
                        $Data['CHANGED-'.$InputFieldName],
                        $ContactSuffix
                    );
                }

                if ($AltFieldName)
                {
                    $field->setAltFieldName($AltFieldName);
                }

                if ($FieldDef['CustomCodes'])
                {
                    $field->setCustomCodes($FieldDef['CustomCodes']);
                }

                if ($FieldDef["OnChangeExtra"])
                {
                    $field->setOnChangeExtra($FieldDef["OnChangeExtra"]);
                }

                if ($FieldDef['IgnoreMaxLength'])
                {
                    $field->setIgnoreMaxLength();
                }

                // audit currently handled by old dropdown code
    			if ($AuditValue == "<no value>")
                {
                    // otherwise validation messes up message
    				$AuditObj->MakeCustomField('&lt;' . _tk('no_value_audit') . '&gt;');
                }
    			elseif ($AuditValue)
                {
    				$AuditObj->MakeMultiListBox($FieldDef["Module"], $RowField, $AuditValue);
                }

    			$DBField = true;
                break;
            case "date":
            case FieldInterface::DATE:
                $FieldObj->MakeDateField($InputFieldName, $Data[$InputFieldName], $FieldDef);

                if ($AuditValue == "<no value>")
                {
                    // otherwise validation messes up message
                    $AuditObj->MakeCustomField('&lt;' . _tk('no_value_audit') . '&gt;');
                }
                elseif ($AuditValue)
                {
                    $AuditObj->MakeDateField("Audit_" . $InputFieldName, $AuditValue);
                }

                $DBField = true;
                break;
            case "datetime":
                $FieldObj->MakeDateTimeField($InputFieldName, $Data[$InputFieldName]);

                if ($AuditValue == "<no value>")
                {
                    // otherwise validation messes up message
                    $AuditObj->MakeCustomField('&lt;' . _tk('no_value_audit') . '&gt;');
                }
                elseif ($AuditValue)
                {
                    $AuditObj->MakeDateTimeField("Audit_" . $InputFieldName, $AuditValue);
                }

                $DBField = true;
                break;
            case "time":
            case FieldInterface::TIME:
                $FieldObj->MakeTimeField($InputFieldName, $Data[$InputFieldName]);

                if ($AuditValue == "<no value>")
                {
                    // otherwise validation messes up message
                    $AuditObj->MakeCustomField('&lt;' . _tk('no_value_audit') . '&gt;');
                }
                elseif ($AuditValue)
                {
                    $AuditObj->MakeTimeField("Audit_" . $InputFieldName, $AuditValue);
                }

                break;
            case "decimal":
                $FieldObj->MakeDecimalField($InputFieldName, $Data[$InputFieldName], $FieldDef["Width"], $FieldDef['MaxLength'], $FieldDef['MaxValue'], $FieldDef['MinValue']);

                if ($AuditValue)
                {
                    $AuditObj->MakeDecimalField("Audit_" . $InputFieldName, $AuditValue, $FieldDef["Width"], $FieldDef['MaxLength'], $FieldDef['MaxValue'], $FieldDef['MinValue']);
                }

                break;
            case "number":
            case FieldInterface::NUMBER:
                $FieldObj->MakeNumberField($InputFieldName, $Data[$InputFieldName], $FieldDef["Width"], $FieldDef['MaxLength'], $FieldDef['MaxValue'], $FieldDef['MinValue']);

                if ($AuditValue)
                {
                    $AuditObj->MakeNumberField("Audit_" . $InputFieldName, $AuditValue, $FieldDef["Width"], $FieldDef['MaxLength'], $FieldDef['MaxValue'], $FieldDef['MinValue']);
                }

                break;
            case "money":
            case FieldInterface::MONEY:
                $FieldObj->MakeMoneyField($InputFieldName, $Data[$InputFieldName], $FieldDef["Width"], $FieldDef["MaxLength"], '', '', $mode);

                if ($AuditValue)
                {
                    $AuditObj->MakeMoneyField("Audit_" . $InputFieldName, $AuditValue, $FieldDef["Width"], $FieldDef["MaxLength"]);
                }

                break;
            case "textarea":
            case FieldInterface::TEXT:
                // Always use MaxLength from FieldDefs if it's set.  This is
                // to cater for textarea fields that are VARCHARs.
                if (!$FieldDef['MaxLength'] && $this->FormDesign->TextareaMaxChars[$InputFieldName])
                {
                    $MyMaxLength = $this->FormDesign->TextareaMaxChars[$InputFieldName];
                }
                else
                {
                    $MyMaxLength = $FieldDef['MaxLength'];
                }

                $FieldObj->MakeTextAreaField(
                    $InputFieldName,
                    $FieldDef["Rows"],
                    $FieldDef["Columns"],
                    $MyMaxLength,
                    $Data[$InputFieldName],
                    ($FieldDef["NoSpellcheck"] == "" ? true : false),
                    true,
                    '',
                    $Label,
                    $Data['CURRENT_'.$InputFieldName]
                );

                if ($AuditValue)
                {
                    $AuditObj->MakeTextAreaField(
                        "Audit_" . $InputFieldName,
                        $FieldDef["Rows"],
                        $FieldDef["Columns"],
                        $FieldDef["MaxLength"],
                        $AuditValue,
                        ($FieldDef["NoSpellcheck"] == "" ? true : false)
                    );
                }

                break;
            case "string":
            case FieldInterface::STRING:
                $FieldObj->MakeInputField(
                    $InputFieldName,
                    $FieldDef["Width"],
                    $FieldDef["MaxLength"],
                    $Data[$InputFieldName],
                    $FieldDef
                );

                if ($AuditValue)
                {
                    $AuditObj->MakeInputField(
                        "Audit_" . $InputFieldName,
                        $FieldDef["Width"],
                        $FieldDef["MaxLength"],
                        $AuditValue
                    );
                }

                break;
            case "email":
                $FieldObj->MakeEmailField(
                    $InputFieldName,
                    $FieldDef["Width"],
                    $FieldDef["MaxLength"],
                    $Data[$InputFieldName]
                );

                if ($AuditValue)
                {
                    $AuditObj->MakeInputField("Audit_" . $InputFieldName, $FieldDef["Width"], $FieldDef["MaxLength"], $AuditValue);
                }

    			break;
    		case "string_search":
                $field = Forms_SelectFieldFactory::createSelectField(
                    $OriginalFieldName,
                    $FieldDef['Module'],
                    $Data[$InputFieldName],
                    $mode,
                    false,
                    $FieldDef['Title'],
                    $this->CurrentSection,
                    $Data['CHANGED-'.$InputFieldName],
                    $ContactSuffix
                );

                $field->setFreeText();

                if ($FieldDef['CustomCodes'])
                {
                    $field->setCustomCodes($FieldDef['CustomCodes']);
                }

                if ($FieldDef["DefaultHidden"])
                {
                    $field->setDefaultHidden($FieldDef["DefaultHidden"]);
                }

                if ($FieldDef["FieldFormatsName"])
                {
                    $field->setFieldFormatsName($FieldDef["FieldFormatsName"]);
                }

                if ($AuditValue)
                {
                    $AuditObj->MakeInputFieldSearch(
                        "Audit_" . $InputFieldName,
                        $FieldDef["Width"],
                        $FieldDef["MaxLength"],
                        $AuditValue
                    );
                }

                break;
            case "string_med_search":
                $FieldObj->MakeInputFieldMedSearch(
                    $InputFieldName,
                    $FieldDef["Width"],
                    $FieldDef["MaxLength"],
                    $Data[$InputFieldName],
                    $FieldDef,
                    $FieldDef["Module"],
                    $ContactSuffix
                );

                if ($AuditValue)
                {
                    $AuditObj->MakeInputFieldSearch(
                        "Audit_" . $InputFieldName,
                        $FieldDef["Width"],
                        $FieldDef["MaxLength"],
                        $AuditValue
                    );
                }

                break;
            case "moduleselect":  //A select box containing modules
                $field = Forms_SelectFieldFactory::createSelectField($InputFieldName, 'ADM', $Data[$InputFieldName], $FieldObj->FieldMode);
                $field->setCustomCodes(getModArray($FieldDef['Exclude'], $FieldDef['GroupModules']));
                $field->setSuppressCodeDisplay();
                break;
            case "accesslevel":  //A select box containing access levels
                $field = MakeAccessLevelField($FieldObj, $InputFieldName,$FieldDef['FormModule'],$Data[$InputFieldName], true);
                break;
            case "formdesign":  //A select box containing form designs
                $field = MakeFormDesignField($FieldDef['FormModule'], $Data[$InputFieldName], $FieldDef['FormLevel'], $mode, $FieldDef['OverrideFormDesignGlobal']);
                break;
            case "searchformdesign":  //A select box containing form designs
                $field = MakeSearchFormDesignField($FieldDef['FormModule'], $Data[$InputFieldName], $FieldDef['FormLevel'], $mode);
                break;
            case "listingdesign":  //A select box containing listing designs
                $field = MakeListingFormDesignField($FieldDef['FormModule'], $Data[$InputFieldName], $FieldDef['FormLevel'], $mode);
                break;
    		case "custom":
                $FieldObj->MakeCustomField($FieldDef["HTML"], $FieldObj->FieldMode);
                break;
            case "function": // include file containing the function
                if ($FieldDef["Include"] != '' && $FieldDef["Include"] != 'Source/libs/FormClasses.php')
                {
                    require_once Sanitize::SanitizeFilePath($FieldDef["Include"]);
                }

                // replace dummy name with real name
                if ($this->FormDesign->MandatoryFields[$FieldDef["MandatoryField"]])
                {
                    $this->FormDesign->MandatoryFields[$InputFieldName] = $this->FormDesign->MandatoryFields[$FieldDef["MandatoryField"]];
                }

                $passArray = null;
                $FormType = $FieldObj->FieldMode;

                foreach ($FieldDef["Parameters"] as $key => $param)
                {
                    $param_type = $param[0];
                    $param_val = $param[1];

                    switch ($param_type)
                    {
                        case "S":
                            $passArray[$key]=$param_val;
                            break;
                        case "V":
                            $passArray[$key]=$$param_val;
                            break;
                    }
                }

                if ($FieldDef["Include"] == 'Source/libs/FormClasses.php')
                {
                    $FieldObj->MakeCustomField($FieldObj->$FieldDef["Function"]($passArray));
                }
                else
                {
                    $FieldObj->MakeCustomField($FieldDef["Function"]($passArray));
                }

                break;
            case "checkbox":
                    if ($this->FormDesign->MandatoryFields[$InputFieldName] || $FieldDef["YesNo"])
                    {
                        if (array_key_exists($InputFieldName, $Data))
                        {
                            if ($Data[$InputFieldName] === true)
                            {
                                $Value = "Y";
                            }
                            elseif ($Data[$InputFieldName] === false)
                            {
                                $Value = "N";
                            }
                            else
                            {
                                $Value = $Data[$InputFieldName];
                            }
                        }
                        elseif ($Data["recordid"] != "")
                        {
                            $Value = "N";
                        }
                        else
                        {
                            $Value = "";
                        }

                        $field = $FieldObj->MakeDivYesNo($InputFieldName, $FieldDef["Div"], $Value);
                    }
                    else
                    {
                        $FieldObj->MakeDivCheckbox($InputFieldName, $FieldDef["Div"], $Data[$InputFieldName], '', true);
                    }

                break;
            case "yesno":
            case FieldInterface::YESNO:
                if ($FieldDef["Div"])
                {
                    $field = $FieldObj->MakeDivYesNo($InputFieldName, $FieldDef["Div"], $Data[$InputFieldName], $FieldDef['DefaultHidden']);

                    if ($AuditValue)
                    {
                        $AuditObj = Forms_SelectFieldFactory::createSelectField('Audit_' . $InputFieldName, $FieldDef['Module'], $AuditValue, 'ReadOnly');
                    }
                }
                else
                {
                    // display as radio buttons
                    if ($this->FormDesign->DisplayAsRadioButtons[$OriginalFieldName] && $mode != 'Search' &&
                        !($this->FormDesign->HideFields[$OriginalFieldName] && $this->FormDesign->DefaultValues[$OriginalFieldName]))
                    {
                        $field = Forms_RadioButtonsFieldFactory::MakeYesNoSelect (
                            $InputFieldName,
                            $FieldDef['Module'],
                            $Data[$InputFieldName],
                            $mode,
                            GetFormFieldLabel($InputFieldName, $FieldDef["Title"]),
                            $this->CurrentSection,
                            $Data['CHANGED-'.$InputFieldName],
                            null, //this was holding the $ContactSuffix, doesn't seem to be necessary though
                            isset($this->FormDesign->MandatoryFields[$InputFieldName])
                        );
                    }
                    else
                    {
                        $field = $FieldObj->MakeYesNoSelect (
                            $InputFieldName,
                            $FieldDef["Module"],
                            $Data[$InputFieldName],
                            $mode,
                            $FieldDef["OnChangeExtra"],
                            GetFormFieldLabel($InputFieldName, $FieldDef["Title"]),
                            $FieldDef['DefaultHidden']
                        );
                    }

                    if ($AuditValue)
                    {
                        if ($this->FormDesign->DisplayAsRadioButtons[$OriginalFieldName] && $mode != 'Search')
                        {
                            $field = Forms_RadioButtonsFieldFactory::createField (
                                "Audit_" . $InputFieldName, 
                                $FieldDef['Module'], 
                                $AuditValue, 
                                $mode
                            );
                        }
                        else
                        {
                            $AuditObj->MakeYesNoSelect(
                                "Audit_" . $InputFieldName, 
                                $FieldDef["Module"], 
                                $AuditValue, 
                                $mode, 
                                '', 
                                '', 
                                $FieldDef['DefaultHidden']
                            );
                        }
                    }
                }
                break;
            case "formfield":
                if ($FieldDef["FormField"] instanceof Forms_FormField)
                {
                    $field = $FieldDef["FormField"];
                }
                else
                {
                    $FieldObj = $FieldDef["FormField"];
                }

                if ($AuditValue)
                {
                    $AuditObj->MakeDropdown(
                        $FieldDef["Module"],
                        $OriginalFieldName,
                        $AuditValue,
                        $FieldDef["Parent"],
                        $FieldDef["Child"],
                        $FieldDef["Parent2"],
                        $FieldDef["Child2"],
                        "Audit_" . $OriginalFieldName,
                        $FieldDef["ShowCodes"],
                        $FieldDef["NPSA"],
                        $FieldDef["DefaultHidden"],
                        $FieldDef["OldCodes"],
                        $ContactSuffix
                    );
                }

    			break;
            case "grading":
                $FieldObj->MakeRatingFields(
                    false,
                    $FieldDef["Consequence"],
                    $FieldDef["Likelihood"],
                    $InputFieldName,
                    "",
                    "",
                    "",
                    _tk('consequence'),
                    _tk('likelihood'),
                    _tk('level'),
                    false
                );

    			break;
            case "udf":
                if ($this->FormMode == 'New' || $this->FormMode == 'Search'
                    || ($this->FormMode == 'Print' && isset($Data[$InputFieldName]))) //hack to allow DIF1 snapshot (generated in "Print" mode) to get data from the posted data array
                {
                    $UDFField = new Fields_ExtraField($FieldDef["ID"]);
                    $format = $UDFField->getFormat();
                    $UDFData = array(
                        'udv_string' => $Data[$InputFieldName],
                        'udv_number' => $Data[$InputFieldName],
                        'udv_date' => $Data[$InputFieldName],
                        'udv_money' => $Data[$InputFieldName],
                        'udv_text' => $Data[$InputFieldName]
                    );
                }
                // Don't want to get the values if we are in search mode.
                elseif ($this->FormMode != 'Search')
                {
                    if (isset($Data[$FieldDef['Name'].$SuffixString]) || ($FieldDef['DataType'] == 'T' && isset($Data['show_field_'.$FieldDef['Name'].$SuffixString])))
                    {
                        $UDFData = array(
                            'udv_string' => $Data[$InputFieldName],
                            'udv_number' => $Data[$InputFieldName],
                            'udv_date' => $Data[$InputFieldName],
                            'udv_money' => $Data[$InputFieldName],
                            'udv_text' => isset($Data['CURRENT_'.$InputFieldName]) ? $Data['CURRENT_'.$InputFieldName] : $Data[$InputFieldName]
                        );
                    }
                    else
                    {
                        $UDFData = $this->GetUDFValue($FieldDef["ModID"], $FieldDef["RecordID"], $FieldDef["GroupID"], $FieldDef["ID"]);
                    }

                    $UDFField = new Fields_ExtraField($FieldDef["ID"]);
                    $format = $UDFField->getFormat();
                }

                // have to check field type here for the time being to know whether or not to use the new form field class structure
                if (in_array($FieldDef["DataType"], array('C','T','Y')))
                { 
                    $field = $this->MakeUDFField($InputFieldName, $FieldDef["ID"], $FieldDef["DataType"], $UDFData, $FieldDef["Title"], $format, $Data['CHANGED-'.$InputFieldName], $FieldDef["GroupID"], !empty($Data["error"]), $mode);
                }
                else
                {
                    $FieldObj = $this->MakeUDFField($InputFieldName, $FieldDef["ID"], $FieldDef["DataType"], $UDFData, $FieldDef["Title"], $format, $Data['CHANGED-'.$InputFieldName], '', !empty($Data["error"]), $FieldDef["MaxLength"]);
                }

    	        if ($AuditValue == "<no value>")
                {
                    // otherwise validation messes up message
                    $AuditObj->MakeCustomField('&lt;' . _tk('no_value_audit') . '&gt;');
                }
                
    			break;
    	}

    	return $field !== null ? $field : $FieldObj;
    }

    /**
     * Gets the HTML for rendering a contact search button
     *
     * @param string $Name The name of input field to a add a button to
     * @param string $module Defaults to empty string
     * @param string $ContactSuffix Defaults to empty string
     * @return string
     */
    function addContactSearchBtn($Name, $module = '', $ContactSuffix = '')
    {
        $searchFields = $this->getSearchFields($ContactSuffix);
        $ctrlData = array(
            'module' => $module,
            'parentmodule' => $_GET['module'] ? Sanitize::getModule($_GET['module']) : Sanitize::getModule($_POST['module']),
            'fieldname' => $Name,
            'suffix' => $ContactSuffix,
            'fieldlist' => implode(',', $searchFields)
        );
        $fields = array();
        $return = '';

        foreach ($searchFields as $field)
        {
            $key = $field . '_' . $ContactSuffix;
            $fields[$key] = GetFormFieldLabel($field);

            $ctrlData[$field] = $field . '_' . $ContactSuffix;
        }

        $return .= '&nbsp;<input type="button" value="Search" class="contact_check_btn_'.$ContactSuffix.'" id="check_btn_' . $Name . '"
            onclick=\'contactSearch(' . json_encode($fields) . ', ' . json_encode($ctrlData) . ')\' />';

        return $return;
    }

    /**
     * Gets the HTML for rendering a organisation search button
     *
     * @param string $Name The name of input field to a add a button to
     * @param string $module Defaults to empty string
     * @param string $ContactSuffix Defaults to empty string
     *
     * @return string
     */
    function addOrganisationSearchBtn($Name, $module = '', $ContactSuffix = '')
    {
        $searchFields = $this->getOrganisationSearchFields($ContactSuffix);
        $ctrlData = array(
            'module' => $module,
            'parentmodule' => $_GET['module'] ? Sanitize::getModule($_GET['module']) : Sanitize::getModule($_POST['module']),
            'fieldname' => $Name,
            'suffix' => $ContactSuffix,
            'fieldlist' => implode(',', $searchFields)
        );
        $fields = array();
        $return = '';

        foreach ($searchFields as $field)
        {
            $key = $field . '_' . $ContactSuffix;
            $fields[$key] = GetFormFieldLabel($field);

            $ctrlData[$field] = $field . '_' . $ContactSuffix;
        }

        $return .= '&nbsp;<input type="button" value="'._tk('btn_search').'" class="organisation_check_btn_'.$ContactSuffix.'" id="check_btn_' . $Name . '"
            onclick=\'organisationSearch(' . json_encode($fields) . ', ' . json_encode($ctrlData) . ')\' />';

        return $return;
    }

    /**
     * Gets the HTML for rendering a Go to link button
     *
     * @param string $Name The name of input field to a add a button to
     * @return string
     */
    function addWebLinkBtn($Name)
    {
        $return .= '&nbsp;<input type="button" value="Go" id="check_btn_' . $Name . '"
            onclick=\'popitup(jQuery(' . $Name . ').val())\' />';

        return $return;
    }

    /**
     * Gets the fields that are used for contact searching on DIF1 forms
     *
     * @param string $contactSuffix Defaults to empty string
     * @return array
     */
    function getSearchFields($contactSuffix = '')
    {
        $fieldlist = GetParm('CON_PAS_CHK_FIELDS', 'con_number,con_surname');
        $con_pas_search_fields = explode(',', $fieldlist);

        foreach ($con_pas_search_fields as $key => $con_field)
        {
            if (is_array($GLOBALS['HideFields']) &&
                array_key_exists($con_field . '_' . $contactSuffix, $GLOBALS['HideFields']))
            {
                unset($con_pas_search_fields[$key]);
            }
        }

        return $con_pas_search_fields;
    }

    /**
     * Gets the fields that are used for organisation searching on CLA1 forms
     *
     * @param string $contactSuffix Defaults to empty string
     * @return array
     */
    function getOrganisationSearchFields($contactSuffix = '')
    {
        $fieldlist = GetParm('ORG_PAS_CHK_FIELDS', 'org_name');
        $org_pas_search_fields = explode(',', $fieldlist);

        foreach ($org_pas_search_fields as $key => $org_field)
        {
            if (is_array($GLOBALS['HideFields']) &&
                array_key_exists($org_field . '_' . $contactSuffix, $GLOBALS['HideFields']))
            {
                unset($org_pas_search_fields[$key]);
            }
        }

        return $org_pas_search_fields;
    }

    /*
     * Gets the Level of the current form
     *
     * @return int $Level
     */
    function getFormLevel()
    {
    	return $this->FormDesign->getLevel();
    }

    function renderFields($Section, $Module, $Data = array(), $UseLists = true)
    {
        $SectionArray = $this->FormArray[$Section];

        if (!$UseLists)
        {
            // Div: $Section_section_contents surrounds everything below the title of a section.
            $this->Contents .= '<div id="'.$Section.'_section_contents">';
        }

        // Special case - contact suffix for e.g. additional witnesses on same form
        // Hide it in a caption tag which is never displayed
        if ($SectionArray['ContactSuffix'] && !$SectionArray['ContactSubForm'])
        {
            $ContactSuffix = $SectionArray['ContactSuffix'];
            $this->Contents .= '<div>';

            if ($SectionArray['LinkRole'])
            {
                if (!$this->FormDesign->DefaultValues["link_role_$ContactSuffix"])
                {
                    $this->FormDesign->DefaultValues["link_role_$ContactSuffix"] = $SectionArray['LinkRole'];
                }
            }

            if ($SectionArray['LinkType'])
            {
                $this->Contents .= '<input type="hidden" name="link_type_'
                    . $ContactSuffix . '" value="'
                    . $SectionArray["LinkType"] .'" />';
            }

            $this->Contents .= '</div>';
        }

        if($this->FormMode == 'Print')
        {
            $SectionArray['ExtraParameters']['parentForm'] = $this->Module;
        }

        $ExtraParameters['sectionarray'] = $SectionArray;

        // If this is a "Special" section and we are not in Design mode,
        // do special processing for it.
        if ($SectionArray['Special'] != '' && $this->FormMode != 'Design')
        {
            if (is_array($this->FormDesign->ReadOnlyFields) &&
                array_key_exists($Section, $this->FormDesign->ReadOnlyFields) &&
                !($this->FormMode == 'Search' || $this->FormMode == 'Design'))
            {
                // need to make sure this section is set to read-only
                $FormType = 'ReadOnly';
            }
            else
            {
                $FormType = $this->FormMode;
            }

            $this->DoSpecialSection($SectionArray, $Module, $Data, $FormType);
        }
        // If this is a "Function" section, do likewise.
        elseif ($SectionArray['Function'] != '' && $this->FormMode != 'Design')
        {
            if ($SectionArray['Include'] != '' && $SectionArray['Include'] != 'Source/libs/FormClasses.php')
            {
                if (file_exists($SectionArray['Include']))
                {
                    $file = allowedInclude($SectionArray['Include']);
                    require_once $file;
                }
            }

            if (is_array($this->FormDesign->ReadOnlyFields) &&
                array_key_exists($Section, $this->FormDesign->ReadOnlyFields) &&
                !($this->FormMode == 'Search' || $this->FormMode == 'Design'))
            {
                // need to make sure this section is set to read-only
                $FormType = 'ReadOnly';
            }
            else
            {
                $FormType = $this->FormMode;
            }

            ob_start();

            if ($SectionArray['Class'])
            {
                $FunctionToCall = array($SectionArray['Class'], $SectionArray['Function']);
            }
            else
            {
                $FunctionToCall = $SectionArray['Function'];
            }

            if ($SectionArray['ExtraParameters'])
            {
                if (is_callable($FunctionToCall))
                {
                    call_user_func($FunctionToCall, $Data, $FormType, $Module, $SectionArray['ExtraParameters']);
                }
            }
            else
            {
                if (is_callable($FunctionToCall))
                {
                    call_user_func($FunctionToCall, $Data, $FormType, $Module);
                }
            }

            $this->Contents .= ob_get_contents();
            ob_end_clean();
        }
        // enable use of controllers to construct form sections
        elseif ($SectionArray['ControllerAction'] != '' && $this->FormMode != 'Design')
        {
            $loader = new src\framework\controller\Loader();
            $controller = $loader->getController(array_shift(array_values($SectionArray['ControllerAction'])));

            if (is_array($this->FormDesign->ReadOnlyFields) &&
                array_key_exists($Section, $this->FormDesign->ReadOnlyFields) &&
                !($this->FormMode == 'Search' || $this->FormMode == 'Design'))
            {
                // need to make sure this section is set to read-only
                $FormType = 'ReadOnly';
            }
            else
            {
                $FormType = $this->FormMode;
            }

            //ensure controller knows what module it is in.
            $controller->setRequestParameter('module', $this->Module);
            $controller->setRequestParameter('data', $Data);
            $controller->setRequestParameter('FormType', $FormType);
            $controller->setRequestParameter('extraParameters', $SectionArray['ExtraParameters']);

            $this->Contents .= $controller->doAction(key($SectionArray['ControllerAction']));
        }
        // Now process the rows in the section.
        // If $FieldOrders[$Section] contains values, use it to
        // determine the order of the fields within the section.
        elseif ($this->FormDesign->FieldOrders[$Section])
        {
            foreach ($this->FormDesign->FieldOrders[$Section] as $Field )
            {
                $RowField = '';

                // Need to cater for contact suffixes - strip out the
                // numerical suffix
                if ($ContactSuffix)
                {
                    $LastUnderscore = \UnicodeString::strrpos($Field, "_");
                    $Field = \UnicodeString::substr($Field, 0, $LastUnderscore);
                }

                if ($InRow = $SectionArray['Rows'][$Field])
                {
                    $RowField = $InRow;
                }
                else
                {
                    if (is_array($SectionArray['Rows']))
                    {
                        if (in_array($Field, $SectionArray['Rows']))
                        {
                            $RowField = $Field;
                        }
                        else
                        {
                            foreach ($SectionArray['Rows'] as $fieldrow)
                            {
                                /*
                                 * The condition check is to make sure that fields that appear more than once
                                 * are not duplicated
                                 * e.g. adm_location
                                 */
                                if ($fieldrow['Name'] == $Field && ($fieldrow['Condition'] != false ||
                                    !isset($fieldrow['Condition'])))
                                {
                                    $RowField = $fieldrow;
                                }
                            }
                        }
                    }
                }

                if ($SectionArray['NoSectionActions'])
                {
                    if (is_array($RowField))
                    {
                        $RowField['NoSectionActions'] = $SectionArray['NoSectionActions'];
                    }
                    else
                    {
                        $RowField = array(
                            'Name' => $RowField,
                            'NoSectionActions' => $SectionArray['NoSectionActions']
                        );
                    }
                }

                if ($RowField)
                {
                    $removeFromReadonlyArray = false;
                    if (is_array($this->FormDesign->ReadOnlyFields) &&
                        array_key_exists(RemoveSuffix($Section), $this->FormDesign->ReadOnlyFields) &&
                        !($this->FormMode == 'Search' || $this->FormMode == 'Design'))
                    {
                        // this section has been set to read only, so add field to global array $ReadOnlyFields
                        $field = is_array($RowField) ? $RowField['Name'] : $RowField;

                        if (!array_key_exists($field, $this->FormDesign->ReadOnlyFields))
                        {
                            $this->FormDesign->ReadOnlyFields[$field] = true;
                            $removeFromReadonlyArray = true;
                        }
                    }

                    $this->MakeFieldRow($RowField, $Data, '', $ContactSuffix,
                        $SectionArray['DropdownWidth'], $Section, $ExtraParameters, $Module);

                    if ($removeFromReadonlyArray)
                    {
                        // we need to remove the fieldname if not explicitly declared as read-only
                        // to prevent a knock-on effect if the same field is used in subsequent sections
                        unset($this->FormDesign->ReadOnlyFields[$field]);
                    }
                }
            }

            // Output any fields that weren't contained in the order array.
            if (is_array($SectionArray['Rows']))
            {
                foreach ($SectionArray['Rows'] as $RowField)
                {
                    $Field = $RowField;

                    if (is_array($Field))
                    {
                        $Field = $Field['Name'];
                    }
                    if ($ContactSuffix)
                    {
                        $Field .= '_' . $ContactSuffix;
                    }

                    if (!array_search($Field, $this->FormDesign->FieldOrders[$Section]))
                    {
                        $removeFromReadonlyArray = false;

                        if (is_array($this->FormDesign->ReadOnlyFields) &&
                            array_key_exists(RemoveSuffix($Section), $this->FormDesign->ReadOnlyFields) &&
                            !($this->FormMode == 'Search' || $this->FormMode == 'Design'))
                        {
                            // this section has been set to read only, so add field to global array $ReadOnlyFields
                            $field = is_array($RowField) ? $RowField['Name'] : $RowField;

                            if (!array_key_exists($field, $this->FormDesign->ReadOnlyFields))
                            {
                                $this->FormDesign->ReadOnlyFields[$field] = true;
                                $removeFromReadonlyArray = true;
                            }
                        }

                        $this->MakeFieldRow($RowField, $Data, '', $ContactSuffix,
                            $SectionArray['DropdownWidth'], $Section, $ExtraParameters, $Module);

                        if ($removeFromReadonlyArray)
                        {
                            // we need to remove the fieldname if not explicitly declared as read-only
                            // to prevent a knock-on effect if the same field is used in subsequent sections
                            unset($this->FormDesign->ReadOnlyFields[$field]);
                        }
                    }
                }
            }
        }
        else
        {
            if (is_array($SectionArray['Rows']))
            {
                foreach ($SectionArray['Rows'] as $RowField)
                {
                    if ($SectionArray['NoSectionActions'])
                    {
                        if (is_array($RowField))
                        {
                            $RowField['NoSectionActions'] = $SectionArray['NoSectionActions'];
                        }
                        else
                        {
                            $RowField = array(
                                'Name' => $RowField,
                                'NoSectionActions' => $SectionArray['NoSectionActions']
                            );
                        }
                    }

                    $removeFromReadonlyArray = false;

                    if (is_array($this->FormDesign->ReadOnlyFields) &&
                        array_key_exists($Section, $this->FormDesign->ReadOnlyFields) &&
                        !($this->FormMode == 'Search' || $this->FormMode == 'Design'))
                    {
                        // this section has been set to read only, so add field to global array $ReadOnlyFields
                        $field = (is_array($RowField) ? $RowField['Name'] : $RowField).($ContactSuffix ? '_'.$ContactSuffix : '');

                        if (!array_key_exists($field, $this->FormDesign->ReadOnlyFields))
                        {
                            $this->FormDesign->ReadOnlyFields[$field] = true;
                            $removeFromReadonlyArray = true;
                        }
                    }

                    $this->MakeFieldRow($RowField, $Data, '', $ContactSuffix, $SectionArray['DropdownWidth'], $Section, $ExtraParameters, $Module);

                    if ($removeFromReadonlyArray)
                    {
                        // we need to remove the fieldname if not explicitly declared as read-only
                        // to prevent a knock-on effect if the same field is used in subsequent sections
                        unset($this->FormDesign->ReadOnlyFields[$field]);
                    }
                }
            }
        }

        if (!$UseLists)
        {
            $this->Contents .= '
                    </div>';
        }
    }

    function addUDFFields($Data)
    {
        if (is_array($this->FormDesign->ExtraFields))
        {
            foreach ($this->FormDesign->ExtraFields as $Field => $SectionName)
            {
                list($FieldID, $GroupID) = explode('_', $Field);

                $ExtraFieldDef = array();
                $ExtraField = new Fields_ExtraField($FieldID, $GroupID);
                $ExtraFieldDef['ID'] = $FieldID;
                $ExtraFieldDef['Type'] = 'udf';
                $ExtraFieldDef['Name'] = $ExtraField->getName();

                if ($this->FormMode == 'Design')
                {
                    $ExtraFieldDef['Title'] = $ExtraField->GetDesignLabel();
                }
                else
                {
                    $ExtraFieldDef['Title'] = $ExtraField->getLabel();
                }

                $ExtraFieldDef['DataType'] = $ExtraField->getFieldType();
                $ExtraFieldDef['RecordID'] = Sanitize::SanitizeInt($this->FormArray['Parameters']['Suffix'] ? $Data[$ModuleDefs[$this->Module]['FK'].'_'.$this->FormArray['Parameters']['Suffix']] : $Data['recordid']);
                $ExtraFieldDef['ModID'] = GetModIDFromShortName($this->Module);
                $ExtraFieldDef['Suffix'] = $this->FormArray['Parameters']['Suffix'];
                $ExtraFieldDef['GroupID'] = $GroupID;
                $ExtraFieldDef['MaxLength'] = $ExtraField->getLength();

                if (isset($this->FieldDefs['UDF_'.$ExtraFieldDef['DataType'].'_'.$GroupID.'_'.$FieldID]))
                {
                    // *should* only be the case if we're showing contact matching button for this field
                    $ExtraFieldDef['OriginalType'] = $ExtraFieldDef['Type'];
                    $ExtraFieldDef['Type'] = $this->FieldDefs['UDF_'.$ExtraFieldDef['DataType'].'_'.$GroupID.'_'.$FieldID]['Type'];
                }

                $this->FormArray[$SectionName]['Rows'][$ExtraField->getName()] = $ExtraFieldDef;
            }
        }
    }

    function MoveFieldsToSections()
    {
        if ($this->FormDesign->MoveFieldsToSections)
        {
            foreach ($this->FormDesign->MoveFieldsToSections as $FieldName => $MoveSettings)
            {
                //exception for inc_type, which appears in formArray twice for the level 2 form.
                //The alternative to this would be to alter every form design whenever the global controlling it was changed.
                if ($this->getFormLevel() == 2 && $FieldName == 'inc_type')
                {
                    $typeFieldLocation = GetParm('DIF_'.$this->getFormLevel().'_CCS');

                    if ($typeFieldLocation == 'CCS')
                    {
                        $MoveSettings['Original'] = 'ccs';
                    }
                    else
                    {
                        $MoveSettings['Original'] = 'tcs';
                    }
                }

                if ($this->FormArray['Parameters']['Suffix'])
                {
                    $MoveSettings['Original'] .= '_'.$this->FormArray['Parameters']['Suffix'];
                    $MoveSettings['New'] .= '_'.$this->FormArray['Parameters']['Suffix'];
                }

                // Check that the 'Original' section actually has some
                // rows in it, otherwise array_search() will give an error.
                if ($this->FormArray[$MoveSettings['Original']]['Rows'])
                {
                    // We have to cater for three situations.  For most form
                    // arrays, the 'Rows' array will take the form of
                    // array('field1', 'field2', 'field3'), i.e. indexed by
                    // number.  In this situation, we need to find the index
                    // so we can remove the field from the original array.
                    // In other arrays, there may be a key and a value, e.g.
                    // Rows => array('field1' => array('Name' => 'field1',
                    // ...)).  Here we can simply use the field name as the
                    // key to remove the value from the original array.
                    // Finally, some fields are defined by
                    // array('Name'=> fieldname, 'Condition' => condition...)
                    //
                    $FieldKey = array_search($FieldName, $this->FormArray[$MoveSettings['Original']]['Rows']);

                    if ($FieldKey !== false)
                    {
                        // Array is indexed by number
                        // Remove the field from the original section
                        unset($this->FormArray[$MoveSettings['Original']]['Rows'][$FieldKey]);
                        // Append the field to the new section
                        $this->FormArray[$MoveSettings['New']]['Rows'][] = $FieldName;
                    }
                    elseif ($this->FormArray[$MoveSettings['Original']]['Rows'][$FieldName])
                    {
                        // Array is associative, keyed by field name
                        $FieldValue = $this->FormArray[$MoveSettings['Original']]['Rows'][$FieldName];
                        // Remove the field from the original section
                        unset($this->FormArray[$MoveSettings['Original']]['Rows'][$FieldName]);
                        // Append the field to the new section
                        $this->FormArray[$MoveSettings['New']]['Rows'][$FieldName] = $FieldValue;
                    }
                    else
                    {
                        foreach ($this->FormArray[$MoveSettings['Original']]['Rows'] as $key => $aField)
                        {
                            if (is_array($aField) && $aField['Name'] == $FieldName)
                            {
                                // Unset old array
                                $FieldValue = $this->FormArray[$MoveSettings['Original']]['Rows'][$key];
                                unset($this->FormArray[$MoveSettings['Original']]['Rows'][$key]);
                                // Append the field to the new section
                                $this->FormArray[$MoveSettings['New']]['Rows'][] = $FieldValue;
                            }
                        }
                    }
                }
            }
        }
    }

    function AddExpandCollapseAllSectionsJS()
    {
        $this->Contents.= '
            <script type="text/javascript">
                function CollapseAllSections()
                {
                    var sections = null,
                        parentLi = null,
                        childs = null;

                    // Get all img elements that have id like "twisty_image"
                    jQuery(".toggle-target").hide();
                    jQuery(".toggle-trigger").find("img").attr({
                        src: "Images/expand.gif",
                        alt: "+"
                    });
                }

                function ExpandAllSections()
                {
                    var sections = \''.json_encode($this->ExpandSections).'\',
                        obj = jQuery.parseJSON(sections);

                    for (var i = 0; i < obj.length; i++)
                    {
                        jQuery("#SECTIONEXPANDED-"+obj[i]+"-"+obj[i]).val("1");
                    }

                    jQuery("<input>").attr({
                        type: "hidden",
                        id: "expandall",
                        name: "expandall",
                        value: "1"
                    }).appendTo("form");

                    jQuery("form[name=\'frmDesign\']").submit();
                }
            </script>
        ';
    }

    function setCurrentSection($Section)
    {
        $this->CurrentSection = $Section;
    }
    
    /**
     * Retrieves the field list for the "Add extra field to this section" dropdown.
     * 
     * @param string $module
     * 
     * @return array
     */
    protected function getExtraFieldList($module)
    {
        if ($this->extraFields === null)
        {
            if ($module == 'ADM')
            {
                $modId = ModuleCodeToID('CON');
            }
            else
            {
                $modId = ModuleCodeToID($module);
            }

            $ExtraFields = DatixDBQuery::PDO_fetch_all(
                'SELECT CAST(udf_fields.recordid as varchar) + \'_\' + CAST(UDF_LINKS.group_id as varchar) as udf_id, fld_name + \' (\' + udf_groups.grp_descr + \')\' as fld_name
                FROM UDF_FIELDS
                LEFT JOIN UDF_MOD_LINK ON uml_id = udf_fields.recordid
                LEFT JOIN UDF_LINKS on UDF_LINKS.field_id = udf_fields.recordid
                JOIN UDF_GROUPS on UDF_groups.recordid = UDF_LINKS.group_id
                WHERE (udf_groups.mod_id = :mod_id) AND (uml_module = :module OR uml_module IS NULL)
                UNION
                SELECT CAST(udf_fields.recordid as varchar) + \'_0\' as udf_id, fld_name
                FROM UDF_FIELDS
                LEFT JOIN UDF_MOD_LINK ON uml_id = udf_fields.recordid
                WHERE (uml_module = :module2 OR uml_module IS NULL)
                ORDER BY fld_name', array('module' => $module, 'module2' => $module, 'mod_id' => $modId));
            
            $this->extraFields = array();
            if (!empty($ExtraFields))
            {
                foreach ($ExtraFields as $ExtraFieldDetails)
                {
                    $this->extraFields[strval($ExtraFieldDetails['udf_id'])] = $ExtraFieldDetails['fld_name'];
                }
            }
        }
        return $this->extraFields;
    }
}

/**
* Re-formats a number value by emulating the specified picture format used by Gupta
*
* Returns the reformatted value.
*
* @param float     $Value   number value to be re-formatted.
* @param string    $format_pattern  Gupta picture format.
*
* @return float    The re-formatted value.
*/
function GuptaFormatEmulate($Value, $format_pattern)
{
    $decimal_places = 0;
    $decimal_char = '';
    $split_format = explode('.', $format_pattern);

    if (count($split_format) > 1)
    {
        $decimal_places = \UnicodeString::strlen($split_format[1]);
        $decimal_char = '.';
    }

    if (\UnicodeString::stripos( $format_pattern, ','))
    {
        $commas = true;
    }

    return number_format(floatval($Value), $decimal_places, $decimal_char, $commas ? ',' : '');
}

/*
 * checks to see if the filepath is allowed to be included
 *
 * @param string	$Path 	file path to be included
 *
 * @return boolean	true if allowed, false if not
 */
function allowedInclude($path)
{
	// allowed includes
	$allowed = array(
		'Source/security/SecurityBase.php',
		'Source/security/Profiles.php',
		'Source/generic/Subjects.php',
        'Source/generic_modules/COM/ModuleFunctions.php',
        'Source/generic_modules/CLA/ModuleFunctions.php',
        'Source/generic_modules/CLA/Payments.php',
		'Source/generic_modules/PAY/ModuleFunctions.php',
		'Source/libs/Subs.php',
		'Source/libs/FormClasses.php',
		'Source/generic/Subjects.php',
        'Source/generic/CausalFactors.php',
		'Source/medications/MainMedications.php',
		'Source/libs/FormClasses.php',
		'Source/risks/RiskBase.php',
        'Source/sabs/MainSABS.php',
        'Source/actions/ActionForm.php'
	);

	$key = array_search($path, $allowed);

	if ($key !== false)
	{
		// Test to see if the file exists
		if (!file_exists($path))
		{
			throw new Exception('attempted include file ' . $path . ' does not exists');
		}

		return $allowed[$key];
	}

	throw new Exception ('Path ' . $path . ' not defined in whitelist');
}
