<?php

class XmlExport
{
    var $content = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
    
    function __construct()
    {
        $this->content .= '<!DOCTYPE DATIXDATA[';                                                                                                             
        $this->content .= GetEntityCodes();                                          
        $this->content .= ']>';        
    }
    
    function XOpenTag($tagname = "", $parameternames = "", $parameters = "")
    {
        $xml .="<".$tagname;

        if(is_array($parameternames) && is_array($parameters))
        {
            // array_combine() only in php5
            // $params = array_combine($parameternames, $parameters);
            $keys = array_values( (array) $parameternames );
            $vals = array_values( (array) $parameters );
            $n = max( count( $keys ), count( $vals ) );
            $r = array();
            for( $i=0; $i<$n; $i++ ) {
              $params[ $keys[ $i ] ] = $vals[ $i ];
            }

            foreach($params as $parametername => $parameter)
                $xml .= " ".$parametername."=\"".$parameter."\" ";
        }
        elseif($parameters)
            $xml .= " ".$parameternames."=\"".$parameters."\"";

        $xml .= ">";
        $this->content .= $xml;
    }

    function XCloseTag($tagname = "")
    {
        $xml .="</".$tagname.">";
        $this->content .= $xml;
    }

    function XNewDataSet($parameter = "")
    {
        $this->XOpenTag("dataset", "name", $parameter);
    }

    function XCloseDataSet()
    {
        $this->XCloseTag("dataset");
    }

    function XNewLine($tagname = "", $value = "", $parameternames = "", $parameters = "")
    {
        $xml = "<".$tagname;

        if(is_array($parameternames) && is_array($parameters))
        {
            // array_combine() only in php5
            // $params = array_combine($parameternames, $parameters);
            $keys = array_values( (array) $parameternames );
            $vals = array_values( (array) $parameters );
            $n = max( count( $keys ), count( $vals ) );
            $r = array();
            for( $i=0; $i<$n; $i++ ) {
              $params[ $keys[ $i ] ] = $vals[ $i ];
            }

            foreach($params as $parametername => $parameter)
                $xml .= " ".$parametername."=\"".$parameter."\" ";
        }
        elseif($parameters)
            $xml .= " ".$parameternames."=\"".$parameters."\"";

        $xml .= ">" . ReplaceAllEntityCodes($value) . "</".$tagname."> ";
        $this->content .= $xml;
    }

    function XGlobal($tagname = "", $value = "", $alttagname = "global")
    {
        $xml = "<".$alttagname.">";
        $xml .= "<name>".$tagname."</name>";
        $xml .= "<value>".ReplaceAllEntityCodes($value)."</value>";
        $xml .= "</".$alttagname.">";
        $this->content .= $xml;
    }

    function XTitleLine($value = "")
    {
        $this->XNewLine("title", ReplaceAllEntityCodes($value));
    }

    function XValueLine($value = "")
    {
        $this->XNewLine("value", ReplaceAllEntityCodes($value));
    }

    function XNameLine($value = "")
    {
        $this->XNewLine("name", ReplaceAllEntityCodes($value));
    }

    function GetXML()
    {
        return $this->content;
    }

    function WriteXML($xmloutput, $outputpath)
    {
        $handle=fopen($outputpath . "/config.xml", 'w+');
        $somecontent = $xmloutput;
        if (fwrite($handle, $somecontent) === FALSE)
        {
           echo "Cannot write to file test.xml";
           exit;
        }
        fclose($handle);

    }

    function OpenXML($path)
    {
        $file_source = $path . "/config.xml";
        $handle=fopen($file_source, 'rb');
        $contents = fread($handle, filesize($file_source));
        fclose($handle);

        return $contents;
    }

    // Calling this function with a tablename exports all the records to XML
    function XExportTabletoXML($tablenames)
    {
        if(is_array($tablenames))
        {
            $this->XNewDataSet("Tables");
            $this->XTitleLine("Database Tables");
            
            if(!is_array($tablenames))
                $tablenames = array($tablenames);

            if(is_array($tablenames))
            foreach($tablenames as $tablename)
            {
                $tableresult = ExportTabletoXML($tablename, $cols);
                $this->XOpenTag("table");
                $this->XNewLine("name", $tablename);

                while($row = db_fetch_array($tableresult))
                {
                    if(is_array($cols))
                    foreach($cols as $val)
                    {
                        $this->XOpenTag("row");
                        $this->XGlobal($val, $row[$val], "col");
                        $this->XCloseTag("row");
                    }
                }

                $this->XCloseTag("table");
        }

        $this->XCloseDataSet();
        }
        return $this;
    }
}

class XmlElement {
  var $name;
  var $attributes;
  var $content;
  var $children;
};

function xml_to_object($xml) {


  $xml = str_replace('<content><?php', '<content>', $xml);
  $xml = str_replace('?></content>', '</content>', $xml);

  $parser = xml_parser_create('UTF-8');
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parse_into_struct($parser, $xml, $tags);
  xml_parser_free($parser);

  $elements = array();
  $stack = array();
  foreach ($tags as $tag)
  {
       $index = count($elements);
       if ($tag['type'] == "complete" || $tag['type'] == "open")
       {
            $elements[$index] = new XmlElement;
            $elements[$index]->name = $tag['tag'];
            if(array_key_exists('attributes', $tag))
                $elements[$index]->attributes = $tag['attributes'];
            if(array_key_exists('value', $tag))
                $elements[$index]->content = $tag['value'];
            if ($tag['type'] == "open") {
               $elements[$index]->children = array();
               $stack[count($stack)] = &$elements;
               $elements = &$elements[$index]->children;
            }
       }
       if ($tag['type'] == "close") {
            $elements = &$stack[count($stack) - 1];
            unset($stack[count($stack) - 1]);
       }
  }
  return $elements[0];
}

// This function is called by function XExportTabletoXML to collect data
function ExportTabletoXML($tablename = "", &$cols)
{

    if($tablename != "")
    {
        $sql = "SELECT * FROM $tablename";

        $result = db_query($sql);

    	$cols_ret = db_fetch_array($result, MSSQL_ASSOC);

        $cols = array();
        if (is_array($cols_ret))
        {
            foreach($cols_ret as $key => $value)
            {
                array_push($cols, $key);
            }
        }
    }
    return $result;

}

// import records from XML to database
function ImportTablefromXML()
{
}
?>