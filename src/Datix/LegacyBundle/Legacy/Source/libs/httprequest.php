<?php

function httpRequest()
{
	$action = $_GET['type'];

    if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
    {
        $WordSuffix = '';
    }
    else
    {
        $WordSuffix = 'PHP';
    }

	$requestType = array(
        // Code selection popup window -- NB when Rico is gone and we no longer need CodeSelectControl JS objects, we can lose this action
        'codelist' => array(
            'controller' => 'src\\codeselection\\controllers\\CodesController'
        ),
        
	    // dropdown codes
	    'getcodes' => array(
            'controller' => 'src\\codeselection\\controllers\\CodesController'
        ),   
	        
		'getparents' => array("Source/libs/CodeSelectionCtrl.php", 'GetParents'),
		'getchildren' => array("Source/libs/CodeSelectionCtrl.php", 'GetChildren'),
		'checkchild' => array("Source/libs/CodeSelectionCtrl.php", 'CheckChild'),
        'selectfunction' => array("Source/libs/SelectFunctions.php", 'getSelectFunction'),

        // Contact match on Patient number
        'contactlist' => array(
            'controller' => 'src\\contacts\\controllers\\ContactCheckController'
        ),

        // Medication match
        'medicationlist' => array(
            'controller' => 'src\\med\\controllers\\MedicationsCheckController'
        ),

		// email templates
        'gettemplatetypes' => array(
            'controller' => 'src\\emailtemplates\\controllers\\EmailTemplateTypeController'
        ),
        'getemaildetails' => array(
            'controller' => 'src\\emailtemplates\\controllers\\EmailDetailController'
        ),

        //overdue emails
        'getoverdueemailrecipients' => array("Source/libs/OverdueEmails.php", 'GetRecipients'),
        'sendoverdueemails' => array("Source/libs/OverdueEmails.php", 'SendOverdueEmails'),

        //udf searching
        'addudfsearchrow' => array("Source/libs/UDF.php", 'AddUDFRow'),

        // injury table
        'insertinjuryrow' => array("Source/incidents/Injuries.php", 'printInjuryRow'),
		// property table
		'insertpropertyrow' => array("Source/incidents/PersonalProperty.php", 'printPropertyRow'),

		// adding contacts to DIF1
		'addcontacttoDIF1' => array("Source/libs/Subs.php", 'get_contact_section'),
        // adding generic sections to form
        'adddynamicsection' => array("Source/libs/Subs.php", 'addDynamicSection'),
        'getrowlist' => array("Source/libs/Subs.php", 'getRowList'),

        // adding generic fields
        'getfieldhtml' => array("Source/libs/BatchUpdate.php", 'getFieldHTMLAjax'),

        //batchupdate operations
        'getbatchupdaterow' => array("Source/libs/BatchUpdate.php", 'getBatchUpdateRowAjax'),

        // adding sections to standards tree
        'getstandards' => array("Source/Standards/EvidenceForm.php", 'getStandards'),
        'getelements' => array("Source/Standards/EvidenceForm.php", 'getElements'),

        // change expanding fields/sections
        'changeexpand' => array(
            'controller' => 'src\\formdesign\\controllers\\ChangeExpandController'
        ),
		// change section a field appears in
		'changefieldsection' => array("Source/AdminSetup.php", 'ChangeFieldSection'),

        // Record Locking
		'checklock' => array("Source/libs/RecordLocks.php", 'CheckLock'),
        'resettimer' => array("Source/libs/RecordLocks.php", 'ResetTimer'),
		'unlockrecord' => array("Source/libs/RecordLocks.php", 'UnlockRecord_ajax'),

		// Staff responsibilities
		'staffrespresponse' => array("Source/incidents/StaffRespResponse.php", 'StaffRespResponse'),

		// Main page
		'setglobal' => array("Source/libs/Subs.php", 'SetGlobalAJAX'),
        'setsession' => array("Source/libs/Subs.php", 'SetSessionAJAX'),

        // Security groups
        'unlink_user_groups' => array("Source/security/SecurityUserGroupLinks.php", 'UnlinkUserGroups'),
        'get_location_type_panel' => array("Source/security/SecurityUsers.php", 'panelLocationTypeSettings'),
        'unlink_profiles' => array("Source/security/SecurityUserGroupLinks.php", 'UnlinkProfiles'),

        //UserConfigGathering
        'sendconfigtodatix' => array("Source/libs/ConfigTracking/ConfigSenderLib.php", 'SendToDatix'),

        // Risk Rating
        'getratinglevel' => array("Source/libs/RiskRatingBase.php", 'GetRatingLevel'),

        // Saved queries
        'getquerywhere' => array(
            'controller' => 'src\\savedqueries\\controllers\\SavedQueriesController'
        ),

        //Email alerts
        'sendemailalerts' => array("Source/libs/Email.php", 'EmailAlertsService'),
        'sendfeedback' => array("Source/libs/Email.php", 'SendFeedback'),

        //Mark action as complete
        'actioncomplete' => array(
            'controller' => 'src\\actions\\controllers\\ActionsController'
        ),
        'changeactionprogress' => array(
            'controller' => 'src\\actions\\controllers\\ActionsController'
        ),
        // Group report
        'usergrouprepresponse' => array(
            'controller' => 'src\\admin\\controllers\\UserGroupReportController'
        ),

        // Dashboards
        'getwidgetcontents' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'editwidget' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'movewidget' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'setwidgetorders' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'addtomydashboard' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'createnewdashboard' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'setdefaultdashboard' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'updatedashboardsettings' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'deletedashboard' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'editdashboard' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'getdashboardhtml' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),

        // MS Word
        'msword_merge' => array("Source/MSWord/MSWordIntegrated{$WordSuffix}.php", 'MSWordMerge'),
        'msword_get_datasource' => array("Source/MSWord/MSWordIntegrated{$WordSuffix}.php", 'MSWordGetDataSource'),
        'msword_get_doc_as_xml' => array("Source/MSWord/MSWordIntegrated{$WordSuffix}.php", 'MSWordGetDocAsXML'),
        'returnmergesql' => array("Source/MSWord/MSWordIntegrated{$WordSuffix}.php", 'ReturnMergeSQL'),
        'copytemplatedoctoclient' => array("Source/MSWord/MSWordIntegrated{$WordSuffix}.php", 'CopyTemplateDoctoClient'),
        'deletewordtmpfiles' => array("Source/MSWord/MSWordIntegrated{$WordSuffix}.php", 'DeleteWordTmpFile'),
        'downloadmergedoc' => array("Source/MSWord/MSWordIntegrated{$WordSuffix}.php", 'DownloadMergeDoc'),

        // Reporting
	    'exporttopdfoptions' => array("Source/reports.php", 'ExportToPDFOptions'),
	
        'getTrafficLightOptions' => array(
            'controller' => 'src\\reports\\controllers\\ReportDesignFormController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'getExportFormHTML' => array(
            'controller' => 'src\\reports\\controllers\\ReportDesignFormController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'exportreport' => array(
            'controller' => 'src\\reports\\controllers\\ReportController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'riskTrackerReport' => array(
        	'controller' => 'src\\reports\\controllers\\RiskTrackerTemplateController',
        	'filters' => array(
        		'src\\framework\\controller\\LoggedInFilter'
        	)
        ),
        'addreportprompt' => array(
            'controller' => 'src\\reports\\controllers\\ReportController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'addtomyreports' => array(
            'controller' => 'src\\reports\\controllers\\ReportController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),

        // Risk grade tracker report
        'riskgradetracker' => array("Source/risks/riskgradetracker.php", 'getOptionsHtml'),

        // CodeTags
        'GetCodeTagLinkEditDropdown' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagLinkController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\FullAdminFilter'
            )
        ),
        // NPSA mapping
        'countin05' => array(
            'controller' => 'src\\admin\\controllers\\IN05Controller',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'generatein05' => array(
            'controller' => 'src\\admin\\controllers\\IN05Controller',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'getanddeletepngfile' => array(
            'controller' => 'src\\reports\\controllers\\ReportController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'importtabledata' => array("Source/setups/ImportTableData.php", 'importTableData'),

        // Risk matrix mapping
        'generateriskmatrix' => array("Source/setups/RiskMatrix.php", 'generateRiskMatrix'),

        // Listing checkboxes
        'flagrecord' => array("Source/classes/RecordLists/RecordListClass.php", array('RecordLists_RecordListShell', 'FlagSessionRecordList')),
        'flagallrecords' => array("Source/classes/RecordLists/RecordListClass.php", array('RecordLists_RecordListShell', 'FlagAllSessionRecordList')),
        'unflagrecord' => array("Source/classes/RecordLists/RecordListClass.php", array('RecordLists_RecordListShell', 'UnFlagSessionRecordList')),
        'unflagallrecords' => array("Source/classes/RecordLists/RecordListClass.php", array('RecordLists_RecordListShell', 'UnFlagAllSessionRecordList')),

        // set flag state for session recordlists
        'setsessionflagstate' => array("Source/libs/Subs.php", array('RecordLists_RecordListShell', 'SetSessionFlagState')),
        'clearsessionrecordlist' => array("Source/libs/Subs.php", array('RecordLists_RecordListShell', 'ClearSessionRecordList')),

        // create new action chain dialogue
        'newactionchain' => array(
            'controller' => 'src\\actionchains\\controllers\\ActionChainController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),

        // cascade data up through parent fields
        'getparentvalue' => array("Source/libs/Subs.php", 'GetParentValue'),

        // get adm_year dropdown for popup when distributing assessments
        'getadmyearprompttext' => array("Source/libs/Subs.php", 'GetAdmYearPromptText'),


        // add new/edit stage history dialogue
        'editstagehistory' => array(
            'controller' => 'src\\claims\\controllers\\StageHistoryController'
        ),
        'savestagehistory' => array(
            'controller' => 'src\\claims\\controllers\\StageHistoryController'
        ),
        'deletestagehistory' => array(
            'controller' => 'src\\claims\\controllers\\StageHistoryController'
        ),
        'renderfields' => array(
            'controller' => 'src\\formdesign\\controllers\\RenderFieldsController'
        ),
        // Organisation match on CLA1
        'organisationlist' => array(
            'controller' => 'src\\organisations\\controllers\\OrganisationsController'
        ),

        'duplicaterecordcheck' => array(
            'controller' => 'src\\generic\\controllers\\DuplicateRecordCheckController'
        )
    );

    if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
    {
        $requestType['checkmergecontactprompt'] = array("Source/MSWord/MSWordIntegrated.php", 'CheckMergeContactPrompt');
    }
    else
    {
        $requestType['getcontactchoicearray'] = array(
            'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        );
    }

    try
    {
        if (isset($requestType[$action]['controller']) && class_exists($requestType[$action]['controller']) &&
            in_array('src\\framework\\controller\\Controller', class_parents($requestType[$action]['controller'])))
        {
            // use new Controller class structure
            $loader = new src\framework\controller\Loader();
            $controller = $loader->getController($requestType[$action]);
            echo $controller->doAction($action, true);
        }
        else
        {
            $includeFile = $requestType[$action][0];
            $executeFunction = $requestType[$action][1];

            require_once $includeFile;

            if ($executeFunction != '')
            {
                call_user_func($executeFunction);
            }
        }

        if (!in_array($_GET['responsetype'], array('json', 'autocomplete'))) //will cause invalid return array.
        {
            echoJSFunctions();
        }

        obExit();
    }
    catch (Exception $e)
    {
        header('A system error occurred when processing the ajax request', null, 500);
    }
}
