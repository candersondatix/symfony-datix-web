<?php 
/* vim: set expandtab tabstop=4 shiftwidth=4: */

/* Create new object of class */ 
$ses_class = new session();

/* Change the save_handler to use the class functions */ 
session_set_save_handler (array(&$ses_class, '_open'), 
                          array(&$ses_class, '_close'), 
                          array(&$ses_class, '_read'), 
                          array(&$ses_class, '_write'), 
                          array(&$ses_class, '_destroy'), 
                          array(&$ses_class, '_gc')); 

/* Start the session */ 
session_start();

class session 
{ 
    /* Define the table you wish to use with 
       this class, this table MUST exist. 
    */ 
    
    var $ses_table = "web_sessions"; 


    /* Open session, if you have your own db connection 
       code, put it in here! 
       The database connection will already be open */ 
    function _open($path, $name) { 
        global $sess_save_path;

        $sess_save_path = $path;

        return TRUE; 
    } 

    /* Close session */ 
    function _close() { 
        /* This is used for a manual call of the 
           session gc function */ 
        //$this->_gc(0); 
        return TRUE;          
    } 

    /* Read session data from database */ 
    function _read($ses_id) { 
        $session_sql = "SELECT ses_value FROM " . $this->ses_table 
                     . " WHERE ses_id = '$ses_id'"; 
        $session_row = DatixDBQuery::PDO_fetch($session_sql); 

        if ($session_row) { 
            $ses_data = $session_row["ses_value"]; 
            return $ses_data; 
        } else { 
            return ''; 
        } 
    } 

    /* Write new data to database */ 
    function _write($ses_id, $data) { 
           
        // escape single quotes
        $data = str_replace("'","''",$data);
        
        $session_sql = "UPDATE " . $this->ses_table 
                     . " SET ses_time='" . time() 
                     . "', ses_value=:ses_value, ses_user=:ses_user WHERE ses_id=:ses_id"; 
        
        $query = new DatixDBQuery($session_sql);      
        $session_res = $query->prepareAndExecute(array('ses_value' => $data, 'ses_user' => $_SESSION[user], 'ses_id' => $ses_id));
        
        if (!$session_res) { 
            return FALSE; 
        } 
        if ($query->getRowsAffected() > 0) { 
            // Update the user_licence table
            $sql = "UPDATE user_licence SET session_start = :session_start WHERE ses_id = :ses_id";
            $res = DatixDBQuery::PDO_query($sql, array('session_start' => date('Y-m-d H:i:s'), ses_id => $ses_id));
            return TRUE;
        } 

        $session_sql = "INSERT INTO " . $this->ses_table 
                     . " (ses_id, ses_time, ses_start, ses_value)" 
                     . " VALUES (:ses_id, :ses_time, :ses_start, :ses_value)"; 
        $session_res = DatixDBQuery::PDO_query($session_sql, array('ses_id' => $ses_id, 'ses_time' => time(), 'ses_start' => time(), 'ses_value' => $data)); 
        if (!$session_res) {     
            return FALSE; 
        }         else { 
            if ($_SESSION["user"])
            {
                $now = date('Y-m-d H:i:s');
                $sql = "INSERT INTO user_licence (login, session_start, user_type, first_login, ses_id, terminal)
                    VALUES (:login, :session_start, :user_type, :first_login, :ses_id, :terminal)";
                $result = DatixDBQuery::PDO_query($sql, array('login' => $_SESSION[user], 'session_start' => $now, 'user_type' => 'W', 'first_login' => $now, 
                                                                'ses_id' => $ses_id, 'terminal' => $_SERVER['REMOTE_ADDR']));
            }
            return TRUE; 
        } 
    } 

    /* Destroy session record in database */ 
    function _destroy($ses_id) { 
        $session_sql = "DELETE FROM " . $this->ses_table
                     . " WHERE ses_id = :ses_id"; 
        $query = new DatixDBQuery($session_sql);      
        $session_res = $query->prepareAndExecute(array('ses_id' => $ses_id));
        if ($query->getRowsAffected() > 0) 
        { 
            return FALSE; 
        }         
        else 
        { 
            $sql = "DELETE FROM user_licence WHERE ses_id = :ses_id";
            $result = DatixDBQuery::PDO_query($sql, array('ses_id' => $ses_id));
            return TRUE; 
        } 
    } 

    /* Garbage collection, deletes old sessions */ 
    function _gc($life) { 
        $ses_life = strtotime("-$life second"); 

        $session_sql = "DELETE FROM " . $this->ses_table 
                     . " WHERE ses_time < :ses_life"; 
        $session_res = DatixDBQuery::PDO_query($session_sql, array('ses_life' => $ses_life));

        if (!$session_res) { 
            return FALSE; 
        }         else { 
            return TRUE; 
        } 
    } 
} 
?> 