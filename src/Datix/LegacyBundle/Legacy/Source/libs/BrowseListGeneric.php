<?php
// ==========================================================================//
// Initialise conditions                                                     //
// ==========================================================================//

$record_url_part = 'fromsearch=1';

$DrillIntoReport = false;

if ($_SESSION[$module]['DRILL_QUERY'] instanceof \src\framework\query\Query &&
    isset($_REQUEST['from_report']) && $_REQUEST['from_report'] == '1' || isset($_REQUEST['fromcrosstab']) && $_REQUEST['fromcrosstab'] == '1')
{
    $DrillIntoReport = true;
    $record_url_part = 'from_report=1';
}

if (isset($_REQUEST['overdue']))
{
    $record_url_part .= '&overdue=1';
}

$old_ExtraWhere = $_SESSION[$module]["EXTRAWHERE"];
$_SESSION[$module]["EXTRAWHERE"] = '';

$ListType = Sanitize::SanitizeString($_GET["listtype"]);
$ListRef = Sanitize::SanitizeString($_GET["listref"]);

//Clear last saved query id if we are replacing the "last search" with a hard coded listing.
if ($ListType != 'search' || $ListRef)
{
    unset($_SESSION[$module]['LASTQUERYID']);
}

$orderby = $_SESSION["LIST"][$module]["ORDERBY"];
$order = $_SESSION["LIST"][$module]["ORDER"];

if (!$orderby)
{
    $orderby = $ModuleDefs[$module]['DEFAULT_ORDER'];
}
if (!$order)
{
    $order = $ModuleDefs[$module]['DEFAULT_ORDER_DIRECTION'];
}

if (!$order && $module == 'INC')
{
    if (bYN(GetParm('LISTINGS_ORDER_DESC', 'N')))
    {
        $order = "DESC";
    }
    else
    {
        $order = "ASC";
    }
}
elseif (!$order)
{
    $order = "DESC";
}

if ($_POST["listnum"])
{
    $listnum = Sanitize::SanitizeInt($_POST["listnum"]);
}

$America = GetParm("COUNTRY") == "CANADA" || GetParm("COUNTRY") == "USA";

// ==========================================================================//
// Build where clause according to access level, permission, timescales      //
// and criteria specific to the type of listing                              //
// ==========================================================================//

// Default title and action
$ListTitle = _tk(strtolower($module)."_listing");

if($ModuleDefs[$module]['GENERIC'] && !$ModuleDefs[$module]['NO_GENERIC_FORM'])
{
    $ActionType = 'record';
}
else
{
    $ActionType = $ModuleDefs[$module]['ACTION'];
}

$Perms = GetParm($ModuleDefs[$module]['PERM_GLOBAL']);

$ListTitleArray = array();
$ListTitleArray = GetLevelFieldLabels($module);

if ($ListType == 'search')
{
    $ListTitle = $ModuleDefs[$module]['NAME']." Search Listing";
}

if (isset($ListRef))
{
    $ListType = "search";
    $_SESSION[$module]["WHERE"] = $ModuleDefs[$module]['TABLE'] . ".rep_approved like '$ListRef'";
    //$ExtraWhere[] = "rep_approved like '$ListRef'";
    $ListTitle = ($_GET['overdue'] ? 'Overdue ' : '') . \UnicodeString::ucwords($ModuleDefs[$module]['REC_NAME_PLURAL']) . ' with status: '.$ListTitleArray["$ListRef"]['description'];
}

if ($ModuleDefs[$module]['GENERIC'] && file_exists('Source/generic_modules/'.$ModuleDefs[$module]['GENERIC_FOLDER'].'/BrowseList.php'))
{
    require_once 'Source/generic_modules/'.$ModuleDefs[$module]['GENERIC_FOLDER'].'/BrowseList.php';
}

if ($ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType])
{
    if ($_GET['overdue'])
    {
        if ($ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType]['OverdueWhere'])
        {
            $_SESSION[$module]["WHERE"] = $ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType]['OverdueWhere'];
            $Where[] = $ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType]['OverdueWhere'];
        }
        else
        {
            //this list type cannot become overdue
            $_SESSION[$module]["WHERE"] = '1=2';
            $Where[] = '1=2';
        }

        $ListTitle = 'Overdue ' . $ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType]['Title'];
    }
    else
    {
        $_SESSION[$module]["WHERE"] = $ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType]['Where'];
        $Where[] = $ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType]['Where'];
        $ListTitle = $ModuleDefs[$module]['HARD_CODED_LISTINGS'][$ListType]['Title'];
    }
}
elseif ($ModuleDefs[$module]['CUSTOM_BROWSE_LIST'])
{
    require_once $ModuleDefs[$module]['CUSTOM_BROWSE_LIST'];
}

if ($_REQUEST["drillwhere"] != "")
{
    $ListTitle = 'Listing from '.$ModuleDefs[$module]['NAME']." report";
}

if ($TitleComments)
{
    $ListTitle .= " - $TitleComments";
}

// Store table(s) used for the query in session var, mainly used when reporting
$_SESSION[$module]["TABLES"] = ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']);

// Passed from crosstab report or from a graphical report
if ($_REQUEST["drillwhere"] != "")
{
    $Where = array(stripslashes(Sanitize::SanitizeRaw($_REQUEST["drillwhere"])));
    $_SESSION[$module]["EXTRAWHERE"] = $old_ExtraWhere;
    $_SESSION[$module]["current_drillwhere"] = Sanitize::SanitizeRaw($_REQUEST["drillwhere"]);
}
elseif (($_GET['overdue'] == 1 || ! isset($_GET['overdue'])) && $DrillIntoReport)
{
    $query = $_SESSION[$module]['DRILL_QUERY'];
    $table = ($ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE']);

    $writer = new \src\framework\query\SqlWriter();

    list($statement, $parameters) = $writer->writeStatement($query);

    // Replace all occurrences of ? with values on the where clause
    $needle = '?';

    foreach ($parameters as $parameter)
    {
        $parameterPos = UnicodeString::strpos($statement, $needle);

        if ($parameterPos !== false)
        {
            $statement = UnicodeString::substr_replace($statement, DatixDBQuery::quote($parameter), $parameterPos, UnicodeString::strlen($needle));
        }
    }

    $Where[] = $table.'.recordid IN ('.$statement.')';

    $_SESSION[$module]['current_drillwhere'] = $Where[0];
}
else
{
    if (is_array($ExtraWhere) && !empty($ExtraWhere))
    {
	    $Where[] = implode(" and ", $ExtraWhere);
        $_SESSION[$module]["EXTRAWHERE"] = implode(" and ", $ExtraWhere);
    }

    if ($_GET['fromcrosstab'] == '1')
    {
        $CrosstabWhere = $_SESSION['CurrentReport']->ConstructCellWhereClause($_GET);
        if ($CrosstabWhere)
        {
            $Where[] =  "(" . $CrosstabWhere . ")";
        }
    }
    elseif ($ListType == "search" && $_SESSION[$module]["WHERE"])
    {
	    $Where[] =  "(" . $_SESSION[$module]["WHERE"] . ")";
    }
}

if ($_GET['overdue'] && !is_array($ModuleDefs[$module]['HARD_CODED_LISTINGS']))
{
    $overdueSQL = getOverdueSQL($module, $ListRef);
    $join = $overdueSQL['join'];
    if ($overdueSQL['where'])
    {
        $Where[] = $overdueSQL['where'];
    }
}

//FIXME: This conflicts with hard coded listings that happen to be called "all". Hence the exception for HOT
if ($ListType == 'all' && $module != 'HOT')
{
    $_SESSION[$module]["WHERE"] = '1=1';
    unset($_SESSION[$module]['FILTER']);
}

if (isset($_GET['btn']) && $_GET['btn'] == 'create' && !isset($Where))
{
    $ListWhereClause = '';
}
else
{
    $ListWhereClause = MakeSecurityWhereClause($Where, $module, $_SESSION["initials"]);
}

// module-specific listing-only where clause
if (isset($ModuleDefs[$module]['LISTING_WHERE']))
{
    if ($ListWhereClause != '')
    {
        $ListWhereClause .= ' AND '.$ModuleDefs[$module]['LISTING_WHERE'];
    }
    else
    {
        if (isset($_GET['btn']) && $_GET['btn'] == 'create' && $ListWhereClause == '')
        {
            $ListWhereClause = '';
        }
        else
        {
            $ListWhereClause.= $ModuleDefs[$module]['LISTING_WHERE'];
        }
    }
}

if ($ModuleDefs[$module]['LISTING_FILTERS'] && !empty($_SESSION[$module]['FILTER']) &&
    !empty($_SESSION[$module]['WHERE']) && !$_REQUEST["drillwhere"] && $DrillIntoReport === false)
{
    $ListWhereClause.= ' AND ' . $_SESSION[$module]['WHERE'];
}

// ==========================================================================//
// Image file to display at the top left of the form                         //
// ==========================================================================//
$img = 'Images/'.$ModuleDefs[$module]['ICON'];

// ==========================================================================//
// Build the URL to be used when clicking on a record and column headers     //
// ==========================================================================//
if (isset($ListRef))
{
    $timescales_url = "$scripturl?action=list&amp;module=$module&amp;listref=$ListRef";
}
// linking from a graphic report
elseif ($_REQUEST["rows"] != '')
{
    $timescales_url = "$scripturl?". htmlspecialchars($_SERVER[QUERY_STRING]);
}
else
{
    $timescales_url = "$scripturl?action=list&amp;module=$module&amp;listtype=$ListType";
}
if ($_GET['fromcrosstab'] == '1')
{
    $timescales_url .= '&amp;fromcrosstab=1&amp;row='.$_GET['row'].'&amp;col='.$_GET['col'];
}

//Add accreditation-specific url parameters
if ($_GET['creatingassignments'])
{
    $timescales_url .= '&creatingassignments='.$_GET['creatingassignments'];
}
if ($_GET['adm_year'])
{
    $timescales_url .= '&adm_year='.$_GET['adm_year'];
}
if ($_GET['sidemenu'])
{
    $timescales_url .= '&sidemenu='.$_GET['sidemenu'];
}

$record_url = "$scripturl?action=$ActionType&module=$module&". $record_url_part;

// ==========================================================================//
// Columns to be displayed                                                   //
// ==========================================================================//

require_once 'Source/libs/ListingClass.php';
$list_columns = Listings_ModuleListingDesign::GetListingColumns($module);

//==========================================================================//
// Prepare SQL Statement to be executed by BrowseList.php                   //
//==========================================================================//

if (is_array($list_columns))
{
     foreach ($list_columns as $col_name => $col_info)
     {
         if (is_array($list_columns_extra[$col_name]))
         {
             if (!array_key_exists("condition", $list_columns_extra[$col_name]) || $list_columns_extra[$col_name]["condition"])
             {
                 $selectfields[$col_name] = $col_name;
             }
             else
             {
                 unset($list_columns[$col_name]);
             }
             if (array_key_exists("prefix", $list_columns_extra[$col_name]))
             {
                 $col_info_extra['prefix'] = $list_columns_extra[$col_name]["prefix"];
             }
             if (array_key_exists("dataalign", $list_columns_extra[$col_name]))
             {
                 $col_info_extra['dataalign'] = $list_columns_extra[$col_name]["dataalign"];
             }
             if (array_key_exists("colrename", $list_columns_extra[$col_name]))
             {
                 $col_info_extra['colrename'] = $list_columns_extra[$col_name]["colrename"];
             }
         }
         else
         {
             $selectfields[$col_name] = $col_name;
         }
     }
    //$selectfields[] = "ram_rating";
   // $selectfields[] = "ram_level";
}

if ($ModuleDefs[$module]['OVERDUE_CHECK_FIELD'])
{
    $selectfields[$ModuleDefs[$module]['OVERDUE_CHECK_FIELD']] = $ModuleDefs[$module]['OVERDUE_CHECK_FIELD'];
}
if (!$ModuleDefs[$module]['NO_REP_APPROVED'])
{
    $selectfields['rep_approved'] = 'rep_approved';
}
if (empty($selectfields))
{
    $selectfields['recordid'] = 'recordid';
}

$list_request = GetSQLResultsTimeScale($selectfields, ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']),
            $order, $orderby, $ListWhereClause, $listnum, $listdisplay, $listnum2, $listnumall, $sqla, $join, $module);
