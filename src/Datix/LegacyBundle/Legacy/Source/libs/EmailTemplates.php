<?php

require_once 'EmailTemplateClass.php';
/*
 * EditTemplate provides the form for both creating new and editing existing templates.
 *
 * if $_GET[recordid] is blank, the page will act as a 'new template' form
 */
function EditTemplate()
{
    global $scripturl, $ModuleDefs, $txt, $TableDefs, $MinifierDisabled;

    LoggedIn();

    if (!IsFullAdmin())
    {
        $yySetLocation = $scripturl;
        redirectexit();
    }

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $recordid = Sanitize::SanitizeInt($_GET["recordid"]);

    $sql = 'SELECT emt_module, emt_name, emt_type, emt_notes, emt_author,
                emt_subject, emt_text, emt_content_type FROM EMAIL_TEMPLATES WHERE recordid = :recordid';

    $row = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

    if ($row["emt_author"] && $_SESSION["initials"]!=$row["emt_author"] && $row["emt_author"]!='Datix')
    {
        $FormMode = "Print";
    }

    $row["emt_subject"] = StripXMLTagWrapper($row["emt_subject"]);
    $row["emt_text"] = StripXMLTagWrapper($row["emt_text"]);

    $TableObj = new FormTable();

    $TableObj->MakeTitleRow('<b>'.($recordid?"Edit Email Template":"New Email Template").'</b>');

    if ($error)
    {
        $CTable->Contents .= '<font color="red"><b>' . $error["message"] . '</b></font><br /><br />';
    }

    $FieldObj = new FormField($FormMode);

    $FieldObj->MakeCustomField($recordid);
    $TableObj->MakeRow("$error[sq_name]<b>Template ID</b>", $FieldObj);

    $ModArray = array_intersect(array('INC' => $txt["mod_incidents_title"], 'HOT' => $txt["mod_hotspots_title"], 'ACT' => $txt["mod_actions_title"]), getModArray()); //"RAM"=>"Risk Register", "SAB"=>"Safety Alerts", "ACT" => "Actions" );

    if ($row["emt_module"])
    {
        $FieldObj->MakeCustomField($row["emt_module"].'<input type="hidden" id="emt_module" name="emt_module" value="'.$row["emt_module"].'">');
    }
    else
    {
        $FieldObj = Forms_SelectFieldFactory::createSelectField('emt_module', '', $row["emt_module"], $FormMode);
        $FieldObj->setCustomCodes($ModArray);
        $FieldObj->setOnChangeExtra('filterTemplateTypes("emt_module");');
    }

    $TableObj->MakeRow("$error[sq_name]".'<img src="images/Warning.gif" alt="Mandatory"> <b>Module</b>', $FieldObj);

    $FieldObj = new FormField($FormMode);

    if ($row["emt_type"])
    {
        $TypeArray = include_config_array();
        $FieldObj->MakeCustomField($TypeArray[$row["emt_module"]][$row["emt_type"]]["title"].'<input type="hidden" id="emt_type" name="emt_type" value="'.$row["emt_type"].'">');
    }
    elseif ($row["emt_module"])
    {
        $FieldObj->MakeCustomField('<div id="emt_type_div">'.
            $this->call('src\emailtemplates\controllers\EmailTemplateTypeController', 'gettemplatetypes', array('module' => $row['emt_module']))
            .'</div>');
    }
    else
    {
        $FieldObj->MakeCustomField('<div id="emt_type_div"><select style="width:200px" disabled="true"></select></div>');
    }

    $TableObj->MakeRow("$error[sq_name]".'<img src="images/Warning.gif" alt="Mandatory"> <b>Template Type</b>', $FieldObj);

    $FieldObj->MakeInputField('emt_name', 50, 50, $row["emt_name"], "");
    $TableObj->MakeRow($error["sq_name"].'<img src="images/Warning.gif" alt="Mandatory"> <b>Template Name</b>', $FieldObj);

    $FieldObj->MakeTextAreaField('emt_notes', 3, 70, 0, $row["emt_notes"], false);
    $TableObj->MakeRow("$error[sq_name]<b>Template Description</b>", $FieldObj);

    $FieldObj->MakeCustomField(($row["emt_author"]?$row["emt_author"]:$_SESSION["fullname"]));
    $TableObj->MakeRow("$error[sq_name]<b>Template Author</b>", $FieldObj);

    $TableObj->MakeTitleRow('<b>Email Contents</b>');

    $FieldObj->MakeTextAreaField('emt_subject', 2, 70, 0, $row["emt_subject"], false);
    $TableObj->MakeRow("$error[sq_name]<b>Email Subject</b>", $FieldObj);

    $FieldObj->MakeTextAreaField('emt_text', 8, 70, 0, $row["emt_text"], false);
    $TableObj->MakeRow("$error[sq_name]<b>Email Body</b>", $FieldObj);

    $FieldObj->MakeCustomField('<div id="emt_type_div">'.getContentTypes($row["emt_content_type"])."</div>");
    $TableObj->MakeRow("$error[sq_name]<b>Content type</b>", $FieldObj);

    require_once 'Source/TableDefs.php';

    $TableObj->MakeTable();

    $dtxtitle = "Email Templates";

    getPageTitleHTML(array(
         'title' => $dtxtitle,
         'module' => 'ADM'
     ));

    GetSideMenuHTML(array(
        'module' => 'ADM',
        'menu_array' => GetMenuArray()
    ));

    template_header();

    echo "<script language='javascript' src='js_functions/email' . $addMinExtension . '.js'></script>";

    echo '<form enctype="multipart/form-data" method="post" name="frmDocument" id="frmDocument" action="'.$scripturl.'?action=emailtemplateoptions&recordid='.$recordid.'">
    <input type="hidden" name="module" value="'.$module.'" />
    <input type="hidden" name="rbWhat" value="Save" />
    <input type="hidden" name="recordid" value="'. Sanitize::SanitizeInt($_GET["recordid"]).'" />';

    echo $TableObj->GetFormTable();

    echo '<div class="button_wrapper">';

    echo '<input type="button" value="Save" onclick="Javascript:document.frmDocument.rbWhat.value=\'Save\';if(emt_validateOnSubmit()){document.forms[0].submit()}" />';

    if ($recordid)
    {
        echo '<input type="button" value="Delete" onclick="if(confirm(\'' . $txt["delete_confirm"] . '\')){document.frmDocument.rbWhat.value=\'Delete\';submitClicked = false;this.form.submit()}" />';
    }

    echo '<input type="submit" value="'._tk('btn_cancel').'" onclick="Javascript:document.frmDocument.rbWhat.value=\'Cancel\'" />';

    echo '
                        </div>
    </form>';

    footer();

    obExit();
}


function GetMenuArray()
{
    return array(
        array('label' => 'New Template', 'link' => 'action=emailtemplate'),
        array('label' => 'List All Templates', 'link' => 'action=listemailtemplates'),
        array('label' => 'Configure Email Templates', 'link' => 'action=configureemailtemplates'),
    );
}

/*
 * DeleteTemplate deletes a template record from the database.
 */
function DeleteTemplate($paramArray)
{
    global $scripturl, $yySetLocation;

    $recordid = $paramArray["recordid"];

    $sql = "DELETE FROM EMAIL_TEMPLATES WHERE recordid=$recordid";
    DatixDBQuery::PDO_query($sql);

    AddSessionMessage('INFO', _tk("email_template_deleted"));

    $yySetLocation = "$scripturl?action=listemailtemplates";
    redirectexit();
}

 /**
 * Saves an e-mail template record to the database or updates an existing record.
 *
 * @global array
 * @global string
 * @global string
 *
 * @param $paramArray["recordid"]               The template id.
 * @param $paramArray["posted"]["emt_module"]   The module code.
 * @param $paramArray["posted"]["emt_type"]     The template type.
 * @param $paramArray["posted"]["emt_notes"]    The template description.
 * @param $paramArray["posted"]["emt_subject"]  The e-mail subject.
 * @param $paramArray["posted"]["emt_text"]     The e-mail body.
 * @param $paramArray["posted"]["updateid"]     The previous update id for the record.
 * @param $_SESSION[initials]                   The initials of the current user.
 */
function SaveTemplate($paramArray)
{
    global $scripturl, $yySetLocation;

    $recordid = $paramArray["recordid"];
    $posted = $paramArray["posted"];

    $posted['emt_subject'] = AddXMLTagWrapper($posted['emt_subject']);
    $posted['emt_text'] = AddXMLTagWrapper($posted['emt_text']);

    if ($recordid)
    {
        $sql = 'UPDATE EMAIL_TEMPLATES SET
        emt_module = :emt_module,
        emt_name = :emt_name,
        emt_type = :emt_type,
        emt_notes = :emt_notes,
        emt_subject = :emt_subject,
        emt_text = :emt_text,
        emt_content_type = :emt_content_type,
        updateid = :updateid,
        updatedby = :updatedby,
        updateddate = :updateddate
        WHERE recordid = :recordid';

        DatixDBQuery::PDO_query($sql, array(
            'emt_module' => $posted['emt_module'],
            'emt_name' => $posted['emt_name'],
            'emt_type' => $posted['emt_type'],
            'emt_notes' => $posted['emt_notes'],
            'emt_subject' => $posted['emt_subject'],
            'emt_text' => $posted['emt_text'],
            'emt_content_type' => $posted['emt_content_type'],
            'updateid' => GensUpdateID($posted['updateid']),
            'updatedby' => $_SESSION['initials'],
            'updateddate' => date('Y-m-d H:i:s'),
            'recordid' => $recordid,
        ));
    }
    else
    {
        $recordid = DatixDBQuery::PDO_build_and_insert('email_templates', 
            array(
                'emt_module' => $posted['emt_module'],
                'emt_name' => $posted['emt_name'],
                'emt_type' => $posted['emt_type'],
                'emt_notes' => $posted['emt_notes'],
                'emt_subject' => $posted['emt_subject'],
                'emt_text' => $posted['emt_text'],
                'emt_content_type' => $posted['emt_content_type'],
                'emt_author' => $_SESSION['initials'],
                'updateid' => GensUpdateID($posted['updateid']),
                'updatedby' => $_SESSION['initials'],
                'updateddate' => date('d-M-Y H:i:s'),
            )
        );
    }

    AddSessionMessage('INFO', 'Template has been saved successfully');

    // redirect back to display 'saved' message.
    $yySetLocation = "$scripturl?action=emailtemplate&pagetype=edittemplate&recordid=$recordid&saved=1";
    redirectexit();
}

/*
 * SaveTemplateOptions saves the template options (which template to use as default in which situation.
 */
function SaveTemplateOptions($parameterArray)
{
    global $scripturl, $yySetLocation;

    $posted = $parameterArray["posted"];

    $DefaultArray = include_config_array();

    foreach ($DefaultArray as $module => $GlobalArray)
    {
        foreach ($GlobalArray as $globalval => $aDetails)
        {
            $num = DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM GLOBALS WHERE parameter = :parameter', array('parameter' => $globalval), \PDO::FETCH_COLUMN);

            if ($num)
            {
                $sql = "UPDATE GLOBALS SET parmvalue = :parmvalue WHERE parameter = :parameter";
                DatixDBQuery::PDO_query($sql, array('parmvalue' => $posted[$globalval], 'parameter' => $globalval));
            }
            else
            {
                DatixDBQuery::PDO_build_and_insert('globals',
                    array(
                        'parameter' => $globalval,
                        'description' => $aDetails['title'],
                        'parmvalue' => $posted[$globalval],
                    )
                );
            }
        }
    }

    AddSessionMessage('INFO', _tk('email_template_configured'));

    // redirect back to display 'saved' message.
    $yySetLocation = "$scripturl?action=configureemailtemplates";
    redirectexit();
}

/*
 * getLinkToRecord returns the url of a particular record so that a user can include it in the email.
 */
function getLinkToRecord($parameterArray)
{
    global $scripturl, $ModuleDefs;

    $recordid = $parameterArray['recordid'];
    $module = $parameterArray['module'];
    $table = $parameterArray['table'];

    return "$scripturl?".($table=='main'?$ModuleDefs[$module]["MAIN_URL"]:$ModuleDefs[$module]["HOLDING_URL"])."&recordid=$recordid";
}

function include_old_config_array()
{
    // config type = 'GEN' (general), 'DEF' (default) and 'AUTO' (automatic)
    // DefaultArray[module][config_type][global] = description
    return array(
        "ALL" => array("GEN" => array("EMAIL_TEMPLATES" => "Activate Email Templates")),
        "INC" => array(
            "DEF" => array("EMT_INC_FBK" => "Incident Feedback Template"),
            "AUTO" => array(
                "EMT_INC_ACK" => "Acknowledgment of incident report submission",
                "EMT_INC_NOT" => "Notification of incident report submission",
            )
        ),
        "RAM" => array(
            "AUTO" => array(
                "EMT_RAM_ACK" => "Acknowledgment of risk report submission",
                "EMT_RAM_NOT" => "Notification of risk report submission",
            )
        ),
        "SAB" => array(
            "DEF" => array(
                "EMT_SAB_NOT" => "Safety Alerts Notification Template",
                "EMT_SAB_REM" => "Safety Alerts Reminder Template"
            )
        ),
        "ACT" => array("DEF" => array("EMT_ACT_NOT" => "Action Notification Template"))
    );
}

/*
 * include_config_array returns an array of globals (so new ones only need to be added here to appear everywhere).
 */
function include_config_array()
{
    return array(
        "INC" => array(
            "EMT_INC_OVD" => array("title" => "Overdue "._tk('INCName')." notification", "type" => "AUTO", "history_map" => "Overdue"),
            "EMT_INC_PRO" => array("title" => "Reporter progress notification", "type" => "AUTO", "history_map" => "REPPROGRESS")
        ),
        "COM" => array(
            "EMT_COM_OVD" => array("title" => "Overdue "._tk('COMName')." notification", "type" => "AUTO", "history_map" => "Overdue"),
        ),
        "ACT" => array(
            "EMT_ACT_OVD" => array("title" => "Overdue Action notification", "type" => "AUTO", "history_map" => "OVD"),
            "EMT_ACT_REM" => array("title" => "Action reminder", "type" => "AUTO", "history_map" => "REM")
        ),
        'HOT' => array('EMT_HOT_NOT' => array('title' => 'Hotspot notification', 'type' => 'AUTO', "history_map" => "Notify"))
    );
}

function getContentTypes($value)
{
    $TypeArray = array("TEXT" => "TEXT", "HTML" => "HTML");

    if (!empty($TypeArray))
    {
        return ArrayToSelect(array('id' => 'emt_content_type', 'options' => $TypeArray, 'value' => $value));
    }
    else
    {
        return '<div id="emt_content_type"><select style="width:200px" disabled="true" /></div>';
    }
}