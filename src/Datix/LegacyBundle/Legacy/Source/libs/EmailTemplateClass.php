<?php

use src\email\EmailTemplateInterface;

class NewEmailTemplate implements EmailTemplateInterface
{
    var $TemplateID;    
    var $TemplateData;    
    var $RecordID;
    var $Module;
    
    var $RecordData;
    var $ExtraData;
    
    var $SubjectMatchArray = array();
    var $BodyMatchArray = array();
    
    var $EmailSubject;
    var $EmailBody;
    
    
    function NewEmailTemplate($TemplateID)
    {
        $this->TemplateID = $TemplateID;
        
        if($TemplateID)
        {            
            $sql = 'SELECT emt_module, emt_name, emt_type, emt_notes, emt_subject, emt_text, emt_content_type, emt_author
                        FROM email_templates WHERE recordid = :recordid';
                        
            $this->TemplateData = DatixDBQuery::PDO_fetch($sql, array('recordid' => $this->TemplateID));
            
            $this->Module = $this->TemplateData['emt_module'];
        }        
    }
    
    function DefineExtraData($data)
    {
        $this->ExtraData = $data;    
    }
    
    function FindDataPoints($text)
    {
        preg_match_all("/<datix [^>]*>/iu", $text, $Matches);
        
        $MatchArray = array();
        
        foreach($Matches[0] as $id => $Match)
        {
            $ParamList = array();
            preg_match_all("/([^ ><=\"]+)[ ]?=[ ]?\"([^ ><=\"]+)\"/iu", $Match, $Params);
            
            foreach($Params[0] as $paramId => $Param)
            {
                $ParamList[$Params[1][$paramId]] = $Params[2][$paramId];
            }       
                        
            $ParamList['MatchString'] = $Match;
            $MatchArray[$id] = $ParamList; 
        }
        
        return $MatchArray;        
    }
    
    function GetRecordData($RecordID)
    {
        global $ModuleDefs, $TableDefs;
        
        $this->RecordID = $RecordID;
        
        $FieldList = array();
        $ModuleList[] = $this->TemplateData['emt_module'];  
        $TableList = array(); 
        
        foreach($this->SubjectMatchArray as $Match)
        {
            $this->processMatch($Match, $ModuleList, $FieldList, $TableList, $WhereList); 
        }       
        
        foreach($this->BodyMatchArray as $Match)
        {
            $this->processMatch($Match, $ModuleList, $FieldList, $TableList, $WhereList); 
        }
                     
        if(!empty($FieldList))
        {
            array_unique($FieldList);

            $sql = 'SELECT '.implode(', ', $FieldList).' FROM ' .$ModuleDefs[$this->TemplateData['emt_module']]['TABLE'];
            
            //Include other tables and JOIN with LEFT OUTER JOIN to main table
            foreach($TableList as $i => $table)
            {
                if (!empty($WhereList[$i]))
                {
                     $sql .= ' LEFT OUTER JOIN ' . $table . " ON " .  $WhereList[$i];   
                }
            }
            
            $sql .= ' WHERE ' . $ModuleDefs[$this->TemplateData['emt_module']]['TABLE'] . '.recordid = '.$RecordID;            
              
            $data = DatixDBQuery::PDO_fetch($sql, array());            
        }
        
        return $data;
    }
    
    private function processMatch(&$Match, &$ModuleList, &$FieldList, &$TableList, &$WhereList)
    {
        global $ModuleDefs;
        
        require 'Source/TableDefs.php';
        
        if($Match['type'] == 'other')
        {
            return;   
        }
        
        if(!empty($Match['module']))
        {
            $ModuleList[] = $Match['module']; 
        }
   
        if($Match['field'])
        {
            $module = ($Match['module'] ? $Match['module'] : $this->TemplateData['emt_module']);

            //this all obviously needs to be replaced by FieldDef object when refactored.
            $TableAdded = false;
            if ($Match['field'] == 'recordid')
            {
                $Match['field'] = $ModuleDefs[$module]['TABLE'].'.recordid';
                $TableAdded = true;
            }

            $field_fmt = getFieldFormat($Match['field'], $module, $Match["table"]);
                      
            //Only include fields defined in field_formats
            if (!empty($field_fmt))
            {    
                $current_table = $field_fmt["fmt_table"];  
                
                if ($current_table != $ModuleDefs[$this->TemplateData['emt_module']]['TABLE'] && array_search($current_table, $TableList) === false )
                {
                    $LinkWhere = $TableDefs[$ModuleDefs[$this->TemplateData['emt_module']]['TABLE']]['LINKS_WHERE_CLAUSE'][$current_table];
                    //Only include field outside of main table if we have a valid Link WHERE clause defined, otherwise do nothing.
                    if(!empty($LinkWhere))
                    {
                        $TableList[] = $current_table;
                        $WhereList[] = $LinkWhere;
                        if ($TableAdded)
                        {
                            $FieldList[] =  $Match['field'];
                        }
                        else
                        {
                            $FieldList[] =  $current_table . "." . $Match['field'];
                        }
                    }
                }
                else
                {
                    if ($TableAdded)
                    {
                        $FieldList[] =  $Match['field'];
                    }
                    else
                    {
                        $FieldList[] =  $current_table . "." . $Match['field'];
                    }
                }
            }
        }   
    }
    
    function ConstructEmailToSend($RecordID)
    {
        $this->SubjectMatchArray = $this->FindDataPoints($this->TemplateData['emt_subject']);
        $this->BodyMatchArray = $this->FindDataPoints($this->TemplateData['emt_text']);
        
        if($RecordID)
        {
            $this->RecordData = $this->GetRecordData($RecordID);
        }
         
        $this->EmailSubject = $this->ReplaceDataPoints($this->TemplateData['emt_subject'], $this->SubjectMatchArray);
        $this->EmailBody = $this->ReplaceDataPoints($this->TemplateData['emt_text'], $this->BodyMatchArray);
        
        $this->EmailSubject = StripXMLTagWrapper($this->EmailSubject);
        $this->EmailBody = StripXMLTagWrapper($this->EmailBody);
    }
    
    
    function ReplaceDataPoints($text, $MatchArray)
    {        
        global $scripturl, $ModuleDefs, $FieldDefs;
        
        foreach($MatchArray as $Match)
        {
            switch($Match['type'])
            {
                case 'other': 
                    if($Match['other'])
                    {
                        switch($Match['other'])
                        {
                            case 'hyperlink':
                                $text = str_replace($Match['MatchString'], $scripturl.'?action='.$ModuleDefs[$this->Module]['ACTION'].'&recordid='.$this->RecordID , $text);
                            
                            break;
                            default:
                            
                                if($this->ExtraData[$Match['other']])
                                {
                                    $text = str_replace($Match['MatchString'], $this->ExtraData[$Match['other']] , $text);
                                }
                            
                        }
                    }
                    
                break;                
                
                case 'field':
                default:
                    if($Match['field'])
                    {
                        $Data = $this->RecordData[$Match['field']];    
                        $field_module = ($Match['module'] ? $Match['module'] : $this->Module);
                        if($FieldDefs[$field_module][$Match['field']]['Type'] == 'ff_select')
                        {
                            $Data = code_descr($field_module, $Match['field'], $Data, "", false);
                        }
                        else if ($FieldDefs[$field_module][$Match['field']]['Type'] == 'multilistbox')
                        {
                            $DataItems = array();
                            foreach (explode(' ', $Data) as $data_item)
                            {
                                $DataItems[] = code_descr($field_module, $Match['field'], $data_item, "", false);
                            }

                            $Data = implode(', ', $DataItems);
                        }
                        else if (in_array($FieldDefs[$field_module][$Match['field']]['Type'], array('yesno')))
                        {
                            $YNArray = Array('Y' => _tk('yes'), 'N' => _tk('no'));
                            $Data = $YNArray[$Data];
                        }
                        else if (in_array($FieldDefs[$field_module][$Match['field']]['Type'], array('date')))
                        {
                            $Data = FormatDateVal($Data);
                        }
                        else if (in_array($FieldDefs[$field_module][$Match['field']]['Type'], array('money')))
                        {
                            $Data = FormatMoneyVal($Data);
                        }

                        $text = str_replace($Match['MatchString'], $Data, $text);
                    }
            }
            
        }
        
        return $text;        
    } 
    
    public function getContentType()
    {
        return $this->TemplateData['emt_content_type'];
    }

    public function getBody($data)
    {
        $this->DefineExtraData($data);
        $this->ConstructEmailToSend($data["recordid"]);
        return $this->EmailBody;
    }

    public function getSubject($data)
    {
        $this->DefineExtraData($data);
        $this->ConstructEmailToSend($data["recordid"]);
        return $this->EmailSubject;
    }

    public function isHTMLEmail()
    {
        return ($this->TemplateData['emt_content_type'] == 'HTML' ? true : false);
    }
}