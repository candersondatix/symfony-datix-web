<?php
use src\reports\model\report\Report;
/**
* Gets content for Ajax code selection controls by retrieving info from database on-the-fly.
*
* Can be used to help resolve performance issues (mainly in IE) associated with
* building code selection controls from Javascript arrays containing a large number of codes.
*/
function getSelectFunction()
{
    echo formatSelectFunctionOutput();
}

/**
* Generates response output from data returned by select functions.
*
* Calls a specified function to retrieve the data, then creates either JSON (by default) or XML output.
*
* @param string $_POST['function']  The name of the function used to retrieve the codes.
* @param string $_POST['method']    'xml' if XML content desired.
*
* @return string $output The codes available for selection, in either JSON or XML format.
*/
function formatSelectFunctionOutput()
{
    $codes = $_POST['function']();
    if ($_POST['method'] == 'xml')
    {
        $output = getInitialXML();
        if (array_key_exists('!ERROR!', $codes))
        {
            // we are returning a custom error message
            $output .= '<error>' . $codes['!ERROR!'] . '</error>';
        }
        else
        {
            // return the codes
            foreach ($codes as $value => $description)
            {
                $output .= '<code>
                                <value>'.$value.'</value>
                                <description>'.$description.'</description>
                            </code>';
            }
        }
        $output .= '</codes>';
    }
    else
    {
        // we use json by default
        if ($_GET['responsetype'] == 'autocomplete')
        {
            if (empty($codes))
            {
                $code['value'] = '!NOCODES!';
                $code['description'] = 'No codes available';
                $output[] = $code;
            }
            else if (array_key_exists('!ERROR!', $codes))
            {
                // we are returning a custom error message
                $output['!ERROR!'] = $codes['!ERROR!'];
            }
            else
            {
                // return the codes
                foreach ($codes as $value => $description)
                {
                    $code = array();
                    $code['value'] = $value;
                    $code['description'] = $description;
                    $output[] = $code;
                }
            }
        }
        else
        {
            // old code can be removed when all selectFunction fields are v11 dropdowns
            $output = getInitialJson();
            if (array_key_exists('!ERROR!', $codes))
            {
                // we are returning a custom error message
                $output['!ERROR!'] = $codes['!ERROR!'];
            }
            else
            {
                // return the codes
                foreach ($codes as $value => $description)
                {
                    $code = array();
                    $code['value'] = $value;
                    $code['description'] = $description;
                    $output['code'][] = $code;
                }
            }
        }
        $output = json_encode($output);
    }
    return $output;
}

/**
* Creates the initial XML for the contents of a code selection control.
*
* @param string $_POST['ctrlidx']       The ID for this code selection control.
* @param string $_POST['title']         The title for this code selection control.
* @param string $_POST['searchmode']    'true' if the form is in search mode.
* @param string $_POST['multilistbox']  'true' if  the field is a multi select.
* @param string $_POST['freetext']      'freetext' if allows text to be typed in the control which is not a code in the list.
*
* @return string $xml The initial XML.
*/
function getInitialXML()
{
    $title = preg_replace('/&(?![A-Za-z]{0,7};)/u', '&amp;', $_POST['title']);
    $title = StripHTML(Sanitize::SanitizeRaw($title));

    $xml = '<codes>';
    $xml .= '<title>' . $title . '</title>';
    $xml .= '<ctrlidx>' . Sanitize::SanitizeRaw($_POST['ctrlidx']) . '</ctrlidx>';

    if ($_POST['searchmode'] == 'true' || $_POST['multilistbox'] == 'true')
    {
        $xml .= '<multiple>multiple</multiple>';
    }

    if ($_POST['searchmode'] == 'true')
    {
        $xml .= '<search>search</search>';
    }

    if ($_POST['freetext'] == 'freetext')
    {
        $xml .= '<freetext>freetext</freetext>';
    }

    if ($_POST['searchmode'] != 'true' && $_POST['multilistbox'] != 'true')
    {
        $xml .= '<code><value/><description/></code>';
    }

    return $xml;
}

/**
* Creates the initial JSON data for the contents of a code selection control.
*
* @param string $_POST['ctrlidx']       The ID for this code selection control.
* @param string $_POST['title']         The title for this code selection control.
* @param string $_POST['searchmode']    'true' if the form is in search mode.
* @param string $_POST['multilistbox']  'true' if  the field is a multi select.
* @param string $_POST['freetext']      'freetext' if allows text to be typed in the control which is not a code in the list.
*
* @return string $json The initial JSON data.
*/
function getInitialJson()
{
    $title = preg_replace('/&(?![A-Za-z]{0,7};)/u', '&amp;', $_POST['title']);
    $title = StripHTML($title);

    $json = array();
    $json['title'] = $title;
    $json['ctrlidx'] = $_POST['ctrlidx'];

    if ($_POST['searchmode'] == 'true' || $_POST['multilistbox'] == 'true')
    {
        $json['multiple'] = true;
    }

    if ($_POST['searchmode'] == 'true')
    {
        $json['search'] = true;
    }

    if ($_POST['freetext'] == 'freetext')
    {
        $json['freetext'] = true;
    }

    if ($_POST['searchmode'] != 'true' && $_POST['multilistbox'] != 'true')
    {
        $json['code'][] = array('value' => '', 'description' => '');
    }

    return $json;
}

/**
* Creates a custom error which can be passed back to the code control.
*
* @param string $errorMessage The error message to be displayed.
*
* @return array $error        The representation of the error.
*/
function createCustomError($errorMessage)
{
    $error = array('!ERROR!' => $errorMessage);
    return $error;
}

/**
* Gets a list of security groups not currently assigned to this profile/user.
*
* @param array   $_POST['current']  The values currently selected.
* @param string  $_POST['pfl_id']   The profile ID.
* @param string  $_POST['con_id']   The contact ID.
*
* @return array $codes A list of security groups IDs/descriptions.
*/
function getSecurityGroups()
{
    $codes = array();
    $current = '';
    $term = '';
    $bindArray = array();

    if (isset($_POST['current']) && is_array($_POST['current']))
    {
        $i = 0;
        $current = 'AND recordid NOT IN (';
        foreach ($_POST['current'] as $value)
        {
            $current .= ':current'.$i.',';
            $bindArray['current'.$i] = $value;
            $i++;
        }
        $current = \UnicodeString::substr($current, 0, -1) . ')';
    }

    if ($_POST['term'])
    {
        $term = 'AND grp_code LIKE :term';
        $bindArray['term'] = '%'.$_POST['term'].'%';
    }

    if($_POST['pfl_id'])
    {
        $sql = "SELECT recordid, grp_code
                FROM sec_groups
                WHERE recordid NOT IN (
                    SELECT lpg_group
                    FROM link_profile_group
                    WHERE lpg_profile = :lpg_profile)
                $current
                $term
                ORDER BY grp_code";
        $bindArray['lpg_profile'] = $_POST['pfl_id'];
    }
    else if($_POST['con_id'])
    {
        $sql = "SELECT recordid, grp_code
                FROM sec_groups
                WHERE recordid NOT IN (
                    SELECT grp_id
                    FROM sec_staff_group
                    WHERE con_id = :con_id)
                $current
                $term
                ORDER BY grp_code";
        $bindArray['con_id'] = $_POST['con_id'];
    }
    else //no profile or contact specified - just return all groups
    {
        $sql = "SELECT recordid, grp_code
                FROM sec_groups
                WHERE (1=1)
                $current
                $term
                ORDER BY grp_code";
    }

    $codes = PDO_fetch_all($sql, $bindArray, PDO::FETCH_KEY_PAIR);
    $codes = array_map('htmlfriendly', $codes);

    if (empty($codes))
    {
        $codes['!NOCODES!'] = 'No security groups available';
    }

    return $codes;
}

/**
* Gets a list of managers for DIF1 'Your manager' field.
*
* @global array $DefaultValues
* @global array $HideFields
* @global array $ModuleDefs
* @global array $txt
*
* @return array $codes A list of managers' initials/names.
*/
function getDIF1Managers()
{
    global $DefaultValues, $HideFields, $ModuleDefs, $txt;

    $codes = array();

    // Form ID either comes through GET or is set in the session or by the default form design in the app.
    if (isset($_REQUEST['form_id']) && is_numeric($_REQUEST['form_id']))
    {
        $FormID = $_REQUEST['form_id'];
        $_SESSION['form_id']['INC'][1] = $FormID;
    }
    else if(isset($_SESSION['form_id']['INC'][1]) && is_numeric($_SESSION['form_id']['INC'][1]))
    {
        $FormID = $_SESSION['form_id']['INC'][1];
    }
    else
    {
        $FormID = Forms_FormDesign::getGlobalFormDesignID('INC', 1);
    }

    // This code is repeated a lot.  Maybe needs a function
    IncludeCurrentFormDesign('INC', 1, $FormID);

    $today = getdate();
    $todayStr = "$today[year]-$today[mon]-$today[mday] 00:00:00";
    $xml = getInitialXML();

    $Where = array("con_email IS NOT NULL AND con.login IS NOT NULL AND con_dclosed is null AND initials IS NOT NULL AND initials != ''  AND (con_staff_include IS NULL OR con_staff_include != 'N') AND (sta_daccessend = '' OR sta_daccessend IS NULL OR sta_daccessend >= '" . $todayStr . "')");

    $sql = GetContactListSQLByAccessLevel(array(
            'module' => 'INC',
            'where' => $Where,
            'levels' => array('DIF2', 'RM')
            ));

    $result = PDO_fetch_all($sql);
    foreach ($result as $row)
    {
        $ManagerName = FormatContactNameForList(array('data' => $row));
        if ($_POST['term'] == '' || \UnicodeString::stripos($ManagerName, $_POST['term']) !== false || (bYN(GetParm('WEB_SHOW_CODE', 'N')) && \UnicodeString::strpos($row['initials'], $_POST['term']) !== false))
        {
            $codes[$row['initials']] = htmlfriendly($ManagerName);
        }
    }
    return $codes;
}

/**
* Fetches a list of date fields that can be used to create a hotspot agent,
* based on the currently selected module.
*
* @global array  $FieldDefs
* @global array  $ModuleDefs
*
* @param  string $_POST['parent_value'][0]  The module currently selected on the form.
*
* @return array  $codes                     A list of the module's date fields.
*/
function getHsaBasedOnDate()
{
    global $FieldDefs, $ModuleDefs;

    $codes = array();

    if (empty($_POST['parent_value'][0]))
    {
        // pass custom error message back to code control
        $codes = createCustomError('Please select a Module.');
    }
    else
    {
        // we could have more that one module on the form if in search mode
        $modules = explode('|', $_POST['parent_value'][0]);

        foreach ($modules as $module)
        {
            // get date fields from the module's field array definition
            foreach ($ModuleDefs[$module]['FIELD_ARRAY'] as $field)
            {
                if ($FieldDefs[$module][$field]['Type'] == 'date')
                {
                    $label = GetFieldLabel($field);
                    if ($_POST['term'] == '' || (\UnicodeString::strpos($field, $_POST['term']) !== false || \UnicodeString::strpos($label, $_POST['term']) !== false))
                    {
                        $codes[$field] = GetFieldLabel($field);
                    }
                }
            }

            /* --------- Hotpots engine does not currently support UDFs --------- */
            /* NB - USE PDO INSTEAD IF REINSTATING.
            // get any additional UDF date fields
            $sql = 'SELECT DISTINCT f.recordid, f.fld_name
                    FROM udf_fields f
                    INNER JOIN udf_links l
                        ON l.field_id = f.recordid
                    INNER JOIN udf_groups g
                        ON l.group_id = g.recordid
                    WHERE g.mod_id = '. ModuleCodeToID($module) . '
                    AND f.fld_type = \'D\'';

            $request = db_query($sql);
            while ($row = db_fetch_array($request))
            {
                $codes['UDF_'.$row['recordid']] = htmlfriendly($row['fld_name']);
            }
            */
        }

        asort($codes);
    }

    if (empty($codes))
    {
        $codes['!NOCODES!'] = 'No fields available';
    }

    return $codes;
}

/**
* Retrieves list of fields that can be used for each condition on the
* Hotspots Agent form, based on the currently selected module.
*
* @global array  $ModuleDefs
*
* @param  string $_POST['parent_value'][0]  The module currently selected on the form.
*
* @return array  $codes                     A list of the module's fields.
*/
function getHsaFieldNames()
{
    global $ModuleDefs, $FieldDefs;

    $codes = array();

    if (empty($_POST['parent_value'][0]))
    {
        // pass custom error message back to code control
        $codes = createCustomError('Please select a Module.');
    }
    else
    {
        $validFieldTypes = array('ff_select', 'multilistbox', 'yesno');

        // get fields from the module's field array definition
        foreach ($ModuleDefs[$_POST['parent_value'][0]]['FIELD_ARRAY'] as $field)
        {
            if (in_array($FieldDefs[$_POST['parent_value'][0]][$field]['Type'], $validFieldTypes))
            {
                $codes[$field] = GetFieldLabel($field);
                if ($_POST['term'] != '')
                {
                    // check whether the label matches the search term
                    if (\UnicodeString::stripos($codes[$field], $_POST['term']) === false)
                    {
                        unset($codes[$field]);
                    }
                }
            }
        }

        /* --------- Hotpots engine does not currently support UDFs --------- */
        /* NB - USE PDO INSTEAD IF REINSTATING.
        // get any additional UDF fields
        $sql = 'SELECT DISTINCT f.recordid, f.fld_name
                FROM udf_fields f
                INNER JOIN udf_links l
                    ON l.field_id = f.recordid
                INNER JOIN udf_groups g
                    ON l.group_id = g.recordid
                WHERE g.mod_id = '. ModuleCodeToID($_POST['parent_value'][0]) .'
                AND f.fld_type in (\'C\',\'T\',\'Y\')';

        $request = db_query($sql);
        while ($row = db_fetch_array($request))
        {
            $codes['UDF_'.$row['recordid']] = htmlfriendly($row['fld_name']);
        }
        */
        asort($codes);
    }

    if (empty($codes))
    {
        $codes['!NOCODES!'] = 'No fields available';
    }

    return $codes;
}

/**
* Retrieves a list of codes that can be used as values for each condition
* on the Hotspots Agent form, based on the coded field currently selected for this
* condition.
*
* @param string $_POST['parent_value'][0]  The field this condition is based on.
* @param string $_POST['parent_value'][1]  The module currently selected on the form.
* @param string $_POST['formType']         The form mode (new/search/read-only etc).
* @param string $_POST['current']          The values currently selected.
*
* @return array $codes                     A list of code values for this conditions's Field.
*/
function getHsaFieldValues()
{
    global $ModuleDefs;
    
    if (empty($_POST['parent_value'][0]))
    {
        // pass custom error message back to code control
        $codes = createCustomError('Please select a Field.');
    }
    else
    {
        $udf = \UnicodeString::substr($_POST['parent_value'][0], 0, 4) == 'UDF_';
        $search = $_POST['formType'] == 'Search';

        if ($udf)
        {
            // need to retrieve the field type for UDFs to pass definition in correct format
            $fieldId = \UnicodeString::substr(Sanitize::SanitizeString($_POST['parent_value'][0]), 4);
            if (is_numeric($fieldId))
            {
                if ($fieldType = PDO_fetch('SELECT fld_type FROM udf_fields WHERE recordid = ' . $fieldId, array(), PDO::FETCH_COLUMN))
                {
                    $_POST['parent_value'][0] = 'UDF_' . $fieldType . '_X_' . $fieldId;
                }
            }
        }

        $FDR_info = new Fields_Field(Sanitize::SanitizeString($_POST['parent_value'][0]));

        // check for known non-standard codes
        switch ($FDR_info->getCodeTable())
        {
            case 'modules':
                $ExcludeArray = getExcludeArray('actions');
                $modules = getModArray($ExcludeArray);

                foreach ($modules as $value => $description)
                {
                    $result[] = array('value' => $value, 'description' => $description);        
                }

                break;
            
            default:
                // use standard code retrieval method
                require_once 'Source/libs/CodeSelectionCtrl.php';
                $bindArray = array();
                $sql = GetCodeListSQL(Sanitize::SanitizeString($_POST['parent_value'][1]), Sanitize::SanitizeString($_POST['parent_value'][0]), '', $search, $udf, false, '', $title, $bindArray, $_POST['term']);
                $result = PDO_fetch_all($sql, $bindArray);
                break;
        }
        
        foreach ($result as $row)
        {
            if (!is_array($_POST['current']) || !in_array($row['value'], $_POST['current']))
            {
                // exclude currently selected values
                $codes[$row['value']] = htmlfriendly($row['description']);
            }
        }
    }

    return $codes;
}

/**
* Retrieves a list of saved queries to populate the 'Query' field
* on the HSA form, based on the currently selected module.
*
* @param  string $_POST['parent_value'][0]  The module currently selected on the form.
*
* @return array  $codes                     A list of saved queries.
*/
function getHsaQuery()
{
    if (empty($_POST['parent_value'][0]))
    {
        // pass custom error message back to code control
        $codes = createCustomError('Please select a Module.');
    }
    else
    {
        // retrieve saved queries for this module
        require_once 'Source/libs/QueriesBase.php';
        $codes = get_saved_queries(Sanitize::SanitizeString($_POST['parent_value'][0]));
    }

    if (empty($codes))
    {
        $codes = array('!NOCODES!' => 'No saved queries to display');
    }

    return $codes;
}

/**
* Retrieves a list of users that can be added as recipients for hotspot alerts.
*
* NB This function may need to be amended when new staff dropdown functionality is checked in.
*
* @param  string $_POST['current']  The values currently selected.
*
* @return array  $codes             A list of available recipients.
*/
function getHsaRecipients()
{
    // use standard code retrieval method
    require_once 'Source/libs/CodeSelectionCtrl.php';
    $bindArray = array();
    $sql = GetCodeListSQL('HSA', 'hsr_users', '', false, false, false, '', $title, $bindArray, $_POST['term']);

    // add additional WHERE clause to retrieve only users with Hotspot access
    $where = ' AND (vw_staff_combos.recordid IN
                        (
                            (SELECT sec_staff_group.con_id
                             FROM sec_group_permissions
                             JOIN sec_staff_group ON sec_group_permissions.grp_id = sec_staff_group.grp_id
                             WHERE sec_group_permissions.item_code = \'HOT_PERMS\'
                             AND sec_group_permissions.perm_value in (\'HOT2_READ_ONLY\',\'HOT2\'))
                        UNION
                            (SELECT contacts_main.recordid
                             FROM contacts_main, profiles, link_profile_group
                             WHERE contacts_main.sta_profile = profiles.recordid AND profiles.recordid = lpg_profile AND lpg_group IN
                                (SELECT grp_id FROM sec_group_permissions WHERE item_code = \'HOT_PERMS\' AND sec_group_permissions.perm_value in (\'HOT2_READ_ONLY\',\'HOT2\'))
                            )
                        )
                        OR
                        (
                            (SELECT count(*)
                             FROM sec_staff_group l
                             JOIN sec_groups g on l.grp_id = g.recordid
                             WHERE l.con_id = vw_staff_combos.recordid AND g.grp_type = (g.grp_type | 0)
                            ) = 0
                            AND
                            vw_staff_combos.login in (SELECT user_parms.login
                                FROM user_parms
                                WHERE user_parms.parameter = \'HOT_PERMS\'
                                AND user_parms.parmvalue in (\'HOT2_READ_ONLY\',\'HOT2\'))
                        )
                   )';

    $sql = str_replace('recordid as [value]', 'vw_staff_combos.recordid as [value]', $sql);
    $sql = str_replace('(recordid', '(vw_staff_combos.recordid', $sql);
    $sql = preg_replace('/( ORDER BY.+$)/u', $where.'$1', $sql);

    $result = PDO_fetch_all($sql, $bindArray);
    foreach ($result as $row)
    {
        if (!is_array($_POST['current']) || !in_array($row['value'], $_POST['current']))
        {
            // exclude currently selected values
            $codes[$row['value']] = htmlfriendly($row['description']);
        }
    }

    if (empty($codes))
    {
        $codes = array('!NOCODES!' => 'No users available');
    }

    return $codes;
}

/**
* Retrieves a list of fields that can be used to populate the 'Field' field in Recipient Conditions sections
* on the HSA form, based on the fields currently selected in the Criteria section.
*
* @param string $_POST['parent_value'][0]  A list of the Criteria section Fields currently selected on the form.
*
* @return array $codes                     A list of code values for this conditions's Field.
*/
function getHsaRecipientFieldNames()
{
    $fields = array();
    
    // create a list of fields currently selected in the Criteria section
    if ($_POST['parent_value'][0] != '')
    {
        $fields = explode(',', $_POST['parent_value'][0]);   
    }
    
    // location fields can also be used, if they have a current value
    if ($_POST['locationFieldValues']['hsa_use_loc_for_crit'] == 'Y' && $_POST['locationFieldValues']['hsa_module'] != '')
    {
        $module = $_POST['locationFieldValues']['hsa_module'];
        unset($_POST['locationFieldValues']['hsa_use_loc_for_crit']);
        unset($_POST['locationFieldValues']['hsa_module']);
        
        foreach ($_POST['locationFieldValues'] as $field => $value)
        {
            if ($value != '')
            {
                // map the location field to the equivalent in the selected module
                $fieldObj = new Fields_Field($field);
                $mappedField = $fieldObj->getFieldMapping($module);
                
                if ($mappedField != '')
                {
                    $fields[] = $mappedField;    
                }     
            }    
        }   
    }   
    
    if (empty($fields))
    {
        // pass custom error message back to code control
        $codes = createCustomError('Please select at least one Condition in the Criteria section.');
    }
    else
    {
        foreach ($fields as $field)
        {
            $label = GetFieldLabel($field);
            if ($_POST['term'] == '' || (\UnicodeString::strpos($field, $_POST['term']) !== false || \UnicodeString::strpos($label, $_POST['term']) !== false))
            {
                $codes[$field] = htmlfriendly(GetFieldLabel($field));
            }
        }
    }

    if (empty($codes))
    {
        $codes['!NOCODES!'] = 'No fields available';
    }

    asort($codes);
    return $codes;
}

/**
* Retrieves a list of codes that can be used as values for each Recipient Condition
* on the Hotspots Agent form, based on the values currently selected for this field in the Criteria section.
*
* @global array  $ModuleDefs
*
* @param  string $_POST['parent_value'][0]  The field this condition is based on.
* @param  string $_POST['parent_value'][1]  The Condition Field Values currently selected on the form.
* @param  string $_POST['current']          The values currently selected.
*
* @return array  $codes                     A list of code values for this conditions's Field.
*/
function getHsaRecipientFieldValues()
{
    global $ModuleDefs;

    $codes = array();

    if (empty($_POST['parent_value'][0]))
    {
        // pass custom error message back to code control
        $codes = createCustomError('Please select a Field.');
    }
    else
    {
        // current condition values
        $values = explode(',', $_POST['parent_value'][1]);
        
        // also add any current location values if using as conditions
        if ($_POST['locationFieldValues']['hsa_use_loc_for_crit'] == 'Y' && $_POST['locationFieldValues']['hsa_module'] != '')
        {
            $module = $_POST['locationFieldValues']['hsa_module'];
            unset($_POST['locationFieldValues']['hsa_use_loc_for_crit']);
            unset($_POST['locationFieldValues']['hsa_module']);
            
            foreach ($_POST['locationFieldValues'] as $locField => $locValue)
            {
                if ($locValue != '')
                {
                    $fieldObj = new Fields_Field($locField);
                    $mappedField = $fieldObj->getFieldMapping($module);
                    if ($mappedField != '')
                    {
                        $value = $module.'*'.$mappedField.'*'.$locValue;
                        if (!in_array($value, $values))
                        {
                            $values[] = $value;    
                        }   
                    }        
                }       
            }       
        }
        
        foreach ($values as $value)
        {
            // current condition values are stored in the format module*field_name*value
            $valueParts = explode('*', $value);
            if ($valueParts[1] == $_POST['parent_value'][0] && (!is_array($_POST['current']) || !in_array($valueParts[2], $_POST['current'])))
            {
                // this value is currently selected in the criteria section, so needs to be available here
                if (\UnicodeString::substr($valueParts[1], 0, 3) == 'UDF')
                {
                    $udfParts = explode('_', $valueParts[1]);
                    $description = code_descr_udf($udfParts[1], $valueParts[2], $valueParts[0]);
                    if ($_POST['term'] == '' || \UnicodeString::strpos($description, $_POST['term']) !== false || \UnicodeString::strpos($valueParts[2], $_POST['term']) !== false)
                    {
                        $codes[$valueParts[2]] = $description;
                    }
                }
                else
                {
                    $description = code_descr($valueParts[0], $valueParts[1], $valueParts[2]);
                    if ($_POST['term'] == '' || \UnicodeString::strpos($description, $_POST['term']) !== false || \UnicodeString::strpos($valueParts[2], $_POST['term']) !== false)
                    {
                        $codes[$valueParts[2]] = $description;
                    }
                }
            }
        }

        /*
        REMOVED FOR FIRST RELEASE

        // add @user_con options for location fields
        if ($_POST['parent_value'][1] != '' && is_array($ModuleDefs[$valueParts[0]]['STAFF_EMPL_FILTER_MAPPINGS']) && array_key_exists($_POST['parent_value'][0], $ModuleDefs[$valueParts[0]]['STAFF_EMPL_FILTER_MAPPINGS']))
        {
            $atCode = '@user_' . $ModuleDefs[$valueParts[0]]['STAFF_EMPL_FILTER_MAPPINGS'][$_POST['parent_value'][0]];
            $codes[$atCode] = $atCode;
        }
        */
    }
    asort($codes);

    return $codes;
}

/**
* Retrieves a list of e-mail templates for a given module.
*
* @param  string $_GET['module'] The module we're interested in.
*
* @return array  $codes          A list of e-mail templates.
*/
function getEmailTemplates()
{
    $codes = array();
    $params = array();

    $sql = 'SELECT recordid, emt_name FROM email_templates WHERE emt_module = :module';
    $params['module'] = $_POST['module'];

    if ($_POST['term'])
    {
        $sql .= ' AND emt_name LIKE :term';
        $params['term'] = '%'.$_POST['term'].'%';
    }

    $sql .=' ORDER BY emt_name';
    $codes = PDO_fetch_all($sql, $params, PDO::FETCH_KEY_PAIR);

    if (empty($codes))
    {
        $codes = array('!NOCODES!' => 'No templates available');
    }

    return $codes;
}

/**
* Retrieves codes for the consequence field on the standard grading section of the Risk form.
*
* @return array The code array.
*/
function getRamConsequence()
{
    $sql = 'SELECT code, description FROM ' . (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_conseq' : 'code_ra_conseq') . ' ORDER BY listorder';
    return PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);
}

/**
* Retrieves codes for the likelihood field on the standard grading section of the Risk form.
*
* @return array The code array.
*/
function getRamLikelihood()
{
    $sql = 'SELECT code, description FROM ' . (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_likeli' : 'code_ra_likeli') . ' ORDER BY listorder';
    return PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);
}

/**
* Returns a list of months for the ISD export.
* 
* @return array
*/
function getIsdMonth()
{
    return array_combine(
        array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'),    
        array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12')
    );    
}

/**
* Returns a list of years (from 5 years previous to 10 years ahead) for the ISD export.
* 
* @return array
*/
function getIsdYear()
{
    return array_combine(range(date('Y') - 5, date('Y') + 10), range(date('Y') - 5, date('Y') + 10));    
}

/**
* Retrieves list of fields from field directory
*
* @return array  $codes                     A list of fields.
*/
function getFieldDirectoryNames()
{
    global $FieldDefs;

    $codes = array();

    //$validFieldTypes = array('ff_select', 'multilistbox', 'yesno');
    $table = $_POST['parent_value'][0];
    if ($_POST['parent_value'][0] == '')
    {
        // pass custom error message back to code control
        $codes = createCustomError('Please select a table.');
    }
    else
    {

        $sql = "SELECT fdr_name, fdr_label FROM field_directory WHERE (fdr_name LIKE '%" . Sanitize::SanitizeString($_POST['term']) ."%' OR fdr_label LIKE '%" . Sanitize::SanitizeString($_POST['term']) . "%')
                AND fdr_table = :table";
        $PDOParams = array('table' => $table);
        
        $result = DatixDBQuery::PDO_fetch_all($sql, $PDOParams);
        // get fields from the module's field array definition
        foreach ($result as $field)
        {
              //  $codes[$field] = GetFieldLabel($field);
                $codes[$field['fdr_name']] = htmlfriendly($field['fdr_label']);
        }
    }
    asort($codes);

    if (empty($codes))
    {
        $codes['!NOCODES!'] = 'No fields available';
    }

    return $codes;
}

/**
* Retrieves list of table from table directory
*
* @return array  $codes                     A list of tables.
*/
function getTableDirectoryNames()
{
    global $FieldDefs;

    $codes = array();

    $sql = "SELECT tdr_name, tdr_desc FROM table_directory WHERE tdr_name LIKE '%" . Sanitize::SanitizeString($_POST['term']) ."%' OR tdr_desc LIKE '%" . Sanitize::SanitizeString($_POST['term']) . "%'";
    $PDOParams = array();
    
    $result = DatixDBQuery::PDO_fetch_all($sql, $PDOParams);
    // get fields from the module's field array definition
    foreach ($result as $field)
    {
          //  $codes[$field] = GetFieldLabel($field);
            $codes[$field['tdr_name']] = htmlfriendly($field['tdr_desc']);
    }

    asort($codes);

    if (empty($codes))
    {
        $codes['!NOCODES!'] = 'No tables available';
    }

    return $codes;
}

/**
* Returns a list of actions chains that can be assigned to a record.
* 
* @return array $chains
*/
function getActionChains()
{
    $where[] = 'ach_module IN (:module, \'ALL\')';
    $PDOParams['module'] = $_POST['module'];
    if ($_POST['term'])
    {
        $where[] = 'ach_title LIKE :term';
        $PDOParams['term'] = '%'.$_POST['term'].'%';   
    }
    $sql = 'SELECT recordid, ach_title FROM action_chains WHERE '.implode(' AND ', $where).' AND ach_archive = 0 ORDER BY ach_title';
    $result = DatixDBQuery::PDO_fetch_all($sql, $PDOParams);
    
    foreach ($result as $chain)
    {
            $chains[$chain['recordid']] = htmlfriendly($chain['ach_title']);
    }
    
    return $chains;   
}

use Source\classes\Filters\Container;

/**
* Handles retrieval of a list of corresponding values for the selected field on a given filter layer.
* 
* @global array $txt
* 
* @return array $codes
*/
function getFilterValues()
{
    global $txt;
    
    if ($_POST['fields'][$_POST['id']] == '' || $_POST['fields'][$_POST['id']] == $txt['filter_choose_field'])
    {
        $codes = createCustomError($txt['filter_select_field_error']);    
    }
    else
    {
        $layer = Container::getLayer($_POST['module'], $_POST['fields'][$_POST['id']]);
        foreach ($layer->getFilterValues($_POST['term']) as $row)
        {
            $codes[$row['value']] = htmlfriendly($row['description']);
        }
    }
    
    return $codes;    
}

/**
* Retrieves the location codes for the New Evidence Link (Manage CQC Links) screen. 
* 
* @return array $codes
*/
function getLocations()
{
    $codes = array();
    
    $secWhere = MakeSecurityWhereClause('', 'LOC', $_SESSION['initials']) ?: '1=1';
    
    $sql = 'SELECT 
                vw_locations_main.recordid, vw_locations_main.loc_name 
            FROM 
                vw_locations_main
            INNER JOIN
                cqc_outcomes ON cqc_outcomes.cdo_location = vw_locations_main.recordid
            WHERE 
                vw_locations_main.loc_name LIKE :term
            AND
                '.$secWhere.' 
            ORDER BY 
                vw_locations_main.loc_name';
    
    $locations = DatixDBQuery::PDO_fetch_all($sql, array('term' => '%'.$_POST['term'].'%'));
    foreach ($locations as $location)
    {
        $codes[$location['recordid']] = htmlfriendly($location['loc_name']);   
    }
    return $codes;    
}

/**
* Retrieves the outcome codes for the New Evidence Link (Manage CQC Links) screen.
* 
* @global array $txt
* 
* @return array $codes
*/
function getOutcomes()
{
    global $txt;
    
    $codes = array();
    
    $validLocations = array();
    $locations = explode('|', $_POST['parent_value'][0]);
    
    foreach ($locations as $location)
    {
        // sanitize parent (location) values
        if (ctype_digit($location))
        {
            $validLocations[] = $location;    
        }    
    }
    
    if (empty($validLocations))
    {
        // pass custom error message back to code control
        $codes = createCustomError($txt['cqc_new_link_select_loc']);
    }
    else
    {
        $secWhere = MakeSecurityWhereClause('', 'CQO', $_SESSION['initials']) ?: '1=1';
        
        $sql = 'SELECT
                    cqc_outcome_template_id, cto_ref + \': \' + cto_desc_short AS description
                FROM
                    cqc_outcomes
                WHERE
                    cdo_location IN ('.implode(',', $validLocations).')
                AND
                    cto_ref + \': \' + cto_desc_short LIKE :term
                AND
                    '.$secWhere.'
                GROUP BY
                    cqc_outcome_template_id, cto_ref + \': \' + cto_desc_short
                ORDER BY
                    cto_ref + \': \' + cto_desc_short';
                    
        $outcomes = DatixDBQuery::PDO_fetch_all($sql, array('term' => '%'.$_POST['term'].'%'));
        foreach ($outcomes as $outcome)
        {
            $codes[$outcome['cqc_outcome_template_id']] = htmlfriendly($outcome['description']);   
        }
        return $codes;   
    }
    
    return $codes;    
}

/**
* Retrieves the prompt codes for the New Evidence Link (Manage CQC Links) screen.
* 
* @global array $txt
* 
* @return array $codes
*/
function getPrompts()
{
    global $txt;
    
    $codes = array();
    
    $validLocations = array();
    $locations = explode('|', $_POST['parent_value'][0]);
    
    foreach ($locations as $location)
    {
        // sanitize parent (location) values
        if (ctype_digit($location))
        {
            $validLocations[] = $location;    
        }    
    }
    
    $validOutcomes = array();
    $outcomes = explode('|', $_POST['parent_value'][1]);
    
    foreach ($outcomes as $outcome)
    {
        // sanitize parent (outcome) values
        if (ctype_digit($outcome))
        {
            $validOutcomes[] = $outcome;    
        }    
    }
    
    if (empty($validLocations))
    {
        // pass custom error message back to code control
        $codes = createCustomError($txt['cqc_new_link_select_loc']);
    }
    else
    {
        $secWhere = MakeSecurityWhereClause('', 'CQP', $_SESSION['initials']) ?: '1=1';
        
        $sql = 'SELECT
                    cqc_prompt_template_id, ctp_ref + \': \' + ctp_desc_short AS description
                FROM
                    cqc_prompts
                WHERE
                    cdo_location IN ('.implode(',', $validLocations).')'.
                (!empty($validOutcomes) ? '
                AND
                    cqc_outcome_template_id IN ('.implode(',', $validOutcomes).')' : '').'
                AND
                    ctp_ref + \': \' + ctp_desc_short LIKE :term
                AND
                    '.$secWhere.'
                GROUP BY
                    cqc_prompt_template_id, ctp_ref + \': \' + ctp_desc_short
                ORDER BY
                    ctp_ref + \': \' + ctp_desc_short';
                    
        $prompts = DatixDBQuery::PDO_fetch_all($sql, array('term' => '%'.$_POST['term'].'%'));
        foreach ($prompts as $prompt)
        {
            $codes[$prompt['cqc_prompt_template_id']] = htmlfriendly($prompt['description']);   
        }
        return $codes;   
    }
    
    return $codes;    
}

/**
* Retrieves the sub-prompt codes for the New Evidence Link (Manage CQC Links) screen.
* 
* @global array $txt
* 
* @return array $codes
*/
function getSubprompts()
{
    global $txt;
    
    $codes = array();
    
    $validLocations = array();
    $locations = explode('|', $_POST['parent_value'][0]);
    
    foreach ($locations as $location)
    {
        // sanitize parent (location) values
        if (ctype_digit($location))
        {
            $validLocations[] = $location;    
        }    
    }
    
    $validOutcomes = array();
    $outcomes = explode('|', $_POST['parent_value'][1]);
    
    foreach ($outcomes as $outcome)
    {
        // sanitize parent (outcome) values
        if (ctype_digit($outcome))
        {
            $validOutcomes[] = $outcome;    
        }    
    }
    
    $validPrompts = array();
    $prompts = explode('|', $_POST['parent_value'][2]);
    
    foreach ($prompts as $prompt)
    {
        // sanitize parent (prompt) values
        if (ctype_digit($prompt))
        {
            $validPrompts[] = $prompt;    
        }    
    }
    
    if ($_POST['parent_value'][0] == '')
    {
        // pass custom error message back to code control
        $codes = createCustomError($txt['cqc_new_link_select_loc']);
    }
    else
    {
        $secWhere = MakeSecurityWhereClause('', 'CQS', $_SESSION['initials']) ?: '1=1';
        
        $sql = 'SELECT
                    cqc_subprompt_template_id, cts_ref + \': \' + cts_desc_short AS description
                FROM
                    cqc_subprompts
                WHERE
                    cdo_location IN ('.implode(',', $validLocations).')'.
                (!empty($validOutcomes) ? '
                AND
                    cqc_outcome_template_id IN ('.implode(',', $validOutcomes).')' : '').
                (!empty($validPrompts) ? '
                AND
                    cqc_prompt_template_id IN ('.implode(',', $validPrompts).')' : '').'
                AND
                    cts_ref + \': \' + cts_desc_short LIKE :term
                AND
                    '.$secWhere.'
                GROUP BY
                    cqc_subprompt_template_id, cts_ref + \': \' + cts_desc_short
                ORDER BY
                    cts_ref + \': \' + cts_desc_short';
                    
        $subprompts = DatixDBQuery::PDO_fetch_all($sql, array('term' => '%'.$_POST['term'].'%'));
        foreach ($subprompts as $subprompt)
        {
            $codes[$subprompt['cqc_subprompt_template_id']] = htmlfriendly($subprompt['description']);
        }
        return $codes;   
    }
    
    return $codes;
}

/**
 * Retrieves a list of fields that can be selected for a given table on the report designer.
 * 
 * @return array
 */
function getNewReportFields()
{
    global $ModuleDefs;

    list($fieldset, $contactType) = explode('-', $_POST['tableValues'][$_POST['table']]);
    $reportType = $_POST['reportType']['type'];
    $module = $_POST['module'];
    $showExcluded = ($_POST['showExcluded'] !== null ? $_POST['showExcluded'] : '0');
    $context = $_POST['context'];
    
    if ($fieldset == '' && $reportType != Report::TRAFFIC)
    {
        return createCustomError(_tk('reports_no_table_error'));
    }

    $queryFactory = new src\framework\query\QueryFactory();
    $query = $queryFactory->getQuery();

    if ($_POST['term'] != '')
    {
        $where = $queryFactory->getWhere();
        $where->add($queryFactory->getFieldCollection()->field('description')->like('%'.$_POST['term'].'%'));
        $query->where($where);
    }
    
    if ($reportType == Report::TRAFFIC)
    {
        $where = $queryFactory->getWhere();
        $where->add($queryFactory->getFieldCollection()->field('code')->in($ModuleDefs[$module]['TRAFFICLIGHTS_FIELDS']));
        $query->where($where);
    }
    
    $code_list = (new src\reports\model\field\ReportField($fieldset, $module, false, (($showExcluded == '1' && $context == 'administration') ? true : false)))->getCodes($query);

    foreach ($code_list as $code)
    {
        $codes[$code->code] = htmlspecialchars($code->description);
    }

    return $codes;
}

/**
 * Retrieves a list of fields that can be selected for drilling down on a crosstab.
 *
 * @return array
 */
function getDrilldownFields()
{
    global $ModuleDefs;

    $module       = $_POST['module'];
    $queryFactory = new src\framework\query\QueryFactory();
    $query        = $queryFactory->getQuery();
    $where        = $queryFactory->getWhere();

    if ($_POST['term'] != '')
    {
        $where->add($queryFactory->getFieldCollection()->field('description')->like('%'.$_POST['term'].'%'));
    }

    // Excludes fields used to design a crosstab from the drill down select box
    if ($_POST['rowsField'] != '' && $_POST['columnsField'] != '')
    {
        $where->add($queryFactory->getFieldCollection()->field('code')->notIn(array($_POST['rowsField'], $_POST['columnsField'])));
    }

    $where->add($queryFactory->getFieldCollection()->field('data_type')->in(array('C','Y','T')));
    $query->where($where);

    foreach ((new src\reports\model\field\ReportField(0, $module))->getCodes($query) as $code)
    {
        $codes[$code->code] = htmlspecialchars($code->description);
    }

    return $codes;
}

/**
 * Retrieves a list of respondents for a payment.
 *
 * @return array
 */
function getRespondentsForPayment()
{
    $cla_id = $_POST['cla_id'];

    $queryFactory = new src\framework\query\QueryFactory();
    $query = $queryFactory->getQuery();
    $query->where(array('cla_id' => $cla_id));

    if ($_POST['term'] != '')
    {
        $query->getWhere()->add($queryFactory->getFieldCollection()->field('name')->like('%'.$_POST['term'].'%'));
    }

    $code_list = (new src\payments\model\respondentfield\RespondentField())->getCodes($query);

    foreach ($code_list as $code)
    {
        $codes[$code->code] = htmlspecialchars($code->description);
    }

    return $codes;
}
//Selects the approval status with english.php codes for generating records from other modules
function getRelevantApprovalStatus()
{
    if (empty($_POST['parent_value'][0]))
    {
        $codes = createCustomError(_tk('generate_select_module'));
    }
    else
    {
        $codesList = GetLevelsTo($_POST['parent_value'][0], GetAccessLevel($_POST['parent_value'][0]), 'NEW');
        $codes = array();
        foreach ($codesList as $code => $desc)
        {
            $codes = array_merge($codes, array($code => $desc['description']));
        }
    }
    if (empty($codes))
    {
        $codes = array('!NOCODES!' => _tk('generate_select_module_no_perms'));
    }

    return $codes;
}

function getMapCodeFieldsList()
{
    global $ModuleDefs;

    $module = $_POST['module']['module'];

    $queryFactory = new src\framework\query\QueryFactory();
    $query = $queryFactory->getQuery();
    $where = $queryFactory->getWhere();

    if ($module != '')
    {
       $ModuleGroups = GetModuleGroups();

        // If this is a module group, we want to collect all sub-module fields.
        $setupModuleList = [];
        $setupTableList = [];

        if (in_array($module, array_keys($ModuleGroups)))
        {
            foreach (getModArray() as $subModule => $name)
            {
                if ($ModuleDefs[$subModule]['MODULE_GROUP'] == $module)
                {
                    $setupModuleList[] = $subModule;
                }
            }
        }
        else
        {
            $setupModuleList = [$module];
        }

        foreach ($setupModuleList as $setupModule)
        {
            $setupTableList[] = $ModuleDefs[$setupModule]['TABLE'];
        }

        $where->add($queryFactory->getFieldCollection()->field('fdr_table')->in($setupTableList));
    }

    $where->add($queryFactory->getFieldCollection()->field('fdr_data_type')->in(['C', 'T']));

    if ($_POST['term'] != '' && $module != '')
    {
        $where->add($queryFactory->getFieldCollection()->field('(SELECT TOP 1 fmt_new_label FROM vw_field_formats WHERE fdr_name = fmt_field AND fmt_module = \''.$module.'\')')->like('%'.$_POST['term'].'%'));
    }
    elseif ($_POST['term'] != '' && $module == '')
    {
        $where->add($queryFactory->getFieldCollection()->field('(SELECT TOP 1 fmt_new_label FROM vw_field_formats WHERE fdr_name = fmt_field)')->like('%'.$_POST['term'].'%'));
    }

    // Filter approval status and a dummy field from the results
    $where->add($queryFactory->getFieldCollection()->field('fdr_name')->notIn(['rep_approved', 'initial_current']));

    if ($module != '')
    {
        $coalesce = ['COALESCE((SELECT TOP 1 fmt_new_label FROM vw_field_formats WHERE fdr_name = fmt_field AND fmt_module = \'?\'), fdr_label) AS description', $module];
    }
    else
    {
        $coalesce = 'COALESCE((SELECT TOP 1 fmt_new_label FROM vw_field_formats WHERE fdr_name = fmt_field), fdr_label) AS description';
    }

    $query->select(['fdr_name AS code', $coalesce])
        ->from('field_directory')
        ->where($where)
        ->orderBy(['description']);

    $writer = $queryFactory->getSqlWriter();
    list($sql, $parameters) = $writer->writeStatement($query);

    $result = DatixDBQuery::PDO_fetch_all($sql, $parameters);

    $codes = [];

    foreach ($result as $code)
    {
        $codes[$code['code']] = $code['description'];
    }

    return $codes;
}

function getProfileCodes()
{
    $sql = 'SELECT recordid as code, pfl_name as description FROM profiles';
    $codes = \DatixDBQuery::PDO_fetch_all($sql);

    return $codes;
}
