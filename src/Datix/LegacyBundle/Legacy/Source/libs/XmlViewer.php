<?php
////////////////////////////////////////////////////
// Copy config.xml next to this stand-alone file  //
// to view the xml contet in a structured         //
// reader-fiendly way                             //
////////////////////////////////////////////////////

XMLView();

function XMLView()
{
    global $MinifierDisabled;

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    echo  '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<script language="javascript" type="text/javascript" src="js_functions/common' . $addMinExtension . '.js"></script>
<STYLE TYPE="text/css">
<!--
body{font-size: 12px;font-family: verdana, arial, helvetica, serif}
table{empty-cells: show}
td{font-size: 12px;color: #000000;font-family: verdana, arial, helvetica, serif}
.windowbg{font-size: 12px;color: #000000;font-family: verdana, arial, helvetica, serif;background-color: #d2e4fc}
.windowbg2{font-size: 12px;color: #000000;font-family: verdana, arial, helvetica, serif;background-color: #E3EFFF}
.gridlines{font-size: 12px;color: #FFFFFF;font-family: verdana, arial, helvetica, serif;background-color: #FFFFFF}
.titlebg{color: #ffffff;background-color: #6E94B7}
-->
</STYLE>
</head><body text="#000000" link="#0033ff" bgcolor="#ffffff">';

    $dtxtitle = "Datix Configuration Settings (XML)";

    $message = '<table border="0" width="100%" cellspacing="1" cellpadding="1" class="" align="center">
        <tr>
            <td colspan="2">';
    $message .= '
    <table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
        <tr>
            <td class="titlebg">
                <b>Configuration settings</b><br />
            </td>
        </tr>
    </table>
    </td>
    </tr>
    ';

    echo $message;

    $file_source = "config.xml";
    $handle=fopen($file_source, 'rb');
    $content = fread($handle, filesize($file_source));
    fclose($handle);

    if(is_object($xmlobj = xml_to_object($content)))
        if(is_array($xmlobj->children))
            $newarray = $xmlobj->children;

    if(is_array($newarray))
    {
        foreach($newarray as $newarrayx)
        {

        $xmlobj2 = $newarrayx;
        if(is_object($xmlobj2))
           if(is_array($newarray2 = $xmlobj2->children))
                foreach($newarray2 as $narray2)
                {
                    if(is_object($xmlobj3 = $narray2))
                    {
                        if($xmlobj3->name == "title")
                            XHTitle($xmlobj3->content);
                        if($xmlobj3->name == "global" || $xmlobj3->name == "variable")
                        {
                            if(is_array($newarray3 = $xmlobj3->children))
                                if(is_object($xmlobj4name = $newarray3[0]) && is_object($xmlobj4val = $newarray3[1]))
                                    XHRow($xmlobj4name->content, $xmlobj4val->content);

                        }
                        if($xmlobj3->name == "user" || $xmlobj3->name == "directive")
                        {
                            if(is_array($newarray3 = $xmlobj3->children))
                            {
                                foreach($newarray3 as $narray3)
                                {
                                    if(is_object($xmlobj3user = $narray3))
                                    {
                                        if($xmlobj3user->name == "name")
                                            XHRow("<b>".$xmlobj3user->content."</b>", "", "windowbg");
                                        if($xmlobj3user->name == "global" || $xmlobj3user->name == "setting")
                                        {
                                            if(is_array($newarray3a = $xmlobj3user->children))
                                                if(is_object($xmlobj4name = $newarray3a[0]) && is_object($xmlobj4val = $newarray3a[1]))
                                                    XHRow($xmlobj4name->content, $xmlobj4val->content);
                                        }
                                    }
                                }
                            }
                        }
                        if($xmlobj3->name == "table")
                        {
                            if(is_array($newarray3 = $xmlobj3->children))
                            {
                                foreach($newarray3 as $narray3)
                                {
                                    if(is_object($xmlobj4 = $narray3))
                                    {
                                        if($xmlobj4->name == "name")
                                            XHRow("<b>TABLE: ".$xmlobj4->content."</b>", "", "windowbg");
                                        if($xmlobj4->name == "row")
                                        {
                                            if(is_array($newarray4 = $xmlobj4->children))
                                               foreach($newarray4 as $narray4)
                                               {
                                                   if(is_object($xmlobj5 = $narray4))
                                                    {
                                                        if($xmlobj5->name == "col")
                                                        {
                                                            if(is_array($newarray5 = $xmlobj5->children))
                                                                if(is_object($xmlobj5name = $newarray5[0]) && is_object($xmlobj5val = $newarray5[1]))
                                                                XHRow($xmlobj5name->content, $xmlobj5val->content);

                                                        }
                                                    }
                                               }
                                        }
                                    }
                                }
                            }
                        }
                        if($xmlobj3->name == "file")
                            if(is_array($newarray3 = $xmlobj3->children))
                            {
                                foreach($newarray3 as $narray3)
                                {
                                    if(is_object($xmlobj3user = $narray3))
                                    {
                                        if($xmlobj3user->name == "title")
                                            XHRow("<b>".$xmlobj3user->content."</b>", "", "windowbg", true);
                                        if($xmlobj3user->name == "content")
                                            XHRow($xmlobj3user->content, "", "windowbg2", true);
                                    }
                                }
                            }
                    }
                }
        }
    }
}

function XHTitle($name = "")
{
    echo  '
    <tr>
    <td colspan="2">
    <table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
    <tr height="20">
    	<td class="titlebg" colspan="2">
        <b>'.$name.'</b>
    	</td>
    </tr>
    </table>
    </td>
    </tr>';
}

function XHRow($name = "", $val = "", $style = "windowbg2", $singlerow = false)
{
    if($singlerow)
        echo '<tr>
                <td class='.$style.' colspan="2">'.$name.'</td>
            </tr>';
    else

        echo '<tr>
                <td class='.$style.'>'.$name.'</td>
                <td class='.$style.'><font color="red">'.$val.'</font></td>
            </tr>';
}

function xml_to_object($xml) {


  $xml = str_replace('<content><?php', '<content>', $xml);
  $xml = str_replace('?></content>', '</content>', $xml);

  $parser = xml_parser_create('UTF-8');
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parse_into_struct($parser, $xml, $tags);
  xml_parser_free($parser);

  $elements = array();
  $stack = array();

  foreach ($tags as $tag) {
    $index = count($elements);
    if ($tag['type'] == "complete" || $tag['type'] == "open")
    {
         $elements[$index] = new XmlElement();
         $elements[$index]->name = $tag['tag'];
         if(array_key_exists('attributes', $tag))
            $elements[$index]->attributes = $tag['attributes'];
         if(array_key_exists('value', $tag))
            $elements[$index]->content = $tag['value'];

         if ($tag['type'] == "open") {
             $elements[$index]->children = array();
             $stack[count($stack)] = &$elements;
             $elements = &$elements[$index]->children;
         }
    }

    if ($tag['type'] == "close")
    {
         $elements = &$stack[count($stack) - 1];
         unset($stack[count($stack) - 1]);
    }
  }

  return $elements[0];
}

class XmlElement {
  var $name;
  var $attributes;
  var $content;
  var $children;
};



?>