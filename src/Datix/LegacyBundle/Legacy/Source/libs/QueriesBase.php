<?php
function get_saved_queries($MOD){

    //global $saved_queries;

    $user_id = $_SESSION["contact_login_id"];

    $sql = "SELECT recordid as qry_recordid, sq_name, sq_where, sq_tables
			FROM queries WHERE sq_module = '$MOD'
            AND (sq_user_id = $user_id OR sq_user_id is null)
            AND (sq_type != 'UI' OR sq_type IS NULL)
            ORDER BY sq_name";
	$request = db_query($sql);
	while ($qry = db_fetch_array($request))
	{
        $saved_queries[$qry["qry_recordid"]] = $qry["sq_name"];
	}

    return $saved_queries;
}

function GetSaveQueryDetails($query_name)
{
    $sql = "SELECT sq_where, sq_tables
			FROM queries
            WHERE sq_name = '$query_name'
            ";
	$request = db_query($sql);
    $SavedQueryDetails = db_fetch_array($request);

    return $SavedQueryDetails;
}

function GetSaveQueryDetailsByRecordID($query_id)
{
    $sql = "SELECT sq_where, sq_tables
            FROM queries
            WHERE recordid = $query_id
            ";
    $request = db_query($sql);
    $SavedQueryDetails = db_fetch_array($request);

    return $SavedQueryDetails;
}
