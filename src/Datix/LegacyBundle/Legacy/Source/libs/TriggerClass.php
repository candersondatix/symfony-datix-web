<?php
use src\users\model\UserModelFactory;

class TriggerClauses
{
    var $bInitialised;
    var $nCurrentModule;
    var $nNumCaseTriggers;
    var $nACaseTriggers = array();
    var $sACaseTriggersTypes = array();
    var $nNumTriggers;
    var $nATriggerModules = array();
    var $sATriggerExpressions = array();
    var $nATriggerIDs = array();
    var $sATriggerTypes = array();
    var $sATriggerMessages = array();
    var $sATriggerEventCodes = array();
    var $nATriggerTemID = array();
    var $sATriggerConID = array();
    var $nATriggerDocEdit = array();
    var $sATriggerEmailSubjMess = array();
    var $sATriggerEmailMess = array();
    var $nATriggerActionChains = array();
    var $record = array();

    function InitialiseTriggerClauses($nMod)
    {
        $this->nCurrentModule = $nMod;
        if (!$this->bInitialised || $this->nCurrentModule != $nMod)
        {
            $this->__RetrieveTriggerClauses();
            $this->bInitialised = true;
        }
    }

    private function __RetrieveTriggerClauses()
    {
        $this->nNumTriggers = 0;
        $sql = "SELECT recordid, mod_id, trg_expression, trg_message, trg_type, trg_evt_code, trg_tem_id, trg_con_id,
                        trg_doc_edit, trg_email_subject, trg_email_mess, trg_action_chain
                 FROM triggers_main WHERE trg_app_show IN ('WEB','BOTH') order by mod_id ASC";
        $request = db_query($sql);
        while ($row = db_fetch_array($request))
        {
            $this->nATriggerIDs[$this->nNumTriggers] = $row["recordid"];
            $this->nATriggerModules[$this->nNumTriggers] = $row["mod_id"];
            $this->sATriggerExpressions[$this->nNumTriggers] = $row["trg_expression"];
            $this->sATriggerMessages[$this->nNumTriggers] = $row["trg_message"];
            $this->sATriggerTypes[$this->nNumTriggers] = $row["trg_type"];
            $this->sATriggerEventCodes[$this->nNumTriggers] = $row["trg_evt_code"];
            $this->nATriggerTemID[$this->nNumTriggers] = $row["trg_tem_id"];
            $this->sATriggerConID[$this->nNumTriggers] = $row["trg_con_id"];
            $this->nATriggerDocEdit[$this->nNumTriggers] = $row["trg_doc_edit"];
            $this->sATriggerEmailSubjMess[$this->nNumTriggers] = $row["trg_email_subject"];
            $this->sATriggerEmailMess[$this->nNumTriggers] = $row["trg_email_mess"];
            $this->nATriggerActionChains[$this->nNumTriggers] = $row["trg_action_chain"];
            $this->nNumTriggers++;
        }
    }

    function CheckTriggers($CasID, $TriggerType, $RunTrigger, $RecordData)
    {
        $this->record = $RecordData;
        $Message = $this->__CheckTriggersForCase($CasID, $TriggerType, $RunTrigger);

        return $Message;
    }

    private function __RetrieveTriggersForCase($CasID)
    {
        $this->nNumCaseTriggers = 0;

        $sql = "SELECT trigger_id, trigger_type
                FROM triggers_links
                WHERE mod_id = ". $this->nCurrentModule . " AND cas_id = " . $CasID;

        $request = db_query($sql);
        while ($row = db_fetch_array($request))
        {
                $this->nACaseTriggers[] = $row["trigger_id"];
                $this->sACaseTriggersTypes[] = $row["trigger_type"];
                $this->nNumCaseTriggers++;
        }
    }

    private function __CheckTriggersForCase($CasID, $TriggerType, $RunTrigger)
    {
        global $ModuleDefs;
        $nI =0;
        $this->__RetrieveTriggersForCase($CasID);

        $messageArray = array();

        while ($nI < $this->nNumTriggers)
        {
            if ($this->nATriggerModules[$nI] == $this->nCurrentModule && ($this->sATriggerTypes[$nI] == $TriggerType || $TriggerType == ""))
            {
                if ($this->sATriggerExpressions[$nI] != "")
                {
                   $sql = "
                   SELECT recordid FROM " .  ($ModuleDefs[ModuleIDToCode($this->nCurrentModule)]['VIEW'] ? $ModuleDefs[ModuleIDToCode($this->nCurrentModule)]['VIEW'] : $ModuleDefs[ModuleIDToCode($this->nCurrentModule)]['TABLE']) .
                           "  WHERE (" . $this->sATriggerExpressions[$nI] . ") AND recordid = " . $CasID;
                }
                else
                {
                   $sql = "SELECT recordid FROM " .  ($ModuleDefs[ModuleIDToCode($this->nCurrentModule)]['VIEW'] ? $ModuleDefs[ModuleIDToCode($this->nCurrentModule)]['VIEW'] : $ModuleDefs[ModuleIDToCode($this->nCurrentModule)]['TABLE']) .
                           "  WHERE recordid = " . $CasID;
                }

                $request = db_query($sql);
                if ($row = db_fetch_array($request))
                {
                    // If the current trigger matches, we need to check if it has already matched (need to check that type matches too).
                    $nCaseTriggerIndex = array_search( $this->nATriggerIDs[$nI], $this->nACaseTriggers);
                    if ($nCaseTriggerIndex === false || ($nCaseTriggerIndex >= 0 && $this->sACaseTriggersTypes[$nCaseTriggerIndex] != $this->sATriggerTypes[$nI]))
                    {
                        $sql = "INSERT INTO triggers_links (mod_id, cas_id, trigger_id, trigger_type)
                                values (" . $this->nCurrentModule . ", $CasID, " . $this->nATriggerIDs[$nI] . ", '" . $this->sATriggerTypes[$nI] . "')";
                        $result = db_query($sql);

                        $this->nACaseTriggers[$this->nNumCaseTriggers] = $this->nATriggerIDs[$nI];
                        $this->sACaseTriggersTypes[$this->nNumCaseTriggers] = $this->sATriggerTypes[$nI];
                        $this->nNumCaseTriggers++;

                        if ($RunTrigger)
                        {
                            if ($this->sATriggerTypes[$nI] == "M" || $this->sATriggerTypes[$nI] == "")
                            {
                                $messageArray[] = $this->sATriggerMessages[$nI];
                            }
                            else if ($this->sATriggerTypes[$nI] == "EM" || $this->sATriggerTypes[$nI] == "ED")
                            {
                                //$From = $_SESSION["Globals"]["SYS_E_MAIL_ADDRESS"];
                                $From = $_SESSION["email"];

                                 if ($this->sATriggerConID[$nI])
                                 {
                                    $con_id = $this->sATriggerConID[$nI];

                                    $Factory = new UserModelFactory();
                                    $User = $Factory->getMapper()->findByInitials($con_id);

                                    if ($User->con_email != "")
                                    {
                                        \src\framework\registry\Registry::getInstance()->getLogger()->logEmail('Sending email to '.$User->con_email.' concerning module:'.$this->nCurrentModule.', recordid:'.$CasID.' based on trigger '. $this->nATriggerIDs[$nI] . '(type '.$this->sATriggerTypes[$nI].')');

                                        $this->EmailTriggerTemplate($User->con_email, $this->sATriggerEmailSubjMess[$nI] . " (ID = " .$CasID . ")",
                                                    $this->sATriggerEmailMess[$nI], $From);
                                        $EmailsSentMessage .= '<br> An email trigger has been sent to: ' . $User->fullname;
                                    }
                                 }
                            }
                            else if ($this->sATriggerTypes[$nI] == "AC") //action chain
                            {
                                try
                                {
                                    $manager = new src\actionchains\ActionChainManager();
                                    $InstanceID = $manager->createConcreteActionChain($this->nATriggerActionChains[$nI], $this->record['recordid'], ModuleIDToCode($this->nCurrentModule), new DateTime(GetTodaysDate()));
                                }
                                catch (Exception $e)
                                {
                                    logErrorReport(array('message' => $e->getMessage(), 'details' => 'Task Generator error: failed to create Action Chain ID '.$this->nATriggerActionChains[$nI].' when saving '.ModuleIDToCode($this->nCurrentModule).' ID '.$this->record['recordid']));  
                                }

                                if($InstanceID)
                                {
                                    DatixDBQuery::PDO_query('UPDATE triggers_links SET action_chain_instance = :instance WHERE mod_id = :mod_id AND cas_id = :cas_id AND trigger_id = :trigger_id AND trigger_type = :trigger_type',
                                        array('instance' => $InstanceID, 'mod_id' => $this->nCurrentModule, 'cas_id' => $CasID, 'trigger_id' => $this->nATriggerIDs[$nI], 'trigger_type' => $this->sATriggerTypes[$nI]));
                                }

                            }
                            if ($this->nCurrentModule == MOD_INCIDENTS)
                            {
                                $LinkRiskTriggerMsg = $this->__RunExtraTrigger($CasID, $nI);
                                if ($LinkRiskTriggerMsg != "")
                                {
                                    $messageArray[] = '<br>' . $LinkRiskTriggerMsg;
                                }
                            }
                        }
                    }
                }
                else
                {
                    //If the trigger did not match now, but has matched before, we want to delete it from the links table
                    // and from the array of matched triggers.

                    if ($this->sATriggerTypes[$nI] == "AC") //need to delete action chain here.
                    {
                        $Instance = DatixDBQuery::PDO_fetch('SELECT action_chain_instance FROM triggers_links WHERE mod_id = :mod_id AND cas_id = :cas_id AND trigger_id = :trigger_id AND trigger_type = :trigger_type',
                            array('mod_id' => $this->nCurrentModule, 'cas_id' => $CasID, 'trigger_id' => $this->nATriggerIDs[$nI], 'trigger_type' => $this->sATriggerTypes[$nI]), PDO::FETCH_COLUMN);
                        
                        $acFactory = new src\actionchains\model\ActionChainModelFactory();
                        $acFactory->getMapper()->deleteConcreteActionChain($this->record['recordid'], ModuleIDToCode($this->nCurrentModule), $this->nATriggerActionChains[$nI], $Instance);
                    }


                    $nCaseTriggerIndex = array_search($this->nATriggerIDs[$nI], $this->nACaseTriggers);

                    if ($nCaseTriggerIndex !== false && $nCaseTriggerIndex >= 0)
                    {
                        $sql = "DELETE FROM triggers_links WHERE mod_id = " . $this->nCurrentModule . " AND cas_id = $CasID
                                AND trigger_id = " . $this->nATriggerIDs[$nI] . " AND trigger_type = '" . $this->sATriggerTypes[$nI] . "'";

                        if ($result = db_query($sql))
                        {
                            unset($this->nACaseTriggers[$nCaseTriggerIndex]);
                            $this->nACaseTriggers = array_values($this->nACaseTriggers);
                            unset($this->sACaseTriggersTypes[$nCaseTriggerIndex]);
                            $this->sACaseTriggersTypes = array_values($this->sACaseTriggersTypes);
                            $this->nNumCaseTriggers--;
                        }
                    }

                }
                if ($this->nATriggerIDs[$nI] > 0)
                {
                    $sql = "SELECT trg_link_cas_id , trg_link_mod_id
                            FROM triggers_main
                            WHERE triggers_main.recordid = " . $this->nATriggerIDs[$nI];

                    $request2 = db_query($sql);
                    if ($row = db_fetch_array($request2))
                    {
                        if ($row["Trg_link_mod_id"] == MOD_RISKREGISTER)
                        {
                            $sql = "UPDATE ra_main
                                    SET ram_link_count = (
                                            SELECT COUNT(*)
                                            FROM triggers_links, triggers_main
                                            WHERE triggers_links.trigger_id = triggers_main.recordid
                                            AND triggers_main.trg_link_mod_id = " . $row["Trg_link_mod_id"] .
                                            " AND triggers_main.trg_link_cas_id = " . $row["nTrg_link_cas_id"] . ")
                                    WHERE ra_main.recordid = " . $row["nTrg_link_cas_id"] ;

                            $result = db_query($sql);
                        }
                    }
                }
            }
            $nI++;
        }

        if ($EmailsSentMessage != "")
        {
            $messageArray[] = $EmailsSentMessage;
        }

        return implode('<br>', $messageArray);
    }

    function EmailTriggerTemplate($EmailTo, $EmailSubject, $EmailBody, $From)
    {
        global $UserEmailTextFile, $scripturl;

  /* This code can be used for substituting data into the email, but has not been included since the main app does not do this yet.
            foreach ($this->record as $field => $value)
            {
                $EmailBody = str_ireplace( '$record[' . $field .']', $value, $EmailBody);
            }

            $EmailBody = str_ireplace( '$scripturl', $scripturl, $EmailBody);        */

            $From = "From: $From";
            return @\UnicodeString::mail($EmailTo, $EmailSubject, $EmailBody, $From);

    }

    private function __RunExtraTrigger($Cas_id, $TriggerIndex)
    {
        global $ModuleDefs;
        $nCurrentModule = $this->nATriggerModules[$TriggerIndex];

        switch ($nCurrentModule)
        {
        case MOD_COMPLAINTS:
            $CurrentModuleIncidentDateField = 'com_dincident';
            $ModShortName = "COM";
            break;
        case MOD_INCIDENTS:
            $CurrentModuleIncidentDateField = 'inc_dincident';
            $ModShortName = "INC";
            break;
        }

        $CurrentModulePrimaryKey = $ModuleDefs[$ModShortName]["TABLE"] . '.recordid';

        $sql = 'SELECT ram_dclosed, ' . $CurrentModuleIncidentDateField . ', ram_name
                    FROM ra_main, triggers_main, ' . $ModuleDefs[$ModShortName]["TABLE"]
                    . ' WHERE ra_main.recordid = triggers_main.trg_link_cas_id AND
                    triggers_main.trg_link_mod_id = ' . MOD_RISKREGISTER
                    . ' AND triggers_main.recordid = ' . $this->nATriggerIDs[$TriggerIndex] . ' AND '
                    . $CurrentModulePrimaryKey . ' = ' . $Cas_id;

        $riskresult = db_query($sql);

        if ($row = db_fetch_array($riskresult))
        {
            $Message = 'Link made to risk: ' . $row['ram_name'];

            if ($row['ram_dclosed'] != "" && $row[$CurrentModuleIncidentDateField] != "" && $row[$CurrentModuleIncidentDateField] >= $row['ram_dclosed'])
            {
                $Message .=  '<br>Warning: this risk has a closed date of ' .
                    FormatDateVal($row['ram_dclosed']) . '. This is earlier than the incident date for the current record.';
            }
        }
        return $Message;
    }
}

?>
