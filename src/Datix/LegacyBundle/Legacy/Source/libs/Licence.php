<?php

include_once "constants.php";

class licence {

        var $isValidLicence;
        var $licenceErrorMsg;
        var $client;
        var $licenceKey;
        var $radix;
        var $licenceSet;
        var $licensedModules;
        var $UsersPerModule;
        var $numUsers;
        var $expiryDate;
        var $evaluation;

        function licence () {

            $this->licenceErrorMsg = "";
            $this->isValidLicence = true;

            //retrieve client name
            $cli['parmvalue'] = GetParm('CLIENT');

            if (!$cli) {
                $this->isValidLicence = false;
                $this->licenceErrorMsg = LIC_ERR_CLIENT_NOTSET;
            }

            //retrieve licence key
            $key['parmvalue'] = GetParm('LICENCE_KEY_WEB');

            if (!$key) {
                $this->isValidLicence = false;
                $this->licenceErrorMsg = LIC_ERR_KEY_NOTSET;
            }

            $this->client=$cli["parmvalue"];
            $this->licenceKey=$key["parmvalue"];
            $this->radix=36;

            $this->isValidLicence = $this->checkLicenceKey();

        }

        function save() {
            //delete existing licence key
            $sql = "delete from globals WHERE parameter = 'LICENCE_KEY_WEB'";
            db_query($sql);

            //insert new licence key
            $sql = "insert into globals (parameter,parmvalue) values ('LICENCE_KEY_WEB','$this->licenceKey')";
            db_query($sql);

            //update the cached licence key value
            $_SESSION["Globals"]['LICENCE_KEY_WEB'] = $this->licenceKey;
        }

        function licenceErrorMsg() {
            return $this->licenceErrorMsg;
        }

        function isValid() {
            return $this->isValidLicence;
        }

        function clientName() {
            return $this->client;
        }

        function licenceKey() {
            return $this->licenceKey;
        }

        function expiryDate() {
            return $this->expiryDate;
        }

        function numUsers() {
            return $this->numUsers;
        }

        function UsersPerModule() {
            return $this->UsersPerModule;
        }

        function lastError() {
            return $this->licenceErrorMsg;
        }

        function getLicenceInfo() {

            echo
            '<table border="1">
                <tr><td>Client</td><td>' . $this->client . '</td></tr>
                <tr><td>Key</td><td>' . $this->licenceKey . '</td></tr>
                <tr><td>Users</td><td>' . $this->numUsers . '</td></tr>
                <tr><td>Expiry date</td><td>' . $this->expiryDate . '</td></tr>
                <tr><td>Evaluation version</td><td>' . ($this->evaluation?"Yes":"No") . '</td></tr>
                <tr><td>Modules</td><td>' . implode(',',$this->modules) . '</td></tr>
            </table>';

        }

        function CRC ($input,$inputCRC) {

            $strLen = \UnicodeString::strlen($input)+1;
            $retCRC = $inputCRC;

            for ($i = 0; $i < $strLen; $i++) {

                 $byte = ord($input);
                 $input = \UnicodeString::substr($input,1,\UnicodeString::strlen($input)-1);

                for ($j = 0; $j < 8; $j++) {

                    $bit = ((($retCRC&0x8000)==0x8000)^(($byte & pow(2, $j))==pow(2, $j)));
                    $retCRC = ($retCRC & 0x7FFF) * 2;
                    If ($bit<>0)
                        $retCRC = ($retCRC^0x1021);
                }
            }

            return $retCRC;

        }

        function validateLicenceKey()
        {
            //Check if still using old pre 11.3 license key.
            $KeyPieces = explode("-", $this->licenceKey);
            if (count($KeyPieces) <= 6)
            {
                return false;
            }

            $strCheckSum = \UnicodeString::substr($this->licenceKey,\UnicodeString::strlen($this->licenceKey)-4,4);
            $numCheckSum = base_convert($strCheckSum,$this->radix,10);
           // if (strlen($this->licenceKey)<>29)
          //      return false;
            $keyTmp = \UnicodeString::substr($this->licenceKey,0,\UnicodeString::strlen($this->licenceKey)-5);
            if ($this->CRC($keyTmp,0)==$numCheckSum)
                return true;
            return false;

        }

        function mangleKey($key,$index1,$bitNo1,$index2,$bitNo2) {

            $keyParts=explode('-',$key);

            $word1=base_convert($keyParts[$index1],$this->radix,10);
            $word2=base_convert($keyParts[$index2],$this->radix,10);

            $bit1=(pow(2,$bitNo1)&$word1)/pow(2,$bitNo1);
            $bit2=(pow(2,$bitNo2)&$word2)/pow(2,$bitNo2);

            $word1=(0xffff-pow(2,$bitNo1))&$word1;
            $word2=(0xffff-pow(2,$bitNo2))&$word2;

            $word1=$word1+$bit2*pow(2,$bitNo1);
            $word2=$word2+$bit1*pow(2,$bitNo2);

            $keyParts[$index1]=base_convert($word1,10,$this->radix);
            $keyParts[$index2]=base_convert($word2,10,$this->radix);

            //pad with leading zeros
            $keyParts[$index1]=str_pad($keyParts[$index1], 4, "0", STR_PAD_LEFT);
            $keyParts[$index2]=str_pad($keyParts[$index2], 4, "0", STR_PAD_LEFT);

            return implode("-", $keyParts);
        }

        function checkLicenceKey() {

            $licenceSet = false;
            $numUsers = 0;

            $licenceSet = true;
            if (!$this->validateLicenceKey()) {
                $this->licenceErrorMsg = LIC_ERR_INVALID;
                return false;
            }

            //Strip out the checksum
            $licenceKey=\UnicodeString::substr($this->licenceKey,0,\UnicodeString::strlen($this->licenceKey)-5);

            $keyParts=explode('-',$licenceKey);
            $i=count($keyParts) - 5;
            while ($i >= 0)
            {
                $UserPerModMangleKeys[] = dechex(1+$i).dechex(15).dechex(5+$i).dechex(12);
                $i--;
            }
            $UserPerModMangleKeys = (array_reverse($UserPerModMangleKeys));

            //Unmangle the key
            $mangleKey = "3F13-2A19-3814-1A23-073E-021B-2F3B-2630-1F2C-320F-4B2F-2E16-350D" ;

            $UserPerModMangleKey = implode("-", $UserPerModMangleKeys);
            if ($UserPerModMangleKey)
            {
                $mangleKey = $mangleKey . "-" . \UnicodeString::strtoupper($UserPerModMangleKey);
            }

            $mangleKeyParts = explode("-", $mangleKey);

            for ($i = count($mangleKeyParts)-1; $i >= 0; $i--) {

                $licenceKey = $this->mangleKey(
                    $licenceKey,
                    base_convert($mangleKeyParts[$i][0],16,10),
                    base_convert($mangleKeyParts[$i][1],16,10),
                    base_convert($mangleKeyParts[$i][2],16,10),
                    base_convert($mangleKeyParts[$i][3],16,10));

            }

            $keyParts = explode('-',$licenceKey);

            //Check the client name against the checksum
            if ($this->CRC(\UnicodeString::strtoupper($this->client),0)<>base_convert($keyParts[3],$this->radix,10)) {
                $this->licenceErrorMsg = LIC_ERR_INVALID;
                return false;
            }

            //Check if DATIXWeb licence key
            $this->evaluation=($keyParts[0]=='B2D2');

            //Check the expiry date
            $expires=base_convert($keyParts[2],$this->radix,10);
            //$this->expiryDate=date("M-d-Y", mktime(0,0,0,1,1+$expires,1970));
            $this->expiryDate=date("d-M-Y", mktime(0,0,0,1,1+$expires,1970));
            $today=getdate();
            if (mktime(0,0,0,1,1+$expires,1970)<$today["0"]) {
                $this->licenceErrorMsg = LIC_ERR_EXPIRED;
                return false;
            }

            //Get the number of users and the licensed modules
            $usersModulesKey = base_convert($keyParts[1],$this->radix,10);
            $this->numUsers = base_convert($keyParts[4],$this->radix,10);

            $numModules=MOD_COUNT;
            $modulesKey = fmod($usersModulesKey, pow(2,$numModules));

            // create modules struct default to non licensed,
            // modules are listed in temp array in reverse order as when licence key is created
            $moduleTemp = array(
                MOD_ASSESS => false,
                MOD_RISKREGISTER => false,
                MOD_INCIDENTS => false,
                MOD_COMPLAINTS => false,
                MOD_CLAIMS => false,
                MOD_PALS => false,
                MOD_TRAINING => false,
                MOD_RFI => false,
                MOD_TIME => false,
                MOD_STANDARDS => false,
                MOD_SABS => false,
              //  MOD_MESSAGES => false,
                MOD_CIV => false,
                MOD_DASHBOARD => false,
                MOD_HOTSPOTS => false,
                MOD_CQC_OUTCOMES => false,
                MOD_ASSESSMENT_MODULES => false);

            $nNextMod = 0;
            $nI = count($keyParts) - 2;
            foreach ($moduleTemp as $moduleNo => $Permission)
            {
                $this->licensedModules[$moduleNo] = (fmod($modulesKey,2)==1);
                if ($this->licensedModules[$moduleNo])
                {
                    $this->UsersPerModule[$moduleNo] = base_convert($keyParts[$nI-$nNextMod],$this->radix,10);
                    $nNextMod++;
                }
                if (fmod($modulesKey,2)==1)
                {
                    $modulesKey--;
                }
                $modulesKey=$modulesKey/2;
            }

            return true;

        }

        function isModuleLicensed($module) {
            return  $this->licensedModules[$module];
        }

        function licensedModules()
        {
            global $txt;

            if ($this->isModuleLicensed(MOD_INCIDENTS))
                $modules[MOD_INCIDENTS] = "Incidents";
            if ($this->isModuleLicensed(MOD_RISKREGISTER))
                $modules[MOD_RISKREGISTER] = "Risk register";
            if ($this->isModuleLicensed(MOD_PALS))
                $modules[MOD_PALS] = "PALS";
            if ($this->isModuleLicensed(MOD_COMPLAINTS))
                $modules[MOD_COMPLAINTS] = "Complaints";
            if ($this->isModuleLicensed(MOD_STANDARDS))
                $modules[MOD_STANDARDS] = "Standards";
            if ($this->isModuleLicensed(MOD_CLAIMS))
                $modules[MOD_CLAIMS] = "Claims";
            if ($this->isModuleLicensed(MOD_SABS))
                $modules[MOD_SABS] = "Safety Alerts";
            //if ($this->isModuleLicensed(MOD_MESSAGES))
            //    $modules[MOD_MESSAGES] = "Secure Messages";
            if ($this->isModuleLicensed(MOD_DASHBOARD))
                $modules[MOD_DASHBOARD] = "Dashboard";
            if ($this->isModuleLicensed(MOD_HOTSPOTS))
                $modules[MOD_HOTSPOTS] = $txt["mod_hotspots_title"];
            if ($this->isModuleLicensed(MOD_CQC_OUTCOMES))
                $modules[MOD_CQC_OUTCOMES] = $txt["CQCNamesTitle"];
            if ($this->isModuleLicensed(MOD_ASSESSMENT_MODULES))
                $modules[MOD_ASSESSMENT_MODULES] = $txt["AMONamesTitle"];
            return $modules;
        }

        /**
        * Checks to see if by adding the specified array of users to a module, whether the license limit for that module will be exceeded.
        *
        * @param string $Module Module
        * @param int array $UsersToCheck Array of User recordids to be added to module.
        */
        public function checkUserLimitExceeded($Module, $UsersToCheck = array())
        {
            global $ModuleDefs;

            $NewUsers = 0;
            foreach ($UsersToCheck as $con_id)
            {
                 if (self::checkUserLicensedForModule($Module, $con_id) == false)
                 {
                    $NewUsers++;
                 }
            }

            if ($this->licensedModules[$ModuleDefs[$Module]["MOD_ID"]] && $this->UsersPerModule[$ModuleDefs[$Module]["MOD_ID"]] > 0)
            {
                $LicensesUsed = $this->getLicensesUsed($Module);
                if (($LicensesUsed + $NewUsers) > $this->UsersPerModule[$ModuleDefs[$Module]["MOD_ID"]])
                {
                    return true;
                }
            }

            return false;
        }

        public function getLicensesUsed($Module)
        {
            global $ModuleDefs;

            $ModulePerm = $ModuleDefs[$Module]["PERM_GLOBAL"];
            $sql = "select COUNT(*) as user_count from USER_PARMS where PARAMETER = :parameter";
            $UserCount = DatixDBQuery::PDO_fetch($sql, array("parameter" => $ModulePerm));

            $sql = "SELECT COUNT(DISTINCT(con_id)) as group_count FROM
                        ((SELECT con_id FROM staff, sec_group_permissions, sec_staff_group
                            WHERE staff.recordid = con_id AND (sec_group_permissions.grp_id = sec_staff_group.grp_id
                            AND sec_group_permissions.item_code = :ModulePerm1)
                            )
                        UNION
                        (SELECT con_id FROM sec_group_permissions,
                                            (SELECT link_profile_group.lpg_group AS grp_id, staff.recordid AS con_id FROM profiles, staff, link_profile_group
                                                WHERE profiles.recordid = link_profile_group.lpg_profile
                                                AND profiles.recordid = staff.sta_profile) sec_staff_group_via_profile
                        WHERE sec_group_permissions.grp_id = sec_staff_group_via_profile.grp_id
                        AND sec_group_permissions.item_code = :ModulePerm2)) num_group_profile_users";
            $GroupUserCount = DatixDBQuery::PDO_fetch($sql, array("ModulePerm1" => $ModulePerm, "ModulePerm2" => $ModulePerm));

            return $UserCount["user_count"] + $GroupUserCount["group_count"];

        }

        /**
        * Checks if user is already licensed for a specified module
        *
        * @param string $Module Module short name
        * @param int $UserID Recordid of user.
        */
        public function checkUserLicensedForModule($Module, $UserID)
        {
            global $ModuleDefs;

            $ModulePerm = $ModuleDefs[$Module]["PERM_GLOBAL"];
            $sql = "SELECT COUNT(*) as user_count FROM staff LEFT JOIN user_parms ON staff.login = user_parms.login
                    WHERE parameter = :parameter and staff.recordid = :recordid";
            $UserCount = DatixDBQuery::PDO_fetch($sql, array("parameter" => $ModulePerm, "recordid" => $UserID));

            $sql = "SELECT COUNT(con_id) as group_count FROM
                        ((SELECT con_id FROM staff, sec_group_permissions, sec_staff_group
                            WHERE staff.recordid = con_id AND (sec_group_permissions.grp_id = sec_staff_group.grp_id
                            AND sec_group_permissions.item_code = :ModulePerm1)
                            )
                        UNION
                        (SELECT con_id FROM sec_group_permissions,
                                            (SELECT link_profile_group.lpg_group AS grp_id, staff.recordid AS con_id FROM profiles, staff, link_profile_group
                                                WHERE profiles.recordid = link_profile_group.lpg_profile
                                                AND profiles.recordid = staff.sta_profile) sec_staff_group_via_profile
                        WHERE sec_group_permissions.grp_id = sec_staff_group_via_profile.grp_id
                        AND sec_group_permissions.item_code = :ModulePerm2)) num_group_profile_users
                        WHERE num_group_profile_users.con_id = :con_id";
            $GroupUserCount = DatixDBQuery::PDO_fetch($sql, array("ModulePerm1" => $ModulePerm, "ModulePerm2" => $ModulePerm, "con_id" => $UserID));

            if (($UserCount["user_count"] + $GroupUserCount["group_count"]) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }


?>
