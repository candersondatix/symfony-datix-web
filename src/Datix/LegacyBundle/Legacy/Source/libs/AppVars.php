<?php

// Set names, foreign keys, codes, etc. for each module
// (similar to AppVars.apl for the main app)

require_once 'Source/libs/constants.php';

// Assets (Equipment)
$ModuleDefs['AST'] = array(
    'MOD_ID' => MOD_ASSETS,
    'CODE' => 'AST',
    'NAME' => _tk('mod_assets_title'),
    'REC_NAME' => _tk('ASTName'),
    'REC_NAME_PLURAL' => _tk('ASTNames'),
	'REC_NAME_TITLE' => _tk('ASTNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('ASTNamesTitle'),
    'USES_APPROVAL_STATUSES' => true,
    'FK' => 'ast_id',
    'OURREF' => 'ast_ourref',
    'TABLE' => 'assets_main',
    'ACTION' => 'asset',
    'PERM_GLOBAL' => 'AST_PERMS',
    'MAIN_URL' => 'action=asset',
    'LIBPATH' => 'Source/assets',
    'FIELD_NAMES' => array(
        "NAME" => "ast_name"
    ),
    'LOCATION_FIELD_PREFIX' => 'ast_',
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'AST1'),
        2 => array('LEVEL' => 2, 'CODE'=>'AST')
    ),
    'FORMCODE' => 'AST',
    'ICON' => 'icons/icon_AST.png',
    'SEARCH_URL' => 'action=assetssearch',
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_NOTES' => true,
    'FORMCODE1' => 'AST',
    "BASIC_FORM_FILES" => array(
        1 => "Source/assets/BasicForm_NoPanels.php",
        2 => "Source/assets/BasicForm.php"
    ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserASTForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserAST1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserASTSettings',
    'FIELD_ARRAY' => array(
        'rep_approved', 'ast_name', 'ast_ourref', 'ast_organisation', 'ast_unit', 'ast_clingroup', 'ast_directorate',
        'ast_specialty', 'ast_loctype', 'ast_locactual', 'ast_type', 'ast_product', 'ast_model', 'ast_manufacturer',
        'ast_supplier', 'ast_catalogue_no', 'ast_batch_no', 'ast_serial_no', 'ast_dmanufactured', 'ast_dputinuse',
        'ast_cemarking', 'ast_location', 'ast_quantity', 'ast_dlastservice', 'ast_dnextservice', 'ast_descr',
        'ast_category', 'ast_cat_other_info'
    ),
    'LINK_FIELD_ARRAY' => array(
        'link_damage_type',
        'link_other_damage_info',
        'link_replaced',
        'link_replace_cost',
        'link_repaired',
        'link_repair_cost',
        'link_written_off',
        'link_disposal_cost',
        'link_sold',
        'link_sold_price',
        'link_decommissioned',
        'link_decommission_cost',
        'link_residual_value'
    ),
    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Where' => '(1=1)'
        )
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Equipment/c_dx_equipment_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Equipment/c_dx_equipment_guide.xml',

);

$ModuleDefs['CON'] = array(
    'MOD_ID' => MOD_CONTACTS,
    'CODE' => 'CON',
    'NAME' => _tk('mod_contacts_title'),
    'REC_NAME' => _tk('CONName'),
    'REC_NAME_PLURAL' => _tk('CONNames'),
	'REC_NAME_TITLE' => _tk('CONNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('CONNamesTitle'),
    'USES_APPROVAL_STATUSES' => true,
    'FK' => 'con_id',
    'TABLE' => 'contacts_main',
    'ACTION' => 'editcontact',
    'LINK_ACTION' => 'linkcontactgeneral',
    'LIBPATH' => 'Source/contacts',
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'CON1', 'TITLE'=>'Level 1 Forms', 'HIDECODE' => true, 'LINK_TEXT' => 'Design a new level 1 contact form'),
        2 => array('LEVEL' => 2, 'CODE'=>'CON', 'TITLE'=>'Level 2 Forms', 'HIDECODE' => true, 'LINK_TEXT' => 'Design a new level 2 contact form')
    ),
    'FORMCODE' => 'CON',
    'ICON' => 'icons/icon_CON.png',
	'PERM_GLOBAL' => 'CON_PERMS',
	'SEARCH_URL' => 'action=contactssearch',
    'CAN_LINK_DOCS' => true,
    'FORMCODE1' => 'CON',
    "BASIC_FORM_FILES" => array(
        1 => "Source/contacts/BasicForm_Simple.php",
        2 => "Source/contacts/BasicForm.php"
    ),
    'FIELD_ARRAY' => array(
        'con_type', 'con_title', 'con_forenames', 'con_surname', 'con_dob', 'con_dod', 'con_ethnicity',
        'con_disability', 'con_language', 'con_address','con_postcode','con_gender', 'con_number', 'con_nhsno',
        'con_police_number', 'con_work_alone_assessed', 'con_religion', 'con_sex_orientation', 'con_name', 'con_tel1',
        'con_tel2', 'con_fax', 'con_email','con_jobtitle', 'con_orgcode', 'con_unit', 'con_clingroup',
        'con_directorate','con_specialty', 'con_loctype', 'con_locactual', 'con_empl_grade','con_payroll',
        'con_subtype', 'con_dopened', 'con_dclosed', 'con_notes', 'con_organisation', 'rep_approved', 'show_document'
    ),
    // Required for work around when selecting VARCHAR data fields greater than 255 in length.
    'FIELD_ARRAY_SELECT' => array(
        'con_type', 'con_title', 'con_forenames', 'con_surname', 'con_dob', 'con_dod', 'con_ethnicity',
        'con_disability', 'con_language', 'con_address','con_postcode','con_gender', 'con_number','con_nhsno',
        'con_police_number', 'con_work_alone_assessed', 'con_religion', 'con_sex_orientation', 'con_name', 'con_tel1',
        'con_tel2', 'con_fax', 'con_email','con_jobtitle', 'con_orgcode', 'con_unit',
        'CAST (con_orgcode AS TEXT) AS con_orgcode', 'CAST (con_unit AS TEXT) AS con_unit',
        'CAST (con_clingroup AS TEXT) AS con_clingroup', 'CAST (con_directorate AS TEXT) AS con_directorate',
        'CAST (con_specialty AS TEXT) AS con_specialty', 'CAST (con_loctype AS TEXT) AS con_loctype',
        'CAST (con_locactual AS TEXT) AS con_locactual', 'con_empl_grade','con_payroll', 'con_subtype', 'con_dopened',
        'con_dclosed', 'con_notes', 'con_organisation', 'rep_approved'
    ),
    'LINKED_FIELD_ARRAY' => array(
        'link_type', 'link_role', 'link_status', 'link_notes', 'link_treatment', 'link_bodypart1', 'link_injury1',
        'link_ref', 'link_dear', 'link_npsa_role','link_deceased', 'link_age', 'link_age_band', 'link_occupation',
        'link_riddor','link_is_riddor', 'link_daysaway', 'link_clin_factors', 'link_direct_indirect',
        'link_injury_caused', 'link_attempted_assault', 'link_discomfort_caused', 'link_public_disorder',
        'link_verbal_abuse', 'link_harassment', 'link_police_pursue', 'link_police_persue_reason', 'link_pprop_damaged',
        'link_worked_alone', 'link_become_unconscious', 'link_req_resuscitation', 'link_hospital_24hours',
        'link_abs_start', 'link_abs_end', 'link_mhact_section', 'link_mhcpa', 'link_patrelation', 'link_legalaid',
        'link_lip', 'link_ndependents', 'link_agedependents', 'link_marriage', 'link_injuries', 'link_resp',
        'link_plapat', 'link_notify_progress', 'link_date_admission'
    ),
    'LINKED_FIELD_ARRAY_RESPONDENT' => array(
        'link_type', 'con_id', 'link_notes', 'link_resp', 'link_role', 'main_recordid', 'indemnity_reserve_assigned', 'expenses_reserve_assigned', 'remaining_indemnity_reserve_assigned', 'remaining_expenses_reserve_assigned', 'resp_total_paid'
    ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserCONForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserCON1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserCONSettings',
    'CAN_LINK_EQUPMENT' => true,
        
    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Where' => '(1=1)'
        )        
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Contacts/c_dx_contacts_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Contacts/c_dx_contacts_guide.xml',
);

// Safety Alerts
$ModuleDefs['SAB'] = array(
    'MOD_ID' => MOD_SABS,
    'CODE' => 'SAB',
	'NAME' => _tk('mod_sabs_title'),
	'REC_NAME' => _tk('SABSName'),
	'REC_NAME_PLURAL' => _tk('SABSName'),
	'REC_NAME_TITLE' => _tk('SABSNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('SABSNameTitle'),
    'FK' => 'sab_id',
    'OURREF' => 'sab_reference',
    'TABLE' => 'sabs_main',
    'ACTION' => 'sabs',
    'PERM_GLOBAL' => 'SAB_PERMS',
    'MAIN_URL' => 'action=sabs',
    'LIBPATH' => 'Source/sabs',
    'GSRECNAME' => 'sab_title',
    'FIELD_NAMES' => array(
        "NAME" => "sab_title",
        "HANDLER" => "sab_handler"
    ),
    'LOCATION_FIELD_PREFIX' => 'sab_',
    'SUBFORMS' => array(
        'CON' => array(
            'LINKTYPES' => array(
                "S" => "For action by",
			    "I" => "Information only",
			    "N" => "Contact"
            )
        )
    ),
    'FORMS' => array(
        1 => array('LEVEL' => 2, 'CODE'=>'SAB1'),
        2 => array('LEVEL' => 2, 'CODE'=>'SAB2')
    ),
    'FORMCODE1' => 'SAB1',
    'FORMCODE2' => 'SAB2',
    'LEVEL1_PERMS' => array('SAB1'),
    'BASIC_FORM_FILES' => array(
        1 => 'Source/sabs/BasicFormSAB.php',
        2 => 'Source/sabs/BasicFormSAB.php'
    ),
    'CON_FORM_FILES' => array('S' => 'Source/contacts/BasicForm_SAB_CON.php'),
    'ICON' => 'icons/icon_SAB.png',
	'SEARCH_URL' => 'action=search&module=SAB',
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_CONTACTS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
    'FORM_DESIGN_ARRAY_FILE' => 'UserSABForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserSAB1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserSAB2Settings',
    'STAFF_EMPL_FILTER_MAPPINGS' => array(
        'sab_organisation' => 'con_orgcode',
        'sab_unit' => 'con_unit',
        'sab_clingroup' => 'con_clingroup',
        'sab_directorate' => 'con_directorate',
        'sab_specialty' => 'con_specialty',
        'sab_loctype' => 'con_loctype',
        'sab_locactual' => 'con_locactual'
    ),
    'FIELD_ARRAY' => array(
        'sab_title', 'sab_reference', 'sab_source', 'sab_type', 'sab_subtype', 'sab_action_type', 'sab_action_descr',
        'sab_manager', 'sab_handler', 'sab_issued_date','sab_dstart_due','sab_dend_due', 'sab_dstart','sab_dend',
        'sab_cost', 'sab_cost_notes', 'sab_read_by_due', 'sab_dopened', 'sab_dclosed','sab_descr', 'sab_organisation',
        'sab_unit','sab_clingroup', 'sab_directorate', 'sab_specialty', 'sab_loctype','sab_locactual'
    ),
    'CONTACTTYPES' => array(
        "S" => array( "Type"=> "S", "Name"=>_tk('sab_for_action_by'), "Plural"=>_tk('sab_for_action_by'), 'NoDuplicatesWith' => array('S'), "PanelName" => 'contacts_type_S'),
        "I" => array( "Type"=> "I", "Name"=>_tk('sab_information_only'), "Plural"=>_tk('sab_information_only'), 'NoDuplicatesWith' => array('I'), "PanelName" => 'contacts_type_I'),
        "N" => array( "Type"=> "N", "Name"=>_tk('sab_contact'), "Plural"=>_tk('sab_contact_plural'), "PanelName" => 'contacts_type_N')
    ),
    'LEVEL1_CON_OPTIONS' => array(
        "S" => array("Title" => "For action by", "DivName" => 'contacts_type_S', "Max"=> 1),
        "I" => array("Title" => _tk('person_affected'), "DivName" => 'contacts_type_I'),
        "N" => array("Title" => "Contact", "DivName" => 'contacts_type_N'),
    ),
    'TRAFFICLIGHTS_FIELDS' => array(
        'sab_action_type',  'sab_source', 'sab_type'
    ),
    'HARD_CODED_LISTINGS' => array(
        'own' => array(
            'Where' => ($_SESSION["AdminUser"] ? '(1=1)' : 'sabs_main.rep_approved = \'UN\' OR sabs_main.rep_approved IS NULL OR sabs_main.rep_approved = \'\''),
            'NoOverdue' => true
         ),
        'mysabs' => array(
            'Where' => 'sabs_main.recordid IN (SELECT sab_id FROM sab_link_contacts WHERE con_id = ' . $_SESSION['contact_login_id'] . ')',
            'NoOverdue' => true
         ),
        'mysabsresponse' => array(
            'Where' => 'sabs_main.recordid IN
                           (SELECT sab_id FROM sab_link_contacts  WHERE con_id = ' . $_SESSION['contact_login_id'] .'
                            AND (link_rsp_type IS NULL OR link_rsp_type = \'\') AND link_type = \'S\')',
            'NoOverdue' => true
         ),
         'open' => array(
            'Title' => _tk('list_all_open_sab'),
            'Link' => _tk('list_all_open_sab'),
            'Where' => 'sabs_main.sab_dclosed is null',
            'NoOverdue' => true
         ),
         'closed' => array(
            'Title' => _tk('list_all_closed_sab'),
            'Link' => _tk('list_all_closed_sab'),
            'Where' => 'sabs_main.sab_dclosed is not null',
            'NoOverdue' => true
         ),
         'alloverdue' => array(
            'OverdueWhere' => "(sab_dstart is null and sab_dclosed is null and sab_dstart_due < '" . date('Y-m-d') . "') OR
                               (sab_dend is null and sab_dclosed is null and sab_dend_due < '" . date('Y-m-d') . "') OR
                               (sabs_main.recordid IN
                                   (SELECT sab_id FROM sab_link_contacts, contacts_main
                                    WHERE sab_link_contacts.con_id = contacts_main.recordid AND link_type = 'S' AND LINK_READ_DATE is null
                                    AND sab_link_contacts.link_owner_id " . ($_SESSION["Globals"]["SAB_PERMS"] == "SAB2" ? "IS NULL" : $_SESSION["contact_login_id"]).")
                                and sab_dclosed is null and sab_read_by_due < '" . date('Y-m-d') . "')",
         ),
         'underway' => array(
            'OverdueWhere' => "sab_dstart is null and sab_dclosed is null and sab_dstart_due < '" . date('Y-m-d') . "'",
            'Title' => "deadline for action underway is overdue.",
         ),
         'completed' => array(
            'OverdueWhere' => "sab_dend is null and sab_dclosed is null and sab_dend_due < '" . date('Y-m-d') . "'",
            'Title' => "deadline for action completed is overdue.",
         ),
         'readby' => array(
            'OverdueWhere' => "sabs_main.recordid IN 
                                   (SELECT sab_id FROM sab_link_contacts, contacts_main
                                    WHERE sab_link_contacts.con_id = contacts_main.recordid AND link_type = 'S' AND LINK_READ_DATE is null
                                    AND sab_link_contacts.link_owner_id " . ($_SESSION["Globals"]["SAB_PERMS"] == "SAB2" ? "IS NULL" : " = ".$_SESSION["contact_login_id"]).")
                               and sab_dclosed is null and sab_read_by_due < '" . date('Y-m-d') . "'",
            'Title' => "deadline to be read is overdue for some contacts.",
         ),
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#SafetyAlerts/c_dx_safetyalerts_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/SafetyAlerts/c_dx_safetyalerts_guide.xml',
    'CONTACT_LINK_TABLE_ID' => array('sab_link_contacts' => 'con_id')

);

// Risk register
$ModuleDefs['RAM'] = array(
    'MOD_ID' => MOD_RISKREGISTER,
    'CODE' => 'RAM',
    'NAME' => _tk('mod_risks_title'),
    'REC_NAME' =>  _tk('RAMName'),
    'REC_NAME_PLURAL' => _tk('RAMNames'),
	'REC_NAME_TITLE' =>  _tk('RAMNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('RAMNamesTitle'),
    'FK' => 'ram_id',
    'OURREF' => 'ram_ourref',
    'OVERDUE_CHECK_FIELD' => 'ram_dcreated',
    'USES_APPROVAL_STATUSES' => true,
    'TABLE' => 'ra_main',
    'ACTION' => 'risk',
    'PERM_GLOBAL' => 'RISK_PERMS',
    'NO_LEVEL1_GLOBAL' => 'RISK_NO_OPEN',
    'MAIN_URL' => 'action=risk&table=main',
    'LIBPATH' => 'Source/risks',
    'FIELD_NAMES' => array(
        "NAME" => "ram_name",
        "HANDLER" => "ram_handler",
		"MANAGER" => "ram_responsible"
    ),
    'LOCATION_FIELD_PREFIX' => 'ram_',
    'LEVEL1_PERMS' => array('RISK1'),
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'RISK1'),
        2 => array('LEVEL' => 2, 'CODE'=>'RISK2')
    ),
    'LOGGED_OUT_LEVEL1' => true,
    'FORMCODE1' => 'RISK1',
    'FORMCODE2' => 'RISK2',
    'DEFAULT_ORDER' => 'ram_rating',
    'APPROVAL_LEVELS' => true,
    "BASIC_FORM_FILES" => array(
        1 => "Source/risks/BasicForm.php",
        2 => "Source/risks/BasicFormRISK2.php"
    ),
    'LINKCONTACTACTION' => 'linkcontactgeneral',
    'ICON' => 'icons/icon_RAM.png',
    'SEARCH_URL' => 'action=risksearch',
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_CONTACTS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
    'ASSURANCE' => array(
        "PRINOBJECT" => array("title" => "Principal objectives", "section" => "ass_ram"),
        "OBJECT" => array("title" => "Detailed objectives", "section" => "ass_object"),
        "CTRL" => array("title" => "Controls", "section" => "ass_ctrl"),
        "GCTRL" => array("title" => "Gaps in controls", "section" => "ass_gctrl"),
        "ASSURE" => array("title" => "Assurance", "section" => "ass_assure"),
        "GAPASS" => array("title" => "Gaps in assurance", "section" => "ass_gapass")
    ),
    'CONTACTTYPES' => array(
        "N" => array( "Type"=> "N", "Name"=>_tk('ram_contact'), "Plural"=>_tk('ram_contact_plural'), "None"=>_tk('no_ram_contact_plural'), "CreateNew"=>_tk('ram_contact_link'))
    ),
    'LEVEL1_CON_OPTIONS' => array(
        "R" => array("Title" => "Reporter", "Role" => GetParm('REPORTER_ROLE', 'REP'), "ActualType" => "N", "DivName" => 'contacts_type_R', 'Max' => 1),
        "N" => array("Title" => "Contact","Show" => "show_contacts_RAM", "DivName" => 'contacts_type_N')
    ),
    'AGE_AT_DATE' => 'ram_dcreated',
    'LINKED_CONTACT_LISTING_COLS' => array('recordid', 'ram_name', 'ram_dcreated', 'ram_description'),
    'SHOW_EMAIL_GLOBAL' => 'RAM_SHOW_EMAIL',
    'EMAIL_REPORTER_GLOBAL' => 'RISK_1_ACKNOWLEDGE',
    'EMAIL_HANDLER_GLOBAL' => 'RISK_EMAIL_MGR',
    'EMAIL_USER_PARAMETER' => 'RISK_STA_EMAIL_LOCS',
    'FORM_DESIGN_ARRAY_FILE' => 'UserRiskForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserRISK1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserRISK2Settings',
    'STAFF_EMPL_FILTER_MAPPINGS' => array(
        'ram_organisation' => 'con_orgcode',
        'ram_unit' => 'con_unit',
        'ram_clingroup' => 'con_clingroup',
        'ram_directorate' => 'con_directorate',
        'ram_specialty' => 'con_specialty',
        'ram_location' => 'con_loctype',
        'ram_locactual' => 'con_locactual'
    ),
    'FIELD_ARRAY' => array(
        'updateddate','ram_name','ram_risk_type','ram_risk_subtype', 'ram_organisation','ram_unit','ram_clingroup',
        'ram_directorate', 'ram_specialty','ram_assurance','ram_location','ram_locactual', 'ram_objectives',
        'ram_responsible','ram_description', 'ram_synopsis','ram_consequence','ram_likelihood', 'ram_rating',
        'ram_cur_rating','ram_after_rating','ram_level','ram_after_conseq','ram_after_likeli', 'ram_after_level',
        'ram_cur_conseq','ram_cur_likeli','ram_cur_level', 'ram_cur_cost','ram_dreview','ram_ourref','ram_dcreated',
        'ram_dclosed', 'updatedby','ram_handler','rep_approved','ram_adeq_controls', 'show_document', 'ram_last_updated'
    ),
    'TRAFFICLIGHTS_FIELDS' => array(
        'rep_approved', 'ram_level',  'ram_cur_level', 'ram_after_level', 'ram_type'
    ),
    'OVERDUE_STATUSES' => array('AWAREV', 'STCL', 'INREV', 'AWAFA', 'INFA'),
    'HOME_SCREEN_STATUS_LIST' => true,
    'GENERATE_MAPPINGS' => array(
        'CLA' => array(
            'ram_name' => 'cla_name',
            'ram_organisation' => 'cla_organisation',
            'ram_unit' => 'cla_unit',
            'ram_clingroup' => 'cla_clingroup',
            'ram_directorate' => 'cla_directorate',
            'ram_specialty' => 'cla_specialty',
            'ram_location' => 'cla_location',
            'ram_locactual' => 'cla_locactual',
            'ram_handler' => 'cla_mgr',
            'ram_description' => 'cla_synopsis',
            'rep_approved' => 'rep_approved'
        )
    ),
    'DOCUMENT_SECTION_KEY' => 'extra_document',
    'LOCAL_HELP' => 'WebHelp/index.html#RiskRegister/c_dx_riskregister_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/RiskRegister/c_dx_riskregister_guide.xml',
);

// Incidents
$ModuleDefs['INC'] = array(
    'MOD_ID' => MOD_INCIDENTS,
    'CODE' => 'INC',
    'NAME' => _tk('mod_incidents_title'),
    'REC_NAME' => _tk('INCName'),
    'REC_NAME_PLURAL' => _tk('INCNames'),
	'REC_NAME_TITLE' => _tk('INCNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('INCNamesTitle'),
    'FK' => 'inc_id',
    'USES_APPROVAL_STATUSES' => true,
    'OURREF' => 'inc_ourref',
    'OVERDUE_CHECK_FIELD' => 'inc_dreported',
    'TABLE' => 'incidents_main',
    'ACTION' => 'incident',
    'PERM_GLOBAL' => 'DIF_PERMS',
    'NO_LEVEL1_GLOBAL' => 'DIF_NO_OPEN',
    'MAIN_URL' => 'action=incident',
    'LIBPATH' => 'Source/incidents',
    'DEFAULT_ORDER' => 'inc_dincident',
    'FIELD_NAMES' => array(
        "NAME" => "inc_name",
        "HANDLER" => "inc_mgr",
		"MANAGER" => "inc_head",
        'INVESTIGATORS' => 'inc_investigator'
    ),
    'LOCATION_FIELD_PREFIX' => 'inc_',
    'LEVEL1_PERMS' => array('DIF1'),
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'DIF1'),
        2 => array('LEVEL' => 2, 'CODE'=>'DIF2')
    ),
    'EXTRA_RECORD_DATA_FUNCTIONS' => array(
        'GetYNINCMedications', 'GetYNCausalFactors'
    ),
    'EXTRA_RECORD_DATA_FUNCTION_INCLUDES' => array(
        'Source/medications/MainMedications.php', 'Source/generic/CausalFactors.php'
    ),
    'LOGGED_OUT_LEVEL1' => true,
    'FORMCODE1' => 'DIF1',
    'FORMCODE2' => 'DIF2',
    'OVERDUE_DAY_GLOBAL' => 'DIF_OVERDUE_DAYS',
    'OVERDUE_TYPE_GLOBAL' => 'DIF_OVERDUE_TYPE',
    "BASIC_FORM_FILES" => array(
        1 => "Source/incidents/BasicForm.php",
        2 => "Source/incidents/BasicFormDIF2.php"
    ),
    'LINKCONTACTACTION' => 'linkcontactgeneral',
    'ICON' => 'icons/icon_INC.png',
	'SEARCH_URL' => 'action=incidentssearch',
    'CAN_CREATE_EMAIL_TEMPLATES' => true,
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_CONTACTS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
    //todo: evaluate whether ['CONTACTTYPES']['<someletter>']['Name'] is actually used anywhere any more
    'CONTACTTYPES' => array(
        'A' => array( 'Type'=> 'A', 'Name'=>_tk('person_affected'), 'Plural'=>_tk('person_affected_plural'), 'None'=>_tk('no_person_affected_plural'), 'CreateNew'=>_tk('person_affected_link')),
        'E' => array( 'Type'=> 'E', 'Name'=>_tk('employee'), 'Plural'=>_tk('employee_plural'), 'None'=>_tk('no_employee_plural'), 'CreateNew'=>_tk('employee_link')),
        'N' => array( 'Type'=> 'N', 'Name'=>_tk('other_contact'), 'Plural'=>_tk('other_contact_plural'), 'None'=>_tk('no_other_contact_plural'), 'CreateNew'=>_tk('other_contact_link'))
    ),
    'LEVEL1_CON_OPTIONS' => array(
        'A' => array('Title' => _tk('person_affected'),'Show' => 'show_person', 'DivName' => 'contacts_type_A'),
        'E' => array('Title' => _tk('employee'), 'Show' => 'show_employee', 'DivName' => 'contacts_type_E'),
        'N' => array('Title' => _tk('other_contact'), 'Show' => 'show_other_contacts', 'DivName' => 'contacts_type_N'),
        'W' => array('Title' => 'Witness', 'Role' => 'WITN', 'Show' => 'show_witness', 'ActualType' => 'N', 'DivName' => 'contacts_type_W'),
        'R' => array('Title' => 'Reporter', 'Role' => GetParm('REPORTER_ROLE', 'REP'), 'ActualType' => 'N', 'DivName' => 'contacts_type_R', 'Max' => 1),
        'P' => array('Title' => 'Police Officer', 'ActualType' => 'N', 'DivName' => 'contacts_type_P'),
        'L' => array('Title' => 'Alleged Assailant', 'ActualType' => 'N', 'DivName' => 'contacts_type_L'),
    ),
    'AGE_AT_DATE' => 'inc_dincident',
    'LINKED_DOCUMENTS' => true,
    'DOCUMENT_SECTION_KEY' => 'extra_document',
    'LINKED_CONTACT_LISTING_COLS' => array('inc_name', 'inc_dincident', 'inc_type', 'inc_locactual', 'inc_notes'),
    'LINKED_CONTACT_ORDERBY_COLS' => array('inc_name'),
    'SHOW_EMAIL_GLOBAL' => 'DIF_SHOW_EMAIL',
    'EMAIL_REPORTER_GLOBAL' => 'DIF_1_ACKNOWLEDGE',
    'EMAIL_HANDLER_GLOBAL' => 'DIF_EMAIL_MGR',
    'EMAIL_USER_PARAMETER' => 'DIF_STA_EMAIL_LOCS',
    'FORM_DESIGN_ARRAY_FILE' => 'UserIncidentForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserDIF1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserDIF2Settings',
    'LEVEL_1_ONLY_FORM_GLOBAL' => 'DIF1_ONLY_FORM',
    'STAFF_EMPL_FILTER_MAPPINGS' => array(
        'inc_organisation' => 'con_orgcode',
        'inc_unit' => 'con_unit',
        'inc_clingroup' => 'con_clingroup',
        'inc_directorate' => 'con_directorate',
        'inc_specialty' => 'con_specialty',
        'inc_loctype' => 'con_loctype',
        'inc_locactual' => 'con_locactual'
    ),
    'FIELD_ARRAY' => array(
        'rep_approved', 'inc_mgr', 'inc_type', 'inc_category', 'inc_subcategory', 'inc_dincident', 'inc_time',
        'inc_time_band', 'inc_locactual', 'inc_unit', 'inc_unit_type', 'inc_notes', 'inc_actiontaken', 'inc_result',
        'inc_eqpt_type', 'inc_manufacturer', 'inc_serialno', 'inc_defect', 'inc_description', 'inc_supplier',
        'inc_servrecords', 'inc_model', 'inc_location', 'inc_qdef', 'inc_dmanu', 'inc_lastservice', 'inc_dputinuse',
        'inc_batchno', 'inc_cemarking', 'inc_outcomecode', 'inc_dmda', 'inc_outcome', 'inc_rep_tel', 'inc_rep_email',
        'inc_head', 'inc_impact', 'inc_inv_dstart', 'inc_inv_dcomp', 'inc_inv_outcome', 'inc_is_riddor',
        'inc_root_causes', 'inc_reportedby', 'inc_repname', 'inc_dreported', 'inc_directorate', 'inc_specialty',
        'inc_severity', 'inc_loctype', 'inc_name', 'inc_ourref', 'inc_consequence', 'inc_inv_action', 'inc_inv_lessons',
        'inc_lessons_code', 'inc_action_code', 'inc_likelihood', 'inc_grade', 'inc_clin_detail', 'inc_clintype',
        'inc_consequence_initial', 'inc_likelihood_initial', 'inc_grade_initial', 'inc_cnstitype', 'inc_carestage',
        'inc_clingroup', 'inc_cost', 'inc_inquiry', 'inc_organisation', 'inc_equipment', 'inc_dopened',
        'inc_submittedtime', 'inc_dsched', 'inc_med_stage', 'inc_med_error', 'inc_med_drug', 'inc_med_drug_rt',
        'inc_med_form', 'inc_med_form_rt', 'inc_med_dose', 'inc_med_dose_rt', 'inc_med_route', 'inc_med_route_rt',
        'inc_dnotified', 'inc_ridloc', 'inc_address', 'inc_localauth', 'inc_acctype', 'inc_riddor_ref', 'inc_riddorno',
        'inc_report_npsa', 'inc_n_effect', 'inc_n_patharm', 'inc_n_nearmiss', 'inc_n_actmin', 'inc_n_paedward',
        'inc_n_paedspec', 'inc_dnpsa', 'inc_agg_issues', 'inc_pol_crime_no', 'inc_pol_called', 'inc_pol_call_time',
        'inc_pol_attend', 'inc_pol_att_time', 'inc_pol_action', 'inc_pars_pri_type', 'inc_pars_sec_type',
        'inc_pars_clinical', 'inc_user_action', 'inc_pars_address', 'inc_postcode', 'inc_tprop_damaged',
        'inc_investigator', 'inc_n_clindir', 'inc_n_clinspec', 'inc_notify', 'inc_injury', 'inc_bodypart', 'inc_pasno1',
        'inc_pasno2', 'inc_pasno3', 'inc_rc_required', 'show_person', 'show_witness', 'show_employee',
        'show_other_contacts', 'show_equipment', 'show_document', 'show_medication', 'show_pars', 'show_assailant',
        'inc_pars_first_dexport','inc_pars_dexport', 'inc_affecting_tier_zero', 'inc_type_tier_one',
        'inc_type_tier_two', 'inc_type_tier_three','inc_level_intervention', 'inc_level_harm', 'inc_never_event',
        'inc_last_updated', 'inc_ot_q1', 'inc_ot_q2', 'inc_ot_q3', 'inc_ot_q4', 'inc_ot_q5', 'inc_ot_q6', 'inc_ot_q7',
        'inc_ot_q8', 'inc_ot_q9', 'inc_ot_q10', 'inc_ot_q11', 'inc_ot_q12', 'inc_ot_q13', 'inc_ot_q14', 'inc_ot_q15',
        'inc_ot_q16', 'inc_ot_q17', 'inc_ot_q18', 'inc_ot_q19', 'inc_ot_q20'
    ),
    'LINKED_RECORDS' => array(
        'inc_medication' => array(
            'section' => 'multi_medication',
            'type' => 'inc_medication', 'table' => 'INC_MEDICATIONS', 'recordid_field' => 'recordid', 'save_listorder' => true,
            'basic_form' => array(
                'Rows' => array(
                    'dummy_imed_search_admin',
                    'imed_name_admin',
                    'imed_brand_admin',
                    'imed_manu_admin',
                    'imed_class_admin',
                    'imed_type_admin',
                    'imed_route_admin',
                    'imed_dose_admin',
                    'imed_form_admin',
                    'imed_controlled_admin',
                    'imed_price_admin',
                    'imed_reference_admin',
                    'imed_right_wrong_medicine_admin',
                    'imed_serial_no',
                    'imed_batch_no',
                    'imed_manufacturer_special_admin',
                    'dummy_imed_search_correct',
                    'imed_name_correct',
                    'imed_brand_correct',
                    'imed_manu_correct',
                    'imed_class_correct',
                    'imed_type_correct',
                    'imed_route_correct',
                    'imed_dose_correct',
                    'imed_form_correct',
                    'imed_controlled_correct',
                    'imed_price_correct',
                    'imed_reference_correct',
                    'imed_right_wrong_medicine_correct',
                    'imed_error_stage',
                    'imed_error_type',
                    'imed_notes',
                    'imed_other_factors'
                )
            ),
            'main_recordid_label'=>'INC_ID'
        ),
        'inc_causal_factor' => array(
            'section' => 'causal_factor',
            'type' => 'inc_causal_factor', 'table' => 'causal_factors', 'recordid_field' => 'recordid', 'save_listorder' => true,
            'save_controller' => 'inc_causal_factors_linked',
            'basic_form' => array('Rows' => array('caf_level_1', 'caf_level_2')),
            'main_recordid_label' => 'INC_ID',
            'useIdentity' => true
        )
    ),
    'LINKED_MODULES' => array(
        'CON' => array('action' => 'linkcontactgeneral')
    ),
    'MEDICATION_TYPE' => 'inc_medication',
    'USE_WORKFLOWS' => true,
    'WORKFLOW_GLOBAL' => 'WORKFLOW',
    'DEFAULT_WORKFLOW' => 2,
    'APPROVAL_LEVELS' => true,
    'TRAFFICLIGHTS_FIELDS' => array(
        'rep_approved', 'inc_grade',  'inc_result', 'inc_severity', 'inc_type'
    ),
    'ADDITIONAL_COPY_LIB' => 'Source/generic_modules/INC/CopyFunctions.php',
    'ADDITIONAL_COPY_CHECK_OPTIONS' => array(
        'copy_linked_assets' => true,
        'copy_medications' => true,
        'copy_causal_factors' => true
    ),
    'ADDITIONAL_COPY_FUNCTION_CALLS' => array(
        'CopyIncidentAssets',
        'CopyIncidentMedications',
        'CopyCausalFactors'
    ),
    'OVERDUE_STATUSES' => array('AWAREV', 'STCL', 'INREV', 'AWAFA', 'INFA'),
    'HOME_SCREEN_STATUS_LIST' => true,
    // A list of all possible contacts fields that a user can search on from a DIF1 form.
    // Exact fields used in a specific installation will be set in the CON_PAS_CHK_FIELDS global
    'DIF1_CON_SEARCHING_FIELDS' => array(
        'con_title',
        'con_forenames',
        'con_surname',
        'con_jobtitle',
        'con_organisation',
        'con_email',
        'con_address',
        'con_postcode',
        'con_gender',
        'con_dob',
        'con_dod',
        'con_type',
        'con_subtype',
        'con_tel1',
        'con_tel2',
        'con_fax',
        'con_number',
        'con_nhsno',
        'con_police_number',
        'con_ethnicity',
        'con_language',
        'con_disability',
        'con_religion',
        'con_sex_orientation'
    ),
    'CAUSAL_FACTOR_TYPE' => 'inc_causal_factor',
    'GENERATE_MAPPINGS' => array(
        'CLA' => array(
            'inc_name' => 'cla_name',
            'inc_organisation' => 'cla_organisation',
            'inc_unit' => 'cla_unit',
            'inc_clingroup' => 'cla_clingroup',
            'inc_directorate' => 'cla_directorate',
            'inc_specialty' => 'cla_specialty',
            'inc_loctype' => 'cla_location',
            'inc_locactual' => 'cla_locactual',
            'inc_mgr' => 'cla_mgr',
            'inc_notes' => 'cla_synopsis',
            'rep_approved' => 'rep_approved',
            'inc_dincident' => 'cla_dincident',
            'inc_carestage' => 'cla_carestage',
            'inc_clin_detail' => 'cla_clin_detail',
            'inc_clintype' => 'cla_clintype'
        ),
        'COM' => array(
            'inc_name' => 'com_name',
            'inc_ourref' => 'com_ourref',
            'inc_organisation' => 'com_organisation',
            'inc_unit' => 'com_unit',
            'inc_clingroup' => 'com_clingroup',
            'inc_directorate' => 'com_directorate',
            'inc_specialty' => 'com_specialty',
            'inc_loctype' => 'com_loctype',
            'inc_locactual' => 'com_locactual',
            'inc_mgr' => 'com_mgr',
            'inc_notes' => 'com_detail',
            'inc_root_causes' => 'com_root_causes',
            'rep_approved' => 'rep_approved',
            'inc_dincident' => 'com_dincident',
            'inc_head' => 'com_head',
            'inc_type' => 'com_inc_type'
        )
    ),
    'CCS2_FIELDS' => array(
        'inc_affecting_tier_zero',
        'inc_type_tier_one',
        'inc_type_tier_two',
        'inc_type_tier_three',
        'inc_level_intervention',
        'inc_level_harm'
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Incidents/c_dx_incidents_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Incidents/c_dx_incidents_guide.xml',
);

// Actions
$ModuleDefs['ACT'] = array(
    'MOD_ID' => MOD_ACTIONS,
    'CODE' => 'ACT',
    'NAME' => _tk("mod_actions_title"),
    'REC_NAME' => _tk('ACTName'),
    'REC_NAME_PLURAL' => _tk('ACTNames'),
	'REC_NAME_TITLE' => _tk('ACTNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('ACTNamesTitle'),
    'FK' => 'act_id',
    'TABLE' => 'ca_actions',
    'ACTION' => 'action',
    'LIBPATH' => 'Source/actions',
    'FORMCODE' => 'ACT',
    'PERM_GLOBAL' => 'ACT_PERMS',
    'ICON' => 'icons/icon_ACT.png',
	'SEARCH_URL' => 'action=actionssearch',
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'ACT')
    ),
    'FORMCODE1' => 'ACT',
    "BASIC_FORM_FILES" => array(2 => "Source/actions/BasicForm.php"),
    'FORM_DESIGN_ARRAY_FILE' => 'UserACTForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserACTSettings',
    'FIELD_ARRAY' => array(
        'act_module', 'act_organisation', 'act_unit', 'act_clingroup', 'act_directorate', 'act_specialty',
        'act_loctype', 'act_locactual', 'act_cas_id', 'act_ddue', 'act_dstart', 'act_ddone', 'act_cost', 'act_descr',
        'act_type', 'act_priority', 'act_from_inits', 'act_to_inits', 'act_by_inits', 'act_synopsis', 'act_cost_type',
        'act_score', 'act_resources', 'act_monitoring', 'act_progress'
    ),
    'FIELD_NAMES' => array(
        "HANDLER" => "act_to_inits"
    ),
    'STAFF_EMPL_FILTER_MAPPINGS' => array(
        'act_organisation' => 'con_orgcode',
        'act_unit' => 'con_unit',
        'act_clingroup' => 'con_clingroup',
        'act_directorate' => 'con_directorate',
        'act_specialty' => 'con_specialty',
        'act_loctype' => 'con_loctype',
        'act_locactual' => 'con_locactual'
    ),
    'TRAFFICLIGHTS_FIELDS' => array(
        'act_module', 'act_priority', 'act_type'
    ),
    'CAN_CREATE_EMAIL_TEMPLATES' => true,
    'CAN_LINK_ACTIONS' => array('INC', 'RAM', 'PAL', 'COM', 'CLA', 'STN', 'ELE', 'PRO', 'CQO', 'CQP', 'CQS', 'SAB', 'AST', 'HOT', 'AMO', 'AQU'),
    'CAN_LINK_ACTIONS_CHAINS' => array('INC', 'RAM', 'PAL', 'COM', 'CLA', 'STN', 'ELE', 'PRO', 'CQO', 'CQP', 'CQS', 'SAB', 'AST', 'HOT'),
    
    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Where' => '(1=1)',
            'NoOverdue' => true
         ),
         'overdue' => array(
            'OverdueWhere' => "ca_actions.act_ddone is null and ca_actions.act_ddue is not null and ca_actions.act_ddue < '" . date('Y-m-d') . "'",
            'Title' => _tk('overdue_actions_listing')
         ),
     ),
    'LOCAL_HELP' => 'WebHelp/index.html#Actions/c_dx_actions_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Actions/c_dx_actions_guide.xml',

);

// Reports
$ModuleDefs['REP'] = array(
    'CODE' => 'REP',
    'DUMMY_MODULE' => true,
    'NAME' => 'Reports',
    'REC_NAME' => 'report',
	'REC_NAME_PLURAL' => 'reports',
	'REC_NAME_TITLE' => 'Report',
	'REC_NAME_PLURAL_TITLE' => 'Reports',
    'TABLE' => 'reports_packaged',
);

// Document templates
$ModuleDefs['TEM'] = array(
    'CODE' => 'TEM',
    'DUMMY_MODULE' => true,
    'NAME' => 'Document Templates',
    'REC_NAME' => 'document template',
	'REC_NAME_PLURAL' => 'document templates',
	'REC_NAME_TITLE' => 'Document Template',
	'REC_NAME_PLURAL_TITLE' => 'Document Templates',
    'TABLE' => 'templates_main',
);

// Dashboard
$ModuleDefs['DAS'] = array(
    'MOD_ID' => MOD_DASHBOARD,
    'CODE' => 'DAS',
    'NAME' => 'Dashboard',
    'REC_NAME' => 'dashboard',
	'REC_NAME_PLURAL' => 'dashboards',
	'REC_NAME_TITLE' => 'Dashboard',
	'REC_NAME_PLURAL_TITLE' => 'Dashboards',
    'TABLE' => 'reports_dashboards',
    'PERM_GLOBAL' => 'DAS_PERMS',
    'NO_SEC_WHERE_CLAUSE' => true,
    'LOCAL_HELP' => 'WebHelp/index.html#Dashboard/c_dx_dashboard_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Dashboard/c_dx_dashboard_guide.xml',

);

$ModuleDefs['MED'] = array(
    'MOD_ID' => MOD_MEDICATIONS,
    'CODE' => 'MED',
    'NAME' => _tk('mod_medications_title'),
    'REC_NAME' => _tk('MEDName'),
    'REC_NAME_PLURAL' => _tk('MEDNames'),
	'REC_NAME' => _tk('MEDNameTitle'),
	'REC_NAME_PLURAL' => _tk('MEDNamesTitle'),
    'FK' => 'med_id',
    'OURREF' => 'med_ourref',
    'TABLE' => 'medications_main',
    'ACTION' => 'medication',
    'PERM_GLOBAL' => 'MED_PERMS',
    'MAIN_URL' => 'action=medication',
    'LIBPATH' => 'Source/medications',
    'FIELD_NAMES' => array(
        "NAME" => "med_name"
    ),
    'DEFAULT_ORDER' => 'med_name',
    'DEFAULT_ORDER_DIRECTION' => 'ASC',
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'MED')
    ),
    'FORMCODE' => 'MED',
    'ICON' => 'icons/icon_MED.png',
    'SEARCH_URL' => 'action=medicationssearch',
    'FORMCODE1' => 'MED',
    "BASIC_FORM_FILES" => array(
        1 => "Source/medications/BasicForm.php",
        2 => "Source/medications/BasicForm.php"
    ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserMEDForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserMEDSettings',
    'FIELD_ARRAY' => array(
        'med_name', 'med_type', 'med_class', 'med_brand', 'med_manufacturer', 'med_controlled', 'med_price',
        'med_reference'
    ),
    'SEARCH_FIELD_MAPPING' => array(
        'dummy_imed_search_admin' => array(
            'dummy_imed_search_admin' => 'dummy_imed_search_admin',
            'med_name' => 'imed_name_admin',
            'med_class' => 'imed_class_admin',
            'med_brand' => 'imed_brand_admin',
            'med_manufacturer' => 'imed_manu_admin',
            'med_type' => 'imed_type_admin',
            'med_controlled' => 'imed_controlled_admin',
            'med_price' => 'imed_price_admin',
            'med_reference' => 'imed_reference_admin',
            'med_right_wrong_medicine' => 'imed_right_wrong_medicine_admin'
        ),
        'dummy_imed_search_correct' => array(
            'dummy_imed_search_correct' => 'dummy_imed_search_correct',
            'med_name' => 'imed_name_correct',
            'med_class' => 'imed_class_correct',
            'med_brand' => 'imed_brand_correct',
            'med_manufacturer' => 'imed_manu_correct',
            'med_type' => 'imed_type_correct',
            'med_controlled' => 'imed_controlled_correct',
            'med_price' => 'imed_price_correct',
            'med_reference' => 'imed_reference_correct',
            'med_right_wrong_medicine' => 'imed_right_wrong_medicine_correct',
            'med_manufacturer_special' => 'imed_manufacturer_special_admin'
        ),
    ),
    'NO_FIELD_ADDITIONS' => true,
        
    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Title' => _tk("med_listing"),
            'Where' => '(1=1)'
        )        
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Medications/c_dx_medications_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Medications/c_dx_medications_guide.xml',
);

$ModuleDefs['ADM'] = array(
    'MOD_ID' => MOD_ADMIN,
    'CODE' => 'ADM',
    'PERM_GLOBAL' => 'ADM_PERMS',
    'NAME' => _tk('mod_admin_short_title'),
    'REC_NAME' => _tk('mod_admin_short_title'),
    'ICON' => 'icons/icon_ADM.png',
    'PK' => 'recordid',
    'ACTION' => 'edituser',
    'TABLE' => 'staff',
    'NO_REP_APPROVED' => true,
    'LIBPATH' => 'Source/security',
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'ADM', 'TITLE'=>'User Forms', 'HIDECODE' => true)
    ),
    "BASIC_FORM_FILES" => array(2 => "Source/security/BasicForm.php"),
    'FORM_DESIGN_ARRAY_FILE' => 'UserADMForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserADMSettings',
    'SEARCH_URL' => 'action=search',
    'FIELD_ARRAY' => array_merge($ModuleDefs['CON']['FIELD_ARRAY'],
        array('login', 'initials', 'permission', 'lockout', 'login_tries', 'con_staff_include',
            'sta_user_type', 'sta_daccessstart', 'sta_daccessend', 'sta_pwd_change')),

    'LOCAL_HELP' => 'WebHelp/index.html#Admin/c_dx_admin_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Admin/c_dx_admin_guide.xml'
);

$ModuleDefs['TOD'] = array(
    'MOD_ID' => MOD_TODO,
    'CODE' => 'TOD',
    'PERM_GLOBAL' => 'TOD_PERMS',
    'NAME' => _tk('mod_todo_title'),
    'REC_NAME' => _tk('mod_todo_title'),
    'ICON' => 'icons/icon_ADM.png',
    'NO_SEC_WHERE_CLAUSE' => true,
    'LIBPATH' => 'Source/todo',
    'TOD_USERS_FIELDS' => array(
        'ACT' => array('act_to_inits', 'act_from_inits'),
        'INC' => array('inc_mgr', 'inc_head', 'inc_investigator'),
        'COM' => array('com_mgr', 'com_head', 'com_investigator'),
        'RAM' => array('ram_handler', 'ram_responsible')
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#ToDoList/c_dx_todolist_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/ToDoList/c_dx_todolist_guide.xml',

);



$Dir = opendir('Source/generic_modules');

while (false !== ($subdir = readdir($Dir)))
{
    if ($subdir != '.' && $subdir != '..')
    {
        if (file_exists('Source/generic_modules/'.$subdir.'/AppVars.php'))
        {
            require_once 'Source/generic_modules/'.$subdir.'/AppVars.php';
        }
    }
}

$GLOBALS['ButtonDefs'] = array(
    'btnSubmit' => array(
        'constant' => BTN_SUBMIT,
        'value' => 'Submit',
        'rbwhat' => 'Save',
        'savechanges' => true
    ),
    'btnSubmitApprove' => array(
        'constant' => BTN_SUBMIT_APPROVE, // Used when RISK2 user submits a new risk for final approval.
        'value' => 'Submit',
        'rbwhat' => 'Approve',
        'savechanges' => true
    ),
    'btnPending' => array(
        'constant' => BTN_PENDING,
        'value' => 'Save to complete later',
        'rbwhat' => 'Save',
        'savechanges' => true,
        'nomandatory' => true
    ),
    'btnSave' => array(
        'constant' => BTN_SAVE,
        'value' => 'Save',
        'rbwhat' => 'Save',
        'savechanges' => true
    ),
    'btnSaveApprove' => array(
        'constant' => BTN_SAVE_APPROVE, // Used when RISK2 open a finally approved risk and saves it.
        'value' => 'Save',
        'rbwhat' => 'Approve',
        'savechanges' => true
    ),
    'btnApprove' => array(
        'constant' => BTN_APPROVE,
        'value' => 'Approve',
        'rbwhat' => 'Approve',
        'savechanges' => true
    ),
	'btnContinue' => array(
		'constant' => BTN_CONTINUE,
		'value' => 'Continue',
		'rbwhat' => 'Search',
		'savechanges' => false,
        'nomandatory' => true
    ),
    'btnSearch' => array(
        'constant' => BTN_SEARCH,
        'value' => 'Search',
        'rbwhat' => 'Search',
        'savechanges' => false,
        'nomandatory' => true
    ),
    'btnCancel' => array(
        'constant' => BTN_CANCEL,
        'value' => _tk('btn_cancel'),
        'rbwhat' => 'Cancel',
        'savechanges' => false,
        'nomandatory' => true
    ),
    'btnRestore' => array(
        'constant' => BTN_RESTORE,
        'value' => 'Restore to holding area',
        'rbwhat' => 'Restore',
        'savechanges' => false,
        'nomandatory' => true
    ),
    'btnFinish' => array(
        'constant' => BTN_FINISH,
        'value' => 'Finish',
        'rbwhat' => 'Cancel',
        'savechanges' => false,
        'nomandatory' => true
    ),
    'btnReject' => array(
        'constant' => BTN_REJECT,
        'value' => 'Reject',
        'rbwhat' => 'Reject',
        'savechanges' => false,
        'nomandatory' => true
    )
);

$GLOBALS['ModuleDefs'] = $ModuleDefs;
$GLOBALS['ModuleListOrder'] = array('INC', 'RAM', 'PAL', 'COM', 'CLA', 'STN', 'ELE', 'PRO', 'CQO', 'CQP', 'CQS', 'LOC', 'LIB', 'SAB', 'ACT', 'CON', 'ORG', 'AST', 'DST', 'MED', 'PAY', 'POL', 'HSA', 'HOT', 'ATM', 'ATQ', 'ATI', 'AMO', 'AQU', 'ADM');

/**
* @desc Enters a particular access level definition into the global $AccessLvlDefs;
*
* @param string $name The global in which this access level is stored
* @param string $value The code which represents the access level
* @param string $module The current module
* @param string $title The description of this access level.
* @param int $order The order this access level will appear relative to the others for the same module (1 = "lowest" access).
*
* */
function DefineAccessLevel($name, $value, $module, $title, $order)
{
	global $AccessLvlDefs;
	$AccessLvlDefs[$name][$value] = array("module" => $module, "title" => $title, "order" => $order);
}

if ($_SESSION['AccessLvlDefs'])
{
    $AccessLvlDefs = $_SESSION['AccessLvlDefs'];
}
else
{
    $AccessLvlDefs = array();
    //Get access levels from access_levels table
    $sql = 'SELECT DISTINCT acl_module FROM ACCESS_LEVELS';
    $result = PDO_fetch_all($sql);

    foreach ($result as $row)
    {
        $ModuleWorkflows[GetWorkflowID($row['acl_module'])][] = $row['acl_module'];
    }

    if (is_array($ModuleWorkflows) && !empty($ModuleWorkflows))
    {
        foreach ($ModuleWorkflows as $Workflow => $Modules)
        {
            $ModuleWorkflowsWhere[] = '(acl_module IN (\''.implode('\',\'', $Modules).'\') AND acl_workflow = '.$Workflow.')';
        }

        $sql = 'SELECT acl_module, acl_code, acl_description, acl_order FROM ACCESS_LEVELS WHERE '.implode(' OR ', $ModuleWorkflowsWhere).' ORDER BY acl_order';
        $result = PDO_fetch_all($sql);

        foreach ($result as $row)
        {
            DefineAccessLevel($ModuleDefs[$row['acl_module']]['PERM_GLOBAL'], $row['acl_code'], $row['acl_module'], _tk($row['acl_description']), $row['acl_order']);
        }

        $_SESSION['AccessLvlDefs'] = $AccessLvlDefs;
    }
}