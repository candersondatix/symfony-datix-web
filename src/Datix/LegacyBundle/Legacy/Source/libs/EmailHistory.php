<?php
use src\users\model\User;
use src\users\model\UserModelFactory;
use src\framework\registry\Registry;

function SectionFeedbacktoReporter($module, $data, $FormType)
{
    global $ModuleDefs, $scripturl, $MinifierDisabled;

    $registry = Registry::getInstance();
    $showJobTitle = $registry->getParm('STAFF_NAME_DROPDOWN', 'N');

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $recordid = $data["recordid"];

    $inc = $data; // some feedback templates will use $inc[...]

    $Perms = GetParm($ModuleDefs[$module]['PERM_GLOBAL']);

    $bHideContacts = false;

    $EmailTo = array();

    $feedbackFormDesign = \Forms_FormDesign::GetFormDesign([
        'module' => $module,
        'level' => 2
    ]);

    echo'
<script language="JavaScript" type="text/javascript">
    FeedbackValidationNeeded = true;
</script>
';

    // work out which permission-sets can see which contacts.
    if (!CanSeeContacts('INC', $Perms, $data['rep_approved']))
    {
        $bHideContacts = true;
    }

    //These two lines are legacy from when you could send feedback from readonly forms. Can be factored out now that this functionality has been removed.
    $FeedbackFormType = ($FormType == 'Print' ? $FormType : 'Edit');
    $RecipientsTable = new FormTable($FeedbackFormType, $module);

    if ((is_array($feedbackFormDesign->ReadOnlyFields) && array_key_exists('feedback', $feedbackFormDesign->ReadOnlyFields)) || $FormType == 'ReadOnly' || $bHideContacts === true)
    {
        $ReadOnly = true;
    }

    ///////////////////////////////////////
    /// Feedback //////////////////////////
    ///////////////////////////////////////

    if (!$ReadOnly)
    {
        // Display recipients subtitle
        echo '
        <li class="section_title_row" id="recipients_title_row" name="recipients_title_row">
            <div class="section_title_group">
                <div class="section_title">'._tk('recipients').'</div>
            </div>
        </li>
        ';

        if ($data[$ModuleDefs[$module]['FIELD_NAMES']['HANDLER']])
        {
            $Factory = new UserModelFactory();
            $Handler = $Factory->getMapper()->findByInitials($data[$ModuleDefs[$module]['FIELD_NAMES']['HANDLER']]);

            if (null !== $Handler && validEmailIsAvailable($Handler))
            {
                $EmailTo[$Handler->recordid] = ($showJobTitle == 'B' && $Handler->con_jobtitle != '' ? $Handler->con_jobtitle . ' - ' : '') .  '[Handler] ' . $Handler->con_surname . ', ' . $Handler->con_forenames . ' ' . $Handler->con_title . ($showJobTitle == 'A' && $Handler->con_jobtitle != '' ? ' - ' . $Handler->con_jobtitle : '');
            }
        }

        $fb_sentObj = new FormField($FeedbackFormType);

        if ($_GET["feedbackmessage"])
        {
            $CustomField = '<font color=\'red\'><b>' . _tk('feedback_sent')
                . '</b></font>';
            $RecipientsTable->MakeTitleRow($CustomField, "windowbg2");
        }

        if ($_GET["feedbackerrormessage"])
        {
            $CustomField = '<font color=\'red\'><b>' . _tk('feedback_err')
                . '</b></font>';
            $RecipientsTable->MakeTitleRow($CustomField, "windowbg2");
        }

        if ($recordid && (!$bHideContacts && $Perms != "DIF2_READ_ONLY"))
        {
            $sql = "select recordid as con_id, link_role, con_email, con_surname, con_forenames, con_title, fullname, con_jobtitle
                    from contacts_main, link_contacts
                    where contacts_main.recordid = link_contacts.con_id
                    AND con_email != '' AND con_email IS NOT NULL
                    AND link_contacts.".$ModuleDefs[$module]['FK']." = :recordid
                    AND contacts_main.rep_approved = :rep_approved
                    AND con_dod IS NULL";

            $contacts = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $recordid, 'rep_approved' => 'FA'));
            $reporterAdded = false;

            foreach ($contacts as $row)
            {
                if ($row["link_role"])
                {
                    if ($row["link_role"] == GetParm('REPORTER_ROLE', 'REP'))
                    {
                        $role = 'Reporter';
                    }
                }

                if (validEmailIsAvailable($row))
                {
                    $EmailTo[$row["con_id"]] = ($showJobTitle == 'B' && $row['con_jobtitle'] != '' ? $row['con_jobtitle'] . ' - ' : '') . ($role ? '['.$role.'] ' : ''). $row["con_surname"] . ', ' . $row["con_forenames"] . ' ' . $row["con_title"] . ($showJobTitle == 'A' && $row['con_jobtitle'] != '' ? ' - ' . $row['con_jobtitle'] : '');
                    if ($role == 'Reporter')
                    {
                        $reporterAdded = true;
                    }
                }
            }
            //get reporter details
            if ($module == 'INC' && $reporterAdded == false)
            {
                if ($data["inc_rep_email"] && !$fb["email"] && !$bHideContacts && $Perms != "DIF2_READ_ONLY")
                {
                    if (validEmailIsAvailable(['con_email' => $data["inc_rep_email"], 'fullname' => $data["inc_repname"]]))
                    {
                        $EmailTo[$data["inc_rep_email"]] = ($showJobTitle == 'B' && $data['inc_reportedby'] != '' ? $data['inc_reportedby'] . ' - ' : '') . '[Reporter] ' . $data["inc_repname"] . ($showJobTitle == 'A' && $data['inc_reportedby'] != '' ? ' - ' . $data['inc_reportedby'] : '');
                    }
                }
            }

            $sql = '
                SELECT
                    recordid,
                    email,
                    sta_surname,
                    sta_forenames,
                    sta_title,
                    jobtitle
                FROM
                    staff
            ';

            $Where[] = '
                (email != \'\' AND email IS NOT NULL)
            AND
                (con_staff_include IS NULL OR con_staff_include = \'Y\' OR con_staff_include = \'\')
            AND
                (sta_daccessend IS NULL OR sta_daccessend > GETDATE())
            AND
                (sta_daccessstart IS NULL OR sta_daccessstart <= GETDATE())
            AND
                (con_dclosed IS NULL OR con_dclosed > GETDATE())
            AND
                REP_APPROVED = \'FA\'';

            $WhereClause = MakeSecurityWhereClause($Where, 'ADM');

            $sql .= 'WHERE ' . $WhereClause . ' ORDER BY sta_surname ASC';

            $Result = DatixDBQuery::PDO_fetch_all($sql, array());

            $GlobalAddressBook = array();

            foreach ($Result as $Contact)
            {
                $GlobalAddressBook[$Contact['recordid']] = ($showJobTitle == 'B' && $Contact['jobtitle'] != '' ? $Contact['jobtitle'] . ' - ' : '') . $Contact['sta_surname'] . ', ' . $Contact['sta_forenames'] . ' ' . $Contact['sta_title'] . ($showJobTitle == 'A' && $Contact['jobtitle'] != '' ? ' - ' . $Contact['jobtitle'] : '');
            }
        }

        if (!$bHideContacts && $Perms != "DIF2_READ_ONLY")
        {
            // Staff and contacts attached to this record
            $staffField = Forms_SelectFieldFactory::createSelectField('fbk_to', $module, '', $FeedbackFormType, true);
            $staffField->setCustomCodes($EmailTo);
            $staffField->setSuppressCodeDisplay();
            $staffField->setIgnoreMaxLength();

            $staffHtml = '<label for="fbk_to_title"><b>'.($feedbackFormDesign->UserLabels['dum_fbk_to'] ?: _tk('staff_contacts')).'</b></label>';

            if (isset($feedbackFormDesign->HelpTexts['dum_fbk_to']) && $feedbackFormDesign->HelpTexts['dum_fbk_to'] != '')
            {
                $label = ($feedbackFormDesign->UserLabels['dum_fbk_to'] ?: _tk('staff_contacts'));
                $label = str_replace('"', '&quot;',$label);
                $label = str_replace('\'', '\\\'',$label);

                $staffHtml .= '&nbsp;
                <a href="javascript:PopupDivFromURL(\'help\', \''._tk('datix_help_title').$label.'\', \''
                    .$scripturl . '?action=fieldhelp&module='.$module.'&field=dum_fbk_to&level=2&parent_module=&link_type=&parent_level=2&id='.$feedbackFormDesign->GetID()
                    .'\', \'\', [{\'value\':\''._tk('btn_help_close').'\',\'onclick\':\'GetFloatingDiv(\\\'help\\\').CloseFloatingControl()\'}], \''.DROPDOWN_WIDTH_DEFAULT.'px\')">
                    <img id="dum_fbk_to_help_image" src="images/Help.gif" style="cursor:pointer;border:0" alt="'._tk('help_alt').' ('.$label.')" />
                </a>';
            }

            $staffHtml .= '<br/ >'.$feedbackFormDesign->UserExtraText['dum_fbk_to'];

            // All users
            $allUsersField = Forms_SelectFieldFactory::createSelectField('fbk_gab', $module, '', $FeedbackFormType, true);
            $allUsersField->setCustomCodes($GlobalAddressBook);
            $allUsersField->setSuppressCodeDisplay();
            $allUsersField->setIgnoreMaxLength();

            $allUsersHtml = '<label for="fbk_gab_title"><b>'.($feedbackFormDesign->UserLabels['dum_fbk_gab'] ?: _tk('all_users')).'</b></label>';

            if (isset($feedbackFormDesign->HelpTexts['dum_fbk_gab']) && $feedbackFormDesign->HelpTexts['dum_fbk_gab'] != '')
            {
                $label = ($feedbackFormDesign->UserLabels['dum_fbk_gab'] ?: _tk('all_users'));
                $label = str_replace('"', '&quot;',$label);
                $label = str_replace('\'', '\\\'',$label);

                $allUsersHtml .= '&nbsp;
                <a href="javascript:PopupDivFromURL(\'help\', \''._tk('datix_help_title').$label.'\', \''
                    .$scripturl . '?action=fieldhelp&module='.$module.'&field=dum_fbk_gab&level=2&parent_module=&link_type=&parent_level=2&id='.$feedbackFormDesign->GetID()
                    .'\', \'\', [{\'value\':\''._tk('btn_help_close').'\',\'onclick\':\'GetFloatingDiv(\\\'help\\\').CloseFloatingControl()\'}], \''.DROPDOWN_WIDTH_DEFAULT.'px\')">
                    <img id="dum_fbk_gab_help_image" src="images/Help.gif" style="cursor:pointer;border:0" alt="'._tk('help_alt').' ('.$label.')" />
                </a>';
            }

            $allUsersHtml .= '<br />'.$feedbackFormDesign->UserExtraText['dum_fbk_gab'];

            // Additional recipients
            $additionalRecipientsHtml = '<label for="fbk_email"><b>'.($feedbackFormDesign->UserLabels['dum_fbk_email'] ?: _tk('additional_recipients')).'</b></label>';

            if (isset($feedbackFormDesign->HelpTexts['dum_fbk_email']) && $feedbackFormDesign->HelpTexts['dum_fbk_email'] != '')
            {
                $label = ($feedbackFormDesign->UserLabels['dum_fbk_email'] ?: _tk('additional_recipients'));
                $label = str_replace('"', '&quot;',$label);
                $label = str_replace('\'', '\\\'',$label);

                $additionalRecipientsHtml .= '&nbsp;
                <a href="javascript:PopupDivFromURL(\'help\', \''._tk('datix_help_title').$label.'\', \''
                    .$scripturl . '?action=fieldhelp&module='.$module.'&field=dum_fbk_email&level=2&parent_module=&link_type=&parent_level=2&id='.$feedbackFormDesign->GetID()
                    .'\', \'\', [{\'value\':\''._tk('btn_help_close').'\',\'onclick\':\'GetFloatingDiv(\\\'help\\\').CloseFloatingControl()\'}], \''.DROPDOWN_WIDTH_DEFAULT.'px\')">
                    <img id="dum_fbk_email_help_image" src="images/Help.gif" style="cursor:pointer;border:0" alt="'._tk('help_alt').' ('.$label.')" />
                </a>';
            }

            $additionalRecipientsHtml .= '<br />'.$feedbackFormDesign->UserExtraText['dum_fbk_email'];

            $additionalRecipientsField = $fb_sentObj->MakeEmailField("fbk_email", 68, 200, "");

            $fieldOrders = [
                'dum_fbk_to'    => [
                    'html' => $staffHtml,
                    'field' => $staffField
                ],
                'dum_fbk_gab'   => [
                    'html' => $allUsersHtml,
                    'field' => $allUsersField
                ],
                'dum_fbk_email' => [
                    'html' => $additionalRecipientsHtml,
                    'field' => $additionalRecipientsField
                ]
            ];

            $formDesignFieldOrders = $feedbackFormDesign->FieldOrders['feedback'];

            $ordered = [];

            foreach ($formDesignFieldOrders as $key)
            {
                if (array_key_exists($key, $fieldOrders))
                {
                    $ordered[$key] = $fieldOrders[$key];
                    unset($fieldOrders[$key]);
                }
            }

            $orderedFields = $ordered + $fieldOrders;

            foreach ($orderedFields as $orderedFieldKey => $orderedField)
            {
                if (!$feedbackFormDesign->HideFields[$orderedFieldKey])
                {
                    $RecipientsTable->MakeRow($orderedField['html'], $orderedField['field']);
                }
            }
        }
        else
        {
            $fb_sentObj->MakeCustomField('<input type="hidden" name="fbk_to" id="fbk_to" value="' . $row["con_id"]
                . '" />'. $EmailTo[$row["con_id"]]);
            $RecipientsTable->MakeRow('<b>'._tk('staff_contacts').'</b>', $fb_sentObj);
        }

        if (!$_GET["print"])
        {
            echo $RecipientsTable->GetFormTable();
        }

        // Display message subtitle
        echo '
            <li class="section_title_row" id="message_title_row" name="message_title_row">
                <div class="section_title_group">
                    <div class="section_title">'._tk('message_title').'</div>
                </div>
            </li>
        ';

        $MessageTable = new FormTable($FeedbackFormType, $module); //Even if read only, want to be able to send messages.

        require_once "Source/libs/EmailTemplates.php";
        $MessageTable->Contents .= '<script language="javascript" src="js_functions/email' . $addMinExtension . '.js"></script>';

        if (bYN(GetParm('EMAIL_TEMPLATES','N')) && $row["recordid"])
        {
            require_once 'Source/libs/EmailTemplateClass.php';

            $INCFeedbackTemplate = new NewEmailTemplate($row["recordid"]);

            $INCFeedbackTemplate->ConstructEmailToSend($data["recordid"]);

            $EmailText["Feedback"]["Subject"] = $INCFeedbackTemplate->EmailSubject;
            $EmailText["Feedback"]["Body"] = $INCFeedbackTemplate->EmailBody;
        }
        else
        {
            $OldEmailVersion = true;
        }

        if ($OldEmailVersion)
        {
            require_once "Source/libs/Email.php";

            if ($EmailTextFile = getUserEmailTextFile($module))
            {
                include $EmailTextFile;
            }
            elseif (file_exists("Source/EmailText_$module.php"))
            {
                include "Source/EmailText_$module.php";
            }
        }

        $html = '<label for="fbk_subject"><b>'.($feedbackFormDesign->UserLabels['dum_fbk_subject'] ?: _tk('subject')).'</b></label>';

        if (isset($feedbackFormDesign->HelpTexts['dum_fbk_subject']) && $feedbackFormDesign->HelpTexts['dum_fbk_subject'] != '')
        {
            $label = ($feedbackFormDesign->UserLabels['dum_fbk_subject'] ?: _tk('subject'));
            $label = str_replace('"', '&quot;',$label);
            $label = str_replace('\'', '\\\'',$label);

            $html .= '&nbsp;
            <a href="javascript:PopupDivFromURL(\'help\', \''._tk('datix_help_title').$label.'\', \''
                .$scripturl . '?action=fieldhelp&module='.$module.'&field=dum_fbk_subject&level=2&parent_module=&link_type=&parent_level=2&id='.$feedbackFormDesign->GetID()
                .'\', \'\', [{\'value\':\''._tk('btn_help_close').'\',\'onclick\':\'GetFloatingDiv(\\\'help\\\').CloseFloatingControl()\'}], \''.DROPDOWN_WIDTH_DEFAULT.'px\')">
                <img id="dum_fbk_subject_help_image" src="images/Help.gif" style="cursor:pointer;border:0" alt="'._tk('help_alt').' ('.$label.')" />
            </a>';
        }

        $html .= '<br />'.$feedbackFormDesign->UserExtraText['dum_fbk_subject'];

        $MessageTable->MakeRow($html, $fb_sentObj->MakeInputField('fbk_subject', 68, 80, $EmailText["Feedback"]["Subject"]));

        $ChangedField = new FormField;
        $ChangedField->MakeChangedFlagField('fbk_body');
        $fb_sentObj->MakeTextAreaField('fbk_body', 10, 70, "", $EmailText["Feedback"]["Body"]);
        $fb_sentObj->ConcatFields($fb_sentObj, $ChangedField);

        $html = '<label for="fbk_body"><b>'.($feedbackFormDesign->UserLabels['dum_fbk_body'] ?: 'Body of message').'</b></label>';

        if (isset($feedbackFormDesign->HelpTexts['dum_fbk_body']) && $feedbackFormDesign->HelpTexts['dum_fbk_body'] != '')
        {
            $label = ($feedbackFormDesign->UserLabels['dum_fbk_body'] ?: 'Body of message');
            $label = str_replace('"', '&quot;',$label);
            $label = str_replace('\'', '\\\'',$label);

            $html .= '&nbsp;
            <a href="javascript:PopupDivFromURL(\'help\', \''._tk('datix_help_title').$label.'\', \''
                .$scripturl . '?action=fieldhelp&module='.$module.'&field=dum_fbk_body&level=2&parent_module=&link_type=&parent_level=2&id='.$feedbackFormDesign->GetID()
                .'\', \'\', [{\'value\':\''._tk('btn_help_close').'\',\'onclick\':\'GetFloatingDiv(\\\'help\\\').CloseFloatingControl()\'}], \''.DROPDOWN_WIDTH_DEFAULT.'px\')">
                <img id="dum_fbk_body_help_image" src="images/Help.gif" style="cursor:pointer;border:0" alt="'._tk('help_alt').' ('.$label.')" />
            </a>';
        }

        $html .= '<br />'.$feedbackFormDesign->UserExtraText['dum_fbk_body'];

        $MessageTable->MakeRow($html, $fb_sentObj);

        //TODO: when refactoring, it would be good to be able to add hidden fields easily using FormField without having to write some custom html to inject in
        if ($FormType != "Print" && !$_GET["print"])
        {
            $hiddenField = '<input type="hidden" id="fbk_html" name="fbk_html" value="' . ($EmailText['Feedback']['HTML'] ? 'true' : 'false') . '" />';
            $MessageTable->MakeTitleRow($hiddenField, "");

            $CustomField = '

            <div class="send_email_btn_div">
                <input type="button" class="send_email_btn" name="Feedback" id="feedback_btn" value="'
                . _tk("btn_send_message") . '" '.($INCFeedbackTemplate->duplicates_exist?'disabled="true"':'').'
            onclick="SendFeedback(\''.$module.'\','.$recordid.')" />';

            $CustomField .= '
            </div>
            <div id="ajax_email_status" class="email_status_div" style="display:none">Sending feedback...</div>';
            $MessageTable->MakeTitleRow($CustomField, "new_windowbg");
        }

        if (!$_GET["print"])
        {
            echo $MessageTable->GetFormTable();
        }
    }

    ///////////////////////////////
    ///// History /////////////////
    ///////////////////////////////

    if ($recordid)
    {
        $link_id = abs($recordid);

        $sql = "SELECT emh_body, emh_email, emh_dsent, emh_type, con_id, emh_from_id
                  FROM email_history
                  WHERE mod_id = :mod_id
                  AND link_id = :link_id
                  AND emh_type like :emh_type
                  AND (emh_not_sent != 1 OR emh_not_sent IS NULL)
                  ORDER BY emh_dsent DESC";

        $fb = DatixDBQuery::PDO_fetch_all($sql, array('mod_id' => GetModIDFromShortName($module), 'link_id' => $link_id, 'emh_type' => 'FBK_%'));

        $fb_histObj = new FormField("Print");

            echo '
    <li>
<table class="gridlines" cellspacing="1" cellpadding="4" width="99%" align="center" border="0">
    <tr>
        <td class="titlebg" colspan="4">
        <b>' . _tk("message_history_title") . '</b>
        </td>
    </tr>
    <tr>
        <td class="windowbg">
        <b>' . _tk("date_time_header") . '</b>
        </td>
        <td class="windowbg">
        <b>' . _tk("sender") . '</b>
        </td>
        <td class="windowbg">
        <b>' . _tk("recipient") . '</b>
        </td>
        <td class="windowbg">
        <b>' . _tk("body_of_message_header") . '</b>
        </td>
    </tr>
    ';

        if (!$fb)
        {
            echo '
    <tr>
        <td class="windowbg2" colspan="4">
        <b>' . _tk("no_messages_for_inc") . '</b>
        </td>
    </tr>';
        }
        elseif (is_array($fb))
        {
            foreach ($fb as $history)
            {
                if ($history["emh_from_id"])
                {
                    $sql = 'select con_email, con_surname, con_forenames, con_title
                                     from contacts_main
                                     where recordid = :recordid';
                    $sender = DatixDBQuery::PDO_fetch($sql, array('recordid' => $history['emh_from_id']));
                }

                if ($sender["con_forenames"] || $sender["con_title"])
                {
                    $sender_name = "$sender[con_surname], $sender[con_forenames] $sender[con_title]";
                }
                elseif ($sender["con_surname"])
                {
                    $sender_name = "$sender[con_surname]";
                }
                else
                {
                    $sender_name = "";
                }

                $sender = "";

                if ($history["con_id"])
                {
                    $sql = "select con_email, con_surname, con_forenames, con_title
                                     from contacts_main
                                     where recordid = :recordid";
                    $recipient = DatixDBQuery::PDO_fetch($sql, array('recordid' => $history['con_id']));
                }

                if ($recipient["con_forenames"] || $recipient["con_title"])
                {
                    $recipient_name = "$recipient[con_surname], $recipient[con_forenames] $recipient[con_title]";
                }
                elseif ($recipient["con_surname"])
                {
                    $recipient_name = "$recipient[con_surname]";
                }
                elseif ($history["emh_email"])
                {
                    $recipient_name = str_replace(",",", ",$history["emh_email"]);
                }
                else
                {
                    $recipient_name = "";
                }

                $recipient = "";

                echo "
                <tr>
                    <td class='windowbg2' valign='top' nowrap='nowrap' align='right'>".FormatDateVal($history['emh_dsent'], true)."</td>
                    <td class='windowbg2' valign='top'>".\src\security\Escaper::escapeForHTML($sender_name)."</td>
                    <td class='windowbg2' valign='top' style='word-break:break-all;'>".\src\security\Escaper::escapeForHTML($recipient_name)."</td>
                    <td class='windowbg2' valign='middle' style='word-break:break-all;'>".\src\security\Escaper::escapeForHTML($history['emh_body'])."</td>
                </tr>";
            }
        }

        echo "</table></li>";
    }

    if($Perms == "DIF2_READ_ONLY")
    {
        $FormType = "ReadOnly";
    }
}

/**
 * Determines whether a contact has at least one valid e-mail address.
 * 
 * @param User|array $contact
 * 
 * @return boolean
 */
function validEmailIsAvailable($contact)
{
    require_once __DIR__.'/EmailDomainWhitelistClass.php';
    $whitelist   = new EmailDomainWhitelist();
    $logMessages = [];
    
    if ($contact instanceof User)
    {
        $emails = $contact->con_email;
        $name   = $contact->fullname;
    }
    else
    {
        $emails = $contact['con_email'];
        $name   = $contact['fullname'];
    }
    
    foreach (explode(',', $emails) as $email)
    {
        try 
        {
            if ($whitelist->isEmailAllowed($email))
            {
                return true;
            }
            else
            {
                $logMessages[] = 'Email domain is not permitted ('.$email.')';
            }
        }
        catch (Exception $e)
        {
            $logMessages[] = $e->getMessage().' ('.$email.')';
        }
    }
    
    $logger = src\framework\registry\Registry::getInstance()->getLogger();
    $logger->logInfo('Contact "'.$name.'" excluded from Communication and Feedback section.  Reason: '.implode(';', $logMessages));
    
    return false;
}

?>
