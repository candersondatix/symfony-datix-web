<?php

require_once 'Source/libs/ListingClass.php';
//needed because contact listings have some weird custom behaviour.

class ContactListing extends Listing
{
    public $ShowFullContactDetails = false;

    /**
    * @desc Constructs HTML representation of the listing as a table. Currently using copy-and-pasted code from browselist,
    * this will expand and refine in time.
    *
    * @param string $formType The current form mode.
    *
    * @return string HTML representation of the listing.
    *
    * @codeCoverageIgnoreStart
    * No unit test, since it deals with constructing HTML (which will, in this case, evolve regularly in the future),
    * rather than providing any testable return value.
    */
    function GetListingHTML($formType = '')
    {
        global $FieldDefs, $FieldDefsExtra, $scripturl, $ModuleDefs;

        if(!CanAccessContacts() || $formType == 'Print')
        {
            $this->ReadOnly = true;
        }

        if (count($this->Columns) == 0)
        {
            return '<div class="padded_div"><b>This listing has no columns. Please contact your Datix administrator</b></div>';
        }

        if (count($this->Data) == 0)
        {
            return '<div class="padded_div windowbg2"><b>'.$this->EmptyMessage.'</b></div>';
        }

        $HTML = '<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';

        $HTML .= '<tr name="element_section_row" id="element_section_row" class="tableHeader head2">'; //title row

        if($this->ShowFullContactDetails)
        {
            $HTML .= '<td class="windowbg" width="1%"></td>';
        }
        if ($this->Checkbox)
        {
             $HTML .= '<th class="windowbg" width="1%">
                <input type="checkbox" id = "check_all_checkbox" name="check_all_checkbox" onclick="ToggleCheckAll(\'checkbox_id_' . $this->CheckboxType . '\', this.checked)"/>
            </th>';
        }

        foreach ($this->Columns as $col_field => $col_info)
        {
            $HTML .= '<th class="windowbg"' . ($col_info['width'] ? 'width="' . $col_info['width'] . '%"' : '') . '>';

            if ($FieldDefs[$this->Module][$col_field]['Type'] != 'textarea' && $this->Sortable && !$col_info['custom'])
            {
                $HTML .= '<a href="Javascript:sortList(\'' . $col_field . '\');">';
            }
            if ($col_field == "mod_title")
            {
                $currentCols["mod_title"] = "Module";
            }
            elseif ($col_field == "recordid")
            {
                if ($col_info_extra['colrename'])
                {
                    $currentCols[$col_field] = $col_info_extra['colrename'];
                }
                else
                {
                    $currentCols[$col_field] = GetColumnLabel($col_field, $FieldDefsExtra[$this->Module][$col_field]["Title"], false, '', 'CON');
                }
            }
            elseif($col_info['title'])
            {
                $currentCols[$col_field] = $col_info['title'];
            }
            else
            {
                $currentCols[$col_field] = GetColumnLabel($col_field, $FieldDefsExtra[$this->Module][$col_field]["Title"]);
            }

            $HTML .= $currentCols[$col_field];

            if ($FieldDefs[$this->Module][$col_field]['Type'] != 'textarea' && $this->Sortable && !$col_info['custom'])
            {
                $HTML .= '</a>';
            }

            $HTML .= '</th>';
        }

        $HTML .= '</tr>';

        foreach ($this->Data as $row)
        {
            $record_url = $this->getRecordUrl($row);

            $HTML .= '<tr class="listing-row">';

            if($this->ShowFullContactDetails)
            {
                $HTML .= '<td class="windowbg2"><a href="javascript:ToggleTwistyExpand(\'contact_'.$row["con_id"].'\',\'image_'.$row["con_id"].'\',\'Images/expand_windowbg2.gif\',\'Images/collapse_windowbg2.gif\')">
                    <img id=\'image_'.$row["con_id"].'\' src="Images/expand_windowbg2.gif" alt="+" border="0"/></a></td>';
            }
            if ($this->Checkbox)
            {
                $HTML .= '<td class="windowbg2" width="1%">
                        <input type="checkbox" id="checkbox_id_' . $this->CheckboxType . '" name="checkbox_include_' . $row[$this->CheckboxIDField] . '" onclick="" />
                        </td>';
            }

            foreach ($this->Columns as $col_field => $col_info )
            {
                if($col_info['type'])
                {
                    $FieldType = $col_info['type'];
                }
                else
                {
                    $FieldType = $FieldDefsExtra[$this->Module][$col_field]['Type'];
                }

                // special columns
                if ($col_field == 'ram_cur_level' || $col_field == 'ram_after_level' || $col_field == 'ram_level')
                {
                    if ($col_field == "ram_cur_level")
                    {
                        $col = $row["ram_cur_level"];
                    }
                    if ($col_field == "ram_after_level")
                    {
                        $col = $row["ram_after_level"];
                    }
                    if ($col_field == "ram_level")
                    {
                        $col = $row["ram_level"];
                    }

                    if (bYN(GetParm('RISK_MATRIX', 'N')))
                    {
                        $sql = 'SELECT cod_web_colour, description FROM code_inc_grades WHERE code = :code';
                    }
                    else
                    {
                        $sql = 'SELECT cod_web_colour, description FROM code_ra_levels WHERE code = :code';
                    }

                    $level = \DatixDBQuery::PDO_fetch($sql, array('code' => $col));

                    if (!isset($level['description']))
                    {
                        $level['description'] = $col;
                    }

                    $colour = $level["cod_web_colour"];

                    $HTML .= '<td align="left" '.(!empty($colour) ? 'class="windowbg2">' : 'bgcolor="#'.$colour.'">').$level["description"].'</td>';
                }
                elseif ($col_field == 'inc_grade')
                {
                    // Get grade map information
                    $sql = 'SELECT cod_web_colour, description FROM code_inc_grades WHERE code = :code';

                    $incgrade = DatixDBQuery::PDO_fetch($sql, array('code' => $row['inc_grade']));

                    if (!empty($incgrade))
                    {
                        $HTML .= '<td align="left" bgcolor="#' . $incgrade["cod_web_colour"] . '">' . $incgrade["description"] . '</td>';
                    }
                    else
                    {
                        $HTML .= '<td align="left" class="rowfieldbg">' . $row['inc_grade'] . ' </td>';
                    }
                }
                // regular columns
                else
                {
                    if ($FieldType == 'ff_select' || $FieldType == 'multilistbox')
                    {
                        if($this->Module == 'CON' && \UnicodeString::substr($col_field, 0, 5) == 'link_')
                        {
                            //hack needed because we store link fields under 'INC' rather than 'CON'
                            if ($FieldType == 'ff_select')
                            {
                                $codeinfo = get_code_info('INC', $col_field, $row[$col_field]);
                            }
                            else if ($FieldType == 'multilistbox')
                            {
                                 $codeinfo['description'] = GetCodeDescriptions('INC', $col_field, $row[$col_field], '', ', ');
                            }
                        }
                        else
                        {
                            if ($FieldType == 'ff_select')
                            {
                                $codeinfo = get_code_info($this->Module, $col_field, $row[$col_field]);
                            }
                            else if ($FieldType == 'multilistbox')
                            {
                                 $codeinfo['description'] = GetCodeDescriptions($this->Module, $col_field, $row[$col_field], '', ', ');
                            }
                        }

                        $colour = '';

                        if ($codeinfo["cod_web_colour"])
                        {
                            $colour = $codeinfo["cod_web_colour"];
                        }

                        if ($colour)
                        {
                            $HTML .= '<td valign="left" style="background-color:#' . $colour. '"';
                        }
                        else
                        {
                            $HTML .= '<td class="windowbg2" valign="top"';
                        }
                    }
                    else
                    {
                        $HTML .= '<td class="windowbg2" valign="top"';
                    }

                    if ($col_info_extra['dataalign'])
                    {
                         $HTML .= ' align="' . $col_info_extra['dataalign'] . '"';
                    }

                    $HTML .= '>';

                    if ($BoldRowField && $row[$BoldRowField] == $BoldRowConditionValue)
                    {
                        $HTML .= '<b>';
                    }

                    if ($row[$col_field] && !$this->ReadOnly && !$col_info['custom'])
                    {
                        $HTML .= '<a href="javascript:if(CheckChange()){SendTo(\''.$record_url.'\');}" >';
                    }

                    if(!$col_info['custom'])
                    {
                        $row[$col_field] = htmlspecialchars($row[$col_field]);
                    }
                    if ($col_field == 'recordid')
                    {
                        if ($col_info_extra['prefix'])
                        {
                            $HTML .= $col_info_extra['prefix'];
                        }
                        $HTML .= $row[$col_field];
                    }
                    elseif ($col_field == "act_module")
                    {
                        $HTML .= $ModuleDefs[$row[$col_field]]["NAME"];
                    }
                    else if ($col_field == 'rep_approved')
                    {
                        $HTML .= FirstNonNull(array(_tk('approval_status_'.$this->Module.'_'.$row[$col_field]), $codeinfo['description']));
                    }
                    elseif ($FieldType == 'ff_select' || $FieldType == 'multilistbox')
                    {
                        //echo code_descr($this->Module, $col_field, $row[$col_field]);
                        $HTML .= $codeinfo['description'];
                    }
                    elseif ($FieldType == 'yesno')
                    {
                        $HTML .= code_descr($this->Module, $col_field, $row[$col_field]);
                    }
                    elseif ($FieldType == 'date')
                    {
                        $HTML .= FormatDateVal($row[$col_field]);
                    }
                    elseif ($FieldType == 'money')
                    {
                        $HTML .= FormatMoneyVal($row[$col_field]);
                    }
                    /*elseif ($FieldDefs[$this->Module][$col_field]['Type'] == 'textarea')
                   {
                       echo htmlspecialchars($row[$col_field]);
                   } */
                    elseif ($FieldType == 'time')
                    {
                        if (\UnicodeString::strlen($row[$col_field]) == 4)
                        {
                            $row[$col_field] = $row[$col_field]{0} . $row[$col_field]{1} . ":" . $row[$col_field]{2} . $row[$col_field]{3};
                        }

                        $HTML .= $row[$col_field];
                    }
                    else
                    {
                        $HTML .= $row[$col_field];
                    }

                    if(!$col_info['custom'])
                    {
                        // need to undo any html encoding here so it's not duplicated when showing full contact details below
                        $row[$col_field] = html_entity_decode($row[$col_field]);
                    }

                    if ($row[$col_field] && !$this->ReadOnly && !$col_info['custom'])
                    {
                        $HTML .= '</a>';
                    }
                    if ($BoldRow)
                    {
                        $HTML .= '</b>';
                    }
                    $HTML .= '</td>';
                }
            }
            $HTML .= '</tr>';

            if($this->ShowFullContactDetails)
            {
                $HTML .= $this->ShowFullContactDetails(array(
                    "contact" => $row,
                    "FormType" => 'Print',
                    "module" => $this->Module,
                    "ExtraParameters" => array_merge(array("RowClass"=>"windowbg2_light"), $this->ExtraParameters)
                    )
                );
            }
        }

        $HTML .= '</table>';

        return $HTML;
    }


    // gives full printout of details of contact.
    function ShowFullContactDetails($ParameterArray)
    {
        global $FormDesigns;

        $contact = $ParameterArray["contact"];
        $FormType = $ParameterArray["FormType"];
        $ExtraParameters = $ParameterArray["ExtraParameters"];
        $ShowLinkDetails = true;

        $FormDesignSave = $FormDesigns;

        $SavedFormDesignSettings = saveFormDesignSettings();
        unsetFormDesignSettings();

        //Recreate and Reset $FormDesigns global to old value
        global $FormDesigns;
        $FormDesigns = $FormDesignSave;

        $HTML = '';

        include(GetContactFormSettings($contact["link_type"]));

        $HTML .= '<tr class="windowbg2_light full-contact-listing" id="contact_'.$contact["con_id"].'" style="display: none"><td style="padding:0px;spacing:0px">';

        $module = $this->LinkModule;  // referenced in form array
        $registry = src\framework\registry\Registry::getInstance();
        $ModuleDefs = $registry->getModuleDefs();
        include("Source/contacts/BasicForm.php");

        if ($this->LinkModule == 'INC')
        {
            require_once 'Source/incidents/Injuries.php';
        }

        $HTML .= '
        </td><td style="padding:0px;spacing:0px" colspan="'.count($this->Columns).'">';

        $Table = new FormTable($FormType, 'CON');
        $Table->MakeForm($FormArray, $contact, $ParameterArray["module"], $ExtraParameters);
        $HTML .= $Table->GetFormTable();

        $HTML .= '</td></tr>';

        loadFormDesignSettings($SavedFormDesignSettings);

        return $HTML;
    }
    // @codeCoverageIgnoreStart


}


?>
