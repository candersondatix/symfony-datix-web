<?php

function SaveNotes($aParams)
{
    $link_field = $aParams['id_field'];
    $module = explode('_', $link_field);
    $module = \UnicodeString::strtoupper($module[0]);
    $recordid = $aParams['id'];
    $notes = $aParams['notes'];

    if(isset($notes) && $_POST['CHANGED-notes'] != '')
    {
        $PDOParamsArray["recordid"] = $recordid;
        $sql = "SELECT $link_field, notes, updateid FROM notepad WHERE $link_field = :recordid";

        $Query = new DatixDBQuery($sql);
        $request = $Query->prepareAndExecute($PDOParamsArray);

        if (!$request)
        {
            $error = "An error has occurred when trying to save the notes attached to this record. Please report the following to the Datix administrator: $sql";
        }
        else
        {
            $PDOParamsArray["updatedby"] = $_SESSION["initials"];
            $PDOParamsArray["updateid"] = GensUpdateID($row["updateid"]);
            $PDOParamsArray["updateddate"] = date('d-M-Y H:i:s');
            $PDOParamsArray["notes"] = $notes;

            if ($row = $Query->fetch())
            {
                $oldNotes = $row["notes"];  // for audit table

                $sql = "UPDATE notepad SET
                updatedby = :updatedby,
                updateddate = :updateddate,
                updateid = :updateid,
                notes = :notes
                WHERE $link_field = :recordid";
            }
            else if($notes != "")
            {
                $sql = "INSERT INTO notepad ($link_field, notes, updateddate, updatedby, updateid)
                VALUES (:recordid, :notes, :updateddate, :updatedby, :updateid)";
            }

            $request = PDO_query($sql, $PDOParamsArray);

            if (!$request)
            {
                $error = "An error has occurred when trying to save the notes attached to this record. Please report the following to the Datix administrator: $sql";
            }
            else if (!$aParams['new'])
            {
                $PDOAuditParams["module"] = $module;
                $PDOAuditParams["recordid"] = $recordid;
                $PDOAuditParams["updatedby"] = $PDOParamsArray["updatedby"];
                $PDOAuditParams["updateddate"] = $PDOParamsArray["updateddate"];
                $PDOAuditParams["aud_detail"] = $oldNotes;
                $PDOAuditParams["aud_action"] = 'WEB:notes';

                $sql = "INSERT INTO full_audit
                              (aud_module, aud_record,
                               aud_login, aud_date, aud_action,
                               aud_detail)
                          VALUES
                              (:module, :recordid, :updatedby, :updateddate, :aud_action, :aud_detail)";

                $request = PDO_query($sql, $PDOAuditParams);
            }
        }
    }

    return $error;
}
?>
