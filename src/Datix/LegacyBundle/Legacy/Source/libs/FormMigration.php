<?php
function CheckSpecificMigrationNeeded($Version)
{
    if (CompareVersions($Version, GetVersion()))
    {
        // The migration is for a later version than the current one.
        return false;
    }

    if (CompareVersions($Version, GetParm('FORM_MIGRATE_VERSION')))
    {
        // The migration is for a later version than the last migration done, so needs to be run
        return true;
    }

    return false;
}

function MigrateForms()
{
    global $ClientFolder, $scripturl, $OriginalMigrateVersion;

    $OriginalMigrateVersion = GetParm('FORM_MIGRATE_VERSION');

    $VersionsWithMigrations = array(
        '9.2',
        // various array keys changed.

        '10.0',
        // default reason for rejection form action added.
        // rep_approved hidden on level 1 forms by default.

        '10.1',
        //added changes for PALS + Complaints Documents.
        //added hidden additional information fields on dif2 and generic actions for same fields on dif1

        '10.1.1',
        //added changes for new SIRS screen

        '10.1.2',
        //added changes for actions location fields

        '10.2',
        //migration of listing designs to the database.
        //migration for progress notes

        '10.2.1',
        //Hide Healthcare commission, Independent Review and promary complainant dates on the complaints form.

        '10.6',
        //Hid dum_inc_grading_initial field

        '10.7',

        '11.1',
        //migration extra field groups to custom fields and independent extra fields.

        '11.2',

        '11.3',

        '11.5',

        '12.0',

        '12.1',
        //hide date of admission by default

        '12.1.2',
        //remove mandatory flag from medication search fields

        '12.2',
        // Add documents action to Risk Register Level 1 design
        //remove mandatory flag from medication search fields

        '12.3',
        // Hide link_notify_progress
        // Update tem_documents.path

        '12.3.3',
        // Added option for view own user and setting in admin

        '12.4.2',

        '14.0'
        // Added fieldset columns
        // Hide Reserve audit section by default
        // Add default extra text for the Communication and Feedback section
        // Remove ReadOnly option for the Communication and Feedback section
        // Hide 'Recipient' and 'Last updated' fields
        // Hide Openness and Transparency section
        // Add field actions for question 1 of section Openness and Transparency
        // Hide new Medications fields (see ticket DW-11650)
        // Hide Details of individual respondent section
        // Hide Respondents - Organisation section

    );

    $OutcomeMessages = array();

    foreach ($VersionsWithMigrations as $VersionWithMigration)
    {
        if ((CheckSpecificMigrationNeeded($VersionWithMigration)) && !$MigrationError)
        {
            if (function_exists('FormMigrationCode_'.str_replace('.', '_', $VersionWithMigration)))
            {
                eval('$Result = FormMigrationCode_'.str_replace('.', '_', $VersionWithMigration).'();');

                if ($Result)
                {
                    SetGlobal('FORM_MIGRATE_VERSION', $VersionWithMigration);
                    $_SESSION["Globals"]['FORM_MIGRATE_VERSION'] = $VersionWithMigration; //ensure we don't require a logout/in

                    $OutcomeMessages[] = '<i>The form migration released with version '.$VersionWithMigration.' has been successfully completed</i>';
                }
                else
                {
                    $MigrationError = true;
                    $OutcomeMessages[] = '<b>There was an error while performing the form migration released with version '.$VersionWithMigration.'. The form migration function returned an error. Please contact Datix Support.</b>';
                }

            }
            else
            {
                $MigrationError = true;
                $OutcomeMessages[] = '<b>There was an error while performing the form migration released with version '.$VersionWithMigration.'. The form migration function was not found. Please contact Datix Support.</b>';
            }
        }
    }

    if (!$MigrationError) // update to the latest version in case we are using a version without any migrations
    {
        SetGlobal('FORM_MIGRATE_VERSION', GetVersion());
        $_SESSION["Globals"]['FORM_MIGRATE_VERSION'] = GetVersion(); //ensure we don't require a logout/in
    }

    template_header();

    echo '<div class="general_div">
    ';

    echo '<div class="padded_div page_title">Form Migration.</div>
    ';

    echo '<div class="padded_div">Due to a recent DatixWeb upgrade, the form designs saved on this system are being migrated. This process is automatic.</div>
    ';

    if ($MigrationError)
    {
        foreach ($OutcomeMessages as $Message)
        {
            echo '<div class="padded_div">'.$Message.'</div>
            ';
        }
    }
    else
    {
        echo '<div class="padded_div"><b>All form designs have been successfully migrated.</b></div>
        ';
    }

    echo '<div class="padded_div"><a href="'.$scripturl.'">Proceed to main menu</a></div>
    ';

    echo '</div>
    ';

    footer();
}

function AlterAllDesigns($aParams)
{
    global $ModuleDefs, $ClientFolder;

    //add action to show document section to PAL1 and COM1 designs
    foreach ($aParams['modules'] as $module)
    {
        $FormFiles = array();
        $FormFiles2 = array();

        if (file_exists($ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
        {
            require $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
        }

        if ($aParams['levels'][1])
        {
            if (empty($FormFiles))
            {
                $aParams['params']['file'] = $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php';
                $aParams['function']($aParams['params']);
            }
            else
            {
                foreach ($FormFiles as $id => $FormFile)
                {
                    if ($id == 0)
                    {
                        $aParams['params']['file'] = $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php';
                        $aParams['function']($aParams['params']);
                    }
                    else
                    {
                        $aParams['params']['file'] = $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php';
                        $aParams['function']($aParams['params']);
                    }
                }
            }
        }

        if ($aParams['levels'][2])
        {
            if (empty($FormFiles2))
            {
                $aParams['params']['file'] = $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php';
                $aParams['function']($aParams['params']);
            }
            else
            {
                foreach ($FormFiles2 as $id => $FormFile)
                {
                    if ($id == 0)
                    {
                        $aParams['params']['file'] = $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php';
                        $aParams['function']($aParams['params']);
                    }
                    else
                    {
                        $aParams['params']['file'] = $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'_'.$id.'.php';                $aParams['function']($aParams['params']);
                    }
                }
            }
        }
    }
}

function FormMigrationCode_14_0()
{

    //hide reserve audit
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'fin_calc_total_incurred'),
        'levels'   => array(2 => true)
    ));

    // Add default extra text for the Communication and Feedback section
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_to', 'text' => 'Only staff and contacts with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_gab', 'text' => 'Only users with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_email', 'text' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_to', 'text' => 'Only staff and contacts with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_gab', 'text' => 'Only users with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_email', 'text' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('PAL'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_to', 'text' => 'Only staff and contacts with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('PAL'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_gab', 'text' => 'Only users with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('PAL'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_email', 'text' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_to', 'text' => 'Only staff and contacts with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_gab', 'text' => 'Only users with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_email', 'text' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('RAM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_to', 'text' => 'Only staff and contacts with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('RAM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_gab', 'text' => 'Only users with e-mail addresses are shown.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('RAM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'dum_fbk_email', 'text' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'),
        'levels'   => array(2 => true)
    ));

    // Remove ReadOnly option for the Communication and Feedback section
    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'RemoveSectionReadOnly',
        'params'   => array('section' => 'feedback'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'RemoveSectionReadOnly',
        'params'   => array('section' => 'feedback'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('PAL'),
        'function' => 'RemoveSectionReadOnly',
        'params'   => array('section' => 'feedback'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'RemoveSectionReadOnly',
        'params'   => array('section' => 'feedback'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('RAM'),
        'function' => 'RemoveSectionReadOnly',
        'params'   => array('section' => 'feedback'),
        'levels'   => array(2 => true)
    ));

    // Hide 'Recipient' and 'Last updated' fields
    $OriginalMigrateVersion = GetParm('FORM_MIGRATE_VERSION');

    if ($OriginalMigrateVersion != '12.4.2')
    {
        AlterAllDesigns([
            'modules'  => ['INC'],
            'function' => 'AddHiddenField',
            'params'   => ['field' => 'inc_last_updated'],
            'levels'   => [2 => true]
        ]);

        AlterAllDesigns([
            'modules'  => ['CLA'],
            'function' => 'AddHiddenField',
            'params'   => ['field' => 'cla_last_updated'],
            'levels'   => [2 => true]
        ]);

        AlterAllDesigns([
            'modules'  => ['COM'],
            'function' => 'AddHiddenField',
            'params'   => ['field' => 'com_last_updated'],
            'levels'   => [2 => true]
        ]);

        AlterAllDesigns([
            'modules'  => ['PAL'],
            'function' => 'AddHiddenField',
            'params'   => ['field' => 'pal_last_updated'],
            'levels'   => [2 => true]
        ]);

        AlterAllDesigns([
            'modules'  => ['PAY'],
            'function' => 'AddHiddenField',
            'params'   => ['field' => 'pay_recipient'],
            'levels'   => [2 => true]
        ]);
    }

    // Hide Openness and Transparency section
    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'openness_transparency'),
        'levels'   => array(
            1 => true,
            2 => true
        )
    ));

    // Add field actions for question 1 of section Openness and Transparency
    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q2', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q3', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q4', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q5', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q6', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q7', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q8', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q9', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q10', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q11', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q12', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q13', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q14', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q15', 'values' => array(0 => 'Y', 1 => 'N'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q16', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q17', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q18', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q19', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddFieldShowAction',
        'params'   => array('field' => 'inc_ot_q1', 'action' => array('field' => 'inc_ot_q20', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    // Hide new Medications fields (see ticket DW-11650)
    AlterAllDesigns([
        'modules'  => ['INC'],
        'function' => 'AddHiddenField',
        'params'   => ['field' => 'imed_other_factors'],
        'levels'   => [1 => true, 2 => true]
    ]);

    AlterAllDesigns([
        'modules'  => ['INC'],
        'function' => 'AddHiddenField',
        'params'   => ['field' => 'imed_right_wrong_medicine_admin'],
        'levels'   => [1 => true, 2 => true]
    ]);

    AlterAllDesigns([
        'modules'  => ['INC'],
        'function' => 'AddHiddenField',
        'params'   => ['field' => 'imed_right_wrong_medicine_correct'],
        'levels'   => [1 => true, 2 => true]
    ]);

    AlterAllDesigns([
        'modules'  => ['INC'],
        'function' => 'AddHiddenField',
        'params'   => ['field' => 'imed_manufacturer_special_admin'],
        'levels'   => [1 => true, 2 => true]
    ]);

    // Hide Details of individual respondent section
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'contacts_type_O'),
        'levels'   => array(
            1 => true
        )
    ));

    // Hide Respondents - Organisation section
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'respondents_organisation'),
        'levels'   => array(
            1 => true
        )
    ));

    return true;
}

function FormMigrationCode_12_4_2()
{
    global $ClientFolder, $ModuleDefs;

    $FormFiles = array();
    //get con1 forms
    if (file_exists($ClientFolder.'/'.$ModuleDefs['CON']['FORM_DESIGN_ARRAY_FILE']))
    {
        require $ClientFolder.'/'.$ModuleDefs['CON']['FORM_DESIGN_ARRAY_FILE'];
    }
    //add current date as default value for 'date received' on each con1 form for complainant
    foreach ($FormFiles as $id => $FormFile)
    {
        if ($id == 0)
        {
            AddDefaultValue(array(
                'field' => 'lcom_dreceived',
                'value' => 'TODAY',
                'file' => $ClientFolder.'/'.$ModuleDefs['CON']['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'
            ));
        }
        else
        {
            AddDefaultValue(array(
                'field' => 'lcom_dreceived',
                'value' => 'TODAY',
                'file' => $ClientFolder.'/'.$ModuleDefs['CON']['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'
            ));
        }
    }

    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'reserve_audit'),
        'levels'   => array(2 => true)
    ));
    //hide date closed for expenses reserve
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'indem_dclosed'),
        'levels'   => array(2 => true)
    ));
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'expen_dclosed'),
        'levels'   => array(2 => true)
    ));
    //hide date recieved for CON1 complainant
    AlterAllDesigns(array(
        'modules'  => array('CON'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'lcom_dreceived'),
        'levels'   => array(1 => true)
    ));

    return true;
}

function FormMigrationCode_12_3_3()
{
    AlterAllDesigns(array(
        'modules'  => array('ADM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ADM_VIEW_OWN_USER'),
        'levels'   => array(2 => true)
    ));
    return true;
}

function FormMigrationCode_12_3()
{
    AlterAllDesigns(array(
        'modules'  => array('CON'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'link_notify_progress'),
        'levels'   => array(1 => true, 2 => true)
    ));

    //Update tem_documents.path to match tem_documents.recordid
    $tem_documents = DatixDBQuery::PDO_fetch_all('SELECT recordid, path from tem_documents', array(), PDO::FETCH_KEY_PAIR);

    foreach ($tem_documents as $recordid => $path)
    {
        if ($path && $path != 'TEM'.$recordid.'.doc')
        {
            rename(\Sanitize::SanitizeFilePath(GetParm('PATH_TEMPLATES', '', true) . "\\" . $path), \Sanitize::SanitizeFilePath(GetParm('PATH_TEMPLATES', '', true) . "\\" . 'TEM'.$recordid.'.doc'));
            DatixDBQuery::PDO_query('UPDATE tem_documents SET path = :newpath WHERE recordid = :recordid', array('recordid' => $recordid, 'newpath' => 'TEM'.$recordid.'.doc'));
        }
    }

    DatixDBQuery::PDO_query('UPDATE tem_documents SET path = \'TEM\'+CAST(recordid as varchar(10))+\'.doc\'');

    return true;
}

function FormMigrationCode_12_2()
{
    // Add documents action to Risk Register Level 1 design
    $ActionArray = array (
        'section' => 'documents',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('RAM'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_document', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Hide DIF1 form on all Admin forms
    AlterAllDesigns(array(
        'modules'  => array('ADM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'DIF1_ONLY_FORM'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('RAM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'extra_document'),
        'levels'   => array(1 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ACT'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'documents'),
        'levels'   => array(2 => true)
    ));

    // Hide by default CCS2 section on Complaints
    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ccs2'),
        'levels'   => array(1 => true, 2 => true)
    ));

    // Hide by default Claims Reserve Remaining fields
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'fin_calc_reserve_1'),
        'levels'   => array(2 => true)
    ));
    AlterAllDesigns(array(
        'modules'  => array('CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'fin_calc_reserve_2'),
        'levels'   => array(2 => true)
    ));

    // Add extra text for logout default module
    AlterAllDesigns(array(
        'modules'  => array('ADM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'LOGOUT_DEFAULT_MODULE', 'text' => 'Select module to be presented to user upon logging out.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('CON'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'additonal_info'),
        'levels'   => array(2 => true)
    ));

    // Add show_documents action to contacts designs
    $ActionArray = array (
        'section' => 'documents',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_document', 'action' => $ActionArray),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('AMO'),
        'function' => 'ReplaceSectionKey',
        'params' => array('replace_array' =>
            array(
                'questions' => 'asm_data_questions',
            )),
        'levels' => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_12_1_2()
{
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'RemoveMandatoryField',
        'params' => array('field' => 'dummy_imed_search_admin'),
        'levels' => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'RemoveMandatoryField',
        'params' => array('field' => 'dummy_imed_search_correct'),
        'levels' => array(1 => true, 2 => true)
    ));

    return true;
}

function FormMigrationCode_12_1()
{
    getBlankWhereClauses();

    // Replace medication section keys on DIF2
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'ReplaceSectionKey',
        'params' => array('replace_array' =>
            array(
                'multi_medication' => 'multi_medication_record',
            )),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'inc_never_event'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('CON'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'link_date_admission'),
        'levels'   => array(1 => true, 2 => true)
    ));

    return true;
}

function FormMigrationCode_12_0()
{
    // Add Help Text for E-mail notification for reviewed assigned assessments
    AlterAllDesigns(array(
        'modules'  => array('ADM'),
        'function' => 'AddHelpText',
        'params'   => array('field' => 'ATI_REVIEWED_EMAIL', 'text' => 'E-mails will be sent only for the main location for which the assigned assessment has been created. Users at other locations selected against the assigned assessment will not receive notification.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC','CLA'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ccs2'),
        'levels'   => array(1 => true, 2 => true)
    ));

    fixMissingFilenames();

    // Replace medication section keys on DIF1
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'ReplaceSectionKey',
        'params' => array('replace_array' =>
            array(
                'multi_medication' => 'multi_medication_record',
            )),
        'levels' => array(1 => true)
    ));

    // For the ADM module, we need to convert extra field groups to custom sections and independent extra fields - this was missed in the original migration in 11.1.
    AlterAllDesigns(array(
        'modules' => array('ADM'),
        'function' => 'MigrateExtraFieldGroups',
        'levels' => array(2 => true)
    ));


    return true;
}

function FormMigrationCode_11_5()
{
    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'imed_price_admin'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'imed_price_correct'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('MED'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'med_price'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'imed_reference_admin'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'imed_reference_correct'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('MED'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'med_reference'),
        'levels'   => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_11_3()
{
    // Hide Payments section
    AlterAllDesigns(array(
        'modules'  => array('INC', 'COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'payments'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'lcom_last_dreopened'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC','RAM','PAL','COM','CLA','STN','ELE','PRO','SAB','AST','HOT'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'action_chains'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('CON'),
        'function' => 'AddNewPanel',
        'params'   => array('panel' => 'claims'),
        'levels'   => array(2 => true)
    ));

    // Hide PHSA standards fields
    AlterAllDesigns(array(
        'modules'  => array('STN'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'stn_ddue'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('STN'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'documents'),
        'levels'   => array(2 => true)
    ));

    // Hide PHSA elements fields
    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_priority'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_action_taken'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_dassigned'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_ddue'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_dreviewed'),
        'levels'   => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_11_1()
{
    // If we've already gone to 10.2.2, don't do this bit.
    if (CompareVersions('10.2.2', GetParm('FORM_MIGRATE_VERSION')))
    {
        AlterAllDesigns(array(
            'modules'  => array('COM'),
            'function' => 'AddHiddenField',
            'params'   => array('field' => 'com_dreopened'),
            'levels'   => array(1 => true, 2 => true)
        ));
    }

    AlterAllDesigns(array(
        'modules'  => array('ADM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'con_staff_include'),
        'levels'   => array(1 => true, 2 => true)
    ));

    // Need to convert extra field groups to custom sections and independent extra fields.
    AlterAllDesigns(array(
        'modules' => array('INC', 'RAM', 'PAL', 'COM', 'SAB', 'STN', 'CON', 'ACT', 'AST'),
        'function' => 'MigrateExtraFieldGroups',
        'levels' => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'dum_com_grading'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ADM'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'LOGIN_DEFAULT_MODULE', 'text' => 'Select module to be presented to user upon logging in.'),
        'levels'   => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_11_2()
{
    global $OriginalMigrateVersion;

    if ($OriginalMigrateVersion != '10.6') // Otherwise this will already have been done
    {
        // Hide Initial Risk Grading
        AlterAllDesigns(array(
            'modules'  => array('INC'),
            'function' => 'AddHiddenField',
            'params'   => array('field' => 'dum_inc_grading_initial'),
            'levels'   => array(1 => true, 2 => true)
        ));

        AlterAllDesigns(array(
            'modules'  => array('COM'),
            'function' => 'AddHiddenField',
            'params'   => array('field' => 'com_dreopened'),
            'levels'   => array(1 => true, 2 => true)
        ));

        // Hide Age Band field
        AlterAllDesigns(array(
            'modules'  => array('CON'),
            'function' => 'AddHiddenField',
            'params'   => array('field' => 'link_age_band'),
            'levels'   => array(1 => true, 2 => true)
        ));
    }

    // Make Distribution list name mandatory
    AlterAllDesigns(array(
        'modules'  => array('DST'),
        'function' => 'AddMandatoryField',
        'params'   => array('field' => 'dst_name'),
        'levels'   => array(2 => true)
    ));

    // Set the COM consent required field to default to hidden
    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'com_consent'),
        'levels'   => array(1 => true, 2 => true)
    ));

    // Hide Causal Factors
    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'causal_factor_header'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'causal_factor'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddSectionAction',
        'params'   => array('field' => 'inc_causal_factors_linked', 'action' => array('section' => 'causal_factor', 'values' => array(0 => 'Y'))),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('HSA'),
        'function' => 'AddExtraText',
        'params'   => array('field' => 'location', 'text' => 'These fields identify the area of the organisation which manages this hotspot agent. If you wish these values to be used as part of the criteria, select the option below.'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('HSA'),
        'function' => 'AddHelpText',
        'params'   => array('field' => 'hsa_use_loc_for_crit', 'text' => 'Selecting this option will include these values in the criteria for this Hotspot Agent.<br /><br />These values will operate in addition to other conditions set.'),
        'levels'   => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_10_6()
{
    // Hide Initial Risk Grading
    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'dum_inc_grading_initial'),
        'levels'   => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'com_dreopened'),
        'levels'   => array(1 => true, 2 => true)
    ));

    // Hide Age Band field
    AlterAllDesigns(array(
        'modules'  => array('CON'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'link_age_band'),
        'levels'   => array(1 => true, 2 => true)
    ));

    // Hide Time Band field
    AlterAllDesigns(array(
        'modules'  => array('INC'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'inc_time_band'),
        'levels'   => array(1 => true, 2 => true)
    ));

    return true;
}

function FormMigrationCode_10_7()
{
    // Hide PHSA standards fields
    AlterAllDesigns(array(
        'modules'  => array('STN'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'stn_ddue'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('STN'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'documents'),
        'levels'   => array(2 => true)
    ));

    // Hide PHSA elements fields
    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_priority'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_action_taken'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_dassigned'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_ddue'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ELE'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'ele_dreviewed'),
        'levels'   => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_10_2_1()
{
    // Hide Independent review
    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'independent_review'),
        'levels'   => array(2 => true)
    ));

    // Hide Healthcare Commision
    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'hcc'),
        'levels'   => array(2 => true)
    ));

    // Hide primary complainant dates on complaint form
    AlterAllDesigns(array(
        'modules'  => array('COM'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'primary_compl_dates'),
        'levels'   => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_10_2()
{
    // Create Level 2 Contact form for Distribution lists
    $DSTCON2FormDetails = CopyDefaultFormDesign('CON', 2, 0, "SABS and Distribution List Contact Form");
    DefineDefaultDSTContactForm($DSTCON2FormDetails);

    // Set new distribution list contact as contact form for default distribution list form.
    SetContactForm("DST", 0, $DSTCON2FormDetails["form_id"], "N", 2);

    // Set new distribution list contact as contact form for default SAB1 and SAB2 form contact sections.
    SetContactForm("SAB", 0, $DSTCON2FormDetails["form_id"], "S", 1);
    SetContactForm("SAB", 0, $DSTCON2FormDetails["form_id"], "I", 1);
    SetContactForm("SAB", 0, $DSTCON2FormDetails["form_id"], "N", 1);
    SetContactForm("SAB", 0, $DSTCON2FormDetails["form_id"], "S", 2);
    SetContactForm("SAB", 0, $DSTCON2FormDetails["form_id"], "I", 2);
    SetContactForm("SAB", 0, $DSTCON2FormDetails["form_id"], "N", 2);

    // Check designs haven't already been added.
    $numdesigns = DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM WEB_LISTING_DESIGNS');

    if ($numdesigns['num'] == 0)
    {
        // Migrate listing page designs to the database.
        foreach (array('INC', 'RAM', 'PAL', 'COM', 'SAB', 'ACT', 'CON', 'ADM') as $module)
        {
            $StandardColSettingsFile = GetSettingsFilename("", $module, "", "column", "standard");
            $ColSettingsFile = GetSettingsFilename("", $module, "", "column", "user");

            if (file_exists($ColSettingsFile))
            {
                require $ColSettingsFile;

                if (is_array($list_columns))
                {
                    require_once 'Source/libs/ListingClass.php';
                    $Listing = new Listing($module);
                    $Listing->LoadColumnsFromArray($list_columns);
                    $Listing->Title = 'User Designed Listing';
                    $Listing->SaveToDB();

                    // Set the newly migrated design as the default.
                    SetGlobal($module.'_LISTING_ID', $Listing->ListingId, true);
                }
            }
        }

        // Add new default listing designs for contacts.
        require_once 'Source/libs/ListingClass.php';
        $Listing = new Listing('CON');

        $list_columns = array(
            'rep_approved' => array('width' => '5'),
            'con_title' => array('width' => '5'),
            'con_forenames' => array('width' => '10'),
            'con_surname' => array('width' => '6'),
            'con_type' => array('width' => '4'),
            'link_status' => array('width' => '5'),
            'link_role' => array('width' => '6'),
        );

        $Listing->LoadColumnsFromArray($list_columns);
        $Listing->Title = 'Contact Listing for Incidents, Risks, PALS and Complaints';
        $Listing->SaveToDB();

        AlterAllDesigns(array(
            'modules' => array('INC', 'COM'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_A', 'key' => 'contacts_type_A', 'listing_id' => $Listing->ListingId),
            'levels' => array(2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('INC', 'COM'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_E', 'key' => 'contacts_type_E', 'listing_id' => $Listing->ListingId),
            'levels' => array(2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('INC', 'PAL', 'COM'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_N', 'key' => 'contacts_type_N', 'listing_id' => $Listing->ListingId),
            'levels' => array(2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('COM'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_C', 'key' => 'contacts_type_C', 'listing_id' => $Listing->ListingId),
            'levels' => array(2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('PAL'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_P', 'key' => 'contacts_type_P', 'listing_id' => $Listing->ListingId),
            'levels' => array(2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('RAM'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_N', 'key' => 'contacts_type_N', 'listing_id' => $Listing->ListingId),
            'levels' => array(2 => true)
        ));

        // Add safety alerts listing
        $Listing = new Listing('CON');

        $list_columns = array(
            'con_title' => array('width' => '5'),
            'con_forenames' => array('width' => '10'),
            'con_surname' => array('width' => '6'),
            'con_subtype' => array('width' => '5'),
            'con_specialty' => array('width' => '5'),
        );

        $Listing->LoadColumnsFromArray($list_columns);
        $Listing->Title = 'Safety Alerts listing';
        $Listing->SaveToDB();

        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_S', 'key' => 'not', 'listing_id' => $Listing->ListingId),
            'levels' => array(1 => true, 2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_S', 'key' => 'sent', 'listing_id' => $Listing->ListingId),
            'levels' => array(1 => true, 2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_S', 'key' => 'read', 'listing_id' => $Listing->ListingId),
            'levels' => array(1 => true, 2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_S', 'key' => 'resp', 'listing_id' => $Listing->ListingId),
            'levels' => array(1 => true, 2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_I', 'key' => 'not', 'listing_id' => $Listing->ListingId),
            'levels' => array(1 => true, 2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_I', 'key' => 'sent', 'listing_id' => $Listing->ListingId),
            'levels' => array(1 => true, 2 => true)
        ));

        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_N', 'key' => 'contacts_type_N', 'listing_id' => $Listing->ListingId),
            'levels' => array(1 => true, 2 => true)
        ));

        // Resave for use with distribution lists
        $Listing->Title = 'Distribution List listing';
        $Listing->ListingId = null;
        $Listing->SaveToDB();

        AlterAllDesigns(array(
            'modules' => array('DST'),
            'function' => 'AddListingDesign',
            'params' => array('section' => 'contacts_type_N', 'key' => 'contacts_type_N', 'listing_id' => $Listing->ListingId),
            'levels' => array(2 => true)
        ));
    }

    // Make con_organisation and con_surname fields mandatory
    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddMandatoryField',
        'params' => array('field' => 'con_surname'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddMandatoryField',
        'params' => array('field' => 'con_organisation'),
        'levels' => array(2 => true)
    ));

    // Add progress notes panel
    AlterAllDesigns(array(
        'modules'  => array('ACT','AST','COM','PAL','INC','RAM','SAB','STN','ELE','PRO','LIB','DST','CON'),
        'function' => 'AddNewPanel',
        'params'   => array('panel' => 'progress_notes'),
        'levels'   => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules'  => array('ACT','AST','COM','PAL','INC','RAM','SAB','STN','ELE','PRO','LIB','DST','CON'),
        'function' => 'AddHiddenField',
        'params'   => array('field' => 'progress_notes'),
        'levels'   => array(2 => true)
    ));

    // Custom function to sort out new, split contacts sections.
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'SplitContactSection',
        'params' => array('module' => 'INC'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('RAM'),
        'function' => 'SplitContactSection',
        'params' => array('module' => 'RAM'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('COM'),
        'function' => 'SplitContactSection',
        'params' => array('module' => 'COM'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('PAL'),
        'function' => 'SplitContactSection',
        'params' => array('module' => 'PAL'),
        'levels' => array(2 => true)
    ));

    // Replacement of ContactForms design setting
    AlterAllDesigns(array(
        'modules' => array('INC', 'RAM', 'COM', 'PAL', 'SAB'),
        'function' => 'ConFormsToFormDesigns',
        'params' => array(),
        'levels' => array(1 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('INC', 'RAM', 'COM', 'PAL', 'DST', 'SAB'),
        'function' => 'ConFormsToFormDesigns',
        'params' => array(),
        'levels' => array(2 => true)
    ));

    // Replace contact section keys on level 1 forms
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'ReplaceSectionKey',
        'params' => array('replace_array' =>
            array(
                'assailant' => 'contacts_type_L',
                'police_officer' => 'contacts_type_P',
                'contact_reporter' => 'contacts_type_R',
                'extra_person' => 'contacts_type_A',
                'extra_witness' => 'contacts_type_W',
                'extra_employee' => 'contacts_type_E',
                'extra_contact' => 'contacts_type_N',
            )
        ),
        'levels' => array(1 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('RAM'),
        'function' => 'ReplaceSectionKey',
        'params' => array('replace_array' =>
            array(
                'extra_contact' => 'contacts_type_N',
            )
        ),
        'levels' => array(1 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('COM'),
        'function' => 'ReplaceSectionKey',
        'params' => array('replace_array' =>
            array(
                'complainant' => 'contacts_type_C',
                'person_affected' => 'contacts_type_A',
                'reporter' => 'contacts_type_R',
            )
        ),
        'levels' => array(1 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('PAL'),
        'function' => 'ReplaceSectionKey',
        'params' => array('replace_array' =>
            array(
                'enquirer' => 'contacts_type_P',
                'extra_contact' => 'contacts_type_N',
            )
        ),
        'levels' => array(1 => true)
    ));

    // Hide rejection history field
    AlterAllDesigns(array(
        'modules' => array('INC', 'COM', 'PAL', 'RAM'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'rejection_history'),
        'levels' => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_10_1_2()
{
    // Hide action locations fields
    AlterAllDesigns(array(
        'modules' => array('ACT'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'act_organisation'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('ACT'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'act_unit'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('ACT'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'act_clingroup'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('ACT'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'act_specialty'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('ACT'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'act_directorate'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('ACT'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'act_loctype'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('ACT'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'act_locactual'),
        'levels' => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_10_1_1()
{
    // Hide police number field
    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'con_police_number'),
        'levels' => array(1 => true, 2 => true)
    ));

    // Hide new linked contact fields
    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'sirs'),
        'levels' => array(1 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'link_sirs'),
        'levels' => array(2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'link_pprop_damaged'),
        'levels' => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'con_sex_orientation'),
        'levels' => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'con_religion'),
        'levels' => array(1 => true, 2 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'con_work_alone_assessed'),
        'levels' => array(1 => true, 2 => true)
    ));

    // Hide personal property section by default
    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'property_section'),
        'levels' => array(1 => true, 2=> true)
    ));

    //hide trust property section by default
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'tprop'),
        'levels' => array(1 => true)
    ));

    // add trust property damaged action to incidents designs
    $ActionArray = array (
        'section' => 'tprop',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'inc_tprop_damaged', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    // Add personal property damaged action to contacts designs
    $ActionArray = array (
        'section' => 'property_section',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'link_pprop_damaged', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    // Add police persue action to contacts designs
    $ActionArray = array (
        'field' => 'con_police_persue_reason',
        'alerttext' => '',
        'values' => array (
            0 => 'N',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddFieldShowAction',
        'params' => array('field' => 'con_police_pursue', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    // Add trust property actions to SIRS (equipment) designs
    $ActionArray = array (
        'field' => 'link_replace_cost',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('AST'),
        'function' => 'AddFieldShowAction',
        'params' => array('field' => 'link_replaced', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    $ActionArray = array (
        'field' => 'link_repair_cost',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('AST'),
        'function' => 'AddFieldShowAction',
        'params' => array('field' => 'link_repaired', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    $ActionArray = array (
        'field' => 'link_disposal_cost',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('AST'),
        'function' => 'AddFieldShowAction',
        'params' => array('field' => 'link_written_off', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    $ActionArray = array (
        'field' => 'link_sold_price',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('AST'),
        'function' => 'AddFieldShowAction',
        'params' => array('field' => 'link_sold', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    $ActionArray = array (
        'field' => 'link_decommission_cost',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('AST'),
        'function' => 'AddFieldShowAction',
        'params' => array('field' => 'link_decommissioned', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    // Add default extra text to DIF1 (used to be hard-coded)
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddExtraText',
        'params' => array('field' => 'inc_rep_email', 'text' => 'Please enter a valid e-mail address so that an acknowledgment can be sent to you'),
        'levels' => array(1 => true)
    ));

    // Relabel old equipment section
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'RelabelField',
        'params' => array('field' => 'equipment', 'value' => 'Equipment details'),
        'levels' => array(1 => true)
    ));

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'RelabelField',
        'params' => array('field' => 'equipment', 'value' => 'Equipment'),
        'levels' => array(2 => true)
    ));

    return true;
}

function FormMigrationCode_10_1()
{
    global $ModuleDefs, $ClientFolder;

    // Add documents action to complaints and pals designs
    $ActionArray = array (
        'section' => 'documents',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('PAL', 'COM'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_document', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add other contacts action to PAL1 designs
    $ActionArray = array (
        'section' => 'extra_contact',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('PAL'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_other_contacts', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add other contacts action to RISK1 designs
    $ActionArray = array (
        'section' => 'extra_contact',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('RAM'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_contacts_RAM', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add injury action to level 2 contacts designs
    $ActionArray = array (
        'section' => 'injury',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_injury', 'action' => $ActionArray),
        'levels' => array(2=>true)
    ));

    // Add injury action to level 1 contacts designs
    $ActionArray = array (
        'section' => 'injury_section',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('CON'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_injury', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add person affected action to complaint designs
    $ActionArray = array (
        'section' => 'person_affected',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('COM'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_person', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Hide additional information on COM2
    AlterAllDesigns(array(
        'modules' => array('COM'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'additional'),
        'levels' => array(2 => true)
    ));

    // Ensure that the new reporter section is readonly by defualt
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddReadOnlyField',
        'params' => array('field' => 'reportedby'),
        'levels' => array(2 => true)
    ));

    // Hide additional information on DIF2
    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddHiddenField',
        'params' => array('field' => 'additional'),
        'levels' => array(2 => true)
    ));

    // Add DIF1 action for people affected
    $ActionArray = array (
        'section' => 'extra_person',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_person', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for witnesses
    $ActionArray = array (
        'section' => 'extra_witness',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_witness', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for witnesses
    $ActionArray = array (
        'section' => 'extra_employee',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_employee', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for other contacts
    $ActionArray = array (
        'section' => 'extra_contact',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_other_contacts', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for equipment
    $ActionArray = array (
        'section' => 'equipment',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_equipment', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for medication
    $ActionArray = array (
        'section' => 'medication',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_medication', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for documents
    $ActionArray = array (
        'section' => 'extra_document',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_document', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for pars
    $ActionArray = array (
        'section' => 'pars',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_pars', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for police attended
    $ActionArray = array (
        'section' => 'police_officer',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'inc_pol_attend', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for alleged assailant
    $ActionArray = array (
        'section' => 'assailant',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'show_assailant', 'action' => $ActionArray),
        'levels' => array(1 => true)
    ));

    // Add DIF1 action for riddor
    $ActionArray = array (
        'section' => 'riddor',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'inc_is_riddor', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));

    // Add DIF1 action for NRLS
    $ActionArray = array (
        'section' => 'nrls',
        'alerttext' => '',
        'values' => array (
            0 => 'Y',
        ),
    );

    AlterAllDesigns(array(
        'modules' => array('INC'),
        'function' => 'AddSectionAction',
        'params' => array('field' => 'inc_report_npsa', 'action' => $ActionArray),
        'levels' => array(1 => true, 2 => true)
    ));


    //Ensure SAB1 fields are read only by default

    foreach (array('name', 'description', 'location', 'udffrommain', 'udf', 'linked_records', 'notepad') as $Section)
    {
        AlterAllDesigns(array(
            'modules' => array('SAB'),
            'function' => 'AddReadOnlyField',
            'params' => array('field' => $Section),
            'levels' => array(1 => true)
        ));
    }

    return true;
}

function FormMigrationCode_10_0()
{
    global $ClientFolder, $ModuleDefs;

    $ModulesToChange = array('INC', 'RAM');

    unsetFormDesignSettings();

    //add rep_approved as a hidden field to DIF1 and RISK1 designs.
    foreach ($ModulesToChange as $module)
    {
        $FormFiles = array();

        if (file_exists($ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
        {
            require $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
        }

        if (empty($FormFiles))
        {
            AddHiddenField(array('field' => 'rep_approved', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
            AddHiddenField(array('field' => 'rep_approved_display', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
        }
        else
        {
            foreach ($FormFiles as $id => $FormFile)
            {
                if ($id == 0)
                {
                    AddHiddenField(array('field' => 'rep_approved', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
                    AddHiddenField(array('field' => 'rep_approved_display', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
                }
                else
                {
                    AddHiddenField(array('field' => 'rep_approved', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
                    AddHiddenField(array('field' => 'rep_approved_display', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
                }
            }
        }
    }

    //add action to show reasons for rejection section to DIF2 and RISK2 designs
    foreach ($ModulesToChange as $module)
    {
        $FormFiles2 = array();

        if (file_exists($ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
        {
            require $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
        }

        $ActionArray = array (
            'section' => 'rejection',
            'alerttext' => 'Please complete the \'Details of rejection\' section before saving this form.',
            'values' => array (
                0 => 'REJECT',
            ),
        );

        if (empty($FormFiles2))
        {
            AddSectionAction(array('field' => 'rep_approved', 'action' => $ActionArray, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
        }
        else
        {
            foreach ($FormFiles2 as $id => $FormFile)
            {
                if ($id == 0)
                {
                    AddSectionAction(array('field' => 'rep_approved', 'action' => $ActionArray, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
                }
                else
                {
                    AddSectionAction(array('field' => 'rep_approved', 'action' => $ActionArray, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'_'.$id.'.php'));
                }
            }
        }
    }

    // add previously hard-coded default extratext to any form designs for which they would have appeared.

    $ExtraTextArray = array(
        'INC' => array(
            'inc_notes' => 'Enter facts, not opinions.  Do not enter names of people',
            'inc_actiontaken' => 'Enter action taken at the time of the incident'
        ),
        'RAM' => array(
            'ram_description' => 'Enter facts, not opinions.  Do not enter names of people'
        ),
        'CON' => array(
            'con_dod' => 'Fill this in for a patient who has died'
        )
    );

    foreach ($ExtraTextArray as $module => $FieldsToChange)
    {
        $FormFiles2 = array();
        $FormFiles = array();

        if (file_exists($ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
        {
            require $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
        }

        foreach ($FieldsToChange as $Field => $ExtraText)
        {
            if (empty($FormFiles))
            {
                AddExtraText(array('field' => $Field, 'text' => $ExtraText, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
            }
            else
            {
                foreach ($FormFiles as $id => $FormFile)
                {
                    if ($id == 0)
                    {
                        AddExtraText(array('field' => $Field, 'text' => $ExtraText, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
                    }
                    else
                    {
                        AddExtraText(array('field' => $Field, 'text' => $ExtraText, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
                    }
                }
            }

            if (empty($FormFiles2))
            {
                AddExtraText(array('field' => $Field, 'text' => $ExtraText, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
            }
            else
            {
                foreach ($FormFiles2 as $id => $FormFile)
                {
                    if ($id == 0)
                    {
                        AddExtraText(array('field' => $Field, 'text' => $ExtraText, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
                    }
                    else
                    {
                        AddExtraText(array('field' => $Field, 'text' => $ExtraText, 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'_'.$id.'.php'));
                    }
                }
            }
        }
    }

    // Add default value to lcom_iscomplpat field in CON
    $module = 'CON';
    $FormFiles = array();
    $FormFiles2 = array();

    if (file_exists($ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
    {
        require $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
    }

    if (empty($FormFiles))
    {
        AddHiddenField(array('field' => 'icon_cost', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
    }
    else
    {
        foreach ($FormFiles as $id => $FormFile)
        {
            if ($id == 0)
            {
                AddHiddenField(array('field' => 'icon_cost', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
            }
            else
            {
                AddHiddenField(array('field' => 'icon_cost', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
            }
        }
    }

    if (empty($FormFiles2))
    {
        AddDefaultValue(array('field' => 'lcom_iscomplpat', 'value' => 'Y', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
        AddHiddenField(array('field' => 'icon_cost', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
    }
    else
    {
        foreach ($FormFiles2 as $id => $FormFile)
        {
            if ($id == 0)
            {
                AddDefaultValue(array('field' => 'lcom_iscomplpat', 'value' => 'Y', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
                AddHiddenField(array('field' => 'icon_cost', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
            }
            else
            {
                AddDefaultValue(array('field' => 'lcom_iscomplpat', 'value' => 'Y', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'_'.$id.'.php'));
                AddHiddenField(array('field' => 'icon_cost', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'_'.$id.'.php'));
            }
        }
    }

    // Add default value to lcom_iscomplpat field in CON
    $module = 'COM';
    $FormFiles = array();
    $FormFiles2 = array();

    if (file_exists($ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
    {
        require $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
    }

    if (empty($FormFiles))
    {
        AddHiddenField(array('field' => 'com_dopened', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
        AddDefaultValue(array('field' => 'com_dopened', 'value' => 'TODAY', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
        AddHiddenField(array('field' => 'com_dclosed', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
    }
    else
    {
        foreach ($FormFiles as $id => $FormFile)
        {
            if ($id == 0)
            {
                AddHiddenField(array('field' => 'com_dopened', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
                AddDefaultValue(array('field' => 'com_dopened', 'value' => 'TODAY', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
                AddHiddenField(array('field' => 'com_dclosed', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
            }
            else
            {
                AddHiddenField(array('field' => 'com_dopened', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
                AddDefaultValue(array('field' => 'com_dopened', 'value' => 'TODAY', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
                AddHiddenField(array('field' => 'com_dclosed', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
            }
        }
    }

    if (empty($FormFiles2))
    {
        AddDefaultValue(array('field' => 'com_dopened', 'value' => 'TODAY', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
    }
    else
    {
        foreach ($FormFiles2 as $id => $FormFile)
        {
            if ($id == 0)
            {
                AddDefaultValue(array('field' => 'com_dopened', 'value' => 'TODAY', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
            }
            else
            {
                AddDefaultValue(array('field' => 'com_dopened', 'value' => 'TODAY', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'_'.$id.'.php'));
            }
        }
    }

    //Ensure any inc_mgr_3 entries are replaced and inc_head is hidden
    $module = 'INC';
    $FormFiles = array();
    $FormFiles2 = array();

    if (file_exists($ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
    {
        require $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
    }

    //Ensure any inc_mgr_3 entries are replaced
    if (empty($FormFiles))
    {
        ReplaceFieldKey(array('find' => 'inc_mgr_3', 'replace' => 'inc_mgr', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
    }
    else
    {
        foreach ($FormFiles as $id => $FormFile)
        {
            if ($id == 0)
            {
                ReplaceFieldKey(array('find' => 'inc_mgr_3', 'replace' => 'inc_mgr', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
            }
            else
            {
                ReplaceFieldKey(array('find' => 'inc_mgr_3', 'replace' => 'inc_mgr', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'_'.$id.'.php'));
            }
        }
    }

    //Ensure inc_head is hidden
    if (empty($FormFiles2))
    {
        AddHiddenField(array('field' => 'inc_head', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_1_FILENAME'].'.php'));
    }
    else
    {
        foreach ($FormFiles2 as $id => $FormFile)
        {
            if ($id == 0)
            {
                AddHiddenField(array('field' => 'inc_head', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'.php'));
            }
            else
            {
                AddHiddenField(array('field' => 'inc_head', 'file' => $ClientFolder.'/'.$ModuleDefs[$module]['FORM_DESIGN_LEVEL_2_FILENAME'].'_'.$id.'.php'));
            }
        }
    }

    return true;
}

function AddHiddenField($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        $GLOBALS['HideFields'][$aParams['field']] = true;

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function AddListingDesign($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        $GLOBALS['ListingDesigns'][$aParams['section']][$aParams['key']] = $aParams['listing_id'];

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function MigrateExtraFieldGroups($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (is_array($GLOBALS["DIF1UDFGroups"]))
        {
            foreach ($GLOBALS["DIF1UDFGroups"] as $GroupID)
            {
                $MaxOrder = 0;

                $NextID = count($GLOBALS["ExtraSections"]) + 1;
                $GLOBALS["ExtraSections"][$NextID] = 'Migrated Extra Field Section '.$GroupID;

                if (!$GLOBALS["UserLabels"]['UDFGROUP'.$GroupID])
                {
                    $GroupName = DatixDBQuery::PDO_fetch('SELECT grp_descr FROM udf_groups WHERE recordid = :group_id', array('group_id' => $GroupID), PDO::FETCH_COLUMN);
                    $GLOBALS["UserLabels"]['UDFGROUP'.$GroupID] = $GroupName;
                }

                $Fields = DatixDBQuery::PDO_fetch_all('SELECT recordid, fld_type, listorder FROM udf_links, udf_fields WHERE group_id = :group_id and recordid = field_id ORDER BY listorder', array('group_id' => $GroupID));

                $SectionReplaceArray['UDFGROUP'.$GroupID] = 'section'.$NextID;

                // Find any pre-defined listorders
                if($GLOBALS["FieldOrders"]['UDFGROUP'.$GroupID])

                    $Order = 0;

                if (is_array($GLOBALS["FieldOrders"]['UDFGROUP'.$GroupID]))
                {
                    foreach ($GLOBALS["FieldOrders"]['UDFGROUP'.$GroupID] as $OrderedField)
                    {
                        $Order++;
                        $FieldParts = explode('_', $OrderedField);
                        $FieldOrders[$FieldParts[3]] = $Order;
                    }

                    $MaxOrder = $Order;
                }

                foreach ($Fields as $FieldDetails)
                {
                    // If moved to another section, we don't have to generate an order for it in this section.
                    if (!$GLOBALS["MoveFieldsToSections"]['UDF_'.$FieldDetails['fld_type'].'_'.$GroupID.'_'.$FieldDetails['recordid']])
                    {
                        $GLOBALS["ExtraFields"][$FieldDetails['recordid'].'_'.$GroupID] = 'section'.$NextID;

                        if ($FieldOrders[$FieldDetails['recordid']])
                        {
                            $FieldOrder = $FieldOrders[$FieldDetails['recordid']];
                        }
                        else
                        {
                            $MaxOrder ++;
                            $FieldOrder = $MaxOrder;
                        }

                        $GLOBALS["FieldOrders"]['section'.$NextID][$FieldOrder] = 'UDF_'.$FieldDetails['fld_type'].'_'.$GroupID.'_'.$FieldDetails['recordid'];
                    }
                    else
                    {
                        // moved to another section, so we need to add it to this section
                        $GLOBALS["ExtraFields"][$FieldDetails['recordid'].'_'.$GroupID] = $GLOBALS["MoveFieldsToSections"]['UDF_'.$FieldDetails['fld_type'].'_'.$GroupID.'_'.$FieldDetails['recordid']]['New'];
                        unset($GLOBALS["MoveFieldsToSections"]['UDF_'.$FieldDetails['fld_type'].'_'.$GroupID.'_'.$FieldDetails['recordid']]);
                    }

                    $FieldReplaceArray['UDF_'.$FieldDetails['fld_type'].'_'.$GroupID.'_'.$FieldDetails['recordid']] = 'UDF_'.$FieldDetails['fld_type'].'_'.$GroupID.'_'.$FieldDetails['recordid'];
                }
            }
        }

        unset($GLOBALS["DIF1UDFGroups"]);
        unset($GLOBALS["FieldOrders"]['UDFGROUP'.$GroupID]);

        if ($GLOBALS["FieldOrders"]['section'.$NextID])
        {
            ksort($GLOBALS["FieldOrders"]['section'.$NextID]);
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();

        if (!empty($SectionReplaceArray))
        {
            ReplaceSectionKey(array('file' => $aParams['file'], 'replace_array' => $SectionReplaceArray));
        }

        if (!empty($FieldReplaceArray))
        {
            foreach ($FieldReplaceArray as $find => $replace)
            {
                ReplaceFieldKey(array('find' => $find, 'replace' => $replace, 'file' => $aParams['file']));
            }
        }

        ReplaceSectionKey(array('file' => $aParams['file'], 'replace_array' => array('udffrommain' => 'orphanedudfs')));
    }
}

function AddMandatoryField($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        $GLOBALS['MandatoryFields'][$aParams['field']] = true;

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function RemoveMandatoryField($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        unset($GLOBALS['MandatoryFields'][$aParams['field']]);

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function ReplaceSectionKey($aParams)
{
    global $ClientFolder;

    if (file_exists($aParams['file']))
    {
        $File = fopen($aParams['file'], "r+b");

        while (!feof($File))
        {
            $FileStr = $FileStr . fgets($File, 4096);
        }

        fclose($File);

        foreach ($aParams['replace_array'] as $find => $replace)
        {
            $FileStr = str_replace('\''.$find.'\'', '\''.$replace.'\'', $FileStr);
        }

        $File = fopen($aParams['file'], "wb");

        fwrite($File, $FileStr);

        fclose($File);
    }
}

//custom field used when splitting contact sections
function SplitContactSection($aParams)
{
    global $ModuleDefs;

    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (is_array($GLOBALS['OrderSections']))
        {
            $SectionOrderArray = array_flip($GLOBALS['OrderSections']);

            foreach ($GLOBALS['OrderSections'] as $order => $Section)
            {
                if ($order > $SectionOrderArray['contacts'])
                {
                    $NewOrderArray[$order + count($ModuleDefs[$aParams['module']]['CONTACTTYPES'])] = $Section;
                }
                else
                {
                    $NewOrderArray[$order] = $Section;
                }
            }

            if ($SectionOrderArray['contacts'])
            {
                $increment = 0;

                foreach ($ModuleDefs[$aParams['module']]['CONTACTTYPES'] as $ContactTypeDetails)
                {
                    $NewOrderArray[$SectionOrderArray['contacts'] + $increment] = 'contacts_'.$ContactTypeDetails['Type'];
                    $increment++;
                }
            }

            $GLOBALS['OrderSections'] = $NewOrderArray;
        }

        if ($GLOBALS['NewPanels']['contacts'])
        {
            $PanelSet = false;

            foreach ($ModuleDefs[$aParams['module']]['CONTACTTYPES'] as $ContactTypeDetails)
            {
                if (!$PanelSet)
                {
                    $GLOBALS['NewPanels']['contacts_type_'.$ContactTypeDetails['Type']] = true;
                    $PanelSet = true;
                }
            }
        }

        if (is_array($GLOBALS["ExpandSections"]))
        {
            foreach ($GLOBALS["ExpandSections"] as $field => $actions)
            {
                foreach ($actions as $key => $ActionDetails)
                {
                    if ($ActionDetails['section'] == 'contacts')
                    {
                        foreach ($ModuleDefs[$aParams['module']]['CONTACTTYPES'] as $ContactTypeDetails)
                        {
                            $NewActionDetails = $ActionDetails;
                            $NewActionDetails['section'] = 'contacts_type_'.$ContactTypeDetails['Type'];
                            $GLOBALS["ExpandSections"][$field][] = $NewActionDetails;
                        }

                        unset($GLOBALS["ExpandSections"][$field][$key]);
                    }
                }
            }
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

// Custom field used when moving ConForms data to FormDesigns
function ConFormsToFormDesigns($aParams)
{
    global $ModuleDefs;

    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (is_array($GLOBALS['ContactForms']))
        {
            foreach ($GLOBALS['ContactForms'] as $Type => $Form)
            {
                $GLOBALS['FormDesigns']['contacts_type_'.$Type][$Type] = $Form;
            }

            unset($GLOBALS['ContactForms']);
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function AddReadOnlyField($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        $GLOBALS['ReadOnlyFields'][$aParams['field']] = true;

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function AddDefaultValue($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (!$GLOBALS['DefaultValues'][$aParams['field']])
        {
            $GLOBALS['DefaultValues'][$aParams['field']] = $aParams['value'];
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function RelabelField($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (!$GLOBALS['UserLabels'][$aParams['field']])
        {
            $GLOBALS['UserLabels'][$aParams['field']] = $aParams['value'];
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

// Add section action
function AddSectionAction($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (!$GLOBALS['ExpandSections'][$aParams['field']] || !in_array($aParams['action'], $GLOBALS['ExpandSections'][$aParams['field']]))
        {
            $GLOBALS['ExpandSections'][$aParams['field']][] = $aParams['action'];
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

// Add field action function
function AddFieldShowAction($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (!$GLOBALS['ExpandFields'][$aParams['field']] || !in_array($aParams['action'], $GLOBALS['ExpandFields'][$aParams['field']]))
        {
            $GLOBALS['ExpandFields'][$aParams['field']][] = $aParams['action'];
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function AddExtraText($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (!isset($GLOBALS['UserExtraText'][$aParams['field']]) || $GLOBALS['UserExtraText'][$aParams['field']] == '')
        {
            $GLOBALS['UserExtraText'][$aParams['field']] = $aParams['text'];
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function AddHelpText($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (!isset($GLOBALS['HelpTexts'][$aParams['field']]) || $GLOBALS['HelpTexts'][$aParams['field']] == '')
        {
            $GLOBALS['HelpTexts'][$aParams['field']] = $aParams['text'];
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function ReplaceFieldKey($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        foreach (Forms_FormDesign::getFormDesignGlobals() as $DesignKey)
        {
            $NewArray = array();

            if ($DesignKey == 'FieldOrders')
            {
                if (is_array($GLOBALS[$DesignKey]))
                {
                    foreach ($GLOBALS[$DesignKey] as $Section => $Details)
                    {
                        foreach ($Details as $Order => $Field)
                        {
                            if ($Field == $aParams['find'])
                            {
                                $GLOBALS[$DesignKey][$Section][$Order] = $aParams['replace'];
                            }
                        }
                    }
                }
            }
            elseif (is_array($GLOBALS[$DesignKey]))
            {
                foreach ($GLOBALS[$DesignKey] as $Key => $Details)
                {
                    if ($Key == $aParams['find'])
                    {
                        $NewArray[$aParams['replace']] = $GLOBALS[$DesignKey][$Key];
                    }
                    else
                    {
                        $NewArray[$Key] = $GLOBALS[$DesignKey][$Key];
                    }
                }

                $GLOBALS[$DesignKey] = $NewArray;
            }
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function FormMigrationCode_9_2()
{
    global $ClientFolder;

    $fileArray = array(
        array('file' => 'UserIncidentForms.php', 'level1' => 'UserDIF1Settings', 'level2' => 'UserDIF2Settings'),
        array('file' => 'UserRiskForms.php', 'level1' => 'UserRISK1Settings', 'level2' => 'UserRISK2Settings'),
        array('file' => 'UserCONForms.php', 'level1' => 'UserCONSettings'),
        array('file' => 'UserASTForms.php', 'level1' => 'UserASTSettings'),
        array('file' => 'UserACTForms.php', 'level1' => 'UserACTSettings'),
        array('file' => 'UserSABSForms.php', 'level1' => 'UserSAB1Settings', 'level2' => 'UserSAB2Settings')
    );

    foreach ($fileArray as $fileDetails)
    {
        $FormFiles = array();
        $FormFiles2 = array();

        if (file_exists($ClientFolder.'/'.$fileDetails['file']))
        {
            require $ClientFolder.'/'.$fileDetails['file'];

            if ($fileDetails['level1'])
            {
                if (empty($FormFiles))
                {
                    ReplaceSectionKeys($fileDetails['level1']);
                }
                else
                {
                    foreach ($FormFiles as $id => $FormFile)
                    {
                        if ($id == 0)
                        {
                            ReplaceSectionKeys($fileDetails['level1']);
                        }
                        else
                        {
                            ReplaceSectionKeys($fileDetails['level1'].'_'.$id);
                        }
                    }
                }
            }

            if ($fileDetails['level2'])
            {
                if (empty($FormFiles2))
                {
                    ReplaceSectionKeys($fileDetails['level2']);
                }
                else
                {
                    foreach ($FormFiles2 as $id => $FormFile)
                    {
                        if ($id == 0)
                        {
                            ReplaceSectionKeys($fileDetails['level2']);
                        }
                        else
                        {
                            ReplaceSectionKeys($fileDetails['level2'].'_'.$id);
                        }
                    }
                }
            }
        }
        else
        {
            ReplaceSectionKeys($fileDetails['level1']);
        }
    }

    return true;
}

function ReplaceSectionKeys($FileName)
{
    global $ClientFolder;

    if (file_exists($ClientFolder.'/'.$FileName.'.php'))
    {
        $File = fopen($ClientFolder.'/'.$FileName.'.php', "r+b");

        while (!feof($File))
        {
            $FileStr = $FileStr . fgets($File, 4096);
        }

        fclose($File);

        $replaceArray = array(
            'linkedrecords' => 'linked_records',
            'documents_inc' => 'documents',
            'contacts_inc' => 'contacts',
            'actions_inc' => 'linked_actions',
            'actions_ram' => 'linked_actions',
            'actions_sab' => 'linked_actions',
            'actions_ast' => 'linked_actions',
            'standards_ram' => 'linked_standards',
            'ram_grading' => 'grading'
        );

        if (mb_substr_count($FileName, 'UserASTSettings'))
        {
            $replaceArray['header'] = 'details';
        }

        foreach ($replaceArray as $find => $replace)
        {
            $FileStr = str_replace('\''.$find.'\'', '\''.$replace.'\'', $FileStr);
        }

        $File = fopen($ClientFolder.'/'.$FileName.'.php', "wb");

        fwrite($File, $FileStr);

        fclose($File);
    }
}

function AddNewPanel($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        $GLOBALS['NewPanels'][$aParams['panel']] = true;

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function CopyDefaultFormDesign($module, $formlevel, $CopyFrom, $form_name)
{
    global $scripturl, $yySetLocation, $ModuleDefs;

    LoggedIn();

    // Set formlevel to 1 if it isn't set.  This is so we can use it
    // to get the basic form filename
    if ($formlevel == "" || $formlevel == 0)
    {
        $formlevel = 1;
    }

    if (!$_SESSION["AdminUser"])
    {
        return;
    }

    $FormsFileName = GetFormsFilename($module);

    if (file_exists($FormsFileName))
    {
        require $FormsFileName;
    }
    else
    {
        $FormFiles = "";
        $FormFiles2 = "";
    }

    if (!is_array($FormFiles))
    {
        $Code = GetFormCode(array('module' => $module, 'level' => 1));
        $FormFiles = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Code ? ' ('.$Code.')' : '')." form");
    }

    if (!is_array($FormFiles2))
    {
        $Code = GetFormCode(array('module' => $module, 'level' => 2));
        $FormFiles2 = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Code ? ' ('.$Code.')' : '')." form");
    }

    // Get the highest form number from the array
    $MaxFormNo = 0;

    if ($formlevel == 2)
    {
        foreach ($FormFiles2 as $Number => $Name)
        {
            if ($Number > $MaxFormNo)
            {
                $MaxFormNo = $Number;
            }
        }

        $NewFormNo = $MaxFormNo + 1;

        $FormFiles2[$NewFormNo] = $form_name;
    }
    else
    {
        foreach ($FormFiles as $Number => $Name)
        {
            if ($Number > $MaxFormNo)
            {
                $MaxFormNo = $Number;
            }
        }

        $NewFormNo = $MaxFormNo + 1;

        $FormFiles[$NewFormNo] = $form_name;
    }

    Forms_FormDesign::WriteFormsFile($FormFiles, $FormFiles2, $module);

    // Get the UserSettings filename
    $FromFile = GetSettingsFilename($CopyFrom, $module, $formlevel, "form", "standard");

    // The name of the new UserSettings file
    $FormDesign = new Forms_FormDesign(array('module' => $module, 'level' => $formlevel, 'id' => $NewFormNo));
    $ToFile = $FormDesign->GetFilename(array('NonExistentFile' => 'IGNORE'));

    if (!$FromFile || !$ToFile || !copy($FromFile, $ToFile))
    {
        fatal_error("Cannot copy settings to new file.  Please contact your IT department.");
    }

    $aReturn = array("form_id" => $NewFormNo, "file" => $ToFile);

    return $aReturn;
}

function DefineDefaultDSTContactForm($aParams)
{

    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        $GLOBALS["FormTitle"] = "SABS and Distribution List Contact Form";
        $GLOBALS["Show_all_section"] = "N";
        $GLOBALS["HideFields"] = array (
            'link' => true,
            'icon_cost' => true,
            'injury' => true,
            'link_sirs' => true,
            'link_worked_alone' => true,
            'link_become_unconscious' => true,
            'link_req_resuscitation' => true,
            'link_hospital_24hours' => true,
            'property_section' => true,
            'con_police_number' => true,
            'con_work_alone_assessed' => true,
            'employee' => true,
            'udffrommain' => true,
            'progress_notes' => true,
            'incidents' => true,
            'risks' => true,#
            'pals' => true,
            'complaints' => true,
            'word' => true,
        );

        $GLOBALS["MandatoryFields"] = array (
            'link_role' => 'link',
            'con_surname' => 'contact',
            'con_organisation' => 'contact',
        );

        $GLOBALS["UserExtraText"] = array (
            'con_dod' => 'Fill this in for a patient who has died',
        );

        $GLOBALS["ExpandSections"] = array (
            'show_injury' => array (
                0 => array (
                    'section' => 'injury',
                    'alerttext' => '',
                    'values' => array (
                        0 => 'Y',
                    ),
                ),
            ),
            'link_pprop_damaged' => array (
                0 => array (
                    'section' => 'property_section',
                    'alerttext' => '',
                    'values' => array (
                        0 => 'Y',
                    ),
                ),
            ),
        );

        $GLOBALS["ExpandFields"] = array (
            'link_police_pursue' => array (
                0 => array (
                    'field' => 'link_police_persue_reason',
                    'alerttext' => '',
                    'values' => array (
                        0 => 'N',
                    ),
                ),
            ),
        );

        $GLOBALS["NewPanels"] = array (
            'link' => true,
            'contact' => true,
            'employee' => true,
            'incidents' => true,
            'risks' => true,
            'pals' => true,
            'complaints' => true,
            'word' => true,
        );

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}

function SetContactForm($module, $ModFormID = 0, $CONFormID, $ConLinkType = "N", $FormLevel = 2)
{
    $File = GetSettingsFilename($ModFormID, $module, $FormLevel);

    if (file_exists($File))
    {
        require $File;
    }
    elseif ($ModFormID == 0)
    {
        $StandardFile = GetSettingsFilename($ModFormID, $module, $FormLevel, '' , 'standard');

        if (file_exists($StandardFile))
        {
            require $StandardFile;
        }
    }

    if (!$GLOBALS['ContactForms'][$ConLinkType])
    {
        $GLOBALS['ContactForms'][$ConLinkType] = $CONFormID;
    }

    SaveFormDesignValuesToFile(array('outputfilename' => $File));
    unsetFormDesignSettings();
}

// Returns the relative path to a settings file.
// Parameters:
// $FormID:         The ID number of a form
// $Module:         Three-letter module code
// $formlevel:      Form level - 1 or 2
// $SettType:       String: "form" (form design settings file) or "column"
//                  (listing setting file)
// $SettType2:      String: "user" (UserSettings file) or "standard"
function GetSettingsFilename($FormID = "", $Module = "DIF1", $formlevel = "", $SettType = "form", $SettType2 = "user")
{
    global $ClientFolder, $ModuleDefs;

    $formlevel = ($formlevel == '' ? 1 : $formlevel);
    $Module = ($Module == 'DIF1' ? 'INC' : $Module);

    if (!array_key_exists($Module, $ModuleDefs))
    {
        return '';
    }

    if ($SettType == "column")
    {
        if ($SettType2 == "standard")
        {
            if ($ModuleDefs[$Module]['GENERIC'])
            {
                if (file_exists('Source/generic_modules/'.$ModuleDefs[$Module]['GENERIC_FOLDER'].'/StandardListSettings.php'))
                {
                    $SettingsFilename = 'Source/generic_modules/'.$ModuleDefs[$Module]['GENERIC_FOLDER'].'/StandardListSettings.php';
                }
                else
                {
                    $SettingsFilename = 'Source/generic/StandardListSettings.php';
                }
            }
            else
            {
                $SettingsFilename = "{$ModuleDefs[$Module][LIBPATH]}/Standard{$Module}ListSettings.php";
            }
        }
        elseif ($SettType2 == "user")
        {
            $SettingsFilename = $ClientFolder . "/User{$Module}ListSettings.php";
        }
    }
    else
    {
        if ($SettType2 == "standard")
        {
            if ($ModuleDefs[$Module]['GENERIC'])
            {
                if (file_exists('Source/generic_modules/'.$ModuleDefs[$Module]['GENERIC_FOLDER'].'/StandardLevel'.$formlevel.'Settings.php'))
                {
                    $SettingsFilename = 'Source/generic_modules/'.$ModuleDefs[$Module]['GENERIC_FOLDER'].'/StandardLevel'.$formlevel.'Settings.php';
                }
                else
                {
                    $SettingsFilename = 'Source/generic/StandardLevel'.$formlevel.'Settings.php';
                }
            }
            else
            {
                $SettingsFilename = "{$ModuleDefs[$Module][LIBPATH]}/Standard{$ModuleDefs[$Module][FORMS][$formlevel][CODE]}Settings.php";
            }
        }
        else
        {
            $formCode = $ModuleDefs[$Module]['FORMS'][$formlevel]['CODE'];

            $SettingsFileVar = "User{$formCode}SettingsFile";

            if ($GLOBALS[$SettingsFileVar])
            {
                $SettingsFilename = $GLOBALS[$SettingsFileVar];
            }
            else
            {
                $SettingsFilename = $ClientFolder . "/User{$formCode}Settings";
            }

            if ($FormID)
            {
                $SettingsFilename .= "_$FormID";
            }

            $SettingsFilename .= ".php";
        }

        $_SESSION['LASTUSEDFORMDESIGN'] = $SettingsFilename;
        $_SESSION['LASTUSEDFORMDESIGNFORMLEVEL'] = $formlevel;
    }

    return $SettingsFilename;
}

function SaveFormDesignValuesToFile($aParams)
{
    //form design globals
    global $HideFields, $ReadOnlyFields, $MandatoryFields, $TimestampFields, $OrderSections, $UserLabels, $UserExtraText,
           $DefaultValues, $DropdownWidths, $TextareaMaxChars, $FieldOrders, $DIF1UDFGroups,
           $ExpandSections, $ExpandFields, $HelpTexts, $UDFGroups, $NewPanels,
           $ContactMatch, $ContactForms, $FormDesigns, $EquipmentForms, $ListingDesigns, $MoveFieldsToSections,
           $FormTitle, $FormTitleDescr, $ExtraFields, $ExtraSections, $Show_all_section, $OrganisationMatch,
           $DisplayAsRadioButtons, $DisplayAsCheckboxes;

    // Create output file.
    $OutputFile = @fopen($aParams['outputfilename'], "wb");

    if (!$OutputFile)
    {
        fatal_error("Cannot create settings file.  Please contact your IT department.");
    }

    if (!fwrite($OutputFile,"<?php\n"))
    {
        fatal_error("Cannot write to settings file.  Please contact your IT department.");
    }

    if ($FormTitle)
    {
        fwrite($OutputFile, '$GLOBALS["FormTitle"] = "' . EscapeQuotestoFile(str_replace('\\', '\\\\', $FormTitle)) . "\";\n");
    }

    if ($FormTitleDescr)
    {
        fwrite($OutputFile, '$GLOBALS["FormTitleDescr"] = "' . EscapeQuotestoFile(str_replace('\\', '\\\\', $FormTitleDescr)) . "\";\n");
    }

    if ($Show_all_section)
    {
        fwrite($OutputFile, '$GLOBALS["Show_all_section"] = "' . $Show_all_section . "\";\n");
    }

    if ($HideFields)
    {
        fwrite($OutputFile, '$GLOBALS["HideFields"] = ' . var_export($HideFields, true) . ";\n");
    }

    if ($ReadOnlyFields)
    {
        fwrite($OutputFile, '$GLOBALS["ReadOnlyFields"] = ' . var_export($ReadOnlyFields, true) . ";\n");
    }

    if ($MandatoryFields)
    {
        fwrite($OutputFile, '$GLOBALS["MandatoryFields"] = ' . var_export($MandatoryFields, true) . ";\n");
    }

    if ($DisplayAsCheckboxes)
    {
        fwrite($OutputFile, '$GLOBALS["DisplayAsCheckboxes"] = ' . var_export($DisplayAsCheckboxes, true) . ";\n");
    }

    if ($DisplayAsRadioButtons)
    {
        fwrite($OutputFile, '$GLOBALS["DisplayAsRadioButtons"] = ' . var_export($DisplayAsRadioButtons, true) . ";\n");
    }

    if ($TimestampFields)
    {
        fwrite($OutputFile, '$GLOBALS["TimestampFields"] = ' . var_export($TimestampFields, true) . ";\n");
    }

    if ($OrderSections)
    {
        fwrite($OutputFile, '$GLOBALS["OrderSections"] = ' . var_export($OrderSections, true) . ";\n");
    }

    if ($UserLabels)
    {
        fwrite($OutputFile, '$GLOBALS["UserLabels"] = ' . var_export($UserLabels, true) . ";\n");
    }

    if ($UserExtraText)
    {
        fwrite($OutputFile, '$GLOBALS["UserExtraText"] = ' . var_export($UserExtraText, true) . ";\n");
    }

    if ($DefaultValues)
    {
        fwrite($OutputFile, '$GLOBALS["DefaultValues"] = ' . var_export($DefaultValues, true) . ";\n");
    }

    if ($DropdownWidths)
    {
        fwrite($OutputFile, '$GLOBALS["DropdownWidths"] = ' . var_export($DropdownWidths, true) . ";\n");
    }

    if ($TextareaMaxChars)
    {
        fwrite($OutputFile, '$GLOBALS["TextareaMaxChars"] = ' . var_export($TextareaMaxChars, true) . ";\n");
    }

    if ($FieldOrders)
    {
        fwrite($OutputFile, '$GLOBALS["FieldOrders"] = ' . var_export($FieldOrders, true) . ";\n");
    }

    if ($DIF1UDFGroups)
    {
        fwrite($OutputFile, '$GLOBALS["DIF1UDFGroups"] = ' . var_export($DIF1UDFGroups, true) . ";\n");
    }

    if ($ExpandSections)
    {
        fwrite($OutputFile, '$GLOBALS["ExpandSections"] = ' . var_export($ExpandSections, true) . ";\n");
    }

    if ($ExpandFields)
    {
        fwrite($OutputFile, '$GLOBALS["ExpandFields"] = ' . var_export($ExpandFields, true) . ";\n");
    }

    if ($HelpTexts)
    {
        fwrite($OutputFile, '$GLOBALS["HelpTexts"] = ' . var_export($HelpTexts, true) . ";\n");
    }

    if ($NewPanels)
    {
        fwrite($OutputFile, '$GLOBALS["NewPanels"] = ' . var_export($NewPanels, true) . ";\n");
    }

    if ($ContactMatch)
    {
        fwrite($OutputFile, '$GLOBALS["ContactMatch"] = ' . var_export($ContactMatch, true) . ";\n");
    }

    if ($OrganisationMatch)
    {
        fwrite($OutputFile, '$GLOBALS["OrganisationMatch"] = ' . var_export($OrganisationMatch, true) . ";\n");
    }

    if ($ContactForms)
    {
        fwrite($OutputFile, '$GLOBALS["ContactForms"] = ' . var_export($ContactForms, true) . ";\n");
    }

    if ($FormDesigns)
    {
        fwrite($OutputFile, '$GLOBALS["FormDesigns"] = ' . var_export($FormDesigns, true) . ";\n");
    }

    if ($EquipmentForms)
    {
        fwrite($OutputFile, '$GLOBALS["EquipmentForms"] = ' . var_export($EquipmentForms, true) . ";\n");
    }

    if ($ListingDesigns)
    {
        fwrite($OutputFile, '$GLOBALS["ListingDesigns"] = ' . var_export($ListingDesigns, true) . ";\n");
    }

    if ($MoveFieldsToSections)
    {
        fwrite($OutputFile, '$GLOBALS["MoveFieldsToSections"] = ' . var_export($MoveFieldsToSections, true) . ";\n");
    }

    if ($ExtraSections)
    {
        fwrite($OutputFile, '$GLOBALS["ExtraSections"] = ' . var_export($ExtraSections, true) . ";\n");
    }

    if ($ExtraFields)
    {
        fwrite($OutputFile, '$GLOBALS["ExtraFields"] = ' . var_export($ExtraFields, true) . ";\n");
    }

    if ($LockFieldAtribs)
    {
        fwrite($OutputFile, '$GLOBALS["LockFieldAtribs"] = ' . var_export($LockFieldAtribs, true) . ";\n");
    }


    fwrite($OutputFile, '?>');
    fclose($OutputFile);

    return $OutputFile;
}

/**
 * Attempts to automatically fix LIB/AST1 form design files with malformed filenames (see DW-6367)
 */
function fixMissingFilenames()
{
    global $ClientFolder, $ModuleDefs, $FieldDefs;

    $fielddefs = $FieldDefs;

    // add linked equipment fields, which are defined under INC
    $fielddefs['AST'] = array_merge($fielddefs['AST'], array(
        'link_damage_type' => array(),
        'link_other_damage_info' => array(),
        'link_replaced' => array(),
        'link_replace_cost' => array(),
        'link_repaired' => array(),
        'link_repair_cost' => array(),
        'link_written_off' => array(),
        'link_disposal_cost' => array(),
        'link_sold' => array(),
        'link_sold_price' => array(),
        'link_decommissioned' => array(),
        'link_decommission_cost' => array(),
        'link_residual_value' => array()
    ));

    $badFiles = array();
    $goodFiles = array();

    require 'Source/generic_modules/LIB/BasicForm2.php';

    $moduleSections['LIB'] = array_keys($FormArray);

    require 'Source/assets/BasicForm.php';

    $moduleSections['AST'] = array_keys($FormArray);

    // check client folder for form design files with incorrect filenames
    $dir = scandir($ClientFolder);

    foreach ($dir as $file)
    {
        if ($file == '.php' || preg_match('/^_\d+\.php$/u', $file))
        {
            $badFiles[] = $file;
        }
    }

    // attempt to identify the module form the design file contents
    foreach ($badFiles as $badFile)
    {
        require $ClientFolder .'/'. $badFile;

        $modules = array('LIB' => $ModuleDefs['LIB']['FORM_DESIGN_LEVEL_2_FILENAME'], 'AST' => $ModuleDefs['AST']['FORM_DESIGN_LEVEL_1_FILENAME']);

        // first check all form design arrays where the keys are field names for module-specific fields
        $settings = array('HideFields','ReadOnlyFields','MandatoryFields','TimestampFields','UserLabels','UserExtraText',
            'DefaultValues','TextareaMaxChars','ExpandSections','ExpandFields','HelpTexts','MoveFieldsToSections');

        foreach ($settings as $setting)
        {
            if (is_array($GLOBALS[$setting]))
            {
                foreach (array_keys($GLOBALS[$setting]) as $field)
                {
                    foreach ($modules as $module => $correctFileName)
                    {
                        if (in_array($field, array_diff(array_keys($fielddefs[$module]), array_keys($fielddefs[$module == 'LIB' ? 'AST' : 'LIB']))))
                        {
                            // the field name is unique to this module's field definitions, so we can infer the correct filename for this form design file
                            $goodFiles[$badFile] = $correctFileName . $badFile;
                            break 3;
                        }
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        // next, check all form design arrays where the keys are section names for module-specific sections
        $settings = array('HideFields','ReadOnlyFields','UserLabels','UserExtraText','NewPanels');

        foreach ($settings as $setting)
        {
            if (is_array($GLOBALS[$setting]))
            {
                foreach (array_keys($GLOBALS[$setting]) as $section)
                {
                    foreach ($modules as $module => $correctFileName)
                    {
                        if (in_array($section, array_diff($moduleSections[$module], $moduleSections[$module == 'LIB' ? 'AST' : 'LIB'])))
                        {
                            // the section name is unique to this module, so we can infer the correct filename for this form design file
                            $goodFiles[$badFile] = $correctFileName . $badFile;
                            break 3;
                        }
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        // still no idea what the file should be - let's look at other settings, starting with 'OrderSections'
        if (is_array($GLOBALS['OrderSections']))
        {
            foreach ($GLOBALS['OrderSections'] as $section)
            {
                foreach ($modules as $module => $correctFileName)
                {
                    if (in_array($section, array_diff($moduleSections[$module], $moduleSections[$module == 'LIB' ? 'AST' : 'LIB'])))
                    {
                        // the section name is unique to this module, so we can infer the correct filename for this form design file
                        $goodFiles[$badFile] = $correctFileName . $badFile;
                        break 2;
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        // no? okay, check both sections and field in 'FieldOrders'
        if (is_array($GLOBALS['FieldOrders']))
        {
            foreach ($GLOBALS['FieldOrders'] as $section => $fields)
            {
                foreach ($modules as $module => $correctFileName)
                {
                    // can we identify from the section?
                    if (in_array($section, array_diff($moduleSections[$module], $moduleSections[$module == 'LIB' ? 'AST' : 'LIB'])))
                    {
                        // the section name is unique to this module, so we can infer the correct filename for this form design file
                        $goodFiles[$badFile] = $correctFileName . $badFile;
                        break 2;
                    }

                    // can we identify from any of the fields in this section?
                    foreach ($fields as $field)
                    {
                        if (in_array($field, array_diff(array_keys($fielddefs[$module]), array_keys($fielddefs[$module == 'LIB' ? 'AST' : 'LIB']))))
                        {
                            // the field name is unique to this module's field definitions, so we can infer the correct filename for this form design file
                            $goodFiles[$badFile] = $correctFileName . $badFile;
                            break 3;
                        }
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        // we're running out of options - perhaps one of the sections in 'ExpandSections'?
        if (is_array($GLOBALS['ExpandSections']))
        {
            foreach ($GLOBALS['ExpandSections'] as $field => $sections)
            {
                foreach ($modules as $module => $correctFileName)
                {
                    foreach ($sections as $section)
                    {
                        if (in_array($section['section'], array_diff($moduleSections[$module], $moduleSections[$module == 'LIB' ? 'AST' : 'LIB'])))
                        {
                            // the section name is unique to this module, so we can infer the correct filename for this form design file
                            $goodFiles[$badFile] = $correctFileName . $badFile;
                            break 3;
                        }
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        // or fields in 'ExpandFields'?
        if (is_array($GLOBALS['ExpandFields']))
        {
            foreach ($GLOBALS['ExpandFields'] as $f => $fields)
            {
                foreach ($modules as $module => $correctFileName)
                {
                    foreach ($fields as $field)
                    {
                        if (in_array($field['field'], array_diff(array_keys($fielddefs[$module]), array_keys($fielddefs[$module == 'LIB' ? 'AST' : 'LIB']))))
                        {
                            // the field name is unique to this module's field definitions, so we can infer the correct filename for this form design file
                            $goodFiles[$badFile] = $correctFileName . $badFile;
                            break 3;
                        }
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        // perhaps one of the sections in 'MoveFieldsToSections'...
        if (is_array($GLOBALS['MoveFieldsToSections']))
        {
            foreach ($GLOBALS['MoveFieldsToSections'] as $sections)
            {
                foreach ($modules as $module => $correctFileName)
                {
                    if (in_array($sections['Original'], array_diff($moduleSections[$module], $moduleSections[$module == 'LIB' ? 'AST' : 'LIB'])))
                    {
                        // the section name is unique to this module, so we can infer the correct filename for this form design file
                        $goodFiles[$badFile] = $correctFileName . $badFile;
                        break 2;
                    }

                    if (in_array($sections['New'], array_diff($moduleSections[$module], $moduleSections[$module == 'LIB' ? 'AST' : 'LIB'])))
                    {
                        // the section name is unique to this module, so we can infer the correct filename for this form design file
                        $goodFiles[$badFile] = $correctFileName . $badFile;
                        break 2;
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        // last chance - sections in 'ExtraFields'
        if (is_array($GLOBALS['ExtraFields']))
        {
            foreach ($GLOBALS['ExtraFields'] as $section)
            {
                foreach ($modules as $module => $correctFileName)
                {
                    if (in_array($section, array_diff($moduleSections[$module], $moduleSections[$module == 'LIB' ? 'AST' : 'LIB'])))
                    {
                        // the section name is unique to this module, so we can infer the correct filename for this form design file
                        $goodFiles[$badFile] = $correctFileName . $badFile;
                        break 2;
                    }
                }
            }
        }

        if (isset($goodFiles[$badFile]))
        {
            // we've identified the correct file name, move onto the next bad file
            unsetFormDesignSettings();
            continue;
        }

        unsetFormDesignSettings();
    }

    // rename files that we've identified
    foreach ($badFiles as $key => $badFile)
    {
        if (isset($goodFiles[$badFile]))
        {
            rename($ClientFolder .'/'. $badFile, $ClientFolder .'/'. $goodFiles[$badFile]);
            unset($badFiles[$key]);
        }
    }

    // report files that couldn't be automatically fixed
    $message = '';

    if (!empty($badFiles))
    {
        foreach ($badFiles as $badFile)
        {
            $message .= $ClientFolder .'/'. $badFile.'<br />';
        }
    }

    if ($message != '')
    {
        $message = 'An error has been detected with the following form design files:<br /><br />' . $message . '<br />Please contact Datix Support.';
        AddSessionMessage('ERROR', $message);
    }
}

/**
 * Detects security groups with blank where clauses.
 */
function getBlankWhereClauses()
{
    global $ModuleDefs;

    $output = \src\security\groups\model\Group::getGroupsWithBlankWhereClauses();

    if (empty($output))
    {
        $message = 'The system has detected that there are no security groups with blank where clauses.';
    }
    else
    {
        $message = '<div style="margin-bottom:20px;">The following security groups have blank where clauses for the listed modules:</div>';

        foreach ($output as $id => $details)
        {
            $message .= '<div style="font-weight:bold;margin-bottom:10px;">'.$id.': '.htmlspecialchars($details['name']).'</div>';

            foreach ($details['modules'] as $module)
            {
                $message .= '<div style="margin-left:20px">'.htmlspecialchars($ModuleDefs[$module]['NAME']).'</div>';
            }

            $message .= '<div style="margin-top:20px"></div>';
        }
    }

    AddSessionMessage('INFO', $message);
}

function RemoveSectionReadOnly($aParams)
{
    if (file_exists($aParams['file']))
    {
        require $aParams['file'];

        if (isset($GLOBALS['ReadOnlyFields'][$aParams['section']]) || $GLOBALS['ReadOnlyFields'][$aParams['section']] === true)
        {
            unset($GLOBALS['ReadOnlyFields'][$aParams['section']]);
        }

        SaveFormDesignValuesToFile(array('outputfilename' => $aParams['file']));
        unsetFormDesignSettings();
    }
}
