<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */

include_once "constants.php";

class datixLDAP {

    var $ldap; // object
    var $error; // boolean
    var $ldapErrorMsg; // string
    var $user; // object
    var $DatixAuthentication; // boolean
    var $server_index; // integer
    var $ldap_errorno; // integer
    var $ldap_conn; // integer
    var $user_dn;   // The user's distinguished name
    var $user_cn;
    var $UID;       // The user's UID or sAMAccountName
    var $group; // object
    var $usergroups;
    var $userprofiles; 

    function DatixLDAP() 
    {
        global $ldap_config;

        // Include the LDAP config file if it exists.
        $LDAPConfigFile = $this->GetLDAPConfigFile();
        if (file_exists($LDAPConfigFile))
        {
            include($LDAPConfigFile);
        }

        $this->ldap = $ldap_config;
    }

    function ErrorMsg() 
    {
        return $this->ldapErrorMsg;
    }

    function LDAPErrorNo() 
    {
        return $this->$ldap_errorno;
    }

    function DatixAuthentication() {
        return $this->DatixAuthentication;
    }

    function LDAPEnabled() {
        return $this->ldap["enable"];
    }

    function ServerIndex() {
        return $this->server_index;
    }

    function LDAPSettings() 
    {
        // LDAP settings used for the current LDAP connection
        return $this->ldap["settings"][$this->server_index];
    }

    // Returns the user array
    function GetUser()
    {
        return $this->user;
    }

    function LDAPDomains() 
    {
        return $this->ldap["domains"];
    }

    function GetLDAPConfigFile()
    {
        global $UserLDAPConfigFile, $ClientFolder;

        if ($UserLDAPConfigFile)
        {
            return $UserLDAPConfigFile;
        }
        else
        {
            return $ClientFolder . "/LDAPConfig.php";
        }
    }

    // Function performs a bind, then checks to see if the user exists
    // in the directory, then gets all the groups of which the user is a
    // member.
    // If WEB_NETWORK_LOGIN = 'Y' (i.e. we are using SSO), $p_a_s_s_w_o_r_d  is ignored and no bind is
    // attempted using the username and p_a_s_s_w_o_r_d.
    function AuthenticateUser($user, $password, $ldap_index = 0)
    {
        $ldap = $this->BindWithAvailableServer($user, $password, $ldap_index);

        if ($this->ldap_errorno != LDAP_SUCCESS) 
        {

            // "ldap_only" means do not attempt DATIX authentication if
            // LDAP authentication has failed for some reason.
            if ($this->ldap['ldap_only'])
            {
                $this->error = true;
                $this->ldapErrorMsg = "LDAP Error - " . ldap_error($ldap["conn"]);
                return false;
            }
            else 
            {
                // Could not connect to any of the servers: use DATIX authentication
                $this->error = true;
                $this->DatixAuthentication = true;
                return false;
            }
        }

        // Set the UID from the user name passed to this function.
        $UserIDParts = explode('\\', $user);
        if (count($UserIDParts) > 1) 
        {
            $this->UID = $UserIDParts[1];
        }
        else 
        {
            $this->UID = $UserIDParts[0];
        }

        $SearchFilter = "($ldap[att_account]={$this->UID})";
        //echo $SearchFilter;
        //echo $user;
        $ldap['result'] = @ldap_search($ldap["conn"], $ldap['base_dn'], $SearchFilter, $ldap['user_search_attributes']);

        $this->ldap_errorno = ldap_errno($ldap["conn"]);

        if ($this->ldap_errorno != LDAP_SUCCESS) 
        //if (true)   // For testing only!
        {
            $this->error = true;
            $this->ldapErrorMsg = "LDAP Search Error - " . ldap_error($ldap["conn"]);
            return false;
        }



        // Retrieve user distinguished name
        // scan tree structure from base dn
        $ldap['info'] = @ldap_get_entries($ldap["conn"], $ldap['result']);

        $this->ldap_errorno = ldap_errno($ldap["conn"]);

        if ($this->ldap_errorno != LDAP_SUCCESS) 
        {
            $this->error = true;
            $this->ldapErrorMsg = "LDAP Error - " . ldap_error($ldap["conn"]);
            return false;
        }

        // Return an error if the search didn't return anything
        if ($ldap['info']['count'] === 0)
        {
            $this->error = true;
            // Allow Datix authentication if ldap_only is NOT set!
            if (!$this->ldap['ldap_only'])
            {
                $this->DatixAuthentication = true;
            }
            $this->ldapErrorMsg = 'User does not exist in LDAP directory';
            return false;
        }

        // TO DO: if we have bound anonymously, re-bind with the user name
        // and p_a_s_s_w_o_r_d.

        // User entry has been found.  Retrieve active directory groups
        // and their distinguished names.
        // Assume that only one entry has been returned for the DN, user 
        // should be unique.
        $this->ldap['memberof'] = $ldap['info'][0][$ldap['att_memberof']];

        $DN = $ldap['info'][0][$ldap['att_dn']];
        if (is_array($DN))
        {
            $this->user_dn = $DN[0];
        }
        else
        {
            $this->user_dn = $DN;
        }

        $SIDentry = ldap_first_entry($ldap["conn"], $ldap['result']);

        // There may in fact be more than one cn for the user, but here
        // we are just getting the first one.
        $this->user_cn = $ldap['info'][0]['cn'][0];

        // Set the SID value.
        if ($ldap['att_sid'] == '')
        {
            $ldap['att_sid'] = 'objectsid';
            $ldap['att_sid_type'] = 'binary';
        }

        if ($ldap['att_sid_type'] == 'binary' && !empty($SIDentry))
        {
            $this->user['sid'] = bin2hex(implode("", ldap_get_values_len($ldap["conn"], $SIDentry, $ldap['att_sid'])));
        }
        else
        {
            $this->user['sid'] = $ldap['info'][0][$ldap['att_sid']][0];
        }
        if (isset($ldap['ldap_to_datix_data_mapping']))
        {
            // Get user details
            foreach ($ldap['ldap_to_datix_data_mapping'] as $datix_con_field => $ldap_field)
            {
                $this->user[$datix_con_field] = EscapeQuotes($ldap['info'][0][$ldap_field][0]);
            }
        }
        // Set the login name
        $this->user['login'] = $user;

        return true;
    }

    // I don't think this is used anywhere.
    function SearchLDAP($ldap_conn, $ldap_base_dn, $userdn)
    {
        $ldap['result'] = @ldap_search($ldap_conn, $ldap_base_dn, $userdn);

        $this->ldap_errorno = ldap_errno($ldap_conn);

        if ($this->ldap_errorno != LDAP_SUCCESS) 
        {
            $this->error = true;
            $this->ldapErrorMsg = "LDAP Error - " . ldap_error($ldap_conn);
            return false;
        }

        // Retrieve user distinguished name
        // scan tree structure from base dn
        $ldap['info'] = @ldap_get_entries($ldap_conn, $ldap['result']);

        $this->ldap_errorno = ldap_errno($ldap_conn);

        if ($this->ldap_errorno != LDAP_SUCCESS) 
        {
            $this->error = true;
            $this->ldapErrorMsg = "LDAP Error - " . ldap_error($ldap_conn);
            return false;
        }
        return true;
    }           

    function GetUserDetails() 
    {
        return $this->user;
    }

    // Used in LDAPBrowser
    function GetContainerContents($resource, $dn, $size_limit = 0, 
        $filter = '(objectClass=*)', $deref = LDAP_DEREF_ALWAYS, 
        $mappableObjects) 
    {

        global $dtxdebug;

        $return = array();

        // $ds is a valid link identifier (see ldap_connect)
        ldap_set_option($resource, LDAP_OPT_PROTOCOL_VERSION, 3);

        // enable pagination with a page size of 1000.
        $pageSize = GetParm('LDAP_PAGESIZE', 1000);

        $cookie = '';
        do {
            ldap_control_paged_result($resource, $pageSize, true, $cookie);

            $searchResult = @ldap_list($resource, $dn, $filter);

            if (!is_array($mappableObjects))
                $mappableObjects = array('user', 'group');


            $search = ldap_get_entries( $resource, $searchResult);

            for ($i = 0; $i < $search['count']; $i++)
            {
                $entry = $search[$i];

                if (array_key_exists('displayname', $entry))
                    $title = $entry['displayname'][0];
                elseif (array_key_exists('name', $entry))
                    $title = $entry['name'][0];
                else
                    $title = $entry['dn'];

                $object = array(
                    'dn' => $entry['dn'],
                    'title' => $title
                );

                if (is_array($entry['objectclass']))
                {

                    if ($dtxdebug)
                        $object['objectclass'] = implode(',', $entry['objectclass']);

                    foreach ($entry['objectclass'] as $objectclass)
                    {
                        if (in_array($objectclass, $mappableObjects))
                        {
                            $object['mappable'] = true;
                            break;
                        }
                    }
                }

                $return[] = $object;
            }

            ldap_control_paged_result_response($resource, $searchResult, $cookie);

        } while($cookie !== null && $cookie != '');

        return $return;
    }


    function Connect($user = "", $password = "", $ldap_index = 0)
    {

        $Connection = $this->BindWithAvailableServer($user, $password, $ldap_index);

        if ($this->ldap_errorno != LDAP_SUCCESS) 
        {
            if ($this->ldap['ldap_only'])
            {
                // we haven't managed to connect to any of the servers
                $this->error = true;
                $this->ldapErrorMsg = "LDAP Error - " . ldap_error($Connection['conn']);
                return false;
            }
            else 
            {
                // Could not connect to any of the servers: use DATIX authentication
                $this->error = true;
                $this->DatixAuthentication = true;
                return false;
            }
        }

        return $Connection;
    }

    function GetGroupInfo($user, $password, $group_dn, $ldap_index = 0)
    {
        if (!$this->ldap["enable"]) 
        {
            // Use DATIX authentication
            $this->error = true;
            $this->DatixAuthentication = true;
            return false;
        }

        $ldap = $this->Connect($user, $password, $ldap_index);

        /**
         * Domain Users group is a special case that we can't handle using the "membership" attribute
         * we use for other groups below.
         */
        if (preg_match('/^CN=Domain Users/iu',$group_dn) == 1)
        {
            /**
             * We lock this functionality down using a non-public global as it is not the way we advise users
             * to set the system up.
             */
            if (!bYN(GetParm('SYNC_DOMAIN_USERS_GROUP', 'N')))
            {
                $this->error = true;
                $this->ldapErrorMsg = 'LDAP Error - Cannot sync Domain Users group.';
                return false;
            }

            $SearchString = "primaryGroupID=513";

            ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);

            // enable pagination with a default page size of 1500.
            $maxValRange = GetParm('LDAP_MAXVALRANGE', 1500);

            $cookie = '';
            do
            {
                /*
                 * TODO: The "range" method used for groups later may be replacable with this paged method:
                 * check whether we can simplify these blocks.
                 */
                ldap_control_paged_result($ldap['conn'], $maxValRange, true, $cookie);

                $result  = ldap_search($ldap['conn'], $ldap['base_dn'], $SearchString, $ldap['group_search_attributes']);
                $entries = ldap_get_entries($ldap['conn'], $result);

                foreach ($entries as $e)
                {
                    $allMembers[] = $e['dn'];
                }

                ldap_control_paged_result_response($ldap['conn'], $result, $cookie);

            } while($cookie !== null && $cookie != '');

        }
        else
        {
            $group_dn = $this->ldap_escape($group_dn);
            $SearchString = "$ldap[att_dn]=$group_dn";

            // $ds is a valid link identifier (see ldap_connect)
            ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);

            $maxValRange = GetParm('LDAP_MAXVALRANGE', 1500);

            $firstMember = 0;
            $lastMember = $maxValRange-1;

            $allMembers = array();

            $lastGroup = false;

            do
            {
                $memberAttributeName = 'member;range='.$firstMember.'-'.$lastMember;
                $ldap['group_search_attributes'][0] = $memberAttributeName;

                $ldap['result'] = @ldap_search($ldap['conn'], $ldap['base_dn'], $SearchString, $ldap['group_search_attributes']);

                $this->ldap_errorno = ldap_errno($ldap['conn']);

                if ($this->ldap_errorno != LDAP_SUCCESS)
                {
                    $this->error = true;
                    $this->ldapErrorMsg = 'LDAP Error - ' . ldap_error($ldap['conn']);
                    return false;
                }

                // Retrieve user distinguished name
                // scan tree structure from base dn
                $ldap['groupinfo'] = @ldap_get_entries($ldap['conn'], $ldap['result']);

                $this->ldap_errorno = ldap_errno($ldap['conn']);

                if ($this->ldap_errorno != LDAP_SUCCESS) {
                    $this->error = true;
                    $this->ldapErrorMsg = 'LDAP Error - ' . ldap_error($ldap['conn']);
                    return false;
                }

                $currentMembers = $ldap['groupinfo'][0][$memberAttributeName];

                if (count($currentMembers) == 0)
                {
                    //check for additional members in the stupid alternate names that they might have.
                    $currentMembers = $ldap['groupinfo'][0]['member;range='.$firstMember.'-*'];
                    //if we loop again after this, we will get an ldap error.
                    $lastGroup = true;
                }

                if (count($currentMembers) == 0)
                {
                    //check for additional members in the stupid alternate names that they might have.
                    $currentMembers = $ldap['groupinfo'][0]['member'];
                    //if we loop again after this, we will get an ldap error.
                    $lastGroup = true;
                }

                unset($currentMembers['count']);

                if (count($currentMembers) > 0)
                {
                    $allMembers = array_merge($allMembers, $currentMembers);
                }

                $firstMember += $maxValRange;
                $lastMember += $maxValRange;
            }
            while (count($currentMembers) > 0 && !$lastGroup);

        }

        $this->group['member'] = $allMembers;
        $this->group['member']['count'] = count($allMembers);

        // User entry has been found retrieve active directory groups
        // and their distinguished names.
        // Assume that only one entry has been returned, user should be unique.
        //$this->group['member'] = $ldap['groupinfo'][0]['member;range=0-1499'];
        $this->group['samaccountname'] = $ldap['groupinfo'][0][$ldap['att_account']][0];

        return true;
    }

    function GetUserInfo($user, $password, $target_user_dn, $ldap_index = 0) {

        if (!$this->ldap["enable"]) {
                // Use DATIX authentication
                $this->error = true;
                $this->datixAuthentication = true;
                return false;
        }
        
        $ldap = $this->BindWithAvailableServer($user, $password, $ldap_index);

        $this->ldap_errorno = ldap_errno($ldap["conn"]);

        //Make sure a p_a_s_s_w_o_r_d is specified as for some reason LDAP authenticates if no p_a_s_s_w_o_r_d is given.
        if ($password == "")
            $this->ldap_errorno = LDAP_INVALID_CREDENTIALS;


        if ($this->ldap_errorno <> LDAP_SUCCESS) {

            if ($_SESSION["Globals"]["LDAP_ONLY"] == "Y") {
                // we haven't managed to connect to any of the servers
                $this->error = true;
                $this->ldapErrorMsg = "LDAP Error - " . ldap_error($ldap["conn"]);
                return false;
            }
            else {
                // Could not connect to any of the servers: use DATIX authentication
                $this->error = true;
                $this->datixAuthentication = true;
                return false;
            }
        }
        
        $SearchString = "$ldap[att_dn]=$target_user_dn";
        $SearchString =  $this->ldap_escape($SearchString);  
        //$SearchString = preg_replace('/\\\\,/', '\\\\\\\\,', $SearchString);

        $ldap['result'] = @ldap_search($ldap["conn"], $ldap['base_dn'], 
            $SearchString);
        
        $this->ldap_errorno = ldap_errno($ldap["conn"]);

        if ($this->ldap_errorno <> LDAP_SUCCESS) {
            $this->error = true;
            $this->ldapErrorMsg = "LDAP Error - " . ldap_error($ldap["conn"]);
            return false;
        }

        // Retrieve user distinguished name
        // scan tree structure from base dn
        $ldap['userinfo'] = @ldap_get_entries($ldap["conn"], $ldap['result']);

        $this->ldap_errorno = ldap_errno($ldap["conn"]);

        if ($this->ldap_errorno <> LDAP_SUCCESS) {
            $this->error = true;
            $this->ldapErrorMsg = "LDAP Error - " . ldap_error($ldap["conn"]);
            return false;
        }

        // User entry has been found retrieve active directory groups
        // and their distinguished names.
        // Assume that only one entry has been returned, user should be unique.

        $SIDentry = ldap_first_entry($ldap["conn"], $ldap['result']);

        $this->user_cn = $ldap['userinfo'][0]['cn'][0];
        // Get active directory user details
        $this->user['sid'] = bin2hex(implode("", ldap_get_values_len ( $ldap["conn"], $SIDentry, "objectsid")));
        $this->user['login'] = $this->ldap['domains'][$this->server_index] . "\\" . $ldap['userinfo'][0]['samaccountname'][0];
        $this->user['objectclass'] = implode(';', $ldap['userinfo'][0]['objectclass']);
        $this->user['useraccountcontrol'] = $ldap['userinfo'][0]['useraccountcontrol'][0];
        
        foreach ($ldap['ldap_to_datix_data_mapping'] as $datix_con_field => $ldap_field)
        {
            $this->user[$datix_con_field] = EscapeQuotes($ldap['userinfo'][0][$ldap_field][0]);         
        }

        return true;
    }

    // Used in Login.php login2()
    function CheckUserAccess() 
    {
        global $dtxdebug;
        // check whether user is granted access by one of the groups
        // store all valid group mappings for later synchronizing

        $userHasAccess = false;
        $where = array();
        $ValidGroups = array();

        // $i changed to start at 0 instead of 1 (JEH 24/10/07)
        for($i = 0; $i < $this->ldap['memberof']['count']; $i++) 
        {
            $where[] = "lg.ldap_dn LIKE '" . EscapeQuotes($this->ldap['memberof'][$i]) . "'";
        }

        $where[] = "lg.ldap_dn LIKE '" . EscapeQuotes($this->user_dn) . "'";
        
        //Now need to build the DN for parent DN of the user to pick up any OU's mapped
        $explodedDN = ldap_explode_dn ($this->user_dn,0);

        for ($i = 1; $i < $explodedDN['count']; $i++)
        {
            $ParentDNPieces[] = $explodedDN[$i];
        }
        $ParentDN = implode(",", $ParentDNPieces);
        $where[] = "ldap_dn LIKE '" . EscapeQuotes($ParentDN) . "'";

        $whereString = '(' . implode(' OR ', $where) . ')';

        $sql = "SELECT DISTINCT g.recordid AS [grp_id]
            FROM sec_groups g
            JOIN sec_ldap_groups lg ON g.recordid = lg.grp_id
            AND $whereString";

        $request = db_query($sql);

        while ($row = db_fetch_array($request)) 
        {
            $ValidGroups[] = $row['grp_id'];
        }

        if (!empty($ValidGroups)) 
        {
            $this->usergroups = $ValidGroups;
            return true;
        }

        $this->error = true;
        $this->ldapErrorMsg = "User does not belong to a valid security group.";
        return false;
    }
    
    function CheckUserProfileAccess()
    {
        global $dtxdebug;

        $userHasAccess = false;
        $where = array();
        $ValidProfiles = array();

        // $i changed to start at 0 instead of 1 (JEH 24/10/07)
        for($i = 0; $i < $this->ldap['memberof']['count']; $i++) 
        {
            $where[] = "ldap_dn LIKE '" . EscapeQuotes($this->ldap['memberof'][$i]) . "'";
        }  

        $where[] = "ldap_dn LIKE '" . EscapeQuotes($this->user_dn) . "'";
        
        //Now need to build the DN for parent DN of the user to pick up any OU's mapped
        $explodedDN = ldap_explode_dn ($this->user_dn,0);

        for ($i = 1; $i < $explodedDN['count']; $i++)
        {
            $ParentDNPieces[] = $explodedDN[$i];
        }
        $ParentDN = implode(",", $ParentDNPieces);
        $where[] = "ldap_dn LIKE '" . EscapeQuotes($ParentDN) . "'";
        
        $whereString = '(' . implode(' OR ', $where) . ')';

        $sql = "SELECT recordid AS [pfl_id], ISNULL(pfl_ldap_order , 2147483647) as porder
            FROM profiles
            JOIN sec_ldap_profiles ON profiles.recordid = pfl_id
            AND $whereString
            ORDER BY porder, recordid";

        $request = db_query($sql);

        while ($row = db_fetch_array($request)) 
        {
            $ValidProfiles[] = $row['pfl_id'];
        }
   
        if (!empty($ValidProfiles)) 
        {
            $this->userprofiles = $ValidProfiles;
            return true;
        }

        $this->error = true;
        $this->ldapErrorMsg = "User does not belong to a valid user profile.";
        return false;
    }

    // Function updates contacts_main based on $this->user['sid'], making sure
    // that information is not overwritten by empty LDAP fields.
    function UpdateContactRecordBySID($recordid = '')
    {
        global $txt;
        
        $UpdateArray = array('login' => EscapeQuotes($this->user['login'])); 
        //If user account is disabled or locked out in AD, then we need to lock user out in Datix as well, otherwise, unlock if necessary.
        //The ldap properties for "disabled" and "locked" are stored as bitwise values - so I'm converting to a binary string and then
        //examining the appropriate bit here. see also: http://ldapwiki.willeke.com/wiki/User-Account-Control%20Attribute#section-User-Account-Control+Attribute-DirxmlValues
        $UserDisabled = (\UnicodeString::substr(decbin($this->user['useraccountcontrol']),\UnicodeString::strlen(decbin($this->user['useraccountcontrol']))-2,1) == '1');
        $UserLockedOut = (\UnicodeString::substr(decbin($this->user['useraccountcontrol']),\UnicodeString::strlen(decbin($this->user['useraccountcontrol']))-4,1) == '1');

        if($UserDisabled || $UserLockedOut)
        {
            $UpdateArray['lockout'] = "Y";   
            $UpdateArray['sta_lockout_dt'] = date('Y-m-d H:i:s');  
            $UpdateArray['sta_lockout_reason'] = $txt["ldap_disable_lockout_reason"];  
        }
        else
        {
            $UpdateArray['lockout'] = "N";
            $UpdateArray['sta_lockout_dt'] = NULL;  
            $UpdateArray['sta_lockout_reason'] = "";  
        }
            
        foreach ($this->ldap['settings'][$this->server_index]['ldap_to_datix_data_mapping'] as $datix_con_field => $ldap_field)
        {
            $UpdateArray[$datix_con_field] = $this->user[$datix_con_field];
        }

        // Make up the SET part of the UPDATE statement
        foreach ($UpdateArray as $ColumnName => $Value)
        {
            //if (!empty($Value))
            //{
                $ff_def = getFieldFormat($ColumnName, 'CON' , 'contacts_main');
                if (!empty($ff_def["fmt_data_type"]) && array_search($ff_def["fmt_data_type"], array('S', 'L', 'C', 'D')) === false)
                {
                    $SetArray[] = "$ColumnName = $Value";
                }
                else if ($ff_def["fmt_data_type"] == "D")
                {
                    //Convert date from AD time stamp to unix time stamp
                    if (is_numeric($Value) === true) 
                    {
                        $Value = $this->ConvertADDateToUnixDate($Value);
                    }
                    if (!empty($Value))
                    {
                        $SetArray[] = "$ColumnName = '$Value'";
                    }
                    else
                    {
                        $SetArray[] = "$ColumnName = NULL"; 
                    }
                }
                else
                { 
                    $SetArray[] = "$ColumnName = '$Value'";
                }
           // }
        }

        if(GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS')
        {
            if ($this->userprofiles[0])
            {
               $SetArray[] = "sta_profile = " . $this->userprofiles[0];
            }
        }

        if ($SetArray)
        {
            $SetString = implode(',', $SetArray);

            $sql = "UPDATE contacts_main SET 
                $SetString 
                WHERE con_sid = '" . $this->user["sid"] . "'";

            $result = db_query($sql);

            if (affected_rows($result) == 0)
            {
                return false;
            }
            else
            {
                $sql = "INSERT INTO full_audit
                    (aud_module, aud_record,
                    aud_login, aud_date, aud_action,
                    aud_detail)
                    VALUES
                    (:aud_module, :aud_record, :aud_login, :aud_date, :aud_action, :aud_detail)";
                PDO_query($sql, array(
                    "aud_module" => 'CON',
                    "aud_record" => $recordid,
                    "aud_login" => $_SESSION["initials"],
                    "aud_date" => date('Y-m-d H:i:s'),
                    "aud_action" => "LDAP:UPDATE",
                    "aud_detail" => 'Contact updated via LDAP sync.'
                ));
                return true;
            }
        }
        return true;
    }

    // Creates a new contact record from the information stored in
    // $this->user.
    function CreateContactRecord()
    {
        $LDAP_con_id = GetNextRecordID("contacts_main", true);
        if (!$LDAP_con_id)
        {
            return 0;
        }
        
        foreach ($this->ldap['settings'][$this->server_index]['ldap_to_datix_data_mapping'] as $datix_con_field => $ldap_field)
        {
            $UpdateArray[$datix_con_field] = $this->user[$datix_con_field];       
        }

         // Make up the SET part of the UPDATE statement
        if (is_array($UpdateArray))
        {
            foreach ($UpdateArray as $ColumnName => $Value)
            {
                if ($Value != '')
                {
                    $ff_def = getFieldFormat($ColumnName, 'CON' , 'contacts_main');
                    if (array_search($ff_def["fmt_data_type"], array('S', 'L', 'C', 'D')) === false)
                    {
                        $SetArray[] = "$ColumnName = $Value";
                    }
                    else if ($ff_def["fmt_data_type"] == "D")
                    {
                        //Convert date from AD time stamp to unix time stamp
                        if (is_numeric($Value) === true) 
                        {
                            $Value = $this->ConvertADDateToUnixDate($Value);
                        }
                        if (!empty($Value))
                        {
                            $SetArray[] = "$ColumnName = '$Value'";
                        }
                        else
                        {
                            $SetArray[] = "$ColumnName = NULL"; 
                        }
                    }
                    else
                    { 
                        $SetArray[] = "$ColumnName = '$Value'";
                    }
                }
            }   
        }
   
        $Permission = '1792;3|256;3|1807;1|2560;1|271;1|527;1|512;3|1024;3|1039;1|2319;1|2304;3|783;1|2048;3|2063;1|1295;1|1280;3|1536;3|1551;1';  
        
        if ($this->userprofiles[0])
        {
           $SetArray[] = "sta_profile = " . $this->userprofiles[0];    
        }

        if ($SetArray)
        {
            $SetString = implode(',', $SetArray);

            $sql = "UPDATE contacts_main SET 
                $SetString 
                WHERE con_sid = '" . $this->user["sid"] . "'";
                
            $sql = "UPDATE contacts_main SET
            login = '" . EscapeQuotes($this->user['login']) . "',
            sta_user_type = 'WEB',
            permission = '$Permission',
            con_dopened = '" . date('Y-m-d H:i:s') . "',
            con_sid = '" . $this->user['sid'] . "',
            initials = '$LDAP_con_id',
            rep_approved = 'FA',
            $SetString
            WHERE recordid = '$LDAP_con_id'";

            $result = db_query($sql);

            if (affected_rows($result) == 0)
            {
                return 0;
            }
            else
            {
                $sql = "INSERT INTO full_audit
                    (aud_module, aud_record,
                    aud_login, aud_date, aud_action,
                    aud_detail)
                    VALUES
                    (:aud_module, :aud_record, :aud_login, :aud_date, :aud_action, :aud_detail)";
                PDO_query($sql, array(
                    "aud_module" => 'CON',
                    "aud_record" => $LDAP_con_id,
                    "aud_login" => $_SESSION["initials"],
                    "aud_date" => date('Y-m-d H:i:s'),
                    "aud_action" => "LDAP:ADD",
                    "aud_detail" => 'Contact added via LDAP sync.'
                ));

                return $LDAP_con_id;
            }
        }

    }

    // Private function BindWithAvailableServer() does an LDAP bind and 
    // returns the $ldap array.
    // It attempts to connect to the designated server, then does a bind.  If the
    // bind fails, it returns an error.
    function BindWithAvailableServer($User, $Password, $ldap_index = 0)
    {
        $ldap = $this->ldap['settings'][$ldap_index];
        $ldap['conn'] = @ldap_connect($ldap['host'],$ldap['port']);

        @ldap_set_option($ldap["conn"],LDAP_OPT_REFERRALS,0);
        @ldap_set_option($ldap["conn"],LDAP_OPT_PROTOCOL_VERSION,3);

        $this->ldap_conn = $ldap['conn'];

        if (bYN(GetParm('WEB_NETWORK_LOGIN', 'N')))
        {
            $User = GetParm('LDAP_USER');
            $Password = \src\admin\controllers\LDAPConfigController::getPassword();
        }

        // Some LDAP implementations require you to use the full dn
        // to bind.
        if ($ldap['bind_uses_dn'])
        {
            $BindUser = "$ldap[att_account]=$User,$ldap[base_dn]";
        }
        else
        {
            $BindUser = $User;
        }

        $ldap["bind"] = @ldap_bind($ldap["conn"], $BindUser, $Password);

        $this->ldap_errorno = ldap_errno($ldap['conn']);

        if ($this->ldap_errorno == LDAP_SUCCESS)
        {
            // we've managed to connect to a valid LDAP server and authenticate the user
            $this->server_index = $ldap_index;
        }

        //Make sure a p_a_s_s_w_o_r_d is specified as for some reason Active Directory
        //authenticates if no p_a_s_s_w_o_r_d is given.
        if ($Password == "" && !bYN(GetParm('WEB_NETWORK_LOGIN', 'N')))
            $this->ldap_errorno = LDAP_INVALID_CREDENTIALS;
        
        // For Active Directory, the domain will be included in the user name
        // passed to this function.  If it is, we need to remove it.
        // This code assumes that the current user is the same as the one
        // we used to bind with the server.
        $UserIDParts = explode('\\', $User);
        if (count($UserIDParts) > 1) 
        {
            $this->UID = $UserIDParts[1];
        }
        else 
        {
            $this->UID = $UserIDParts[0];
        }

        return $ldap;
    }

    // Function closes the LDAP connection
    function Close()
    {
        // ldap_unbind() and ldap_close() are the same thing
        ldap_unbind($this->ldap_conn);
    }
    
    /**
    * @desc Convert date from AD time stamp to unix time stamp.
    * 
    * @param date $ADDateTime AD Datetime integer to be processed. This value represents the number of 100 nanosecond intervals since January 1, 1601 (UTC).
    * 
    * @return date in Unix date format.
    */
    function ConvertADDateToUnixDate($ADDateTime)
    {
        //A value of 0 or 0x7FFFFFFFFFFFFFFF (9223372036854775807) indicates a null date.
        if ($ADDateTime != 0 && $ADDateTime != 9223372036854775807)
        {
            $dateLargeInt=$ADDateTime; // nano seconds since jan 1st 1601
            $secsAfterADEpoch = $dateLargeInt / (10000000); // seconds since jan 1st 1601
            $ADToUnixConvertor=((1970-1601) * 365.242190) * 86400; // unix epoch - AD epoch * number of tropical days * seconds in a day
            $unixTs=intval($secsAfterADEpoch-$ADToUnixConvertor); // unix Timestamp version of AD timestamp
            return date("Y-m-d", $unixTs); // formatted date
        }
        else
        {
            return null;
        }
    }
    
    //Escapes restricted characters with Hex number for character  
    /**
    * @desc This escaping function is required for dn's which contain certain characters which require escaping otherwise
    * the dn will not be found in any ldap searches.
    * 
    * @param string to be escaped
    * 
    * @return string escaped.
    */
    function ldap_escape($str)
    {
        $metaChars = array('\\', '(', ')', '#', '*', ' ');
        $quotedMetaChars = array();
        foreach ($metaChars as $key => $value) 
        {
            $quotedMetaChars[$key] = '\\'.dechex(ord($value));
        }
        $str=str_replace($metaChars,$quotedMetaChars,$str); //replace them
        return ($str);
    }
    
    /**
    * @desc This function populates a class array with a list of user dn's under the OU dn branch specified.
    * 
    * @param string user with permission to access the AD
    * @param string p_a_s_s_w_o_r_d for user to access AD
    * @param string dn for OU branch
    * 
    * @return bool users successfully retrieved.
    */
    function GetOUInfo($user, $password, $group_dn, $ldap_index = 0)
    {
        if (!$this->ldap["enable"]) 
        {
            // Use DATIX authentication
            $this->error = true;
            $this->DatixAuthentication = true;
            return false;
        }
        
        $ldap = $this->Connect($user, $password, $ldap_index);
        if ($ldap === false)
        {
            $this->error = true;
            $this->ldapErrorMsg = 'LDAP Error - ' . ldap_error($ldap['conn']);
            return false; 
        }

        $group_dn = $this->ldap_escape($group_dn);
        $SearchString = "(&(objectClass=user)(objectCategory=person)(cn=*))";

        // $ds is a valid link identifier (see ldap_connect)
        ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);

        // enable pagination with a page size of 1000.
        $pageSize = GetParm('LDAP_PAGESIZE', 1000);

        $cookie = '';
        do {
            ldap_control_paged_result($ldap['conn'], $pageSize, true, $cookie);

            $ldap['result'] = @ldap_list($ldap['conn'], $group_dn, $SearchString, $ldap['user_search_attributes']);

            $this->ldap_errorno = ldap_errno($ldap['conn']);

            if ($this->ldap_errorno != LDAP_SUCCESS)
            {
                $this->error = true;
                $this->ldapErrorMsg = 'LDAP Error - ' . ldap_error($ldap['conn']);
                return false;
            }

            // Retrieve user distinguished name
            // scan tree structure from base dn
            $ldap['groupinfo'] = @ldap_get_entries($ldap['conn'], $ldap['result']);

            $entry = ldap_first_entry($ldap['conn'], $ldap['result']);

            while ($entry)
            {
                $row = array();
                $attr = ldap_first_attribute($ldap['conn'], $entry);
                while ($attr)
                {
                    $val = ldap_get_values_len($ldap['conn'], $entry, $attr);
                    if (array_key_exists('count', $val) AND $val['count'] == 1)
                    {
                        $row[\UnicodeString::strtolower($attr)] = $val[0];
                    }
                    else
                    {
                        $row[\UnicodeString::strtolower($attr)] = $val;
                    }
                    $attr = ldap_next_attribute($ldap['conn'], $entry);
                }
                $this->group['member'][] = $row["distinguishedname"];
                $entry = ldap_next_entry($ldap['conn'], $entry);
            }

            ldap_control_paged_result_response($ldap['conn'], $ldap['result'], $cookie);

        } while($cookie !== null && $cookie != '');

        $this->ldap_errorno = ldap_errno($ldap['conn']);

        if ($this->ldap_errorno != LDAP_SUCCESS)
        {
            $this->error = true;
            $this->ldapErrorMsg = 'LDAP Error - ' . ldap_error($ldap['conn']);
            return false;
        }

        // Return list of users listed directly under OU
        $this->group['samaccountname'] = $group_dn;
        return true;
    }
    
    /**
    * @desc This function returns the objectclass attribute for the specified dn
    * 
    * @param object existing valid LDAP connection
    * @param string Distinguished name of the AD oobject
    * 
    * @return string objectclass expressed as a string.
    */
    function GetObjectClass($ldap, $ldap_dn)
    {       
        //Check Object Class
        $ldap_dn = $this->ldap_escape($ldap_dn);
        $SearchString = "$ldap[att_dn]=$ldap_dn";         
    
        $ldap['result'] = @ldap_search($ldap['conn'], $ldap['base_dn'], $SearchString, array("objectclass"));

        $this->ldap_errorno = ldap_errno($ldap['conn']);

        if ($this->ldap_errorno != LDAP_SUCCESS) 
        {
            $this->error = true;
            $this->ldapErrorMsg = 'LDAP Error - ' . ldap_error($ldap['conn']);
            return false;
        }
    
        $ObjectInfo = @ldap_get_entries($ldap['conn'], $ldap['result']);
        if (is_array($ObjectInfo[0]['objectclass']))
        {         
            $ObjectClass = implode(";", $ObjectInfo[0]['objectclass']);
        }
        return $ObjectClass;
    }
}
?>
