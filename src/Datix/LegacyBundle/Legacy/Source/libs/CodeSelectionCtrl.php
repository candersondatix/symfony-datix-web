<?php

function GetCodeListSQL($module, $field, $ParentValues, $search, $udf, $FreeText, $currentValues, &$title, &$bindArray, $term = '')
{
    global $FieldDefs, $FieldDefsExtra, $ModuleDefs;
    $extraFieldModelFactory = new \src\system\database\extrafield\ExtraFieldModelFactory();
    $extraFieldMapper = $extraFieldModelFactory->getMapper();

    //TODO There are enough exceptions here to consider some sort of exception list when bringing into src
    //Need to convert "ACR" into "ATM", "AMO"... etc. in order to correctly retrieve codes.
    if($module == 'ACR')
    {
        require_once 'Source/setups/ImportCodes.php';
        $module = getAccreditationModule($FieldDefsExtra, $field);
    }

    // we need to sanitize parameters before attempting to build the SQL string
    if (!empty($module) && !in_array($module, GetModuleIdCodeList()))
    {
        return false;
    }

    if ($module == 'INC' && ($field == 'inc_injury' || $field == 'inc_bodypart'))
    {
        $table = 'incidents_main';
    }

    $showCodes = bYN(GetParm('WEB_SHOW_CODE', 'N')) && $_POST['showCodes'] == 'true';

    if($module == 'ADM')
    {
       $module = 'CON';
    }

    if($module == 'RAM'
        && in_array($field, array('ram_level', 'ram_cur_level', 'ram_after_level'))
        && bYN(GetParm('RISK_MATRIX', 'N')))
    {
        $module = 'INC';
        $field = 'inc_grade';
    }

    if($table == 'staff')
    {
        $table = 'contacts_main';
    }

    if($module && !$table && $field == 'rep_approved')
    {
        $table = ($ModuleDefs[$module]['VIEW']) ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE'];
    }

    if (!$udf)
    {
        try
        {
            $FDR_info = new Fields_Field($field, $table);
        }
        catch (Exception $e)
        {
            returnError($e);
        }

    }

    if ($udf) // Popup control should only be used for single coded and multicoded UDFs,
			  // or maybe Yes/No for setting up actions in form design
	{
		$udf_field_parts = explode("_",$field); // Format: UDF_<Type>_<GroupID>_<FieldID>
		$field_type = $udf_field_parts[1];
		$field_id = $udf_field_parts[3];

        if ($term != '')
        {
            if ($showCodes)
            {
                $where = '(udc_code LIKE :term1 OR udc_description LIKE :term2)';
                $bindArray['term1'] = '%'.$term.'%';
                $bindArray['term2'] = '%'.$term.'%';
            }
            else
            {
                $where = '(udc_description LIKE :term)';
                $bindArray['term'] = '%'.$term.'%';
            }
        }

		if ($field_type == 'Y')
        {
            if ($term != '')
            {
                if ($showCodes)
                {
                    $where = '(t.value LIKE :term1 OR t.description LIKE :term2)';
                }
                else
                {
                    $where = '(t.description LIKE :term)';
                }
            }
			$sql = "SELECT * FROM (SELECT 'Y' AS [value], 'Yes' AS [description] UNION SELECT 'N' AS [value], 'No' AS [description]) AS t" . (isset($where) ? ' WHERE '.$where : '');
        }
		else
        {
            //some "like" fields are stored with a view prefix, so we need to remove it before getting field info.
            $fld_code_like = RemoveTableFromFieldName(GetFieldCodeLike($field_id));

            if ($fld_code_like != '')
            {
                //we don't know what module the fld_code_like field belongs to:
                $module = getModuleFromField($fld_code_like);

                $sql = GetCodeListSQL($module, $fld_code_like, $ParentValues, $search, false, $FreeText, $currentValues, $title, $bindArray, $term);
            }
            else
            {
                if (isset($where))
                {
                    $where .= ' AND (active IS NULL OR active = \'Y\' ';
                }
                else
                {
                    $where = '(active IS NULL OR active = \'Y\' ';
                }

                if ($search)
                {
                    $where .= 'OR active = \'N\'';
                }

                if (isset($where))
                {
                    $where .= ')';
                }

                $sql = 'SELECT udc_code as [value], udc_description as [description]
                        FROM udf_codes
                        WHERE field_id = :field_id'
                        .(isset($where) ? ' AND ('.$where : '').'
                       ) ORDER BY listorder, udc_description, udc_code';
                $bindArray['field_id'] = $field_id;
            }
        }

		return $sql;
	}

	$Where = array();

	if (!empty($FDR_info) && is_object($FDR_info))
	{
        if ($FieldDefs[$module][$field]["Title"])
        {
            $title = $FieldDefs[$module][$field]["Title"];
        }
        else
        {
            $title = $FDR_info->getLabel();
        }

		$code_table = $FDR_info->getCodeTable();
		if ($code_table{0} == '!')
		{
			$code_type = \UnicodeString::substr($code_table, 1);
			$code_field = ($FDR_info->getFieldType() == 'C' && $FieldDefs[$module][$field]['Type'] != 'string_search' ? "cod_code" : "cod_descr");
			$code_descr = "cod_descr";
			$code_table = "code_types";
			$Where[] = "cod_type = '$code_type'";
			$OrderBy = "cod_listorder, REPLACE(REPLACE(cod_descr, '\"', ''),'''','')";
            $colour = $code_table.'.cod_web_colour';
		}
		else
		{
			$code_field = ($FDR_info->getFieldType() == 'C' ? $FDR_info->getCodeTableField() : $FDR_info->getCodeDescription());

			if ($code_table == "staff" || $code_table == "vw_staff_combos")
			{
                if (bYN(GetParm('STAFF_EMPL_FILTERS', 'N', true)))
                {
                    $Parents = GetParents(array('module' => $module, 'field' => $field));

                    foreach($Parents as $Parent)
                    {
                        if($Parent)
                        {
                            $StaffFields[] = $ModuleDefs[$module][STAFF_EMPL_FILTER_MAPPINGS][$Parent];
                        }
                    }
                }

                // need to filter staff according to their permissions on level one forms
                if (!$_SESSION["logged_in"] || $_GET['action'] == 'newdif1' || $_GET['level'] == '1')
                {
                    // there are no conventions with regards to access levels, so we need to hard-code a list of
                    // "level 2 access and level 1 review" statuses for each module
                    $levels = array(
                        'INC' => array('DIF2', 'RM'),
                        'RAM' => array('RISK2', 'RM'),
                        'PAL' => array('PAL2', 'RM'),
                        'COM' => array('COM2'),
                        'CLA' => array('CLA2'),
                    );

                    $module == 'INC' ? array('DIF2', 'RM') : array($module.'2', 'RM');
                    $Where[] = '(' . getContactListSQLByAccessLevel(
                        array(
                            'module' => $module,
                            'levels' => $levels[$module],
                            'table' => $code_table,
                            'return_where' => true,
                        )
                    ) . ')';
                }
                
                if (bYN(GetParm('CONTACTS_CLAUSE_AFFECTS_STAFF_FIELDS', 'Y')))
                {
                    $conPermWhere = MakeSecurityWhereClause("", "CON", $_SESSION["initials"]);
    
                    if ($conPermWhere)
                    {
                        $Where[] = $code_table . ".recordid in (SELECT recordid FROM contacts_main WHERE $conPermWhere)";
                    }    
                }

                $stafftables = true;
                $colour = $code_table.'.cod_web_colour';

                $code_table = "$code_table left join code_parents on cop_field = '$field' AND cop_code = initials";

                if (bYN(GetParm('CONTACT_SURNAME_SORT', 'Y')))
                {
                    $code_descr = "coalesce(sta_surname, '') + ', ' + coalesce(sta_title, '')  + ' ' + coalesce(sta_forenames, '')";
                }
                else
                {
                    $code_descr = "coalesce(sta_title, '') + ' ' + coalesce(sta_forenames, '')  + ' ' + coalesce(sta_surname, '')";
                }

                if (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'A')
                {
                    $code_descr .= " + ' - ' + coalesce(jobtitle, '')";
                }
                else if (GetParm('STAFF_NAME_DROPDOWN', 'N') == 'B')
                {
                    $code_descr = "coalesce(jobtitle, '') + ' - ' + " . $code_descr;
                }

                $OrderBy = $code_descr;
			}
			else
			{
                $colour = $code_table.'.cod_web_colour';
				$code_descr = $FDR_info->getCodeDescription();
                if(!$FDR_info->isCustomCodedField())
                {
                    $OrderBy = "cod_listorder";
                }
                if ($FDR_info->getCodeOrder() != "" && $FDR_info->getCodeOrder() != " "  && $FDR_info->getCodeOrder() != "cod_listorder")
                {
                    $OrderBy .= ($OrderBy ? "," : '') . $FDR_info->getCodeOrder();
                }
                else
                {
                    $OrderBy .= ($OrderBy ? "," : '') . $code_descr;
                }
			}
		}
	}

	// Take into account "cod_priv_level" column
    // If we are doing a check for valid child codes, we include inactive codes as "valid" (DW-8804)
    if(!$FDR_info->isCustomCodedField() && !$childcheck)
    {
        $fieldName = explode('_', $_POST['fieldname']);

        // Make sure that codes with 'N' appear on the old value column when we are batch updating
        if ($fieldName[3] == 'old')
        {
            $Where[] =
                "(cod_priv_level != 'X' OR cod_priv_level IS NULL)";
        }
        else
        {
            $Where[] =
                "(cod_priv_level != 'X'" .
                ($search === false ? " AND cod_priv_level != 'N'" : "") .
                " OR cod_priv_level IS NULL)";
        }
    }

    if($stafftables)
    {
        $Where[] = GetActiveStaffWhereClause();
    }

	if (is_array($ParentValues) && !empty($ParentValues) && (!$search || $search && bYN(GetParm('COMBO_LINK_IN_SEARCH', 'Y'))))
	{
		if (isset($ParentValues[0]))
		{
            GetParentCodeWhere(($StaffFields[0] ? $StaffFields[0] : 'cod_parent'), $ParentValues[0], $Where, $bindArray);
		}

		if (isset($ParentValues[1]))
		{
            GetParentCodeWhere(($StaffFields[1] ? $StaffFields[1] : 'cod_parent2'), $ParentValues[1], $Where, $bindArray);
		}
	}

	if ($FreeText && isset($currentValues[0])) // free text field using the popup window can only have one value
	{
		$currentValues[0] = preg_replace('/\*/u', '%', $currentValues[0]);
        $Where[] = $code_field . ' LIKE :value';
        $bindArray['value'] = $currentValues[0] . '%';
	}

    if($FDR_info->getCodeWhere())
    {
        $Where[] = $FDR_info->getCodeWhere();
    }

    if ($term != '')
    {
        if ($showCodes)
        {
            $Where[] = '(' . $code_field . ' LIKE :term1 OR ' . $code_descr . ' LIKE :term2)';
            $bindArray['term1'] = '%'.$term.'%';
            $bindArray['term2'] = '%'.$term.'%';
        }
        else
        {
            $Where[] = '(' . $code_descr . ' LIKE :term)';
            $bindArray['term'] = '%'.$term.'%';
        }
    }

    if (!empty($currentValues))
    {
        foreach ($currentValues as $key => $value)
        {
            $currentValuesWhere[] = $code_field . '!= :value' . $key;
            $bindArray['value'.$key] = $value;
        }
        $Where[] = '(' . implode(' AND ', $currentValuesWhere) . ')';
    }

    if ($extraFieldMapper->isStaffCode($field) && substr($_POST['fieldname'], 0, 3) == 'UDF')
    {
        $udfFieldName = explode('_', $_POST['fieldname']);
        
        $Fields = \src\framework\registry\Registry::getInstance()->getFieldDefs();
        $CodeExtraField = $Fields['UDF_'.$udfFieldName[3]];
        $savedQueryModelFactory = new \src\savedqueries\model\SavedQueryModelFactory();
        $savedQueryMapper = $savedQueryModelFactory->getMapper();
        $savedQuery = $savedQueryMapper->find($CodeExtraField->queryid);
        if($savedQuery != null)
        {
            $query = $savedQuery->createQuery();
            $query->select(array('recordid'));
            $query->from('profiles');
            $SqlWriter = new \src\framework\query\SqlWriter();
            list($sql, $params) = $SqlWriter->writeStatement($query);
            $profiles = \DatixDBQuery::PDO_fetch_all($sql, $params, \PDO::FETCH_COLUMN);
        }
    }

	$sql = "SELECT $code_field as [value], $code_descr as [description]". (!$FDR_info->isCustomCodedField() ? ", $colour as [colour], cod_parent as [parent1], cod_parent2 as [parent2]" : '')." FROM $code_table";

	if (!empty($Where))
	{
        foreach($profiles as $key => $profile)
        {
            $profiles[$key] = 'sta_profile = \''. $profile . '\'';
        }
        if (count($profiles) > 1)
        {
            $sql .= ' WHERE ('.implode($profiles, ' OR ').') AND '.implode(' AND ', $Where);
        }
        elseif(count($profiles) == 1)
        {
            $sql .= ' WHERE ('.$profiles['0'].') AND '.implode(' AND ', $Where);
        }
        else
        {
            $sql .= ' WHERE '.implode(' AND ', $Where);
        }
	}

	if ($OrderBy != '')
	{
		$sql .= " ORDER BY " . $OrderBy;
	}

	return $sql;
}

function CheckChild()
{
	$module = $_POST['module'];
	$parent_field = $_POST['parent_field'];
	$parent_value = $_POST['parent_value'];
	$child_field = $_POST['child_field'];
	$Child_value = $_POST['child_value'];
}

function GetParentCodeWhere($ParentField, $ParentCodes, &$Where, &$bindArray)
{
	if (!isset($ParentCodes) || $ParentCodes == '')
		return '';

	$codes = explode('|', $ParentCodes);

	if (!empty($codes))
	{
		$parent_where = array();
        $counter = 0;

		foreach ($codes as $code)
		{
			if($code != '')
            {
                $codeArray[] = $code;
                $codeArray[] = $code . ' %';
                $codeArray[] = '% ' . $code . ' %';
                $codeArray[] = '% ' . $code;

                $parent_where[] =
				    $ParentField . ' like :' . $ParentField . 'code' . $counter .
				    ' OR ' . $ParentField . ' like :' . $ParentField . 'code' . ($counter+1) .
				    ' OR ' . $ParentField . ' like :' . $ParentField . 'code' . ($counter+2) .
				    ' OR ' . $ParentField . ' like :' . $ParentField . 'code' . ($counter+3);

                $bindArray[$ParentField . 'code' . $counter] = $codeArray[$counter];
                $bindArray[$ParentField . 'code' . ($counter+1)] = $codeArray[$counter+1];
                $bindArray[$ParentField . 'code' . ($counter+2)] = $codeArray[$counter+2];
                $bindArray[$ParentField . 'code' . ($counter+3)] = $codeArray[$counter+3];

                $counter += 4;
            }
		}

		if (!empty($parent_where))
		{
			$Where[] = '(' . $ParentField . ' is null OR ' . $ParentField . ' = \'\' OR ' . implode(' OR ', $parent_where) . ')';
		}
	}
}

function returnError(Exception $e)
{
    $error = $GLOBALS['dtxdebug'] ? $e->getMessage() : 'Error retrieving codes.';
    echo json_encode(array('!ERROR!' => $error));
    obExit();
}