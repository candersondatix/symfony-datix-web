<?php

/**
 * Deals with whitelisting of email domains
 */
class EmailDomainWhitelist
{
    private $allowedDomains = array ();
    
    public function __construct() 
    {
        $this->initList();
    }
    
    private function initList ()
    {
        $value = \UnicodeString::trim(GetParm('PERMITTED_EMAIL_DOMAINS', '', true));
        $this->allowedDomains = empty($value) ? array() : preg_split('/\s+/u', $value);
    }
        
    /**
     * Save whitelist domains
     * @param array $list list of domains
     * @return array (
     *  bool 'success', 
     *  array 'invalid'
     * )
     */
    public function setAllowedDomains (array $list)
    {
        $list = array_unique($list);
        $invalid = array ();

        // check for invalid domains
        foreach ($list as $domain) 
        {
            if (!empty($domain) && !$this->isValidDomain($domain))
            {
                $invalid[] = $domain;
            }
        }

        // if invalid domains were found, return list of 
        // offenders and don't save
        if (!empty($invalid)) 
        {
            $response = array ('success' => false, 'invalid' => $invalid);
        }
        else
        {
            SetGlobal ('PERMITTED_EMAIL_DOMAINS', join ("\n", $list));
            $response = array ('success' => true);
        }

        return $response;
    }

    /**
     * Checks if domain is valid (doesn't check if it's permitted)
     * @param string $domain
     * @throws Exception
     * @return bool
     */
    public function isValidDomain ($domain)
    {
        if (empty($domain)) {
            throw new Exception ('Can\'t validate empty domain');
        }
        
        $name = \UnicodeString::strstr($domain, '.', true );
        
        // the FILTER_VALIDATE_EMAIL filter is the safest way to check
        // for domains with unicode characters
        return filter_var ('test@'.$domain, FILTER_VALIDATE_EMAIL) && ( \UnicodeString::strlen( $name ) >= 1 && \UnicodeString::strlen( $name ) <= 63 );
    }
    
    public function getAllowedDomains ()
    {
        return $this->allowedDomains;
    }

    /**
     * Checks if domain is allowed
     * @param string $domain (ie: datix.co.uk)
     * @throws Exception
     * @return boolean
     */
    public function isDomainAllowed ($domain)
    {
        if (empty($domain)) {
            throw new Exception ('Can\'t validate empty domain');
        }

        if (!$this->isWhitelistActive()) {
            return true;
        }

        $allowed = in_array ($domain, $this->allowedDomains);
        return $allowed;
    }

    /**
     * Checks if email's domain is allowed
     * @param string $email (e.g: emedina@datix.co.uk)
     * @param boolean $log Log if it fails
     * @throws Exception
     * @return boolean
     */
    public function isEmailAllowed ($email)
    {
        if (empty($email)) {
            throw new Exception ('Can\'t validate empty email');
        }

        if (!$this->isWhitelistActive()) {
            return true;
        }
        
        $emailParts = preg_split('/\@/u', $email); 
        if (count($emailParts) != 2) {
            throw new Exception ('Invalid email');    
        }
        
        $domain = \UnicodeString::trim($emailParts[1]);
        
        $allowed = $this->isDomainAllowed($domain);

        return $allowed;
    }

    /**
     * Splits and sanitizes a RFC 2822 email recipient formatted string into an array of emails
     * @param string $recipient (ie: "Edson Medina <emedina@datix.co.uk>, "Developers" <datixdevelopers@datix.co.uk>, whatever@gmail.com")
     * @return array ('emedina@datix.co.uk', 'datixdevelopers@datix.co.uk', 'whatever@gmail.com')
     */
    public function splitRFC2822recipient ($recipient)
    {
        $list = preg_split ('/\,/u', $recipient);

        // filter names out
        foreach ($list as &$email)
        {
            if (preg_match ('/\<.*?\@.*?\>/', $email)) {
                $email = preg_replace ('/.*\<(.*?)\>.*/u', '$1', $email);
            }
        }

        return $list;
    }

    /**
     * Checks if white list is active (has any domains)
     * If the list of permitted domains is empty, every domain is accepted
     * @return bool
     */
    public function isWhitelistActive ()
    {
        return !empty($this->allowedDomains);
    }
}

?>
