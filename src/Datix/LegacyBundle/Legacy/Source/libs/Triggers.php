<?php
function ExecuteTriggers($Cas_id, $record, $mod_id)
{
    require_once 'Source/libs/TriggerClass.php';
    $clsTrigger = new TriggerClauses();
    $clsTrigger->InitialiseTriggerClauses($mod_id);
    $TriggerMsg = $clsTrigger->CheckTriggers($Cas_id, "", TRUE, $record);

    if ($TriggerMsg != "")
    {
        AddSessionMessage('INFO', $TriggerMsg);
    }
}