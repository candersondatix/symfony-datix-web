<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */


function LDAPBrowse()
{

    if (isset($_GET['form_element']))
    {
        $_SESSION['ldap']['return_form_element'] = Sanitize::SanitizeString($_GET['form_element']);
    }
    else
    {
        $_SESSION['ldap']['return_form_element'] = null;
    }

    require_once 'Source/libs/LDAP.php';

    if (!$_SESSION['ldap']['user'] || !$_SESSION['ldap']['passwrd'])
    {
		// Prompt user for active directory credentials
		LDAPLoginPrompt();
		obExit();
	}

	LDAPListEntries();
	obExit();
}

function LDAPListEntries()
{
	global $scripturl, $NoMainMenu, $dtxtitle, $dtxdebug, $dbtype, $MinifierDisabled;

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    if (isset($_GET['container']))
    {
        $container = rawurldecode(Sanitize::SanitizeRaw($_GET['container']));
    }
    else
    {
        $container = false;
    }

	$return_form_element = $_SESSION['ldap']['return_form_element'];

    if (isset($_GET['rdn']))
    {
        $rdn =  Sanitize::SanitizeString($_GET['rdn']);
    }
    else
    {
       $rdn = null;
    }

    echo '
<html><head>
<link rel="stylesheet" type="text/css" href="css/global' . $addMinExtension . '.css" />
<link rel="SHORTCUT ICON" href="Images/datix.ico" />
<title>Datix - Browse Directory</title>
</head><body text="#000000" link="#0033ff" bgcolor="#ffffff">

<table class="bordercolor" cellspacing="0" cellpadding="0" width="100%" align="center" border="0" style="border:0">
  <tr>
    <td style="padding:0;margin:0">
	  <table class="bordercolor" cellspacing="1" cellpadding="0" width="100%" align="center" border="0" style="border:0">
        <tr>
          <td style="padding:0;margin:0">
            <table cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#ffffff" border="0" bordercolor="#6394bd">
              <tr>
                <td valign="top" width="100%" style="padding:0;margin:0">
<table border="0" width="100%" cellspacing="1" cellpadding="4" class="formbg" align="center" style="border:0">
    <tr>
		<td class="new_titlebg" width="100%" valign="middle" style="padding:0;margin:0;border:0">
			<table style="border:0">
                <tr>
                    <td style="border:0"><font size="3"><b>Browse Directory</b></font></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
	    <td class="formbg" width="100%" style="padding:0;margin:0;border:0">
	        <table class="formbg" width="100%" cellspacing="1" cellpadding="1" align="left" border="0" style="border:0">
	            <tr>
					<td class="menubg" width="85%" valign="top" style="padding:0;margin:0;border:0">
                        <table class="titlebg" align="left" border="0" cellpadding="0" cellspacing="1" width="100%" style="border:0">
                            <tr>
                                <td style="padding:0;margin:0;border:0">
                                    <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%" style="border:0">
';

    // Retrieve LDAP information
    require_once 'Source/libs/LDAP.php';
	$ldap = new datixLDAP();
	$LDAPEnabled = $ldap->LDAPEnabled();

	// Do binding user LDAP credentials
    if ($_SESSION['ldap']['user'] && $_SESSION['ldap']['passwrd'])
    {
		$usr = $_SESSION['ldap']['user'];
		$pwd = $_SESSION['ldap']['passwrd'];
		$ldap_index = $_SESSION['ldap']['ldap_index'];
	}

	$connect = $ldap->Connect($usr, $pwd, $ldap_index);

    if ($ldap->error)
    {
        $ldap->Close();
		LDAPLogin($usr, $ldap->errorMsg());
		obExit();
	}

    if ($container)
    {
        echo '
    <tr>
        <td class="titlebg" colspan="2">
            <span style="font:12px Arial, Helvetica, sans-serif; color:#083c5c; margin-left:5px;">Looking in: <b>' .
            htmlspecialchars($container)
            . '</b></span>
		</td>
	</tr>
';
	}

	/* Has the use already begun to descend into a specific server tree? */
    if ($container !== false)
    {
		$filter = $connect['filter'];
		$mappableObjects = $connect['mappableobjects'];

		$dn_list = $ldap->GetContainerContents($connect['conn'], $container, 0, $filter, LDAP_DEREF_ALWAYS, $mappableObjects);

        if (!pla_compare_dns($container,$connect["base_dn"]))
        {
			$parent_container = false;
			$up_href = sprintf($scripturl . '?action=ldaplistentries&form_element=%s&rdn=%s&token='.CSRFGuard::getCurrentToken(),$return_form_element,$rdn);
        }
        else
        {
			$parent_container = get_container($container);
			$up_href = sprintf($scripturl . '?action=ldaplistentries&form_element=%s&rdn=%s&amp;container=%s&token='.CSRFGuard::getCurrentToken(),
				$return_form_element, $rdn, rawurlencode($parent_container));
    	}

        echo '
		<tr>
			<td class="rowtitlebg">
				&nbsp;<a href="' . $up_href . '" style="text-decoration:none;border:0">
				<img src="images/up.png" border="0" alt="Back up">&nbsp;&nbsp;Back Up...</a>
			</td>
		</tr>
';

        if (!count($dn_list))
        {
            echo '
			<tr>
				<td class="rowtitlebg">
                <span style="font:12px Arial, Helvetica, sans-serif; color:#083c5c; margin-left:5px;">
				&nbsp;&nbsp;&nbsp;(No Entries)<br />
                </span>
				</td>
			</tr>
';
		}
    	else
        {
            foreach ($dn_list as $entry)
            {
				//$href = sprintf("javascript:returnDN('%s%s')",($rdn ? "$rdn," : ''),$dn);
                $Raw_entry_dn = rawurlencode($entry['dn']);
                $entry['dn'] = str_replace("\\", "\\\\", $entry['dn']);
                $entry['dn'] = str_replace("'", "\\'", $entry['dn']);
				$href = sprintf("javascript:opener.document.getElementById('%s').value = '%s';window.close()",$return_form_element,$entry['dn']);

				echo '
		<tr>
			<td class="rowtitlebg">';
                print '
				&nbsp;&nbsp;&nbsp;';

                printf('<a href="' . $scripturl .
                '?action=ldaplistentries&amp;form_element=%s&rdn=%s&amp;container=%s&token='.CSRFGuard::getCurrentToken().'"><img src="images/plus.png" border="0" alt="Add" /></a>',
					$return_form_element, $rdn, $Raw_entry_dn);
                if ($entry['mappable'] === true)
                {
				    echo '
					&nbsp;&nbsp;<a href="' . $href . '">'
					. htmlspecialchars($entry['title'])
					. ($dtxdebug ? '&nbsp;(' . $entry['objectclass'] . ')' : '')
					. '</a>';
				}
                else
                {
                    echo '&nbsp;&nbsp;<span style="font:12px Arial, Helvetica, sans-serif; color:#083c5c; margin-left:5px;">'
                        . htmlspecialchars($entry['title'])
					. ($dtxdebug ? '&nbsp;(' . $entry['objectclass'] . ')' : '')
                    . '</span>';
				}

				echo '
			</td>
		</tr>';
			}
        }

    /* draw the root of the selection tree (ie, list all the servers) */
	}
    else
    {
		$href = "javascript:opener.document.getElementById('$return_form_element').value = '$connect[base_dn]';window.close()";
		/*?>
		<tr>
			<td class="titlebg">
		<a href="<?= "$scripturl?action=ldaplistentries&amp;form_element=$return_form_element"
		. "&amp;rdn=$connect[base_dn]&amp;container=$connect[base_dn]"?>">
		<img src="images/plus.png" /></a>
		&nbsp;&nbsp;<a href="' . $href . '"><font color=#ffffff>
		<?= $connect['base_dn'] ?></font></a><?= htmlspecialchars($dn) ?>
			</td>
		</tr>
		<?php*/

        echo '
		<tr>
			<td class="titlebg" style="border:0">
            <a href="'
            . "$scripturl?action=ldaplistentries&amp;form_element=$return_form_element&amp;rdn=$connect[base_dn]&amp;container=$connect[base_dn]"
            . '&token='.CSRFGuard::getCurrentToken().'"><img src="images/plus.png" border="0" alt="Add" /></a>
            &nbsp;&nbsp;<b>'
            . $connect['base_dn'] . '</b>'
            . htmlspecialchars($dn)
            . '
			</td>
            </tr>
';

	}
    echo '
									</table>
                                </td>
                            </tr>
                        </table>
                    </td>
				</tr>
            </table>
		</td>
    </tr>
</table>
';
    $ldap->Close();
	obExit();
}


function pla_compare_dns( $dn1, $dn2 ) {

	// If they are obviously the same, return immediately
	if( 0 === \UnicodeString::strcasecmp( $dn1, $dn2 ) )
		return 0;

	$dn1_parts = pla_explode_dn( pla_reverse_dn($dn1) );
	$dn2_parts = pla_explode_dn( pla_reverse_dn($dn2) );
	assert( is_array( $dn1_parts ) );
	assert( is_array( $dn2_parts ) );

	// Foreach of the "parts" of the smaller DN
	for( $i=0; $i<count( $dn1_parts ) && $i<count( $dn2_parts ); $i++ )
	{
		// dnX_part is of the form: "cn=joe" or "cn = joe" or "dc=example"
		// ie, one part of a multi-part DN.
		$dn1_part = $dn1_parts[$i];
		$dn2_part = $dn2_parts[$i];

		// Each "part" consists of two sub-parts:
		//   1. the attribute (ie, "cn" or "o")
		//   2. the value (ie, "joe" or "example")
		$dn1_sub_parts = explode( '=', $dn1_part, 2 );
		$dn2_sub_parts = explode( '=', $dn2_part, 2 );

		$dn1_sub_part_attr = \UnicodeString::trim( $dn1_sub_parts[0] );
		$dn2_sub_part_attr = \UnicodeString::trim( $dn2_sub_parts[0] );
		if( 0 != ( $cmp = \UnicodeString::strcasecmp( $dn1_sub_part_attr, $dn2_sub_part_attr ) ) )
			return $cmp;

		$dn1_sub_part_val = \UnicodeString::trim( $dn1_sub_parts[1] );
		$dn2_sub_part_val = \UnicodeString::trim( $dn2_sub_parts[1] );
		if( 0 != ( $cmp = \UnicodeString::strcasecmp( $dn1_sub_part_val, $dn2_sub_part_val ) ) )
			return $cmp;
	}

    // If we iterated through all entries in the smaller of the two DNs
    // (ie, the one with fewer parts), and the entries are different sized,
    // then, the smaller of the two must be "less than" than the larger.
    if( count($dn1_parts) > count($dn2_parts) ) {
        return 1;
    } elseif( count( $dn2_parts ) > count( $dn1_parts ) ) {
        return -1;
    } else {
        return 0;
    }
}
function pla_explode_dn( $dn, $with_attributes=0 ) {

    // replace "\," with the hexadecimal value for safe split
    $var = preg_replace("/\\\,/u","\\\\\\\\2C",$dn);

    // split the dn
    $result = explode(",",$var);

    //translate hex code into ascii for display
    foreach( $result as $key => $value )
        $result[$key] = preg_replace("/\\\([0-9A-Fa-f]{2})/eu", "''.chr(hexdec('\\1')).''", $value);

    return $result;
}

function pla_reverse_dn($dn) {

    foreach (pla_explode_dn($dn) as $key => $branch) {

		// pla_expode_dn returns the array with an extra count attribute, we can ignore that.
		if ( $key === "count" ) continue;

		if (isset($rev)) {
			$rev = $branch.",".$rev;
		} else {
			$rev = $branch;
		}
	}
	return $rev;
}

function get_container( $dn ) {

	$parts = pla_explode_dn( $dn );
	if( count( $parts ) <= 1 )
		return null;
	$container = $parts[1];
	for( $i=2; $i<count($parts); $i++ )
		$container .= ',' . $parts[$i];
	return $container;
}

function  LDAPLoginPrompt($user = '', $errormsg = '', $action = 'ldaplogin')
{
    global $MinifierDisabled;

    $addMinExtension = ($MinifierDisabled ? '' : '.min');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<script language="javascript" type="text/javascript" src="js_functions/common<?php echo $addMinExtension; ?>.js"></script>
	<link rel="stylesheet" type="text/css" href="css/global<?php echo $addMinExtension; ?>.css" />
	<link rel="SHORTCUT ICON" href="Images/datix.ico" />
	<title>Directory Login</title>
</head>
<body style="min-width:0;">
<table cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" border="0">
<?php
	if ($errormsg) {
?>
	<tr>
		<td class="windowbg">
			<br /><font size="2"><?= $errormsg ?></font><br /><br />
		</td>
	</tr>
<?php
	}
?>
<tr>
<td width="100%" style="padding:0;margin:0">
<form name="frmLogin" action="<?= htmlspecialchars("$scripturl?action=$action").'&token='.CSRFGuard::getCurrentToken(); ?>" method="post">
<table border="0" width="400" cellspacing="1" cellpadding="0" class="bordercolor" align="center">
<tr>
<td class="windowbg" width="100%" style="padding:0;margin:0">
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td class="new_titlebg" colspan="2">
		<img src="Images/login_sm.gif" alt="" />
		<font size="2" class="text1"><b>Directory Login</b></font>
		</td>
	</tr>
	<tr>
		<td align="center" class="windowbg2" colspan="2" style="padding:0;margin:0;">
			<table style="border:0">
				<tr>
					<td class="windowbg2" align="left" style="border:0">
					<font size="2"><b>User name:&nbsp;</b></font>
					</td>
					<td class="windowbg2" align="left" style="border:0">
					<font size="2"><input type="text" name="user" size="20" value="<?= htmlspecialchars($user) ?>" /></font>
					</td>
				</tr>
				<tr>
					<td class="windowbg2" align="left" style="border:0">
					<font size="2"><b>Password:&nbsp;</b></font>
					</td>
					<td class="windowbg2" align="left" style="border:0">
					<font size="2"><input type="password" name="passwrd" size="20"  autocomplete="off" /></font>
					</td>
				</tr>
<?php
    require_once 'Source/libs/LDAP.php';
    $ldap = new datixLDAP();

    //$LDAPAuthentication = $ldap->LDAPEnabled();
    $ldap_domains = $ldap->ldapDomains();
echo '
    <tr>
		<td class="windowbg2" align="left" style="border:0">
		<font size="2"><b>Domain:&nbsp;</b></font>
		</td>
		<td class="windowbg2" align="left" style="border:0">
		<font size="2">
        <select name="domain" id="domain">';
        $NumDomains = count($ldap_domains);
        $i = 0;
        //echo '<option value="" style="font-style:italic">&lt;None&gt;</option>';
        echo '<option value="'. $ldap_domains[$i] .'" selected>' . $ldap_domains[$i]. '</option>';
        $i++;
        while ($i < $NumDomains)
        {
            echo '<option value="'. $ldap_domains[$i] .'">'. $ldap_domains[$i] .'</option>';
            $i++;
        }

        echo '</select>
		</td>
	</tr>';
?>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2" class="windowbg2">
		<input type="submit" value="Log in" />
		</td>
	</tr>
</table>
</td>
</tr>
</table>
</form>
<script language="javascript" type="text/javascript"><!--
document.frmLogin.user.focus();
//--></script>
</td>
</tr>
</table>
</body>
</html>
<?php

	obExit();

}

function LDAPLogin($user = '', $errormsg = '') {

    require_once 'Source/libs/LDAP.php';

	$ldap = new datixLDAP();

	if (is_array($_POST))
	{
        if ($_POST['domain'])
        {
            $ldap_index = array_flip($ldap->ldap['domains'])[$_POST['domain']];
        }
		if ($_POST['domain'])
        {
			$user = Sanitize::SanitizeString($_POST['domain']) . '\\' . Sanitize::SanitizeString($_POST['user']);
        }
        else
        {
			$user = Sanitize::SanitizeString($_POST['user']);
        }
        //$user = $_POST['user'];
		$passwrd = Sanitize::SanitizeString($_POST['passwrd']);
	}

	if (!$ldap->LDAPEnabled())
    {
		LDAPLoginPrompt(Sanitize::SanitizeString($_POST['user']), 'LDAP not enabled.','ldaplogin');
		obExit();
	}

	$ldap->authenticateUser($user, $passwrd, $ldap_index);

	if ($ldap->error)
    {
		LDAPLoginPrompt(Sanitize::SanitizeString($_POST['user']), "LDAP Error - Unable to authenticate user.  " . $ldap->ldapErrorMsg . ldap_err2str($ldap->ldap_errorno));
		obExit();
	}

	$_SESSION['ldap']['user'] = $user;
	$_SESSION['ldap']['passwrd'] = $passwrd;
	$_SESSION['ldap']['ldap_index'] = $ldap_index;

	LDAPListEntries();
	obExit();
}

function LDAPSyncLogin($user = '', $errormsg = '') {

    require_once 'Source/libs/LDAP.php';

	$ldap = new datixLDAP();

	if (is_array($_POST)) {
        if ($_POST['domain'])
        {
            $ldap_index = array_flip($ldap->ldap['domains'])[$_POST['domain']];
        }
        $user = Sanitize::SanitizeString($_POST['domain']) . '\\' . Sanitize::SanitizeString($_POST['user']);
        //$user = $_POST['user'];
		$passwrd = Sanitize::SanitizeString($_POST['passwrd']);
	}

	if (!$ldap->LDAPEnabled()) {
		LDAPLoginPrompt(Sanitize::SanitizeString($_POST['user']), 'LDAP not enabled.', 'ldapsynclogin&groupid=' . Sanitize::SanitizeInt($_GET["groupid"]));
		obExit();
	}

	$ldap->AuthenticateUser($user, $passwrd, $ldap_index);

	if ($ldap->error) {
		//LDAPLoginPrompt($_POST['user'], $ldap->errorMsg(), 'ldapsynclogin&groupid=' . $_GET["groupid"]);
        $ldap->Close();
        LDAPLoginPrompt(Sanitize::SanitizeString($_POST['user']), "LDAP Error - Unable to authenticate user", 'ldapsynclogin&groupid=' . Sanitize::SanitizeInt($_GET["groupid"]));
        obExit();
	}

	$_SESSION['ldap']['user'] = $user;
	$_SESSION['ldap']['passwrd'] = $passwrd;
	$_SESSION['ldap']['ldap_index'] = $ldap_index;

    // Don't need to keep the connection open as ImportLDAPGroup(),
    // called iteratively by DoLDAPSync, opens a new connection each
    // time it is called.
    $ldap->Close();

    if ($_GET["type"] == "profile")
    {
        DoLDAPProfileSync(Sanitize::SanitizeInt($_GET["profileid"]), Sanitize::SanitizeString($_GET["updateonly"]));
    }
    else if ($_GET['type'] == 'user')
    {
	    DoLDAPUserSync(Sanitize::SanitizeString($_GET["updateonly"]));
	}
    else
    {
	    DoLDAPSync(Sanitize::SanitizeInt($_GET["groupid"]), Sanitize::SanitizeString($_GET["updateonly"]));
	}
    obExit();
}

function LDAPSync()
{

	$_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;

    require_once 'Source/libs/LDAP.php';

    if (!$_SESSION['ldap']['user'] || !$_SESSION['ldap']['passwrd'])
    {
		// Prompt user for active directory credentials
		LDAPLoginPrompt('','','ldapsynclogin&groupid=' . Sanitize::SanitizeInt($_GET["groupid"]) . '&updateonly=' . Sanitize::SanitizeString($_GET["updateonly"]));
		obExit();
	}

	DoLDAPSync(Sanitize::SanitizeInt($_GET["groupid"]), Sanitize::SanitizeString($_GET["updateonly"]));
	obExit();
}

function DoLDAPSync($grp_id = "", $UpdateExistingOnly = "")
{
    global $NoMainMenu, $txt;
    $NoMainMenu = true;
    $Progress = template_header('Updating users', 'Please wait', true);

    $_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;
    //$grp_id = $_GET["groupid"];
?>
<html><head>
<link rel="stylesheet" type="text/css" href="css/datix.css" />
<link rel="SHORTCUT ICON" href="Images/datix.ico" />
<title></title>
</head><body text="#000000" link="#0033ff" bgcolor="#ffffff">

<table class="bordercolor" cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#6394bd" border="0">
  <tr>
    <td>
	  <table class="bordercolor" cellspacing="1" cellpadding="0" width="100%" align="center" bgcolor="#6394bd" border="0">
        <tr>
          <td>
            <table cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#ffffff" border="0" bordercolor="#6394bd">
              <tr>
                <td valign="top" width="100%">
<table border="0" width="100%" cellspacing="1" cellpadding="4" class="formbg" align="center">
    <tr>
		<td class="windowbg" width="100%" valign="middle">
			<table>
                <tr>
                    <td><font size="3"><b><?=$txt["sync_group_mappings_title"]?></b></font></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
	    <td class="formbg" width="100%">
	        <table class="formbg" width="100%" cellspacing="1" cellpadding="1" align="left" border="0">
	            <tr>
					<td class="menubg" width="85%" valign="top">
                        <table class="titlebg" align="left" border="0" cellpadding="0" cellspacing="1" width="100%">
                            <tr>
                                <td>
                                    <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">

<?php
    //Clear out group links for LDAP users before re-linking required users
    if ($grp_id)
    {
        $resetsql = "DELETE FROM sec_staff_group WHERE grp_id = " . $grp_id . " AND grp_id IN (SELECT grp_id FROM sec_ldap_groups) AND con_id in (SELECT recordid FROM staff WHERE sta_sid IS NOT NULL AND sta_sid != '')";
    }
    else
    {
        $resetsql = "DELETE FROM sec_staff_group WHERE con_id in (SELECT recordid FROM staff WHERE sta_sid IS NOT NULL AND sta_sid != '') AND grp_id IN (SELECT grp_id FROM sec_ldap_groups)";
    }

    db_query($resetsql);

    // Retrieve LDAP mappings
    $sql = "SELECT ldap_dn, grp_id FROM sec_ldap_groups";
    if ($grp_id)
    {
        $sql .= " WHERE grp_id = $grp_id";
    }
    $request = db_query($sql);
	while ($row = db_fetch_array($request))
    {
        $GroupDN[] = $row;
    }

    if($GroupDN)
    {
        $usersSyncedToGroup = [];
		$i = 0;
        foreach ($GroupDN as $ldap_dn)
        {
			$usersSyncedToGroup = array_merge($usersSyncedToGroup, ImportLDAPGroup($ldap_dn["ldap_dn"], $ldap_dn["grp_id"], $UpdateExistingOnly));
            $i++;
            if ($Progress)
            {
                $Progress->SetProgressBar(($i / count($GroupDN)) * 100);
            }

			if (!empty($usersSyncedToGroup))
			{
				//now remove from the profile any AD-synced users who no longer exist as part of the AD group.
				DatixDBQuery::PDO_query('DELETE FROM sec_staff_group WHERE grp_id = '.$ldap_dn["grp_id"].' AND con_id IN (SELECT recordid FROM contacts_main WHERE con_sid NOT IN (\'' . implode('\',\'', $usersSyncedToGroup) . '\') AND con_sid IS NOT NULL AND con_sid != \'\')');
			}
			else
			{
				//no one left in this group - remove all AD-synced users from it
				DatixDBQuery::PDO_query('DELETE FROM sec_staff_group WHERE grp_id = '.$ldap_dn["grp_id"].' AND con_id IN (SELECT recordid FROM contacts_main WHERE con_sid IS NOT NULL AND con_sid != \'\')');
			}


		}
    }
?>
									</table>
                                </td>
                            </tr>
                        </table>
                    </td>
				</tr>
            </table>
		</td>
    </tr>
    <tr>
        <td class="titlebg" align="center">
            <input type="button" value="<?= $txt["btn_close"] ?>" onclick="javascript:window.close();"/>
        </td>
    </tr>
</table>
<?php
    if ($Progress)
	{
		echo '<script>
document.getElementById("waiting").style.display="none";
</script>';
		ob_flush();
		flush();
	}
    obExit();
}

function ImportLDAPGroup($ldap_dn, $GroupID, $UpdateExistingOnly = "")
{
    global $Progress, $txt, $ModuleDefs;
    // Retrieve LDAP information
    require_once 'Source/libs/LDAP.php';
    require_once 'Source/security/SecurityBase.php';
	$ldap = new datixLDAP();
	$LDAPEnabled = $ldap->LDAPEnabled();
	// Do binding user LDAP credentials
    if ($_SESSION['ldap']['user'] && $_SESSION['ldap']['passwrd'])
    {
		$usr = $_SESSION['ldap']['user'];
		$pwd = $_SESSION['ldap']['passwrd'];
		$ldap_index = $_SESSION['ldap']['ldap_index'];
	}

	$connect = $ldap->Connect($usr, $pwd, $ldap_index);

    if ($ldap->error)
    {
        $ldap->Close();
		LDAPLogin($usr, $ldap->errorMsg());
		obExit();
	}

    //Check for Object Class (i.e. Group, OU, CN etc.)
    $ObjectClass = $ldap->GetObjectClass($connect, $ldap_dn);

    if ($ObjectClass != "2;top;group")
    {
        $ldap->GetOUInfo($usr, $pwd, $ldap_dn, $ldap_index);
    }
    else
    {
        $ldap->GetGroupInfo($usr, $pwd, $ldap_dn, $ldap_index);
    }

    if ($ldap->group["samaccountname"] != "")
        echo '<tr><td class="titlebg"><b>' . $ldap_dn . '</b></td></tr>';

    if ($ObjectClass == "4;top;person;organizationalPerson;user")    //Mapping direct to user
    {
        $GroupMembers[0] = $ldap_dn;
    }
    else
    {
        $GroupMembers = $ldap->group['member'];
    }

    if ($ObjectClass != "2;top;group")
    {
        $NumMembers = count($GroupMembers);
    }
    else
    {
        $NumMembers = $GroupMembers['count'];
    }

    $ldapMemberSIDs = array();

    for ($i=0; $i < $NumMembers; $i++)
    {
        $staffID = "";
        $ldap->GetUserInfo($usr, $pwd, $GroupMembers[$i], $ldap_index);

        if ($ldap->user["sid"])
        {
            $ldapMemberSIDs[] = $ldap->user["sid"];
        }

        //Check that object is a user
        if ($ldap->user["objectclass"] == "4;top;person;organizationalPerson;user")
        {

            $sql = "SELECT recordid, initials, fullname, lockout, permission, email, tel1,
    		    login_tries, sta_clingroup, sta_orgcode, sta_directorate,
    		    sta_specialty, sta_unit, login, sta_surname, sta_forenames
                FROM staff
    		    WHERE sta_sid = '" . $ldap->user["sid"] ."'";
    	    $request = db_query($sql);

    	    if ($StaffRow = db_fetch_array($request))
            {
    		    $ModulePerms = ParsePermString($StaffRow["permission"]);

                if ($ldap->user["sid"])
                {
                    if (!$ldap->UpdateContactRecordBySID())
                    {
                        $ldap->Close();
                        echo "Failed to update Datix staff record for " . $ldap->user["login"];
                        continue;
                    }
                    else
                    {
                        echo '<tr><td class="rowtitlebg">'. $ldap->user["login"] . ' - Updated</td></tr>';
                    }
                }
                else
                {
                    $ldap->Close();
                    echo "Unable to update Datix staff record with LDAP details. No SID set for ". $ldap->user["login"];
    		        continue;
                }
                $staffID = $StaffRow["recordid"];
                $initials = $StaffRow["initials"];
            }
            else
            {
			    if ($UpdateExistingOnly != 1)
                {
                    // Insert new staff record
                    $staffID = $ldap->CreateContactRecord();

			        if ($staffID == 0)
                    {
                        echo "Failed to create Datix staff record for " . $ldap->user["login"];
    		            continue;
                    }
                    else
                    {
                        echo '<tr><td class="rowtitlebg">'. $ldap->user["login"] . ' - Added</td></tr>';
                    }

			        $initials = "$staffID";
                }
            }

            $error = false;
            $lic_error_message = array();
            if (!empty($staffID))
            {
                //Check if group link already exists and insert if it does not.
                $sql = "SELECT COUNT(*) as link_count FROM sec_staff_group WHERE grp_id = $GroupID AND con_id = $staffID";
                $result = DatixDBQuery::PDO_fetch($sql);

                if ($result["link_count"] < 1)
                {
                    require_once 'Source/libs/Licence.php';
                    $licence = New licence();
                    require_once 'Source/security/SecurityGroups.php';
                    $GroupModules = GetGroupModules($GroupID);
                    if (!empty($GroupModules))
                    {
                        //Foreach Module Perm
                        foreach ($GroupModules as $index => $Mod)
                        {
                            //Check if limit reached already
                            if ($licence->checkUserLimitExceeded($Mod, array($staffID)))
                            {
                                $lic_error_message[$Mod] = $ModuleDefs[$Mod]["NAME"];
                                $error = true;
                            }
                        }

                    }

                    if ($error)
                    {
                        echo '<tr><td class="rowtitlebg">Failed to add ' . $ldap->user["login"] . " to group ". $GroupID . ". " .  $txt["user_limit_exceeded_err"] . ": ".  implode(", ", $lic_error_message) . '</td></tr>';
                        continue;
                    }

                    $sql = "INSERT INTO sec_staff_group (grp_id, con_id) VALUES ($GroupID, $staffID)";
                    if (db_query($sql) === false)
                    {
                        echo "Failed to add " . $ldap->user["login"] . " to group ". $GroupID;
                        continue;
                    }
                }
            }
        }
    }

	if ($ldap->error)
	{
		echo '<div>An error occurred while importing from group '.$GroupID.':</div>';
		echo '<div>'.$ldap->ldapErrorMsg.'</div>';
	}

	$ldap->Close();

	return $ldapMemberSIDs;

}

function LDAPSyncAll()
{
	$_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;

    require_once 'Source/libs/LDAP.php';

	if (!$_SESSION['ldap']['user'] || !$_SESSION['ldap']['passwrd']) {
		// Prompt user for active directory credentials
        LDAPLoginPrompt('','','ldapsynclogin');
		obExit();
	}

	DoLDAPSync("", $_GET["updateonly"]);
	obExit();
}

function LDAPProfileSync()
{
    $_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;

    require_once 'Source/libs/LDAP.php';
    $ProfileID = Sanitize::SanitizeInt($_GET["profileid"]);
    $Updateonly = Sanitize::SanitizeString($_GET["updateonly"]);

    if (!$_SESSION['ldap']['user'] || !$_SESSION['ldap']['passwrd'])
    {
        // Prompt user for active directory credentials
        LDAPLoginPrompt('','','ldapsynclogin&type=profile&profileid=' . $ProfileID . '&updateonly=' . $Updateonly);
        obExit();
    }

    DoLDAPProfileSync($ProfileID, $Updateonly);
    obExit();
}

function LDAPProfileSyncAll()
{
    $_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;

    require_once 'Source/libs/LDAP.php';

    if (!$_SESSION['ldap']['user'] || !$_SESSION['ldap']['passwrd']) {
        // Prompt user for active directory credentials
        LDAPLoginPrompt('','','ldapsynclogin&type=profile');
        obExit();
    }

    DoLDAPProfileSync("", $_GET["updateonly"]);
    obExit();
}

function DoLDAPProfileSync($pfl_id = "", $UpdateExistingOnly = "")
{
    global $NoMainMenu, $Progress, $txt;
    $NoMainMenu = true;
    $Progress = template_header_nomenu();

    $_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;
    //$grp_id = $_GET["groupid"];
?>
<div class="heading">
    <div>
        <h1>
            <img align="middle" alt="" src="Images/icons/icon_ADM.png" border="0" style="float:left;" />
            <span style="padding:1px"><?=$txt["sync_profile_mappings_title"]?></span>
        </h1>
    </div>
    <div class="clearfix"></div>
</div>

<?php

    // Retrieve LDAP mappings
    $sql = "SELECT ldap_dn, pfl_id, ISNULL(pfl_ldap_order , 2147483647) as porder FROM sec_ldap_profiles, profiles WHERE sec_ldap_profiles.pfl_id = profiles.recordid";
    if ($pfl_id)
    {
            $sql .= " AND pfl_id = $pfl_id";
    }
    $sql .= " ORDER BY porder DESC, profiles.recordid DESC";

    $request = db_query($sql);
    while ($row = db_fetch_array($request))
    {
        $ProfileDN[] = $row;
    }

    if($ProfileDN)
    {
        $usersSynced = [];
		$i = 0;
        foreach ($ProfileDN as $ldap_dn)
        {
			$usersSynced = array_merge($usersSynced, ImportLDAPProfile($ldap_dn["ldap_dn"], $ldap_dn["pfl_id"], $UpdateExistingOnly));
            $i++;
            if ($Progress)
            {
                $Progress->SetProgressBar(($i / count($ProfileDN)) * 100);
            }
        }

		if (!empty($usersSynced))
		{
			//now remove from the profile any AD-synced users who no longer exist as part of the AD group.
			DatixDBQuery::PDO_query('UPDATE contacts_main SET sta_profile = NULL WHERE sta_profile = :sta_profile AND con_sid IS NOT NULL AND con_sid != \'\' AND con_sid NOT IN (\'' . implode('\',\'', $usersSynced) . '\')', array('sta_profile' => $ldap_dn["pfl_id"]));
		}
		else
		{
			//no one left in this profile - remove all AD-synced users from it
			DatixDBQuery::PDO_query('UPDATE contacts_main SET sta_profile = NULL WHERE sta_profile = :sta_profile AND con_sid IS NOT NULL AND con_sid != \'\'', array('sta_profile' => $ldap_dn["pfl_id"]));
		}
	}
?>
        <div class="button_wrapper" align="center">
            <input type="button" value="<?= $txt["btn_close"] ?>" onclick="javascript:window.close();"/>
        </div>
<?php
    if ($Progress)
    {
        echo '<script>
document.getElementById("waiting").style.display="none";
</script>';
        ob_flush();
        flush();
    }
    obExit();
}

function LDAPUserSyncAll()
{
    $_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;

    require_once 'Source/libs/LDAP.php';

    if (!$_SESSION['ldap']['user'] || !$_SESSION['ldap']['passwrd'])
    {
        // Prompt user for active directory credentials
        LDAPLoginPrompt('','','ldapsynclogin&type=user');
        obExit();
    }

    DoLDAPUserSync($_GET["updateonly"]);
    obExit();
}

function DoLDAPUserSync($UpdateExistingOnly = "")
{
    global $NoMainMenu, $Progress, $txt;
    $NoMainMenu = true;
    $Progress = template_header_nomenu();

    $_SESSION['ldap']['return_form_element'] = isset($_GET['form_element']) ? Sanitize::SanitizeString($_GET['form_element']) : null;
    //$grp_id = $_GET["groupid"];
?>
<div class="heading">
    <div>
        <h1>
            <img align="middle" alt="" src="Images/icons/icon_ADM.png" border="0" style="float:left;" />
            <span style="padding:1px"><?php echo _tk('sync_user_mappings_title') ?></span>
        </h1>
    </div>
    <div class="clearfix"></div>
</div>

<?php

    //TODO: What should happen to users that no longer exist?

    // Retrieve LDAP mappings
    $UserDN = \DatixDBQuery::PDO_fetch_all('SELECT dn FROM sec_ldap_all', array(), \PDO::FETCH_COLUMN);

        $i = 0;
        foreach ($UserDN as $ldap_dn)
        {
            ImportLDAPUser($ldap_dn, $UpdateExistingOnly);
            $i++;
            if ($Progress)
            {
                $Progress->SetProgressBar(($i / count($UserDN)) * 100);
            }
        }

?>
        <div class="button_wrapper" align="center">
            <input type="button" value="<?= $txt["btn_close"] ?>" onclick="window.close();"/>
        </div>
<?php
    if ($Progress)
    {
        echo '<script language="text/JavaScript">
document.getElementById("waiting").style.display="none";
</script>';
        ob_flush();
        flush();
    }
    obExit();
}

function ImportLDAPUser($ldap_dn, $UpdateExistingOnly = "")
{
    global $Progress, $ModuleDefs, $txt;
    // Retrieve LDAP information
    require_once 'Source/libs/LDAP.php';
    require_once 'Source/security/SecurityBase.php';
    $ldap = new datixLDAP();
    $LDAPEnabled = $ldap->LDAPEnabled();
    // Do binding user LDAP credentials
    if ($_SESSION['ldap']['user'] && $_SESSION['ldap']['passwrd'])
    {
        $usr = $_SESSION['ldap']['user'];
        $pwd = $_SESSION['ldap']['passwrd'];
        $ldap_index = $_SESSION['ldap']['ldap_index'];
    }

    $connect = $ldap->Connect($usr, $pwd, $ldap_index);

    if ($ldap->error)
    {
        $ldap->Close();
        LDAPLogin($usr, $ldap->errorMsg());
        obExit();
    }

    //Check for Object Class (i.e. Group, OU, CN etc.)
    $ObjectClass = $ldap->GetObjectClass($connect, $ldap_dn);

    if ($ObjectClass != "2;top;group")
    {
        $ldap->GetOUInfo($usr, $pwd, $ldap_dn, $ldap_index);
    }
    else
    {
        $ldap->GetGroupInfo($usr, $pwd, $ldap_dn, $ldap_index);
    }
    //$ldap['info'] = @ldap_get_entries($ldap["conn"], $ldap['result']);
    if ($ldap->group["samaccountname"] != "")
    {
        echo '<div class="padded_div"><b>' . $ldap_dn . ' - Complete</b></div>';
    }

    // $sql = "UPDATE contacts_main SET sta_profile = NULL WHERE sta_profile = $ProfileID";
    // db_query($sql);

    if ($ObjectClass == "4;top;person;organizationalPerson;user")    //Mapping direct to user
    {
        $GroupMembers[0] = $ldap_dn;
    }
    else
    {
        $GroupMembers = $ldap->group['member'];
    }

    if ($ObjectClass != "2;top;group")
    {
        $NumMembers = count($GroupMembers);
    }
    else
    {
        $NumMembers = $GroupMembers['count'];
    }

    $ContactsAdded = 0;
    $ContactsUpdated = 0;

    for ($i=0; $i < $NumMembers; $i++)
    {
        $staffID = "";
        $ldap->GetUserInfo($usr, $pwd, $GroupMembers[$i], $ldap_index);

        //Check that object is a user and that they have a surname
        if ($ldap->user["objectclass"] == "4;top;person;organizationalPerson;user" && !empty($ldap->user["con_surname"]))
        {
            $sql = "SELECT recordid, initials, fullname, lockout, permission, email, tel1,
                login_tries, sta_clingroup, sta_orgcode, sta_directorate,
                sta_specialty, sta_unit, login, sta_surname, sta_forenames
                FROM staff
                WHERE sta_sid = '" . $ldap->user["sid"] ."'";
            $request = db_query($sql);

            if ($StaffRow = db_fetch_array($request))
            {
                if ($ldap->user["sid"])
                {
                    if (!$ldap->UpdateContactRecordBySID($StaffRow['recordid']))
                    {
                        $ldap->Close();
                        echo "Failed to update Datix staff record for " . $ldap->user["login"];
                        continue;
                    }
                    else
                    {
                        $ContactsUpdated++;
                    }
                }
                else
                {
                    $ldap->Close();
                    echo "Unable to update Datix staff record with LDAP details. No SID set for " . $ldap->user["login"];
                    continue;
                }
                $staffID = $StaffRow["recordid"];
                $initials = $StaffRow["initials"];
            }
            else
            {
                if ($UpdateExistingOnly != 1)
                {
                    // Insert new staff record
                    $staffID = $ldap->CreateContactRecord();

                    if ($staffID == 0)
                    {
                        echo "Failed to create Datix staff record for " . $ldap->user["login"];
                        continue;
                    }
                    else
                    {
                        $ContactsAdded++;
                    }

                    $initials = "$staffID";
                }
            }
        }
    }

    echo '<div class="padded_div">'. $ContactsAdded . ' contacts inserted. '. $ContactsUpdated .' contacts updated.</div>';

    $ldap->Close();
}


function ImportLDAPProfile($ldap_dn, $ProfileID, $UpdateExistingOnly = "")
{
    global $Progress, $ModuleDefs, $txt;
    // Retrieve LDAP information
    require_once 'Source/libs/LDAP.php';
    require_once 'Source/security/SecurityBase.php';
    $ldap = new datixLDAP();
    $LDAPEnabled = $ldap->LDAPEnabled();
    // Do binding user LDAP credentials
    if ($_SESSION['ldap']['user'] && $_SESSION['ldap']['passwrd'])
    {
        $usr = $_SESSION['ldap']['user'];
        $pwd = $_SESSION['ldap']['passwrd'];
        $ldap_index = $_SESSION['ldap']['ldap_index'];
    }

    $connect = $ldap->Connect($usr, $pwd, $ldap_index);

    if ($ldap->error)
    {
        $ldap->Close();
        LDAPLogin($usr, $ldap->errorMsg());
        obExit();
    }

    //Check for Object Class (i.e. Group, OU, CN etc.)
    $ObjectClass = $ldap->GetObjectClass($connect, $ldap_dn);

    if ($ObjectClass != "2;top;group")
    {
        $ldap->GetOUInfo($usr, $pwd, $ldap_dn, $ldap_index);
    }
    else
    {
        $ldap->GetGroupInfo($usr, $pwd, $ldap_dn, $ldap_index);
    }
    //$ldap['info'] = @ldap_get_entries($ldap["conn"], $ldap['result']);
    if ($ldap->group["samaccountname"] != "")
    {
        echo '<div class="padded_div"><b>' . $ldap_dn . '</b></div>';
    }

   // $sql = "UPDATE contacts_main SET sta_profile = NULL WHERE sta_profile = $ProfileID";
   // db_query($sql);

    if ($ObjectClass == "4;top;person;organizationalPerson;user")    //Mapping direct to user
    {
        $GroupMembers[0] = $ldap_dn;
    }
    else
    {
        $GroupMembers = $ldap->group['member'];
    }

    if ($ObjectClass != "2;top;group")
    {
        $NumMembers = count($GroupMembers);
    }
    else
    {
        $NumMembers = $GroupMembers['count'];
    }

    $ldapMemberSIDs = array();

    for ($i=0; $i < $NumMembers; $i++)
    {
        $staffID = "";
        $ldap->GetUserInfo($usr, $pwd, $GroupMembers[$i], $ldap_index);

        if ($ldap->user["sid"])
        {
            $ldapMemberSIDs[] = $ldap->user["sid"];
        }

        //Check that object is a user and that they have a surname
        if ($ldap->user["objectclass"] == "4;top;person;organizationalPerson;user")
        {

            $sql = "SELECT recordid, initials, fullname, lockout, permission, email, tel1,
                login_tries, sta_clingroup, sta_orgcode, sta_directorate,
                sta_specialty, sta_unit, login, sta_surname, sta_forenames
                FROM staff
                WHERE sta_sid = '" . $ldap->user["sid"] ."'";
            $request = db_query($sql);

            if ($StaffRow = db_fetch_array($request))
            {
                $ModulePerms = ParsePermString($StaffRow["permission"]);

                if ($ldap->user["sid"])
                {
                    if (!$ldap->UpdateContactRecordBySID())
                    {
                        $ldap->Close();
                        echo "<div>Failed to update Datix staff record for " . $ldap->user["login"].'</div>';
                        continue;
                    }
                    else
                    {
                        echo '<div>'. $ldap->user["login"] . ' - Updated. </div>';
                    }
                }
                else
                {
                    $ldap->Close();
                    echo "<div>Unable to update Datix staff record with LDAP details. No SID set for " . $ldap->user["login"].'</div>';
                    continue;
                }
                $staffID = $StaffRow["recordid"];
                $initials = $StaffRow["initials"];
            }
            else
            {
                if ($UpdateExistingOnly != 1)
                {
                    // Insert new staff record
                    $staffID = $ldap->CreateContactRecord();

                    if ($staffID == 0)
                    {
                        echo "<div>Failed to create Datix staff record for " . $ldap->user["login"].'</div>';
                        continue;
                    }
                    else
                    {
                        echo '<div>'. $ldap->user["login"] . ' - Added</div>';
                    }

                    $initials = "$staffID";
                }
            }

            if (!empty($staffID))
            {
                $sql = "UPDATE contacts_main SET sta_profile = $ProfileID WHERE recordid = $staffID";
                if (db_query($sql) === false)
                {
                    echo "<div>Failed to add " . $ldap->user["login"] . " to profile ". $ProfileID.'</div>';
                    continue;
                }
            }
        }
    }

	if ($ldap->error)
	{
		echo '<div>An error occurred while importing from profile '.$ProfileID.':</div>';
		echo '<div>'.$ldap->ldapErrorMsg.'</div>';
	}

    $ldap->Close();

	//Return an array of SIDs that have just been synced to use when cleaning up old contacts.
	return $ldapMemberSIDs;
}

?>
