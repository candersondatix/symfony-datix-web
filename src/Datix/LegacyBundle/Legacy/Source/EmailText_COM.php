<?php

$formName = "complaint";

$EmailText["Acknowledge"]["Subject"] = "Acknowledgment of $formName submission";

$EmailText["Acknowledge"]["Body"] = "You reported a $formName on ".$data["com_dreceived"]." using the Datix $formName form.
The $formName has been given number ".$data["com_ourref"].".\n
We would like to thank you for taking the time to report this $formName.\n
The Datix administrators.";

$EmailText["Notify"]["Subject"] = "Datix $formName Number ".$data["recordid"];

$EmailText["Notify"]["Body"] = "A $formName has been submitted via the DATIX web form.

The details are:

Form number: ".$data["recordid"]."

Description:

".$data["com_detail"]."

Please go to $scripturl?action=record&module=COM&recordid=".$data["recordid"]." to view and approve it.
";


$EmailText["Feedback"]["Subject"] = "DatixWeb feedback message";

$message = "This is a feedback message from $_SESSION[fullname]. Complaint form reference is ";
$message .= $data['recordid'];
$message .= ".\nThe feedback is: \n\n";
$message .= "\n\nPlease select this link to view the record: $scripturl?action=record&module=COM&recordid=$data[recordid]";

$EmailText["Feedback"]["Body"] = $message;


$EmailText['NewHandler']['Subject'] = "You are now handler for Datix "._tk('COMName')." $data[recordid]";
$EmailText['NewHandler']['Body'] = "You have been assigned as handler for Datix "._tk('COMName')." $data[recordid].

The details are:

Record ID: $data[recordid]

Description:

$data[com_summary]

Please go to $scripturl?action=record&module=COM&recordid=$data[recordid] to view it.
";

$EmailText['NewInvestigator']['Subject'] = "You are now an investigator for Datix "._tk('COMName')." $data[recordid]";
$EmailText['NewInvestigator']['Body'] = "You have been assigned as an investigator for Datix "._tk('COMName')." $data[recordid].

The details are:

Record ID: $data[recordid]

Description:

$data[com_summary]

Please go to $scripturl?action=record&module=COM&recordid=$data[recordid] to view it.
";

$EmailText['Overdue']['Subject'] = 'You have '.$data['overdue_num'].' overdue complaints to review';
$EmailText['Overdue']['Body'] = 'The following complaints are overdue for review.  Click on the links to access each record.

'.$data['overdue_list_html'];

?>