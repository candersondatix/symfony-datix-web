<?php

if ($module)
{
    $Module = $module; // because this will get called from multiple files
}

require_once "Source/incidents/Injuries.php";
require_once "Source/generic_modules/COM/ModuleFunctions.php";

$linkDetailsSectionForNormalContacts = array (
    "Title" => "Link details",
    "Condition" => ($ShowLinkDetails && $con['link_type'] != 'O'),
    'LinkFields' => true,
    'ContactsLinkTable' => 'link_contacts',
    "NotModes" => array("Search"),
    "Rows" => array(
        "link_role",
        "link_status",
        "link_occupation",
        "link_abs_start",
        "link_abs_end",
        "link_daysaway",
        "icon_cost",
        "link_is_riddor",
        "link_riddor",
        "link_age",
        "link_age_band",
        "link_deceased",
        "link_npsa_role",
        "link_mhact_section",
        "link_mhcpa",
        "link_notes",
        "link_dear",
        "link_ref",
        array('Name' => 'link_legalaid', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($con['link_type'] == 'M' || $con['link_type'] == 'A')))),
        array('Name' => 'link_lip', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($con['link_type'] == 'M' || $con['link_type'] == 'A')))),
        array('Name' => 'link_ndependents', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($con['link_type'] == 'M' || $con['link_type'] == 'A')))),
        array('Name' => 'link_agedependents', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($con['link_type'] == 'M' || $con['link_type'] == 'A')))),
        array('Name' => 'link_marriage', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($con['link_type'] == 'M' || $con['link_type'] == 'A')))),
        array('Name' => 'link_injuries', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($con['link_type'] == 'M' || $con['link_type'] == 'A')))),
        array('Name' => 'link_resp', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && $con['link_type'] == 'E'))),
        array('Name' => 'lcom_primary', 'Condition' => ($FormType == "Design" || ($Module == 'COM' && $con['link_type'] == 'C'))),
        array('Name' => 'link_plapat', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && $con['link_type'] == 'M'))),
        array('Name' => 'lcom_iscomplpat', 'Condition' => ($FormType == "Design" || ($Module == 'COM' && $con['link_type'] == 'C'))),
        array('Name' => 'link_patrelation', 'Condition' => ($FormType == "Design" || ($Module == 'COM' && $con['link_type'] == 'C') || ($Module == 'CLA' && $con['link_type'] == 'M'))),
        array("Name" => "show_injury", "Condition" => ($FormType == "Design" || $ShowLinkDetails && $module == "INC")),
        'link_date_admission',
        array('Name' => 'link_notify_progress', 'Condition' => ($FormType == "Design" || $con['link_role'] == GetParm('REPORTER_ROLE', 'REP'))),
    )
);

$linkDetailsSectionForRespondentsContacts = array (
    "Title" => "Link details Respondent",
    "Condition" => ($ShowLinkDetails && $con['link_type'] == 'O')|| ($FormType == "Design"),
    'LinkFields' => true,
    'ContactsLinkTable' => 'link_respondents',
    "NotModes" => array("Search"),
    "Rows" => array(
        'link_resp',
        'link_role',
        'link_notes',
        'indemnity_reserve_assigned',
        'expenses_reserve_assigned',
        'remaining_indemnity_reserve_assigned',
        'remaining_expenses_reserve_assigned',
        'resp_total_paid'
    )
);

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "link" => $linkDetailsSectionForNormalContacts,
    "link_respondents" => $linkDetailsSectionForRespondentsContacts,
    'policies' => array(
        "Title" => _tk('mod_policies_title'),
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'listpoliciesforrespondent' => array(
                'controller' => 'src\\policies\\controllers\\PoliciesController'
            )
        ),
        "Condition" => (GetParm('POL_PERMS') != '' && ($Module == 'CLA' && $con['link_recordid'] != '')) || $FormType == 'Design',
        "NotModes" => array('New','Search','Print'),
        'Listings' => array('policies' => array('module' => 'POL')),
        "Rows" => array()
    ),
    "response" => array(
        "Title" => "Response",
        'NewPanel' => true,
        'LinkFields' => true,
        "Condition" => ($con["link_exists"] && $con["link_type"] == "S" && $module == "SAB")|| ($FormType == "Design"),
        "Rows" => array(
            "link_read_date",
            "link_rsp_type",
            "link_rsp_date",
            "link_comments"
        )
    ),
    "injury" => array(
        "Title" => "Injury details",
        "Condition" => (($FormType == "Design" || $ShowLinkDetails && $Module == "INC")),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "ContactSubForm" => true,
        'Special' => 'SectionInjuryDetails',
        'LinkFields' => true,
        "LinkType" => $con['link_type'],
        "Rows" => array(
            array('Name' => 'dum_injury', 'Title' => 'Injury', 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_bodypart', 'Title' => 'Body part', 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_treatment', 'Title' => 'Treatment', 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true)
        )
    ),
    "link_sirs" => array(
        "Title" => "SIRS link details",
        "Condition" => (($FormType == 'Design' || $ShowLinkDetails && $Module == 'INC')),
        "NotModes" => array("Search"),
        'LinkFields' => true,
        "Rows" => array(
            "link_worked_alone",
            "link_become_unconscious",
            "link_req_resuscitation",
            "link_hospital_24hours",
            "link_clin_factors",
            "link_direct_indirect",
            "link_injury_caused",
            "link_attempted_assault",
            "link_discomfort_caused",
            "link_public_disorder",
            "link_verbal_abuse",
            "link_harassment",
            "link_police_pursue",
            "link_police_persue_reason",
            "link_pprop_damaged",
        )
    ),
    "property_section".($Suffix ? '_'.$Suffix :'') => array(
        "Title" => "Personal Property",
        "ContactSuffix" => $Suffix,
        'Special' => 'SectionPropertyDetails',
        "Condition" => (($FormType == 'Design' || $ShowLinkDetails && $Module == 'INC')),
        "LinkType" => $Type,
        'LinkFields' => true,
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "Rows" => array(
            array('Name' => 'dum_description', 'Title' => Labels_FormLabel::GetFormFieldLabel('ipp_description'), 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_property_type', 'Title' => Labels_FormLabel::GetFormFieldLabel('ipp_damage_type'), 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_value', 'Title' => Labels_FormLabel::GetFormFieldLabel('ipp_value'), 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true)
        )
    ),
    "compl_dates" => array(
        "Title" => "Complainant Chain",
        "Condition" => ($FormType == 'Design' || ($Module == 'COM' && $con['link_type'] == 'C')),
        "NotModes" => array("Search"),
        'LinkFields' => true,
        'ControllerAction' => [
            'complaintDates' => [
                'controller' => 'src\\complaints\\controllers\\ComplaintController'
            ]
        ]
    ),
    "compl_dates_history" => array(
        "Title" => "Complainant Chain History",
        "Condition" => ($FormType == 'Design' || ($Module == 'COM' && $con['link_type'] == 'C' && ShowComplaintDatesHistory($con['com_id'], $con['recordid']))),
        "NotModes" => array("Search"),
        'LinkFields' => true,
        'ControllerAction' => [
            'complaintDatesHistory' => [
                'controller' => 'src\\complaints\\controllers\\ComplaintController'
            ]
        ]
    ),
    "contact" => array(
        "Title" => "Contact details",
        "Rows" => array(
            array(
                "Name" => "recordid",
                "Title" => "Contact ID",
                "Type" => "string",
                "ReadOnly" => (!($form_action == "Design" || $form_action == "search"))
            ),
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'CON',
                'perms' => $CONPerms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $inc,
                'module' => 'CON',
                'perms' => $CONPerms,
                'approveobj' => $ApproveObj
            )),
            "con_title",
            "con_forenames",
            "con_surname",
            "con_jobtitle",
            "con_organisation",
            array(
                'Name' => "con_email",
                'ReadOnly' => ($FormType != 'Search' && $FormType !='Design' && array_key_exists('initials', $con) && $con['initials'] != '')
            ),
            "con_address",
            "con_postcode",
            "con_gender",
            "con_dopened",
            "con_dclosed",
            "con_dob",
            "con_dod",
            array(
                "Name" => "con_tel1",
                "Type" => "string",
                "Title" => "Telephone no. 1",
                "Width" => 30,
                "MaxLength" => 254
            ),
            array(
                "Name" => "con_tel2",
                "Type" => "string",
                "Title" => "Telephone no. 2",
                "Width" => 30,
                "MaxLength" => 254
            ),
            "con_fax",
            "con_number",
            "con_nhsno",
            "con_police_number",
            "con_ethnicity",
            "con_language",
            "con_disability",
            'con_religion',
            'con_sex_orientation',
            'con_work_alone_assessed',
            "con_type",
            "con_subtype",
            "con_notes",
        )
    ),
    "employee" => array(
        "Title" => "Employee details",
        'ReadOnly' =>  ($module == 'SAB' || $module == 'DST'),
        "Rows" => array(
            "con_orgcode",
            "con_unit",
            "con_clingroup",
            "con_directorate",
            "con_specialty",
            "con_loctype",
            "con_locactual",
            "con_empl_grade",
            "con_payroll"
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $con['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('CON', $con['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    'additional_info' => array(
        "Title" => "Additional Information",
        "Rows" => array(
            'show_document'
        )
    ),
    "documents" => array(
        "Title" => "Documents",
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk("mod_templates_title"),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController'
            )
        ),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N')) && (!$ShowLinkDetails || $FormType == 'Design')),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "incidents" => array(
        "Title" => $ModuleDefs['INC']['NAME'].($con["linkedincnum"] ? ' ('.$con["linkedincnum"].')' : ''),
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'ControllerAction' => [
            'listLinkedRecords' => [
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            ]
        ],
        "ExtraParameters" => array('link_module' => 'INC'),
        "Condition" => $_SESSION["licensedModules"][MOD_INCIDENTS],
        "Rows" => array()
    ),
    "risks" => array(
        "Title" => $ModuleDefs['RAM']['NAME'].($con["linkedramnum"] ? ' ('.$con["linkedramnum"].')' : ''),
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'ControllerAction' => [
            'listLinkedRecords' => [
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            ]
        ],
        "ExtraParameters" => array('link_module' => 'RAM'),
        "Condition" => $_SESSION["licensedModules"][MOD_RISKREGISTER],
        "Rows" => array()
    ),
    "pals" => array(
        "Title" => $ModuleDefs['PAL']['NAME'].($con["linkedpalnum"] ? ' ('.$con["linkedpalnum"].')' : ''),
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'ControllerAction' => [
            'listLinkedRecords' => [
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            ]
        ],
        "ExtraParameters" => array('link_module' => 'PAL'),
        "Condition" => $_SESSION["licensedModules"][MOD_PALS],
        "Rows" => array()
    ),
    "complaints" => array(
        "Title" => $ModuleDefs['COM']['NAME'].($con["linkedcomnum"] ? ' ('.$con["linkedcomnum"].')' : ''),
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'ControllerAction' => [
            'listLinkedRecords' => [
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            ]
        ],
        "ExtraParameters" => array('link_module' => 'COM'),
        "Condition" => $_SESSION["licensedModules"][MOD_COMPLAINTS],
        "Rows" => array()
    ),
    "claims" => array(
        "Title" => $ModuleDefs['CLA']['NAME'].($con["linkedclanum"] ? ' ('.$con["linkedclanum"].')' : ''),
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'ControllerAction' => [
            'listLinkedRecords' => [
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            ]
        ],
        "ExtraParameters" => array('link_module' => 'CLA'),
        "Condition" => $_SESSION["licensedModules"][MOD_CLAIMS],
        "Rows" => array()
    ),
);

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}
