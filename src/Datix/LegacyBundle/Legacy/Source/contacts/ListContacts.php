<?php
use src\security\Escaper;

function ListContacts($message = "", $userLogin = "")
{
	global $dtxtitle, $yySetLocation, $scripturl, $dtxdebug, $NoMainMenu, $ModuleDefs, $FieldDefs, $FieldDefsExtra,
           $MinifierDisabled;

    //page load time display - only testing purpose
	if($dtxdebug) {
		$starttime = explode(' ', microtime());
		$starttime = $starttime[1] + $starttime[0];
	}

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

	LoggedIn();

	$dtxtitle = "Contact list";

    // Set LAST_PAGE to be able to redirect user when a Bacth Update is performed
    $_SESSION['LAST_PAGE'] = $scripturl.'?'.Sanitize::SanitizeString($_SERVER['QUERY_STRING']);

	if ($userLogin)
    {
		$MatchLetter = \UnicodeString::strtoupper($userLogin{0});
    }
	elseif (isset($_GET["match"]))
    {
		$MatchLetter = Sanitize::SanitizeString($_GET["match"]);
    }

	$fromsearch = (isset($_GET["fromsearch"]) ? Sanitize::SanitizeString($_GET["fromsearch"]) : 0);

    $LinkMode = ($_GET['link'] == '1');

	if ($fromsearch || $MatchLetter < 'A' || $MatchLetter > 'Z')
    {
		$MatchLetter = 'all';
	}

    if(!$fromsearch) //this will already have been done at the search stage otherwise.
    {
        //get rid of any stored recordlist
        unset($_SESSION['CON']['RECORDLIST']);
    }

	$MatchLetter = EscapeQuotes($MatchLetter);

	$MatchField = Sanitize::SanitizeString($_GET["field"]);

	if (!$MatchField || $MatchField == 'fullname')
    {
		$MatchField = 'con_surname';
		$orderby = "con_surname, con_forenames, recordid";
	}
	elseif ($MatchField == 'con_forenames')
    {
		$orderby = "con_forenames, con_surname, recordid";
	}
	else
    {
		$orderby = $MatchField.($MatchField != 'recordid' ? ', recordid' : '');
	}

	$MatchField = EscapeQuotes($MatchField);

	$sort = Sanitize::SanitizeString($_GET["sort"]);

	if ($sort <> "desc")
    {
        $sort = "asc";
    }

	$listdisplay = GetParm("LISTING_DISPLAY_NUM", 20);

	if ($_REQUEST["page"])
    {
		$page = Sanitize::SanitizeString($_REQUEST["page"]);
    }
    else
    {
		$page = 1;
    }

	if ($_SESSION["FROMMODULE"] != '' && $_SESSION[$_SESSION["FROMMODULE"]]["CURRENTID"] != '')
    {
		$sql = "SELECT ". $ModuleDefs[$_SESSION['FROMMODULE']]['FIELD_NAMES']['NAME'] . " as rectitle"
		. " FROM " . $ModuleDefs[$_SESSION['FROMMODULE']]['TABLE']
		. " WHERE recordid =" . $_SESSION[$_SESSION["FROMMODULE"]]["CURRENTID"];

		$result = db_query($sql);
        $row = db_fetch_array($result);
		$RecordTitle = $row['rectitle'];
	}

	$where = array();

	if ($LinkMode && $_SESSION["CON"]["WHERELINK"] != "")
    {
		$where[] =  "(" . $_SESSION["CON"]["WHERELINK"] . ")";
	}
	elseif ($fromsearch && $_SESSION["CON"]["WHERE"] != "")
    {
		$where[] =  "(" . $_SESSION["CON"]["WHERE"] . ")";
	}
	elseif (isset($MatchLetter) && $MatchLetter != 'all')
    {
		$_SESSION["CON"]["WHERE"] = "($MatchField LIKE '$MatchLetter%')";
        $where[] =  "(" . $_SESSION["CON"]["WHERE"] . ")";
	}
    elseif ($MatchLetter == 'all')
    {
        $_SESSION["CON"]["WHERE"] = $ModuleDefs['CON']['HARD_CODED_LISTINGS']['all']['Where'];
    }

	$conPermWhere = MakeSecurityWhereClause("", "CON", $_SESSION["initials"]);

	if ($conPermWhere)
    {
		$where[] = "($conPermWhere)";
    }

	if (!empty($where))
    {
		$ContactsWhere = implode(" AND ", $where);
    }

    $ContactsWhere = TranslateWhereCom($ContactsWhere);

    // ==========================================================================//
    // Columns to be displayed                                                   //
    // ==========================================================================//

    $module = 'CON';
    require_once 'Source/libs/ListingClass.php';
    $Listing = new Listing('CON', GetParm( $LinkMode ? 'CON_MATCH_LISTING_ID' : 'CON_LISTING_ID' ) );
    $Listing->LoadColumnsFromDB();
    $list_columns = $Listing->Columns;


    //==========================================================================//
    // Prepare SQL Statement                                                    //
    //==========================================================================//

    if (is_array($list_columns))
    {
         foreach ($list_columns as $col_name => $col_info)
         {
             $table = GetTableForField ($col_name, 'CON');

             if ($table == 'contacts_main') {
                 $selectfields[$col_name] = $col_name;
             } else {
                 // linked fields should be excluded
                 unset ($list_columns[$col_name]);
             }
         }
    }

    if(!isset($selectfields['recordid']))
    {
        $selectfields['recordid'] = 'recordid';
    }

    foreach($selectfields as $key => $field)
    {
        if(!isset($FieldDefs['CON'][$field]))
        {
            $selectfields[$key] = 'null as '.$field;
        }
    }

	$request = GetPagedResults(
		"contacts_main", $selectfields, $ContactsWhere, $orderby, $sort,
		$page, $recordsPerPage,
		$overralCount, $actualCount, $sqloutout);

    if($_SESSION['CON']["WHERE"])
    {
        $ContactsWhere = TranslateWhereCom($_SESSION['CON']["WHERE"]);
    }

    //==========================================================================//
    // Push record list into session                                                    //
    //==========================================================================//

    if(!$_SESSION['CON']['RECORDLIST'])
    {
        /*foreach(explode(', ', $orderby) as $Field)
        {
            $Column = new Listings_ListingColumn($Field, 'contacts_main');
            $Column->setAscending();
            $OrderByFields[] = $Column;
        }  */

        $sql = 'SELECT recordid FROM contacts_main';

        if($ContactsWhere)
        {
            $sql .= ' WHERE '.$ContactsWhere;
        }

        if($orderby)
        {
            if($_GET['sort'] == 'desc')
            {
                $order = 'DESC';
            }
            else
            {
                $order = 'ASC';
            }

            $orderByArray = explode(',',$orderby);

            foreach($orderByArray as $orderByField)
            {
                $orderByArrayFinal[] = $orderByField.' '.$order;
            }

            $sql .= ' ORDER BY '.implode(',',$orderByArrayFinal);
        }

        $RecordData = DatixDBQuery::PDO_fetch_all($sql, array(), PDO::FETCH_COLUMN);

        $RecordList = new RecordLists_RecordListShell();
        $RecordList->AddRecordData($RecordData);

        $_SESSION['CON']['RECORDLIST'] = $RecordList;
    }

    $list_columns_count = count($list_columns);
    if($LinkMode)
    {
        $NoMainMenu = true;
    }

    if($LinkMode || $_SESSION['CON']['DUPLICATE_SEARCH'])
    {
        $list_columns_count++;
    }

    getPageTitleHTML(array(
         'title' => ($LinkMode ? "Matching contacts": "Contact List"),
         'module' => 'CON'
         ));

	if($LinkMode)
    {
        echo getBasicHeader();
        echo '<script type="text/javascript" src="js_functions/jquery/jquery-1.7.1.min.js"></script>'."\n";

        if (bYN(GetParm('CSRF_PREVENTION','N')))
        {
            echo '<script type="text/javascript"> var token = "'.CSRFGuard::getCurrentToken().'"; </script>'."\n";
            echo '<script type="text/javascript" src="js_functions/tokenizer'.$addMinExtension.'.js"></script>'."\n";
        }
    }
    else
    {
        GetSideMenuHTML(array('module' => 'CON', 'listing' => ($_GET["fromsearch"])));
        template_header_nopadding();
    }

	if($dtxdebug)
    {
		echo $sqloutout;
	}

    $data = \DatixDBQuery::PDO_fetch_all($request->queryString, array());
    foreach ($data as $val)
    {
        $recordids[] = $val['recordid'];
    }

    foreach ($list_columns as $col_field => $width)
    {
        if (preg_match('/^UDF.*_([0-9]+)$/ui', $col_field, $matches))
        {
            $udf_field_ids[] = $matches[1];
        }
    }

    $udfData = array();

    if (!empty($udf_field_ids) && !empty($recordids))
    {
        $udfData = getUDFDisplayData($module, $recordids, $udf_field_ids);
    }


?>
<table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
<?php
	if ($message != "") {
		echo '<tr><td class="titlebg" colspan="' . $list_columns_count . '">'
		. '<b>' . $message . '</b></td></tr>';
	}

    if ($LinkMode || ($fromsearch && $_SESSION["FROMMODULE"]))
    {
	    echo '<tr>
	    <td class="title-row" colspan="'.$list_columns_count.'" width="80%">'.
        ($LinkMode ? "Matching contacts": (($fromsearch && $_SESSION["FROMMODULE"]) ? "Link to record '" . $RecordTitle . "' as type '" . $ModuleDefs[$_SESSION["FROMMODULE"]]["SUBFORMS"]["CON"]["LINKTYPES"][$_SESSION[$_SESSION["FROMMODULE"]]["CURRENTLINKTYPE"]] ."'" : '')).'
	    </td></tr>';
    }

	if (!$_GET["fromsearch"])
    {
		echo '<tr class="page-numbers"><td colspan="'.$list_columns_count.'">';

		for ($a = ord('A'); $a <= ord('Z'); $a++)
        {
            $url = $scripturl.'?action=listcontacts&match='.chr($a).'&field='.Escape::EscapeEntities($MatchField).'&sort='.Escape::EscapeEntities($sort).'&page=1&fromsearch='.Escape::EscapeEntities($fromsearch);

			echo "<a href=\"{$url}\">";

			if (ord($MatchLetter) == $a)
            {
				echo "<b>[" . chr($a) . "]</b>";
            }
			else
            {
				echo chr($a);
            }

			echo '</a>&nbsp;&nbsp;';
		}

		echo '&nbsp;&nbsp;&nbsp;';

        $url = $scripturl . "?action=listcontacts&match=all&sort=".Escape::EscapeEntities($sort)."&page=1&fromsearch=".$fromsearch;

		echo "<a href=\"{$url}\">";

		if ($MatchLetter == 'all')
			echo "<b>[All]</b>";
		else
			echo "All";

		echo '</a></td></tr>';
	}
    elseif (bYN(GetParm("SAVED_QUERIES", "Y")) && !$LinkMode)
    {
?>    <tr>
        <td class="titlebg" colspan="<?= $list_columns_count ?>" nowrap="nowrap">
            <form method="post" name="queryform" action="<?= "$scripturl?action=executequery&module=CON"?>">
            <table>
                <tr>
                    <td class="titlebg" width="100%">
                        <b><?php echo _tk('query_colon')?></b>
                        <select name="qry_name" <?php if(!is_array($saved_queries) || empty($saved_queries)) { echo 'style="width:'.DROPDOWN_WIDTH_DEFAULT.'px"'; } ?> onchange="Javascript:if (document.queryform.qry_name.options[document.queryform.qry_name.selectedIndex].value != 'range' && document.queryform.qry_name.options[document.queryform.qry_name.selectedIndex].value != '') {document.queryform.submit()}">
<?php
        require_once 'Source/libs/QueriesBase.php';
        $query_recordid = Sanitize::SanitizeString($_GET["query"]);
        $saved_queries = get_saved_queries('CON');
        if (is_array($saved_queries))
        {
            echo '<option value=""'
                    . ($query_recordid == $query ? ' selected="selected"' : '') . '>'
                    . 'Choose</option>';

            foreach ($saved_queries as $query => $querytitle)
            {
                echo '<option value="' . $query . '"'
                    . ($query_recordid == $query ? ' selected="selected"' : '') . '>'
                    . $querytitle . '</option>';
            }
        }
?>
                        </select>
                    </td>
                </tr>
            </table>
                <input type="hidden" name="orderby" value="<?= $orderby ?>" />
            </form>
        </td>
    </tr>
<?php
    }

	if ($overralCount == 0  && (!bYN(GetParm('EXTERNAL_CONTACTS', 'N')) || isset($GLOBALS['external_contact_address'])))
    {
		echo '<tr><td class="windowbg2" colspan="' . $list_columns_count . '"><b>'._tk('no_contacts_found').'</b></td><tr>';
	}
	else
    {
		echo '<tr><td class="windowbg2" colspan="' . $list_columns_count . '">'
		. '<b>' . $overralCount . ' contact' . ($overralCount == 1 ? "" : "s") . ' found. Displaying '
		. (($page-1)*$recordsPerPage+1) . '-' . (($page-1)*$recordsPerPage+$actualCount)
		. '.</b></td></tr>';

        if ($overralCount > $recordsPerPage) {
            $prevPageLink = '';
            $nextPageLink = '';
            if ($page > 1)
            {
                $prevPageUrl = Sanitize::SanitizeURL($scripturl . '?action=listcontacts&match=' . $MatchLetter
                . '&field=' . Escape::EscapeEntities($_GET["field"]) . '&sort=' . Escape::EscapeEntities($_GET["sort"])
                . '&page=' . ($page-1) . '&fromsearch=' . $fromsearch
                . ($LinkMode ? "&link=1" : "") . ($query_recordid ? "&query=$query_recordid" : "")
                . (bYN(GetParm('CSRF_PREVENTION','N')) ? "&token=".CSRFGuard::getCurrentToken() : ""));

                $prevPageLink = '<a href="' . $prevPageUrl . '"><b>'._tk('previous_page').'</b></a>';
            }

            if($overralCount > ($recordsPerPage*($page-1) + $actualCount))
            {
                $nextPageUrl = Sanitize::SanitizeURL($scripturl . '?action=listcontacts&match=' . $MatchLetter
                . '&field=' . Escape::EscapeEntities($_GET["field"]) . '&sort=' . Escape::EscapeEntities($_GET["sort"])
                . '&page=' . ($page+1) . '&fromsearch=' . $fromsearch
                . ($LinkMode ? "&link=1" : "") . ($query_recordid ? "&query=$query_recordid" : "")
                . (bYN(GetParm('CSRF_PREVENTION','N')) ? "&token=".CSRFGuard::getCurrentToken() : ""));

                $nextPageLink = '<a href="' . $nextPageUrl . '"><b>'._tk('next_page').'</b></a>';
            }
?>
        <tr>
        <td class="windowbg" align="left" nowrap="nowrap" >
            <?= $prevPageLink ?>
        </td>
        <td colspan="<?= ($list_columns_count-2) ?>">
<?php

        $tmplistnum = $overralCount;

        $pagenumber = ($_GET['page'] ? $_GET['page'] : 1);

        $CurrentPage = $pagenumber;
        if($listdisplay != 0)
        {
            $maxpagenum = min(array($pagenumber + 10, floor($overralCount/$listdisplay)));
        }
        if($pagenumber > 10)
        {
            $pagenumber = $pagenumber - 10;
        }
        else
        {
            $pagenumber = 1;
        }
        if ($CurrentPage > 11)
        {
            $backwardsURL = Sanitize::SanitizeURL($scripturl . '?action=listcontacts&match=' . $MatchLetter
            . '&field=' . Escape::EscapeEntities($_GET["field"]) . '&sort=' . Escape::EscapeEntities($_GET["sort"])
            . '&page=' . ($pagenumber) . '&fromsearch=' . $fromsearch
            . ($LinkMode ? "&link=1" : "") . ($query_recordid ? "&query=$query_recordid" : "")
            . (bYN(GetParm('CSRF_PREVENTION','N')) ? "&token=".CSRFGuard::getCurrentToken() : ""));

            echo '<a href="'.$backwardsURL.'"><b><< </b></a>';
        }

        while($pagenumber <= $maxpagenum)
        {
            $pageURL = Sanitize::SanitizeURL($scripturl . '?action=listcontacts&match=' . $MatchLetter
            . '&field=' . Escape::EscapeEntities($_GET["field"]) . '&sort=' . Escape::EscapeEntities($_GET["sort"])
            . '&page=' . ($pagenumber) . '&fromsearch=' . $fromsearch
            . ($LinkMode ? "&link=1" : "") . ($query_recordid ? "&query=$query_recordid" : "")
            . (bYN(GetParm('CSRF_PREVENTION','N')) ? "&token=".CSRFGuard::getCurrentToken() : ""));

            echo '<a href="'.$pageURL.'">'. ($pagenumber == $CurrentPage ? '<b>' . $pagenumber . '</b>' : $pagenumber) . ' </a>';

            $pagenumber = $pagenumber + 1;
        }
        if ($listdisplay != 0 && $pagenumber < floor($overralCount/$listdisplay))
        {
            $forwardsURL = Sanitize::SanitizeURL($scripturl . '?action=listcontacts&match=' . $MatchLetter
            . '&field=' . Escape::EscapeEntities($_GET["field"]) . '&sort=' . Escape::EscapeEntities($_GET["sort"])
            . '&page=' . ($pagenumber) . '&fromsearch=' . $fromsearch
            . ($LinkMode ? "&link=1" : "") . ($query_recordid ? "&query=$query_recordid" : "")
            . (bYN(GetParm('CSRF_PREVENTION','N')) ? "&token=".CSRFGuard::getCurrentToken() : ""));
                echo '<a href="'.$forwardsURL.'"><b>>> </b></a>';
        }
        echo '</td>';

?>
        </td>
        <td class="windowbg" align="right">
            <?= $nextPageLink ?>
        </td>
        </tr>
<?php
        }

        echo '<tr class="tableHeader">';

        if (is_array($list_columns))
        {
            // Take into account extra column
            if ($LinkMode)
            {
                echo '<th class="windowbg" width="6%" align="center">Choose</th>';
            }
            else if($_SESSION["CON"]["DUPLICATE_SEARCH"]) //flag for duplicate searching
            {
                echo '<th class="windowbg" width="2%" align="center">';

                if(count($_SESSION['CON']['RECORDLIST']->FlaggedRecords) == count($_SESSION['CON']['RECORDLIST']->Records))
                {
                    echo '<img src="Images/flag_filled_list_header.png" ';
                }
                else
                {
                    echo '<img src="Images/flag_unfilled_list_header.png" ';
                }

                echo '
                id="record_flag_all" class="record_flag_list_header" style="cursor:pointer"
                onclick="
                    if(jQuery(\'#record_flag_all\').attr(\'src\')==\'Images/flag_unfilled_list_header.png\')
                    {
                        FlagAllRecords(\'CON\');
                        jQuery(\'.record_flag\').attr(\'src\', \'Images/flag_filled.png\')
                        jQuery(\'.record_flag_list_header\').attr(\'src\', \'Images/flag_filled_list_header.png\')
                    }
                    else
                    {
                        UnFlagAllRecords(\'CON\');
                        jQuery(\'.record_flag\').attr(\'src\',\'Images/flag_unfilled.png\')
                        jQuery(\'.record_flag_list_header\').attr(\'src\',\'Images/flag_unfilled_list_header.png\')
                    }
                ">';


                echo '</th>';
            }

            foreach ($list_columns as $col_field => $col_info)
            {
                echo '<th class="windowbg"' . ($col_info['width'] ? 'width="' . $col_info['width'] . '%"' : '') . '>';
                if ($FieldDefs[$module][$col_field]['Type'] != 'textarea' && in_array($col_field, $ModuleDefs[$module]['FIELD_ARRAY']) || $col_field == 'recordid')
                {
                    echo '<a href="' . "{$scripturl}?action=listcontacts&match={$MatchLetter}"
                    . '&field='.$col_field.'&sort=' . ($_GET['field'] == $col_field && $sort == 'asc' ? 'desc' : 'asc')
                    . "&page=1&fromsearch={$fromsearch}" . ($LinkMode ? "&link=1" : "")
                    . ($query_recordid ? "&query=$query_recordid" : "") . '">';
                }
                if($col_field == "mod_title")
                {
                    $currentCols["mod_title"] = "Module";
                }
                elseif($col_field == "recordid")
                {
                    if($col_info_extra['colrename'])
                    {
                        $currentCols[$col_field] = $col_info_extra['colrename'];
                    }
                    else
                    {
                        $currentCols[$col_field] = GetColumnLabel($col_field, $FieldDefsExtra[$module][$col_field]["Title"], false, '', 'CON');
                    }
                }
                else
                {
                    $currentCols[$col_field] = GetColumnLabel($col_field, $FieldDefsExtra[$module][$col_field]["Title"], false, '', 'CON');
                }
                echo '<b>' . $currentCols[$col_field] . '</b>';
                if ($FieldDefs[$module][$col_field]['Type'] != 'textarea' && in_array($col_field, $ModuleDefs[$module]['FIELD_ARRAY']) || $col_field == 'recordid')
                {
                    echo '</a>';
                }
                echo '</th>';
            }
        }

		echo '</tr>';
	}

    //Retrieve list of already-linked contacts which cannot thus be re-linked.
    $DisallowedContacts = GetDisallowedContacts($_SESSION['CON']['LINK_DETAILS']);

    if ($overralCount == 0 && $LinkMode && bYN(GetParm('EXTERNAL_CONTACTS', 'N')) && !isset($GLOBALS['external_contact_address']))
    {
        $client = new SoapClient($_SESSION["CON"]["EXTERNAL_WSDL"], array("trace"=> 1,"exceptions"=> 0));
        $contactdetails = ($client->getContact($_SESSION["login"], "", $_SESSION["session_id"], $_SESSION["CON"]["EXTERNAL_WHERE"]));
        //$contactdetails = ($client->getContact($ContactsWhere));

        require_once 'soap/interfaceclass.php';

        $DatixInterface = new DatixInterface();
        $DatixInterface->LoadXMLDoc($contactdetails);
        $DatixInterface->parseXMLDataToArray($request);

        if (is_array($request))
        {
            foreach ($request as $row)
            {
                echo '<tr class="listing-row">';

                $row = ProcessHTMLEntities($row);
                $fullname = $row['fullname'];

                echo '<td class="windowbg2">'
                . '<input type="button" value="Choose" onclick="opener.location.href=\''
                . $scripturl
                . $_SESSION["CON"]["LINK"]["URL"]
                . "&external=1&from_contact_match=1&contact_match_link_id=".$_SESSION["CON"]["LINK"]["LINK_RECORDID"]["VALUE"]
                . "&". Escaper::escapeForHTMLParameter($_SESSION["CON"]["EXTERNAL_KEY"]) . "=" . Escaper::escapeForHTMLParameter($row[$_SESSION["CON"]["EXTERNAL_KEY"]])
                . ($_REQUEST['fromsearch'] ? "&fromsearch=1" : '')
                . "&token=".CSRFGuard::getCurrentToken()
                . '\';window.open(\'\', \'_self\');window.close();" /></td>';

                if (!empty($row["con_dob"]))
                {
                    $row["con_dob"] = date('d M Y H:i:s',strtotime(str_replace("T", " ", $row["con_dob"])));
                }

                foreach ($list_columns as $col_field => $col_info)
                {
                    if ($FieldDefs[$module][$col_field]['Type'] == 'ff_select')
                    {
                        $codeinfo = get_code_info($module, $col_field, $row[$col_field]);

                        $colour = '';

                        if ($codeinfo["cod_web_colour"])
                        {
                            $colour = $codeinfo["cod_web_colour"];
                        }

                        if ($colour)
                        {
                            echo '<td valign="left" style="background-color:#' . htmlspecialchars($colour). '"';
                        }
                        else
                        {
                            echo '<td class="windowbg2" valign="top"';
                        }
                    }
                    else
                    {
                        echo '<td class="windowbg2" valign="top"';
                    }

                    if ($col_info_extra['dataalign'])
                    {
                         echo ' align="' . $col_info_extra['dataalign'] . '"';
                    }

                    echo '>';

                    //extra field hack
                    if (preg_match('/^UDF.*_([0-9]+)$/', $col_field, $matches))
                    {
                        echo $udfData[$row['recordid']][$matches[1]];
                    }
                    else if ($col_field == 'recordid')
                    {
                        if ($col_info_extra['prefix'])
                        {
                            echo Escape::EscapeEntities($col_info_extra['prefix']);
                        }
                        echo Escaper::escapeForHTML($row[$col_field]);
                    }
                    else if($col_field == 'rep_approved')
                    {
                        $codeinfo = get_code_info('INC', $col_field, $row[$col_field]);
                        echo Escape::EscapeEntities(FirstNonNull(array(_tk('approval_status_CON_'.$row[$col_field]), $codeinfo['description'])));
                    }
                    elseif ($FieldDefs[$module][$col_field]['Type'] == 'ff_select')
                    {
                        $codeinfo = get_code_info($module, $col_field, $row[$col_field]);
                        echo Escape::EscapeEntities($codeinfo['description']);
                    }
                    elseif ($FieldDefs[$module][$col_field]['Type'] == 'date')
                    {
                        echo FormatDateVal($row[$col_field]);
                    }
                    elseif ($FieldDefs[$module][$col_field]['Type'] == 'textarea')
                    {
                        echo Escaper::escapeForHTML($row[$col_field]);
                    }
                    elseif ($FieldDefs[$module][$col_field]['Type'] == 'time')
                    {
                        if (\UnicodeString::strlen($row[$col_field]) == 4)
                            $row[$col_field] = $row[$col_field]{0} . $row[$col_field]{1} . ":" . $row[$col_field]{2} . $row[$col_field]{3};

                        echo Escaper::escapeForHTML($row[$col_field]);
                    }
                    else
                    {
                        echo Escaper::escapeForHTML($row[$col_field]);
                    }

                    echo '</td>';
                }

                echo '</tr>';
                $overralCount++;
                $actualCount++;
            }
        }
    }
    else
    {
        foreach ($data as $key => $row)
        {
            echo '<tr class="listing-row">';
            $count = count($row);
            for($i = 0; $i < $count; $i++)
            {
                if($FieldDefsExtra['CON'][$key]['Type'] != 'date')
                {
                    $row[$i] = \src\security\Escaper::escapeForHTML($row[$i]);
                }
            }
            $fullname = $row['fullname'];

            if ($LinkMode)
            {
                if(is_array($DisallowedContacts) && in_array($row['recordid'], $DisallowedContacts))
                {
                    // This contact is already linked with the relevent link_type, and so cannot be re-selected to link again.
                    echo '<td class="windowbg2" style="font-color:grey">'
                    . '<i>Link already exists</i></td>';
                }
                else
                {
                    echo '<td class="windowbg2">'
                    . '<input type="button" value="Choose" onclick="opener.location.href=\''
                    . $scripturl
                    . $_SESSION["CON"]["LINK"]["URL"]
                    . "&from_contact_match=1&contact_match_link_id=".$_SESSION["CON"]["LINK"]["LINK_RECORDID"]["VALUE"]
                    . "&{$_SESSION[CON][LINK][MAIN_ID][NAME]}={$row[recordid]}"
                    . ($_REQUEST['fromsearch'] ? "&fromsearch=1" : '')
                    . "&token=".CSRFGuard::getCurrentToken()
                    . '\';window.open(\'\', \'_self\');window.close();" /></td>';
                }
            }
            else if($_SESSION["CON"]["DUPLICATE_SEARCH"])
            {

                $CurrentIndex = $_SESSION['CON']['RECORDLIST']->getRecordIndex(array('recordid' => $row['recordid']));

                // flag for duplicate searching.
                echo '<td class="windowbg2">';

                if($_SESSION['CON']['RECORDLIST']->FlaggedRecords[$CurrentIndex])
                {
                    echo '<img src="Images/flag_filled.png" ';
                }
                else
                {
                    echo '<img src="Images/flag_unfilled.png" ';
                }

                echo '
                id="record_flag_'.$row['recordid'].'" class="record_flag" style="cursor:pointer"
                onclick="
                    if(jQuery(\'#record_flag_'.$row['recordid'].'\').attr(\'src\')==\'Images/flag_unfilled.png\')
                    {
                        FlagRecord(\'CON\', '.$row['recordid'].');
                        jQuery(\'#record_flag_'.$row['recordid'].'\').attr(\'src\', \'Images/flag_filled.png\')
                    }
                    else
                    {
                        UnFlagRecord(\'CON\', '.$row['recordid'].');
                        jQuery(\'#record_flag_'.$row['recordid'].'\').attr(\'src\',\'Images/flag_unfilled.png\')
                    }
                ">';
            }

            $url = "{$scripturl}?action=editcontact&recordid={$row[recordid]}";

            foreach ($list_columns as $col_field => $col_info)
            {
                if ($FieldDefs[$module][$col_field]['Type'] == 'ff_select')
                {
                    $codeinfo = get_code_info($module, $col_field, $row[$col_field]);

                    $colour = '';

                    if ($codeinfo["cod_web_colour"])
                    {
                        $colour = $codeinfo["cod_web_colour"];
                    }

                    if ($colour)
                    {
                        echo '<td valign="left" style="background-color:#' . Escape::EscapeEntities($colour). '"';
                    }
                    else
                    {
                        echo '<td class="windowbg2" valign="top"';
                    }
                }
                else
                {
                    echo '<td class="windowbg2" valign="top"';
                }

                if ($col_info_extra['dataalign'])
                {
                     echo ' align="' . $col_info_extra['dataalign'] . '"';
                }

                echo '>';

                if (!$PrintMode && !$LinkMode && $row[$col_field] || (preg_match('/^UDF.*_([0-9]+)$/', $col_field, $matches) && $udfData[$row['recordid']][$matches[1]]))
                    echo '<a href="' . $url . '">';

                //extra field hack
                if (preg_match('/^UDF.*_([0-9]+)$/', $col_field, $matches))
                {
                    echo $udfData[$row['recordid']][$matches[1]];
                }
                else if ($col_field == 'recordid')
                {
                    if ($col_info_extra['prefix'])
                        echo $col_info_extra['prefix'];
                    echo $row[$col_field];
                }
                else if($col_field == 'rep_approved')
                {
                    $codeinfo = get_code_info('CON', $col_field, $row[$col_field]);
                    echo FirstNonNull(array(_tk('approval_status_CON_'.$row[$col_field]), $codeinfo['description']));
                }
                elseif ($FieldDefs[$module][$col_field]['Type'] == 'ff_select')
                {
                    $codeinfo = get_code_info($module, $col_field, $row[$col_field]);
                    echo Escape::EscapeEntities($codeinfo['description']);
                }
                elseif ($FieldDefs[$module][$col_field]['Type'] == 'date')
                {
                    echo FormatDateVal($row[$col_field]);
                }
                elseif ($FieldDefs[$module][$col_field]['Type'] == 'textarea')
                {
                    echo $row[$col_field];
                }
                elseif ($FieldDefs[$module][$col_field]['Type'] == 'time')
                {
                    if (\UnicodeString::strlen($row[$col_field]) == 4)
                        $row[$col_field] = $row[$col_field]{0} . $row[$col_field]{1} . ":" . $row[$col_field]{2} . $row[$col_field]{3};

                    echo $row[$col_field];
                }
                else
                {
                    echo $row[$col_field];
                }

                if (!$PrintMode && !$LinkMode && $row[$col_field] || (preg_match('/^UDF.*_([0-9]+)$/', $col_field, $matches) && $udfData[$row['recordid']][$matches[1]]))
                    echo '</a>';
                echo '</td>';
            }

            echo '</tr>';
        }
    }


    if ($overralCount != 0  || (bYN(GetParm('EXTERNAL_CONTACTS', 'N')) && !isset($GLOBALS['external_contact_address'])))
    {
?>
        <tr>
        <td class="windowbg" align="left" nowrap="nowrap" colspan="<?= ($list_columns_count-1) ?>">
            <?= $prevPageLink ?>
        </td>
        <td class="windowbg" align="right">
            <?= $nextPageLink ?>
        </td>
        </tr>
<?php
    }

    if (!$LinkMode && $query_recordid == "" && $fromsearch && (bYN(GetParm("SAVED_QUERIES", "Y")))) {
        echo '<tr>
                  <td class="windowbg2" colspan="'. $list_columns_count .'">
                      <a href="' . $scripturl . '?action=savequery&module=CON&form_action=new"><b>Save the current search as a query.</b></a>
                  </td>
              </tr>';
    }

?>
	<tr>
    	<td class="button_wrapper" align="center" colspan="<?=$list_columns_count?>">
        	<?php
			if ($LinkMode)
            {
				echo '<input type="button" value="'._tk('btn_cancel').'" onClick="window.open(\'\', \'_self\');window.close()">';
			}
			elseif ($fromsearch)
            {
				echo'<table><tr>';

				if ($_SESSION["FROMMODULE"] != "" and $overralCount > 0)
                {
					if ($_SESSION["FROMMODULE"] == "SAB")
                    {
						$contactlinkaction = "sabscontactlinkaction";
					}
					elseif ($_SESSION["FROMMODULE"] == "DST")
                    {
						$contactlinkaction = "distrcontactlinkaction";
					}

					echo '<td><form method="post" name="frmBatchLink" action="' . $scripturl
					. '?action=' . $contactlinkaction
					. '&' . $ModuleDefs[$_SESSION["FROMMODULE"]]["FK"] . '=' . $_SESSION[$_SESSION["FROMMODULE"]]["CURRENTID"]
					. '&formtype=Main&linktype=' . $_SESSION[$_SESSION["FROMMODULE"]]["CURRENTLINKTYPE"]
					. '"><input type="hidden" name="form_action" value="batch" />'
					. '<input type="submit" value="Link contacts (' . $overralCount . ') to '
					. $ModuleDefs[$_SESSION["FROMMODULE"]]["REC_NAME"] .'"  /></form></td>';
				}

				if ($_SESSION["FROMMODULE"] != "")
                {
                    echo '<td><form method="post" action="' . $scripturl . '?action=contactssearch&module=CON&from_module='.$_SESSION["FROMMODULE"].'&'.$ModuleDefs[$_SESSION["FROMMODULE"]]['FK'].'='.$_SESSION[$_SESSION["FROMMODULE"]]["CURRENTID"].'&searchtype=lastsearch'
                    . '" name="contactaction"><input type="submit" value="'._tk('btn_back').'" /></form></td>';

					echo '<td><form method="post" action="' . $scripturl . '?action='.$ModuleDefs[$_SESSION["FROMMODULE"]]['ACTION']
					. '&recordid=' . $_SESSION[$_SESSION["FROMMODULE"]]["CURRENTID"]
					. '&panel=' . $_SESSION[$_SESSION["FROMMODULE"]]["CURRENTPANEL"]
					. '"><input type="submit" value="'._tk('back_to').$ModuleDefs[$_SESSION["FROMMODULE"]]['REC_NAME'].'" /></form></td>';
				}
				else
                {
                    if ($_SESSION['CON']['DUPLICATE_SEARCH'] === true)
                    {
                        echo '<td><input type="button" onclick="SendTo(\'?service=duplicatesearch&event=performsearch\');" value="'._tk('btn_back').'"></td>';
                    }
                    else
                    {
                        echo '<td><form method="post" action="' . $scripturl . '?action=contactssearch&module=CON&searchtype=lastsearch'
                            . '" name="contactaction"><input type="submit" value="'._tk('btn_back').'" /></form></td>';
                    }

					echo '<td><form method="post" action="'.$scripturl.'?module=CON">'
					. '<input type="submit" value="'._tk('btn_cancel').'" /></form></td>';
				}

                echo '</tr></table>';
            }
			else
            {
				echo '<form method="post" action="'.$scripturl.'?action=newcontact" name="contactaction">'
				. '<input type="submit" value="'._tk('add_new_contact').'" /></form>';
			}
?>
		</td>
	</tr>
</table>
<?php
if($dtxdebug){
    $mtime = explode(' ', microtime());
    $totaltime = $mtime[0] + $mtime[1] - $starttime;
    printf('Page loaded in %.3f seconds.', $totaltime);
}
?>

<?php
	if($LinkMode)
    {
        echo getBasicFooter();
    }
    else
    {
        footer();
    }

	obExit();
}

