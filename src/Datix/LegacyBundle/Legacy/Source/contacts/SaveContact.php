<?php

function SaveLinkedContact($aParams)
{
    global $ModuleDefs, $formlevel;

    $Suffix = $aParams['suffixstring'];

    $aParams['data'] = QuotePostArray($_POST);  //needed for one of the  PRE_LINK_CONTACT_SAVE_FUNCTIONS functions

    $formlevel = $formlevel ? $formlevel : $aParams['formlevel'];

    // run pre link hooks
    runPreLinkContactSaveFunctions($aParams);

    //needs to be after the PRE_LINK_CONTACT_SAVE_FUNCTIONS are run because they might change $_POST values.
    $data = $_POST;

    $ContactErrors = array_merge(ValidateContactData(array('data' => $data)), ValidateInjuryData(array('data' => $data, 'suffixstring' => $Suffix)));

    if (!empty($ContactErrors))
    {
        $error['Validation'] = $ContactErrors;
    }

    if ($error)
    {
        $con = $_POST;
        $con['recordid'] = Sanitize::SanitizeInt($con['recordid']); //Santizing int here b/c it does not corrupt the value if it's sanitized multiple times

        $con['error'] = $error;

        //Need to reformat dates to ensure they are in SQL format to be interpreted when reloading the form.
        $aDateValues = CheckDatesFromArray(GetAllFieldsByType('CON', 'date'), $con);
        $con = array_merge($con, Sanitize::SanitizeStringArray($aDateValues)); //Sanitizing $aDateValues here because these values are not sanitized later, and the FILTER_SANITIZE_STRING will not affect a string, as formed by UserDateToSQLDate()

        // Workaround to convert dates to SQL format
        // TODO: This needs to be removed when refactored
        $ContactDateFields = array_merge(GetAllFieldsByType('CON', 'date'), getAllUdfFieldsByType('D', '', $con));//No filtering done here because this is effectively filtering by a whitelist

        foreach ($ContactDateFields as $ContactDate)
        {
            if (array_key_exists($ContactDate, $con) && $con[$ContactDate] != '')
            {
                $con[$ContactDate] = UserDateToSQLDate($con[$ContactDate]);

                // Some extra field dates look directly at the post value, so need to blank it out here.
                unset($_POST[$ContactDate]);
                // In case on contacts we need also to unset the session values
                if ($_SESSION['CON']['LINK_DETAILS'][$ContactDate])
                {
                    unset($_SESSION['CON']['LINK_DETAILS'][$ContactDate]);
                }
            }
        }

        ShowContactForm($con, $aParams['main_recordid'], $error);
        obExit();
    }

    //PostedContact is the array of contact data to be saved, without suffix.
    //It is used for the pure contact data (not link data) to simplify merging.
    $PostedContact = GetContactArray($data, $Suffix);

    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'CON', 'level' => $formlevel));
    $FormDesign->LoadFormDesignIntoGlobals();
    
    $data = ParseSaveData(array('module' => 'CON', 'data' => $data, 'suffix' => $Suffix));

    if ($_POST['show_section_injury'] != '1')
    {
        $data['link_treatment'] = '';
    }

    //NHS Number should not be saved with spaces in: causes inconsistencies with the RC.
    $data['con_nhsno'] = str_replace(' ', '', $data['con_nhsno']);

    if (!$data['con_recordid'.$Suffix])
    {
        //this is a new contact record.

        //the MergeContact function will alter the $PostedContact array to include database data where appropriate.
        if (!MergeContact($PostedContact))
        {
            //if we are here, no match has been found
            $PostedContact = GetContactArray($data, $Suffix);
            $PostedContact['recordid'] = GetNextRecordid('contacts_main', true);
        }
    }
    else
    {
        $PostedContact = GetContactArray($data, $Suffix);
        $PostedContact['recordid'] = $data['con_recordid'.$Suffix];
    }

    $aParams['recordid'] = $PostedContact['recordid'];
    $aParams['data'] = $data;

    if (!$PostedContact['rep_approved'] || $PostedContact['rep_approved'] == 'NEW')
    {
        $PostedContact['rep_approved'] = ($_SESSION['logged_in'] && !$data['holding_form'] ? 'FA' : 'UN');
    }

    // update contact changes
    updateContactsMain($PostedContact, $Suffix);

    $link_age = Sanitize::SanitizeString($_POST["link_age".$Suffix]);

    if (!empty($_POST["con_dob".$Suffix]) )
    {
       $link_age = getAgeAtDateForModule($aParams['module'], $aParams['main_recordid'], UserDateToSQLDate($_POST["con_dob".$Suffix]), $link_age);
    }

    if (!empty($link_age))
    {
        $data["link_age_band".$Suffix] = getAgeBand($link_age);
    }

    if ($link_age === "")
    {
        $link_age = 'NULL';
    }

    $data["link_age".$Suffix] = $link_age;

    if ($_POST['link_exists'.$Suffix])
    {
        updateContactLink($aParams, $Suffix, $data, $PostedContact);
    }
    else
    {
        if ($_POST['contact_match_link_id'])
        {
            $LinkArray = getContactLinkFields($data['link_type']);

            //need to make sure that link data is not lost, so as long as it hasn't been altered on the form, pick up all the link fields.
            if ($data['link_type'] == 'O') {
                $sql = 'SELECT '.implode(', ',$LinkArray).' FROM link_respondents WHERE recordid = '.Sanitize::SanitizeInt($_POST['contact_match_link_id']);
            } else {
                $sql = 'SELECT '.implode(', ',$LinkArray).' FROM link_contacts WHERE link_recordid = '.Sanitize::SanitizeInt($_POST['contact_match_link_id']);
            }

            $row = DatixDBQuery::PDO_fetch($sql);

            foreach ($LinkArray as $field)
            {
                if (!$data[$field])
                {
                    $linkdata[$field] = $row[$field];
                }
            }

            $data = array_merge($data, $linkdata);

            if ($aParams['module'] == 'COM')
            {
                //need to make sure that link_compl data is not lost, so change the con_id to refer to the new contact.
                $sql = 'SELECT link_contacts.con_id, '.implode(', ',GetComplaintDateFields()).', lcom_iscomplpat, lcom_primary FROM link_compl, link_contacts WHERE link_compl.com_id = '.$aParams['main_recordid'].' AND link_contacts.con_id = link_compl.con_id AND link_contacts.link_recordid = :contact_match_link_id';
                $row = DatixDBQuery::PDO_fetch($sql, array("contact_match_link_id" => $_POST['contact_match_link_id']));

                $sql = 'UPDATE link_compl SET con_id = :new_con_id WHERE con_id = :con_id AND com_id = :com_id';
                DatixDBQuery::PDO_query($sql, array( "new_con_id" => $PostedContact['recordid'], "con_id" => $row['con_id'], "com_id" => $aParams['main_recordid']));
            }
        }

        if ($aParams['module'] == 'COM')
        {
             //Check if link exists before inserting new link_contacts record.
            $sql = 'SELECT COUNT(*) as check_link_exists FROM link_contacts WHERE com_id = :com_id AND link_contacts.con_id =:con_id AND link_contacts.link_type= :link_type';
            $row = DatixDBQuery::PDO_fetch($sql, array("com_id" => $aParams['main_recordid'], "con_id" => $PostedContact['recordid'], "link_type" => $aParams['data']['link_type'.$Suffix]));
        }
        else
        {
            // this is stupid, but seems like the least-risky way of fixing this...
            $row["check_link_exists"] = 0;
        }

        if ($row["check_link_exists"] == 0)
        {
            insertContactLink($aParams, $data, $PostedContact, $Suffix);
        }
    }
    
    if ($formlevel == '1' || $data['show_section_injury'] !== null) // prevent injury details being lost if the contact is linked again (e.g. as a different type) using a form design with no injuries section
    {
        SaveInjuries($aParams);
    }


    // save stuff for INC module
    require_once 'Source/incidents/PersonalProperty.php';
    SaveProperty($aParams);


    // if extra hooks present, call them
    runExtraRecordDataFunctions($aParams);


    require_once "Source/libs/UDF.php";
    SaveUDFs($PostedContact['recordid'],MOD_CONTACTS,$aParams['suffix']);


    // Save Progress Notes
    $loader = new src\framework\controller\Loader();
    $controller = $loader->getController(array(
        'controller' => 'src\progressnotes\controllers\ProgressNotesController'
    ));
    $controller->setRequestParameter('module', 'CON');
    $controller->setRequestParameter('data', $aParams['data']);
    echo $controller->doAction('saveProgressNotes');


    //delete matched contact if appropriate.
    if ($_POST['from_contact_match'] && $_POST['contact_match_link_id'])
    {
        $con_id = getContactIdFromLink($_POST['contact_match_link_id']);

        // if extra hooks present, call them
        runExtraContactUnlinkFunctions($aParams, $con_id);

        $sql = 'DELETE FROM contacts_main WHERE recordid = :recordid AND rep_approved != \'FA\'';
        DatixDBQuery::PDO_query($sql, array("recordid" => $con_id));

        $sql = 'DELETE FROM link_contacts WHERE con_id = :con_id';
        DatixDBQuery::PDO_query($sql, array("con_id" => $con_id));

        if ($aParams['module'] == 'INC')
        {
            $sql = 'DELETE FROM inc_injuries WHERE con_id = :con_id';
            DatixDBQuery::PDO_query($sql, array("con_id" => $con_id));
        }
    }

    // if post hooks present, call them
    runPostLinkSaveFunctions($aParams);
}

function updateContactsMain($PostedContact, $Suffix)
{
    global $ModuleDefs;

    $PDOParamsArray = array ();

    $sql = "UPDATE contacts_main SET " . GeneratePDOSQLFromArrays(
            array(
                'FieldArray' => $ModuleDefs['CON']['FIELD_ARRAY'],
                'DataArray' => $PostedContact,
                'Module' => 'CON',
                'end_comma' => true
            ),
            $PDOParamsArray
        );

    $PDOParamsArray["updatedby"]   = $_SESSION["initials"];
    $PDOParamsArray["updateid"]    = GensUpdateID($_POST["updateid" . $Suffix]);
    $PDOParamsArray["updateddate"] = date('d-M-Y H:i:s');

    $sql .= " updatedby = :updatedby, updateid = :updateid, updateddate = :updateddate";
    $sql .= " WHERE recordid = " . $PostedContact['recordid'];

    if ($_POST["updateid" . $Suffix]) {
        $PDOParamsArray["old_updateid"] = $_POST["updateid" . $Suffix];
        $sql .= " AND (updateid = :old_updateid)";
    } else {
        $sql .= " AND (updateid IS NULL OR updateid = '')";
    }

    if (!DatixDBQuery::PDO_query($sql, $PDOParamsArray)) {
        fatal_error("Cannot insert contact data");
    }
}

function runPreLinkContactSaveFunctions($aParams)
{
    global $ModuleDefs;

    if (is_array($ModuleDefs[$aParams['module']]['PRE_LINK_CONTACT_SAVE_INCLUDES'])) {
        foreach ($ModuleDefs[$aParams['module']]['PRE_LINK_CONTACT_SAVE_INCLUDES'] as $includeFile) {
            require_once $includeFile;
        }
    }

    if (is_array($ModuleDefs[$aParams['module']]['PRE_LINK_CONTACT_SAVE_FUNCTIONS'])) {
        foreach ($ModuleDefs[$aParams['module']]['PRE_LINK_CONTACT_SAVE_FUNCTIONS'] as $func) {
            $aParams['data'] = QuotePostArray($_POST);
            $func($aParams);
        }
    }
}

function runPostLinkSaveFunctions($aParams)
{
    global $ModuleDefs;

    if (is_array($ModuleDefs[$aParams['module']]['POST_LINK_CONTACT_SAVE_INCLUDES'])) {
        foreach ($ModuleDefs[$aParams['module']]['POST_LINK_CONTACT_SAVE_INCLUDES'] as $includeFile) {
            require_once $includeFile;
        }
    }

    if (is_array($ModuleDefs[$aParams['module']]['POST_LINK_CONTACT_SAVE_FUNCTIONS'])) {
        foreach ($ModuleDefs[$aParams['module']]['POST_LINK_CONTACT_SAVE_FUNCTIONS'] as $func) {
            $aParams['data'] = QuotePostArray($_POST);
            $func($aParams);
        }
    }
}

function getContactIdFromLink($link_recordid)
{
    $sql = 'SELECT con_id FROM link_contacts WHERE link_recordid = :link_recordid';
    $row = DatixDBQuery::PDO_fetch($sql, array("link_recordid" => $link_recordid));
    return $row['con_id'];
}

function runExtraContactUnlinkFunctions($aParams, $con_id)
{
    global $ModuleDefs;

    if (is_array($ModuleDefs[$aParams['module']]['EXTRA_CONTACT_UNLINK_FUNCTIONS'])) {
        foreach ($ModuleDefs[$aParams['module']]['EXTRA_CONTACT_UNLINK_FUNCTIONS'] as $func) {
            $func(array('data' => array('main_recordid' => $aParams['main_recordid'], 'con_recordid' => $con_id)));
        }
    }
}

function insertContactLink($aParams, $data, $PostedContact, $Suffix = '')
{
    global $ModuleDefs;

    // log if we are going to save with link_type = W, since this should be possible and we can't work out why it sometimes happens.
    if ($data['link_type'.$Suffix] == 'W')
    {
        \src\framework\registry\Registry::getInstance()->getLogger()->logEmergency('Link contact saved with link_type = "W". User='.$_SESSION['login'].',Request='.$_SERVER['REQUEST_URI'].', $_POST='.var_dump($_POST)); //Not going to sanitize this as it goes straight into logs, but theres an option to do something to make sure it behaves it's self
    }

    $PDOInsertParams = array ();

    $LinkArray = getContactLinkFields($data['link_type'.$Suffix]);

    if ($data['link_type'.$Suffix] == 'O') // individual respondents
    {
        $tmpdata = $data;
        $tmpdata['link_type'.$Suffix] = 'CON';
        $tmpdata['main_recordid'.$Suffix] = $aParams['main_recordid'];
        $tmpdata['con_id'.$Suffix] = $aParams['recordid'];

        if ($tmpdata['con_id'.$Suffix] == null || $tmpdata['con_id'.$Suffix] == '')
        {
            $tmpdata['con_id'] = $aParams['recordid'];
        }

        $sql = "INSERT INTO link_respondents " . GeneratePDOInsertSQLFromArrays(
            array (
                'FieldArray' => $LinkArray,
                'DataArray'  => $tmpdata,
                'Module'     => 'CON',
                'Suffix'     => $aParams['suffix']
            ),
            $PDOInsertParams
        );
    }
    else
    {
        // NOTE: DO NOT INCLUDE the updatedby column in any INSERT
        // or UPDATE statement for the table link_contacts.  This is
        // because in some older versions of the database, this column
        // is incorrectly typed as datetime.

        $sql = "INSERT INTO link_contacts " . GeneratePDOInsertSQLFromArrays(
            array(
                'FieldArray' => $LinkArray,
                'DataArray'  => $data,
                'Module'     => 'INC', //linked fields are stored under INC
                'Suffix'     => $aParams['suffix'],
                'Extras'     => array('con_id' => $PostedContact['recordid'], $ModuleDefs[$aParams['module']]['FK'] => $aParams['main_recordid'])
            ),
            $PDOInsertParams
        );
    }

    if (!DatixDBQuery::PDO_query($sql, $PDOInsertParams)) {
        fatal_error("Cannot insert contact link data");
    }

    AuditContactLink($aParams['module'], $aParams['main_recordid'], $PostedContact['recordid'], $data['link_type_' . $aParams['suffix']]);
}

function updateContactLink($aParams, $Suffix, $data, $PostedContact)
{
    // log if we are going to save with link_type = W, since this should be possible and we can't work out why it sometimes happens.
    if ($data['link_type'.$Suffix] == 'W')
    {
        \src\framework\registry\Registry::getInstance()->getLogger()->logEmergency('Link contact saved with link_type = "W". User='.$_SESSION['login'].',Request='.$_SERVER['REQUEST_URI'].', $_POST='.var_dump($_POST));
    }

    $link_recordid = Sanitize::SanitizeInt($_POST['link_recordid' . $Suffix]);

    $bindParams = array ();

    $LinkArray = getContactLinkFields($data['link_type'], true);

    if ($data['link_type'] == 'O' && in_array('main_recordid', $LinkArray) && $aParams['data']['link_recordid'] != null)
    {
        unset($LinkArray[array_search('main_recordid',$LinkArray)]);
    }

    $UpdateSetLink = GeneratePDOSQLFromArrays(
        array(
            'FieldArray' => $LinkArray,
            'DataArray' => $data,
            'Module' => 'INC', //linked fields are stored under INC
            'Suffix' => $aParams['suffix'],
            'end_comma' => true
        ),
        $bindParams
    );

    if ($data['link_type'] == 'O') // individual respondents
    {
        $bindParams['link_type'] = 'CON';

        $sql = "UPDATE link_respondents SET $UpdateSetLink
                    updateddate = '" . date('Y-m-d H:i:s') . "'
                    WHERE recordid = $link_recordid";
        DatixDBQuery::PDO_query($sql, $bindParams);

        DoFullAudit('CON', 'contacts_main', $PostedContact['recordid'], $data, "recordid = $link_recordid");
    } else {
        // NOTE: DO NOT INCLUDE the updatedby column in any INSERT
        // or UPDATE statement for the table link_contacts.  This is
        // because in some older versions of the database, this column
        // is incorrectly typed as datetime.
        $sql = "UPDATE link_contacts
                    SET $UpdateSetLink
                    updateddate = '" . date('Y-m-d H:i:s') . "'
                    WHERE link_recordid = $link_recordid";
        DatixDBQuery::PDO_query($sql, $bindParams);

        DoFullAudit('CON', 'link_contacts', $PostedContact['recordid'], $data, "link_recordid = $link_recordid");
    }

    DoFullAudit('CON', 'contacts_main', $PostedContact['recordid'], $data);
}

function SaveContacts($aParams)
{
    global $ModuleDefs;

    $MaxSuffix = $_POST['contact_max_suffix'];

    // make sure that witness (W), reporter (R) etc. are given their correct DIF2 link_types (N)
    if ($ModuleDefs[$aParams['module']]['LEVEL1_CON_OPTIONS'])
    {
        for ($i = 0; $i < $MaxSuffix; $i++)
        {
            if ($_POST['link_type_'.$i] && $ModuleDefs[$aParams['module']]['LEVEL1_CON_OPTIONS'][$_POST['link_type_'.$i]]['ActualType'])
            {
                $_POST['link_type_'.$i] = $ModuleDefs[$aParams['module']]['LEVEL1_CON_OPTIONS'][$_POST['link_type_'.$i]]['ActualType'];
            }
            elseif (!$_POST['link_type_'.$i])
            {
                $_POST['link_type_'.$i] = 'N';
            }
        }
    }

    for ($i = 0; $i <= $MaxSuffix; $i++)
    {
        $aParams['suffix'] = $i;

        if ($aParams['suffix'])
        {
            $aParams['suffixstring'] = '_'.$i;
        }

        if (!ContactNotFilledIn($_POST, $i) && !ContactHidden($_POST, $i, $aParams['module']))
        {
            $_POST['con_recordid'.$aParams['suffixstring']] = Sanitize::SanitizeInt($_POST['con_id_'.$i]);
            $aParams['data']['main_recordid'] = $aParams['main_recordid'];

            SaveLinkedContact($aParams);
        }
    }
}

function SaveInjuries($aParams)
{
    if ($aParams['module'] == 'INC') //no support for other modules yet.
    {
        $SuffixString = ($aParams['suffix'] ? '_'.$aParams['suffix'] : '');

        /* Injuries are in POST arrays injury[] and bodypart[]
        */
        $sql = "DELETE FROM inc_injuries
        WHERE inc_id = :inc_id
        AND con_id = :con_id";
        DatixDBQuery::PDO_query($sql, array("inc_id" => $aParams['main_recordid'], "con_id" => $aParams['recordid']));

        if (($SuffixString && (isset($_POST['show_field_show_injury'.$SuffixString]) && ($_POST['show_injury'.$SuffixString] == '' || $_POST['show_injury'.$SuffixString] == 'N'))) ||
            (!$SuffixString && isset($_POST['show_field_show_injury']) && ($_POST['show_injury'] == '' || $_POST['show_injury'] == 'N')))
        {
            return;
        }

        if ($_POST["injury".$SuffixString] != "")
        {
            $_POST["injury".$SuffixString] = Sanitize::SanitizeStringArray($_POST["injury".$SuffixString]);

            foreach ($_POST["injury".$SuffixString] as $ref => $Injury)
            {
                if (!$PrimaryDone) // fill in primary injury/body part.
                {
                    $sql = "UPDATE link_contacts SET
                        link_injury1 = :link_injury1,
                        link_bodypart1 = :link_bodypart1
                        WHERE inc_id = :inc_id AND con_id = :con_id";

                    DatixDBQuery::PDO_query($sql, array(
                        "link_injury1" => $Injury,
                        "link_bodypart1" => $_POST["bodypart".$SuffixString][$ref],
                        "inc_id" => $aParams['main_recordid'],
                        "con_id" => $aParams['recordid']
                    ));

                    $PrimaryDone = true;
                }

                if ($Injury!="")
                {
                    $injury_id = GetNextRecordID("inc_injuries");

                    $Listorder++;

                    $sql = "INSERT INTO inc_injuries
                        (recordid, inc_id, con_id, listorder,
                        inc_injury, inc_bodypart)
                        VALUES
                        (:injury_id, :inc_id, :con_id,
                        :listorder, :inc_injury, :inc_bodypart)";

                    DatixDBQuery::PDO_query($sql, array(
                        "injury_id" => $injury_id,
                        "inc_id" => $aParams['main_recordid'],
                        "con_id" => $aParams['recordid'],
                        "listorder" => $Listorder,
                        "inc_injury" => $Injury,
                        "inc_bodypart" => $_POST["bodypart".$SuffixString][$ref]
                    ));
                }
            }
        }
    }
}

