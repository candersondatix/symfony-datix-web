<?php
$GLOBALS['FormTitle'] = 'Datix Contact Form';

$GLOBALS["DefaultValues"] = array(
    "lcom_dreceived" => "TODAY",
    "link_type" => $con["link_type"]
    );
$GLOBALS["MandatoryFields"] = array(
    "link_role" => "contact",
    "con_surname" => "contact");
$GLOBALS["HideFields"] = array(
    "icon_cost" => true,
    "link_worked_alone" => true,
    "link_become_unconscious" => true,
    "link_req_resuscitation" => true,
    "link_hospital_24hours" => true,
    "lcom_dreceived" => true,
    "con_police_number" => true,
    "con_work_alone_assessed" => true,
    'sirs' => true,
    'property_section' => true,
    'link_age_band' => true,
    'link_date_admission' => true,
    'link_notify_progress' => true,
    );

$GLOBALS["UserExtraText"] = array (
    'con_dod' => 'Fill this in for a patient who has died',
);
$GLOBALS["ExpandFields"] = array (
  'lcom_iscomplpat' =>
  array (
    0 =>
    array (
      'field' => 'link_patrelation',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'N',
      ),
    ),
  ),
  'con_police_pursue' =>
  array (
    0 =>
    array (
      'field' => 'con_police_persue_reason',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'N',
      ),
    ),
  ),
  'link_plapat' =>
  array (
    0 =>
    array (
      'field' => 'link_patrelation',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'N',
      ),
    ),
  ),
);

$GLOBALS["ExpandSections"] = array (
  'show_injury' =>
  array (
    0 =>
    array (
      'section' => 'injury_section',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'link_pprop_damaged' =>
  array (
    0 =>
    array (
      'section' => 'property_section',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
);
?>
