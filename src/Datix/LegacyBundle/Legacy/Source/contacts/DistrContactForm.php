<?php

function ContactMatch()
{
	global $scripturl, $NoMainMenu, $dtxtitle, $dtxdebug, $MinifierDisabled;

	$NoMainMenu = true;

	$dtxtitle = "Check for matching contacts";

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    if ($_GET["con_forenames"] != "")
    {
        $con_forenames = str_replace("*", "%", Sanitize::SanitizeString($_GET["con_forenames"]));
        $con_forenames = str_replace("?", "_", $con_forenames);

        if ($dbtype == "mssql" && !preg_match('/[%?_*]/u', $con_forenames))
        {
            $Search[] = "(soundex(con_forenames) = soundex('$con_forenames'))";
        }
        else
        {
            $Search[] = "con_forenames LIKE '$con_forenames'";
        }
    }

    if ($_GET["con_surname"] != "")
    {
        $con_surname = str_replace("*", "%", Sanitize::SanitizeString($_GET["con_surname"]));
        $con_surname = str_replace("?", "_", $con_surname);

        if ($dbtype == "mssql" && !preg_match('/[%?_*]/u', $con_surname))
        {
            $Search[] = "soundex(con_surname) = soundex('".EscapeQuotes($con_surname)."')";
        }
        else
        {
            $Search[] = "con_surname LIKE '".EscapeQuotes($con_surname)."'";
        }
    }

	if ($_GET["con_number"] != "")
    {
        $Search[] = "con_number LIKE '" . Sanitize::SanitizeString($_GET['con_number']) . "%'";
    }

	$conPermWhere = MakeSecurityWhereClause("", "CON", $_SESSION["initials"]);

	if ($conPermWhere)
    {
        $Search[] = "($conPermWhere)";
    }

	if ($Search)
	{
		$where = implode($Search, ' AND ');

		$sql = "SELECT
            con_title, con_forenames, con_surname,
			recordid AS con_id, con_type, con_gender, con_ethnicity,
			con_subtype, con_address,
			con_postcode, con_number, con_tel1,
			con_fax, con_dob, con_email, updateid,
            con_orgcode, con_clingroup, con_directorate, con_specialty,
            con_unit, con_loctype, con_locactual,
            con_empl_grade, con_payroll, con_jobtitle
			FROM contacts_main
			WHERE $where
			ORDER BY con_name ASC";

		$result = db_query($sql);
	}

    echo '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset="UTF-8" />
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <script language="javascript" type="text/javascript" src="js_functions/common' . $addMinExtension . '.js"></script>
        <link rel="stylesheet" type="text/css" href="css/datix.css" />
        <title>'. $dtxtitle .'</title>
    </head>
    <body text="#000000" link="#0033ff" bgcolor="#ffffff">';
	echo '<SCRIPT language="JavaScript" type="text/javascript" src="js_functions/listbox' . $addMinExtension . '.js"></script>';
	echo '
	<form name="link" method="post" action="' . $scripturl . '?action=distrlinkcontact">
	<table class="titlebg" cellspacing="1" cellpadding="0" width="100%" align="center" border="0">
    <tr>
	<td class="windowbg">
	<br /><font size="3"><b>Matching contacts</b></font><br /><br />
	</td>
	</tr>
	<tr>
	<td>
	' . ($dtxdebug ? $where : '') . '
	<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
	<tr>
        <td class="titlebg" width="6%" align="center"><b>Choose</b></td>
		<td class="titlebg" width="15%" align="center"><b>Name</b></td>
		<td class="titlebg" width="5%" align="center"><b>Type</b></td>
		<td class="titlebg" width="5%" align="center"><b>Subtype/Status</b></td>
        <td class="titlebg" width="5%" align="center"><b>Patient/staff number</b></td>
        <td class="titlebg" width="5%" align="center" nowrap><b>Job Title</b></td>
        <td class="titlebg" width="5%" align="center"><b>DOB</b></td>
        <td class="titlebg" width="5%" align="center"><b>Postcode</b></td>
        <td class="titlebg" width="5%" align="center"><b>Telephone no.</b></td>
        <td class="titlebg" width="6%" align="center" nowrap><b>E-mail</b></td>
	</tr>
';
	$ContactCount = 0;

	while ($result && $row = db_fetch_array($result))
	{
		$EscapedAddress = str_replace("'", "\\'", $row["con_address"]);
		$EscapedAddress = str_replace ('"', "\\'", $EscapedAddress);
		$EscapedAddress = str_replace("\r\n", "\\n", $EscapedAddress);
        $conforenames = addslashes($row["con_forenames"]);
        $condob = FormatDateVal($row["con_dob"]);

        if (ini_get('magic_quotes_sybase'))
        {
            $EscapedSurname = str_replace("'", "\\'", $row["con_surname"]);
        }
        elseif (ini_get('magic_quotes_gpc'))
        {
            $EscapedSurname = addslashes($row["con_surname"]);
        }
        else
        {
            $EscapedSurname = str_replace("'", "\\'", $row["con_surname"]);
        }

        $setfieldsArray = array(
            "con_id" => "$row[con_id]",
            "main_recordid" => "$row[con_id]",
            "con_type" => "$row[con_type]",
            "con_subtype" => "$row[con_subtype]",
            "con_number" => "$row[con_number]",
            "con_title" => "$row[con_title]",
            "con_forenames" => "$conforenames",
            "con_surname" => "$EscapedSurname",
            "con_gender" => "$row[con_gender]",
            "con_dob" => "$condob",
            "con_address" => "$EscapedAddress",
            "con_postcode" => "$row[con_postcode]",
            "con_jobtitle" => "$row[con_jobtitle]",
            "con_tel1" => "$row[con_tel1]",
            "con_email" => "$row[con_email]",
            "updateid" => "$row[updateid]",
            "con_ethnicity" => "$row[con_ethnicity]",
            "con_orgcode" => "$row[con_orgcode]",
            "con_unit" => "$row[con_unit]",
            "con_clingroup" => "$row[con_clingroup]",
            "con_directorate" => "$row[con_directorate]",
            "con_specialty" => "$row[con_specialty]",
            "con_loctype" => "$row[con_loctype]",
            "con_locactual" => "$row[con_locactual]",
            "con_empl_grade" => "$row[con_empl_grade]",
            "con_payroll" => "$row[con_payroll]"
        );

        echo '
	<tr>
		<td class="windowbg2">
        <input type="button" value="Choose" onclick="';

        foreach ($setfieldsArray as $fname => $fval)
        {
            if ($fname == "con_subtype")
            {
                echo '
                if (opener.document.forms[0].con_subtype.value == \'\') {
                    opener.document.forms[0].con_subtype.value = \'' . $row["con_subtype"] . '\'};';
            }

            if ($fname == "con_id")
            {
                echo "opener.document.forms[0].con_id.value = $row[con_id];";
            }
            else
            {
                echo "if(opener.document.getElementById('$fname'))
                         opener.document.forms[0].$fname.value = '$fval';";
            }
        }

        echo '
        window.close()" />
		</td>
		<td class="windowbg2" nowrap>' .
		"$row[con_title] $row[con_forenames] $row[con_surname]" . '
		</td>
        <td class="windowbg2">' . code_descr('CON', 'con_type', $row["con_type"]) .'</td>
		<td class="windowbg2">' . code_descr('CON', 'con_subtype', $row["con_subtype"]) . '</td>
        <td class="windowbg2">' . $row["con_number"] . '</td>
        <td class="windowbg2">' . $row["con_jobtitle"] . '</td>
        <td class="windowbg2">' . FormatDateVal($row["con_dob"]) . '</td>
        <td class="windowbg2">' . $row["con_postcode"] . '</td>
        <td class="windowbg2">' . $row["con_tel1"] . '</td>
        <td class="windowbg2">' . $row["con_email"] . '</td>
	</tr>';
		$ContactCount++;
	}

	if ($ContactCount == 0)
		echo '
	<tr>
		<td class="windowbg2" colspan="10">
		<br />
		<b>No matching contacts found.</b>
		<br /><br />
		</td>
	</tr>';
	echo '
	<tr>
		<td class="titlebg" align="center" colspan="10">
';
		echo '
		<input type="button" value="'._tk('btn_cancel').'" onClick="window.close()">
		</td>
	</tr>';

	echo '</table></td></tr></table></form>';

	obExit();
}

// Called when the user clicks Cancel on the match list page.
function ReshowContactLink()
{
	$dst_id = $_POST["dst_id"];

	$con = Sanitize::SanitizeRawArray($_POST);
	$con["link_exists"] = false;

	ShowContactForm($con, $dst_id);
}

function ContactLink()
{
    global $ModuleDefs;

	LoggedIn();

	if ($_POST["link_action"] == "Cancel")
    {
        ReshowContactLink();
    }

	// Check if this function has been called by a POST from the matching function
	if ($_POST["con_id"] != "")
	{
		$dst_id = $_POST["dst_id"];
		$sql = 'SELECT '.
                implode(', ',$ModuleDefs['CON']['FIELD_ARRAY']).'
			FROM contacts_main
			WHERE recordid = '. Sanitize::SanitizeInt($_POST['con_id']);

		$result = db_query($sql);
		$con = db_fetch_array($result);

		$con = array_merge(Sanitize::SanitizeStringArray($_POST), $con);
	}
	// A pre-existing linked contact
	elseif ($_GET["con_id"] != "" && $_GET["formtype"] == "Main")
	{
		$dst_id = $_GET["dst_id"];
		$sql = 'SELECT recordid AS con_id,'.
                implode(', ',$ModuleDefs['CON']['FIELD_ARRAY']).'
			FROM contacts_main
			WHERE recordid = '. Sanitize::SanitizeInt($_GET['con_id']);

		$result = db_query($sql);
		$con = db_fetch_array($result);

		// Check for existing link

		$sql = "SELECT link_type, link_status, link_notes FROM link_contacts
				WHERE con_id = " . Sanitize::SanitizeInt($_GET['con_id']) . " AND
				dst_id = " . Sanitize::SanitizeInt($_GET['dst_id']);
		$result = db_query($sql);

		if ($row = db_fetch_array($result))
		{
			$con = array_merge($con, $row);
			$con["link_exists"] = true;
		}
    }
	else
    {
        $dst_id = $_GET["dst_id"];
    }

	ShowContactForm($con, $dst_id);
}

function ShowContactForm($con = "", $dst_id = "", $error = "")
{
	global $scripturl, $dtxtitle;

    //Check whether we are in ReadOnly mode

    if($_SESSION["ReadOnlyMode"])
    {
        $ReadOnly = true;
    }

    $dst_id = is_numeric($dst_id) ? (int) $dst_id : "";

    $DSTPerms = GetParm('DST_PERMS');

    $LinkTypes = array(
        "N" => "Contact"
    );

    if ($_GET["linktype"])
    {
        $con["link_type"] = Sanitize::SanitizeString($_GET["linktype"]);
    }

    $FormType = Sanitize::SanitizeString($_GET["formtype"]);

	if ($con["link_exists"])
    {
        $dtxtitle = $LinkTypes[$con["link_type"]];
    }
	else
    {
        $dtxtitle = "New contact for distribution list";
    }

	if ($con["con_subtype"] != "")
    {
        $con["link_status"] = $con["con_subtype"];
    }

    if ($ReadOnly)
    {
        $FieldObj = new FormField("Print");
    }
    else
    {
        $FieldObj = new FormField();
    }

    $FormArray = array(
        "Parameters" => array(
            "Panels" => true,
            "Condition" => false
        ),
        "contact" => array(
            "Title" => "Contact details",
            "NewPanel" => true,
            "Rows" => array(
                array(
                    "Name" => "recordid",
                    "Title" => "Contact ID",
                    "Type" => "string",
                    "ReadOnly" => (!($form_action == "Design" || $form_action == "search"))
                ),
                "con_title",
                "con_forenames",
                "con_surname",
                "con_jobtitle",
                "con_organisation",
                "con_email",
                "con_address",
                "con_postcode",
                "con_gender",
                "con_dopened",
                "con_dclosed",
                "con_dob",
                "con_dod",
                array(
                    "Name" => "con_tel1",
                    "Type" => "string",
                    "Title" => "Telephone no. 1",
                    "Width" => 15,
                    "MaxLength" => 254
                ),
                array(
                    "Name" => "con_tel2",
                    "Type" => "string",
                    "Title" => "Telephone no. 2",
                    "Width" => 15,
                    "MaxLength" => 254
                ),
                "con_fax",
                "con_number",
                "con_nhsno",
                "con_ethnicity",
                "con_language",
                "con_type",
                "con_subtype",
                "con_notes",
            )
        ),
        "employee" => array("Title" => "Employee details",
            'NewPanel' => true,
            'ReadOnly' => true,
            "Rows" => array(
                "con_orgcode",
                "con_clingroup",
                "con_directorate",
                "con_unit",
                "con_loctype",
                "con_locactual",
                "con_specialty",
                "con_empl_grade",
                "con_payroll")
        )
    );

	template_header();

    $ConTable = new FormTable($FormType, 'CON');
    $ConTable->MakeForm($FormArray, $con, 'CON');

    ?>
    <script type="text/javascript" language="javascript">
        function MatchExistingContacts()
        {
            document.frmDistr.form_action.value = 'link';
            var url = '<?= $scripturl . "$scripturl?action=distrcontactlinkaction&amp;formtype=" . $FormType . "&token=". CSRFGuard::getCurrentToken(); ?>';
            document.frmDistr.target = "wndMatchExisting";
            document.frmDistr.onsubmit = "window.open(url, 'wndMatchExisting', 'dependent,menubar=false,screenX=200,screenY=330,titlebar,scrollbars,resizable')";
            document.frmDistr.submit();

            document.frmDistr.target = "";
            document.frmDistr.form_action.value = "new";
        }
     </script>
<?php
    echo '<form method="post" name="frmDistr" action="' . $scripturl . '?action=distrcontactlinkaction&amp;formtype=' . $FormType . '">';

    echo '
        <input type="hidden" name="dst_id" value="'. $dst_id .'" />
        <input type="hidden" name="con_id" value="'. is_numeric($con["con_id"]) ? (int) $con["con_id"] : "" .'" />
        <input type="hidden" name="updateid" value="'. htmlspecialchars($con["updateid"]) .'" />
        <input type="hidden" name="main_recordid" value="'. is_numeric($con["main_recordid"]) ? (int) $con["main_recordid"] : "" .'" />
        <input type="hidden" name="form_action" value="match" />
        <input type="hidden" name="link_type" value="'. htmlspecialchars($con["link_type"]) .'" />
        <input type="hidden" name="formtype" value="'. $FormType .'" />
        <input type="hidden" name="module" value="DST" /> ';

    $FormTitle = ($con["link_exists"] || $con["link_type"] != "" ? $LinkTypes[$con["link_type"]] : "Add Contact Link");

    echo GetPageTopDivs(array(
        'module' => 'DST',
        'data' => $con,
        'formtype' => $FormType,
        'pagetitle' => array(
            'title' => $FormTitle,
            'image' => 'Images/distrtitle.gif'
        ),
        'formtable' => $ConTable,
    ));

    $ConTable->MakeTable();
    echo $ConTable->GetFormTable();

    echo '<div class="button_wrapper">
        ';

	    // Display different options if showing a link or creating a link
	    if (!$con["link_exists"] && !$ReadOnly)
	    {
            $HideLinkButton = (bYN(GetParm('REQUIRE_CONTACT_MATCH_CHECK')) && !$con["con_id"]);

    			    echo '
        		    <input type="button" value="'._tk('check_matching_contacts').'" onClick="javascript:'.($HideLinkButton ? '$(\'btnSave\').show();' : '').'MatchExistingContacts()"/>';
        		    echo '
        		    <input type="button" id="btnSave" name="btnSave" '.($HideLinkButton ? 'style="display:none" ' : '').'value="'._tk('create_new_link').'" onClick="document.frmDistr.form_action.value=\'new\';submitClicked = true; this.form.submit()" />
        		    <input type="button" value="'._tk('btn_cancel').'" onClick="document.frmDistr.form_action.value=\'cancel\';submitClicked = true;this.form.submit()" />';
	    }
	    else
	    {
		     if (!$ReadOnly)
             {
                 echo '<input type="button" value="Update link details" onClick="document.frmDistr.form_action.value=\'new\';submitClicked = true; this.form.submit()" /> ';

                 if (IsSubAdmin() && $DSTPerms != 'DST_READ_ONLY')
                 {
                     echo '<input type="button" value="Unlink contact" onClick="if(confirm(\''._tk('unlink_contact_confirm').'\')){document.frmDistr.form_action.value=\'unlink\';submitClicked = true;this.form.submit()}" /> ';
                 }
             }

             echo '<input type="button" value="'._tk('btn_back_distribution').'" onClick="document.frmDistr.form_action.value=\'cancel\';submitClicked = true;this.form.submit()" />';
	    }

        echo '</div>';

    echo '
                </div>
            </div>
        </div>
    </div>
</form>';

    if ($FormType != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $ConTable);
    }

    footer();

    obExit();
}

function ContactLinkAction()
{
	global $scripturl, $yySetLocation;
	$form_action = $_POST["form_action"];

	switch ($form_action)
	{
		case "match":
			ContactMatch();
			break;
        case "link":
            $yySetLocation = $scripturl . '?action=searchcontact';
            redirectexit();
            break;
		case "new":
            SaveContactLinkToMain();
            break;
        case "batch":
            ContactBatchLink();
			break;
		case "unlink":
			UnlinkContact();
			break;
		case "cancel":
		    $yySetLocation = "$scripturl?action=record&module=DST&recordid=$_POST[dst_id]";

            if ($_POST["link_type"])
            {
                $yySetLocation .= "&panel=contacts_type_". $_POST["link_type"];
            }

            redirectexit();
			break;
	}

	obExit();
}

function UnlinkContact()
{
	global $scripturl, $yySetLocation;
	LoggedIn();

	$con_id = Sanitize::SanitizeInt($_POST["con_id"]);
	$dst_id = Sanitize::SanitizeInt($_POST["dst_id"]);
    $DSTPerms = $_SESSION["Globals"]["DST_PERMS"];
    $LinkOwnerId = $_SESSION["contact_login_id"];

	$sql = "DELETE FROM link_contacts
		WHERE con_id = $con_id
		AND dst_id = $dst_id";

	db_query($sql);

	$yySetLocation = "$scripturl?action=record&module=DST&recordid=$dst_id&panel=contacts_type_" . $_POST["link_type"];
	redirectexit();
}

function ContactBatchLink()
{
	global $scripturl, $yySetLocation;

	LoggedIn();

    $DSTPerms = $_SESSION["Globals"]["DST_PERMS"];
    $LinkOwnerId = $_SESSION["contact_login_id"];
    $dst_id = $_SESSION["DST"]["CURRENTID"];

    $sql = "SELECT contacts_main.recordid as con_id, link_contacts.link_recordid
		FROM contacts_main LEFT JOIN link_contacts ON link_contacts.con_id = contacts_main.recordid AND dst_id = $dst_id
		WHERE ";

    $sql .= MakeSecurityWhereClause($_SESSION["CON"]["WHERE"], "CON", $_SESSION["initials"]);

    $conresult = DatixDBQuery::PDO_fetch_all($sql);

    $con_type = $_SESSION["DST"]["CURRENTLINKTYPE"];

	foreach ($conresult as $row)
    {
        if ($row['link_recordid'] != '')
        {
            $UpdateRecords[] = $row['con_id'];
        }
        else
        {
            $InsertRecordSQL[] = "SELECT $row[con_id], $dst_id, '$con_type'";
        }
    }

    if (!empty($UpdateRecords))
    {
        $sql = "UPDATE link_contacts
            SET link_type = '$con_type',
            updateddate = '" . date('d-M-Y H:i:s') . "'
            WHERE con_id IN (".implode(',', $UpdateRecords).") AND dst_id = $dst_id";
        DatixDBQuery::PDO_query($sql);
    }

    if (!empty($InsertRecordSQL))
    {
        $sql = "INSERT INTO link_contacts (con_id, dst_id, link_type) ".
            implode(' UNION ALL ', $InsertRecordSQL);
        DatixDBQuery::PDO_query($sql);
    }

    $yySetLocation = "$scripturl?action=record&module=DST&recordid=$dst_id&panel=contacts_type_$con_type";
	redirectexit();
}

function SaveContactLinkToMain()
{
	global $scripturl, $yySetLocation, $ModuleDefs;

	LoggedIn();

	$con_id = Sanitize::SanitizeInt($_POST["con_id"]);
	$dst_id = Sanitize::SanitizeInt($_POST["dst_id"]);
    $DSTPerms = $_SESSION["Globals"]["DST_PERMS"];

	if ($dst_id == "")
    {
        fatal_error("No ID");
    }

	$ErrorMark = '<font size="3" color="red"><b>*</b></font>';

    if ($_POST["link_type"] == "")
	{
		$error["message"] .= "<br />You must enter a link type (Link as..)";
		$error["link_type"] = $ErrorMark;
	}

	if ($_POST["con_surname"] == "")
	{
		$error["message"] .= "<br />You must enter a surname.";
		$error["con_surname"] = $ErrorMark;
	}

	if ($_POST["con_dob"] != "" && CompareDates($_POST["con_dob"], date('d/m/Y')) > 0)
	{
		$error ["message"] .= "<br />Date of birth must be the same as or earlier than today.";
		$error["con_dob"] = $ErrorMark;
	}

	if ($error)
	{
		ShowContactForm(Sanitize::SanitizeStringArray($_POST), $dst_id, $error);
		obExit();
	}

	$con = QuotePostArray($_POST);
    $con = ParseSaveData(array('module' => 'CON', 'data' => $con));

	// Do we need to create a new contact?
	if ($con_id == "")
	{
		$con_id = GetNextRecordID("contacts_main", true);
		$NewContact = true;
	}

    $SQLupdate = GeneratePDOSQLFromArrays(array(
            'FieldArray' => $ModuleDefs['CON']['FIELD_ARRAY'],
            'DataArray' => $con,
            'Module' => 'CON',
            'end_comma' => true
        ),
        $PDOParamsArray
    );

	$sql = "UPDATE contacts_main
		SET
		$SQLupdate
        ";

    $sql .= " updateid = '" . GensUpdateID($_POST["updateid"]) . "'
		WHERE recordid = $con_id
        ";

    $result = DatixDBQuery::PDO_query($sql, $PDOParamsArray);

	if (!$result)
    {
		fatal_error("Could not insert contact" . $sql);
    }

	// Check if the link already exists.  If so, we just need to update it

	$sql = "SELECT link_recordid, con_id, dst_id
		FROM link_contacts
		WHERE con_id = $con_id
		AND dst_id = $dst_id";

	$result = db_query($sql);

	if ($fetchrow = db_fetch_array($result))
	{
        $DSTLinkID = $fetchrow["link_recordid"];
        $sql = "UPDATE link_contacts
			SET link_type = '$con[link_type]',
			link_status = '$con[con_subtype]',
			link_notes = '$con[link_notes]',
			updateddate = " . UserDateToSQLDate(date('d/m/Y'))
			. "
			WHERE link_recordid = $DSTLinkID";
		db_query($sql);
	}
	else
	{
        $sql = "INSERT INTO link_contacts
			(con_id, dst_id, link_type,
			link_status, link_notes)
			VALUES
			($con_id, $dst_id, '$con[link_type]',
			'$con[con_subtype]',
			'$con[link_notes]')";

		if (!db_query($sql))
        {
            fatal_error("Cannot insert link");
        }
	}

    if ($_POST["form_action"] != 'email')
    {
        $yySetLocation = "$scripturl?action=record&module=DST&recordid=$dst_id";
        redirectexit();
    }
}

Function ContactBatchUnlink($dst)
{
    global $scripturl;

    $dst_id = $dst["recordid"];
    $DSTPerms = $_SESSION["Globals"]["DST_PERMS"];

    $sql = "select link_contacts.con_id AS con_id
            from link_contacts
            where link_contacts.dst_id = $dst_id";

    $request = db_query($sql);

	while ($row = db_fetch_array($request))
	{
        $con_id = $row['con_id'];

        if ($_POST["checkbox_include_" . $con_id] == "on")
        {

	        $sql = "DELETE FROM link_contacts
		            WHERE con_id = $con_id
		            AND dst_id = $dst_id";

	        db_query($sql);
        }
    }
}

?>