<?php

/* Variables:
 * main_recordid : recordid in incidents_main, civ_main, pals_main etc.
 * con_recordid: recordid in contacts_main
 * conrep_recordid : recordid in increp_contacts, palrep_contacts etc.
 * link_recordid : recordid in link_contacts
 *
 */

// Called when the user clicks Cancel on the match list page.
function ReshowContactLink()
{
    $main_recordid = Sanitize::SanitizeInt($_POST["main_recordid"]);

    $con = Sanitize::SanitizeStringArray($_POST);
    $con["link_exists"] = false;

    ShowContactForm($con, $main_recordid);
}

function ContactLink()
{
    global $ModuleDefs, $ClientFolder;

    $module        = Sanitize::getModule($_GET['module']);
    $main_recordid = Sanitize::SanitizeInt($_GET['main_recordid']);

    $contact_fields = implode(",", $ModuleDefs['CON']['FIELD_ARRAY_SELECT']);

    //also need initials to check that someone is a user so we know whether their email address is editable.
    $contact_fields .= ', initials';

    LoggedIn();

    if ($_POST["link_action"] == "Cancel")
    {
        ReshowContactLink();
    }

    if ($_GET["external"] == 1)
    {
        require_once $ClientFolder.'/external_src_config.php';
        $client = new SoapClient($_SESSION["CON"]["EXTERNAL_WSDL"],array(
        "trace"      => 1,
        "exceptions" => 0));


        if ($_SESSION["CON"]["EXTERNAL_KEY_DATATYPE"] == "string")
        {
            $contactdetails = ($client->getContact($_SESSION["login"], "", $_SESSION["session_id"], $_SESSION["CON"]["EXTERNAL_KEY"] . ' = \'' . $_GET[$_SESSION["CON"]["EXTERNAL_KEY"]] .'\''));
        }
        else
        {
            $contactdetails = ($client->getContact($_SESSION["login"], "", $_SESSION["session_id"], $_SESSION["CON"]["EXTERNAL_KEY"] . ' = ' . $_GET[$_SESSION["CON"]["EXTERNAL_KEY"]]));
        }

        require_once 'soap/interfaceclass.php';
        $DatixInterface = new DatixInterface();
        $DatixInterface->LoadXMLDoc($contactdetails);
        $DatixInterface->parseXMLDataToArray($request);
        $con = $request[0];
        $con["recordid"] = "";

        if ($con["con_dopened"] != "")
        {
            $con["con_dopened"] = date('Y-m-d H:i:s.000', strtotime(str_replace("T", " ", $con["con_dopened"])));
        }

        if ($con["con_dclosed"] != "")
        {
            $con["con_dclosed"] = date('Y-m-d H:i:s.000', strtotime(str_replace("T", " ", $con["con_dclosed"])));
        }

        if ($con["con_dob"] != "")
        {
            $con["con_dob"] = date('Y-m-d H:i:s.000', strtotime(str_replace("T", " ", $con["con_dob"])));
        }

        if ($con["con_dod"] != "")
        {
            $con["con_dod"] = date('Y-m-d H:i:s.000', strtotime(str_replace("T", " ", $con["con_dod"])));
        }

        $con = array_merge(Sanitize::SanitizeStringArray($_POST), $con);
    }
    elseif ($_GET["link_recordid"] != "")  // An already-linked contact
    {

        if ($_GET['link_type'] == 'O') // respondents
        {
            $sql = "SELECT " . implode(",", $ModuleDefs["CON"]['LINKED_FIELD_ARRAY_RESPONDENT']) . ", recordid AS link_recordid
            FROM link_respondents
            WHERE recordid = :link_recordid";
        }
        else
        {
            $sql = "SELECT " . implode(",", $ModuleDefs["CON"]['LINKED_FIELD_ARRAY']) . ", link_recordid, con_id, ".$ModuleDefs[$module]['FK']."
            FROM link_contacts
            WHERE link_recordid = :link_recordid";
        }

        $con = DatixDBQuery::PDO_fetch($sql, array("link_recordid" => $_GET["link_recordid"]));

        if ($con === false)
        {
            throw new Exception('Unable to retrieve details for contact with link id '.$_GET["link_recordid"].'.');
        }

        $con["link_exists"] = true;

        $sql = "SELECT recordid, recordid AS con_recordid, rep_approved, updateid, " .
                $contact_fields . " FROM contacts_main
                WHERE recordid = $con[con_id]";

        $result = db_query($sql);
        $row = db_fetch_array($result);

        $con = array_merge($con, $row);

        if (is_array($ModuleDefs[$module]['EXTRA_CONTACT_LINK_DATA_FUNCTIONS']))
        {
            foreach ($ModuleDefs[$module]['EXTRA_CONTACT_LINK_DATA_FUNCTIONS'] as $func)
            {
                $Extra = $func(array('data' => $con));

                if (is_array($Extra))
                {
                    $con = array_merge($con, $Extra);
                }
            }
        }

        if ($module == 'INC')
        {
            $sql = "SELECT inc_injury, inc_bodypart, recordid, listorder,
                con_id, inc_id
                FROM inc_injuries
                WHERE con_id = $con[con_id]
                AND inc_id = ".$con[$ModuleDefs[$module]['FK']]."
                ORDER BY listorder ASC";

            $request = db_query($sql);

            while ($row = db_fetch_array($request))
            {
                $con["injury_table"][$row["listorder"]] = $row;
            }

            /* Check for property information in inc_personal_property */
            $sql = "SELECT recordid, ipp_description, ipp_damage_type, ipp_value, listorder,
                con_id, inc_id
                FROM inc_personal_property
                WHERE con_id = $con[con_id]
                AND inc_id = ".$con[$ModuleDefs[$module]['FK']]."
                ORDER BY listorder ASC";

            $request = db_query($sql);

            while ($row = db_fetch_array($request))
            {
                $con["property_table"][$row["listorder"]] = $row;
            }

            $sql = "SELECT icon_cost
                FROM vw_inc_con_cost
                WHERE con_id = $con[con_id]
                AND inc_id = ".$con[$ModuleDefs[$module]['FK']];

            $request = db_query($sql);
            $inccost = db_fetch_array($request);
            $con["icon_cost"] = $inccost["icon_cost"];

        }
    }
    elseif ($_GET["con_recordid"] != "")  // A contact not yet linked.
    {
        $sql = "SELECT recordid, recordid AS con_recordid, rep_approved, updateid, " .
                $contact_fields . " FROM contacts_main
                WHERE recordid = :con_recordid";

        $con = DatixDBQuery::PDO_fetch($sql, array("con_recordid" => Sanitize::SanitizeInt($_GET["con_recordid"])));
        $con["conrep_recordid"] = Sanitize::SanitizeInt($_GET["conrep_recordid"]);

        if ($module == 'INC')
        {
            $sql = "SELECT inc_injury, inc_bodypart, recordid, listorder,
                con_id, inc_id
                FROM inc_injuries
                WHERE con_id = :con_id
                AND inc_id = :inc_id
                ORDER BY listorder ASC";

            $rows = DatixDBQuery::PDO_fetch_all($sql, array("con_id" =>  $_GET["con_recordid"], "inc_id" => $main_recordid));

            foreach ($rows as $row)
            {
                $con["injury_table"][$row["listorder"]] = $row;
            }

            /* Check for property information in inc_personal_property */
            $sql = "SELECT recordid, ipp_description, ipp_damage_type, ipp_value, listorder,
                con_id, inc_id
                FROM inc_personal_property
                WHERE con_id = :con_id
                AND inc_id = :inc_id
                ORDER BY listorder ASC";

            $rows = DatixDBQuery::PDO_fetch_all($sql, array("con_id" =>  $_GET["con_recordid"], "inc_id" => $main_recordid));

            foreach ($rows as $row)
            {
                $con["property_table"][$row["listorder"]] = $row;
            }
        }
    }
    else
    {
        if (empty($con["con_dopened"]))
        {
            $con["con_dopened"] = date('Y-m-d H:i:s.000');
        }
	}

    // If the contact might have changed we need to get rid of the link details which may now be innaccurate
    if (!$_GET['from_contact_match'])
    {
        unset($_SESSION["CON"]["LINK_DETAILS"]);
    }

    if (is_array($_SESSION["CON"]["LINK_DETAILS"]["injury"]) && $module=='INC')
    {
        foreach ($_SESSION["CON"]["LINK_DETAILS"]["injury"] as $id => $injury)
        {
            if ($injury)
            {
                $_SESSION["CON"]["LINK_DETAILS"]["injury_table"][$id] = array("inc_injury" => $injury, "inc_bodypart" => $_SESSION["CON"]["LINK_DETAILS"]["bodypart"][$id]);
            }
        }
    }

    $con["show_injury"] = ($con["injury_table"] != "");

    if (!empty($con["con_dob"]))
    {
        $con["link_age"] = getAgeAtDateForModule($module,  Sanitize::SanitizeInt($_GET["main_recordid"]), $con["con_dob"], $con["link_age"]);
    }

    if (!empty($con["link_age"]))
    {
        $con["link_age_band"] = getAgeBand($con["link_age"]);
    }

    // Save main module
    $con['main_module'] = $module;

    ShowContactForm($con, $main_recordid);
}

function ShowContactForm($con, $main_recordid, $error = "")
{
    global $scripturl, $dtxtitle, $ClientFolder, $ModuleDefs, $AccessLvlDefs, $JSFunctions, $OnSubmitJS, $MinifierDisabled;

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    AuditOpenRecord("CON", $con['recordid'], "");

    if (bYN(GetParm('CASCADE_PERMISSIONS', 'N')))
    {
        // Give full permissions for the purposes of linking.
        $CONPerms = 'CON_FULL';
    }
    else
    {
        $CONPerms = GetParm("CON_PERMS");
    }

    $con_recordid          = is_numeric($con['con_recordid']) ? (int) $con['con_recordid'] : "";
    $contact_match_link_id = is_numeric($_GET['contact_match_link_id']) ? (int) $_GET['contact_match_link_id'] : "";
    $main_recordid         = is_numeric($main_recordid) ? (int) $main_recordid : "";
    $link_exists           = isset($con["link_exists"]) ? (bool) $con["link_exists"] : false;

    if (!$con['con_recordid'])
    {
        $FormType = "New";
    }

    $module = filter_var($_REQUEST['module'], FILTER_SANITIZE_STRING);
    $Module = $module;

    // Check whether we are in ReadOnly mode
    // New session variable ContactsReadOnlyMode is used when we only
    // want to make the Contacts module read-only, not anything else
    if ($_SESSION["ReadOnlyMode"] || $_SESSION["ContactsReadOnlyMode"])
    {
        $ReadOnly = true;
    }

    if (!bYN(GetParm('CASCADE_PERMISSIONS', 'N')))
    {
        //---------------------------------------------------------------//
        // Access based on DB table link_access_approvalstatus
        $AccessFlag = GetAccessFlag('CON', $CONPerms, ($con['rep_approved'] ? $con['rep_approved'] : 'NEW'));

        if ($AccessFlag == '')
        {
            $msg = "You do not have the necessary permissions to view the ".$ModuleDefs['CON']['REC_NAME']." with ID $recordid.";

            fatal_error($msg, "Information");
        }

        if ($AccessFlag == 'R')
        {
            $ReadOnly = true;
        }
    }

    $LinkTypes = $ModuleDefs[$module]['CONTACTTYPES'];

    if ($link_exists)
    {
        if ($_REQUEST["link_type"] != $con["link_type"])
        {
            $con["link_type"] = Sanitize::SanitizeString($_REQUEST["link_type"]);
        }

        $dtxtitle = $LinkTypes[$con["link_type"]]['Name'];

    }
    else
    {
        $dtxtitle = "Link new contact";
        $con["link_type"] = Sanitize::SanitizeString($_REQUEST["link_type"]);
    }

    if ($con["link_status"] != "" && !$link_exists)
    {
        $con["con_subtype"] = $con["link_status"];
    }

    if (is_array($_SESSION["CON"]["LINK_DETAILS"]))
    {
        foreach ($_SESSION["CON"]["LINK_DETAILS"] as $fname => $fval)
        {
            if (!in_array($fname,
                    array($ModuleDefs[$module]['FK'], "con_recordid", "main_recordid", "updateid", "link_exists", "link_recordid", 'injury_table'))
                && !($con[$fname] != '' && $fval == '')   // don't overwrite a db value with a blank
                && !($fname == 'link_age' && $con[$fname] != '') //don't overwrite a newly calculated age with one from a past submission that might now be wrong

                )
            {
                $con[$fname] = $fval;
            }
            elseif ($fname == 'injury_table' && !empty($fval)) //need to merge contacts.
            {
                foreach ($fval as $injuryarray)
                {
                    $con[$fname][] = $injuryarray;
                }
            }
        }

        if (!empty($con['injury_table']))
        {
            $con['show_injury'] = true;
        }
    }

    $_SESSION["CON"]["LINK_DETAILS"] = "";

    // Set this so that the link details section is displayed
    $ShowLinkDetails = true;
    SetUpFormTypeAndApproval('CON', $con, $FormType, $form_action, $CONPerms);

    $con['recordid'] = $con["con_recordid"];
    CheckForRecordLocks('CON', $con, $FormType, $sLockMessage);

    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'CON', 'level' => 2, 'form_type' => $FormType, 'link_type' => $con["link_type"], 'parent_module' => $module));
    $FormDesign->LoadFormDesignIntoGlobals();

    if ($FormType != 'New')
    {
        $FormDesign->UnsetDefaultValues();
    }

    StoreContactLabels();

    SetUpApprovalArrays('CON', $con, $CurrentApproveObj, $ApproveObj, $FormType, $FormDesign, 'CON_FULL');

    require_once $ModuleDefs['CON']['BASIC_FORM_FILES'][2];

    if ($link_exists)
    {
       $FormTitle = $LinkTypes[$con["link_type"]]['Name'];
    }
    else
    {
        $FormTitle = "Add '".$LinkTypes[$con["link_type"]]['Name']."' link to ".$ModuleDefs[$module]['REC_NAME'];
    }

    if ($ReadOnly)
    {
        $FormType = "ReadOnly";
    }

    $ConTable = new FormTable($FormType, 'CON', $FormDesign);
    
    if (!$link_exists)
    {
        $ConTable->setNewLink(true);
    }

    $ConTable->MakeForm($FormArray, $con, 'CON');

    getPageTitleHTML(array(
        'title' => $FormTitle,
        'subtitle' => $FormTitleDescr,
        'module' => 'CON'
    ));

    // Display different options if showing a link or creating a link
    $HideLinkButton = (bYN(GetParm('REQUIRE_CONTACT_MATCH_CHECK')) && !$contact_match_link_id && $con['rep_approved'] != 'FA');

    $ButtonGroup = new ButtonGroup();
    $ButtonGroup->DefaultVals['rbWhat'] = 'match';

    if (!$_GET['print'])
    {
        if (!$ReadOnly)
        {
            if ($HideLinkButton)
            {
                $JSFunctions[] = 'jQuery(\'#rep_approved_row\').hide();jQuery(\'#btnSave\').hide();jQuery(\'#icon_link_btnSave\').hide();';
            }

            if (!$link_exists)
            {
                //If we are here, then we are looking at a contact that has been matched but not yet linked.
                $ButtonGroup->AddButton(array('label' => _tk('check_matching_contacts'), 'onclick' => ($HideLinkButton ? '$(\'btnSave\').show();jQuery(\'#icon_link_btnSave\').show();if($(\'rep_approved_row\')){$(\'rep_approved_row\').show();}' : '').'MatchExistingContacts();', 'action' => 'SEARCH'));
                $ButtonGroup->AddButton(array('label' => _tk('create_new_link'), 'name' => 'btnSave', 'id' => 'btnSave', 'onclick' => 'this.disabled=true;'.($OnSubmitJS['btnSave'] ? $OnSubmitJS['btnSave'] : '').'selectAllMultiCodes();submitClicked = true;document.frmContact.rbWhat.value=\'new\';if(validateOnSubmit()){document.frmContact.submit()}else{this.disabled=false}', 'action' => 'SAVE'));
            }
            else
            {
                //If we are here, we are looking at a contact that has already been linked to a record.
                if ($con['rep_approved'] == 'UN' || $con['rep_approved'] == 'REJECT')
                {
                    $ButtonGroup->AddButton(array('label' => _tk('check_matching_contacts'), 'onclick' => ($HideLinkButton ? '$(\'btnSave\').show();jQuery(\'#icon_link_btnSave\').show();if($(\'rep_approved_row\')){$(\'rep_approved_row\').show();}' : '').'MatchExistingContacts();', 'action' => 'SEARCH'));
                    $ButtonGroup->AddButton(array('label' => _tk('save_contact'), 'name' => 'btnSave', 'id' => 'btnSave', 'onclick' => 'this.disabled=true;'.($OnSubmitJS['btnSave'] ? $OnSubmitJS['btnSave'] : '').'selectAllMultiCodes();submitClicked = true;document.frmContact.rbWhat.value=\'new\';if(validateOnSubmit()){document.frmContact.submit()}else{this.disabled=false}', 'action' => 'SAVE'));
                }
                else
                {
                    $ButtonGroup->AddButton(array('label' => _tk('save_contact'), 'name' => 'btnSave', 'id' => 'btnSave', 'onclick' => 'this.disabled=true;'.($OnSubmitJS['btnSave'] ? $OnSubmitJS['btnSave'] : '').'selectAllMultiCodes();submitClicked = true;document.frmContact.rbWhat.value=\'new\';if(validateOnSubmit()){document.frmContact.submit()}else{this.disabled=false}', 'action' => 'SAVE'));
                }
            }
        }

        if ($_SESSION["AdminUser"] && $link_exists)
        {
            if ('O' == $con['link_type'])
            {
                $ButtonGroup->AddButton(array('label' => _tk('unlink_contact'), 'name' => 'btnUnlink', 'id' => 'btnUnlink', 'onclick' => 'unlinkRespondent(\''.$main_recordid.'\', \''.$con['link_recordid'].'\', \''.$Module.'\', \'contact\');', 'action' => 'DELETE'));
            }
            else
            {
                $ButtonGroup->AddButton(array('label' => _tk('unlink_contact'), 'name' => 'btnUnlink', 'id' => 'btnUnlink', 'onclick' => 'if(confirm(\''._tk('unlink_contact_confirm').'\')){submitClicked = true;document.frmContact.rbWhat.value=\'unlink\';document.frmContact.submit()}', 'action' => 'DELETE'));
            }
        }

        $ButtonGroup->AddButton(array('label' => (_tk('contacts_back_to_record') ?: _tk('back_to').' '.$ModuleDefs[$module]['REC_NAME']), 'name' => 'btnBack', 'id' => 'btnBack', 'onclick' => getConfirmCancelJavascript('frmContact'), 'action' => 'BACK'));
        if(strpos($_SERVER['QUERY_STRING'], 'from_parent_record=1') != false)
        {
            $_REQUEST['fromsearch'] = true;
        }
    }

    // If we are in "Print" mode, we don't want to display the menu
    if ($FormType != "Print")
    {
        GetSideMenuHTML(array('module' => 'CON', 'table' => $ConTable, 'buttons' => $ButtonGroup, 'recordid' => $con_recordid, 'no_audit' => true));
        template_header();

    }
    else
    {
        template_header_nomenu();
    }

    // TODO: this should be removed when this is refactored into a controller
    ?><script type="text/javascript">globals.FormType = '<?php echo $FormType; ?>';</script>
    <script language="javascript" type="text/javascript" src="src/respondents/js/respondents<?php echo $addMinExtension; ?>.js"></script><?php

    echo '<form method="post" name="frmContact" action="' . $scripturl . '?action=contactlinkactiongeneral'.($_REQUEST['fromsearch'] ? '&fromsearch=1' : '').'">';
?>
<input type="hidden" name="con_recordid" value="<?= Sanitize::SanitizeInt($con_recordid) ?>" />
<input type="hidden" name="link_recordid" value="<?= (is_numeric($con["link_recordid"]) ? (int) $con["link_recordid"] : $contact_match_link_id) ?>" />
<input type="hidden" name="updateid" value="<?= Escape::EscapeEntities($con["updateid"]) ?>" />
<input type="hidden" name="main_recordid" value="<?= Sanitize::SanitizeInt($main_recordid) ?>" />
<input type="hidden" id="rbWhat" name="rbWhat" value="match" />
<input type="hidden" name="link_exists" value="<?= Escape::EscapeEntities($link_exists) ?>" />
<input type="hidden" name="link_type" value="<?= Escape::EscapeEntities($con["link_type"])?>" />
<input type="hidden" name="form_action" value="" />
<input type="hidden" name="module" value="<?= Sanitize::getModule($module) ?>" />
<input type="hidden" name="from_contact_match" value="<?= Sanitize::SanitizeInt($_GET['from_contact_match']) ?>" />
<input type="hidden" name="contact_match_link_id" value="<?= Sanitize::SanitizeInt($contact_match_link_id) ?>" />
<?php
    if ($sLockMessage)
    {
        echo '
        <div class="lock_message">
            <div id="LockMessage">'.$sLockMessage.'</div>
            <div id="userLockMessage"></div>
        </div>';
    }

    if ($error)
    {
        echo '
        <div class="form_error_wrapper">
            <div class="form_error">'._tk('form_general_errors').'</div>
            <div class="form_error">'.$error['message'].'</div>
        </div>';
    }

    $ConTable->MakeTable();
    echo $ConTable->GetFormTable();

    echo $ButtonGroup->getHTML();

    echo '
    </form>';

?>
    <script type="text/javascript" language="javascript">
        function MatchExistingContacts()
        {
            document.frmContact.rbWhat.value = 'link';
            document.frmContact.form_action.value = 'link';
            var url = '<?= $scripturl . "?action=contactlinkactiongeneral&token=" . CSRFGuard::getCurrentToken() ?>';
            document.frmContact.target = "wndMatchExisting";
            document.frmContact.onsubmit = "window.open(url, 'wndMatchExisting', 'dependent,menubar=false,screenX=200,screenY=330,titlebar,scrollbars,resizable')";
            selectAllMultiCodes();
            document.frmContact.submit();

            document.frmContact.target = "";
        }

        <?php echo MakeJavaScriptValidation(); ?>
        var submitClicked = false;
    </script>
<?php

    if ($FormType != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $ConTable);
    }

    footer();

    obExit();
}

function ContactLinkAction()
{
    global $scripturl, $yySetLocation, $ModuleDefs;

    $form_action = $_POST["rbWhat"];
    $module = Sanitize::getModule($_POST["module"]);

    UnlockLinkedRecords(
        array(
            'main' => array(
                'module' => $module,
                'recordid' => $_POST['main_recordid']
            ),
            'link' => array(
                'module' => 'CON',
                'recordid' => $_POST['con_recordid']
            )
        )
    );

    switch ($form_action)
    {
        case "link":
            $loader = new \src\framework\controller\Loader();
            $controller = $loader->getController(
                array('controller' => 'src\\contacts\\controllers\\ContactsDoSelectionController')
            );
            $controller->setRequestParameter('form_action', 'link');
            echo $controller->doAction('searchcontact');
            obExit();
            break;
        case "new":
            // Update record last updated
            $loader = new \src\framework\controller\Loader();
            $controller = $loader->getController(['controller' => 'src\\generic\\controllers\\RecordController']);
            $controller->setRequestParameter('module', $module);
            $controller->setRequestParameter('recordId', $_POST['main_recordid']);
            $controller->doAction('updateRecordLastUpdated');

            require_once 'Source/contacts/SaveContact.php';

            SaveLinkedContact(array('main_recordid' => Sanitize::SanitizeInt($_POST['main_recordid']), 'module' => $module, 'formlevel' => 2));

            // redirect back to main record
            $panel = $_POST['link_type'] == 'O' ? 'respondents' : GetContactPanelName($module, $_POST['link_type']);

            $yySetLocation = "$scripturl?action=". ($ModuleDefs[$module]['GENERIC'] ? 'record&module='.$module : $ModuleDefs[$module]['ACTION'])."&recordid=".
                            $_POST['main_recordid'].($_REQUEST['fromsearch'] ? '&fromsearch=1' : '')."&panel=".$panel;
            redirectexit();
            break;
        case "unlink":
            // Update record last updated
            $loader = new \src\framework\controller\Loader();
            $controller = $loader->getController(['controller' => 'src\\generic\\controllers\\RecordController']);
            $controller->setRequestParameter('module', $module);
            $controller->setRequestParameter('recordId', $_POST['main_recordid']);
            $controller->doAction('updateRecordLastUpdated');
            UnlinkContact();
            break;
        case "cancel":
        case "Cancel":
            // redirect back to main record
            $panel = $_POST['link_type'] == 'O' ? 'respondents' : GetContactPanelName($module, $_POST['link_type']);

            $yySetLocation = "$scripturl?action=".($ModuleDefs[$module]['GENERIC'] ? 'record&module='.$module : $ModuleDefs[$module]['ACTION']).
                            "&recordid={$_POST['main_recordid']}&panel=".$panel.($_REQUEST['fromsearch'] ? '&fromsearch=1' : '');
            redirectexit();
            break;
    }

    obExit();
}

function UnlinkContact()
{
    global $scripturl, $yySetLocation, $ModuleDefs;

    LoggedIn();

    $con_recordid = Sanitize::SanitizeInt($_POST["con_recordid"]);
    $main_recordid = Sanitize::SanitizeInt($_POST["main_recordid"]);
    $link_recordid = Sanitize::SanitizeInt($_POST["link_recordid"]);
    $module = Sanitize::getModule($_POST["module"]);

    if ($link_recordid)
    {
        if (is_array($ModuleDefs[$module]['EXTRA_CONTACT_UNLINK_FUNCTIONS']))
        {
            foreach ($ModuleDefs[$module]['EXTRA_CONTACT_UNLINK_FUNCTIONS'] as $func)
            {
                $func(array('data' => $_POST));
            }
        }

        if ($_POST['link_type'] == 'O')
        {
            $mapper = (new src\respondents\model\RespondentModelFactory)->getMapper();
            $respondent = $mapper->find($link_recordid);

            if ($respondent !== null)
            {
                $mapper->delete($respondent);
            }
        }
        else
        {
            $sql = "DELETE FROM link_contacts WHERE link_recordid = :link_recordid";
            DatixDBQuery::PDO_query($sql, array("link_recordid" => $link_recordid));
        }
    }

    $sql = "DELETE FROM contacts_main
        WHERE recordid = :con_recordid AND rep_approved = 'UN'";

    DatixDBQuery::PDO_query($sql, array("con_recordid" => $con_recordid));

    AuditContactUnlink($module, $main_recordid, $con_recordid);

    $panel = $_POST['link_type'] == 'O' ? 'respondents' : GetContactPanelName($module, $_POST['link_type']);
    $yySetLocation = "$scripturl?action=". ($ModuleDefs[$module]['GENERIC'] ? 'record&module='.$module : $ModuleDefs[$module]['ACTION'])."&recordid=$main_recordid&panel=".$panel;

    redirectexit();
}

function ListContactsSection($aParams)
{
    global $scripturl, $ModuleDefs, $ListingDesigns;

    $module = $aParams['module'];
    $data = $aParams['data'];
    $FormType = $aParams['formtype'];

    $UseLists = (isset($aParams['uselists']) ? (boolean) $aParams['uselists'] : true); //Just in case this came from $_POST/$_GET

    // Contacts are stored inside the data array
    $con = $data["con"];
    $recordid = $data["recordid"];

    $AdminUser = $_SESSION["AdminUser"];
    $Perms = GetParm($ModuleDefs[$module]['PERM_GLOBAL']);

    $LinkPerms = (!$_GET["print"] && $FormType != "Print" && $FormType != "ReadOnly" && CanLinkNewContacts());
    $ShowFullContactDetails = ($_GET["print"] && bYN(GetParm("PRINT_CON_DETAILS","Y")) && CanSeeContacts($module, $Perms, $data['rep_approved']));

    if ($UseLists)
    {
        echo '<li name="contacts_row" id="contacts_row">';
    }
    else
    {
        echo '<div name="contacts_row" id="contacts_row">';
    }

    echo '

<input type="hidden" name="contact_url" value="" />
';
    if ($_GET["panel"] == "contacts" && $_GET["contactwarning"])
    {
        echo '
        <div class="new_windowbg">
            <b>The report has been saved. The reference number is ' . $recordid . '</b>
        </div>
        <div class="new_windowbg form_error">
            There are still unapproved contacts attached to this record. They are listed below.
        </div> ';
    }

    $ContactArray = $ModuleDefs[$module]['CONTACTTYPES'];

    foreach ($ContactArray as $Type => $ContactGroup)
    {
        $total = count($con[$Type]);

        echo '
        <div style="margin:5px;">
            <div class="section_title_row row_above_table">
                <b>' . ($ContactGroup["Max"]==1 ? $ContactGroup["Name"] : $ContactGroup["Plural"]) . '</b>
            </div>
        ';

        //*
        require_once 'Source/libs/ListingClass.php';
        $Listing = new Listing('CON', $ListingDesigns['contacts'][$Type]);
        $Listing->LinkModule = $module;
        $Listing->LoadColumnsFromDB();
        $Listing->LoadData($con[$Type]);
        $Listing->EmptyMessage = $ContactGroup["None"];

        echo $Listing->GetListingHTML($FormType);

        if ($LinkPerms && ($ContactGroup["Max"]=="" || $total<$ContactGroup["Max"]))
        {
            $url = $scripturl . '?action='.($ModuleDefs[$module]['GENERIC'] ? 'linkcontactgeneral' : $ModuleDefs[$module]['LINKCONTACTACTION']).'&amp;module='.$module.'&amp;main_recordid='. Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;formtype=' . htmlspecialchars($FormType) . '&amp;link_type=' . htmlspecialchars($Type);

            echo '
            <div class="section_link_row row_below_table">
                <a href="javascript:if(CheckChange()){SendTo(\''.$url.'\');}" ><b>';

            if (_tk('create_new_link_'.$Type))
            {
                echo htmlspecialchars(_tk('create_new_link_'.$Type));
            }
            else
            {
                echo 'Create a new ' . $ContactGroup["Name"] . ' link';
            }

            echo '</b></a>
            </div>';
        }
        echo '
        </div>';
    }

    if ($UseLists)
    {
        echo '
        </li>';
    }
    else
    {
        echo '
        </div>';
    }
}

/**
* Gets the panel name for the contact link type defined in ModuleDefs
*
* Returns the panel name (Default is contacts).
*
* @param string    $Module   Module short name
* @param string    $Link_type  Contact link type
*
* @return string   Panel name.
*/
function GetContactPanelName($module, $Link_type)
{
    global $ModuleDefs;

    if (!empty($ModuleDefs[$module]['CONTACTTYPES'][$Link_type]['PanelName']))
    {
        return $ModuleDefs[$module]['CONTACTTYPES'][$Link_type]['PanelName'];
    }
    else
    {
        return "contacts_type_".$Link_type;
    }

}

