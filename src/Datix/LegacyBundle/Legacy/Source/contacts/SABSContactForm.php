<?php

// Called when the user clicks Cancel on the match list page.
function ReshowContactLink()
{
	$sab_id = $_POST["sab_id"];

	$con = Sanitize::SanitizeRawArray($_POST);
	$con["link_exists"] = false;

	ShowContactForm($con, $sab_id);
}

function ShowContactForm($con = "", $sab_id = "", $error = "")
{
	global $ModuleDefs;

    //Check whether we are in ReadOnly mode

    $FormType = Sanitize::SanitizeString($_GET["formtype"]);

    if ($_SESSION["ReadOnlyMode"])
    {
        $ReadOnly = true;
        $FormType = "ReadOnly";
    }
    else
    {
        $ReadOnly = false;
    }

    if (!$con["con_id"])
    {
        $FormType = "New";
    }

    if ($con["con_id"] != '' && $FormType != "ReadOnly" && $FormType != "Print" && $FormType != "Search"
        && $FormType != "Design" && bYN(GetParm("RECORD_LOCKING","N")))
    {
        require_once 'Source/libs/RecordLocks.php';

        if ($con["con_id"] != '')
        {
            $aReturn = LockRecord(array("link_id" => $con["con_id"], "table" => "CONTACTS_MAIN"));
        }

        if ($aReturn["lock_message"])
        {
            $sLockMessage = $aReturn["lock_message"];
        }

        if ($aReturn["formtype"] == "ReadOnly")
        {
            $FormType = $aReturn["formtype"];
            $ReadOnly = true;
        }
    }

    $LinkTypes = array(
        "S" => "For action by",
        "I" => "Information only",
        "N" => "Contact"
    );

    if ($_GET["linktype"])
    {
        $con["link_type"] = Sanitize::SanitizeString($_GET["linktype"]);
    }

	if ($con["con_subtype"] != "")
    {
        $con["link_status"] = $con["con_subtype"];
    }

    $SABFormLevel = ($_SESSION["Globals"]["SAB_PERMS"] == "SAB1" ? 1 : 2);

    //Always use level 2 contact forms for both SAB1 and SAB2 users.
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'CON', 'parent_module' => 'SAB', 'level' => 2, 'parent_level' => $SABFormLevel, 'link_type' => $con["link_type"]));

    $module = "SAB";
    include($ModuleDefs['CON']['BASIC_FORM_FILES'][2]);

    $SABConTable = new FormTable($FormType, 'CON', $FormDesign);
    $SABConTable->MakeForm($FormArray, $con, 'CON');

    $ButtonGroup = new ButtonGroup();

    // Display different options if showing a link or creating a link
    if (!$con["link_exists"] && !$ReadOnly)
    {
        $HideLinkButton = (bYN(GetParm('REQUIRE_CONTACT_MATCH_CHECK')) && !$con["con_id"]);

        if ($HideLinkButton)
        {
            $JSFunctions[] = 'jQuery(\'#btnSave\').hide();jQuery(\'#icon_link_btnSave\').hide();';
        }

        $ButtonGroup->AddButton(array('label' => 'Check for matching contacts', 'onclick' => ($HideLinkButton ? '$(\'btnSave\').show();jQuery(\'#icon_link_btnSave\').show();' : '').'MatchExistingContacts()', 'action' => 'SEARCH'));
        $ButtonGroup->AddButton(array('label' => 'Create new link', 'id' => 'btnSave', 'name' => 'btnSave', 'onclick' => 'this.disabled=true;document.frmSabsSAB1.form_action.value=\'new\';submitClicked = true;selectAllMultiCodes();if(validateOnSubmit()){document.frmSabsSAB1.submit();}else{this.disabled=false;}', 'action' => 'SAVE'));
        $ButtonGroup->AddButton(array('label' => 'Cancel', 'id' => 'btnCancel', 'name' => 'btnCancel', 'onclick' => 'document.frmSabsSAB1.form_action.value=\'cancel\';'.getConfirmCancelJavascript('frmSabsSAB1'), 'action' => 'CANCEL'));
    }
    else
    {
        if (!$ReadOnly)
        {
            $ButtonGroup->AddButton(array('label' => 'Save', 'id' => 'btnSave', 'name' => 'btnSave', 'onclick' => 'this.disabled=true;selectAllMultiCodes();document.frmSabsSAB1.form_action.value=\'new\';submitClicked = true; if(validateOnSubmit()){document.frmSabsSAB1.submit()}else{this.disabled=false}', 'action' => 'SAVE'));

            if ($con["link_type"] != 'N')
            {
                $ButtonGroup->AddButton(array('label' => 'Update and send email', 'id' => 'btnUpdate', 'name' => 'btnUpdate', 'onclick' => 'this.disabled=true;document.frmSabsSAB1.form_action.value=\'email\';submitClicked = true; if(validateOnSubmit()){document.frmSabsSAB1.submit()}else{this.disabled=false}', 'action' => 'SAVE', 'icon' => 'images/icons/icon_footer_save_email.png'));
            }

            if ($AdminUser = $_SESSION["AdminUser"])
            {
                $ButtonGroup->AddButton(array('label' => 'Unlink contact', 'id' => 'btnUnlink', 'name' => 'btnUnlink', 'onclick' => 'if(confirm(\''._tk('unlink_contact_confirm').'\')){document.frmSabsSAB1.form_action.value=\'unlink\';submitClicked = true;document.frmSabsSAB1.submit()}', 'action' => 'DELETE'));
            }
        }

        $ButtonGroup->AddButton(array('label' => _tk('back_to').' '._tk('SABSName'), 'id' => 'btnBack', 'name' => 'btnBack', 'onclick' => 'document.frmSabsSAB1.form_action.value=\'cancel\';'.getConfirmCancelJavascript('frmSabsSAB1'), 'action' => 'CANCEL'));
    }

    ShowSABConForm($con, $error, $SABConTable, $ButtonGroup, $sLockMessage);
    obExit();
}

function ContactReject()
{
    global $scripturl, $yySetLocation;

    LoggedIn();

    $recordid = Sanitize::SanitizeInt($_POST["conrep_recordid"]);
    $link_id = Sanitize::SanitizeInt($_POST["sab_id"]);
    $table = Sanitize::SanitizeString($_POST["table"]);

    $sql = "UPDATE rep_contacts SET rep_approved = 'D'
        WHERE recordid = :recordid";

    DatixDBQuery::PDO_query($sql, array("recordid" => $recordid));

    $yySetLocation = "$scripturl?action=".($table=="holding" ? $ModuleDefs["SAB"]["HOLDING_URL"] : $ModuleDefs["SAB"]["MAIN_URL"])."&recordid=$link_id&panel=contacts";
    redirectexit();
}

function UnlinkContact()
{
	global $scripturl, $yySetLocation;

	LoggedIn();

	$con_id = Sanitize::SanitizeInt($_POST["con_id"]);
	$sab_id = Sanitize::SanitizeInt($_POST["sab_id"]);
    $SABPerms = $_SESSION["Globals"]["SAB_PERMS"];
    $LinkOwnerId = $_SESSION["contact_login_id"];

	$sql = "DELETE FROM sab_link_contacts
		WHERE con_id = :con_id
		AND sab_id = :sab_id";

    $PDOParams = array("con_id" => $con_id, "sab_id" => $sab_id);

    if ($SABPerms != "SAB2")
    {
        $sql .= " AND link_owner_id = :link_owner_id";
        $PDOParams["link_owner_id"] = $LinkOwnerId;
    }
    else
    {
        $sql .= " AND link_owner_id IS NULL";
    }

    DatixDBQuery::PDO_query($sql, $PDOParams);

    AuditContactUnlink('SAB', $sab_id, $con_id);

	$yySetLocation = "$scripturl?action=sabs&recordid=$sab_id&panel=contacts_type_" . $_POST["link_type"];
	redirectexit();
}

function ContactBatchLink()
{
	global $scripturl, $yySetLocation;

	LoggedIn();

    $SABPerms = $_SESSION["Globals"]["SAB_PERMS"];
    $LinkOwnerId = $_SESSION["contact_login_id"];
	$sab_id = $_SESSION["SAB"]["CURRENTID"];

    $sql = "SELECT contacts_main.recordid as con_id, con_forenames, con_surname
		FROM contacts_main
		WHERE ";

    $sql .= MakeSecurityWhereClause($_SESSION["CON"]["WHERE"], "CON", $_SESSION["initials"]);

	$conresult = db_query($sql);

    $con_type = $_SESSION["SAB"]["CURRENTLINKTYPE"];

	while ($row = db_fetch_array($conresult))
    {
        $con_id = $row["con_id"];

	    // Check if the link already exists.  If so, we just need to update it

	    $sql = "SELECT recordid, con_id, sab_id, link_type, link_owner_id
		FROM sab_link_contacts
		WHERE con_id = $con_id
		AND sab_id = $sab_id";

	    $result = db_query($sql);

	    if ($FetchRow = db_fetch_array($result))
	    {
            if ($FetchRow["link_type"] != $con["link_type"] ||
                ($SABPerms != "SAB2" && $FetchRow["link_owner_id"] != $_SESSION["contact_login_id"]))
            {
                $WarningMsg[] = "The following contacts were already linked to this ". _tk('SABSNameTitle') . " record: ";
                $WarningMsg[] = "";
                $WarningMsg[] = ProcessDuplicateLinkMsg($con_id, $FetchRow["link_owner_id"], $FetchRow["link_type"]);
            }
            else
            {
                $SABLinkID = $fetchrow["recordid"];
		        $sql = "UPDATE sab_link_contacts
			        SET link_type = '$con_type',
			        updateddate = " . UserDateToSQLDate(date('d/m/Y'))
			        . "
			        WHERE recordid = $SABLinkID";
                db_query($sql);
                // Audit this update as an unlink/link pair
                AuditContactLink('SAB', $sab_id, $con_id, $con_type);
            }
	    }
        else
	    {
		    $sql = "INSERT INTO sab_link_contacts
			(con_id, sab_id, link_type". ($SABPerms != 'SAB2' ? ', link_owner_id' : '') .")
			VALUES
			($con_id, $sab_id, '$con_type'". ($SABPerms != 'SAB2' ? ', ' . $LinkOwnerId : '') .")";

            if (!db_query($sql))
            {
                fatal_error("Cannot insert link");
            }

            AuditContactLink('SAB', $sab_id, $con_id, $con_type);
	    }
    }

    if ($WarningMsg != "")
    {
        $_SESSION["SAB"]["WARNING_MESSAGE"] = $WarningMsg;
    }
    else
    {
        $_SESSION["SAB"]["WARNING_MESSAGE"] = "";
    }

    $yySetLocation = "$scripturl?action=sabs&recordid=$sab_id&panel=contacts_type_" . $con_type;
    redirectexit();
}

function SaveContactLinkToMain()
{
	global $scripturl, $yySetLocation, $ModuleDefs;

	LoggedIn();

	$con_id = Sanitize::SanitizeInt($_POST["con_id"]);
	$sab_id = Sanitize::SanitizeInt($_POST["sab_id"]);
    $SABPerms = $_SESSION["Globals"]["SAB_PERMS"];
    $LinkOwnerId = $_SESSION["contact_login_id"];

	if ($sab_id == "")
    {
        fatal_error("No ID");
    }

	$ErrorMark = '<font size="3" color="red"><b>*</b></font>';

    if ($_POST["link_type"] == "")
	{
		$error["message"] .= "<br />You must enter a link type (Link as..)";
		$error["link_type"] = $ErrorMark;
	}

	if ($_POST["con_surname"] == "")
	{
		$error["message"] .= "<br />You must enter a surname.";
		$error["con_surname"] = $ErrorMark;
	}

    if ($_POST["con_dob"] != "")
    {
        require_once 'Date.php';
        $Today = new Date;
        $ConDoB = new Date(UserDateToSQLDate($_POST["con_dob"]));

        if ($Today->Compare($ConDoB, $Today) == 1)
        {
            $error ["message"] .= "<br />Date of birth must be the same as or earlier than today.";
            $error["con_dob"] = $ErrorMark;
        }
    }

    if ($_POST["con_dod"] != "")
    {
        require_once 'Date.php';
        $Today = new Date;
        $ConDoD = new Date(UserDateToSQLDate($_POST["con_dod"]));

        if ($Today->Compare($ConDoD, $Today) == 1)
        {
            $error ["message"] .= "<br />Date of death must be the same as or earlier than today.";
            $error["con_dod"] = $ErrorMark;
        }
    }

	if ($error)
	{
		ShowContactForm(Sanitize::SanitizeRawArray($_POST), $sab_id, $error);
		obExit();
	}

	$con = Sanitize::SanitizeRawArray($_POST);

	// Do we need to create a new contact?
	if ($con_id == "")
	{
		$con_id = GetNextRecordID("contacts_main", true);
		$NewContact = true;
	}

    $con = ParseSaveData(array('module' => 'CON', 'data' => $con));

    $con['rep_approved'] = GetValidApprovalStatusValueFromPOST(array('module' => 'CON'));

    $sql = "UPDATE contacts_main
        SET ";

    $sql .= GeneratePDOSQLFromArrays(array(
            'FieldArray' => $ModuleDefs['CON']['FIELD_ARRAY'],
            'DataArray' => $con,
            'end_comma' => true,
            'Module' => 'CON'
        ),
        $PDOParamsArray
    );

    $PDOParamsArray["updatedby"] = $_SESSION["initials"];
    $PDOParamsArray["updateid"] = GensUpdateID($_POST["updateid"]);
    $PDOParamsArray["updateddate"] = date('d-M-Y H:i:s');
    $PDOParamsArray["con_id"] = $con_id;

    $sql .= " updatedby = :updatedby, updateid = :updateid, updateddate = :updateddate";
    $sql .= " WHERE recordid = :con_id";

    DoFullAudit('CON', 'contacts_main', $con_id, $UpdateMainArray);

	$result = DatixDBQuery::PDO_query($sql, $PDOParamsArray);

    if (!$result)
    {
		fatal_error("Could not insert contact" . $sql);
    }

	// Check if the link already exists.  If so, we just need to update it

	$sql = "SELECT recordid, con_id, sab_id, link_type, link_owner_id
		FROM sab_link_contacts
		WHERE con_id = :con_id
		AND sab_id = :sab_id";

	$FetchRow = DatixDBQuery::PDO_fetch($sql, array('con_id' => $con_id, 'sab_id' => $sab_id));

	if ($FetchRow)
	{
        $UpdateLinkArray = array(
            'link_type' => $con["link_type"],
            'link_role' => $con["link_role"],
            'link_status' => $con["link_status"],
            'link_notes' => $con["link_notes"],
            'link_rsp_type' => $con["link_rsp_type"],
            'link_rsp_date' => $con['link_rsp_date'],
            'link_read_date' => $con['link_read_date'],
            'link_comments' => $con["link_comments"]
        );

        // Check if the user is trying to link the same contact as a different type
        // or the user is a non-admin user trying to link a contact who has already been linked by someone else.
        // In which case the system will warn them that they cannot create/update this link.
        if ($FetchRow["link_type"] != $con["link_type"] || ($SABPerms != "SAB2" && $FetchRow["link_owner_id"] != $_SESSION["contact_login_id"]))
        {
            $WarningMsg[] = "The following contacts were already linked to this ". _tk('SABSNameTitle') . " record: ";
            $WarningMsg[] = "";
            $WarningMsg[] = ProcessDuplicateLinkMsg($con_id, $FetchRow["link_owner_id"], $FetchRow["link_type"]);
        }
        else
        {
		    $SABLinkID = $FetchRow["recordid"];
            DoFullAudit('CON', 'sab_link_contacts', $con_id, $UpdateLinkArray,
                "con_id = $con_id AND sab_id = $sab_id");

            $UpdateSetLink = GeneratePDOSQLFromArrays(array(
                    'FieldArray' => array_keys($UpdateLinkArray),
                    'DataArray' => $UpdateLinkArray,
                    'Module' => 'SAB', //linked fields are stored under SAB
                    'end_comma' => true
                ),
                $bindParams
            );

            $sql = "UPDATE sab_link_contacts
                SET $UpdateSetLink
                updateddate = '" . date('Y-m-d H:i:s') . "',
                updatedby = '$_SESSION[initials]'
                WHERE recordid = $SABLinkID";

            DatixDBQuery::PDO_query($sql, $bindParams);
        }
	}
	else
	{
        $sql = "INSERT INTO sab_link_contacts ";

        $InsertLinkArray = array(
            'con_id' => $con_id,
            'sab_id' => $sab_id,
            'link_type' => $con["link_type"],
            'link_role' => $con["link_role"],
            'link_status' => $con["link_status"],
            'link_notes' => $con["link_notes"],
            'link_rsp_type' => $con["link_rsp_type"],
            'link_rsp_date' => $con['link_rsp_date'],
            'link_read_date' => $con['link_read_date'],
            'link_comments' => $con["link_comments"]
        );

        if ($SABPerms != 'SAB2')
        {
            $InsertLinkArray['link_owner_id'] = $LinkOwnerId;
        }

        $sql .= GeneratePDOInsertSQLFromArrays(array(
                'Module' => "SAB",
                'FieldArray' => array_keys($InsertLinkArray),
                'DataArray' => $InsertLinkArray
            ),
            $PDOParamsArray
        );

		if (!DatixDBQuery::PDO_query($sql, $InsertLinkArray))
        {
            fatal_error("Cannot insert link");
        }

        AuditContactLink('SAB', $sab_id, $con_id, $con['link_type']);
	}

    require_once 'Source/sabs/SaveSABS.php';
    
    // save udf values
    require_once 'Source/libs/UDF.php';
    SaveUDFs($con_id, MOD_CONTACTS);


    // Save Progress Notes
    $loader = new src\framework\controller\Loader();
    $controller = $loader->getController(array(
        'controller' => 'src\progressnotes\controllers\ProgressNotesController'
    ));
    $controller->setRequestParameter('module', 'CON');
    $controller->setRequestParameter('data', $con);
    echo $controller->doAction('saveProgressNotes');

    if ($_POST["form_action"] != 'email')
    {
        if (!empty($WarningMsg))
        {
            $_SESSION["SAB"]["WARNING_MESSAGE"] = $WarningMsg;
        }
        else
        {
            $_SESSION["SAB"]["WARNING_MESSAGE"] = "";
        }

        $yySetLocation = "$scripturl?action=sabs&recordid=$sab_id&panel=contacts_type_" . $con['link_type'];
        redirectexit();
    }
}

function ShowDistributionContactForm($con = "", $sab_id = "", $error = "")
{
	global $scripturl, $dtxtitle;

    $sab_id = $_GET["sab_id"];
    $con["link_type"] = Sanitize::SanitizeString($_GET["linktype"]);

    // Check whether we are in ReadOnly mode
    if ($_SESSION["ReadOnlyMode"])
    {
        $ReadOnly = true;
        $FormType = 'ReadOnly';
    }

    $Where =  "((dst_private != 'Y' OR dst_private IS NULL OR dst_private = '')
                OR (dst_owner = ". $_SESSION["contact_login_id"] . " OR dst_owner IS NULL))";

    $DSTWhere = MakeSecurityWhereClause($Where, 'DST', $_SESSION['initials']);

    $sql = "select distribution_lists.recordid AS dst_id, dst_name
            from distribution_lists".($DSTWhere ? ' WHERE '.$DSTWhere : '');

    $result = db_query($sql);
    $DistributionTypes = array();

    while ($row = db_fetch_array($result))
	{
         $DistributionTypes[$row["dst_id"]] = $row["dst_name"];
    }

    $LinkTypes = array(
        "S" => "For action by",
        "I" => "Information only",
        "N" => "Contact"
    );

	if ($con["link_status"] != "")
    {
        $con["con_subtype"] = $con["link_status"];
    }

    $field = Forms_SelectFieldFactory::createSelectField('dst_id', 'SAB', $con["dst_id"], $FormType);
    $field->setCustomCodes($DistributionTypes);
    $field->setSuppressCodeDisplay();
    $ContactLinkingOptions[] = array("Title" => "Distribution lists","Type" => "formfield","FormField" => $field);

    $field2 = Forms_SelectFieldFactory::createSelectField('link_type', 'SAB', $con["link_type"], $FormType);
    $field2->setCustomCodes($LinkTypes);
    $field2->setSuppressCodeDisplay();
    $ContactLinkingOptions[] = array("Title" => "Link as","Type" => "formfield","FormField" => $field2);

    $FormArray = array(
        "Parameters" => array(
            "Panels" => "True",
            "Condition" => false
        ),
        "header" => array(
            "Title" => "Distribution list linking options",
            "Rows" => $ContactLinkingOptions
        )
    );

    $SABConTable = new FormTable($FormType, 'CON');
    $SABConTable->MakeForm($FormArray, $con, 'CON');

    $ButtonGroup = new ButtonGroup();
    $ButtonGroup->AddButton(array('label' => _tk('link_distribution_list_contacts'), 'onclick' => 'document.frmSabsSAB1.form_action.value=\'distrib\';submitClicked=true;document.frmSabsSAB1.submit();', 'action' => 'SAVE'));
    $ButtonGroup->AddButton(array('label' => _tk('btn_cancel'), 'onclick' => 'document.frmSabsSAB1.form_action.value=\'cancel\';'.getConfirmCancelJavascript('frmSabsSAB1'), 'action' => 'CANCEL'));

    ShowSABConForm($con, $error, $SABConTable, $ButtonGroup);

    obExit();
}

// Shows a contact form for the Safety Alerts module.  Can also be used to
// display distribution list linking form
// Parameters:
//
// $con:            Data array
// $error:          Any error message returned by a previous save operation
// $Table:          A FormTable object containing the body of the form
// $Buttons:        A string containing HTML for the buttons
// $LockMessage:    A locking error message
function ShowSABConForm($con = "", $error = "", $Table = '', $Buttons, $LockMessage = '', $FormDesign = '')
{
	global $scripturl, $dtxtitle;

    $LinkTypes = array(
        "S" => "For action by",
        "I" => "Information only",
        "N" => "Contact"
    );

    $sab_id = is_numeric($_REQUEST["sab_id"]) ? (int) $_REQUEST["sab_id"] : "";

    if ($_SESSION["ReadOnlyMode"])
    {
        $ReadOnly = true;
    }

	if ($con["link_exists"])
    {
		$dtxtitle = $LinkTypes[$con["link_type"]];
    }
	else
    {
		$dtxtitle ="Add Contact Link";
    }

    GetPageTitleHTML(array(
        'title' => $dtxtitle,
        'module' => 'SAB'
    ));

    $con_recordid = isset($con['con_id']) ? (int) $con['con_id'] : "";
    GetSideMenuHTML(array('module' => 'CON', 'table' => $Table, 'buttons' => $Buttons, 'recordid' => $con_recordid, 'no_audit' => true, 'print_link_params' => array('linktype' => $con["link_type"], 'from_module' => 'SAB' )));

    template_header_nopadding();

?>
    <script type="text/javascript" language="javascript">
        function MatchExistingContacts()
        {
            document.frmSabsSAB1.form_action.value = 'link';
            var url = '<?= $scripturl . "$scripturl?action=sabscontactlinkaction&amp;formtype=" . $FormType . "&token=" . CSRFGuard::getCurrentToken() ?>';
            document.frmSabsSAB1.target = "wndMatchExisting";
            document.frmSabsSAB1.onsubmit = "window.open(url, 'wndMatchExisting', 'dependent,menubar=false,screenX=200,screenY=330,titlebar,scrollbars,resizable')";
            document.frmSabsSAB1.submit();

            document.frmSabsSAB1.target = "";
            document.frmSabsSAB1.form_action.value = "new";
        }

        <?php echo MakeJavaScriptValidation(); ?>
        var submitClicked = false;
     </script>
<?php

    $FormType = Sanitize::SanitizeString($_GET["formtype"]);

    echo '<script language="javascript" type="text/javascript">var submitClicked = false;</script>';
    echo '<form method="post" name="frmSabsSAB1" onsubmit="return(submitClicked);" action="' . $scripturl . '?action=sabscontactlinkaction&amp;formtype=' . $FormType . '">';

    echo '
<input type="hidden" name="sab_id" value="' . $sab_id . '" />
<input type="hidden" name="con_id" value="' . (is_numeric($con["con_id"]) ? (int) $con["con_id"] : "") . '" />
<input type="hidden" name="updateid" value="' . htmlspecialchars($con['updateid']) . '" />
<input type="hidden" name="main_recordid" value="' . (is_numeric($con["main_recordid"]) ? (int) $con["main_recordid"] : "" ). '" />
<input type="hidden" name="formtype" value="' . $FormType . '" />
<input type="hidden" name="module" value="SAB" />
<input type="hidden" name="form_action" value="match" />
<input type="hidden" name="rep_approved" value="'.htmlspecialchars($con['rep_approved']).'" /> <!--htmlspecialchars used here as used in previous lines-->
<input type="hidden" name="rep_approved_old" value="'.htmlspecialchars($con['rep_approved']).'" /><!--htmlspecialchars used here as used in previous lines-->';

    if ($_GET["action"] != "sabslinkdistribution")
    {
        echo '<input type="hidden" name="link_type" value="' . htmlspecialchars($con['link_type']) . '" />';
    }

    if ($error['message'])
    {
        echo '<div class="form_error">'.htmlspecialchars(_tk('form_errors')).'</div>';
        echo '<div class="form_error">'.htmlspecialchars($error['message']).'</div>';
    }

    if ($LockMessage)
    {
        echo '
        <div class="lock_message">
            <div id="LockMessage">'.$LockMessage.'</div>
            <div id="userLockMessage"></div>
        </div>';
    }

    $Table->MakeTable();
    echo $Table->GetFormTable();

    echo $Buttons->getHTML();

    echo '
    </form>';

    if ($FormType != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $Table);
    }

    footer();

    obExit();
}

function ContactDistributionLink()
{
	global $scripturl, $yySetLocation;

	LoggedIn();

    $SABPerms = $_SESSION["Globals"]["SAB_PERMS"];

    if ($con_id == "")
    {
        $con_id = Sanitize::SanitizeInt($_POST["con_id"]);
    }

    $sab_id = Sanitize::SanitizeInt($_POST["sab_id"]);
    $dst_id = Sanitize::SanitizeInt($_POST["dst_id"]);
    $LinkOwnerId = $_SESSION["contact_login_id"];

	if ($sab_id == "")
    {
        fatal_error("No ID");
    }

	$ErrorMark = '<font size="3" color="red"><b>*</b></font>';

    if ($_POST["dst_id"] == "")
	{
		$error["message"] .= "<br />You must enter a distribution list";
		$error["dst_id"] = $ErrorMark;
	}

    if ($_POST["link_type"] == "")
	{
		$error["message"] .= "<br />You must enter a link type (Link as..)";
		$error["link_type"] = $ErrorMark;
	}

	if ($error)
	{
		ShowDistributionContactForm(Sanitize::SanitizeRawArray($_POST), $sab_id, $error);
		obExit();
	}

    $sql = "SELECT contacts_main.recordid as con_id
		FROM contacts_main
		WHERE contacts_main.recordid in
        (SELECT con_id FROM link_contacts
        WHERE DST_ID = :dst_id)";

    $con_type = Sanitize::SanitizeString($_POST["link_type"]);
    $conresult = DatixDBQuery::PDO_fetch_all($sql, array("dst_id" => $dst_id));

	foreach ($conresult as $row)
    {
        $con_id = $row["con_id"];

	    // Check if the link already exists.  If so, we just need to update it
	    $sql = "SELECT recordid, con_id, sab_id, link_type, link_owner_id
		FROM sab_link_contacts
		WHERE con_id = $con_id
		AND sab_id = $sab_id";

	    $result = db_query($sql);

	    if ($FetchRow = db_fetch_array($result))
	    {
            if ($FetchRow["link_type"] != $con["link_type"] ||
                ($SABPerms != "SAB2" && $FetchRow["link_owner_id"] != $_SESSION["contact_login_id"]))
            {
                $WarningMsg[] = "The following contacts were already linked to this ". _tk('SABSNameTitle') . " record: ";
                $WarningMsg[] = "";
                $WarningMsg[] = ProcessDuplicateLinkMsg($con_id, $FetchRow["link_owner_id"], $FetchRow["link_type"]);
            }
            else
            {
                $SABLinkID = $FetchRow["recordid"];
                $sql = "UPDATE sab_link_contacts
                    SET link_type = '$con_type',
                    updateddate = " . UserDateToSQLDate(date('d/m/Y'))
                    . " WHERE recordid = $SABLinkID";
		            db_query($sql);
            }
	    }
        else
	    {
            $sql = "INSERT INTO sab_link_contacts
            (con_id, sab_id, link_type". ($SABPerms != 'SAB2' ? ', link_owner_id' : '') .")
            VALUES
            ($con_id, $sab_id, '$con_type'". ($SABPerms != 'SAB2' ? ', ' . $LinkOwnerId : '') .")";

            if (!db_query($sql))
            {
                fatal_error("Cannot insert link");
            }

            AuditContactLink('SAB', $sab_id, $con_id, $con_type);
	    }
    }

    if (!empty($WarningMsg))
    {
        $_SESSION["SAB"]["WARNING_MESSAGE"] = $WarningMsg;
    }
    else
    {
        $_SESSION["SAB"]["WARNING_MESSAGE"] = "";
    }

    $yySetLocation = "$scripturl?action=sabs&recordid=$sab_id&panel=contacts_type_" . $con_type;
    redirectexit();
}

function ProcessDuplicateLinkMsg($con_id, $link_owner_id, $link_type)
{
    $WarningMsg .= GetContactFullname($con_id) . " (" . $con_id . ")";

    if ($link_owner_id)
    {
        $Owner_fullname = GetContactFullname($link_owner_id);
        $WarningMsg .= "(By " . $Owner_fullname . ")";
    }
    else // Non admin user
    {
        $WarningMsg .= "(By Alert Administrator)";
    }

    $LinkTypes = array(
        "S" => "For action by",
        "I" => "Information only",
        "N" => "Contact"
    );

    $WarningMsg .= "(" . $LinkTypes[$link_type] . ")";

    return $WarningMsg;
}

?>