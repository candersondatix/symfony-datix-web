<?php

/*
 * This form array is used to generate the forms where a simpler implementation
 * of the Contact Form is required, with just the link and contact fields, and without sections
 *
 * It is used in the functions get_contact_section() and ShowFullContactDetails()
 */

if ($module)
{
    $Module = $module; // because this will get called from multiple files
}

require_once "Source/incidents/Injuries.php";


$linkDetailsSectionForNormalContacts = array(
    "link_role",
    "link_status",
    "link_notes",
    "link_ref",
    "link_dear",
    "link_npsa_role",
    "link_deceased",
    "link_age",
    "link_age_band",
    "link_occupation",
    "link_riddor",
    "link_is_riddor",
    "link_daysaway",
    "icon_cost",
    "link_mhact_section",
    "link_mhcpa",
    array('Name' => 'link_legalaid', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($Type == 'M' || $Type == 'A')))),
    array('Name' => 'link_lip', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($Type == 'M' || $Type == 'A')))),
    array('Name' => 'link_ndependents', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($Type == 'M' || $Type == 'A')))),
    array('Name' => 'link_agedependents', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($Type == 'M' || $Type == 'A')))),
    array('Name' => 'link_marriage', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($Type == 'M' || $Type == 'A')))),
    array('Name' => 'link_injuries', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && ($Type == 'M' || $Type == 'A')))),
    array('Name' => 'link_resp', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && $Type == 'E'))),
    array(
        "Name" => "link_abs_start",
        "Title" => "Absence start",
        "Type" => "date",
        'onChangeExtra' => 'getAbsenceDays(\''.GetParm("FMT_DATE_WEB").'\', '.$Suffix.')'),
    array(
        "Name" => "link_abs_end",
        "Title" => "Absence end",
        "Type" => "date",
        'onChangeExtra' => 'getAbsenceDays(\''.GetParm("FMT_DATE_WEB").'\', '.$Suffix.')'),
    'link_date_admission',
    array('Name' => 'link_plapat', 'Condition' => ($FormType == "Design" || ($Module == 'CLA' && $Type == 'M'))),
    array('Name' => 'lcom_iscomplpat', 'Condition' => ($FormType == "Design" || ($Module == 'COM' && $Type == 'C'))),
    array('Name' => 'lcom_dreceived', 'Condition' => ($FormType == "Design" || ($Module == 'COM' && $Type == 'C'))),
    array('Name' => 'link_patrelation', 'Condition' => ($FormType == "Design" || ($Module == 'COM' && $Type == 'C') || ($Module == 'CLA' && ($Type == 'M' || $Type == 'A')))),
    array('Name' => 'lcom_primary', 'Condition' => ($FormType == "Design" || ($Module == 'COM' && $Type == 'C'))),
    array('Name' => 'link_notify_progress', 'Condition' => ($FormType == "Design" || ($Role && $Role == GetParm('REPORTER_ROLE', 'REP')))),
);


$linkDetailsSectionForRespondentsContacts = array (
    'link_notes',
    'link_resp',
    'link_role'
);


$FormArray = array(
    "Parameters" => array(
        "Condition" => false,
        "Suffix" => $Suffix
    ),
    ($SectionName && $FormType != 'Design' ? $SectionName : 'contact') => array(
        "Title" => ($Title ? $Title : 'Contact Details'),
        "ContactSuffix" => $Suffix,
        "LinkType" => $Type,
        "right_hand_link" => $RightHandLink,
        "LinkRole" => $Role,
        'ContactsLinkTable' => ($Type == 'O' || $Type == 'G' ? 'link_respondents' : 'link_contacts'),
        "Rows" => array_merge (($Type == 'O' ? $linkDetailsSectionForRespondentsContacts : $linkDetailsSectionForNormalContacts), array(
            array("Name" => "recordid", "Title" => "Contact ID","Type" => "string", "ReadOnly" => (($form_action == "Design" || $form_action == "search") ? false : true)),
            "con_title",
            "con_forenames",
            "con_surname",
            "con_jobtitle",
            "con_organisation",
            "con_email",
            "con_address",
            "con_postcode",
            "con_gender",
            "con_dopened",
            "con_dclosed",
            "con_dob",
            "con_dod",
            array(
                "Name" => "con_tel1",
                "Type" => "string",
                "Title" => "Telephone no. 1",
                "Width" => 30,
                "MaxLength" => 254
            ),
            array(
                "Name" => "con_tel2",
                "Type" => "string",
                "Title" => "Telephone no. 2",
                "Width" => 30,
                "MaxLength" => 254
            ),
            "con_fax",
            "con_number",
            "con_nhsno",
            "con_police_number",
            "con_ethnicity",
            "con_language",
            "con_disability",
            'con_religion',
            'con_sex_orientation',
            'con_work_alone_assessed',
            "con_type",
            "con_subtype",
            "con_notes",
            "con_orgcode",
            "con_unit",
            "con_clingroup",
            "con_directorate",
            "con_specialty",
            "con_loctype",
            "con_locactual",
            "con_empl_grade",
            "con_payroll",
            array(
                "Name" => "show_injury",
                "Condition" => ($Module == "INC" || $FormType == 'Design')
            )
        ))
    ),
    "injury_section".($Suffix ? '_'.$Suffix :'') => array(
        "Title" => "Injury Details",
        "ContactSuffix" => $Suffix,
        'Special' => 'SectionInjuryDetails',
        "Condition" => ($Module == "INC" || $FormType == 'Design'),
        "LinkType" => $Type,
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "ContactSubForm" => true,
        "Rows" => array(
            array('Name' => 'dum_injury', 'Title' => 'Injury', 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_bodypart', 'Title' => 'Body part', 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_treatment', 'Title' => 'Treatment', 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true)
        )
    ),
    'sirs'.($Suffix ? '_'.$Suffix :'') => array(
        'Title' => 'Security Incident Details',
        'Condition' =>  ($Module == "INC" || $FormType == 'Design'),
        "ContactSuffix" => $Suffix,
        'Rows' => array(
            "link_worked_alone",
            "link_become_unconscious",
            "link_req_resuscitation",
            "link_hospital_24hours",
            "link_clin_factors",
            "link_direct_indirect",
            "link_injury_caused",
            "link_attempted_assault",
            "link_discomfort_caused",
            "link_public_disorder",
            "link_verbal_abuse",
            "link_harassment",
            "link_police_pursue",
            "link_police_persue_reason",
            "link_pprop_damaged",
        )
    ),
    "property_section".($Suffix ? '_'.$Suffix :'') => array(
        "Title" => "Personal Property",
        "ContactSuffix" => $Suffix,
        'Special' => 'SectionPropertyDetails',
        "Condition" => ($Module == "INC" || $FormType == 'Design'),
        "LinkType" => $Type,
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "Rows" => array(
            array('Name' => 'dum_description', 'Title' => Labels_FormLabel::GetFormFieldLabel('ipp_description'), 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_property_type', 'Title' => Labels_FormLabel::GetFormFieldLabel('ipp_damage_type'), 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true),
            array('Name' => 'dum_value', 'Title' => Labels_FormLabel::GetFormFieldLabel('ipp_value'), 'NoOrder' => true, 'NoExtraText' => true, 'NoHelpText' => true)
        )
    ),
    "udf".($Suffix ? '_'.$Suffix :'') => array(
        "ExtraFields" => $DIF1UDFGroups,
        "NoFieldAdditions" => true,
        "ContactSuffix" => $Suffix,
        "RecordID" => ($con["recordid"])
    )
);

?>