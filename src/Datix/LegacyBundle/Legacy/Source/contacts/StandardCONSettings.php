<?php
$GLOBALS['FormTitle'] = 'Datix Contact Form';

$GLOBALS["DefaultValues"] = array(
    "link_type" => $con["link_type"],
    'lcom_iscomplpat' => 'Y',
    );
$GLOBALS["MandatoryFields"] = array(
        "link_role" => "link",
        "con_surname" => "contact",
        "con_organisation" => "contact"
        );

$GLOBALS["HideFields"] = array(
    "icon_cost" => true,
    "link_worked_alone" => true,
    "link_become_unconscious" => true,
    "link_req_resuscitation" => true,
    "link_hospital_24hours" => true,

    "con_police_number" => true,
    "con_work_alone_assessed" => true,

    'sirs' => true,
    'property_section' => true,

    'progress_notes' => true,
    'additional_info' => true,
    'documents' => true,
    'link_age_band' => true,
    'link_date_admission' => true,
    'link_notify_progress' => true,
    );

$GLOBALS["NewPanels"] = array (
  'link' => true,
  'contact' => true,
  'employee' => true,
  'risks' => true,
  'incidents' => true,
  'pals' => true,
  'complaints' => true,
  'word' => true,
  'progress_notes' => true,
);
$GLOBALS["UserExtraText"] = array (
    'con_dod' => 'Fill this in for a patient who has died',
);

$GLOBALS["ExpandFields"] = array (
  'lcom_iscomplpat' =>
  array (
    0 =>
    array (
      'field' => 'link_patrelation',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'N',
      ),
    ),
  ),
  'link_police_pursue' =>
  array (
    0 =>
    array (
      'field' => 'link_police_persue_reason',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'N',
      ),
    ),
  ),
  'link_plapat' =>
  array (
    0 =>
    array (
      'field' => 'link_patrelation',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'N',
      ),
    ),
  ),
);

$GLOBALS["ExpandSections"] = array (
  'show_injury' =>
  array (
    0 =>
    array (
      'section' => 'injury',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'link_pprop_damaged' =>
  array (
    0 =>
    array (
      'section' => 'property_section',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_document' =>
  array (
    0 =>
    array (
      'section' => 'documents',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
);
?>
