<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '4'),
    "con_surname" => array(
        'width' => '6'),
    "con_forenames" => array(
        'width' => '10'),
    "con_type" => array(
        'width' => '4'),
    "con_subtype" => array(
        'width' => '7'),
);
?>

