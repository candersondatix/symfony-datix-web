<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */

$FormArray = array(
        'Parameters' => array('Panels' => true,
            'Condition' => false),
        "response" => array(
            "Title" => "Response",
            'NewPanel' => true,
            "Condition" => ($con["link_exists"] && $con["link_type"] == "S"),
            "Rows" => array(
                "link_read_date",
                "link_rsp_type",
                "link_rsp_date",
                "link_comments")),
        "contact" => array("Title" => "Contact details",
            "Rows" => array(
            "con_type",
            "con_subtype",
            "con_number",
            "con_title",
            "con_forenames",
            "con_surname",
            "con_gender",
            "con_dob",
            "con_language",
            "con_address",
            "con_postcode",
            "con_jobtitle",
            "con_tel1",
            "con_email",
            "link_notes",
            )),
    "employee" => array("Title" => "Employee details",
        'NewPanel' => true,
        'ReadOnly' => true,
        "Rows" => array(
            "con_orgcode",
            "con_clingroup",
            "con_directorate",
            "con_unit",
            "con_loctype",
            "con_locactual",
            "con_specialty",
            "con_empl_grade",
            "con_payroll")
        )
    );