<?php

/*
 * This form array is used to generate the form for the user registration section.
 * It has no panels and no link fields.
 * 
 * It is used in the functions get_contact_section() and ShowFullContactDetails()
 */ 

require_once 'Source/incidents/Injuries.php';

$FormArray = array(
    "Parameters" => array(
        "Condition" => false,
        "Suffix" => $Suffix
    ),
    'details' => array(
        'Title' => 'User details',
        'Rows' => array(
            'login',
            array('Name' => 'sta_domain', 'Condition' => ($staff["sta_sid"] != "")),
            array('Name' => 'sta_sid', 'Condition' => ($staff["sta_sid"] != "")),
            array('Name' => 'initials', 'ReadOnly' => ($staff['con_id'])),
            array('Name' => 'password', 'Condition' => ($staff["sta_sid"] == "")),
            array('Name' => 'retype_password', 'Condition' => ($staff["sta_sid"] == "")),
            'sta_pwd_change',
            'sta_daccessstart',
            'sta_daccessend',
            'lockout',
			'con_staff_include'
        )
    ),
    'contact' => array(
        "Title" => ($Title ? $Title : 'Contact Details'),
        "right_hand_link" => $RightHandLink,
        "Rows" => array(
            array("Name" => "recordid", "Title" => "Contact ID","Type" => "string", "ReadOnly" => (($form_action == "Design" || $form_action == "search") ? false : true)),
            "con_title",
            array("Name" => 'con_forenames', 'SetOnce' => true),
            array("Name" => 'con_surname', 'SetOnce' => true),
            "con_jobtitle",
            "con_organisation",
            array("Name" => 'con_email', 'SetOnce' => true),
            "con_address",
            "con_postcode",
            "con_gender",
            "con_dopened",
            "con_dclosed",
            "con_dob",
            "con_dod",
            "con_tel1", 
            "con_tel2", 
            "con_fax",
            "con_number",
            "con_nhsno",
            "con_ethnicity",
            "con_language",
            "con_disability",
            "con_type",
            "con_subtype",
            "con_notes",
        )
    ),
    'employee' => array(
        'Title' => _tk('employee_user_title'),
        'Rows' => array(
            'con_orgcode',
            'con_unit',
            'con_clingroup',
            'con_directorate',
            'con_specialty',
            'con_loctype',
            'con_locactual',
            'con_empl_grade',
            'con_payroll'
        )
    ),
    "udf" => array(
        "ExtraFields" => $DIF1UDFGroups,
        "NoFieldAdditions" => true,
        "RecordID" => $con["recordid"]
    ),
    'admin' => array(
        'Title' => 'Administrative options',
        'Condition' => ($_SESSION['logged_in'] && $_GET['action'] == 'viewregistrationform'),
        'Rows' => array(
            array(
                "Name" => "con_action", 
                "Type" => "array_select", 
                "Title" => _tk('registration_action'),
                'IgnoreReadOnly' => true, 
                'AlwaysMandatory' => true,
                'SelectArray' => array('APP' => _tk('approve_registration'), 'REJ' => _tk('reject_registration'))
            ),
            array(
                "Name" => "con_reason", 
                "Type" => "string", 
                "Title" => _tk('registration_reason'),
                'IgnoreReadOnly' => true,
                "Width" => 64,
                "MaxLength" => 254
            )
        )
    )
);