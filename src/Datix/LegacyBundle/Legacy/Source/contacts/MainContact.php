<?php

function ShowHoldingForm($con = "", $FormAction = "", $formlevel = "")
{
    global $Show_all_section, $scripturl, $yySetLocation;

    $yySetLocation = $scripturl . '?action=newcontact&form_action=' . $FormAction;
    redirectexit();
}

function SaveContactLink($form_action = "")
{
	global $scripturl, $yySetLocation, $ModuleDefs;

	LoggedIn();

    if ($form_action == "addnew")
    {
        $addnew = true;
    }
    elseif ($form_action == "editupdate")
    {
        $editupdate = true;
        $con_id = $_POST["recordid"];
    }

    $ErrorMark = '<font size="3" color="red"><b>*</b></font>';

    /*
    * rep_approved should always be passed in $_POST when saving a record. This lets us know where to save the record.
    * We need to check the value passed is valid before saving.
    */
    if (isset($_POST['rep_approved']) && $_POST['rep_approved'] != ''
        && checkApprovalStatusTransferLegitimate(array(
            'to' => $_POST['rep_approved'],
            'from' => $_POST['rep_approved_old'],
            'perm' => GetAccessLevel('CON'),
            'module' => 'CON'
        )))
    {
        //no further action needed
    }
    else
    {
        //invalid or missing value - needs to be set automatically;
        $_POST['rep_approved'] = getDefaultRepApprovedValue(array(
            'data' => $_POST,
            'perms' => GetAccessLevel('CON'),
            'module' => 'CON'
        ));
    }

    GetSectionVisibility('CON', 2, $_POST);
    BlankOutPostValues('CON', 2, null, null, $this->request);

    $ValidationErrors = ValidateContactData(array());

    if ($ValidationErrors)
    {
        foreach ($ValidationErrors as $field => $error)
        {
            AddValidationMessage($field, $error);
        }
        $error['Validation'] = $ValidationErrors;
    }

    $con = Sanitize::SanitizeStringArray($_POST);

    if ($error)
    {
        AddSessionMessage('ERROR', _tk('missing_data_error'));
        $_POST['error'] = $error;

        //Need to reformat dates to ensure they are in SQL format to be interpreted when reloading the form.
        $contactDateFields = array_merge(GetAllFieldsByType('CON', 'date'), getAllUdfFieldsByType('D', '', $con));

        foreach ($contactDateFields as $contactDate)
        {
            if (array_key_exists($contactDate, $con) && $con[$contactDate] != '')
            {
                $con[$contactDate] = UserDateToSQLDate($con[$contactDate]);

                // Some extra field dates look directly at the post value, so need to blank it out here.
                unset($_POST[$contactDate]);
            }
        }

        if ($addnew)
        {
            $doaction = "newcontact";
        }
        elseif ($editupdate)
        {
            $doaction = "editcontact";
            $yySetLocation = $scripturl . '?action=editcontact&recordid='.$con['recordid'];
            redirectexit();
        }
        else
        {
            $doaction = "";
        }

        $loader = new src\framework\controller\Loader();
        $controller = $loader->getController(array(
            'controller' => 'src\\contacts\\controllers\\ShowContactFormTemplateController',
        ));
        $controller->setRequestParameter('data', $con);
        echo $controller->doAction('newcontact');
        obExit();

	}

    if (bYN(GetParm("RECORD_LOCKING","N")) && $con_id)
    {
        require_once "Source/libs/RecordLocks.php";
        UnlockRecord(array("table" => "CONTACTS_MAIN", "link_id"=> $con_id));
    }

	// Do we need to create a new contact?
	if ($con_id == "")
	{
		$con_id = GetNextRecordID("contacts_main", true);
		$NewContact = true;
	}

    DoFullAudit('CON', 'contacts_main', $con_id);

    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'CON', 'level' => 2));
    $FormDesign->LoadFormDesignIntoGlobals();

    $con = ParseSaveData(array('module' => 'CON', 'data' => $con));

    require_once "Source/libs/UDF.php";
    SaveUDFs($con_id, MOD_CONTACTS);

    // Save Progress Notes
    $loader = new src\framework\controller\Loader();
    $controller = $loader->getController(array(
        'controller' => 'src\progressnotes\controllers\ProgressNotesController'
    ));
    $controller->setRequestParameter('module', 'CON');
    $controller->setRequestParameter('data', $con);
    echo $controller->doAction('saveProgressNotes');

    if($NewContact)
    {
        $con["con_dopened"] = date('Y-m-d H:i:s');
    }

    $sql = "UPDATE contacts_main SET ";

    $sql .= GeneratePDOSQLFromArrays(array(
            'FieldArray' => $ModuleDefs['CON']['FIELD_ARRAY'],
            'DataArray' => $con,
            'end_comma' => true,
            'Module' => 'CON'
        ),
        $PDOParamsArray
    );

	$sql .= " updateid = '" . GensUpdateID($_POST["updateid"]) . "',
              updateddate = '" . date('d-M-Y H:i:s') . "',
              updatedby = '". $_SESSION["initials"]."'

		WHERE recordid = :con_id
		AND (updateid = :updateid OR updateid IS NULL)";

    $PDOParamsArray['con_id'] = $con_id;
    $PDOParamsArray['updateid'] = $_POST['updateid'];
    $result = DatixDBQuery::PDO_query($sql, $PDOParamsArray);

	if (!$result)
    {
		fatal_error("Could not insert contact" . $sql);
    }
    else
    {
        $_SESSION["contactname"] = $con["con_title"] ." ". $con["con_forenames"] ." ". $con["con_surname"];
        $_SESSION["contactid"] = $con_id;
        $yySetLocation = "$scripturl?action=showsavecontact";
        redirectexit();
    }
}

?>