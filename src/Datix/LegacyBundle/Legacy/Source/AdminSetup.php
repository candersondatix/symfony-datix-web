<?php

function ChangeFieldSection()
{
    require_once 'Source/libs/FormClasses.php';

    $Sections = ($_SESSION["FieldAdditionsSectionsSetup"] ? $_SESSION["FieldAdditionsSectionsSetup"] : array());

    if ($Sections[$_POST["originalsection"]] && !$_POST['udf'])
    {
        $Sections[$_POST["originalsection"]] .= ' [Default section]';
    }

    if ($Sections[$_POST["section"]])
    {
        $Sections[$_POST["section"]] .= ' [Current section]';
    }

    foreach ($Sections as $SectionKey => $SectionName)
    {
        if ($_SESSION['RecursionLimitations']['movement'][$_POST["inputfield"]] && in_array($SectionKey, $_SESSION['RecursionLimitations']['movement'][$_POST["inputfield"]]))
        {
            // cannot move to this section because it could cause a recursion error.
            unset($Sections[$SectionKey]);
        }
    }
    
    // if the field has display locked than we need to clear sections from dropdown that have sections actions
    if (isset($_GET['lockdisplay']) && $_GET['lockdisplay'] == '1' && is_array($_SESSION['ExpandSections']))
    {
    	foreach ($_SESSION['ExpandSections'] as $Field)
    	{
    		foreach ($Field as $row)
    		{
    			$SectionToExpand = $row['section'];
    			if (isset($Sections[$SectionToExpand]))
    			{
    				unset($Sections[$SectionToExpand]);
    			}
    		}
    	}
    }

    $TargetDropdown = Forms_SelectFieldFactory::createSelectField('sections', '', $_POST["section"], '');
    $TargetDropdown->setCustomCodes($Sections);

    echo '<script language="JavaScript" type="text/javascript">
function setReturns()
{
    var sectionListBox = document.getElementById("sections");

    var valuesReturn = "";

    if(sectionListBox.value || sectionListBox.selectedIndex)
    {
        $("CHANGEDSECTION-'. Sanitize::SanitizeString($_POST["originalsection"]).'-' .  Sanitize::SanitizeString($_POST["inputfield"]) . '").value = sectionListBox.value || sectionListBox.options[sectionListBox.selectedIndex].value;
        var NewSection = sectionListBox.value || sectionListBox.options[sectionListBox.selectedIndex].value;
    }
    else
    {
        $("CHANGEDSECTION-'. Sanitize::SanitizeString($_POST["originalsection"]).'-' .  Sanitize::SanitizeString($_POST["inputfield"]) . '").value = "";
        var NewSection = "' . Sanitize::SanitizeString($_POST["originalsection"]).'";
    }

    $("NEWCHANGEDSECTION-'. Sanitize::SanitizeString($_POST["originalsection"]).'-' .  Sanitize::SanitizeString($_POST["inputfield"]) . '").value = "1";

    var FieldOrderObj = $("FIELDORDER-'. Sanitize::SanitizeString($_POST["section"]).'-' . Sanitize::SanitizeString($_POST["inputfield"]) . '");

    if(FieldOrderObj)
    {
        FieldOrderObj.id = "FIELDORDER-"+NewSection+"-' . Sanitize::SanitizeString($_POST["inputfield"]) . '";
        FieldOrderObj.name = "FIELDORDER-"+NewSection+"-' . Sanitize::SanitizeString($_POST["inputfield"]) . '";
    }

    document.forms[0].btnSaveDesign.click();
    return true;';

     echo '
}

function returnToDefault()
{
    var FieldOrderObj = $("FIELDORDER-'. Sanitize::SanitizeString($_POST["section"]).'-' . Sanitize::SanitizeString($_POST["inputfield"]) . '");

    if(FieldOrderObj)
    {
        FieldOrderObj.id = "FIELDORDER-'.Sanitize::SanitizeString($_POST["originalsection"]).'-' . Sanitize::SanitizeString($_POST["inputfield"]) . '";
        FieldOrderObj.name = "FIELDORDER-'.Sanitize::SanitizeString($_POST["originalsection"]).'-' . Sanitize::SanitizeString($_POST["inputfield"]) . '";
    }

    $("CHANGEDSECTION-'. Sanitize::SanitizeString($_POST["originalsection"]).'-' . Sanitize::SanitizeString($_POST["inputfield"]) . '").value = "";
    $("NEWCHANGEDSECTION-'. Sanitize::SanitizeString($_POST["originalsection"]).'-' . Sanitize::SanitizeString($_POST["inputfield"]) . '").value = "1";

    document.forms[0].btnSaveDesign.click();
    return true;
}
</script>
';

    echo '
<table width="600px">
    <tr>
        <td>
            <table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
                <tr>
                    <td class="windowbg2" width="200px">
                    <b>Move to section:</b>
                    </td>
                    <td class="windowbg2">
                    ' . $TargetDropdown->GetField()
                    . '
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>';
}

function ExportConfigtoXML()
{
	global $dbtype, $dtxversion, $scripturl, $ClientFolder, $ModuleDefs;

    $hideSensitiveConfigInfo = bYN(GetParm('HIDE_SENSITIVE_CONFIG_DATA', 'N'));

    require_once 'libs/XmlExport.php';
    $XML = new XmlExport();

    $sql = "SELECT @@version as dbversion, @@SERVERNAME as dbservername, db_name() as dbname";
    $row = DatixDBQuery::PDO_fetch($sql);
    $dbversion = $row["dbversion"];
    $dbservername = $row["dbservername"];
    $dbname = $row["dbname"];

    $XML->XOpenTag("ConfigurationSettings");

    ///////////////////////////////
    ////// General Settings ///////
    ///////////////////////////////

    $XML->XNewDataSet("GeneralSettings");
    $XML->XTitleLine("General Settings");

    require_once 'externals/keygen/DatixLicenceKeyFactory.php';
    require_once 'externals/keygen/DatixLicenceKey120.php';
    require_once 'externals/keygen/DatixLicenceKey121.php';
    require_once 'externals/keygen/DatixLicenceKey.php';
    require_once 'externals/keygen/DatixLicenceKey114.php';

    $LicenceKey = DatixLicenceKeyFactory::createLicenceKey();
    $LicenceKey->SetLicenceKey(GetParm('LICENCE_KEY_WEB'));
    $LicenceKey->SetClientName(GetParm('CLIENT'));
    $LicenceKey->DecodeKey();

    $XML->XGlobal("ClientName", $LicenceKey->GetClientName());

    if (!$hideSensitiveConfigInfo)
    {
        $XML->XGlobal("LicenceKey", $LicenceKey->GetLicenceKey());
        if ($LicenceKey->GetExpiryDate())
        {
            $XML->XGlobal("ExpiryDate", $LicenceKey->GetExpiryDate());
        }
        $modules = $LicenceKey->GetLicencedModules();
        if (!empty($modules))
        {
            foreach ($modules as $modId => $modLicenced)
            {
                if($modLicenced)
                {
                    $LicencedModules[] = $ModuleDefs[ModuleIDToCode($modId)]['NAME'];
                }
            }
            $lastModule = array_pop($LicencedModules);
            if(count($LicencedModules) > 0)
            {
                $modulesStr = implode(', ', $LicencedModules).' and '.$lastModule;
            }
            else
            {
                $modulesStr = $lastModule;
            }

            $XML->XGlobal("LicensedModules", $modulesStr);
        }
    }

    $XML->XGlobal("DATIXWebVersion", htmlspecialchars($dtxversion));
    $XML->XGlobal("DATIXDatabaseVersion", htmlspecialchars($dbversion));

    if (!$hideSensitiveConfigInfo)
    {
        $XML->XGlobal("DatabaseServerName", htmlspecialchars($dbservername));
    }

    $XML->XGlobal("DatabaseType", htmlspecialchars($dbtype));

    if (!$hideSensitiveConfigInfo)
    {
        $XML->XGlobal("DatabaseName", htmlspecialchars($dbname));
    }

    $XML->XGlobal("ScriptURL", htmlspecialchars($scripturl));

    $XML->XCloseDataSet();

    ////////////////////////////
    ////// DATIX Globals ///////
    ////////////////////////////

    $XML->XNewDataSet("DATIXGlobals");
    $XML->XTitleLine("DATIX Globals");

    $where = '';
    if ($hideSensitiveConfigInfo)
    {
        foreach (getExcludedGlobals() as $key => $global)
        {
            $where .= $key == 0 ? 'WHERE' : 'AND';
            $where .= ' parameter NOT LIKE \''.$global.'\' ';
        }
    }

    $sql = 'SELECT parameter, parmvalue FROM globals WHERE parameter NOT IN (\''.implode('\',\'', getExcludedGlobals()).'\') ORDER BY parameter';
    $parameters = DatixDBQuery::PDO_fetch_all($sql);
    foreach ($parameters as $parameter)
    {
        $XML->XGlobal($parameter["parameter"], htmlspecialchars($parameter["parmvalue"]));
    }

    $XML->XCloseDataSet();

    ////////////////////////////
    /////// User params ////////
    ////////////////////////////

    if (!$hideSensitiveConfigInfo)
    {
        $XML->XNewDataSet("UserParams");
        $XML->XTitleLine("User Params");

        $sql = "SELECT login, parameter, parmvalue
		    FROM user_parms
		    ORDER BY login, parameter";
	    $userParameters = DatixDBQuery::PDO_fetch_all($sql);

        $rowgrbyname = array();
        foreach ($userParameters as $userParameter)
        {
            $rowgrbyname[$row["login"]][] = $userParameter;
        }

        foreach($rowgrbyname as $userlogin => $rows)
        {
            $XML->XOpenTag("user");
            $XML->XNameLine($userlogin);
            foreach($rows as $row)
            {
                $XML->XGlobal($row["parameter"], htmlspecialchars($row["parmvalue"]));
            }
            $XML->XCloseTag("user");
        }

        $XML->XCloseDataSet();
    }

    ////////////////////////////
    ///////// PHPInfo //////////
    ////////////////////////////

    $XML->XNewDataSet("PHPini");
    $XML->XTitleLine("PHP ini");

    $inis = ini_get_all();

    foreach($inis as $ikey => $ival)
    {
        if(!$hideSensitiveConfigInfo || !in_array($ikey, getExcludedIniSettings()))
        {
            $XML->XOpenTag("directive");
            $XML->XNameLine($ikey);
                foreach($ival as $iikey => $iival)
                {
                    $XML->XGlobal($iikey, htmlspecialchars($iival), "setting");
                }
            $XML->XCloseTag("directive");
        }
    }

    $XML->XCloseDataSet();

    //////////////////////////////////
    ///////// PHP Variables //////////
    //////////////////////////////////

    $XML->XNewDataSet("PHPVariables");
    $XML->XTitleLine("PHP Variables");

    foreach($_SERVER as $pvkey => $pvval)
    {
        if (!$hideSensitiveConfigInfo || !in_array($pvkey, getExcludedServerVars()))
        {
            $XML->XGlobal($pvkey, htmlspecialchars($pvval), "variable");
        }
    }

    $XML->XCloseDataSet();

    /////////////////////////////////
    ////// Configuration files //////
    /////////////////////////////////

    if (!$hideSensitiveConfigInfo)
    {
        $XML->XNewDataSet("ConfigFiles");
        $XML->XTitleLine("Configuration files");

        $filearray = array();

        $XML->XOpenTag("file");
        $XML->XTitleLine("config");

        $filename = "client/DatixConfig.php";
        $handle = fopen($filename, "rb");
        $contents = fread($handle, filesize($filename));
        fclose($handle);

        $XML->XNewLine("content", $contents);

        $XML->XCloseTag("file");

        if ($handle = opendir('./client')) {
            while (false !== ($file = readdir($handle)))
            {
                if ($file != "." && $file != ".." && preg_match("/User/u", $file))
                {

                    $filearray[] = array("name" => $file, "source" => $ClientFolder . "/$file");
                }
            }
            closedir($handle);
        }

        if(is_array($filearray)){
            foreach($filearray as $file)
            {
                $XML->XOpenTag("file");
                $XML->XTitleLine(str_replace(".php", "", $file["name"]));

                $filename = $file["name"];
                $handle = fopen($file["source"], "rb");

                if (filesize($file['source']) > 0) {
                    $contents = fread($handle, filesize($file["source"]));
                }

                fclose($handle);

                $contents = str_replace("\n", "\r\n", $contents);

                $XML->XNewLine("content", $contents);
                $XML->XCloseTag("file");
            }
        }

        $XML->XCloseDataSet();
    }

    $XML->XNewDataSet("UDFGroupStats");
    $XML->XNewLine("udfgroupstat", '', ['value', 'question'], [count((new \src\admin\model\systemFlags\ExtraFieldsInGroupsFlag)->getFieldIds()), 'Extra fields in a group other than 0']);
    $XML->XNewLine("udfgroupstat", '', ['value', 'question'], [count((new \src\admin\model\systemFlags\ExtraFieldsInMultipleGroupsFlag)->getFieldIds()), 'Extra fields in more than one group within the same module']);
    $XML->XNewLine("udfgroupstat", '', ['value', 'question'], [count((new \src\admin\model\systemFlags\ExtraFieldsWithConflictingDataFlag)->getRecordIds()), 'Records with conflicting extra field data']);

    $XML->XCloseDataSet();

    /////////////////////////////
    ///////// Tables ////////////
    /////////////////////////////

    // records of any table in the database can be exported to the xml file
    //$tablenames = array("user_parms", "code_stages");
    //$XML->XExportTabletoXML($tablenames);

    /////////////////////////////
    /////////////////////////////
    /////////////////////////////

    $XML->XCloseTag("ConfigurationSettings");

    $xoutput = $XML->GetXML();

    $XML->WriteXML($xoutput, $ClientFolder);
}

/**
* Returns an array of globals which need to be excluded from config.xml
*
* @return array
*/
function getExcludedGlobals()
{
    return array(
        '%_ADMIN_EMAIL',
        'LDAP_PASSWORD',
        'LDAP_USER',
        'LICENCE_KEY',
        'LICENCE_KEY_WEB',
        'WEB_LICENCE',
        'WHO_EMAIL_%',
        'E_MAIL_SMTPSERVER',
        'LDAP_BASE_DN',
        'KO41_TRUST_NAME',
        'LDAP_USERS_DN',
        'PATH_%',
        'SYS_E_MAIL_SMTPSRV',
        'SYS_E_MAIL_SMTPSRV',
        'EXPORT_INCIDENT_TO_XML_PATH',
        'PATH_ASPELL',
        'PATH_DBTEMP',
        'PATH_DOC_HOLDING',
        'PATH_DOCUMENTS',
        'PATH_REPORTS',
        'PATH_TEMPLATES',
        'PATH_UPGRADE',
        'PATH_UPGRADE_INI',
        'PATH_UPGRADE_TXT',
        'LDAP_HOST',
        'DATIXWEB_URL',
        'DIF_ADMIN_EMAIL',
        'PAL_ADMIN_EMAIL',
        'SABS_E_MAIL_ADDRESS',
        'SUPPORT_EMAIL_ADDR',
        'SYS_E_MAIL_ADDRESS'
    );
}

/**
* Returns an array of server variables which need to be excluded from config.xml
*
* @return array
*/
function getExcludedServerVars()
{
    return array(
        'COMPUTERNAME',
        'HTTP_REFERER',
        'USERNAME',
        'USERDOMAIN',
        'HTTP_HOST',
    );
}

/**
* Returns an array of ini settings which need to be excluded from config.xml
*
* @return array
*/
function getExcludedIniSettings()
{
    return array(
        'SMTP',
        'smtp_port',
    );
}