<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => false,
        "Suffix" => $Suffix,
        "Condition" => false
    ),
    ($SectionName ? $SectionName : 'equipment') => array(
        "Title" => "Equipment",
        "ContactSuffix" => $Suffix,
        "right_hand_link" => $RightHandLink,
        "Rows" => array(
            "ast_name",
            "ast_ourref",
            "ast_organisation",
            "ast_unit",
            "ast_clingroup",
            "ast_directorate",
            "ast_specialty",
            "ast_loctype",
            "ast_locactual",
            "ast_type",
            "ast_product",
            "ast_model",
            "ast_manufacturer",
            "ast_supplier",
            "ast_catalogue_no",
            "ast_batch_no",
            "ast_serial_no",
            "ast_dmanufactured",
            "ast_dputinuse",
            "ast_dlastservice",
            "ast_dnextservice",
            "ast_cemarking",
            "ast_location",
            "ast_quantity",
            "ast_descr",
            'ast_category',
            'ast_cat_other_info',
            "link_damage_type",
            "link_other_damage_info",
            "link_replaced",
            "link_replace_cost",
            "link_repaired",
            "link_repair_cost",
            "link_written_off",
            "link_disposal_cost",
            "link_sold",
            "link_sold_price",
            "link_decommissioned",
            "link_decommission_cost",
            "link_residual_value",
        )
    ),
    "orphanedudfs".($Suffix ? '_'.$Suffix :'') => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($ast['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('AST', $ast['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
);

?>
