<?php
// ==========================================================================//
// Initialise conditions                                                     //
// ==========================================================================//

if ($_SESSION['AST']['DRILL_QUERY'] instanceof \src\framework\query\Query &&
    (isset($_REQUEST['from_report']) && $_REQUEST['from_report'] == '1' || isset($_REQUEST['fromcrosstab']) && $_REQUEST['fromcrosstab'] == '1'))
{
    $drillIntoReport = true;
}

$_SESSION["AST"]["EXTRAWHERE"] = '';

$ListType = $_GET["listtype"];

$orderby = $_SESSION["LIST"]["AST"]["ORDERBY"];
$order = $_SESSION["LIST"]["AST"]["ORDER"];

if (!$orderby)
{
    $orderby = "ast_name";
    $order = "ASC";
}

if($_POST["listnum"])
    $listnum = Sanitize::SanitizeInt($_POST["listnum"]);

// ==========================================================================//
// Build where clause according to access level, permission, timescales      //
// and criteria specific to the type of listing                              //
// ==========================================================================//

// Default title and action
if($LinkMode)
{
    $ListTitle = "Matching Equipment";
    $ActionType = "linkequipment";
}
else
{
    $ListTitle = _tk('ast_listing');
    $ActionType = "asset";
}
$tables = 'assets_main';

// Store table(s) used for the query in session var, mainly used when reporting
$_SESSION["AST"]["TABLES"] = $tables;

if ($ListType == "search" && $_SESSION["AST"]["WHERE"])
{
    $Where[] =  "(" . $_SESSION["AST"]["WHERE"] . ")";
}
elseif ($ListType == 'all')
{
    $_SESSION["AST"]["WHERE"] = $ModuleDefs['AST']['HARD_CODED_LISTINGS']['all']['Where'];
}

if ($_REQUEST["drillwhere"] != "")
{
    $Where[] = Sanitize::SanitizeRaw($_REQUEST["drillwhere"]);
}
else if ($drillIntoReport)
{
    $query = $_SESSION['AST']['DRILL_QUERY'];

    $writer = new \src\framework\query\SqlWriter();

    list($statement, $parameters) = $writer->writeStatement($query);

    // Replace all occurrences of ? with values on the where clause
    $needle = '?';

    foreach ($parameters as $parameter)
    {
        $parameterPos = UnicodeString::strpos($statement, $needle);

        if ($parameterPos !== false)
        {
            $statement = UnicodeString::substr_replace($statement, DatixDBQuery::quote($parameter), $parameterPos, UnicodeString::strlen($needle));
        }
    }

    $Where = $ModuleDefs['AST']['TABLE'].'.recordid IN ('.$statement.')';

    $_SESSION['AST']['current_drillwhere'] = $Where;
}

$ListWhereClause = MakeSecurityWhereClause($Where, "AST", $_SESSION["initials"]);

// ==========================================================================//
// Image file to display at the top left of the form                         //
// ==========================================================================//
$img = "Images/assetstitle.gif";

// ==========================================================================//
// Build the URL to be used when clicking on a record                        //
// ==========================================================================//
$timescales_url = "$scripturl?action=list&amp;module=AST&amp;listtype=$ListType";
if ($_REQUEST["rows"] != '')
{
    $timescales_url = "$scripturl?". htmlspecialchars($_SERVER[QUERY_STRING]);
}
if($_GET['fromcrosstab'] == '1')
{
    $timescales_url .= '&amp;fromcrosstab=1&amp;row='.$_GET['row'].'&amp;col='.$_GET['col'];
}

$record_url = "$scripturl?action=$ActionType&fromsearch=1";

// ==========================================================================//
// Columns to be displayed                                                   //
// ==========================================================================//

require_once 'Source/libs/ListingClass.php';
$list_columns = Listings_ModuleListingDesign::GetListingColumns($module);

//==========================================================================//
// Prepare SQL Statement to be executed by BrowseList.php                   //
//==========================================================================//

if (is_array($list_columns))
{
     foreach ($list_columns as $col_name => $col_info)
     {
         if(is_array($list_columns_extra[$col_name])){
             if(!array_key_exists("condition", $list_columns_extra[$col_name]) || $list_columns_extra[$col_name]["condition"]){
                 $selectfields[$col_name] = $col_name;
             }
             else {
                 unset($list_columns[$col_name]);
             }
         }
         else
             $selectfields[$col_name] = $col_name;
     }
}

$list_request = GetSQLResultsTimeScale($selectfields, $tables,
            $order, $orderby, $ListWhereClause, $listnum, $listdisplay, $listnum2, $listnumall, $sqla, null, $module);
?>