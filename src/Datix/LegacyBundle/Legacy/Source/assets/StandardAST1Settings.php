<?php
$GLOBALS["FormTitle"] = "DIF1 Equipment Form";

$GLOBALS["ExpandFields"] = array (
  'link_replaced' => 
  array (
    0 => 
    array (
      'field' => 'link_replace_cost',
      'alerttext' => '',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
  'link_repaired' => 
  array (
    0 => 
    array (
      'field' => 'link_repair_cost',
      'alerttext' => '',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
  'link_written_off' => 
  array (
    0 => 
    array (
      'field' => 'link_disposal_cost',
      'alerttext' => '',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
  'link_sold' => 
  array (
    0 => 
    array (
      'field' => 'link_sold_price',
      'alerttext' => '',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
  'link_decommissioned' => 
  array (
    0 => 
    array (
      'field' => 'link_decommission_cost',
      'alerttext' => '',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
);
?>
