<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "details" => array(
        "Title" => "Equipment details",
        "MenuTitle" => "Details",
        "Rows" => array(
            array(
                "Title" => "ID",
		        "Name" => "recordid",
                "Type" => "formfield",
                "FormField" => $IDFieldObj
            ),
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'AST',
                'perms' => $ASTPerms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $inc,
                'module' => 'AST',
                'perms' => $ASTPerms,
                'approveobj' => $ApproveObj
            )),
            "ast_name",
            "ast_ourref",
            "ast_organisation",
            "ast_unit",
            "ast_clingroup",
            "ast_directorate",
            "ast_specialty",
            "ast_loctype",
            "ast_locactual",
            "ast_type",
            "ast_product",
            "ast_model",
            "ast_manufacturer",
            "ast_supplier",
            "ast_catalogue_no",
            "ast_batch_no",
            "ast_serial_no",
            "ast_dmanufactured",
            "ast_dputinuse",
            "ast_dlastservice",
            "ast_dnextservice",
            "ast_cemarking",
            "ast_location",
            "ast_quantity",
            "ast_descr",
            'ast_category',
            'ast_cat_other_info',
        )
    ),
    "link" => array(
        "Title" => "Link details",
        "Condition" => ($LinkMode || $FormType == 'Design'),
        'LinkFields' => true,
        "Rows" => array(
            "link_damage_type",
            "link_other_damage_info",
            "link_replaced",
            "link_replace_cost",
            "link_repaired",
            "link_repair_cost",
            "link_written_off",
            "link_disposal_cost",
            "link_sold",
            "link_sold_price",
            "link_decommissioned",
            "link_decommission_cost",
            "link_residual_value",
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $ast['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('AST', $ast['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "notepad" => array(
        "Title" => "Notepad",
        "NewPanel" => true,
        "NotModes" => array("Search"),
        "Rows" => array("notes")
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NotModes" => array("Search", "New"),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NewPanel" => true,
        "Special" => "LinkedActions",
        "NoFieldAdditions" => true,
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "safety_alerts" => array(
        "Title" => "Safety Alerts",
        "NewPanel" => true,
        'ControllerAction' => array(
            'ListSafetyAlerts' => array(
                'controller' => 'src\\ast\\controllers\\AssetSafetyAlertsController'
            )
        ),
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "incidents" => array(
        "Title" => 'Incidents',
        "NewPanel" => true,
        'ControllerAction' => array(
            'ListIncidents' => array(
                'controller' => 'src\\ast\\controllers\\ListLinkedIncidentsController'
            )
        ),
        'Listings' => array('incidents' => array('module' => 'INC')),
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController'
            )
        ),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
);

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}
