<?php
require_once 'src/autoload/autoload.php';

require_once 'src\framework\profiler\Profiler.php';
$profiler = new src\framework\profiler\Profiler();
$profiler->start();

if ($logLevel == 200)
{
    $start_time = microtime(true);
}

require_once("Source/libs/Subs.php");
require_once("Source/libs/DatixDBQueryClass.php");

// Log CLI events
if (\src\framework\registry\Registry::getInstance()->getLogger()->cliToBeLogged() && $logLevel == 200)
{
    \src\framework\registry\Registry::getInstance()->getLogger()->logInfo('CLI message received.');
}

//GZIP HTML output if browser support it.
if (
    (!isset($_GET['action']) || $_GET['action'] != 'document')
    && isset($_SERVER['HTTP_ACCEPT_ENCODING'])
    && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')
    && (!bYN(GetParm("DISABLE_GZIP", "N", true)))
)
{
    ob_start('ob_gzhandler');
}
else
{
    ob_start();
}

require_once("Source/Version.php");
require_once("Source/classes/SanitizeClass.php");

//Ensure that this page request can only be loaded into an iframe from the same domain. (prevents XFS attacks)
header('X-Frame-Options:SAMEORIGIN');

if ($_SERVER["HTTPS"] == "on")
{
    $HTTP_Prefix = "https";
}
else
{
    $HTTP_Prefix = "http";
}

$GLOBALS['basescripturl'] = "$HTTP_Prefix://$_SERVER[SERVER_NAME]";
$GLOBALS['scripturl'] = ('cli' != php_sapi_name()) ? Sanitize::SanitizeURL("$HTTP_Prefix://$_SERVER[SERVER_NAME]$_SERVER[SCRIPT_NAME]") : $scripturl;

$db_connect = $dbtype . "_connect";
$db_select_db = $dbtype . "_select_db";

set_exception_handler(array('Exceptions_Handler', 'handle'));

if ($_GET['action'] != 'httprequest')
{
    require_once("Source/libs/FormClasses.php");
    require_once("Source/classes/templateClass.php");
}

if ($SessionsInDB)
{
	require_once 'Source/libs/Sessions.php';
}

session_set_cookie_params(0, '', '', ($_SERVER["HTTPS"] == "on" ? true : false));
session_start();

if ($_GET['action'] != 'httprequest' && bYN(GetParm('DETECT_TIMEZONES', 'N', true)) && !isset($_SESSION['Timezone']) && 'cli' != php_sapi_name())
{
    // If we don't reload after setting this timezone variable,
    // the date value won't be available until the next pageload.
    echo '<script type="text/javascript" src="js_functions/jquery/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js_functions/common'.($MinifierDisabled ? '' : '.min').'.js"></script>
    <script language="javascript" type="text/javascript">
    var scripturl="'.$ClientSpecificPrefix.$scripturl.'";
    SetTimezoneValue(\''.Sanitize::SanitizeString($_GET['action']).'\');
    </script>';
    exit();
}

if ($_GET['action'] != 'httprequest' && $_GET['action'] != 'fieldhelp')
{
    //Ensure help text from previous pages isn't retained.
    unset($_SESSION["HelpTexts"]);
}

ValidateURL();

setupViewClass();
getLanguageArray();

require_once "Source/libs/AppVars.php";

//Do the following only if not a dropdown related httprequest.
//TODO - this is starting to get a bit silly as you need to add any action type that comes from a jquery ajax request that returns json or it'll fail
if (!($_GET['action'] == 'httprequest' && ($_GET['type'] == 'getparents' || $_GET['type'] == 'codelist' || $_GET['type'] == 'selectfunction' || $_GET['type'] == 'copytemplatedoctoclient' || $_GET['type'] == 'checkmergecontactprompt' || $_GET['type'] == 'returnmergesql' || $_GET['type'] == 'duplicaterecordcheck' || $_GET['type'] == 'getdashboardhtml')))
{
    DatixDBQuery::PDO_query('SET DATEFORMAT ymd');
    CheckSessionVersion();
    AddCustomTextToJSGlobal();
    addGlobalsToJSGlobal();

    if (!$_SESSION["Globals"] || !$_SESSION["logged_in"])
    {
        GetParms($_SESSION["user"]);
    }

    //stop everything here if we are in maintenance mode and not an admin or actively logging in as an admin
    if ($_GET['action'] != 'httprequest' && bYN(GetParm("WEB_MAINTMODE", "N", true)))
    {
        if ($_GET['action'] == 'login' && $_GET['adminlogin'] == 'y')
        {
            $nonAdminAccess = false;
        }
        else if ($_GET["action"] == 'login2')
        {
            $username = ($_POST['domain'] ? $_POST['domain'].'\\' : '').$_POST['user'];
            $nonAdminAccess = (!bYN(GetUserParm($username, 'FULL_ADMIN', 'N')));
        }
        else
        {
            $nonAdminAccess = (!$_SESSION["AdminUser"]);
        }

        if ($nonAdminAccess && $_GET["action"] != "disagreementmessage")
        {
            $_GET["action"] = "logout";
        }
    }

    if ($_SESSION["logged_in"] && bYN(GetParm("RECORD_LOCKING","N")))
    {
        DatixDBQuery::CallStoredProcedure(array(
        	"procedure" => "ClearExpiredRecords",
        	"parameters" => array()
        ));

        if ($_SESSION["session_id"] &&  $_GET["action"] != "logout")
        {
	        $sql = 'SELECT count(*) as total FROM SESSIONS WHERE RECORDID = :recordid';
	        $result = DatixDBQuery::PDO_fetch($sql, array(':recordid' => $_SESSION["session_id"]));

            DatixDBQuery::CallStoredProcedure(array(
            	"procedure" => "SetSessionActive",
            	"parameters" => array(
                	array("@session_id", $_SESSION["session_id"], SQLINT4),
                	array("@lock_id", NULL, SQLINT4)
                )
            ));

			if ($result['total'] == "0")
			{
				$_GET["action"] = "logout";
				$_GET["no_session"] = "1";
				src\framework\registry\Registry::getInstance()->getLogger()->logDebug('Automatic logout. Reason: No Datix session found');
			}
        }
    }

    if (GetParm("FMT_DATE_WEB", "GB") == "US")
    {
        $_SESSION["DATE_FORMAT"] = "MM/dd/yyyy";
    }
    else
    {
        $_SESSION["DATE_FORMAT"] = "dd/MM/yyyy";
    }

    $_SESSION["WEB_LABELS_ONLY"] = bYN(GetParm("WEB_LABELS_ONLY", "N"));

    if ($_GET['action'] != 'httprequest' && false) //user config gathering - will be opened up in the future.
    {
        require_once 'Source/libs/ConfigTracking/ConfigSenderClass.php';

        $ConfigSender = new ConfigSender();
        $JSFunctions[] = $ConfigSender->CheckSendToDatix();
    }
}

if (!$ClientFolder)
{
    $GLOBALS['ClientFolder'] = "client";
}

IncludeFieldDefs();

CacheParents();

ValidateSSO();

datixmain();

\src\logger\DatixLogger::logScriptExecutionLength();

echo $profiler->stopAndPublish();

function datixmain()
{
	global $action, $NoOpenAccess, $scripturl, $yySetLocation, $logLevel, $start_time;

	$action = 'cli' == php_sapi_name() ? $_SERVER['argv'][1] : $_GET["action"];

    $actionArray = array(
        'list' => array("Source/BrowseList.php", 'BrowseList'),
        'login' => array(
            'controller' => 'src\\login\\controllers\\LoginTemplateController'
        ),
        'login2' => array("Source/Login.php", 'Login2'),
        'register' => array(
            'controller' => 'src\\login\\controllers\\RegisterTemplateController'
        ),
        'contactformregister2' => array(
            'controller' => 'src\\login\\controllers\\RegisterTemplateController'
        ),
        'listawaitingapproval' => array(
            'controller' => 'src\\admin\\controllers\\RegistrationController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'viewregistrationform' => array(
            'controller' => 'src\\admin\\controllers\\ViewRegistrationFormTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\SubAdminFilter',
            )
        ),
        'approveuserregistration' => array(
            'controller' => 'src\\admin\\controllers\\ViewRegistrationFormTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\SubAdminFilter',
           )
        ),
        'reset_password' => array(
            'controller' => 'src\\login\\controllers\\ResetPasswordTemplateController'
        ),
        'reset_password2' => array(
            'controller' => 'src\\login\\controllers\\ResetPasswordTemplateController'
        ),
        'logout' => array(
            'controller' => 'src\\login\\controllers\\LogoutTemplateController'
        ),
        'savedatixwebconf' => array(
            'controller' => 'src\\admin\\controllers\\SaveDatixwebConfController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\controller\\LoggedInFilter'
            )
        ),
        'setup' => array(
            'controller' => 'src\\admin\\controllers\\ShowDatixwebConfTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listformdesigns' => array(
            'controller' => 'src\\admin\\controllers\\ListFormDesignsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'newformdesign' => array(
            'controller' => 'src\\formdesign\\controllers\\NewFormDesignTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savenewformdesign' => array(
            'controller' => 'src\\formdesign\\controllers\\SaveNewFormDesignController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savedesignsettings' => array(
            'controller' => 'src\\formdesign\\controllers\\FormDesignSetupController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'formdesignsetup' => array(
            'controller' => 'src\\formdesign\\controllers\\FormDesignSetupController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveIncident' => array(
            'controller' => 'src\\incidents\\controllers\\IncidentController'
        ),
        'addnewincident' => array(
            'controller' => 'src\\incidents\\controllers\\AddNewIncidentController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'incidentssearch' => array(
            'controller' => 'src\\incidents\\controllers\\IncidentsSearchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'incidentsdoselection' => array(
            'controller' => 'src\\incidents\\controllers\\IncidentsDoSelectionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'contactlist' => array(
            'controller' => 'src\\contacts\\controllers\\ContactCheckController'
        ),
        'contactlinkmainaction' => array(
            'controller' => 'src\\contacts\\controllers\\ContactLinkMainActionController'
        ),
        'listcontacts' => array("Source/contacts/ListContacts.php", 'ListContacts'),
        'newcontact' => array(
            'controller' => 'src\\contacts\\controllers\\ShowContactFormTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editcontact' => array(
            'controller' => 'src\\contacts\\controllers\\EditContactController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'contactssearch' => array(
            'controller' => 'src\\contacts\\controllers\\ContactsSearchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'searchcontact' => array(
            'controller' => 'src\\contacts\\controllers\\ContactsDoSelectionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'showsavecontact' => array(
            'controller' => 'src\\contacts\\controllers\\ShowSaveContactTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveactioncontacts' => array(
            'controller' => 'src\\contacts\\controllers\\SaveActionContactsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'password' => array(
            'controller' => 'src\\admin\\controllers\\ChangePasswordTemplateController'
        ),
        'password2' => array(
            'controller' => 'src\\admin\\controllers\\ChangePasswordTemplateController'
        ),
        'clearsearch' => array(
            'controller' => 'src\\search\\controllers\\ClearSearchController'
        ),
        'showuaincident' => array(
            'controller' => 'src\\incidents\\controllers\\ShowUAIncidentController'
        ),
        'incident' => array(
            'controller' => 'src\\incidents\\controllers\\ShowIncidentController'
        ),
        'newdif1' => array(
            'controller' => 'src\\incidents\\controllers\\NewDIF1Controller'
        ),
        'disagreementmessage' => array(
            'controller' => 'src\\login\\controllers\\DisagreementMessageController'
        ),
        'fieldhelp' => array(
            'controller' => 'src\\formdesign\\controllers\\ShowFieldHelpController'
        ),
        'helpfinder' => array(
            'controller' => 'src\\helpfinder\\controllers\\HelpFinderController'
        ),
        'sendconfigxml' => array(
            'controller' => 'src\\admin\\controllers\\SendConfigXMLController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'showconfig' => array(
            'controller' => 'src\\admin\\controllers\\ShowConfigTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'showerrorlog' => array(
            'controller' => 'src\\admin\\controllers\\ShowErrorLogTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        // Risk register
        'newrisk1' => array(
            'controller' => 'src\\riskregister\\controllers\\NewRisk1Controller'
        ),
        'risk' => array(
            'controller' => 'src\\riskregister\\controllers\\ShowRiskController'
        ),
        'newrisk2' => array(
            'controller' => 'src\\riskregister\\controllers\\NewRisk2Controller',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'risksearch' => array(
            'controller' => 'src\\riskregister\\controllers\\RiskSearchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saverisk' => array(
            'controller' => 'src\\riskregister\\controllers\\SaveRiskController'
        ),
        'risksdoselection' => array(
            'controller' => 'src\\riskregister\\controllers\\RisksDoSelectionController'
        ),
        'editassurance' => array(
            'controller' => 'src\\riskregister\\controllers\\EditAssuranceController'
        ),
        'saveassurance' => array(
            'controller' => 'src\\riskregister\\controllers\\SaveAssuranceController'
        ),
        'assurancelist' => array(
            'controller' => 'src\\riskregister\\controllers\\AssuranceListController'
        ),
        // Generic
        'addnew' => array(
            'controller' => 'src\\generic\\controllers\\MainRecordController'
        ),
        'saverecord' => array(
            'controller' => 'src\\generic\\controllers\\MainRecordController'
        ),
        'search' => array(
            'controller' => 'src\\generic\\controllers\\MainRecordController'
        ),
        'doselection' => array(
            'controller' => 'src\\generic\\controllers\\DoSelectionController'
        ),
        'record' => array(
            'controller' => 'src\\generic\\controllers\\ShowRecordController'
        ),
        'deleterecord' => array(
            'controller' => 'src\\generic\\controllers\\DeleteRecordController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'copyrecord' => array(
            'controller' => 'src\\generic\\controllers\\CopyRecordTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'docopyrecord' => array(
            'controller' => 'src\\generic\\controllers\\DoCopyRecordTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'generaterecord' => array(
            'controller' => 'src\\generic\\controllers\\RecordTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'dogeneraterecord' => array(
            'controller' => 'src\\generic\\controllers\\RecordController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        // Admin
        'listglobals' => array(
            'controller' => 'src\\admin\\controllers\\GlobalsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'lockglobals' => array(
            'controller' => 'src\\admin\\controllers\\GlobalsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'unlockglobals' => array(
            'controller' => 'src\\admin\\controllers\\GlobalsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listUserGlobals' => array(
            'controller' => 'src\\admin\\controllers\\GlobalsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\admin\\filters\\UserParameterUnlockedFilter',
                'src\\framework\\controller\\LoggedInFilter',
            )
        ),
        'licence' => array(
            'controller' => 'src\\admin\\controllers\\LicenceTemplateController',
        ),
        'listlistingdesigns' => array(
            'controller' => 'src\\admin\\controllers\\ListListingDesignsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'newlistingdesign' => array(
            'controller' => 'src\\admin\\controllers\\NewListingDesignTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savenewlistingdesign' => array(
            'controller' => 'src\\admin\\controllers\\SaveNewListingDesignController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editcolumns' => array(
            'controller' => 'src\\admin\\controllers\\EditColumnsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savecolumns' => array(
            'controller' => 'src\\admin\\controllers\\SaveColumnsController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'resetcolumns' => array(
            'controller' => 'src\\admin\\controllers\\ResetColumnsController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'deletelistingdesign' => array(
            'controller' => 'src\\admin\\controllers\\DeleteListingDesignController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'staffresponsibilities' => array(
            'controller' => 'src\\admin\\controllers\\StaffResponsibilitiesTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\FullAdminFilter',
            )
        ),
        'usergroupreport' => array(
            'controller' => 'src\\admin\\controllers\\UserAccessReportTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'codesetupslist' => array(
            'controller' => 'src\\admin\\controllers\\CodeSetupsTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'codesetupsaction' => array(
            'controller' => 'src\\admin\\controllers\\CodeSetupsActionTemplateController',
            'filters' => array(
                'src\\admin\\filters\\ModuleSetupFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editmulticodevalue' => array(
            'controller' => 'src\\admin\\controllers\\CodeSetupsController'
        ),
        'exportobsoletenpsamappings' => array(
            'controller' => 'src\\admin\\controllers\\CodeSetupsController',
            'filters' => array(
                'src\\admin\\filters\\ModuleSetupFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listtaggroups' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagGroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'edittaggroup' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagGroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savetaggroup' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagGroupController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'deletetagset' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagGroupController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listtagfields' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagLinkTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'linktagfield' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagLinkTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savetagfield' => array(
            'controller' => 'src\\admin\\controllers\\CodeTagLinkController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'udfsetup' => array(
            'controller' => 'src\\admin\\controllers\\UDFCodeSetupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'full_audit' => array(
            'controller' => 'src\\admin\\controllers\\FullAuditController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'show_login_audit' => array(
            'controller' => 'src\\admin\\controllers\\LoginAuditTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'email_audit' => array(
            'controller' => 'src\\admin\\controllers\\EmailAuditController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'lockcodesetup' => array(
            'controller' => 'src\\admin\\controllers\\LockCodeSetupController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\admin\\filters\\CentrallyAdminSysFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'unlockcodesetup' => array(
            'controller' => 'src\\admin\\controllers\\UnlockCodeSetupController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\admin\\filters\\CentrallyAdminSysFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'exportcodesetup' => array(
            'controller' => 'src\\admin\\controllers\\CodeSetupsController',
            'filters' => array(
                'src\\admin\\filters\\ModuleSetupFilter'
            )
        ),
        'importcodesform' => array(
            'controller' => 'src\\admin\\controllers\\ShowImportCodesFormTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'importcodesetup' => array(
            'controller' => 'src\\admin\\controllers\\CodeSetupsController',
            'filters' => array(
                'src\\admin\\filters\\ModuleSetupFilter'
            )
        ), 
        'listcombolinks' => array(
            'controller' => 'src\\admin\\controllers\\ComboLinkingTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'iscombolinklocked' => array("Source/setups/ComboLinking.php", 'IsComboLinkLocked', IsFullAdmin()),
        'login_settings' => array(
            'controller' => 'src\\admin\\controllers\\LoginSettingsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'login_settings_update' => array(
            'controller' => 'src\\admin\\controllers\\LoginSettingsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'in05' => array(
            'controller' => 'src\\admin\\controllers\\IN05TemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\INCModuleLicensedFilter',
                'src\\admin\\filters\\INCSetupPermissionsFilter',
            )
        ),
        'locked_users' => array(
            'controller' => 'src\\admin\\controllers\\LockedOutUsersTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'unlockusers' => array(
            'controller' => 'src\\admin\\controllers\\LockedOutUsersController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'password_policy' => array(
            'controller' => 'src\\admin\\controllers\\PasswordPolicyTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'password_policy_update' => array(
            'controller' => 'src\\admin\\controllers\\PasswordPolicyTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'riskmatrixsetup' => array(
            'controller' => 'src\\admin\\controllers\\RiskMatrixSetupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\INCModuleSetupFilter'
            )
        ),
        'excesssetup' => array(
            'controller' => 'src\\admin\\controllers\\CodeSetupsActionTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\admin\\filters\\ClaimsSetupCodesFilter'
            )
        ),
        // Standards
        'liststandards' => array(
            'controller' => 'src\\standards\\controllers\\ListAllStandardsTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listmyelements' => array(
            'controller' => 'src\\standards\\controllers\\ListMyElementsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listpendingelements' => array(
            'controller' => 'src\\standards\\controllers\\ListPendingElementsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listmypendingelements' => array(
            'controller' => 'src\\standards\\controllers\\ListMyPendingElementsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'standard' => array(
            'controller' => 'src\\standards\\controllers\\ShowStandardController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savestandard' => array(
            'controller' => 'src\\standards\\controllers\\SaveStandardController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'element' => array(
            'controller' => 'src\\standards\\controllers\\ShowElementController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveelement' => array(
            'controller' => 'src\\standards\\controllers\\SaveElementController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'prompt' => array(
            'controller' => 'src\\standards\\controllers\\ShowPromptController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveprompt' => array(
            'controller' => 'src\\standards\\controllers\\SavePromptController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'evidence' => array(
            'controller' => 'src\\standards\\controllers\\ShowEvidenceController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveevidence' => array(
            'controller' => 'src\\standards\\controllers\\SaveEvidenceController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'evidencematch' => array(
            'controller' => 'src\\standards\\controllers\\EvidenceMatchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'newstandard' => array(
            'controller' => 'src\\standards\\controllers\\NewStandardTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        // Actions
        // TODO: This entry can be removed once all functions that redirect to this controller are refactored
        'showactionform' => array(
            'controller' => 'src\\actions\\controllers\\ShowActionFormTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'newaction' => array(
            'controller' => 'src\\actions\\controllers\\NewActionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'action' => array(
            'controller' => 'src\\actions\\controllers\\OpenActionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveaction' => array(
            'controller' => 'src\\actions\\controllers\\SaveActionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'actionssearch' => array(
            'controller' => 'src\\actions\\controllers\\ActionsSearchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'actionsdoselection' => array(
            'controller' => 'src\\actions\\controllers\\ActionsDoSelectionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'showsaveaction' => array(
            'controller' => 'src\\actions\\controllers\\ShowSaveActionTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveactionactions' => array(
            'controller' => 'src\\actions\\controllers\\SaveActionActionsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        //SABS
        'addnewsabs' => array(
            'controller' => 'src\\sabs\\controllers\\NewSABSTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savesabs' => array(
            'controller' => 'src\\sabs\\controllers\\SaveSABSController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'sabs' => array(
            'controller' => 'src\\sabs\\controllers\\ShowMainSABSController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'sabshistory' => array(
            'controller' => 'src\\sabs\\controllers\\ShowHistoryFormTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'sabshistoryaction' => array(
            'controller' => 'src\\sabs\\controllers\\HistoryActionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'sabslinkcontact' => array(
            'controller' => 'src\\sabs\\controllers\\SABSLinkContactController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'sabscontactlinkaction' => array(
            'controller' => 'src\\sabs\\controllers\\SABSContactLinkActionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'sabssearch' => array(
            'controller' => 'src\\sabs\\controllers\\SABSSearchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'sabsdoselection' => array(
            'controller' => 'src\\sabs\\controllers\\SABSDoSelectionController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveresponselinktomain' => array("Source/SABS/SABSResponses.php", 'SaveResponseLinkToMain'),
        'response' => array("Source/SABS/SABSResponses.php", 'ShowResponse'),
        'saveresponse' => array("Source/sabs/SaveSABS.php", 'SaveSABSResponse'),
        'sabslinkdistribution' => array("Source/contacts/SABSContactForm.php", 'ShowDistributionContactForm'),
        'activate' => array("Source/Login.php", 'ActivateLogin'),
        // Assets (Equipment) Module
        // TODO: This entry can be removed once all functions that redirect to this controller are refactored
        'showassetform' => array(
            'controller' => 'src\\ast\\controllers\\ShowAssetFormTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'addnewasset' => array(
            'controller' => 'src\\ast\\controllers\\NewAssetController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'asset' => array(
            'controller' => 'src\\ast\\controllers\\ShowAssetController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'assetssearch' => array(
            'controller' => 'src\\ast\\controllers\\AssetsSearchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'assetlinkaction' => array(
            'controller' => 'src\\ast\\controllers\\SaveAssetController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'linkequipment' => array("Source/incidents/TrustProperty.php", 'EquipmentLink'),
        // Medications Module
        'addnewmedication' => array(
            'controller' => 'src\\med\\controllers\\NewMedicationController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'medication' => array(
            'controller' => 'src\\med\\controllers\\ShowMedicationController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'medicationssearch' => array(
            'controller' => 'src\\med\\controllers\\MedicationsSearchController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'medicationlinkaction' => array(
            'controller' => 'src\\med\\controllers\\SaveMedicationController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        // Distribution lists
		'distrlinkcontact' => array("Source/contacts/DistrContactForm.php", 'ContactLink'),
        'distrcontactlinkaction' => array("Source/contacts/DistrContactForm.php", 'ContactLinkAction'),
		'distrdoselection' => array("Source/distribution/SearchDistr.php", "DistrDoSelection"),
		// LDAP
		'ldapbrowse' => array("Source/libs/LDAPBrowser.php", 'LDAPBrowse'),
		'ldaplistentries' => array("Source/libs/LDAPBrowser.php", 'LDAPListEntries'),
		'ldaplogin' => array("Source/libs/LDAPBrowser.php", 'LDAPLogin'),
        'ldapsync' => array("Source/libs/LDAPBrowser.php", 'LDAPSync'),
        'ldapsynclogin' => array("Source/libs/LDAPBrowser.php", 'LDAPSyncLogin'),
        'ldapsyncall' => array("Source/libs/LDAPBrowser.php", 'LDAPSyncall'),
        'ldapprofilesync' => array("Source/libs/LDAPBrowser.php", 'LDAPProfileSync'),
        'ldapprofilesyncall' => array("Source/libs/LDAPBrowser.php", 'LDAPProfileSyncall'),
        'ldapusersyncall' => array("Source/libs/LDAPBrowser.php", 'LDAPUserSyncall'),
        'ldapconfig' => array(
            'controller' => 'src\\admin\\controllers\\LDAPConfigController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        // Documents
        'downloaddocument' => array(
            'controller' => 'src\\documents\\controllers\\DocumentController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'editdocument' => array(
            'controller' => 'src\\documents\\controllers\\DocumentTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'savedocument' => array(
            'controller' => 'src\\documents\\controllers\\DocumentTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'deletedocument' => array(
            'controller' => 'src\\documents\\controllers\\DocumentTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // Record Linking
        'linkrecords' => array(
            'controller' => 'src\\generic\\controllers\\LinkRecordsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'editlinkedrecord' => array(
            'controller' => 'src\\generic\\controllers\\EditLinkedRecordTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // Email Templates
        'emailtemplate' => array(
    		'controller' => 'src\\admin\\controllers\\EmailTemplateController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        'listemailtemplates' => array(
    		'controller' => 'src\\admin\\controllers\\EmailTemplateController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        'configureemailtemplates' => array(
            'controller' => 'src\\admin\\controllers\\EmailTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'emailtemplateoptions' => array(
            'controller' => 'src\\admin\\controllers\\EmailTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        //Web Links
        'linkweblink' => array(
            'controller' => 'src\\weblinks\\controllers\\WebLinksTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'weblinkaction' => array(
            'controller' => 'src\\weblinks\\controllers\\SaveWebLinkController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        // Reports
        'dashboard' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'addcurrenttomydashboard' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'savewidget' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'deletewidget' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'savewidgetasnew' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController'
        ),
        'listmyreports' => array(
            'controller' => 'src\\reports\\controllers\\MyReportsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')),
        'deletemyreport' => array(
            'controller' => 'src\\reports\\controllers\\MyReportsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')),
        'savequery' => array("Source/libs/SavedQueries.php", 'NewSavedQuery'),
        'savequeryaction' => array("Source/libs/SavedQueries.php", 'SaveQuery'),
        'savedqueries' => array(
            'controller' => 'src\\savedqueries\\controllers\\ShowSavedQueriesTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'executequery' => array(
            'controller' => 'src\\savedqueries\\controllers\\ExecuteQueryController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'isQueryPinned' => [
            'controller' => 'src\\savedqueries\\controllers\\SavedQueriesController',
            'filters' => [
                'src\\framework\\controller\\LoggedInFilter'
            ]
        ],
		'getpromptdata' => array("Source/libs/SavedQueries.php", 'GetPromptData'),
        'reportsadminlist' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listlistingreports' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'reportsadmineditsave' => array("Source/libs/ReportsAdmin.php", 'ReportsAdminSaveAction'),
        'packagedreportsadmineditsave' => array("Source/libs/ReportsAdmin.php", 'PackagedReportsAdminSaveAction'),
        'reportsadminaction' => array("Source/libs/ReportsAdmin.php", 'ReportsAdminAction'),
        'reportpackage' => array("Source/libs/ReportsAdmin.php", 'PackagedReport'),
        'exportswitch' => array("Source/reports.php", 'ExportSwitch'),
        'exportreportdesign' => array("Source/libs/ReportsAdmin.php", 'ExportReportDesign'),
        'exportswitch_dashboard' => array("Source/reports.php", 'ExportSwitchFromDashboard'),

        'riskTrackerReport' => array(
            'controller' => 'src\\reports\\controllers\\RiskTrackerTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
            )
        ),
        'doctemplateadminlist' => array(
            'controller' => 'src\\admin\\controllers\\DocTemplatesAdminListTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\FullAdminFilter'
            )
        ),
        'doctemplateadmineditsave' => array(
            'controller' => 'src\\wordmergetemplate\\controllers\\DocumentTemplatesController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\FullAdminFilter'
            )
        ),
        'doctemplateaction' => array(
            'controller' => 'src\\wordmergetemplate\\controllers\\DocumentTemplatesController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\FullAdminFilter'
            )
        ),
        // AJAX
        'httprequest' => array("Source/libs/httprequest.php", 'httpRequest'),
		// Generic Functions
        'linkcontactgeneral' => array("Source/contacts/GenericContactForm.php", 'ContactLink'),
        'contactlinkactiongeneral' => array("Source/contacts/GenericContactForm.php", 'ContactLinkAction'),
        'contactgeneral' => array("Source/contacts/GenericContactForm.php", 'ContactLink'),
        // Respondents
        'redirecttorespondent' => array(
            'controller' => 'src\\respondents\\controllers\\RespondentsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'unlinkrespondent' => array(
            'controller' => 'src\\respondents\\controllers\\RespondentsController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'editorganisationlink' => array(
            'controller' => 'src\\organisations\\controllers\\OrganisationsTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'getrespondentpayments' => [
            'controller' => 'src\\respondents\\controllers\\RespondentsController',
            'filters' => [
                'src\\framework\\controller\\LoggedInFilter'
            ]
        ],
        // Policies
        'displaypolicylinkform' => array(
            'controller' => 'src\\policies\\controllers\\PoliciesTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'editpolicylink' => array(
            'controller' => 'src\\policies\\controllers\\PoliciesTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'listmatchingpolicies' => array(
            'controller' => 'src\\policies\\controllers\\PoliciesTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // ing
        'usersessions' => array(
            'controller' => 'src\\admin\\controllers\\ShowUserSessionsTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\admin\\filters\\SubAdminFilter',
            )
        ),
        'lockedrecords' => array(
            'controller' => 'src\\admin\\controllers\\LockedRecordsTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // Overdue Emails
        'overdueemails' => array(
            'controller' => 'src\\admin\\controllers\\OverdueEmailsTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
		// Security
        'listusers' => array(
            'controller' => 'src\\admin\\controllers\\ListUsersTemplateController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'listgroups' => array(
            'controller' => 'src\\admin\\security\\controllers\\GroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\ADM_GROUP_SETUPGlobalFilter',
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'listblanksecuritygroups' => array(
            'controller' => 'src\\admin\\security\\controllers\\GroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\ADM_GROUP_SETUPGlobalFilter',
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'edituser' => array(
            'controller' => 'src\\users\\controllers\\UserTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveuser' => array(
            'controller' => 'src\\users\\controllers\\UserController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'deleteuser' => array(
            'controller' => 'src\\users\\controllers\\UserController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'deletegroup' => array(
            'controller' => 'src\\admin\\security\\controllers\\GroupController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editgroup' => array(
            'controller' => 'src\\admin\\security\\controllers\\GroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\ADM_GROUP_SETUPGlobalFilter',
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'savegroup' => array(
            'controller' => 'src\\admin\\security\\controllers\\GroupController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'add_group_for_user' => array(
            'controller' => 'src\\security\\controllers\\AddGroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'add_group_for_profile' => array(
            'controller' => 'src\\security\\controllers\\AddGroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'add_user_to_group' => array(
            'controller' => 'src\\security\\controllers\\AddGroupTemplateController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'add_user_to_profile' => array(
            'controller' => 'src\\security\\controllers\\AddUserToProfileTemplateController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'save_user_group_link' => array(
            'controller' => 'src\\security\\controllers\\SaveUserGroupLinkController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'save_user_profile_link' => array(
            'controller' => 'src\\security\\controllers\\SaveUserProfileLinkController',
            'filters' => array(
                'src\\admin\\filters\\SubAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
		'editrecordperms' => array("Source/security/SecurityRecordPerms.php", 'EditRecordPerms'),	
        'saverecordperms' => array("Source/security/SecurityRecordPerms.php", 'SaveRecordPerms'),
        'editprofile' => array(
            'controller' => 'src\\admin\\controllers\\ProfileTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'deleteprofile' => array(
            'controller' => 'src\\admin\\controllers\\ProfileController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'copyprofile' => array(
            'controller' => 'src\\admin\\controllers\\ProfileController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'saveprofile' => array(
            'controller' => 'src\\admin\\controllers\\ProfileController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'listprofiles' => array(
            'controller' => 'src\\admin\\controllers\\ProfileTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'saveprofileorder' => array(
            'controller' => 'src\\admin\\controllers\\ProfileController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // Import data
        'import_form' => array(
            'controller' => 'src\\admin\\controllers\\ImportXMLDataTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'import' => array("soap/Import.php", 'ImportData'),
        'importtabledata' => array(
            'controller' => 'src\\admin\\controllers\\ImportTableDataTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // Exclude report columns
        'excludefields' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // Extra Fields
    	'listextrafields' => array(
    		'controller' => 'src\\admin\\controllers\\UDFSetupController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
    	'editextrafield' => array(
    		'controller' => 'src\\admin\\controllers\\UDFSetupController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        'saveextrafield' => array(
    		'controller' => 'src\\admin\\controllers\\UDFSetupController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        'checkmappingchanged' => [
            'controller' => 'src\\extrafields\\controllers\\ExtraFieldController',
            'filters' => [
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            ]
        ],
        //Home screen
        'home' => array(
            'controller' => 'src\\generic\\controllers\\HomeScreens',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        //Workflow administration
        'workflowadmin' => array(
            'controller' => 'src\\admin\\controllers\\WorkflowAdminTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'workflowadminsave' => array(
            'controller' => 'src\\admin\\controllers\\WorkflowAdminTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),

        //Excel import mapping
        'listimportprofiles' => array(
            'controller' => 'src\\admin\\controllers\\ListExcelImportProfilesTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editimportprofile' => array(
            'controller' => 'src\\excelimport\\controllers\\EditImportProfileTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'saveimportprofile' => array(
            'controller' => 'src\\excelimport\\controllers\\SaveImportProfileController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'deleteimportprofile' => array(
            'controller' => 'src\\excelimport\\controllers\\DeleteImportProfileController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'exportimportprofile' => array("Source/setups/ExcelImport.php", "ExportProfile"),
        'importimportprofile' => array(
            'controller' => 'src\\excelimport\\controllers\\ImportImportProfileTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'doimportimportprofile' => array("Source/setups/ExcelImport.php", "DoImportProfile"),

        //CQC Outcomes
        'listcqctemplates' => array(
            'controller' => 'src\\admin\\cqc\\controllers\\CqcOutcomeTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editcqcoutcometemplate' => array(
            'controller' => 'src\\admin\\cqc\\controllers\\CqcOutcomeTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editcqcprompttemplate' => array(
            'controller' => 'src\\admin\\cqc\\controllers\\CqcPromptTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'editcqcsubprompttemplate' => array(
            'controller' => 'src\\admin\\cqc\\controllers\\CqcSubpromptTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'cqcselectlocations' => array(
            'controller' => 'src\\admin\\cqc\\controllers\\ApplyCqcOutcomesController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'cqcselectoutcomes' => array(
            'controller' => 'src\\admin\\cqc\\controllers\\ApplyCqcOutcomesController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'cqccheckduplicateoutcomes' => array("Source/classes/CQC/TemplateClass.php", array('CQC_Template', 'CheckDuplicateOutcomes')),
        'cqcgenerateoutcomes' => array("Source/classes/CQC/TemplateClass.php", array('CQC_Template', 'GenerateOutcomes')),
        'cqclocations' => array("Source/classes/CQC/LocationHierarchy.php", array('CQC_LocationHierarchy', 'index')),
        'savelocation' => array("Source/classes/CQC/LocationHierarchy.php", array('CQC_LocationHierarchy', 'save')),
        'addtier' => array("Source/classes/CQC/LocationHierarchy.php", array('CQC_LocationHierarchy', 'addTier')),
        'newlocation' => array("Source/classes/CQC/LocationHierarchy.php", array('CQC_LocationHierarchy', 'newLocation')),
        'editlocation' => array("Source/classes/CQC/LocationHierarchy.php", array('CQC_LocationHierarchy', 'editLocation')),
        'deletelocation' => array("Source/classes/CQC/LocationHierarchy.php", array('CQC_LocationHierarchy', 'deleteLocation')),
        'report8' => array("Source/reporting/CustomListingReports.php", 'report8'), //cqc hard coded listing report
        'deletetier' => array("Source/classes/CQC/LocationHierarchy.php", array('CQC_LocationHierarchy', 'deleteTier')),
        'cqcoutcomesummaryreport' => array(
            'controller' => 'src\\admin\\cqc\\controllers\\CqcOutcomeTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),

        //Tasks
        'listtasks' => array(
    		'controller' => 'src\\admin\\controllers\\ListTasksTemplateController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        'task' => array(
            'controller' => 'src\\tasks\\controllers\\TasksTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'savetask' => array(
            'controller' => 'src\\tasks\\controllers\\TaskController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'deletetask' => array(
            'controller' => 'src\\tasks\\controllers\\TaskController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        // NPSA
        'npsaexport' => array("Source/libs/ExportNPSA.php", "ExportNPSA"),
        'publicholidays' => array(
    		'controller' => 'src\\admin\\controllers\\PublicHolidaysTemplateController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        'publicholidaysupdate' => array(
    		'controller' => 'src\\admin\\controllers\\PublicHolidaysTemplateController',
    		'filters' => array(
    			'src\\admin\\filters\\FullAdminFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        
        // Action Chains
        'listactionchains' => array(
            'controller' => 'src\\admin\\actionchains\\controllers\\ActionChainsAdminController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'displayactionchain' => array(
            'controller' => 'src\\admin\\actionchains\\controllers\\ActionChainsAdminController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'saveactionchain' => array(
            'controller' => 'src\\admin\\actionchains\\controllers\\ActionChainsAdminController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'deleteactionchain' => array(
            'controller' => 'src\\admin\\actionchains\\controllers\\ActionChainsAdminController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'createactionsfromchain' => array(
            'controller' => 'src\\actionchains\\controllers\\ActionChainController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'deletechainfromrecord' => array(
            'controller' => 'src\\actionchains\\controllers\\ActionChainController',
            'filters' => array(
                'src\\actionchains\\filters\\DeleteActionChainFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),

        // Filters
        'dofilter' => array(
            'controller' => 'src\\listingfilters\\controllers\\FilterController',
            'filters' => array('src\\framework\\controller\\LoggedInFilter')
            ),

        // CQC Manage Links
        'cqcunlink' => array(
            'controller' => 'src\\library\\controllers\\CQCLinkController',
            'filters' => array('src\\framework\\controller\\LoggedInFilter')
        ),
        'newcqclink' => array(
            'controller' => 'src\\library\\controllers\\CQCLinkTemplateController',
            'filters' => array('src\\framework\\controller\\LoggedInFilter')
        ),
        'savecqclinks' => array(
            'controller' => 'src\\library\\controllers\\CQCLinkController',
            'filters' => array('src\\framework\\controller\\LoggedInFilter')
        ),

        // Exclude Batch update columns
        'excludebatchupdate' => array(
            'controller' => 'src\\admin\\controllers\\ExcludeBatchUpdateTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'saveexcludebatchupdate' => array(
            'controller' => 'src\\admin\\controllers\\ExcludeBatchUpdateTemplateController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
            
        // Assessment Modules
        'asmselecttemplatetype' => array("Source/classes/ASM/TemplateClass.php", array('ASM_Template', 'SelectTemplateType'), bYN(GetParm('ATM_CAI', 'N'))),
        'asmselectlocations' => array("Source/classes/ASM/TemplateClass.php", array('ASM_Template', 'SelectLocations'), CanSeeModule('LOC') && bYN(GetParm('ATM_CAI', 'N'))),
        'asmselectmodules' => array("Source/classes/ASM/TemplateClass.php", array('ASM_Template', 'SelectModules'), CanSeeModule('LOC') && (CanSeeModule('ATM') || CanSeeModule('ATI')) && bYN(GetParm('ATM_CAI', 'N'))),
        'asmcreateinstances' => array("Source/classes/ASM/TemplateClass.php", array('ASM_Template', 'CreateInstances'), CanSeeModule('LOC') && (CanSeeModule('ATM') || CanSeeModule('ATI')) && bYN(GetParm('ATM_CAI', 'N'))),
        'asmredistributetemplates' => array(
            'controller' => 'src\\assessments\\controllers\\CyclesController',
            'filters' => array(
                'src\\assessments\\filters\\RedistributeTemplatesFilter',
                'src\\framework\\controller\\LoggedInFilter')
            ),
        'asmcreateassignments' => array(
            'controller' => 'src\\assessments\\controllers\\AssessmentTemplatesController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'asmlocations' => array(
            'controller' => 'src\\admin\\controllers\\AsmLocationHierarchyController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),  
        'getlocation' => array(
            'controller' => 'src\\locations\\controllers\\LocationController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'saveasmlocation' => array(
            'controller' => 'src\\locations\\controllers\\LocationController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'asmshowsearchlocation' => array(
            'controller' => 'src\\admin\\controllers\\AsmLocationHierarchyController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),

        'newcreateinstances' => array(
            'controller' => 'src\\assessments\\controllers\\InstanceController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\assessments\\filters\\CanCreateInstancesFilter',
            )),
        'filterlocationscreateinstances' => array(
            'controller' => 'src\\assessments\\controllers\\InstanceController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter',
                'src\\assessments\\filters\\CanCreateInstancesFilter',
            )),
        'listcreatedassessmentinstances' => array(
            'controller' => 'src\\assessments\\controllers\\InstanceController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')),

        'reportdesigner' => array(
            'controller' => 'src\\reports\\controllers\\ReportDesignerTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')),

        'getdatefields' => array(
            'controller' => 'src\\reports\\controllers\\ReportDesignFormController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')),
        'drillin' => array(
            'controller' => 'src\\reports\\controllers\\DrillInController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter'
            )
        ),
        'queryhasatprompt' => array(
            'controller' => 'src\\reports\\controllers\\ReportDesignFormController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'getatpromptsection' => array(
            'controller' => 'src\\reports\\controllers\\ReportDesignFormController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'designabasereport' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'saveabasereport' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
    	'exportabasereport' => array(
    		'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
    		'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
    			'src\\framework\\controller\\LoggedInFilter')
    	),
        'deleteabasereport' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'designapackagedreport' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'querybyexampleforpackagedreport' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'saveapackagedreport' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'deleteapackagedreport' => array(
            'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminController',
            'filters' => array(
                'src\\admin\\filters\\DesignReportFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'openinexcel' => array(
            'controller' => 'src\\reports\\controllers\\ReportController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'loadlistingreportpage' => array(
            'controller' => 'src\\reports\\controllers\\ListingReportWriter',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'getrecordcount' => array(
            'controller' => 'src\\reports\\controllers\\ListingReportWriter',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'loaddashboardlistingreportpage' => array(
            'controller' => 'src\\dashboard\\controllers\\DashboardController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),

        //new document merge
        'mergedocument' => array(
            'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')),
            
        'processhotspotsqueue' => array(
            'controller' => 'src\\hotspots\\controllers\\HotspotsServiceController',
            'filters'    => array('src\\hotspots\\filters\\CliOnlyFilter')
        ),
        'clearhotspotqueue' => array(
            'controller' => 'src\\hotspots\\controllers\\HotspotsServiceController',
            'filters'    => array('src\\hotspots\\filters\\CliOnlyFilter')
        ),
        'printSuccessMessage' => array(
            'controller' => 'src\\login\\controllers\\RegisterTemplateController'
        ),

        'showsnapshot' => array(
            'controller' => 'src\\incidents\\controllers\\IncidentSnapshotController',
            'filters' => array(
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'automaticcontactmerge' => array(
            'controller' => 'src\\contacts\\controllers\\ContactMergeController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'diagnosesystemproblems' => array(
            'controller' => 'src\\admin\\controllers\\SystemDiagnosisController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\admin\\filters\\DatixSupportOnlyFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'dmdimportform' => array(
            'controller' => 'src\\admin\\dmd\\controllers\\DmdController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'importdmd' => array(
            'controller' => 'src\\admin\\dmd\\controllers\\DmdController',
            'filters' => array(
                'src\\admin\\filters\\FullAdminFilter',
                'src\\framework\\controller\\LoggedInFilter')
        ),
        'sendemails' => array(
            'controller' => 'src\\email\\controllers\\NotificationEmailController',
            'filters' => array('src\\hotspots\\filters\\CliOnlyFilter')
        ),
    );

    if ($_GET['action'] != "httprequest" && (checkGeneralMigrationNeeded()) && SetSessionActive() && $_SESSION['logged_in'] && $_SESSION['AdminUser'])
    {
        require_once 'Source/libs/FormMigration.php';

        //hack to prevent page reload for dod.
        $_GET['action'] = 'formmigrate';
        MigrateForms();
        obExit();
    }

    if ($_GET["action"] != 'httprequest' && $_GET["action"] != 'login' && $_GET["action"] != 'login2'
        && $_GET["action"] != 'password2' && $_GET["action"] != 'logout' && $_GET["action"] != 'licence')
    {
        PutLicenceIntoSession();
    }

    //Whitelist logged out pages
    $WhiteListLoggedOutPages = array(
        'login',
        'login2',
        'password2',
        'logout',
        'httprequest',
        'licence',
        'register',
        'contactformregister2',
        'saveincident',
        'saverisk',
        'saverecord',
        'incident',
        'risk',
        'record',
        'password',
        'reset_password',
        'reset_password2',
        'fieldhelp',
        'disagreementmessage',
        'clearhotspotqueue',
        'processhotspotsqueue',
        'printsuccessmessage'
    );

    //=================================================
    //  Check if upload doesn't exceed maximum limit
    //=================================================
    $isThisAFileUpload = (strstr($_SERVER['CONTENT_TYPE'],'multipart/form-data') !== FALSE);
    $fileUploadExceedsMaximumUploadSize = ($isThisAFileUpload && empty($_POST) && empty($_FILES));

    if ($fileUploadExceedsMaximumUploadSize)
    {
        fatal_error(_tk('large_document_upload_error'));
    }


    //=================================================
    //  Cross-site Request Forgery attach prevention
    //=================================================
    $CSRFWhitelist = array (
        'httprequest','logout','login','login2','list','reports','reports2','staffresponsibilities',
        'showerrorlog','home','dashboard','codelist','password', 'reset_password', 'disagreementmessage',
        'drillin', 'fieldhelp', 'record', 'incident', 'risk', 'sabs'
    );

    if (bYN(GetParm('CSRF_PREVENTION','N')) && !empty($_REQUEST['action']))
    {
        // When file uploads exceed the maximum upload limit, the request is broken
        // and we don't get a token, but we still need to display a "file upload
        // exceeded" message, so we have to skip token verification (otherwise we
        // get a misleading "invalid token" error message)
        if (!$fileUploadExceedsMaximumUploadSize)
        {
            $CSRFGuard = new CSRFGuard ($CSRFWhitelist);
            if (!$CSRFGuard->validateAuth ($_REQUEST['action'], $_REQUEST['token']))
            {
                throw new PermissionDeniedException('Invalid token.');
            }
        }
    }
    //=================================================


    if ($_GET['action'] && !in_array(strtolower($_GET['action']), $WhiteListLoggedOutPages))
    {
        LoggedIn();
    }

    if (!ini_get('max_input_vars') || ini_get('max_input_vars') == 1000)
    {
        fatal_error(_tk('max_input_vars_error'));
    }

    if (isset($actionArray[$action]))
    {
        if (isset($actionArray[$action]['controller']) && class_exists($actionArray[$action]['controller']) && in_array('src\\framework\\controller\\Controller', class_parents($actionArray[$action]['controller'])))
        {
            // use new Controller class structure
            $loader = new src\framework\controller\Loader();

            $controller = $loader->getController($actionArray[$action]);
            echo $controller->doAction($action);
            obExit();
        }

        $includeFile = $actionArray[$action][0];
        $executeFunction = $actionArray[$action][1];

        if (isset($actionArray[$action][2]))
        {
            if (!$actionArray[$action][2])
            {
                switch ($action)
                {
                    case 'asmselectlocations':
                    case 'filterlocationscreateinstances':
                        fatal_error(_tk('no_permission_loc'), 'Notification', 'ADM', 'info_div');
                        break;
                    case 'asmselectmodules':
                        fatal_error(_tk('no_permission_asm'), 'Notification', 'ADM', 'info_div');
                        break;
                    default:
                        fatal_error(_tk('no_permission'));
                        break;
                }
            }
        }

        if ($includeFile != null)
        {
            require_once $includeFile;
        }

        if ($executeFunction != '')
        {
            if (is_array($actionArray[$action][3]))
            {
                call_user_func_array($executeFunction, $actionArray[$action][3]);
            }
            else
            {
                call_user_func($executeFunction);
            }
		}
    }

	if ($action == "httprequest")
    {
		obExit();
	}

    //call class method directly. This requires less overhead and doesn't require large arrays mapping to different functions.
    if (!$_GET['action'] && $_GET['service'] && $_GET['event'])
    {
        //provide $_GET parameters as local parameters to allow same execution whether called locally or via url.
        if (method_exists('Service_'.$_GET['service'], $_GET['event']))
        {
            $Service = 'Service_'.$_GET['service'];
            $ServiceObj = new $Service($_GET);
            $ServiceObj->$_GET['event']($_GET);
            obExit();
        }
    }

    if ($_SESSION["logged_in"])
    {
        //require_once 'Source/HomeScreens.php';
        //HomeScreen();

        $loader = new src\framework\controller\Loader();
        $controller = $loader->getController($actionArray['home']);
        echo $controller->doAction('home');
    }
    else
    {
        require_once "Source/Login.php";

        if (!$NoOpenAccess) // if NoOpenAccess is set, we just go straight to the login page.
        {
            $Level1Module = GetDefaultModule(array('default'=> Sanitize::getModule($_GET['module'])));
            $_GET['module'] = $Level1Module; //newrecord functions look at $_GET
        }

        switch ($Level1Module)
        {
            case 'CLA':
                require_once "Source/generic/MainRecord.php";
                NewRecord();
                break;
            case 'PAL':
                require_once "Source/generic/MainRecord.php";
                NewRecord();
                break;
            case 'COM':
                require_once "Source/generic/MainRecord.php";
                NewRecord();
                break;
            case 'RAM':
                // Use new Controller class structure
                $loader = new src\framework\controller\Loader();
                $controller = $loader->getController($actionArray['newrisk1']);
                echo $controller->doAction('newrisk1');
                break;
            case 'INC':
                // Use new Controller class structure
                $loader = new src\framework\controller\Loader();
                $controller = $loader->getController($actionArray['newdif1']);
                echo $controller->doAction('newdif1');
                break;
            default:
                require_once "Source/Login.php";

                if (bYN(GetParm('WEB_NETWORK_LOGIN')))
                {
                    LoggedOutSSOPage();
                }
                else
                {
                    $yySetLocation = $scripturl . '?action=login';
                    redirectexit();
                }

                break;
        }
    }

    obExit();
}

/**
* @desc Gets all the parent/child combinations from the database and stores them in the session as
* $_SESSION['CachedValues']['ParentChild'][<module>][<field>] = array of children.
* and $_SESSION['CachedValues']['ChildParent'][<module>][<field>] = array of parents.
*
* @global array $ModuleDefs
*/
function CacheParents()
{
    global $ModuleDefs;

    if (!is_array($_SESSION['CachedValues']['ParentChild']) || !is_array($_SESSION['CachedValues']['ChildParent']))
    {
        $sql = 'SELECT cmb_module, cmb_parent, cmb_child, cmb_table
        FROM combo_links
        WHERE cmb_parent is not null AND cmb_parent != \'\'
        AND cmb_child is not null AND cmb_child != \'\'
        ORDER BY recordid';

        $parents = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($parents as $parent)
        {
            if ($parent['cmb_table'] != $ModuleDefs[$parent['cmb_module']]['TABLE'])
            {
                $parent['cmb_parent'] = CheckFieldMappings($parent['cmb_module'], $parent['cmb_parent']);
                $parent['cmb_child'] = CheckFieldMappings($parent['cmb_module'], $parent['cmb_child']);
            }

            $_SESSION['CachedValues']['ParentChild'][$parent['cmb_module']][$parent['cmb_parent']][] = $parent['cmb_child'];
            $_SESSION['CachedValues']['ChildParent'][$parent['cmb_module']][$parent['cmb_child']][] = $parent['cmb_parent'];
        }

        //get children from field formats too
        $sql =
        "SELECT DISTINCT fmt_field, fmt_module, fmt_code_parent, fmt_code_parent2, fmt_table
        FROM field_formats
        WHERE
            ((fmt_code_parent != '' AND fmt_code_parent IS NOT NULL) OR (fmt_code_parent2 != '' AND fmt_code_parent2 IS NOT NULL))
            AND fmt_field NOT IN (
                SELECT cmb_child
                FROM combo_links
                WHERE
                    cmb_module = fmt_module
                    AND cmb_table = fmt_table
                    AND cmb_child is not null
                    AND cmb_child != ''
                )";

        $children = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($children as $child)
        {
            if ($child['fmt_table'] != $ModuleDefs[$child['fmt_module']]['TABLE'])
            {
                $child['fmt_field'] = CheckFieldMappings($child['fmt_module'], $child['fmt_field']);
                $child['fmt_code_parent'] = CheckFieldMappings($child['fmt_module'], $child['fmt_code_parent']);
                $child['fmt_code_parent2'] = CheckFieldMappings($child['fmt_module'], $child['fmt_code_parent2']);
            }

            if ($child['fmt_code_parent'])
            {
                $_SESSION['CachedValues']['ParentChild'][$child['fmt_module']][$child['fmt_code_parent']][] = $child['fmt_field'];
                $_SESSION['CachedValues']['ChildParent'][$child['fmt_module']][$child['fmt_field']][] = $child['fmt_code_parent'];
            }

            if ($child['fmt_code_parent2'])
            {
                $_SESSION['CachedValues']['ParentChild'][$child['fmt_module']][$child['fmt_code_parent2']][] = $child['fmt_field'];
                $_SESSION['CachedValues']['ChildParent'][$child['fmt_module']][$child['fmt_field']][] = $child['fmt_code_parent2'];
            }
        }

        // add CON parenting setup to ADM for employee fields etc
        $_SESSION['CachedValues']['ParentChild']['ADM'] = $_SESSION['CachedValues']['ParentChild']['CON'];
        $_SESSION['CachedValues']['ChildParent']['ADM'] = $_SESSION['CachedValues']['ChildParent']['CON'];
    }
}

function setupViewClass()
{
    global $DatixView, $scripturl;

    $DatixView = new Zend_View();

    $DatixView->setScriptPath('Views'); //set base path for view file locations

    $DatixView->scripturl = $scripturl;
}