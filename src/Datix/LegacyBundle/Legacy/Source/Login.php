<?php

// Function Login2() is either called by a POST from the Login page or is
// called directly from Login() if WEB_NETWORK_LOGIN (i.e. SSO) is set.
// If POSTed, no parameters are required.  Otherwise:
// $NetworkLogin:       TRUE if SSO is being used.
// $User:               The login name
// $Domain:             The domain name, if applicable
// $URL:                The URL to redirect to
function Login2($NetworkLogin = false, $User = '', $Domain = '', $URL = '')
{
	global $cookielength, $passwrd, $cookieneverexp, $yySetLocation, $scripturl, $dbname, $dtxdebug;

    require_once 'Source/security/SecurityBase.php';

    // No need to do validation if using SSO.
    if (!$NetworkLogin)
    {
        $URL  = Sanitize::SanitizeURL($_POST['url']);
        $User = \UnicodeString::trim($_POST["user"]);

        if ($User == "")
        {
            AuditLogin('', 'Login failed: empty user name');
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                $yySetLocation = $scripturl . '?action=login&user=' . StripPostQuotes($User) . '&login_failed=1';
            }
            else
            {
                $yySetLocation = $scripturl . '?action=login&user=' . StripPostQuotes($User) . '&error=' . _tk('no_user_name_err');
            }
            redirectexit();
        }

        if (preg_match("/([\"\/\[\]\:\;\|\=\,\+\*\?\<\>]+)/u", $User, $matches))
        {
            AuditLogin($User, 'Login failed: invalid character');
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                $yySetLocation = $scripturl . '?action=login&user=' . StripPostQuotes($User) . '&login_failed=1';
            }
            else
            {
                $yySetLocation = $scripturl . '?action=login&user=' . StripPostQuotes($User) . '&error=' . _tk('wrong_user_pwd_err');
            }
            redirectexit();
        }

        if (!preg_match("/^([0-9A-Za-z-]{0,})([\\\\]{0,1})([^\"\/\\\[\]\:\;\|\=\,\+\*\?\<\>]+)$/u", $User, $matches))
        {
            AuditLogin($User, 'Login failed: invalid character');
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                $yySetLocation = $scripturl . '?action=login&user=' . StripPostQuotes($User) . '&login_failed=1';
            }
            else
            {
                $yySetLocation = $scripturl . '?action=login&user=' . StripPostQuotes($User) . '&error=' . _tk('wrong_user_pwd_err');
            }
            redirectexit();
        }

        $passwrd = $_POST["passwrd"];
    }

    // If using SSO, the domain will have been passed to this function
    // as a parameter.
    if (!$NetworkLogin)
    {
        $Domain = Sanitize::SanitizeString($_POST["domain"]);
    }

    $user_no_domain = $User;

    if ($Domain != "")
    {
        $User = $Domain . '\\' . $User;
    }

    // Display maintenance mode error.
    if (bYN(GetParm("NO_LOGIN", "N")))
    {
        if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
        {
            $yySetLocation = $scripturl . '?action=login&user=' . $User . '&login_failed=1';
        }
        else
        {
            $yySetLocation = $scripturl . '?action=login&user=' . $User . '&error=' . _tk('maintenance_mode_err');
        }
        redirectexit();
    }

	$cookielength = $_POST["cookielength"];

	if ($cookielength == "Forever")
	{
		$cookielength   = 1;
		$cookieneverexp   = 'on';
	}

	require_once 'Source/libs/LDAP.php';
	$ldap = new datixLDAP();

	$LDAPAuthentication = $ldap->LDAPEnabled() && !empty($Domain);

    if ($LDAPAuthentication === true)
    {
        if ($Domain)
        {
            $ldap_index = array_flip($ldap->ldap['domains'])[$Domain];
        }

        // Perform active directory authentication
		$ldap->AuthenticateUser($User, $passwrd, $ldap_index);

        if ($ldap->error && !$ldap->DatixAuthentication())
        {
            $ldap->Close();
    		AuditLogin($User, 'Login failed: LDAP: ' . EscapeQuotes($ldap->errorMsg()));
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
            }
            else
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('wrong_user_pwd_err');
            }
            redirectexit();
        }

		if ($dtxdebug)
        {
            $_SESSION["ldap_debug"] = serialize($ldap);
        }
    }

    if ($LDAPAuthentication && !$ldap->error)
    {
        // Return an error if the user has no SID
        if ($ldap->user['sid'] == '')
        {
            $ldap->Close();
    		AuditLogin($User, 'Login failed: LDAP: no SID found');
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
            }
            else
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=Incorrect user name or password';
            }
            redirectexit();
        }

        //If we are not using LDAP for security, we can ignore this step.
        if(GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS')
        {
            // check that user has access according to LDAP Profile and Group mappings
            $HasMappedProfile = $ldap->CheckUserProfileAccess();
            $HasMappedGroup = $ldap->CheckUserAccess();

            if ($HasMappedProfile === false && $HasMappedGroup === false )
            {
                $ldap->Close();
                AuditLogin($User, 'Login failed: LDAP: ' . $ldap->errorMsg());
                if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                {
                    $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
                }
                else
                {
                    $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=Incorrect user name or password';
                }
                redirectexit();
            }
        }

		// check for an existing staff record, if there isn't one create it
        $sql = "SELECT recordid, initials, fullname, lockout, permission, email, tel1,
    		login_tries, sta_clingroup, sta_orgcode, sta_directorate,
    		sta_specialty, sta_unit, login, sta_surname, sta_forenames, sta_pwd_change
            FROM staff
			WHERE sta_sid = :sid";
        $StaffRow = DatixDBQuery::PDO_fetch($sql, array('sid' => $ldap->user["sid"]));

        if (empty($StaffRow))
        {
            // Insert new staff record
            $LDAP_con_id = $ldap->CreateContactRecord();

            if (!$LDAP_con_id)
            {
                $ldap->Close();
                AuditLogin($User, 'Login failed: LDAP: cannot create staff record');
                if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                {
                    $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
                }
                else
                {
                    $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('ldap_auth_fail_err');
                }
                redirectexit();
            }

            $_SESSION["contact_login_id"] = $LDAP_con_id;
            $initials = $LDAP_con_id;

            // check for an existing staff record, if there isn't one create it
            $sql = "SELECT recordid, initials, fullname, lockout, permission, email, tel1,
    		login_tries, sta_clingroup, sta_orgcode, sta_directorate,
    		sta_specialty, sta_unit, login, sta_surname, sta_forenames, sta_pwd_change
            FROM staff
			WHERE sta_sid = :sid";
            $StaffRow = DatixDBQuery::PDO_fetch($sql, array('sid' => $ldap->user["sid"]));
        }

        if ($StaffRow['lockout'] == 'Y')
        {
            AuditLogin($User, 'Login failed: user is locked out');
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
            }
            else
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('lockout_err');
            }
            redirectexit();
        }

        $_SESSION["fullname"] = $StaffRow["fullname"];
        $_SESSION["initials"] = $StaffRow["initials"];
        $_SESSION["login"] = $User;
        $_SESSION["email"] = $StaffRow["email"];
        $_SESSION["tel"] =  $StaffRow["tel1"];
        $_SESSION["permission"] = $StaffRow["permission"];
        $_SESSION["surname"] =  $StaffRow["sta_surname"];
        $_SESSION["forenames"] = $StaffRow["sta_forenames"];

        $ModulePerms = ParsePermString($StaffRow["permission"]);
        $_SESSION["Perms"] = $ModulePerms;

        // Location codes from staff record
        if ($StaffRow["sta_orgcode"])
        {
            $_SESSION["sta_orgcode"] = $StaffRow["sta_orgcode"];
        }

        if ($StaffRow["sta_unit"])
        {
            $_SESSION["sta_unit"] = $StaffRow["sta_unit"];
        }

        if ($StaffRow["sta_directorate"])
        {
            $_SESSION["sta_directorate"] = $StaffRow["sta_directorate"];
        }

        if ($StaffRow["sta_specialty"])
        {
            $_SESSION["sta_specialty"] = $StaffRow["sta_specialty"];
        }

        if ($StaffRow["sta_clingroup"])
        {
            $_SESSION["sta_clingroup"] = $StaffRow["sta_clingroup"];
        }

        if (!$ldap->UpdateContactRecordBySID())
        {
            $ldap->Close();
            AuditLogin($User, 'Login failed: LDAP: cannot update staff record');
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
            }
            else
            {
                $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('update_staff_LDAP_err');
            }
            redirectexit();
        }

        $_SESSION["contact_login_id"] = $StaffRow["recordid"];
        $initials = $StaffRow["initials"];

        //Only alter security if system is set up to have security handled on the AD side.
        if (GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS')
        {
            //Clear out groups for user before re-linking LDAP mapped groups.
            $sql = 'DELETE FROM sec_staff_group WHERE con_id = :con_id AND grp_id IN (SELECT grp_id FROM sec_ldap_groups) AND con_id IN (SELECT recordid FROM staff WHERE sta_sid IS NOT NULL AND sta_sid != \'\')';
            DatixDBQuery::PDO_query($sql, array('con_id' => $_SESSION["contact_login_id"]));

            // Create the group links for the user
            if (!empty($ldap->usergroups))
            {
                $ValidGroupsList = implode(",", $ldap->usergroups);

                $error = false;

                foreach ($ldap->usergroups as $GroupID)
                {
                    DatixDBQuery::PDO_build_and_insert('sec_staff_group', array('grp_id' => $GroupID, 'con_id' => $_SESSION["contact_login_id"]));
                }
            }

            //Clear out profile for user before re-linking LDAP mapped profile.
            $sql = 'UPDATE contacts_main SET sta_profile = NULL WHERE recordid = :recordid AND sta_profile IN (SELECT pfl_id FROM sec_ldap_profiles) AND contacts_main.recordid IN (SELECT recordid FROM staff WHERE sta_sid IS NOT NULL AND sta_sid != \'\')';
            DatixDBQuery::PDO_query($sql, array('recordid' => $_SESSION["contact_login_id"]));

            // Create the profile link for the user (only use first profile)
            if (!empty($ldap->userprofiles[0]))
            {
                $sql = 'UPDATE contacts_main SET sta_profile = :profile WHERE recordid = :recordid';
                DatixDBQuery::PDO_query($sql, array('profile' => $ldap->userprofiles[0], 'recordid' => $_SESSION["contact_login_id"]));
            }
        }
        // No need to keep the LDAP connection open now.
        $ldap->Close();
	}

    if (!$LDAPAuthentication || $LDAPAuthentication && $ldap->DatixAuthentication())
    {
		// DATIX Authentication
        // user type can be WEB, MAIN, BOTH or empty.
		$sql = "SELECT initials, fullname, lockout, permission, email, tel1,
    		login_tries, sta_clingroup, sta_orgcode, sta_directorate,
    		sta_specialty, sta_unit, sta_forenames, sta_surname, sta_pwd_change,
            sta_last_login, sta_lockout_dt, sta_lockout_reason
    		FROM staff
    		WHERE login = :login AND (login is not null and login != '')
            AND (sta_user_type != 'MAIN' OR sta_user_type IS NULL)";

        // When LDAP authentication has previously failed, only allow
        // DATIX authentication if the user has not been created by LDAP,
        // i.e. if the user does not have a SID.
        // In addition, if no domain is provided, users with sids cannot be allowed to log in.
        if (($LDAPAuthentication && $ldap->DatixAuthentication) || empty($Domain))
        {
            $sql .= " AND (sta_sid IS NULL or sta_sid = '')";
        }

		$StaffRow = DatixDBQuery::PDO_fetch($sql, array('login' => $User));

        // User does not exist in the staff table
        if (!$StaffRow)
        {
            if(bYN(GetParm('WEB_NETWORK_LOGIN', 'N')))
            {
                fatal_error(_tk('wrong_user_pwd_err'));
            }
            else
            {
                AuditLogin($User, 'Login failed: incorrect user name');
                if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                {
                    $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
                }
                else
                {
                    $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('wrong_user_pwd_err');
                }
                redirectexit();
            }
    	}
    	else // Login from staff table
    	{
            // Check for group permissions and retrieve them if needed
            // If there are permissions they will be overriden by the user security settings
    		$staff = $StaffRow;

    		if ($staff['lockout'] == 'Y')
    		{
                // login_tries == -1 means that the user has not been
    			// fully registered yet.
    			if ($staff['login_tries'] == '-1')
    			{
    				AuditLogin($User, 'Login failed: unregistered user');
                    if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
                    }
                    else
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('wrong_user_pwd_err');
                    }
                    redirectexit();
    			}
    			else
    			{
    				AuditLogin($User, 'Login failed: user is locked out');
                    if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
                    }
                    else
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('lockout_err');
                    }
                    redirectexit();
    			}
    		}

    		$_SESSION["fullname"] = $staff["fullname"];
    		$_SESSION["initials"] = $staff["initials"];
    		$_SESSION["email"] = $staff["email"];
            $_SESSION["tel"] = $staff["tel1"];
            $_SESSION["login"] = $User;
            $_SESSION["forenames"] = $staff["sta_forenames"];
			$_SESSION["surname"] = $staff["sta_surname"];

            // Get location codes for user
			if ($staff["sta_orgcode"])
            {
                $_SESSION["sta_orgcode"] = $staff["sta_orgcode"];
            }

			if ($staff["sta_unit"])
            {
                $_SESSION["sta_unit"] = $staff["sta_unit"];
            }

			if ($staff["sta_directorate"])
            {
                $_SESSION["sta_directorate"] = $staff["sta_directorate"];
            }

			if ($staff["sta_specialty"])
            {
                $_SESSION["sta_specialty"] = $staff["sta_specialty"];
            }

			if ($staff["sta_clingroup"])
            {
                $_SESSION["sta_clingroup"] = $staff["sta_clingroup"];
            }

			$ModulePerms = ParsePermString($staff["permission"]);
			$_SESSION["Perms"] = $ModulePerms;

            // Set any linked contact ID for the staff record.
			$sql = 'SELECT recordid as con_id FROM staff WHERE staff.login = :login';

			$_SESSION["contact_login_id"] = DatixDBQuery::PDO_fetch($sql, array('login' => $_SESSION['login']), PDO::FETCH_COLUMN);
		}

        // If we get this far, the user exists.
        // If using SSO (WEB_NETWORK_LOGIN), we don't want to use any
        // p_a_s_s_w_o_r_d authentication.
        if (!$NetworkLogin)
        {
            $passwordMapper = (new \src\passwords\model\PasswordModelFactory())->getMapper();

            if (!$passwordMapper->userHasPassword($User))
            {
    		    AuditLogin($User, "User set password for first time");
                $yySetLocation = $scripturl . '?action=password&user=' . $User . '&fullname=' . $_SESSION['fullname'];
                redirectexit();
		    }

            if (!ValidatePassword($User, $passwrd))
            {
    		    $Sta_lockout_reason = "Login failed: incorrect password";
                AuditLogin($User,  $Sta_lockout_reason);
                // Update staff table with number of tries and lockout
                $staff["login_tries"] = $staff["login_tries"] + 1;

                if (!$_SESSION["Globals"]["LOGIN_TRY"])
                {
                    $_SESSION["Globals"]["LOGIN_TRY"] = LOGIN_TRY;
                }

                if ($staff["login_tries"] >= $_SESSION["Globals"]["LOGIN_TRY"])
                {
                    $Parameters = array(
                        'lockout' => 'Y',
                        'login_tries' => $staff['login_tries'],
                        'sta_lockout_dt' => date('Y-m-d H:i:s'),
                        'sta_lockout_reason' => $Sta_lockout_reason,
                        'login' => $User
                    );
                }
                else
                {
                    $Parameters = array(
                        'lockout' => 'N',
                        'login_tries' => $staff['login_tries'],
                        'sta_lockout_dt' => null,
                        'sta_lockout_reason' => '',
                        'login' => $User
                    );
                }

                $sql = 'UPDATE contacts_main SET
                        lockout = :lockout,
                        login_tries = :login_tries,
                        sta_lockout_dt = :sta_lockout_dt,
                        sta_lockout_reason =  :sta_lockout_reason
                        WHERE login = :login';

                DatixDBQuery::PDO_query($sql, $Parameters);

				if ($Parameters['lockout'] == 'Y')
				{
					AuditLogin($User, 'Login failed: user is locked out');
                    if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
                    }
                    else
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('lockout_err');
                    }
                    redirectexit();
				}
                else
                {
                    if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1&url='.urlencode($URL);
                    }
                    else
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('wrong_user_pwd_err'). '&attempts='.($_SESSION['Globals']['LOGIN_TRY'] - $staff['login_tries']).'&url='.urlencode($URL);
                    }
                    redirectexit();
				}

    		    obExit();
    	    }
            else
            {
                $MaxInactivityDays = GetParm('LOGIN_INACTIVITY_DAYS', '-1');
                $today = new DateTime();

                if ($staff["sta_last_login"] == '')
                {
                    $LastLogin = $today;
                }
                else
                {
                    $LastLogin = $staff["sta_last_login"];
                    $LastLogin = DateTime::createFromFormat('Y-m-d H:i:s.u', $LastLogin);
                }

                $ActualInactivityDays = $today->diff($LastLogin);
                // Get number of days
                $ActualInactivityDays =  $ActualInactivityDays->format('%a');

                if ($ActualInactivityDays > $MaxInactivityDays && $MaxInactivityDays != -1)
                {
                    $Sta_lockout_reason = 'Locked out due to inactivity of more than ' . $MaxInactivityDays . ' day(s)';

                    $sql = 'UPDATE contacts_main SET
                        lockout = :lockout,
                        login_tries = :login_tries,
                        sta_lockout_dt = :today,
                        sta_lockout_reason =  :lockout_reason
                        WHERE login = :user';

                    DatixDBQuery::PDO_query($sql, array('lockout' => 'Y', 'login_tries' => $staff['login_tries'], 'today' => $today->format('Y-m-d H:i:s.000'), 'lockout_reason' => $Sta_lockout_reason, 'user' => $User));

                    AuditLogin($User, $Sta_lockout_reason);
                    if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
                    }
                    else
                    {
                        $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('lockout_err');
                    }
                    redirectexit();
                }
                else
                {
                    // Successfuly authenticated user, update staff table to clear locks
			        $sql = "UPDATE contacts_main SET
                    lockout = :lockout,
                    login_tries = :login_tries,
                    sta_last_login = GETDATE(),
                    sta_lockout_reason = :lockout_reason
                    WHERE login = :user";

                    DatixDBQuery::PDO_query($sql, array('lockout' => 'N', 'login_tries' => 0, 'lockout_reason' => '', 'user' => $User));
                }
            }
        }
	} // End of DATIX authentication

    System_Globals::InitialiseSessionObject();
    $_SESSION["AdminUser"] = bYN(System_Globals::GetCurrentValue('FULL_ADMIN', ''));

	$accessLevels = GetUserAccessLevels($_SESSION["contact_login_id"]);

    $userFactory = new \src\users\model\UserModelFactory();
    $_SESSION['CurrentUser'] = $userFactory->getMapper()->findLoggedInUser($_SESSION["contact_login_id"]);

	if (is_array($accessLevels) && !empty($accessLevels))
    {
		$_SESSION["Globals"] = array_merge($_SESSION["Globals"], $accessLevels);
    }

	// Check access period for user
	// Only check p_a_s_s_w_o_r_d expiry for DATIXWeb logins not LDAP
    if (!$LDAPAuthentication && !$ldap->DatixAuthentication() && !checkAccessPeriod($User))
    {
		AuditLogin($User, 'Login failed: no user access for period');
        if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
        {
            $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&login_failed=1';
        }
        else
        {
            $yySetLocation = $scripturl . '?action=login&user=' . $user_no_domain . '&error=' . _tk('account_inactive_err');
        }
        redirectexit();
    }

	$_SESSION["user"] = $User;
	$_SESSION["scripturl"] = $scripturl;
	$_SESSION['form_id'][$_POST['module']][1] = Sanitize::SanitizeInt($_POST["form_id"]);

	// Check access period for user
	// Only check p_a_s_s_w_o_r_d expiry for DATIXWeb logins not LDAP
    if (!$LDAPAuthentication && !$ldap->DatixAuthentication() && !$NetworkLogin
        && (!checkPasswordExpiry($User) || $staff["sta_pwd_change"] == "Y"))
    {
		AuditLogin($User,  "Password expired");
        $yySetLocation = $scripturl . '?action=password&user=' . $User . '&fullname=' . $_SESSION['fullname'] . '&PwdAction=Expired&error=' . _tk('password_expired_enter_new');
        redirectexit();
    }

	$_SESSION["logged_in"] = true;
	$_SESSION["dbname"] = $dbname;

    if(bYN(GetParm("RECORD_LOCKING","N")))
    {
        DatixDBQuery::CallStoredProcedure(array(
            "procedure" => "NewSession",
            "parameters" => array(
                array("@application", "WEB", SQLVARCHAR),
                array("@userid", $_SESSION["contact_login_id"], SQLINT4),
                array("@session_id", &$_SESSION["session_id"], SQLINT4, true),
                array("@hostname", getenv("REMOTE_ADDR"), SQLVARCHAR)
            )
        ));
    }

	AuditLogin($User, "User logged in");
	// Set this so that template_header knows to display the login message
    $_SESSION["first_login"] = true;

    //Prevent session hijacking if the user's session id was grabbed while logged out.
    session_regenerate_id();

    GetParms("$User");

	// Users in contacts table can never be admin users (set earlier).
    // and should only be given DIF1 access
	if ($_SESSION["contact_login"])
    {
        $_SESSION["Globals"]["DIF_PERMS"] = "DIF1";
    }

    // Remove token from URL
    if (UnicodeString::stripos($URL, 'token') !== false)
    {
        $urlArray = explode('&', $URL);

        foreach ($urlArray as $key => $values)
        {
            // We need to remove also user and login_failed parameters from URL because if we are here it's because the login is successful
            if (UnicodeString::stripos($values, 'token') !== false ||
                UnicodeString::stripos($values, 'user') !== false ||
                UnicodeString::stripos($values, 'login_failed') !== false)
            {
                unset($urlArray[$key]);
            }
        }

        $URL = implode('&', $urlArray);
    }

    if ($URL != 'action=login' && $URL != 'action=login&adminlogin=y' && $URL != 'action=login2'
        && \UnicodeString::strpos($URL, "logout") === false && \UnicodeString::strpos($URL, "form_id") === false)
    {
        $yySetLocation = $scripturl . '?' . $URL;
    }
    else
    {
        $yySetLocation = $scripturl;
    }

    // if there are agreement messages set up then users will
    // have to agree to these before they are logged in
    if (bYN(GetParm('LOGIN_USAGE_AGREEMENT', 'N')))
    {
        DisplayAgreementMessages();
    }

	redirectexit();
}

function CheckSSOAccountStatus($LDAPAuthentication = false)
{
    global $UserSSOSettingsFile;

    $NetworkLogin = bYN(GetParm('WEB_NETWORK_LOGIN', 'N'));

    if ($NetworkLogin && $UserSSOSettingsFile)
    {
        require $UserSSOSettingsFile;
    }

    //need to check whether this user has already registered.
    if (!SSOConfigError())
    {
        if(!$LDAPAuthentication) //LDAP might mean that there is no user in the DB - so no point in checking here.
        {
            $sql = 'SELECT recordid, lockout, login_tries FROM contacts_main WHERE login = \''.$UserArray['UserID'].'\'';

            $user = DatixDBQuery::PDO_fetch($sql);

            if (!empty($user))
            {
                if ($user['login_tries'] == -1 && $user['lockout'] = 'Y')
                {
                    //Account is 'Pending';
                    $NoRegistrationForm = true;
                    $Message = _tk('reg_pending_message');
                }
                else
                {
                    $today = GetTodaysDate();

                    $sql = 'SELECT recordid, lockout, login_tries FROM contacts_main
                        WHERE login = :login
                        AND (sta_daccessstart > :today_1 OR sta_daccessend < :today_2)';

                    $result = DatixDBQuery::PDO_fetch($sql, array('today_1' => $today, 'today_2' => $today, 'login' => $UserArray['UserID']));

                    if (!empty($result))
                    {
                        //Account is Deactivated
                        $NoRegistrationForm = true;
                        $Message = _tk('reg_deactivated_message');
                    }
                    else
                    {
                        if ($user['lockout'] == 'Y')
                        {
                            //Account is Inactive
                            $NoRegistrationForm = true;
                            $Message = _tk('reg_inactive_message');
                        }
                        else
                        {
                            //Account is Active
                            $NoRegistrationForm = true;
                            $AccountActive = true;
                            $Message = _tk('reg_active_message');
                        }
                    }
                }
            }
            else
            {
                $Message = _tk('please_register_message');
            }
        }
        else
        {
            $AccountActive = true;
            $NoRegistrationForm = true;
        }
    }
    else
    {
        $Message = _tk('sso_setup_error');
        $NoRegistrationForm = true;
    }

    return array('message' => $Message, 'no_reg_form' => $NoRegistrationForm, 'account_active' => $AccountActive);
}

function checkPasswordExpiry($username)
{
    if (GetParm("PWD_SHARED", "N") == "Y")
    {
        $sql = 'SELECT pwd_dexpires
		FROM passwords
		WHERE (pwd_login =:pwd_login_web OR pwd_login = :pwd_login)
		AND pwd_current = :pwd_current';

        $expiryDate = DatixDBQuery::PDO_fetch($sql, array('pwd_login_web' => '_'.$username, 'pwd_login' => $username, 'pwd_current' => 'Y'), PDO::FETCH_COLUMN);
    }
    else
    {
        $sql = 'SELECT pwd_dexpires
		FROM passwords
		WHERE pwd_login = :pwd_login_web
		AND pwd_current = :pwd_current';

        $expiryDate = DatixDBQuery::PDO_fetch($sql, array('pwd_login_web' => '_'.$username, 'pwd_current' => 'Y'), PDO::FETCH_COLUMN);
    }

    $today = date('Y-m-d H:i:s.000');

    if (strtotime($expiryDate) < strtotime($today))
    {
        return false;
    }

    return true;
}

function ActivateLogin()
{
    global $dtxtitle, $scripturl, $action, $color, $yySetLocation;

    $ActivateCode = Sanitize::SanitizeString($_GET["code"]);

    if ($_SESSION["logged_in"] != true)
    {
        $sql = "UPDATE staff
					SET login_tries = :login_tries,
					lockout = :lockout,
                    sta_activate_code = :sta_activate_code
					WHERE sta_activate_code = :sta_activate_code_2";

        $result = DatixDBQuery::PDO_query($sql, array('login_tries' => 0, 'lockout' => 'N', 'sta_activate_code' => null, 'sta_activate_code_2' => $ActivateCode));

	    if ($result->rowCount() != 0)
	    {
            $message = _tk('registration_activated');
            $yySetLocation = $scripturl . '?action=login&user=' . $user;
            redirectexit();
	    }
        else
        {
            success_message(_tk('invalid_activation_err'));
        }
    }
    else
    {
        $yySetLocation = "$scripturl";
		redirectexit();
    }
}

function DisplayAgreementMessages()
{
    global $yySetLocation, $MinifierDisabled;

    $agreementMessages = GetUsageAgreementMessages();

    if (empty($agreementMessages))
    {
        return;
    }

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $_SESSION["first_login"] = '';  // prevent login messages being fired first
    template_header();

    echo '
    <script language="javascript" type="text/javascript">
        var redirectLocation = \''.$yySetLocation.'\';
    </script>
    <script language="javascript" type="text/javascript" src="js_functions/agreementMessages' . $addMinExtension . '.js"></script>
    <script language="javascript" type="text/javascript">

        var agreementMessages = new Array();';

    $count = count($agreementMessages);

    foreach ($agreementMessages as $message)
    {
        $count--;
        $message = htmlspecialchars($message);
        $message = str_replace("'", "\'", $message);
        $message = str_replace("\n", "<br />", $message);
        $message = str_replace("\r", "", $message);
        echo "
        agreementMessages[$count] = '$message';";
    }

    $disagreementMessage = htmlspecialchars(GetParm('DISAGREEMENT_MSG', ''));
    $disagreementMessage = str_replace("'", "\'", $disagreementMessage);
    $disagreementMessage = str_replace("\n", "<br />", $disagreementMessage);
    $disagreementMessage = str_replace("\r", "", $disagreementMessage);

    echo '
        var disagreementMessage = \'' . $disagreementMessage . '\'';

    echo '
    </script>';

    $_SESSION["first_login"] = true;
    footer();
    obExit();
}

function ConstructLogin($aParams)
{
    if (!$aParams['data'])
    {
        $aParams['data'] = DatixDBQuery::PDO_fetch_all('SELECT con_forenames, con_surname, login FROM contacts_main WHERE recordid = :recordid', array('recordid' => $aParams['recordid']));
    }

    $Login = $aParams['data']['login'];

    if(!$Login)
    {
        if (!$aParams['data']['con_surname'])
        {
            $Names = explode(' ', $aParams['data']['con_forenames']);
            $Login = \UnicodeString::strtolower(StripNonAlphabet($Names[0]));
        }
        else
        {
            $Login = \UnicodeString::strtolower(\UnicodeString::substr(StripNonAlphabet($aParams['data']['con_forenames']),0,1).StripNonAlphabet($aParams['data']['con_surname']));
        }
    }

    $Login = \UnicodeString::substr($Login, 0, 251); // make sure the login is not too big for the db (however unlikely this seems

    $UnusedLogin = false;
    $LoginSuffix = 1;
    $FinalLogin = $Login;

    while ($FinalLogin == '' || !$UnusedLogin)
    {
        if (CheckUniqueLogin($FinalLogin))
        {
            $UnusedLogin = true;
        }
        else
        {
            $FinalLogin = $Login.$LoginSuffix;
            $LoginSuffix++;
        }
    }

    return $FinalLogin;
}

function CheckUniqueLogin($Login)
{
    if (!$Login)
    {
        return false;
    }

    $count = DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM contacts_main WHERE login = :login', array('login' => $Login), PDO::FETCH_COLUMN);

    return ($count == 0);
}

/**
* @desc When a user is logged out and cannot see any level 1 forms, they would normally see the login screen. With SSO on, they cannot see this
* page, and so they can never exist in a "logged out" state. This page will act as a "logged out" home page for these users.
*/
function LoggedOutSSOPage()
{
    AddSessionMessage('INFO', _tk('logged_out_sso'));

    template_header();

    echo GetPageTopDivs(array(
        'pagetitle' => array(
            'title' => 'Logged Out',
        )
    ));

    echo '</div></div></div></div>';

    footer();
}
