<?php

$IsAdminUser = $_SESSION["AdminUser"];

$FieldDefs["INC"] = array(
	"inc_directorate" => array(
        "Type" => "ff_select",
		"Title" => "Directorate",
		"OldCodes" => true
    ),
	"inc_specialty" => array(
        "Type" => "ff_select",
		"Title" => "Specialty",
		"OldCodes" => true
    ),
	"inc_unit" => array(
        "Type" => "ff_select",
		"Title" => "Unit",
		"OldCodes" => true
    ),
	"inc_loctype" => array(
        "Type" => "ff_select",
		"Title" => "Location (type)",
		"OldCodes" => true
    ),
	"inc_locactual" => array(
        "Type" => "ff_select",
		"Title" => "Location (exact)",
		"OldCodes" => true
    ),
	"inc_clingroup" => array(
        "Type" => "ff_select",
		"Title" => "Clinical group/Locality"
    ),
	"inc_organisation" => array(
        "Type" => "ff_select",
		"Title" => "Trust"
    ),
	"inc_dincident" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Incident date",
    ),
    "inc_time" => array(
        "Type" => "time",
        "Title" => "Time",
    ),
	"inc_time_band" => array(
        "Type" => "ff_select",
		"Title" => "Time Band",
    ),
	"inc_notes" => array(
        "Type" => "textarea",
		"Title" => "Description of incident",
		"Rows" => 10,
		"Columns" => 70
    ),
    "inc_actiontaken" => array(
        "Type" => "textarea",
		"Title" => "Immediate action taken",
		"Rows" => 7,
		"Columns" => 70
    ),
	"inc_inv_outcome" => array(
        "Type" => "ff_select",
		"Title" => "Outcome of investigation"
    ),
	"inc_inv_dstart" => array(
        "Type" => "date",
        'NotFuture' => true,
        'NotEarlierThan' => array('inc_dincident'),
		"Title" => "Date investigation started"
    ),
	"inc_inv_dcomp" => array(
        "Type" => "date",
        'NotFuture' => true,
        'NotEarlierThan' => array('inc_inv_dstart'),
		"Title" => "Date investigation completed"
    ),
	"inc_inv_lessons" => array(
        "Type" => "textarea",
		"Title" => "Lessons learned",
		"Rows" => 7,
		"Columns" => 70
    ),
	"inc_inv_action" => array(
        "Type" => "textarea",
		"Title" => "Action taken",
		"Rows" => 7,
		"Columns" => 70
    ),
	"inc_action_code" => array(
        "Type" => "multilistbox",
		"Title" => "Action taken codes",
 		"MaxLength" => 128,
        "NoListCol" => true
    ),
    "inc_lessons_code" => array(
        "Type" => "multilistbox",
		"Title" => "Lessons learned codes",
    	"MaxLength" => 128,
        "NoListCol" => true
    ),
    "inc_root_causes" => array(
        "Type" => "multilistbox",
        "Title" => "Causes",
    	"MaxLength" => 248,
        "NoListCol" => true
    ),
	"inc_name" => array(
        "Type" => "string",
		"Title" => "Name",
		"Width" => 30,
		"MaxLength" => 128,
        'UpperCase' => true
    ),
    "inc_mgr" => array(
        "Type" => "ff_select",
        "Title" => "Handler"
    ),
	"inc_head" => array(
        "Type" => "ff_select",
		"Title" => "Manager"
    ),
	"inc_type" => array(
        "Type" => "ff_select",
		"Title" => "Type",
		"Child" => "inc_category",
		"OldCodes" => true
    ),
	"inc_category" => array(
        "Type" => "ff_select",
		"Title" => "Category",
		"OldCodes" => true
    ),
	"inc_subcategory" => array(
        "Type" => "ff_select",
		"Title" => "Subcategory",
		"OldCodes" => true
    ),
	"inc_carestage" => array(
        "Type" => "ff_select",
		"Title" => "Stage of care",
		"Child" => "inc_clin_detail",
		"Child2" => "inc_result",
		"DropdownWidth" => -1,
		"OldCodes" => true
    ),
	"inc_clin_detail" => array(
        "Type" => "ff_select",
		"Title" => "Detail",
		"Parent" => "inc_carestage",
		"Child" => "inc_clintype",
		"DropdownWidth" => -1
    ),
	"inc_clintype" => array(
        "Type" => "ff_select",
		"Title" => "Adverse event",
		"Parent" => "inc_clin_detail",
		"DropdownWidth" => -1
    ),
	"inc_inquiry" => array(
        "Type" => "ff_select",
		"Title" => "Further inquiry?",
		"DropdownWidth" => -1
    ),
	"inc_result" => array(
        "Type" => "ff_select",
		"Title" => "Result",
		"Parent" => "inc_carestage",
		"DropdownWidth" => -1,
		"OldCodes" => true
    ),
	"inc_severity" => array(
        "Type" => "ff_select",
		"Title" => "Severity",
		"OldCodes" => true
    ),
	"inc_eqpt_type" => array(
        "Type" => "ff_select",
		"Title" => "Product type"
    ),
	"inc_equipment" => array(
        "Type" => "string",
		"Title" => "Brand name",
		"Width" => 30,
		"MaxLength" => 254
    ),
	"inc_manufacturer" => array(
        "Type" => "string",
		"Title" => "Manufacturer",
		"Width" => 30,
		"MaxLength" => 254
    ),
	"inc_serialno" => array(
        "Type" => "string",
		"Title" => "Serial no.",
		"Width" => 30,
		"MaxLength" => 64
    ),
    "inc_dmanu" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Date of manufacture"
    ),
    "inc_lastservice" => array(
        "Type" => "date",
        'NotFuture' => true,
        'NotEarlierThan' => array('inc_dmanu'),
		"Title" => "Last serviced"
    ),
    "inc_dputinuse" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Date put in use"
    ),
    "inc_batchno" => array(
        "Type" => "string",
		"Title" => "Batch/lot no.",
		"Width" => 30,
		"MaxLength" => 64
    ),
    "inc_cemarking" => array(
        "Type" => "yesno",
		"Title" => "CE marking"
    ),
    "inc_outcomecode" => array(
        "Type" => "ff_select",
		"Title" => "Outcome code",
        "OldCodes" => true
    ),
    "inc_description" => array(
        "Type" => "textarea",
		"Title" => "Description of device",
        "Rows" => 3,
		"Columns" => 50
    ),
    "inc_supplier" => array(
        "Type" => "string",
		"Title" => "Supplier",
		"Width" => 30,
		"MaxLength" => 254
    ),
    "inc_servrecords" => array(
        "Type" => "string",
		"Title" => "Service records held by",
		"Width" => 30,
		"MaxLength" => 50
    ),
    "inc_model" => array(
        "Type" => "string",
		"Title" => "Model/size",
		"Width" => 30,
		"MaxLength" => 64
    ),
    "inc_location" => array(
        "Type" => "string",
		"Title" => "Current location",
		"Width" => 30,
		"MaxLength" => 64
    ),
    "inc_qdef" => array(
        "Type" => "number",
		"Title" => "Quantity defective",
		"Width" => 7,
        'MaxLength' => 7
    ),
    "inc_dmda" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Reported to MHRA"
    ),
    "inc_cost" => array(
        "Type" => "money",
		"Title" => "Cost",
		"Width" => 15
    ),
    "inc_defect" => array(
        "Type" => "textarea",
		"Title" => "Description of defect",
		"Rows" => 3,
		"Columns" => 50
    ),
    "inc_outcome" => array(
        "Type" => "textarea",
		"Title" => "Outcome of MHRA investigation",
        "Rows" => 3,
		"Columns" => 50
    ),
    "show_document" => array(
        "Type" => "checkbox",
        "Title" => "Are there any documents to be attached to this record?",
        "NoListCol" => true
    ),
    "show_person" => array(
        "Type" => "checkbox",
        "Title" => "Was any person involved in the incident?",
        "NoListCol" => true
    ),
    "show_employee" => array(
        "Type" => "checkbox",
        "Title" => "Was any employee involved in the incident?",
        "NoListCol" => true
    ),
    "show_witness" => array(
        "Type" => "checkbox",
        "Title" => "Were there any witnesses to the incident?",
        "NoListCol" => true
    ),
    "show_other_contacts" => array(
        "Type" => "checkbox",
        "Title" => "Was any other contact involved in the incident?",
        "NoListCol" => true
    ),
	"show_equipment" => array(
        "Type" => "checkbox",
		"Title" => "Was any equipment involved in the incident?",
        "NoListCol" => true
    ),
    "show_medication" => array(
        "Type" => "checkbox",
        "Title" => "Was this a medication incident?",
        "NoListCol" => true
    ),
	"inc_repname" => array(
        "Type" => "string",
		"Title" => "Full name",
		"Width" => 32,
		"MaxLength" => 64,
        "NoListCol" => true
    ),
	"inc_reportedby" => array(
        "Type" => "ff_select",
		"Title" => "Job role/grade"
    ),
	"inc_med_stage" => array(
        "Type" => "ff_select",
		"DropdownWidth" => -1,
		"Title" => "Stage of medication error"
    ),
	"inc_med_error" => array(
        "Type" => "ff_select",
		"DropdownWidth" => -1,
		"Title" => "Medication error"
    ),
	"inc_med_drug" => array(
        "Type" => "string_search",
		"Title" => "Drug administered",
		"Width" => 40,
		"MaxLength" => 64,
        "requireMinChars" => true
    ),
	"inc_med_drug_rt" => array(
        "Type" => "string_search",
		"Title" => "Correct drug",
		"Width" => 40,
		"MaxLength" => 64,
        "requireMinChars" => true
    ),
	"inc_med_form" => array(
        "Type" => "ff_select",
		"DropdownWidth" => -1,
		"Title" => "Form administered"
    ),
	"inc_med_form_rt" => array(
        "Type" => "ff_select",
		"DropdownWidth" => -1,
		"Title" => "Correct form"
    ),
	"inc_med_route" => array(
        "Type" => "ff_select",
		"DropdownWidth" => -1,
		"Title" => "Route administered"
    ),
	"inc_med_route_rt" => array(
        "Type" => "ff_select",
		"DropdownWidth" => -1,
		"Title" => "Correct route"
    ),
	"inc_med_dose" => array(
        "Type" => "string",
		"Title" => "Dose and strength administered",
		"Width" => 20,
		"MaxLength" => 64
    ),
	"inc_med_dose_rt" => array(
        "Type" => "string",
		"Title" => "Correct dose and strength",
		"Width" => 20,
		"MaxLength" => 64
    ),
	"inc_is_riddor" => array(
        "Type" => "ff_select",
        "Title" => "RIDDOR?"
    ),
	"inc_dsched" => array(
        "Type" => "date",
        'NotFuture' => true,
        'NotEarlierThan' => array('inc_dopened'),
		"Title" => "Closed date"
    ),
	"inc_dopened" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Opened date",
        "ReadOnly" => true
    ),
	"inc_submittedtime" => array(
        "Type" => "time",
		"Title" => "Submitted time",
        "ReadOnly" => true
    ),
    "inc_dreported" => array(
        "Type" => "date",
        'SetOnce' => true,
        'NotFuture' => true,
        'NotEarlierThan' => array('inc_dincident'),
		"Title" => "Reported date"
    ),
    "dum_inc_grading" => array(
        "Type" => "function",
        "Title" => "Risk grading",
        "Include" => 'Source/libs/FormClasses.php',
        "Function" => "MakeRatingFields",
        "Parameters" => array(
            array("V","Data"),
            array("V","FormType"),
            array("S","grading"),
            array("V","Module")
        ),
        "NoListCol" => true,
        "MandatoryField" => "inc_grade"
    ),
    "dum_inc_grading_initial" => array(
        "Type" => "function",
        "Title" => "Initial risk grading",
        "Include" => 'Source/libs/FormClasses.php',
        "Function" => "MakeRatingFields",
        "Parameters" => array(
            array("V","Data"),
            array("V","FormType"),
            array("S","initial"),
            array("V","Module")
        ),
        "NoListCol" => true,
        "MandatoryField" => "inc_grade_initial"
    ),
    "inc_ourref" => array(
        "Type" => "string",
		"Title" => "Reference",
		"Width" => 50,
		"MaxLength" => 32
    ),
	"inc_cnstitype" => array(
        "Type" => "multilistbox",
		"Title" => "Codes",
		"MaxLength" => 254
    ),
    "inc_dnotified" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "RIDDOR Notified"
    ),
    "inc_ridloc" => array(
        "Type" => "ff_select",
        "Title" => "Location"
    ),
    "inc_localauth" => array(
        "Type" => "string",
		"Title" => "Local authority",
		"Width" => 64,
		"MaxLength" => 64
    ),
    "inc_acctype" => array(
        "Type" => "ff_select",
		"Title" => "Accident type"
    ),
    "inc_riddor_ref" => array(
        "Type" => "string",
        "Title" => "RIDDOR Ref",
        "Width" => 64,
        "MaxLength" => 64
    ),
    "inc_riddorno" => array(
        "Type" => "ff_select",
		"Title" => "Disease/occurrence no."
    ),
    "inc_further_inv" => array(
        "Type" => "ff_select",
        "Title" => "Further investigation?",
        "DropdownWidth" => -1
    ),
    "inc_rc_required" => array(
        "Type" => "ff_select",
        "Title" => "Cause analysis required?",
        "DropdownWidth" => -1
    ),
    "inc_report_npsa" => array(
        "Type" => "ff_select",
        "DropdownWidth" => -1,
        "Title" => "Report to NRLS?"
    ),
    "inc_n_effect" => array(
        "Type" => "ff_select",
        "DropdownWidth" => -1,
        "Title" => "Effect on patient"
    ),
    "inc_n_patharm" => array(
        "Type" => "ff_select",
        "DropdownWidth" => -1,
        "Title" => "Patient harmed?"
    ),
    "inc_n_nearmiss" => array(
        "Type" => "ff_select",
        "DropdownWidth" => -1,
        "Title" => "Near miss?"
    ),
    "inc_n_actmin" => array(
        "Type" => "ff_select",
        "DropdownWidth" => -1,
        "Title" => "Did any actions minimise the impact of the incident?"
    ),
    "inc_n_paedward" => array(
        "Type" => "ff_select",
        "DropdownWidth" => -1,
        "Title" => "Dedicated paediatric ward/unit?"
    ),
    "inc_n_paedspec" => array(
        "Type" => "ff_select",
        "DropdownWidth" => -1,
        "Title" => "Paediatric specialty?"
    ),
    "inc_dnpsa" => array(
        "Type" => "date",
		"Title" => "Date exported",
        "ReadOnly" => true
    ),
    "recordid" => array(
        "Type" => "number",
		"Title" => "ID",
        "ReadOnly" => true,
		"Width" => 5
    ),
    "inc_rep_tel" => array(
        "Type" => "string",
        "Title" => "Telephone no.",
 		"Width" => 15,
 		"MaxLength" => 254
    ),
    "inc_rep_email" => array(
        "Type" => "email",
        "Title" => "Your e-mail address",
        "Width" => 40,
 		"MaxLength" => 120
    ),
    "fbk_body" => array(
        "Type" => "textarea",
		"Title" => "Feedback to reporter",
        "Rows" => 10,
		"Columns" => 70,
        "NoListCol" => true
    ),
    "fbk_dsent" => array(
        "Type" => "date",
		"Title" => "Date feedback sent to reporter",
        "NoListCol" => true
    ),
    "fbk_email" => array(
        "Type" => "email",
        "Title" => "Send feedback to e-mail address",
        "NoListCol" => true
    ),
    "fbk_subject" => array(
        "Type" => "string",
        "Title" => "Subject",
        "NoListCol" => true
    ),
    "inc_agg_issues" => array(
        "Type" => "multilistbox",
        "Title" => "Aggravating factors",
    	"MaxLength" => 248
    ),
    "inc_user_action" => array(
        "Type" => "ff_select",
        "Title" => "Immediate action taken?"
    ),
    "inc_pol_crime_no" => array(
        "Type" => "string",
        "Title" => "Police crime reference number",
        "Width" => 40,
 		"MaxLength" => 64
    ),
    "inc_pol_called" => array(
        "Type" => "yesno",
        "Title" => "Police called?"
    ),
    "inc_pol_call_time" => array(
        "Type" => "time",
		"Title" => "Time police called"
    ),
    "inc_pol_attend" => array(
        "Type" => "yesno",
        "Title" => "Police attended?"
    ),
    "inc_pol_att_time" => array(
        "Type" => "time",
		"Title" => "Time police attended"
    ),
    "inc_pol_action" => array(
        "Type" => "multilistbox",
        "Title" => "Action taken by police",
    	"MaxLength" => 248
    ),
    "inc_n_clindir" => array(
        "Type" => "ff_select",
        "Title" => "Clinical directorate",
        "OldCodes" => true
    ),
    "inc_n_clinspec" => array(
        "Type" => "ff_select",
        "Title" => "Clinical specialty",
        "OldCodes" => true
    ),
    "notes" => array(
        "Type" => "textarea",
		"Title" => "Notes",
        "Rows" => 10,
		"Columns" => 70,
        "NoListCol" => true
    ),
    "inc_pars_clinical" => array(
        "Type" => "yesno",
        "Title" => "Were there clinical factors?"
    ),
    "inc_address" => array(
        "Type" => "textarea",
        "Title" => "Address",
		"Rows" => 5,
		"Columns" => 50,
		"MaxLength" => 254,
        "NoSpellcheck" => true
    ),
    "inc_pars_address" => array(
        "Type" => "textarea",
        "Title" => "Incident address",
        "Rows" => 5,
        "Columns" => 50,
        "MaxLength" => 254,
        "NoSpellcheck" => true
    ),
    "inc_pars_pri_type" => array(
        "Type" => "multilistbox",
        "Title" => "What type of incident was the perpetrator involved in?",
    	"MaxLength" => 254
    ),
    "inc_pars_sec_type" => array(
        "Type" => "multilistbox",
        "Title" => "Non-physical assault sub-categories",
    	"MaxLength" => 254
    ),
    "inc_tprop_damaged" => array(
        "Type" => "yesno",
        "Title" => "Was trust property damaged/stolen?"
    ),
    /* link fields for equipment */
    "link_damage_type" => array(
        "Type" => "ff_select",
        "Title" => "Type of damage or loss",
        "NoListCol" => true
    ),
    "link_other_damage_info" => array(
        "Type" => "textarea",
        "Title" => "If other please describe",
        "Rows" => 5,
        "Columns" => 50,
        "NoListCol" => true
    ),
    "link_replaced" => array(
        "Type" => "yesno",
        "Title" => "Were item(s) replaced?",
        "NoListCol" => true
    ),
    "link_replace_cost" => array(
        "Type" => "money",
        "Title" => "Cost of replacement",
        "NoListCol" => true,
        "MaxLength" => 11
    ),
    "link_repaired" => array(
        "Type" => "yesno",
        "Title" => "Were item(s) repaired?",
        "NoListCol" => true
    ),
    "link_repair_cost" => array(
        "Type" => "money",
        "Title" => "Cost of repair",
        "NoListCol" => true,
        "MaxLength" => 11
    ),
    "link_written_off" => array(
        "Type" => "yesno",
        "Title" => "Were item(s) written off?",
        "NoListCol" => true
    ),
    "link_disposal_cost" => array(
        "Type" => "money",
        "Title" => "Cost of disposal",
        "NoListCol" => true,
        "MaxLength" => 11
    ),
    "link_sold" => array(
        "Type" => "yesno",
        "Title" => "Were item(s) sold?",
        "NoListCol" => true
    ),
    "link_sold_price" => array(
        "Type" => "money",
        "Title" => "Price sold for",
        "NoListCol" => true,
        "MaxLength" => 11
    ),
    "link_decommissioned" => array(
        "Type" => "yesno",
        "Title" => "Were item(s) decommissioned?",
        "NoListCol" => true
    ),
    "link_decommission_cost" => array(
        "Type" => "money",
        "Title" => "Cost of decommission",
        "NoListCol" => true,
        "MaxLength" => 11
    ),
    "link_residual_value" => array(
        "Type" => "money",
        "Title" => "Residual value of item(s) at time of loss or damage",
        "NoListCol" => true,
        "MaxLength" => 11
    ),
    "inc_pars_first_dexport" => array(
        "Type" => "date",
        "Title" => "SIRS original export date",
        "ReadOnly" => true,
        'NoDefault' => true
    ),
    "inc_pars_dexport" => array(
        "Type" => "date",
        "Title" => "SIRS updated export date",
        "ReadOnly" => true,
        'NoDefault' => true
    ),
    "inc_postcode" => array(
        "Type" => "string",
        "Title" => "Postcode",
        "Width" => 10,
        'MaxLength' => 10
    ),
    "inc_investigator" => array(
        "Type" => "multilistbox",
        "Title" => "Investigator",
    	"MaxLength" => 254
    ),
    "inc_notify" => array(
        "Type" => "multilistbox",
        "Title" => "Notify",
    	"MaxLength" => 254
    ),
    "inc_injury" => array(
        "Type" => "ff_select",
        "Title" => "Loss/damage"
    ),
    "inc_bodypart" => array(
        "Type" => "ff_select",
        "Title" => "Item",
        "NoListCol" => true
    ),
    "link_type" => array(
        "Type" => "array_select",
        "Title" => "Type of contact",
        "NoListCol" => true,
        "NoListCol_Override" => true,
        "SelectArray" => array(
            "A" => "Person affected",
            "E" => "Member of staff/employee",
            "N" => "Other type of contact"
        )
    ),
    "link_notes" => array(
        "Type" => "textarea",
        "Title" => "Link Notes",
        "NoListCol" => true,
        "Rows" => 5,
        "Columns" => 70
    ),
    "link_status" => array(
        "Type" => "ff_select",
        "Title" => "Status",
        "Parent" => "con_type",
        "NoListCol" => true
    ),
    "link_treatment" => array(
        "Type" => "ff_select",
        "Title" => "Treatment",
        "NoListCol" => true
    ),
    "link_daysaway" => array(
        "Type" => "number",
        "Title" => "No of days",
        "Width" => 3,
        "NoListCol" => true
    ),
    "link_abs_start" => array(
        "Type" => "date",
        "Title" => "Absence start",
        "NoListCol" => true,
        'onChangeExtra' => 'getAbsenceDays(\''.GetParm("FMT_DATE_WEB").'\')'
    ),
    "link_abs_end" => array(
        "Type" => "date",
        "Title" => "Absence end",
        'NotEarlierThan' => array('link_abs_start'),
        "NoListCol" => true,
        'onChangeExtra' => 'getAbsenceDays(\''.GetParm("FMT_DATE_WEB").'\')'
    ),
    "link_is_riddor" => array(
        "Type" => "yesno",
        "Title" => "RIDDOR?",
        "NoListCol" => true
    ),
    "link_riddor" => array(
        "Type" => "ff_select",
        "Title" => "RIDDOR injury type",
        "NoListCol" => true
    ),
    "link_occupation" => array(
        "Type" => "string",
        "Title" => "Job title/employer",
        "Width" => 30,
        "MaxLength" => 64,
        "NoListCol" => true
    ),
    "link_age" => array(
        "Type" => "number",
        "Title" => "Age",
        "Width" => 3,
        "NoListCol" => true
    ),
    "link_age_band" => array(
        "Type" => "ff_select",
        "Title" => "Age Band",
        "NoListCol" => true
    ),
    "link_deceased" => array(
        "Type" => "yesno",
        "Title" => "Deceased?",
        "NoListCol" => true
    ),
    "link_npsa_role" => array(
        "Type" => "ff_select",
        "Title" => "Staff role (NPSA)",
        "NoListCol" => true,
        "NoListCol_Override" => true
    ),
    "link_role" => array(
        "Type" => "ff_select",
        "Title" => "Contact role",
        "NoListCol" => true
    ),
    "link_mhact_section"  => array(
        "Type" => "ff_select",
        "Title" => "MHA Section",
        "NoListCol" => true
    ),
    "link_mhcpa" => array(
        "Type" => "ff_select",
        "Title" => "CPA",
        "NoListCol" => true
    ),
    "link_dear" => array(
        "Type" => "string",
        "Title" => "Dear",
        "Width" => 30,
        "MaxLength" => 64,
        "NoListCol" => true
    ),
    "link_ref" => array(
        "Type" => "string",
        "Title" => "Your ref",
        "Width" => 30,
        "MaxLength" => 64,
        "NoListCol" => true
    ),
    "link_tocnst" => array(
        "Type" => "yesno",
        "Title" => "Report to CNST?",
        "NoListCol" => true
    ),
    "link_lip" => array(
        "Type" => "yesno",
        "Title" => "Litigant in person?",
        "NoListCol" => true
    ),
    "link_worked_alone" => array(
        "Type" => "yesno",
        "Title" => "Was the staff member lone working?",
        "NoListCol" => true
    ),
    "link_become_unconscious" => array(
        "Type" => "yesno",
        "Title" => "Did the individual become unconscious?",
        "NoListCol" => true
    ),
    "link_req_resuscitation" => array(
        "Type" => "yesno",
        "Title" => "Did the individual need resuscitation?",
        "NoListCol" => true
    ),
    "link_hospital_24hours" => array(
        "Type" => "yesno",
        "Title" => "Did the individual remain in hospital for more than 24 hours?",
        "NoListCol" => true
    ),
    "link_pprop_damaged" => array(
        "Type" => "yesno",
        "Title" => "Personal property lost/damaged/stolen?",
        "NoListCol" => true
    ),
    "link_clin_factors" => array(
        "Type" => "multilistbox",
        "Title" => "Clinical factors involved",
        "NoListCol" => true,
    	"MaxLength" => 254
    ),
    "link_direct_indirect" => array(
        "Type" => "yesno",
        "Title" => "Direct / indirect contact made?",
        "NoListCol" => true
    ),
    "link_injury_caused" => array(
        "Type" => "yesno",
        "Title" => "Was physical Injury caused?",
        "NoListCol" => true
    ),
    "link_attempted_assault" => array(
        "Type" => "yesno",
        "Title" => "Was there an attempted assault?",
        "NoListCol" => true
    ),
    "link_discomfort_caused" => array(
        "Type" => "yesno",
        "Title" => "Was personal discomfort caused?",
        "NoListCol" => true
    ),
    "link_public_disorder" => array(
        "Type" => "yesno",
        "Title" => "Was there public disorder?",
        "NoListCol" => true
    ),
    "link_verbal_abuse" => array(
        "Type" => "yesno",
        "Title" => "Was there verbal abuse?",
        "NoListCol" => true
    ),
    "link_harassment" => array(
        "Type" => "yesno",
        "Title" => "Was there harassment/malicious communications?",
        "NoListCol" => true
    ),
    "link_police_pursue" => array(
        "Type" => "yesno",
        "Title" => "Does the individual want the police to pursue the matter?",
        "NoListCol" => true
    ),
    "link_police_persue_reason" => array(
        "Type" => "textarea",
        "Title" => "If no answered to the above question please state why",
        "Rows" => 5,
        "Columns" => 50,
        "NoListCol" => true
    ),
    'link_date_admission' => array(
        'Type' => 'date',
        'NotFuture' => true,
        'Title' => 'Date of admission'
    ),
    'link_notify_progress' => array(
        "Type" => "yesno",
        "Title" => "Do you require progress updates on this incident?",
        "NoListCol" => true
    ),
    "link_injury1" => array(
        'BlockFromReports' => true,
        "Type" => "ff_select",
        "Title" => "Injury",
        "NoListCol" => true
    ),
    "link_bodypart1" => array(
        'BlockFromReports' => true,
        "Type" => "ff_select",
        "Title" => "Body part",
        "NoListCol" => true
    ),
    "show_injury" => array(
        "Type" => "checkbox",
        "Title" => "Was the person injured in the incident?",
        "NoOrder" => true,
        "NoListCol" => true
    ),
    "show_pars" => array(
        "Type" => "checkbox",
        "Title" => "Is this incident reportable to SIRS?",
        "NoListCol" => true
    ),
    "show_assailant" => array(
        "Type" => "checkbox",
		"Title" => "Is there an alleged assailant for the incident?",
        "NoOrder" => true,
        "NoListCol" => true
    ),
    "inc_pasno1" => array(
        "Type" => "string",
        "Title" => "PAS no. 1",
        "Width" => 30,
        "MaxLength" => 254
    ),
    "inc_pasno2" => array(
        "Type" => "string",
        "Title" => "PAS no. 2",
        "Width" => 30,
        "MaxLength" => 254
    ),
    "inc_pasno3" => array(
        "Type" => "string",
        "Title" => "PAS no. 3",
        "Width" => 30,
        "MaxLength" => 254
    ),
    "icon_cost" => array(
        "Type" => "money",
        "Title" => "Staff absence cost",
        "Width" => 15,
        "ReadOnly" => true,
        "NoListCol" => true
    ),
    "rep_approved" => array(
        "Type" => "ff_select",
        "Title" => "Approval status",
        "Width" => 32,
    ),
    "inc_consequence" => array(
        "Type" => "ff_select",
        "Title" => "Consequence"
    ),
    "inc_consequence_initial" => array(
        "Type" => "ff_select",
        "Title" => "Consequence (Initial)"
    ),
    "inc_grade" => array(
        "Type" => "ff_select",
        "Title" => "Grade"
    ),
    "inc_grade_initial" => array(
        "Type" => "ff_select",
        "Title" => "Grade (Initial)"
    ),
    "inc_likelihood" => array(
        "Type" => "ff_select",
        "Title" => "Likelihood of recurrence"
    ),
    "inc_likelihood_initial" => array(
        "Type" => "ff_select",
        "Title" => "Likelihood of recurrence (Initial)"
    ),
    "caf_level_1" => array(
        "Type" => "ff_select",
        "Title" => "Level 1"),
    "caf_level_2" => array(
        "Type" => "ff_select",
        "Title" => "Level 2"),
    // Multi medications
    "dummy_imed_search_admin" => array(
        "Type" => "string_med_search",
        "Title" => "Search for medication administered",
        "Width" => 40,
        "MaxLength" => 64,
        "FieldFormatsSrc" => "med_name",
        "DummyField" => true,
        "NoMandatory" => true,
        "NoReadOnly" => true,
        "NoListCol" => true,
        "NoSearch" => true,
        "SearchMappingType" => "dummy_imed_search_admin"
    ),
    "imed_class_admin" => array(
        "Type" => "ff_select",
        "Title" => "Class of drug administered",
        'FieldFormatsSrc' => 'med_class',
        "NoListCol" => true
    ),
    "imed_class_correct" => array(
        "Type" => "ff_select",
        "Title" => "Class of correct drug",
        'FieldFormatsSrc' => 'med_class',
        "NoListCol" => true
    ),
    "imed_name_admin" => array(
        "Type" => "ff_select",
        "Title" => "Drug administered",
        "FieldFormatsSrc" => "med_name",
        "NoListCol" => true,
        "requireMinChars" => true
    ),
    "imed_name_correct" => array(
        "Type" => "ff_select",
        "Title" => "Correct drug",
        "FieldFormatsSrc" => "med_name",
        "NoListCol" => true,
        "requireMinChars" => true
    ),
    "imed_brand_admin" => array(
        "Type" => "ff_select",
        "Title" => "Brand name of drug administered",
        'FieldFormatsSrc' => 'med_brand',
        "NoListCol" => true,
        "requireMinChars" => true
    ),
    "imed_brand_correct" => array(
        "Type" => "ff_select",
        "Title" => "Brand name of correct drug",
        'FieldFormatsSrc' => 'med_brand',
        "NoListCol" => true,
        "requireMinChars" => true
    ),
    "imed_manu_admin" => array(
        "Type" => "ff_select",
        "Title" => "Manufacturer of drug administered",
        'FieldFormatsSrc' => 'med_manufacturer',
        "NoListCol" => true
    ),
    "imed_manu_correct" => array(
        "Type" => "ff_select",
        "Title" => "Manufacturer of correct drug",
        'FieldFormatsSrc' => 'med_manufacturer',
        "NoListCol" => true
    ),
    "imed_serial_no" => array(
        "Type" => "string",
        "Title" => "Serial number of drug administered",
        "Width" => 30,
        "MaxLength" => 254,
        'FieldFormatsSrc' => 'med_serial_no',
        "NoListCol" => true
    ),
    "imed_batch_no" => array(
        "Type" => "string",
        "Title" => "Batch number of drug administered",
        "Width" => 30,
        "MaxLength" => 254,
        'FieldFormatsSrc' => 'med_batch_no',
        "NoListCol" => true
    ),
    "imed_route_admin" => array(
        "Type" => "ff_select",
        "Title" => "Actual route of delivery",
        'FieldFormatsSrc' => 'med_route',
        "NoListCol" => true
    ),
    "dummy_imed_search_correct" => array(
        "Type" => "string_med_search",
        "Title" => "Search for correct medication",
        "Width" => 40,
        "MaxLength" => 64,
        "FieldFormatsSrc" => "med_name",
        "DummyField" => true,
        "NoMandatory" => true,
        "NoReadOnly" => true,
        "NoListCol" => true,
        "NoSearch" => true,
        "SearchMappingType" => "dummy_imed_search_correct"
    ),
    "imed_route_correct" => array(
        "Type" => "ff_select",
        "Title" => "Correct route",
        'FieldFormatsSrc' => 'med_route',
        "NoListCol" => true
    ),
    "imed_dose_admin" => array(
        "Type" => "ff_select",
        "Title" => "Actual dose administered",
        'FieldFormatsSrc' => 'med_dose',
        "NoListCol" => true
    ),
    "imed_dose_correct" => array(
        "Type" => "ff_select",
        "Title" => "Correct dose",
        'FieldFormatsSrc' => 'med_dose',
        "NoListCol" => true
    ),
    "imed_form_admin" => array(
        "Type" => "ff_select",
        "Title" => "Actual form administered",
        'FieldFormatsSrc' => 'med_form',
        "NoListCol" => true
    ),
    "imed_form_correct" => array(
        "Type" => "ff_select",
        "Title" => "Correct form",
        'FieldFormatsSrc' => 'med_form',
        "NoListCol" => true
    ),
    "imed_controlled_admin" => array(
        "Type" => "ff_select",
        "Title" => "Is drug administered a controlled drug?",
        'FieldFormatsSrc' => 'med_controlled',
        "NoListCol" => true
    ),
    "imed_controlled_correct" => array(
        "Type" => "ff_select",
        "Title" => "Is correct drug a controlled drug?",
        'FieldFormatsSrc' => 'med_controlled',
        "NoListCol" => true
    ),
    "imed_type_admin" => array(
        "Type" => "ff_select",
        "Title" => "Type of drug administered",
        'NoListCol' => true,
        'FieldFormatsSrc' => 'med_type'
    ),
    "imed_type_correct" => array(
        "Type" => "ff_select",
        "Title" => "Type of correct drug",
        'FieldFormatsSrc' => 'med_type',
        "NoListCol" => true
    ),
    "imed_error_stage" => array(
        "Type" => "ff_select",
        "Title" => "Stage at which error occurred",
        'FieldFormatsSrc' => 'med_error_stage',
        "NoListCol" => true
    ),
    "imed_error_type" => array(
        "Type" => "ff_select",
        "Title" => "Type of error",
        'FieldFormatsSrc' => 'med_error_type',
        "NoListCol" => true
    ),
    "imed_notes" => array(
        "Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 10,
        "Columns" => 70,
        "NoListCol" => true
    ),
    "imed_price_admin" => array(
        "Type" => "string",
        "Title" => "Price of drug administered",
        "Width" => 30,
        "MaxLength" => 255,
        'FieldFormatsSrc' => 'med_price',
        "NoListCol" => true
    ),
    "imed_price_correct" => array(
        "Type" => "string",
        "Title" => "Price of correct drug",
        "Width" => 30,
        "MaxLength" => 255,
        'FieldFormatsSrc' => 'med_price',
        "NoListCol" => true
    ),
    "imed_reference_admin" => array(
        "Type" => "string",
        "Title" => "Reference of drug administered",
        "Width" => 30,
        "MaxLength" => 255,
        'FieldFormatsSrc' => 'med_reference',
        "NoListCol" => true
    ),
    "imed_reference_correct" => array(
        "Type" => "string",
        "Title" => "Reference of correct drug",
        "Width" => 30,
        "MaxLength" => 255,
        'FieldFormatsSrc' => 'med_reference',
        "NoListCol" => true
    ),
    "inc_causal_factors_linked" => array(
        "Type" => "yesno",
        "Title" => "Were there any Causal factors?",
        "NoListCol" => true
    ),
    // Issues
    "caf_level_1" => array(
        "Type" => "ff_select",
        "Title" => "Level 1",
        'FieldFormatsName' => 'caf_level_1',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "caf_level_2" => array(
        "Type" => "ff_select",
        "Title" => "Level 2",
        'FieldFormatsName' => 'caf_level_2',
        'NoListCol' => true,
        "OldCodes" => true
    ),
	// CCS2
    'inc_affecting_tier_zero' => array(
        'Type' => 'ff_select',
        'Title' => 'Incident affecting'
    ),
	'inc_type_tier_one' => array(
		'Type' => 'ff_select',
		'Title' => 'Incident type tier 1'
    ),
	'inc_type_tier_two' => array(
		'Type' => 'ff_select',
		'Title' => 'Incident type tier 2'
    ),
	'inc_type_tier_three' => array(
		'Type' => 'ff_select',
		'Title' => 'Incident type tier 3'
    ),
    'inc_level_intervention' => array(
        'Type' => 'ff_select',
        'Title' => 'Level of intervention'
    ),
    'inc_level_harm' => array(
        'Type' => 'ff_select',
        'Title' => 'Level of harm'
    ),
    'inc_never_event' => array(
        'Type' => 'yesno',
		'Title' => 'Was the incident a Never Event?'
    ),
    'rea_code' => array(
        'Type' => 'ff_select',
        'Title' => 'Reason for rejection',
        'NoListCol' => true
    ),
    'rea_text' => array(
        'Type' => 'textarea',
        'Title' => 'Further Details',
        'NoListCol' => true
    ),
    'rea_con_name' => array(
        'Type' => 'ff_select',
        'Title' => 'Rejected by',
        'NoListCol' => true
    ),
    'INC_SAVED_QUERIES' => array(
        'Type' => 'multilistbox',
        'MaxLength' => 70
    ),
    'inc_last_updated' => [
        'Type' => 'string',
        'Title' => 'Last updated',
        'Width' => 32,
        'Computed' => true,
        'NoSearch' => true
    ],
    'inc_ot_q1' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q1_title')
    ),
    'inc_ot_q2' => array(
        'Type' => 'date',
        'Title' => _tk('inc_ot_q2_title'),
        'NotFuture' => true
    ),
    'inc_ot_q3' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q3_title')
    ),
    'inc_ot_q4' => array(
        'Type' => 'textarea',
        'Title' => _tk('inc_ot_q4_title'),
        'Rows' => 10,
        'Columns' => 70
    ),
    'inc_ot_q5' => array(
        'Type' => 'textarea',
        'Title' => _tk('inc_ot_q5_title'),
        'Rows' => 10,
        'Columns' => 70
    ),
    'inc_ot_q6' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q6_title')
    ),
    'inc_ot_q7' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q7_title')
    ),
    'inc_ot_q8' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q8_title')
    ),
    'inc_ot_q9' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q9_title')
    ),
    'inc_ot_q10' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q10_title')
    ),
    'inc_ot_q11' => array(
        'Type' => 'date',
        'Title' => _tk('inc_ot_q11_title'),
        'NotFuture' => true
    ),
    'inc_ot_q12' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q12_title')
    ),
    'inc_ot_q13' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q13_title')
    ),
    'inc_ot_q14' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q14_title')
    ),
    'inc_ot_q15' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q15_title')
    ),
    'inc_ot_q16' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q16_title')
    ),
    'inc_ot_q17' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q17_title')
    ),
    'inc_ot_q18' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q18_title')
    ),
    'inc_ot_q19' => array(
        'Type' => 'textarea',
        'Title' => _tk('inc_ot_q19_title'),
        'Rows' => 10,
        'Columns' => 70
    ),
    'inc_ot_q20' => array(
        'Type' => 'ff_select',
        'Title' => _tk('inc_ot_q20_title')
    ),
    "imed_other_factors" => array(
        "Type" => "ff_select",
        "Title" => "Were there other important factors?",
        'FieldFormatsSrc' => 'med_other_factors',
        "NoListCol" => true
    ),
    "imed_right_wrong_medicine_admin" => array(
        "Type" => "ff_select",
        "Title" => "In this incident, this was the right / wrong medicine?",
        'FieldFormatsSrc' => 'med_right_wrong_medicine',
        "NoListCol" => true
    ),
    "imed_right_wrong_medicine_correct" => array(
        "Type" => "ff_select",
        "Title" => "In this incident, this was the right / wrong medicine?",
        'FieldFormatsSrc' => 'med_right_wrong_medicine',
        "NoListCol" => true
    ),
    "imed_manufacturer_special_admin" => array(
        "Type" => "ff_select",
        "Title" => "Is the medicine a manufactured special?",
        'FieldFormatsSrc' => 'med_manufacturer_special',
        "NoListCol" => true
    ),
    /* fields not used in the web, but default names might be needed for reporting if WEB_LABELS_ONLY is set */
    'inc_staffgroups' => array('BlockFromReports' => true, 'Title' => 'Staff groups', 'Type' => 'ff_select'),
    'secgroup' => array('BlockFromReports' => true, 'NoListCol_Override' => true),
    'seclevel' => array('BlockFromReports' => true, 'NoListCol_Override' => true),
    'inc_recommend' => array('Title' => 'Recommendations'),
    'inc_imprstrats' => array('Title' => 'Improvement strategies'),
    'inc_extrainfo' => array('Title' => 'Extra information'),
    'inc_dsched-inc_dopened' => array('BlockFromReports' => true, 'Title' => 'Days opened to closed', 'NoListCol_Override' => true),
    'inc_dreported-inc_dincident' => array('BlockFromReports' => true, 'Title' => 'Days to report', 'NoListCol_Override' => true),
    'inc_dsched-inc_dincident' => array('BlockFromReports' => true, 'Title' => 'Incident date to closed', 'NoListCol_Override' => true),
    'inc_unit_type' => array('BlockFromReports' => true, 'Title' => 'Unit type', 'Type' => 'ff_select'),
    'inc_recomm_code' => array('Title' => 'Recommendations codes', 'Type' => 'ff_select'),
    'inc_impact' => array('BlockFromReports' => true, 'Title' => 'Impact on person', 'Type' => 'ff_select'),
    'inc_imprstrats2' => array('BlockFromReports' => true, 'Title' => 'Improvement strategies', 'Type' => 'ff_select'),
    'inc_problems' => array('BlockFromReports' => true, 'Title' => 'Problems', 'Type' => 'ff_select'),
    'inc_clinoutcome2' => array('Title' => 'Outcome (long term)', 'Type' => 'ff_select'),
    'ipp_value' => array('Title' => 'Value of personal property', 'NoListCol_Override' => true),
    'ipp_description' => array('Title' => 'Description of personal property', 'NoListCol_Override' => true),
    'ipp_damage_type' => array('Title' => 'Type of damage/loss', 'NoListCol_Override' => true),
    'link_npsa' => array('Title' => 'NPSA', 'NoListCol_Override' => true),
    'link_mhact' => array('Title' => 'Mental Health Act?', 'NoListCol_Override' => true),
    'listorder' => array('Title' => 'List Order', 'NoListCol_Override' => true),
    'inc_pal_count' => array('Title' => 'No. of Linked PALS', 'NoListCol_Override' => true),
    'inc_com_count' => array('Title' => 'No. of Linked Complaints', 'NoListCol_Override' => true),
    'inc_cla_count' => array('Title' => 'No. of Linked Claims', 'NoListCol_Override' => true),
    'cst_subtype' => array('Title' => 'Subtype', 'NoListCol_Override' => true),
    'cst_act_est' => array('Title' => 'Actual/estimate', 'NoListCol_Override' => true),
    'doc_dcreated' => array('Title' => 'Date Document Created', 'NoListCol_Override' => true),
    'doc_notes' => array('Title' => 'Document Description', 'NoListCol_Override' => true),
    'rea_type' => array('Title' => 'Type', 'BlockFromReports' => true, 'NoListCol_Override' => true),
    'con_id' => array('Title' => 'Rejected By', 'NoListCol_Override' => true),
    'rea_dlogged' => array('Title' => 'Date Rejected', 'NoListCol_Override' => true),
    'sum_sev_cost' => array('Title' => 'Severity Cost', 'NoListCol_Override' => true),
    'sum_oth_cost' => array('Title' => 'Other Cost', 'NoListCol_Override' => true),
    'sum_icon_cost' => array('Title' => 'Staff absence cost', 'NoListCol_Override' => true),
    'sum_sev_cost+sum_icon_cost+sum_oth_cost' => array('Title' => 'Total cost', 'NoListCol_Override' => true),
    '(con_name + " " + con_forenames)' => array('Title' => 'Name (Contacts)', 'NoListCol_Override' => true),

    'link_injuries' => array('BlockFromReports' => true, 'Title' => 'Injuries'),
);

$OptionalFieldDefs['CON_EMPL_MULTI'] = array (
    'con_orgcode' => array(
        'Type' => 'multilistbox',
        'Title' => 'Trust',
    	'MaxLength' => 8000
    ),
    'con_clingroup' => array(
        'Type' => 'multilistbox',
        'Title' => 'Clinical group/Locality',
    	'MaxLength' => 8000
    ),
    'con_directorate' => array(
        'Type' => 'multilistbox',
        'Title' => 'Directorate',
        'OldCodes' => true,
    	'MaxLength' => 8000
    ),
    'con_specialty' => array(
        'Type' => 'multilistbox',
        'Title' => 'Specialty',
        'OldCodes' => true,
    	'MaxLength' => 8000
    ),
    'con_unit' => array(
        'Type' => 'multilistbox',
        'Title' => 'Unit',
    	'MaxLength' => 8000
    ),
    'con_loctype' => array(
        'Type' => 'multilistbox',
        'Title' => 'Location type',
    	'MaxLength' => 8000
    ),
    'con_locactual' => array(
        'Type' => 'multilistbox',
        'Title' => 'Location (exact)',
    	'MaxLength' => 8000
    ),
);

$OptionalFieldDefs['CON_EMPL_SINGLE'] = array (
    'con_orgcode' => array(
        'Type' => 'ff_select',
        'Title' => 'Trust'
    ),
    'con_clingroup' => array(
        'Type' => 'ff_select',
        'Title' => 'Clinical group/Locality'
    ),
    'con_directorate' => array(
        'Type' => 'ff_select',
        'Title' => 'Directorate',
        'OldCodes' => true
    ),
    'con_specialty' => array(
        'Type' => 'ff_select',
        'Title' => 'Specialty',
        'OldCodes' => true
    ),
    'con_unit' => array(
        'Type' => 'ff_select',
        'Title' => 'Unit'
    ),
    'con_loctype' => array(
        'Type' => 'ff_select',
        'Title' => 'Location type'
    ),
    'con_locactual' => array(
        'Type' => 'ff_select',
        'Title' => 'Location (exact)'
    ),
);

$FieldDefs["CON"] = array (
    "con_type" => array(
        "Type" => "ff_select",
		"Title" => "Type",
		"Child" => "con_subtype",
        "OldCodes" => true
    ),
	"con_subtype" => array(
        "Type" => "ff_select",
		"Title" => "Subtype",
		"Parent" => "con_type"
    ),
	"con_number" => array(
        "Type" => "string",
		"Title" => "Patient/staff number",
		"Width" => 30,
		"MaxLength" => 64
    ),
	"con_title" => array(
        "Type" => "string",
		"Title" => "Title",
		"Width" => 5,
		"MaxLength" => 10
    ),
	"con_forenames" => array(
        "Type" => "string",
		"Title" => "First names",
		"Width" => 30,
		"MaxLength" => 64
    ),
	"con_surname" => array(
        "Type" => "string",
		"Title" => "Surname",
		"Width" => 30,
        "MaxLength" => 64
    ),
	"con_gender" => array(
        "Type" => "ff_select",
		"Title" => "Gender"
    ),
	"con_dob" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Date of birth"
    ),
	"con_dod" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Date of death",
        'NotEarlierThan' => array('con_dob'),
    ),
    "con_dopened" => array(
        "Type" => "date",
        'NotFuture' => true,
        "ReadOnly" => true,
		"Title" => "Opened"
    ),
    "con_dclosed" => array(
        "Type" => "date",
        'NotFuture' => true,
        'NotEarlierThan' => array('con_dopened'),
		"Title" => "Closed"
    ),
	"con_address" => array(
        "Type" => "textarea",
		"Title" => "Address",
		"Rows" => 5,
		"Columns" => 50,
		"MaxLength" => 254,
        "NoSpellcheck" => true
    ),
	"con_postcode" => array(
        "Type" => "string",
		"Title" => "Postcode",
		"Width" => 10,
		"MaxLength" => 10,
        'UpperCase' => true
    ),
	"con_jobtitle" => array(
        "Type" => "string",
		"Title" => "Job title",
		"Width" => 30,
		"MaxLength" => 128
    ),
    "con_organisation" => array(
        "Type" => "string",
		"Title" => "Organisation",
		"Width" => 30,
		"MaxLength" => 64
    ),
	"con_tel1" => array(
        "Type" => "string",
		"Title" => "Telephone no. 1",
		"Width" => 30,
		"MaxLength" => 254
    ),
    "con_tel2" => array(
        "Type" => "string",
		"Title" => "Telephone no. 2",
		"Width" => 30,
		"MaxLength" => 254
    ),
    "con_fax" => array(
        "Type" => "string",
		"Title" => "Fax",
		"Width" => 30,
		"MaxLength" => 254
    ),
    "con_email" => array(
        "Type" => "email",
		"Title" => "E-mail",
		"Width" => 40,
		"MaxLength" => 128
    ),
	"con_ethnicity" => array(
        "Type" => "ff_select",
		"Title" => "Ethnicity"
    ),
    "con_language" => array(
        "Type" => "ff_select",
		"Title" => "Language"
    ),
    "con_empl_grade" => array(
        "Type" => "ff_select",
		"Title" => "Grade"
    ),
    "con_payroll" => array(
        "Type" => "string",
		"Title" => "Payroll number",
		"Width" => 30,
		"MaxLength" => 128
    ),
    "con_notes" => array(
        "Type" => "textarea",
		"Title" => "Notes",
		"Rows" => 5,
		"Columns" => 50
    ),
    "con_nhsno" => array(
        "Type" => "string",
        "Title" => "NHS number",
        "Width" => 30,
        "MaxLength" => bYN(GetParm('VALID_NHSNO', 'Y')) ? 12 : 20,
        "eventExtra" => bYN(GetParm('VALID_NHSNO', 'Y')) ? "onkeyup=\"formatNhsNo(jQuery(this))\"" : ""
    ),
    "con_police_number" => array(
        "Type" => "string",
        "Title" => "Police officer shoulder/collar number(s)",
        "Width" => 30,
        "MaxLength" => 128
    ),
    "con_disability" => array(
        "Type" => "multilistbox",
        "Title" => "Disabilities",
    	"MaxLength" => 254
    ),
    "con_religion" => array(
        "Type" => "ff_select",
        "Title" => "Religion"
    ),
    "con_sex_orientation" => array(
        "Type" => "ff_select",
        "Title" => "Sexual orientation"
    ),
    "con_work_alone_assessed" => array(
        "Type" => "yesno",
        "Title" => "Has staff member been risk assessed as a lone worker?"
    ),
    "con_hier_location" => array(
        "Type" => "multilistbox",
        "Title" => "Location",
    	"MaxLength" => 8000
    ),
    "rep_approved" => array(
        "Type" => "ff_select",
        "Title" => "Approval status",
        "Width" => 32,
        'CodeColours' => array('UN' => 'FF0000', 'FA' => '00FF00', 'REJECT' => 'E0E0E0'),
    ),
    "recordid" => array(
        "Type" => "number",
		"Title" => "ID",
		"Width" => 5,
        "ReadOnly" => true
    ),
    //needed for mandatory field on registration screen
    'con_action'=> array('Title' => 'Action', "NoListCol" => true),
    /*complaints link fields - not sure if they should be here or in "COM", but if we move them to "COM" the validation will stop working.*/
    "lcom_dreceived" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Date received",
        "NoListCol" => true
    ),
    "lcom_dack" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Date acknowledged",
        "NoListCol" => true
    ),
    "lcom_dactioned" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Date actioned",
        "NoListCol" => true
    ),
    "lcom_dresponse" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Date responded",
        "NoListCol" => true
    ),
    "lcom_dholding" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Date holding",
        "NoListCol" => true
    ),
    "lcom_dreplied" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Date replied",
        "NoListCol" => true
    ),
    "lcom_dreopened" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Re-opened (Complainant)",
        "NoListCol" => true
    ),
    "inc_injury" => array(
        "Type" => "ff_select",
        "Title" => "Injury",
        "NoListCol" => true
    ),
    "inc_bodypart" => array(
        "Type" => "ff_select",
        "Title" => "Body part",
        "NoListCol" => true
    ),
    "link_injury1" => array(
        "Type" => "ff_select",
        "Title" => "Injury (Primary)"
    ),
    "link_bodypart1" => array(
        "Type" => "ff_select",
        "Title" => "Body part (Primary)"
    ),
    "show_document" => array(
        "Type" => "checkbox",
        "Title" => "Are there any documents to be attached to this record?",
        "NoListCol" => true
    ),
    "indemnity_reserve_assigned" => array(
        "Type" => "money",
        "Title" => "Indemnity incurred",
        "NoListCol" => true,
        "ReadOnly" => true,
        "CalculatedField" => true
    ),
    "expenses_reserve_assigned" => array(
        "Type" => "money",
        "Title" => "Expenses incurred",
        "NoListCol" => true,
        "ReadOnly" => true,
        "CalculatedField" => true
    ),
    "remaining_indemnity_reserve_assigned" => array(
        "Type" => "money",
        "Title" => "Indemnity reserve assigned",
        "NoListCol" => true,
        "ReadOnly" => true,
        "CalculatedField" => true
    ),
    "remaining_expenses_reserve_assigned" => array(
        "Type" => "money",
        "Title" => "Expenses reserve assigned",
        "NoListCol" => true,
        "ReadOnly" => true,
        "CalculatedField" => true
    ),
    "resp_total_paid" => array(
        "Type" => "money",
        "Title" => "Total Paid",
        "NoListCol" => true,
        "ReadOnly" => true,
        "CalculatedField" => true
    ),

    /* fields not used in the web, but default names might be needed for reporting if WEB_LABELS_ONLY is set */
    'sta_user_type' => array('Title' => 'User type', "NoListCol_Override" => true),
    'con_staff_include' => array('Title' => 'Include in staff fields?', "NoListCol_Override" => true),
    'crd_start_time' => array('Title' => 'Start time (Attended)', 'NoListCol_Override' => true),
    'con_browse_order' => array('Title' => 'Browse order'),
    'fullname' => array('Title' => 'Fullname'),
    'link_datetime' => array('Title' => 'Date/Time', 'NoListCol_Override' => true),
    'con_browse_name' => array('Title' => 'Name'),
    'con_cla_count' => array('Title' => 'No. of Linked Claims', 'NoListCol_Override' => true),
    'con_cla_open' => array('Title' => 'No. of Linked Open Claims', 'NoListCol_Override' => true),
    'con_com_count' => array('Title' => 'No. of Linked Complaints', 'NoListCol_Override' => true),
    'con_com_open' => array('Title' => 'No. of Linked Open Complaints', 'NoListCol_Override' => true),
    'con_inc_count' => array('Title' => 'No. of Linked Incidents', 'NoListCol_Override' => true),
    'con_inc_open' => array('Title' => 'No. of Linked Open Incidents', 'NoListCol_Override' => true),
    'con_pal_count' => array('Title' => 'No. of Linked PALS', 'NoListCol_Override' => true),
    'con_pal_open' => array('Title' => 'No. of Linked Open PALS', 'NoListCol_Override' => true),
    'con_rfi_count' => array('Title' => 'No. of Linked RFIs', 'NoListCol_Override' => true),
    'con_rfi_open' => array('Title' => 'No. of Linked Open RFIs', 'NoListCol_Override' => true),
    'crs_name' => array('Title' => 'Name (Attended)', 'NoListCol_Override' => true),
    'crs_synopsis' => array('Title' => 'Description (Attended)', 'NoListCol_Override' => true),
    'crs_organisation' => array('Title' => 'Trust (Attended)'),
    'crd_date' => array('Title' => 'Start date (Attended)', 'NoListCol_Override' => true),
    'crs_type' => array('Title' => 'Type (Attended)', 'NoListCol_Override' => true),
    'crs_frequency' => array('Title' => 'Frequency (Attended)', 'NoListCol_Override' => true),
    'crs_dopened' => array('Title' => 'Opened date (Attended)', 'NoListCol_Override' => true),
    'crs_dclosed' => array('Title' => 'Closed date (Attended)', 'NoListCol_Override' => true),
    'crs_manager' => array('Title' => 'Manager (Attended)', 'NoListCol_Override' => true),
    'crs_tutor' => array('Title' => 'Tutor (Attended)'),
    'crs_subtype' => array('Title' => 'Subtype (Attended)'),
    'crs_mandatory' => array('Title' => 'Mandatory? (Attended)', 'NoListCol_Override' => true),
    'crs_cost' => array('Title' => 'Cost (Attended)', 'NoListCol_Override' => true),
    'crs_min_attend' => array('Title' => 'Min. Attendees (Attended)', 'NoListCol_Override' => true),
    'crs_max_attend' => array('Title' => 'Max. Attendees (Attended)', 'NoListCol_Override' => true),
    'crs_pass_mark' => array('Title' => 'Pass Mark (Attended)', 'NoListCol_Override' => true),
    'crs_pass_grade' => array('Title' => 'Pass Grade (Attended)', 'NoListCol_Override' => true),
    'crd_location' => array('Title' => 'Location (Attended)', 'NoListCol_Override' => true),
    'crd_dexpiry' => array('Title' => 'Expiry date (Attended)', 'NoListCol_Override' => true),
    'crd_end_time' => array('Title' => 'End time (Attended)', 'NoListCol_Override' => true),
    'crd_comments' => array('Title' => 'Comments (Attended)', 'NoListCol_Override' => true),
    'crd_end_date' => array('Title' => 'End date (Attended)', 'NoListCol_Override' => true),
    'crs_name' => array('Title' => 'Name (Missed)', 'NoListCol_Override' => true),
    'crs_synopsis' => array('Title' => 'Description (Missed)', 'NoListCol_Override' => true),
    'crs_organisation' => array('Title' => 'Trust (Missed)'),
    'crs_type' => array('Title' => 'Type (Missed)'),
    'crs_frequency' => array('Title' => 'Frequency (Missed)', 'NoListCol_Override' => true),
    'crs_dopened' => array('Title' => 'Opened date (Missed)', 'NoListCol_Override' => true),
    'crs_dclosed' => array('Title' => 'Closed date (Missed)', 'NoListCol_Override' => true),
    'crs_manager' => array('Title' => 'Manager (Missed)', 'NoListCol_Override' => true),
    'crs_tutor' => array('Title' => 'Tutor (Missed)'),
    'crs_subtype' => array('Title' => 'Subtype (Missed)', 'NoListCol_Override' => true),
    'crs_mandatory' => array('Title' => 'Mandatory? (Missed)', 'NoListCol_Override' => true),
    'crs_cost' => array('Title' => 'Cost (Missed)', 'NoListCol_Override' => true),
    'crs_min_attend' => array('Title' => 'Min. Attendees (Missed)', 'NoListCol_Override' => true),
    'crs_max_attend' => array('Title' => 'Max. Attendees (Missed)', 'NoListCol_Override' => true),
    'crs_pass_mark' => array('Title' => 'Pass Mark (Missed)', 'NoListCol_Override' => true),
    'crs_pass_grade' => array('Title' => 'Pass Grade (Missed)', 'NoListCol_Override' => true),
    'crd_start_time' => array('Title' => 'Start time (Missed)', 'NoListCol_Override' => true),
    'crd_date' => array('Title' => 'Start date (Missed)', 'NoListCol_Override' => true),
    'crd_location' => array('Title' => 'Location (Missed)', 'NoListCol_Override' => true),
    'crd_dexpiry' => array('Title' => 'Expiry date (Missed)', 'NoListCol_Override' => true),
    'crd_end_time' => array('Title' => 'End time (Missed)', 'NoListCol_Override' => true),
    'crd_comments' => array('Title' => 'Comments (Missed)', 'NoListCol_Override' => true),
    'crs_name' => array('Title' => 'Name (Pending)', 'NoListCol_Override' => true),
    'crs_synopsis' => array('Title' => 'Description (Pending)', 'NoListCol_Override' => true),
    'crs_organisation' => array('Title' => 'Trust (Pending)', 'NoListCol_Override' => true),
    'crs_type' => array('Title' => 'Type (Pending)', 'NoListCol_Override' => true),
    'crs_frequency' => array('Title' => 'Frequency (Pending)', 'NoListCol_Override' => true),
    'crs_dopened' => array('Title' => 'Opened date (Pending)', 'NoListCol_Override' => true),
    'crs_dclosed' => array('Title' => 'Closed date (Pending)', 'NoListCol_Override' => true),
    'crs_manager' => array('Title' => 'Manager (Pending)', 'NoListCol_Override' => true),
    'crs_tutor' => array('Title' => 'Tutor (Pending)', 'NoListCol_Override' => true),
    'crs_subtype' => array('Title' => 'Subtype (Pending)', 'NoListCol_Override' => true),
    'crs_mandatory' => array('Title' => 'Mandatory? (Pending)', 'NoListCol_Override' => true),
    'crs_cost' => array('Title' => 'Cost (Pending)', 'NoListCol_Override' => true),
    'crs_min_attend' => array('Title' => 'Min. Attendees (Pending)', 'NoListCol_Override' => true),
    'crs_max_attend' => array('Title' => 'Max. Attendees (Pending)', 'NoListCol_Override' => true),
    'crs_pass_mark' => array('Title' => 'Pass Mark (Pending)', 'NoListCol_Override' => true),
    'crs_pass_grade' => array('Title' => 'Pass Grade (Pending)', 'NoListCol_Override' => true),
    'crd_start_time' => array('Title' => 'Start time (Pending)', 'NoListCol_Override' => true),
    'crd_date' => array('Title' => 'Start date (Pending)', 'NoListCol_Override' => true),
    'crd_location' => array('Title' => 'Location (Pending)', 'NoListCol_Override' => true),
    'crd_dexpiry' => array('Title' => 'Expiry date (Pending)', 'NoListCol_Override' => true),
    'crd_end_time' => array('Title' => 'End time (Pending)', 'NoListCol_Override' => true),
    'crd_comments' => array('Title' => 'Comments (Pending)', 'NoListCol_Override' => true),
);

$FieldDefs["RAM"] = array(
    "recordid" => array(
        "Type" => "number",
	    "Title" => "ID",
        "ReadOnly" => true,
		"Width" => 5
    ),
    "ram_name" => array(
        "Type" => "string",
		"Title" => "Risk title",
		"Width" => 70,
		"MaxLength" => 128
    ),
	"ram_description" => array(
        "Type" => "textarea",
		"Title" => "Description of risk",
		"Rows" => 7,
		"Columns" => 70
    ),
	"ram_synopsis" => array(
        "Type" => "textarea",
		"Title" => "Controls in place",
		"Rows" => 7,
		"Columns" => 70
    ),
	"ram_handler" => array(
        "Type" => "ff_select",
		"Title" => "Handler"
    ),
	"ram_responsible" => array(
        "Type" => "ff_select",
		"Title" => "Manager"
    ),
	"ram_dcreated" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Opened date"
    ),
	"ram_dreview" => array(
        "Type" => "date",
		"Title" => "Review date"
    ),
	"ram_dclosed" => array(
        "Type" => "date",
        'NotFuture' => true,
        'NotEarlierThan' => array('ram_dcreated'),
		"Title" => "Closed date"
    ),
	"ram_risk_type" => array(
        "Type" => "ff_select",
		"Title" => "Type"
    ),
	"ram_risk_subtype" => array(
        "Type" => "ff_select",
		"Title" => "Subtype"
    ),
	"ram_organisation" => array(
        "Type" => "ff_select",
		"Title" => "Trust"
    ),
	"ram_unit" => array(
        "Type" => "ff_select",
		"Title" => "Unit",
        "OldCodes" => true
    ),
	"ram_clingroup" => array(
        "Type" => "ff_select",
		"Title" => "Clinical group"
    ),
	"ram_directorate" => array(
        "Type" => "ff_select",
		"Title" => "Directorate",
        "OldCodes" => true
    ),
	"ram_specialty" => array(
        "Type" => "ff_select",
		"Title" => "Specialty",
        "OldCodes" => true
    ),
	"ram_location" => array(
        "Type" => "ff_select",
		"Title" => "Location (type)",
        "OldCodes" => true
    ),
	"ram_locactual" => array(
        "Type" => "ff_select",
		"Title" => "Location (exact)",
        "OldCodes" => true
    ),
    "notes" => array(
        "Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 10,
        "Columns" => 70,
        "NoListCol" => true
    ),
	"ram_rating" => array(
        "Type" => "number",
		"Title" => "Rating (Initial)"
    ),
	"ram_cur_rating" => array(
        "Type" => "number",
        "Title" => "Rating (Current)",
        'ReadOnly' => true
    ),
	"ram_after_rating" => array(
        "Type" => "number",
		"Title" => "Rating (Target)"
    ),
    "ram_cur_cost" => array(
        "Type" => "money",
        "Title" => "Costs (current)"
    ),
    "ram_level" => array(
        "Type" => "ff_select",
		"Title" => "Level (Initial)",
        'ReadOnly' => true
    ),
    "ram_cur_level" => array(
        "Type" => "ff_select",
        'ReadOnly' => true,
		"Title" => "Level (Current)"
    ),
    "ram_after_level" => array(
        "Type" => "ff_select",
		"Title" => "Level (Target)",
        'ReadOnly' => true
    ),
    "ram_consequence" => array(
        "Type" => "ff_select",
		"Title" => "Consequence (initial)",
        "fformat" => array(
            'fmt_code_table' => (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_conseq' : 'code_ra_conseq'),
            'fmt_code_descr' => 'description',
            'fmt_code_field' => 'code'
        )
    ),
    "ram_cur_conseq" => array(
        "Type" => "ff_select",
		"Title" => "Consequence (current)",
        "fformat" => array(
            'fmt_code_table' => (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_conseq' : 'code_ra_conseq'),
            'fmt_code_descr' => 'description',
            'fmt_code_field' => 'code'
        )
    ),
    "ram_after_conseq" => array(
        "Type" => "ff_select",
		"Title" => "Consequence (target)",
        "fformat" => array(
            'fmt_code_table' => (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_conseq' : 'code_ra_conseq'),
            'fmt_code_descr' => 'description',
            'fmt_code_field' => 'code'
        )
    ),
    "ram_likelihood" => array(
        "Type" => "ff_select",
		"Title" => "Likelihood (initial)",
        "fformat" => array(
            'fmt_code_table' => (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_likeli' : 'code_ra_likeli'),
            'fmt_code_descr' => 'description',
            'fmt_code_field' => 'code'
        )
    ),
    "ram_cur_likeli" => array(
        "Type" => "ff_select",
		"Title" => "Likelihood (current)",
        "fformat" => array(
            'fmt_code_table' => (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_likeli' : 'code_ra_likeli'),
            'fmt_code_descr' => 'description',
            'fmt_code_field' => 'code'
        )
    ),
    "ram_after_likeli" => array(
        "Type" => "ff_select",
		"Title" => "Likelihood (Target)",
        "fformat" => array(
            'fmt_code_table' => (bYN(GetParm('RISK_MATRIX', 'N')) ? 'code_inc_likeli' : 'code_ra_likeli'),
            'fmt_code_descr' => 'description',
            'fmt_code_field' => 'code'
        )
    ),
    "ram_adeq_controls" => array(
        "Type" => "ff_select",
		"Title" => "Controls"
    ),
    "ram_objectives" => array(
        "Type" => "multilistbox",
		"Title" => "Principal objectives",
    	"MaxLength" => 248
    ),
    "dum_ram_initial" => array(
        "Type" => "function",
        "Title" => "Initial",
        "Include" => 'Source/risks/RiskBase.php',
        "Function" => "SectionRiskGrading",
        "Parameters" => array(
            array("V","Data"),
            array("V","FormType"),
            array("S","initial"),
            array("V","Module")
        ),
        "MandatoryField" => "ram_level",
        "NoListCol" => true
    ),
    "dum_ram_current" => array(
        "Type" => "function",
        "Title" => "Current",
        "Include" => 'Source/risks/RiskBase.php',
        "Function" => "SectionRiskGrading",
        "Parameters" => array(
            array("V","Data"),
            array("V","FormType"),
            array("S","current"),
            array("V","Module")
        ),
        "MandatoryField" => "ram_cur_level",
        "NoListCol" => true
    ),
    "dum_ram_residual" => array(
        "Type" => "function",
        "Title" => "Target",
        "Include" => 'Source/risks/RiskBase.php',
        "Function" => "SectionRiskGrading",
        "Parameters" => array(
            array("V","Data"),
            array("V","FormType"),
            array("S","target"),
            array("V","Module")
        ),
        "MandatoryField" => "ram_after_level",
        "NoListCol" => true
    ),
    "show_contacts_RAM" => array(
        "Type" => "checkbox",
        "Title" => "Are there any contacts to attach to this record?",
        "NoListCol" => true
    ),
    "ram_ourref" => array(
        "Type" => "string",
        "Title" => "Risk form reference",
        "Width" => 32,
        "MaxLength" => 32
    ),
    "ram_adeq_controls" => array(
        "Type" => "ff_select",
		"Title" => "Adequacy of Controls"
    ),
    "rep_approved" => array(
        "Type" => "ff_select",
        "Title" => "Approval status",
        "Width" => 32,
    ),
    'ram_last_updated' => [
        'Type' => 'string',
        'Title' => 'Last updated',
        'Width' => 32,
        'Computed' => true,
        'NoSearch' => true
    ],
    /* fields not used in the web, but default names might be needed for reporting if WEB_LABELS_ONLY is set */
    'ram_subcategory' => array('Title' => 'Sub category', 'Type' => 'ff_select'),
    'ram_carestage' => array('Title' => 'Care stage', 'Type' => 'ff_select'),
    'ram_cost_type' => array('Title' => 'Fin. cons. Type', 'Type' => 'ff_select'),
    'ram_unit_type' => array('Title' => 'Unit type', 'Type' => 'ff_select'),
    'ram_clin_detail' => array('Title' => 'Detail', 'Type' => 'ff_select'),
    'ram_type' => array('Title' => 'Type', 'Type' => 'ff_select'),
    'ram_benefit' => array('Title' => 'Benefit', 'Type' => 'ff_select'),
    'ram_category' => array('Title' => 'Category', 'Type' => 'ff_select'),
    'ram_clintype' => array('Title' => 'Adverse event', 'Type' => 'ff_select'),
    'ram_assurance' => array('Title' => 'Assurance sources', 'Type' => 'ff_select'),
    'ram_num_actions' => array('Title' => 'Actions'),
    'ram_parent' => array('Title' => 'Root risk ID'),
    'ram_link_count' => array('Title' => 'Links'),
    'ram_investment' => array('Title' => 'Investment'),
    'ram_after_cost' => array('Title' => 'Costs (Target)', 'Type' => 'money'),
    'ram_action_summary' => array('Title' => 'Action summary'),
    'reminder_note' => array('Title' => 'Notepad Reminder', 'NoListCol_Override' => true),
    'ram_open_actions' => array('Title' => 'Open actions', 'NoListCol_Override' => true),
    'ram_annual_cost' => array('Title' => 'Costs (initial)', 'Type' => 'money'),
    'ram_cb' => array('Title' => 'Cost/benefit', 'Type' => 'money'),
    'ram_act_count' => array('Title' => 'No. of Actions', 'NoListCol_Override' => true),
    'ram_act_open' => array('Title' => 'No. of Open Actions', 'NoListCol_Override' => true),
    'ram_ass_impact' => array('Title' => 'Impact on organisation'),
    'ram_ass_rating' => array('Title' => 'Assurance rating'),
    'ass_object' => array('Title' => 'Objectives', 'NoListCol_Override' => true),
    'ass_ctrl' => array('Title' => 'Controls', 'NoListCol_Override' => true),
    'ass_gctrl' => array('Title' => 'Gaps in controls', 'NoListCol_Override' => true),
    'ass_assure' => array('Title' => 'Assurance', 'NoListCol_Override' => true),
    'ass_gapass' => array('Title' => 'Gaps in assurance', 'NoListCol_Override' => true),
    'ass_source' => array('Title' => 'Sources', 'NoListCol_Override' => true),
    'ram_reduction' => array('Title' => 'Reduction'),
    'reminder_date' => array('Title' => 'Notepad Reminder Date', 'NoListCol_Override' => true),
    'show_document' => array(
        'Type' => 'checkbox',
        'Title' => 'Are there any documents to be attached to this record?',
        'NoListCol' => true
    ),

    //contacts fields to be blocked from reports in this modules
    'link_injuries' => array('BlockFromReports' => true, 'Title' => 'Injuries (Persons)', 'NoListCol' => true),
    'link_injury1' => array('Title' => 'Injury (primary)', 'BlockFromReports' => true, 'Type' => 'ff_select'),
    'link_bodypart1' => array('Title' => 'Body part (primary)', 'BlockFromReports' => true, 'Type' => 'ff_select'),
);

$FieldDefs["ACT"] = array(
    "act_dstart" => array(
        "Type" => "date",
        "Title" => "Start date"
    ),
    "act_ddue" => array(
        "Type" => "date",
        'NotEarlierThan' => array('act_dstart'),
        "Title" => "Due date"
    ),
    "act_ddone" => array(
        "Type" => "date",
        "NotFuture" => true,
        "Title" => "Done date"
    ),
    "act_priority" => array(
        "Type" => "ff_select",
        "Title" => "Priority"
    ),
    "act_descr" => array(
        "Type" => "string",
        "Title" => "Description",
        "MaxLength" => 254,
        "Width" => 47
    ),
    "act_from_inits" => array(
        "Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Assigned by ('From')"
    ),
    "act_to_inits" => array(
        "Type" => "ff_select",
        "Title" => "Responsibility ('To')"
    ),
    "act_by_inits" => array(
        "Type" => "ff_select",
        "Title" => "Completed by"
    ),
    "act_type" => array(
        "Type" => "ff_select",
        "Title" => "Type"
    ),
    "act_synopsis" => array(
        "Type" => "textarea",
        "Title" => "Synopsis",
        "Rows" => 5,
        "Columns" => 70
    ),
    "act_resources" => array(
        "Type" => "textarea",
        "Title" => "Resource requirements",
        "Rows" => 3,
        "Columns" => 70
    ),
    "act_monitoring" => array(
        "Type" => "textarea",
        "Title" => "Reporting/monitoring requirements",
        "Rows" => 3,
        "Columns" => 70
    ),
    "act_progress" => array(
        "Type" => "textarea",
        "Title" => "Progress",
        "Rows" => 3,
        "Columns" => 70
    ),
    "act_cost" => array(
        "Type" => "money",
        "Title" => "Cost"
    ),
    "act_cost_type" => array(
        "Type" => "ff_select",
        "Title" => "Cost Type"
    ),
    "act_module" => array(
        "Type" => "ff_select",
        "Title" => "Module",
        "NoMandatory" => true,
        "ReadOnly" => true,
        "NoListCol" => true
    ),
    "act_organisation" => array(
        "Type" => "ff_select",
        "Title" => "Trust"
    ),
    "act_unit" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
        "Title" => "Unit"
    ),
    "act_clingroup" => array(
        "Type" => "ff_select",
        "Title" => "Clinical Group"
    ),
    "act_directorate" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
        "Title" => "Directorate"
    ),
    "act_specialty" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
        "Title" => "Specialty"
    ),
    "act_loctype" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
        "Title" => "Location (Type)"
    ),
    "act_locactual" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
        "Title" => "Location exact"
    ),
    "act_cas_id" => array(
        "Type" => "number",
        "Title" => "Linked record ID",
        "ReadOnly" => true
    ),
    "recordid" => array(
        "Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5
    ),
    "act_score" => array(
        "Type" => "number",
        "Condition" => bYN(GetParm("SHOW_ACT_SCORE", "N")),
        "Title" => "Score"
    ),
    /* fields not used in the web, but default names might be needed for reporting if WEB_LABELS_ONLY is set */
    'act_model' => array('Title' => 'Model element', 'Type' => 'ff_select', 'NoListCol_Override' => true),
    'name' => array('Title' => 'Record name', 'NoListCol_Override' => true),
);

$FieldDefs["DOC"] = array (
	"doc_type" => array(
        "Type" => "ff_select",
		"Title" => "Document type"
    ),
	"doc_notes" => array(
        "Type" => "string",
		"Title" => "Description",
        "Width" =>50,
		"MaxLength" =>128
    )
);

$FieldDefs["SAB"] = array(
    "recordid" => array(
        "Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5
    ),
    "sab_title" => array(
        "Type" => "textarea",
        "Title" => "Title",
        "Rows" => 3,
        "Columns" => 70,
        "MaxLength" => 245,
        'NoTimestamp' => true
    ),
	"sab_reference" => array(
        "Type" => "string",
		"Title" => "Reference no.",
		"Width" => 32,
		"MaxLength" => 64
    ),
	"sab_source" => array(
        "Type" => "ff_select",
		"Title" => "Source",
        "DropdownWidth" => -1
    ),
    "sab_type" => array(
        "Type" => "ff_select",
		"Title" => "Type"
    ),
    "sab_subtype" => array(
        "Type" => "ff_select",
		"Title" => "Subtype"
    ),
	"sab_action_type" => array(
        "Type" => "ff_select",
		"Title" => "Action type"
    ),
    "sab_action_descr" => array(
        "Type" => "textarea",
		"Title" => "Action",
		"Rows" => 7,
		"Columns" => 70
    ),
	"sab_manager" => array(
        "Type" => "ff_select",
		"Title" => "Manager"
    ),
	"sab_handler" => array(
        "Type" => "ff_select",
		"Title" => "Handler"
    ),
	"sab_issued_date" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Issued date",
    ),
	"sab_dstart_due" => array(
        "Type" => "date",
		"Title" => "Deadline (action underway)",
        'NotEarlierThan' => array('sab_issued_date')
    ),
    "sab_dend_due" => array(
        "Type" => "date",
		"Title" => "Deadline (action complete)",
        'NotEarlierThan' => array('sab_dstart_due')
    ),
    "sab_dstart" => array(
        "Type" => "date",
		"Title" => "Action underway",
        'NotFuture' => true
    ),
    "sab_dend" => array(
        "Type" => "date",
		"Title" => "Action completed",
        'NotFuture' => true,
        'NotEarlierThan' => array('sab_dstart')
    ),
    "sab_read_by_due" => array(
        "Type" => "date",
		"Title" => "Read by date",
        'NotEarlierThan' => array('sab_issued_date')
    ),
    "sab_dopened" => array(
        "Type" => "date",
		"Title" => "Opened",
        'NotFuture' => true
    ),
    "sab_dclosed" => array(
        "Type" => "date",
		"Title" => "Closed",
        'NotEarlierThan' => array('sab_issued_date','sab_dopened','sab_dstart','sab_dend'),
        'NotFuture' => true
    ),
	"sab_descr" => array(
        "Type" => "textarea",
		"Title" => "Description",
		"Rows" => 7,
		"Columns" => 70
    ),
    "sab_cost" => array(
        "Type" => "money",
		"Title" => "Cost"
    ),
    "sab_cost_notes" => array(
        "Type" => "textarea",
		"Title" => "Financial implications",
		"Rows" => 7,
		"Columns" => 70
    ),
    "rsp_type" => array(
        "Type" => "ff_select",
		"Title" => "Response type",
        "NoListCol" => true
    ),
    "link_rsp_type" => array(
        "Type" => "ff_select",
		"Title" => "Response type",
        "NoListCol" => true
    ),
    "link_rsp_date" => array(
        "Type" => "date",
		"Title" => "Date of response",
        "NoListCol" => true
    ),
    "link_comments" => array(
        "Type" => "textarea",
		"Title" => "Comments",
        "Rows" => 7,
        "Columns" => 70,
        "NoListCol" => true
    ),
    "link_read_ackn" => array(
        "Type" => "ff_select",
		"Title" => "Read?",
        "NoListCol" => true
    ),
    "link_read_date" => array(
        "Type" => "date",
		"Title" => "Date read",
        "NoListCol" => true
    ),
    "sab_organisation" => array(
        "Type" => "ff_select",
        "Title" => "Trust"
    ),
    "sab_unit" => array(
        "Type" => "ff_select",
        "Title" => "Unit"
    ),
    "sab_clingroup" => array(
        "Type" => "ff_select",
        "Title" => "Clinical group"
    ),
    "sab_directorate" => array(
        "Type" => "ff_select",
        "Title" => "Directorate"
    ),
    "sab_specialty" => array(
        "Type" => "ff_select",
        "Title" => "Specialty"
    ),
    "sab_loctype" => array(
        "Type" => "ff_select",
        "Title" => "Location type"
    ),
    "sab_locactual" => array(
        "Type" => "ff_select",
        "Title" => "Location (exact)"
    ),
    "notes" => array(
        "Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 10,
        "Columns" => 70,
        "NoListCol" => true,
        'NoListCol_Override' => true
    ),
    /* fields not used in the web, but default names might be needed for reporting if WEB_LABELS_ONLY is set */
    'sab_issued_time' => array('Title' => 'Time issued', 'Type' => 'date', 'NoListCol_Override' => true),
    'his_type' => array('Title' => 'History type', 'NoListCol_Override' => true),
    'his_email' => array('Title' => 'e-mail', 'NoListCol_Override' => true),
    'his_datetime' => array('Title' => 'Date/Time', 'NoListCol_Override' => true),
    'link_datetime' => array('Title' => 'Date/Time (For action by)', 'NoListCol_Override' => true),
    'wlk_type' => array('Title' => 'Web link type', 'NoListCol_Override' => true),	
	'link_primary' => array('BlockFromReports' => true),
	'updateddate' => array('BlockFromReports' => true),
);

$FieldDefs["AST"] = array(
	"ast_name" => array("Type" => "string",
		"Title" => "Name",
		"Width" => 70,
		"MaxLength" => 254
    ),
	"ast_ourref" => array(
        "Type" => "string",
		"Title" => "Ref.",
		"Width" => 32,
		"MaxLength" => 254
    ),
	"ast_organisation" => array(
        "Type" => "ff_select",
		"Title" => "Trust"
    ),
	"ast_unit" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
		"Title" => "Unit"
    ),
	"ast_clingroup" => array(
        "Type" => "ff_select",
		"Title" => "Clinical group"
    ),
	"ast_directorate" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
		"Title" => "Directorate"
    ),
	"ast_specialty" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
		"Title" => "Specialty"
    ),
	"ast_loctype" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
		"Title" => "Location (type)"
    ),
	"ast_locactual" => array(
        "Type" => "ff_select",
        'OldCodes' => true,
		"Title" => "Location (exact)"
    ),
	"ast_type" => array(
        "Type" => "ff_select",
		"Title" => "Type"
    ),
	"ast_product" => array(
        "Type" => "ff_select",
		"Title" => "Product"
    ),
	"ast_model" => array(
        "Type" => "ff_select",
		"Title" => "Model"
    ),
	"ast_manufacturer" => array(
        "Type" => "ff_select",
		"Title" => "Manufacturer"
    ),
	"ast_supplier" => array(
        "Type" => "ff_select",
		"Title" => "Supplier"
    ),
	"ast_catalogue_no" => array(
        "Type" => "string",
		"Title" => "Catalogue no.",
		"Width" => 32,
		"MaxLength" => 254
    ),
	"ast_batch_no" => array(
        "Type" => "string",
		"Title" => "Batch no.",
		"Width" => 32,
		"MaxLength" => 254
    ),
	"ast_serial_no" => array(
        "Type" => "string",
		"Title" => "Serial no.",
		"Width" => 32,
		"MaxLength" => 254
    ),
	"ast_dmanufactured" => array(
        "Type" => "date",
		"Title" => "Date of manufacture",
        'NotFuture' => true,
    ),
	"ast_dputinuse" => array(
        "Type" => "date",
        'NotFuture' => true,
		"Title" => "Date put in use",
    ),
	"ast_dlastservice" => array(
        "Type" => "date",
		"Title" => "Date of last service",
        'NotFuture' => true
    ),
	"ast_dnextservice" => array(
        "Type" => "date",
		"Title" => "Date of next service",
    ),
	"ast_cemarking" => array(
        "Type" => "yesno",
        "Title" => "CE marking?"
    ),
	"ast_location" => array(
        "Type" => "string",
		"Title" => "Location",
		"Width" => 70,
		"MaxLength" => 254
    ),
    "ast_quantity" => array(
        "Type" => "number",
		"Title" => "Quantity",
		"Width" => 5
    ),
	"ast_descr" => array(
        "Type" => "textarea",
		"Title" => "Description",
		"Rows" => 7,
		"Columns" => 70
    ),
    "ast_category" => array(
        "Type" => "ff_select",
        "Title" => "Property category"
    ),
    "ast_cat_other_info" => array(
        "Type" => "textarea",
        "Title" => "Additional information",
        "Rows" => 5,
        "Columns" => 50
    ),
	"notes" => array(
        "Type" => "textarea",
		"Title" => "Notes",
		"Rows" => 14,
		"Columns" => 70,
        'NoListCol_Override' => true
    ),
    "recordid" => array(
        "Type" => "number",
		"Title" => "ID",
        "ReadOnly" => true,
		"Width" => 5
    ),
    "rep_approved" => array(
        "Type" => "ff_select",
        "Title" => "Approval status",
        "Width" => 32,
    )
);

$FieldDefs["DST"] = array(
    "recordid" => array(
        "Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5
    ),
	"dst_name" => array(
        "Type" => "string",
		"Title" => "Name",
		"Width" => 70,
		"MaxLength" => 254
    ),
	"dst_descr" => array(
        "Type" => "textarea",
		"Title" => "Description",
		"Rows" => 7,
		"Columns" => 70
    ),
    "dst_private" => array(
        "Type" => "yesno",
        "Title" => "Private?"
    ),
);

$FieldDefs["MSG"] = array(
    "msg_subject" => array(
        "Type" => "string",
        "Title" => "Subject",
        "Width" => 70,
        "MaxLength" => 254
    ),
    "msg_message" => array(
        "Type" => "textarea",
        "Title" => "Message",
        "Rows" => 7,
        "Columns" => 70
    ),
    "msg_status" => array(
        "Type" => "ff_select",
        "Title" => "Status"
    ),
    "msg_from_fullname" => array(
        "Type" => "string",
        "Title" => "From",
        "Width" => 70,
        "MaxLength" => 254
    ),
    "msg_to_fullname" => array(
        "Type" => "string",
        "Title" => "To",
        "Width" => 70,
        "MaxLength" => 254
    ),
    "msg_date" => array(
        "Type" => "date",
        "Title" => "Date",
    ),
    "msg_time" => array(
        "Type" => "time",
        "Title" => "Time",
    ),
    "mod_title" => array(
        "Type" => "string",
        "Title" => "Module",
        "Width" => 70,
        "MaxLength" => 254
    ),
    "link_id" => array(
        "Type" => "number",
        "Title" => "Module Link ID",
        "Width" => 5
    ),
    "msg_read_date" => array(
        "Type" => "date",
        "Title" => "Read date",
    ),
);

//Medications
$FieldDefs["MED"] = array(
    "recordid" => array(
        "Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5
    ),
    "med_class" => array(
        "Type" => "ff_select",
        "Title" => "Class"
    ),
    "med_name" => array(
        "Type" => "ff_select",
        "Title" => "Drug"
    ),
    "med_brand" => array(
        "Type" => "ff_select",
        "Title" => "Brand"
    ),
    "med_manufacturer" => array(
        "Type" => "ff_select",
        "Title" => "Manufacturer"
    ),
    "med_controlled" => array(
        "Type" => "ff_select",
        "Title" => "Is drug administered a controlled drug?",
        "NoListCol" => true
    ),
    "med_type" => array(
        "Type" => "ff_select",
        "Title" => "Type"
    ),
    "med_price" => array(
        "Type" => "string",
        "Title" => "Price",
        "MaxLength" => 255
    ),
    "med_reference" => array(
        "Type" => "string",
        "Title" => "Reference",
        "MaxLength" => 255
    )
);

$FieldDefs['ADM'] = array (      //used for user forms
    'login' => array(
        'Type' => 'string',
        'Title' => 'Login name',
        'Width' => 30,
        'MaxLength' => 254,
    	"NoListCol_Override" => true
    ),
    'login_no_domain' => array(
        'Type' => 'string',
        'Title' => 'Login name',
        'Width' => 30,
        'MaxLength' => 254
    ),
    'sta_domain' => array(
        'Type' => 'string',
        'Title' => 'Domain',
        'ReadOnly' => true
    ),
    'sta_sid' => array(
        'Type' => 'string',
        'Title' => 'SID',
        'ReadOnly' => true
    ),
    'initials' => array(
        'Type' => 'string',
        'Title' => 'Initials',
        'UpperCase' => true,
        'Width' => 5,
        'MaxLength' => 6
    ),
    'sta_title' => array(
        'Type' => 'string',
        'Title' => 'Title',
        'Width' => 30,
        'MaxLength' => 64
    ),
    'sta_forenames' => array(
        'Type' => 'string',
        'Title' => 'Forenames',
        'Width' => 30,
        'MaxLength' => 64
    ),
    'sta_surname' => array(
        'Type' => 'string',
        'Title' => 'Surname',
        'Width' => 30,
        'MaxLength' => 64
    ),
    'jobtitle' => array(
        'Type' => 'string',
        'Title' => 'Job title',
        'Width' => 30,
        'MaxLength' => 64
    ),
    'email' => array(
        'Type' => 'email',
        'Title' => 'E-mail',
        'Width' => 30,
        'MaxLength' => 128
    ),
    'password' => array(
        'Type' => 'string',
        'Password' => true,
        "NoListCol_Override" => true,
        'Title' => _tk('password'),
        'Width' => 30
    ),
    'retype_password' => array(
        'Type' => 'string',
        'Password' => true,
        "NoListCol_Override" => true,
        'Title' => _tk('retype_pwd'),
        'Width' => 30
    ),
    'sta_pwd_change' => array(
        'Type' => 'checkbox',
        'Title' => _tk('pwd_change_next_login')
    ),
    'sta_daccessstart' => array(
        'Type' => 'date',
        'Title' => _tk('access_start')
    ),
    'sta_daccessend' => array(
        'Type' => 'date',
        'Title' => _tk('access_end')
    ),
    'lockout' => array(
        'Type' => 'yesno',
        'Title' => _tk('lockout')
    ),
    'con_staff_include' => array(
        'Type' => 'yesno',
        'Title' => 'Include in staff fields?'
    ),
    'sta_lockout_dt' => array(
        'Type' => 'date',
        'Title' => 'Locked out date'
    ),
    'sta_lockout_reason' => array(
        'Type' => 'string',
        'Title' => 'Locked out reason',
        'Width' => 30,
        'MaxLength' => 254
    ),
    'sta_last_login' => array(
        'Type' => 'date',
        'Title' => 'Last login date'
    ),
    'sta_profile' => array(
        'NullZeros' => true,
        'Title' => 'Profile'
    ),
    'con_dob' => array(
        'Type' => 'date',
        'NotFuture' => true,
        'Title' => 'Date of birth'
    ),
    'con_dod' => array(
        'Type' => 'date',
        'NotFuture' => true,
        'Title' => 'Date of death',
        'NotEarlierThan' => array('con_dob'),
    ),
    'con_dopened' => array(
        'Type' => 'date',
        'NotFuture' => true,
        'ReadOnly' => true,
        'Title' => 'Opened'
    ),
    'con_dclosed' => array(
        'Type' => 'date',
        'NotFuture' => true,
        'NotEarlierThan' => array('con_dopened'),
        'Title' => 'Closed'
    ),
    'con_disability' => array(
        'Type' => 'multilistbox',
        'Title' => 'Disabilities',
    	'MaxLength' => 254
    ),
    //profile fields
    'recordid' => array(
        'Type' => 'string',
        'ReadOnly' => true,
        'Title' => 'ID'
    ),
    'pfl_name' => array(
        'Type' => 'string',
        'Title' => 'Name',
        'Width' => 70,
        "NoListCol_Override" => true,
        'MaxLength' => 254
    ),
    "pfl_description" => array(
        "Type" => "textarea",
        "Title" => "Description",
        "Rows" => 7,
        "NoListCol_Override" => true,
        "Columns" => 70
    ),
    'profile_select' => array(
      'Type' => 'multilistbox',
      'Title' => 'Profile',
      'MaxLength' => 254,
      'TitleExtra' => 'Select profiles here to limit the user names presented in this field on end-user forms'
    ),
    'LOGIN_DEFAULT_MODULE' => array(
        'Type' => 'moduleselect',
        "NoListCol_Override" => true,
        'Title' => 'Module at login',
        'Exclude' => array('ELE', 'PRO'),
        'GroupModules' => true
    ),
    'LOGOUT_DEFAULT_MODULE' => array(
        'Type' => 'moduleselect',
        "NoListCol_Override" => true,
        'Title' => 'Module at logout',
        'Exclude' => array(
            'CON', 'ACT', 'STN', 'LIB', 'ELE', 'PRO', 'SAB', 'AST', 'DST', 'DAS', 'MED', 'HSA', 'HOT', 'CQO', 'CQP',
            'CQS', 'PAY', 'TOD', 'LOC', 'ATM', 'ATQ', 'ATI', 'AMO', 'AQU', 'ADM', 'ORG', 'POL'
        ),
        'GroupModules' => true
    ),
    'CUSTOM_REPORT_BUILDER' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Give user access to the customised report builder?'
    ),
    'ADDITIONAL_REPORT_OPTIONS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Give user access to additional reporting options?'
    ),
    'PROG_NOTES_EDIT' => array(
        'Type' => 'formfield',
        "NoListCol_Override" => true,
        'Title' => 'Set permissions for editing progress notes'
    ),
    'ENABLE_GENERATE' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow cross module generation of records'
    ),
    'ENABLE_BATCH_DELETE' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to batch delete records?'
    ),
    'ENABLE_BATCH_UPDATE' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to batch update records?'
    ),
    'ACT_OWN_ONLY' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Restrict user to viewing their own actions?'
    ),
    'PAL_OWN_ONLY' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Restrict user to approving and viewing their own '._tk("PALSNames")
    ),
    'COM_OWN_ONLY' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Restrict user to approving and viewing their own '._tk("COMNames")
    ),
    'CLA_OWN_ONLY' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Restrict user to approving and viewing their own '._tk("CLANames")
    ),
    'RISK_OWN_ONLY' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Restrict user to approving and viewing their own risks'
    ),
	'POL_OWN_ONLY' => array(
		'Type' => 'yesno',
		"NoListCol_Override" => true,
		'Title' => 'Restrict user to approving and viewing their own '._tk("POLNames")
	),
    'DIF1_ONLY_FORM' => array(
        'Type' => 'formdesign',
        'FormModule' => 'INC',
        "NoListCol_Override" => true,
        'FormLevel' => 1,
        'Title' => 'DIF1 form for this user'
    ),
    'DIF2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'INC',
        "NoListCol_Override" => true,
        'Title' => 'DIF2 form for this user'
    ),
    'ACT_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'ACT',
        "NoListCol_Override" => true,
        'Title' => 'Actions form for this user'
    ),
    'RISK2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'RAM',
        "NoListCol_Override" => true,
        'Title' => _tk('risk2_user')
    ),
	'POL2_DEFAULT' => array(
		'Type' => 'formdesign',
		'FormModule' => 'POL',
		"NoListCol_Override" => true,
		'Title' => _tk('pol2_user')
	),
    'PAL2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'PAL',
        "NoListCol_Override" => true,
        'Title' => _tk('pal2_user')
    ),
    'COM2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'COM',
        "NoListCol_Override" => true,
        'Title' => _tk('com2_user')
    ),
    'CLAIM2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'CLA',
        "NoListCol_Override" => true,
        'FormLevel' => 2,
        'Title' => _tk('cla2_user')
    ),
    'CLAIM1_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'CLA',
        "NoListCol_Override" => true,
        'FormLevel' => 1,
        'Title' => _tk('cla1_user')
    ),
    'HSA2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'HSA',
        "NoListCol_Override" => true,
        'Title' => _tk('hsa2_user')
    ),
    'HOT2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'HOT',
        "NoListCol_Override" => true,
        'Title' => _tk('hot2_user')
    ),
    'CON_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'CON',
        "NoListCol_Override" => true,
        'Title' => _tk('CONNamesTitle').' form for this user'
    ),
    'AST_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'AST',
        "NoListCol_Override" => true,
        'Title' =>  'Equipment form for this user'
    ),
    'MED_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'MED',
        "NoListCol_Override" => true,
        'Title' =>  _tk("MEDNameTitle") . ' form for this user'
    ),
    'SAB1_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'SAB',
        'FormLevel' => 1,
        "NoListCol_Override" => true,
        'Title' =>  'SAB1 form for this user'
    ),
    'SAB2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'SAB',
        'FormLevel' => 2,
        "NoListCol_Override" => true,
        'Title' =>  'SAB2 form for this user'
    ),
    'STN2_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'STN',
        "NoListCol_Override" => true,
        'Title' => _tk('stn2_user')
    ),
    'ADM_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'ADM',
        "NoListCol_Override" => true,
        'Title' =>  'User Admin form design'
    ),
    'CQO_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'CQO',
        "NoListCol_Override" => true,
        'Title' =>  'User '._tk('CQONameTitle').' form design'
    ),
    'CQP_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'CQP',
        "NoListCol_Override" => true,
        'Title' =>  'User '._tk('CQPNameTitle').' form design'
    ),
    'CQS_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'CQS',
        "NoListCol_Override" => true,
        'Title' =>  'User '._tk('CQSNameTitle').' form design'
    ),
    'LOC_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'LOC',
        "NoListCol_Override" => true,
        'Title' =>  _tk('LOCNameTitle').' form for this user'
    ),
    'ATI_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'ATI',
        "NoListCol_Override" => true,
        'Title' => _tk('ATIFormUser')
    ),
    'ATI_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'ATI',
        "NoListCol_Override" => true,
        'Title' => _tk('ATI_listing_design_user')
    ),
    'ATQ_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'ATQ',
        "NoListCol_Override" => true,
        'Title' => _tk('ATQFormUser')
    ),
    'ATQ_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'ATQ',
        "NoListCol_Override" => true,
        'Title' => _tk('ATQ_listing_design_user')
    ),
    'ATM_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'ATM',
        "NoListCol_Override" => true,
        'Title' => _tk('ATMFormUser')
    ),
    'ATM_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'ATM',
        "NoListCol_Override" => true,
        'Title' => _tk('ATM_listing_design_user')
    ),
    'AQU_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'AQU',
        "NoListCol_Override" => true,
        'Title' => _tk('AQUFormUser')
    ),
    'AQU_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'AQU',
        "NoListCol_Override" => true,
        'Title' => _tk('AQU_listing_design_user')
    ),
    'AMO_DEFAULT' => array(
        'Type' => 'formdesign',
        'FormModule' => 'AMO',
        "NoListCol_Override" => true,
        'Title' => _tk('AMOFormUser')
    ),
    'AMO_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'AMO',
        "NoListCol_Override" => true,
        'Title' => _tk('AMO_listing_design_user')
    ),
    'ATM_MQ_TEMP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk("ATMMQTEMPNameTitle")
    ),
    'ATM_AAT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk("ATMAATNameTitle")
    ),
    'ATM_CAI' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk("ATMCAINameTitle")
    ),
    'ATI_STAFF_EMAIL' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk("ATI_STAFF_EMAIL_desc")
    ),
    'ATI_LOC_EMAIL' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk("ATI_LOC_EMAIL_desc")
    ),
    'ATM_STAFF_EMAIL' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk("ATM_STAFF_EMAIL_desc")
    ),
    'ASM_CREATE_INSTANCES_EMAIL' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk('ASM_CREATE_INSTANCES_EMAIL_desc')
    ),
    'AMO_SUBMIT_INSTANCE_EMAIL' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk('AMO_SUBMIT_INSTANCE_EMAIL_desc')
    ),
    'AMO_STAFF_EMAIL' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk('AMO_STAFF_EMAIL_desc')
    ),
    'ATI_REVIEWED_EMAIL' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' =>  _tk('ATI_REVIEWED_EMAIL_desc')
    ),
    'ASM_MANAGE_CYCLES' => array(
        'Type' => 'yesno',
        'NoListCol_Override' => true,
        'Title' =>  _tk('ASM_MANAGE_CYCLES_desc')
    ),
    'FULL_ADMIN' => array(
        'Type' => 'yesno',
        'FormModule' => 'ADM',
        "NoListCol_Override" => true,
        'Title' =>  _tk("full_administrator")
    ),
    'DIF2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'INC',
        "NoListCol_Override" => true,
        'Title' => 'DIF2 search form for this user'
    ),
    'ACT_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'ACT',
        "NoListCol_Override" => true,
        'Title' => 'Actions search form for this user'
    ),
    'RISK2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'RAM',
        "NoListCol_Override" => true,
        'Title' => _tk('risk2_search_form_user')
    ),
	'POL2_SEARCH_DEFAULT' => array(
		'Type' => 'searchformdesign',
		'FormModule' => 'POL',
		"NoListCol_Override" => true,
		'Title' => _tk('pol2_search_form_user')
	),
    'PAL2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'PAL',
        "NoListCol_Override" => true,
        'Title' => _tk('pal2_search_form_user')
    ),
    'COM2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'COM',
        "NoListCol_Override" => true,
        'Title' => _tk('com2_search_form_user')
    ),
    'CLAIM2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'CLA',
        "NoListCol_Override" => true,
        'Title' => _tk('cla2_search_form_user')
    ),
    'HSA2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'HSA',
        "NoListCol_Override" => true,
        'Title' => _tk('hsa2_search_form_user')
    ),
    'HOT2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'HOT',
        "NoListCol_Override" => true,
        'Title' => _tk('hot2_search_form_user')
    ),
    'CON_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'CON',
        "NoListCol_Override" => true,
        'Title' => _tk('CONNamesTitle').' search form for this user'
    ),
    'AST_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'AST',
        "NoListCol_Override" => true,
        'Title' =>  'Equipment search form for this user'
    ),
    'MED_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'MED',
        "NoListCol_Override" => true,
        'Title' =>  _tk("MEDNameTitle") . ' search form for this user'
    ),
    'SAB1_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'SAB',
        'FormLevel' => 1,
        "NoListCol_Override" => true,
        'Title' => 'SAB1 search form for this user'
    ),
    'STN2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'STN',
        "NoListCol_Override" => true,
        'Title' => _tk('stn2_search_form_user')
    ),
    'STN_ADD_NEW' => array(
        'Type' => 'yesno',
        'FormModule' => 'STN',
        "NoListCol_Override" => true,
        'Title' => _tk('stn_add_new')
    ),
    'INC_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'INC',
        "NoListCol_Override" => true,
        'Title' => _tk('INC_listing_design_user')
    ),
    'ACT_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'ACT',
        "NoListCol_Override" => true,
        'Title' => _tk('ACT_listing_design_user')
    ),
    'RAM_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'RAM',
        "NoListCol_Override" => true,
        'Title' => _tk('RAM_listing_design_user')
    ),
	'POL_LISTING_ID' => array(
		'Type' => 'listingdesign',
		'FormModule' => 'POL',
		"NoListCol_Override" => true,
		'Title' => _tk('POL_listing_design_user')
	),
    'PAL_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'PAL',
        "NoListCol_Override" => true,
        'Title' => _tk('PAL_listing_design_user')
    ),
    'COM_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'COM',
        "NoListCol_Override" => true,
        'Title' => _tk('COM_listing_design_user')
    ),
    'CLA_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'CLA',
        "NoListCol_Override" => true,
        'Title' => _tk('CLA_listing_design_user')
    ),
    'HSA_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'HSA',
        "NoListCol_Override" => true,
        'Title' => _tk('HSA_listing_design_user')
    ),
    'HOT_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'HOT',
        "NoListCol_Override" => true,
        'Title' => _tk('HOT_listing_design_user')
    ),
    'CON_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'CON',
        "NoListCol_Override" => true,
        'Title' => _tk('CON_listing_design_user')
    ),
    'ADM_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'ADM',
        "NoListCol_Override" => true,
        'Title' => _tk('ADM_listing_design_user')
    ),
    'AST_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'AST',
        "NoListCol_Override" => true,
        'Title' => _tk('AST_listing_design_user')
    ),
    'MED_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'MED',
        "NoListCol_Override" => true,
        'Title' => _tk('MED_listing_design_user')
    ),
    'SAB_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'SAB',
        "NoListCol_Override" => true,
        'FormLevel' => 1,
        'Title' => _tk('SAB_listing_design_user')
    ),
    'STN_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'STN',
        "NoListCol_Override" => true,
        'Title' => _tk('STN_listing_design_user')
    ),
    'CQO_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'CQO',
        "NoListCol_Override" => true,
        'Title' => _tk('CQO_listing_design_user')
    ),
    'CQP_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'CQP',
        "NoListCol_Override" => true,
        'Title' => _tk('CQP_listing_design_user')
    ),
    'CQS_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'CQS',
        "NoListCol_Override" => true,
        'Title' => _tk('CQS_listing_design_user')
    ),
    'LOC_LISTING_ID' => array(
        'Type' => 'listingdesign',
        'FormModule' => 'LOC',
        "NoListCol_Override" => true,
        'Title' => _tk('LOC_listing_design_user')
    ),
    'INC_SHOW_AUDIT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to see audited incident data?'
    ),
    'RAM_SHOW_AUDIT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to see audited risk data?'
    ),
	'POL_SHOW_AUDIT' => array(
		'Type' => 'yesno',
		"NoListCol_Override" => true,
		'Title' => 'Allow user to see audited '. _tk('POLName') .' data?'
	),
    'PAL_SHOW_AUDIT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to see audited PALS record data?'
    ),
    'COM_SHOW_AUDIT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to see audited complaint data?'
    ),
    'CLA_SHOW_AUDIT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to see audited claims data?'
    ),
    'ACT_SHOW_AUDIT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to see audited action data?'
    ),
    'ADM_SHOW_AUDIT' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to see audited user data?'
    ),
    'ADM_GROUP_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to create and edit security groups?'
    ),
    'ADM_VIEW_OWN_USER' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk('adm_view_own_user')
    ),
    'ADM_NO_ADMIN_REPORTS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk('adm_no_admin_reports')
    ),
    'DIF_SHOW_REJECT_BTN' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk('enable_reject_button')
    ),
    'DIF2_HIDE_CONTACTS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk("hide_contacts_tab")
    ),
    'USER_GRANT_ACCESS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk("allow_user_grant_access"),
        'Condition' => bYN(GetParm('SEC_RECORD_PERMS', 'N'))
    ),
    'DIF_OWN_ONLY' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Restrict user to approving and viewing their own incidents'
    ),
    'CON_SHOW_REJECT_BTN' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Enable the reject status for this user'
    ),
    'CON_ALLOW_MERGE_DUPLICATES' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Enable this user to merge duplicate '._tk('CONNames')
    ),
    'INC_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'RAM_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
	'POL_SETUP' => array(
		'Type' => 'yesno',
		"NoListCol_Override" => true,
		'Title' => 'Allow user to set up codes for this module?'
	),
    'COM_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'CLA_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'PAL_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'STN_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'SAB_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'ACT_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'AST_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'CON_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'LIB_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'MED_SETUP' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to set up codes for this module?'
    ),
    'PAY2_SEARCH_DEFAULT' => array(
        'Type' => 'searchformdesign',
        'FormModule' => 'PAY',
        "NoListCol_Override" => true,
        'Title' => 'PAY2 search form for this user'
    ),
    'DELETE_PAY' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete records in this module?'
    ),
    'COPY_INCIDENTS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk('allow_copy_option')
    ),
    'COPY_RISKS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk('allow_copy_option')
    ),
	'COPY_POL' => array(
		'Type' => 'yesno',
		"NoListCol_Override" => true,
		'Title' => _tk('allow_copy_option')
	),
    'COPY_STANDARDS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk('allow_copy_option')
    ),
    'DELETE_ACT_CHAIN' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete action chains?'
    ),
	'CLA_FINANCE_UDFS' => array(
		'Type' => 'multilistbox',
		'MaxLength' => 254
    ),
	'CON_PAS_CHK_FIELDS' => array(
		'Type' => 'multilistbox',
		'MaxLength' => 91
    ), // restricted to 13 codes, for some reason... (see Source/AdminSetup.php)
	'TOD_USERS_ACT' => array(
		'Type' => 'multilistbox',
		'MaxLength' => 254
    ),
	'TOD_USERS_INC' => array(
		'Type' => 'multilistbox',
		'MaxLength' => 254
    ),
	'TOD_USERS_COM' => array(
		'Type' => 'multilistbox',
		'MaxLength' => 254
    ),
	'TOD_USERS_RAM' => array(
		'Type' => 'multilistbox',
		'MaxLength' => 254
    ),
	'TOD_USERS_CQC' => array(
		'Type' => 'multilistbox',
		'MaxLength' => 254
    ),
    'INC_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Incidents documents?'
    ),
    'PAL_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete PALS documents?'
    ),
    'COM_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Complaints documents?'
    ),
    'CLA_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Claims documents?'
    ),
    'STN_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Standards documents?'
    ),
    'LIB_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Library documents?'
    ),
    'SAB_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Safety Alerts documents?'
    ),
    'ACT_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Actions documents?'
    ),
    'AST_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Equipment documents?'
    ),
    'HOT_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Hotspots documents?'
    ),
    'RAM_DELETE_DOCS' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => 'Allow user to delete Risk Register documents?'
    ),
	'POL_DELETE_DOCS' => array(
		'Type' => 'yesno',
		"NoListCol_Override" => true,
		'Title' => 'Allow user to delete '. _tk('POLNameTitle') .' documents?'
	),
    'INC_SAVED_QUERIES_HOME_SCREEN' => array(
        'Type' => 'yesno',
        'NoListCol_Override' => true,
        'Title' => _tk('enable_saved_queries_home_screen')
    ),
    'COM_SHOW_ADD_NEW' => array(
        'Type' => 'yesno',
        "NoListCol_Override" => true,
        'Title' => _tk('com_show_add_new')
    )
);

$FieldDefs['TOD'] = array (
    "recordid" => array(
        "Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5
    ),
    "tod_module" => array(
        "Type" => "ff_select",
        "Title" => "Module"
    ),
    'tod_descr' => array(
        'Type' => 'textarea',
        'Title' => 'Name',
        'Width' => 30,
        'MaxLength' => 254
    ),
    'tod_type' => array(
        'Type' => 'string',
        'Title' => 'Action required',
        'Width' => 30,
        'MaxLength' => 254
    ),
    "tod_mod" => array(
        "Type" => "ff_select",
        "Title" => "Main Module"
    ),
    'tod_due' => array(
        'Type' => 'date',
        'Title' => 'Due date'
    ),
);

if (bYN(GetParm("EMPLOYEE_MULTI_CODES","N")))
{
    $FieldDefs["CON"] = array_merge ($FieldDefs["CON"], $OptionalFieldDefs["CON_EMPL_MULTI"]);
    $FieldDefs["ADM"] = array_merge ($FieldDefs["ADM"], $OptionalFieldDefs["CON_EMPL_MULTI"]);
}
else
{
    $FieldDefs["CON"] = array_merge ($FieldDefs["CON"], $OptionalFieldDefs["CON_EMPL_SINGLE"]);
    $FieldDefs["ADM"] = array_merge ($FieldDefs["ADM"], $OptionalFieldDefs["CON_EMPL_SINGLE"]);
}

$Dir = opendir('Source/generic_modules');

while (false !== ($subdir = readdir($Dir)))
{
    if ($subdir != '.' && $subdir != '..' && is_dir('Source/generic_modules/'.$subdir))
    {
        if (file_exists('Source/generic_modules/'.$subdir.'/FieldDefs.php'))
        {
            include('Source/generic_modules/'.$subdir.'/FieldDefs.php');
        }
    }
}

foreach ($FieldDefs as $main_module => $module_fields)
{
    foreach ($FieldDefs[$main_module] as $field_name => $field_def)
    {
        $FieldDefs[$main_module][$field_name]["Module"] = $main_module;
    }
}

//Ensure FieldDefsExtra has at least all of the information FieldDefs does.
$FieldDefsExtra = $FieldDefs;
//Add linked module fields to main module array definitions for searching etc.
$FieldDefsExtra["INC"] = array_merge ($FieldDefs["INC"], $FieldDefs["CON"], $FieldDefs["MED"]);
$FieldDefsExtra["RAM"] = array_merge ($FieldDefs["RAM"], $FieldDefs["CON"]);
$FieldDefsExtra["SAB"] = array_merge ($FieldDefs["SAB"], $FieldDefs["CON"], $FieldDefs["AST"]);
$FieldDefsExtra["ADM"] = array_merge ($FieldDefs["CON"], $FieldDefs["ADM"]);
//Add link fields (stored in INC) and staff fields (stored in ADM) to 'CON' array
$FieldDefsExtra["CON"] = array_merge ($FieldDefs["INC"], $FieldDefs["ADM"], $FieldDefs["COM"], $FieldDefs["SAB"], $FieldDefs["CLA"], $FieldDefs["CON"]);
//Add linked fields to Asset array
$FieldDefsExtra["AST"] = array_merge ($FieldDefs["INC"], $FieldDefs["AST"]);
//Add elements and prompts to standards
$FieldDefsExtra["STN"] = array_merge ($FieldDefs["STN"], $FieldDefs["ELE"], $FieldDefs["PRO"]);

