<?php

/**
* Returns an array of non-standard fields that are available to include as listing design columns.
*
* @param string $module
*
* @return array $fields Where the keys/values are field names/descriptions
*/
function getCustomFields($module, $Long = true)
{
    switch ($module)
    {
        case 'CQO':
        case 'CQP':
        case 'CQS':
            $fields = getLocationTiers($module, $Long);
            break;
        default:
            $fields = array();
            break;
    }

    return $fields;
}

/**
* Retrieves a list of tiers from the location hierarchy.
*
* @global array $ModuleDefs
*
* @return array
*/
function getLocationTiers($module, $Long = true)
{
    global $ModuleDefs;

    if($Long)
    {
        return  DatixDBQuery::PDO_fetch_all("SELECT 'tier_' + CAST(lti_depth AS varchar), lti_name + ' (".$ModuleDefs['LOC']['NAME'].")' FROM location_tiers", array(), PDO::FETCH_KEY_PAIR);
    }
    else
    {
        return  DatixDBQuery::PDO_fetch_all("SELECT 'tier_' + CAST(lti_depth AS varchar), lti_name FROM location_tiers", array(), PDO::FETCH_KEY_PAIR);
    }
    $PDOParams = array('table' => $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE']);

    return  DatixDBQuery::PDO_fetch_all($sql, $PDOParams, PDO::FETCH_KEY_PAIR);
}
