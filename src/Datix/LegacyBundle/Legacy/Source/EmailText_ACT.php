<?php


$EmailText['New']['Subject'] = 'New action for Datix ' . $inc['record_name'] . ' ' . $inc['mod_ourref'];

$EmailText["New"]["Body"] = "A new action has been assigned to you.  The details are:

Due date: $inc[act_ddue]

Description:

$inc[act_descr]
";

$EmailText['Update']['Subject'] = 'Updated action for Datix ' . $inc['record_name'] . ' ' . $inc['mod_ourref'];

$EmailText["Update"]["Body"] = "An action has been updated.  The details are:

Due date: $inc[act_ddue]

Description:

$inc[act_descr]
";

$EmailText['Complete']['Subject'] = 'Completed action for Datix ' . $inc['record_name'] . ' ' . $inc['mod_ourref'];

$EmailText["Complete"]["Body"] = "An action has been completed.  The details are:

Due date: $inc[act_ddue]

Description:

$inc[act_descr]
";

?>
