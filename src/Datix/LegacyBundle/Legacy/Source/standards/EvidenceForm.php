<?php

/**
* Creates and displays the Evidence form.
*
* @global string $scripturl
* @global array  $JSFunctions
*
* @param  array  $lib             The data for this evidence record.
* @param  string $FormAction      The form mode (new/edit/search etc).
* @param  string $_GET['print']   Set to 1 if we're in print mode.
* @param  string $_GET['panel']   The form panel to display.
*/
function ShowEvidenceForm($lib = "", $FormAction = 'New')
{
	global $scripturl, $JSFunctions, $dtxtitle, $FormType;

	$scripturl = Sanitize::SanitizeURL($scripturl);

    if ($_GET["print"] == 1)
    {
		$FormAction = "Print";
    }

    if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && $FormAction != 'search')
    {
    	require_once 'Source/security/SecurityBase.php';
    	$Perms = GetUserHighestAccessLvlForRecord('LIB_PERMS', 'LIB', (int) $lib['recordid']);
    }
    else
    {
    	$Perms = GetParm('LIB_PERMS');
    }
    $ReadOnly = ($Perms == "LIB_READ");

    if ($ReadOnly)
    {
        $FormAction = 'ReadOnly';
    }

    CheckForRecordLocks('LIB', $lib, $FormAction, $sLockMessage);

    if (($Perms == "LIB_FULL" || $Perms == "LIB_INPUT") && $FormAction != 'Locked')
    {
        $Editable = true;
    }
    else
    {
        $Editable = false;
    }

    // Load form settings
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'LIB', 'level' => 2, 'form_type' => $FormAction));
    //IncludeCurrentFormDesign('LIB', 2, '', $FormAction);

    include('Source/generic_modules/LIB/BasicForm2.php');

    $dtxtitle = $FormDesign->FormTitle;

    getPageTitleHTML(array(
        'title' => $FormDesign->FormTitle,
        'subtitle' => $FormDesign->FormTitleDescr,
        'module' => 'STN'
    ));

    $EvidenceTable = New FormTable($FormAction, 'LIB', $FormDesign);
    $EvidenceTable->MakeForm($FormArray, $lib, 'LIB');

    $buttonGroup = new ButtonGroup();

    if (!$_GET['print'])
    {
        if ($FormAction == "Search")
        {
            $buttonGroup->AddButton(array('id' => 'btnSearch', 'name' => 'btnSearch', 'label' => _tk('btn_search'), 'onclick' => 'document.frmEvidence.rbWhat.value = \'Search\';submitClicked = true;document.frmEvidence.submit();', 'action' => 'SEARCH'));
        }

        if ($Editable)
        {
            if ($FormAction == 'New')
            {
                $buttonGroup->AddButton(array('id' => 'btnSubmitEvi', 'name' => 'btnSubmitEvi', 'label' => _tk('btn_save'), 'onclick' => 'submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()){enableLinkFields();document.frmEvidence.submit();}', 'action' => 'SAVE'));
            }
            else
            {
                if ($lib["recordid"] == "")
                {
                    $buttonGroup->AddButton(array('id' => 'btnCheck', 'name' => 'btnCheck', 'label' => 'Check for matching evidence', 'onclick' => 'evidenceMatch();', 'action' => 'SEARCH'));
                }

                $buttonGroup->AddButton(array('id' => 'btnSaveLib', 'name' => 'btnSaveLib', 'label' => ($_GET['main_recordid'] && $_GET['recordid'] ? 'Create new link' : 'Save'), 'onclick' => 'submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()){enableLinkFields();document.frmEvidence.submit();}', 'action' => 'SAVE'));

                if (is_numeric($_GET['link_recordid']))
                {
                    $buttonGroup->AddButton(array('id' => 'btnUnlink', 'name' => 'btnUnlink', 'label' => 'Unlink evidence', 'onclick' => 'document.frmEvidence.rbWhat.value = \'Unlink\';submitClicked = true; document.frmEvidence.submit();', 'action' => 'DELETE'));
                }
            }
        }

        $buttonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => getConfirmCancelJavascript('frmEvidence'), 'action' => 'CANCEL'));

        if ($_GET['fromlisting'])
        {
            $buttonGroup->AddButton(array('label' => _tk('btn_back_to_report'), 'id' => 'btnBack', 'name' => 'btnBack', 'onclick' => 'submitClicked=true;selectAllMultiCodes();document.frmEvidence.rbWhat.value=\'BackToListing\';document.frmEvidence.submit();', 'action' => 'BACK'));
        }
    }

    if ($FormAction != 'Search' && $FormAction != 'Print' && $_REQUEST['fromsearch'] &&!empty($_SESSION['LIB']['RECORDLIST']) && (!isset($ModuleDefs['LIB']['NO_NAV_ARROWS']) || $ModuleDefs['LIB']['NO_NAV_ARROWS'] != true))
    {
        $CurrentIndex = $_SESSION['LIB']['RECORDLIST']->getRecordIndex(array('recordid' => $lib['recordid']));

        if ($CurrentIndex !== false)
        {
            $buttonGroup->AddNavigationButtons($_SESSION['LIB']['RECORDLIST'], $lib['recordid'], 'LIB');
        }
    }

    if ($FormAction != "Print" && $Perms != "LIB_INPUT")
    {
        GetSideMenuHTML(array('module' => 'LIB', 'recordid' => $lib['recordid'], 'table' => $EvidenceTable, 'buttons' => $buttonGroup));
        template_header();
    }
    else
    {
        template_header_nomenu();
    }

// TODO: this should be removed when this is refactored into a controller
?><script type="text/javascript">
globals.FormType = '<?php echo $FormType; ?>';
globals.printSections = <?php echo json_encode($EvidenceTable->printSections); ?>;
</script><?php

    $JSFunctions[] = 'AlertAfterChange = true;';
    $JSFunctions[] = 'var submitClicked = false;';
    $JSFunctions[] = MakeJavaScriptValidation('LIB', $FormDesign);

    if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && ($FormType != "Print" && $_GET['print'] != 1))
    {
        ?><script type="text/javascript" src="src/generic/js/panelData.js"></script><?php
    }

    if($FormType == "Print" || $_GET['print'] == 1)
    {
        ?><script type="text/javascript" src="src/generic/js/print<?php $MinifierDisabled ? '' : '.min' ?>.js"></script><?php
    }
    ?>
<script type="text/javascript" language="javascript">
    <!--
    function evidenceMatch()
    {
        var url = '<?= $scripturl.'?action=evidencematch&main_module='.Sanitize::getModule($_GET['main_module']).'&main_recordid='.Sanitize::SanitizeInt($_GET['main_recordid']).'&token='.CSRFGuard::getCurrentToken() ?>';

        if (document.frmEvidence.lib_name && document.frmEvidence.lib_name.value != "")
        {
            url += "&lib_name=" + document.frmEvidence.lib_name.value;
        }

        if (document.frmEvidence.lib_ourref && document.frmEvidence.lib_ourref.value != "")
        {
            url += "&lib_ourref=" + document.frmEvidence.lib_ourref.value;
        }

        if (document.frmEvidence.lib_organisation && document.frmEvidence.lib_organisation.value != "")
        {
            url += "&lib_organisation=" + document.frmEvidence.lib_organisation.value;
        }

        if (document.frmEvidence.lib_unit && document.frmEvidence.lib_unit.value != "")
        {
            url += "&lib_unit=" + document.frmEvidence.lib_unit.value;
        }

        if (document.frmEvidence.lib_clingroup && document.frmEvidence.lib_clingroup.value != "")
        {
            url += "&lib_clingroup=" + document.frmEvidence.lib_clingroup.value;
        }

        if (document.frmEvidence.lib_directorate && document.frmEvidence.lib_directorate.value != "")
        {
            url += "&lib_directorate=" + document.frmEvidence.lib_directorate.value;
        }

        if (document.frmEvidence.lib_specialty && document.frmEvidence.lib_specialty.value != "")
        {
			url += "&lib_specialty=" + document.frmEvidence.lib_specialty.value;
        }

        if (document.frmEvidence.lib_type && document.frmEvidence.lib_type.value != "")
        {
            url += "&lib_type=" + document.frmEvidence.lib_type.value;
        }

        if (document.frmEvidence.lib_subtype && document.frmEvidence.lib_subtype.value != "")
        {
            url += "&lib_subtype=" + document.frmEvidence.lib_subtype.value;
        }

        if (document.frmEvidence.lib_loctype && document.frmEvidence.lib_loctype.value != "")
        {
            url += "&lib_loctype=" + document.frmEvidence.lib_loctype.value;
        }

        if (document.frmEvidence.lib_locactual && document.frmEvidence.lib_locactual.value != "")
        {
            url += "&lib_locactual=" + document.frmEvidence.lib_locactual.value;
        }

        if (document.frmEvidence.lib_manager && document.frmEvidence.lib_manager.value != "")
        {
            url += "&lib_manager=" + document.frmEvidence.lib_manager.value;
        }

        if (document.frmEvidence.lib_handler && document.frmEvidence.lib_handler.value != "")
        {
            url += "&lib_handler=" + document.frmEvidence.lib_handler.value;
        }

        if (document.frmEvidence.lib_dopened && document.frmEvidence.lib_dopened.value != "")
        {
            url += "&lib_dopened=" + document.frmEvidence.lib_dopened.value;
        }

        if (document.frmEvidence.lib_dclosed && document.frmEvidence.lib_dclosed.value != "")
        {
            url += "&lib_dclosed=" + document.frmEvidence.lib_dclosed.value;
        }

        if (document.frmEvidence.lib_dreview && document.frmEvidence.lib_dreview.value != "")
        {
            url += "&lib_dreview=" + document.frmEvidence.lib_dreview.value;
        }

        if (document.frmEvidence.lib_descr && document.frmEvidence.lib_descr.value != "")
        {
			url += "&lib_descr=" + document.frmEvidence.lib_descr.value;
        }

        if (document.frmEvidence.lib_references && document.frmEvidence.lib_references.value != "")
        {
            url += "&lib_references=" + document.frmEvidence.lib_references.value;
        }

        if (document.frmEvidence.lib_web_link && document.frmEvidence.lib_web_link.value != "")
        {
            url += "&lib_web_link=" + document.frmEvidence.lib_web_link.value;
        }

        PopupDivFromURL('match_evidence', 'Matching evidence', url, '', [], '400px');
    }

    function enableLinkFields()
    {
        jQuery("input[id*=_link_]").attr('disabled', false);
    }
    //-->
</script>
<form method="post" enctype="multipart/form-data" id="frmEvidence" name="frmEvidence" action="<?= $scripturl . "?action=saveevidence" ?>">
    <input type="hidden" name="form_type" value="<?= $FormAction ?>">
    <input type="hidden" name="recordid" value="<?= Sanitize::SanitizeInt($lib["recordid"]) ?>">
    <input type="hidden" name="updateid" value="<?= Escape::EscapeEntities($lib["updateid"]) ?>">
    <input type="hidden" name="main_module" value="<?= Escape::EscapeEntities($lib['main_module']) ?>">
    <input type="hidden" name="main_recordid" value="<?= Sanitize::SanitizeInt($lib['main_recordid']) ?>">
    <input type="hidden" name="link_recordid" value="<?= Sanitize::SanitizeInt($lib['link_recordid']) ?>">
    <input type="hidden" name="fromsearch" value="<?= Sanitize::SanitizeInt($_REQUEST['fromsearch']) ?>">
    <input type="hidden" name="module" value="<?= Sanitize::SanitizeString($_GET['module']) ?>">
    <input type="hidden" name="fromlisting" value="<?php echo Sanitize::SanitizeInt($_GET['fromlisting']); ?>" />
    <input type="hidden" name="rbWhat" value="Save">
<?php

    if ($lib['error'])
    {
        echo '<div class="error_div">'._tk('form_errors').'</div>';
    }

    if ($sLockMessage)
    {
        echo '
        <div class="lock_message">
            <div id="LockMessage">'.$sLockMessage.'</div>
            <div id="userLockMessage"></div>
        </div>';
    }

	// Display the sections
    $EvidenceTable->MakeTable();
    echo $EvidenceTable->GetFormTable();

    echo $buttonGroup->getHTML();

    echo '
    </form>';

    if ($FormAction != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $EvidenceTable);
    }

    footer();

    obExit();
}

/**
* Displays the documents section of the Evidence form.
*
* @global string $scripturl
*
* @param  array  $lib                   The data for this evidence record.
* @param  string $FormAction            The form mode (new/edit/search etc).
* @param  string $_GET['link_recordid'] ID of the link record if this is a linked piece of evidence.
*/
function ListDocuments($lib, $FormAction)
{
	global $scripturl;

	$scripturl = Sanitize::SanitizeURL($scripturl);

    echo '<div>
    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';

    if (!$lib["documents"])
    {
		echo '
        <tr name="documents_row" id="documents_row">
    	    <td class="windowbg2" colspan="4">
    	    <b>No documents saved.</b>
    	    </td>
        </tr>';
    }
	else
    {
        echo '
        <tr name="documents_row" id="documents_row">
            <td class="windowbg" width="10%" align="center"><b>Created</b></td>
            <td class="windowbg" width="10%" align="center"><b>Type</b></td>
            <td class="windowbg" width="70%" align="center"><b>Description</b></td>
            <td class="windowbg" width="5%" align="center"><b>ID</b></td>';

        if ($FormAction != 'Print' && $FormAction != 'ReadOnly')
        {
            echo '
                <td class="windowbg" width="5%" align="center"></td>';
        }

        echo '
            </tr>';

        foreach ($lib["documents"] as $document)
        {
            $Document = (new \src\documents\model\DocumentModelFactory)->getMapper()->find($document['recordid']);
    ?>
    <tr name="documents_row" id="documents_row">
    	<td class="windowbg2" align="center"><?php echo FormatDateVal($document["doc_dcreated"]); ?></td>
    	<td class="windowbg2"><?= htmlspecialchars($document["type_descr"]) ?></td>
    	<td class="windowbg2"><?php
            if ($Document instanceof \src\documents\model\Document && is_file($Document->GetPath()) && !in_array($FormAction, ["Print", "ReadOnly"]))
            {
                echo "<a href=\"$scripturl?action=downloaddocument&recordid=$document[recordid]&lib_id=$lib[recordid]&main_table=1\">".htmlspecialchars($document[doc_notes])."</a>";
            }
            else
            {
                echo htmlspecialchars($document['doc_notes']);
            }

            ?></td>
        <td class="windowbg2" align="center"><?= $document["recordid"] ?></td>
    	<td class="windowbg2" align="center">
<?php
            if ($FormAction != 'Print' && $FormAction != 'ReadOnly')
            {
                $Document = (new \src\documents\model\DocumentModelFactory)->getMapper()->find($document['recordid']);

                if ($Document instanceof \src\documents\model\Document && is_file($Document->GetPath()))
                {
                    echo '
                        <a href="' . $scripturl . '?action=editdocument&module=LIB&recordid=' . $document['recordid'] . '&link_id=' . $lib['recordid'] . '">[edit]</a>';
                }
                else
                {
                    echo '
                    <img src="Images/no_doc.png" alt="'._tk('missing_document_alt_text').'" title="'. _tk('missing_document_alt_text') .'" />';
                }
            }
?>
        </td>
    </tr>
<?php
		}
	}
    echo '</table></div>';

    if ($FormAction != 'Print' && $FormAction != 'ReadOnly')
    {
        echo '<div class="padded_div"><a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=editdocument&module=LIB&link_id='.$lib['recordid'].'\');}"><b>Add a new document</b></a></div>';
    }
}

/**
* @desc Outputs a listing containing references to any evidence linked to the current record the user is
* accessing or to any records beneath it in the standards tree.
*
* @param string $module The module that the user is currently accessing
* @param int $recordid The recordid of the record the user is currently accessing
* @param string $formType The current form mode.
*
*/
function ListLinkedEvidence($module, $recordid, $formtype)
{
    global $ListingDesigns, $ModuleDefs;

    require_once 'Source/libs/ListingClass.php';

    $Listing = new Listing('LIB', ($ListingDesigns['evidence']['evidence'] ? $ListingDesigns['evidence']['evidence'] : '0'));
    $Listing->LinkModule = $module;

    $Listing->LoadColumnsFromDB();

    if ($module == 'ELE' || $module == 'STN')
    {
        if ($module == 'STN')
        {
            $Listing->AddAdditionalColumns(array('ele_id' => array('title' => 'Element', 'width' => '8')));
        }

        if ($module == 'ELE' || $module == 'STN')
        {
            $Listing->AddAdditionalColumns(array('pro_id' => array('title' => 'Compliance', 'width' => '8')));
        }
    }

    if (CanSeeModule('LIB'))
    {
        $sql = '
            SELECT
                recordid, \'' . Sanitize::SanitizeInt($recordid) . '\' AS ' . $ModuleDefs[$module]['FK'] . ', link_recordid, ' . implode(', ', array_keys($Listing->Columns)) . '
            FROM
                library_main, link_library
            WHERE
                link_library.lib_id = library_main.recordid
                and link_library.' . $ModuleDefs[$module]['FK'] . ' = :recordid
        ';

        $securityWhere = MakeSecurityWhereClause('', 'LIB');

        if ($securityWhere != '')
        {
            $sql .= ' AND '.$securityWhere;
        }

        $sql .= '
            ORDER BY
                library_main.lib_name
        ';

        $EvidenceArray = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $recordid));
    }
    else
    {
        $EvidenceArray = [];
    }

    $Listing->LoadData($EvidenceArray);

    echo '<li>';

    echo $Listing->GetListingHTML($formtype);

    if ($formtype != 'ReadOnly' && !$_GET['print'] && (GetParm('LIB_PERMS') == 'LIB_FULL' || GetParm('LIB_PERMS') == 'LIB_INPUT'))
    {
        echo '<div class="padded_div"><a href="Javascript:if(CheckChange()){SendTo(\'app.php?action=evidence&main_module='.$module.'&main_recordid='.$recordid.'\');}"><b>Add a new piece of evidence</b></a></div>';
    }

    echo '</li>';
}

function ListRelatedEvidenceAtSTNLevel($where, $FormAction)
{
    global $scripturl;

    $sql =
    "SELECT DISTINCT (library_main.recordid) as recordid,
    library_main.lib_ourref as lib_ourref,
    library_main.lib_name as lib_name";
    $sql .= " FROM library_main, link_library, standard_prompts pro, standard_elements ele
                where ((link_library.lib_id = library_main.recordid AND pro.recordid = link_library.pro_id AND ele.recordid = link_library.ele_id)
                    OR (link_library.lib_id = library_main.recordid AND ele.recordid = link_library.ele_id)
                    OR link_library.lib_id = library_main.recordid)
                    AND link_library.stn_id in (SELECT distinct(lnk_id2) FROM LINK_LIBRARY, LINKS
                                            WHERE LINKS.LNK_MOD1 = 'STN' AND LINKS.LNK_ID1 = LINK_LIBRARY.stn_id
                                            AND LINKS.LNK_MOD2 = 'STN'" . ($where?" and $where":"") .")";
    $sql .= " ORDER BY library_main.lib_name";

    $docArray = DatixDBQuery::PDO_fetch_all($sql);

    if ($docArray)
    {

        echo '
<li>
    <table class="gridlines" cellspacing="1" cellpadding="4" width="99%" align="center" border="0">
        <td class="windowbg" width="10%"><b>ID</b></td>
        <td class="windowbg" width="10%"><b>Ref.</b></td>
        <td class="windowbg"><b>Title</b></td>';

        echo '</tr>';

        if (count($docArray) == 0)
        {
            ?><tr><td class="rowtitlebg" colspan="5"><b>No documents saved.</b></td></tr><?php
        }
        else
        {
            foreach ($docArray as $row)
            {
                $linkPresent = ($FormAction != "Print" && $FormAction != 'ReadOnly');

                echo '<tr>';

                if ($linkPresent)
                {
                    $linkToEvidence = $scripturl.'?action=evidence&module=LIB&recordid='.$row['recordid'];

                    $recordidCellContents = ($linkPresent ?
                        '<a href="'.$linkToEvidence.'">'.$row["recordid"].'</a>' : $row["recordid"]);

                    $referenceCellContents = ($linkPresent ?
                        '<a href="'.$linkToEvidence.'">'.$row["lib_ourref"].'</a>' : $row["lib_ourref"]);

                    $nameCellContents = ($linkPresent ?
                        '<a href="'.$linkToEvidence.'">'.$row["lib_name"].'</a>' : $row["lib_name"]);
                    ?>
                <td class="rowtitlebg" width="10%">
                    <?php echo $recordidCellContents ?>
                </td>
                <td class="rowtitlebg" width="10%">
                    <?php echo $referenceCellContents ?>
                    </td>
                <td class="rowtitlebg">
                    <?php echo $nameCellContents ?>
                </td>
                <?php
                }
                else
                {
                    echo '<td class="rowtitlebg" width="10%">' . $row["recordid"] . '</td>';
                    echo '<td class="rowtitlebg" width="10%">' . $row["lib_ourref"] . '</td>';
                    echo '<td class="rowtitlebg" width="10%">' . $row["lib_name"] . '</td>';
                    echo '<td class="rowtitlebg" width="10%">' . $row["stn_id"] . '</td>';
                }
                echo '</tr>';
            }
        }

    echo '
        </table>
    </li>';
    }
}

/**
* Displays a message if users do not have permission to access this Evidence record.
*/
function NoAccessSection()
{
?>
<table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
    <tr>
        <td class="titlebg" colspan="5"><b>Record not found</b>
        </td>
    </tr>
    <tr>
        <td class="rowtitlebg" colspan="5"><b>
        You do not have the necessary permissions to view this record or this record has been deleted.
        </b>
        </td>
    </tr>
</table>
<?php
}

/**
* Creates the section used for multi-linking of this Evidence record.
*
* @global array $JSFunctions
*
* @param  array  $lib         The data for this evidence record.
*/
function linksSection($lib)
{
    global $JSFunctions;

    require_once 'Source/libs/FormClasses.php';

    $section = '
    <li>
        <input type="hidden" name="manageLinksTree" value="1" />
        <script type="text/javascript">
            function toggleStandards(id, img, showOnly)
            {
                var element = jQuery("#"+id);
                var image = "";

                if (id.indexOf("domains") > -1)
                {
                    image = "titlebg";
                }
                else if (id.indexOf("standards") > -1 || id.indexOf("elements") > -1)
                {
                    image = "windowbg";
                }
                else
                {
                    image = "windowbg2";
                }

                if (id.indexOf("standards") > -1 && element.html().indexOf("fromAjax") == -1)
                {
                    // we need to fetch the standards section via AJAX
                    var queryString = "&recordid='.htmlspecialchars($lib['recordid']).'&link_recordid='.htmlspecialchars($lib['link_recordid']).'&main_module='.htmlspecialchars($lib['main_module']).'&responsetype=json";

                    for (key in domains[id.substring(9)])
                    {
                        queryString += "&" + key + "=" + domains[id.substring(9)][key];
                    }

                    jQuery.ajax({
                        url: scripturl + "?action=httprequest&type=getstandards"+queryString+"&responsetype=json",
                        success: function(html)
                        {
                            element.html(html);
                        }
                    });

                    // ensure this section is excluded subsequently if "Show links" clicked
                    domains[id.substring(9)]["linked"] = false;

                    element.show();
                    img.src = "Images/collapse_"+image+".gif";
                }
                else if (id.indexOf("elements") > -1 && element.html().indexOf("fromAjax") == -1)
                {
                    // we need to fetch the elements section via AJAX
                    var standardid = id.substring(8);
                    jQuery.ajax({
                        url: scripturl + "?action=httprequest&type=getelements&stn_id="+standardid+"&lib_id='.htmlspecialchars($lib['recordid']).'&link_recordid='.htmlspecialchars($lib['link_recordid']).'&main_module='.htmlspecialchars($lib['main_module']).'&responsetype=json",
                        success: function(html)
                        {
                            element.html(html);
                        },
                        error: function()
                        {
                            element.html("<div class=\"standards_element\">No elements found.</div>");
                        }
                    });

                    element.show();
                    img.src = "Images/collapse_"+image+".gif";
                }
                else if (element.css("display") && element.css("display") == "none")
                {
                    element.show();
                    img.src = "Images/collapse_"+image+".gif";
                }
                else if (showOnly != true)
                {
                    element.hide();
                    img.src = "Images/expand_"+image+".gif";
                }
            }

            function showAllLinks()
            {
                // notify the user if this evidence record is not linked
                if (domainLinks.length == 0)
                {
                    alert("This record is not linked.");
                    return;
                }

                // display domain sections
                for (var i = 0; i < domainLinks.length; i++)
                {
                    toggleStandards("domains"+domainLinks[i], document.getElementById("domainImg"+domainLinks[i]), true);
                }

                if (elementLinks.length > 0)
                {
                    // start fetching element sections once standard sections are loaded
                    jQuery(document).unbind("ajaxStop");
                    jQuery(document).bind("ajaxStop", function(){
                        jQuery(document).unbind("ajaxStop");

                        if (complianceLinks.length > 0)
                        {
                            // display compliance sections once elements loaded
                            jQuery(document).bind("ajaxStop", function(){
                                jQuery(document).unbind("ajaxStop");
                                jQuery(document).bind("ajaxStop", function(){
                                    hideLoadPopup();
                                });

                                for (i = 0; i < complianceLinks.length; i++)
                                {
                                    toggleStandards("compliance"+complianceLinks[i], document.getElementById("complianceImg"+complianceLinks[i]), true);
                                }
                                hideLoadPopup();
                            });
                        }
                        else
                        {
                            jQuery(document).bind("ajaxStop", function(){
                                hideLoadPopup();
                            });
                        }

                        for (i = 0; i < elementLinks.length; i++)
                        {
                            toggleStandards("elements"+elementLinks[i], document.getElementById("elementImg"+elementLinks[i]), true);
                        }
                    });
                }

                // display standard sections
                for (var i = 0; i < domains.length; i++)
                {
                    if (domains[i]["linked"] == true)
                    {
                        toggleStandards("standards"+i, document.getElementById("standardImg"+i), true);
                    }
                }
            }
            jQuery(document).ajaxStart(function(){
                displayLoadPopup();
            });
            jQuery(document).ajaxStop(function(){
                hideLoadPopup();
            });
        </script>
        <style>
            div.standards_set
            {
                font-weight: bold;
                color: #fff;
                background-color: #6e94b7;
                border: 1px solid #fff;
                padding: 3px;
                vertical-align: top;
            }
            div.standards_domain
            {
                font-weight: bold;
                background-color: #d2e4fc;
                border-bottom: 1px solid #fff;
                padding: 3px;
                vertical-align: top;
            }
            div.standards_standard
            {
                background-color: #d2e4fc;
                border-bottom: 1px solid #fff;
                padding: 3px;
            }
            div.standards_element
            {
                background-color: #e3efff;
                border-bottom: 1px solid #fff;
                padding: 3px;
            }
            div.standards_compliance
            {
                background-color :#f1f7ff;
                border-bottom: 1px solid #fff;
                padding: 3px;
            }
            .prop
            {
                height: 20px;
                float: right;
                width: 1px;
            }
            .clear
            {
                clear: both;
                height: 1px;
                overflow: hidden;
            }
        </style>
    ';

    $PDOParamsArray = array();

    $sql = 'SELECT
                standards_main.recordid as stn_id, standards_main.stn_descr, standards_main.stn_ourref, standards_main.stn_name,
                standards_main.stn_handler, standards_main.stn_domain, standards_main.stn_set,
                standards_main.stn_version, standards_main.stn_organisation, standards_main.stn_assessment,
                code_domain.cod_descr as domain_descr,
                code_set.cod_descr as set_descr,
                css.cod_colour';

    if ($lib['recordid'])
    {
        $sql .= ',
                stn_link.link_recordid, stn_link.link_notes';
    }

    $sql .= '
            FROM
                standards_main
            LEFT JOIN
                code_types code_domain ON standards_main.stn_domain = code_domain.cod_code AND code_domain.cod_type = \'STNDOM\'
            LEFT JOIN
                code_types code_set ON standards_main.stn_set = code_set.cod_code AND code_set.cod_type = \'STNSET\'
            LEFT JOIN
                code_types code_version ON standards_main.stn_version = code_set.cod_code AND code_set.cod_type = \'STNVER\'
            LEFT JOIN
                code_types code_organisation ON standards_main.stn_organisation = code_set.cod_code AND code_set.cod_type = \'ORG\'
            LEFT JOIN
                code_types code_assessment ON standards_main.stn_assessment = code_set.cod_code AND code_set.cod_type = \'STNASS\'
            LEFT JOIN
                standards_status ss ON ss.stn_id = standards_main.recordid
            LEFT JOIN
                code_stn_status css ON css.code = ss.stn_status';

    if ($lib['recordid'])
    {
        $sql .= '
            LEFT JOIN
                link_library stn_link ON stn_link.stn_id = standards_main.recordid AND stn_link.lib_id = :lib_id AND stn_link.ele_id IS NULL AND stn_link.pro_id IS NULL';

        $PDOParamsArray['lib_id'] = $lib['recordid'];
    }

    $permWhere = MakeSecurityWhereClause("", "STN", $_SESSION["initials"]);

    if ($permWhere)
    {
        $where[] = $permWhere;
    }

    if (!empty($where))
    {
        $sql .= ' WHERE ' . implode(' AND ', $where);
    }

    $sql .= '
            ORDER BY
                code_set.cod_listorder, set_descr,
                code_version.cod_listorder, standards_main.stn_version,
                code_organisation.cod_listorder, standards_main.stn_organisation,
                code_assessment.cod_listorder, standards_main.stn_assessment,
                code_domain.cod_listorder, domain_descr,
                standards_main.stn_listorder, standards_main.stn_ourref';

    $standards = PDO_fetch_all($sql, $PDOParamsArray);
    $domainSection = 0;
    $standardSection = 0;

    $domains = array();
    $elementLinks = getElementLinks($lib);
    $complianceLinks = getComplianceLinks($lib);

    foreach ($standards as $standard)
    {
        $standard = array_map('htmlentities', $standard);

        $set = array(
            'stn_set' => $standard['stn_set'],
            'stn_version' => $standard['stn_version'],
            'stn_organisation' => $standard['stn_organisation'],
            'stn_assessment' => $standard['stn_assessment']
        );

        if ($set != $previousSet)
        {
            if ($previousSet)
            {
                // close the previous set's domain, standards and elements divs
                $section .= '
                    </div>
                </div>';
            }

            // output new set header section
            $section .= '
                <div class="standards_set">
                    <img id="domainImg'.$domainSection.'" src="Images/expand_titlebg.gif" alt="+" border="0" onclick="toggleStandards(\'domains'.$domainSection.'\', this)" />
                    <span style="vertical-align:top">' .
                    ($standard['set_descr'] ? $standard['set_descr'] : $standard['stn_set']) .
                    ($standard['stn_version'] ? ' (Version: ' . $standard['stn_version'] . ')' : '') .
                    ($standard['stn_organisation'] ? ' (' . $standard['stn_organisation'] . ')' : '') .
                    ($standard['stn_assessment'] ? ' (' . $standard['stn_assessment'] . ')' : '') .
                    '</span>
                </div>
                <div id="domains'.$domainSection.'" style="display:none;">';
            $domainSection++;
        }

        if ($set != $previousSet || $standard['stn_domain'] !== $previousDomain)
        {
            // collect domain info for expanding standards section
            $domains[] = array(
                'set' => $standard['stn_set'],
                'version' => $standard['stn_version'],
                'organisation' => $standard['stn_organisation'],
                'assessment' => $standard['stn_assessment'],
                'domain' => $standard['stn_domain'],
                'linked' => 'false',
            );

            if ($previousDomain && ($set == $previousSet))
            {
                // close the previous domain's standards div
                $section .= '
                    </div>';
            }

            // output new domain header section
            $section .= '
                    <div class="standards_domain">
                        <div class="prop"></div>
                        <div style="float:left; width:2%">&nbsp;</div>
                        <div style="float:left; width:2%">
                            <img id="standardImg'.$standardSection.'" src="Images/expand_windowbg.gif" alt="+" border="0" onclick="toggleStandards(\'standards'.$standardSection.'\', this)" />
                        </div>
                        <div style="float:left; vertical-align:top">&nbsp;' . ($standard['domain_descr'] ? $standard['domain_descr'] : $standard['stn_domain']) . '</div>
                        <div class="clear"></div>
                    </div>
                    <div id="standards'.$standardSection.'" style="display:none;">';
            $standardSection++;
        }

        // add any of this domain's additional standard links to this evidence to the form
        if ($standard['link_recordid'])
        {
            $section .= '
                        <input type="hidden" name="standard_link_'.$standard['stn_id'].'" value="Y" />
                        <input type="hidden" name="standard_notes_'.$standard['stn_id'].'" value="'.htmlspecialchars($standard['link_notes']).'" />';

            // note that this domain/standard conatins standards linked to this evidence record for use with "Show links" functionality
            $domains[count($domains)-1]['linked'] = 'true';

            if (empty($domainLinks) || !in_array($domainSection, $domainLinks))
            {
                $domainLinks[] = $domainSection - 1;
            }
        }

        // add any of this standard's additional element links to this evidence to the form
        $additionalElementLinks = outputAdditionalElementLinks($elementLinks, $standard['stn_id']);
        if ($additionalElementLinks != '')
        {
            $section .= $additionalElementLinks;

            // also note that this standard contains nested links for use with "Show links" functionality
            $domains[count($domains)-1]['linked'] = 'true';

            if (empty($domainLinks) || !in_array($domainSection, $domainLinks))
            {
                $domainLinks[] = $domainSection - 1;
            }
        }

        // add any of this standard's additional compliance links to this evidence to the form
        $additionalComplianceLinks = outputAdditionalComplianceLinks($complianceLinks, $standard['stn_id']);

        if ($additionalComplianceLinks != '')
        {
            $section .= $additionalComplianceLinks;

            // also note that this standard contains nested links for use with "Show links" functionality
            $domains[count($domains)-1]['linked'] = 'true';

            if (empty($domainLinks) || !in_array($domainSection, $domainLinks))
            {
                $domainLinks[] = $domainSection - 1;
            }
        }

        $previousDomain = $standard['stn_domain'];
        $previousSet = $set;
    }

    $section .= '
                    </div>
                </div>
    </li>';

    // create javascript array to hold domain info used when expanding standards sections
    $javascript = '
        var domains = new Array();';
    for ($i = 0; $i < count($domains); $i++)
    {
        $javascript .= '
        domains['.$i.'] = {
            "set": "'.htmlspecialchars($domains[$i]['set']).'",
            "version": "'.htmlspecialchars($domains[$i]['version']).'",
            "organisation": "'.htmlspecialchars($domains[$i]['organisation']).'",
            "assessment": "'.htmlspecialchars($domains[$i]['assessment']).'",
            "domain": "'.htmlspecialchars($domains[$i]['domain']).'",
            "linked": '.$domains[$i]['linked'].'
        };';
    }

    // we also need to store all link info for use with "show links" functionality
    if (is_array($domainLinks))
    {
        $domainLinksIds = '"' . implode('","', $domainLinks) . '"';
    }

    foreach ($elementLinks as $element)
    {
        $elementLinksIds .= '"'.$element['stn_id'].'",';
    }

    foreach ($complianceLinks as $compliance)
    {
        $complianceLinksIds .= '"'.$compliance['ele_id'].'",';

        if (\UnicodeString::strpos($elementLinksIds, '"'.$compliance['stn_id'].'"') === false)
        {
            $elementLinksIds .= '"'.$compliance['stn_id'].'",';
        }
    }

    $elementLinksIds = \UnicodeString::substr($elementLinksIds, 0, -1);
    $complianceLinksIds = \UnicodeString::substr($complianceLinksIds, 0, -1);

    $javascript .= '
        var domainLinks = new Array('.$domainLinksIds.');
        var elementLinks = new Array('.$elementLinksIds.');
        var complianceLinks = new Array('.$complianceLinksIds.');';

    $JSFunctions[] = $javascript;

    echo $section;
}

/**
* Retrieves all element links to this evidence record.
*
* Used to include this information on the evidence form so link info is not lost if the
* relevant element sections haven't been loaded via AJAX.
*
* @param  string  $lib['recordid']    The ID for this evidence.
* @param  string  $lib['element_id']  The ID for the linked element.
*
* @return array   $links              The element links for this evidence record.
*/
function getElementLinks($lib)
{
    $links = array();

    if ($lib['recordid'])
    {
        $sql = 'SELECT stn_id, ele_id, link_notes
                FROM link_library
                WHERE lib_id = :lib_id
                AND pro_id IS NULL
                AND ele_id IS NOT NULL';

        $PDOParamsArray = array('lib_id' => $lib['recordid']);
        $links = PDO_fetch_all($sql, $PDOParamsArray);
    }

    return $links;
}

/**
* Retrieves all compliance links to this evidence record.
*
* Used to include this information on the evidence form so link info is not lost if the
* relevant compliance sections haven't been loaded via AJAX.
*
* @param  string  $lib['recordid']    The ID for this evidence.
* @param  string  $lib['prompt_id']   The ID for the linked compliance.
*
* @return array   $links              The compliance links for this evidence record.
*/
function getComplianceLinks($lib)
{
    $links = array();

    if ($lib['recordid'])
    {
        $sql = 'SELECT stn_id, ele_id, pro_id, link_notes
                FROM link_library
                WHERE lib_id = :lib_id
                AND pro_id IS NOT NULL';

        $PDOParamsArray = array('lib_id' => $lib['recordid']);
        $links = PDO_fetch_all($sql, $PDOParamsArray);
    }

    return $links;
}

/**
* Formats a given set of element links to this evidence record as hidden form inputs.
*
* Used to include these values on the additional links form before the
* element/compliance section is loaded via AJAX, to prevent these links being lost on saving.
*
* @param  array   $elementLinks  An array of all the element links to this evidence record.
* @param  string  $stn_id        The ID of the current standard whose elements we're interested in.
*
* @return array   $section       The HTML for the hidden form inputs.
*/
function outputAdditionalElementLinks($elementLinks, $stn_id)
{
    foreach ($elementLinks as $elementLink)
    {
        if ($elementLink['stn_id'] == $stn_id)
        {
            $section .= '
                        <input type="hidden" name="element_link_'.$elementLink['ele_id'].'" value="Y" />
                        <input type="hidden" name="element_notes_'.$elementLink['ele_id'].'" value="'.htmlspecialchars($elementLink['link_notes']).'" />';
        }
    }
    return $section;
}

/**
* Formats a given set of compliance links to this evidence record as hidden form inputs.
*
* Used to include these values on the additional links form before the
* element/compliance section is loaded via AJAX, to prevent these links being lost on saving.
*
* @param  array   $complianceLinks  An array of all the compliance links to this evidence record.
* @param  string  $stn_id           The ID of the current standard whose compliances we're interested in.
*
* @return array   $section          The HTML for the hidden form inputs.
*/
function outputAdditionalComplianceLinks($complianceLinks, $stn_id)
{
    foreach ($complianceLinks as $complianceLink)
    {
        if ($complianceLink['stn_id'] == $stn_id)
        {
            $section .= '
                    <input type="hidden" name="compliance_link_'.$complianceLink['pro_id'].'" value="Y" />
                    <input type="hidden" name="compliance_notes_'.$complianceLink['pro_id'].'" value="'.htmlspecialchars($complianceLink['link_notes']).'" />';
        }
    }

    return $section;
}

/**
* Retrives standards records for a given domain and outputs as HTML (via echo).
*
* Used in the additional links tree to retrieve standards sections via AJAX
* to prevent a large amount of HTML on load significantly slowing down page rendering time
* when there is a large amount of standards in the system.
*
* @param string $_GET['recordid']       ID of a main evidence form (unlinked) that needs to be loaded.
* @param string $_GET['link_recordid']  ID of the link record if this is a linked piece of evidence.
* @param string $_GET['set']            The 'set' value for this group of standards.
* @param string $_GET['version']        The 'version' value for this group of standards.
* @param string $_GET['organisation']   The 'organisation' value for this group of standards.
* @param string $_GET['assessment']     The 'assessment' value for this group of standards.
* @param string $_GET['domain']         The 'domain' value for this group of standards.
* @param string $_SESSION["initials"]   The user initials, used to retrieve the security where clause.
*/
function getStandards()
{
    global $scripturl;

    require_once 'Source/libs/FormClasses.php';

    if (GetParm("LIB_PERMS") == "LIB_READ")
    {
        $formType = 'ReadOnly';
    }

    $linkBox = new FormField($formType);
    $linkNotes = new FormField($formType);
    $section = '<input type="hidden" name="fromAjax">'; // used as a place holder to state that section has been retrieved via AJAX

    $sql = 'SELECT standards_main.recordid AS stn_id, standards_main.stn_ourref, standards_main.stn_name';

    if ($_GET['recordid'] || $_GET['link_recordid'])
    {
        $sql .= ', ll.link_recordid, ll.link_notes';
    }

    $sql .= ' FROM standards_main ';

    if ($_GET['recordid'] || $_GET['link_recordid'])
    {
        $sql .= 'LEFT JOIN link_library ll ON ' . ($_GET['recordid'] ? 'll.lib_id = :recordid' : 'll.link_recordid = :link_recordid') . ' AND ll.stn_id = standards_main.recordid AND ll.ele_id IS NULL AND ll.pro_id IS NULL';
        $_GET['recordid'] ? $PDOParamsArray['recordid'] = $_GET['recordid'] : $PDOParamsArray['link_recordid'] = $_GET['link_recordid'];
    }

    // add empty strings to fields below incase the values are NULL
    $where[] = 'standards_main.stn_set + \'\' = :set AND standards_main.stn_version + \'\' = :version AND standards_main.stn_organisation + \'\' = :organisation AND standards_main.stn_assessment + \'\' = :assessment AND standards_main.stn_domain + \'\' = :domain';

    $PDOParamsArray['set'] = $_GET['set'];
    $PDOParamsArray['version'] = $_GET['version'];
    $PDOParamsArray['organisation'] = $_GET['organisation'];
    $PDOParamsArray['assessment'] = $_GET['assessment'];
    $PDOParamsArray['domain'] = $_GET['domain'];

    $permWhere = MakeSecurityWhereClause("", "STN", $_SESSION["initials"]);

    if ($permWhere)
    {
        $where[] = $permWhere;
    }

    if (!empty($where))
    {
        $sql .= ' WHERE ' . implode(' AND ', $where);
    }

    $sql .= ' ORDER BY standards_main.stn_listorder, standards_main.stn_ourref';

    $standards = PDO_fetch_all($sql, $PDOParamsArray);

    // if we're looking at a linked record we need main recordid so we can tweak the display link info in the additional links tree, as it's duplicated on the evidence form itself.
    if ($_GET['main_module'] == 'STN')
    {
        $standardid = PDO_fetch('SELECT stn_id FROM link_library WHERE link_recordid = :link_recordid', array('link_recordid' => $_GET['link_recordid']), PDO::FETCH_COLUMN);
    }

    $elementLinks = getElementLinks(array('recordid' => $_GET['recordid']));
    $complianceLinks = getComplianceLinks(array('recordid' => $_GET['recordid']));

    foreach ($standards as $standard)
    {
        // output standard section
        $linkBoxValue = $standard['link_recordid'] != '';

        if ($linkBoxValue)
        {
            if ($standardid == $standard['stn_id'])
            {
                // link notes aren't editable, must be edited on main evidence form
                $linkNotes->MakeTextAreaFieldWithEdit('standard_notes_'.$standard['stn_id'], 5, 80, null, $standard['link_notes']);
                $linkNotes->Field .= '<div style="margin-top:10px; font-weight:bold; font-style:italic">Cannot edit this link here. Manage link from evidence record itself.</div>';
            }
            else
            {
                $linkNotes->MakeTextAreaFieldWithEdit('standard_notes_'.$standard['stn_id'], 5, 80, null, $standard['link_notes'], true, true, true, 'jQuery(\'#standard_link_'.$standard['stn_id'].'\').attr(\'disabled\', false)');
            }

            $linkBox->MakeDivCheckbox('standard_link_'.$standard['stn_id'], 'standard_notes_wrapper_'.$standard['stn_id'], $linkBoxValue, 'standard_link_'.$standard['stn_id'], false, 'disabled="disabled"');
        }
        else
        {
            $linkNotes->MakeTextAreaField('standard_notes_'.$standard['stn_id'], 5, 80, null, null);
            $linkBox->MakeDivCheckbox('standard_link_'.$standard['stn_id'], 'standard_notes_wrapper_'.$standard['stn_id'], $linkBoxValue);
        }

        $section .= '
                        <div id="standard_notes_'.$standard['stn_id'].'_row" class="standards_standard">
                            <div class="prop"></div>
                            <div style="float:left; width:5%">&nbsp;</div>
                            <div style="float:left; width:2%">
                                <img id="elementImg'.$standard['stn_id'].'" src="Images/expand_windowbg.gif" alt="+" border="0" onclick="toggleStandards(\'elements'.$standard['stn_id'].'\', this)" />
                            </div>
                            <div style="float:left; width:8%; font-weight:bold">&nbsp;
                                Link&nbsp;' . $linkBox->GetField() . '&nbsp;
                            </div>
                            <div style="float:left; margin-top:5px; width:6%; word-wrap:break-word;">&nbsp;<a href="'.$scripturl.'?action=standard&amp;recordid='.$standard['stn_id'].'&amp;token='.CSRFGuard::getCurrentToken().'">' . $standard['stn_ourref'] . '</a></div>
                            <div style="float:left; margin-top:5px;">&nbsp;<a href="'.$scripturl.'?action=standard&amp;recordid='.$standard['stn_id'].'&amp;token='.CSRFGuard::getCurrentToken().'">' . $standard['stn_name'] . '</a></div>
                            <div class="clear"></div>
                            <div id="standard_notes_wrapper_'.$standard['stn_id'].'" style="padding:5px;' . ($standard['link_recordid'] ? '' : ' display:none;') . '">
                                <div class="prop"></div>
                                <div style="float:left; width:14%">&nbsp;</div>
                                <div style="float:left; vertical-align:top; font-weight:bold">Link notes&nbsp;&nbsp;&nbsp;</div>
                                <div style="float:left; width:60%">' . $linkNotes->GetField() . '</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div id="elements'.$standard['stn_id'].'" style="display:none;">';

        // add any of this standard's additional element links to this evidence to the form
        $section .= outputAdditionalElementLinks($elementLinks, $standard['stn_id']);

        // add any of this standard's additional compliance links to this evidence to the form
        $section .= outputAdditionalComplianceLinks($complianceLinks, $standard['stn_id']);

        $section .= '
                        </div>';
    }

    echo $section;
}

/**
* Retrieves element/compliance records for a given standard and outputs as HTML (via echo).
*
* Used in the additional links tree to retrieve element/compliance sections via AJAX
* to prevent a large amount of HTML on load significantly slowing down page rendering time.
*
* @param string $_GET['lib_id']        The ID for this evidence record.
* @param string $_GET['stn_id']        The ID for the standard whose elements/compliances we're interested in.
* @param string $_GET['main_module']   The module this evidence is to be linked to (if any).
* @param string $_GET['link_recordid'] The ID of the link record if this is a linked piece of evidence.
*/
function getElements()
{
    global $scripturl;

    $scripturl = Sanitize::SanitizeURL($scripturl);

    require_once 'Source/libs/FormClasses.php';

    if (GetParm("LIB_PERMS") == "LIB_READ")
    {
        $formType = 'ReadOnly';
    }

    $linkBox = new FormField($formType);
    $linkNotes = new FormField($formType);
    $section = '<input type="hidden" name="fromAjax">'; // used as a place holder to state that section has been retrieved via AJAX

    $sql = 'SELECT
                se.recordid AS ele_id, se.ele_code, se.ele_descr, se.ele_listorder,
                sp.recordid AS pro_id, sp.pro_code, sp.pro_descr, sp.pro_level';

    if ($_GET['lib_id'])
    {
        $sql .= ',
                ele_link.link_recordid AS ele_link_recordid, ele_link.link_notes AS ele_link_notes,
                pro_link.link_recordid AS pro_link_recordid, pro_link.link_notes AS pro_link_notes';
    }

    $sql .= '
            FROM
                standard_elements se
            LEFT JOIN
                standard_prompts sp ON sp.pro_ele_id = se.recordid';

    if ($_GET['lib_id'])
    {
        $sql .= '
            LEFT JOIN
                link_library ele_link ON ele_link.ele_id = se.recordid AND ele_link.lib_id = :lib_id1 AND ele_link.pro_id IS NULL
            LEFT JOIN
                link_library pro_link ON pro_link.pro_id = sp.recordid AND pro_link.lib_id = :lib_id2';

        $PDOParamsArray['lib_id1'] = $_GET['lib_id'];
        $PDOParamsArray['lib_id2'] = $_GET['lib_id'];
    }

    $sql .= '
            WHERE
                se.ele_stn_id = :stn_id
            ORDER BY
                se.ele_listorder, se.ele_code, sp.pro_level, sp.pro_code';

    $PDOParamsArray['stn_id'] = $_GET['stn_id'];
    $result = PDO_fetch_all($sql, $PDOParamsArray);

    // if we're looking at a linked record we need main recordid so we can tweak the display link info in the additional links tree, as it's duplicated on the evidence form itself.
    if ($_GET['main_module'] == 'ELE')
    {
        $elementid = PDO_fetch('SELECT ele_id FROM link_library WHERE link_recordid = :link_recordid', array('link_recordid' => $_GET['link_recordid']), PDO::FETCH_COLUMN);
    }

    if ($_GET['main_module'] == 'PRO')
    {
        $promptid = PDO_fetch('SELECT pro_id FROM link_library WHERE link_recordid = :link_recordid', array('link_recordid' => $_GET['link_recordid']), PDO::FETCH_COLUMN);
    }

    foreach ($result as $row)
    {
        $row = array_map('htmlentities', $row);

        if ($row['ele_id'] != $previousElement)
        {
            if ($previousElement)
            {
                // close the previous element's compliance divs
                $section .= '
                            </div>';
            }

            // output element section
            $linkBoxValue = $row['ele_link_recordid'] != '';

            if ($linkBoxValue)
            {
                if ($elementid == $row['ele_id'])
                {
                    // link notes aren't editable, must be edited on main evidence form
                    $linkNotes->MakeTextAreaFieldWithEdit('element_notes_'.$row['ele_id'], 5, 80, null, $row['ele_link_notes']);
                    $linkNotes->Field .= '<div style="margin-top:10px; font-weight:bold; font-style:italic">Cannot edit this link here. Manage link from evidence record itself.</div>';
                }
                else
                {
                    $linkNotes->MakeTextAreaFieldWithEdit('element_notes_'.$row['ele_id'], 5, 80, null, $row['ele_link_notes'], true, true, true, 'jQuery(\'#element_link_'.$row['ele_id'].'\').attr(\'disabled\', false)');
                }

                $linkBox->MakeDivCheckbox('element_link_'.$row['ele_id'], 'element_notes_wrapper_'.$row['ele_id'], $linkBoxValue, 'element_link_'.$row['ele_id'], false, 'disabled="disabled"');
            }
            else
            {
                $linkNotes->MakeTextAreaField('element_notes_'.$row['ele_id'], 5, 80, null, null);
                $linkBox->MakeDivCheckbox('element_link_'.$row['ele_id'], 'element_notes_wrapper_'.$row['ele_id'], $linkBoxValue);
            }

            $section .= '
                            <div id="element_notes_'.$row['ele_id'].'_row" class="standards_element">
                                <div class="prop"></div>
                                <div style="float:left; width:8%">&nbsp;</div>
                                <div style="float:left; width:2%">
                                    <img id="complianceImg'.$row['ele_id'].'" style="float:left" src="Images/expand_windowbg2.gif" alt="+" border="0" onclick="toggleStandards(\'compliance'.$row['ele_id'].'\', this)" />
                                </div>
                                <div style="float:left; width:8%; font-weight:bold">&nbsp;
                                    Link&nbsp;' . $linkBox->GetField() . '&nbsp;
                                </div>
                                <div style="float:left; width:4%;"><a href="' . $scripturl . '?action=element&amp;recordid=' . $row['ele_id'] . '">' . $row['ele_listorder'] . '</a></div>
                                <div style="float:left; width:70%;"><a href="' . $scripturl . '?action=element&amp;recordid=' . $row['ele_id'] . '">' . $row['ele_descr'] . '</a></div>
                                <div style="float:left"><a href="' . $scripturl . '?action=element&amp;recordid=' . $row['ele_id'] . '">&nbsp;' . $row['ele_code'] . '</a></div>
                                <div class="clear"></div>
                                <div id="element_notes_wrapper_'.$row['ele_id'].'" style="padding:5px;' . ($row['ele_link_recordid'] ? '' : ' display:none;') . '">
                                    <div class="prop"></div>
                                    <div style="float:left; width:22%">&nbsp;</div>
                                    <div style="float:left; vertical-align:top; font-weight:bold">Link notes&nbsp;&nbsp;&nbsp;</div>
                                    <div style="float:left; width:60%">' . $linkNotes->GetField() . '</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div id="compliance'.$row['ele_id'].'" style="display:none;">';
        }

        if ($row['pro_id'])
        {
            // output compliance section
            $linkBoxValue = $row['pro_link_recordid'] != '';

            if ($linkBoxValue)
            {
                if ($promptid == $row['pro_id'])
                {
                    // link notes aren't editable, must be edited on main evidence form
                    $linkNotes->MakeTextAreaFieldWithEdit('compliance_notes_'.$row['pro_id'], 5, 80, null, $row['pro_link_notes']);
                    $linkNotes->Field .= '<div style="margin-top:10px; font-weight:bold; font-style:italic">Cannot edit this link here. Manage link from evidence record itself.</div>';
                }
                else
                {
                    $linkNotes->MakeTextAreaFieldWithEdit('compliance_notes_'.$row['pro_id'], 5, 80, null, $row['pro_link_notes'], true, true, true, 'jQuery(\'#compliance_link_'.$row['pro_id'].'\').attr(\'disabled\', false)');
                }

                $linkBox->MakeDivCheckbox('compliance_link_'.$row['pro_id'], 'compliance_notes_wrapper_'.$row['pro_id'], $linkBoxValue, 'compliance_link_'.$row['pro_id'], false, 'disabled="disabled"');
            }
            else
            {
                $linkNotes->MakeTextAreaField('compliance_notes_'.$row['pro_id'], 5, 80, null, null);
                $linkBox->MakeDivCheckbox('compliance_link_'.$row['pro_id'], 'compliance_notes_wrapper_'.$row['pro_id'], $linkBoxValue);
            }

            $section .= '
                                <div id="compliance_notes_'.$row['pro_id'].'_row" class="standards_compliance">
                                    <div class="prop"></div>
                                    <div style="float:left; width:12%">&nbsp;</div>
                                    <div style="float:left; width:8%; font-weight:bold">&nbsp;
                                        Link&nbsp;' . $linkBox->GetField() . '&nbsp;
                                    </div>
                                    <div style="float:left; width:4%"><a href="' . $scripturl . '?action=prompt&amp;recordid=' . $row['pro_id'] . '">' . $row['pro_level'] . '</a></div>
                                    <div style="float:left; width:70%"><a href="' . $scripturl . '?action=prompt&amp;recordid=' . $row['pro_id'] . '">' . $row['pro_descr'] . '</a></div>
                                    <div style="float:left"><a href="' . $scripturl . '?action=prompt&amp;recordid=' . $row['pro_id'] . '">' . $row['pro_code'] . '</a></div>
                                    <div class="clear"></div>
                                    <div id="compliance_notes_wrapper_'.$row['pro_id'].'" style="padding:5px;' . ($row['pro_link_recordid'] ? '' : ' display:none;') . '">
                                        <div class="prop"></div>
                                        <div style="float:left; width:24%">&nbsp;</div>
                                        <div style="float:left; vertical-align:top; font-weight:bold">Link notes&nbsp;&nbsp;&nbsp;</div>
                                        <div style="float:left; width:60%">' . $linkNotes->GetField() . '</div>
                                        <div class="clear"></div>
                                    </div>
                                </div>';
        }

        $previousElement = $row['ele_id'];
    }

    if ($section == '')
    {
        $section = '<div class="standards_element">No elements found.</div>';
    }

    echo $section;
}

?>