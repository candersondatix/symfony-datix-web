<?php

function ShowPromptForm( $pro = "", $FormAction = 'New' )
{
	global $scripturl, $dtxtitle, $TitleWidth, $JSFunctions;

    $Perms = GetParm("STN_PERMS");
    $ReadOnly = ($Perms == "STN_READ_ONLY");

    CheckForRecordLocks('PRO', $pro, $FormAction, $sLockMessage);

    if ($Perms == "STN_FULL" && $FormAction != 'Locked')
    {
        $Editable = true;
    }
    else
    {
        $Editable = false;
    }

    if ($_GET["print"] == 1)
    {
		$FormAction = "Print";
    }

    if ($ReadOnly)
    {
        $FormAction = 'ReadOnly';
    }

    // Load form settings
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'PRO', 'link_type' => 'compliance', 'level' => 2, 'form_type' => $FormType));
    $FormDesign->LoadFormDesignIntoGlobals();
    $_SESSION['LASTUSEDFORMDESIGN'] = $FormDesign->GetFilename();

	$TitleWidth = 20;

    $mode = $Perms == 'STN_READ_ONLY' ? 'Print' : $FormAction;
    $statusFieldObj = Forms_SelectFieldFactory::createSelectField('pro_status', 'PRO', $pro["pro_status"], $mode);
    $statusFieldObj->setOnChangeExtra('initDropdown(jQuery(\'#pro_status_title\'));updatePromptScore(jQuery(\'#pro_status_title\').data(\'datixselect\').field.val(), document.getElementById(\'pro_max_score\'), document.getElementById(\'pro_act_score\'));');

    $statusFieldArray = array(
        "Title" => GetFieldLabel("pro_status", "Compliant?"),
        "Name" => "pro_status",
        "Type" => "formfield",
        "ReadOnly" => true,
        "FormField" => $statusFieldObj
    );

    include('Source/generic_modules/PRO/BasicForm2.php');

    $dtxtitle = $GLOBALS['FormTitle'];

    getPageTitleHTML(array(
        'title' => $GLOBALS['FormTitle'],
        'subtitle' => $GLOBALS['FormTitleDescr'],
        'module' => 'STN'
    ));

    $buttonGroup = new ButtonGroup();

    if ($FormAction != "Print")
    {
        if ($FormAction == "Search")
        {
            $buttonGroup->AddButton(array('id' => 'btnSearch', 'name' => 'btnSearch', 'label' => _tk('btn_search'), 'onclick' => 'document.frmPrompt.rbWhat.value = \'Search\';submitClicked = true;document.frmPrompt.submit();', 'action' => 'SEARCH'));
        }

        if ($Editable)
        {
            $buttonGroup->AddButton(array('id' => 'btnSavePro', 'name' => 'btnSavePro', 'label' => _tk('btn_save'), 'onclick' => 'submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()){document.frmPrompt.submit();}', 'action' => 'SAVE'));
        }

        $buttonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => getConfirmCancelJavascript('frmPrompt'), 'action' => 'CANCEL'));
    }

    $PromptTable = New FormTable($FormAction, 'PRO', $FormDesign);
    $PromptTable->MakeForm($FormArray, $pro, 'PRO');

    if ($FormAction != "Print")
    {
        GetSideMenuHTML(array(
            'module' => 'PRO',
            'table' => $PromptTable,
            'buttons' => $buttonGroup,
            'menu_array' => array(
                array('label' => 'Back to element', 'link' => 'action=element&recordid='.$pro['pro_ele_id']),
                array('label' => 'Back to standard', 'link' => 'action=standard&recordid='.$pro['ele_stn_id'])
            )
        ));

        template_header();
    }
    else
    {
        template_header_nomenu();
    }

	template_header();

    if ($FormAction != "Search")
    {
        echo '<script language="Javascript">AlertAfterChange = true;</script>';
    }

    $JSFunctions[] = 'var submitClicked = false;';
    $JSFunctions[] = MakeJavaScriptValidation('PRO', $FormDesign);
?>
    <script language="JavaScript" type="text/javascript">
    function updatePromptScore(statusValue, eleMaxScore, eleActScore)
    {
        if (statusValue != 'PART')
        {
            eleActScore.value = 0;

            if (statusValue == 'YES')
            {
                if (eleMaxScore == '')
                {
                    eleActScore.value = 100;
                }
                else
                {
                    eleActScore.value = eleMaxScore.value;
                }
            }
            else if (statusValue == 'NO')
            {
                eleActScore.value = 0;
            }
            else if (statusValue == 'N/A')
            {
                eleActScore.value = '';
            }

            setChanged('pro_act_score');
        }
    }
    </script>
    <form method="post" id="frmPrompt" name="frmPrompt" action="<?= $scripturl . "?action=saveprompt" ?>">
        <input type="hidden" name="form_type" value="<?= htmlspecialchars($FormAction) ?>">
        <input type="hidden" name="recordid" value="<?= Sanitize::SanitizeInt($pro["recordid"]) ?>">
        <input type="hidden" name="updateid" value="<?= htmlspecialchars($pro["updateid"]) ?>">
        <input type="hidden" name="pro_ele_id" value="<?= Sanitize::SanitizeInt($pro["pro_ele_id"]) ?>">
        <input type="hidden" name="ele_stn_id" value="<?= Sanitize::SanitizeInt($pro["ele_stn_id"]) ?>">
        <input type="hidden" name="rbWhat" value="Save">
    <?php

    if ($ReadOnly)
    {
        echo '<input type="hidden" name="pro_max_score" id="pro_max_score" value="' . htmlspecialchars($pro["pro_max_score"]) . '">';
    }

    if ($sLockMessage)
    {
        echo '
        <div class="lock_message">
            <div id="LockMessage">'.$sLockMessage.'</div>
            <div id="userLockMessage"></div>
        </div>';
    }

	// Display the sections
    $PromptTable->MakeTable();
    echo $PromptTable->GetFormTable();

    echo $buttonGroup->getHTML();

    echo '
    </form>';

    if ($FormAction != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $PromptTable);
    }

    footer();

    obExit();
}

function ListEvidence($pro, $FormAction)
{
    require_once 'Source/standards/EvidenceForm.php';
    ListLinkedEvidence('PRO', $pro['recordid'], $FormAction);
}

function ElementDetailsSection($pro, $FormAction)
{
    $sql = "
        SELECT
            recordid as pro_stn_id,
            ele_code,
            ele_descr
        FROM
            standard_elements
        WHERE
            recordid = :ele_id
    ";

    $row = DatixDBQuery::PDO_fetch($sql, array('ele_id' => $pro['pro_ele_id']));

    if ($row)
    {
        $pro["pro_stn_id"] = $row["pro_stn_id"];

        echo GetDivFieldHTML('Code', Sanitize::SanitizeString($row["ele_code"]));
        echo GetDivFieldHTML('Description', Sanitize::SanitizeString($row["ele_descr"]));
    }

}

?>