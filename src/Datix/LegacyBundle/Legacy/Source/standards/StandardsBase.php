<?php
function CountLinkedRisk($stn_id = '', $ele_id = '') {

    if (!$stn_id)
        return 0;

	$sql = "SELECT count(*) as count from ra_main, link_modules l, standard_elements e";

    $Where[] = "ra_main.recordid = l.ram_id and l.ele_id = e.recordid";

    if ($stn_id)
        $Where[] = "l.stn_id = $stn_id";
    if ($ele_id)
        $Where[] = "l.ele_id = $ele_id";

    $Where = MakeSecurityWhereClause($Where, "RAM",  $_SESSION["initials"]);

	$sql .= " WHERE $Where";

    $row = DatixDBQuery::PDO_fetch($sql);
    
	return $row["count"];
}

function CountLinkedEvidence($module, $recordid) 
{
    global $ModuleDefs;
            
    $sql = 'SELECT count(*) as num
            FROM
                library_main, link_library
            WHERE
                link_library.lib_id = library_main.recordid
                and link_library.'.$ModuleDefs[$module]['FK'].' = :recordid'.$extraWhere;
                
    $Results = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));
    
    return $Results['num'];
}

?>