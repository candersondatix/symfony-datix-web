<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */

function ListSchemes()
{
    global $dtxtitle, $scripturl, $yySetLocation;

	$sql = "SELECT cod_code, cod_descr
		FROM code_types
		WHERE cod_type = 'STNSET'
		ORDER BY cod_listorder, cod_descr";

	$scheme_result = db_query($sql);

	if ($CurrentScheme = Sanitize::SanitizeString($_GET["stnset"]))
	{
		$sql = "SELECT cod_code, cod_descr
			FROM code_types
			WHERE cod_type = 'STNDOM'
			AND cod_parent = '$CurrentScheme'
			ORDER BY cod_listorder, cod_descr";
		$domain_result = db_query($sql);
	}

	if ($CurrentDomain = Sanitize::SanitizeString($_GET["stndom"]))
	{
		$sql = "SELECT recordid, stn_standard, stn_descr,
			stn_ourref, stn_name, stn_handler, stn_domain
			FROM standards_main
			WHERE stn_domain = '$CurrentDomain'";
		$standard_result = db_query($sql);
	}

    $dtxtitle = "Datix Standards";

	template_header();

	echo '<table border="0" width="100%" cellspacing="1" cellpadding="4" class="bordercolor" align="center">';

	$last_scheme = "";
	while ($scheme_row = db_fetch_array($scheme_result))
	{
		echo '
<tr>
	<td class="titlebg">
	<big><b><a href="' . $scripturl . '?action=liststandards&stnset='
		. $scheme_row["cod_code"] . '">' . $scheme_row["cod_descr"] . '</a></b></big>
	</td>
</tr>';
		if ($scheme_row["cod_code"] == $CurrentScheme)
		{
            while ($domain_row = db_fetch_array($domain_result))
            {
                echo '
<tr>
	<td class="windowbg2">
	<b><a href="' . $scripturl . '?action=liststandards&stnset=' . $CurrentScheme
        . '&stndom=' . $domain_row["cod_code"] . '">'
            . $domain_row["cod_descr"] . '</a></b>
	</td>
</tr>';
                if ($domain_row["cod_code"] == $CurrentDomain)
                {
                    while ($standard_row = db_fetch_array($standard_result))
                        echo '
<tr>
	<td class="windowbg2">
	&nbsp;&nbsp;&nbsp;&nbsp;<a href="' . $scripturl . '?action=showstandard&recordid='
                            . $standard_row["recordid"] . '">'
                            . $standard_row["stn_name"] . '</a>'
                            . '
	</td>
</tr>';
                }
            }
        }


	}
	echo '</table></td></tr></table></td></tr></table></td></tr></table>';
    footer();

	obExit();
}

?>