<?php

function ShowElementForm( $ele = "", $FormAction = 'New' )
{
	global $scripturl;
	global $dtxtitle, $TitleWidth, $JSFunctions;

    $Perms = GetParm("STN_PERMS");
    $ReadOnly = ($Perms == "STN_READ_ONLY");

    CheckForRecordLocks('ELE', $ele, $FormAction, $sLockMessage);

    if ($Perms == "STN_FULL" && $FormAction != 'Locked')
    {
        $Editable = true;
    }
    else
    {
        $Editable = false;
    }

    if ($_GET["print"] == 1)
    {
		$FormAction = "Print";
    }

    if ($ReadOnly)
    {
        $FormAction = 'ReadOnly';
    }

    // Load form settings
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'ELE', 'link_type' => 'elements', 'level' => 2, 'form_type' => $FormType));

    $_SESSION['LASTUSEDFORMDESIGN'] = $FormDesign->GetFilename();

    if ($ele["recordid"])
    {
        $PrintView = "window.open('{$scripturl}?action=element&recordid={$ele['recordid']}&print=1&token=".CSRFGuard::getCurrentToken()."')";
    }

	$TitleWidth = 20;

	include('Source/generic_modules/ELE/BasicForm2.php');

    $dtxtitle = $FormDesign->FormTitle;

    getPageTitleHTML(array(
        'title' => $FormDesign->FormTitle,
        'subtitle' => $FormDesign->FormTitleDescr,
        'module' => 'STN'
    ));

    $buttonGroup = new ButtonGroup();

    if ($FormAction != "Print")
    {
        if ($FormAction == "Search")
        {
            $buttonGroup->AddButton(array('id' => 'btnSearch', 'name' => 'btnSearch', 'label' => _tk('btn_search'), 'onclick' => 'document.frmElement.rbWhat.value = \'Search\';submitClicked = true;document.frmElement.submit();', 'action' => 'SEARCH'));
        }

        if ($Editable)
        {
            if ($FormAction == 'New')
            {
                $buttonGroup->AddButton(array('id' => 'btnSubmitEle', 'name' => 'btnSubmitEle', 'label' => 'Submit element', 'onclick' => 'submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()){document.frmElement.submit();}', 'action' => 'SAVE'));
            }
            else
            {
                $buttonGroup->AddButton(array('id' => 'btnSaveEle', 'name' => 'btnSaveEle', 'label' => _tk('btn_save'), 'onclick' => 'submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()){document.frmElement.submit();}', 'action' => 'SAVE'));
            }
        }
        $stnurl = $scripturl.'?action=standard&recordid='.$ele['ele_stn_id'];
        $buttonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'),
            'onclick' => 'if(confirm(\''.addslashes(_tk('confirm_or_cancel')).'\')){'.
                'window.location.replace(\''.$stnurl.'\')}',
            'action' => 'CANCEL'));
    }

    $ElementTable = new FormTable($FormAction, 'ELE', $FormDesign);
    $ElementTable->MakeForm($FormArray, $ele, 'ELE');

    if ($FormAction != "Print")
    {
        GetSideMenuHTML(array(
            'module' => 'ELE',
            'table' => $ElementTable,
            'buttons' => $buttonGroup,
            'menu_array' => array(
                array('label' => 'Back to standard', 'link' => 'action=standard&recordid='.$ele['ele_stn_id'])
            )
        ));

        template_header();
    }
    else
    {
        template_header_nomenu();
    }

    if ($FormAction != "Search")
    {
        echo '<script language="Javascript">AlertAfterChange = true;</script>';
    }

    $JSFunctions[] = 'var submitClicked = false;';
    $JSFunctions[] = MakeJavaScriptValidation('ELE', $FormDesign);
?>

    <form method="post" id="frmElement" name="frmElement" action="<?= $scripturl . "?action=saveelement" ?>">
        <input type="hidden" name="form_type" value="<?= htmlspecialchars($FormAction) ?>">
        <input type="hidden" name="recordid" value="<?= Sanitize::SanitizeInt($ele["recordid"]) ?>">
        <input type="hidden" name="updateid" value="<?= htmlspecialchars($ele["updateid"]) ?>">
        <input type="hidden" name="ele_stn_id" value="<?= Sanitize::SanitizeInt($ele["ele_stn_id"]) ?>">
        <input type="hidden" name="stn_set" value="<?= htmlspecialchars($ele["stn_set"]) ?>">
        <input type="hidden" name="stn_domain" value="<?= htmlspecialchars($ele["stn_domain"]) ?>">
        <input type="hidden" name="rbWhat" value="Save">

<?php

    if ($inc['error'])
    {
        echo '<div class="form_error">'._tk('form_errors').'</div>';
        echo '<div class="form_error">'.$inc['error']['message'].'</div>';
    }

    if ($sLockMessage)
    {
        echo '
        <div class="lock_message">
            <div id="LockMessage">'.$sLockMessage.'</div>
            <div id="userLockMessage"></div>
        </div>';
    }

    // Display the sections
    $ElementTable->MakeTable();
    echo $ElementTable->GetFormTable();

    echo $buttonGroup->getHTML();

    echo '
    </form>';

    if ($FormAction != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, Sanitize::SanitizeString($_GET['panel']), $ElementTable);
    }

    footer();

    obExit();
}

function ComplianceSection($ele, $FormAction)
{
    global $ListingDesigns;

    $Perms = GetParm("STN_PERMS");
	$AdminUser = ($Perms == 'STN_FULL');

    require_once 'Source/libs/ListingClass.php';
    $Listing = new Listing('PRO', $ListingDesigns['compliance']['compliance']);
    $Listing->LoadColumnsFromDB();

    $Listing->WhereClause = 'pro_ele_id = '.$ele['recordid'];
    $Listing->OrderBy = 'pro_listorder, pro_code';
    $Listing->LoadDataFromDB();

    echo '<li>';

    echo $Listing->GetListingHTML($FormAction);

    if ($AdminUser && $FormAction != "Print")
    {
        echo '<div class="padded_div"><a href="Javascript:if(CheckChange()){SendTo(\'app.php?action=prompt&ele_id=' . $ele["recordid"] .'\');}"><b>'._tk('add_new_prompt').'</b></a></div>';
        
        
    }

    echo '</li>';
}

function StandardDetailsSection($ele, $FormAction)
{
    $sql =
    "SELECT
        dom.cod_descr as domain,
        ori.cod_descr as origin,
        stn.stn_name as stnTitle,
        stn.stn_ourref as stnRef
	FROM
        code_types dom,
        standards_main stn,
        code_types ori
    where
        stn.stn_domain = dom.cod_code
        and dom.cod_type = 'STNDOM'
        and stn.stn_set = ori.cod_code
        and ori.cod_type = 'STNSET'
        and stn.recordid = :stn_id";

    $row = DatixDBQuery::PDO_fetch($sql, array('stn_id' => $ele['ele_stn_id']));

    if ($row)
    {
        echo GetDivFieldHTML('Origin', $row["origin"]);
        echo GetDivFieldHTML('Domain', $row["domain"]);
        echo GetDivFieldHTML('Title', $row["stnTitle"]);
        echo GetDivFieldHTML('Ref.', $row["stnRef"]);
    }
}

function ListEvidence($ele, $FormAction)
{
    require_once 'Source/standards/EvidenceForm.php';
    ListLinkedEvidence('ELE', $ele['recordid'], $FormAction);
}

?>
