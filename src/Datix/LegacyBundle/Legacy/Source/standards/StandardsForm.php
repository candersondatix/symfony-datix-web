<?php

function ElementsSection($stn, $FormAction)
{
    $Perms = GetParm("STN_PERMS");
	$AdminUser = ($Perms == 'STN_FULL');
    $ReadOnly = ($FormAction == 'ReadOnly' || $FormAction == 'Print'  ? true : false);

    require_once 'Source/libs/ListingClass.php';
    $Design = new Listings_ModuleListingDesign(array('module' => 'ELE', 'parent_module' => 'STN', 'link_type' => 'elements'));
    $Design->LoadColumnsFromDB();

    $RecordList = new RecordLists_ModuleRecordList();
    $RecordList->Module = 'ELE';
    $RecordList->Columns = $Design->Columns;
    $RecordList->WhereClause = 'ele_stn_id = '.$stn['recordid'];
    // We need to disable paging for record listings
    $RecordList->Paging = false;

    $ListByListorder = new Listings_ListingColumn('ele_listorder');
    $ListByListorder->setAscending();

    $RecordList->OrderBy = array($ListByListorder, new Listings_ListingColumn('ele_code'));
    $RecordList->RetrieveRecords();

    echo '<li>';

    $ListingDisplay = new Listings_ListingDisplay($RecordList, $Design, '', $ReadOnly);
    echo $ListingDisplay->GetListingHTML();

    if ($AdminUser && $FormAction != "Print" && $FormAction != 'ReadOnly')
    {
        echo '<div class="padded_div"><a href="Javascript:if(CheckChange()){SendTo(\'app.php?action=element&stn_id='.$stn['recordid'].'\');}"><b>'._tk('add_new_element').'</b></a></div>';
    }
    
    echo '</li>';
}

function ListEvidence($stn, $FormAction)
{
    require_once 'Source/standards/EvidenceForm.php';
    ListLinkedEvidence('STN', $stn['recordid'], $FormAction);
}

function ListRelatedEvidence($stn, $FormAction)
{
    require_once 'Source/standards/EvidenceForm.php';
    ListRelatedEvidenceAtSTNLevel("link_library.stn_id = $stn[recordid]", $FormAction);
}

?>