<?php

function CanEditUser($recordid)
{
    $sql = 'SELECT count(*) as num FROM staff';
    $Where[] = 'recordid = '.$recordid;

    $WhereClause = MakeSecurityWhereClause($Where, 'ADM');

    $sql .= ' WHERE '.$WhereClause;

    $resultarray = DatixDBQuery::PDO_fetch($sql);

    if (intval($resultarray['num']) > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function panelLocationTypeSettings($staff = null, $FormAction = null)
{
	global $ModuleDefs;

    if ($staff['recordid'] && is_numeric($staff['recordid']))
    {
        // Invalidate the cache before calling GetUserSecurityGroups() - this is needed for panel location type settings
        \src\framework\registry\Registry::getInstance()->getSessionCache()->delete('security.groups-of-type-0-for-user-'.$staff['recordid']);

        $groups = GetUserSecurityGroups($staff['recordid']);

        if (count($groups) > 0)
        {
            echo '<tr><td><table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';
            echo '<tr><td class="rowtitlebg">';
            echo 'Location and type settings are not available because security groups are being used. <input type="hidden" name="no_loctype_save" id="no_loctype_save" value="1">';
            echo '</td></tr></table></td></tr>';
            return;
        }
    }

	include_once 'Source/security/SecurityBase.php';

	if (!isset($staff))
    {
		// Retrieve incident location/type settings stored against staff/contact record
		// and user parameters
		$sql = "SELECT
			recordid,
			login,
			sta_orgcode as inc_organisation,
			sta_clingroup as inc_clingroup,
			sta_unit as inc_unit,
			sta_directorate as inc_directorate,
			sta_specialty as inc_specialty
			FROM contacts_main
			WHERE recordid = :recordid";
        $staff = DatixDBQuery::PDO_fetch($sql, array("recordid" => Sanitize::SanitizeInt($_POST["recordid"])));

		$ParamDefs = UserParamList();

		foreach ($ParamDefs as $Param)
        {
			$staff[$Param] = FetchUserParm($staff["login"], $Param);
		}
	}
    else
    {
        //some params are passed back after error as lower case - need to make sure they are compatible
        //with uppercase fieldnames
        $ParamDefs = UserParamList();

        foreach ($ParamDefs as $Param)
        {
            if ($staff[\UnicodeString::strtolower($Param)] && !$staff[$Param])
            {
                $staff[$Param] = $staff[\UnicodeString::strtolower($Param)];
            }
        }
    }

	echo '<li class="section_title_row">
            <div class="title_text_wrapper">
	            <b>Security settings</b>
            </div>
            <div class="title_select_wrapper">';

    $ExcludeArray = array('ELE', 'PRO');

    if (!IsFullAdmin())
    {
        $ExcludeArray[] = 'ADM';
    }
    $setChangedFlag = ($FormAction != 'ReadOnly');
    $onChange = ($FormAction == 'ReadOnly') ? 'showModuleSection(jQuery(\'#securitySettingsModule\').val(), \'security_settings\'); AlertOnChange = false;'
        : 'showModuleSection(jQuery(\'#securitySettingsModule\').val(), \'security_settings\')';
    echo getModuleDropdown(array(
                    'name' => 'securitySettingsModule',
                    'onchange' => $onChange,
                    'current' => $currentModule,
                    'not' => $ExcludeArray,
                    'setChangedFlag' => $setChangedFlag
                    ));

    getSecuritySettingJS(array('not_parameters' => true));

	echo '
			</div>
		</li>
    <li>
	<ol>';

    foreach (getModArray() as $Mod => $Name)
    {
        if (!$ModuleDefs[$Mod]['NO_SECURITY_LEVEL'])
        {
            echo '<li id="'.$Mod.'_security_settings" style="display:' . (($currentModule == $Mod) ? 'block' : 'none') . '"><ol>';
            $fn = 'sectionSecuritySettings_'.$Mod;

            if (function_exists($fn))
            {
                $fn($staff, $FormAction);
            }
            elseif ($ModuleDefs[$Mod]['GENERIC'])
            {
                sectionSecuritySettingsGeneric($Mod, $staff, $FormAction);
            }
            else
            {
                sectionSecuritySettings($Mod, $staff, $FormAction);
            }

            echo '</ol></li>';
        }
    }

	echo '</ol></li>';
}

function sectionSecuritySettings_ADM($staff, $FormAction)
{
    $FieldMode = ($FormAction == "ReadOnly" ? "Print" : $FormAction);
    $FieldObj = new FormField($FieldMode);

    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'ADM_PERMS', 'ADM', $staff['ADM_PERMS']));
}

function sectionSecuritySettings_TOD($staff, $FormAction)
{
    $FieldMode = ($FormAction == "ReadOnly" ? "Print" : $FormAction);
    $FieldObj = new FormField($FieldMode);

    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'TOD_PERMS', 'TOD', $staff['TOD_PERMS']));
}

function sectionSecuritySettings_INC($staff, $FormAction)
{
	include_once 'Source/libs/FormClasses.php';

	$FieldMode = ($FormAction == "ReadOnly" ? "Print" : $FormAction);
	$FieldObj = new FormField($FieldMode);

    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'DIF_PERMS', 'INC', $staff['DIF_PERMS']));

	if (!(bYN(GetParm("DIF_WHERE_OVERRIDE")) && bYN(GetParm("DIF_1_EMAIL_SELECT")) && $Where))
	{
        $field = Forms_SelectFieldFactory::createSelectField('inc_organisation', 'INC', $staff['inc_organisation'], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("inc_organisation", "Trust"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('inc_unit', 'INC', $staff['inc_unit'], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("inc_unit", "Unit"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('inc_clingroup', 'INC', $staff['inc_clingroup'], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("inc_clingroup", "Clinical group"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('inc_directorate', 'INC', $staff['inc_directorate'], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("inc_directorate", "Directorate"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('inc_specialty', 'INC', $staff['inc_specialty'], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("inc_specialty", "Specialty"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('inc_loctype', 'INC', $staff['INC_LOCTYPE'], $FieldMode, true);
        $field->setAltFieldName('loctypes');
        $field->setIgnoreMaxLength();
        echo GetDivFieldHTML(GetFieldLabel("inc_loctype", "Location type"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('inc_locactual', 'INC', $staff['INC_LOCACTUAL'], $FieldMode, true);
        $field->setAltFieldName('locactuals');
        $field->setIgnoreMaxLength();
        echo GetDivFieldHTML(GetFieldLabel("inc_locactual", "Location (exact)"), $field->getField());

        $TypeObj = Forms_SelectFieldFactory::createSelectField('inc_type', 'INC', '', '');
        $CatObj = Forms_SelectFieldFactory::createSelectField('inc_category', 'INC', '', '');

        // ignore parenting
        $TypeObj->setParents(array());
        $TypeObj->setChildren(array());
        $CatObj->setParents(array());
        $CatObj->setChildren(array());

        $FieldHTML = '<div><select name="type_cat_table[]" multiple="multiple" size="5" id="type_cat_table" style="width:300px">';

		if ($staff['DIF_TYPECAT_PERMS'])
        {
			foreach (explode(':',$staff['DIF_TYPECAT_PERMS']) as $TypeCat)
            {
				$TC = explode(" ", $TypeCat);

				if ($TC)
                {
                    $FieldHTML .= '
                        <option value="' . $TC[0] . ' '
                        . $TC[1] . '">'
                        . code_descr("INC", "inc_type", $TC[0])
                        . ' - '
                        . code_descr("INC", "inc_category", $TC[1])
                        . '</option>
			        ';
                }
			}
		}
        //this code adds (for type + category field) the delete button, type coded field, category coded field and add button
        if($FormAction != 'ReadOnly')
        {
            $FieldHTML .= '</select> &nbsp;
			<input type="button" name="btnDel" value="Delete" onclick="deleteSelected(document.getElementById(\'type_cat_table\'))" />
		    </div>
		    <div><ol>'.
            GetDivFieldHTML('Type',$TypeObj->getField()).
            GetDivFieldHTML('Category',$CatObj->getField()).
            GetDivFieldHTML('','<input type="button" name="btnAdd" value="Add type and category to list" onclick="add2Codes(document.forms[0].inc_type, document.forms[0].inc_category, document.getElementById(\'type_cat_table\'))" /> ');
        }
        $FieldHTML .= '</ol></div>';

        echo GetDivFieldHTML('Types and categories',$FieldHTML);
	}

    include_once 'SecurityBase.php';

    $Perm = ParsePermString($staff["permission"]);
    $Where = $Perm["INC"]["where"];

    if(bYN(GetParm("DIF_WHERE_OVERRIDE")) || bYN(GetParm("DIF_1_EMAIL_SELECT")))
    {
        echo GetDivFieldHTML('Security "WHERE" clause from main application',($Where ? htmlspecialchars($Where) : 'Access to all records.') );
    }

    if ($staff["initials"])
    {
        echo GetDivFieldHTML(_tk('sql_generated'),MakeSecurityWhereClause("", "INC", $staff["initials"], "", "", false, false) );
    }

	if (bYN(GetParm("DIF_1_EMAIL_SELECT")))
	{
		if (bYN(GetParm("DIF_WHERE_OVERRIDE")))
		{
			if ($Where)
            {
				$EmailExtraText = 'User will receive e-mail notifications for incident reports matching the main application WHERE clause.';
            }
			else
            {
				$EmailExtraText = 'User will receive e-mail notifications for incident reports matching the above location and type settings.';
            }
		}
		else
		{
			if ($Where)
            {
				$EmailExtraText = 'User will receive e-mail notifications for incident reports matching both the above location and type settings and the main application WHERE clause.';
            }
			else
            {
				$EmailExtraText = 'User will receive e-mail notifications for incident reports matching the above location and type settings.';
            }
		}
	}
	else
	{
		$EmailExtraText = 'User will receive e-mail notifications for incident reports matching the above location and type settings.';
	}

    $FieldObj->MakeDivCheckbox('DIF_STA_EMAIL_LOCS', "", $staff["DIF_STA_EMAIL_LOCS"]);
    echo GetDivFieldHTML('Receive e-mail notifications:',$FieldObj->GetField(), $EmailExtraText);
}


function sectionSecuritySettings_RAM($staff, $FormAction)
{
    $FieldMode = ($FormAction == "ReadOnly" ? "Print" : $FormAction);
    $FieldObj = new FormField($FieldMode);

    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'RISK_PERMS', 'RAM', $staff['RISK_PERMS']));

    if (!(bYN(GetParm("RISK_WHERE_OVERRIDE")) && bYN(GetParm("RISK_1_EMAIL_SELECT")) && $Where))
    {
        $field = Forms_SelectFieldFactory::createSelectField('ram_organisation', 'RAM', $staff["RAM_ORGANISATION"], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("ram_organisation", "Trust"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('ram_unit', 'RAM', $staff["RAM_UNIT"], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("ram_unit", "Unit"),  $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('ram_clingroup', 'RAM', $staff["RAM_CLINGROUP"], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("ram_clingroup", "Clinical group"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('ram_directorate', 'RAM', $staff["RAM_DIRECTORATE"], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("ram_directorate", "Directorate"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('ram_specialty', 'RAM', $staff["RAM_SPECIALTY"], $FieldMode);
        echo GetDivFieldHTML(GetFieldLabel("ram_specialty", "Specialty"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('ram_location', 'RAM', $staff['RAM_LOCATION'], $FieldMode, true);
        $field->setIgnoreMaxLength();
        echo GetDivFieldHTML(GetFieldLabel("ram_location", "Location type"), $field->getField());

        $field = Forms_SelectFieldFactory::createSelectField('ram_locactual', 'RAM', $staff['RAM_LOCACTUAL'], $FieldMode, true);
        $field->setIgnoreMaxLength();
        echo GetDivFieldHTML(GetFieldLabel("ram_locactual", "Location (exact)"), $field->getField());
    }

    require_once 'SecurityBase.php';

    $Perm = ParsePermString($staff["permission"]);
    $Where = $Perm["RAM"]["where"];

    if(bYN(GetParm("RISK_WHERE_OVERRIDE")) || bYN(GetParm("RISK_1_EMAIL_SELECT")))
    {
        echo GetDivFieldHTML('Security "WHERE" clause from main application',($Where ? htmlspecialchars($Where) : 'Access to all records.') );
    }

    echo GetDivFieldHTML(_tk('sql_generated'), MakeSecurityWhereClause("", "RAM", $staff["initials"], "", "", false, false));

    if (bYN(GetParm("RISK_1_EMAIL_SELECT")))
    {
        if (bYN(GetParm("RISK_WHERE_OVERRIDE")))
        {
            if ($Where)
            {
                $EmailExtraText = 'User will receive e-mail notifications for risks matching the main application WHERE clause.';
            }
            else
            {
                $EmailExtraText = 'User will receive e-mail notifications for risks matching the above location and type settings.';
            }
        }
        else
        {
            if ($Where)
            {
                $EmailExtraText = 'User will receive e-mail notifications for risks matching both the above location and type settings and the main application WHERE clause.';
            }
            else
            {
                $EmailExtraText = 'User will receive e-mail notifications for risks matching the above location and type settings.';
            }
        }
    }
    else
    {
        $EmailExtraText = 'User will receive e-mail notifications for risks matching the above location and type settings.';
    }

    $FieldObj->MakeDivCheckbox('RISK_STA_EMAIL_LOCS', "", $staff["RISK_STA_EMAIL_LOCS"]);
    echo GetDivFieldHTML('Receive e-mail notifications:',$FieldObj->GetField(), $EmailExtraText);
}


function sectionSecuritySettingsGeneric($module, $staff, $FormAction)
{
    global $ModuleDefs;

    $FieldMode = ($FormAction == "ReadOnly" ? "Print" : $FormAction);
    $FieldObj = new FormField($FieldMode);

    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, $ModuleDefs[$module]['PERM_GLOBAL'], $module, $staff[$ModuleDefs[$module]['PERM_GLOBAL']]));

    if (!bYN(GetParm($module."_WHERE_OVERRIDE")) || (bYN(GetParm($module."_WHERE_OVERRIDE")) && !$Where))
    {
        if (is_array($ModuleDefs[$module]['SECURITY']['LOC_FIELDS']))
        {
            foreach ($ModuleDefs[$module]['SECURITY']['LOC_FIELDS'] as $Field => $Details)
            {
                $UserParmField = \UnicodeString::strtoupper($Field);

                $field = Forms_SelectFieldFactory::createSelectField($Field, $module, $staff[$UserParmField], $FieldMode, $Details['multi']);

                if ($Details['multi'])
                {
                	$field->setIgnoreMaxLength();
                }
                
                echo GetDivFieldHTML(GetFieldLabel($Field, _tk($Field)), $field->getField());
            }
        }
    }

    include_once 'SecurityBase.php';

    $Perm = ParsePermString($staff["permission"]);
    $Where = $Perm[$module]["where"];

    echo GetDivFieldHTML('Security "WHERE" clause from main application',($Where ? htmlspecialchars($Where) : 'Access to all records.') );

    echo GetDivFieldHTML(_tk('sql_generated'), MakeSecurityWhereClause("", $module, $staff["initials"], "", "", false, false));

    if ($ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL'])
    {
        if (bYN(GetParm($ModuleDefs[$module]['SECURITY']['EMAIL_SELECT_GLOBAL'])))
        {
            if (bYN(GetParm($module."_WHERE_OVERRIDE")))
            {
                if ($Where)
                {
                    $EmailExtraText = 'User will receive e-mail notifications for records matching the main application WHERE clause.';
                }
                else
                {
                    $EmailExtraText = 'User will receive e-mail notifications for records matching the above location and type settings.';
                }
            }
            else
            {
                if ($Where)
                {
                    $EmailExtraText = 'User will receive e-mail notifications for records matching both the above location and type settings and the main application WHERE clause.';
                }
                else
                {
                    $EmailExtraText = 'User will receive e-mail notifications for records matching the above location and type settings.';
                }
            }
        }
        else
        {
            $EmailExtraText = 'User will receive e-mail notifications for records matching the above location and type settings.';
        }

        $FieldObj->MakeDivCheckbox($ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL'], "", $staff[$ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL']]);
        echo GetDivFieldHTML('Receive e-mail notifications:',$FieldObj->GetField(), $EmailExtraText);
    }
}

function sectionSecuritySettings_HSA($staff, $FormAction)
{
    $FieldObj = new FormField($FormAction);
    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'HSA_PERMS', 'HSA', $staff['HSA_PERMS']));
}

function sectionSecuritySettings_HOT($staff, $FormAction)
{
    $FieldObj = new FormField($FormAction);
    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'HOT_PERMS', 'HOT', $staff['HOT_PERMS']));
}

function sectionSecuritySettings_MED($staff, $FormAction)
{
    $FieldObj = new FormField($FormAction);
    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'MED_PERMS', 'MED', $staff['MED_PERMS']));
}

function sectionSecuritySettings_PAY($staff, $FormAction)
{
    $FieldObj = new FormField($FormAction);
    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, 'PAY_PERMS', 'PAY', $staff['PAY_PERMS']));
}

function sectionSecuritySettings($module, $staff, $FormAction)
{
	global $ModuleDefs;

	$fld = array(); // Array of field objects used to create the page
	$FieldMode = ($FormAction == "ReadOnly" ? "Print" : $FormAction);
	$table = new FormTable($FormAction);

    include_once 'SecurityBase.php';

	$Perm = ParsePermString($staff["permission"]);
	$Where = $Perm[$module]["where"];

	$FieldObj = new FormField();

    if ($FormAction == 'ReadOnly')
    {
        $FieldObj->FieldMode = 'ReadOnly';
    }

    echo GetDivFieldHTML('User access level', MakeAccessLevelField($FieldObj, $ModuleDefs[$module]['PERM_GLOBAL'], $module, $staff[$ModuleDefs[$module]['PERM_GLOBAL']]));

    echo GetDivFieldHTML('Security "WHERE" clause from main application', ($Where ? htmlspecialchars($Where) : 'Access to all records.'));
}

function UserParamList()
{
	global $ModuleDefs;

    $ParamDefs = array(
		// Access levels
		'DIF_PERMS',
        'RISK_PERMS',
		'PAL_PERMS',
		'SAB_PERMS',
		'STN_PERMS',
		'ACT_PERMS',
		'AST_PERMS',
		'CON_PERMS',
		'DST_PERMS',
        'DAS_PERMS',
        'COM_PERMS',
        'MED_PERMS',
        'HOT_PERMS',
        'ADM_PERMS',
        'TOD_PERMS',
		// Incidents
		'DIF_STA_EMAIL_LOCS',
		'DIF_SHOW_REJECT_BTN',
        'DIF2_HIDE_CONTACTS',
		'USER_GRANT_ACCESS',
        'DIF_OWN_ONLY',
		'INC_SHOW_AUDIT',
        'DIF1_ONLY_FORM',
        'DIF2_DEFAULT',
		'INC_LISTING_ID',
        'DIF2_SEARCH_DEFAULT',
		'INC_LOCACTUAL',
        'INC_LOCTYPE',
		'INC_SETUP',
        'COPY_INCIDENTS',
		'DIF_TYPECAT_PERMS',
        'INC_DELETE_DOCS',
        'INC_SAVED_QUERIES_HOME_SCREEN',
        'INC_SAVED_QUERIES',
		// Risk
		'RISK_OWN_ONLY',
        'RISK2_DEFAULT',
		'RAM_LISTING_ID',
        'RISK2_SEARCH_DEFAULT',
		'RAM_ORGANISATION',
		'RAM_UNIT',
		'RAM_CLINGROUP',
		'RAM_DIRECTORATE',
		'RAM_SPECIALTY',
		'RAM_LOCATION',
		'RAM_LOCACTUAL',
        'RISK_STA_EMAIL_LOCS',
        'RAM_SHOW_AUDIT',
		'RAM_SETUP',
        'COPY_RISKS',
        'RAM_DELETE_DOCS',
		// Contacts
        'CON_DEFAULT',
		'CON_LISTING_ID',
        'CON_SEARCH_DEFAULT',
        'CON_SHOW_REJECT_BTN',
        'CON_SETUP',
        'CON_ALLOW_MERGE_DUPLICATES',
		// Actions
		'ACT_OWN_ONLY',
        'ACT_DEFAULT',
		'ACT_LISTING_ID',
        'ACT_SEARCH_DEFAULT',
        'ACT_SHOW_AUDIT',
        'ACT_SETUP',
        'DELETE_ACT_CHAIN',
        'ACT_DELETE_DOCS',
		// Assets - Equipment
        'AST_DEFAULT',
		'AST_LISTING_ID',
        'AST_SEARCH_DEFAULT',
        'AST_SETUP',
        'AST_DELETE_DOCS',
        //All modules
        'LOGIN_DEFAULT_MODULE',
        'LOGOUT_DEFAULT_MODULE',
        'CUSTOM_REPORT_BUILDER',
        'ADDITIONAL_REPORT_OPTIONS',
		'PROG_NOTES_EDIT',
        'ENABLE_GENERATE',
        'ENABLE_BATCH_DELETE',
        'ENABLE_BATCH_UPDATE',
        // Safety Alerts
        'SAB1_DEFAULT',
        'SAB2_DEFAULT',
        'SAB_LISTING_ID',
        'SAB1_SEARCH_DEFAULT',
        'SAB_SETUP',
        'SAB_DELETE_DOCS',
        // PALS
        'PAL2_DEFAULT',
        'PAL_LISTING_ID',
        'PAL2_SEARCH_DEFAULT',
        'PAL_OWN_ONLY',
        'PAL_SHOW_AUDIT',
        'PAL_SETUP',
        'PAL_DELETE_DOCS',
        // COMPAINTS
        'COM2_DEFAULT',
        'COM_LISTING_ID',
        'COM2_SEARCH_DEFAULT',
        'COM_OWN_ONLY',
        'COM_SHOW_AUDIT',
        'COM_SETUP',
        'COM_DELETE_DOCS',
        'COM_SHOW_ADD_NEW',
        // Claims
        'CLAIM1_DEFAULT',
        'CLAIM2_DEFAULT',
        'CLA_LISTING_ID',
        'CLAIM2_SEARCH_DEFAULT',
        'CLA_OWN_ONLY',
        'CLA_SHOW_AUDIT',
        'CLA_SETUP',
        'CLA_DELETE_DOCS',
    	// Policies
    	'POL2_DEFAULT',
    	'POL_LISTING_ID',
    	'POL2_SEARCH_DEFAULT',
    	'POL_OWN_ONLY',
    	'POL_SHOW_AUDIT',
    	'POL_SETUP',
    	'COPY_POL',
    	'POL_DELETE_DOCS',
        //Payments
        'PAY2_SEARCH_DEFAULT',
        'DELETE_PAY',
        // Medications
        'MED_DEFAULT',
        'MED_LISTING_ID',
        'MED_SEARCH_DEFAULT',
        'MED_SETUP',
        // Hotspot Agents
        'HSA2_DEFAULT',
        'HSA_LISTING_ID',
        'HSA2_SEARCH_DEFAULT',
        // Hotspots
        'HOT2_DEFAULT',
        'HOT_LISTING_ID',
        'HOT2_SEARCH_DEFAULT',
        'HOT_DELETE_DOCS',
        //'HOT_SETUP',
        //Standards
        'STN2_DEFAULT',
        'STN_LISTING_ID',
        'STN2_SEARCH_DEFAULT',
        'STN_ADD_NEW',
        'STN_SETUP',
        'COPY_STANDARDS',
        'STN_DELETE_DOCS',
        //Elements
        //'ELE_SETUP',
        //Prompts
        //'PRO_SETUP',
        //Library
        'LIB_SETUP',
        'LIB_DELETE_DOCS',
        // CQC Standards
        'CQO_LISTING_ID',
        'CQP_LISTING_ID',
        'CQS_LISTING_ID',
        'CQO_DEFAULT',
        'CQP_DEFAULT',
        'CQS_DEFAULT',
        //Accreditation
        'ATQ_DEFAULT',
        'ATQ_LISTING_ID',
        'ATI_DEFAULT',
        'ATI_LISTING_ID',
        'ATM_DEFAULT',
        'ATM_LISTING_ID',
        'AQU_DEFAULT',
        'AQU_LISTING_ID',
        'AMO_DEFAULT',
        'AMO_LISTING_ID',
        'ATM_MQ_TEMP',
        'ATM_AAT',
        'ATM_CAI',
        'ATI_STAFF_EMAIL',
        'ATI_LOC_EMAIL',
        'ATM_STAFF_EMAIL',
        'ASM_MANAGE_CYCLES',
        'ASM_CREATE_INSTANCES_EMAIL',
        'AMO_SUBMIT_INSTANCE_EMAIL',
        'AMO_STAFF_EMAIL',
        'ATI_REVIEWED_EMAIL',
        //Locations
        'LOC_LISTING_ID',
        'LOC_DEFAULT',
        //Admin
        'ADM_PROFILES',
        'FULL_ADMIN',
        'ADM_DEFAULT',
        'ADM_LISTING_ID',
        'ADM_SHOW_AUDIT',
        'ADM_GROUP_SETUP',
        'ADM_VIEW_OWN_USER',
        'ADM_NO_ADMIN_REPORTS'
	);

    foreach ($ModuleDefs as $module => $Details)
    {
        if ($Details['GENERIC'] && ModIsLicensed($module))
        {
            if (is_array($ModuleDefs[$module]['SECURITY']['LOC_FIELDS']))
            {
                foreach ($ModuleDefs[$module]['SECURITY']['LOC_FIELDS'] as $field => $fieldDetails)
                {
                    $ParamDefs[] = \UnicodeString::strtoupper($field);
                }
            }

            if ($ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL'])
            {
                $ParamDefs[] = $ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL'];
            }

            $ParamDefs[] = $ModuleDefs[$module]['PERM_GLOBAL'];
        }
    }

	return $ParamDefs;
}

function saveLocationTypeSettings($sta)
{
	global $ModuleDefs;

	// This function save the information on the location/type settings panel
	// It should only be called if the used is NOT a member of security groups

	// Actions
	SetUserParm($sta["login"], "ACT_PERMS", $sta["ACT_PERMS"]);

	// Contacts
	SetUserParm($sta["login"], "CON_PERMS", $sta["CON_PERMS"]);

    // Equipment/Assets
    SetUserParm($sta["login"], "AST_PERMS", $sta["AST_PERMS"]);

    // Medications
    SetUserParm($sta["login"], "MED_PERMS", $sta["MED_PERMS"]);

    // Distribution lists
    SetUserParm($sta["login"], "DST_PERMS", $sta["DST_PERMS"]);

	// Incidents
	if ($_SESSION["licensedModules"][MOD_INCIDENTS])
    {
		SetUserParm($sta["login"], "DIF_PERMS", $sta["DIF_PERMS"]);
		if(empty($sta["DIF_STA_EMAIL_LOCS"]))
		{
		    $sta["DIF_STA_EMAIL_LOCS"] = 'N';
		}
		SetUserParm($sta["login"], "DIF_STA_EMAIL_LOCS", $sta["DIF_STA_EMAIL_LOCS"]);

		$LocActuals = $LocTypes = $TypeCats = '';

		if ($sta["loctypes"] != "")
        {
            $LocTypes = implode(" ", $sta["loctypes"]);
        }

		if ($sta["locactuals"] != "")
        {
            $LocActuals = implode(" ", $sta["locactuals"]);
        }

		if ($sta["type_cat_table"] != "")
        {
            $TypeCats = implode(":", $sta["type_cat_table"]);
        }

		SetUserParm($sta["login"], "INC_LOCTYPE", $LocTypes);
		SetUserParm($sta["login"], "INC_LOCACTUAL", $LocActuals);
		SetUserParm($sta["login"], "DIF_TYPECAT_PERMS", $TypeCats);
	}

    // Risk register
    if ($_SESSION["licensedModules"][MOD_RISKREGISTER])
    {
        SetUserParm($sta["login"], "RISK_PERMS", $sta["RISK_PERMS"]);
        SetUserParm($sta["login"], "RAM_ORGANISATION", $sta["ram_organisation"]);
        SetUserParm($sta["login"], "RAM_UNIT", $sta["ram_unit"]);
        SetUserParm($sta["login"], "RAM_CLINGROUP", $sta["ram_clingroup"]);
        SetUserParm($sta["login"], "RAM_DIRECTORATE", $sta["ram_directorate"]);
        SetUserParm($sta["login"], "RAM_SPECIALTY", $sta["ram_specialty"]);
        SetUserParm($sta["login"], "RISK_STA_EMAIL_LOCS", $sta["RISK_STA_EMAIL_LOCS"]);

        $LocActuals = $LocTypes = '';

        if ($sta["ram_location"])
        {
            $LocTypes = implode(" ", $sta["ram_location"]);
        }

        if ($sta["ram_locactual"])
        {
            $LocActuals = implode(" ", $sta["ram_locactual"]);
        }

        SetUserParm($sta["login"], "RAM_LOCATION", $LocTypes);
        SetUserParm($sta["login"], "RAM_LOCACTUAL", $LocActuals);
    }

    foreach ($ModuleDefs as $module => $Details)
    {
        if ($Details['GENERIC'] && ModIsLicensed($module) && !$ModuleDefs[$module]['NO_SECURITY_LEVEL'])
        {
            SetUserParm($sta["login"], $ModuleDefs[$module]['PERM_GLOBAL'], $sta[$ModuleDefs[$module]['PERM_GLOBAL']]);

            if (is_array($ModuleDefs[$module]['SECURITY']['LOC_FIELDS']))
            {
                foreach ($ModuleDefs[$module]['SECURITY']['LOC_FIELDS'] as $Field => $Details)
                {
                	if ($Details['multi'])
                    {
                        if ($sta[$Field] != '')
                        {
                            SetUserParm($sta["login"], \UnicodeString::strtoupper($Field), implode(" ", $sta[$Field]));
                        }
						else
						{
							// $sta['Field] doesn't exist which means that every values were deleted
							SetUserParm($sta["login"], \UnicodeString::strtoupper($Field), '');
						}
                    }
                    else
                    {
                        SetUserParm($sta["login"], \UnicodeString::strtoupper($Field), $sta[$Field]);
                    }
                }
            }

            SetUserParm($sta["login"], $ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL'], $sta[$ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL']]);
        }
    }

	// Safety alerts
	if ($_SESSION["licensedModules"][MOD_SABS])
    {
		SetUserParm($sta["login"], "SAB_PERMS", $sta["SAB_PERMS"]);
	}

	// Standards
	if ($_SESSION["licensedModules"][MOD_STANDARDS])
    {
		SetUserParm($sta["login"], "STN_PERMS", $sta["STN_PERMS"]);
	}

    // Dashboards
    if ($_SESSION["licensedModules"][MOD_DASHBOARD])
    {
        SetUserParm($sta["login"], "DAS_PERMS", $sta["DAS_PERMS"]);
    }

    // Complaints
    if ($_SESSION["licensedModules"][MOD_COMPLAINTS])
    {
        SetUserParm($sta["login"], "COM_PERMS", $sta["COM_PERMS"]);
    }

    // Hotspots
    if ($_SESSION["licensedModules"][MOD_HOTSPOTS])
    {
        SetUserParm($sta["login"], "HOT_PERMS", $sta["HOT_PERMS"]);
    }

    // Hotspots Agents
    if ($_SESSION["licensedModules"][MOD_HOTSPOTAGENTS])
    {
        SetUserParm($sta["login"], "HSA_PERMS", $sta["HSA_PERMS"]);
    }

    // CQC Outcomes
    if ($_SESSION["licensedModules"][MOD_CQC_OUTCOMES])
    {
        SetUserParm($sta["login"], "CQO_PERMS", $sta["CQO_PERMS"]);
    }

    // Locations
    if ($_SESSION["licensedModules"][MOD_LOCATIONS])
    {
        SetUserParm($sta["login"], "LOC_PERMS", $sta["LOC_PERMS"]);
    }

    // Admin
    SetUserParm($sta["login"], "ADM_PERMS", $sta["ADM_PERMS"]);

    // To Do List
    SetUserParm($sta["login"], "TOD_PERMS", $sta["TOD_PERMS"]);
}

function deleteConfigurationParameters($sta)
{
    $sql = 'DELETE FROM user_parms WHERE login = :login
        AND parameter IN (\''.implode('\', \'', getConfigurationParameters()).'\')';

    DatixDBQuery::PDO_query($sql, array('login' => $sta['login']));
}

function deleteSecurityGroups($sta)
{
    $sql = 'DELETE FROM sec_staff_group WHERE con_id = :con_id';
    DatixDBQuery::PDO_query($sql, array('con_id' => $sta['recordid']));
}

function deleteLocTypeSettings($sta)
{
    global $ModuleDefs;

    $ParametersToDelete = array(
        'ACT_PERMS',
        'CON_PERMS',
        'AST_PERMS',
        'MED_PERMS',
        'DST_PERMS',
        'DIF_PERMS',
        'DIF_STA_EMAIL_LOCS',
        'INC_LOCTYPE',
        'INC_LOCACTUAL',
        'DIF_TYPECAT_PERMS',
        'RISK_PERMS',
        'RAM_ORGANISATION',
        'RAM_UNIT',
        'RAM_CLINGROUP',
        'RAM_DIRECTORATE',
        'RAM_SPECIALTY',
        'RISK_STA_EMAIL_LOCS',
        'RAM_LOCATION',
        'RAM_LOCACTUAL',
        'SAB_PERMS',
        'STN_PERMS',
        'DAS_PERMS',
        'COM_PERMS',
        'HOT_PERMS',
        'HSA_PERMS',
        'ADM_PERMS',
        'TOD_PERMS');

    foreach ($ModuleDefs as $module => $Details)
    {
        if ($Details['GENERIC'] && ModIsLicensed($module) && !$ModuleDefs[$module]['NO_SECURITY_LEVEL'])
        {
            if (isset($ModuleDefs[$module]['PERM_GLOBAL']))
            {
                $ParametersToDelete[] = $ModuleDefs[$module]['PERM_GLOBAL'];
            }

            if (is_array($ModuleDefs[$module]['SECURITY']['LOC_FIELDS']))
            {
                foreach ($ModuleDefs[$module]['SECURITY']['LOC_FIELDS'] as $Field => $Details)
                {
                    $ParametersToDelete[] = \UnicodeString::strtoupper($Field);
                }
            }

            if (isset($ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL']))
            {
                $ParametersToDelete[] = $ModuleDefs[$module]['SECURITY']['EMAIL_NOTIFICATION_GLOBAL'];
            }
        }
    }

    $sql = 'DELETE FROM user_parms WHERE login = :login
        AND parameter IN (\''.implode('\', \'', $ParametersToDelete).'\')';

    DatixDBQuery::PDO_query($sql, array('login' => $sta['login']));

    $sql = 'UPDATE contacts_main SET sta_orgcode = NULL, sta_unit = NULL, sta_clingroup = NULL, sta_specialty = NULL, sta_directorate = NULL WHERE recordid = :recordid';
    DatixDBQuery::PDO_query($sql, array('recordid' => $sta['recordid']));

}

function saveConfigurationParameters($sta)
{
	// Actions
	SetUserParm($sta["login"], "ACT_OWN_ONLY", $sta["ACT_OWN_ONLY"]);
    SetUserParm($sta["login"], "ACT_DEFAULT", $sta["ACT_DEFAULT"]);
	SetUserParm($sta["login"], "ACT_LISTING_ID", $sta["ACT_LISTING_ID"]);
    SetUserParm($sta["login"], "ACT_SEARCH_DEFAULT", $sta["ACT_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "ACT_SHOW_AUDIT", $sta["ACT_SHOW_AUDIT"]);
    SetUserParm($sta["login"], "ACT_SETUP", $sta["ACT_SETUP"]);
    SetUserParm($sta["login"], "DELETE_ACT_CHAIN", $sta["DELETE_ACT_CHAIN"]);
    SetUserParm($sta['login'], 'ACT_DELETE_DOCS', $sta['ACT_DELETE_DOCS']);

    // All modules
    SetUserParm($sta["login"], "LOGIN_DEFAULT_MODULE", $sta["LOGIN_DEFAULT_MODULE"]);
    SetUserParm($sta['login'], 'LOGOUT_DEFAULT_MODULE', $sta['LOGOUT_DEFAULT_MODULE']);
    SetUserParm($sta["login"], "CUSTOM_REPORT_BUILDER", $sta["CUSTOM_REPORT_BUILDER"]);
    SetUserParm($sta["login"], "ADDITIONAL_REPORT_OPTIONS", $sta["ADDITIONAL_REPORT_OPTIONS"]);
    SetUserParm($sta["login"], "PROG_NOTES_EDIT", $sta["PROG_NOTES_EDIT"]);
    SetUserParm($sta["login"], "ENABLE_GENERATE", $sta["ENABLE_GENERATE"]);
    SetUserParm($sta["login"], "ENABLE_BATCH_DELETE", $sta["ENABLE_BATCH_DELETE"]);
    SetUserParm($sta["login"], "ENABLE_BATCH_UPDATE", $sta["ENABLE_BATCH_UPDATE"]);

    // Admin
    if ($_POST['show_field_ADM_PROFILES'])
    {
        $sta["ADM_PROFILES"] = CheckMultiListValue("ADM_PROFILES", $_POST) ?: '';
    }
    
    SetUserParm($sta["login"], "ADM_DEFAULT", $sta["ADM_DEFAULT"]);
    SetUserParm($sta["login"], "ADM_LISTING_ID", $sta["ADM_LISTING_ID"]);
    SetUserParm($sta["login"], "ADM_PROFILES", $sta["ADM_PROFILES"]);
    SetUserParm($sta["login"], "FULL_ADMIN", $sta["FULL_ADMIN"]);
    SetUserParm($sta["login"], "ADM_SHOW_AUDIT", $sta["ADM_SHOW_AUDIT"]);
    SetUserParm($sta["login"], "ADM_GROUP_SETUP", $sta["ADM_GROUP_SETUP"]);
    SetUserParm($sta["login"], "ADM_VIEW_OWN_USER", $sta["ADM_VIEW_OWN_USER"]);
    SetUserParm($sta["login"], 'ADM_NO_ADMIN_REPORTS', $sta['ADM_NO_ADMIN_REPORTS']);

	// Contacts
	SetUserParm($sta["login"], "CON_DEFAULT", $sta["CON_DEFAULT"]);
    SetUserParm($sta["login"], "CON_LISTING_ID", $sta["CON_LISTING_ID"]);
    SetUserParm($sta["login"], "CON_SEARCH_DEFAULT", $sta["CON_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "CON_SHOW_REJECT_BTN", $sta["CON_SHOW_REJECT_BTN"]);
    SetUserParm($sta["login"], "CON_SETUP", $sta["CON_SETUP"]);
    SetUserParm($sta["login"], "CON_ALLOW_MERGE_DUPLICATES", $sta["CON_ALLOW_MERGE_DUPLICATES"]);

	// Equipment/Assets
	SetUserParm($sta["login"], "AST_DEFAULT", $sta["AST_DEFAULT"]);
    SetUserParm($sta["login"], "AST_LISTING_ID", $sta["AST_LISTING_ID"]);
    SetUserParm($sta["login"], "AST_SEARCH_DEFAULT", $sta["AST_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "AST_SETUP", $sta["AST_SETUP"]);
    SetUserParm($sta['login'], 'AST_DELETE_DOCS', $sta['AST_DELETE_DOCS']);

    // Medications
    SetUserParm($sta["login"], "MED_DEFAULT", $sta["MED_DEFAULT"]);
    SetUserParm($sta["login"], "MED_LISTING_ID", $sta["MED_LISTING_ID"]);
    SetUserParm($sta["login"], "MED_SEARCH_DEFAULT", $sta["MED_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "MED_SETUP", $sta["MED_SETUP"]);

    // Safety Alerts
    SetUserParm($sta["login"], "SAB1_DEFAULT", $sta["SAB1_DEFAULT"]);
    SetUserParm($sta['login'], 'SAB2_DEFAULT', $sta['SAB2_DEFAULT']);
    SetUserParm($sta["login"], "SAB_LISTING_ID", $sta["SAB_LISTING_ID"]);
    SetUserParm($sta["login"], "SAB1_SEARCH_DEFAULT", $sta["SAB1_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "SAB_SETUP", $sta["SAB_SETUP"]);
    SetUserParm($sta['login'], 'SAB_DELETE_DOCS', $sta['SAB_DELETE_DOCS']);

    // PALS
    SetUserParm($sta["login"], "PAL2_DEFAULT", $sta["PAL2_DEFAULT"]);
    SetUserParm($sta["login"], "PAL_LISTING_ID", $sta["PAL_LISTING_ID"]);
    SetUserParm($sta["login"], "PAL2_SEARCH_DEFAULT", $sta["PAL2_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "PAL_OWN_ONLY", $sta["PAL_OWN_ONLY"]);
    SetUserParm($sta["login"], "PAL_SHOW_AUDIT", $sta["PAL_SHOW_AUDIT"]);
    SetUserParm($sta["login"], "PAL_SETUP", $sta["PAL_SETUP"]);
    SetUserParm($sta['login'], 'PAL_DELETE_DOCS', $sta['PAL_DELETE_DOCS']);

    // Complaints
    SetUserParm($sta["login"], "COM2_DEFAULT", $sta["COM2_DEFAULT"]);
    SetUserParm($sta["login"], "COM_LISTING_ID", $sta["COM_LISTING_ID"]);
    SetUserParm($sta["login"], "COM2_SEARCH_DEFAULT", $sta["COM2_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "COM_OWN_ONLY", $sta["COM_OWN_ONLY"]);
    SetUserParm($sta["login"], "COM_SHOW_AUDIT", $sta["COM_SHOW_AUDIT"]);
    SetUserParm($sta["login"], "COM_SETUP", $sta["COM_SETUP"]);
    SetUserParm($sta['login'], 'COM_DELETE_DOCS', $sta['COM_DELETE_DOCS']);
    SetUserParm($sta['login'], 'COM_SHOW_ADD_NEW', $sta['COM_SHOW_ADD_NEW']);

    // Claims
    SetUserParm($sta["login"], "CLAIM1_DEFAULT", $sta["CLAIM1_DEFAULT"]);
    SetUserParm($sta["login"], "CLAIM2_DEFAULT", $sta["CLAIM2_DEFAULT"]);
    SetUserParm($sta["login"], "CLA_LISTING_ID", $sta["CLA_LISTING_ID"]);
    SetUserParm($sta["login"], "CLAIM2_SEARCH_DEFAULT", $sta["CLAIM2_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "CLA_OWN_ONLY", $sta["CLA_OWN_ONLY"]);
    SetUserParm($sta["login"], "CLA_SHOW_AUDIT", $sta["CLA_SHOW_AUDIT"]);
    SetUserParm($sta["login"], "CLA_SETUP", $sta["CLA_SETUP"]);
    SetUserParm($sta['login'], 'CLA_DELETE_DOCS', $sta['CLA_DELETE_DOCS']);
    
    // Policies
    SetUserParm($sta["login"], "POL2_DEFAULT", $sta["POL2_DEFAULT"]);
    SetUserParm($sta["login"], "POL_LISTING_ID", $sta["POL_LISTING_ID"]);
    SetUserParm($sta["login"], "POL2_SEARCH_DEFAULT", $sta["POL2_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "POL_OWN_ONLY", $sta["POL_OWN_ONLY"]);
    SetUserParm($sta["login"], "POL_SHOW_AUDIT", $sta["POL_SHOW_AUDIT"]);
    SetUserParm($sta["login"], "POL_SETUP", $sta["POL_SETUP"]);
    SetUserParm($sta["login"], "COPY_POL", $sta["COPY_POL"]);
    SetUserParm($sta['login'], 'POL_DELETE_DOCS', $sta['POL_DELETE_DOCS']);

    //Payments
    SetUserParm($sta["login"], "PAY2_SEARCH_DEFAULT", $sta["PAY2_SEARCH_DEFAULT"]);
    SetUserParm($sta["login"], "DELETE_PAY", $sta["DELETE_PAY"]);


	// Incidents
	if ($_SESSION["licensedModules"][MOD_INCIDENTS])
    {
		SetUserParm($sta["login"], "DIF_SHOW_REJECT_BTN", $sta["DIF_SHOW_REJECT_BTN"]);
        SetUserParm($sta["login"], "DIF2_HIDE_CONTACTS", $sta["DIF2_HIDE_CONTACTS"]);
		SetUserParm($sta["login"], "USER_GRANT_ACCESS", $sta["USER_GRANT_ACCESS"]);
        SetUserParm($sta["login"], "DIF_OWN_ONLY", $sta["DIF_OWN_ONLY"]);
		SetUserParm($sta["login"], "INC_SHOW_AUDIT", $sta["INC_SHOW_AUDIT"]);
        SetUserParm($sta["login"], 'DIF1_ONLY_FORM', $sta['DIF1_ONLY_FORM']);
        SetUserParm($sta["login"], "DIF2_DEFAULT", $sta["DIF2_DEFAULT"]);
        SetUserParm($sta["login"], "INC_LISTING_ID", $sta["INC_LISTING_ID"]);
        SetUserParm($sta["login"], "DIF2_SEARCH_DEFAULT", $sta["DIF2_SEARCH_DEFAULT"]);
        SetUserParm($sta["login"], "INC_SETUP", $sta["INC_SETUP"]);
        SetUserParm($sta["login"], "COPY_INCIDENTS", $sta["COPY_INCIDENTS"]);
        SetUserParm($sta['login'], 'INC_DELETE_DOCS', $sta['INC_DELETE_DOCS']);
        SetUserParm($sta['login'], 'INC_SAVED_QUERIES_HOME_SCREEN', $sta['INC_SAVED_QUERIES_HOME_SCREEN']);

        if ($_POST['show_field_INC_SAVED_QUERIES'])
        {
            $sta["INC_SAVED_QUERIES"] = CheckMultiListValue("INC_SAVED_QUERIES", $_POST) ?: '';
        }

        SetUserParm($sta["login"], "INC_SAVED_QUERIES", $sta["INC_SAVED_QUERIES"]);
	}

	// Risk register
	if ($_SESSION["licensedModules"][MOD_RISKREGISTER])
    {
        SetUserParm($sta["login"], "RISK_OWN_ONLY", $sta["RISK_OWN_ONLY"]);
		SetUserParm($sta["login"], "RAM_SHOW_AUDIT", $sta["RAM_SHOW_AUDIT"]);
        SetUserParm($sta["login"], "RISK2_DEFAULT", $sta["RISK2_DEFAULT"]);
        SetUserParm($sta["login"], "RAM_LISTING_ID", $sta["RAM_LISTING_ID"]);
        SetUserParm($sta["login"], "RISK2_SEARCH_DEFAULT", $sta["RISK2_SEARCH_DEFAULT"]);
        SetUserParm($sta["login"], "RAM_SETUP", $sta["RAM_SETUP"]);
        SetUserParm($sta["login"], "COPY_RISKS", $sta["COPY_RISKS"]);
        SetUserParm($sta['login'], 'RAM_DELETE_DOCS', $sta['RAM_DELETE_DOCS']);
	}

    // Hotspot Agents
    SetUserParm($sta["login"], "HSA2_DEFAULT", $sta["HSA2_DEFAULT"]);
    SetUserParm($sta["login"], "HSA_LISTING_ID", $sta["HSA_LISTING_ID"]);
    SetUserParm($sta["login"], "HSA2_SEARCH_DEFAULT", $sta["HSA2_SEARCH_DEFAULT"]);

    // Hotspots
    SetUserParm($sta["login"], "HOT2_DEFAULT", $sta["HOT2_DEFAULT"]);
    SetUserParm($sta["login"], "HOT_LISTING_ID", $sta["HOT_LISTING_ID"]);
    SetUserParm($sta["login"], "HOT2_SEARCH_DEFAULT", $sta["HOT2_SEARCH_DEFAULT"]);
    SetUserParm($sta['login'], 'HOT_DELETE_DOCS', $sta['HOT_DELETE_DOCS']);

    // Standards
    if ($_SESSION["licensedModules"][MOD_STANDARDS])
    {
        SetUserParm($sta["login"], "STN2_DEFAULT", $sta["STN2_DEFAULT"]);
        SetUserParm($sta["login"], "STN_LISTING_ID", $sta["STN_LISTING_ID"]);
        SetUserParm($sta["login"], "STN2_SEARCH_DEFAULT", $sta["STN2_SEARCH_DEFAULT"]);
        SetUserParm($sta["login"], "STN_ADD_NEW", $sta["STN_ADD_NEW"]);
        SetUserParm($sta["login"], "STN_SETUP", $sta["STN_SETUP"]);
        SetUserParm($sta["login"], "COPY_STANDARDS", $sta["COPY_STANDARDS"]);
        SetUserParm($sta['login'], 'STN_DELETE_DOCS', $sta['STN_DELETE_DOCS']);
    }

    //Library
    SetUserParm($sta["login"], "LIB_SETUP", $sta["LIB_SETUP"]);
    SetUserParm($sta['login'], 'LIB_DELETE_DOCS', $sta['LIB_DELETE_DOCS']);

    // CQC Standards
    SetUserParm($sta["login"], "CQO_LISTING_ID", $sta["CQO_LISTING_ID"]);
    SetUserParm($sta["login"], "CQP_LISTING_ID", $sta["CQP_LISTING_ID"]);
    SetUserParm($sta["login"], "CQS_LISTING_ID", $sta["CQS_LISTING_ID"]);

    SetUserParm($sta["login"], "CQO_DEFAULT", $sta["CQO_DEFAULT"]);
    SetUserParm($sta["login"], "CQP_DEFAULT", $sta["CQP_DEFAULT"]);
    SetUserParm($sta["login"], "CQS_DEFAULT", $sta["CQS_DEFAULT"]);

    //Accreditation
    SetUserParm($sta['login'], 'ATI_DEFAULT', $sta['ATI_DEFAULT']);
    SetUserParm($sta['login'], 'ATI_LISTING_ID', $sta['ATI_LISTING_ID']);
    SetUserParm($sta['login'], 'ATQ_DEFAULT', $sta['ATQ_DEFAULT']);
    SetUserParm($sta['login'], 'ATQ_LISTING_ID', $sta['ATQ_LISTING_ID']);
    SetUserParm($sta['login'], 'ATM_DEFAULT', $sta['ATM_DEFAULT']);
    SetUserParm($sta['login'], 'ATM_LISTING_ID', $sta['ATM_LISTING_ID']);
    SetUserParm($sta['login'], 'AQU_DEFAULT', $sta['AQU_DEFAULT']);
    SetUserParm($sta['login'], 'AQU_LISTING_ID', $sta['AQU_LISTING_ID']);
    SetUserParm($sta['login'], 'AMO_DEFAULT', $sta['AMO_DEFAULT']);
    SetUserParm($sta['login'], 'AMO_LISTING_ID', $sta['AMO_LISTING_ID']);
    SetUserParm($sta['login'], 'ATM_MQ_TEMP', $sta['ATM_MQ_TEMP']);
    SetUserParm($sta['login'], 'ATM_AAT', $sta['ATM_AAT']);
    SetUserParm($sta['login'], 'ATM_CAI', $sta['ATM_CAI']);
    SetUserParm($sta['login'], 'ATI_STAFF_EMAIL', $sta['ATI_STAFF_EMAIL']);
    SetUserParm($sta['login'], 'ATI_LOC_EMAIL', $sta['ATI_LOC_EMAIL']);
    SetUserParm($sta['login'], 'ATM_STAFF_EMAIL', $sta['ATM_STAFF_EMAIL']);
    SetUserParm($sta['login'], 'ASM_MANAGE_CYCLES', $sta['ASM_MANAGE_CYCLES']);
    SetUserParm($sta['login'], 'ASM_CREATE_INSTANCES_EMAIL', $sta['ASM_CREATE_INSTANCES_EMAIL']);
    SetUserParm($sta['login'], 'AMO_SUBMIT_INSTANCE_EMAIL', $sta['AMO_SUBMIT_INSTANCE_EMAIL']);
    SetUserParm($sta['login'], 'AMO_STAFF_EMAIL', $sta['AMO_STAFF_EMAIL']);
    SetUserParm($sta['login'], 'ATI_REVIEWED_EMAIL', $sta['ATI_REVIEWED_EMAIL']);

    //Locations
    SetUserParm($sta["login"], "LOC_LISTING_ID", $sta["LOC_LISTING_ID"]);
    SetUserParm($sta["login"], "LOC_DEFAULT", $sta["LOC_DEFAULT"]);
}

?>
