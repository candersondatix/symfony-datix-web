<?php

$progNotesEditField = Forms_SelectFieldFactory::createSelectField('PROG_NOTES_EDIT', 'ADM', $staff['PROG_NOTES_EDIT'], $FormType);
$progNotesEditField->setCustomCodes(array('OWN' => 'Edit own notes only', 'ALL' => 'Edit all notes'));
$progNotesEditField->setSuppressCodeDisplay();

$FormArray = array(
    'Parameters' => array(
        'Panels' => true,
        'Condition' => false
    ),
    'details' => array(
        'Title' => 'User details',
        'Rows' => array(
            'login',
            array('Name' => 'sta_domain', 'Condition' => ($staff["sta_sid"] != "")),
            array('Name' => 'sta_sid', 'Condition' => ($staff["sta_sid"] != "")),
            array('Name' => 'initials', 'ReadOnly' => ($staff['recordid'])),
            array('Name' => 'password', 'Condition' => ($staff["sta_sid"] == "")),
            array('Name' => 'retype_password', 'Condition' => ($staff["sta_sid"] == "")),
            'sta_pwd_change',
            'sta_daccessstart',
            'sta_daccessend',
            'lockout',
            'con_staff_include',
            array('Name' => 'sta_lockout_dt', 'ReadOnly' => true),
            array('Name' => 'sta_lockout_reason', 'ReadOnly' => true),
        )
    ),
    'contact' => array(
        'Title' => 'Contact details',
        'Rows' => array(
            "con_title",
            "con_forenames",
            "con_surname",
            "con_jobtitle",
            "con_organisation",
            "con_email",
            "con_address",
            "con_postcode",
            "con_gender",
            "con_dopened",
            "con_dclosed",
            "con_dob",
            "con_dod",
            "con_tel1",
            "con_tel2",
            "con_fax",
            "con_number",
            "con_nhsno",
            "con_police_number",
            "con_ethnicity",
            "con_language",
            "con_disability",
            "con_type",
            "con_subtype",
            "con_notes",
            "con_work_alone_assessed",
        )
    ),
    'employee' => array(
        'Title' => _tk('employee_user_title'),
        'Rows' => array(
            'con_orgcode',
            'con_unit',
            'con_clingroup',
            'con_directorate',
            'con_specialty',
            'con_loctype',
            'con_locactual',
            'con_empl_grade',
            'con_payroll',
            array('Name' => 'con_hier_location', 'Type' => 'custom', 'HTML' => getLocationsControlAdmin('con_hier_location', $staff['con_hier_location'], 'ADM', $FormType)),
        )
    ),
    'profile' => array(
        'Title' => 'Profile',
        'Condition' => ($FormType!='Search'),
        "NoFieldAdditions" => true,
        'Rows' => array(
            array('Name' => 'sta_profile', 'Title' => 'Profile', 'Type' => 'custom', 'HTML' => MakeProfileField($staff['sta_profile'], $FormType))
        )
    ),
    'parameters' => array(
        'Title' => 'Configuration parameters',
        'Condition' => ($FormType!='Search'),
        'Rows' => array()
    ),
    'ALL_MODULES_parameters' => array(
        'Title' => 'All Modules settings',
        'Condition' => ($FormType!='Search'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'LOGIN_DEFAULT_MODULE',
            'LOGOUT_DEFAULT_MODULE',
            'CUSTOM_REPORT_BUILDER',
            'ADDITIONAL_REPORT_OPTIONS',
            array('Type' => 'formfield', 'Name' => 'PROG_NOTES_EDIT', 'FormField' => $progNotesEditField),
            'ENABLE_GENERATE',
            'ENABLE_BATCH_DELETE',
            'ENABLE_BATCH_UPDATE'
        )
    ),
    'ACT_parameters' => array(
        'Title' => $ModuleDefs['ACT']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('ACT')),
        "NoFieldRemoval" => true,
        "NoFieldAdditions" => true,
        'Rows' => array(
            'ACT_DEFAULT',
            'ACT_LISTING_ID',
            'ACT_SEARCH_DEFAULT',
            'ACT_OWN_ONLY',
            'ACT_SHOW_AUDIT',
            'ACT_SETUP',
            'DELETE_ACT_CHAIN',
            'ACT_DELETE_DOCS'
        )
    ),
    'INC_parameters' => array(
        'Title' => $ModuleDefs['INC']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('INC')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            array('Name' => 'DIF1_ONLY_FORM', 'OverrideFormDesignGlobal' => true),
            'DIF2_DEFAULT',
            'INC_LISTING_ID',
            'DIF2_SEARCH_DEFAULT',
            'DIF_SHOW_REJECT_BTN',
            'DIF2_HIDE_CONTACTS',
            'USER_GRANT_ACCESS',
            'DIF_OWN_ONLY',
            'INC_SHOW_AUDIT',
            'INC_SETUP',
            'COPY_INCIDENTS',
            'INC_DELETE_DOCS',
            'INC_SAVED_QUERIES_HOME_SCREEN',
            [
                'Name'=>'INC_SAVED_QUERIES',
                'Title' => _tk('choose_saved_queries'),
                'Type' => 'custom',
                'HTML' => MakeIncSavedQueriesSetupField($staff['INC_SAVED_QUERIES'], NULL, $FormType)
            ]
        )
    ),
    'RAM_parameters' => array(
        'Title' => $ModuleDefs['RAM']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('RAM')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'RISK2_DEFAULT',
            'RAM_LISTING_ID',
            'RISK2_SEARCH_DEFAULT',
            'RISK_OWN_ONLY',
            'RAM_SHOW_AUDIT',
            'RAM_SETUP',
            'COPY_RISKS',
            'RAM_DELETE_DOCS'
        )
    ),
	'POL_parameters' => array(
		'Title' => $ModuleDefs['POL']['NAME'] .' settings',
		'Condition' => ($FormType!='Search' && ModIsLicensed('POL')),
		"NoFieldAdditions" => true,
		"NoFieldRemoval" => true,
		'Rows' => array(
			'POL2_DEFAULT',
			'POL_LISTING_ID',
			'POL2_SEARCH_DEFAULT',
			'POL_OWN_ONLY',
			'POL_SHOW_AUDIT',
			'POL_SETUP',
			'COPY_POL',
			'POL_DELETE_DOCS'
		)
	),
    'PAL_parameters' => array(
        'Title' => $ModuleDefs['PAL']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('PAL')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'PAL2_DEFAULT',
            'PAL_LISTING_ID',
            'PAL2_SEARCH_DEFAULT',
            'PAL_OWN_ONLY',
            'PAL_SHOW_AUDIT',
            'PAL_SETUP',
            'PAL_DELETE_DOCS'
        )
    ),
    'COM_parameters' => array(
        'Title' => $ModuleDefs['COM']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('COM')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'COM_SHOW_ADD_NEW',
            'COM2_DEFAULT',
            'COM_LISTING_ID',
            'COM2_SEARCH_DEFAULT',
            'COM_OWN_ONLY',
            'COM_SHOW_AUDIT',
            'COM_SETUP',
            'COM_DELETE_DOCS'
        )
    ),
    'CLA_parameters' => array(
        'Title' => $ModuleDefs['CLA']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('CLA')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'CLAIM1_DEFAULT',
            'CLAIM2_DEFAULT',
            'CLA_LISTING_ID',
            'CLAIM2_SEARCH_DEFAULT',
            'CLA_OWN_ONLY',
            'CLA_SHOW_AUDIT',
            'CLA_SETUP',
            'CLA_DELETE_DOCS'
        )
    ),
    'PAY_parameters' => array(
        'Title' => $ModuleDefs['PAY']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('PAY')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'PAY2_SEARCH_DEFAULT',
            'DELETE_PAY',
        )
    ),
    'CON_parameters' => array(
        'Title' => $ModuleDefs['CON']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('CON')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'CON_DEFAULT',
            'CON_LISTING_ID',
            'CON_SEARCH_DEFAULT',
            'CON_SHOW_REJECT_BTN',
            'CON_SETUP',
            'CON_ALLOW_MERGE_DUPLICATES',
        )
    ),
    'AST_parameters' => array(
        'Title' => $ModuleDefs['AST']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('AST')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'AST_DEFAULT',
            'AST_LISTING_ID',
            'AST_SEARCH_DEFAULT',
            'AST_SETUP',
            'AST_DELETE_DOCS'
        )
    ),
    'MED_parameters' => array(
        'Title' => $ModuleDefs['MED']['NAME'],
        'Condition' => ($FormType!='Search' && bYN(GetParm('MULTI_MEDICATIONS', 'N')) && ModIsLicensed('MED')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'MED_DEFAULT',
            'MED_LISTING_ID',
            'MED_SEARCH_DEFAULT',
            'MED_SETUP',
        )
    ),
    'SAB_parameters' => array(
        'Title' => $ModuleDefs['SAB']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('SAB')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'SAB1_DEFAULT',
            'SAB2_DEFAULT',
            'SAB_LISTING_ID',
            'SAB1_SEARCH_DEFAULT',
            'SAB_SETUP',
            'SAB_DELETE_DOCS'
        )
    ),
    'STN_parameters' => array(
        'Title' => $ModuleDefs['STN']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('STN')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'STN2_DEFAULT',
            'STN_LISTING_ID',
            'STN2_SEARCH_DEFAULT',
            'STN_ADD_NEW',
            'STN_SETUP',
            'COPY_STANDARDS',
            'STN_DELETE_DOCS'
        )
    ),
    'LOC_parameters' => array(
        'Title' => $ModuleDefs['LOC']['NAME'] . ' settings',
        'Condition' => ($FormType != 'Search' && ModIsLicensed('LOC')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'LOC_DEFAULT',
        )
    ),
    'LIB_parameters' => array(
        'Title' => $ModuleDefs['LIB']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('LIB')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'LIB_SETUP',
            'LIB_DELETE_DOCS'
        )
    ),
    'HSA_parameters' => array(
        'Title' => $ModuleDefs['HSA']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('HOT')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'HSA2_DEFAULT',
            'HSA_LISTING_ID',
            'HSA2_SEARCH_DEFAULT'
        )
    ),
    'HOT_parameters' => array(
        'Title' => $ModuleDefs['HOT']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('HOT')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'HOT2_DEFAULT',
            'HOT_LISTING_ID',
            'HOT2_SEARCH_DEFAULT',
            'HOT_DELETE_DOCS'
        )
    ),
    'CQO_parameters' => array(
        'Title' => $ModuleDefs['CQO']['MENU_NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('CQO')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'CQO_LISTING_ID',
            'CQP_LISTING_ID',
            'CQS_LISTING_ID',
            'LOC_LISTING_ID',
            'CQO_DEFAULT',
            'CQP_DEFAULT',
            'CQS_DEFAULT'
        )
    ),
    'ATM_parameters' => array(
        'Title' => $ModuleDefs['AMO']['MENU_NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('ATM')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'ATM_MQ_TEMP',
            'ATM_AAT',
            'ATM_CAI',
            'ASM_MANAGE_CYCLES',
            'ATM_DEFAULT',
            'ATM_LISTING_ID',
            'ATQ_DEFAULT',
            'ATQ_LISTING_ID',
            'ATI_DEFAULT',
            'ATI_LISTING_ID',
            'AMO_DEFAULT',
            'AMO_LISTING_ID',
            'AQU_DEFAULT',
            'AQU_LISTING_ID',
        )
    ),
    'ATM_notifications' => array(
        'Title' => $ModuleDefs['AMO']['MENU_NAME'].' notifications',
        'Condition' => ($FormType!='Search' && ModIsLicensed('ATM')),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            'ATM_STAFF_EMAIL',
            'ATI_LOC_EMAIL',
            'ATI_STAFF_EMAIL',
            'ATI_REVIEWED_EMAIL',
            'AMO_SUBMIT_INSTANCE_EMAIL',
            'ASM_CREATE_INSTANCES_EMAIL',
            'AMO_STAFF_EMAIL'
        )
    ),
    'ADM_parameters' => array(
        'Title' => $ModuleDefs['ADM']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && IsFullAdmin()),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'Rows' => array(
            array('Name' => 'FULL_ADMIN', 'Condition' => IsFullAdmin(), 'Type' => 'yesno'),
            'ADM_DEFAULT',
            'ADM_LISTING_ID',
            array('Name'=>'ADM_PROFILES', 'Title' => 'Profiles', 'Type' => 'custom', 'HTML' => MakeProfileSetupField($staff['ADM_PROFILES'])),
            'ADM_SHOW_AUDIT',
            'ADM_GROUP_SETUP',
            'ADM_VIEW_OWN_USER',
            'ADM_NO_ADMIN_REPORTS'
        )
    ),
    'groups' => array(
        'Title' => 'Security groups',
        'Condition' => ($FormType!='Search'),
        'Include' => 'Source/security/SecurityBase.php',
        'Function' => 'sectionSecurityGroups',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    'loctype' => array(
        'Title' => 'Location/Type settings',
        'Condition' => ($FormType!='Search'),
        'NewPanel' => true,
        'NoTitle' => true,
        'Function' => 'panelLocationTypeSettings',
        "NoFieldAdditions" => true,
        'Rows' => array()
    )
);