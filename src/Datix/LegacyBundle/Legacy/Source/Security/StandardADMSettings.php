<?php
$GLOBALS['FormTitle'] = 'Datix User Form';

$GLOBALS["Show_all_section"] = "N";

$GLOBALS["HideFields"] = array (
    'contact' => true,
    'profile' => true
);

$GLOBALS["MandatoryFields"] = array (
    'login' => 'details',
    'initials' => 'details',
    'con_surname' => 'details',
    'con_jobtitle' => 'details',
    'con_email' => 'details',
);

$GLOBALS["UserExtraText"] = array (
    'LOGIN_DEFAULT_MODULE' => 'Select module to be presented to user upon logging in.',
    'LOGOUT_DEFAULT_MODULE' => 'Select module to be presented to user upon logging out.',
    'CUSTOM_REPORT_BUILDER' => 'Identify whether users will have access to the customised report building section when accessing the reporting area.',
    'ADDITIONAL_REPORT_OPTIONS' => 'This option relates only to those users who have access to the customised report builder.',
    'PROG_NOTES_EDIT' => 'Users can be granted access to edit any progress note, or only those that they have created themselves.',
    'ENABLE_GENERATE' => 'Users can be granted access to generate a new record in another module if available and they have permission to add records.',
    'ACT_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'ACT_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'ACT_OWN_ONLY' => 'If you set this option, this user will only be able to see actions where their name appears in \'Responsibility\'.',
    'DIF2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'DIF2_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'DIF2_HIDE_CONTACTS' => 'If set to Yes, the Contacts side-menu item and linked contacts page will be hidden for this user.  Note: this setting affects only those users with an access level of \'DIF2 read-only access\' or \'DIF2 access only - no review of DIF1 forms\'.',
    'USER_GRANT_ACCESS' => 'Selecting Yes will allow this user to give other users access to specific incident records.',
    'DIF_OWN_ONLY' => 'If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',
    'RISK2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'RISK2_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'RISK_OWN_ONLY' => 'If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',#
    'PAL2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'PAL2_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'PAL_OWN_ONLY' => 'If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',
    'COM2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'COM2_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'COM_OWN_ONLY' => 'If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',
    'CON_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'CON_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'AST_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'AST_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'MED_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'MED_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'SAB1_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'SAB1_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'SAB2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'HSA2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'HSA2_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'HOT2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'HOT2_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'STN2_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'STN2_SEARCH_DEFAULT' => 'This option overrides the setting in \'Design forms\'.',
    'STN_ADD_NEW' => 'This option gives a users the ability to add new records.',
    'ADM_PROFILES' => 'Identify the profiles which the user will be able to apply to users they manage.  Leaving this field blank will give the user access to all profiles',
    'parameters' => 'Any settings made here will override settings selected on the profile selected for this user.',
);

$GLOBALS["HelpTexts"] = array (
    'sta_profile' => 'Any configuration settings made on the user screen will override any settings made on the profile.  This allows you to make specific changes for individual users, while having the majority of their settings based upon a profile',
    'ATI_REVIEWED_EMAIL' => 'E-mails will be sent only for the main location for which the assigned assessment has been created. Users at other locations selected against the assigned assessment will not receive notification.'
);

$GLOBALS["FieldOrders"] = array (
    'details' => array (
        1 => 'login',
        2 => 'initials',
        3 => 'con_title',
        4 => 'con_forenames',
        5 => 'con_surname',
        6 => 'con_jobtitle',
        7 => 'con_email',
    ),
);

$GLOBALS["NewPanels"] = array (
    'loctype' => true,
    'parameters' => true,
    'groups' => true,
);

$GLOBALS["MoveFieldsToSections"] = array (
    'con_title' => array (
        'Original' => 'contact',
        'New' => 'details',
    ),
    'con_forenames' => array (
        'Original' => 'contact',
        'New' => 'details',
    ),
    'con_surname' => array (
        'Original' => 'contact',
        'New' => 'details',
    ),
    'con_jobtitle' => array (
        'Original' => 'contact',
        'New' => 'details',
    ),
    'con_email' => array (
        'Original' => 'contact',
        'New' => 'details',
    )
);

?>