<?php

require_once 'Source/libs/LDAP.php';
$ldap = new datixLDAP();

$progNotesEditField = Forms_SelectFieldFactory::createSelectField('PROG_NOTES_EDIT', 'ADM', $data['PROG_NOTES_EDIT'], $FormType);
$progNotesEditField->setCustomCodes(array('OWN' => 'Edit own notes only', 'ALL' => 'Edit all notes'));
$progNotesEditField->setSuppressCodeDisplay();

$FormArray = array(
    'Parameters' => array(
        'Panels' => true,
        'Condition' => false
    ),
    'details' => array(
        'Title' => 'Profile Details',
        'Rows' => array(
            array('Name' => 'recordid', 'Condition' => ($data['recordid'] != '')),
            'pfl_name',
            'pfl_description',
        )
    ),
    'parameters' => array(
        'Title' => '<span style="float:left;margin-right:5px;">Configuration parameters</span>',
        'TitleSuffix' => ($FormType!='Design' ? ' '.getUserSecTitleDropdown() : ''),
        'Condition' => ($FormType!='Search'),
        'Rows' => array()
    ),
    'ALL_MODULES_parameters' => array(
        'Title' => 'All Modules',
        'NoTitle' => true,
        'Condition' => ($FormType!='Search'),
        'Rows' => array(
            'LOGIN_DEFAULT_MODULE',
            'LOGOUT_DEFAULT_MODULE',
            'CUSTOM_REPORT_BUILDER',
            'ADDITIONAL_REPORT_OPTIONS',
            array('Type' => 'formfield',
                  'Name' => 'PROG_NOTES_EDIT',
                  'FormField' => $progNotesEditField
            ),
            'ENABLE_GENERATE',
            'ENABLE_BATCH_DELETE',
            'ENABLE_BATCH_UPDATE'
        )
    ),
    'ACT_parameters' => array(
        'Title' => $ModuleDefs['ACT']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('ACT')),
        'Rows' => array(
            'ACT_DEFAULT',
            'ACT_LISTING_ID',
            'ACT_SEARCH_DEFAULT',
            'ACT_OWN_ONLY',
            'ACT_SHOW_AUDIT',
            'ACT_SETUP',
            'DELETE_ACT_CHAIN',
            'ACT_DELETE_DOCS'
        )
    ),
    'INC_parameters' => array(
        'Title' => $ModuleDefs['INC']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('INC')),
        'Rows' => array(
            array('Name' => 'DIF1_ONLY_FORM', 'OverrideFormDesignGlobal' => true),
            'DIF2_DEFAULT',
            'INC_LISTING_ID',
            'DIF2_SEARCH_DEFAULT',
            'DIF_SHOW_REJECT_BTN',
            'DIF2_HIDE_CONTACTS',
            'USER_GRANT_ACCESS',
            'DIF_OWN_ONLY',
            'INC_SHOW_AUDIT',
            'INC_SETUP',
            'COPY_INCIDENTS',
            'INC_DELETE_DOCS',
            'INC_SAVED_QUERIES_HOME_SCREEN',
            [
                'Name'=>'INC_SAVED_QUERIES',
                'Title' => _tk('choose_saved_queries'),
                'Type' => 'custom',
                'HTML' => MakeIncSavedQueriesSetupField($data['INC_SAVED_QUERIES'], NULL, $FormType)
            ]
        )
    ),
    'RAM_parameters' => array(
        'Title' => $ModuleDefs['RAM']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('RAM')),
        'Rows' => array(
            'RISK2_DEFAULT',
            'RAM_LISTING_ID',
            'RISK2_SEARCH_DEFAULT',
            'RISK_OWN_ONLY',
            'RAM_SHOW_AUDIT',
            'RAM_SETUP',
            'COPY_RISKS',
            'RAM_DELETE_DOCS'
        )
    ),
    'PAL_parameters' => array(
        'Title' => $ModuleDefs['PAL']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('PAL')),
        'Rows' => array(
            'PAL2_DEFAULT',
            'PAL_LISTING_ID',
            'PAL2_SEARCH_DEFAULT',
            'PAL_OWN_ONLY',
            'PAL_SHOW_AUDIT',
            'PAL_SETUP',
            'PAL_DELETE_DOCS'
        )
    ),
    'COM_parameters' => array(
        'Title' => $ModuleDefs['COM']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('COM')),
        'Rows' => array(
            'COM_SHOW_ADD_NEW',
            'COM2_DEFAULT',
            'COM_LISTING_ID',
            'COM2_SEARCH_DEFAULT',
            'COM_OWN_ONLY',
            'COM_SHOW_AUDIT',
            'COM_SETUP',
            'COM_DELETE_DOCS'
        )
    ),
    'CLA_parameters' => array(
        'Title' => $ModuleDefs['CLA']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('CLA')),
        'Rows' => array(
            'CLAIM1_DEFAULT',
            'CLAIM2_DEFAULT',
            'CLA_LISTING_ID',
            'CLAIM2_SEARCH_DEFAULT',
            'CLA_OWN_ONLY',
            'CLA_SHOW_AUDIT',
            'CLA_SETUP',
            'CLA_DELETE_DOCS'
        )
    ),
    'PAY_parameters' => array(
        'Title' => $ModuleDefs['PAY']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('CLA')),
        'Rows' => array(
            'PAY2_SEARCH_DEFAULT',
            'DELETE_PAY',
        )
    ),
    'CON_parameters' => array(
        'Title' => $ModuleDefs['CON']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('CON')),
        'Rows' => array(
            'CON_DEFAULT',
            'CON_LISTING_ID',
            'CON_SEARCH_DEFAULT',
            'CON_SHOW_REJECT_BTN',
            'CON_SETUP',
            'CON_ALLOW_MERGE_DUPLICATES',
        )
    ),
    'AST_parameters' => array(
        'Title' => $ModuleDefs['AST']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('AST')),
        'Rows' => array(
            'AST_DEFAULT',
            'AST_LISTING_ID',
            'AST_SEARCH_DEFAULT',
            'AST_SETUP',
            'AST_DELETE_DOCS'
        )
    ),
    'SAB_parameters' => array(
        'Title' => $ModuleDefs['SAB']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('SAB')),
        'Rows' => array(
            'SAB1_DEFAULT',
            'SAB2_DEFAULT',
            'SAB_LISTING_ID',
            'SAB1_SEARCH_DEFAULT',
            'SAB_SETUP',
            'SAB_DELETE_DOCS'
        )
    ),
    'STN_parameters' => array(
        'Title' => $ModuleDefs['STN']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('STN')),
        'NoTitle' => true,
        'Rows' => array(
            'STN2_DEFAULT',
            'STN_LISTING_ID',
            'STN2_SEARCH_DEFAULT',
            'STN_ADD_NEW',
            'STN_SETUP',
            'COPY_STANDARDS',
            'STN_DELETE_DOCS'
        )
    ),
    'LOC_parameters' => array(
        'Title' => $ModuleDefs['LOC']['NAME'] . ' settings',
        'Condition' => ($FormType != 'Search' && ModIsLicensed('LOC')),
        'NoFieldAdditions' => true,
        'Rows' => array(
            'LOC_DEFAULT'
        )
    ),
    'LIB_parameters' => array(
        'Title' => $ModuleDefs['LIB']['NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('LIB')),
        "NoFieldAdditions" => true,
        'Rows' => array(
            'LIB_SETUP',
            'LIB_DELETE_DOCS'
        )
    ),
    'MED_parameters' => array(
        'Title' => $ModuleDefs['MED']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && bYN(GetParm('MULTI_MEDICATIONS', 'N')) && ModIsLicensed('MED')),
        'Rows' => array(
            'MED_DEFAULT',
            'MED_LISTING_ID',
            'MED_SEARCH_DEFAULT',
            'MED_SETUP',
        )
    ),
    'HSA_parameters' => array(
        'Title' => $ModuleDefs['HSA']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('HOT')),
        'Rows' => array(
            'HSA2_DEFAULT',
            'HSA_LISTING_ID',
            'HSA2_SEARCH_DEFAULT'
        )
    ),
    'HOT_parameters' => array(
        'Title' => $ModuleDefs['HOT']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('HOT')),
        'Rows' => array(
            'HOT2_DEFAULT',
            'HOT_LISTING_ID',
            'HOT2_SEARCH_DEFAULT',
            'HOT_DELETE_DOCS'
        )
    ),
    'CQO_parameters' => array(
        'Title' => $ModuleDefs['CQO']['MENU_NAME'].' settings',
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('CQO')),
        "NoFieldAdditions" => true,
        'Rows' => array(
            'CQO_LISTING_ID',
            'CQP_LISTING_ID',
            'CQS_LISTING_ID',
            'LOC_LISTING_ID',
            'CQO_DEFAULT',
            'CQP_DEFAULT',
            'CQS_DEFAULT'
        )
    ),
    'ACR_parameters' => array(
        'Title' => $ModuleDefs['AMO']['MENU_NAME'].' settings',
        'Condition' => ($FormType!='Search' && ModIsLicensed('ATM')),
        "NoFieldAdditions" => true,
        'Rows' => array(
            'ATM_MQ_TEMP',
            'ATM_AAT',
            'ATM_CAI',
            'ASM_MANAGE_CYCLES',
            'ATM_DEFAULT',
            'ATM_LISTING_ID',
            'ATQ_DEFAULT',
            'ATQ_LISTING_ID',
            'ATI_DEFAULT',
            'ATI_LISTING_ID',
            'AMO_DEFAULT',
            'AMO_LISTING_ID',
            'AQU_DEFAULT',
            'AQU_LISTING_ID',
        )
    ),
    'ACR_notifications' => array(
        'Title' => $ModuleDefs['AMO']['MENU_NAME'].' notifications',
        'Condition' => ($FormType!='Search' && ModIsLicensed('ATM')),
        "NoFieldAdditions" => true,
        'Rows' => array(
            'ATM_STAFF_EMAIL',
            'ATI_LOC_EMAIL',
            'ATI_STAFF_EMAIL',
            'ATI_REVIEWED_EMAIL',
            'AMO_SUBMIT_INSTANCE_EMAIL',
            'ASM_CREATE_INSTANCES_EMAIL',
            'AMO_STAFF_EMAIL',
        )
    ),
    'ADM_parameters' => array(
        'Title' => $ModuleDefs['ADM']['NAME'],
        'NoTitle' => true,
        'Condition' => ($FormType!='Search' && ModIsLicensed('ADM')),
        'Rows' => array(
            array('Name' => 'FULL_ADMIN', 'Condition' => IsFullAdmin(), 'Type' => 'yesno'),
            'ADM_DEFAULT',
            'ADM_LISTING_ID',
            array('Name'=>'ADM_PROFILES', 'Title' => 'Profiles', 'Type' => 'custom', 'HTML' => MakeProfileSetupField($data['ADM_PROFILES'])),
            'ADM_SHOW_AUDIT',
            'ADM_GROUP_SETUP',
            'ADM_VIEW_OWN_USER',
            'ADM_NO_ADMIN_REPORTS'
        )
    ),
	'POL_parameters' => array(
		'Title' => $ModuleDefs['POL']['NAME'],
		'NoTitle' => true,
		'Condition' => ($FormType!='Search' && ModIsLicensed('POL')),
		'Rows' => array(
			'POL2_DEFAULT',
			'POL_LISTING_ID',
			'POL2_SEARCH_DEFAULT',
			'POL_OWN_ONLY',
			'POL_SHOW_AUDIT',
			'POL_SETUP',
			'COPY_POL',
			'POL_DELETE_DOCS'
		)
	),
    'groups' => array(
        'Title' => 'Security groups',
        'Condition' => ($FormType!='Search'),
        'Include' => 'Source/security/SecurityBase.php',
        'Function' => 'sectionProfileSecurityGroups',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    'LDAP_mappings' => array(
        'Title' => _tk('active_directory_mappings_section'),
        'Condition' => ($ldap->LDAPEnabled() && $FormType!='Search' && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS'),
        'Include' => 'Source/security/SecurityBase.php',
        'Function' => 'sectionProfileLDAP',
        'NoTitle' => true,
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    'users' => array(
        'Title' => 'Users',
        'NotModes' => array('New', 'Search'),
        'Include' => 'Source/security/Profiles.php',
        'Function' => 'sectionProfileUsers',
        "NoFieldAdditions" => true,
        'NewPanel' => true,
        'Rows' => array()
    ),
);

?>