<?php

function saveGroupPermission($grp_id, $permCode, $permValue)
{
	if (empty($permValue))
	{
        DatixDBQuery::PDO_query('DELETE FROM sec_group_permissions WHERE grp_id = :grp_id AND item_code = :item_code', array('grp_id' => $grp_id, 'item_code' => $permCode));
	}
	else
	{
		if ($permValue == "on")
        {
			$permValue = "Y";
        }

		$sql = 'UPDATE sec_group_permissions SET
		perm_value = :perm_value,
		updateddate = :updateddate,
		updatedby = :updatedby
		WHERE
		grp_id = :grp_id
		AND item_code = :item_code';

        $PDOParameters = array(
            'perm_value' => $permValue,
            'updateddate' => date('Y-m-d H:i:s'),
            'updatedby' => $_SESSION['initials'],
            'grp_id' => $grp_id,
            'item_code' => $permCode,
        );

        $query = new DatixDBQuery($sql);
        $query->prepareAndExecute($PDOParameters);

		if ($query->getRowsAffected() == 0)
		{
            DatixDBQuery::PDO_build_and_insert('sec_group_permissions', array('grp_id' => $grp_id, 'item_code' => $permCode, 'perm_value' => $permValue, 'updateddate' => date('Y-m-d H:i:s'), 'updatedby' => $_SESSION['initials']));
		}

		return $permCode;
	}

	return '';
}

function MakeGroupWhereRow($Module, $Where, $FormType, $error = "")
{
	// $QBE : "Query By Example", indicates that we are coming back from
	//            defining a WHERE close from a query by example and we need to
	//            set the "WHERE" clause field for the relevant module

	$QBE = (isset($_SESSION['security_group']) && $_SESSION['security_group']['success'] == true && $_SESSION['security_group']['module'] == $Module);

	$ReadOnly = ($FormType == 'ReadOnly' || $FormType == 'Print');

	$FieldObj = new FormField($FormType);

    // Remove REP_APPROVE WHERE clause if present
    $ModuleWhere = $_SESSION[$Module]["WHERE"];
    $ModuleWhere = ParseSecurityGroupWhere($ModuleWhere);

    if ($ReadOnly)
    {
        $TextAreaField = ($QBE ? $ModuleWhere : $Where);
    }
    else
    {
        $TextAreaField =
            '<textarea name="permissionwhere_' . $Module . '" id="permissionwhere_' . $Module . '" cols="70" rows="7">' .
            ($QBE ? $ModuleWhere : $Where) .
            '</textarea><br />';

        /*=================================================================*/
        /*= The standards module doesn't currently have a search facility =*/
        /*=================================================================*/
        if (($ModuleWhere) && !$QBE)
        {
            $grpWhere = '';

            if (isset($ModuleWhere) && $ModuleWhere != '')
            {
                $grpWhere = $ModuleWhere;
            }

            $TextAreaField .= '<input type="hidden" name="searchwhere_' . $Module . '" id="searchwhere_' . $Module . '" value="' . $grpWhere . '" />';

            $TextAreaField .= '<input type="button" value="Use current search criteria"' .
            ' onclick="javascript:document.getElementById(\'permissionwhere_' . $Module . '\').value = document.getElementById(\'searchwhere_' . $Module . '\').value;">&nbsp;';
        }

        $TextAreaField .= '<input type="submit" value="Define criteria using query by example" onclick="selectAllMultiple(document.getElementById(\'ldap_dn\'));document.frmEditGroup.user_action.value=\'search\'; document.frmEditGroup.search_module.value=\'' . $Module . '\';" />';
    }


    $FieldObj->MakeCustomField($TextAreaField);

    echo GetDivFieldHTML('Criteria for this security group (SQL WHERE clause)'.GetValidationErrors($Data,'permissionwhere_' . $Module),  $FieldObj->GetField(), $error, 25, 'permissionwhere_'.$Module.'_row');
}

function panelGroupDetails($grp, $FormType)
{
	global $ModuleDefs;

	$error = $grp['error'];

	// Need to check for LDAP authentication to display LDAP mapping section
	require_once 'Source/libs/LDAP.php';
	$ldap = new datixLDAP();

    echo '<ol>
    ';

    echo '
        <li class="section_title_row">
            <div class="section_title_group">
            <div class="section_title">' . ($grp["grp_id"] ?  "Security Group Details" : "New Security Group") . '</div>
            </div>
        </li>';

	//$FieldMode = ($FormType == "ReadOnly" ? "Print" : $FormType);
    $FieldMode = $FormType;

    $table = new FormTable($FormType, 'ADM');
    $FieldObj = new FormField($FieldMode);

	$fld['grp_id'] = new FormField('Print');
	$fld['grp_id']->MakeInputField('grp_id', 0, 0, $grp["grp_id"]);
    echo GetDivFieldHTML('Group ID',  $fld['grp_id']->GetField());

    $FieldObj->MakeInputField('grp_code', 60, 254, $grp["grp_code"]);
    echo GetDivFieldHTML('<img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" />Group name',  $FieldObj->GetField(), $error['grp_code']);

	$FieldObj->MakeInputField('grp_description', 60, 254, $grp["grp_description"]);
    echo GetDivFieldHTML('<img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" />Group description',  $FieldObj->GetField(), $error['grp_description']);

	if ($ldap->LDAPEnabled() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS')
	{
		$fld['ldap_dn'] = new FormField($FieldMode);
		$fld['ldap_dn']->MakeInputToMultiListbox("ldap_dn", $grp['ldap_dn']);

		if ($FormType != "ReadOnly" && $FormType != "Print")
			$fld['ldap_dn']->Field .= '<br /><a href="javascript:dnChooserPopup(\'ldap_dnInput\',\'\');">Browse directory</a>';

		$table->MakeRow($error["ldap_dn"] . '<b>Directory groups:</b>', $fld['ldap_dn']);
	}
	//end of LDAP settings

	echo $table->Contents;

	// Group type
	if (!isset($grp['grp_type']) || $grp['grp_type'] == '')
	{
		$grp['grp_type'] = GRP_TYPE_ACCESS;
	}

    $typeOptions = array(
        GRP_TYPE_ACCESS => 'Record access',
        (GRP_TYPE_ACCESS | GRP_TYPE_EMAIL) => 'Record access and e-mail notification',
        GRP_TYPE_EMAIL => 'E-mail notification',
        GRP_TYPE_DENY_ACCESS => 'Deny access');

    $field = Forms_SelectFieldFactory::createSelectField('grp_type', 'ADM', $grp['grp_type'], $FieldMode);
    $field->setCustomCodes($typeOptions);
    $field->setOnChangeExtra('showAccessLevels(jQuery("#grp_type").val())');
    $field->setSuppressCodeDisplay();
    echo GetDivFieldHTML('Group type',  $field->getField(),'','','tr_group_type');

    $ExcludeArray = array('ELE', 'PRO');

    if (!IsFullAdmin())
    {
        $ExcludeArray[] = 'ADM';
    }

    $ModuleNames = array();

	// Select all permissions for security group
	if ($grp["grp_id"] != "")
	{
        $data = DatixDBQuery::PDO_fetch_all('SELECT item_code, perm_value FROM sec_group_permissions WHERE grp_id = :grp_id', array('grp_id' => $grp['grp_id']));

		foreach ($data as $perm_value)
		{
			$grp[\UnicodeString::strtoupper($perm_value["item_code"])] = $perm_value["perm_value"];
		}
	}

    echo '
        <li class="section_title_row">
            <div class="section_title_group" style="margin-right:5px;">
                <div class="section_title">Group security settings</div>
            </div>
            '.
            getModuleDropdown(array(
                    'name' => 'lbModule',
                    'onchange' => 'showModuleSettings(jQuery(\'#lbModule\').val())',
                    'current' => ($grp['default_selected_module'] ? $grp['default_selected_module'] : getCurrentModuleSelect($grp, $ExcludeArray)),
                    'not' => $ExcludeArray
                    ))
            .'
        </li>';
	
	
	echo '<li>';

	foreach ($ModuleDefs as $Module => $Defs)
	{
		if (ModIsLicensed($Module) && !$Defs['DUMMY_MODULE'] && !in_array($Module, $ExcludeArray))
		{
			MakeModuleSection(array(
                'module' => $Module,
				'data' => $grp,
				'form_type' => $FormType
            ));
		}
	}

	echo '</li></ol>';
}

/**
 * Returns current module for module list. 
 * It takes first from top of the $ModuleListOrder which
 * has set criteria afainst it in Group security settings 
 * @param array $data Group security settings
 * @param array $ExcludeArray List of modules
 * @return string MODULE (default INC)
 */
function getCurrentModuleSelect($data, $ExcludeArray)
{
	global $ModuleDefs;
    
	$CurrentModuleSelect = 'INC';
	
	if (isset($data['DIF_PERMS']))
	{
	    return $CurrentModuleSelect;
	}
	
	$ModArray = getModArray($ExcludeArray);
	
	foreach ($ModArray as $Module => $desc)
	{
	    if (isset($data[$ModuleDefs[$Module]['PERM_GLOBAL']]) || isset($data['permission'][$Module]['where']))
	    {
	        return $Module;
	    }
	}

	return $CurrentModuleSelect;
}

function panelProfiles($grp, $FormAction)
{
	$sql = "
        SELECT
        	profiles.recordid,
        	profiles.pfl_name,
        	profiles.pfl_description	
        FROM
        	link_profile_group
        	JOIN profiles ON link_profile_group.lpg_profile = profiles.recordid
        WHERE
        	link_profile_group.lpg_group = $grp[grp_id]
	";

	$ProfileArray = DatixDBQuery::PDO_fetch_all($sql);
	
?>
<tr>
	<td>
	<table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
<?php

	echo '<tr>';
    echo '<td class="windowbg" width="1%"><b>Id</b></td>';
    echo '<td class="windowbg"><b>Name</b></td>';
    echo '<td class="windowbg"><b>Description</b></td>';
	echo '</tr>';

	if (count($ProfileArray) == 0)
	{
		echo '<tr><td class="windowbg2" colspan="7"><b>No profiles.</b></td></tr>';
	}
	else
	{
		foreach ($ProfileArray as $row)
		{
			$url = "$scripturl?action=editprofile&recordid=$row[recordid]&from_grp_id=$grp[grp_id]";
			echo '<tr>';

			echo '<td class="windowbg2"><a href="' . htmlspecialchars($url) . '">' . htmlspecialchars($row['recordid']) . '</a></td>';
			echo '<td class="windowbg2"><a href="' . htmlspecialchars($url) . '">' . htmlspecialchars($row['pfl_name']) . '</a></td>';
			echo '<td class="windowbg2"><a href="' . htmlspecialchars($url) . '">' . htmlspecialchars($row['pfl_description']) . '</a></td>';
			
			echo '</tr>';
		}
	}
	echo '</table></td></tr>';	

}

function panelUsers($grp, $FormAction)
{
	global $dtxdebug;

    require_once 'Source/libs/LDAP.php';
	$ldap = new datixLDAP();
	$LDAPEnabled = $ldap->LDAPEnabled();

    $Where = MakeSecurityWhereClause("sec_staff_group.grp_id = $grp[grp_id]", 'ADM');

	$sql = "SELECT
	staff.con_surname, staff.con_forenames, staff.con_title,
	sec_staff_group.grp_id, sec_staff_group.con_id, sec_staff_group.recordid as link_id
	from
	sec_staff_group
	join staff ON sec_staff_group.con_id = staff.recordid
	WHERE
	$Where
	ORDER BY
	staff.con_surname, staff.con_forenames, staff.con_title";

	$contactarray = DatixDBQuery::PDO_fetch_all($sql);
?>
<tr>
	<td>
	<input type="hidden" name="redirect_url" value="" />
	<script language="javascript">
	function SelectAllUsers(checked)
	{
		var groups = document.getElementsByName('cbSelectUser');
		for (var i = 0; i < groups.length; i++)
		{
			groups[i].checked = checked;
		}
	}

	function CheckSelectAll()
	{
		var checked = true;
		var groups = document.getElementsByName('cbSelectUser');
		if (groups.length == 0)
		{
			checked = false;
		}
		else
		{
			for (var i = 0; i < groups.length; i++)
			{
				if (groups[i].checked == false)
				{
					checked = false;
					break;
				}
			}
		}
		document.getElementById('cbSelectAllUsers').checked = checked;
	}

	function UnlinkSelectedUserGroups()
	{
		var urlGroups = '';
		var groups = document.getElementsByName('cbSelectUser');
		for (var i = 0; i < groups.length; i++)
		{
			if (groups[i].checked == true)
			{
				urlGroups += '&link_id[]=' + groups[i].value;
			}
		}

		if (urlGroups != '')
		{
			urlGroups = urlGroups.substr(1);
			if (confirm('Remove selected user(s) from security group?') == false)
			{
				return;
			}
		}
		else
		{
			alert('No user selected.');
		}

		var oAjaxCall = new Ajax.Request(
			scripturl + '?action=httprequest&type=unlink_user_groups',
			{
				method: 'post',
				postBody: urlGroups + '&type=user',
				onSuccess: function(r)
				{
					var users = document.getElementsByName('cbSelectUser');
					if (users.length > 0)
					{
						for (var i = 0; i < users.length; i++)
						{
							if (users[i].checked == true)
							{
								//var row = groups[i].parentNode.parentNode.removeNode(true);
								var row = users[i].parentNode.parentNode;
								var table = row.parentNode;
								table.deleteRow(row.rowIndex);
								i--;
							}
							if (i >= users.length)
							{
								break;
							}
						}
					}
					CheckSelectAll();

					var users = document.getElementsByName('cbSelectUser');
					if (users.length == 0)
					{
						row = document.getElementById('row_remove_selected_users');
						var table = row.parentNode;
						table.deleteRow(row.rowIndex);
						ele = document.getElementById('link_add_user');
						ele.innerHTML = 'Add a user to this security group';
					}
				},

				onFailure: function()
				{
					alert('Error occurred trying to remove selected users from group.');
				}
			}
		);
	}
	</script>
	<table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
<?php

	echo '<tr>';

	if ($FormAction != "Print" && $FormAction != "ReadOnly")
	{
		echo '<td class="windowbg" width="1%">' .
			'<input type="checkbox" id="cbSelectAllUsers" name="cbSelectAllUsers" onclick="javascript:SelectAllUsers(this.checked);" />' .
			'</td>';
	}

    echo '<td class="windowbg"><b>Name</b></td>';
	echo '</tr>';

	if (count($contactarray) == 0)
	{
		echo '<tr><td class="windowbg2" colspan="7"><b>No users.</b></td></tr>';
	}
	else
	{
		foreach ($contactarray as $row)
		{
			$contactName = "$row[con_surname], $row[con_forenames] $row[con_title]";
			$contactName = \UnicodeString::trim($contactName, ' ,');

			$url = "$scripturl?action=edituser&grp_id=$row[grp_id]&recordid=$row[con_id]";
			$href = $url;

			echo '<tr>';

			if ($FormAction != "Print" && $FormAction != "ReadOnly")
			{
				echo '<td class="windowbg2"><input type="checkbox" ' .
					'id="cbSelectUser" ' .
					'name="cbSelectUser" ' .
					'value="' .  Sanitize::SanitizeInt($row['link_id']) . '" ' .
					'onclick="javascript:CheckSelectAll();"' .
					' /></td>';
			}

			echo '<td class="windowbg2"><a href="' . htmlspecialchars($href) . '">' . htmlspecialchars($contactName) . '</a></td>';

			echo '</tr>';
		}
	}

	if (!($FormAction == "Print" || $FormAction == "ReadOnly"))
	{
		if (count($contactarray) > 0)
		{
			echo '<tr id="row_remove_selected_users"><td class="windowbg2" colspan="5">';
			$href = "javascript:UnlinkSelectedUserGroups()";
			echo '<a href="' . $href . '" ><b>Remove selected user(s) from security group</b></a>';
			echo '</td></tr>';
		}

		echo '<tr><td class="windowbg2" colspan="5">';
		$url = "$scripturl?action=add_user_to_group";
		echo '<a href="JavaScript:document.forms[0].redirect_url.value=\'' .
			$url. '\';selectAllMultiCodes();document.forms[0].submit();" ><b><div id="link_add_user">Add ' . ($num_rows > 0 ? 'another' : 'a') . ' user to this security group</div></b></a>';
		echo '</td></tr>';
	}

	echo '</table></td></tr>';
}

function MakeModuleSection($Options)
{
	global $ModuleDefs, $JSFunctions;

    require_once 'Source/security/SecurityBase.php';

	$Module = $Options['module'];
	$Data = $Options['data'];
	$FormType = $Options['form_type'];

	if (isset($ModuleDefs[$Module]['PERM_GLOBAL']))
    {
        $AccessLevelParam = $ModuleDefs[$Module]['PERM_GLOBAL'];
    }

	echo '<ol id="div' . $Module . '" style="display:none">';

	if (isset($ModuleDefs[$Module]['PERM_GLOBAL']) && !$ModuleDefs[$Module]['NO_ACCESS_LEVEL'])
	{
        $FieldObj = new FormField($FormType);
        echo GetDivFieldHTML('Access level'.GetValidationErrors($Data,'access_level'),  MakeAccessLevelField($FieldObj, $AccessLevelParam, $Module, $Data[$AccessLevelParam]),'<br/><b>Note:</b> where users are linked to more than one security group, the highest access level identified will apply'
        ,'','tr_access_level', !($Data['grp_type'] & GRP_TYPE_ACCESS == GRP_TYPE_ACCESS));
	}

    if ($ModuleDefs[$Module]['DISABLE_SEC_WHERE_CLAUSE'])
    {
        // disabling WHERE clause removes all access to records so e.g. user/group record permissions can be used exclusively to grant access
        if ($Data['permission'][$Module]['where'] == '1=2')
        {
            $value = true;
            $Data['permission'][$Module]['where'] = '';
        }

        $FieldObj->MakeDivCheckbox('disablewhere_'.$Module, '', $value, 'disablewhere_'.$Module, true);

        echo GetDivFieldHTML(
            'Access only where user is selected in User or Group Permissions'.
                AddHelpBubble(array(
                    'field' => 'disablewhere_'.$Module,
                    'title' => 'Access only where user is selected in User or Group Permissions',
                    'help'  => 'If selected, this option will give users access only to those records on which they are identified in the \'User permissions\' or \'Group permissions\' sections. Any where clause text will be ignored.')
                ),
            $FieldObj->GetField()
        );

        $JSFunctions[] = '
            function OnChange_disablewhere_'.$Module.'()
            {
                if (jQuery("#disablewhere_'.$Module.':checked").val() != null)
                {
                    jQuery("#permissionwhere_'.$Module.'_row").hide();
                    jQuery("#permissionwhere_'.$Module.'").attr("disabled", true);
                }
                else
                {
                    jQuery("#permissionwhere_'.$Module.'_row").show();
                    jQuery("#permissionwhere_'.$Module.'").removeAttr("disabled");
                }
            }
            OnChange_disablewhere_'.$Module.'();';
    }

	if (!$ModuleDefs[$Module]['NO_SEC_WHERE_CLAUSE'])
    {
        MakeGroupWhereRow(
		    $Module,
		    $Data['permission'][$Module]['where'],
		    $FormType,
		    $Data["error"]["permissionwhere_" . $Module]
        );
    }

	echo '</ol>';
}

function ParseSecurityGroupWhere($where)
{
	$ret = $where;
	// Remove references to rep_approved
    $ret = preg_replace("/(\band\b\s+)?[a-zA-Z]+_[a-zA-Z]+.rep_approved\s*=\s*'[a-zA-Z]'\s*/iu", "", $ret);
	$ret = preg_replace("/(\band\b\s+)?\brep_approved\b\s*=\s*'[a-zA-Z]'\s*/iu", "", $ret);
	$ret = \UnicodeString::ltrim($ret, ' ');

	if (\UnicodeString::strpos(\UnicodeString::strtolower($ret), 'and ') === 0)
    {
        $ret = \UnicodeString::substr($ret, 4);
    }

    $ret = \UnicodeString::ltrim($ret, ' ');

	// Remove references to submit_stage
	$ret = preg_replace("/(\band\b\s+)?\bsubmit_stage\b\s*=\s*'[0-9]'\s*/iu", "", $ret);
	$ret = \UnicodeString::ltrim($ret, ' ');

	if (\UnicodeString::strpos(\UnicodeString::strtolower($ret), 'and ') === 0)
    {
        $ret = \UnicodeString::substr($ret, 4);
    }

    $ret = \UnicodeString::ltrim($ret, ' ');

	return $ret;
}
function GetGroupModules($GroupID)
{
    global $ModuleDefs;

    $sql = "SELECT item_code FROM sec_group_permissions WHERE grp_id = :grp_id";
    $resultArray = DatixDBQuery::PDO_fetch_all($sql, array("grp_id" => $GroupID));

    $GroupPerms = array();

    foreach ($resultArray as $row)
    {
        $GroupPerms[] = $row["item_code"];
    }

    $GroupModule = array();

    foreach ($ModuleDefs as $Mod => $ModDef)
    {
        if (in_array($ModDef['PERM_GLOBAL'], $GroupPerms))
        {
            $GroupModule[] = $Mod;
        }
    }

    return $GroupModule;
}

?>