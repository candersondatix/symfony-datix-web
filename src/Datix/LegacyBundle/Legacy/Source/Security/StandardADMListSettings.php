<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '2'),
    "sta_surname" => array(
        'width' => '10'),
    "sta_forenames" => array(
        'width' => '10'),
    "login_no_domain" => array(
        'width' => '10'),
    "jobtitle" => array(
        'width' => '15'),
    "email" => array(
        'width' => '10'),
);

?>
