<?php

use src\security\PasswordHasher;
use src\passwords\model\PasswordModelFactory;

function SetUserPassword($user, $password)
{
    $passwordModelFactory = new src\passwords\model\PasswordModelFactory();
    $passwordMapper       = $passwordModelFactory->getMapper();
    $registry             = src\framework\registry\Registry::getInstance();

    // expiration date
    $pwdExpiryDays = $registry->getParm('PWD_EXPIRY_DAYS', PWD_EXPIRY_DAYS);
    $expiryDate    = new DateTime();
    $expiryDate->add (DateInterval::createFromDateString($pwdExpiryDays . ' days'));

    // create password object
    $passwordHasher = new PasswordHasher($registry->getEncryptionType());

    $data = [
        'login'       => $user,
        'password'    => $passwordHasher->hash($password),
        'expire_date' => $expiryDate->format('Y-m-d H:i:s'),
    ];

    $passwordObject = $passwordModelFactory->getEntityFactory()->createObject($data);
    $passwordMapper->setNewPassword($passwordObject);

    // audit trail
    $CurrentUser = $_SESSION["user"];

    if ($user == $CurrentUser)
    {
        AuditLogin($user, "User changed password");
    }
    else
    {
        // Make two entries if the user has changed someone else's p_a_s_s_w_o_r_d
        AuditLogin($user, "Password changed by $CurrentUser");
        AuditLogin($CurrentUser, "User changed password for $user");
    }
}

/**
 * Checks if username/password combination is valid
 * @param string $login
 * @param string $password plain text
 * @return bool
 */
function ValidatePassword($login, $password)
{
    $modelFactory    = new src\passwords\model\PasswordModelFactory();
    $passwordMapper  = $modelFactory->getMapper();

    // Validate login against its own currently saved encryption type
    $currentPassword = $passwordMapper->getCurrentPassword ($login);

    // if PWD_SHARED has changed, there won't be a current password
    if (empty($currentPassword)) {
        return false;
    }

    $passwordHasher = new PasswordHasher($currentPassword["pwd_encryption"]);
    $valid = $passwordHasher->validate($password, $currentPassword['pwd_password']);

    // does password need rehashing?
    $data = [
        'login'       => $login,
        'expire_date' => $currentPassword['pwd_dexpires']
    ];

    $object = $modelFactory->getEntityFactory()->createObject($data);
    
    if ($passwordMapper->shouldPasswordBeRehashed($object))
    {
        // rehash password
        $registry = src\framework\registry\Registry::getInstance();
        $newHasher = new PasswordHasher($registry->getEncryptionType());

        $object->password = $newHasher->hash ($password);
        $passwordMapper->setNewPassword($object);
    }

    return $valid;
}

/**
 * Validates new password
 * @param $username
 * @param $newPassword
 * @param $pwdError
 * @return bool
 */
function checkNewPassword($username, $newPassword, &$pwdError)
{
    $username = EscapeQuotes($username); //make sure apostrophes in username are escaped

    // Retrieve all p_a_s_s_w_o_r_ds related global parameters
	// PWD_EXPIRY_DAYS, PWD_MIN_LENGTH, PWD_UNIQUE
    $pwdParams = DatixDBQuery::PDO_fetch_all('SELECT parameter, parmvalue FROM globals WHERE parameter like \'PWD_%\'');
	foreach ($pwdParams as $pwdParam)
    {
		$pwdParams[$pwdParam["parameter"]] = $pwdParam["parmvalue"];
	}

	// Set to defaults if required
	if ($pwdParams["PWD_UNIQUE"] == '')
    {
		$pwdParams["PWD_UNIQUE"] = PWD_UNIQUE;
    }

	if (!array_key_exists("PWD_MIN_LENGTH", $pwdParams) || $pwdParams["PWD_MIN_LENGTH"] == "" || !$pwdParams["PWD_MIN_LENGTH"] > 0)
    {
		$pwdParams["PWD_MIN_LENGTH"] = PWD_MIN_LENGTH;
    }

    if ($pwdParams["PWD_SHARED"] == '')
    {
		$pwdParams["PWD_SHARED"] = "N";
    }

    if ($pwdParams['PWD_ENCRYPT_TYPE'] == '' || $pwdParams['PWD_ENCRYPT_TYPE'] == 'E' || $pwdParams['PWD_ENCRYPT_TYPE'] == 'SHA1')
    {
        $pwdParams["PWD_ENCRYPT_TYPE"] = 'SHA1';
        $pwdEncryption = array('E', 'SHA1');
    }
    else
    {
        $pwdEncryption = array($pwdParams['PWD_ENCRYPT_TYPE']);
    }


    if ($pwdParams["PWD_LOWERCASE"] == '')
    {
        $pwdParams["PWD_LOWERCASE"] = PWD_LOWERCASE;
    }

    if ($pwdParams["PWD_UPPERCASE"] == '')
    {
        $pwdParams["PWD_UPPERCASE"] = PWD_UPPERCASE;
    }

    if ($pwdParams["PWD_NUMBERS"] == '')
    {
        $pwdParams["PWD_NUMBERS"] = PWD_NUMBERS;
    }

    if ($pwdParams["PWD_SPECIAL"] == '')
    {
        $pwdParams["PWD_SPECIAL"] = PWD_SPECIAL;
    }

    $password_array = \UnicodeString::str_split ($newPassword);

    $CountLowerCase = 0;
    $CountUpperCase = 0;
    $CountNumbers = 0;
    $CountSpecial = 0;
    $bSpacesFound = false;

    // FIXME: this method only works for western characters
    // it shouldn't break with unicode, but it won't count uppercase/lowercase 
    // characters in other languages
    foreach ($password_array as $pwdChar)
    {
        $pwdASCII = ord($pwdChar);

        //Check for lowercase characters
        if ($pwdASCII >= 97 && $pwdASCII <= 122)
        {
            $CountLowerCase++;
        }

        //Check for uppercase characters
        if ($pwdASCII >= 65 && $pwdASCII <= 90)
        {
            $CountUpperCase++;
        }

        //Check for numeric characters
        if ($pwdASCII >= 48 && $pwdASCII <= 57)
        {
            $CountNumbers++;
        }

        //Check for special characters
        if (($pwdASCII >= 33 && $pwdASCII <= 47) OR ($pwdASCII >= 58 && $pwdASCII <= 64) OR
                ($pwdASCII >= 91 && $pwdASCII <= 96) OR ($pwdASCII >= 123 && $pwdASCII <= 255))
        {
            $CountSpecial++;
        }

        if ($pwdASCII == 32)
        {
            $bSpacesFound = true;
        }
    }

    $pwdError['message'] = "Password needs to contain at least:";

    if ($pwdParams["PWD_LOWERCASE"] && $CountLowerCase < $pwdParams["PWD_LOWERCASE"])
    {
        $pwdError['message'] .= "<br/>" . $pwdParams["PWD_LOWERCASE"] . " lowercase character(s).";
        $pwdError['field'] = 'password';
    }

    if ($pwdParams["PWD_UPPERCASE"] && $CountUpperCase < $pwdParams["PWD_UPPERCASE"])
    {
        $pwdError['message'] .= "<br/>" . $pwdParams["PWD_UPPERCASE"] . " uppercase character(s).";
        $pwdError['field'] = 'password';
    }

    if ($pwdParams["PWD_NUMBERS"] && $CountNumbers < $pwdParams["PWD_NUMBERS"])
    {
        $pwdError['message'] .= "<br/>" . $pwdParams["PWD_NUMBERS"] . " numeric character(s).";
        $pwdError['field'] = 'password';
    }

    if ($pwdParams["PWD_SPECIAL"] && $CountSpecial < $pwdParams["PWD_SPECIAL"])
    {
        $pwdError['message'] .= "<br/>" . $pwdParams["PWD_SPECIAL"] . " special character(s).";
        $pwdError['field'] = 'password';
    }

    if ($bSpacesFound === true)
    {
        $pwdError['message'] = _tk('new_pwd_spaces_err');
        $pwdError['field'] = 'password';
    }

    if (!empty($pwdError['field']))
    {
        return false;
    }

    $passwordMapper = (new PasswordModelFactory())->getMapper();

    if($username)
    {
	    if (!$passwordMapper->isNewPasswordUnique($username, $newPassword))
        {
		    if ($pwdParams["PWD_UNIQUE"]=='Y')
		    {
			    $pwdError['message'] = _tk('new_pwd_unique_err');

                if ($pwdParams["PWD_UNIQUE_LAST"])
                {
                    $pwdError['message'] = "New password cannot be the same as any of the last ". $pwdParams["PWD_UNIQUE_LAST"] . " password(s).";
                }

			    $pwdError['field'] = 'password';
                return false;
		    }
	    }
    }

	if (\UnicodeString::strtoupper($newPassword) == "DATIX")
    {
		$pwdError['message'] = _tk('datix_pwd_err');
		$pwdError['field'] = 'password';
		return false;
	}
	elseif ($username == $newPassword)
    {
		$pwdError['message'] = _tk('pwd_user_same_err');
		$pwdError['field'] = 'password';
		return false;
	}

	// Get staff details to check against staff full name
	$fullname = DatixDBQuery::PDO_fetch_all('SELECT fullname FROM staff WHERE login = :login', array('login' => '_'.$username), PDO::FETCH_COLUMN);

    if ($pwdParams["PWD_MIN_LENGTH"] && \UnicodeString::strlen($newPassword) < $pwdParams["PWD_MIN_LENGTH"])
    {
        $pwdError['message'] = "New password must be at least " . $pwdParams["PWD_MIN_LENGTH"] . " characters.";
        $pwdError['field'] = 'password';
        return false;
    }

	if ($fullname == $newPassword)
    {
		$pwdError['message'] = _tk('pwd_fullname_same_err');
		$pwdError['field'] = 'password';
		return false;
	}

	return true;
}

function checkAccessPeriod($username)
{
	$sql = 'select recordid from
	contacts_main
	where
	login = :login
	and (sta_daccessstart is null or sta_daccessstart <= :before)
	and (sta_daccessend is null or sta_daccessend >= :after)';

	$userRecordid = DatixDBQuery::PDO_fetch($sql, array('login' => $username, 'before' => date('Y-m-d 00:00:00'), 'after' => date('Y-m-d 00:00:00')));

	if ($userRecordid)
    {
		return true;
    }

	return false;
}

function GetUserSecurityGroups($con_id, $grp_type = 0)
{
	/*
	returns an array of security group IDs the user is a member of
	join all tables to select only valid user-groups links

	$grp_type = 0 is a bitwise value representing all types of group.

	Current possible values or combination of values are:
	GRP_TYPE_ACCESS == Record access
	GRP_TYPE_EMAIL == E-mail notification
	GRP_TYPE_ACCESS | GRP_TYPE_EMAIL == Record access and e-mail notification
	*/

    if (!\src\framework\registry\Registry::getInstance()->getSessionCache()->exists('security.groups-of-type-'.$grp_type.'-for-user-'.$con_id))
    {
        $sql = 'SELECT l.grp_id
        FROM contacts_main c
        JOIN sec_staff_group l ON c.recordid = l.con_id
        JOIN sec_groups g ON l.grp_id = g.recordid
        WHERE c.recordid = :con_id' .
            (isset($grp_type) ? " AND g.grp_type = (g.grp_type | $grp_type)" : "");

        $groups = DatixDBQuery::PDO_fetch_all($sql, array('con_id' => $con_id), PDO::FETCH_COLUMN);

        $sql = 'SELECT link_profile_group.lpg_group FROM link_profile_group, sec_groups, contacts_main
            WHERE
                sec_groups.recordid = link_profile_group.lpg_group
                AND contacts_main.sta_profile = link_profile_group.lpg_profile
                AND contacts_main.recordid = :con_id' .
            (isset($grp_type) ? " AND sec_groups.grp_type = (sec_groups.grp_type | $grp_type)" : "");

        $profilegroups = DatixDBQuery::PDO_fetch_all($sql, array('con_id' => $con_id), PDO::FETCH_COLUMN);

        \src\framework\registry\Registry::getInstance()->getSessionCache()->set('security.groups-of-type-'.$grp_type.'-for-user-'.$con_id, SafelyMergeArrays(array($groups, $profilegroups)));
    }

    return \src\framework\registry\Registry::getInstance()->getSessionCache()->get('security.groups-of-type-'.$grp_type.'-for-user-'.$con_id);
}


/**
 * This method returns the highest access level for current user for the record that matches
 * WhereClause from users security groups 
 * 
 * @param string $param 
 * @param string $module
 * @param int $recordid
 * @return string 
 */
function GetUserHighestAccessLvlForRecord($param, $module, $recordid)
{
	$UserHighestAccessLvlForRecord = '';
	
	$con_id = $_SESSION["contact_login_id"];
	$ModuleDefs = src\framework\registry\Registry::getInstance()->getModuleDefs();
	$AccessLvlDefs = src\framework\registry\Registry::getInstance()->getAccessLvlDefs();
	
	// If the user is member of at least one security group then check highest Access level for record
	$groups = GetUserSecurityGroups($con_id, GRP_TYPE_ACCESS);
	
	if (count($groups))
	{
	    $table = $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE'];
	    
		// access levels for given param
		$currentParamAccessLvlDefs = $AccessLvlDefs[$param];
		
		// highest possible valid access level for given param
		end($currentParamAccessLvlDefs);
		$HighestLvl = key($currentParamAccessLvlDefs);
		
		foreach ($groups as $grp_id)
		{
			$grp_where = MakeGroupSecurityWhereClause($module,$grp_id);

            require_once('Source/libs/SearchSymbols.php');
            $grp_where = TranslateConCode($grp_where, $_SESSION['initials'], $module);
			
			if (!empty($grp_where))
			{
				$grp_where = 'AND ' . $grp_where;
			}
			
			$sql = 'SELECT
					recordid
				FROM
					' . $table . '
				WHERE
					recordid = :recordid ' . $grp_where;
			
			$record = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));
			
			if ($recordid == $record['recordid'])
			{
				$sql = '
				SELECT
					perm_value
				FROM
					sec_group_permissions
				WHERE
					grp_id = :grp_id AND item_code = :param';
					
				$perm_value = \DatixDBQuery::PDO_fetch($sql, array('grp_id' => $grp_id, 'param' => $param));
				$perm_value = $perm_value['perm_value'];

				// if we reach the highest lvl, there is no need to loop any more
				if ($perm_value == $HighestLvl)
				{
					return $HighestLvl;
				}
				
				if ($currentParamAccessLvlDefs[$perm_value]['order'] > $currentParamAccessLvlDefs[$UserHighestAccessLvlForRecord]['order'])
				{
					$UserHighestAccessLvlForRecord = $perm_value;
				}
			}
		}

        //if none of the groups give access, access may be granted by record-level security. In this case we want to return the highest access level
        $recordIds = GetRecordLevelSecurityIds($module, $ModuleDefs[$module]['TABLE'], $con_id);

        if (in_array($recordid, $recordIds))
        {
            return $_SESSION['CurrentUser']->getAccessLevels($module, true)[0];
	    }
	}
	else 
	{
		// return standard cached value
		$UserHighestAccessLvlForRecord = GetParm($param);
	}
	return $UserHighestAccessLvlForRecord;	
}

function GetUserAccessLevels($con_id)
{
	// returns an array of all the access levels set for the user

	global $AccessLvlDefs;

	$userAccessLvls = array();	

	$validParams = array_keys($AccessLvlDefs);

	foreach ($validParams as $param)
	{
		$userAccessLvls[$param] = false;
	}

	// If the user is member of at least one security group then use security groups
	// to determine the access level.
	// Otherwise use the user parameters.

	$groups = GetUserSecurityGroups($con_id);
		
	if (count($groups) > 0)
	{
		$sql =
		'select item_code as parameter, perm_value as [value]
		from sec_group_permissions
		where
		grp_id in (' . implode(', ', $groups) . ')
		AND item_code in (\'' . implode('\', \'', $validParams) . '\')';
	}
	else
	{
		$sql =
		"select p.parameter as parameter, p.parmvalue as [value]
		from user_parms p
		join contacts_main c on p.login = c.login
		where
		c.recordid = $con_id
		AND p.parameter in ('" . implode('\', \'', $validParams) . "')";
	}

	$request = db_query($sql);

	while ($row = db_fetch_array($request))
	{
		
		if (!array_key_exists($row['parameter'], $userAccessLvls) ||
            $AccessLvlDefs[$row['parameter']][$row['value']]['order'] > $AccessLvlDefs[$row['parameter']][$userAccessLvls[$row['parameter']]]['order'])
		{
			$userAccessLvls[$row['parameter']] = $row['value'];
		}
	}
	return $userAccessLvls;
}

// return an array of contacts IDs of the members of the groups
// which IDs have been passed as an array to the function.
// NOTE: This also picks up users in profiles which contain this group.
function GetSecurityGroupUsers($grp_id)
{
	$contacts = array();
	$groups = array();

	if (is_array($grp_id) && !empty($grp_id))
    {
        $groups = $grp_id;
    }
    elseif ($grp_id)
    {
        $groups[] = $grp_id;
    }
	else
    {
        return null;
    }

    $where = new \src\framework\query\Where();
    $where->add((new \src\framework\query\FieldCollection)->field('sec_staff_group.grp_id')->in($groups));
    $where->add((new \src\framework\query\FieldCollection)->field('contacts_main.con_email')->notNull());
    $where->add('OR', (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessstart')->isNull(), (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessstart')->ltEq(GetTodaysDate()));
    $where->add('OR', (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessend')->isNull(), (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessend')->gtEq(GetTodaysDate()));

    $query = (new \src\contacts\model\ContactModelFactory)->getQueryFactory()->getQuery();
    $query->select(['contacts_main.recordid'])
        ->join('sec_staff_group', null, 'INNER', 'contacts_main.recordid = sec_staff_group.con_id');
    $query->where($where)
        ->orderBy(['contacts_main.recordid']);

    list($statement, $parameters) = (new \src\framework\query\SqlWriter)->writeStatement($query);
    $contactIdsFromGroups = DatixDBQuery::PDO_fetch_all($statement, $parameters, PDO::FETCH_COLUMN);

    $where = new \src\framework\query\Where();
    $where->add((new \src\framework\query\FieldCollection)->field('link_profile_group.lpg_group')->in($groups));
    $where->add((new \src\framework\query\FieldCollection)->field('contacts_main.con_email')->notNull());
    $where->add('OR', (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessstart')->isNull(), (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessstart')->ltEq(GetTodaysDate()));
    $where->add('OR', (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessend')->isNull(), (new \src\framework\query\FieldCollection)->field('contacts_main.sta_daccessend')->gtEq(GetTodaysDate()));

    $query = (new \src\contacts\model\ContactModelFactory)->getQueryFactory()->getQuery();
    $query->select(['contacts_main.recordid'])
        ->join('link_profile_group', null, 'INNER', 'contacts_main.sta_profile = link_profile_group.lpg_profile');
    $query->where($where)
        ->orderBy(['contacts_main.recordid']);

    list($statement, $parameters) = (new \src\framework\query\SqlWriter)->writeStatement($query);
    $contactIdsFromProfiles = DatixDBQuery::PDO_fetch_all($statement, $parameters, PDO::FETCH_COLUMN);

	return array_merge($contactIdsFromGroups, $contactIdsFromProfiles);
}

function sectionSecurityGroups($staff, $FormAction)
{
    sectionGenericSecurityGroups('user', $staff, $FormAction);
}

function sectionProfileSecurityGroups($staff, $FormAction)
{
    sectionGenericSecurityGroups('profile', $staff, $FormAction);
}

function sectionGenericSecurityGroups($type, $staff, $FormAction)
{
    global $dtxdebug, $ModuleDefs, $scripturl;

    include_once 'Source/libs/LDAP.php';

    $ldap = new datixLDAP();
    $LDAPEnabled = $ldap->LDAPEnabled();
    
    //set up different strings for different places this will appear
    if($type == 'profile')
    {
        if ($staff['recordid'])
        {
            $sql = "SELECT
            link_profile_group.recordid,
            sec_groups.grp_code, sec_groups.grp_description,
            link_profile_group.lpg_group, link_profile_group.lpg_profile, sec_groups.grp_type
            from sec_groups
            join link_profile_group on sec_groups.recordid = link_profile_group.lpg_group
            WHERE link_profile_group.lpg_profile = $staff[recordid]";

            $resultArray = DatixDBQuery::PDO_fetch_all($sql);

            $num_rows = count($resultArray);
        }
        else
        {
            $num_rows = 0;
        }

        $linkText['add'] = 'Add '.($num_rows > 0 ? 'another' : 'a').' security group to this profile';
        $linkText['add_one'] = 'Add a security group to this profile';
        $linkText['add_many'] = 'Add another security group to this profile';
        $linkText['addlink'] = 'add_group_for_profile';
        $linkText['remove'] = 'Remove selected security group(s) from this profile';
        $linkText['removealert'] = 'Remove selected security group(s) from this profile?';
    }
    else
    {
        if ($staff['recordid'])
        {
            $sql = 'SELECT
            sec_staff_group.recordid,
            sec_groups.grp_code, sec_groups.grp_description,
            sec_staff_group.grp_id, sec_staff_group.con_id, sec_groups.grp_type
            from sec_groups
            join sec_staff_group on sec_groups.recordid = sec_staff_group.grp_id
            WHERE sec_staff_group.con_id = :con_id';

            $resultArray = DatixDBQuery::PDO_fetch_all($sql, array('con_id' => $staff['recordid']));

            $num_rows = count($resultArray);
        }
        else
        {
            $num_rows = 0;
        }

        $linkText['add'] = 'Add this user to ' . ($num_rows > 0 ? 'another' : 'a') . ' security group';
        $linkText['add_one'] = 'Add this user to a security group';
        $linkText['add_many'] = 'Add this user to another security group';
        $linkText['addlink'] = 'add_group_for_user';
        $linkText['remove'] = 'Remove user from selected security group(s)';
        $linkText['removealert'] = 'Remove user from selected security group(s)?';
    }

    ?>
    <script language="javascript">
    function SelectAllGroups(checked)
    {
        var groups = document.getElementsByName('cbSelectGroup');
        for (var i = 0; i < groups.length; i++)
        {
            groups[i].checked = checked;
        }
    }

    function CheckSelectAll()
    {
        var checked = true;
        var groups = document.getElementsByName('cbSelectGroup');
        if (groups.length == 0)
        {
            checked = false;
        }
        else
        {
            for (var i = 0; i < groups.length; i++)
            {
                if (groups[i].checked == false)
                {
                    checked = false;
                    break;
                }
            }
        }
        document.getElementById('cbSelectAllGroups').checked = checked;
    }

    function UnlinkSelectedUserGroups()
    {
        var urlGroups = '&type=<?=$type?>';
        var groups = document.getElementsByName('cbSelectGroup');
        for (var i = 0; i < groups.length; i++)
        {
            if (groups[i].checked == true)
            {
                urlGroups += '&link_id[]=' + groups[i].value;
            }
        }

        if (urlGroups != '')
        {
            urlGroups = urlGroups.substr(1);
            if (confirm('<?=$linkText['removealert']?>') == false)
            {
                return;
            }
        }
        else
        {
            alert('No group selected.');
        }

        var oAjaxCall = new Ajax.Request(
            scripturl + '?action=httprequest&type=unlink_user_groups',
            {
                method: 'post',
                postBody: urlGroups,
                onSuccess: function(r)
                {
                    var groups = document.getElementsByName('cbSelectGroup');
                    if (groups.length > 0)
                    {
                        for (var i = 0; i < groups.length; i++)
                        {
                            if (groups[i].checked == true)
                            {
                                var row = groups[i].parentNode.parentNode;
                                var table = row.parentNode;
                                table.deleteRow(row.rowIndex);
                                i--;
                            }
                            if (i >= groups.length)
                            {
                                break;
                            }
                        }
                    }
                    CheckSelectAll();

                    var users = document.getElementsByName('cbSelectGroup');
                    if (users.length == 0)
                    {
                        row = document.getElementById('row_remove_selected_groups');
                        var table = row.parentNode;
                        table.deleteRow(row.rowIndex);
                        ele = document.getElementById('link_add_group');
                        ele.innerHTML = '<?= $linkText['add_one'] ?>';
                    }

                    <?php if($type == 'user')
                    {
                    	echo 'GetLocationTypePanel();';
                    }
                    ?>
                },

                onFailure: function()
                {
                    alert('Error occurred trying to remove user(s) from selected security groups.');
                }
            }
        );
    }

    function GetLocationTypePanel()
    {
        var oAjaxCall = new Ajax.Request(
            scripturl +    '?action=httprequest&type=get_location_type_panel',
            {
                method: 'post',
                postBody: 'con_id=<?= $staff['con_id'] ?>&module=INC',
                onSuccess: function(r)
                {
                    var locTypePanel = '<table border="0" cellspacing="1" cellpadding="0" width="100%"><tr><td>';
                    locTypePanel += r.responseText;
                    locTypePanel += '</td></tr></table>';

                    document.getElementById('panel-loctype_settings').innerHTML = locTypePanel;

                    currentModule['security_settings'] = 'INC';
                }
            }
        );
    }
    </script>
<?php
    echo '<li>
    <table class="gridlines" cellspacing="1" cellpadding="4" width="99%" align="center" border="0">
    <tr>';

    if ($FormAction != "Print" && $FormAction != "ReadOnly")
    {
        echo '<td class="windowbg" width="1%">' .
            '<input type="checkbox" id="cbSelectAllGroups" name="cbSelectAllGroups" onclick="javascript:SelectAllGroups(this.checked);" />' .
            '</td>';
    }

    echo '<td class="windowbg" width="20%"><b>Name</b></td>';
    echo '<td class="windowbg"><b>Description</b></td>';
    echo '<td class="windowbg"><b>Type</b></td>';
    echo '</tr>';

    if ($num_rows == 0)
    {
        echo '<tr><td class="windowbg2" colspan="7"><b>No security groups.</b></td></tr>';
    }
    else
    {
        foreach ($resultArray as $row)
        {
            if ($FormAction != "Print" && $FormAction != "ReadOnly")
            {
                if ($type == 'user')
                {
                    $href = "$scripturl?action=editgroup&grp_id=$row[grp_id]&con_id=$row[con_id]";
                }
                elseif ($type == 'profile')
                {
                    $href = "$scripturl?action=editgroup&grp_id=$row[lpg_group]&pfl_id=$row[lpg_profile]";
                }
            }

            echo '<tr id="' . htmlspecialchars($row["grp_code"]) . '">';

            if ($FormAction != "Print" && $FormAction != "ReadOnly")
            {
                echo '<td class="windowbg2"><input type="checkbox" ' .
                    'id="cbSelectGroup" ' .
                    'name="cbSelectGroup" ' .
                    'value="' . Sanitize::SanitizeInt($row['recordid']) . '" ' .
                    'onclick="javascript:CheckSelectAll();"' .
                    ' /></td>';
            }
            if ($FormAction != "Print" && $FormAction != "ReadOnly")
            {
                echo '<td class="windowbg2">'.(bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '<a href="javascript:if(CheckChange()){SendTo(\''. htmlspecialchars($href) . '\');}">' : '') . htmlspecialchars($row["grp_code"]) . (bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '</a>' : '') . '</td>';
                echo '<td class="windowbg2">'.(bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '<a href="javascript:if(CheckChange()){SendTo(\''. htmlspecialchars($href) . '\');}">' : '') . htmlspecialchars($row["grp_description"]) . (bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '</a>' : '') . '</td>';
                echo '<td class="windowbg2">'.(bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '<a href="javascript:if(CheckChange()){SendTo(\''. htmlspecialchars($href) . '\');}">' : '');
            }
            else
            {
                echo '<td class="windowbg2">'. htmlspecialchars($row["grp_code"]) . (bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '</a>' : '') . '</td>';
                echo '<td class="windowbg2">'. htmlspecialchars($row["grp_description"]) . (bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '</a>' : '') . '</td>';
                echo '<td class="windowbg2">';
            }
            if ($row['grp_type'] == (GRP_TYPE_ACCESS | GRP_TYPE_EMAIL))
            {
                echo 'Record access and e-mail notification';
            }
            elseif ($row['grp_type'] == GRP_TYPE_ACCESS)
            {
                echo 'Record access';
            }
            elseif ($row['grp_type'] == GRP_TYPE_EMAIL)
            {
                echo 'E-mail notification';
            }
            elseif ($row['grp_type'] == GRP_TYPE_DENY_ACCESS)
            {
                echo 'Deny access';
            }

            echo (bYN(GetParm('ADM_GROUP_SETUP', 'Y')) ? '</a>' : '') . '</td>';

            echo '</tr>';
        }
    }

    if (!($FormAction == "Print" || $FormAction == "ReadOnly"))
    {
        if ($num_rows > 0)
        {
            echo '<tr id="row_remove_selected_groups"><td class="windowbg2" colspan="5">';
            echo '<a href="javascript:if(CheckChange()){UnlinkSelectedUserGroups()}" ><b>'.$linkText['remove'].'</b></a>';
            echo '</td></tr>';
        }

        echo '<tr><td class="windowbg2" colspan="5">';
        $url = $scripturl.'?action='.$linkText['addlink'];

        echo '<a href="JavaScript:if(CheckChange()){document.forms[0].redirect_url.value=\'' .
            $url. '\';selectAllMultiCodes();document.forms[0].submit();}" ><b><div id="link_add_group">'.$linkText['add'].'</div></b></a>';
        echo '</td></tr>';
        echo '<tr><td class="windowbg2" colspan="5">';

        if ($type == 'user')
        {
            echo '<div style="color:red">If the user belongs to any security groups, location/type settings will be disregarded.</div><br>';
            echo '<div style="color:red">Security groups added here will operate in addition to any security groups identified on the profile selected for this user.</div>';
        }

        echo '</td></tr>';
    }

    echo '</table></li>';
}

function GetDefaultPermissionString()
{
	$permissionStr =  (string) (MOD_ADMIN * 256) . ";1";

	$sql = 'SELECT mod_id FROM modules WHERE mod_id NOT IN (' . MOD_ADMIN . ', ' . MOD_ELEMENTS . ', ' . MOD_PROMPTS .')';

	$moduleIds = DatixDBQuery::PDO_fetch_all($sql, array(), PDO::FETCH_COLUMN);

	foreach ($moduleIds as $moduleId)
	{
		// Number: SUB_SETUP = 15 (from main application)
		$permissionStr .= '|' . (string) ($moduleId * 256 + 15) . ';1' . '|' . (string) ($moduleId * 256) . ';3';
	}

	return $permissionStr;
}

function sectionProfileLDAP($profile, $FormAction)
{
    // Need to check for LDAP authentication to display LDAP mapping section
    include_once 'Source/libs/LDAP.php';

    $ldap = new datixLDAP();

    $table = new FormTable($FormAction);

    $FieldMode = ($FormAction == "ReadOnly" ? "Print" : $FormAction);

    echo '
        <li class="section_title_row">
            <div class="section_title_group">
            <div class="section_title">AD Profile mappings</div>
            </div>
        </li>';

    if ($ldap->LDAPEnabled() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS')
    {
        $fld['ldap_dn'] = new FormField($FieldMode);
        $fld['ldap_dn']->MakeInputToMultiListbox("ldap_dn", $profile['ldap_dn']);

        if ($FormAction != "ReadOnly" && $FormAction != "Print")
            $fld['ldap_dn']->Field .= '<br /><a href="javascript:dnChooserPopup(\'ldap_dnInput\',\'\');">Browse directory</a>';

        $table->MakeRow($error["ldap_dn"] . '<b>Directory groups:</b>', $fld['ldap_dn']);
    }

    echo $table->Contents;
}

function ParsePermString($PermString)
{
    $PermElems = explode("|", $PermString);

    $ModuleNo = 0;

    $Modules = GetModuleIdCodeList();

    $PermNos = Array(0 => "ALL",
             1 => "READ",
             2 => "WRITE",
             3 => "NONE");

    foreach ($PermElems as $Elem)
    {
        $ModPerm = explode(";", $Elem);

        // If there are only two elements in this array,
        // it contains the permissions for the module.
        if (!array_key_exists(2, $ModPerm))
        {
            if (($ModPerm[0] & 0xFF) != SUB_SETUP)
            {
                $Module = $ModPerm[0] >> 8;
                $Form = $ModPerm[0] & 0xFF;

                $ModStr = $Modules[$Module];

                $Perm = $PermNos[$ModPerm[1]];
                //$Perm = $ModPerm[1];

                $ModulePerms[$ModStr]["form"][$Form] = $Perm;
                //echo $ModulePerms[$ModStr]["form"][$Form] . "<br />";
            }
            else
            {
                $Module = ($ModPerm[0] & 0xFF00) / 256;
                $ModStr = $Modules[$Module];
                $ModulePerms[$ModStr]["disallow_setup"] = ($ModPerm[1]==1);
            }
        }
        elseif (($ModPerm[0] & 0xFF) == SUB_WHERE)
        {
            $Module = ($ModPerm[0] & 0xFF00) / 256;

            $ModStr = $Modules[$Module];

            $ModulePerms[$ModStr]["secgroup"] = $ModPerm[1];
            $ModulePerms[$ModStr]["seclevel"] = $ModPerm[2];
            $ModulePerms[$ModStr]["where"] = $ModPerm[3];
        }
    }

    return $ModulePerms;
}

function MakeGroupSecurityWhereClause(
    $module,            // module code
    $group_id = "",     // the group id
    $tableSearch = "",  // the table name to be substituted e.g. incidents_main
    $tableReplace = "", // the replacement table name e.g. incidents_report
    $email = false      // Used to specify whether the where clause is to be built for the purpose of sending e-mails
    )
{
    global $ModuleDefs;

    // This function will build the where clause
    // for the specified security group and return it

    $where = array();

    $sql =
    "SELECT permission, grp_type, perm_value as access_level
    from sec_groups
    left join sec_group_permissions on sec_group_permissions.grp_id = sec_groups.recordid and item_code = :item_code
    where
    recordid = $group_id
    AND (grp_type = (grp_type | " . ($email === true ? GRP_TYPE_EMAIL : GRP_TYPE_ACCESS) . ") OR grp_type = " . GRP_TYPE_DENY_ACCESS .
    ($email === false ? " OR grp_type is null" : "") .
    ")";

    $row = DatixDBQuery::PDO_fetch($sql, array('item_code' => $ModuleDefs[$module]['PERM_GLOBAL']));
    if ($row['permission'] != '')
    {
        include_once 'Source/security/SecurityBase.php';

        $Perms = ParsePermString($row['permission']);
        $tempWhere = array();

        if (isset($Perms[$module]['where']) && $Perms[$module]['where'] != '')
        {
            $tempWhere[] = '(' . $Perms[$module]['where'] . ')';
        }
        elseif($row['access_level'] != '' && $Perms[$module]['where'] == '' && !$email && $row['grp_type'] != GRP_TYPE_DENY_ACCESS)
        {
            $tempWhere[] = '1=1';
        }
        
        if ($ModuleDefs[$module]['USES_APPROVAL_STATUSES'] && $row['access_level'] != '' && bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')))
        {
            if (RestrictToOwnOnly($module, $_SESSION['login'], $row['access_level']) && (!bYN(GetParm('EMAIL_IGNORE_OWN_ONLY', 'Y')) || $email == false))
            {
                $tempWhere[] = "(" . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " = '".$_SESSION['initials']."'"
                    . " OR " . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " IS NULL"
                    . " OR " . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " = '')";
            }

            // restrict this subset of records approval statuses available to this access level
            $validApprovalStatuses = \DatixDBQuery::PDO_fetch_all(
                'SELECT code, las_own_createdby, las_own_handler FROM link_access_approvalstatus
                     WHERE module = :module AND access_level = :access_level AND las_workflow = :las_workflow',
                array('module' => $module, 'access_level' => $row['access_level'], 'las_workflow' => GetWorkflowID($module)));

            if (!$email)
            {
                $table = $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE'];
                
                foreach ($validApprovalStatuses as $row)
                {
                    if ($row['code'] != 'NEW')
                    {
                        if (bYN($row['las_own_createdby']) || bYN($row['las_own_handler']))
                        {
                            $lasmodwhere = array();

                            if (bYN($row['las_own_createdby']))
                            {
                                $lasmodwhere[] = $table . ".createdby = '".$_SESSION['initials']."'";
                            }

                            if (bYN($row['las_own_handler']) && $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'])
                            {
                                $lasmodwhere[] = $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " = '".$_SESSION['initials']."'";
                            }
                        }

                        $lasbasewhere = "(".$table.".rep_approved = '$row[code]'";

                        if (!empty($lasmodwhere))
                        {
                            $lasbasewhere .= " AND (".implode(' OR ', $lasmodwhere).")";
                        }

                        $lasbasewhere .= ')';

                        $laswhere[] = $lasbasewhere;
                    }
                }

                if (!empty($laswhere))
                {
                    $laswhereClause = '('.implode(' OR ', $laswhere).')';
                }
                else //user has no access to any approval status in this module and can therefore see no records.
                {
                    $laswhereClause = '(1=2)';
                }

                $tempWhere[] = $laswhereClause;
            }
        }
        
        if (!empty($tempWhere))
        {
            $where[] = '('.implode(' AND ', $tempWhere).')';
        }
    }

    if (!empty($where))
    {
        $returnWhere = implode(' AND ', $where);

        if ($row['grp_type'] == GRP_TYPE_DENY_ACCESS && !$email)
        {
            $returnWhere = ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']) . ".recordid NOT IN (SELECT " . ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']) . ".recordid FROM " . ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']) ." WHERE " . $returnWhere . ")";
        }
    }

    if ($returnWhere)
    {
        $returnWhere = \UnicodeString::strtoupper($returnWhere);
        $tableSearch = \UnicodeString::strtoupper($tableSearch);
        $tableReplace = \UnicodeString::strtoupper($tableReplace);

        // Also replace any occurrences of the table specified
        if ($tableSearch && $tableReplace && $tableSearch <> $tableReplace) {
            $returnWhere = str_replace($tableSearch . ".", $tableReplace . ".", $returnWhere);
        }
    }

    return $returnWhere;
}

function MakeToplevelSecurityWhereClause($module, $con_id, $email = false, $recordid = null)
{
    global $ModuleDefs;

    $where = array();

    $sql = "select initials, login from contacts_main where recordid = :recordid";

    $contact = DatixDBQuery::PDO_fetch($sql, array('recordid' => $con_id));

    $con_initials = $contact['initials'];
    $con_login = $contact['login'];

    //need to add clause controlling access based on link_access_approvalstatus table.
    //(this used to be hard coded within MainXXX.php).
    if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && $recordid != null)
    {
        require_once 'Source/security/SecurityBase.php';
        $userParm = GetUserHighestAccessLvlForRecord('DIF_PERMS', 'INC', $recordid);
    }
    else
    {
        $userAccessLevels = GetUserAccessLevels($con_id);
        $userParm = $userAccessLevels[$ModuleDefs[$module]['PERM_GLOBAL']];
    }
    // If "own only" is set only allow users to see records for which
    // they have been selected as the handler or the handler is blank
    // Risk managers can see everything
    // DIF1 users should be able to see the records they have submitted
    if (RestrictToOwnOnly($module, $con_login, $userParm) && (!bYN(GetParm('EMAIL_IGNORE_OWN_ONLY', 'Y')) || $email == false))
    {
        $where[] = "(" . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " = '$con_initials'"
        . " OR " . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " IS NULL"
        . " OR " . $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " = '')";
    }

    if ($userParm && !$email)
    {
        if (in_array($module, array('INC', 'RAM', 'PAL', 'COM', 'CON', 'HOT', 'CLA'))) //modules that use workflows.
        {
            $sql = 'SELECT code, las_own_createdby, las_own_handler
            FROM link_access_approvalstatus
            WHERE module = :module
            AND access_level = :access_level
            AND las_workflow = :las_workflow';

            $laswhere = array();

            $statuses = DatixDBQuery::PDO_fetch_all($sql, array('module' => $module, 'access_level' => $userParm, 'las_workflow' => GetWorkflowID($module)));

            $statuses[] = array('code' => 'STCL', 'las_own_createdby' => 'Y', 'las_own_handler' => '');

            foreach ($statuses as $row)
            {
                if ($row['code'] != 'NEW')
                {
                    if ((bYN($row['las_own_createdby']) || bYN($row['las_own_handler'])) && $userParm != 'DIF1')
                    {
                        $lasmodwhere = array();

                        if (bYN($row['las_own_createdby']))
                        {
                            $lasmodwhere[] = $ModuleDefs[$module]['TABLE'] . ".createdby = '$con_initials'";
                        }

                        if (bYN($row['las_own_handler']) && $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'])
                        {
                            $lasmodwhere[] = $ModuleDefs[$module]['FIELD_NAMES']['HANDLER'] . " = '$con_initials'";
                        }
                    }

                    $lasbasewhere = "(".$ModuleDefs[$module]['TABLE'].".rep_approved = '$row[code]'";

                    if (!empty($lasmodwhere))
                    {
                        $lasbasewhere .= " AND (".implode(' OR ', $lasmodwhere).")";
                    }

                    $lasbasewhere .= ')';

                    $laswhere[] = $lasbasewhere;
                }
            }

            if (!empty($laswhere))
            {
                $where[] = '('.implode(' OR ', $laswhere).')';
            }
            else //user has no access to any approval status in this module and can therefore see no records.
            {
                $where[] = '(1=2)';
            }
        }
    }


    if (!empty($where))
    {
        $returnWhere = implode(' AND ', $where);
    }

    return $returnWhere;
}

