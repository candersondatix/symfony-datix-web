<?php

include_once('src\src\security\Escaper.php');

use src\security\Escaper;

function MakeRecordSecPanel($module, $table, $recordid, $FormType, $type = 'both')
{
    global $scripturl;

    // Get hold of the current url to be able to go back to it after saving or cancelling
    $url_from = $scripturl . '?';

    $data = Sanitize::SanitizeStringArray($_GET);

    foreach ($data as $key => $value)
    {
        $url_from .= $key . '=' . $value . '&';
    }

    if ($module == 'INC')
    {
        $url_from .= 'panel=permissions_user';
    }
    else
    {
        $url_from .= 'panel=permissions';
    }

    $url_from = Sanitize::SanitizeURL($url_from);

    unset($_SESSION['record_permissions']);
    $_SESSION['record_permissions']['url_from'] = $url_from

?>
<script language="JavaScript">
    function UnlinkSelectedUsers()
    {
        var urlGroups = '';
        var groups = document.getElementsByName('cbSelectUser');

        jQuery.each(jQuery('input[type=checkbox][name=cbSelectUser]:checked'), function(i, checkbox) {urlGroups += '&link_id[]=' + checkbox.value;});

        if (urlGroups != '')
        {
            urlGroups = urlGroups.substr(1);
            if (confirm('Remove selected user(s) from record?') == false)
            {
                return;
            }

            var oAjaxCall = new Ajax.Request(
                scripturl+'?action=editrecordperms&type=user&delete=1&token=<?php echo CSRFGuard::getCurrentToken(); ?>',
                {
                    method: 'post',
                    postBody: urlGroups,
                    onSuccess: function(r)
                    {
                        location.reload();
                    },

                    onFailure: function()
                    {
                        alert('Error occurred trying to remove selected users from record.');
                    }
                }
            );
        }
        else
        {
            alert('No user selected.');
        }

    }
    function UnlinkSelectedGroups()
    {
        var urlGroups = '';
        var groups = document.getElementsByName('cbSelectUser');

        jQuery.each(jQuery('input[type=checkbox][name=cbSelectGroup]:checked'), function(i, checkbox) {urlGroups += '&link_id[]=' + checkbox.value;});

        if (urlGroups != '')
        {
            urlGroups = urlGroups.substr(1);
            if (confirm('Remove selected group(s) from record?') == false)
            {
                return;
            }

            var oAjaxCall = new Ajax.Request(
                scripturl + '?action=editrecordperms&type=group&delete=1&token=<?php echo CSRFGuard::getCurrentToken(); ?>',
                {
                    method: 'post',
                    postBody: urlGroups,
                    onSuccess: function(r)
                    {
                        location.reload();
                    },

                    onFailure: function()
                    {
                        alert('Error occurred trying to remove selected groups from record.');
                    }
                }
            );
        }
        else
        {
            alert('No group selected.');
        }

    }
    </script>
<?php

        echo '
            <input type="hidden" name="record_permissions_url" value="" />
            <input type="hidden" name="url_from" value="' . Escaper::escapeForHTMLParameter($url_from) . '" /><!--Filter moved from here-->
            <table id="tblUsers" class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';

    $UserCanLinkUsers = UserCanLinkUsers(array('module' => $module,'table' => $table,'recordid' => $recordid));
    if($type == 'user' || $type == 'both')
    {
        $UserList = GetRecordAccessUserList(array('module' => $module,'table' => $table,'recordid' => $recordid));

        if (empty($UserList))
        {
            echo '<tr><td class="rowtitlebg" colspan="8">No linked users.</td></tr>';
        }
        else
        {
            echo '<tr><td class="windowbg" width="1%">' .
                '<input type="checkbox" id="cbSelectAllUsers" name="cbSelectAllUsers" onclick="jQuery(\'input[type=checkbox][name=cbSelectUser]\').attr(\'checked\', this.checked);" />' .
                '</td><td><b>Users</b></td></tr>';

            foreach($UserList as $row)
            {
                $contactName = "$row[con_surname], $row[con_forenames] $row[con_title]";
                $contactName = htmlspecialchars(\UnicodeString::trim($contactName, ' ,'));

                if($FormType != "Print" && $FormType != "ReadOnly" && $UserCanLinkUsers)
                {
                    $url = $scripturl . "?action=edituser&recordid=$row[con_id]";
                    $delurl = $scripturl . '?action=editrecordperms&amp;type=user&amp;recordid=' . $row['recordid'];

                    echo '<tr><td width="12px">
                    <input type="checkbox" ' .
                    'name="cbSelectUser" ' .
                    'value="' . Sanitize::SanitizeInt($row['recordid']) . '" ' .
                    'onclick="if(jQuery(\'input[type=checkbox][name=cbSelectUser]\').not(\':checked\').length == 0){jQuery(\'#cbSelectAllUsers\').attr(\'checked\', true)}else{jQuery(\'#cbSelectAllUsers\').attr(\'checked\', false)}"' .
                    ' /></td>
                    <td class="rowtitlebg">
                    <a href="JavaScript:javascript:if(CheckChange()){SendTo(\''.htmlspecialchars($url).'\');}">' .
                    $contactName .
                    '</a></td>';
                    echo '</tr>';
                }
                else
                {
                    echo '<tr><td class="rowtitlebg" colspan="2">' . $contactName . '</td></tr>';
                }
            }
        }

        if ($FormType != "Print" && $FormType != "ReadOnly" && $FormType != "Locked" && $UserCanLinkUsers)
        {
            if(!empty($UserList))
            {
                echo '<tr id="row_remove_selected_users"><td class="windowbg2" colspan="5">';
                echo '<a href="javascript:if(CheckChange()){UnlinkSelectedUsers()}" ><b>Remove selected user(s) from record</b></a>';
                echo '</td></tr>';
            }

            $url = $scripturl . '?action=editrecordperms&amp;type=user&amp;module=' . $module . '&amp;table_name=' . $table . '&amp;link_id=' . $recordid;

            echo '<tr>
            <td class="new_link_row" colspan="8">
            <a href="javascript:if(CheckChange()){SendTo(\''.$url.'\');}">
            <b>Add new user</b></a>
            </td>
            </tr>';
        }
    }
    if($type=='group' || $type == 'both')
    {
        $GroupList = GetRecordAccessGroupList(array('module' => $module,'table' => $table,'recordid' => $recordid));

        if (empty($GroupList))
        {
            echo '<tr><td class="rowtitlebg" colspan="8">No linked groups.</td></tr>';
        }
        else
        {
            echo '<tr><td class="windowbg" width="1%">' .
                '<input type="checkbox" id="cbSelectAllGroups" name="cbSelectAllGroups" onclick="jQuery(\'input[type=checkbox][name=cbSelectGroup]\').attr(\'checked\', this.checked);" />' .
                '</td><td><b>Security Groups</b></td></tr>';
            foreach($GroupList as $row)
            {
                if($FormType != "Print" && $FormType != "ReadOnly" && $UserCanLinkUsers)
                {
                    $url = $scripturl . "?action=editgroup&grp_id=$row[grp_id]";
                    $delurl = $scripturl . '?action=editrecordperms&amp;type=group&amp;recordid=' . $row['recordid'];

                    echo '<tr><td width="12px">
                    <input type="checkbox" ' .
                    'name="cbSelectGroup" ' .
                    'value="' . Sanitize::SanitizeInt($row['recordid']) . '" ' .
                    'onclick="if(jQuery(\'input[type=checkbox][name=cbSelectGroup]\').not(\':checked\').length == 0){jQuery(\'#cbSelectAllGroups\').attr(\'checked\', true)}else{jQuery(\'#cbSelectAllGroups\').attr(\'checked\', false)};"' .
                    ' /></td><td class="rowtitlebg">
                    <a href="javascript:if(CheckChange()){SendTo(\''.htmlspecialchars($url).'\');}">'.
                    htmlspecialchars($row['grp_code']) .
                    '</a></td>';
                    echo '</tr>';
                }
                else
                {
                    echo '<tr><td class="rowtitlebg" colspan="2">' . htmlspecialchars($row['grp_code']) . '</td></tr>';
                }
            }
        }

        if ($FormType != "Print" && $FormType != "ReadOnly" && $FormType != "Locked" && $UserCanLinkUsers)
        {
            if(!empty($GroupList))
            {
                echo '<tr id="row_remove_selected_users"><td class="windowbg2" colspan="5">';
                echo '<a href="javascript:if(CheckChange()){UnlinkSelectedGroups()}" ><b>Remove selected group(s) from record</b></a>';
                echo '</td></tr>';
            }

            $url = $scripturl . '?action=editrecordperms&amp;type=group&amp;module=' . $module . '&amp;table_name=' . $table . '&amp;link_id=' . $recordid;

            echo '<tr>
            <td class="new_link_row" colspan="8">
            <a href="javascript:if(CheckChange()){SendTo(\''.$url.'\');}">
            <b>Add new group</b></a>
            </td>
            </tr>';
        }
    }

    echo '
        </table>';
}

function EditRecordPerms()
{
    global $scripturl, $dtxtitle, $ModuleDefs, $yySetLocation;

    $RecordPerms = array();

    $error = Sanitize::SanitizeString($_GET['error']);

    if ($error != "")
    {
        $RecordPerms['error']['message'] = $error;
    }

    $RecordPerms['delete'] = Sanitize::SanitizeInt($_GET['delete']);
    $RecordPerms['module'] = Sanitize::SanitizeString($_GET['module']);
    $RecordPerms['table_name'] = Sanitize::SanitizeString($_GET['table_name']);
    $RecordPerms['link_id'] = Sanitize::SanitizeInt($_GET['link_id']);
    $RecordPerms['con_id'] = Sanitize::SanitizeInt($_GET['con_id']);
    $RecordPerms['type'] = Sanitize::SanitizeString($_GET['type']);
    $RecordPerms['group_id'] = Sanitize::SanitizeInt($_GET['group_id']);

    if ($RecordPerms['delete']) //delete
    {
        if ($RecordPerms['module'] == 'REP')
        {
            if($RecordPerms['type'] == 'user')
            {
                $sql = "delete from web_packaged_report_users where user_id IN (".implode(', ', $_POST['link_id']).")";
            }
            else if($RecordPerms['type'] == 'group')
            {
                $sql = "delete from web_packaged_report_groups where group_id IN (".implode(', ', $_POST['link_id']).")";
            }
            else if($RecordPerms['type'] == 'profile')
            {
                $sql = "delete from web_packaged_report_profiles where profile_id IN (".implode(', ', $_POST['link_id']).")";
            }
        }
        else
        {
            if($RecordPerms['type'] == 'user')
            {
                $sql = "delete from sec_record_permissions where recordid IN (".implode(', ', $_POST['link_id']).")";
            }
            else if($RecordPerms['type'] == 'group')
            {
                $sql = "delete from sec_group_record_permissions where recordid IN (".implode(', ', $_POST['link_id']).")";
            }
        }

        $result = db_query($sql);
        $yySetLocation = $_SESSION['record_permissions']['url_from'];
        redirectexit();
    }

    if($RecordPerms['module'] == 'REP')
    {
        if($RecordPerms['type'] == 'user')
            $dtxtitle = _tk("user_report_access");
        else if($RecordPerms['type'] == 'group')
            $dtxtitle = _tk("group_report_access");
        else if($RecordPerms['type'] == 'profile')
            $dtxtitle = _tk("profile_report_access");
    }
    else
    {
        if($RecordPerms['type'] == 'user')
            $dtxtitle = _tk("user_record_access");
        else if($RecordPerms['type'] == 'group')
            $dtxtitle = _tk("group_record_access");
    }

    GetPageTitleHTML(array(
         'title' => $dtxtitle,
         'module' => $RecordPerms['module']
         ));

    GetSideMenuHTML(array('module' => $RecordPerms['module']));

    template_header();

    echo '
<form enctype="multipart/form-data" method="post" name="frmRecordPerms" action="'. $scripturl . '?action=saverecordperms">
<input type="hidden" name="recordid" value="' . $RecordPerms['recordid'] . '" />
<input type="hidden" name="link_id" value="' . $RecordPerms['link_id'] . '" />
<input type="hidden" name="module" value="' . $RecordPerms['module'] . '" />
<input type="hidden" name="table_name" value="' . $RecordPerms['table_name']. '" />
<input type="hidden" name="updateid" value="' . $RecordPerms['updateid'] . '" />
<input type="hidden" name="type" value="' . $RecordPerms['type'] . '" />';

if($RecordPerms['group_id'])
{
    echo '
    <input type="hidden" name="group_id" value="' . $RecordPerms['group_id'] . '" />';
}

echo '
<input type="hidden" name="rbWhat" value="save" />';

    RecordPermsSection($RecordPerms);

    echo '<div class="button_wrapper">';

    echo '
            <input type="submit" value="'._tk('btn_save').'" onclick="Javascript:document.frmRecordPerms.rbWhat.value=\'save\'" />
            <input type="submit" value="'._tk('btn_cancel').'" onclick="Javascript:document.frmRecordPerms.rbWhat.value=\'cancel\'" />
';
    if ($RecordPerms['recordid'])
    {
        echo '<input type="submit" value="Delete" onClick="Javascript:frmRecordPerms.rbWhat.value=\'delete\'" />';
    }

    echo '
    </form>';

    footer();

    obExit();
}

function RecordPermsSection($RecordPerms)
{
    $ListArray = array();

    if($RecordPerms['type'] == 'user')
    {
        if ($RecordPerms['module'] == 'REP')
        {
            $sql = "SELECT recordid, initials, fullname, jobtitle as con_jobtitle, sta_forenames as con_forenames,
                        sta_surname as con_surname, sta_title as con_title
                FROM staff
                WHERE ".GetActiveStaffWhereClause()."
                AND recordid NOT IN (SELECT user_id FROM web_packaged_report_users WHERE web_packaged_report_id = :link_id)";
            $parameters = array('link_id' => $RecordPerms['link_id']);
        }
        else
        {
            $sql = "SELECT recordid, initials, fullname, jobtitle as con_jobtitle, sta_forenames as con_forenames,
                        sta_surname as con_surname, sta_title as con_title
                FROM staff
                WHERE ".GetActiveStaffWhereClause()."
                AND recordid NOT IN (SELECT con_id FROM sec_record_permissions WHERE module = :module and link_id = :link_id)
                ORDER BY con_surname, con_forenames";
            $parameters = array('module' => $RecordPerms['module'], 'link_id' => $RecordPerms['link_id']);
        }

        $Contacts = DatixDBQuery::PDO_fetch_all($sql, $parameters);

        foreach ($Contacts as $row)
        {
            $ListArray[$row["recordid"]] = FormatContactNameForList(array('data' => $row));
        }
    }
    else if($RecordPerms['type'] == 'group')
    {
        $GroupList = GetRecordAccessGroupList(array('recordid'=> $RecordPerms['link_id'], 'module' => $RecordPerms['module'], 'table' => $RecordPerms['table_name']));

        foreach($GroupList as $Group)
        {
            if($RecordPerms['group_id'] != $Group['group_id'])
            {
                $CurrentGroups[] = $Group['group_id'];
            }
        }

        $sql = "SELECT recordid as group_id, grp_code
            FROM sec_groups ".
            (!empty($CurrentGroups) ? "WHERE recordid NOT IN (".implode(',',$CurrentGroups).") " : '').
            "ORDER BY grp_code";

        $request = db_query($sql);

        while ($row = db_fetch_array($request))
        {
            $ListArray[$row["group_id"]] = $row["grp_code"];
        }
    }
    else if($RecordPerms['type'] == 'profile')
    {
        $ProfileList = GetRecordAccessProfileList(array('recordid'=> $RecordPerms['link_id'], 'module' => $RecordPerms['module'], 'table' => $RecordPerms['table_name']));

        foreach($ProfileList as $Profile)
        {
            if($RecordPerms['profile_id'] != $Profile['profile_id'])
            {
                $CurrentProfiles[] = $Profile['profile_id'];
            }
        }

        $sql = "SELECT recordid as profile_id, pfl_name
            FROM profiles ".
            (!empty($CurrentProfiles) ? "WHERE recordid NOT IN (".implode(',',$CurrentProfiles).") " : '').
            "ORDER BY pfl_name";

        $request = db_query($sql);

        while ($row = db_fetch_array($request))
        {
            $ListArray[$row["profile_id"]] = $row["pfl_name"];
        }
    }


    $CTable = new FormTable();
    $FieldObj = new FormField();
    if($RecordPerms['module'] == 'REP')
    {
        $CTable->MakeTitleRow("<b>"._tk($RecordPerms['type']."_report_access")."</b>");
    }
    else
    {
        $CTable->MakeTitleRow("<b>"._tk($RecordPerms['type']."_record_access")."</b>");
    }

    if ($RecordPerms['error'])
        $CTable->Contents .= '<tr><td class="windowbg2" colspan="2"><font color="red">' . $RecordPerms['error']["message"] . '</font></td></tr>';

    if($RecordPerms['type'] == 'user')
    {
        $field = Forms_SelectFieldFactory::createSelectField('con_id', $RecordPerms['module'], $RecordPerms['con_id'], '',true);
        $field->setCustomCodes($ListArray);
        $field->setSuppressCodeDisplay();
        $field->setIgnoreMaxLength();
        $CTable->MakeRow("<b>"._tk('add_users')."</b>".GetValidationErrors($RecordPerms,'con_id'), $field);
    }
    else if($RecordPerms['type'] == 'group')
    {
        $field = Forms_SelectFieldFactory::createSelectField('group_id', $RecordPerms['module'], $RecordPerms['group_id'], '', true);
        $field->setCustomCodes($ListArray);
        $field->setSuppressCodeDisplay();
        $field->setIgnoreMaxLength();
        $CTable->MakeRow("<b>"._tk('add_security_groups')."</b>".GetValidationErrors($RecordPerms,'group_id'), $field);
    }
    else if($RecordPerms['type'] == 'profile')
    {
        $field = Forms_SelectFieldFactory::createSelectField('profile_id', $RecordPerms['module'], $RecordPerms['profile_id'], '', true);
        $field->setCustomCodes($ListArray);
        $field->setSuppressCodeDisplay();
        $field->setIgnoreMaxLength();
        $CTable->MakeRow("<b>"._tk('add_profiles')."</b>".GetValidationErrors($RecordPerms,'profile_id'), $field);
    }

    $CTable->MakeTable();

    echo '
        <tr>
            <td>' . $CTable->GetFormTable() . '</td>
        </tr>
    ';
}

//Alternate function for reports, which now use a different set of tables.
function SaveRecordPerms_Report()
{
    global $scripturl, $yySetLocation, $ModuleDefs;

    $RecordPerms = QuotePostArray($_POST);

    if ($RecordPerms["rbWhat"] == 'cancel')
    {
        $yySetLocation = $_SESSION['record_permissions']['url_from'];
        redirectexit();
    }
    else
    {
        if ($RecordPerms['type']=='user')
        {
            if(empty($RecordPerms['con_id']))
            {
                AddValidationMessage('con_id', 'Please select a user');

                $yySetLocation = $scripturl . '?action=editrecordperms&type='.$RecordPerms['type'].'&module='.$RecordPerms['module'].'&table_name='.$RecordPerms['table_name'].'&link_id='.$RecordPerms['link_id'];
                redirectexit();
            }

            foreach($RecordPerms["con_id"] as $ConID)
            {
                // Check whether link already exists between user and record
                $sql = "SELECT *
                FROM web_packaged_report_users
                WHERE
                web_packaged_report_id = {$RecordPerms[link_id]}
                AND user_id = $ConID";

                $request = db_query($sql);

                if ($row = db_fetch_array($request))
                {
                    continue;
                }

                $sql = "insert into web_packaged_report_users (web_packaged_report_id, user_id)
                    values (
                    '$RecordPerms[link_id]',
                    '$ConID')";
                db_query($sql);
            }
        }
        else if($RecordPerms['type']=='group')
        {
            if(empty($RecordPerms['group_id']))
            {
                AddValidationMessage('group_id', 'Please select a group');
                $yySetLocation = $scripturl . '?action=editrecordperms&type='.$RecordPerms['type'].'&module='.$RecordPerms['module'].'&table_name='.$RecordPerms['table_name'].'&link_id='.$RecordPerms['link_id'];
                redirectexit();
            }

            foreach($RecordPerms["group_id"] as $GroupID)
            {
                // Check whether link already exists between user and record
                $sql = "SELECT *
                FROM web_packaged_report_groups
                WHERE
                web_packaged_report_id = {$RecordPerms[link_id]}
                AND group_id = $GroupID";

                $request = db_query($sql);

                if ($row = db_fetch_array($request))
                {
                    continue;
                }

                $sql = "insert into web_packaged_report_groups (web_packaged_report_id, group_id)
                    values (
                    '$RecordPerms[link_id]',
                    '$GroupID')";
                db_query($sql);
            }
        }
        else if($RecordPerms['type']=='profile')
        {
            if(empty($RecordPerms['profile_id']))
            {
                AddValidationMessage('profile_id', 'Please select a profile');
                $yySetLocation = $scripturl . '?action=editrecordperms&type='.$RecordPerms['type'].'&module='.$RecordPerms['module'].'&table_name='.$RecordPerms['table_name'].'&link_id='.$RecordPerms['link_id'];
                redirectexit();
            }

            foreach($RecordPerms["profile_id"] as $ProfileID)
            {
                // Check whether link already exists between user and record
                $sql = "SELECT *
                FROM web_packaged_report_profiles
                WHERE
                web_packaged_report_id = {$RecordPerms[link_id]}
                AND profile_id = $ProfileID";

                $request = db_query($sql);

                if ($row = db_fetch_array($request))
                {
                    continue;
                }

                $sql = "insert into web_packaged_report_profiles (web_packaged_report_id, profile_id)
                    values (
                    '$RecordPerms[link_id]',
                    '$ProfileID')";
                db_query($sql);
            }
        }
    }
}

function SaveRecordPerms()
{
    global $scripturl, $yySetLocation, $ModuleDefs;

    $RecordPerms = QuotePostArray($_POST);

    if($RecordPerms['module'] == 'REP')
    {
        //Hack until this functionality is refactored.
        SaveRecordPerms_Report();
    }
    else
    {
        if ($RecordPerms["rbWhat"] == 'cancel')
        {
            $yySetLocation = $_SESSION['record_permissions']['url_from'];
            redirectexit();
        }
        else if ($RecordPerms["rbWhat"] == 'delete')
        {
            if($RecordPerms['type'] == 'user')
            {
                $sql = "delete from sec_record_permissions where recordid = {$RecordPerms[recordid]}";
            }
            else if($RecordPerms['type'] == 'group')
            {
                $sql = "delete from sec_group_record_permissions where recordid = {$RecordPerms[recordid]}";
            }
        }
        else
        {
            if ($RecordPerms['type']=='user')
            {
                if(empty($RecordPerms['con_id']))
                {
                    AddValidationMessage('con_id', 'Please select a user');

                    $yySetLocation = $scripturl . '?action=editrecordperms&type='.$RecordPerms['type'].'&module='.$RecordPerms['module'].'&table_name='.$RecordPerms['table_name'].'&link_id='.$RecordPerms['link_id'];
                    redirectexit();
                }

                foreach($RecordPerms["con_id"] as $ConID)
                {

                    // Check whether link already exists between user and record
                    $sql = "SELECT *
                    FROM sec_record_permissions
                    WHERE
                    module = '{$RecordPerms[module]}'
                    AND tablename = '{$RecordPerms[table_name]}'
                    AND link_id = {$RecordPerms[link_id]}
                    AND con_id = $ConID";

                    $request = db_query($sql);

                    if ($row = db_fetch_array($request))
                    {
                      continue;
                    }

                    $sql = "insert into sec_record_permissions (module, tablename, link_id, con_id, updatedby, updateddate, updateid)
                        values (
                        '$RecordPerms[module]',
                        '$RecordPerms[table_name]',
                        $RecordPerms[link_id],
                        $ConID,
                        '$_SESSION[initials]',
                        '" . date('d-M-Y H:i:s') . "',
                        '" . GensUpdateID($row["updateid"]) . "')";

                    db_query($sql);
                }
            }
            else if($RecordPerms['type']=='group')
            {
                if(empty($RecordPerms['group_id']))
                {
                    AddValidationMessage('group_id', 'Please select a group');
                    $yySetLocation = $scripturl . '?action=editrecordperms&type='.$RecordPerms['type'].'&module='.$RecordPerms['module'].'&table_name='.$RecordPerms['table_name'].'&link_id='.$RecordPerms['link_id'];
                    redirectexit();
                }

                foreach($RecordPerms["group_id"] as $GroupID)
                {

                    // Check whether link already exists between group and record
                    $sql = "SELECT *
                    FROM sec_group_record_permissions
                    WHERE
                    module = '{$RecordPerms[module]}'
                    AND tablename = '{$RecordPerms[table_name]}'
                    AND link_id = {$RecordPerms[link_id]}
                    AND group_id = $GroupID";

                    $request = db_query($sql);

                    if ($row = db_fetch_array($request))
                    {
                        continue;
                    }

                    $sql = "insert into sec_group_record_permissions (module, tablename, link_id, group_id, updatedby, updateddate, updateid)
                    values (
                    '$RecordPerms[module]',
                    '$RecordPerms[table_name]',
                    $RecordPerms[link_id],
                    $GroupID,
                    '$_SESSION[initials]',
                    '" . date('d-M-Y H:i:s') . "',
                    '" . GensUpdateID($row["updateid"]) . "')";

                    db_query($sql);
                }
            }
        }
    }

    $yySetLocation = $_SESSION['record_permissions']['url_from'];
    redirectexit();
}

function UserCanLinkUsers($aParams)
{
    global $ModuleDefs;

    if($aParams['module'] == 'REP')  // in Reports module (packaged reports) access is always available
    {
        return true;
    }

    // In 'Document Template Administration, if the user is an admin, they will allways be able to link,
    // reguardless of the USER_GRANT_ACCESS setting. @see DW-10896
    if($aParams['module'] == 'TEM' &&  IsSubAdmin(true))
    {
        return true;
    }

    if(GetParm('USER_GRANT_ACCESS') == 'N')  // user permissions explicity remove access except in Reports where access is always available
    {
        return false;
    }

    if($_SESSION["AdminUser"] || GetParm('USER_GRANT_ACCESS') == 'Y') // user permissions explicity grant access
    {
        return true;
    }

    // user permissions must be set to 'Choose' to get to this point. If 'GRANT_ACCESS_PERMISSIONS' is
    // set to 'H', then we can still allow them to edit if they are the handler. Otherwise, return false.

    if(GetParm('GRANT_ACCESS_PERMISSIONS') == 'H')
    {
        $sql = "select ".$ModuleDefs[$aParams['module']]['FIELD_NAMES']['HANDLER']." as handler
        from $aParams[table]
        where recordid = $aParams[recordid]";

        $row = DatixDBQuery::PDO_fetch($sql);

        if($row && $row['handler'] && $row['handler'] == $_SESSION['initials'])
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

function GetRecordAccessUserList($aParams)
{
    $UserList = array();

    if ($aParams['module'] == 'REP')
    {
        $sql = "select c.recordid, c.con_surname, c.con_forenames, c.con_title, c.recordid as con_id
        from contacts_main c
        join web_packaged_report_users p on g.recordid = p.group_id
        where
        p.web_packaged_report_id = :link_id
        order by c.con_surname, c.con_forenames, c.con_title";

        $parameters = array(
            'link_id' => $aParams['recordid']
        );
    }
    else
    {
        $sql = "select p.recordid, c.con_surname, c.con_forenames, c.con_title, c.recordid as con_id
        from contacts_main c
        join sec_record_permissions p on c.recordid = p.con_id
        where
        p.module = :module
        AND p.tablename = :tablename
        and p.link_id = :link_id
        order by c.con_surname, c.con_forenames, c.con_title";

        $parameters = array(
            'module' => $aParams['module'],
            'tablename' => $aParams['table'],
            'link_id' => $aParams['recordid']
        );
    }

    $result = PDO_fetch_all($sql, $parameters);

    foreach ($result as $row)
    {
        $UserList[] = $row;
    }

    return $UserList;
}

function GetRecordAccessGroupList($aParams)
{
    $GroupList = array();

    if ($aParams['module'] == 'REP')
    {
        $sql = "select g.recordid, g.grp_code, g.recordid as grp_id
        from sec_groups g
        join web_packaged_report_groups p on g.recordid = p.group_id
        where
        p.web_packaged_report_id = :link_id
        order by g.grp_code";

        $parameters = array(
            'link_id' => $aParams['recordid']
        );
    }
    else
    {
        $sql = "select p.recordid, p.group_id, g.grp_code, g.recordid as grp_id
        from sec_groups g
        join sec_group_record_permissions p on g.recordid = p.group_id
        where
        p.module = :module
        AND p.tablename = :tablename
        and p.link_id = :link_id
        order by g.grp_code";

        $parameters = array(
            'module' => $aParams['module'],
            'tablename' => $aParams['table'],
            'link_id' => $aParams['recordid']
        );
    }

    $result = PDO_fetch_all($sql,$parameters);

    foreach ($result as $row)
    {
        $GroupList[] = $row;
    }

    return $GroupList;
}

// Only used for reporting
function GetRecordAccessProfileList($aParams)
{
    $ProfileList = array();

    if ($aParams['module'] != 'REP')
    {
        return false;
    }

    $sql = "select p.profile_id, g.pfl_name, g.recordid as grp_id
    from profiles g
    join web_packaged_report_profiles p on g.recordid = p.profile_id
    where
    p.web_packaged_report_id = :link_id
    order by g.pfl_name";


    $result = PDO_fetch_all($sql,
                    array(
                    'link_id' => $aParams['recordid']
                    )
              );

    foreach ($result as $row)
    {
        $ProfileList[] = $row;
    }

    return $ProfileList;
}

