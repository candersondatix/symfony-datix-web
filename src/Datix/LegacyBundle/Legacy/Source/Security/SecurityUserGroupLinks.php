<?php

function UnlinkUserGroups()
{
	if (!empty($_POST['link_id']))
	{
		if (is_array($_POST['link_id']))
		{
			$link_id = implode(',', Sanitize::SanitizeStringArray($_POST['link_id']));
		}
		else
		{
			$link_id = Sanitize::SanitizeString($_POST['link_id']);
		}

        if ($_POST['type'] == 'user')
        {
		    $sql = "delete from sec_staff_group where recordid in ($link_id)";
        }
        elseif ($_POST['type'] == 'profile')
        {
            $sql = "delete from link_profile_group where recordid in ($link_id)";
        }

		db_query($sql);
	}
}

//unlink users from profiles
function UnlinkProfiles()
{
    if (!empty($_POST['link_id']))
    {
        if (is_array($_POST['link_id']))
        {
            $link_id = implode(',', Sanitize::SanitizeStringArray($_POST['link_id']));
        }
        else
        {
            $link_id = Sanitize::SanitizeString($_POST['link_id']);
        }

        DatixDBQuery::PDO_query('UPDATE contacts_main SET sta_profile = NULL WHERE recordid IN ('.$link_id.')');
    }
}

function SectionAddGroup($Link)
{
	$CTable = new FormTable();
	$CTable->MakeTitleRow('<b>Add security group</b>');

    $field = Forms_SelectFieldFactory::createSelectField('grp_id', 'ADM', $Link["grp_id"], 'Edit', true);
    $field->setSelectFunction('getSecurityGroups', array('con_id' => '"'.$Link['con_id'].'"', 'pfl_id' => '"'.$Link['pfl_id'].'"'));
    $field->setIgnoreMaxLength();
	$CTable->MakeRow("$error[doc_type]<b>"._tk('security_group').":</b>", $field);

	$CTable->MakeTable();

	echo $CTable->GetFormTable();
}

function SectionAddUser($Link)
{
    $todayStr = GetTodaysDate();

    $Where = "(login_tries != -1 OR login_tries IS NULL)
    AND initials is not null AND initials != ''
	AND (sta_daccessstart is null or sta_daccessstart <= '$todayStr') AND (sta_daccessend is null or sta_daccessend >= '$todayStr')
	AND ". GetActiveStaffWhereClause() ."
    AND recordid NOT IN (
        SELECT con_id
        FROM sec_staff_group
        WHERE grp_id = $Link[grp_id])";

    $Where = MakeSecurityWhereClause($Where, 'ADM');

    $sql = "SELECT recordid as con_id, con_forenames, con_surname, con_title
    FROM staff
    WHERE $Where
    ORDER BY con_surname, con_forenames, con_title";

    $request = db_query($sql);
    $Users = array();
    while ($row = db_fetch_array($request))
        $Users[$row["con_id"]] = \UnicodeString::trim("$row[con_surname], $row[con_forenames] $row[con_title]");

    $CTable = new FormTable();
    $CTable->MakeTitleRow('<b>Add User</b>');

    $field = Forms_SelectFieldFactory::createSelectField('con_id', 'ADM', $Link["con_id"], '', true);
    $field->setCustomCodes($Users);
    $field->setSuppressCodeDisplay();
    $field->setIgnoreMaxLength();
    $CTable->MakeRow("$error[doc_type]<b>User:</b>", $field);

    $CTable->MakeTable();

    echo '
        <li>
            <ol>' . $CTable->GetFormTable() . '</ol>
        </li>
    ';
}

function SectionAddUserProfile($Link)
{
    $todayStr = GetTodaysDate();

    $Where = "(login_tries != -1 OR login_tries IS NULL)
	AND initials is not null AND initials != ''
	AND (sta_profile IS NULL OR sta_profile = '')
	AND (sta_daccessstart is null or sta_daccessstart <= '$todayStr') AND (sta_daccessend is null or sta_daccessend >= '$todayStr')
	AND ". GetActiveStaffWhereClause();

    $Where = MakeSecurityWhereClause($Where, 'ADM');

	$sql = "SELECT recordid as con_id, con_forenames, con_surname, con_title
	FROM staff
	WHERE $Where
	ORDER BY con_surname, con_forenames, con_title";

	$request = db_query($sql);
    $Users = array();

	while ($row = db_fetch_array($request))
    {
		$Users[$row["con_id"]] = \UnicodeString::trim("$row[con_surname], $row[con_forenames] $row[con_title]");
    }

	$CTable = new FormTable();
	$CTable->MakeTitleRow('<b>Add User</b>');

    $field = Forms_SelectFieldFactory::createSelectField('con_id', 'ADM', '', '', true);
    $field->setCustomCodes($Users);
    $field->setSuppressCodeDisplay();
    $field->setIgnoreMaxLength();
	$CTable->MakeRow("$error[doc_type]<b>User:</b>", $field);

	$CTable->MakeTable();

    echo $CTable->GetFormTable();
}

?>