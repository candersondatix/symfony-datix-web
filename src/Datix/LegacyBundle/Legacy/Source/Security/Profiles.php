<?php

function sectionProfileUsers($profile, $FormAction)
{
    global $dtxdebug, $scripturl;

    require_once 'Source/libs/LDAP.php';
    $ldap = new datixLDAP();
    $LDAPEnabled = $ldap->LDAPEnabled();

    $Where = MakeSecurityWhereClause("sta_profile = $profile[recordid]", 'ADM');

    $sql = "SELECT recordid, con_surname, con_forenames, con_title
    from staff
    WHERE
    $Where
    ORDER BY
    staff.con_surname, staff.con_forenames, staff.con_title";

    $contactarray = DatixDBQuery::PDO_fetch_all($sql);
?>
<tr>
    <td>
    <script language="javascript">

    AlertAfterChange = true;

    function SelectAllUsers(checked)
    {
        var groups = document.getElementsByName('cbSelectUser');
        for (var i = 0; i < groups.length; i++)
        {
            groups[i].checked = checked;
        }
    }

    function CheckSelectAll()
    {
        var checked = true;
        var groups = document.getElementsByName('cbSelectUser');
        if (groups.length == 0)
        {
            checked = false;
        }
        else
        {
            for (var i = 0; i < groups.length; i++)
            {
                if (groups[i].checked == false)
                {
                    checked = false;
                    break;
                }
            }
        }
        document.getElementById('cbSelectAllUsers').checked = checked;
    }

    function UnlinkSelectedUsers()
    {
        var urlGroups = '';
        var groups = document.getElementsByName('cbSelectUser');

        for (var i = 0; i < groups.length; i++)
        {
            if (groups[i].checked == true)
            {
                urlGroups += '&link_id[]=' + groups[i].value;
            }
        }

        if (urlGroups != '')
        {
            urlGroups = urlGroups.substr(1);
            if (confirm('Remove selected user(s) from profile?') == false)
            {
                return;
            }
        }
        else
        {
            alert('No user selected.');
        }

        var oAjaxCall = new Ajax.Request(
            scripturl + '?action=httprequest&type=unlink_profiles',
            {
                method: 'post',
                postBody: urlGroups + '&type=user',
                onSuccess: function(r)
                {
                    var users = document.getElementsByName('cbSelectUser');
                    if (users.length > 0)
                    {
                        for (var i = 0; i < users.length; i++)
                        {
                            if (users[i].checked == true)
                            {
                                //var row = groups[i].parentNode.parentNode.removeNode(true);
                                var row = users[i].parentNode.parentNode;
                                var table = row.parentNode;
                                table.deleteRow(row.rowIndex);
                                i--;
                            }
                            if (i >= users.length)
                            {
                                break;
                            }
                        }
                    }
                    CheckSelectAll();

                    var users = document.getElementsByName('cbSelectUser');
                    if (users.length == 0)
                    {
                        row = document.getElementById('row_remove_selected_users');
                        var table = row.parentNode;
                        table.deleteRow(row.rowIndex);
                        ele = document.getElementById('link_add_user');
                        ele.innerHTML = 'Add a user to this profile';
                    }
                },

                onFailure: function()
                {
                    alert('Error occurred trying to remove selected users from group.');
                }
            }
        );
    }
    </script>
    <table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
<?php
    echo '<tr>';
    if ($FormAction != "Print" )
    {
        echo '<td class="windowbg" width="1%">' .
            '<input type="checkbox" id="cbSelectAllUsers" name="cbSelectAllUsers" onclick="javascript:SelectAllUsers(this.checked);" />' .
            '</td>';
    }
    echo '<td class="windowbg"><b>Name</b></td>';
    echo '</tr>';

    if (count($contactarray) == 0)
    {
        echo '<tr><td class="windowbg2" colspan="7"><b>No users.</b></td></tr>';
    }
    else
    {
        foreach($contactarray as $row)
        {
            $contactName = "$row[con_surname], $row[con_forenames] $row[con_title]";
            $contactName = \UnicodeString::trim($contactName, ' ,');

            $url = "$scripturl?action=edituser&recordid=$row[recordid]&fromprofile=".$profile['recordid'];
            $href = $url;

            echo '<tr>';

            if ($FormAction != "Print" )
            {
                echo '<td class="windowbg2"><input type="checkbox" ' .
                    'id="cbSelectUser" ' .
                    'name="cbSelectUser" ' .
                    'value="' . Sanitize::SanitizeInt($row['recordid']) . '" ' .
                    'onclick="javascript:CheckSelectAll();"' .
                    ' /></td>';
            }
            echo '<td class="windowbg2"><a href="javascript:if(CheckChange()){SendTo(\'' . htmlspecialchars($href) . '\');}">' . htmlspecialchars($contactName) . '</a></td>';

            echo '</tr>';
        }
    }

    if (!($FormAction == "Print" || $FormAction == "ReadOnly"))
    {
        if (count($contactarray) > 0)
        {
            echo '<tr id="row_remove_selected_users"><td class="windowbg2" colspan="5">';
            echo '<a href="javascript:if(CheckChange()){UnlinkSelectedUsers()}" ><b>Remove selected user(s) from profile</b></a>';
            echo '</td></tr>';
        }

        echo '<tr><td class="windowbg2" colspan="5">';
        $url = "$scripturl?action=add_user_to_profile";
        echo '<a href="JavaScript:if(CheckChange()){document.frmProfile.redirect_url.value=\'' .
            $url. '\';selectAllMultiCodes();document.forms[0].submit();}" ><b><div id="link_add_user">Add ' . (count($contactarray) > 0 ? 'another' : 'a') . ' user to this profile</div></b></a>';
        echo '</td></tr>';
    }

    echo '</table></td></tr>';
}

/**
* Retrieves all groups linked to a Profile
*
* @param mixed $ProfileID Profile ID
*
* return array $ProfileGroups Array of Group IDs linked to the profile ID given
*/
function GetProfileGroups($ProfileID)
{
    global $ModuleDefs;

    $sql = "SELECT lpg_group FROM link_profile_group WHERE lpg_profile = :lpg_profile";
    $resultArray = DatixDBQuery::PDO_fetch_all($sql, array("lpg_profile" => $ProfileID));

    foreach($resultArray as $row)
    {
        $ProfileGroups[] = $row["lpg_group"];
    }

    return $ProfileGroups;
}

function CheckProfileIDUsed($ProfileID)
{
    global $ModuleDefs;

    $sql = "SELECT count(*) as ProfileUsed FROM staff WHERE sta_profile = :sta_profile";
    $result = DatixDBQuery::PDO_fetch($sql, array("sta_profile" => $ProfileID));

    if ($result["ProfileUsed"] > 0)
    {
        return true;
    }

    return false;
}