<?php

$America = GetParm("COUNTRY") == "CANADA" || GetParm("COUNTRY") == "USA";

if ($America)
    $formName = "Enquiry/Compliment/Complaint";
else
    $formName = "PALS report";

$EmailText["Notify"]["Subject"] = "Datix $formName Number ".$data["recordid"];

$EmailText["Notify"]["Body"] = "A $formName has been submitted via the Datix web form.

The details are:

Form number: ".$data["recordid"]."

Description:

".$data["pal_synopsis"]."

Please go to $scripturl?action=record&module=PAL&recordid=".$data["recordid"]." to view and approve it.
";


$EmailText["Feedback"]["Subject"] = "DatixWeb feedback message";

$message = "This is a feedback message from $_SESSION[fullname]. PALS form reference is ";
$message .= $data['recordid'];
$message .= ".\nThe feedback is: \n\n";
$message .= "\n\nPlease select this link to view the record: $scripturl?action=record&module=PAL&recordid=$data[recordid]";

$EmailText["Feedback"]["Body"] = $message;


?>