<?php
use src\security\Escaper;

function ShowMedicationForm($med = "", $FormType = "New")
{
	global $scripturl, $dtxdebug;
	global $dtxtitle;
	global $TitleWidth;
	global $ModuleDefs;
    global $txt;

    $LoggedIn = isset($_SESSION["logged_in"]);
    $aReturn = array();

    if(!$FormType)
        $FormType = "New";

    if ($_GET["print"] == 1)
		$FormType = "Print";

    // Not sure if we still need this.
    if ($FormType == "search")
		$FormType = "Search";

    if ($med["med_id"])
    {
        $PrintView = "window.open('{$scripturl}?action=medication&module={$_GET['module']}&link_id={$med['link_id']}&med_id={$med['med_id']}&form_id={$FormID}&print=1&token=".CSRFGuard::getCurrentToken()."')";
    }

    CheckForRecordLocks('MED', $med, $FormType, $sLockMessage);

    $modulename = $ModuleDefs[$_GET['module']]['REC_NAME'];

    // Load AST form settings
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'MED', 'level' => 2, 'form_type' => $FormType));

    $TitleWidth = 20;

	$IDFieldObj = new FormField($FormType);

	if ($FormType == "Search")
    {
		$IDField = '<input type="text" id="recordid" name="recordid" size="5" '.($med['recordid'] ? 'value="'.$med['recordid'].'"' : '').'>';
    }
    elseif (!$med["med_id"])
    {
		$IDField = '<font color="red">No ID assigned yet - adding new medication</font>';
    }
	else
    {
		$IDField = $med["med_id"];
    }

	$IDFieldObj->MakeCustomField($IDField);

    require $ModuleDefs['MED']['BASIC_FORM_FILES'][2];

    // Make up the form
    $MedTable = new FormTable($FormType, 'MED', $FormDesign);
    $MedTable->MakeForm($FormArray, $med, "MED");

    $FormTitle = $FormDesign->FormTitle;

    if($FormType == 'Search')
    {
        $FormTitle .= $txt["search_for_records"];
    }

    getPageTitleHTML(array(
         'title' => $FormTitle,
         'subtitle' => $FormDesign->FormTitleDescr,
         'module' => 'MED'
         ));


    $ButtonGroup = new ButtonGroup();

    if ($FormType == "Search")
    {
        $ButtonGroup->AddButton(array('id' => 'btnSearch', 'name' => 'btnSearch', 'label' => (isset($_SESSION['security_group']['grp_id']) ? 'Continue' : 'Search'), 'onclick' => 'document.frmMedication.rbWhat.value = \'Search\'; submitClicked = true; document.frmMedication.submit()', 'action' => 'SEARCH'));
    }
    else
    {
        if ($med["module"] && $med["link_id"] && !$med["med_id"])
        {
            $ButtonGroup->AddButton(array('id' => 'btnSearch', 'name' => 'btnSearch', 'label' => 'Search for existing medication', 'onclick' => 'MatchExistingMedication(); submitClicked = true; document.frmMedication.submit()', 'action' => 'SEARCH'));
        }

        if($FormType != "ReadOnly" && $FormType != "Locked" && $FormType != "Print")
        {
            $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => (($med['allow_linking'] === true) ? 'Link to ' . $modulename : 'Save'), 'onclick' => 'selectAllMultiCodes();submitClicked = true; if(validateOnSubmit()){document.frmMedication.submit()}', 'action' => 'SAVE'));
        }
    }

    if(!($med['allow_linking'] === true) && $_GET['med_id'] && $_GET['link_id'] && $_GET['module'])
    {
        $ButtonGroup->AddButton(array('id' => 'btnUnlink', 'name' => 'btnUnlink', 'label' => 'Unlink Medication', 'onclick' => 'document.frmMedication.rbWhat.value = \'Unlink\'; submitClicked = true;document.frmMedication.submit()', 'action' => 'DELETE'));
    }

    if (!in_array($FormType, array('ReadOnly', 'Locked', 'Search')))
    {
        $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => getConfirmCancelJavascript('frmMedication'), 'action' => 'CANCEL'));
    }
    else
    {
        $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => 'Cancel', 'onclick' => 'document.frmMedication.rbWhat.value = \'Cancel\'; submitClicked = true;document.frmMedication.submit()', 'action' => 'CANCEL'));
    }

    if ($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) &&!empty($_SESSION['MED']['RECORDLIST']) && (!isset($ModuleDefs['MED']['NO_NAV_ARROWS']) || $ModuleDefs['MED']['NO_NAV_ARROWS'] != true))
    {
        $CurrentIndex = $_SESSION['MED']['RECORDLIST']->getRecordIndex(array('recordid' => $med['recordid']));

        if ($CurrentIndex !== false)
        {
            $ButtonGroup->AddNavigationButtons($_SESSION['MED']['RECORDLIST'], $med['recordid'], 'MED');
        }
    }

    // If we are in "Print" mode, we don't want to display the menu
    if ($FormType != "Print" && $LoggedIn)
    {
        GetSideMenuHTML(array('module' => 'MED', 'recordid' => $med['med_id'], 'table' => $MedTable, 'buttons' => $ButtonGroup));
        template_header();
    }
    else
    {
        template_header_nomenu();
    }

    // TODO: this should be removed when this is refactored into a controller
    ?><script type="text/javascript">
    globals.FormType = '<?php echo $FormType; ?>';
    globals.printSections = <?php echo json_encode($MedTable->printSections); ?>;
    </script><?php

    if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) &&  ! empty($_GET['recordid']) && ($FormType != "Print" && $_GET['print'] != 1))
    {
        ?><script type="text/javascript" src="src/generic/js/panelData.js"></script><?php
    }

    if(($FormType == "Print" || $_GET['print'] == 1))
    {
        ?><script type="text/javascript" src="src/generic/js/print<?php $MinifierDisabled ? '' : '.min' ?>.js"></script><?php
    }

    if ($FormType != "Print" && $FormType != "Search" && $FormType != "ReadOnly" && $FormType != "Locked")

    {
        echo'<script language="JavaScript" type="text/javascript">';
                echo MakeJavaScriptValidation();
                echo '
                var submitClicked = false;
                AlertAfterChange = true;
             </script>';
    }

?>
<script type="text/javascript" language="javascript">
    <!--

    function MatchExistingMedication(submitType) {
        document.frmMedication.rbWhat.value = 'Link';
        var url = '<?= $scripturl . "?action=medicationlinkaction&token=" . CSRFGuard::getCurrentToken() ?>';
        document.frmMedication.target = "wndMatchExisting";
        document.frmMedication.onsubmit = "window.open(url, 'wndMatchExisting', 'dependent,menubar=false,screenX=200,screenY=330,titlebar,scrollbars,resizable')";
        document.frmMedication.submit();
        document.frmMedication.target = "";
        document.frmMedication.rbWhat.value = 'Save';
    }

    //-->
</script>
<?php
    echo '<form method="post" name="frmMedication" id="frmMedication" action="' . $scripturl . '?action=medicationlinkaction"';
    if ($FormType != "Print" && $FormType != "Search")
    {
   		echo ' onsubmit="return (submitClicked &amp;&amp; validateOnSubmit())"';
    }
    echo ' />
<input type="hidden" name="form_type" value="' . $FormType . '" />
<input type="hidden" name="med_id" value="' . $med['med_id'] . '" />
<input type="hidden" name="link_id" value="' . ($med['link_id'] ? $med['link_id'] : Sanitize::SanitizeInt($_GET['link_id'])) . '" />
<input type="hidden" name="module" value="' . Sanitize::getModule($_GET['module']) . '" />
<input type="hidden" name="form_id" value="' . $FormID . '" />
<input type="hidden" name="updateid" value="' . $med['updateid'] . '">
<input type="hidden" name="fromlisting" value="' . Escaper::escapeForHTMLParameter($_GET['fromlisting']) . '" />
<input type="hidden" name="fromsearch" value="' . Escaper::escapeForHTMLParameter($_REQUEST['fromsearch']) . '" />
<input type="hidden" name="qbe_recordid" value="' . Sanitize::SanitizeInt($_GET['qbe_recordid']) . '" />
<input type="hidden" name="qbe_return_module" value="' . Escaper::escapeForHTMLParameter($_GET['qbe_return_module']) . '" />';


    if ($med['error'])
    {
        echo '<div class="form_error">'.$txt["form_errors"].'</div>';
        echo '<div class="form_error">'.$med['error']['message'].'</div>';
    }


    if($sLockMessage)
    {
        echo '
        <div class="lock_message">
            <div id="LockMessage">'.$sLockMessage.'</div>
            <div id="userLockMessage"></div>
        </div>';
    }


    $MedTable->MakeTable();
    echo $MedTable->GetFormTable();

    if ($FormType != "Print")
    {
        echo '<input type="hidden" name="rbWhat" value="Save">';

        echo $ButtonGroup->getHTML();
    }

    echo '</form>';

    if ($FormType != "Print")
    {
        echo JavascriptPanelSelect($Show_all_section, $panel, $MedTable);
    }

    footer();

    obExit();
}

function DoMedicationSection($data, $FormType, $module, $ExtraParams = array())
{
    //need to work out how many medications to display, and then display them.
    $aPostedMedications = getPostedMedications(array('module'=>$module));

    //If medications have been posted to this page, we should use them, as it means an error has occurred when saving.
    if(!empty($aPostedMedications))
    {
        $aMedications = $aPostedMedications;
    }
    else
    {
        $aMedications = getLinkedMedications(array('recordid' => $data['recordid'], 'module'=>$module));
    }

    foreach($aMedications as $id => $LinkedMedication)
    {
        echo getMedicationSectionHTML(array('module'=>$module,'data'=>$LinkedMedication, 'formtype'=>$FormType, 'suffix' => (intval($id) + 1), 'clearsection' => ($id ==0), 'medication_name' => $ExtraParams['medication_name']));
    }

    $NumMedications = count($aMedications);

    if(count($aMedications) == 0) //no linked medication
    {
        echo getMedicationSectionHTML(array('module'=>$module,'data'=>array(), 'formtype'=>$FormType, 'suffix' => 1, 'clearsection'=>true, 'medication_name' => $ExtraParams['medication_name']));
        $NumMedications = 1;
    }

    echo '<script language="javascript">dif1_section_suffix[\''.$ExtraParams['medication_name'].'\'] = '.($NumMedications+1).'</script>';

    if($FormType != 'Print' && $FormType != 'ReadOnly')
    {
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        echo '
            <li class="new_windowbg" id="add_another_'.$ExtraParams['medication_name'].'_button_list">
                <input type="button" id="section_'.$ExtraParams['medication_name'].'_button" value="'._tk('btn_add_another').'" onclick="AddSectionToForm(\''.$ExtraParams['medication_name'].'\',\'\',$(\''.$ExtraParams['medication_name'].'_section_div_\'+(dif1_section_suffix[\''.$ExtraParams['medication_name'].'\']-1)),\''.Sanitize::getModule($module).'\',\'\', true, '.$spellChecker.', '.(isset($_REQUEST['form_id']) && $_REQUEST['form_id'] != '' ? Sanitize::SanitizeInt($_REQUEST['form_id']) : 'null').');">
            </li>';
    }
}

function getLinkedMedications($aParams)
{
    $module = $aParams['module'];
    $recordid = $aParams['recordid'];
    $aMedications = array();

    if($module && is_numeric($recordid))
    {
         if($module == 'INC')
        {
            $Rows = getMedicationFieldsForSave(array('module'=>$module));
            $sql = 'SELECT recordid as imed_id, '.implode(', ',$Rows['Rows']).' FROM INC_MEDICATIONS WHERE inc_id = :recordid ORDER BY recordid';
        }

        $aMedications = PDO_fetch_all($sql, array('recordid' => $recordid));
    }

    return $aMedications;
}

function getPostedMedications($aParams)
{
    $aMedications = array();

    $MaxSuffix = $_POST['inc_medication_max_suffix'];

    for($i=0; $i<$MaxSuffix; $i++)
    {
        $MedicationRecord = array();

        $MedicationFields = getMedicationFieldsForSave(array('module'=>$aParams['module']));

        foreach($MedicationFields['Rows'] as $field)
        {
            if($_POST[$field.'_'.$i])
            {
                $MedicationRecord[$field] = $_POST[$field.'_'.$i];
            }
        }

        if(!empty($MedicationRecord))
        {
            $aMedications[] = $MedicationRecord;
        }
    }

    return $aMedications;
}

function GetMedicationSectionHTML($aParams)
{
    global $JSFunctions, $formlevel;

    if($formlevel == null)
    {
        $formlevel = ($_POST['form_level'] ?: 1);
    }

    $SavedFormDesignSettings = saveFormDesignSettings();
    unsetFormDesignSettings();

    $AJAX = $aParams['ajax'];

    if($aParams['formtype'] == 'Search')
    {
        $aParams['suffix'] = 'search';
    }

    if($AJAX)
    {
        $formlevel = $aParams['level'];
    }

    $aMedicationRows = getBasicMedicationForm($aParams);
    
    $medicationFormDesign = Forms_FormDesign::GetFormDesign(array('module' => $aParams['module'], 'level' => $formlevel));
    $medicationFormDesign = ModifyFormDesignForMedications($medicationFormDesign);

    $aMedicationRowsOrdered = array();
    //Re-order multi-medication fields based on form design.
    if(is_array($medicationFormDesign->FieldOrders['multi_medication']))
    {
        foreach ($medicationFormDesign->FieldOrders['multi_medication'] as $Field )
        {
            $aMedicationRowsOrdered[] = $Field;
        }
    }

    if(is_array($aMedicationRows['multi_medication_record_'.$aParams['suffix']]['Rows']))
    {
        foreach ($aMedicationRows['multi_medication_record_'.$aParams['suffix']]['Rows'] as $i => $RowField)
        {
            if (!in_array($RowField, $aMedicationRowsOrdered))
            {
                $aMedicationRowsOrdered[] = $RowField;
            }
        }
    
        $aMedicationRows['multi_medication_record_'.$aParams['suffix']]['Rows'] = $aMedicationRowsOrdered;
    }
    
    if ($aParams['suffix'])
    {
        $medicationFormDesign->AddSuffixToFormDesign($aParams['suffix']);
        $aParams['data'] = AddSuffixToData($aParams['data'], $aParams['suffix'], getMedicationFieldsForSave(array('module' => $aParams['module'])));
    }

    $oMedicationTable = new FormTable($aParams['formtype'], $aParams['module'], $medicationFormDesign);
    $oMedicationTable->ffTable = 'INCMED';
    $oMedicationTable->MakeForm($aMedicationRows, $aParams['data'], $aParams['module'], array('dynamic_section' => true));

    $html = '';

    if(!($AJAX) && $aParams['formtype'] != 'Print')
    {
        $html .= '<div id="'.$aParams['medication_name'].'_section_div_'.$aParams['suffix'].'">';
    }

    $html .= $oMedicationTable->GetFormTable();
    $html .= '<input type="hidden" name="' . $aParams['medication_name'] . '_link_id_' . $aParams['suffix'] .'" id="' . $aParams['medication_name'] . '_link_id_' . $aParams['suffix'] .'" value="' . $aParams['data']['imed_id'] .'" />';

    if(!($AJAX) && $aParams['formtype'] != 'Print')
    {
        $html .= '</div>';
    }

    loadFormDesignSettings($SavedFormDesignSettings);

    return $html;
}

function getBasicMedicationForm($aParams)
{
    global $ModuleDefs, $FieldDefs;

    $aParams['medication_name'] = $ModuleDefs[$aParams['module']]['MEDICATION_TYPE'];
    $RowList = GetMedicationRowList($aParams);

    if($aParams['formtype'] == 'Print')
    {
        foreach($RowList as $key => $Field)
        {
            if($FieldDefs[$aParams['module']][$Field]['DummyField'] == true)
            {
                unset($RowList[$key]);
            }
        }
    }

    if($aParams['rows'])
    {
        return array('Rows' => $RowList);
    }
    else
    {
        return array(
            'Parameters' => array(
                'Suffix' => $aParams['suffix']
                ),
            "multi_medication_record".($aParams['suffix'] ? '_'.$aParams['suffix'] : '') => array(
                "Title" => 'Medication',
              //  "OrderField" => array('id'=>$aParams['medication_name'].'_listorder_'.$aParams['suffix'], 'value'=>$aParams['data']['listorder_'.$aParams['suffix']]),
                "ClearSectionOption" => $aParams['clearsection'],
                "DeleteSectionOption" => !$aParams['clearsection'],
                "ContactSuffix" => $aParams['suffix'],
                "DynamicSectionType" => $aParams['medication_name'],
                "Rows" => $RowList,
                'FieldFormatsTable' => 'INCMED',
                'ContainedIn' => 'multi_medication_record',
                ));
    }

}

function GetMedicationRowList($aParams)
{
    global $ModuleDefs;

    $RowList = $ModuleDefs[$aParams['module']]['LINKED_RECORDS'][$ModuleDefs[$aParams['module']]['MEDICATION_TYPE']]['basic_form']['Rows'];

    return $RowList;
}

function getMedicationFieldsForSave($aParams)
{
    global $FieldDefs;

    $RowListTmp = GetMedicationRowList($aParams);
    foreach($RowListTmp as $Field  )
    {
        if($FieldDefs[$aParams[module]][$Field]["DummyField"] != true )
        {
            $RowList[] = $Field;
        }
    }
   // $RowList[] = 'listorder';

    return array('Rows' => $RowList);
}

function ModifyFormDesignForMedications(Forms_FormDesign $design)
{
    if (is_array($design->ExpandSections))
    {
        $NewArray = array();
        foreach ($design->ExpandSections as $FieldName => $SectionArray)
        {
            foreach ($SectionArray as $SectionID => $Details)
            {
                if ($Details['section'] != 'multi_medication')
                {
                    $NewArray[$FieldName][] = $Details;
                }
            }
        }

        $design->ExpandSections = $NewArray;
    }

    // remove extra sections/extra fields
    unset($design->ExtraSections);
    unset($design->ExtraFields);

    return $design;
}

function GetYNINCMedications($recordid)
{
    $sql =
        'SELECT count(*) as NUM_MEDICATIONS_LINKED
        FROM INCIDENTS_MAIN
        LEFT JOIN
        INC_MEDICATIONS
        ON INCIDENTS_MAIN.RECORDID = INC_MEDICATIONS.INC_ID
        WHERE INCIDENTS_MAIN.RECORDID = :recordid
        AND INC_MEDICATIONS.RECORDID IS NOT NULL
        GROUP BY INCIDENTS_MAIN.RECORDID';

    $numMeds = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid), PDO::FETCH_COLUMN);

    return array('inc_medications_linked' => ($numMeds > 0 ? 'Y' : 'N'));
}