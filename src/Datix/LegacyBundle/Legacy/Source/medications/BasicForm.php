<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "details" => array(
        "ReadOnly" => bYN(GetParm('DMD_ENABLED', 'N')),
        "Title" => "Medication details",
        "Rows" => array(
            array(
                "Title" => "ID",
                "Name" => "recordid",
                "Type" => "formfield",
                "FormField" => $IDFieldObj
            ),
            "med_name",
            "med_type",
            "med_class",
            "med_brand",
            "med_manufacturer",
            "med_controlled",
            "med_price",
            "med_reference"
        )
    ),
);

?>
