<?php

function MedicationsDoSelection($formAction = "search")
{
	global $db_affected_rows;
	global $scripturl, $report_saved;
	global $yySetLocation;

	$FormAction = $_POST["rbWhat"];
	$FieldWhere = '';
	$Error = false;
	$search_where = array();
    ClearSearchSession("MED");

	if ($FormAction == "Cancel")
	{
		if (isset($_SESSION['security_group']['grp_id']))
		{
			$yySetLocation = $scripturl . '?action=editgroup' .
				'&grp_id=' . $_SESSION['security_group']['grp_id'] .
				'&module=' . $_SESSION['security_group']['module'];
			$_SESSION['security_group']['success'] = false;
		}
        elseif ($_POST['qbe_recordid'] != '')
        {
            // query by example for generic modules
            $yySetLocation = $scripturl . '?action=record&module=' . $_POST['qbe_return_module'] . '&recordid=' . $_POST['qbe_recordid'];
        }
		else
		{
			$yySetLocation = "$scripturl?module=MED";
		}
		redirectexit();
	}

    // construct and cache a new Where object
    try
    {
        $whereFactory = new \src\framework\query\WhereFactory();
        $where = $whereFactory->createFromRequest(new \src\framework\controller\Request());

        if (empty($where->getCriteria()['parameters']))
        {
            $where = null;
        }

        $_SESSION['MED']['NEW_WHERE'] = $where;
    }
    catch(ReportException $e)
    {
        $Error = true;
    }

	// Find all values which start with ram_ and add them to array
	// if they are not empty.
	// To do: parse names.
	$med = QuotePostArray($_POST);
    $_SESSION["MED"]["LAST_SEARCH"] = Sanitize::SanitizeRawArray($_POST);

	while (list($name, $value) = each($med))
	{
		if (MakeFieldWhere('MED', $name, $value, $FieldWhere) === false)
		{
			$Error = true;
			break;
		}

		if ($FieldWhere != '')
		{
			$search_where[$name] = $FieldWhere;
		}
	}

    if (!$Error && !ValidateWhereClause(implode(' AND ', $search_where), 'MED'))
    {
        $Error = true;
    }

    if (!$Error && !ValidateWhereClause(implode(' AND ', $search_where), 'MED'))
    {
        $Error = true;
    }

    if ($Error === true)
    {
        AddSessionMessage('ERROR', _tk('search_errors'));

        $yySetLocation = $scripturl . "?action=medicationssearch&searchtype=lastsearch";
        redirectexit();
    }

	$Where = '';

	if (empty($search_where) === false)
	{
		$Where = implode(" AND ", $search_where);
	}

	$_SESSION["MED"]["WHERE"] = $Where;
	$_SESSION["MED"]["TABLES"] = "medications_main";

    // We are doing a new search so unset LASTQUERYID
    unset($_SESSION['MED']['LASTQUERYID']);

	if (isset($_SESSION['security_group']['grp_id']))
	{
		$yySetLocation = $scripturl . '?action=editgroup' .
			'&grp_id=' . $_SESSION['security_group']['grp_id'] .
			'&module=' . $_SESSION['security_group']['module'];
		$_SESSION['security_group']['success'] = true;
	}
    elseif (isset($_SESSION['packaged_report']['rep_id']))
    {
        $yySetLocation = $scripturl . '?action=reportpackage' .
            '&rep_id=' . $_SESSION['packaged_report']['rep_id'] .
            '&rep_pack_id=' . $_SESSION['packaged_report']['rep_pack_id'] .
            '&module=' . $_SESSION['packaged_report']['module'];
        $_SESSION['packaged_report']['success'] = true;
    }
    elseif ($_POST['qbe_recordid'] != '')
    {
        // query by example for generic modules
        $yySetLocation = $scripturl . '?action=record&module=' . $_POST['qbe_return_module'] .
            '&recordid=' . $_POST['qbe_recordid'] . '&qbe_success=true';
    }
    else
    {
        $yySetLocation = "$scripturl?action=list&module=MED&listtype=search";
        $_SESSION["MED"]["SEARCHLISTURL"] = $yySetLocation;
    }
	redirectexit();
}

?>