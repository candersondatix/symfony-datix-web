<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '10'),
    "med_name" => array(
        'width' => '18'),
    "med_type" => array(
        'width' => '18'),
    "med_class" => array(
        'width' => '18'),
    "med_brand" => array(
        'width' => '18'),
    "med_manufacturer" => array(
        'width' => '18')

);

$list_columns_mandatory = array("recordid");

$list_columns_extra = array(
    "recordid" => array(
        'dataalign' => 'left',
        'showextra' => 'dataalign: left',
    ),
);
?>

