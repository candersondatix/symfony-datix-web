<?php

$EmailText['NewInstances']['Subject'] = 'Notification of assessment: ' . $data['template_title'];
$EmailText['NewInstances']['Body'] = '
    The ' . $data['template_title'] . ' assessment has been selected for completion at your location (' . $data['loc_name'] . '). Select the link below to view the record.

    Link to assessment: ' . $data['scripturl'];

$EmailText['SubmittedAssessmentInstance']['Subject'] = 'Submission of assessment: ' . $data['atm_title'];
$EmailText['SubmittedAssessmentInstance']['Body'] = '
    The ' . $data['atm_title'] . ' assessment, for ' . $data['loc_name'] . ', has been submitted and is ready for review. Select the link below to view the record.

    Link to assessment: ' . $data['scripturl'];

$EmailText['NewStaff']['Subject'] = 'Notification of assessment: ' . $data['atm_title'];
$EmailText['NewStaff']['Body'] = '
    You have been selected to review the ' . $data['atm_title'] . ' assessment. Select the link below to view the record.

    Link to assessment: ' . $data['scripturl'];
?>