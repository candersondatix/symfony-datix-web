<?php
require_once 'Source/reports.php';

function report1($where)
{
    global $dtxtitle, $yySetLocation, $MinifierDisabled;
    global $txt;

    LoggedIn();

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $WhereClause = $where;

    $sql = "SELECT code, description, cause_group
        FROM code_inc_cause
        ORDER BY cause_group, code";
    $request = db_query($sql);

    while ($row = db_fetch_array($request))
        $root_causes[$row["cause_group"]][$row["code"]] = $row["description"];

    $root_cause_groups = Array("1" => Array("descr" => "Identification of higher risk",
                "notes" => "Use this section to identify reasons why a higher risk patient or procedure was not identified or treated as such."),
        "2" => Array ("descr" => "Management of higher risks",
                "notes" => "Use this section to identify any shortcomings there may have been in the process of matching a higher risk patient or procedure to an appropriately skilled clinician."),
        "3" => Array("descr" => "Guidelines or protocols",
                "notes" => "Use this section to identify any shortcomings in the use of guidelines or protocols that may have been implicated in the $txt[INCName]."),
        "4" => Array ("descr" => "Credentialling: skills deficit not identified in the course of",
                "notes" => "Use this section where staff did not exercise skills required of their grade and the skills deficit was not identified/remedied during the process which has most recently been applied"),
        "5" => Array("descr" => "Team factors",
                "notes" => "Use this section to identify any shortcomings in teamwork that may have been implicated in the $txt[INCName]."),
        "6" => Array("descr" => "Work environment/resources",
                "notes" => "Use this section to identify any shortcomings in the work environment or available resources that may have been implicated in the $txt[INCName]"));

    $sql = "SELECT code, description
        FROM code_inc_severity";

    $request = db_query($sql);

    while($sev = db_fetch_array($request))
        $code_severity[$sev["code"]] = $sev["description"];

    $sql = "SELECT recordid, inc_name, inc_notes, inc_severity,
    inc_root_causes
    FROM incidents_main";

    if ($WhereClause != "")
        $sql .= " WHERE ($WhereClause)";

    $sql .= "
    ORDER BY inc_name ASC";

    $request = db_query($sql);

    $dtxtitle = "Datix Listing with Description and Correctable Cause Analysis";

    ////template_header();
    ob_start("output_handler_listingreport");
?>
<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" bgcolor="#6394bd" border="0" id="listingtable">
    <tr>
        <td style="text-align: center; font-size: 20px; font-weight: bold;">
            <?php if($_POST['title']!= NULL){echo Escaper::HtmlEncode($_POST['title']);}else{echo $dtxtitle;} ?>
        </td>
    </tr>
<?php
    while ($inc = db_fetch_array($request))
    {
        $causes = explode(" ", $inc["inc_root_causes"]);
        sort($causes, SORT_NUMERIC);
?>
<tr>
    <td class="titlebg" width="100%">
    <b>ID: <?= $inc["recordid"] . " " . $inc["inc_name"] ?>
    </td>
</tr>
<tr>
    <td class="windowbg2" width="100%">
    <b>Severity: <?= $code_severity[$inc["inc_severity"]] ?></b>
    <br />
    <br />
    <b>Description:</b>
    <br />
    <br />
    <?= htmlspecialchars($inc["inc_notes"]) ?>
    <br />
    <br />
    <b>Correctable Causes:</b>
    <br />
    <br />
<?php
        // Print out the root causes, grouped by root_cause_groups.
        $last_group = "";
        if(is_array($causes)) {
            foreach ($causes as $cause)
            {
                $rc = explode(".", $cause);
                $current_group = $rc[0];
                if ($root_cause_groups[$current_group] && $current_group != $last_group)
                    echo "<br /><i>$current_group "
                    . $root_cause_groups[$current_group]["descr"]
                    . "</i><br /><br />";
                if ($cause)
                    echo $root_causes[$current_group][$cause] . "<br />";
                $last_group = $current_group;
            }
        }
?>
    </td>
</tr>

<?php
    }
    ob_end_flush();
    ?>
    <script language="JavaScript" type="text/javascript" src="js_functions/FloatingWindowClass<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript">
    function ListingHTML2pdf() {
        document.listing.action = '<?= "{$scripturl}?action=exportswitch" ?>';
        document.listing.target = '_self';
        document.listing.submit();
    }
    </script>
    <tr>
        <td class="windowbg2" align="center">
            <form method="post" name="listing" action="<?= "{$scripturl}?action=reportdesigner&module=INC" ?>">
            <input type="hidden" value="INC" name="module" />
            <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
            <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
            <input type="hidden" id="exportmode" name="exportmode" value="listing" />
            <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
            <input type="hidden" id="saved_query" name="saved_query" value="<?= Sanitize::SanitizeInt($_REQUEST['saved_query']) ?>" />
            <input type="hidden" id="crosstabtype" name="crosstabtype" value="listing" />
            <input type="hidden" id="report" name="report" value="report1" />
            <?php
            if(!\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
            {
                $ExportButtonLink = "$scripturl?action=httprequest&type=exporttopdfoptions&noexcel=1&nocsv=1";
                ?>
                <input type="button" onclick="
                    var buttons = new Array();
                    buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(1)){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
                    buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                    PopupDivFromURL('exporttopdfoptions', 'Export', '<?=$ExportButtonLink?>', '', buttons, '');" value="Export"/>
            <?php } ?>
            </form>
        </td>
    </tr>
    <?php
    echo "</table>";
    //obExit();
}

// Listing by directorate and root cause.

function report2($where)
{
    global $dtxtitle, $yySetLocation, $txt, $MinifierDisabled;

    LoggedIn();

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $WhereClause = $where;

    // Get severity codes and descriptions

    $sql = "SELECT code, description
        FROM code_inc_severity";

    $request = db_query($sql);

    while($sev = db_fetch_array($request))
        $code_severity[$sev["code"]] = $sev["description"];

    // Get directorate codes and descriptions

    $sql = "SELECT code, description
        FROM code_directorate
        ORDER BY description";

    $request = db_query($sql);

    while($dir = db_fetch_array($request))
        $directorates[$dir["code"]] = $dir["description"];


    $sql = "SELECT code, description, cause_group
        FROM code_inc_cause
        ORDER BY cause_group, code";
    $request = db_query($sql);

    while ($row = db_fetch_array($request))
        $root_causes[$row["cause_group"]][$row["code"]] = $row["description"];

    $root_cause_groups = Array("1" => Array("descr" => "Identification of higher risk",
                "notes" => "Use this section to identify reasons why a higher risk patient or procedure was not identified or treated as such."),
        "2" => Array ("descr" => "Management of higher risks",
                "notes" => "Use this section to identify any shortcomings there may have been in the process of matching a higher risk patient or procedure to an appropriately skilled clinician."),
        "3" => Array("descr" => "Guidelines or protocols",
                "notes" => "Use this section to identify any shortcomings in the use of guidelines or protocols that may have been implicated in the $txt[INCName]."),
        "4" => Array ("descr" => "Credentialling: skills deficit not identified in the course of",
                "notes" => "Use this section where staff did not exercise skills required of their grade and the skills deficit was not identified/remedied during the process which has most recently been applied"),
        "5" => Array("descr" => "Team factors",
                "notes" => "Use this section to identify any shortcomings in teamwork that may have been implicated in the $txt[INCName]."),
        "6" => Array("descr" => "Work environment/resources",
                "notes" => "Use this section to identify any shortcomings in the work environment or available resources that may have been implicated in the $txt[INCName]"));

    // Get incidents

    $sql = "SELECT recordid, inc_notes, inc_name, inc_severity,
        inc_root_causes, inc_directorate
        FROM
        incidents_main";

    if ($WhereClause)
        $sql .= " WHERE ($WhereClause)";

    $request = db_query($sql);

    // Loop through the incidents, ordering first by directorate, then by root causes

    while ($inc = db_fetch_array($request))
    {
        $causes = explode(" ", $inc["inc_root_causes"]);
        if(is_array($causes))
        {
            sort($causes, SORT_STRING); // root causes can be chars

            foreach ($causes as $cause)
            {
                if ($cause != "" && $inc["inc_directorate"] != "")
                {
                    $rc = explode(".", $cause);
                    $cause_group = $rc[0];
                    $incident_list[$inc["inc_directorate"]][$cause_group][$cause][] = $inc;
                }
            }
        }
    }

    // Now output incidents by directorate.

    //template_header();
    ob_start("output_handler_listingreport");
    if(is_array($directorates)) {
        foreach ($directorates as $directorate_code => $directorate_descr) {
            if ($dir = $incident_list[$directorate_code]) {
?>
<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0" id="listingtable">
    <tr>
        <td style="text-align: center; font-size: 20px; font-weight: bold;">
            <?php if ($_POST['title'] != NULL){echo Escaper::HtmlEncode($_POST['title']);}else {echo GetFieldLabel("inc_directorate", "Directorate"). ": " . $directorate_descr ;} ?>
        </td>
    </tr>


    <tr>
        <td class="windowbg2">
        <table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <tr>
        <td class="windowbg2" width="100%">
    <?php
                // Loop round the cause groups checking for incidents
                foreach ($root_cause_groups as $cause_group => $group_details)
                {
                    if ($dir[$cause_group])
                    {
                        echo '<tr><td class="titlebg"><b>'
                            . $cause_group. ' . '
                            . $root_cause_groups[$cause_group]["descr"] . "</b></td></tr>";

                        foreach ($root_causes[$cause_group] as $rc_code => $rc_descr)
                        {
                            if ($dir[$cause_group][$rc_code])
                            {
                                echo '<tr><td class="windowbg2"><b>Cause: ' . $rc_descr . "</b></td></tr>";
                                foreach ($dir[$cause_group][$rc_code] as $incident) {
    ?>
        <tr>
        <td class="windowbg2">
        <?= $incident["inc_notes"] ?>
        <br />
        <br />
        Severity: <?= $code_severity[$incident["inc_severity"]] ?>.  ID: <?= $incident["recordid"] ?>
        </td></tr>
    <?php
                                }
                            }
                        }
                    }
                }
                echo "</td> </tr> </table>"; // End directorate
            }
            echo "</td></tr>";
        }
    }
    ob_end_flush();
?>
    <script language="JavaScript" type="text/javascript" src="js_functions/reports<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript" src="js_functions/FloatingWindowClass<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript">
        function ListingHTML2pdf() {
            document.listing.action = '<?= "{$scripturl}?action=exportswitch" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }

        function backToReportMenu() {
            document.listing.action = '<?= "{$scripturl}?action=reportdesigner&module=INC" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }
    </script>
    <tr>
        <td class="windowbg2" align="center">
            <form method="post" name="listing" action="<?= "{$scripturl}?action=reportdesigner&module=INC" ?>">
            <input type="hidden" value="INC" name="module" />
            <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
            <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
            <input type="hidden" id="exportmode" name="exportmode" value="listing" />
            <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
            <input type="hidden" id="saved_query" name="saved_query" value="<?= Sanitize::SanitizeInt($_REQUEST['saved_query']) ?>" />
            <input type="hidden" id="crosstabtype" name="crosstabtype" value="listing" />
            <input type="hidden" id="report" name="report" value="report2" />
            <?php
            if(!(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()))
            {
                $ExportButtonLink = "$scripturl?action=httprequest&amp;type=exporttopdfoptions&amp;noexcel=1&nocsv=1";
            ?>
            <input type="button" onclick="
                var buttons = new Array();
                buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(1)){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
                buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                PopupDivFromURL('exporttopdfoptions', 'Export', '<?=$ExportButtonLink?>', '', buttons, '');" value="Export"/>
            <?php } ?>
            </form>
        </td>
    </tr>
</table>

<?php
    //@ob_end_flush();
}

function report3($where)
{
    global $dtxtitle, $yySetLocation, $txt, $MinifierDisabled;

    LoggedIn();

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $WhereClause = $where;

    // Get severity codes and descriptions

    $sql = "SELECT code, description, listorder
        FROM code_inc_severity
        ORDER BY listorder ASC";

    $request = db_query($sql);

    while($sev = db_fetch_array($request))
        $code_severity[$sev["code"]] = $sev["description"];


    $sql = "SELECT code, description, cause_group
        FROM code_inc_cause
        ORDER BY cause_group, code";
    $request = db_query($sql);

    while ($row = db_fetch_array($request))
        $root_causes[$row["cause_group"]][$row["code"]] = $row["description"];

    $root_cause_groups = Array("1" => Array("descr" => "Identification of higher risk",
                "notes" => "Use this section to identify reasons why a higher risk patient or procedure was not identified or treated as such."),
        "2" => Array ("descr" => "Management of higher risks",
                "notes" => "Use this section to identify any shortcomings there may have been in the process of matching a higher risk patient or procedure to an appropriately skilled clinician."),
        "3" => Array("descr" => "Guidelines or protocols",
                "notes" => "Use this section to identify any shortcomings in the use of guidelines or protocols that may have been implicated in the $txt[INCName]."),
        "4" => Array ("descr" => "Credentialling: skills deficit not identified in the course of",
                "notes" => "Use this section where staff did not exercise skills required of their grade and the skills deficit was not identified/remedied during the process which has most recently been applied"),
        "5" => Array("descr" => "Team factors",
                "notes" => "Use this section to identify any shortcomings in teamwork that may have been implicated in the $txt[INCName]."),
        "6" => Array("descr" => "Work environment/resources",
                "notes" => "Use this section to identify any shortcomings in the work environment or available resources that may have been implicated in the $txt[INCName]"));

    // Get incidents

    $sql = "SELECT recordid, inc_notes, inc_name, inc_severity,
        inc_root_causes, inc_directorate
        FROM
        incidents_main";

    if ($WhereClause)
        $sql .= " WHERE ($WhereClause)";

    $request = db_query($sql);

    // Loop through the incidents, ordering first by directorate, then by root causes

    while ($inc = db_fetch_array($request))
    {
        $causes = explode(" ", $inc["inc_root_causes"]);

        if(is_array($causes))
        {
            sort($causes, SORT_STRING); // root causes can be chars

            foreach ($causes as $cause)
            {
                if ($cause != "" && $inc["inc_severity"] != "")
                {
                    $rc = explode(".", $cause);
                    $incident_list[$rc[0]][$cause][$inc["inc_severity"]] = $inc;
                }
            }
        }
    }

    // Now output incidents by directorate.

    //template_header();
    ob_start("output_handler_listingreport");
?>
<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0" id="listingtable">
    <tr>
        <td style="text-align: center; font-size: 20px; font-weight: bold;">
            <?php if($_POST['title'] != NULL){echo Escaper::HtmlEncode($_POST['title']);}else{echo "Incidents grouped by correctable cause and severity";}?>
        </td>
    </tr>
<?php
    if(is_array($incident_list))
    {
        foreach ($incident_list as $cause_group => $inc_cause)
        {
?>
<tr>
    <td class="titlebg" width="100%">
    <b><?= $cause_group . ". " . $root_cause_groups[$cause_group]["descr"] ?></b>
    </td>
</tr>
<tr>
    <td class="windowbg2">
    <table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
<tr>
    <td class="windowbg2" width="100%">
<?php
            if(is_array($inc_cause))
            {
                foreach ($inc_cause as $cause_code => $inc_sev)
                {
?>
<tr>
    <td class="titlebg">
    <b><i>Correctable cause: <?= $root_causes[$cause_group][$cause_code] ?></i></b>
    </td>
</tr>
<?php
                    if(is_array($inc_sev))
                    {
                        foreach ($inc_sev as $incident)
                        {
?>
<tr>
<td class="windowbg2">
<b>Severity: <?= $code_severity[$incident["inc_severity"]] ?>.  <?= $incident["inc_name"] ?> ID: <?= $incident["recordid"] ?></b>
<br />
<br />
<?= $incident["inc_notes"] ?>
<br />
<br />
</td></tr>
<?php
                        }
                    }
                }
            }

            echo "</td> </tr> </table>"; // End directorate
            echo "</td></tr>";
        }
    }
    ob_end_flush();
?>
    <script language="JavaScript" type="text/javascript" src="js_functions/FloatingWindowClass<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript">
    function ListingHTML2pdf() {
        document.listing.action = '<?= "{$scripturl}?action=exportswitch" ?>';
        document.listing.target = '_self';
        document.listing.submit();
    }

    function backToReportMenu() {
        document.listing.action = '<?= "{$scripturl}?action=reportdesigner&module=INC" ?>';
        document.listing.target = '_self';
        document.listing.submit();
    }
    </script>
    <tr>
        <td class="windowbg2" align="center">
            <form method="post" name="listing" action="<?= "{$scripturl}?action=reportdesigner&module=INC" ?>">
            <input type="hidden" value="INC" name="module" />
            <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
            <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
            <input type="hidden" id="exportmode" name="exportmode" value="listing" />
            <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
            <input type="hidden" id="saved_query" name="saved_query" value="<?= Sanitize::SanitizeInt($_REQUEST['saved_query']) ?>" />
            <input type="hidden" id="crosstabtype" name="crosstabtype" value="listing" />
            <input type="hidden" id="report" name="report" value="report3" />
            <?php
            if(!(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()))
            {
                $ExportButtonLink = "$scripturl?action=httprequest&amp;type=exporttopdfoptions&amp;noexcel=1&nocsv=1";
            ?>
            <input type="button" onclick="
                    var buttons = new Array();
                    buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(1)){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
                    buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                    PopupDivFromURL('exporttopdfoptions', 'Export', '<?=$ExportButtonLink?>', '', buttons, '');" value="Export"/>
            <?php } ?>
            </form>
        </td>
    </tr>
</table>
<?php
//obExit();
}

function report4($where, $isAssuranceListController = false)
{
    global $scripturl, $dtxtitle, $yySetLocation, $ModuleDefs, $txt, $MinifierDisabled;

    LoggedIn();

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $list = ($_GET["action"] == "assurancelist");

    $sqlobj = "select cod_code, cod_descr
               from code_types
               where cod_type = 'RAMOBJ'
               order by cod_listorder, cod_descr";

    $requestobj = db_query($sqlobj);

    while($ramobj = db_fetch_array($requestobj))
    {
        $cod_descr[$ramobj['cod_code']] = $ramobj['cod_descr'];

        $sql = "SELECT recordid, ram_objectives, ram_name, ram_description, ass_object, ass_ctrl, ass_gctrl, ass_assure, ass_gapass from vw_ram_assure
                LEFT JOIN
                ra_main on ra_main.recordid = vw_ram_assure.ram_id
                WHERE (ram_objectives like '%$ramobj[cod_code]%')";
        if ($where)
            $sql .= " AND ($where)";

        $request = db_query($sql);

        while ($ram = db_fetch_array($request))
        {
            $ramarray[$ramobj['cod_code']][] = $ram;
        }
    }

    $sql_noobj = "SELECT recordid, ram_objectives, ram_name, ram_description, ass_object, ass_ctrl, ass_gctrl, ass_assure, ass_gapass from vw_ram_assure
                LEFT JOIN
                ra_main on ra_main.recordid = vw_ram_assure.ram_id
                WHERE (ram_objectives is null or ram_objectives = '')";
    if ($where)
        $sql_noobj .= " AND ($where)";

    $request = db_query($sql_noobj);

    while ($ram_noobj = db_fetch_array($request))
    {
        $ramarray['No principal objectives'][] = $ram_noobj;
    }

//    if($list)
//    {
//        getPageTitleHTML(array(
//             'title' => 'Risks - Assurance Framework listing',
//             'module' => 'RAM'
//             ));
//
//        GetSideMenuHTML(array('module' => 'RAM'));
//
//        template_header('','','','', new Template(array('no_padding' => true)));
//    }
//    else
//    {
//        template_header('','','','', new Template(array('no_menu' => true, 'no_padding' => true)));
//    }

$_SESSION['listing_title'] = '';
ob_start("output_handler_listingreport");

echo '
<table id="listingtable" class="titlebg" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
<tr>
        <td style="text-align: center; font-size: 20px; font-weight: bold;" colspan="9">';
                if ($_POST['title'] != NULL){echo Escaper::HtmlEncode($_POST['title']);}else{echo 'Assurance Framework listing';}
    echo '</td>
    </tr>';

echo '
<tr>
    <td class="windowbg" width="100%" colspan="8">';
    echo '<b>Assurance Framework listing</b>';
    echo '<br/>';
    echo '
    </td>
</tr>';
if($list)
echo '<font color="red">';

echo '
<tr>
    <td class="windowbg" width="5%"><b>ID</b></td>
    <td class="windowbg" width="10%"><b>Detailed objectives</b></td>
    <td class="windowbg" width="10%"><b>Risk name</b></td>
    <td class="windowbg" width="10%"><b>Risk description</b></td>
    <td class="windowbg" width="10%"><b>Controls</b></td>
    <td class="windowbg" width="10%"><b>Assurance</b></td>
    <td class="windowbg" width="10%"><b>Gaps in controls</b></td>
    <td class="windowbg" width="10%"><b>Gaps in assurance</b></td>
</tr>';


    if(is_array($ramarray))
    {
        foreach($ramarray as $code => $sub)
        {
            echo '
            <tr>
                <td class="windowbg" width="100%" colspan="8">
                <b>Principal objective: ' . $cod_descr[$code] . '</b>
                </td>
            </tr>
            ';

            if(is_array($sub))
                foreach($sub as $ram2)
                    {

                    if($list)
                        $ram2recordid = "<a href='{$scripturl}?action=risk&amp;table=main&amp;recordid=$ram2[recordid]'>$ram2[recordid]</a>";
                    else
                        $ram2recordid = $ram2["recordid"];
                    echo '
                    <tr>
                        <td class="windowbg2" valign="top">'.$ram2recordid.'</td>
                        <td class="windowbg2" valign="top">'.str_replace("\n", "<br/>", htmlspecialchars($ram2["ass_object"])).'</td>
                        <td class="windowbg2" valign="top">'.htmlspecialchars($ram2["ram_name"]).'</td>
                        <td class="windowbg2" valign="top">'.htmlspecialchars($ram2["ram_description"]).'</td>
                        <td class="windowbg2" valign="top">'.str_replace("\n", "<br/>", htmlspecialchars($ram2["ass_ctrl"])).'</td>
                        <td class="windowbg2" valign="top">'.str_replace("\n", "<br/>", htmlspecialchars($ram2["ass_assure"])).'</td>
                        <td class="windowbg2" valign="top">'.str_replace("\n", "<br/>", htmlspecialchars($ram2["ass_gctrl"])).'</td>
                        <td class="windowbg2" valign="top">'.str_replace("\n", "<br/>", htmlspecialchars($ram2["ass_gapass"])).'</td>
                    </tr>
                    ';
                    }
        }
    }
    ob_end_flush();
     ?>
    <script language="JavaScript" type="text/javascript" src="js_functions/FloatingWindowClass<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript">
    function ListingHTML2pdf() {
        document.listing.action = '<?= "{$scripturl}?action=exportswitch" ?>';
        document.listing.target = '_self';
        document.listing.submit();
    }

    function backToReportMenu() {
        document.listing.action = '<?= "{$scripturl}?action=reportdesigner&module=RAM" ?>';
        document.listing.target = '_self';
        document.listing.submit();
    }
    </script>
    <tr>
        <td class="titlebg" align="center" colspan="8">
            <form method="post" name="listing" action="<?= "{$scripturl}?action=reportdesigner&module=RAM" ?>">
            <input type="hidden" value="RAM" name="module" />
            <input type="hidden" name="report" id="report" value="report4"/>
            <input type="hidden" name="crosstabtype" id="crosstabtype" value="listing"/>
            <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
            <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
            <input type="hidden" id="exportmode" name="exportmode" value="listing" />
            <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
            <input type="hidden" id="saved_query" name="saved_query" value="<?= Sanitize::SanitizeInt($_REQUEST['saved_query']) ?>" />
            <?php
            if(!(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()))
            {
                $ExportButtonLink = "$scripturl?action=httprequest&amp;type=exporttopdfoptions&amp;noexcel=1&nocsv=1";
            ?>
            <input type="button" onclick="
                    var buttons = new Array();
                    buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(<?php echo (!$isAssuranceListController ? '1' : ''); ?>)){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
                    buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                    PopupDivFromURL('exporttopdfoptions', 'Export', '<?=$ExportButtonLink?>', '', buttons, '');" value="Export"/>
                <?php } ?>
            </form>
        </td>
    </tr>
</table>
<?php

//footer();
//obExit();
}


/**
* Custom pre-defined Datix listing report
*
* Listing Full 3 (colour)
* STNLONG3.QRP
*/
function report5($where)
{
    global $dtxtitle, $yySetLocation, $txt, $MinifierDisabled;

    LoggedIn();

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $WhereClause = $where;
    $bReportHasData = false;

    //template_header();

    if($_POST['include_elements'])
    {
        $show_elements = true;
    }

    if($_POST['include_compliance'])
    {
        $show_compliance = true;
    }

    if($_POST['include_evidence'])
    {
        $show_evidence = true;
    }

    if($_POST['include_documents'])
    {
        $show_documents = true;
    }

    // Output the column headers
    ini_set('include_path', ini_get('include_path').';../Classes/');
    require_once 'export/PHPExcel.php';
    require_once 'export/PHPExcel/Writer/Excel2007.php';
    require_once 'export/PHPExcel/IOFactory.php';
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setTitle("DatixWeb Excel export");
    $objPHPExcel->getProperties()->setSubject("DatixWeb Excel export");
    $objPHPExcel->getProperties()->setDescription("DatixWeb Excel export");

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Full Listing 3 (colour)');

    ob_start("output_handler_listingreport");


    echo '<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0" id="listingtable">
    <tr>
        <td style="text-align: center; font-size: 20px; font-weight: bold;">';
            if($_POST['title'] != null){echo Escaper::HtmlEncode($_POST['title']);}else{echo 'Full Listing 3 (colour)';};
        echo '</td>
    </tr>';

    $i = 1;

    $sql = "SELECT recordid, stn_ourref, stn_name, stn_descr, stn_domain, stn_set, stn_status
            FROM standards_main
            left join code_types code_domain on standards_main.stn_domain = code_domain.cod_code  and code_domain.cod_type = 'STNDOM'
            left join code_types code_set on standards_main.stn_set = code_set.cod_code and code_set.cod_type = 'STNSET'
            left join code_types code_version on standards_main.stn_version = code_version.cod_code and code_version.cod_type = 'STNVER'
            left join code_types code_declaration on standards_main.stn_declaration = code_declaration.cod_code and code_declaration.cod_type = 'STNDEC'
            left join standards_status on standards_main.recordid = standards_status.stn_id
    ";

    if($where != '')
    {
        $sql .= ' WHERE ' . $where;
    }

    $sql .= " ORDER BY
              code_set.cod_listorder, code_set.cod_descr,
              code_version.cod_listorder, standards_main.stn_version,
              code_domain.cod_listorder, code_domain.cod_descr,
              stn_ourref, stn_name, stn_type
    ";

    $standard_result = PDO_fetch_all($sql);

    foreach($standard_result as $stn_row)
    {
        ?>
                <tr>
                    <td class="titlebg2" width="95%" colspan="3">
                        <b>Standard - <?=$stn_row['stn_ourref'] . ' - ' . $stn_row['stn_name']?></b>
                    </td>
                    <td class="titlebg2" width="5%">
                        <b>Status</b>
                    </td>

                </tr>
                <tr>
                    <td class="titlebg2" width="95%" colspan="3">
                        <?= htmlspecialchars($stn_row['stn_descr']) ?>
                    </td>
                    <?php
                    $sql = "SELECT TOP 1 cod_web_colour, description FROM code_stn_status WHERE code = :stn_status";
                    $result = PDO_fetch_all($sql, array('stn_status' => $stn_row['stn_status']));
                    $stn_row_colour = '';
                    $stn_row_descr = '';

                    foreach ($result as $row)
                    {
                        $stn_row_colour = htmlspecialchars($row['cod_web_colour']);
                        $stn_row_descr = htmlspecialchars($row['description']);
                        echo '<td bgColor="#' . $stn_row_colour . '" width="5%"><b>' . $stn_row_descr . '</b></td>';
                    }
                    if(count($result) == 0)
                    {
                        echo '<td class="titlebg2" width="5%"></td>';
                    }
                    ?>
                </tr>
        <?php
        $bReportHasData = true;

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array(
                    'argb' => PHPExcel_Style_Color::COLOR_WHITE
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '6E94B7'),
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP
            ),
        );

        $objPHPExcel->getActiveSheet()->SetCellValue("A$i", 'Standard - ' . $stn_row['stn_ourref'] . ' - ' . $stn_row['stn_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue("D$i", 'Status');
        $objPHPExcel->getActiveSheet()->mergeCells("A$i:C$i");
        $objPHPExcel->getActiveSheet()->mergeCells("A$i:C$i");
        $i++;

        $objPHPExcel->getActiveSheet()->SetCellValue("A$i", $stn_row['stn_descr']);
        $objPHPExcel->getActiveSheet()->mergeCells("A$i:C$i");

        $objPHPExcel->getActiveSheet()->SetCellValue("D$i", $stn_row_descr);

        $objPHPExcel->getActiveSheet()->getStyle('A' . ($i-1) . ':D' . $i)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(45);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle("D$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB(count($result)==0?'6E94B7':$stn_row_colour);
        $objPHPExcel->getActiveSheet()->getStyle("D$i")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLACK);

        $i++;

        /**********************************************************************************************/

        if($show_elements || $show_compliance || $show_elements || $show_documents)
        {

            $sql =
                "SELECT
                    standard_elements.recordid, standard_elements.ele_code, standard_elements.ele_descr
                FROM
                    standard_elements, standards_main
                WHERE
                    standard_elements.ele_stn_id = standards_main.recordid
                    AND standard_elements.ele_stn_id = :stn_recordid";

            $element_result = PDO_fetch_all($sql, array('stn_recordid' => $stn_row['recordid']));

            foreach($element_result as $ele_row)
            {
                if($show_elements === true)
                {
                    ?>
                            <tr>
                                <td class="windowbg" width="5%">
                                </td>
                                <td class="windowbg" width="95%" colspan="3">
                                    <b>Element - <?= htmlspecialchars($ele_row['ele_code']) ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td class="windowbg" width="5%">
                                </td>
                                <td class="windowbg" width="95%" colspan="3">
                                    <?= htmlspecialchars($ele_row['ele_descr']) ?>
                                </td>
                            </tr>
                    <?php
                    $styleArray = array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'argb' => 'd2e4fc'),
                        ),
                        'alignment' => array(
                            'wrap' => true
                        ),
                    );

                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, 'Element - ' . $ele_row['ele_code']);
                    $objPHPExcel->getActiveSheet()->getStyle("B$i")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
                    $i++;

                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $ele_row['ele_descr']);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . ($i-1) . ':B' . $i)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
                    $i++;

                    /**********************************************************************************************/

                }
                $sql =
                    "SELECT
                        standard_prompts.recordid, standard_prompts.pro_code, standard_prompts.pro_descr, standard_prompts.pro_status
                    FROM
                        standard_prompts, standard_elements, standards_main
                    WHERE
                        standard_prompts.pro_ele_id = standard_elements.recordid
                        AND standard_elements.ele_stn_id = standards_main.recordid
                        AND standard_elements.ele_stn_id = :stn_recordid
                        AND standard_elements.recordid = :ele_recordid
                        ";

                $prompt_result = PDO_fetch_all($sql, array('stn_recordid' => $stn_row['recordid'],
                                                              'ele_recordid' => $ele_row['recordid']));

                foreach($prompt_result as $pro_row)
                {
                    if($show_compliance === true)
                    {
                        ?>
                                <tr>
                                    <td class="windowbg2" width="5%">
                                    </td>
                                    <td class="windowbg2" width="5%">
                                    </td>

                                    <td class="windowbg2" width="85%">
                                        <b>Compliance - <?= htmlspecialchars($pro_row['pro_code']) ?></b>
                                    </td>
                                    <td class="windowbg2" width="5%">
                                        <b>Compliant?</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="windowbg2" width="5%">
                                    </td>
                                    <td class="windowbg2" width="5%">
                                    </td>
                                    <td class="windowbg2" width="85%">
                                        <?= htmlspecialchars($pro_row['pro_descr']) ?>
                                    </td>
                                    <td class="windowbg2" width="5%">
                                        <?= htmlspecialchars($pro_row['pro_status']) ?>
                                    </td>
                                </tr>
                        <?php

                        $styleArray = array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array(
                                    'argb' => 'E3EFFF'),
                            ),
                            'alignment' => array(
                                'wrap' => true
                            ),
                        );

                        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, 'Compliance - ' . $pro_row['pro_code']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, 'Compliant?');
                        $i++;

                        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $pro_row['pro_descr']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $pro_row['pro_status']);
                        $objPHPExcel->getActiveSheet()->getStyle('A' . ($i-1) . ':D' . $i)->applyFromArray($styleArray);
                        $i++;

                        /**********************************************************************************************/

                    }
                    $sql = "
                        SELECT library_main.recordid as recordid, library_main.lib_ourref as lib_ourref, library_main.lib_name
                        FROM library_main, link_library
                        WHERE link_library.lib_id = library_main.recordid
                            AND link_library.pro_id = :pro_recordid
                        ORDER BY library_main.lib_name";

                    $evidence_result = PDO_fetch_all($sql, array('pro_recordid' => $pro_row['recordid']));

                    foreach($evidence_result as $evi_row)
                    {
                        if($show_evidence === true || $show_documents === true)
                        {
                            if($show_evidence === true)
                            {
                                ?>
                                <tr>
                                    <td class="windowbg2" colspan="2">
                                        </td>
                                    <td bgcolor="#FFFFFF" width="85%" colspan="2">
                                        <b>Evidence - <?= htmlspecialchars($evi_row['lib_name']) ?></b>
                                    </td>
                                </tr>
                                <?php
                                $styleArray = array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'startcolor' => array(
                                            'argb' => 'FFFFFF'),
                                    ),
                                    'alignment' => array(
                                        'wrap' => true
                                    ),
                                );

                                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, 'Evidence - ' . $evi_row['lib_name']);
                                $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$i);
                                $objPHPExcel->getActiveSheet()->mergeCells('C'.$i.':D'.$i);
                                $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':B' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E3EFFF');
                                $objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':D' . $i)->applyFromArray($styleArray);
                                $i++;
                            }

                            if($show_documents === true)
                            {
                                $sql = "
                                    SELECT recordid, doc_notes, doc_dcreated, doc_dupdated, doc_type, typ.description as type_descr, doc_updatedby
                                    FROM documents_main LEFT OUTER JOIN code_doc_type typ ON doc_type = typ.code
                                    WHERE LIB_ID = :lib_id
                                    ORDER BY doc_dcreated DESC";

                                $doc_result = PDO_fetch_all($sql, array('lib_id' => $evi_row['recordid']));

                                foreach($doc_result as $doc_row)
                                {
                                    ?>
                                    <tr>
                                        <td class="windowbg2" colspan="2">
                                        </td>
                                        <td bgcolor="#FFFFFF" width="85%" colspan="2">
                                            <?= htmlspecialchars($doc_row['doc_notes']) ?>
                                            &nbsp;&nbsp;&nbsp;
                                            <i>Last updated <?= htmlspecialchars($doc_row['doc_dupdated']) . ' by ' . htmlspecialchars($doc_row['doc_updatedby']) ?></i>
                                        </td>
                                    </tr>
                                    <?php

                                    $styleArray = array(
                                        'fill' => array(
                                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'startcolor' => array(
                                                'argb' => 'FFFFFF'),
                                        ),
                                        'alignment' => array(
                                            'wrap' => true
                                        ),
                                    );

                                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, 'Last updated ' . $doc_row['doc_dupdated'] . ' by ' . $doc_row['doc_updatedby']);
                                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$i);
                                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$i.':D'.$i);
                                    $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':B' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E3EFFF');
                                    $objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':D' . $i)->applyFromArray($styleArray);
                                    $i++;
                                }
                            }
                        } //end of if($show_evidence === true || $show_documents === true)
                    }
                }
            }
        }//end of if($show_elements || $show_compliance || $show_elements || $show_documents)
    }

    if($bReportHasData === false)
    {
        ?>
        <tr>
            <td class="windowbg2" align="center" colspan="7"><?=$txt['report_no_data_matching_criteria']?></td>
        </tr>
        <?php
    }

    ob_end_flush();

    $objPHPExcel->getActiveSheet()->getStyle("A1:D".--$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    // Store PHPExcel object in session
    $_SESSION['crosstabtablePHPExcel'] = serialize($objPHPExcel);
    $objPHPExcel =  '';

    ?>
    <script language="JavaScript" type="text/javascript" src="js_functions/reports<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript" src="js_functions/FloatingWindowClass<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript">
        function ListingHTML2pdf() {
            document.listing.action = '<?= "{$scripturl}?action=exportswitch" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }

        function backToReportMenu() {
            document.listing.action = '<?= "{$scripturl}?action=reportdesigner&module=STN" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }
    </script>
    <tr>
        <td class="windowbg2" align="center" colspan="4">
            <form method="post" name="listing" action="<?= "{$scripturl}?action=reportdesigner&module=STN" ?>">
            <input type="hidden" value="STN" name="module" />
            <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
            <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
            <input type="hidden" id="exportmode" name="exportmode" value="listing" />
            <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
            <input type="hidden" id="saved_query" name="saved_query" value="<?= Sanitize::SanitizeInt($_REQUEST['saved_query']) ?>" />
            <input type="hidden" id="crosstabtype" name="crosstabtype" value="listing" />
            <input type="hidden" id="report" name="report" value="report5" />
            <?php
            if(!(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()))
            {
                $ExportButtonLink = "$scripturl?action=httprequest&amp;type=exporttopdfoptions&nocsv=1";
            ?>
            <input type="button" onclick="
                var buttons = new Array();
                buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(1)){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
                buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                PopupDivFromURL('exporttopdfoptions', 'Export', '<?=$ExportButtonLink?>', '', buttons, '');" value="Export"/>
                <?php } ?>
            </form>
        </td>
    </tr>
</table>

<?php
    //obExit();
}


/**
* Custom pre-defined Datix listing report
*
* CQC Outcomes Report
*/
function report6($where)
{
    global $dtxtitle, $yySetLocation, $txt, $MinifierDisabled;

    LoggedIn();

    $addMinExtension = ($MinifierDisabled ? '' : '.min');

    $WhereClause = $where;

    $dtxtitle = '';
    $_SESSION['listing_title'] = '';
    $pro_recordids = array();
    $bReportHasData = false;

    //template_header();

    // Output the column headers
    ini_set('include_path', ini_get('include_path').';../Classes/');
    require_once 'export/PHPExcel.php';
    require_once 'export/PHPExcel/Writer/Excel2007.php';
    require_once 'export/PHPExcel/IOFactory.php';
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setTitle("DatixWeb Excel export");
    $objPHPExcel->getProperties()->setSubject("DatixWeb Excel export");
    $objPHPExcel->getProperties()->setDescription("DatixWeb Excel export");

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('CQC Outcomes');
    $i = 1;
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(45);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);

    ob_start("output_handler_listingreport");

    ?>
    <table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0" id="listingtable">
    <tr>
        <td style="text-align: center; font-size: 20px; font-weight: bold;" colspan="6">
            <?php if($_POST['title'] != null){echo Escaper::HtmlEncode($_POST['title']);}else{echo 'CQC Outcomes';}?>
        </td>
    </tr>
    <?php

    $sql = "SELECT recordid, stn_ourref, stn_name, stn_descr, stn_domain, stn_set, stn_status
            FROM standards_main
            left join code_types code_domain on standards_main.stn_domain = code_domain.cod_code  and code_domain.cod_type = 'STNDOM'
            left join code_types code_set on standards_main.stn_set = code_set.cod_code and code_set.cod_type = 'STNSET'
            left join code_types code_version on standards_main.stn_version = code_version.cod_code and code_version.cod_type = 'STNVER'
            left join code_types code_declaration on standards_main.stn_declaration = code_declaration.cod_code and code_declaration.cod_type = 'STNDEC'
            left join standards_status on standards_main.recordid = standards_status.stn_id
    ";

    if($where != '')
    {
        $sql .= ' WHERE ' . $where;
    }

    $sql .= " ORDER BY
              code_set.cod_listorder, code_set.cod_descr,
              code_version.cod_listorder, standards_main.stn_version,
              code_domain.cod_listorder, code_domain.cod_descr,
              stn_ourref, stn_name, stn_type
    ";

    $standard_result = PDO_fetch_all($sql);

    foreach($standard_result as $stn_row)
    {
        // FIX ME: are these replacements necessary with unicode?
    	$stn_row['stn_descr'] = htmlspecialchars(\UnicodeString::str_ireplace(chr(129), '', $stn_row['stn_descr']));
        $stn_row['stn_descr'] = \UnicodeString::str_ireplace('&Acirc;', '', $stn_row['stn_descr']);
        $stn_row['stn_descr'] = \UnicodeString::str_ireplace('&#129;', '', $stn_row['stn_descr']);
    	
        ?>
                <tr>
                    <td class="titlebg2" width="95%" colspan="7">
                        <b>Standard - <?= htmlspecialchars($stn_row['stn_ourref'] . ' - ' . $stn_row['stn_name']) ?></b>
                    </td>
                </tr>
                <tr>
                    <td class="titlebg2" width="95%" colspan="7">
                        <?= $stn_row['stn_descr'] ?>
                    </td>
                </tr>

        <?php
        $bReportHasData = true;

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array(
                    'argb' => PHPExcel_Style_Color::COLOR_WHITE
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '6E94B7'),
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP
            ),
        );

        $objPHPExcel->getActiveSheet()->SetCellValue("A$i", 'Standard - ' . $stn_row['stn_ourref'] . ' - ' . $stn_row['stn_name']);
        $objPHPExcel->getActiveSheet()->mergeCells("A$i:G$i");
        $objPHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($styleArray);
        $i++;

        $styleArray = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'argb' => PHPExcel_Style_Color::COLOR_WHITE
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '6E94B7'),
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP
            ),
        );

        $objPHPExcel->getActiveSheet()->SetCellValue("A$i", $stn_row['stn_descr']);
        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(65);
        $objPHPExcel->getActiveSheet()->mergeCells("A$i:G$i");
        $objPHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($styleArray);
        $i++;

        /**********************************************************************************************/


        $sql =
            "SELECT
                standard_elements.recordid, standard_elements.ele_code, standard_elements.ele_descr
            FROM
                standard_elements, standards_main
            WHERE
                standard_elements.ele_stn_id = standards_main.recordid
                AND standard_elements.ele_stn_id = :stn_recordid";

        $element_result = PDO_fetch_all($sql, array('stn_recordid' => $stn_row['recordid']));

        foreach($element_result as $ele_row)
        {
            ?>

                    <tr>
                        <td class="windowbg" width="5%">
                        </td>
                        <td class="windowbg" width="95%" colspan="6">
                            <b>Element - <?= $ele_row['ele_descr'] ?></b>
                        </td>
                    </tr>

            <?php
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                        'argb' => 'd2e4fc'),
                ),
                'alignment' => array(
                    'wrap' => true,
                ),
            );

            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, 'Element - ' . $ele_row['ele_descr']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':B' . $i)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':G'.$i);

            /**********************************************************************************************/

            $sql =
                "SELECT
                    standard_prompts.recordid, standard_prompts.pro_code, standard_prompts.pro_descr, standard_prompts.pro_status
                FROM
                    standard_prompts, standard_elements, standards_main
                WHERE
                    standard_prompts.pro_ele_id = standard_elements.recordid
                    AND standard_elements.ele_stn_id = standards_main.recordid
                    AND standard_elements.ele_stn_id = :stn_recordid
                    AND standard_elements.recordid = :ele_recordid
                    ";

            $prompt_result = PDO_fetch_all($sql, array('stn_recordid' => $stn_row['recordid'],
                                                       'ele_recordid' => $ele_row['recordid']));

            foreach($prompt_result as $pro_row)
            {
                // FIX ME: are these replacements necessary with unicode?
            	$pro_row['pro_descr'] = htmlspecialchars(\UnicodeString::str_ireplace(chr(129), '', $pro_row['pro_descr']));
            	$pro_row['pro_descr'] = \UnicodeString::str_ireplace('&Acirc;', '', $pro_row['pro_descr']);
            	$pro_row['pro_descr'] = \UnicodeString::str_ireplace('&#129;', '', $pro_row['pro_descr']);
            	
                ?>
                            <tr>
                                <td class="windowbg2" width="5%">
                                </td>
                                <td class="windowbg2" width="5%">
                                </td>

                                <td class="windowbg2" width="85%" colspan="4">
                                    <b><?= htmlspecialchars($pro_row['pro_code']) ?></b>
                                    <br/>
                                   <?= $pro_row['pro_descr'] ?>
                                </td>
                                <?php
                                $sql = "SELECT TOP 1 cod_web_colour, cod_descr FROM code_types WHERE cod_code = :pro_status AND cod_type = 'PROSTA'";
                                $result = PDO_fetch_all($sql, array('pro_status' => $pro_row['pro_status']));
                                $bgColor = 'E3EFFF';

                                foreach ($result as $row)
                                {
                                    if($row['cod_web_colour'] != '' && $row['cod_web_colour'] != '000000')
                                    {
                                        $bgColor = $row['cod_web_colour'];
                                    }
                                    echo '<td bgColor="#' . $bgColor . '" width="5%">' . htmlspecialchars($row['cod_descr']) . '</td>';
                                }
                                if(count($result) == 0)
                                {
                                    echo '<td class="windowbg2" width="5%"></td>';
                                }
                                ?>
                            </tr>

                <?php

                $styleArray = array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'argb' => 'E3EFFF'),
                    ),
                    'alignment' => array(
                        'wrap' => true,
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP
                    ),
                );

                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $pro_row['pro_code'] . "\n" . $pro_row['pro_descr']);
                $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(35);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':F' . $i)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->mergeCells('C'.$i.':F'.$i);

                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $i, $pro_row['pro_status']);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($bgColor);
                $i++;

                /**********************************************************************************************/

                if($pro_row['pro_status'] != 'YES')
                {
                    $pro_recordids[$pro_row['recordid']] = $pro_row['pro_code'];
                }

                $sql = "
                    SELECT library_main.recordid as recordid, library_main.lib_ourref as lib_ourref, library_main.lib_name, library_main.lib_manager, library_main.lib_descr, link_library.link_notes, contacts_main.fullname
                    FROM link_library, library_main
                    LEFT JOIN contacts_main on contacts_main.initials = library_main.lib_manager
                    WHERE link_library.lib_id = library_main.recordid
                        AND link_library.pro_id = :pro_recordid
                    ORDER BY library_main.lib_name";

                $evidence_result = PDO_fetch_all($sql, array('pro_recordid' => $pro_row['recordid']));

                if(!empty($evidence_result))
                {
                    ?>
                            <tr>
                                <td class="windowbg2" width="5%"></td>
                                <td class="windowbg2" width="5%"></td>
                                <td class="windowbg" border="1"><b>Lead(s)</b></td>
                                <td class="windowbg"><b>Evidence</b></td>
                                <td class="windowbg" colspan="2"><b>What evidence shows</b></td>
                                <td class="windowbg"><b></b></td>
                            </tr>
                    <?php

                    $styleArray = array(
                        'font' => array(
                            'bold' => true,
                        ),
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'argb' => 'd2e4fc'),
                        ),
                        'alignment' => array(
                            'wrap' => true,
                        ),
                    );

                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, 'Lead(s)');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, 'Evidence');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, 'What evidence shows');
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$i);
                    $objPHPExcel->getActiveSheet()->mergeCells('E'.$i.':F'.$i);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':B' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E3EFFF');
                    $objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':G' . $i)->applyFromArray($styleArray);

                    $i++;

                    foreach($evidence_result as $evi_row)
                    {
                        ?>
                        <tr>
                            <td class="windowbg2" width="5%"></td>
                            <td class="windowbg2" width="5%"></td>
                            <td bgcolor="#FFFFFF"><?= htmlspecialchars($evi_row['fullname']) ?></td>
                            <td bgcolor="#FFFFFF"><?= htmlspecialchars($evi_row['lib_name']) ?></td>
                            <td bgcolor="#FFFFFF" colspan="2"><?= htmlspecialchars($evi_row['link_notes']) ?></td>
                            <td bgcolor="#FFFFFF"><b></b></td>
                        </tr>
                        <?php

                        $styleArray = array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array(
                                    'argb' => 'FFFFFF'),
                            ),
                            'alignment' => array(
                                'wrap' => true,
                            ),
                        );

                        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $evi_row['fullname']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $evi_row['lib_name']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $evi_row['link_notes']);
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$i);
                        $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':B' . $i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E3EFFF');
                        $objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':F' . $i)->applyFromArray($styleArray);

                        $i++;
                    }
                }
            }
        }
    }


    // Linked Actions to Prompts (which are not Compliant)

    if(empty($pro_recordids))
    {
        $proKeysImpl = "''";
    }
    else
    {
        $proKeysImpl = implode(',', array_keys($pro_recordids));
    }

    $sql = "SELECT ca_actions.recordid, act_synopsis, act_progress, act_to_inits, act_ddue, act_dstart, act_cas_id, act_descr, contacts_main.fullname
            FROM ca_actions
            LEFT JOIN contacts_main on contacts_main.initials = ca_actions.act_to_inits
            WHERE act_module = 'PRO' and act_cas_id in (" . $proKeysImpl . ")";

            $actions_pro_result = PDO_fetch_all($sql);

            if(!empty($actions_pro_result))
            {
                ?>
                        <tr>
                            <td bgcolor="#FFFFFF" colspan="7"><br/><br/></td>
                        </tr>

                        <tr>
                            <td class="titlebg2" colspan="7"><b>Action Plans</b></td>
                        </tr>
                        <tr>
                            <td class="windowbg"><b>Prompt</b></td>
                            <td class="windowbg" width="5"><b>Date</b></td>
                            <td class="windowbg"><b>Gaps Identified</b></td>
                            <td class="windowbg"><b>Action Plan</b></td>
                            <td class="windowbg" width="5"><b>Lead</b></td>
                            <td class="windowbg" width="5"><b>Deadline</b></td>
                            <td class="windowbg"><b>Progress</b></td>
                        </tr>
                <?php

                $i+=2;

                $styleArray = array(
                    'font' => array(
                        'bold' => true,
                        'color' => array(
                            'argb' => PHPExcel_Style_Color::COLOR_WHITE,
                        ),
                    ),
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'argb' => '6E94B7'),
                    ),
                );

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, 'Action Plans');
                $objPHPExcel->getActiveSheet()->mergeCells("A$i:G$i");
                $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray($styleArray);
                $i++;


                $styleArray = array(
                    'font' => array(
                        'bold' => true,
                    ),
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'argb' => 'd2e4fc'),
                    ),
                );

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, 'Prompt');
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, 'Date');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, 'Gaps Identified');
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, 'Action Plan');
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, 'Lead');
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $i, 'Deadline');
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $i, 'Progress');
                $objPHPExcel->getActiveSheet()->getStyle("A$i:G$i")->applyFromArray($styleArray);
                $i++;

                foreach($actions_pro_result as $act_pro_row)
                {
                    ?>
                        <tr>
                            <td class="windowbg2"><?= htmlspecialchars($pro_recordids[$act_pro_row['act_cas_id']]) ?></td>
                            <td class="windowbg2" width="5"><?= htmlspecialchars(FormatDateVal($act_pro_row['act_dstart'])) ?></td>
                            <td class="windowbg2"><?= htmlspecialchars($act_pro_row['act_synopsis']) ?></td>
                            <td class="windowbg2"><?= htmlspecialchars($act_pro_row['act_descr']) ?></td>
                            <td class="windowbg2" width="5%"><?= htmlspecialchars($act_pro_row['fullname']) ?></td>
                            <td class="windowbg2" width="5%"><?= htmlspecialchars(FormatDateVal($act_pro_row['act_ddue'])) ?></td>
                            <td class="windowbg2"><?= htmlspecialchars($act_pro_row['act_progress']) ?></td>
                        </tr>
                    <?php

                    $styleArray = array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'argb' => 'E3EFFF'),
                        ),
                        'alignment' => array(
                            'wrap' => true,
                        ),
                    );

                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $pro_recordids[$act_pro_row['act_cas_id']]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, FormatDateVal($act_pro_row['act_dstart']));
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $act_pro_row['act_synopsis']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $act_pro_row['act_descr']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $act_pro_row['fullname']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $i, FormatDateVal($act_pro_row['act_ddue']));
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $i, $act_pro_row['act_progress']);
                    $objPHPExcel->getActiveSheet()->getStyle("A$i:G$i")->applyFromArray($styleArray);
                    $i++;
                }
            }

    if($bReportHasData === false)
    {
        ?>
        <tr>
            <td class="windowbg2" align="center" colspan="7"><?=$txt['report_no_data_matching_criteria']?></td>
        </tr>
        <?php
    }
    ob_end_flush();

    $objPHPExcel->getActiveSheet()->getStyle("A1:G".--$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    // Store PHPExcel object in session
    $_SESSION['crosstabtablePHPExcel'] = serialize($objPHPExcel);
    $objPHPExcel =  '';

    ?>
    <script language="JavaScript" type="text/javascript" src="js_functions/reports<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript" src="js_functions/FloatingWindowClass<?php echo $addMinExtension; ?>.js"></script>
    <script language="JavaScript" type="text/javascript">
        function ListingHTML2pdf() {
            document.listing.action = '<?= "{$scripturl}?action=exportswitch" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }

        function backToReportMenu() {
            document.listing.action = '<?= "{$scripturl}?action=reportdesigner&module=STN" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }
    </script>
    <tr>
        <td class="windowbg2" align="center" colspan="7">
            <form method="post" name="listing" action="<?= "{$scripturl}?action=reportdesigner&module=STN" ?>">
            <input type="hidden" value="STN" name="module" />
            <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
            <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
            <input type="hidden" id="exportmode" name="exportmode" value="listing" />
            <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
            <input type="hidden" id="saved_query" name="saved_query" value="<?= Sanitize::SanitizeInt($_REQUEST['saved_query']) ?>" />
            <input type="hidden" id="crosstabtype" name="crosstabtype" value="listing" />
            <input type="hidden" id="report" name="report" value="report6" />
            <?php
            if(!(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()))
            {
                $ExportButtonLink = "$scripturl?action=httprequest&amp;type=exporttopdfoptions&nocsv=1";
            ?>
            <input type="button" onclick="
                var buttons = new Array();
                buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(1)){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
                buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                PopupDivFromURL('exporttopdfoptions', 'Export', '<?=$ExportButtonLink?>', '', buttons, '');" value="Export"/>
                <?php } ?>
            </form>
        </td>
    </tr>
</table>

<?php
    //obExit();
}

/**
* Custom claims listing report
*/
function report7($where)
{
    global $ModuleDefs, $DatixView, $txt, $dtxtitle;

    $_SESSION['listing']['whereclause'] = $where;

    $sql = '
    SELECT claims_main.recordid as cla_id, claims_main.cla_name, claims_main.cla_synopsis, claims_main.cla_dopened, claims_main.cla_dclosed, claims_main.cla_ourref,
(CASE
    WHEN cla_id2 is not null and cla_id2 != 0 THEN \'CLA\'
    WHEN inc_id is not null and inc_id != 0 THEN \'INC\'
    WHEN com_id is not null and com_id != 0 THEN \'COM\'
    WHEN pal_id is not null and pal_id != 0 THEN \'PAL\'
    WHEN ram_id is not null and ram_id != 0 THEN \'RAM\'
    WHEN stn_id is not null and stn_id != 0 THEN \'STN\'
    WHEN sab_id is not null and sab_id != 0 THEN \'SAB\'
    WHEN hot_id is not null and hot_id != 0 THEN \'HOT\'
    ELSE null END) as module,

(CASE
    WHEN cla_id2 is not null and cla_id2 != 0 THEN cla_id2
    WHEN inc_id is not null and inc_id != 0 THEN inc_id
    WHEN com_id is not null and com_id != 0 THEN com_id
    WHEN pal_id is not null and pal_id != 0 THEN pal_id
    WHEN ram_id is not null and ram_id != 0 THEN ram_id
    WHEN stn_id is not null and stn_id != 0 THEN stn_id
    WHEN sab_id is not null and sab_id != 0 THEN sab_id
    WHEN hot_id is not null and hot_id != 0 THEN hot_id
    ELSE null END) as record_id,

(CASE
    WHEN cla_id2 is not null and cla_id2 != 0 THEN claims_link.cla_name
    WHEN inc_id is not null and inc_id != 0 THEN inc_name
    WHEN com_id is not null and com_id != 0 THEN com_name
    WHEN pal_id is not null and pal_id != 0 THEN pal_name
    WHEN ram_id is not null and ram_id != 0 THEN ram_name
    WHEN stn_id is not null and stn_id != 0 THEN stn_name
    WHEN sab_id is not null and sab_id != 0 THEN sab_title
    WHEN hot_id is not null and hot_id != 0 THEN hot_name
    ELSE null END) as record_name,

(CASE
    WHEN cla_id2 is not null and cla_id2 != 0 THEN claims_link.cla_ourref
    WHEN inc_id is not null and inc_id != 0 THEN inc_ourref
    WHEN com_id is not null and com_id != 0 THEN com_ourref
    WHEN pal_id is not null and pal_id != 0 THEN pal_ourref
    WHEN ram_id is not null and ram_id != 0 THEN ram_ourref
    WHEN stn_id is not null and stn_id != 0 THEN stn_ourref
    WHEN sab_id is not null and sab_id != 0 THEN sab_reference
    WHEN hot_id is not null and hot_id != 0 THEN hot_ourref
    ELSE null END) as record_ourref,

(CASE
    WHEN cla_id2 is not null and cla_id2 != 0 THEN claims_link.cla_dopened
    WHEN inc_id is not null and inc_id != 0 THEN inc_dopened
    WHEN com_id is not null and com_id != 0 THEN com_dopened
    WHEN pal_id is not null and pal_id != 0 THEN NULL
    WHEN ram_id is not null and ram_id != 0 THEN ram_dcreated
    WHEN stn_id is not null and stn_id != 0 THEN NULL
    WHEN sab_id is not null and sab_id != 0 THEN sab_dopened
    WHEN hot_id is not null and hot_id != 0 THEN hot_opened_date
    ELSE null END) as record_dopened,

(CASE
    WHEN cla_id2 is not null and cla_id2 != 0 THEN claims_link.cla_dclosed
    WHEN inc_id is not null and inc_id != 0 THEN inc_dsched
    WHEN com_id is not null and com_id != 0 THEN com_dclosed
    WHEN pal_id is not null and pal_id != 0 THEN pal_dclosed
    WHEN ram_id is not null and ram_id != 0 THEN ram_dclosed
    WHEN stn_id is not null and stn_id != 0 THEN NULL
    WHEN sab_id is not null and sab_id != 0 THEN sab_dclosed
    WHEN hot_id is not null and hot_id != 0 THEN hot_closed_date
    ELSE null END) as record_dclosed


  FROM (
     SELECT cla_id, NULL as cla_id2, inc_id, com_id,pal_id,ram_id,stn_id,sab_id,hot_id FROM link_modules
     UNION ALL
         (
    SELECT lnk_id2 as cla_id2, lnk_id1 as cla_id, NULL as inc_id, NULL as com_id, NULL as pal_id, NULL as ram_id, NULL as stn_id, NULL as sab_id, NULL as hot_id FROM links JOIN claims_main c2 ON lnk_id2 = c2.recordid WHERE lnk_mod1 = \'CLA\' AND lnk_mod2 = \'CLA\'
    UNION ALL
    SELECT lnk_id1 as cla_id2, lnk_id2 as cla_id, NULL as inc_id, NULL as com_id, NULL as pal_id, NULL as ram_id, NULL as stn_id, NULL as sab_id, NULL as hot_id FROM links JOIN claims_main c2 ON lnk_id1 = c2.recordid WHERE lnk_mod1 = \'CLA\' AND lnk_mod2 = \'CLA\'
    )
    ) link_mod


        LEFT JOIN claims_main ON claims_main.recordid = link_mod.cla_id

        LEFT JOIN claims_main claims_link ON link_mod.cla_id2 = claims_link.recordid

        LEFT JOIN incidents_main ON incidents_main.recordid = link_mod.inc_id
        LEFT JOIN compl_main ON compl_main.recordid = link_mod.com_id
        LEFT JOIN pals_main ON pals_main.recordid = link_mod.pal_id
        LEFT JOIN ra_main ON ra_main.recordid = link_mod.ram_id
        LEFT JOIN standards_main ON standards_main.recordid = link_mod.stn_id
        LEFT JOIN sabs_main ON sabs_main.recordid = link_mod.sab_id
        LEFT JOIN hotspots_main ON hotspots_main.recordid = link_mod.hot_id
        WHERE
        link_mod.cla_id IS NOT NULL AND link_mod.cla_id != 0 and
        (
        link_mod.inc_id is not null and link_mod.inc_id != 0 or
        link_mod.com_id is not null and link_mod.com_id != 0 or
        link_mod.pal_id is not null and link_mod.pal_id != 0 or
        link_mod.ram_id is not null and link_mod.ram_id != 0 or
        link_mod.stn_id is not null and link_mod.stn_id != 0 or
        link_mod.sab_id is not null and link_mod.sab_id != 0 or
        link_mod.hot_id is not null and link_mod.hot_id != 0 or
        link_mod.cla_id2 is not null and link_mod.cla_id2 != 0
        )'.
        ($where ? ' AND ('.$where.')' : '').'
        ORDER BY cla_id, module';

    $Records = DatixDBQuery::PDO_fetch_all($sql);

    $CurrentClaimID = 0;
    $CurrentLinkedModule = '';
    $OpenTable = false;

    // Output the column headers
    ini_set('include_path', ini_get('include_path').';../Classes/');
    require_once 'export/PHPExcel.php';
    require_once 'export/PHPExcel/Writer/Excel2007.php';
    require_once 'export/PHPExcel/IOFactory.php';
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setTitle("DatixWeb Excel export");
    $objPHPExcel->getProperties()->setSubject("DatixWeb Excel export");
    $objPHPExcel->getProperties()->setDescription("DatixWeb Excel export");

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Analysis of claims links');

    $i = 2;
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(70);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);

    $objPHPExcel->getActiveSheet()->SetCellValue("A1", 'Name');
    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->SetCellValue("B1", 'Our ref');
    $objPHPExcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->SetCellValue("C1", 'Opened');
    $objPHPExcel->getActiveSheet()->getStyle("C1")->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->SetCellValue("D1", 'Closed');
    $objPHPExcel->getActiveSheet()->getStyle("D1")->getFont()->setBold(true);

    $HTML = '<table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" bgcolor="#6394bd" border="0" id="listingtable">';

    if ($_POST['title'] == null OR $_POST['title'] == '')
    {
        $HTML .= '<tr>
                    <td style="text-align: center; font-size: 20px; font-weight: bold;" colspan = "4">
                        Claims - Analysis of claims links report
                    </td>
                </tr>';
    }
    else
    {
        $HTML .= '<tr>
                    <td style="text-align: center; font-size: 20px; font-weight: bold;" colspan = "4">  '
                        .Escaper::HtmlEncode($_POST['title']).'
                    </td>
                </tr>';
    }


    $HTML .= '<tr><th>Name</th><th>Our ref</th><th>Opened</th><th>Closed</th></tr>';

    foreach ($Records as $Record)
    {
        if($Record['cla_id'] != $CurrentClaimID)
        {
            $claim_url = getRecordURL(array('module' => 'CLA', 'recordid' => $Record['cla_id'])).'&fromlisting=report7';
            $HTML .= '<tr style="background-color:#C7CCE0">
                <td><a href="'.$claim_url.'">'.$Record['cla_name'].'</a></td>
                <td><a href="'.$claim_url.'">'.$Record['cla_ourref'].'</a></td>
                <td><a href="'.$claim_url.'">'.FormatDateVal($Record['cla_dopened']).'</a></td>
                <td><a href="'.$claim_url.'">'.FormatDateVal($Record['cla_dclosed']).'</a></td></tr>';
            $HTML .= '<tr><td colspan="4"><a href="'.$claim_url.'">'.$Record['cla_synopsis'].'</a></td></tr>';

            $CurrentClaimID = $Record['cla_id'];
            $CurrentLinkedModule = '';

            $objPHPExcel->getActiveSheet()->SetCellValue("A$i", $Record['cla_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue("B$i", $Record['cla_ourref']);
            $objPHPExcel->getActiveSheet()->SetCellValue("C$i", FormatDateVal($Record['cla_dopened']));
            $objPHPExcel->getActiveSheet()->SetCellValue("D$i", FormatDateVal($Record['cla_dclosed']));

            $styleArray = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                        'argb' => 'C7CCE0'),
                ),
                'alignment' => array(
                    'wrap' => true
                ),
            );

            $objPHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle("B$i")->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle("C$i")->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle("D$i")->applyFromArray($styleArray);
            $i++;

            $styleArray = array(
                'alignment' => array(
                    'wrap' => true
                ),
            );

            $objPHPExcel->getActiveSheet()->SetCellValue("A$i", $Record['cla_synopsis']);
            $objPHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->mergeCells("A$i:D$i");
            $i++;

        }

        if ($Record['module'] != $CurrentLinkedModule)
        {
            $HTML .= '<tr><td><b>'.$ModuleDefs[$Record['module']]['NAME'].'</b></td><td></td><td></td><td></td></tr>';

            $CurrentLinkedModule = $Record['module'];

            $objPHPExcel->getActiveSheet()->SetCellValue("A$i", $ModuleDefs[$Record['module']]['NAME']);
            $objPHPExcel->getActiveSheet()->getStyle("A$i")->getFont()->setBold(true);
            $i++;
        }

        $record_url = getRecordURL(array('module' => $Record['module'], 'recordid' => $Record['record_id'])).'&fromlisting=report7';
        $HTML .= '<tr>
            <td><a href="'.$record_url.'">'.$Record['record_name'].'</a></td>
            <td><a href="'.$record_url.'">'.$Record['record_ourref'].'</a></td>
            <td><a href="'.$record_url.'">'.FormatDateVal($Record['record_dopened']).'</a></td>
            <td><a href="'.$record_url.'">'.FormatDateVal($Record['record_dclosed']).'</a></td></tr>';

        $objPHPExcel->getActiveSheet()->SetCellValue("A$i", $Record['record_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue("B$i", $Record['record_ourref']);
        $objPHPExcel->getActiveSheet()->SetCellValue("C$i", FormatDateVal($Record['record_dopened']));
        $objPHPExcel->getActiveSheet()->SetCellValue("D$i", FormatDateVal($Record['record_dclosed']));
        $i++;
    }

    $HTML .= '</table>';

    $objPHPExcel->getActiveSheet()->getStyle("A1:D".--$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    // Store PHPExcel object in session
    $_SESSION['crosstabtablePHPExcel'] = serialize($objPHPExcel);
    $objPHPExcel =  '';



    //template_header_nomenu();
    ob_start("output_handler_listingreport");

    echo $HTML;

    ob_end_flush();

?>

    <script language="JavaScript" type="text/javascript">
        function ListingHTML2pdf() {
            document.listing.action = '<?= "{$scripturl}?action=exportswitch" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }

        function backToReportMenu() {
            document.listing.action = '<?= "{$scripturl}?action=reportdesigner&module=CLA" ?>';
            document.listing.target = '_self';
            document.listing.submit();
        }
    </script>
    <div class="button_wrapper">
            <form method="post" name="listing" action="<?= "{$scripturl}?action=reportdesigner&module=CLA" ?>">
            <input type="hidden" value="CLA" name="module" />
            <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
            <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
            <input type="hidden" id="exportmode" name="exportmode" value="listing" />
            <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
            <input type="hidden" id="saved_query" name="saved_query" value="<?= Sanitize::SanitizeInt($_REQUEST['saved_query']) ?>" />
            <input type="hidden" id="crosstabtype" name="crosstabtype" value="listing" />
            <input type="hidden" id="report" name="report" value="report7" />
            <?php
            if(!\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
            {
                $ExportButtonLink = "$scripturl?action=httprequest&amp;type=exporttopdfoptions&nocsv=1";
            ?>
            <input type="button" onclick="
                var buttons = new Array();
                buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(1)){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
                buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                PopupDivFromURL('exporttopdfoptions', 'Export', '<?=$ExportButtonLink?>', '', buttons, '');" value="Export"/>
            <?php } ?>

    </div>

<?php


    //footer();
    //obExit();
}

function constructCQCreportsections($Data, $module, &$objPHPExcel, &$excelRow)
{
    $HTML = '';

    $hierarchy = array(
        'CQO' => array('childkey' => 'prompts', 'nextlevel' => 'CQP', 'excelcol' => 'A'),
        'CQP' => array('childkey' => 'subprompts', 'nextlevel' => 'CQS', 'excelcol' => 'B'),
        'CQS' => array('excelcol' => 'C')
    );

    foreach($Data as $RecordID => $Record)
    {
        $HTML .= '<tr><td>';

        $HTML .= '<table class="bordercolor" width="600px" align="left">';

        $Listing = Listings_ModuleListingDesignFactory::getListingDesign(array('module' => $module));
        $Listing->LoadColumnsFromDB();

        //value column is one to the right of the title column.
        $valueCol = $hierarchy[$module]['excelcol'];
        $valueCol++;

        $BorderStart = $hierarchy[$module]['excelcol'].$excelRow;

        foreach ($Listing->getColumnObjects() as $fieldObj)
        {
            if (preg_match('/tier_(\d)/u', $fieldObj->getName()))
            {
                require_once 'Source/classes/Listings/CQCListing.php';
                $ListingObj = new CQCListing($module);
                $Record[$fieldObj->getName()] = array_shift($ListingObj->getNodeAtTier($fieldObj->getName(), $Record['recordid']));
            }

            $fieldObj->setValue($Record[$fieldObj->getName()]);
            $fieldObj->getDisplayInfo();
            $HTML .= '<tr><td bgcolor="#6394bd" style="width:250px">'.$fieldObj->getLabel().'</td><td style="width:350px">'.$fieldObj->getDescription().'</td></tr>';

            //add to excel object
            $objPHPExcel->getActiveSheet()->SetCellValue($hierarchy[$module]['excelcol'].$excelRow, $fieldObj->getLabel());
            $objPHPExcel->getActiveSheet()->getStyle($hierarchy[$module]['excelcol'].$excelRow)->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->SetCellValue($valueCol.$excelRow, $fieldObj->getDescription());

            $excelRow++;
        }

        $BorderEnd = $valueCol.($excelRow-1);

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ),
            ),
        );

        $objPHPExcel->getActiveSheet()->getStyle($BorderStart.':'.$BorderEnd)->applyFromArray($styleArray);

        $excelRow++;

        $HTML .= '</table>';

        $HTML .= '</tr></td>';

        if (is_array($Data[$RecordID]['actions']) && !empty($Data[$RecordID]['actions']))
        {
            $objPHPExcel->getActiveSheet()->SetCellValue($hierarchy[$module]['excelcol'].$excelRow, 'Actions');
            $excelRow++;

            $HTML .= '<tr><td>Actions:';

            $ListingDesign = new Listings_ModuleListingDesign(array('module' => 'ACT'));
            $ListingDesign->LoadColumnsFromDB();

            $RecordList = new RecordLists_ModuleRecordList();
            $RecordList->Module = 'ACT';
            $RecordList->AddRecordData($Data[$RecordID]['actions']);

            $ListingDisplay = new Listings_ListingDisplay($RecordList, $ListingDesign);
            $HTML .= $ListingDisplay->GetListingHTML();

            $HTML .= '</tr></td>';

            //add to excel object
            $ListingDisplay->addToExcel($objPHPExcel, $hierarchy[$module]['excelcol'], $excelRow);
            $excelRow+=2;
        }

        if (is_array($Data[$RecordID]['evidence']) && !empty($Data[$RecordID]['evidence']))
        {
            $objPHPExcel->getActiveSheet()->SetCellValue($hierarchy[$module]['excelcol'].$excelRow, 'Evidence');
            $excelRow++;

            $HTML .= '<tr><td>Evidence:';

            $ListingDesign = new Listings_ModuleListingDesign(array('module' => 'LIB'));
            $ListingDesign->LoadColumnsFromDB();
            $AdditionalColumns[] = new Fields_DummyField(array('name' => 'doc_type', 'type' => 'C', 'label' => 'Type', 'codes' => DatixDBQuery::PDO_fetch_all('SELECT code, description FROM code_doc_type', array(), PDO::FETCH_KEY_PAIR)));
            $AdditionalColumns[] = new Fields_DummyField(array('name' => 'doc_notes', 'type' => 'S', 'label' => 'Notes'));
            $ListingDesign->AddAdditionalColumns($AdditionalColumns);

            $RecordList = new RecordLists_ModuleRecordList();
            $RecordList->Module = 'LIB';
            $RecordList->AddRecordData($Data[$RecordID]['evidence']);

            $ListingDisplay = new Listings_ListingDisplay($RecordList, $ListingDesign);
            $HTML .= $ListingDisplay->GetListingHTML();

            $HTML .= '</tr></td>';

            //add to excel object
            $ListingDisplay->addToExcel($objPHPExcel, $hierarchy[$module]['excelcol'], $excelRow);
            $excelRow+=2;
        }

        if ($hierarchy[$module] && is_array($Data[$RecordID][$hierarchy[$module]['childkey']]) && !empty($Data[$RecordID][$hierarchy[$module]['childkey']]))
        {
            $HTML .= '<tr><td>';

            $HTML .= '<table width="95%" align="right" border="0">';

            $HTML .= constructCQCreportsections($Data[$RecordID][$hierarchy[$module]['childkey']], $hierarchy[$module]['nextlevel'], $objPHPExcel, $excelRow);

            $HTML .= '</table>';

            $HTML .= '</tr></td>';
        }
    }

    return $HTML;
}
