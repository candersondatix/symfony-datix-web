<?php

$FormArray = array (
    "Parameters" => array (
        "Panels" => true,
        "Condition" => false,
        'Suffix' => $Suffix
    ),
    "link_details" => array(
        'Title' => 'Link details',
        "ContactSuffix" => $Suffix,
        'right_hand_link' => $RightHandLink,
        'Condition' => $ShowLinkDetails,
        'LinkFields' => true,
        "NotModes" => array("Search"),
        "Rows" => array(
            'link_notes',
            'link_resp',
            'link_role'
        ),
        'ContactsLinkTable' => 'link_respondents'
    ),
    "details" => array (
        "Title" => "Details",
        "ContactSuffix" => $Suffix,
        "Rows" => array(
            'recordid',
            "org_name",
            "org_reference",
            "org_address",
            "org_postcode",
            "org_tel1",
            "org_tel2",
            "org_email",
            "org_organisation",
            "org_unit",
            "org_clingroup",
            "org_directorate",
            "org_specialty",
            "org_loctype",
            "org_locactual",
            "org_notes"
        )
    ),
    "orphanedudfs" => array (
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        "Condition" => ($FormMode == 'Design' || ($FormMode != 'Search' && $data['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('ORG', $data['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    )
);


