<?php

$FormArray = array (
    "Parameters" => array (
        "Panels" => true,
        "Condition" => false
    ),
    "link_details" => array(
        'Title' => 'Link details',
        'Condition' => $ShowLinkDetails,
        'LinkFields' => true,
        "NotModes" => array("Search"),
        "Rows" => array(
            'link_notes',
            'link_resp',
            'link_role',
            "indemnity_reserve_assigned",
            "expenses_reserve_assigned",
            "remaining_expenses_reserve_assigned",
            "remaining_indemnity_reserve_assigned",
            'resp_total_paid'
        ),
        'ContactsLinkTable' => 'link_respondents'
    ),
    "details" => array (
        "Title" => "Details",
        "Rows" => array(
            'recordid',
            "org_name",
            "org_reference",
            "org_address",
            "org_postcode",
            "org_tel1",
            "org_tel2",
            "org_email",
            "org_organisation",
            "org_unit",
            "org_clingroup",
            "org_directorate",
            "org_specialty",
            "org_loctype",
            "org_locactual",
            "org_notes"
        )
    ),
    'policies' => array(
        "Title" => _tk('mod_policies_title'),
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'listpoliciesforrespondent' => array(
                'controller' => 'src\\policies\\controllers\\PoliciesController'
            )
        ),
        "Condition" => (GetParm('POL_PERMS') != '' && ($link_recordid != '' || $FormMode == 'Design')),
        "NotModes" => array('New','Search','Print'),
        'Listings' => array('policies' => array('module' => 'POL')),
        "Rows" => array()
    ),
    "claims" => array(
        "Title" => "Claims",
        'Condition' => $_SESSION["licensedModules"][MOD_CLAIMS],
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkedclaims' => array(
                'controller' => 'src\\organisations\\controllers\\OrganisationsController'
            )
        ),
        "Rows" => array()
    ),
    "orphanedudfs" => array (
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        "Condition" => ($FormMode == 'Design' || ($FormMode != 'Search' && $data['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('ORG', $data['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    )
);


