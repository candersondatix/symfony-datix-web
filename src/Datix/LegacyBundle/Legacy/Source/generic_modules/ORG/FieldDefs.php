<?php
$FieldDefs["ORG"] = array(
    "recordid" => array(
        "Type" => "number",
        "ReadOnly" => true,
        "Title" => "ID",
        "Width" => 5,
        'IdentityCol' => true
    ),
    "org_name" => array(
        "Type" => "string",
        "Width" => 100,
        "MaxLength" => 120,
        "Title" => "Name"
    ),
    "org_reference" => array(
        "Type" => "string",
        "MaxLength" => 120,
        "Title" => "Reference"
    ),
    "org_address" => array(
        "Type" => "textarea",
        "MaxLength" => 250,
        "Rows" => 5,
        "Columns" => 50,
        "Title" => "Address",
        "NoSpellcheck" => true
    ),
    "org_postcode" => array(
        "Type" => "string",
        "MaxLength" => 100,
        "Title" => "Postcode"
    ),
    "org_tel1" => array(
        "Type" => "string",
        "MaxLength" => 100,
        "Title" => "Telephone no.1"
    ),
    "org_tel2" => array(
        "Type" => "string",
        "MaxLength" => 100,
        "Title" => "Telephone no.2"
    ),
    "org_email" => array(
        "Type" => "email",
        "MaxLength" => 128,
        "Title" => "E-mail address"
    ),
    "org_loctype" => array(
        "Type" => "ff_select",
        "Title" => "Location (Type)"
    ),
    "org_locactual" => array(
        "Type" => "ff_select",
        "Title" => "Location (Exact)"
    ),
    "org_directorate" => array(
        "Type" => "ff_select",
        "Title" => "Directorate",
        "NoListCol" => true,
        "OldCodes" => true
    ),
    "org_specialty" => array(
        "Type" => "ff_select",
        "Title" => "Specialty",
        "NoListCol" => true,
        "OldCodes" => true
    ),
    "org_clingroup" => array(
        "Type" => "ff_select",
        "Title" => "Clinical Group",
        "NoListCol" => true,
        "OldCodes" => true
    ),
    "org_organisation" => array(
        "Type" => "ff_select",
        "Title" => "Organisation",
        "NoListCol" => true,
        "OldCodes" => true
    ),
    "org_unit" => array(
        "Type" => "ff_select",
        "Title" => "Unit",
        "NoListCol" => true,
        "OldCodes" => true
    ),
    "org_issues_linked" => array(
        "Type" => "yesno",
        "Title" => "Do you want to add any issues to this record?",
        "NoListCol" => true
    ),
    "org_notes" => array(
        "Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 10,
        "Columns" => 70,
        "NoListCol" => true
    ),

    // link fields
    "link_notes" => array(
        "Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 10,
        "Columns" => 70,
        "NoListCol" => true
    ),
    "link_resp" => array("Type" => "number",
        "Title" => "Responsibility %",
        "Width" => 5,
        "MaxLength" => 10,
        "MaxValue" => 100,
        "MinValue" => 0,
        "NoListCol" => true
    ),
    "link_role" => array(
        "Type" => "ff_select",
        "Title" => "Role",
        "NoListCol" => true
    ),
    'indemnity_reserve_assigned' => array(
        'Type' => 'money',
        'Title' => 'Indemnity incurred',
        'ReadOnly' => true
    ),
    'expenses_reserve_assigned' => array(
        'Type' => 'money',
        'Title' => 'Expenses incurred',
        'ReadOnly' => true
    ),
    'remaining_expenses_reserve_assigned' => array(
        'Type' => 'money',
        'Title' => 'Indemnity reserve assigned',
        'ReadOnly' => true
    ),
    'remaining_indemnity_reserve_assigned' => array(
        'Type' => 'money',
        'Title' => 'Expenses reserve assigned',
        'ReadOnly' => true
    ),
    'resp_total_paid' => array(
        'Type' => 'money',
        'Title' => 'Total paid',
        'ReadOnly' => true
    ),
	'ORG_PAS_CHK_FIELDS' => array(
        'Type' => 'multilistbox',
        'MaxLength' => 91
    )
);
