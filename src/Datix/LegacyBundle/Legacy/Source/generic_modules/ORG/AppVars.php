<?php

$ModuleDefs['ORG'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'ORG',
    'MOD_ID' => MOD_ORGANISATIONS,
    'CODE' => 'ORG',
    'APPROVAL_LEVELS' => false,
    'USES_APPROVAL_STATUSES' => false,
    'NAME' => _tk("ORGNamesTitle"),
    'ICON' => 'icons/icon_ORG.png',
    'TABLE' => 'organisations_main',
    'AUDIT_TABLE' => 'aud_compl_main',
    'REC_NAME' => _tk("ORGName"),
    'REC_NAME_PLURAL' => _tk("ORGNames"),
	'REC_NAME_TITLE' => _tk("ORGNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("ORGNamesTitle"),
    'FK' => 'org_id',
    'ACTION' => 'record&module=ORG',
    'SEARCH_URL' => 'action=search',
    'PERM_GLOBAL' => 'ORG_PERMS',
    'NO_REP_APPROVED' => true,
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'ORG'),
        2 => array('LEVEL' => 2, 'CODE'=>'ORG2')
    ),
    'FIELD_ARRAY' => array(
        "org_name",
        "org_reference",
        "org_address",
        "org_postcode",
        "org_tel1",
        "org_tel2",
        "org_email",
        "org_organisation",
        "org_unit",
        "org_clingroup",
        "org_directorate",
        "org_specialty",
        "org_loctype",
        "org_locactual",
        "org_notes"
    ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserORGForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserORG1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserORG2Settings',
    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,

    'LINKED_MODULE' => array(
//        'parent_ids' => array('CLA' => 'main_recordid'),
//        'link_recordid' => 'recordid',
        'panel' => 'respondents'
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Organisations/c_dx_organisations_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Organisations/c_dx_organisations_guide.xml'
);
