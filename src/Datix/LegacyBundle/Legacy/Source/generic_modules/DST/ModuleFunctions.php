<?php
function CheckForBatchUnlink($dst)
{
    global $scripturl, $yySetLocation;

    $form_action = $_POST["rbWhat"];
    //$dst = QuotePostArray($_POST);
    $dst_id = $dst["recordid"];

    switch ($form_action)
    {
        case "BatchUnlink":
            require_once 'Source/contacts/DistrContactForm.php';
            ContactBatchUnlink($dst);
            $yySetLocation = "$scripturl?action=record&module=DST&recordid=$dst_id&panel=contacts_type_N";
            redirectexit();
            break;
    }
}

function SaveOwnerID($dst)
{
    $dst_id = $dst["recordid"];

    if ($dst_id)
    {
        $sql = "update distribution_lists set dst_owner = " . $_SESSION["contact_login_id"] .
                " WHERE recordid = :dst_id";

        DatixDBQuery::PDO_query($sql, array("dst_id" => $dst_id));
    }
}
