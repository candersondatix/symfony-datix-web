<?php
$GLOBALS['FormTitle'] = 'Distribution List';

$GLOBALS["NewPanels"] = array (
    'contacts' => true,
    'progress_notes' => true,
);

$GLOBALS["HideFields"] = array (
    'progress_notes' => true,
);
$GLOBALS["MandatoryFields"] = array (
    'dst_name' => true,
);
?>