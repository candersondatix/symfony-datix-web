<?php

// Distribution lists
$ModuleDefs['DST'] = array(
    'MOD_ID' => MOD_DISTRIBUTION,
    'CODE' => 'DST',
    'NAME' => _tk("mod_distlists_title"),
    'REC_NAME' => _tk("DSTName"),
    'REC_NAME_PLURAL' => _tk("DSTNames"),
	'REC_NAME_TITLE' => _tk("DSTNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("DSTNamesTitle"),
    'FK' => 'dst_id',
    'URL_RECORDID' => 'dst_id',
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'DST',
   // 'NO_GENERIC_FORM' => true,
    'USES_APPROVAL_STATUSES' => false,
    'NO_REP_APPROVED' => true,
    'NO_REPORTING' => true,
    'OURREF' => 'dst_name',
    'TABLE' => 'distribution_lists',
    'ACTION' => 'record&module=DST',
    'PERM_GLOBAL' => 'DST_PERMS',
    'MAIN_URL' => 'action=distr',
    'SEARCH_URL' => 'action=search',
    'FIELD_NAMES' => array(
        "NAME" => "dst_name"
        ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'DST', 'HIDECODE' => true)
        ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserDSTForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserDSTSettings',

    'NO_LEVEL1' => true,
    'ICON' => 'icons/icon_DST.png',
    'MAIN_MENU_ICON' => 'distr.gif',

    'ADD_NEW_RECORD_LEVELS' => array('DST_FULL', 'DST_INPUT'),
    'INPUT_ONLY_LEVELS' => array('DST_INPUT'),
    'ADD_NEW_RECORD_PERM' => 'DST_ADD_NEW',
    'NEW_RECORD_ACTION' => 'addnew&amp;level=2&amp;module=DST',

    'FIELD_ARRAY' => array(
        "dst_name", "dst_descr", "dst_private"
        ),
    'EXTRA_SAVE_INCLUDES' => array(
        'Source/generic_modules/DST/ModuleFunctions.php'
    ),
    'EXTRA_SAVE_FUNCTIONS' => array('SaveOwnerID', 'CheckForBatchUnlink'
    ),

    'HARD_CODED_LISTINGS' => array(
        'all_distribution_lists' => array(
            'Title' => _tk("dst_list_all"),
            'Link' => _tk("dst_list_all_link"),
            'Where' => ''
            ),
        'my_distribution_lists' => array(
            'Title' => _tk("dst_list_my"),
            'Link' => _tk("dst_list_my_link"),
            'Where' => 'distribution_lists.dst_owner = ' . $_SESSION["contact_login_id"],
            'Condition' => ($_SESSION["Globals"]["DST_PERMS"] == 'DST_FULL')
            ),
    ),
    'CONTACTTYPES' => array(
                        "N" => array( "Type"=> "N", "Name"=>_tk('dst_member'), "Plural"=>_tk('dst_member_plural') )
                        ),
  /*  'LEVEL1_CON_OPTIONS' => array(
                        "N" => array("Title" => "Member", "DivName" => 'contacts'),
                        ),     */
    'DATA_VALIDATION_INCLUDES' => array(
        'Source/generic_modules/DST/ModuleFunctions.php'
    ),
    'CAN_LINK_NOTES' => true,
    'LOCAL_HELP' => 'WebHelp/index.html#DistributionLists/c_dx_distribution_lists_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/DistributionLists/c_dx_distribution_lists_guide.xml',
);
