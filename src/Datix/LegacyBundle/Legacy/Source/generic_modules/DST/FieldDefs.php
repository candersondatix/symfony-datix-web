<?php
$FieldDefs["DST"] = array(
    "recordid" => array("Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5),
    "dst_name" => array("Type" => "string",
        "Title" => "Name",
        "Width" => 70,
        "MaxLength" => 254),
 /*   "dst_type" => array("Type" => "ff_select",
        "Title" => "Type"),  */
    "dst_descr" => array("Type" => "textarea",
        "Title" => "Description",
        "Rows" => 7,
        "Columns" => 70),
    "dst_private" => array("Type" => "yesno",
        "Title" => "Private"),  
        );
?>
