<?php
function ListDSTContacts($dst, $FormType = "Main")
{
    ListContactType($dst, $FormType, "N", "Contact Members", true, "contact");
}

function ListContactType($dst, $FormType = "Main", $ConType, $Header, $EmailCheckBox = false, $ConItemName = "contact")
{
    global $scripturl;

    $AdminUser = $_SESSION["AdminUser"];
    $DSTPerms = $_SESSION["Globals"]["DST_PERMS"];

    if ($DSTPerms == "DST_READ_ONLY")
        $EmailCheckBox = false;

    if ($EmailCheckBox)
        $ColSpan = 8;
    else
        $ColSpan = 7;

    $LinkPerms = (!$_GET["print"]);

    $DstID = $dst["recordid"];

    require_once 'Source/libs/ListingClass.php';

echo '
<tr name="contacts_type_'.$ConType.'_row" id="contacts_type_'.$ConType.'_row">
<td class="windowbg2">
';
    if ($con)
    {
        foreach ($con["$ConType"] as $contact)
        {
            if ($ConType == "N")
                $Contacts[] = $contact;
        }
    }

    if ($ConType == "N")
    {
            //DoContactSection('Contacts', $ConType, $DstID, $Contacts, $ColSpan, $EmailCheckBox, $FormType, $IncludeColsList);
            DoDSTContactSection($dst, $FormType);
    }

if ($EmailCheckBox && !$_GET["print"] && $DSTPerms != "DST_READ_ONLY")
    echo '<tr>
    <td class="windowbg2" colspan="' . $ColSpan . '">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="windowbg2" align="right">
                    <input type="button" value="Unlink selected contacts" class="button" onclick="if(jQuery(\'.list_checkbox_DST:checked\').length > 0){document.forms[0].rbWhat.value=\'BatchUnlink\';submitClicked=true;this.form.submit()}else{alert(\'You must select a contact record(s) in order to perform this function\');}" name="btnBatchUnlink"/>
                </td>
            </tr>
        </table>
    </td>
    </tr>';

echo '</td></tr>';

if (!$_GET["print"] && $DSTPerms != "DST_READ_ONLY")
{
    if ($LinkPerms && $FormType != 'Print')
        echo '
    <div class="new_link_row" colspan="' . $ColSpan . '">
    <a href="' . $scripturl . '?action=linkcontactgeneral&amp;module=DST&amp;main_recordid='
    . Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;formtype=' . $FormType . '&amp;link_type=' . $ConType .'"><b>Add a ' . $ConItemName . ' to this distribution list</b></a>
    </div>';
echo '
    <div class="new_link_row" colspan="' . $ColSpan . '">
    <a href="' . $scripturl . '?action=contactssearch&amp;from_module=DST&amp;dst_id='
    . Sanitize::SanitizeInt($_GET["recordid"]) . '&amp;formtype=' . $FormType . '&amp;linktype=' . $ConType . '"><b>Search for contacts to link as ' . $ConItemName . 's for this distribution list</b></a>
    </div>';
}

}

function DoDSTContactSection($dst, $FormType)
{
    global $ListingDesigns;

    $Perms = GetParm("DST_PERMS");
    $AdminUser = ($Perms == 'DST_FULL');
    $ConType = 'DST';

    require_once 'Source/libs/ListingClass.php';
    $Listing = new Listing('CON', $ListingDesigns['contacts_type_N']['contacts_type_N']);

    $Listing->LoadColumnsFromDB();

    $ColsToGet = $Listing->Columns;

    //need to do rep_approved differently to avoid "ambiguous column errors"
    if(isset($ColsToGet['rep_approved']))
    {
        $ColsToGet['contacts_main.rep_approved'] = $ColsToGet['rep_approved'];
        unset($ColsToGet['rep_approved']);
    }

    //$Listing->WhereClause = ' recordid in (SELECT con_id FROM link_contacts WHERE dst_id = ' . $dst['recordid'] . ')';
    $sql = 'SELECT
                recordid, link_recordid, dst_id, link_type, '. implode(', ', array_keys($ColsToGet)).'
            FROM
                contacts_main, link_contacts
            WHERE
                link_contacts.con_id = contacts_main.recordid
                and link_contacts.dst_id = :recordid
            ORDER BY
                contacts_main.con_surname';

    $MembersArray = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $dst['recordid']));

    $Listing->LoadData($MembersArray);

   // $Listing->LoadDataFromDB();

    echo'
<tr height="20">
    <td class="titlebg" colspan="' . $ColSpan . '">
    <a href="Javascript:ToggleTwistyExpand(\'' . $ConType . '_' . $Div . '\', \'twisty_' . $ConType . '_' . $Div . '\',\'Images/expand.gif\', \'Images/collapse.gif\')">
    <img hspace="2" id="twisty_'. $ConType . '_' . $Div . '" src="Images/expand.gif" alt="+" border="0" /></a>
    <b>' . $Header . ' (' . count($Listing->Data) . ')</b>
    </td>
</tr>
<tr>
<td>
<div id="' . $ConType . '_' . $Div .'" style="display:none">
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
<tr>
';

    if(!$_GET["print"] && $FormType != 'ReadOnly')
    {
        $Listing->Checkbox = true;
        $Listing->CheckboxType = $ConType;
    }
    $Listing->LinkModule = "DST";
    echo $Listing->GetListingHTML($FormType);

    echo '</tr></table></div>';
}

?>
