<?php

require_once 'Source/generic_modules/DST/FormFunctions.php';

$IDFieldObj = new FormField($FormAction);

if ($FormAction == "Search")
{
    $IDField = '<input type="text" name="recordid" size="5">';
}
else
{
    $IDField = $dst["recordid"];
}

$IDFieldObj->MakeCustomField($IDField);

if ($_GET['dst_id'] && !$dst["recordid"])
{
    $FormArray = array("Sections" => array("NoAccessSection"));
}
else
{
    $FormArray = array(
        "Parameters" => array(
            "Panels" => "True",
            "Condition" => false
        ),
        "details" => array(
            "Title" => _tk('DSTNameTitle') . " Details",
            "MenuTitle" => _tk('DSTNameTitle') . " Details",
            "Rows" => array(
                'recordid',
                'dst_name',
                'dst_descr',
                'dst_private'
            )
        ),
        "contacts_type_N" => array(
            "Title" => "Members",
            "Function" => "ListDSTContacts",
            'Listings' => array('contacts_type_N' => array('module' => 'CON')),
            'LinkedForms' => array('N' => array('module' => 'CON')),
            "NoFieldAdditions" => true,
            "Condition" => $_GET["recordid"] || $FormMode == 'Design'
        ),
        'progress_notes' => array(
            'Title' => 'Progress notes',
            'NotModes' => array('New','Search'),
            'Special' => 'ProgressNotes',
            "NoFieldAdditions" => true,
            'Rows' => array()
        ),
    );
}

