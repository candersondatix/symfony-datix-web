<?php

// Standards
$ModuleDefs['ELE'] = array(
    'MOD_ID' => MOD_ELEMENTS,
    'CODE' => 'ELE',
    'NAME' => _tk("mod_elements_title"),
    'REC_NAME' => _tk("ELEName"),
    'REC_NAME_PLURAL' => _tk("ELENames"),
	'REC_NAME' => _tk("ELENameTitle"),
	'REC_NAME_PLURAL' => _tk("ELENamesTitle"),
    'FK' => 'ele_id',
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'ELE',
    'NO_GENERIC_FORM' => true,
    'NO_REP_APPROVED' => true,
    'NO_MAIN_MENU' => true,
    'OURREF' => 'ele_code',
    'TABLE' => 'standard_elements',
    'ACTION' => 'element',
    'PERM_GLOBAL' => 'STN_PERMS',
    'NO_SECURITY_LEVEL' => true, //uses another module's sec level
    'MAIN_URL' => 'action=element',
    'MODULE_GROUP' => 'STD',
    'MAIN_URL_OVERRIDE' => true,
   // 'LIBPATH' => 'Source/standards',
    'FIELD_NAMES' => array(
        "NAME" => "ele_code",
        "HANDLER" => "ele_handler",
        ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'ELE', 'HIDECODE' => true)
        ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserELEForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserELESettings',
    'FIELD_ARRAY' => array('ele_code', 'ele_descr', 'ele_sources', 'ele_listorder', 'ele_guidance', 'ele_examples', 'ele_score', 'ele_comments',
                            'ele_manager', 'ele_handler', "ele_priority", "ele_action_taken","ele_dassigned","ele_ddue", "ele_dreviewed"),

    'ICON' => 'icons/icon_STN.png',
    'ACTION_LINK_LIST_FIELDS' => array(
        array('field' => 'recordid', 'width' => '5'),
        array('field' => 'ele_code', 'width' => '10'),
        array('field' => 'ele_descr', 'width' => '85')
        ),

    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
);
