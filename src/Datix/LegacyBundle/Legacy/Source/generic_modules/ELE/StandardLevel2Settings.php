<?php
$GLOBALS['FormTitle'] = 'Details of Element';

$GLOBALS["NewPanels"] = array (
    'guidance' => true,
    'evidence' => true,
    'linked_records' => true,
    'linked_actions' => true,
    'progress_notes' => true,
    'action_chains' => true,
);

$GLOBALS["HideFields"] = array (
    'progress_notes' => true,
    'ele_priority' => true,
    'ele_action_taken' => true,
    'ele_dassigned' => true,
    'ele_ddue' => true,
    'ele_dreviewed' => true,
	'action_chains' => true,
);
?>
