<?php

$eleRowsArray = array(
    array("Name" => "ele_code", "ReadOnly" => $ReadOnly),
    array("Name" => "ele_descr", "ReadOnly" => $ReadOnly),
    array("Name" => "ele_manager", "ReadOnly" => $ReadOnly),
    array("Name" => "ele_handler", "ReadOnly" => $ReadOnly),
    array("Name" => "ele_listorder", "ReadOnly" => $ReadOnly)
);

if ($_GET["action"] <> "liststandards" && $_GET["action"] <> "reports" && $_GET["action"] <> "stnreports" && $FormType != 'Design')
{
    require_once 'Source/standards/StandardsBase.php';
    $libCount = CountLinkedEvidence('ELE', $ele["recordid"]);
}

$FormArray = array(
    "Parameters" => array(
        "Panels" => "True",
        "Condition" => false
    ),
    "standard" => array(
        "Title" => "Standard Details",
        "Type" => "Panel",
        "Function" => 'StandardDetailsSection',
        "NoFieldAdditions" => true,
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "details" => array(
        "Title" => "Element Details",
        "Rows" => array(
            array("Name" => "ele_code", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_descr", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_manager", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_handler", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_listorder", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_score", "ReadOnly" => !$Editable, 'Condition' => ($FormMode == 'Design' || $Perms)),
            array("Name" => "ele_comments", "ReadOnly" => !$Editable, 'Condition' => ($FormMode == 'Design' || $Perms)),
            array("Name" => "ele_priority", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_action_taken", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_dassigned", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_ddue", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_dreviewed", "ReadOnly" => $ReadOnly),
        )
    ),
    "compliance" => array(
        "Title" => "Compliance for standard",
        "Function" => 'ComplianceSection',
        'Listings' => array('compliance' => array('module' => 'PRO')),
        'LinkedForms' => array('compliance' => array('module' => 'PRO')),
        "NoFieldAdditions" => true,
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "guidance" => array(
        "Title" => "Guidance",
        "Type" => "Panel",
        "Rows" => array(
            array("Name" => "ele_guidance", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_examples", "ReadOnly" => $ReadOnly),
            array("Name" => "ele_sources", "ReadOnly" => $ReadOnly)
        ),
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "evidence" => array(
        "Title" => "Evidence" . ($libCount?" ($libCount)":""),
        "Function" => 'ListEvidence',
        'Listings' => array('evidence' => array('module' => 'LIB')),
        "NoFieldAdditions" => true,
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        "NotModes" => array("New", "Search"),
        "Special" => "LinkedActions"
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    'linked_records' => array(
        'Title' => 'Linked Records',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Special' => 'LinkedRecords',
        'Rows' => array()
    ),
);

if ($ele["recordid"])
{
    $FormArray["Sections"][] = "ComplianceSection";
}

?>