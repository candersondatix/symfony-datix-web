<?php

$FormArray = array(
    'Parameters' => array(
        'Panels' => true,
        'Condition' => false
    ),
    'assesment_template' => array(
        'Title' => _tk('AMONameTitle')." details",
        'Rows' => array(
            array('Name' => 'atm_ref', 'ReadOnly' => true),
            array('Name' => 'atm_version', 'ReadOnly' => true),
            array('Name' => 'atm_category', 'ReadOnly' => true),
            array('Name' => 'atm_title', 'ReadOnly' => true),
            array('Name' => 'atm_description', 'ReadOnly' => true),
            array('Name' => 'atm_notes', 'ReadOnly' => true),
            array('Name' => 'atm_staff', 'ReadOnly' => true),
        )
    ),
    'question' => array(
        'Title' => _tk('AQUNameTitle').' Details',
        'Rows' => array(
            'recordid',
            'atq_ref',
            'atq_order',
            'atq_theme',
            'atq_question',
            'atq_notes',
            'atq_staff'
        )
    )
);
