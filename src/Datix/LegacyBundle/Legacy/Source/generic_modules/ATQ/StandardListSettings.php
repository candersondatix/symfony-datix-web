<?php
$list_columns_standard = array(
    "atm_title" => array(
        'width' => '5'),
    "atq_ref" => array(
        'width' => '2'),
    "atq_theme" => array(
        'width' => '3'),
    "atq_question" => array(
        'width' => '10'),
    "atm_version" => array(
        'width' => '2'),
);