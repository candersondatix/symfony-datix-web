<?php
$FieldDefs['ATQ'] = array(
    'recordid' => array(
        'Type' => 'number',
        'Title' => 'ID',
        'ReadOnly' => true,
        'Width' => 5,
        'IdentityCol' => true,
        'NoListCol_Override' => true
    ),
    'atq_ref' => array(
        'Type' => 'string',
        'MaxLength' => 50,
        'Title' => 'Reference',
    	"SubmoduleSuffix" => _tk('ATQSubmoduleSuffix')
    ),
    'atq_question' => array(
        'Type' => 'textarea',
        'Columns' => 70,
        'Rows' => 10,
        'Title' => 'Criterion',
    	"SubmoduleSuffix" => _tk('ATQSubmoduleSuffix')
    ),
    'atq_notes' => array(
        'Type' => 'textarea',
        'Title' => 'Notes',
        'Rows' => 7,
        'Columns' => 70,
    	"SubmoduleSuffix" => _tk('ATQSubmoduleSuffix')
    ),
    'atq_order' => array(
        'Type' => 'number',
        'Title' => 'Order',
        'Width' => 5,
    	"SubmoduleSuffix" => _tk('ATQSubmoduleSuffix')
    ),
    'atq_staff' => array(
        'Type' => 'multilistbox',
        'Title' => 'Staff',
    	'MaxLength' => 254,
    	"SubmoduleSuffix" => _tk('ATQSubmoduleSuffix')
    ),
    'atq_theme' => array(
        'Type' => 'ff_select',
        'Title' => 'Theme',
    	"SubmoduleSuffix" => _tk('ATQSubmoduleSuffix')
    ),
    'atm_category' => array(
        'Type' => 'ff_select',
        'Title' => 'Category',
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')
    ),
    'atm_staff' => array(
        'Type' => 'multilistbox',
        'Title' => 'Staff',
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix'),
    	'MaxLength' => 254
    ),
    'atm_version' => array(
        'Type' => 'ff_select',
        'Title' => 'Version',
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')
    ),
    "atm_notes" => array("Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_title" => array("Type" => "string",
        "MaxLength" => 255,
        'Width' => 70,
        "Title" => "Title",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_ref" => array("Type" => "string",
        "MaxLength" => 64,
        "Width" => 15,
        "Title" => "Reference",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_description" => array("Type" => "textarea",
        "Title" => "Description",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),

);