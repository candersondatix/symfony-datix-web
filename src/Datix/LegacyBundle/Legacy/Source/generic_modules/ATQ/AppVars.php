<?php 
// Assesment module instance records
$ModuleDefs['ATQ'] = array(
    'MOD_ID'  => MOD_ASSESSMENT_QUESTION_TEMPLATES, // Module identifier. Used in licensing checks and db tables, such as udf_groups. See Source/libs/constants.php for value of constant
    'CODE'    => 'ATQ', // not sure what this is for
    'GENERIC' => true,
    'GENERIC_FOLDER' => 'ATQ',
    'NAME'    => _tk('ATQNamesTitle'),
    'REC_NAME'  => _tk("ATQName"), // name of record?
    'REC_NAME_PLURAL' => _tk("ATQNames"),
	'REC_NAME_TITLE'  => _tk("ATQNameTitle"), // name of record?
	'REC_NAME_PLURAL_TITLE' => _tk("ATQNamesTitle"),
    'TABLE' => 'asm_template_questions',
    'VIEW' => 'asm_question_templates',
    'TEMPLATE' => '',
    'ACTION' => 'record&module=ATQ',

    'MODULE_GROUP' => 'ACR',
    'APPROVAL_LEVELS' => false,
    'USES_APPROVAL_STATUSES' => false,
    'NO_REP_APPROVED' => true,
    'FORMCODE' => 'ATQ',
    'PERM_GLOBAL' => 'ATQ_PERMS',
    'ICON' => '',
    'FK' => '',
    'FIELD_ARRAY' => array(
        'atq_ref',
        'atq_question',
        'atq_notes',
        'atq_order',
        'atq_staff',
        'atq_theme'
    ),
    'VIEW_FIELD_ARRAY' => array(
        'atq_ref',
        'atq_question',
        'atq_notes',
        'atq_order',
        'atq_staff',
        'atq_theme',
        'atm_category',
        'atm_staff',
        'atm_version',
        'atm_ref',
        'atm_title',
        'atm_description',
        'atm_notes'
    ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'ATQ', 'HIDECODE' => true)
    ),
    'FORMCODE1' => 'ATQ2',
    'FORM_DESIGN_ARRAY_FILE' => 'UserATQForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserATQSettings',
    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,
    'LISTING_FILTERS' => true,
    'BREADCRUMBS' => false,
    'LISTING_WHERE' => ' recordid IS NOT NULL',
    'LINKED_MODULE' => array(
        'parent_ids' => array('ATM' => 'asm_module_template_id'),
        'link_recordid' => 'recordid',
        'panel' => 'asm_template_questions'
    ),
    'HAS_HOMESCREEN' => false,   // Variable to decide if this module will appear as a Homescreen
    'COMBO_LINK_WHERE' => "AND fmt_field NOT IN
        ('atm_category', 'atm_staff', 'atm_version', 'atq_staff')"
);
