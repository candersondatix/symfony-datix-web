<?php


use Source\classes\ASM\TemplateInstance;

/**
 *  It is not possible to create assigned assessment templates for locations which already have an assigned assessment record created for them, based upon the template, the location AND the 'cycle year' matching
 *  It is not possible to create assigned assessment templates for locations which sit in the same path on the hierarchy as locations for which an assigned assessment template has already been created (again, based upon the template, the location AND the 'cycle year' matching).
 *
 * @param $data
 * @return bool
 */
function validateExistingRecords($data)
{
	$locations = getLocationsByTemplateCycleYear($_POST['assessment_template_id'], $_POST['ati_cycle']);

	$used_locations = array();

	foreach ($locations as $nodeId) {
		$used_locations = array_merge(getLocationsPath($nodeId),$used_locations);
	}
	$used_locations = array_unique($used_locations);

	if (count(array_intersect($used_locations, $data['locations'])))
	{
		return false;
	}

	return true;
}

/**
 * Funtion to filter a set of locations based on the location tier depth of a previously selected Assessment Template
 *
 * @param $locations
 * @param $recordID Assessment Template ID
 */
function filterLocations($locations, $recordId)
{
	// todo
}

function getLocationsPath ($nodeId)
{

    $locHierarchy = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);

    $Ancestors = array_keys($locHierarchy->getAncestors($nodeId));
    $LeafNodes = array_keys($locHierarchy->getLeafNodes($nodeId));

    $LocationsPath = array_merge($Ancestors, $LeafNodes);

    return $LocationsPath;
}

function getLocationsByTemplateCycleYear($asm_module_template_id, $ati_cycle)
{
    $sql = '
        SELECT
            distinct ati_location
        FROM
            asm_template_instances
        WHERE
            asm_module_template_id = :asm_module_template_id AND ati_cycle = :ati_cycle
    ';

    $locations =  DatixDBQuery::PDO_fetch_all($sql, array('asm_module_template_id' => $asm_module_template_id,'ati_cycle' => $ati_cycle), PDO::FETCH_COLUMN);

	return $locations;

}

