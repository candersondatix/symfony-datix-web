<?php

$FormArray = array(
    'Parameters' => array(
        'Panels' => true,
        'Condition' => false
    ),
    'module' => array(
        'Title' => _tk('AMONameTitle').' Details',
        'Rows' => array(
            'recordid',
            'atm_ref',
            'atm_version',
            'atm_category',
            'atm_title',
            'atm_description',
            'atm_notes',
            'atm_staff'
        )
    ),
    /*
     * If we want to create a section with a linked form the name of the section
     * MUST BE the name of the linked module TABLE
     *
     * e.g. In this case we are connecting ATQ form to ATM so the name of the section
     * is the name of the table of ATQ module.
     *
     * Note: The same rule applies for the LinkedForms array
     */
    'asm_template_questions' => array(
        'Title' => _tk('AQUNamesTitle'),
        'NotModes' => array('New', 'Search'),
        'Special' => 'LinkedASMQuestionTemplates',
        'NoFieldAdditions' => true,
        'Rows' => array(),
        'LinkedForms' => array('asm_template_questions' => array('module' => 'ATQ')),
        'Listings' => array('ATQ' => array('module' => 'ATQ', 'Label' => 'Select a listing to use for the list of '.$ModuleDefs['ATQ']['REC_NAME_PLURAL']))
    ),
);
