<?php
  //CQC Outcomes
$FieldDefs["ATM"] = array(
    "recordid" => array("Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "atm_version" => array("Type" => "ff_select",
        "Title" => "Version"),
    "atm_category" => array("Type" => "ff_select",
        "Title" => "Category"),
    "atm_title" => array("Type" => "string",
        "MaxLength" => 255,
        'Width' => 70,
        "Title" => "Title"),
    "atm_ref" => array("Type" => "string",
        "MaxLength" => 64,
        "Width" => 15,
        "Title" => "Reference"),
    "atm_description" => array("Type" => "textarea",
        "Title" => "Description",
        "Rows" => 7,
        "Columns" => 70),
    "atm_notes" => array("Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 7,
        "Columns" => 70),
    'atm_staff' => array(
        'Type' => 'multilistbox',
        'Title' => 'Staff',
    	'MaxLength' => 254
    )
);
?>
