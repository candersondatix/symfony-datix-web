<?php

// Assesment module instance records
$ModuleDefs['ATM'] = array(
    'MOD_ID'  => MOD_ASSESSMENT_MODULE_TEMPLATES, // Module identifier. Used in licensing checks and db tables, such as udf_groups. See Source/libs/constants.php for value of constant
    'CODE'    => 'ATM',
    'GENERIC' => true,
    'GENERIC_FOLDER' => 'ATM',
    'NAME'    => _tk("ATMNamesTitle"),
    'REC_NAME'  => _tk("ATMName"),
    'REC_NAME_PLURAL' => _tk("ATMNames"),
	'REC_NAME_TITLE'  => _tk("ATMNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("ATMNamesTitle"),
    'TABLE' => 'asm_template_modules',
    'TEMPLATE' => '',
    'ACTION' => 'record&module=ATM',
    'MODULE_GROUP' => 'ACR',
    'APPROVAL_LEVELS' => false,
    'USES_APPROVAL_STATUSES' => false,
    'NO_REP_APPROVED' => true,
    'FORMCODE' => 'ATM',
    'PERM_GLOBAL' => 'ATM_PERMS',
    'ICON' => '',
    'FK' => '',
    'FIELD_ARRAY' => array(
        "atm_version",
        "atm_category",
        "atm_title",
        "atm_ref",
        "atm_description",
        "atm_notes",
        'atm_staff'
    ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'ATM', 'HIDECODE' => true)
    ),
    'FORMCODE2' => 'ATM2',
    'FORM_DESIGN_ARRAY_FILE' => 'UserATMForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserATMSettings',
    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,
    'LISTING_FILTERS' => true,
    'BREADCRUMBS' => false,
    'LISTING_WHERE' => ' recordid IS NOT NULL',
    'HAS_HOMESCREEN' => false,   // Variable to decide if this module will appear as a Homescreen
    'EXTRA_FORM_ACTIONS' => array(
        'createAssignments' => array(
            'title'      => 'create_assigned_assessments',
            'sourceFile' => 'Source/generic_modules/ATM/ModuleFunctions.php',
            'callback'   => 'createAssignments',
            'condition'  => (bYN(GetParm('ATM_AAT', 'N')) && $_GET['action'] != 'addnew'),
            'js'         => '', // using js file instead of inline js, see CUSTOM_JS below
            'action'     => 'CREATE'
        )
    ),
    'CUSTOM_JS' => '<script type="text/javascript" src="js_functions/atm_assignments' . ($MinifierDisabled ? '' : '.min') . '.js"></script>',
    'NO_NAV_ARROWS' => true,
    'COMBO_LINK_WHERE' => "AND fmt_field NOT IN ('atm_staff')",

    'LOCAL_HELP' => 'WebHelp/index.html#Accreditation/c_dx_accreditation_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Accreditation/c_dx_accreditation_guide.xml',

);