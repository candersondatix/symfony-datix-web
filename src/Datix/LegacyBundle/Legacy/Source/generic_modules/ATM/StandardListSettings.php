<?php
$list_columns_standard = array(
    "atm_ref" => array(
        'width' => '2'),
    "atm_category" => array(
        'width' => '3'),
    "atm_title" => array(
	    'width' => '5'),
    "atm_description" => array(
	    'width' => '10'),
    "atm_version" => array(
	    'width' => '2'),
);