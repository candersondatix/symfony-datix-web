<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "subprompt" => array(
        "Title" => "Sub-prompt Details",
        "Rows" => array(
            array('Name' => "cts_ref", 'ReadOnly' => true),
            array('Name' => "cts_desc_short", 'ReadOnly' => true),
            array('Name' => "cts_desc", 'ReadOnly' => true),
            array('Name' => "cts_supportingtext", 'ReadOnly' => true),
            array('Name' => "cts_guidance", 'ReadOnly' => true),
            array('Name' => "cdo_location", 'ReadOnly' => $FormType != 'Search'),
            "cds_manager",
            "cds_handlers",
            "cds_status",
            'cds_dlastreview',
            'cds_dnextreview',
            'cds_comments',
        )
    ),
    "evidence" => array(
        "Title" => "Evidence" . ($libCount?" ($libCount)":""),
        "Function" => array('CQC_Outcome', 'listEvidence'),
        "Condition" => $_GET["recordid"] || $FormMode == 'Design',
        "NoFieldAdditions" => true,
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $act['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('ACT', $act['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NewPanel' => true,
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        'NewPanel' => true,
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "linked_records" => array(
        "Title" => _tk('linked_records'),
        "NoFieldAdditions" => true,
        "Special" => "LinkedRecords",
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
);
