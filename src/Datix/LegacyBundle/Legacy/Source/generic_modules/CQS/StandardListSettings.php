<?php

$LowestTier = DatixDBQuery::PDO_fetch('SELECT MAX(loc_tier_depth) FROM vw_locations_main', array(), PDO::FETCH_COLUMN);

$list_columns_standard = array(
    "cts_ref" => array(
        'width' => '10'),
    "cts_desc" => array(
        'width' => '10'),
    "tier_".$LowestTier => array(
        'width' => '10'),
    "cds_manager" => array(
        'width' => '6'),
    "cds_handlers" => array(
        'width' => '6'),
    "cds_status" => array(
        'width' => '6'),
);

//if there's no tiers set up, adding this field will cause an error.
if($LowestTier == '')
{
    unset($list_columns_standard['tier_']);
}
?>
