<?php
/**
* @desc Copies equipment records from one incident record to another. Wrapper function just for incidents until equipment is
* completely cross modular like contacts.
* 
* @param string $module Module short name.
* @param int $SourceIncidentID Recordid of source incident.
* @param int $DestIncidentID Recordid of new incident record
* @param array $aAdditionalParams Array containing additional options.
*/
function CopyIncidentAssets($module, $SourceIncidentID, $DestIncidentID, $aAdditionalParams)
{
    $CopyLinkedAssets = bYN($aAdditionalParams->copy_linked_assets);  
    
    if ($CopyLinkedAssets == true)
    {
        return CopyRecords::CopyLinkedAssets($module, $SourceIncidentID, $DestIncidentID, "INC");
    }
    
    return true; 
}
  
/**
* @desc Copies incident Medication data
* 
* @param string $module Module short name.
* @param int $SourceIncidentID Recordid of source incident.
* @param int $DestIncidentID Recordid of new incident record
* @param array $aAdditionalParams Array containing additional options.
*/
function CopyIncidentMedications($module, $SourceIncidentID, $DestIncidentID, $aAdditionalParams)
{
    $CopyMedications = bYN($aAdditionalParams->copy_medications);

    if ($CopyMedications == true)
    {
        $field_array = array(
            'recordid',
            'inc_id',
            'LISTORDER',
            'IMED_CLASS_ADMIN',
            'IMED_CLASS_CORRECT',
            'IMED_NAME_ADMIN',
            'IMED_NAME_CORRECT',
            'IMED_BRAND_ADMIN',
            'IMED_BRAND_CORRECT',
            'IMED_MANU_ADMIN',
            'IMED_MANU_CORRECT',
            'IMED_SERIAL_NO',
            'IMED_BATCH_NO',
            'IMED_ROUTE_ADMIN',
            'IMED_ROUTE_CORRECT',
            'IMED_DOSE_ADMIN',
            'IMED_DOSE_CORRECT',
            'IMED_FORM_ADMIN',
            'IMED_FORM_CORRECT',
            'IMED_CONTROLLED_ADMIN',
            'IMED_CONTROLLED_CORRECT',
            'IMED_TYPE_ADMIN',
            'IMED_TYPE_CORRECT',
            'IMED_ERROR_STAGE',
            'IMED_ERROR_TYPE',
            'IMED_NOTES',
            'imed_other_factors',
            'imed_right_wrong_medicine_admin',
            'imed_right_wrong_medicine_correct',
            'imed_manufacturer_special_admin'
        );

        $sql = "SELECT recordid, " . implode(', ', $field_array). " FROM inc_medications
                 WHERE inc_id = $SourceIncidentID";
        $result = PDO_fetch_all($sql);

        foreach ($result as $data)
        {
            $recordid = GetNextRecordID('inc_medications', false, 0, 'recordid', "", $_SESSION["initials"]);
            $data["recordid"] = $recordid;
            $data["inc_id"] = $DestIncidentID;

            $InsertSql = GeneratePDOInsertSQLFromArrays(array(
                                'FieldArray' => $field_array,
                                'DataArray' => $data,
                                'Module' => 'INC',
                                ), $PDOParams);

            $sql = "INSERT INTO inc_medications $InsertSql";

            if (!PDO_query($sql, $PDOParams))
            {
                fatal_error("Cannot copy medications data");
                return false;
            }
        }
    }
    return true;
}

/**
 * Copies incident Causal factors data
 *
 * @param string $module Module short name.
 * @param int $SourceID Recordid of source incident/claim.
 * @param int $DestID Recordid of new incident/claim record.
 * @param array $aAdditionalParams Array containing additional options.
 * @return bool True if copy is success, false otherwise.
 */
function CopyCausalFactors($module, $SourceID, $DestID, $aAdditionalParams)
{
    $CopyCausalFactors = bYN($aAdditionalParams->copy_causal_factors);

    if ($CopyCausalFactors == true)
    {
        $field_array = array(
            'listorder',
            'caf_level_1',
            'caf_level_2'
        );

        if ($module == 'INC')
        {
            $field_array[] = 'inc_id';
        }
        elseif ($module == 'CLA')
        {
            $field_array[] = 'cla_id';
        }

        $sql = '
            SELECT
                ' . implode(', ', $field_array) . '
            FROM
                causal_factors
            WHERE
        ';

        if ($module == 'INC')
        {
            $sql.= 'inc_id = ' . $SourceID;
        }
        elseif ($module == 'CLA')
        {
            $sql.= 'cla_id = ' . $SourceID;
        }


        $result = PDO_fetch_all($sql);

        foreach ($result as $data)
        {
            if ($module == 'INC')
            {
                $data['inc_id'] = $DestID;
            }
            elseif ($module == 'CLA')
            {
                $data['cla_id'] = $DestID;
            }

            $InsertSql = GeneratePDOInsertSQLFromArrays(array(
                'FieldArray' => $field_array,
                'DataArray' => $data,
                'Module' => $module,
            ), $PDOParams);

            $sql = 'INSERT INTO causal_factors ' . $InsertSql;

            if (!PDO_query($sql, $PDOParams))
            {
                fatal_error('Cannot copy causal factors data');
                return false;
            }
        }
    }

    return true;
}

?>