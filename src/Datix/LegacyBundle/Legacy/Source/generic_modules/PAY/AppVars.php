<?php

$ModuleDefs['PAY'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'PAY',
    'AUDIT_TRAIL_PERMS' => array('PAY2'),  //which permissions can see the audit trail?

    'MOD_ID' => MOD_PAYMENTS,
    'CODE' => 'PAY',
    'APPROVAL_LEVELS' => false,
    'NAME' => _tk("mod_payments_title"),
    'USES_APPROVAL_STATUSES' => false,
    'VIEW' => 'vw_payments',
    'TABLE' => 'payments',
    'PERM_GLOBAL' => 'PAY_PERMS',
    'REC_NAME' => _tk("PAYName"),
    'REC_NAME_PLURAL' => _tk("PAYNames"),
	'REC_NAME_TITLE' => _tk("PAYNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("PAYNamesTitle"),
   // 'FK' => 'cla_id',
    'ACTION' => 'record&module=PAY',
    'LINK_ACTION' => 'record&module=PAY',
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'PAY2')
        ),
    'FORMCODE2' => 'PAY2',
    'ICON' => 'icons/icon_PAY.png',
    'ADD_NEW_RECORD_LEVELS' => array('CLA2', 'CLA1'),
    'NO_REP_APPROVED' => true,
    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Title' => _tk("pay_all_listing_title"),
            'Link' => _tk("pay_all_listing_link"),
            'Where' => '1=1'
            ),
    ),

    'SEARCH_URL' => 'action=search',
    'DEFAULT_ORDER' => 'pay_date',

    'FIELD_ARRAY' => array(
        'pay_date', 'pay_type', 'pay_subtype', 'pay_amount', 'pay_vat_rate', 'pay_total', 'pay_notes',
        'pay_calc_vat_amount', 'pay_calc_total', 'resp_id', 'pay_recipient'
        ),

    'VIEW_FIELD_ARRAY' => array(
        'pay_date', 'pay_type', 'pay_subtype', 'pay_amount', 'pay_vat_rate', 'pay_total', 'pay_notes',
        'pay_calc_vat_amount', 'pay_calc_total', 'pay_module', 'pay_cas_id', 'pay_link_title', 'resp_id', 'pay_recipient'
        ),

    'FORM_DESIGN_ARRAY_FILE' => 'UserPAYForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserPAY2Settings',

    'DATA_VALIDATION_INCLUDES' => array(
        'Source/generic_modules/PAY/ModuleFunctions.php'
    ),
    'DATA_VALIDATION_FUNCTIONS' => array(
        'validateVAT'
    ),

    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,

    'LINKED_MODULE' => array(
        'parent_ids' => array('CLA' => 'cla_id', 'INC' => 'inc_id', 'COM' => 'com_id'),
        'link_recordid' => 'recordid',
        'panel' => 'payments'
        ),
        
    'EXTRA_SAVE_INCLUDES' => array('Source/generic_modules/PAY/ModuleFunctions.php'),
    'EXTRA_SAVE_FUNCTIONS' => array('updateRCFields'),

    'BEFORE_SAVE_FUNCTIONS' => array('formatRespondentID', 'CheckZeroReserves'),
        
    'CUSTOM_DATA_TRANSFORM' => array('file' => 'Source/generic_modules/PAY/ModuleFunctions.php', 'function' => 'transformPaymentData'),
    'EXTRA_RECORD_DATA_FUNCTION_INCLUDES' => array(
        'Source/generic_modules/PAY/ModuleFunctions.php'
    ),
    'EXTRA_RECORD_DATA_FUNCTIONS' => array(
        'ParentRecordAccessCheck'
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Payments/c_dx_payments_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Payments/c_dx_payments_guide.xml',
);


