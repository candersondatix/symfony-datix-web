<?php
function ListPayments($Data, $FormType, $Module)
{
    global $ModuleDefs;

    $Design = new Listings_ModuleListingDesign(array('module' => 'PAY', 'parent_module' => $Module, 'link_type' => 'payments'));
    $Design->LoadColumnsFromDB();

    $RecordList = new RecordLists_ModuleRecordList();
    $RecordList->Module = 'PAY';
    $RecordList->Columns = $Design->Columns;

    $RecordList->Columns[] = new Listings_ListingColumn($ModuleDefs['PAY']['LINKED_MODULE']['parent_ids'][$Module], 'vw_payments');

    // Remove extra field columns
    foreach ($RecordList->Columns as $key => $Column)
    {
        if ($Column instanceof Listings_UDFListingColumn)
        {
            preg_match('/^UDF.*_([0-9]+)$/ui', $Column->getName(), $matches);
            $extraFieldColumns[$matches[1]] = $Column;
            $extrafieldIds[] = $matches[1];
            unset($RecordList->Columns[$key]);
        }
    }

    $RecordList->Paging = false;

    $whereClause = '1=2';

    if (CanSeeModule('PAY'))
    {
        $whereClause = MakeSecurityWhereClause($ModuleDefs['PAY']['LINKED_MODULE']['parent_ids'][$Module].' = '.$Data['recordid'], 'PAY');
    }

    $RecordList->WhereClause = $whereClause;

    $OrderCol = new Listings_ListingColumn('pay_date', 'vw_payments');
    $OrderCol->setDescending();

    $RecordList->OrderBy = array($OrderCol, new Listings_ListingColumn('recordid', 'vw_payments'));
    $RecordList->RetrieveRecords();

    if ($extraFieldColumns)
    {
        foreach ($RecordList->Records as $record)
        {
            $recordids[] = $record->Data['recordid'];
        }

        $udfData = getUDFData('PAY', $recordids, $extrafieldIds);

        // Add extra field columns and data
        foreach ($extraFieldColumns as $udfId => $Column)
        {
            $RecordList->Columns[] = $Column;

            foreach ($RecordList->Records as $key => $record)
            {
                $RecordList->Records[$key]->Data[$Column->getName()] = $udfData[$record->Data['recordid']][$udfId];
            }
        }
    }

    // transform negative values stored for receipts
    foreach ($RecordList->Records as $record)
    {
        foreach (array('pay_calc_total','pay_calc_vat_amount','pay_amount') as $field)
        {
            if (isset($record->Data[$field]) && $record->Data['pay_type'] == 'RECE')
            {
                $record->Data->pay_calc_total = abs($record->Data->pay_calc_total);
            }
        }
    }

    $ReadOnly = false;
    $AccessLevel = GetAccessLevel('PAY');

    if ($AccessLevel != 'PAY_FULL')
    {
        $ReadOnly = true;
    }

    $Listing = new Listings_ListingDisplay($RecordList, $Design, $Module, $ReadOnly);
    $Listing->EmptyMessage = 'No payments.';

    echo '<li>';

    echo $Listing->GetListingHTML();

    if (!$ReadOnly && $FormType != 'Print')
    {
        echo '<div class="new_link_row" ><a href="'.$scripturl.'?action=addnew&module=PAY&level=2&link_module='.Sanitize::getModule($Module).'&main_recordid='.Sanitize::SanitizeInt($Data['recordid']).'"><b>Add a new payment</b></a></div>';
    }

    echo '</li>';
}

function validateVAT()
{
    $Error = array();

    if($_POST['pay_vat_rate'] < 0)
    {
        $Error['pay_vat_rate'] = 'VAT cannot be less than 0%';
    }

    return $Error;

}

/**
 * Updates the Rich Client payments fields on save to maintain interoperability between the two apps.
 *
 * @param array $data The submitted record data.
 */
function updateRCFields($data)
{
    DatixDBQuery::PDO_query('UPDATE payments SET pay_vat = pay_calc_vat_amount, pay_total = pay_calc_total WHERE recordid = :recordid', array('recordid' => $data['recordid']));
    if ($data['pay_type'] == 'RECE')
    {
        // RC stores these as negavtive values, so...
        DatixDBQuery::PDO_query('UPDATE payments SET pay_amount = 0 - pay_amount, pay_vat = 0 - pay_vat, pay_total = 0 - pay_total WHERE recordid = :recordid', array('recordid' => $data['recordid']));
    }
}

/**
 * Inverts negative values stored for receipts before form view is rendered.
 *
 * @param array $data
 *
 * @return array $data
 */
function transformPaymentData($data)
{
    if ($data['pay_amount'] && $data['pay_type'] == 'RECE')
    {
        $data['pay_amount'] = abs($data['pay_amount']);
    }

    if ($data['pay_calc_total'] && $data['pay_type'] == 'RECE')
    {
        $data['pay_calc_total'] = abs($data['pay_calc_total']);
    }

    if ($data['pay_calc_vat_amount'] && $data['pay_type'] == 'RECE')
    {
        $data['pay_calc_vat_amount'] = abs($data['pay_calc_vat_amount']);
    }

    return $data;
}
/*
 * This checks whether a claim indemnity or expenses reserve needs to be adjusted after this payment has been added to
 * it. This happens if the relevant reserve is closed; this payment is assigned to that reserve and the pay date for this
 * payment; and the date for this payment is blank or before the close date
 *
 * @param $data               takes the data posted in saving a claim and manipulates it before saving
 * @return $data              the return of the manipulated data
 */
function CheckZeroReserves($data)
{
    require_once 'source/generic_modules/CLA/ModuleFunctions.php';
    return zeroReservesOnDate($data);
}

function formatRespondentID($data)
{
    if($data['resp_id'] === "") //stored as an INT in the database, but for DW purposes is a coded field. This writes it to the database in the correct format.
    {
        $data['resp_id'] = NULL;
    }
    return $data;
}

/**
 * Check whether the user has access to the parent record for this payment.
 * @param $data
 */
function ParentRecordAccessCheck($recordid, $module)
{
    $ModuleDefs = \src\framework\registry\Registry::getInstance()->getModuleDefs();
    $parentModule = strtoupper(DatixDBQuery::PDO_fetch('SELECT pay_module FROM vw_payments WHERE recordid= :recordid', array('recordid' => $recordid), PDO::FETCH_COLUMN));

    if ($ModuleDefs[$parentModule]['FK'])
    {
        $parentRecordid = DatixDBQuery::PDO_fetch('SELECT '.$ModuleDefs[$parentModule]['FK'].' FROM vw_payments WHERE recordid = :recordid', array('recordid' => $recordid), PDO::FETCH_COLUMN);

        $whereClause = MakeSecurityWhereClause('recordid = '.$parentRecordid, $parentModule);

        $accessToParent = DatixDBQuery::PDO_fetch(
            'SELECT count(*) FROM '.$ModuleDefs[$parentModule]['TABLE'].' WHERE '.$whereClause, array(), PDO::FETCH_COLUMN
        );
    }

    return array('permissions_to_linked_record' => ($accessToParent == '1'));
}