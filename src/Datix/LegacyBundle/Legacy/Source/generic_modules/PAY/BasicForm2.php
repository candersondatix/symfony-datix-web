<?php

$RespondentField = Forms_SelectFieldFactory::createSelectField('resp_id', 'PAY', $data['resp_id'], $FormType, false, 'Respondent Select', 'Payment Details', $data['CHANGED-resp_id']);
$RespondentField->setSelectFunction('getRespondentsForPayment', array('cla_id' => '"'.$mainRecordid.'"'));

$codes = (new \src\payments\model\respondentfield\RespondentField())->getCodes((new \src\framework\query\Query())->where(['resp_id' => $data['resp_id']]));
if (($code = $codes[$data['resp_id']]) !== null)
{
    if (bYN(GetParm('WEB_SHOW_CODE'), 'N'))
    {
        $RespondentField->setDescription($code->description.': '.$data['resp_id']);
    }
    else
    {
        $RespondentField->setDescription($code->description);
    }
}



$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "details" => array(
        "Title" => "Payment Details",
        "Rows" => array(
            'recordid',
            'pay_module',
            'pay_cas_id',
            array('Name' => 'pay_link_title', 'Condition' => ((CanSeeModule(strtoupper($data['pay_module'])) && $data['permissions_to_linked_record'] === true) || $FormMode == 'Design')),
            array('Name' => 'pay_recipient', 'Condition' => ($linkModule == 'CLA' || $FormMode == 'Design')),
            'pay_date',
            'pay_type',
            'pay_subtype',
            'pay_amount',
            'pay_vat_rate',
            'pay_calc_vat_amount',
            'pay_calc_total',
            'pay_notes',
            array(
                'Type' => 'formfield',
                'Name' => 'resp_id',
                'FormField' => $RespondentField,
                'Condition' => ($linkModule == 'CLA' || $FormMode == 'Design')
            ),
        )
    ),
);

?>