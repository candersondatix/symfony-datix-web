<?php
$FieldDefs["PAY"] = array(
    "recordid" => array("Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5),
    "pay_date" => array("Type" => "date",
        "Title" => "Payment date"),
    "pay_type" => array("Type" => "ff_select",
        "Title" => "Type"),
    "pay_subtype" => array("Type" => "ff_select",
        "Title" => "Subtype"),
    "pay_amount" => array("Type" => "money",
        "Title" => "Amount"),
    "pay_vat_rate" => array("Type" => "decimal",
        "Title" => "VAT rate (%)",
        "MaxValue" => "999.99",
        "MinValue" => "-999.99"),
    "pay_calc_vat_amount" => array("Type" => "money",
        'ReadOnly' => true,
        'Computed' => true,
        "Title" => "VAT Amount"),
    "pay_calc_total" => array("Type" => "money",
        'ReadOnly' => true,
        'Computed' => true,
        "Title" => "Total"),
    "pay_notes" => array("Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 10,
        "Columns" => 70),
    "pay_cas_id" => array("Type" => "number",
        "Title" => "Linked record ID",
        "ReadOnly" => true),
    "pay_module" => array("Type" => "ff_select",
        "Title" => "Module",
        "ReadOnly" => true,
        'CustomCodes' =>
            array(
                'INC' => _tk('mod_incidents_title'),
                'CLA' => _tk('mod_claims_title'),
                'COM' => _tk('mod_complaints_title')
                )),
    "pay_link_title" => array("Type" => "string",
        "Title" => "Name",
        "ReadOnly" => true),
    "resp_id" => array("Type" => "ff_select",
        "Title" => "Respondents",
        "Width" => 5,
        'SelectFunction' => array(
            'getRespondentsForPayment', array(
                'cla_id' => '')),
        ),
    "pay_recipient" => array("Type" => "ff_select",
        "Title" => _tk('pay_recipient'), 
        "data" => array(
            'can-add' => "true")),
);
