<?php
$FieldDefs["AQU"] = array(
    "recordid" => array("Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_question_id" => array("Type" => "number",
        "Title" => "Criterion ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_question_template_id" => array("Type" => "number",
        "Title" => "Criterion template ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_module_id" => array("Type" => "number",
        "Title" => "Assessment ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_template_instance_id" => array("Type" => "number",
        "Title" => "Assigned assessment ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_module_template_id" => array("Type" => "number",
        "Title" => "Assessment template ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "atq_ref" => array("Type" => "string",
        "MaxLength" => 50,
        "Title" => "Reference",
        'SubmoduleSuffix' => _tk('ATQSubmoduleSuffix')),
    "atq_question" => array("Type" => "textarea",
        "MaxLength" => 255,
        'Width' => 70,
        'BlockFromReports' => true, //Otherwise we end up with duplicates with atq_question_code
        "Title" => "Criterion",
        'SubmoduleSuffix' => _tk('ATQSubmoduleSuffix')
    ),
    "atq_question_code" => array(
        "Type" => "ff_select",
        "Title" => "Criterion",
        'SubmoduleSuffix' => _tk('ATQSubmoduleSuffix'),
        'NoListCol_Override' => true
    ),
    "atq_notes" => array("Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 7,
        "Columns" => 70,
        'SubmoduleSuffix' => _tk('ATQSubmoduleSuffix')),
    "atq_order" => array("Type" => "number",
        "Title" => "Order",
        "Width" => 5,
        'SubmoduleSuffix' => _tk('ATQSubmoduleSuffix')),
    'atq_theme' => array(
        'Type' => 'ff_select',
        'Title' => 'Theme',
        'SubmoduleSuffix' => _tk('ATQSubmoduleSuffix')
    ),
    "adq_end_user" => array("Type" => "ff_select",
        "Title" => "End user"),
    "adq_compliance" => array("Type" => "ff_select",
        "Title" => "Compliance level"),
    "adq_action_plan" => array("Type" => "ff_select",
        "Title" => "Action plan"),
    "adq_comments" => array("Type" => "textarea",
        "Title" => "Comments",
        "Rows" => 7,
        "Columns" => 70),
    "adq_score" => array("Type" => "number",
        "Title" => "Score",
        "ReadOnly" => true),
    // module fields
    "atm_version" => array("Type" => "ff_select",
        "Title" => "Version",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_category" => array("Type" => "ff_select",
        "Title" => "Category",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_title" => array("Type" => "string",
        "MaxLength" => 255,
        'Width' => 70,
        "Title" => "Title",
        'BlockFromReports' => true, //Otherwise we end up with duplicates with atm_title_code
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_title_code" => array(
        "Type" => "ff_select",
        "Title" => "Title",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix'),
        'NoListCol_Override' => true
    ),
    "atm_ref" => array("Type" => "string",
        "MaxLength" => 64,
        "Width" => 15,
        "Title" => "Reference",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_description" => array("Type" => "textarea",
        "Title" => "Description",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_notes" => array("Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),

    "ati_location" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Organisation",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')),
    "ati_cycle" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Cycle year",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')),
    'ati_location_additional' => array(
        'Type' => 'multilistbox',
        'Title' => 'Locations',
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix'),
    	'MaxLength' => 8000
    ),
    'ati_due_date' => array(
        'Type' => 'date',
        'Title' => 'Due date',
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')
    ),

    "adm_year" => array("Type" => "ff_select",
        "Title" => "Assessment year",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')),
    "adm_location" => array("Type" => "ff_select",
        "Title" => "Site",
        "SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')),
    "adm_coordinator" => array("Type" => "ff_select",
        "Title" => "Coordinator",
        "SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')),
    "adm_sub_coordinator" => array("Type" => "ff_select",
        "Title" => "Sub-coordinator",
        "SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')),
    "adm_end_user" => array("Type" => "ff_select",
        "Title" => "End user",
        "SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')),
    "ati_sub_rank" => array("Type" => "ff_select",
        "Title" => "Subscriber risk rank",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')),
    "adm_comments" => array("Type" => "textarea",
        "Title" => "Comments",
        "SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')),
    'adm_compliance' => array(
        'Type' => 'ff_select',
        'Title' => 'Compliance level',
        'ReadOnly' => true,
        "SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')),
    "loc_type" => array(
        'Type'     => 'ff_select',
        'Title'    => "Location Type",
        "SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')),
    'adm_rep_approved' => array(
        'Type' => 'ff_select',
        'CustomCodes' => GetLevelstoView('AMO', GetAccessLevel('AMO')),
        'Title' => 'Current approval status',
        'ReadOnly' => true,
		"SubmoduleSuffix" => _tk('AMOSubmoduleSuffix')
    ),
    'loc_name' => array(
        'Type' => 'string',
        'MaxLength' => 254,
        'Title' => 'Name',
        'SubmoduleSuffix' => _tk('LOCSubmoduleSuffix')
    ),
    'loc_type' => array(
        'Type' => 'ff_select',
        'Title' => 'Location type',
        'SubmoduleSuffix' => _tk('LOCSubmoduleSuffix')
    ),
    'loc_address' => array(
        'Type' => 'textarea',
        'Title' => 'Address',
        'Rows' => 7,
        'Columns' => 70,
        'SubmoduleSuffix' => _tk('LOCSubmoduleSuffix')
    ),
    'loc_active' => array(
        'Type' => 'yesno',
        'Title' => 'Active?',
        'BlockFromReports' => true,
        'SubmoduleSuffix' => _tk('LOCSubmoduleSuffix')
    ),
    'loc_tier_name' => array(
        'Type' => 'string',
        'Title' => 'Tier',
        'ReadOnly' => true,
        'SubmoduleSuffix' => _tk('LOCSubmoduleSuffix')
    ),
    'ati_rank' => array(
        'Type' => 'ff_select',
        'Title' => 'Risk Rank',
        'SubmoduleSuffix' => _tk('ATISubmoduleSuffix'),
        'ReadOnly' => true
    ),
    'ati_priority' => array(
        'Type' => 'ff_select',
        'Title' => 'Assessment priority',
        'SubmoduleSuffix' => _tk('ATISubmoduleSuffix'),
        'ReadOnly' => true
    ),
    'adm_percentage' => array(
        'Type' => 'number',
        'Title' => 'Compliance %',
        'ReadOnly' => true
    ),
);
