<?php

$GLOBALS['FormTitle'] = _tk('AQUNameTitle').' Form';

$GLOBALS["HideFields"] = array (
    'atm_notes' => true,
    'ati_location' => true,
    'ati_location_additional' => true,
    'ati_priority' => true,
    'ati_rank' => true,
    'ati_sub_rank' => true,
    'adm_sub_coordinator' => true,
    'adm_compliance' => true,
    'adm_percentage' => true,
    'adm_comments' => true,
    'adm_rep_approved' => true,
    'atq_order' => true,
    'atq_notes' => true,
    'adq_end_user' => true,
    'adq_comments' => true,
    'linked_actions' => true
);

$GLOBALS["UserExtraText"] = array (
    'adq_action_plan' => 'Identify action to be taken to address non-compliance'
);

$GLOBALS["NewPanels"] = array (
  'linked_actions' => true,
);