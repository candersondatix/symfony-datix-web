<?php
$list_columns_standard = array(
    "ati_location" => array(
        'width' => '3'),
    "adm_location" => array(
        'width' => '3'),
    "ati_cycle" => array(
        'width' => '2'),
    "adm_year" => array(
        'width' => '2'),
    "atm_title" => array(
        'width' => '10'),
    "atq_question" => array(
        'width' => '12'),
    "adq_compliance" => array(
        'width' => '2')
);