<?php

    $FormArray = array(
        'Parameters' => array(
            'Panels' => true,
            'Condition' => false
        ),
        'assessment' => array(
            'Title' =>  _tk('AMONameTitle').' Details',
            'Rows' => array(
                array('Name' => 'atm_ref', 'ReadOnly' => true),
                array('Name' => 'atm_version', 'ReadOnly' => true),
                array('Name' => 'atm_category', 'ReadOnly' => true),
                array('Name' => 'atm_title', 'ReadOnly' => true),
                array('Name' => 'atm_description', 'ReadOnly' => true),
                array('Name' => 'atm_notes', 'ReadOnly' => true),
                array('Name' => 'ati_location', 'ReadOnly' => true),
                array('Name' => 'ati_location_additional', 'ReadOnly' => true, 'Type' => 'custom', 'HTML' => getLocationControl($data, 'ReadOnly'), 'Condition' => $FormType != 'Search'),
                array('Name' => 'ati_cycle', 'ReadOnly' => true),
                array('Name' => 'adm_year', 'ReadOnly' => true),
                array('Name' => 'ati_due_date', 'ReadOnly' => true),
                array('Name' => 'ati_priority', 'ReadOnly' => true),
                array('Name' => 'ati_rank', 'ReadOnly' => true),
                array('Name' => 'ati_sub_rank', 'ReadOnly' => true),
                array('Name' => 'adm_location', 'ReadOnly' => true, 'Type' => 'custom', 'HTML' => getLocationParentage($data['adm_location']), 'Condition' => $FormType != 'Search'),
                array('Name' => 'adm_location', 'ReadOnly' => true, 'Condition' => $FormType == 'Search'),
                array('Name' => 'adm_coordinator', 'ReadOnly' => true),
                array('Name' => 'adm_sub_coordinator', 'ReadOnly' => true),
                array('Name' => 'adm_end_user', 'ReadOnly' => true),
                array('Name' => 'adm_compliance', 'ReadOnly' => true),
                array('Name' => 'adm_percentage', 'ReadOnly' => true),
                array('Name' => 'adm_comments', 'ReadOnly' => true),
                'adm_rep_approved'
            )
        ),
        'question' => array(
            'Title' =>  _tk('AQUNameTitle').' Details',
            'Rows' => array(
                array('Name' => 'atq_ref', 'ReadOnly' => true),
                array('Name' => 'atq_order', 'ReadOnly' => true),
                array('Name' => 'atq_theme', 'ReadOnly' => true),
                array('Name' => 'atq_question', 'ReadOnly' => true),
                array('Name' => 'atq_notes', 'ReadOnly' => true),
                'adq_end_user',
                'adq_compliance',
                'adq_action_plan',
                'adq_comments',
                'adq_score'
            )
        ),
        'linked_actions' => array(
            'Title' => 'Actions',
            'NoFieldAdditions' => true,
            'Special' => 'LinkedActions',
            'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
            'Listings' => array('ACT' => array('module' => 'ACT', 'Label' => 'Select a listing to use for the list of '.$ModuleDefs['ACT']['REC_NAME_PLURAL'])),
            'NotModes' => array('New', 'Search'),
            'Rows' => array()
        ),
    );
