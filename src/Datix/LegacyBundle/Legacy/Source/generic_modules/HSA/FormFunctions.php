<?php
/**
* Retrieves module list for Hotspot Agent form module dropdown.
*
* @return array $modules The available modules.
*/
function getHsaModules()
{
    // get all licensed modules
    $licencedModules = getModArray();

    // modules accessible to hotspots
    $validModules = array('INC', 'PAL', 'COM', 'HOT');

    // cross check possible hotspots modules with licensed modules
    foreach ($validModules as $module)
    {
        if (array_key_exists($module, $licencedModules))
        {
            $modules[$module] = $licencedModules[$module];
        }
    }

    asort($modules);
    return $modules;
}

/**
* Executes passed section function and echos result.
*
* Can be used when initially creating the form and when adding an additional section via AJAX.
*
* @param array  $data                               The existing form data.
* @param string $formType                           The form type (e.g. ReadOnly).
* @param string $module                             The module code (not currently used, needs to be included for call to section function from FormClasses.php)
* @param string $extraParameters['sectionFunction'] The function called to retrieve the form section.
* @param string $extraParameters['suffix']          The ID for this section.
*/
function getFormSection($data, $formType, $module, $extraParameters)
{
    echo $extraParameters['sectionFunction']($data, $formType, $extraParameters['suffix']);
}

/**
* Creates the Criteria section of the hotspots agent form which holds individual Conditions.
*
* @param  array   $data                          The existing form data.
* @param  int     $data['condition_max_suffix']  The number (+1) of condition sections currently on the form.
* @param  string  $formType                      The form type (e.g. ReadOnly).
*
* @return string  $section                       The section HTML.
*/
function getCriteriaSection($data, $formType)
{
    // custom subheader for section - display toggled based on value of hsa_use_loc_for_crit field value
    $section .= '
        <div id="hsa_criteria_subheader" class="new_titlebg" style="display:none">&nbsp;'._tk('hsa_criteria_subheader').'</div>';
    
    if (!$data['condition_max_suffix'])
    {
        // new form
        $data['condition_max_suffix'] = 2;
    }

    for ($i = 1; $i < $data['condition_max_suffix']; $i++)
    {
        $section .= '<li id="condition_section_div_' . $i . '">';
        $section .= getConditionSection($data, $formType, $i);
        $section .= '</li>';
    }

    if ($formType != 'ReadOnly' && $formType != 'Print')
    {
        $section .= '
            <li class="new_windowbg" id="add_condition_button_list">
                <input type="button" id="condition_section" value="Add condition" onclick="AddSectionToForm(\'condition\', \'\', $(\'add_condition_button_list\'), \'HSA\', \'\', false, false, null);" />
                <input type="hidden" id="condition_fields" />
                <input type="hidden" id="condition_values" />
                <script language="JavaScript" type="text/javascript">dif1_section_suffix[\'condition\'] = ' . $data['condition_max_suffix'] . '</script>';
    }

    return $section;
}

/**
* Creates individual Condition sections in the Criteria section of the hotspots agent form.
* Called each time the section is added to the form.
*
* @param  array   $data      The existing form data.
* @param  string  $formType  The form type (e.g. ReadOnly).
* @param  string  $suffix    The id for this condition.
*
* @return string  $section   The section HTML.
*/
function getConditionSection($data, $formType, $suffix)
{
    //creating a new form design here so that the extra sections and fields from the main form are not appended to condition sections.
    $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'HSA', 'level' => 2, 'form_type' => $formType));
    $FormDesign->ExtraSections = array();
    $FormDesign->ExtraFields = array();

    $conditionTable = new FormTable($formType, 'HSA', $FormDesign);
    $countTypeField = new FormField($formType);

    $fieldField = Forms_SelectFieldFactory::createSelectField('hsc_field_name', 'HSA', $data['hsc_field_name_' . $suffix], $formType, false, 'Field', 'criteria', $data['CHANGED-hsc_field_name_' . $suffix], $suffix);
    $fieldField->setSelectFunction('getHsaFieldNames');
    $fieldField->setParents(array('hsa_module'));
    $fieldField->setNoParentSuffix(array(0));
    if ($formType != 'Search')
    {
        $fieldField->setOnChangeExtra('clearConditionValues(' . $suffix . ');writeConditionFields();clearCorrespondingRecipientFields();');
    }

    $valuesField = Forms_SelectFieldFactory::createSelectField('hsc_values', 'HSA', $data['hsc_values_' . $suffix], $formType, true, 'Values', 'condition', $data['CHANGED-hsc_values_' . $suffix], $suffix);
    $valuesField->setParents(array('hsc_field_name', 'hsa_module'));
    $valuesField->setNoParentSuffix(array(1));
    $valuesField->setSelectFunction('getHsaFieldValues', array('formType' => '"'.$formType.'"', 'suffix' => '"'.$suffix.'"'));
    if ($formType != 'Search')
    {
        $valuesField->setOnChangeExtra('writeConditionValues();clearCorrespondingRecipientValues();');
    }
    $valuesField->setMimic($data['hsc_field_name_' . $suffix], $data['hsa_module']);
    $valuesField->setIgnoreMaxLength();

    if (!$data['hsc_count_type_' . $suffix])
    {
        // value defaults to 'Count across all' on new forms
        $data['hsc_count_type_' . $suffix] = 'ALL';
    }
    $countTypeField->MakeRadioButtons('hsc_count_type', $data['hsc_count_type_' . $suffix], '', false, '', '', array('ALL' => 'Count across all', 'EACH' => 'Count for each'), $suffix);

    if($suffix > 1 && $formType != 'ReadOnly' && $formType != 'Print')
    {
       $rightHandLink['onclick'] = 'var DivToDelete=$(\'condition_section_div_'.$suffix.'\');DivToDelete.parentNode.removeChild(DivToDelete);writeConditionFields();clearCorrespondingRecipientFields();';
       $rightHandLink['text'] = _tk('delete_section');
    }

    $formArray = array(
        'Parameters' => array(
            'Condition' => false,
            'Suffix' => $suffix),
        'condition' => array(
            'Title' => 'Condition',
            'ContactSuffix' => $suffix,
            'right_hand_link' => $rightHandLink,
            'Rows' => array(
                array('Type' => 'formfield',
                      'Name' => 'hsc_field_name',
                      'FormField' => $fieldField),
                array('Type' => 'formfield',
                      'Name' => 'hsc_values',
                      'FormField' => $valuesField),
                array('Type' => 'formfield',
                      'Name' => 'hsc_count_type',
                      'FormField' => $countTypeField)
            ))
        );

    $conditionTable->MakeForm($formArray, $data, 'HSA');
    $section .= $conditionTable->GetFormTable();

    return $section;
}

/**
* Creates the Hotspot records section used to show hotpost records that have been generated by this agent.
*
* @global string  $scripturl
*
* @param  string  $data      The existing form data.
* @param  string  $formType  The form type (e.g. ReadOnly).
* @param  string  $suffix    The id for this condition (not used in this section function).
*
* @return string  $section   The section HTML.
*/
function getHotspotsSection($data, $formType, $suffix)
{
    global $scripturl;

    $section = '<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';
    $records = '';

    $sql = 'SELECT recordid, hot_name, hot_opened_date, rep_approved
            FROM hotspots_main
            WHERE hsa_id = :recordid
            ORDER BY hsa_id desc';

    $hotspots = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $data['recordid']));
    foreach ($hotspots as $row)
    {
        $openAnchor = ($formType != 'ReadOnly' && $formType != 'Print') ? '<a href="Javascript:if(CheckChange()){SendTo(\'' . $scripturl .
            '?action=record&module=HOT&recordid=' . $row['recordid'] . '\');}">' : '';
        $closeAnchor = ($formType != 'ReadOnly' && $formType != 'Print') ? '</a>' : '';

        $records .= '
        <tr>
            <td class="windowbg2" width="5%">
                ' . $openAnchor . $row['recordid'] . $closeAnchor . '
            </td>
            <td class="windowbg2" width="50%">
                ' . $openAnchor . htmlspecialchars($row['hot_name']) . $closeAnchor . '
            </td>
            <td class="windowbg2" width="20%">
                ' . $openAnchor . FormatDateVal($row['hot_opened_date']) . $closeAnchor . '
            </td>
            <td class="windowbg2" width="25%">
                ' . $openAnchor . code_descr('HOT', 'rep_approved', $row['rep_approved']) . $closeAnchor . '
            </td>
        </tr>';
    }

    if (!$records)
    {
        $section .= '<tr><td class="windowbg2"><strong>No ' . _tk('HOTName') . ' records.</strong></td></tr>';
    }
    else
    {
        $section .= '
        <tr>
            <td class="windowbg" width="5%"><strong>' . GetFormFieldLabel('recordid', 'ID', 'HOT') . '</strong></td>
            <td class="windowbg" width="50%"><strong>' . GetFormFieldLabel('hot_name', 'Title', 'HOT') . '</strong></td>
            <td class="windowbg" width="20%"><strong>' . GetFormFieldLabel('hot_opened_date', 'Opened date', 'HOT') . '</strong></td>
            <td class="windowbg" width="25%"><strong>' . GetFormFieldLabel('rep_approved', 'Approval status', 'HOT') . '</strong></td>
        </tr>';
        $section .= $records;
    }

    $section .= '</table>';
    return $section;
}

/**
* Creates the Recipient Criteria section of the hotspots agent form.
*
* @global array   $JSFunctions
* 
* @param  array   $data                           The existing form data.
* @param  int     $data['recipients_max_suffix']  The number (+1) of individual Recipients sections currently on the form.
* @param  string  $formType                       The form type (e.g. ReadOnly).
* @param  string  $suffix                         The id for this section (not used in this section function).
*
* @return string  $section                        The section HTML.
*/
function getRecipientCriteriaSection($data, $formType, $suffix)
{
    global $JSFunctions;

    $section = '';
    
    if (!$data['recipients_max_suffix'])
    {
        // new form
        $data['recipients_max_suffix'] = 2;
    }

    for ($i = 1; $i < $data['recipients_max_suffix']; $i++)
    {
        $section .= '<li id="recipients_section_div_' . $i . '"><ol>';
        $section .= getRecipientsSection($data, $formType, $i);
        $section .= '</ol></li>';
    }

    if ($formType != 'ReadOnly' && $formType != 'Print')
    {
        $section .= '
            <li class="new_windowbg" id="add_recipients_button_list">
                <input type="button" id="recipients_section" value="Add recipients" onclick="AddSectionToForm(\'recipients\', \'\', $(\'add_recipients_button_list\'), \'HSA\', \'\', false, false, null);" />
                <script language="JavaScript" type="text/javascript">dif1_section_suffix[\'recipients\'] = ' . $data['recipients_max_suffix'] . '</script>
            </li>';
    }

    // we also need to create a javascript array which contains the location field mappings for each of the modules
    // so we can refer to it when clearing out recipient conditions etc
    $locationMapping = 'var locationFieldMappings = {};';
    foreach (getHsaModules() as $code => $module)
    {
        $locationMapping .= 'locationFieldMappings["'.$code.'"] = {};';
        foreach (array('hsa_organisation','hsa_unit','hsa_clingroup','hsa_directorate','hsa_specialty','hsa_loctype','hsa_locactual') as $locField)
        {
            $fieldObj = new Fields_Field($locField);
            $fieldMapping = $fieldObj->getFieldMapping($code);
            if ($fieldMapping != '')
            {
                $locationMapping .= 'locationFieldMappings["'.$code.'"]["'.$fieldMapping.'"] = "'.$locField.'";';   
            }   
        }    
    }
    $JSFunctions[] = $locationMapping;
    
    return $section;
}

/**
* Creates individual Recipients sections in the Recipients Criteria section of the hotspots agent form.
* Called each time the section is added to the form.
*
* @global array   $UserLabels
*
* @param  array   $data                                       The existing form data.
* @param  string  $data['recipient_conditions_x_max_suffix']  The number (+1) of individual Recipient Condition sections currently within this Recipient Criteria section
*                                                             (where 'x' is the id of the Recipient Criteria section).
* @param  string  $formType                                   The form type (e.g. ReadOnly).
* @param  string  $suffix                                     The id for this Recipients section.
*
* @return string  $section                                    The section HTML.
*/
function getRecipientsSection($data, $formType, $suffix)
{
    global $UserLabels;

    if ($suffix > 1)
    {
        // prevents malformed HTML causing the setting of the new section's innerHTML
        // property to fail when adding new section via AJAX
        //$section = '<ol>';

        // also add a new header so criteria sections are easier to differentiate
        $section .= '
            <div name="recipients_criteria_title_row" id="recipients_criteria_title_row" class="section_title_row">
                <div class="section_title_wrapper">
                    <div class="section_title_group">
                        <div class="section_title">' . ($UserLabels['recipients_criteria'] ? $UserLabels['recipients_criteria'] : 'Recipient criteria') . '</div>
                    </div>';

        if ($formType != 'ReadOnly' && $formType != 'Print')
        {
            // delete link for this recipients section
            $section .= '
                    <div class="title_rhs_container">
                        <div class="section_title_right_hand_link" onclick="var DivToDelete=$(\'recipients_section_div_' . $suffix . '\');DivToDelete.parentNode.removeChild(DivToDelete)">
                            Delete Section
                        </div>
                    </div>';
        }


        $section .= '
                </div>
            </div>
            <li><ol>';
    }

    $recipientTable = new FormTable($formType);

    $secGroupField = Forms_SelectFieldFactory::createSelectField('hsr_sec_groups', 'HSA', $data['hsr_sec_groups_' . $suffix], $formType, true, _tk('security_group'), 'recipients', $data['CHANGED-hsr_sec_groups_' . $suffix], $suffix);
    $secGroupField->setSelectFunction('getSecurityGroups');
    $secGroupField->setIgnoreMaxLength();

    $usersField = Forms_SelectFieldFactory::createSelectField('hsr_users', 'HSA', $data['hsr_users_' . $suffix], $formType, true, 'Users', 'recipients', $data['CHANGED-hsr_users_' . $suffix], $suffix);
    $usersField->setSelectFunction('getHsaRecipients');
    $usersField->setIgnoreMaxLength();
    
    $formArray = array(
        'Parameters' => array(
            'Condition' => false,
            'Suffix' => $suffix),
        'recipients' => array(
            'Title' => 'Recipients',
            'ContactSuffix' => $suffix,
            'Rows' => array(
                array('Type' => 'formfield',
                      'Name' => 'hsr_sec_groups',
                      'FormField' => $secGroupField),
                array('Type' => 'formfield',
                      'Name' => 'hsr_users',
                      'FormField' => $usersField)
            )),
        );

    $recipientTable->MakeForm($formArray, $data, 'HSA');
    $section .= $recipientTable->GetFormTable();

    if (!$data['recipient_conditions_' . $suffix . '_max_suffix'])
    {
        // no conditions defined on this criteria section
        $data['recipient_conditions_' . $suffix . '_max_suffix'] = 2;
    }

    //$section .= '</ol></li>';
    for ($i = 1; $i < $data['recipient_conditions_' . $suffix . '_max_suffix']; $i++)
    {
        $section .= '<li id="recipient_conditions_' . $suffix . '_section_div_' . $i . '"><ol>';
        $section .= getRecipientConditionSection($data, $formType, $suffix . '_' . $i);
        $section .= '</ol></li>';
    }

    if ($formType != 'ReadOnly' && $formType != 'Print')
    {
        $section .= '
            <div class="new_windowbg" id="add_recipient_conditions_' . $suffix . '_button_list">
                <input type="button" id="recipient_conditions" value="Add condition" onclick="AddSectionToForm(\'recipient_conditions_' . $suffix . '\', \'\', $(\'add_recipient_conditions_' . $suffix . '_button_list\'), \'HSA\', \'\', false, false, null);" />
                <input type="hidden" id="recipient_conditions_'.$suffix.'_max_suffix" name="recipient_conditions_'.$suffix.'_max_suffix" value="'.$data['recipient_conditions_' . $suffix . '_max_suffix'].'" />
            </div>
            ';
    }

    if ($suffix > 1)
    {
        // maintain well-formed HTML when adding sections via AJAX
        $section .= '</li>';//</ol>';
    }

    return $section;
}

/**
* Creates individual Recipient condition sections which appear in each of the Recipient criteria sections of the hotspots agent form.
* Called each time the section is added to the form.
*
* @param  array   $data                 The existing form data.
* @param  string  $formType             The form type (e.g. ReadOnly).
* @param  string  $suffix               The id for this field in the format n_n, where the first number is
*                                       the recipient criteria id and the second the condition id.
*
* @return string  $valuesField->Field   The field HTML.
*/
function getRecipientConditionSection($data, $formType, $suffix)
{
    // split suffix into values for criteria block and condition section
    $suffixParts = explode('_', $suffix);

    $recipientConditionTable = new FormTable($formType);

    $fieldField = Forms_SelectFieldFactory::createSelectField('hrc_field_name', 'HSA', $data['hrc_field_name_' . $suffix], $formType, false, 'Field', 'recipient_conditions', $data['CHANGED-hrc_field_name_' . $suffix], $suffix);
    $fieldField->setParents(array('condition_fields'));
    $fieldField->setNoParentSuffix(array(0));
    $fieldField->setSelectFunction('getHsaRecipientFieldNames', array('locationFieldValues' => 'locationFieldValues'));
    $fieldField->setOnChangeExtra('clearRecipientConditionValues(\''.$suffix.'\');');

    $valuesField = Forms_SelectFieldFactory::createSelectField('hrc_values', 'HSA', $data['hrc_values_' . $suffix], $formType, true, 'Values', 'recipient_conditions', $data['CHANGED-hrc_values_' . $suffix], $suffix);
    $valuesField->setParents(array('hrc_field_name', 'condition_values'));
    $valuesField->setNoParentSuffix(array(1));
    $valuesField->setSelectFunction('getHsaRecipientFieldValues', array('formType' => '"'.$formType.'"', 'suffix' => '"'.$suffix.'"', 'locationFieldValues' => 'locationFieldValues'));
    $valuesField->setMimic($data['hrc_field_name_' . $suffix], $data['hsa_module']);
    $valuesField->setIgnoreMaxLength();

    if ($suffixParts[1] > 1 && $formType != 'ReadOnly' && $formType != 'Print')
    {
       $rightHandLink['onclick'] = 'var DivToDelete=$(\'recipient_conditions_'.$suffixParts[0].'_section_div_'.$suffixParts[1].'\');DivToDelete.parentNode.removeChild(DivToDelete)';
       $rightHandLink['text'] = _tk('delete_section');
    }

    $formArray = array(
        'Parameters' => array(
            'Condition' => false,
            'Suffix' => $suffix),
        'recipient_conditions' => array(
            'Title' => 'Recipient conditions',
            'ContactSuffix' => $suffix,
            'right_hand_link' => $rightHandLink,
            'Rows' => array(
                array('Type' => 'formfield',
                      'Name' => 'hrc_field_name',
                      'FormField' => $fieldField),
                array('Type' => 'formfield',
                      'Name' => 'hrc_values',
                      'FormField' => $valuesField),
            ))
        );

    $recipientConditionTable->MakeForm($formArray, $data, 'HSA');
    $section .= $recipientConditionTable->GetFormTable();

    return $section;
}
