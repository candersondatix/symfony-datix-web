<?php

require_once 'Source/generic_modules/HSA/FormFunctions.php';
    
$moduleField = Forms_SelectFieldFactory::createSelectField('hsa_module', 'HSA', $data['hsa_module'], $FormType, false, 'Module', 'details', $data['CHANGED-hsa_module']);

if ($FormType != 'Search' && $FormType != 'Print')
{
    $moduleField->setOnChangeExtra('clearModuleDependentFields();writeLocationValues();');
}

$moduleField->setCustomCodes(getHsaModules());
    
$basedOnDateField = Forms_SelectFieldFactory::createSelectField('hsa_date_field', 'HSA', $data['hsa_date_field'], $FormType, false, 'Based on date field', 'settings', $data['CHANGED-hsa_date_field']);
$basedOnDateField->setParents(array('hsa_module'));
$basedOnDateField->setSelectFunction('getHsaBasedOnDate');
    
$retriggerField = Forms_SelectFieldFactory::createSelectField('hsa_retrigger', 'HSA', $data['hsa_retrigger'], $FormType, false, 'Re-trigger setting', 'settings', $data['CHANGED-hsa_retrigger']);
$retriggerField->setCustomCodes(array('NOSAME' => 'Not for same specific criteria', 'NEVER' => 'Never'));
    
$queryField = Forms_SelectFieldFactory::createSelectField('hsa_query', 'HSA', '', $FormType, false, 'Query', 'additionalcriteria');
$queryField->setParents(array('hsa_module'));
$queryField->setSelectFunction('getHsaQuery');
$queryField->setOnChangeExtra('getQueryString(jQuery("#hsa_query").val(), jQuery("#hsa_module").val());');
    
$currentQuery = $_GET['qbe_success'] ? $_SESSION[$data['hsa_module']]['WHERE'] : $data['hsa_where_clause'];
$whereClauseField = new FormField($FormType);
$whereClauseField->MakeTextAreaField('hsa_where_clause', 10, 70, '', $currentQuery, false);

if ($FormType != 'ReadOnly')
{
    $whereClauseField->Field .= '
    <div>
        <input type="hidden" id="qbe_search_module" name="qbe_search_module" value="" />
        <input type="button" value="Define criteria using query by example" onclick="queryByExample(this.form);" />
    </div>';
}

if ($_GET['qbe_success'] && $_SESSION[$data['hsa_module']]['WHERE'] != $data['hsa_where_clause'])
{
    $whereClauseField->Field .= '<script type="text/javascript">Event.observe(window, \'load\', function(){setChanged(\'hsa_where_clause\');});</script>';
}
    
$emailTemplateField = Forms_SelectFieldFactory::createSelectField('hsa_emt_recordid', 'HSA', $data['hsa_emt_recordid'], $FormType, false, 'Email templates', 'email_templates', $data['CHANGED-hsa_emt_recordid']);
$emailTemplateField->setSelectFunction('getEmailTemplates', array('module' => '"HOT"'));
$emailTemplateField->setSuppressCodeDisplay();
    
$FormArray = array(
    'Parameters' => array(
        'Panels' => true,
        'Condition' => false
    ),
    'details' => array(
        'Title' => 'Details of '._tk('HSANameTitle'),
        'Rows' => array(
            array(
                'Name' => 'recordid',
                'Condition' => ($data['recordid'] || $FormType == 'Design' || $FormType == 'Search')
            ),
            'hsa_name',
            'hsa_description',
            array(
                'Type' => 'formfield',
                'Name' => 'hsa_module',
                'FormField' => $moduleField
            )
        )
    ),
    "location" => array(
        "Title" => "Location",
        "Rows" => array(
            'hsa_organisation',
            'hsa_unit',
            'hsa_clingroup',
            'hsa_directorate',
            'hsa_specialty',
            'hsa_loctype',
            'hsa_locactual',
            'hsa_use_loc_for_crit'
        )
    ),
    'settings' => array(
        'Title' => 'Settings',
        'Rows' => array(
            'hsa_count_threshold',
            'hsa_within',
            'hsa_effective_from',
            'hsa_effective_to',
            array(
                'Type' => 'formfield',
                'Name' => 'hsa_date_field',
                'FormField' => $basedOnDateField
            ),
            array(
                'Type' => 'formfield',
                'Name' => 'hsa_retrigger',
                'FormField' => $retriggerField
            ),
            'hsa_handler',
        )
    ),
    'criteria' => array(
        'Title' => 'Criteria',
        'Condition' => $FormType != 'Search',
        'Function' => 'getFormSection',
        'ExtraParameters' => array('sectionFunction' => 'getCriteriaSection'),
        'Rows' => array()
    ),
    'email_templates' => array(
        'Title' => 'Email templates',
        'Condition' => $FormType != 'Search',
        'Rows' => array(
            array(
                'Type' => 'formfield',
                'Name' => 'hsa_emt_recordid',
                'FormField' => $emailTemplateField
            ),
        )
    ),
    'recipients_criteria' => array(
        'Title' => 'Recipients criteria',
        'Condition' => $FormType != 'Search',
        'NewPanel' => true,
        'Function' => 'getFormSection',
        'ExtraParameters' => array('sectionFunction' => 'getRecipientCriteriaSection'),
        'Rows' => array()
    ),
    'hotspots' => array(
        'Title' => _tk('HOTNameTitle') . ' records',
        'Condition' => $FormType != 'New' && $FormType != 'Search',
        'NewPanel' => true,
        'Function' => 'getFormSection',
        'ExtraParameters' => array('sectionFunction' => 'getHotspotsSection'),
        'Rows' => array()
    ),
);
