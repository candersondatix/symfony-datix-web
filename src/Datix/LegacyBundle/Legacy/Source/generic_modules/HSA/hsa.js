/**
* Holds the current values of the location fields.  Used when deciding 
* which fields/values are available for recipient criteria.
*/
var locationFieldValues = {};

/**
* Store current Condition/Location Field values on page load and display/hide criteria 
* section subheder depending on whether location values are being used.
*/
jQuery(document).ready(function() 
{
    writeConditionFields();
    writeConditionValues();
    writeLocationValues();
    toggleCriteriaSubheader();    
});

/**
* Called when the HSA Module field is changed
* to clear all module-dependent fields.
*/
function clearModuleDependentFields()
{
    clearBasedOnDateField();
    clearConditionFields();
    clearAdditionalCriteria();
    clearRecipientConditionFields();
}

/**
* Clears the 'Based on date' field on the HSA form.
*/
function clearBasedOnDateField()
{
    // clear the value for the input
    jQuery("#hsa_date_field").val("");
   
    // remove the code description text
    jQuery("#hsa_date_field_title").val("");
}

/**
* Clears the fields in each of the 'Condition' sections on the HSA form.
*/
function clearConditionFields()
{
    var idParts = new Array(),
        suffix;
    
    jQuery("input[id^=hsc_field_name][id$=_title]").each(function()
    {
        // clear the field's value/cache
        jQuery(this).clearField();
        
        // get the suffix for this field
        idParts = jQuery(this).data("fieldId").split('_');
        suffix = idParts[idParts.length-1];
        
        // clear the corresponding condition values
        clearConditionValues(suffix);            
    });
}

/**
* Clears the 'Values' field in a given 'Condition' section on the HSA form.
*
* @param int suffix: The ID for this condition.
*/
function clearConditionValues(suffix)
{
    jQuery("#hsc_values_" + suffix + "_title").clearField();
}

/**
* Clears the 'Additional criteria' fields on the HSA form.
*
* Currently just a stub since this section of the form has been (temporarily?) removed.
*/
function clearAdditionalCriteria()
{
    /*
    // clear the value and description for the 'Query' field
    $('hsa_query').setValue('');
    $('hsa_query_title').innerHTML = '';
    
    // clear the 'SQL WHERE clause' field
    $('hsa_where_clause').setValue('');
    */ 
}

/**
* Clears the fields in each of the 'Recipient conditions' sections on the HSA form.
*/
function clearRecipientConditionFields()
{
    var idParts = new Array(),
        suffix;
    
    jQuery("input[id^=hrc_field_name][id$=_title]").each(function()
    {
        // clear the field's value
        jQuery(this).clearField();
        
        // get the suffix for this field
        idParts = jQuery(this).data("fieldId").split('_');
        suffix = idParts[idParts.length-2] + '_' + idParts[idParts.length-1];
        
        // clear the corresponding condition values
        clearRecipientConditionValues(suffix);            
    }); 
}

/**
* Clears the 'Values' field in a given 'Condition' section on the HSA form.
*
* @param int suffix: The ID for this condition.
*/
function clearRecipientConditionValues(suffix)
{
    jQuery("#hrc_values_" + suffix + "_title").clearField();
}

/**
* Clears Recipient Condition Fields of a certain value.  
* Used when Criteria Condition Fields are changed to clear 
* any corresponding Recipient Condition Fields.
*/
function clearCorrespondingRecipientFields()
{
    var idParts = new Array(),
        suffix,
        field,
        valid,
        mappedField;
    
    jQuery("input[id^=hrc_field_name][id$=_title]").each(function()
    {
        field = jQuery(this);
        initDropdown(field);
        
        // clear the field's cache so any newly available fields in the condition section can be selected
        field.data("datixselect").cache = new Array();
        
        // determine if the current value is still valid
        valid = false;
        if (field.val() == "")
        {
            // no need to clear if already empty
            valid = true;    
        }
        else if (jQuery("#condition_fields").val().indexOf(field.data("datixselect").field.val()) > -1)
        {
            // the value still exists in the condition fields section
            valid = true;    
        }
        else if (locationFieldValues['hsa_use_loc_for_crit'] == "Y" && jQuery("#hsa_module").val() != "")
        {
            // we're using location fields as criteria, so check for a valid location field
            mappedField = locationFieldMappings[jQuery("#hsa_module").val()][field.data("datixselect").field.val()];
            if (mappedField != undefined)
            {
                // this value is a location field, check to see if the mapped HSA equivalent has a value assigned
                if (jQuery("#"+mappedField).val() != "")
                {
                    valid = true;    
                }        
            }        
        }
        
        if (!valid)
        {
            // clear the field's value
            field.clearField();
            
            // get the suffix for this field
            idParts = field.data("fieldId").split('_');
            suffix = idParts[idParts.length-2] + '_' + idParts[idParts.length-1];
            
            // clear the corresponding condition values
            clearRecipientConditionValues(suffix);    
        }    
    });         
}

/**
* Clears Recipient Condition Values fields of a certain values.  
* Used when Criteria Condition Values fields are changed to clear 
* any corresponding Recipient Condition Values.
*/
function clearCorrespondingRecipientValues()
{
    var conditionValues = jQuery("#condition_values").length ? jQuery("#condition_values").val().split(',') : "",
        idParts = new Array(),
        suffix,
        field,
        valid,
        mappedField;
    
    jQuery("input[id^=hrc_values][id$=_title]").each(function()
    {
        field = jQuery(this);
        initDropdown(field);
        
        // get the suffix for this 'Values' field
        idParts = field.data("fieldId").split('_');
        suffix = idParts[idParts.length-2] + '_' + idParts[idParts.length-1];
        
        // reset the activeValues for this field
        field.data("datixselect").activeValues = [];
        
        // for each current value of this field
        field.data("datixselect").valuesList.find("li").each(function()
        {
            //determine if value is still valid
            valid = false;
            if (jQuery(this).text().charAt(0) == "@")
            {
                // an @user_con code - these are always available for location fields
                valid = true;    
            }
            else if (conditionValues.indexOf(jQuery("#hsa_module").val() + '*' + jQuery("#hrc_field_name_" + suffix).val() + '*' + jQuery(this).attr("id")) > -1)
            {
                // the value still exists in the condition fields section
                valid = true;    
            }
            else if (locationFieldValues['hsa_use_loc_for_crit'] == "Y" && jQuery("#hsa_module").val() != "")
            {
                // we're using location fields as criteria, so check for a valid location field value
                mappedField = locationFieldMappings[jQuery("#hsa_module").val()][jQuery("#hrc_field_name_" + suffix).val()];
                if (mappedField != undefined)
                {
                    // this value is a location field, check to see if value is the same as the current HSA equivalent
                    if (jQuery(this).attr("id") == jQuery("#"+mappedField).val())
                    {
                        valid = true;    
                    }        
                }       
            }
            
            if (!valid)
            {
                // this is no longer a valid recipient condition value, so add to activeValues
                field.data("datixselect").activeValues.push(jQuery(this).attr("id"));     
            }    
        });
        
        // delete values which are no longer valid
        field.deleteSelectedItems();        
    });         
}

/**
* Fetches the WHERE clause of a specified saved query and 
* populates the 'SQL where clause' field on the HSA form.
* 
* @param int    queryId: The ID of the saved query.
* @param string module:  The module code.
*/
function getQueryString(queryId, module)
{
    new Ajax.Request
    (
        scripturl + '?action=httprequest&type=getQueryString&responsetype=json',
        {
            method: 'post',
            postBody: '&query_id=' + queryId + '&module=' + module,
            onSuccess: function(r)
            {
                var data = r.responseText.evalJSON();
                $('hsa_where_clause').value = data[0];
                setChanged('hsa_where_clause');                 
            },
            onFailure: function()
            {
                alert('Error retrieving query.');
            }
        }
        
    );
}

/**
* Handles the query by example form action.
*
* Users are redirected to the appropriate search screen (based on the currently selected module)
* which they are able to use to create a WHERE clause for the hotspot agent.
*
* @param object form: The HSA form.
*/
function queryByExample(form)
{
    if ($('hsa_module').value == '')
    {
        // user needs to select a module before viewing the search screen
        alert('Please select a Module.');        
    }
    else
    {
        $('rbWhat').value = 'QBE';
        $('qbe_search_module').value = $('hsa_module').value;
        submitClicked = true;
        selectAllMultiCodes();
        if(validateOnSubmit())
        { 
            writeSuffixLimits();
            form.submit(); 
        }   
    }    
}

/**
* Writes a list of the form's current Condition Fields to a hidden form input.
*
* Used when populating Recipient Condition Fields.
* Called when page first loads and when any Condition Field is changed.
*/
function writeConditionFields()
{
    if (jQuery("#condition_fields").length)
    {
        var fieldValues = new Array();
        jQuery("input[id^=hsc_field_name][type=hidden]").each(function()
        {
            if (jQuery(this).val() != "")
            {
                fieldValues.push(jQuery(this).val());    
            }    
        });
        
        // set the value of the condition fields form element
        jQuery("#condition_fields").val(fieldValues.join());   
    }
}

/**
* Writes a list of the form's current Field Values to a hidden form input.
*
* Used when populating Recipient Condition Values.
* Called when page first loads and when any Condition Values are changed.
*/
function writeConditionValues()
{
    if (jQuery("#condition_values").length)
    {
        var valuesValues = new Array(),
            idParts = new Array(),
            suffix,
            fieldValue;
        
        jQuery("select[id^=hsc_values]").each(function()
        {
            // get the suffix for this 'Values' field
            idParts = jQuery(this).attr("id").split('_');
            suffix = idParts[idParts.length-1];
            
            // for each current value
            jQuery(this).children().each(function()
            {
                // values stored in the format module*field_name*value
                valuesValues.push(jQuery("#hsa_module").val() + '*' + jQuery("#hsc_field_name_" + suffix).val() + '*' + jQuery(this).val());    
            });    
        });
        
        // set the value of the condition fields form element
        jQuery("#condition_values").val(valuesValues.join());   
    }    
}

/**
* Shows/hides criteria section subheader based on value of 'use location for criteria' field.
*/
function toggleCriteriaSubheader()
{
    if (jQuery("#hsa_use_loc_for_crit").val() == 'Y')
    {
        jQuery("#hsa_criteria_subheader").show();    
    }
    else
    {
        jQuery("#hsa_criteria_subheader").hide();    
    }
}

/**
* Stores the current location field values in an object, which is used
* when deciding which fields/values are available for recipient criteria.
*/
function writeLocationValues()
{
    jQuery.each(["hsa_organisation","hsa_unit","hsa_clingroup","hsa_directorate","hsa_specialty","hsa_loctype","hsa_locactual","hsa_use_loc_for_crit","hsa_module"], function()
    {
        locationFieldValues[this] = jQuery("#"+this).val();        
    });   
}