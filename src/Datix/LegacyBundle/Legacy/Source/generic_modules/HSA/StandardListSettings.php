<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '2'),
    "hsa_name" => array(
        'width' => '10'),
    "hsa_description" => array(
        'width' => '30'),
    "hsa_effective_from" => array(
	    'width' => '6'),
    'hsa_effective_to' => array (
	    'width' => '6'),
);

$list_columns_mandatory = array("recordid");
?>
