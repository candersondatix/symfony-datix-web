<?php
$ModuleDefs['HSA'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'HSA',
    'LEVEL1_PERMS' => array(),

    'MOD_ID' => MOD_HOTSPOTAGENTS,
    'CODE' => 'HSA',
    'NAME' => _tk("mod_hotspotagents_title"),
    'USES_APPROVAL_STATUSES' => false,
    'NO_REP_APPROVED' => true,
    'TABLE' => 'hotspots_agent',
    //'AUDIT_TABLE' => 'aud_compl_main',
    'REC_NAME' => _tk("HSAName"),
    'REC_NAME_PLURAL' => _tk("HSANames"),
	'REC_NAME_TITLE' => _tk("HSANameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("HSANamesTitle"),
    'FK' => 'hsa_id',
    'PERM_GLOBAL' => 'HSA_PERMS',
    'ACTION' => 'record&module=HSA',
    'FIELD_NAMES' => array(
        'NAME' => 'hsa_name',
        ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'HSA2')
        ),
    'LOGGED_OUT_LEVEL1' => false,
    'NO_LEVEL1' => true,
    'FORMCODE2' => 'HSA',
    'MAIN_MENU_ICON' => 'hotspotagent_24.png',
    'ADD_NEW_RECORD_LEVELS' => array('HSA_FULL'),
    'NEW_RECORD_ACTION' => 'addnew&amp;level=2&amp;module=HSA',
    'ICON' => 'icons/icon_HSA_34x32.png',

    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,

    'SEARCH_URL' => 'action=search',

    'LINKED_RECORDS' => array(
                        'criteria' => array(
                            'type' => 'condition',
                            'table' => 'hotspots_condition',
                            'recordid_field' => 'recordid',
                            'basic_form' => array('Rows' => array('hsc_field_name', 'hsc_values', 'hsc_count_type')),
                            'main_recordid_label'=>'hsa_id',
                            'useIdentity' => true,
                            'useGenericGet' => true
                            )
                        ),

    'STAFF_EMPL_FILTER_MAPPINGS' => array('hsa_organisation' => 'con_orgcode',
                                    'hsa_unit' => 'con_unit',
                                    'hsa_clingroup' => 'con_clingroup',
                                    'hsa_directorate' => 'con_directorate',
                                    'hsa_specialty' => 'con_specialty',
                                    'hsa_loctype' => 'con_loctype',
                                    'hsa_locactual' => 'con_locactual'),

    'FIELD_ARRAY' => array(
        'hsa_name', 'hsa_description', 'hsa_effective_from', 'hsa_effective_to', 'hsa_module',
        'hsa_where_clause', 'hsa_count_threshold', 'hsa_within', 'hsa_date_field', 'hsa_retrigger',
        'hsa_reminder_notsta', 'hsa_reminder_inprog', 'hsa_handler', 'hsa_emt_recordid', 'hsa_organisation',
        'hsa_unit', 'hsa_clingroup', 'hsa_directorate', 'hsa_specialty', 'hsa_loctype', 'hsa_locactual', 'hsa_use_loc_for_crit'
        ),

    'FORM_DESIGN_ARRAY_FILE' => 'UserHSAForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserHSASettings',

    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Title' => _tk("com_all_listing_title"),
            'Link' => _tk("com_all_listing_link"),
            'Where' => '1=1'
            ),
    ),

    'NO_REPORTING' => true,

    'CUSTOM_JS' => '<script language="javascript" type="text/javascript" src="Source/generic_modules/HSA/hsa' . ($MinifierDisabled ? '' : '.min') . '.js"></script>',

    'EXTRA_RECORD_DATA_FUNCTIONS' => array(
        'getRecipients'
    ),
    'EXTRA_RECORD_DATA_FUNCTION_INCLUDES' => array(
        'Source/generic_modules/HSA/ModuleFunctions.php'
    ),
    'EXTRA_SAVE_INCLUDES' => array(
        'Source/generic_modules/HSA/ModuleFunctions.php'
    ),
    'EXTRA_SAVE_FUNCTIONS' => array(
        'saveRecipients'
    ),
    'DATA_VALIDATION_INCLUDES' => array(
        'Source/generic_modules/HSA/ModuleFunctions.php'
    ),
    'DATA_VALIDATION_FUNCTIONS' => array(
        'validateHsaWhereClause', 'validateEffectiveDates', 'validateCriteria'
    ),

    'COMBO_LINK_WHERE' => "AND fmt_field in ('hsa_organisation','hsa_unit','hsa_clingroup','hsa_directorate','hsa_specialty','hsa_loctype','hsa_locactual')",

    'LOCAL_HELP' => 'WebHelp/index.html#HotspotAgents/c_dx_hotspotagents_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/HotspotAgents/c_dx_hotspotagents_guide.xml',
    );
