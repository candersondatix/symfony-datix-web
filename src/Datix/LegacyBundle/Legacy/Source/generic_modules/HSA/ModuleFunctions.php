<?php
/**
* Checks the syntax of the 'SQL WHERE clause' entered on the HSA form.
*
* @param  string  $_POST['hsa_where_clause']  The SQL WHERE clause entered on the HSA form.
* @param  string  $_POST['hsa_module']        The module entered on the HSA form.
*
* @return string  $error                      The error message.
*/
function validateHsaWhereClause()
{
    $error = array();

    if (!empty($_POST['hsa_where_clause']))
    {
        if (!ValidateWhereClause(Sanitize::SanitizeString($_POST['hsa_where_clause'], FILTER_FLAG_NO_ENCODE_QUOTES), Sanitize::getModule($_POST['hsa_module'])))
        {
            $error['hsa_where_clause']['message'] = 'Statement contains a syntax error';
        }
        elseif (\UnicodeString::strpos(\UnicodeString::strtolower($_POST['hsa_where_clause']), '@prompt') !== false)
        {
            $error['hsa_where_clause']['message'] = 'Statement should not contain \'@prompt\'';
        }

    }

    return $error;
}

/**
* Checks that the 'Effective to' date has not been set to earlier than the 'Effective from' date on the HSA form.
*
* @param  string  $_POST['hsa_effective_from']  The SQL WHERE clause entered on the HSA form.
* @param  string  $_POST['hsa_effective_to']    The module entered on the HSA form.
*
* @return string  $error                        The error message.
*/
function validateEffectiveDates()
{
    $error = array();

    if ($_POST['hsa_effective_from'] != '' && $_POST['hsa_effective_to'] != '')
    {
        if (strtotime(UserDateToSQLDate($_POST['hsa_effective_to'])) < strtotime(UserDateToSQLDate($_POST['hsa_effective_from'])))
        {
            $error['hsa_effective_to']['message'] = GetFieldLabel('hsa_effective_to') . ' cannot be earlier than ' . GetFieldLabel('hsa_effective_from');
        }
    }

    return $error;
}

/**
* Checks that the criteria sectio is not blank on the HSA form.
*
* @param  string  $_POST['hsa_effective_from']  The SQL WHERE clause entered on the HSA form.
* @param  string  $_POST['hsa_effective_to']    The module entered on the HSA form.
*
* @return string  $error                        The error message.
*/
function validateCriteria()
{
    $error = array();

    for($i = 1; $i < $_POST['condition_max_suffix']; $i++)
    {
        if ($_POST['hsc_field_name_1'] != '' && $_POST['hsc_values_1'] != '')
        {
            return array();
        }
    }

    $error['hsc_field_name_1']['message'] = GetFieldLabel('hsc_field_name') . ' cannot be blank';
    $error['hsc_values_1']['message'] = GetFieldLabel('hsc_values') . ' cannot be blank';

    return $error;
}

/**
* Saves the Recipient Criteria information on the HSA form.
*
* @param string $data['recipients_max_suffix']              The number (+1) of recipient criteria on this form.
* @param string $data['recordid']                           The ID of this HSA record.
* @param string $data['hsr_sec_groups_x']                   The security groups for this criteria (where 'x' id the criteria ID).
* @param string $data['hsr_users_x']                        The individual users for this criteria (where 'x' id the criteria ID).
* @param string $data['recipient_conditions_x_max_suffix']  The number (+1) of conditions for this criteria (where 'x' id the criteria ID).
* @param string $data['hrc_field_name_x_y']                 The field name for this condition (where 'x' id the criteria ID and 'y' is the condition ID).
* @param string $data['hrc_values_x_y']                     The values for this condition (where 'x' id the criteria ID and 'y' is the condition ID).
*/
function saveRecipients($data)
{
    if (!is_numeric($data['recordid']))
    {
        return;
    }

    for ($i = 1; $i < $data['recipients_max_suffix']; $i++)
    {
        // only attempt to create a criteria record if there security groups/users defined
        if ($data['hsr_sec_groups_'.$i] == '' && $data['hsr_users_'.$i] == '')
        {
            continue;
        }

        // insert new criteria record
        $sql = 'INSERT INTO hotspots_recipients_criteria (hsa_id) VALUES (:hsa_id)';
        $id = DatixDBQuery::PDO_insert($sql, array('hsa_id' => $data['recordid']));

        // store new criteria ID so as not to delete later
        $criteriaIdsNoDelete[] = $id;

        // save security groups for this criteria
        if (is_array($data['hsr_sec_groups_'.$i]))
        {
            foreach ($data['hsr_sec_groups_'.$i] as $value)
            {
                $sql = 'INSERT INTO hotspots_recipients (hrc_id,hsr_type,hsr_value) VALUES (:hrc_id,:hsr_type,:hsr_value)';
                DatixDBQuery::PDO_insert($sql, array('hrc_id' => $id, 'hsr_type' => 'sec_group', 'hsr_value' => $value));
            }
        }

        // save users for this criteria
        if (is_array($data['hsr_users_'.$i]))
        {
            foreach ($data['hsr_users_'.$i] as $value)
            {
                $sql = 'INSERT INTO hotspots_recipients (hrc_id,hsr_type,hsr_value) VALUES (:hrc_id,:hsr_type,:hsr_value)';
                DatixDBQuery::PDO_insert($sql, array('hrc_id' => $id, 'hsr_type' => 'user', 'hsr_value' => $value));
            }
        }

        // save conditions for this criteria
        for ($j = 1; $j < $data['recipient_conditions_'.$i.'_max_suffix']; $j++)
        {
            if ($data['hrc_field_name_'.$i.'_'.$j] != '' && is_array($data['hrc_values_'.$i.'_'.$j]))
            {
                $hrc_values = implode(' ', $data['hrc_values_'.$i.'_'.$j]);
                $sql = 'INSERT INTO hotspots_recip_conditions (hrc_id,hrc_field_name,hrc_values) VALUES (:hrc_id,:hrc_field_name,:hrc_values)';
                DatixDBQuery::PDO_insert($sql, array('hrc_id' => $id, 'hrc_field_name' => $data['hrc_field_name_'.$i.'_'.$j], 'hrc_values' => $hrc_values));
            }
        }
    }

    // retrieve list of criteria values to delete
    $sql = 'SELECT recordid FROM hotspots_recipients_criteria WHERE hsa_id = :hsa_id';
    if (is_array($criteriaIdsNoDelete))
    {
        $criteriaIdsNoDelete = implode(',', $criteriaIdsNoDelete);
        $sql .=' AND recordid NOT IN (' . $criteriaIdsNoDelete . ')';
    }

    $criteriaIdsToDelete = PDO_fetch_all($sql, array('hsa_id' => $data['recordid']), PDO::FETCH_COLUMN);

    if (!empty($criteriaIdsToDelete))
    {
        // remove old criteria records
        PDO_query('DELETE FROM hotspots_recipients_criteria WHERE recordid IN (' . implode(',', $criteriaIdsToDelete) . ')');

        // remove old recipient records
        PDO_query('DELETE FROM hotspots_recipients WHERE hrc_id IN (' . implode(',', $criteriaIdsToDelete) . ')');

        // remove old condition records
        PDO_query('DELETE FROM hotspots_recip_conditions WHERE hrc_id IN (' . implode(',', $criteriaIdsToDelete) . ')');
    }
}

/**
* Retrieves the Recipient Criteria data for this agent record.
*
* @param  string $recordid  The ID of this agent record.
*
* @return array  $data      The recipient data for this agent.
*/
function getRecipients($recordid)
{
    $data = array();
    if (!is_numeric($recordid))
    {
        return $data;
    }

    // get the Recipient Criteria recordids for this agent
    $criteriaRecord = 1;
    $sql = 'SELECT recordid FROM hotspots_recipients_criteria WHERE hsa_id = :hsa_id ORDER BY recordid';
    $criteriaRows = DatixDBQuery::PDO_fetch_all($sql, array('hsa_id' => $recordid));
    foreach ($criteriaRows as $criteriaRow)
    {
        // get the recipients for this criteria
        $sql = 'SELECT hsr_type, hsr_value FROM hotspots_recipients WHERE hrc_id = :hrc_id ORDER BY recordid';
        $recipientsRows = DatixDBQuery::PDO_fetch_all($sql, array('hrc_id' => $criteriaRow['recordid']));
        foreach ($recipientsRows as $recipientsRow)
        {
            switch ($recipientsRow['hsr_type'])
            {
                case 'sec_group':
                    $data['hsr_sec_groups_'.$criteriaRecord][] = $recipientsRow['hsr_value'];
                    break;
                case 'user':
                    $data['hsr_users_'.$criteriaRecord][] = $recipientsRow['hsr_value'];
                    break;
            }
        }

        // get the conditions for this criteria
        $conditionRecord = 1;
        $sql = 'SELECT hrc_field_name, hrc_values FROM hotspots_recip_conditions WHERE hrc_id = :hrc_id ORDER BY recordid';
        $conditionsRows = DatixDBQuery::PDO_fetch_all($sql, array('hrc_id' => $criteriaRow['recordid']));
        foreach ($conditionsRows as $conditionsRow)
        {
            $data['hrc_field_name_'.$criteriaRecord.'_'.$conditionRecord] = $conditionsRow['hrc_field_name'];
            $data['hrc_values_'.$criteriaRecord.'_'.$conditionRecord] = explode(' ', $conditionsRow['hrc_values']);
            $conditionRecord++;
        }
        $data['recipient_conditions_' . $criteriaRecord . '_max_suffix'] = $conditionRecord > 1 ? $conditionRecord : 2;

        $criteriaRecord++;
    }
    $data['recipients_max_suffix'] = $criteriaRecord > 1 ? $criteriaRecord : 2;

    return $data;
}
