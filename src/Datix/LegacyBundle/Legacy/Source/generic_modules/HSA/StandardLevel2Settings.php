<?php
$GLOBALS['FormTitle'] = _tk('hsa_title');

$GLOBALS["MandatoryFields"] = array(
    "hsa_name" => "details",
    "hsa_module" => "details",
    "hsa_count_threshold" => "settings",
    "hsa_within" => "settings",
    "hsa_date_field" => "settings",
    "hsa_retrigger" => "settings",
);

$GLOBALS["UserExtraText"] = array(
  'location' => 'These fields identify the area of the organisation which manages this hotspot agent. If you wish these values to be used as part of the criteria, select the option below.',
);

$GLOBALS["HelpTexts"] = array(
  'hsa_use_loc_for_crit' => 'Selecting this option will include these values in the criteria for this Hotspot Agent.<br /><br />These values will operate in addition to other conditions set.',
);
?>