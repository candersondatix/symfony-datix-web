<?php

// Standards
$ModuleDefs['LIB'] = array(
    'MOD_ID' => MOD_LIBRARY,
    'CODE' => 'LIB',
    'NAME' => _tk("mod_library_title"),
    'REC_NAME' => _tk("LIBName"),
    'REC_NAME_PLURAL' => _tk("LIBNames"),
	'REC_NAME_TITLE' => _tk("LIBNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("LIBNamesTitle"),
    'FK' => 'lib_id',
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'LIB',
    'NO_GENERIC_FORM' => true,
    'NO_REP_APPROVED' => true,
    'USES_APPROVAL_STATUSES' => false,
    'OURREF' => 'lib_ourref',
    'TABLE' => 'library_main',
    'ACTION' => 'evidence',
    'LINK_ACTION' => 'evidence',
    'PERM_GLOBAL' => 'LIB_PERMS',
    'MAIN_URL' => 'action=evidence&module=LIB',
    'MAIN_URL_OVERRIDE' => true,
   // 'LIBPATH' => 'Source/standards',
    'SEARCH_URL' => 'action=search',
    'FIELD_NAMES' => array(
        "NAME" => "lib_name",
        "HANDLER" => "lib_handler",
        ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'LIB', 'HIDECODE' => true)
        ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserLIBForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserLIBSettings',

    'ICON' => 'icons/icon_LIB.gif',
    'MAIN_MENU_ICON' => 'Standards.gif',

    'ADD_NEW_RECORD_LEVELS' => array('LIB_FULL', 'LIB_INPUT'),
    'INPUT_ONLY_LEVELS' => array('LIB_INPUT'),
    'ADD_NEW_RECORD_PERM' => 'LIB_ADD_NEW',
    'NEW_RECORD_ACTION' => 'evidence',

    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Title' => _tk("lib_list_all"),
            'Link' => _tk("lib_list_all_link"),
            'Where' => '1=1'
            ),
        ),
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_CONTACTS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,


    'FIELD_ARRAY' => array(
        'lib_name', 'lib_ourref', 'lib_type', 'lib_subtype', 'lib_loctype', 'lib_locactual', 'lib_manager',
        'lib_handler', 'lib_dopened', 'lib_dclosed', 'lib_descr', 'lib_references', 'lib_web_link', 'lib_organisation',
        'lib_unit', 'lib_clingroup', 'lib_directorate', 'lib_specialty', 'lib_dreview'
     ),

    'LINKED_DOCUMENTS' => true,
    'DOCUMENT_SECTION_KEY' => 'extra_document',
    'LOCAL_HELP' => 'WebHelp/index.html#Library/c_dx_library_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Library/c_dx_library_guide.xml',
);
