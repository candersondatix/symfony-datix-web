<?php

$IDFieldObj = new FormField($FormAction);

if ($FormAction == "Search")
{
    $IDField = '<input type="text" name="recordid" size="5">';
}
else
{
    $IDField = $lib["recordid"];
}

$IDFieldObj->MakeCustomField($IDField);

if ($_GET['lib_id'] && !$lib["recordid"])
{
    $FormArray = array("Sections" => array("NoAccessSection"));
}
else
{
    $FormArray = array(
        "Parameters" => array(
            "Panels" => "True",
            "Condition" => false
        ),
        "details" => array(
            "Title" => "Evidence Details",
            "Rows" => array(
                'recordid',
                'lib_ourref',
                'lib_name',
                'lib_organisation',
                'lib_unit',
                'lib_clingroup',
                'lib_directorate',
                'lib_specialty',
                'lib_type',
                'lib_subtype',
                'lib_loctype',
                'lib_locactual',
                'lib_manager',
                'lib_handler',
                'lib_dopened',
                'lib_dclosed',
                'lib_dreview',
                'lib_descr',
                array('Name' => 'link_notes', 'Condition' => ($_GET['link_recordid'] || ($_GET['main_recordid'] && $_GET['main_module']))),
                'lib_references',
                'lib_web_link',
            )
        ),
        "documents" => array(
            'Title' => 'Documents',
            "NotModes" => array('New', 'Search', 'Link'),
            "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')),
            "NoFieldAdditions" => true,
            "Function" => "ListDocuments",
            "FormDesignSubtitle" => 'This section is actively displayed when viewing evidence records that have already been created.'
        ),
        "extra_document" => array(
            "Title" => "Documents",
            "NotModes" => array('Search', 'Edit'),
            "NoFieldAdditions" => true,
            "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && ($FormAction != 'Print' && $FormAction != 'ReadOnly'),
            "NoReadOnly" => true,
            "Special" => 'DynamicDocument',
            "FormDesignSubtitle" => 'This section is actively displayed during the creation of a new evidence record.',
            "Rows" => array()
        ),
        'progress_notes' => array(
            'Title' => 'Progress notes',
            'NotModes' => array('New','Search'),
            'Special' => 'ProgressNotes',
            "NoFieldAdditions" => true,
            'Rows' => array()
        ),
        "links" => array(
            "Title" => "Manage links",
            "NotModes" => array('New', 'Search', 'Print'),
            "NoFieldAdditions" => true,
            "right_hand_link" => array('text' => 'Show links', 'onclick' => 'showAllLinks()'),
            "Function" => "linksSection",
            "Condition" => $FormType == 'Design' || (ModIsLicensed('STN') && CanSeeModule('STN'))
        ),
         "cqc_links" => array(
            "Title" => "Manage CQC links",
            "NotModes" => array('New', 'Search', 'Print'),
            "NoFieldAdditions" => true,
            "Function" => array('Source\classes\LIB\ManageCQCLinks', 'display'),
            "ExtraParameters" => new DatixDBQuery(''),
            "Condition" => $FormType == 'Design' || (ModIsLicensed('CQO') && (CanSeeModule('CQO') || CanSeeModule('CQP') || CanSeeModule('CQS')))
         )
    );
}

?>