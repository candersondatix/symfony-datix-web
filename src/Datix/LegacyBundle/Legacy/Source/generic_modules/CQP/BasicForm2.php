<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "outcome" => array(
        "Title" => "Prompt Details",
        "Rows" => array(
            array('Name' => "ctp_ref", 'ReadOnly' => true),
            array('Name' => "ctp_desc_short", 'ReadOnly' => true),
            array('Name' => "ctp_desc", 'ReadOnly' => true),
            array('Name' => "ctp_element", 'ReadOnly' => true),
            array('Name' => "ctp_guidance", 'ReadOnly' => true),
            array('Name' => "cdo_location", 'ReadOnly' => $FormType != 'Search'),
            "cdp_manager",
            "cdp_handlers",
            array('Name' => "cdp_status_input", 'Condition' => !in_array(GetParm('CQC_ROLLUP_TYPE', ''), array('AVE', 'WORST'))),
            array('Name' => "cdp_status", 'ReadOnly' => true, 'Condition' => in_array(GetParm('CQC_ROLLUP_TYPE', ''), array('AVE', 'WORST'))),
            "cdp_comments",
            'cdp_dlastreview',
            'cdp_dnextreview',
            'cdp_applicable',
        )
    ),
    "subprompts" => array(
        "Title" => "Sub-prompts",
        "NotModes" => array('New', 'Search'),
        "Special" => "LinkedCQCSubprompts",
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "evidence" => array(
        "Title" => "Evidence" . ($libCount?" ($libCount)":""),
        "Function" => array('CQC_Outcome', 'listEvidence'),
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $FormType != 'Search' && $act['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('ACT', $act['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NewPanel' => true,
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        'NewPanel' => true,
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "linked_records" => array(
        "Title" => _tk('linked_records'),
        "NoFieldAdditions" => true,
        "Special" => "LinkedRecords",
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
);
