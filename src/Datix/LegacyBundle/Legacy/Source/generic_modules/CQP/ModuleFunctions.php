<?php
function GetBreadCrumbs($data)
{
    global $ModuleDefs, $scripturl ;

    $trail = new BreadCrumbs();

    $trail->add(getFieldDataFromID($data['cdo_location'], 'loc_name', 'locations_main'), "$scripturl?action=record&module=LOC&fromsearch=1&recordid=" . $data['cdo_location'], 0);  
    $trail->add($data['cto_desc_short'], "$scripturl?action=record&module=CQO&fromsearch=1&recordid=" . $data['cqc_outcome_id'], 1);
    $trail->add($data['ctp_desc_short'], "$scripturl?action=record&module=CQP&fromsearch=1&recordid=" . $data['recordid'], 2);  
    
    echo $trail->output();

}
