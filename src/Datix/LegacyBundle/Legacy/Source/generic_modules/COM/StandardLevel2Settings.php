<?php

$GLOBALS['FormTitle'] = _tk('com2_title');

$GLOBALS["ExpandSections"] = array (
  'com_subjects_linked' =>
  array (
    0 =>
    array (
      'section' => 'subject',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'com_issues_linked' =>
  array (
    0 =>
    array (
      'section' => 'issue',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'rep_approved' =>
  array (
    0 =>
    array (
      'section' => 'rejection',
      'alerttext' => 'Please complete the \'Details of rejection\' section before saving this form.',
      'values' =>
      array (
        0 => 'REJECT',
      ),
    ),
  ),
);

$GLOBALS["DefaultValues"] = array (
  'com_dopened' => 'TODAY',
  'com_dreceived' => 'TODAY',
);

$GLOBALS["HideFields"] = array (
    'additional' => true,
    'ko41' => true,
    'independent_review' => true,
    'hcc' => true,
    'primary_compl_dates' => true,
    'primary_compl_dates_history' => true, 
    'progress_notes' => true, 
    'rejection_history' => true,
    'dum_com_grading' => true,
    'com_consent' => true,
	'lcom_last_dreopened' => true,
	'payments' => true,
    'com_dreopened' => true,
    'action_chains' => true,
    'com_inc_type' => true,
    'ccs2' => true,
    'com_last_updated' => true
);
$GLOBALS["NewPanels"] = array (
    'location' => true,
    'ko41' => true,
    'subject_header' => true,
    'contacts_type_C' => true,
    'feedback' => true,
    'investigation' => true,
    'notepad' => true,
    'independent_review' => true,
    'hcc' => true,
    'primary_compl_dates' => true,
    'progress_notes' => true,
    'linked_actions' => true,
    'documents' => true,
    'word' => true,
    'linked_records' => true,
    'payments' => true,
    'action_chains' => true,
);

$GLOBALS['UserExtraText'] = [
    'dum_fbk_to' => 'Only staff and contacts with e-mail addresses are shown.',
    'dum_fbk_gab' => 'Only users with e-mail addresses are shown.',
    'dum_fbk_email' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'
];

?>
