<?php

$GLOBALS['FormTitle'] = _tk('com1_title');

$GLOBALS["ExpandSections"] = array (
  'show_person' => 
  array (
    0 => 
    array (
      'section' => 'contacts_type_A',
      'alerttext' => '',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
  'com_subjects_linked' => 
  array (
    0 => 
    array (
      'section' => 'subject',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
  'com_issues_linked' => 
  array (
    0 => 
    array (
      'section' => 'issue',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_document' => 
  array (
    0 => 
    array (
      'section' => 'documents',
      'values' => 
      array (
        0 => 'Y',
      ),
    ),
  )
);

$GLOBALS["HideFields"] = array ( 
    'rep_approved' => true,
    'com_dopened' => true,
    'com_dclosed' => true,
    'com_dreopened' => true, 
    'com_purchaser' => true,
    'com_otherref' => true,
    'ko41' => true,
    'dum_com_grading' => true,
	'com_consent' => true,
    'ccs2' => true
);

$GLOBALS["DefaultValues"] = array (
  'com_dopened' => 'TODAY',
  'com_dreceived' => 'TODAY',
  'rep_approved' => 'UN',
);

?>