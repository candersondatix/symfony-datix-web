<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '2'),
    "com_mgr" => array(
        'width' => '10'),
    "com_name" => array(
        'width' => '10'),
    "com_dreceived" => array(
	    'width' => '6'),
    'com_detail' => array (
	    'width' => '30'),
);

$list_columns_mandatory = array("recordid");

$list_columns_extra = array(
    "com_ourref" => array(
        'condition' => ($ListType == "approved"),
        'showextra' => 'condition: $ListType == approved')
);
?>
