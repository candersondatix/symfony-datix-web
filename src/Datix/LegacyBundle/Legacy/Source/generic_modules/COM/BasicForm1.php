<?php

$FormArray = array(
    "Parameters" => array(
        "Condition" => false,
        "ExtraContacts" => 1
    ),
    "contacts_type_C" => array(
        "Title" => "Details of ". _tk('com_complainant'),
        "LinkRole" => "COMP",
        "module" => 'COM',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        'LinkedForms' => array('C' => array('module' => 'CON')),
        "contacttype" => 'C',
        "suffix" => 1,
        "Rows" => array()
    ),
    'person_header' => array(
        'Title' => _tk('person_affected_plural'),
        'Rows' => array('show_person')
    ),
    "contacts_type_A" => array(
        "Title" => "Details of person affected by the ". _tk('COMName'),
        "LinkRole" => "REL",
        "module" => 'COM',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'A',
        "suffix" => 2,
        'LinkedForms' => array('A' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "details" => array(
        "Title" => "Details of ". _tk('COMNameTitle'),
        "Rows" => array(
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => $module,
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
                )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => $module,
                'perms' => $Perms,
                'approveobj' => $ApproveObj
                )),
            "com_method",
            "com_type",
            "com_subtype",
            "com_dreceived",
            "com_consent",
            "com_dincident",
            "com_dopened",
            "com_dclosed",
            "com_dreopened",
            "com_detail",
            'com_purchaser',
            'com_otherref',
            "com_curstage",
            "com_outcome",
            "com_summary",
            "dum_com_grading"
        )
    ),
    "location" => array(
        "Title" =>"Location admitted",
        "Rows" => array(
            'com_organisation',
            'com_unit',
            'com_clingroup',
            'com_directorate',
            'com_specialty',
            'com_loctype',
            'com_locactual'
        )
    ),
    "ko41" => array(
        "Title" => 'KO41',
        "Rows" => array(
            "com_ko41_type",
            "com_koservarea",
            "com_kosubject",
            "com_koprof",
            "com_koethnic_pat",
            "com_koethnic_staff"
        )
    ),
    "subject_header" => array(
        "Title" => "Subjects of ". _tk('COMNameTitle'),
        'NewPanel' => true,
        "Rows" => array(
            'com_subjects_linked'
        )
    ),
    "subject" => array(
        "Title" => "Subjects",
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoSubjectSection' => array(
                'controller' => 'src\\generic\\controllers\\SubjectsController'
            )
        ),
        "ExtraParameters" => array('subject_name' => 'com_subject'),
        "Rows" => array()
    ),
    "subject_design" => array(
        "Title" => "Subject",
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "NoSectionActions" => true,
        "AltSectionKey" => "subject",
        "Rows" => array(
            'csu_subject',
            'csu_subsubject',
            'csu_stafftype',
            'csu_organisation',
            'csu_unit',
            'csu_clingroup',
            'csu_directorate',
            'csu_specialty',
            'csu_location',
            'csu_loctype',
            'csu_outcome',
            'csu_notes',
            'csu_dcompleted',
        )
    ),
    "isd" => array(
        "Title" => 'ISD',
        "Rows" => array(
            "com_isd_unit",
            "com_isd_locactual",
            "com_isd_consent",
            "com_isd_dconsent_req",
            "com_isd_dconsent_rec",
            "com_isd_div_sent",
            "com_isd_ref_added",
            "com_isd_iaas_involved",
            "com_isd_cas_involved",
            "com_isd_chi_no",
            "com_isd_resp_sent_20",
            "com_isd_resp_20_reason",
            "com_isd_agree_40",
            "com_isd_agree_40_date",
            "com_isd_resp_40_reason",
            "com_isd_actions",
            "com_isd_plan",
            "com_isd_dexport",
        )
    ),
    "issue_header" => array(
        "Title" => "Issues of ". _tk('COMNameTitle'),
        'NewPanel' => true,
        "Rows" => array(
            'com_issues_linked'
        )
    ),
    "issue" => array(
        "Title" => "Issues",
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        "Include" => "Source/generic_modules/COM/ModuleFunctions.php",
        "Function" => "DoIssueSection",
        "ExtraParameters" => array('issue_name' => 'com_issue'),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "issue_design" => array(
        "Title" => "Issues",
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "AltSectionKey" => "issue",
        "NoSectionActions" => true,
        "Rows" => array(
            'cisd_type',
            'cisd_category',
            'cisd_subcategory',
            'cisd_pat_adm_type',
            'cisd_service_area',
            'cisd_specialty',
            'cisd_staff_group',
            'cisd_staff_position',
        )
    ),
    'additonal_info' => array(
        "Title" => "Additional Information",
        "Rows" => array(
            'show_document'
        )
    ),
    "documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && ($FormType != 'Print' && $FormType != 'ReadOnly'),
        "NoFieldAdditions" => true,
        "Special" => 'DynamicDocument',
        "Rows" => array()
    ),
    "linked_documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && $FormType == 'Print',
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController')
        ),
        "Rows" => array()
    ),
    "your_manager" => array(
        "Title" => "Your Manager",
        "module" => 'COM',
        "Condition" => bYN(GetParm("COM_EMAIL_MGR",'N')),
        "Rows" => array(
            array(
                "Type" => "formfield",
                "Name" => "com_mgr",
                "Title" => 'Your Manager',
                "FormField" => MakeManagerDropdownGeneric('COM', $com, $FormType, array('COM2', 'RM'))
            )
        )
    ),
    "contacts_type_R" => array(
        "Title" => "Details of person reporting the ". _tk('COMName'),
        "LinkRole" => GetParm('REPORTER_ROLE', 'REP'),
        "module" => 'COM',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        'LinkedForms' => array('R' => array('module' => 'CON')),
        "contacttype" => 'R',
        "suffix" => 3,
        "Rows" => array()
    ),
    'ccs2' => array(
        'Title' => 'Datix CCS2',
        'Condition' =>  (bYN(GetParm('CCS2_COM', 'N'))),
        'Rows' => array(
            'com_affecting_tier_zero',
            'com_type_tier_one',
            'com_type_tier_two',
            'com_type_tier_three'
        )
    ),
    "udf" => array(
        "ExtraFields" => $UDFGroups,
        "NoFieldAdditions" => true
    )
);

?>