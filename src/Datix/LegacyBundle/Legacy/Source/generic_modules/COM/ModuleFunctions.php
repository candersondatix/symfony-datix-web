<?php
function DoIssueSection($data, $FormType, $module, $ExtraParams = array())
{
    //need to work out how many subjects to display, and then display them.

    if (isset($data['error']))
    {
        $aIssues = getLinkedIssuesFromPost($data, $module);
    }
    else
    {
        $aIssues = getLinkedIssues(array('recordid' => $data['recordid'], 'module'=>$module));
    }

    foreach($aIssues as $id => $LinkedIssue)
    {
        echo getIssueSectionHTML(array('module'=>$module,'data'=>$LinkedIssue, 'formtype'=>$FormType, 'suffix' => (intval($id) + 1), 'clearsection' => ($id ==0), 'issue_name' => $ExtraParams['issue_name']));
    }

    $NumIssues = count($aIssues);

    if(count($aIssues) == 0) //no linked issues
    {
        echo getIssueSectionHTML(array('module'=>$module,'data'=>array(), 'formtype'=>$FormType, 'suffix' => 1, 'clearsection'=>true, 'issue_name' => $ExtraParams['issue_name']));
        $NumIssues = 1;
    }

    echo '<script language="javascript">dif1_section_suffix[\''.$ExtraParams['issue_name'].'\'] = '.($NumIssues+1).'</script>';

    if($FormType != 'Print' && $FormType != 'ReadOnly')
    {
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        echo '
            <li class="new_windowbg" id="add_another_'.$ExtraParams['issue_name'].'_button_list">
                <input type="button" id="section_'.$ExtraParams['issue_name'].'_button" value="'._tk('btn_add_another').'" onclick="AddSectionToForm(\''.$ExtraParams['issue_name'].'\',\'\',$(\''.$ExtraParams['issue_name'].'_section_div_\'+(dif1_section_suffix[\''.$ExtraParams['issue_name'].'\']-1)),\''.Sanitize::getModule($module).'\',\'\', true, '.$spellChecker.', '.(isset($_REQUEST['form_id']) ? Sanitize::SanitizeInt($_REQUEST['form_id']) : 'null').');">
            </li>';
    }
}

function getLinkedIssues($aParams)
{
    $module = $aParams['module'];
    $recordid = $aParams['recordid'];
    $issues = array();

    if($module == 'COM' && $recordid)
    {
        $Rows = getIssueFieldsForSave(array('module'=>$module));
        $sql = 'SELECT recordid as issue_id, '.implode(', ',$Rows['Rows']).' FROM COMPL_ISD_ISSUES WHERE com_id = :recordid ORDER BY listorder';
        $issues = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $recordid));
    }

    return $issues;
}

/**
 * Creates an array of issue records from POST data.
 *
 * Used to populate the Issues section when returning to the form after a validation error.
 *
 * @param  array  $data     The POST data.
 * @param  string $module   The current module.
 *
 * @return array  $issues
 */
function getLinkedIssuesFromPost($data, $module)
{
    $issues = array();
    $fields = getIssueFieldsForSave(array('module' => $module));

    for ($i = 1; $i < $data[\UnicodeString::strtolower($module).'_issue_max_suffix']; $i++)
    {
        $issue = array();
        foreach ($fields['Rows'] as $field)
        {
            $issue[$field] = $field == 'listorder' ? $data[\UnicodeString::strtolower($module).'_issue_listorder'.'_'.$i] : $data[$field.'_'.$i];
        }
        $issues[] = $issue;
    }
    return $issues;
}

function GetIssueSectionHTML($aParams)
{
    global $JSFunctions, $formlevel;

    if($formlevel == null)
    {
        $formlevel = ($_POST['form_level'] ?: 1);
    }

    $SavedFormDesignSettings = saveFormDesignSettings();
    unsetFormDesignSettings();

    $AJAX = $aParams['ajax'];

    if($AJAX)
    {
        $formlevel = $aParams['level'];
    }

    $aIssueRows = getBasicIssueForm($aParams);

    $issuesFormDesign = Forms_FormDesign::GetFormDesign(array('module' => $aParams['module'], 'level' => $formlevel));
    $issuesFormDesign = ModifyFormDesignForIssues($issuesFormDesign);

    if ($aParams['suffix'])
    {
        $issuesFormDesign->AddSuffixToFormDesign($aParams['suffix']);
        $aParams['data'] = AddSuffixToData($aParams['data'], $aParams['suffix'], getIssueFieldsForSave(array('module' => $aParams['module'])));
    }

    $oIssueTable = new FormTable($aParams['formtype'], $aParams['module'], $issuesFormDesign);
    $oIssueTable->MakeForm($aIssueRows, $aParams['data'], $aParams['module'], array('dynamic_section' => true));

    $html = '';

    if(!($AJAX) && $aParams['formtype'] != 'Print')
    {
        $html .= '<div id="'.$aParams['issue_name'].'_section_div_'.$aParams['suffix'].'">';
    }

    $html .= $oIssueTable->GetFormTable();
    $html .= '<input type="hidden" name="' . $aParams['issue_name'] . '_link_id_' . $aParams['suffix'] .'" id="' . $aParams['issue_name'] . '_link_id_' . $aParams['suffix'] .'" value="' . $aParams['data']['issue_id'] .'" />';


    if($aParams['formtype'] != 'Print' && $aParams['formtype'] != 'ReadOnly')
    {
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        $html .= '
            <div id="copy_issue_button_div">
                <ol>
                    <li class="nepassword
                    pw_windowbg">
                        <input type="button" id="copy_issue_section" value="Copy Issue" onclick="CopySectionToForm(\''.$aParams['issue_name'].'\',\'\',$(\''.$aParams['issue_name'].'_section_div_'.$aParams['suffix'].'\'),\''.$aParams['module'].'\','.$aParams['suffix'].', true, '.$spellChecker.', '.(is_numeric($_REQUEST['form_id']) ? Sanitize::SanitizeInt($_REQUEST['form_id']) : 'null').');">
                    </li>
                </ol>
            </div>';
    }

    if(!($AJAX) && $aParams['formtype'] != 'Print')
    {
        $html .= '</div>';
    }

    loadFormDesignSettings($SavedFormDesignSettings);

    return $html;
}

function getBasicIssueForm($aParams)
{
    global $ModuleDefs;

    $aParams['issue_name'] = $ModuleDefs[$aParams['module']]['ISSUE_TYPE'];
    $RowList = GetIssueRowList($aParams);

    if($aParams['rows'])
    {
        return array('Rows' => $RowList);
    }
    else
    {
        return array(
            "Parameters" => array("Suffix" => $aParams['suffix']),
            "issue".($aParams['suffix'] ? '_'.$aParams['suffix'] : '') => array(
                "Title" => 'Issue',
                "OrderField" => array('id'=>$aParams['issue_name'].'_listorder_'.$aParams['suffix'], 'value'=>$aParams['data']['listorder']),
                "ClearSectionOption" => $aParams['clearsection'],
                "DeleteSectionOption" => !$aParams['clearsection'],
                "ContactSuffix" => $aParams['suffix'],
                "DynamicSectionType" => $aParams['issue_name'],
                "Rows" => $RowList,
                'ContainedIn' => 'issue',
            ));
    }
}

function GetIssueRowList($aParams)
{
    global $ModuleDefs;

    $RowList = $ModuleDefs[$aParams['module']]['LINKED_RECORDS'][$ModuleDefs[$aParams['module']]['ISSUE_TYPE']]['basic_form']['Rows'];

    return $RowList;
}

function getIssueFieldsForSave($aParams)
{
    $RowList = GetIssueRowList($aParams);
    $RowList[] = 'listorder';

    return array('Rows' => $RowList);
}

function ModifyFormDesignForIssues(Forms_FormDesign $design)
{
    if (is_array($design->ExpandSections))
    {
        $NewArray = array();
        foreach ($design->ExpandSections as $FieldName => $SectionArray)
        {
            foreach ($SectionArray as $SectionID => $Details)
            {
                if ($Details['section'] != 'issue')
                {
                    $NewArray[$FieldName][] = $Details;
                }
            }
        }

        $design->ExpandSections = $NewArray;
    }

    // remove extra sections/extra fields
    unset($design->ExtraSections);
    unset($design->ExtraFields);

    return $design;
}

function GetYNCOMIssues($recordid)
{
    $sql =
        'SELECT count(*) as NUM_ISSUES_LINKED
        FROM COMPL_MAIN
        LEFT JOIN
        COMPL_ISD_ISSUES
        ON COMPL_MAIN.RECORDID = COMPL_ISD_ISSUES.COM_ID
        WHERE COMPL_MAIN.RECORDID = :recordid
        AND COMPL_ISD_ISSUES.RECORDID IS NOT NULL
        GROUP BY COMPL_MAIN.RECORDID';

    $numIssues = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid), PDO::FETCH_COLUMN);

    return array('com_issues_linked' => ($numIssues > 0 ? 'Y' : 'N'));
}

/**
 * @desc Calculates and updates the Independent Review date fields
 *
 * @param  string $com      All $_POST data for the complaint.
 *
 */
function SetIndependentReviewDates($com)
{
    if (CheckCalculateIRDates(array('data' => $_POST)))
    {
        $CalculatedDates = CalculateIRDates(array('data' => $com, 'com_drequest' => ($_POST['com_drequest'] ?: GetTodaysDate()), 'complaint_recordid' => $_POST['main_recordid']));
    }

    $DateFields = GetComplaintIRDateFields();

    $com_id = $com["recordid"];

    $UpdateSetLink .= GeneratePDOSQLFromArrays(array(
            'FieldArray' => $DateFields,
            'DataArray' => $CalculatedDates,
            'Module' => 'COM',
        ),
        $PDOParamsArray);

    if ($UpdateSetLink)
    {
        $sql = "UPDATE compl_main
            SET $UpdateSetLink
            WHERE recordid = $com_id";

        DatixDBQuery::PDO_query($sql, $PDOParamsArray);
    }
}

/**
 * @desc Clean (first) subject fields on claim record when no subject is linked
 *
 * @param mixed array $aParams $_POST datafrom form submitted.
 */
function CleanupAfterEmptySubject ($aParams)
{
    global $ModuleDefs;

    $sql = "
        SELECT *
        FROM {$ModuleDefs['COM']['LINKED_RECORDS']['com_subject']['real_table']}
        WHERE {$ModuleDefs['COM']['LINKED_RECORDS']['com_subject']['main_recordid_label']} = {$aParams['recordid']}
        ";

    $result = DatixDBQuery::PDO_fetch($sql);

    if (empty($result))
    {
        $sql = "
            UPDATE {$ModuleDefs['COM']['TABLE']}
            SET
                {$ModuleDefs['COM']['PRIMARY_SUBJECT_MAPPINGS']['csu_subject']} = '',
                {$ModuleDefs['COM']['PRIMARY_SUBJECT_MAPPINGS']['csu_subsubject']} = ''
            WHERE recordid = {$aParams['recordid']}
        ";

        DatixDBQuery::PDO_fetch($sql);
    }
}

/**
 * @desc Check whether IR dates need to be updated
 *
 * @param mixed array $aParams $_POST datafrom form submitted.
 */
function CheckCalculateIRDates($aParams)
{
    if (!$aParams['data']['CHANGED-com_drequest'])
    {
        return false;
    }

    $ComplDoneArray = array('com_dackreq');

    foreach ($ComplDoneArray as $field)
    {
        if ($aParams['data'][$field] !== '' && $aParams['data'][$field] !== null)
        {
            return false;
        }
    }

    return true;
}

/**
 * @desc Calculates the dates for each IR due date including working days only
 *
 * @param mixed array $aParams $_POST datafrom form submitted.
 *
 * @return date array $dates Array of date objects for each IR due date
 */
function CalculateIRDates($aParams)
{
    $ComplVars = array(
        'com_ddueackreq' => array('default_parm' => 'IR_ACKDAYS', 'default_val' => 2),
        'com_dduelaychair' => array('default_parm' => 'IR_LAYCHAIRDAYS', 'default_val' => 4),
        'com_dduedecision' => array('default_parm' => 'IR_DECISIONDAYS', 'default_val' => 20),
        'com_dduepappt' => array('default_parm' => 'IR_PANELDAYS', 'default_val' => 20),
        'com_dduepdraft' => array('default_parm' => 'IR_DRAFTDAYS', 'default_val' => 50),
        'com_ddueppublish' => array('default_parm' => 'IR_FINALDAYS', 'default_val' => 10),
        'com_dduecereply' => array('default_parm' => 'IR_REPLYDAYS', 'default_val' => 20)
    );

    foreach ($ComplVars as $key => $Details)
    {
        $days[$key] = GetParm($Details['default_parm'], $Details['default_val']);
    }

    $DateTracker = new \DateTime(UserDateToSQLDate($aParams['com_drequest']));

    $dates = array();
    foreach ($days as $type => $numdays)
    {
        $numdays = CalculateWorkingDays($numdays, $DateTracker);
        $DateTracker->modify('+' . $numdays . ' day');
        $key = $type.($aParams['suffix'] ? '_'.$aParams['suffix'] : '');
        $dates[$key] = $DateTracker->format('Y-m-d');
    }

    return $dates;
}

/**
 * @desc Returns a list of date fields used for the IR section
 *
 * @return string array Array of date field names
 */
function GetComplaintIRDateFields()
{
    return array(
        "com_ddueackreq",
        "com_dackreq",
        "com_dstatement",
        "com_dduelaychair",
        "com_dlaychair",
        "com_dduedecision",
        "com_ddecision",
        "com_dinform",
        "com_dduepappt",
        "com_dpappt",
        "com_dduepdraft",
        "com_dpdraft",
        "com_ddueppublish",
        "com_dppublish",
        "com_dduecereply",
        "com_dcereply",
    );
}

/**
 * @desc Check whether IR dates need to be updated
 *
 * @param mixed array $com $_POST values for comaplaints form submitted
 * @return string $error Any errors message that occured
 */
function SaveHCC($com)
{
    // $com = ParseSaveData(array('module' => 'COM', 'data' => $com));
    $com_id = $com['recordid'];

    $HCCFields = getHHCFields();

    foreach ($HCCFields as $i => $HCCFieldName)
    {
        if ($_POST['CHANGED-' . $HCCFieldName] != '')
        {
            $FieldsToUpdate[] = $HCCFieldName;
        }
    }

    if (is_array($FieldsToUpdate))
    {

        $sql = "SELECT count(*) as check_hcc FROM rev_main WHERE link_id = $com_id and link_module ='COM'";

        $row = DatixDBQuery::PDO_fetch($sql);

        if (!$row)
        {
            $error = "An error has occurred when trying to save the notes attached to this record. Please report the following to the Datix administrator: $sql";
        }
        else
        {
            foreach ($FieldsToUpdate as $i => $HCC_field)
            {
                $PDOParamsArray[$HCC_field] = $com[$HCC_field];
            }

            if ($row["check_hcc"] > 0)
            {

                $UpdateSetLink = GeneratePDOSQLFromArrays(array(
                        'FieldArray' => $FieldsToUpdate,
                        'DataArray' => $com,
                        'Module' => 'COM',
                        'Extras' => array('updatedby' => $_SESSION["initials"], 'updateddate' => date('d-M-Y H:i:s'))
                    ),
                    $PDOParamsArray
                );

                $sql = "UPDATE rev_main
                    SET $UpdateSetLink
                    WHERE link_id = $com_id and link_module = 'COM'";

                DoFullAudit ('COM', 'rev_main', $com_id, $PDOParamsArray, "link_id = $com_id and link_module = 'COM'");
                DatixDBQuery::PDO_query($sql, $PDOParamsArray);

            }
            else
            {
                $rev_id = GetNextRecordID("rev_main", false);
                $sql = "INSERT INTO rev_main ";

                $sql .= GeneratePDOInsertSQLFromArrays(
                    array(
                        'Module' => "COM",
                        'FieldArray' => $FieldsToUpdate,
                        'DataArray' => $com,
                        'Extras' => array('recordid' => $rev_id, 'link_id' => $com_id, 'link_module' => 'COM')
                    ),
                    $PDOParamsArray
                );

                if (!DatixDBQuery::PDO_query($sql, $PDOParamsArray))
                {
                    fatal_error("Cannot insert HCC data");
                }
            }
        }
    }

    return $error;
}

/**
 * @desc Returns a list of fields used for the HCC section
 *
 * @return string array Array of HCC field names
 */
function getHHCFields()
{
    return array('rev_drequested',
        'rev_reason',
        'rev_dclosed',
        'rev_ddecisiondue',
        'rev_ddecisiondone' ,
        'rev_init_handler',
        'rev_init_outcome',
        'rev_investigate_yn',
        'rev_init_comments',
        'rev_dhctermsdue',
        'rev_dhctermsdone',
        'rev_dtermsdue',
        'rev_dtermsdone',
        'rev_dcompleteddue',
        'rev_dcompleteddone',
        'rev_inv_handler',
        'rev_inv_outcome',
        'rev_panelreview_yn',
        'rev_inv_recommend',
        'rev_inv_action',
        'rev_dpanelrequested',
        'rev_dpanelcompleted',
        'rev_panel_outcome',
        'rev_ombudsman_yn',
        'rev_panel_recommend',
        'rev_panel_action',
        'rev_dombrequested',
        'rev_omb_assessment',
        'rev_omb_upheld',
        //'rev_omb_reason',
        'rev_omb_handler',
        'rev_omb_recommend',
        'rev_omb_action');
}

/**
 * @desc Returns an array of data for each field in the HCC section
 *
 * @return mixed array $returnData Array of HCC field data
 */
function getHCC($recordid)
{
    $HCCFields = getHHCFields();
    $HCCFieldList = implode(",", $HCCFields);

    $sql = "SELECT $HCCFieldList FROM rev_main WHERE link_id = $recordid and link_module ='COM'";

    $returnData = DatixDBQuery::PDO_fetch($sql);

    if (is_array($returnData))
    {
        return $returnData;
    }
    else
    {
        return array();
    }
}

/**
 * @desc Saves any data for primary complainant dates shown on main complaints form.
 *
 * @param mixed array $com $_POST values for comaplaints form submitted
 */
function SaveComplaintDatesOnComplaint($com)
{
    global $ModuleDefs;

    $aDateValues = CheckDatesFromArray(GetComplaintDateFields(), Sanitize::SanitizeRawArray($_POST));
    $com = array_merge($com, $aDateValues);
    $com_id = $com['recordid'];

    if ($com['recordid'])
    {
        $sql = "SELECT TOP 1 recordid as link_compl_id, con_id from link_compl WHERE com_id = $com_id AND lcom_current = 'Y' AND lcom_primary = 'Y'";

        $row = DatixDBQuery::PDO_fetch($sql);

        $link_compl_id = $row["link_compl_id"];

        if ($link_compl_id > 0)
        {
            if (CheckCalculateComplaintDates(array('data' => $_POST)))
            {
                $CalculatedDates = CalculateComplaintDates(array('data' => $aParams['data'], 'date_received' => $_POST['lcom_dreceived'], 'complaint_recordid' => $com_id));
                $com = array_merge($com, $CalculatedDates);
            }

            $DateFields = GetComplaintDateFields();
            $DateFields[] = 'lcom_primary'; //non-date fields also stored here.
            $DateFields[] = 'lcom_iscomplpat';

            $UpdateData = $com;

            if($_POST['lcom_dreopened'.$aParams['suffixstring']]) //if this has been reopened, the dates being saved are no longer current.
            {
                $DateFields[] = 'lcom_current';
                $UpdateData['lcom_current'] = 'N';
                $UpdateData['lcom_primary'] = 'N';
            }

            $UpdateSetLink .= GeneratePDOSQLFromArrays(array(
                    'FieldArray' => $DateFields,
                    'DataArray' => $UpdateData,
                    'Module' => 'COM',
                ),
                $PDOParamsArray);

            if ($UpdateSetLink)
            {
                $sql = "UPDATE link_compl
                    SET $UpdateSetLink
                    WHERE recordid = $link_compl_id";

                DatixDBQuery::PDO_query($sql, $PDOParamsArray);
            }

            if($_POST['lcom_dreopened'.$aParams['suffixstring']]) //if this has been reopened, we need to re-calculate the dates and save a new link_compl record.
            {
                $com['lcom_primary'] = 'Y'; // must be the case if we're here, so we ensure this valur is set in case the field is hidden on the form
                ReOpenComplaint(array('recordid' => $row['con_id'], 'main_recordid' => $com['recordid'], 'data' => $com, 'module' => 'COM'));
            }
        }
    }
}

/**
 * @desc Retrieves any data for primary complainant dates shown on main complaints form.
 *
 * @param int $com_id ID for complaint to retrieve primary comaplainant details for.
 */
function GetPrimaryComplLinkData($com_id)
{
    $sql = "SELECT lcom_primary, lcom_ddueack, lcom_ddueact, lcom_ddueresp, lcom_dduehold, lcom_dduerepl,
            lcom_iscomplpat, lcom_dreceived, lcom_dack, lcom_dactioned, lcom_dresponse, lcom_dholding, lcom_dreplied
                FROM LINK_COMPL WHERE com_id = $com_id AND lcom_current = 'Y' AND lcom_primary = 'Y'";

    $row = DatixDBQuery::PDO_fetch($sql);

    return $row;
}

/**
 * @desc Calculates and updates the HCC date fields
 *
 * @param  string $com      All $_POST data for the complaint.
 *
 */
function SetHCCDates($com)
{
    if (CheckCalculateHCCDates(array('data' => $_POST)))
    {
        $CalculatedDates = CalculateHCCDates(array('data' => $com, 'rev_drequested' => ($_POST['rev_drequested'] ?: GetTodaysDate()), 'complaint_recordid' => $aParams['main_recordid']));
    }

    $DateFields = array('rev_ddecisiondue', 'rev_dhctermsdue', 'rev_dtermsdue', 'rev_dcompleteddue');

    $com_id = $com["recordid"];

    $UpdateSetLink .= GeneratePDOSQLFromArrays(array(
            'FieldArray' => $DateFields,
            'DataArray' => $CalculatedDates,
            'Module' => 'COM',
        ),
        $PDOParamsArray);

    if ($UpdateSetLink)
    {
        $sql = "UPDATE rev_main
                SET $UpdateSetLink
                WHERE link_id = $com_id and link_module = 'COM'";

        DatixDBQuery::PDO_query($sql, $PDOParamsArray);
    }

}

/**
 * @desc Check whether HCC dates need to be updated
 *
 * @param mixed array $aParams $_POST datafrom form submitted.
 */
function CheckCalculateHCCDates($aParams)
{
    $ComplDoneArray = array('rev_drequested', 'rev_ddecisiondone', 'rev_dhctermsdone', 'rev_dtermsdone');
    foreach ($ComplDoneArray as $field)
    {
        if ($aParams['data']['CHANGED-' . $field] && ($aParams['data'][$field] !== '' && $aParams['data'][$field] !== null))
        {
            return true;
        }
    }

    return false;
}

/**
 * @desc Calculates the dates for each HCC due date including working days only
 *
 * @param mixed array $aParams $_POST datafrom form submitted.
 *
 * @return date array $dates Array of date objects for each HCC due date
 */
function CalculateHCCDates($aParams)
{
    $ComplVars = array(
        'rev_ddecisiondue' => array('default_parm' => 'REV_DECISION_TIME', 'default_val' => 20, 'from_date_field' => 'rev_drequested'),
        'rev_dhctermsdue' => array('default_parm' => 'REV_TERMS_TIME', 'default_val' => 10, 'from_date_field' => 'rev_ddecisiondone'),
        'rev_dtermsdue' => array('default_parm' => 'REV_COMMENTS_TIME', 'default_val' => 10, 'from_date_field' => 'rev_dhctermsdone'),
        'rev_dcompleteddue' => array('default_parm' => 'REV_INV_TIME', 'default_val' => 180, 'from_date_field' => 'rev_dtermsdone')
    );

    foreach ($ComplVars as $key => $Details)
    {
        $days[$key] = GetParm($Details['default_parm'], $Details['default_val']);
    }

    $dates = array();

    foreach ($days as $type => $numdays)
    {
        $from_date_field = $ComplVars[$type]['from_date_field'];
        if (isset($aParams['data'][$from_date_field]))
        {
            $DateTracker = new \DateTime($aParams['data'][$from_date_field]);
            $numdays = CalculateWorkingDays($numdays, $DateTracker);
            $DateTracker->modify('+' . $numdays . ' day');
            $key = $type.($aParams['suffix'] ? '_'.$aParams['suffix'] : '');
            $dates[$key] = $DateTracker->format('Y-m-d');
        }
    }

    return $dates;
}

function SaveComplaintDates($aParams)
{
    global $ModuleDefs;

    $aDateValues = CheckDatesFromArray(AddSuffixToFields(GetComplaintDateFields(), $aParams['suffixstring']), $_POST);
    $aParams['data'] = array_merge($aParams['data'], $aDateValues);

    if ($aParams['recordid'] && $aParams['main_recordid'])
    {
        $sql = 'SELECT recordid, lcom_primary from link_compl WHERE con_id = :recordid AND '.$ModuleDefs[$aParams['module']]['FK'].' = :main_recordid AND lcom_current = :lcom_current';

        $row = DatixDBQuery::PDO_fetch($sql, array('recordid' => $aParams['recordid'], 'main_recordid' => $aParams['main_recordid'], 'lcom_current' => 'Y'));

        $link_compl_id = $row["recordid"];

        if (!$row)
        {
            if (!$link_compl_id)
            {
                $link_compl_id = GetNextRecordID("link_compl", false);
            }

            DatixDBQuery::PDO_build_and_insert('link_compl',
                array(
                    'recordid' => $link_compl_id,
                    'con_id' => $aParams['recordid'],
                    'com_id' => $aParams['main_recordid'],
                    'lcom_current' => 'Y')
            );

            $NewComplainant = true;
        }

        if (CheckCalculateComplaintDates(array('data' => $_POST)) || $NewComplainant)
        {
            $CalculatedDates = CalculateComplaintDates(array('data' => $aParams['data'],
                'date_received' => ($_POST['lcom_dreceived'] ? $_POST['lcom_dreceived'] : $aParams['data']['lcom_dreceived_'.$aParams['suffixstring']]),
                'complaint_recordid' => $aParams['main_recordid'], 'suffix' => $aParams['suffix']));
            $aParams['data'] = array_merge($aParams['data'], $CalculatedDates);
        }

        $DateFields = GetComplaintDateFields();
        $DateFields[] = 'lcom_primary'; //non-date fields also stored here.
        $DateFields[] = 'lcom_iscomplpat';

        $UpdateData = $aParams['data'];

        if($_POST['lcom_dreopened'.$aParams['suffixstring']]) //if this has been reopened, the dates being saved are no longer current.
        {
            $DateFields[] = 'lcom_current';
            $UpdateData['lcom_current'] = 'N';
            $UpdateData['lcom_primary'] = 'N';
        }

        $UpdateSetLink .= GeneratePDOSQLFromArrays(array(
                'FieldArray' => $DateFields,
                'DataArray' => $UpdateData,
                'Module' => 'COM',
                'Suffix' => $aParams['suffix']
            ),
            $PDOParamsArray);

        if ($UpdateSetLink)
        {
            $sql = "UPDATE link_compl
                SET $UpdateSetLink
                WHERE recordid = $link_compl_id";

            DatixDBQuery::PDO_query($sql, $PDOParamsArray);

            // need to determine if this is the primary complainant in cases where this field is hidden on the form design
            $aParams['data']['lcom_primary'] = $_POST['lcom_primary'] === null ? $row['lcom_primary'] : $_POST['lcom_primary'];

            if ($aParams['data']['lcom_primary'] == 'Y')
            {
                EnsureUniquePrimary($aParams);
            }
        }

        if($_POST['lcom_dreopened'.$aParams['suffixstring']]) //if this has been reopened, we need to re-calculate the dates and save a new link_compl record.
        {
            ReOpenComplaint($aParams);
        }
    }
}

function ReOpenComplaint($aParams)
{
    if ($aParams['recordid'] && $aParams['main_recordid'])
    {
        $DateFields = GetComplaintDateFields();

        foreach ($DateFields as $DateField)
        {
            if ($DateField != 'lcom_dreopened')
            {
                $aParams['data'][$DateField.$aParams['suffixstring']] = null;
            }
        }

        $link_compl_id = GetNextRecordID("link_compl", false);

        \DatixDBQuery::PDO_build_and_insert('link_compl',
            array(
                'recordid'     => $link_compl_id,
                'con_id'       => $aParams['recordid'],
                'com_id'       => $aParams['main_recordid'],
                'lcom_current' => 'Y',
                'lcom_primary' => $aParams['data']['lcom_primary']
            ));

        $aParams['data']['lcom_dreceived'.$aParams['suffixstring']] = UserDateToSQLDate($_POST['lcom_dreopened'.$aParams['suffixstring']]);

        $CalculatedDates = CalculateComplaintDates(array('data' => $aParams['data'], 'date_received' => $aParams['data']['lcom_dreceived'.$aParams['suffixstring']], 'complaint_recordid' => $aParams['main_recordid'], 'suffix' => $aParams['suffix']));
        $aParams['data'] = array_merge($aParams['data'], $CalculatedDates);

        $DateFields[] = 'lcom_primary'; //non-date fields also stored here.
        $DateFields[] = 'lcom_iscomplpat';

        //New record should not have the re-opened date filled in.
        $aParams['data']['lcom_dreopened'] = null;

        $UpdateSetLink = GeneratePDOSQLFromArrays(array(
                'FieldArray' => $DateFields,
                'DataArray' => $aParams['data'],
                'Module' => 'COM',
                'Suffix' => $aParams['suffix']
            ),
            $PDOParamsArray);

        if ($UpdateSetLink)
        {
            $sql = "UPDATE link_compl
                SET $UpdateSetLink
                WHERE recordid = $link_compl_id";

            DatixDBQuery::PDO_query($sql, $PDOParamsArray);

            if ($_POST['lcom_primary'] == 'Y')
            {
                EnsureUniquePrimary($aParams);
            }
        }

    }
}

function CalculateComplaintDates($aParams)
{
    if (bYN(GetParm("SHOW_ACKNOWLEDGED",'Y')))
    {
        $ComplVars['lcom_ddueack'] = array('default_parm' => 'COMPL_ACKDAYS', 'default_val' => 2, 'type_prefix' => 'DACK_');
    }
    if (bYN(GetParm("SHOW_ACTIONED",'Y')))
    {
        $ComplVars['lcom_ddueact'] = array('default_parm' => 'COMPL_ACTDAYS', 'default_val' => 1, 'type_prefix' => 'DACT_');
    }
    if (bYN(GetParm("SHOW_RESPONSE",'Y')))
    {
        $ComplVars['lcom_ddueresp'] = array('default_parm' => 'COMPL_RESPDAYS', 'default_val' => 10, 'type_prefix' => 'DRSP_');
    }
    if (bYN(GetParm("SHOW_HOLDING",'Y')))
    {
        $ComplVars['lcom_dduehold'] = array('default_parm' => 'COMPL_HOLDDAYS', 'default_val' => 2, 'type_prefix' => 'DHLD_');
    }
    if (bYN(GetParm("SHOW_REPLIED",'Y')))
    {
        $ComplVars['lcom_dduerepl'] = array('default_parm' => 'COMPL_REPLDAYS', 'default_val' => 5, 'type_prefix' => 'DRPL_');
    }

    if(!empty($ComplVars))
    {
        $sql = 'SELECT com_type, com_subtype FROM compl_main WHERE recordid = :recordid';
        $row = DatixDBQuery::PDO_fetch($sql, array('recordid' => $aParams['complaint_recordid']));

        $TypeDaysSet = false;

        if ($row['com_type'] && $row['com_subtype'])
        {
            foreach ($ComplVars as $key => $Details)
            {
                $days[$key] = GetParm($Details['type_prefix'].$row['com_type'].'_'.$row['com_subtype'], "-1");

                if ($days[$key] != -1)
                {
                    $TypeDaysSet = true;
                }
            }
        }

        if ($row['com_subtype'] && !$TypeDaysSet)
        {
            foreach ($ComplVars as $key => $Details)
            {
                $days[$key] = GetParm($Details['type_prefix'].'*_'.$row['com_subtype'], "-1");

                if ($days[$key] != -1)
                {
                    $TypeDaysSet = true;
                }
            }
        }

        if ($row['com_type'] && !$TypeDaysSet)
        {
            foreach ($ComplVars as $key => $Details)
            {
                $days[$key] = GetParm($Details['type_prefix'].$row['com_type'].'_*', "-1");

                if ($days[$key] != -1)
                {
                    $TypeDaysSet = true;
                }
            }
        }

        if (!$TypeDaysSet)
        {
            foreach ($ComplVars as $key => $Details)
            {
                $days[$key] = GetParm($Details['default_parm'], $Details['default_val']);
            }
        }
        if($aParams['date_received'] != null || $aParams['data'][($aParams['suffix'] ? 'lcom_dreceived'.'_'.$aParams['suffix'] : 'lcom_dreceived')] != null)
        {
            if ($aParams['date_received'] == null && $aParams['data'][($aParams['suffix'] ? 'lcom_dreceived'.'_'.$aParams['suffix'] : 'lcom_dreceived')] != null)
            {
                $aParams['date_received'] = $aParams['data'][($aParams['suffix'] ? 'lcom_dreceived'.'_'.$aParams['suffix'] : 'lcom_dreceived')];
            }

            $DateTracker = new \DateTime(UserDateToSQLDate($aParams['date_received']));

            $dates = array();

            foreach ($days as $type => $numdays)
            {
                if (GetParm('COM_COMPLAINT_CHAIN_TIMESCALES', 'W') == 'W')
                {
                    $numdays = CalculateWorkingDays($numdays, $DateTracker);
                }

                $DateTracker->modify('+' . $numdays . ' day');
                $key = $type.($aParams['suffix'] ? '_'.$aParams['suffix'] : '');
                $dates[$key] = $DateTracker->format('Y-m-d 00:00:00.000');
            }
        }
        else
        {
            foreach ($days as $type => $numdays)
            {
                $key = $type.($aParams['suffix'] ? '_'.$aParams['suffix'] : '');
                $dates[$key] = null;
            }
        }
    }

    return $dates;
}

function ShowComplaintDatesHistory($com_id, $con_id = '')
{
    if (!$con_id) //use primary complainant
    {
        $con_id = DatixDBQuery::PDO_fetch('SELECT con_id FROM link_compl WHERE lcom_primary = :lcom_primary AND com_id = :com_id', array('lcom_primary' => 'Y', 'com_id' => $com_id), PDO::FETCH_COLUMN);
    }

    $NumRecords = DatixDBQuery::PDO_fetch('SELECT COUNT(*) FROM link_compl WHERE con_id = :con_id AND com_id = :com_id AND lcom_current = :lcom_current', array('con_id' => $con_id, 'com_id' => $com_id, 'lcom_current' => 'N'), PDO::FETCH_COLUMN);

    return ($NumRecords > 0);
}

function CheckCalculateComplaintDates($aParams)
{
    if (!$aParams['data']['CHANGED-lcom_dreceived'])
    {
        return false;
    }

    $ComplDoneArray = array('lcom_dack','lcom_dactioned','lcom_dresponse','lcom_dholding','lcom_dreplied');

    foreach ($ComplDoneArray as $field)
    {
        if ($aParams['data'][$field] !== '' && $aParams['data'][$field] !== null)
        {
            return false;
        }
    }

    return true;
}

/**
 * Retrieves the data for the lcom_last_dreopened field.
 *
 * @param int $com_id The complaint recordid
 *
 * @return string The most recent reopened date for the primary complainaint
 */
function getLastReopenedDate($com_id)
{
    // get primary complainant id
    $con_id = DatixDBQuery::PDO_fetch('SELECT con_id FROM link_compl WHERE lcom_primary = :lcom_primary AND com_id = :com_id', array('lcom_primary' => 'Y', 'com_id' => $com_id), PDO::FETCH_COLUMN);

    // return most recent reopened date for this contact
    return DatixDBQuery::PDO_fetch('SELECT lcom_dreopened FROM link_compl WHERE con_id = :con_id AND com_id = :com_id AND lcom_current = :lcom_current ORDER BY lcom_dreopened DESC', array('con_id' => $con_id, 'com_id' => $com_id, 'lcom_current' => 'N'), PDO::FETCH_COLUMN);
}

/**
 * Used to simplify the SQL statements defined for the complaints statuses by providing a shortcut for a piece of SQL determining
 * that a particular status is part of the chain and "expected" (i.e has a due date)
 *
 * @param $Status The status to calculate the sql for
 * @return string The SQL segment representing this logic
 */
function StatusValid($Status)
{
    global $COMStatuses;
    return 'NOT('.(bYN(GetParm($COMStatuses[$Status]['global'],'Y')) ? '1=2': '1=1').' OR '.$COMStatuses[$Status]['due'].' IS NULL)';
}

/**
 * Used to simplify the SQL statements defined for the complaints statuses by providing a shortcut for a piece of SQL determining
 * that a particular status is not part of the chain (due to global settings) or is not "expected" (i.e has no due date)
 *
 * @param $Status The status to calculate the sql for
 * @return string The SQL segment representing this logic
 */
function StatusNotValid($Status)
{
    global $COMStatuses;
    return '('.(bYN(GetParm($COMStatuses[$Status]['global'],'Y')) ? '1=2': '1=1').' OR '.$COMStatuses[$Status]['due'].' IS NULL)';
}
