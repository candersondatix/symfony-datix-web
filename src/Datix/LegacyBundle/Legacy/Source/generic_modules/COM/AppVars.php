<?php

require_once 'Source/generic_modules/COM/ModuleFunctions.php';

$GLOBALS['COMStatuses'] = array(
    'ACK' => array(
        'global' => 'SHOW_ACKNOWLEDGED',
        'due' => 'lcom_ddueack',
        'complete' => 'lcom_dack',
    ),
    'ACT' => array(
        'global' => 'SHOW_ACTIONED',
        'due' => 'lcom_ddueact',
        'complete' => 'lcom_dactioned',
    ),
    'RSP' => array(
        'global' => 'SHOW_RESPONSE',
        'due' => 'lcom_ddueresp',
        'complete' => 'lcom_dresponse',
    ),
    'HLD' => array(
        'global' => 'SHOW_HOLDING',
        'due' => 'lcom_dduehold',
        'complete' => 'lcom_dholding',
    ),
    'REP' => array(
        'global' => 'SHOW_REPLIED',
        'due' => 'lcom_dduerepl',
        'complete' => 'lcom_dreplied',
    )
);

//Get the WHERE Clause for the last stage available.
if (bYN(GetParm("SHOW_REPLIED",'Y')))
{
    $CompletedWHERECheck = ' AND (lcom_dreplied IS NOT NULL OR lcom_dduerepl IS NULL)';
}
elseif (bYN(GetParm("SHOW_HOLDING",'Y')))
{
    $CompletedWHERECheck = ' AND (lcom_dholding IS NOT NULL OR lcom_dduehold IS NULL)';
}
elseif (bYN(GetParm("SHOW_RESPONSE",'Y')))
{
    $CompletedWHERECheck = ' AND (lcom_dresponse IS NOT NULL OR lcom_ddueresp IS NULL)';
}
elseif (bYN(GetParm("SHOW_ACTIONED",'Y')))
{
    $CompletedWHERECheck = ' AND (lcom_dactioned IS NOT NULL OR lcom_ddueact IS NULL)';
}
elseif (bYN(GetParm("SHOW_ACKNOWLEDGED",'Y')))
{
    $CompletedWHERECheck = ' AND (lcom_dack IS NOT NULL OR lcom_ddueack IS NULL)';
}

$ModuleDefs['COM'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'COM',
    'LEVEL1_PERMS' => array('COM1'),
    'AUDIT_TRAIL_PERMS' => array('COM2'),  //which permissions can see the audit trail?
    'MOD_ID' => MOD_COMPLAINTS,
    'CODE' => 'COM',
    //'APPROVAL_LEVELS' => true,
    'NAME' => _tk("mod_complaints_title"),
    'USES_APPROVAL_STATUSES' => true,
    'TABLE' => 'compl_main',
    'AUDIT_TABLE' => 'aud_compl_main',
    'REC_NAME' => _tk("COMName"),
    'REC_NAME_PLURAL' => _tk("COMNames"),
	'REC_NAME_TITLE' => _tk("COMNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("COMNamesTitle"),
    'FK' => 'com_id',
    'OURREF' => 'com_ourref',
    'PERM_GLOBAL' => 'COM_PERMS',
    'NO_LEVEL1_GLOBAL' => 'COM_NO_OPEN',
    'ACTION' => 'record&module=COM',
    'FIELD_NAMES' => array(
        'NAME' => 'com_name',
        'HANDLER' => 'com_mgr',
        'MANAGER' => 'com_head',
        'INVESTIGATORS' => 'com_investigator',
        'REF' => 'com_ourref',
    ),
    'LOCATION_FIELD_PREFIX' => 'com_',
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'COM1'),
        2 => array('LEVEL' => 2, 'CODE'=>'COM2')
    ),
    'EXTRA_RECORD_DATA_FUNCTIONS' => array(
        'GetYNCOMSubjects',  'GetYNCOMIssues', 'GetHCC', 'GetPrimaryComplLinkData'
    ),
    'EXTRA_RECORD_DATA_FUNCTION_INCLUDES' => array(
        'Source/generic/Subjects.php','Source/generic_modules/COM/ModuleFunctions.php'
    ),
    'EXTRA_RECORD_DATA_SAVE_FUNCTIONS' => array(
        array('Condition' => '$aParams[\'data\'][\'real_link_type\'.$aParams[\'suffixstring\']] == \'C\'', 'Function' => 'SaveComplaintDates')
    ),
    'EXTRA_CONTACT_LINK_DATA_FUNCTIONS' => array(
        'GetComplLinkData'
    ),
    'EXTRA_CONTACT_UNLINK_FUNCTIONS' => array(
        'UnlinkComplLinkData'
    ),
    'EXTRA_SAVE_INCLUDES' => array(
        'Source/generic_modules/COM/ModuleFunctions.php'
    ),
    'EXTRA_SAVE_FUNCTIONS' => array('SetIndependentReviewDates', 'SaveHCC', 'SetHCCDates', 'SaveComplaintDatesOnComplaint', 'CleanupAfterEmptySubject'
    ),
    'GET_LINKED_CONTACTS_FUNCTION' => 'GetLinkedContactsForComplaints',
    'PRE_LINK_CONTACT_SAVE_FUNCTIONS' => array(
        'PreSaveLinkContactForComplaints',
        'DeleteDuplicateComplaintContacts',
    ),
    'LOGGED_OUT_LEVEL1' => true,
    'FORMCODE1' => 'COM1',
    'FORMCODE2' => 'COM2',
    'MAIN_MENU_ICON' => 'complaints24n.gif',
    'ICON' => 'icons/icon_COM.png',
    'AGE_AT_DATE' => 'com_dreceived',
    'LINKED_DOCUMENTS' => true,
    'DOCUMENT_SECTION_KEY' => 'documents',
    'LINKED_CONTACT_LISTING_COLS' => array('recordid', 'com_name', 'com_ourref', 'com_detail', 'com_type'),
    'SEARCH_URL' => 'action=search',
    'DEFAULT_ORDER' => 'com_dreceived',
    'OVERDUE_CHECK_FIELD' => 'com_dreceived',
    'OVERDUE_STATUSES' => array('AWAREV', 'STCL', 'INREV', 'AWAFA', 'INFA'),
    'CAN_CREATE_EMAIL_TEMPLATES' => true,
    'LINKED_RECORDS' => array(
        'com_subject' => array(
            'section' => 'subject',
            'type' => 'com_subject',
            'table' => 'VW_COMPL_SUBJ_WEB',
            'real_table' => 'COMPL_SUBJECTS',
            'recordid_field' => 'recordid',
            'save_listorder' => true,
            'save_controller' => 'com_subjects_linked',
            'basic_form' => array(
                'Rows' => array(
                    'csu_subject', 'csu_subsubject', 'csu_stafftype', 'csu_organisation', 'csu_unit',
                    'csu_clingroup', 'csu_directorate', 'csu_specialty', 'csu_location', 'csu_loctype',
                    'csu_outcome', 'csu_notes', 'csu_dcompleted'
                )
            ),
            'main_recordid_label' => 'COM_ID',
            'RunAfterSave' => array('SavePrimarySubject'),
            'RunAfterSaveIncludes' => array('Source/generic/Subjects.php')
        ),
        'com_issue' => array(
            'section' => 'issue',
            'type' => 'com_issue',
            'table' => 'COMPL_ISD_ISSUES',
            'recordid_field' => 'recordid',
            'save_listorder' => true,
            'save_controller' => 'com_issues_linked',
            'basic_form' => array(
                'Rows' => array(
                    'cisd_type', 'cisd_category', 'cisd_subcategory', 'cisd_pat_adm_type',
                    'cisd_service_area', 'cisd_specialty', 'cisd_staff_group', 'cisd_staff_position',
                )
            ),
            'main_recordid_label'=>'COM_ID'
        )
    ),
    'SUBJECT_TYPE' => 'com_subject',
    'ISSUE_TYPE' => 'com_issue',
    'PRIMARY_SUBJECT_MAPPINGS' => array(
        'csu_subject' => 'com_subject1',
        'csu_subsubject' => 'com_subsubject1',
        'csu_stafftype' => 'com_stafftype1',
        'csu_organisation' => 'com_organisation1',
        'csu_unit' => 'com_unit1',
        'csu_clingroup' => 'com_clingroup1',
        'csu_directorate' => 'com_directorate1',
        'csu_specialty' => 'com_specialty1',
        'csu_location' => 'com_location1',
        'csu_loctype' => 'com_loctype1',
        'csu_outcome' => 'com_outcome1',
        'csu_notes' => 'com_notes1',
        'csu_dcompleted' => 'com_dcompleted1'
    ),
    'IDENTIFY_TABLE' => true, //needed because some complaints field names are duplicated on subforms
    'FIELD_MAPPINGS' => array(
        'com_subject' => 'csu_subject',
        'com_subsubject' => 'csu_subsubject',
        'com_stafftype' => 'csu_stafftype',
        'com_organisation' => 'csu_organisation',
        'com_unit' => 'csu_unit',
        'com_clingroup' => 'csu_clingroup',
        'com_directorate' => 'csu_directorate',
        'com_specialty' => 'csu_specialty',
        'com_location' => 'csu_location',
        'com_loctype' => 'csu_loctype',
        'com_outcome' => 'csu_outcome',
        'csu_notes' => 'csu_notes',
        'csu_dcompleted' => 'csu_dcompleted'
    ),
    'CONTACTTYPES' => array(
        "C" => array( "Type"=> "C", "Name"=>_tk('com_complainant'), "Plural"=>_tk('com_complainant_plural'), "None"=>_tk('no_com_complainant_plural'), "CreateNew"=>_tk('com_complainant_link'), 'NoDuplicatesWith' => array('A')),
        "A" => array( "Type"=> "A", "Name"=>_tk('com_person_affected'), "Plural"=>_tk('com_person_affected_plural'), "None"=>_tk('no_com_person_affected_plural'), "CreateNew"=>_tk('com_person_affected_link'), 'NoDuplicatesWith' => array('C')),
        "E" => array( "Type"=> "E", "Name"=>_tk('com_employee'), "Plural"=>_tk('com_employee_plural'), "None"=>_tk('no_com_employee_plural'), "CreateNew"=>_tk('com_employee_link')),
        "N" => array( "Type"=> "N", "Name"=>_tk('com_other_contact'), "Plural"=>_tk('com_other_contact_plural'), "None"=>_tk('no_com_other_contact_plural'), "CreateNew"=>_tk('com_other_contact_link'))
    ),
    'LEVEL1_CON_OPTIONS' => array(
        "C" => array("Title" => _tk('com_complainant'), "DivName" => 'contacts_type_C', "Max"=> 1),
        "A" => array("Title" => _tk('person_affected'), "DivName" => 'contacts_type_A'),
        "N" => array("Title" => "Contact", "DivName" => 'contacts_type_N'),
        "R" => array("Title" => "Reporter", "Role" => GetParm('REPORTER_ROLE', 'REP'), "ActualType" => "N", "DivName" => 'contacts_type_R', 'Max' => 1)
    ),
    'RECORD_NAME_FROM_CONTACT' => 'A',
    'STAFF_EMPL_FILTER_MAPPINGS' => array(
        'com_organisation' => 'con_orgcode',
        'com_unit' => 'con_unit',
        'com_clingroup' => 'con_clingroup',
        'com_directorate' => 'con_directorate',
        'com_specialty' => 'con_specialty',
        'com_loctype' => 'con_loctype',
        'com_locactual' => 'con_locactual'
    ),
    'FIELD_ARRAY' => array(
        "com_name", "com_ourref", "com_mgr", "com_head", "com_dreceived", "com_loctype", "com_locactual",
        "com_organisation", "com_unit", "com_clingroup", "com_directorate", "com_specialty",
        "com_detail", "com_type", "com_method", "com_summary", "com_outcome", "com_subtype",
        "com_dincident", 'com_consent', 'com_dopened', 'com_dreopened', "com_curstage", 'com_otherref', 'com_purchaser',
        "com_dclosed", "com_investigator", "com_inv_dstart",
        "com_inv_dcomp", "com_grade", "com_consequence", "com_likelihood", "com_inv_outcome", "com_lessons_code",
        "com_inv_lessons", "com_action_code", "com_inv_action",
        "rep_approved", 'show_person',
        "com_ko41_type", "com_koservarea", "com_kosubject", "com_koprof", "com_koethnic_pat", "com_koethnic_staff",
        "com_isd_unit", "com_isd_locactual", "com_isd_consent", "com_isd_dconsent_req", "com_isd_dconsent_rec", "com_isd_div_sent",
        "com_isd_ref_added", "com_isd_iaas_involved", "com_isd_cas_involved", "com_isd_chi_no", "com_isd_resp_sent_20",
        "com_isd_resp_20_reason", "com_isd_agree_40", "com_isd_agree_40_date", "com_isd_resp_40_reason", "com_isd_actions", "com_isd_plan", "com_isd_dexport",
        "com_drequest", "com_ddueackreq", "com_dackreq", "com_dstatement", "com_dduelaychair", "com_dlaychair", "com_dduedecision", "com_ddecision", "com_assessor",
        "com_recir", "com_irsynopsis", "com_dinform", "com_dduepappt", "com_dpappt", "com_dduepdraft", "com_dpdraft", "com_ddueppublish", "com_dppublish", "com_dduecereply", "com_dcereply", "com_ircode",
        'com_inc_type', 'com_affecting_tier_zero', 'com_type_tier_one', 'com_type_tier_two', 'com_type_tier_three',
        'com_last_updated'
    ),
    'SECURITY' => array(
        'LOC_FIELDS' => array(
            'com_organisation' => array(),
            'com_unit' => array(),
            'com_clingroup' => array(),
            'com_directorate' => array(),
            'com_specialty' => array(),
            'com_loctype' => array('multi' => true),
            'com_locactual' => array('multi' => true)
        ),
        'EMAIL_SELECT_GLOBAL' => 'COM_EMAIL_SELECT',
        'EMAIL_NOTIFICATION_GLOBAL' => 'COM_STA_EMAIL_LOCS'
    ),
    'SHOW_EMAIL_GLOBAL' => 'COM_SHOW_EMAIL',
    'EMAIL_REPORTER_GLOBAL' => 'COM_EMAIL_REPORTER',
    'EMAIL_HANDLER_GLOBAL' => 'COM_EMAIL_MGR',
    'EMAIL_USER_PARAMETER' => 'COM_STA_EMAIL_LOCS',
    'FORM_DESIGN_ARRAY_FILE' => 'UserCOMForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserCOM1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserCOM2Settings',
    'HARD_CODED_LISTINGS' => array(
        'unapproved' => array(
            'Title' => _tk("com_unapproved_listing_title"),
            'Link' => _tk("com_unapproved_listing_title"),
            'Colour' => '0',
            'Where' => 'compl_main.rep_approved = \'UN\' OR compl_main.rep_approved IS NULL OR compl_main.rep_approved = \'\'',
            'NoOverdue' => true
        ),
        'awatingacknowledgement' => array(
            'Title' => _tk("com_awatingacknowledgement_listing_title"),
            'Link' => _tk("com_awatingacknowledgement_listing_title"),
            'Where' =>
                'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL OR compl_main.com_dclosed = \'\') AND
                    (compl_main.recordid in (SELECT com_id from LINK_COMPL WHERE (LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                    AND lcom_dreplied IS NULL ' .
                (bYN(GetParm("SHOW_ACKNOWLEDGED",'Y')) ? ' AND lcom_dack IS NULL AND lcom_ddueack IS NOT NULL' : '') .
                (bYN(GetParm("SHOW_ACTIONED",'Y')) ? ' AND lcom_dactioned IS NULL' : '') .
                (bYN(GetParm("SHOW_RESPONSE",'Y')) ? ' AND lcom_dresponse IS NULL' : '') .
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .
                ' AND LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid FROM LINK_COMPL WHERE
                      LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                      ORDER by lcom_primary desc, LINK_COMPL.recordid))
            OR (LCOM_DRECEIVED IS NULL '.
                (bYN(GetParm("SHOW_ACKNOWLEDGED",'Y')) ? ' AND lcom_dack IS NULL' : '') .
                (bYN(GetParm("SHOW_ACTIONED",'Y')) ? ' AND lcom_dactioned IS NULL' : '') .
                (bYN(GetParm("SHOW_RESPONSE",'Y')) ? ' AND lcom_dresponse IS NULL' : '') .
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '').')
                )
                OR
                compl_main.recordid not in (SELECT LINK_COMPL.com_id from LINK_COMPL
                                            WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\')) ',
            'OverdueWhere' =>'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL OR compl_main.com_dclosed = \'\') AND
                        (compl_main.recordid in (SELECT com_id from LINK_COMPL WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL ' .
                (bYN(GetParm("SHOW_ACKNOWLEDGED",'Y')) ? ' AND lcom_dack IS NULL AND lcom_ddueack < \'@TODAY\'' : '') .
                (bYN(GetParm("SHOW_ACTIONED",'Y')) ? ' AND lcom_dactioned IS NULL' : '') .
                (bYN(GetParm("SHOW_RESPONSE",'Y')) ? ' AND lcom_dresponse IS NULL' : '') .
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid FROM LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid)
                ))',
            'OverdueDateField' => 'lcom_ddueack'
        ),
        'awatinginivestigation' => array(
            'Title' => _tk("com_awatinginivestigation_listing_title"),
            'Link' => _tk("com_awatinginivestigation_listing_title"),
            'Where' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                        AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL ' .
                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .

                //AND current status is not complete AND current status is expected
                (bYN(GetParm("SHOW_ACTIONED",'Y')) ? ' AND lcom_dactioned IS NULL AND lcom_ddueact IS NOT NULL' : '') .
                //AND no later status completed
                (bYN(GetParm("SHOW_RESPONSE",'Y')) ? ' AND lcom_dresponse IS NULL' : '') .
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueWhere' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                                AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL ' .
                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .
                (bYN(GetParm("SHOW_ACTIONED",'Y')) ? ' AND lcom_dactioned IS NULL AND lcom_ddueact  < \'@TODAY\'' : '') .
                (bYN(GetParm("SHOW_RESPONSE",'Y')) ? ' AND lcom_dresponse IS NULL' : '') .
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .

                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueDateField' => 'lcom_ddueact'
        ),
        'underinvestigation' => array(
            'Title' => _tk("com_underinvestigation_listing_title"),
            'Link' => _tk("com_underinvestigation_listing_title"),
            'Where' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                        AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL' .

                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('ACT') . ' AND ' . StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('ACT') . ' AND lcom_dactioned IS NOT NULL' .
                ' OR ' .
                StatusNotValid('ACT') . ' AND ' . StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .

                //AND current status is not complete AND current status is expected
                (bYN(GetParm("SHOW_RESPONSE",'Y')) ? ' AND lcom_dresponse IS NULL AND lcom_ddueresp IS NOT NULL' : '') .
                //AND no later status completed
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueWhere' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                        AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL ' .
                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('ACT') . ' AND ' . StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('ACT') . ' AND lcom_dactioned IS NOT NULL' .
                ' OR ' .
                StatusNotValid('ACT') . ' AND ' . StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .
                (bYN(GetParm("SHOW_RESPONSE",'Y')) ? ' AND lcom_dresponse IS NULL AND lcom_ddueresp < \'@TODAY\' ' : '') .
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueDateField' => 'lcom_ddueresp'
        ),
        'awaitingholding' => array(
            'Title' => _tk("com_awaitingholding_listing_title"),
            'Link' => _tk("com_awaitingholding_listing_title"),
            'Where' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                        AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL' .

                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('RSP') . ' AND lcom_dresponse IS NOT NULL' .
                ' OR ' .
                StatusNotValid('RSP') . ' AND ' . StatusValid('ACT') . ' AND lcom_dactioned IS NOT NULL' .
                ' OR ' .
                StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .

                //AND current status is not complete AND current status is expected
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL AND lcom_dduehold IS NOT NULL' : '') .
                //AND no later status completed
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueWhere' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                        AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL '.
                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('RSP') . ' AND lcom_dresponse IS NOT NULL' .
                ' OR ' .
                StatusNotValid('RSP') . ' AND ' . StatusValid('ACT') . ' AND lcom_dactioned IS NOT NULL' .
                ' OR ' .
                StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .
                (bYN(GetParm("SHOW_HOLDING",'Y')) ? ' AND lcom_dholding IS NULL AND lcom_dduehold < \'@TODAY\'' : '') .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueDateField' => 'lcom_dduehold'
        ),
        'awaitingreply' => array(
            'Title' => _tk("com_awaitingreply_listing_title"),
            'Link' => _tk("com_awaitingreply_listing_title"),
            'Where' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                        AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL ' .
                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('HLD') . ' AND ' . StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('HLD') . ' AND lcom_dholding IS NOT NULL' .
                ' OR ' .
                StatusNotValid('HLD') . ' AND ' . StatusValid('RSP') . ' AND lcom_dresponse IS NOT NULL' .
                ' OR ' .
                StatusNotValid('HLD') . ' AND ' . StatusNotValid('RSP') . ' AND ' . StatusValid('ACT') . ' AND lcom_dactioned IS NOT NULL' .
                ' OR ' .
                StatusNotValid('HLD') . ' AND ' . StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL AND lcom_dduerepl IS NOT NULL' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueWhere' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NULL)
                        AND compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID
                                                    AND lcom_dreplied IS NULL ' .
                ' AND (' .
                //preceding status doesn't exist or is not expected
                StatusNotValid('HLD') . ' AND ' . StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusNotValid('ACK') .

                //OR preceding status exists and is completed
                ' OR ' .
                StatusValid('HLD') . ' AND lcom_dholding IS NOT NULL' .
                ' OR ' .
                StatusNotValid('HLD') . ' AND ' . StatusValid('RSP') . ' AND lcom_dresponse IS NOT NULL' .
                ' OR ' .
                StatusNotValid('HLD') . ' AND ' . StatusNotValid('RSP') . ' AND ' . StatusValid('ACT') . ' AND lcom_dactioned IS NOT NULL' .
                ' OR ' .
                StatusNotValid('HLD') . ' AND ' . StatusNotValid('RSP') . ' AND ' . StatusNotValid('ACT') . ' AND ' . StatusValid('ACK') . ' AND lcom_dack IS NOT NULL' .
                ')' .
                (bYN(GetParm("SHOW_REPLIED",'Y')) ? ' AND lcom_dreplied IS NULL AND lcom_dduerepl < \'@TODAY\'' : '') .
                ' AND
                LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                        LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                        ORDER by lcom_primary desc, LINK_COMPL.recordid))',
            'OverdueDateField' => 'lcom_dduerepl'
        ),
        'completed' => array(
            'Title' => _tk("com_completed_listing_title"),
            'Link' => _tk("com_completed_listing_title"),
            'NoOverdue' => true,
            'Where' => 'compl_main.rep_approved = \'FA\' AND (compl_main.com_dclosed IS NOT NULL
                        OR (
                        compl_main.recordid in (SELECT LINK_COMPL.com_id from LINK_COMPL, LINK_CONTACTS
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_CONTACTS.COM_ID = LINK_COMPL.COM_ID AND LINK_COMPL.LCOM_CURRENT=\'Y\')
                        AND
                        compl_main.recordid in (SELECT com_id from LINK_COMPL
                                                    WHERE LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID ' .
                                                    $CompletedWHERECheck .
                                                    ' AND
                                                    LINK_COMPL.recordid = (SELECT top 1 LINK_COMPL.recordid from LINK_COMPL WHERE
                                                                            LINK_COMPL.COM_ID = COMPL_MAIN.RECORDID AND LINK_COMPL.LCOM_CURRENT=\'Y\'
                                                                            ORDER by lcom_primary desc, LINK_COMPL.recordid))))'
        ),
        'rejected' => array(
            'Title' => _tk("com_rejected_listing_title"),
            'Link' => _tk("com_rejected_listing_title"),
            'NoOverdue' => true,
            'Where' => 'compl_main.rep_approved = \'REJECT\''
        ),
    ),
    'ACTION_LINK_LIST_FIELDS' => array(
        array('field' => 'recordid', 'width' => '5'),
        array('field' => 'com_name', 'width' => '17'),
        array('field' => 'com_dreceived', 'width' => '10'),
        array('field' => 'com_type', 'width' => '17'),
        array('field' => 'com_detail', 'width' => '51')
    ),
    'TRAFFICLIGHTS_FIELDS' => array(
        'com_method', 'com_type', 'rep_approved'
    ),
    'HOME_SCREEN_STATUS_LIST' => true,
    'GENERATE_MAPPINGS' => array(
        'INC' => array(
            'com_name' => 'inc_name',
            'com_organisation' => 'inc_organisation',
            'com_unit' => 'inc_unit',
            'com_clingroup' => 'inc_clingroup',
            'com_directorate' => 'inc_directorate',
            'com_specialty' => 'inc_specialty',
            'com_loctype' => 'inc_loctype',
            'com_locactual' => 'inc_locactual',
            'com_mgr' => 'inc_mgr',
            'com_detail' => 'inc_notes',
            'com_root_causes' => 'inc_root_causes',
            'rep_approved' => 'rep_approved',
            'com_dincident' => 'inc_dincident'
        ),
        'CLA' => array(
            'com_name' => 'cla_name',
            'com_organisation' => 'cla_organisation',
            'com_unit' => 'cla_unit',
            'com_clingroup' => 'cla_clingroup',
            'com_directorate' => 'cla_directorate',
            'com_specialty' => 'cla_specialty',
            'com_loctype' => 'cla_location',
            'com_locactual' => 'cla_locactual',
            'com_mgr' => 'cla_mgr',
            'com_detail' => 'cla_synopsis',
            'com_root_causes' => 'cla_root_causes',
            'rep_approved' => 'rep_approved',
            'com_dincident' => 'cla_dincident'
        )
    ),
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_CONTACTS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
    'CCS2_FIELDS' => array(
        'com_affecting_tier_zero',
        'com_type_tier_one',
        'com_type_tier_two',
        'com_type_tier_three',
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Complaints/c_dx_complaints_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Complaints/c_dx_complaints_guide.xml',
);

//Remove unwanted sections according to defined complaints chain globals.
//If stages are hidden, then include them in the next stage count/WHERE clause
if (!bYN(GetParm("SHOW_ACKNOWLEDGED",'Y')))
{
    $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awatinginivestigation']['Where'] = "(" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awatinginivestigation']['Where'] . ")" .
                                                                                " OR (" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awatingacknowledgement']['Where'] . ")";

    unset($ModuleDefs['COM']['HARD_CODED_LISTINGS']['awatingacknowledgement']);
}

if (!bYN(GetParm("SHOW_ACTIONED",'Y')))
{
    $ModuleDefs['COM']['HARD_CODED_LISTINGS']['underinvestigation']['Where'] = "(" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['underinvestigation']['Where'] . ")" .
                                                                                " OR (" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awatinginivestigation']['Where'] . ")";

    unset($ModuleDefs['COM']['HARD_CODED_LISTINGS']['awatinginivestigation']);
}

if (!bYN(GetParm("SHOW_RESPONSE",'Y')))
{
    $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingholding']['Where'] = "(" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingholding']['Where'] . ")" .
                                                                                " OR (" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['underinvestigation']['Where'] . ")";

    unset($ModuleDefs['COM']['HARD_CODED_LISTINGS']['underinvestigation']);
}

if (!bYN(GetParm("SHOW_HOLDING",'Y')))
{
    $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingreply']['Where'] = "(" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingreply']['Where'] . ")" .
                                                                                " OR (" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingholding']['Where'] . ")";

    unset($ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingholding']);
}

if (!bYN(GetParm("SHOW_REPLIED",'Y')))
{
   $ModuleDefs['COM']['HARD_CODED_LISTINGS']['completed']['Where'] = "(" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['completed']['Where'] . ")" .
                                                                                " OR (" . $ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingreply']['Where'] . ")";

   unset($ModuleDefs['COM']['HARD_CODED_LISTINGS']['awaitingreply']);
}

