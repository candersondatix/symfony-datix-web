<?php

require_once 'Source/generic_modules/COM/ModuleFunctions.php';

$lastDReopened = new FormField('ReadOnly');
$lastDReopened->MakeDateField('lcom_last_dreopened', getLastReopenedDate($data['com_id']));
$GradeFields = new FormField($FormType);

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "header" => array(
        "Title" => "Name and Reference",
        "Rows" => array(
            array(
                'Name' => 'recordid',
                'Condition' => ($data['recordid'] || $FormType == 'Design' || $FormType == 'Search')
            ),
            "com_name",
            'com_ourref',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'COM',
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => 'COM',
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            )),
            "com_mgr",
            "com_head",
            "com_dopened",
            "com_dclosed",
            "com_dreopened",
            array(
                'Type' => 'formfield',
                'Name' => 'lcom_last_dreopened',
                'FormField' => $lastDReopened,
                'NoReadOnly' => true
            ),
        )
    ),
    "details" => array(
        "Title" => "Details of ". _tk('COMNameTitle'),
        "Rows" => array(
            ['Name' => 'com_last_updated', 'ReadOnly' => true],
            "com_method",
            "com_type",
            "com_subtype",
            'com_inc_type',
            "com_dreceived",
            "com_consent",
            "com_dincident",
            "com_detail",
            'com_purchaser',
            'com_otherref',
            "com_curstage",
            "com_outcome",
            "com_summary"
        )
    ),
    'additional' => array(
        'Title' => 'Additional Information',
        'Rows' => array(
            'show_person'
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormType == 'Design' || ($FormType != 'Search' && $FormType != 'Search' && $data['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('COM', $data['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "location" => array(
        "Title" =>"Location admitted",
        "Rows" => array(
            'com_organisation',
            'com_unit',
            'com_clingroup',
            'com_directorate',
            'com_specialty',
            'com_loctype',
            'com_locactual'
        )
    ),
    "ko41" => array(
        "Title" => 'KO41',
        "Rows" => array(
            "com_ko41_type",
            "com_koservarea",
            "com_kosubject",
            "com_koprof",
            "com_koethnic_pat",
            "com_koethnic_staff"
        )
    ),
    "subject_header" => array(
        "Title" => "Subjects",
        'NotModes' => array('Search'),
        "Rows" => array(
            'com_subjects_linked'
        )
    ),
    "subject" => array(
        "Title" => "Subjects",
        "Condition" => (($FormType != 'Design' && $FormType != 'Print') || ($FormType == 'Print' && $data['com_subjects_linked'] != 'N')),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoSubjectSection' => array(
                'controller' => 'src\\generic\\controllers\\SubjectsController'
            )
        ),
        "ExtraParameters" => array('subject_name' => 'com_subject'),
        "Rows" => array()
    ),
    "subject_design" => array(
        "Title" => "Subject",
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "NoSectionActions" => true,
        "AltSectionKey" => "subject",
        "Rows" => array(
            'csu_subject',
            'csu_subsubject',
            'csu_stafftype',
            'csu_organisation',
            'csu_unit',
            'csu_clingroup',
            'csu_directorate',
            'csu_specialty',
            'csu_location',
            'csu_loctype',
            'csu_outcome',
            'csu_notes',
            'csu_dcompleted',
        )
    ),
    "isd" => array(
        "Title" => 'ISD',
        "Rows" => array(
            "com_isd_unit",
            "com_isd_locactual",
            "com_isd_consent",
            "com_isd_dconsent_req",
            "com_isd_dconsent_rec",
            "com_isd_div_sent",
            "com_isd_ref_added",
            "com_isd_iaas_involved",
            "com_isd_cas_involved",
            "com_isd_chi_no",
            "com_isd_resp_sent_20",
            "com_isd_resp_20_reason",
            "com_isd_agree_40",
            "com_isd_agree_40_date",
            "com_isd_resp_40_reason",
            "com_isd_actions",
            "com_isd_plan",
            "com_isd_dexport",
        )
    ),
    "issue_header" => array(
        "Title" => "Issues of ". _tk('COMNameTitle'),
        'NotModes' => array('Search'),
        "Rows" => array(
            'com_issues_linked'
        )
    ),
    "issue" => array(
        "Title" => "Issues",
        "Condition" => (($FormType != 'Design' && $FormType != 'Print') || ($FormType == 'Print' && $data['com_issues_linked'] != 'N')),
        'NotModes' => array('Search'),
        "NoFieldAdditions" => true,
        'MandatorySection' => 'issue',
        "Include" => "Source/generic_modules/COM/ModuleFunctions.php",
        "Function" => "DoIssueSection",
        "ExtraParameters" => array('issue_name' => 'com_issue'),
        "Rows" => array()
    ),
    "issue_design" => array(
        "Title" => "Issues",
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "AltSectionKey" => "issue",
        "NoSectionActions" => true,
        "Rows" => array(
            'cisd_type',
            'cisd_category',
            'cisd_subcategory',
            'cisd_pat_adm_type',
            'cisd_service_area',
            'cisd_specialty',
            'cisd_staff_group',
            'cisd_staff_position',
        )
    ),
    "primary_compl_dates" => array(
        "Title" => "Primary Complainant Chain",
        "Condition" => ($FormType != 'Search' && ($FormType == 'Design' || HasPrimaryComplLinkData($data['recordid']))),
        "NotModes" => array("Search"),
        'ControllerAction' => [
            'complaintDates' => [
                'controller' => 'src\\complaints\\controllers\\ComplaintController'
            ]
        ],
    	'Rows' => array(
    			'lcom_dreceived',
    			'lcom_ddueback',
    			'lcom_dack',
    			'lcom_ddueact',
    			'lcom_dactioned',
    			'lcom_ddueresp',
    			'lcom_dresponse',
    			'lcom_dduehold',
    			'lcom_dholding',
    			'lcom_dreopened'
    			)
    ),
    "primary_compl_dates_history" => array(
        "Title" => "Primary Complainant Chain History",
        "Condition" => ($FormType != 'Search' && ($FormType == 'Design' || (HasPrimaryComplLinkData($data['recordid']) && ShowComplaintDatesHistory($data['recordid'])))),
        "NotModes" => array("Search"),
        'ControllerAction' => [
            'complaintDatesHistory' => [
                'controller' => 'src\\complaints\\controllers\\ComplaintController'
            ]
        ],
    	'Rows' => array()
    ),
    "feedback" => array(
        "Title" => _tk('feedback_title'),
        "Special" => "Feedback",
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        'NoFieldRemoval' => true,
        'NoReadOnly' => true,
        "Rows" => [
            [
                'Name'        => 'dum_fbk_to',
                'Title'       => 'Staff and contacts attached to this record',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_gab',
                'Title'       => 'All users',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_email',
                'Title'       => 'Additional recipients',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_subject',
                'Title'       => 'Subject',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ],
            [
                'Name'        => 'dum_fbk_body',
                'Title'       => 'Body of message',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ]
        ]
    ),
    "investigation" => array(
        "Title" => "Details of investigation",
        "Rows" => array(
            "com_investigator",
            "com_inv_dstart",
            "com_inv_dcomp",
            "dum_com_grading",
            "com_inv_outcome",
            "com_lessons_code",
            "com_inv_lessons",
            "com_action_code",
            "com_inv_action"
        )
    ),
    'notepad' => array(
        "Title" => "Notepad",
        'NotModes' => array('New','Search'),
        "Rows" => array(
            'notes'
        )
    ),
    'independent_review' => array(
        "Title" => "Independent Review",
        'NotModes' => array('New','Search'),
        "Rows" => array(
            "com_drequest",
            "com_ddueackreq",
            "com_dackreq",
            "com_dstatement",
            "com_dduelaychair",
            "com_dlaychair",
            "com_dduedecision",
            "com_ddecision",
            "com_assessor",
            "com_recir",
            "com_irsynopsis",
            "com_dinform",
            "com_dduepappt",
            "com_dpappt",
            "com_dduepdraft",
            "com_dpdraft",
            "com_ddueppublish",
            "com_dppublish",
            "com_dduecereply",
            "com_dcereply",
            "com_ircode"
        )
    ),
    'hcc' => array(
        "Title" => "Healthcare Commission",
        'NotModes' => array('New','Search'),
        "Rows" => array(
            'rev_drequested',
            'rev_reason',
            'rev_dclosed',
            'rev_ddecisiondue',
            'rev_ddecisiondone' ,
            'rev_init_handler',
            'rev_init_outcome',
            'rev_investigate_yn',
            'rev_init_comments',
            'rev_dhctermsdue',
            'rev_dhctermsdone',
            'rev_dtermsdue',
            'rev_dtermsdone',
            'rev_dcompleteddue',
            'rev_dcompleteddone',
            'rev_inv_handler',
            'rev_inv_outcome',
            'rev_panelreview_yn',
            'rev_inv_recommend',
            'rev_inv_action',
            'rev_dpanelrequested',
            'rev_dpanelcompleted',
            'rev_panel_outcome',
            'rev_ombudsman_yn',
            'rev_panel_recommend',
            'rev_panel_action',
            'rev_dombrequested',
            'rev_omb_assessment',
            'rev_omb_upheld',
            //'rev_omb_reason',
            'rev_omb_handler',
            'rev_omb_recommend',
            'rev_omb_action'
        )
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController'
            )
        ),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    'linked_records' => array(
        'Title' => 'Linked Records',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Special' => 'LinkedRecords',
        'Rows' => array()
    ),
    "payments" => array(
        "Title" =>"Payments",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Listings' => array('payments' => array('module' => 'PAY')),
        'LinkedForms' => array('payments' => array('module' => 'PAY')),
        'ControllerAction' => [
            'listPayments' => [
                'controller' => 'src\\payments\\controllers\\PaymentController'
            ]
        ]
    ),
    "rejection" => GenericRejectionArray('COM', $data),
    "rejection_history" => array(
        "Title" => _tk('reasons_history_title'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'SectionRejectionHistory' => array(
                'controller' => 'src\\reasons\\controllers\\ReasonsController'
            )
        ),
        "NotModes" => array("New","Search"),
        "Condition" =>  (bYN(GetParm("REJECT_REASON",'Y'))),
        "Rows" => array()
    ),
    'ccs2' => array(
        'Title' => 'Datix CCS2',
        'Condition' =>  (bYN(GetParm('CCS2_COM', 'N'))),
        'Rows' => array(
            'com_affecting_tier_zero',
            'com_type_tier_one',
            'com_type_tier_two',
            'com_type_tier_three'
        )
    ),
);

//add contact sections for each contact type.
foreach($ModuleDefs['COM']['CONTACTTYPES'] as $ContactTypeDetails)
{
    $ContactArray['contacts_type_'.$ContactTypeDetails['Type']] =
        array(
            'Title' => $ContactTypeDetails['Plural'],
            'NoFieldAdditions' => true,
            'ControllerAction' => array(
                'ListLinkedContacts' => array(
                    'controller' => 'src\\contacts\\controllers\\ContactsController'
                )
            ),
            'ExtraParameters' => array('link_type' => $ContactTypeDetails['Type']),
            'NotModes' => array('New','Search'),
            'Listings' => array('contacts_type_'.$ContactTypeDetails['Type'] => array('module' => 'CON')),
            'LinkedForms' => array($ContactTypeDetails['Type'] => array('module' => 'CON')),
            'Condition' => (CanSeeContacts('COM', $DIFPerms, $inc['rep_approved']) || !bYN(GetParm('DIF2_HIDE_CONTACTS', 'N'))),
            'Rows' => array()
        );
}

array_insert_datix($FormArray, 'feedback', $ContactArray);

require_once 'Source/libs/ModuleLinks.php';

foreach(getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array('module' => $link_mod, 'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']);
}

if(GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}