<?php
$FieldDefs["COM"] = array(
    "recordid" => array(
        "Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5
    ),
    "com_organisation" => array(
        "Type" => "ff_select",
        "Title" => "Trust"
    ),
    "com_unit" => array(
        "Type" => "ff_select",
        "Title" => "Unit"
    ),
    "com_clingroup" => array(
        "Type" => "ff_select",
        "Title" => "Clinical Group"
    ),
    "com_directorate" => array(
        "Type" => "ff_select",
        "Title" => "Directorate"
    ),
    "com_specialty" => array(
        "Type" => "ff_select",
        "Title" => "Specialty"
    ),
    "com_loctype" => array(
        "Type" => "ff_select",
        "Title" => "Location (Type)"
    ),
    "com_locactual" => array(
        "Type" => "ff_select",
        "Title" => "Location (Exact)"
    ),
    "com_mgr" => array(
        "Type" => "ff_select",
        "Title" => "Handler"
    ),
    'com_head' => array(
        "Type" => "ff_select",
        'Title' => 'Manager'
    ),
    "com_dreceived" => array(
        "Type" => "date",
        "Title" => "First received",
        'NotFuture' => true
    ),
    "com_dincident" => array(
        "Type" => "date",
        "Title" => "Incident Date",
        'NotFuture' => true
    ),
    "com_detail" => array(
        "Type" => "textarea",
        "Title" => _tk("com_detail"),
        "Rows" => 10,
        "Columns" => 70
    ),
    "com_method" => array(
        "Type" => "ff_select",
        "Title" => _tk("com_method")
    ),
    "com_type" => array(
        "Type" => "ff_select",
        "Title" => "Type"
    ),
    "com_subtype" => array(
        "Type" => "ff_select",
        "Title" => "Subtype"
    ),
    "com_curstage" => array(
        "Type" => "ff_select",
        "Title" => "Current Stage"
    ),
    "com_outcome" => array(
        "Type" => "ff_select",
        "Title" => "Outcome code"
    ),
    "com_summary" => array(
        "Type" => "textarea",
        "Title" => "Outcome",
        "Rows" => 10,
        "Columns" => 70
    ),
    'com_purchaser' => array(
        'Title' => 'Commissioner',
        'Type' => 'ff_select'
    ),
    'com_otherref' => array(
        'Title' => 'Other ref',
        'Type' => 'string',
        "Width" => 32,
        'MaxLength' => 32
    ),
    "csu_notes" => array(
        "Type" => "textarea",
        "Title" => "Notes",
        'NoListCol' => true,
        "Rows" => 10,
        "Columns" => 70
    ),
    "csu_dcompleted" => array(
        "Type" => "date",
        "Title" => "Completed date",
        'NoListCol_Override' => true,
    ),
    "com_name" => array(
        "Type" => "string",
        "Title" => "Name",
        "Width" => 70,
        "MaxLength" => 32
    ),
    "com_ourref" => array(
        "Type" => "string",
        "Title" => "Our Ref",
        "Width" => 50,
        "MaxLength" => 32
    ),
    "com_inc_type" => array(
        "Type" => "ff_select",
        "Title" => "Incident Type"
    ),
    "link_patrelation" => array(
        "Type" => "ff_select",
        "NoListCol" => true,
        "Title" => "Relationship"
    ),
    "com_investigator" => array(
        "Type" => "multilistbox",
        "Title" => "Investigator",
    	"MaxLength" => 252
    ),
    "com_inv_outcome" => array(
        "Type" => "ff_select",
        "Title" => "Outcome of investigation"
    ),
    "com_inv_dstart" => array(
        "Type" => "date",
        'NotFuture' => true,
        "Title" => "Date investigation started"
    ),
    "com_inv_dcomp" => array(
        "Type" => "date",
        'NotFuture' => true,
        'NotEarlierThan' => array('com_inv_dstart'),
        "Title" => "Date investigation completed"
    ),
    "com_inv_lessons" => array(
        "Type" => "textarea",
        "Title" => "Lessons learned",
        "Rows" => 7,
        "Columns" => 70
    ),
    "com_inv_action" => array(
        "Type" => "textarea",
        "Title" => "Action taken",
        "Rows" => 7,
        "Columns" => 70
    ),
    "com_action_code" => array(
        "Type" => "multilistbox",
        "Title" => "Action taken codes",
        "NoListCol" => true,
    	"MaxLength" => 128
    ),
    "com_lessons_code" => array(
        "Type" => "multilistbox",
        "Title" => "Lessons learned codes",
        "NoListCol" => true,
    	"MaxLength" => 128
    ),
    "csu_subject" => array(
        "Type" => "ff_select",
        "Title" => "Subject",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_subject',
        "OldCodes" => true
    ),
    "csu_subsubject" => array(
        "Type" => "ff_select",
        "Title" => "Sub-Subject",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_subsubject',
        "OldCodes" => true
    ),
    "csu_stafftype" => array(
        "Type" => "ff_select",
        "Title" => "Staff Type",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_stafftype',
        "OldCodes" => true
    ),
    "csu_directorate" => array(
        "Type" => "ff_select",
        "Title" => "Directorate",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_directorate',
        "OldCodes" => true
    ),
    "csu_specialty" => array(
        "Type" => "ff_select",
        "Title" => "Specialty",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_specialty',
        "OldCodes" => true
    ),
    "csu_location" => array(
        "Type" => "ff_select",
        "Title" => "Location",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_location',
        "OldCodes" => true
    ),
    "csu_outcome" => array(
        "Type" => "ff_select",
        "Title" => "Outcome",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_outcome',
        "OldCodes" => true
    ),
    "csu_clingroup" => array(
        "Type" => "ff_select",
        "Title" => "Clinical Group",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_clingroup',
        "OldCodes" => true
    ),
    "csu_organisation" => array(
        "Type" => "ff_select",
        "Title" => "Organisation",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_organisation',
        "OldCodes" => true
    ),
    "csu_loctype" => array(
        "Type" => "ff_select",
        "Title" => "Location Type",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_loctype',
        "OldCodes" => true
    ),
    "csu_unit" => array(
        "Type" => "ff_select",
        "Title" => "Unit",
        "NoListCol" => true,
        'FieldFormatsName' => 'com_unit',
        "OldCodes" => true
    ),
    "lcom_iscomplpat" => array("Type" => "yesno", 'Title' => _tk("link_complpat"), "NoListCol" => true),
    "lcom_ddueack" => array('Title' => 'Acknowledged due (Complainant)', "Type" => "date", "NoListCol" => true),
    "lcom_ddueact" => array('Title' => 'Actioned due (Complainant)', "Type" => "date", "NoListCol" => true),
    "lcom_ddueresp" => array('Title' => 'Response due (Complainant)', "Type" => "date", "NoListCol" => true),
    "lcom_dduehold" => array('Title' => 'Holding due (Complainant)', "Type" => "date", "NoListCol" => true),
    "lcom_dduerepl" => array('Title' => 'Replied due (Complainant)', "Type" => "date", "NoListCol" => true),
    "lcom_dreceived" => array(
        'Title' => 'Date Received',
        "Type" => "date",
        "NoListCol" => true,
        "NotFuture" => true
    ),
    "lcom_dack" => array('Title' => 'Acknowledged (Complainant)', "Type" => "date", "NoListCol" => true, "NotFuture" => true),
    "lcom_dactioned" => array('Title' => 'Actioned done (Complainant)', "Type" => "date", "NoListCol" => true, "NotFuture" => true),
    "lcom_dresponse" => array('Title' => 'Response done (Complainant)', "Type" => "date", "NoListCol" => true, "NotFuture" => true),
    "lcom_dholding" => array('Title' => 'Holding done (Complainant)', "Type" => "date", "NoListCol" => true, "NotFuture" => true),
    "lcom_dreplied" => array('Title' => 'Replied done (Complainant)', "Type" => "date", "NoListCol" => true, "NotFuture" => true),
    "lcom_primary" => array('Title' => 'Primary Complainant', 'Type' => 'yesno', "NoListCol" => true),
    "rep_approved" => array(
        "Type" => "ff_select",
        "Title" => "Approval status",
        "Width" => 32
    ),
    "show_document" => array(
        "Type" => "checkbox",
        "Title" => "Would you like to attach any documents?",
        "NoListCol" => true
    ),
    "show_person" => array(
        "Type" => "yesno",
        "Title" => "Are there any additional people affected by this complaint?",
        "NoListCol" => true
    ),
    "com_subjects_linked" => array(
        "Type" => "yesno",
        "Title" => "Do you want to add any subjects to this record?",
        "NoListCol" => true
    ),
    "com_issues_linked" => array(
        "Type" => "yesno",
        "Title" => "Do you want to add any issues to this record?",
        "NoListCol" => true
    ),
    "notes" => array(
        "Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 10,
        "Columns" => 70,
        "NoListCol" => true
    ),
    /*ko41 fields*/
    'com_ko41_type' => array('Title' => 'KO41 Type', 'Type' => 'string', 'MaxLength' => 6),
    'com_kosubject' => array('Title' => 'Subject', 'Type' => 'ff_select'),
    'com_koservarea' => array('Title' => 'Service area', 'Type' => 'ff_select'),
    'com_koprof' => array('Title' => 'Profession', 'Type' => 'ff_select'),
    'com_koethnic_pat' => array('Title' => 'Patient Ethnicity', 'Type' => 'ff_select'),
    'com_koethnic_staff' => array('Title' => 'Staff Ethnicity', 'Type' => 'ff_select'),
    /* fields not used in the web, but default names might be needed for reporting if WEB_LABELS_ONLY is set */
    'com_isd_locactual' => array('Title' => 'Location', 'Type' => 'ff_select'),
    'com_likelihood' => array('Title' => 'Likelihood of recurrence', 'Type' => 'ff_select'),
    'com_dduelaychair' => array('Title' => 'Lay chair appointed (due)', 'Type' => 'date'),
    'com_dlaychair' => array('Title' => 'Lay chair appointed (done)', 'Type' => 'date'),
    'com_unit1' => array('Title' => 'Unit (primary)', 'Type' => 'ff_select'),
    'com_organisation1' => array('Title' => 'Trust (primary)', 'Type' => 'ff_select'),
    'com_loctype1' => array('Title' => 'Location type (primary)', 'Type' => 'ff_select'),
    'com_location1' => array('Title' => 'Location exact (primary)', 'Type' => 'ff_select'),
    'com_isd_dexport' => array('Title' => 'Last exported'),
    'com_ircode' => array('Title' => 'IR Outcome Code'),
    'com_recomm_code' => array('Title' => 'Recommendations codes', 'Type' => 'ff_select', "NoListCol" => true),
    'com_ddueppublish' => array('Title' => 'Final report published (due)', 'Type' => 'date'),
    'com_dppublish' => array('Title' => 'Final report published (done)', 'Type' => 'date'),
    'com_isd_unit' => array('Title' => 'Health sector', 'Type' => 'ff_select'),
    'com_isd_iaas_involved' => array('Title' => 'IASS involved?', 'Type' => 'yesno'),
    'com_impact' => array('Title' => 'Impact on person', 'Type' => 'ff_select'),
    'com_isd_agree_40_date' => array('Title' => 'If yes - date of letter detailing this extension', 'Type' => 'date'),
    'com_isd_resp_20_reason' => array('Title' => 'If greater than 20 days, identify reason', 'Type' => 'ff_select'),
    'com_subsubject1' => array('Title' => 'Sub-subject (primary)', 'Type' => 'ff_select'),
    'com_dopened' => array('Title' => 'Opened date', 'Type' => 'date'),
    'com_drequest' => array('Title' => 'Request received', 'Type' => 'date', 'NotFuture' => true),
    'com_isd_resp_sent_20' => array('Title' => 'Response sent within 20 working days?', 'Type' => 'yesno'),
    'com_ddueackreq' => array('Title' => 'Request acknowledged (due)', 'Type' => 'date'),
    'com_dackreq' => array('Title' => 'Request acknowledged (done)', 'Type' => 'date'),
    'com_isd_ref_added' => array('Title' => 'Ref. number added?', 'Type' => 'yesno'),
    'com_root_causes' => array('Title' => 'Remediable causes', 'Type' => 'ff_select'),
    'com_dreopened' => array('Title' => 'Reopened', 'Type' => 'date', 'NotEarlierThan' => array('com_dopened')),
    'com_specialty1' => array('Title' => 'Specialty (primary)', 'Type' => 'ff_select'),
    'com_outcome1' => array('Title' => 'Outcome (primary)', 'Type' => 'ff_select'),
    'com_subject1' => array('Title' => 'Subject (primary)', 'Type' => 'ff_select'),
    'com_dstatement' => array('Title' => 'Statement received', 'Type' => 'date'),
    'com_dpappt' => array('Title' => 'Panel appointed (done)', 'Type' => 'date'),
    'com_stafftype1' => array('Title' => 'Staff type (primary)', 'Type' => 'ff_select'),
    'com_dduepappt' => array('Title' => 'Panel appointed (due)', 'Type' => 'date'),
    'com_dduepdraft' => array('Title' => 'Draft report published (due)', 'Type' => 'date'),
    'com_unit_type' => array('Title' => 'Unit type', 'Type' => 'ff_select'),
    'com_dinform' => array('Title' => 'Complainant informed', 'Type' => 'date'),
    'com_dclosed' => array('Title' => 'Closed date', 'Type' => 'date'),
    'com_clingroup1' => array('Title' => 'Clin. group (primary)', 'Type' => 'ff_select'),
    'com_isd_consent' => array('Title' => 'Consent required?', 'Type' => 'yesno'),
    'com_consequence' => array('Title' => 'Consequence', 'Type' => 'ff_select'),
    'com_isd_dconsent_rec' => array('Title' => 'Date consent obtained', 'Type' => 'date'),
    'com_isd_dconsent_req' => array('Title' => 'Date consent requested', 'Type' => 'date'),
    'com_ddecision' => array('Title' => 'Decision made (done)', 'Type' => 'date'),
    'com_dduedecision' => array('Title' => 'Decision made (due)', 'Type' => 'date'),
    'com_directorate1' => array('Title' => 'Directorate (primary)', 'Type' => 'ff_select'),
    'com_isd_agree_40' => array('Title' => 'Complainant agreed to timescale greater than 40 working days?', 'Type' => 'ff_select'),
    'com_isd_div_sent' => array('Title' => 'Diversity form sent?', 'Type' => 'yesno'),
    'com_dpdraft' => array('Title' => 'Draft report published (done)', 'Type' => 'date'),
    'com_isd_cas_involved' => array('Title' => 'CAS involved?', 'Type' => 'yesno'),
    'com_dduecereply' => array('Title' => 'C.E. replied to complainant (due)', 'Type' => 'date'),
    'com_dcereply' => array('Title' => 'C.E. replied to complainant (done)', 'Type' => 'date'),
    'com_isd_actions' => array('Title' => 'Actions taken', 'Type' => 'multilistbox', 'MaxLength' => 254),
    'com_isd_dexport' => array('Title' => 'Last exported', 'Type' => 'date'),
    "com_isd_plan" => array(
        "Type" => "textarea",
        "Title" => "Service improvement/long-term plan",
        "Rows" => 7,
        "Columns" => 70
    ),
    "com_isd_chi_no" => array(
        "Type" => "string",
        "Title" => "CHI number",
        "Width" => 50,
        "MaxLength" => 64
    ),
    "com_isd_resp_40_reason" => array(
        "Type" => "textarea",
        "Title" => "Reasons for response taking longer than 40 working days",
        "Rows" => 7,
        "Columns" => 70
    ),
    'com_ddueackreq' => array('Title' => 'Request acknowledged (due)', 'Type' => 'date'),
    'com_dackreq' => array('Title' => 'Request acknowledged (done)', 'Type' => 'date', 'NotFuture' => true),
    'com_dstatement' => array('Title' => 'Statement received', 'Type' => 'date'),
    'com_dduelaychair' => array('Title' => 'Lay chair appointed (due)', 'Type' => 'date'),
    'com_dlaychair' => array('Title' => 'Lay chair appointed (done)', 'Type' => 'date', 'NotFuture' => true),
    'com_dduedecision' => array('Title' => 'Decision made (due)', 'Type' => 'date'),
    'com_ddecision' => array('Title' => 'Decision made (done)', 'Type' => 'date', 'NotFuture' => true),
    'com_assessor' => array('Title' => 'Clinical assessor needed?', 'Type' => 'yesno'),
    'com_recir' => array('Title' => 'I.R. recommended?', 'Type' => 'yesno'),
    "com_irsynopsis" => array(
        "Type" => "textarea",
        "Title" => "Synopsis",
        "Rows" => 7,
        "Columns" => 70
    ),
    'com_dinform' => array('Title' => 'Complainant informed', 'Type' => 'date'),
    'com_dduepappt' => array('Title' => 'Panel appointed (due)', 'Type' => 'date'),
    'com_dpappt' => array('Title' => 'Panel appointed (done)', 'Type' => 'date', 'NotFuture' => true),
    'com_dduepdraft' => array('Title' => 'Draft report published (due)', 'Type' => 'date'),
    'com_dpdraft' => array('Title' => 'Draft report published (done)', 'Type' => 'date', 'NotFuture' => true),
    'com_ddueppublish' => array('Title' => 'Final report published (due)', 'Type' => 'date'),
    'com_dppublish' => array('Title' => 'Final report published (done)', 'Type' => 'date', 'NotFuture' => true),
    'com_dduecereply' => array('Title' => 'C.E. replied to complainant (due)', 'Type' => 'date'),
    'com_dcereply' => array('Title' => 'C.E. replied to complainant (done)', 'Type' => 'date', 'NotFuture' => true),
    'com_ircode' => array('Title' => 'IR Outcome Code', 'Type' => 'ff_select'),
    'com_notes1' => array('Title' => 'Subject notes (primary)'),
    'com_dcompleted1' => array('Title' => 'Completed date (primary)'),
    'rev_drequested' => array('Title' => 'Date of request', 'Type' => 'date', 'NoListCol' => true),
    'rev_reason' => array('Title' => 'Reason for request', 'Type' => 'multilistbox', 'NoListCol' => true, 'MaxLength' => 254),
    'rev_dclosed' => array('Title' => 'Closed date', 'Type' => 'date','NoListCol' => true),
    'rev_ddecisiondue' => array('Title' => 'Initial review decision due date', 'Type' => 'date', 'NoListCol' => true),
    'rev_ddecisiondone' => array('Title' => 'Initial review decision date', 'Type' => 'date', 'NoListCol' => true, 'NotFuture' => true),
    'rev_init_handler' => array('Title' => 'Initial review handler', 'Type' => 'ff_select', 'NoListCol' => true),
    'rev_init_outcome' => array('Title' => 'Initial review outcome', 'Type' => 'ff_select', 'NoListCol' => true),
    'rev_investigate_yn' => array('Title' => 'Investigation required?', 'Type' => 'yesno', 'NoListCol' => true),
    'rev_init_comments' => array(
        'Title' => 'Initial review Comments',
        "Type" => "textarea",
        "Rows" => 7,
        "Columns" => 70,
        'NoListCol' => true
    ),
    'rev_dhctermsdue' => array('Title' => 'HC terms due date', 'Type' => 'date', 'NoListCol' => true),
    'rev_dhctermsdone' => array('Title' => 'HC terms sent date', 'Type' => 'date', 'NoListCol' => true, 'NotFuture' => true),
    'rev_dtermsdue' => array('Title' => 'Terms of reference comments due date', 'Type' => 'date', 'NoListCol' => true),
    'rev_dtermsdone' => array('Title' => 'Terms of reference comments date', 'Type' => 'date', 'NoListCol' => true, 'NotFuture' => true),
    'rev_dcompleteddue' => array('Title' => 'Investigation completion due date', 'Type' => 'date', 'NoListCol' => true),
    'rev_dcompleteddone' => array('Title' => 'Investigation completion date', 'Type' => 'date', 'NoListCol' => true, 'NotFuture' => true),
    'rev_inv_handler' => array('Title' => 'Investigation review handler', 'Type' => 'ff_select', 'NoListCol' => true),
    'rev_inv_outcome' => array('Title' => 'Investigation outcome', 'Type' => 'ff_select', 'NoListCol' => true),
    'rev_panelreview_yn' => array('Title' => 'Panel review requested?', 'Type' => 'yesno', 'NoListCol' => true),
    'rev_inv_recommend' => array(
        'Title' => 'Investigation recommendations',
        "Type" => "textarea",
        "Rows" => 7,
        "Columns" => 70,
        'NoListCol' => true
    ),
    'rev_inv_action' => array(
        'Title' => 'Investigation action taken',
        "Type" => "textarea",
        "Rows" => 7,
        "Columns" => 70,
        'NoListCol' => true
    ),
    'rev_dpanelrequested' => array('Title' => 'Panel review requested date', 'Type' => 'date', 'NoListCol' => true),
    'rev_dpanelcompleted' => array('Title' => 'Panel review completion date', 'Type' => 'date', 'NoListCol' => true, 'NotFuture' => true),
    'rev_panel_outcome' => array('Title' => 'Panel review outcome', 'Type' => 'ff_select', 'NoListCol' => true),
    'rev_ombudsman_yn' => array('Title' => 'HSO requested?', 'Type' => 'yesno', 'NoListCol' => true),
    'rev_panel_recommend' => array(
        'Title' => 'Panel review recommendations',
        "Type" => "textarea",
        "Rows" => 7,
        "Columns" => 70,
        'NoListCol' => true
    ),
    'rev_panel_action' => array(
        'Title' => 'Panel review action taken',
        "Type" => "textarea",
        "Rows" => 7,
        "Columns" => 70,
        'NoListCol' => true
    ),
    'rev_dombrequested' => array('Title' => 'HSO requested date', 'Type' => 'date', 'NoListCol' => true),
    'rev_omb_assessment' => array('Title' => 'Assessment', 'Type' => 'ff_select', 'NoListCol' => true),
    'rev_omb_upheld' => array('Title' => 'Complaint upheld', 'Type' => 'ff_select', 'NoListCol' => true),
    'rev_omb_reason' => array('Title' => 'HSO reason for not considering complaint', 'NoListCol' => true),
    'rev_omb_handler' => array('Title' => 'HSO handler', 'Type' => 'ff_select','NoListCol' => true),
    'rev_omb_recommend' => array(
        'Title' => 'HSO recommendations',
        "Type" => "textarea",
        "Rows" => 7,
        "Columns" => 70,
        'NoListCol' => true
    ),
    'rev_omb_action' => array(
        'Title' => 'HSO action taken',
        "Type" => "textarea",
        "Rows" => 7,
        "Columns" => 70,
        'NoListCol' => true
    ),
    'com_ch8_poc' => array('Title' => 'Programme of Care (Subjects)(CH8)', 'NoListCol' => true),
    'com_subsubject' => array('Title' => 'Sub-subject (Subjects)', 'NoListCol' => true, 'Type' => 'ff_select'),
    'com_grade' => array('Title' => 'Grade', 'Type' => 'ff_select'),
    'com_pasno3' => array('Title' => 'PAS No. 3'),
    'lcom_details' => array('Title' => 'Notes (Complainant)', 'NoListCol' => true),
    'com_ch8_subject' => array('Title' => 'Subjects (Subjects)(CH8)', 'NoListCol' => true),
    'com_pasno2' => array('Title' => 'PAS No. 2'),
    'com_subject' => array('Title' => 'Subjects (Subjects)', 'NoListCol' => true, 'Type' => 'ff_select'),
    'com_cla_count' => array('Title' => 'No. of Linked Claims', 'NoListCol' => true),
    'com_inc_count' => array('Title' => 'No. of Linked Incidents', 'NoListCol' => true),
    'com_pal_count' => array('Title' => 'No. of Linked PALS', 'NoListCol' => true),
    'com_compl_count' => array('Title' => 'No. of Linked Complainants', 'NoListCol' => true),
    'com_consent' => array('Title' => 'Consent obtained', 'Type' => 'yesno'),
    'com_stafftype' => array('Title' => 'Staff types (Subjects)', 'NoListCol' => true, 'Type' => 'ff_select'),
    'com_location' => array('Title' => 'Location exact (Subjects)', 'NoListCol' => true, 'Type' => 'ff_select'),
    'com_notes' => array('Title' => 'Subject notes (Subjects)', 'NoListCol' => true, 'Type' => 'textarea'),
    'com_dcompleted' => array('Title' => 'Subject date completed (Subjects)', 'NoListCol' => true, 'Type' => 'date'),
    'com_pasno1' => array('Title' => 'PAS No. 1'),
    'lcom_dreopened' => array('Title' => 'Re-opened (Complainant)', 'NoListCol' => true, 'Type' => 'date', 'NotFuture' => true),
    'lcom_last_dreopened' => array('Title' => 'Complainant Re-opened date', 'NoListCol' => true, 'Type' => 'date', 'ReadOnly' => true),
    'com_recommend' => array('Title' => 'Recommendations'),
    // Issues
    "cisd_type" => array(
        "Type" => "ff_select",
        "Title" => "Issue type (ISD Issues)",
        'FieldFormatsName' => 'cisd_type',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "cisd_category" => array(
        "Type" => "ff_select",
        "Title" => "Issue category (ISD Issues)",
        'FieldFormatsName' => 'cisd_category',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "cisd_subcategory" => array(
        "Type" => "ff_select",
        "Title" => "Issue sub-category (ISD Issues)",
        'FieldFormatsName' => 'cisd_subcategory',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "cisd_pat_adm_type" => array(
        "Type" => "ff_select",
        "Title" => "Patient admission type (ISD Issues)",
        'FieldFormatsName' => 'cisd_pat_adm_type',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "cisd_service_area" => array(
        "Type" => "ff_select",
        "Title" => "Service area (ISD Issues)",
        'FieldFormatsName' => 'cisd_service_area',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "cisd_specialty" => array(
        "Type" => "ff_select",
        "Title" => "Specialty (ISD Issues)",
        'FieldFormatsName' => 'cisd_specialty',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "cisd_staff_group" => array(
        "Type" => "ff_select",
        "Title" => "Staff group (ISD Issues)",
        'FieldFormatsName' => 'cisd_staff_group',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    "cisd_staff_position" => array(
        "Type" => "ff_select",
        "Title" => "Staff position (ISD Issues)",
        'FieldFormatsName' => 'cisd_staff_position',
        'NoListCol' => true,
        "OldCodes" => true
    ),
    // risk matrix
    "dum_com_grading" => array(
        "Type" => "function",
        "Title" => "Risk grading",
        "Include" => 'Source/libs/FormClasses.php',
        "Function" => "MakeRatingFields",
        "Parameters" => array(
            array("V","Data"),
            array("V","FormType"),
            array("S","grading"),
            array("V","Module")
        ),
        "NoListCol" => true,
        "MandatoryField" => "com_grade"
    ),
    // CCS2
    'com_affecting_tier_zero' => array(
        'Type' => 'ff_select',
        'Title' => 'Incident affecting'
    ),
    'com_type_tier_one' => array(
        'Type' => 'ff_select',
        'Title' => 'Incident type tier 1'
    ),
    'com_type_tier_two' => array(
        'Type' => 'ff_select',
        'Title' => 'Incident type tier 2'
    ),
    'com_type_tier_three' => array(
        'Type' => 'ff_select',
        'Title' => 'Incident type tier 3'
    ),

    //contacts fields to be blocked from reports in this modules
    'link_injuries' => array('BlockFromReports' => true, 'Title' => 'Injuries (Persons)', 'NoListCol' => true),
    'link_injury1' => array('Title' => 'Injury (primary)', 'BlockFromReports' => true),
    'link_bodypart1' => array('Title' => 'Body part (primary)', 'BlockFromReports' => true),
    'com_last_updated' => [
        'Type' => 'string',
        'Title' => 'Last updated',
        'Width' => 32,
        'Computed' => true,
        'NoSearch' => true
    ]
);