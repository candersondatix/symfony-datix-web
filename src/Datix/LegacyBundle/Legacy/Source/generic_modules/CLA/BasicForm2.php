<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "details" => array(
        "Title" => "Claim Details",
        "Rows" => array(
            'recordid',
            'cla_ourref',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'CLA',
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => 'CLA',
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            )),
            'cla_mgr',
            'cla_name',
            'cla_type',
            'cla_subtype',
            'cla_synopsis',
            'cla_dopened',
            'cla_dincident',
            'cla_dclaim',
            'cla_dinsurer',
            'cla_insurer',
            'cla_insurer_ref',
            'cla_probability',
            'cla_estset',
            'cla_dsettled',
            'cla_dclosed',
            'cla_itype',
            'cla_head',
            'cla_outcome',
            'cla_otherref',
            'cla_cspecialty',
            array('Name' => 'cla_level_intervention', 'Condition' =>  (bYN(GetParm('CCS2_CLA', 'N')))),
            array('Name' => 'cla_level_harm', 'Condition' =>  (bYN(GetParm('CCS2_CLA', 'N')))),
            ['Name' => 'cla_last_updated', 'ReadOnly' => true]
        )
    ),
    "ccs" => array(
        "Title" =>"Datix CCS",
        "Rows" => array(
            'cla_carestage',
            'cla_clin_detail',
            'cla_clintype'
        )
    ),
    "ccs2" => array(
        "Title" =>"Datix CCS2",
        "Condition" =>  (bYN(GetParm("CCS2_CLA",'N'))),
        "Rows" => array(
            'cla_affecting_tier_zero',
            'cla_type_tier_one',
            'cla_type_tier_two',
            'cla_type_tier_three'
        )
    ),
    "curstage" => array(
        "Title" =>"Current stage",
        "Rows" => array('cla_curstage')
    ),
    "stagehistory" => array(
        "Title" =>"Stage history",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'stageHistory' => [
                'controller' => 'src\\claims\\controllers\\StageHistoryController'
            ]
        ],
        "NotModes" => array("New","Search"),
    ),
    "additional" => array(
        "Title" => "Additional details",
        "Rows" => array(
            "show_employee",
            "show_other_contacts",
            "show_document",
        )
    ),
    "rejection" => GenericRejectionArray('CLA', $data),
    "rejection_history" => array(
        "Title" => _tk('reasons_history_title'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'SectionRejectionHistory' => array(
                'controller' => 'src\\reasons\\controllers\\ReasonsController'
            )
        ),
        "NotModes" => array("New","Search"),
        "Condition" =>  (bYN(GetParm("REJECT_REASON",'Y'))),
        "Rows" => array()
    ),
    "location" => array(
        "Title" =>"Location",
        "Rows" => array(
            'cla_organisation',
            'cla_unit',
            'cla_clingroup',
            'cla_directorate',
            'cla_specialty',
            'cla_location',
            'cla_locactual'
        )
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "respondents" => array(
        "Title" => "Respondents",
        "contacttype" => 'O',
        'LinkedForms' => array(
            'O' => array('module' => 'CON', 'Label' => _tk('label_individual_respondent_form_design')),
            'G' => array('module' => 'ORG', 'Label' => _tk('label_organisation_respondent_form_design'))
        ),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'ListLinkedRespondents' => array(
                'controller' => 'src\\respondents\\controllers\\RespondentsController'
            )
        ),
        "NotModes" => array('New', 'Search'),
        "Rows" => array()
    ),
    "finance" => array(
        "Title" => 'Finance',
        "Rows" => array(
            'fin_estdamages',
            'fin_estpcosts',
            'fin_estdefence',
            'fin_calc_total',
            'fin_ourshare',
            'fin_calc_ourshare',
            'fin_otherparty',
            'fin_thirdshare',
            'fin_otherparty2',
            'fin_thirdshare2',
            'cla_insurer_excess',
            'fin_calc_liability',
            'fin_calc_reimbursement',
            'fin_indemnity_reserve',
            'fin_expenses_reserve',
            'indem_dclosed',
            'expen_dclosed',
            'fin_calc_reserve_1',
            'fin_calc_reserve_2',
            'fin_calc_total_incurred'
        )
    ),
    "reserve_audit" => array(
        "Title" => "Loss adjustment history",
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        "NotModes" => array('New', 'Search'),
        'ControllerAction' => array(
            'ReserveAudit' => array(
                'controller' => 'src\\claims\\controllers\\FinanceController'
            )
        ),
        "Rows" => array()
    ),
    "payments_summary" => array(
        "Title" =>"Payments summary",
        "NoFieldAdditions" => true,
        'NotModes' => array('New'),
        'ControllerAction' => array(
            'paymentsSummary' => array(
                'controller' => 'src\\claims\\controllers\\FinanceController'
            )
        ),
    ),
    "payments" => array(
        "Title" =>"Payments",
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'listPaymentsForClaim' => array(
                'controller' => 'src\\payments\\controllers\\PaymentController'
            )
        ),
        'NotModes' => array('New','Search'),
        'Listings' => array('payments' => array('module' => 'PAY')),
        'LinkedForms' => array('payments' => array('module' => 'PAY'))
    ),
    "feedback" => array(
        "Title" => _tk('feedback_title'),
        "Special" => "Feedback",
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        'NoFieldRemoval' => true,
        'NoReadOnly' => true,
        "Rows" => [
            [
                'Name'        => 'dum_fbk_to',
                'Title'       => 'Staff and contacts attached to this record',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_gab',
                'Title'       => 'All users',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_email',
                'Title'       => 'Additional recipients',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_subject',
                'Title'       => 'Subject',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ],
            [
                'Name'        => 'dum_fbk_body',
                'Title'       => 'Body of message',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ]
        ]
    ),
    "investigation" => array(
        "Title" => "Investigation",
        "Rows" => array(
            "cla_investigator",
            "cla_inv_dstart",
            "cla_inv_dcomp",
            "cla_cost",
            "dum_cla_grading",
            "cla_inv_outcome",
            "cla_lessons_code",
            "cla_inv_lessons",
            "cla_inquiry",
            "cla_action_code",
            "cla_inv_action"
        )
    ),
    "causes" => array(
        "Title" => "Causes",
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'SectionRootCauseDetails' => array(
                'controller' => 'src\\rootcauses\\controllers\\RootCausesController'
            )
        ),
        "Rows" => array()
    ),
    "causal_factor_header" => array(
        "Title" => "Causal factors for "._tk('CLAName'),
        'NotModes' => array('Search'),
        'NewPanel' => true,
        "Rows" => array(
            'cla_causal_factors_linked'
        )
    ),
    "causal_factor" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoCausalFactorsSection' => array(
                'controller' => 'src\\causalfactors\\controllers\\CausalFactorsController'
            )
        ),
        "ExtraParameters" => array('causal_factor_name' => 'cla_causal_factor'),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "causal_factor_design" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "AltSectionKey" => "causal_factor",
        "NoSectionActions" => true,
        "Rows" => array(
            'caf_level_1',
            'caf_level_2',
        )
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController'
            )
        ),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    'notepad' => array(
        "Title" => "Notepad",
        'NotModes' => array('New','Search'),
        "Rows" => array(
            'notes'
        )
    ),
    'linked_records' => array(
        'Title' => 'Linked Records',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Special' => 'LinkedRecords',
        'Rows' => array()
    ),
    "history" => array(
        "Title" => "Notifications",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'MakeEmailHistoryPanel' => [
                'controller' => 'src\\email\\controllers\\EmailHistoryController'
            ]
        ],
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $data['recordid']
            && count(Fields_ExtraField::getOrphanedUDFs('CLA', $data['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
);

//add contact sections for each contact type.
foreach ($ModuleDefs['CLA']['CONTACTTYPES'] as $ContactTypeDetails)
{
    // skip respondents
    if ($ContactTypeDetails['Type'] == 'O') {
        continue;
    }

    $ContactArray['contacts_type_'.$ContactTypeDetails['Type']] = array(
        'Title' => $ContactTypeDetails['Plural'],
        'NoFieldAdditions' => true,
        'ControllerAction' => array(
            'ListLinkedContacts' => array(
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            )
        ),
        'ExtraParameters' => array('link_type' => $ContactTypeDetails['Type']),
        'NotModes' => array('New','Search'),
        'Listings' => array('contacts_type_'.$ContactTypeDetails['Type'] => array('module' => 'CON')),
        'LinkedForms' => array($ContactTypeDetails['Type'] => array('module' => 'CON')),
        'Condition' => (CanSeeContacts('COM', $DIFPerms, $inc['rep_approved']) || !bYN(GetParm('DIF2_HIDE_CONTACTS', 'N'))),
        'Rows' => array()
    );
}

array_insert_datix($FormArray, 'finance', $ContactArray);

require_once 'Source/libs/ModuleLinks.php';

foreach (getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array(
        'module' => $link_mod,
        'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']
    );
}

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}
