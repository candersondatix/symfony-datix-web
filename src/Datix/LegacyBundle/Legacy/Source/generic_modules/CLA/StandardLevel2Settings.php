<?php
$GLOBALS['FormTitle'] = _tk('cla2_title');

$GLOBALS["DefaultValues"] = array (
  'rep_approved' => 'UN',
  'cla_dopened' => 'TODAY',
);

$GLOBALS["HideFields"] = array(
    'cla_itype' => true,
    'cla_outcome' => true,
    'cla_otherref' => true,
    'cla_cspecialty' => true,
    'cla_cost' => true,
    'cla_head' => true,
    'causal_factor_header' => true,
    'causal_factor' => true,
    'rejection_history' => true,
    'causes' => true,
    'word' => true,
    'history' => true,
    'orphanedudfs' => true,
    'ccs2' => true,
    'action_chains' => true,
    'cla_level_intervention' => true,
    'cla_level_harm' => true,
    'reserve_audit' => true,
    'indem_dclosed' => true,
    'expen_dclosed' => true,
    'cla_last_updated' => true
);

$GLOBALS["NewPanels"] = array (
    'location' => true,
    'progress_notes' => true,
    'contacts_type_M' => true,
    'finance' => true,
    'respondents' => true,
    'feedback' => true,
    'investigation' => true,
    'linked_actions' => true,
    'action_chains' => true,
    'documents' => true,
    'notepad' => true,
    'linked_records' => true,
    'history' => true,
    'rejection' => true,
    'orphanedudfs' => true,
);

$GLOBALS["ExpandSections"] = array (
  'rep_approved' =>
  array (
    0 =>
    array (
      'section' => 'rejection',
      'alerttext' => 'Please complete the \'Details of rejection\' section before saving this form.',
      'values' =>
      array (
        0 => 'REJECT',
      ),
    ),
  ),
  'cla_causal_factors_linked' =>
  array (
    0 =>
    array (
      'section' => 'causal_factor',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
);

$GLOBALS["MandatoryFields"] = array(
    'cla_name' => 'details',
);

$GLOBALS['UserExtraText'] = [
    'dum_fbk_to' => 'Only staff and contacts with e-mail addresses are shown.',
    'dum_fbk_gab' => 'Only users with e-mail addresses are shown.',
    'dum_fbk_email' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'
];

?>