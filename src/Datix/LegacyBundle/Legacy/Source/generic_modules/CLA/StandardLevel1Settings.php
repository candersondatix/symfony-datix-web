<?php
$GLOBALS['FormTitle'] = _tk('cla1_title');

$GLOBALS["ExpandSections"] = array (
  'show_employee' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_E',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_other_contacts' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_N',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'cla_causal_factors_linked' =>
  array (
    0 =>
    array (
      'section' => 'causal_factor',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_document' =>
  array (
    0 =>
    array (
      'section' => 'documents',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  )
);

$GLOBALS["HideFields"] = array(
    'cla_itype' => true,
    'cla_dopened' => true,
    'cla_probability' => true,
    'cla_estset' => true,
    'cla_dsettled' => true,
    'cla_dclosed' => true,
    'cla_outcome' => true,
    'cla_otherref' => true,
    'cla_cspecialty' => true,
    'causal_factor_header' => true,
    'causal_factor' => true,
    'ccs2' => true,
    'cla_level_intervention' => true,
    'cla_level_harm' => true,
    'contacts_type_O' => true,
    'respondents_organisation' => true
);

$GLOBALS["DefaultValues"] = array (
  'rep_approved' => 'UN',
  'cla_dopened' => 'TODAY',
);

$GLOBALS["MandatoryFields"] = array(
    'cla_name' => 'details',
);

$GLOBALS["FormDesigns"] = array (
  'contacts_type_A' =>
  array (
    'A' => '0',
  ),
  'contacts_type_C' =>
  array (
    'E' => '0',
  ),
);

?>