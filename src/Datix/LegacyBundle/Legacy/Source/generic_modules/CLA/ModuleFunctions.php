<?php
/**
 * POST save function for linked contacts in order to link a claimant as a Patient if the link_plapat field is set to 'Y'
 * 'Is claimant the person affected?' is a Y/N field.
 * - If 'N' is selected, upon save the system checks if a link exists to the 'Person affected' section and, if one does, that link is deleted.
 * - If 'Y' is selected, upon save the 'Person affected' section will automatically link to the same contact record.
 *
 * @param mixed $aParams
 */
function SavePatient($aParams)
{
    // We need the suffix to deal with contacts attached on level 1 forms
    $suffix = $aParams['suffixstring'];
    $linkType = 'link_type'.$suffix;
    $linkPlapat = 'link_plapat'.$suffix;

    if ($aParams['data'][$linkType] == 'M' && $aParams['data'][$linkPlapat] == 'Y')
    {
        //Link Claimant as Person as well
        $_POST[$linkType] = 'A';

        $sql = 'SELECT COUNT(*) as check_link_exists FROM link_contacts WHERE cla_id = :com_id AND link_contacts.con_id =:con_id AND link_contacts.link_type= :link_type';
        $row = DatixDBQuery::PDO_fetch($sql, array("com_id" => $aParams['main_recordid'], "con_id" => $aParams['recordid'], "link_type" => 'A'));

        if ($row["check_link_exists"] == 0)
        {
            unset($_POST['link_exists'.$suffix]);
            SaveLinkedContact($aParams);
        }

    }
    elseif ($aParams['data'][$linkType] == 'M' && $aParams['data'][$linkPlapat] == 'N')
    {
        //Delete link as Person if exists
        $sql = 'DELETE FROM link_contacts WHERE cla_id = :com_id AND link_contacts.con_id =:con_id AND link_contacts.link_type= :link_type';
        DatixDBQuery::PDO_query($sql, array("com_id" => $aParams['main_recordid'], "con_id" => $aParams['recordid'], "link_type" => 'A'));

    }
}

function validatePartyPercentages()
{
    $Error = array();

    if($_POST['fin_ourshare'] + $_POST['fin_thirdshare'] + $_POST['fin_thirdshare2'] > 100)
    {
        $Error['fin_ourshare'] = 'Total of share fields cannot come to more than 100%';
        $Error['fin_thirdshare'] = 'Total of share fields cannot come to more than 100%';
        $Error['fin_thirdshare2'] = 'Total of share fields cannot come to more than 100%';
    }

    return $Error;

}

/**
 * If a new stage has been added, adds it to stage history table.
 **/
function AddLatestStage($Data)
{
    if($Data['cla_curstage'])
    {
        $LastStage = DatixDBQuery::PDO_fetch('SELECT TOP 1 EventType from events_history WHERE cas_id = :cas_id AND CHAINNAME = :chainname ORDER BY EVENTDATE DESC, recordid DESC', array('cas_id' => $Data['recordid'], 'chainname' => 'CSTG'), PDO::FETCH_COLUMN);

        if($LastStage != $Data['cla_curstage'])
        {
            $StageID = GetNextRecordID('events_history', false);
            DatixDBQuery::PDO_build_and_insert('events_history',
                array('recordid' => $StageID, 'CHAINNAME' => 'CSTG', 'cas_id' => $Data['recordid'], 'EventType' => $Data['cla_curstage'], 'EVENTDATE' => GetTodaysDate()));
        }
    }
}

/**
 * Validate organisation attached to level 1
 *
 * @return array
 */
function validateLinkedOrganisations()
{
    $maxLinkedOrganisations = $_POST['organisation_max_suffix'];

    $error = array();

    if ($maxLinkedOrganisations == '')
    {
        return array();
    }

    $organisationNumber = 100;

    while ($organisationNumber <= $maxLinkedOrganisations)
    {
        if (!OrganisationNotFilledIn($_POST, $organisationNumber))
        {
            $organisationData = (new \src\organisations\model\OrganisationModelFactory())->getMapper()->findOrganisationByID($_POST['org_id_'.$organisationNumber]);

            if (count($organisationData) == 0 || $organisationData == '')
            {
                $error['org_name_' . $organisationNumber] = _tk('invalid_organisation_error');
            }
        }

        $organisationNumber++;
    }

    return $error;
}

function OrganisationNotFilledIn($Data, $Suffix)
{
    return  ($Data["org_name_$Suffix"] == ""
        && $Data["org_reference_$Suffix"] == ""
        && $Data["org_address_$Suffix"] == ""
        && $Data["org_postcode_$Suffix"] == ""
        && $Data["org_tel1_$Suffix"] == ""
        && $Data["org_tel2_$Suffix"] == ""
        && $Data["org_email_$Suffix"] == ""
        && $Data["org_organisation_$Suffix"] == ""
        && $Data["org_unit_$Suffix"] == ""
        && $Data["org_clingroup_$Suffix"] == ""
        && $Data["org_directorate_$Suffix"] == ""
        && $Data["org_specialty_$Suffix"] == ""
        && $Data["org_loctype_$Suffix"] == ""
        && $Data["org_locactual_$Suffix"] == ""
        && $Data["org_notes_$Suffix"] == "");
}
/*
 * Depending on the global CLAIM_CLOSED_DATES closes either the claim close dates, indemnity & expenses reserve dates or
 * all of the above. This happens when a claim is set to closed
 *
 * @global          CLAIM_CLOSED_DATES can be NONE for no date changes, CLAIM to only close the claim date, RES to only
 *                  close the reserve dates or BOTH for all three dates
 * @param   $data   takes the data posted in saving a claim and manipulates it before saving
 * @return  $data   the return of the manipulated data
 */
function calculateClaimCloseDates($data)
{
    //make sure the claim is being closed (and not previously closed) and the global is set to change dates
    if ($data['rep_approved'] == 'CLOSED' && $data['rep_approved_old'] != 'CLOSED' && getParm('CLAIM_CLOSED_DATES', 'NONE') != 'NONE')
    {
        //make sure we have the relevant dates
        $sql = 'SELECT cla_dclosed, indem_dclosed, expen_dclosed FROM claims_main WHERE recordid = :recordid';
        $dates = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $data['recordid']));

        //get the string for todays date
        $now = (new DateTime())->format('Y-m-d H:i:s');

        //loop through each relevant date
        foreach(array('cla_dclosed', 'indem_dclosed', 'expen_dclosed') as $dateName)
        {
            //if the relevant date is empty...
            if ($data[$dateName] == null && $dates[$dateName] == null)
            {
                //check the global covers each date
                if(($dateName == 'cla_dclosed' && (getParm('CLAIM_CLOSED_DATES', 'NONE') == 'CLAIM' || getParm('CLAIM_CLOSED_DATES', 'NONE') == 'BOTH')) ||
                    ($dateName != 'cla_dclosed' && (getParm('CLAIM_CLOSED_DATES', 'NONE') == 'RES' || getParm('CLAIM_CLOSED_DATES', 'NONE') == 'BOTH')))
                {
                    //if the date is empty and the global covers its closure, close it.
                    $data[$dateName] =  $now;
                    $data['CHANGED-'.$dateName] = 1;
                }
            }
            elseif ($data[$dateName] == null && $dates[$dateName] != null)
            {
                //if the data provided does not hold the relevant date, but it is available on the database, correct here.
                $data[$dateName] = $dates[$dateName];
            }
        }
    }
    return $data;
}
/*
 * Zeroes the indemnity or expenses remaining reserve if the reserve close date has been set. Zeroing takes place on the
 * date in question, and is achieved by setting the reserve level to the equivalent of total payments against that reserve.
 * Payments added after the date of closure do not count and so it is possible for the reserve to not actually be 0.
 * Additionally, payments made on the exact date of closure will be included, and payments with no date will always be
 * included. This method can also be called by a linked payment through a module function. This all happens pre-save on
 * a claim, or payment linked to a claim.
 *
 * Note that this function has been revamped several times, and is now officially a monster. I have put in as many
 * comments as seem reasonable to explain it, but it is essentially a piece of procedural hell.
 *
 * @param   $data   takes the data posted in saving a claim and manipulates it before saving
 * @return  $data   the return of the manipulated data
 */
function zeroReservesOnDate($data)
{
    //make sure this isn't a payment on incidents or complaints
    if($data['module'] == 'CLA' || ($data['module'] == 'PAY' && (strtoupper($data['pay_module']) == 'CLA' || $data['link_module'] == 'CLA')))
    {
        //check we have all the relevant data
        if ($data['module'] == 'PAY')
        {
            //get out any relevant claims data
            $sql = 'SELECT indem_dclosed, expen_dclosed, fin_indemnity_reserve, fin_expenses_reserve
                FROM claims_main
                WHERE recordid = :cla_id';
            $claimData = \DatixDBQuery::PDO_fetch($sql, array('cla_id' => $data['main_recordid']));

            // We can only merge the two arrays if the second one is an array otherwise we will lose the $data array completely.
            if (is_array($claimData))
            {
                $data = array_merge($data, $claimData);
            }
        }
        //this is where the magic happens
        if ((isset($data['indem_dclosed']) && $data['indem_dclosed'] != null) || (isset($data['expen_dclosed']) && $data['expen_dclosed'] != null))
        {
            //todays date for audit
            $dt = new DateTime();
            $now = $dt->format('Y-m-d H:i:s.000');

            //sql for inserting into the regular audit
            $ressql = "INSERT INTO full_audit (aud_module, aud_record, aud_login, aud_date, aud_action, aud_detail)
                        VALUES (:aud_module, :aud_record, :aud_login, :aud_date, :aud_action, :aud_detail)";

            //find out which payment types relate to which reserve
            $ptasql = 'SELECT CODE, cod_reserve FROM CODE_FIN_TYPE';
            $payTypeAssignments = \DatixDBQuery::PDO_fetch_all($ptasql, array(), PDO::FETCH_KEY_PAIR);

            //get all the individual payments made on this claim
            $paysql = 'SELECT recordid, pay_date, pay_type, pay_calc_total FROM payments WHERE cla_id = :cla_id';
            $payments = \DatixDBQuery::PDO_fetch_all($paysql, array('cla_id' => ($data['module'] == 'CLA' ? $data['recordid'] : $data['main_recordid'])));

            //If the module is payments, we need to make sure the payment we are saving is properly considered
            if($data['module'] == 'PAY')
            {
                //pay_calc_total is a calculated field and has yet to be calculated, so we will calculate it manually
                $data['pay_calc_total'] = $data['pay_amount'];
                if(!empty($data['pay_vat_rate']))
                {
                    $data['pay_calc_total'] += ($data['pay_vat_rate'] / 100) * $data['pay_amount'];
                }
                //If the payment is found, it needs to be updated with the values (from $_POST) which have not yet been saved
                $paymentFound = false;
                foreach($payments as $key => $payment)
                {
                    if($payment['recordid'] == $data['recordid'])
                    {
                        $paymentFound = true;
                        $payments[$key]['pay_date'] = $data['pay_date'];
                        $payments[$key]['pay_type'] = $data['pay_type'];
                        $payments[$key]['pay_calc_total'] = $data['pay_calc_total'];
                    }
                }
                //If the payment is not found (i.e. it's a new payment) we should add it in
                if(!$paymentFound)
                {
                    $payments[] = array('pay_date' => $data['pay_date'],
                                        'pay_type' => $data['pay_type'],
                                        'pay_calc_total' => $data['pay_calc_total']);
                }
            }

            //repeat for each reserve
            foreach (array('indem' => 'indemnity', 'expen' => 'expenses') as $reserve => $reserveFull)
            {
                //if the close date is set, and it has changed value (from null or from another date)
                if (($data[$reserve . '_dclosed'] != null))
                {
                    //start finding the values to zero the reserve
                    $expectedReserve = 0;
                    $payTypes = array();
                    //this switch builds an array of pay types that refer to the particular reserve in question
                    //Indemnity = 1, Expenses = 2
                    foreach ($payTypeAssignments as $key => $val)
                    {
                        if ($reserve == 'indem')
                        {
                            if ($val == 1)
                            {
                                $payTypes[] = $key;
                            }
                        } else
                        {
                            if ($val == 2)
                            {
                                $payTypes[] = $key;
                            }
                        }
                    }
                    //check date is the right format
                    $reserveDate = new DateTime($data[$reserve . '_dclosed']);
                    //for each payment repeat:
                    foreach ($payments as $payment)
                    {
                        //if this payment is of the right type, include it:
                        if (in_array($payment['pay_type'], $payTypes))
                        {
                            //if it has a date attached to it
                            if ($payment['pay_date'] != null)
                            {
                                $payDate = new DateTime($payment['pay_date']);
                                //check that the date is before the reserve close date
                                if ($payDate->diff($reserveDate)->invert == 0)
                                {
                                    $expectedReserve += $payment['pay_calc_total'];
                                }
                            } //if no payment date, automatically include it
                            else
                            {
                                $expectedReserve += $payment['pay_calc_total'];
                            }
                        }
                    }
                    /*at the end of this loop, $expectedReserve is now the value the reserve will change to in order to
                    zero the remaining reserve on the given close date*/

                    //if the value has changed or the reserve audit has not yet started an entry needs to go into the audit
                    if ($expectedReserve != $data['fin_' . $reserveFull . '_reserve']
                        || !hasLossAdjustmentHistory($reserveFull, $data['module'] == 'CLA' ? $data['recordid'] : $data['main_recordid']))
                    {
                        //audit insert before save
                        \DatixDBQuery::PDO_query($ressql, array(
                            "aud_module" => 'CLA',
                            "aud_record" => ($data['module'] == 'CLA' ? $data['recordid'] : $data['main_recordid']),
                            "aud_login" => $_SESSION["initials"],
                            "aud_date" => $now,
                            "aud_action" => "WEB:fin_" . $reserveFull . "_reserve",
                            "aud_detail" => floatval($data['fin_' . $reserveFull . '_reserve'])
                        ));
                        $ressql2 = "INSERT INTO fin_" . $reserveFull . "_reserve_audit (cla_id, field_value, change_value, changed_by, datetime_from, reason_for_change)
                        VALUES (:cla_id, :field_value, :change_value, :changed_by, :datetime_from, :reason_for_change)";

                        $change_value = $expectedReserve - $data['fin_' . $reserveFull . '_reserve'];
                        $data['fin_' . $reserveFull . '_reserve'] = $expectedReserve;

                        //reserve (special claims stuff) audit
                        \DatixDBQuery::PDO_query($ressql2, array(
                            "cla_id" => ($data['module'] == 'CLA' ? $data['recordid'] : $data['main_recordid']),
                            "field_value" => floatval($data['fin_' . $reserveFull . '_reserve']),
                            "change_value" => $change_value,
                            "changed_by" => $_SESSION["initials"],
                            "datetime_from" => $now,
                            "reason_for_change" => "Reserves have been automatically adjusted because the record has been closed or a payment has been updated"
                        ));
                    }
                }
            }
        }
        //If the module is PAY then the values won't automatically save after running this function
        if ($data['module'] == 'PAY')
        {
            $insertsql = 'UPDATE CLAIMS_MAIN SET fin_indemnity_reserve=:fin_indemnity_reserve, fin_expenses_reserve=:fin_expenses_reserve
                          WHERE recordid=:recordid';
            $insertarray = array('fin_indemnity_reserve' => $data['fin_indemnity_reserve'],
                                 'fin_expenses_reserve' => $data['fin_expenses_reserve'],
                                 'recordid' => $data['main_recordid'] );
            \DatixDBQuery::PDO_query($insertsql, $insertarray);
        }
    }
    return $data;
}
/*
 * This deals with a very specific case for 'incurred' fields which once they have a value, cannot return to null.
 * The reason for this is that we must accurately track the loss adjustment history, and going from some value to
 * null is not the same as some value to zero (the former not being numerically measurable). If someone then clears
 * a value from one of these incurred fields we will manually interpret null as zero.
 *
 * @param   $data   takes the data posted in saving a claim and manipulates it before saving
 * @return  $data   the return of the manipulated data
 */
function checkIncurredAreNotNull($data)
{
    foreach(array('indemnity', 'expenses') as $reserve)
    {
        if (empty($data['fin_'.$reserve.'_reserve']) && hasLossAdjustmentHistory($reserve, $data['recordid']))
        {
            $data['fin_'.$reserve.'_reserve'] = 0;
        }
    }

    return $data;
}

/*
 * Checks to see whether a claim record has any values set in the loss adjustment history for a particular reserve.
 *
 * @param   $reserve    The reserve to be checked (should be either 'indemnity' or 'expenses')
 * @param   $claimID    The ID of the related claim record
 * @return  bool        True if there are eny reserve audit entries, else false
 */
function hasLossAdjustmentHistory($reserve, $claimId)
{
    $sql = 'SELECT TOP 1 datetime_from FROM fin_'.$reserve.'_reserve_audit
            WHERE cla_id = :cla_id
            ORDER BY datetime_from DESC';
    $result = \DatixDBQuery::PDO_fetch($sql, array('cla_id' => $claimId));

    return $result !== false;
}
