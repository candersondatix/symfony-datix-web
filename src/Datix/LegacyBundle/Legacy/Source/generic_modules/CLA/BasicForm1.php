<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => false,
        "Condition" => false,
        "ExtraContacts" => 1
    ),
    "details" => array(
        "Title" => "Claim details",
        "Rows" => array(
            'cla_ourref',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => $module,
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => $module,
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            )),
            'cla_name',
            'cla_type',
            'cla_subtype',
            'cla_synopsis',
            'cla_dincident',
            'cla_dclaim',
            'cla_dinsurer',
            'cla_insurer',
            'cla_insurer_ref',
            'cla_itype',
            'cla_dopened',
            'cla_probability',
            'cla_estset',
            'cla_dsettled',
            'cla_dclosed',
            'cla_outcome',
            'cla_otherref',
            'cla_cspecialty',
            array('Name' => 'cla_level_intervention', 'Condition' =>  (bYN(GetParm('CCS2_CLA', 'N')))),
            array('Name' => 'cla_level_harm', 'Condition' =>  (bYN(GetParm('CCS2_CLA', 'N'))))
        )
    ),
    "location" => array(
        "Title" =>"Location",
        "Rows" => array(
            'cla_organisation',
            'cla_unit',
            'cla_clingroup',
            'cla_directorate',
            'cla_specialty',
            'cla_location',
            'cla_locactual'
        )
    ),
    "ccs" => array(
        "Title" =>"Datix CCS",
        "Rows" => array(
            'cla_carestage',
            'cla_clin_detail',
            'cla_clintype'
        )
    ),
    "ccs2" => array(
        "Title" =>"Datix CCS2",
        "Condition" =>  (bYN(GetParm("CCS2_CLA",'N'))),
        "Rows" => array(
            'cla_affecting_tier_zero',
            'cla_type_tier_one',
            'cla_type_tier_two',
            'cla_type_tier_three'
        )
    ),
    "contacts_type_M" => array(
        "Title" => "Details of "._tk('cla_claimant'),
        "module" => 'CLA',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        'LinkedForms' => array('M' => array('module' => 'CON')),
        "contacttype" => 'M',
        "suffix" => 1,
        "Rows" => array()
    ),
    "contacts_type_O" => array(
        "Title" => "Details of "._tk('cla_individual_respondent'),
        "module" => 'CLA',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        'LinkedForms' => array('O' => array('module' => 'CON', 'Label' => _tk('label_individual_respondent_form_design'))),
        "contacttype" => 'O',
        "suffix" => 5,
        "Rows" => array()
    ),
    'respondents_organisation' => array(
        'Title' => 'Details of organisation respondent',
        'NoFieldAdditions' => true,
        'Rows' => array(),
        'LinkedForms' => array('G' => array('module' => 'ORG')),
        'ControllerAction' => array(
            'MakeDynamicOrganisationSection' => array(
                'controller' => 'src\\respondents\\controllers\\RespondentsController'
            )
        )
    ),
    "contacts_type_A" => array(
        "Title" => "Details of person affected by the "._tk('CLAName'),
        "module" => 'CLA',
        "NoFieldAdditions" => true,
        "contacttype" => 'A',
        "suffix" => 2,
        'LinkedForms' => array('A' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "additional" => array(
        "Title" => "Additional details",
        "Rows" => array(
            "show_employee",
            "show_other_contacts",
            "show_document",
        )
    ),
    "contacts_type_E" => array(
        "Title" => "Employees",
        "module" => 'CLA',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'E',
        "suffix" => 3,
        'LinkedForms' => array('E' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "contacts_type_N" => array(
        "Title" => "Contacts",
        "module" => 'CLA',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'N',
        "suffix" => 4,
        'LinkedForms' => array('N' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        "NoFieldAdditions" => true,
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && ($FormType != 'Print' && $FormType != 'ReadOnly'),
        "NoReadOnly" => true,
        "Special" => 'DynamicDocument',
        "Rows" => array()
    ),
    "linked_documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && $FormType == 'Print',
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "causal_factor_header" => array(
        "Title" => "Causal factors for "._tk('CLANameTitle'),
        'NewPanel' => true,
        "Rows" => array(
            'cla_causal_factors_linked'
        )
    ),
    "causal_factor" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoCausalFactorsSection' => array(
                'controller' => 'src\\causalfactors\\controllers\\CausalFactorsController'
            )
        ),
        "ExtraParameters" => array('causal_factor_name' => 'cla_causal_factor'),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "causal_factor_design" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "AltSectionKey" => "causal_factor",
        "NoSectionActions" => true,
        "Rows" => array(
            'caf_level_1',
            'caf_level_2',
        )
    )
);

?>