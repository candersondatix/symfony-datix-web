<?php
$ModuleDefs['CLA'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'CLA',
    'LEVEL1_PERMS' => array('CLA1'),
    'AUDIT_TRAIL_PERMS' => array('CLA2'),  //which permissions can see the audit trail?

    'MOD_ID' => MOD_CLAIMS,
    'CODE' => 'CLA',
    // 'APPROVAL_LEVELS' => false,
    'NAME' => _tk("mod_claims_title"),
    'USES_APPROVAL_STATUSES' => true,
    'TABLE' => 'claims_main',
    'REC_NAME' => _tk("CLAName"),
    'REC_NAME_PLURAL' => _tk("CLANames"),
	'REC_NAME_TITLE' => _tk("CLANameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("CLANamesTitle"),
    'FK' => 'cla_id',
    'OURREF' => 'cla_ourref',
    'PERM_GLOBAL' => 'CLA_PERMS',
    'NO_LEVEL1_GLOBAL' => 'CLA_NO_OPEN',
    'ACTION' => 'record&module=CLA',
    'FIELD_NAMES' => array(
        'NAME' => 'cla_name',
        'HANDLER' => 'cla_mgr',
        'MANAGER' => 'cla_head',
        'REF' => 'cla_ourref',
        'INVESTIGATORS' => 'cla_investigator',
        ),
    'LOCATION_FIELD_PREFIX' => 'cla_',
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'CLAIM1'),
        2 => array('LEVEL' => 2, 'CODE'=>'CLAIM2')
        ),
    'EXTRA_RECORD_DATA_FUNCTIONS' => array(
         'GetYNCausalFactors'
    ),
    'EXTRA_RECORD_DATA_FUNCTION_INCLUDES' => array(
        'Source/generic/CausalFactors.php'
    ),
    'POST_LINK_CONTACT_SAVE_INCLUDES' => array(
        'Source/generic_modules/CLA/ModuleFunctions.php'
        ),
    'POST_LINK_CONTACT_SAVE_FUNCTIONS' => array(
        'SavePatient'
        ),
    'CAUSAL_FACTOR_TYPE' => 'cla_causal_factor',
    'LOGGED_OUT_LEVEL1' => true,
    'FORMCODE1' => 'CLAIM1',
    'FORMCODE2' => 'CLAIM2',
    'ICON' => 'icons/icon_CLA.png',
    'AGE_AT_DATE' => 'cla_dincident',
    'ADD_NEW_RECORD_LEVELS' => array('CLA2', 'CLA1'),
    'NO_REP_APPROVED' => true,

    'HARD_CODED_LISTINGS' => array(
        'potential' => array(
            'Title' => _tk('potential_claims'),
            'Link'  => _tk('potential_claims'),
            'UseApprovalStatusDescription' => 'UN',
            'Where' => 'claims_main.rep_approved = \'UN\' OR claims_main.rep_approved IS NULL OR claims_main.rep_approved = \'\'',
            'Condition' => FALSE, // so the menu item doesn't appear in the options menu
            'NoOverdue' => true,
            'workflows' => array('0')
        ),

        'active' => array(
            'Title' => _tk('confirmed_claims'),
            'Link' => _tk('confirmed_claims'),
            'UseApprovalStatusDescription' => 'ACT',
            'Where' => 'claims_main.rep_approved = \'ACT\'',
            'Condition' => FALSE,
            'NoOverdue' => true,
            'workflows' => array('0','1')
        ),

        'closed' => array(
            'Title' => _tk('closed_claims'),
            'Link' => _tk('closed_claims'),
            'UseApprovalStatusDescription' => 'CLOSED',
            'Where' => 'claims_main.rep_approved = \'CLOSED\'',
            'Condition' => FALSE,
            'NoOverdue' => true,
            'workflows' => array('0','1')
        ),

        'rejected' => array(
            'Title' => _tk('rejected_claims'),
            'Link' => _tk('rejected_claims'),
            'UseApprovalStatusDescription' => 'REJECT',
            'Where' => 'claims_main.rep_approved = \'REJECT\'',
            'Condition' => FALSE,
            'NoOverdue' => true,
            'workflows' => array('0','1')
        )
    ),
    'LINKED_DOCUMENTS' => true,
    'DOCUMENT_SECTION_KEY' => 'documents',

    'LINKED_CONTACT_LISTING_COLS' => array('recordid', 'cla_name', 'cla_ourref', 'cla_dopened', 'cla_type'),

    'EXTRA_SAVE_INCLUDES' => array(
        'Source/generic_modules/CLA/ModuleFunctions.php'
    ),
    'EXTRA_SAVE_FUNCTIONS' => array('AddLatestStage'
    ),

    'SEARCH_URL' => 'action=search',
    'DEFAULT_ORDER' => 'cla_dopened',
    'OVERDUE_CHECK_FIELD' => 'cla_dopened',
    'OVERDUE_STATUSES' => array('ACT'),

    'CONTACTTYPES' => array(
                        "M" => array( "Type"=> "M", "Name"=>_tk('cla_claimant'), 'Plural'=> _tk('cla_claimant_plural'), 'None'=> _tk('no_cla_claimant_plural'), "CreateNew"=>_tk('cla_claimant_link')),
                        "A" => array( "Type"=> "A", "Name"=>_tk('cla_person'), 'Plural'=> _tk('cla_person_plural'), 'None'=> _tk('no_cla_person_plural'), "CreateNew"=>_tk('cla_person_link')),
                        "E" => array( "Type"=> "E", "Name"=>_tk('cla_employee'), 'Plural'=> _tk('cla_employee_plural'), 'None'=> _tk('no_cla_employee_plural'), "CreateNew"=>_tk('cla_employee_link')),
                        "N" => array( "Type"=> "N", "Name"=>_tk('cla_other_contact'), 'Plural'=> _tk('cla_other_contact_plural'), 'None'=> _tk('no_cla_other_contact_plural'), "CreateNew"=>_tk('cla_other_contact_link')),
                        "O" => array( "Type"=> "O", "Name"=>_tk('cla_individual_respondent_title'), 'Plural'=> _tk('cla_individual_respondent_plural'), 'None'=> _tk('no_cla_individual_respondent_plural'), "CreateNew"=>_tk('cla_individual_respondent_link'))
                    ),

    'LEVEL1_CON_OPTIONS' => array(
                        "O" => array("Title" => _tk('cla_individual_respondent_title'), "DivName" => 'contacts_type_O'),
                        "M" => array("Title" => _tk('cla_claimant'), "DivName" => 'contacts_type_M'),
                        "A" => array("Title" => _tk('cla_person'), "DivName" => 'contacts_type_A'),
                        "N" => array("Title" => _tk('cla_contact'), "DivName" => 'contacts_type_N'),
                        "R" => array("Title" => _tk('cla_reporter'), "Role" => GetParm('REPORTER_ROLE', 'REP'), "ActualType" => "N", "DivName" => 'contacts_type_R', 'Max' => 1)
                        ),

    'STAFF_EMPL_FILTER_MAPPINGS' => array('cla_organisation' => 'con_orgcode',
                                    'cla_unit' => 'con_unit',
                                    'cla_clingroup' => 'con_clingroup',
                                    'cla_directorate' => 'con_directorate',
                                    'cla_specialty' => 'con_specialty',
                                    'cla_location' => 'con_loctype',
                                    'cla_locactual' => 'con_locactual'),

    'FIELD_ARRAY' => array(
        "cla_name", "cla_carestage", "cla_clin_detail", "cla_clintype",
        "cla_synopsis", "cla_mgr", "cla_head", "cla_ourref", "cla_otherref", "cla_dincident",
        "cla_dopened", "cla_dclaim", "cla_dclosed", "cla_dsettled", "cla_type", "cla_subtype",
        "cla_itype", "cla_outcome", "cla_organisation", "cla_unit", "cla_clingroup", "cla_directorate",
        "cla_specialty", "cla_location", "cla_locactual", "cla_cspecialty", "cla_estset",
        "cla_probability", "fin_maxliability", "fin_estdamages", "fin_estpcosts",
        "fin_estdefence", "fin_totalclaim", "fin_cnstliability", "fin_ourexcess", "fin_ourshare",
        "fin_thisyear", "fin_nextyear", "fin_future", 'cla_insurer', 'cla_insurer_ref',
        "cla_investigator", "cla_inv_dstart", "cla_inv_dcomp", "cla_root_causes", "cla_inv_outcome",
        "cla_lessons_code", "cla_inv_lessons", "cla_action_code", "cla_inv_action", "cla_inquiry",
        "cla_cost", "cla_likelihood", "cla_consequence", "cla_grade", "cla_dinsurer",
        "show_employee", "show_other_contacts", "show_document", 'fin_calc_total', 'fin_calc_ourshare',
        'cla_curstage', 'fin_otherparty', 'fin_thirdshare', 'fin_otherparty2', 'fin_thirdshare2',
        'cla_insurer_excess', 'fin_calc_liability', 'fin_calc_reimbursement', 'rep_approved',
        'cla_affecting_tier_zero', 'cla_type_tier_one', 'cla_type_tier_two', 'cla_type_tier_three', 'fin_indemnity_reserve', 'fin_expenses_reserve',
        'cla_level_intervention', 'cla_level_harm', 'fin_calc_reserve_1', 'fin_calc_reserve_2', 'fin_calc_total_incurred',
        'cla_last_updated', 'indem_dclosed', 'expen_dclosed'
    ),
    'LINKED_RECORDS' => array(
        'cla_causal_factor' => array(
            'section' => 'causal_factor',
            'type' => 'cla_causal_factor', 'table' => 'causal_factors', 'recordid_field' => 'recordid', 'save_listorder' => true,
            'save_controller' => 'cla_causal_factors_linked',
            'basic_form' => array('Rows' => array('caf_level_1', 'caf_level_2')),
            'main_recordid_label' => 'CLA_ID',
            'useIdentity' => true
        )
    ),
    'BEFORE_SAVE_FUNCTIONS' => array(
        'checkIncurredAreNotNull',
        'calculateClaimCloseDates',
        'zeroReservesOnDate'
    ),
    'USE_WORKFLOWS' => true,
    'WORKFLOW_GLOBAL' => 'CLA_WORKFLOW',
    'DEFAULT_WORKFLOW' => 0,
    'SECURITY' => array(
        'LOC_FIELDS' => array(
            'cla_organisation' => array(),
            'cla_unit' => array(),
            'cla_clingroup' => array(),
            'cla_directorate' => array(),
            'cla_specialty' => array(),
            'cla_location' => array('multi' => true),
            'cla_locactual' => array('multi' => true)
            ),
        'EMAIL_SELECT_GLOBAL' => 'CLA_EMAIL_SELECT',
        'EMAIL_NOTIFICATION_GLOBAL' => 'CLA_STA_EMAIL_LOCS'
        ),
    'SHOW_EMAIL_GLOBAL' => 'CLA_SHOW_EMAIL',
    'EMAIL_REPORTER_GLOBAL' => 'CLA_EMAIL_REPORTER',
    'EMAIL_HANDLER_GLOBAL' => 'CLA_EMAIL_MGR',
    'EMAIL_USER_PARAMETER' => 'CLA_STA_EMAIL_LOCS',

    'FORM_DESIGN_ARRAY_FILE' => 'UserCLAForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserCLA1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserCLA2Settings',

    'ACTION_LINK_LIST_FIELDS' => array(
        array('field' => 'recordid', 'width' => '5'),
        array('field' => 'cla_mgr', 'width' => '10'),
        array('field' => 'cla_name', 'width' => '10'),
        array('field' => 'cla_dopened', 'width' => '6'),
        array('field' => 'cla_synopsis', 'width' => '30')
        ),

    'TRAFFICLIGHTS_FIELDS' => array(
        'cla_type'
        ),

    'HOME_SCREEN_STATUS_LIST' => true,

    'DATA_VALIDATION_INCLUDES' => array(
        'Source/generic_modules/CLA/ModuleFunctions.php'
    ),
    'DATA_VALIDATION_FUNCTIONS' => array(
        'validatePartyPercentages',
        'validateLinkedOrganisations'
    ),

    'GENERATE_MAPPINGS' => array('INC' => array(
                                    'cla_name' => 'inc_name',
                                    'cla_organisation' => 'inc_organisation',
                                    'cla_unit' => 'inc_unit',
                                    'cla_clingroup' => 'inc_clingroup',
                                    'cla_directorate' => 'inc_directorate',
                                    'cla_specialty' => 'inc_specialty',
                                    'cla_location' => 'inc_loctype',
                                    'cla_locactual' => 'inc_locactual',
                                    'cla_mgr' => 'inc_mgr',
                                    'cla_synopsis' => 'inc_notes',
                                    'rep_approved' => 'rep_approved',
                                    'cla_dincident'=> 'inc_dincident',
                                    'cla_carestage' => 'inc_carestage',
                                    'cla_clin_detail' => 'inc_clin_detail',
                                    'cla_clintype' => 'inc_clintype'),
                                 'COM' => array(
                                    'cla_name' => 'com_name',
                                    'cla_organisation' => 'com_organisation',
                                    'cla_unit' => 'com_unit',
                                    'cla_clingroup' => 'com_clingroup',
                                    'cla_directorate' => 'com_directorate',
                                    'cla_specialty' => 'com_specialty',
                                    'cla_location' => 'com_loctype',
                                    'cla_locactual' => 'com_locactual',
                                    'cla_mgr' => 'com_mgr',
                                    'cla_synopsis' => 'com_detail',
                                    'rep_approved' => 'rep_approved',
                                    'cla_dincident'=> 'com_dincident'),
                                 'PAL' => array(
                                    'cla_name' => 'pal_name',
                                    'cla_organisation' => 'pal_organisation',
                                    'cla_unit' => 'pal_unit',
                                    'cla_clingroup' => 'pal_clingroup',
                                    'cla_directorate' => 'pal_directorate',
                                    'cla_specialty' => 'pal_specialty',
                                    'cla_location' => 'pal_loctype',
                                    'cla_locactual' => 'pal_locactual',
                                    'cla_mgr' => 'pal_handler',
                                    'cla_synopsis' => 'pal_synopsis',
                                    'rep_approved' => 'rep_approved'),
                                 'RAM' => array(
                                    'cla_name' => 'ram_name',
                                    'cla_organisation' => 'ram_organisation',
                                    'cla_unit' => 'ram_unit',
                                    'cla_clingroup' => 'ram_clingroup',
                                    'cla_directorate' => 'ram_directorate',
                                    'cla_specialty' => 'ram_specialty',
                                    'cla_location' => 'ram_location',
                                    'cla_locactual' => 'ram_locactual',
                                    'cla_mgr' => 'ram_handler',
                                    'cla_synopsis' => 'ram_description',
                                    'rep_approved' => 'rep_approved')
                                    ),
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_CONTACTS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
    'CCS2_FIELDS' => array(
        'cla_affecting_tier_zero',
        'cla_type_tier_one',
        'cla_type_tier_two',
        'cla_type_tier_three',
        'cla_level_intervention',
        'cla_level_harm'
    ),
    'COMBO_LINK_WHERE' => "AND fmt_field NOT IN ('secgroup', 'cla_unit_type')",
    // A list of all possible organisations fields that a user can search on from a CLA1 form.
    // Exact fields used in a specific installation will be set in the ORG_PAS_CHK_FIELDS global
    'CLA1_ORG_SEARCHING_FIELDS' => array(
        'org_name',
        'org_reference',
        'org_address',
        'org_postcode',
        'org_tel1',
        'org_tel2',
        'org_email',
        'org_organisation',
        'org_unit',
        'org_clingroup',
        'org_directorate',
        'org_specialty',
        'org_loctype',
        'org_locactual',
        'org_notes'
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Claims/c_dx_claims_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Claims/c_dx_claims_guide.xml',
    'CONTACT_LINK_TABLE_ID' => array('link_respondents' => 'main_recordid', 'link_contacts' => 'CLA_ID')
);