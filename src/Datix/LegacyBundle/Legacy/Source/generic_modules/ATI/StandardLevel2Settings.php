<?php

$GLOBALS['FormTitle'] = _tk('ATINameTitle').' Form';

$GLOBALS["HideFields"] = array (
  'atm_notes' => true,
  'atm_staff' => true,
  'ati_location_additional' => true,
  'ati_staff' => true,
  'ati_priority' => true,
  'ati_rank' => true,
  'ati_sub_rank' => true  
);

$GLOBALS["NewPanels"] = array (
  'instances' => true,
  'module' => true,
  'assignment_details' => true
);