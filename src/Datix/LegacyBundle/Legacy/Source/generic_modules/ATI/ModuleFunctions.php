<?php

use Source\classes\Filters\Container;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

// Function EmailNewInvestigators() is called when a record is saved, but
// before the record is saved to the incidents_main table. It checks to see
// if the inc_investigator field has been edited and, if it has, sends an
// e-mail of type NewInvestigator to each new member of staff that has been
// added to this field.
function EmailStaff($data)
{
    global $ModuleDefs, $scripturl;;

    $module = 'ATI';

    // Field needed for default e-mail template text
    $data['scripturl'] = $scripturl . '?action=record&module=ATI&recordid=' . $data['recordid'];

    // Check for staff to e-mail if the Investigators field has been edited
    // and is not empty.
    if ($data['CHANGED-ati_staff'] && $data['ati_staff'] != '')
    {
        require_once 'Source/libs/Email.php';

        if($data['ati_staff'])
        {
            $Staff = explode(' ', $data['ati_staff']);
        }

        // Get the value of inc_investigator already in the database, i.e.
        // the old value.
        $OriginalStaffString = PDO_fetch('SELECT ati_staff FROM '.$ModuleDefs[$module]['VIEW'].' WHERE recordid = :recordid', array('recordid' => $data['recordid']), PDO::FETCH_COLUMN);

        if($OriginalStaffString)
        {
            $OriginalStaff = explode(' ', $OriginalStaffString);
        }

        // If the field was empty, e-mail everyone in the new list of
        // investigators
        if (empty($OriginalStaff))
        {
            $EmailStaff = $Staff;
        }
        else
        {
            // Loop through the list of investigators, adding only those
            // which were not in the old list to the array of staff to be
            // e-mailed.
            foreach($Staff as $Sta)
            {
                if (!in_array($Sta, $OriginalStaff))
                {
                    $EmailStaff[] = $Sta;
                }
            }
        }

        if ($EmailStaff != '')
        {
            // Loop through the array, sending e-mails to all those members
            // who have an e-mail address. Also, add all the User entity
            // properties to the $data array so things like fullname
            // can be used in the e-mail template if required.
            foreach ($EmailStaff as $Initials)
            {
                $Factory = new UserModelFactory();
                $User = $Factory->getMapper()->findByInitials($Initials);

                if ($User->con_email != '' && empty($User->con_dod) && bYN(GetUserParm($User->login, 'ATI_STAFF_EMAIL', 'N')))
                {
                    Registry::getInstance()->getLogger()->logEmail('Staff added to Assigned assessment template ' . $data['recordid'] . ' submitted. E-mailing user ' . $User->login . '.');

                    $emailSender = EmailSenderFactory::createEmailSender($module, 'NewStaff');
                    $emailSender->addRecipient($User);
                    $emailSender->sendEmails(array_merge($User->getVars(), $data));
                }
            }
        }
    }
}

function EmailMasterStaff($data)
{
    global $scripturl;

    // Field needed for default e-mail template text
    $data['scripturl'] = $scripturl . '?action=record&module=ATI&recordid=' . $data['recordid'];

    if ($data['rep_approved'] == 'SUBMIT')
    {
        // Get Assigned Assessment Template status and Assessment Template atm_staff fields
        $sql = '
            SELECT
                rep_approved,
                atm_staff,
                loc_name
            FROM
                asm_templates
            WHERE
                recordid = :recordid
        ';

        $TemplateData = DatixDBQuery::PDO_fetch($sql, array('recordid' => $data['recordid']));

        // Field needed for default e-mail template text
        $data['loc_name'] = $TemplateData['loc_name'];

        if ($TemplateData['rep_approved'] != 'SUBMIT')
        {
            require_once 'Source/libs/Email.php';

            $StaffString = explode(' ', $TemplateData['atm_staff']);

            if (count($StaffString) > 0)
            {
                foreach ($StaffString as $Initials)
                {
                    $Factory = new UserModelFactory();
                    $User = $Factory->getMapper()->findByInitials($Initials);

                    if ($User->con_email != '' && empty($User->con_dod) && bYN(GetUserParm($User->login, 'ATM_STAFF_EMAIL', 'N')))
                    {
                        Registry::getInstance()->getLogger()->logEmail('Assigned assessment template ' . $data['recordid'] . ' submitted. E-mailing user ' . $User->login . ' on Assessment template.');

                        $emailSender = EmailSenderFactory::createEmailSender('ATI', 'NewMasterStaff');
                        $emailSender->addRecipient($User);
                        $emailSender->sendEmails(array_merge($User->getVars(), $data));
                    }
                }
            }
        }
    }
}

function EmailAdditionalLocations($data)
{
    global $scripturl;

    // Field needed for default e-mail template text
    $data['scripturl'] = $scripturl . '?action=record&module=ATI&recordid=' . $data['recordid'];

    if ($data['CHANGED-ati_location_additional'])
    {
        // Get previous record additional locations
        $sql = '
            SELECT
                ati_location_additional
            FROM
                asm_template_instances
            WHERE
                recordid = :recordid';

        $previousAdditionalLocations = DatixDBQuery::PDO_fetch($sql, array('recordid' => Sanitize::SanitizeInt($data['recordid'])), PDO::FETCH_COLUMN);
        $locations = explode(' ', $data['ati_location_additional']);

        if ($previousAdditionalLocations == null)
        {
            //Email users that have those locations
            foreach ($locations as $location)
            {
                $usersDetails = getUserPerLocation($location);
                $data['loc_name'] = getLocationName($location);
                emailUsersPerLocation($usersDetails, $data);
            }
        }
        else
        {
            // Check if the posted locations are different from the ones that are saved on DB
            $previousAdditionalLocations = explode(' ', $previousAdditionalLocations);

            foreach ($locations as $location)
            {
                if (!in_array($location, $previousAdditionalLocations))
                {
                    $usersDetails = getUserPerLocation($location);
                    $data['loc_name'] = getLocationName($location);
                    emailUsersPerLocation($usersDetails, $data);
                }
            }
        }
    }
}

/**
 * Function that sends e-mail notification when assigned assessments, for users own 'Organisation', have been reviewed.
 *
 * @param $data Assigned assessment instance record data
 */
function EmailReviewed($data)
{
    global $scripturl;

    // Field needed for default e-mail template text
    $data['scripturl'] = $scripturl . '?action=record&module=ATI&recordid=' . $data['recordid'];

    if ($data['CHANGED-rep_approved'] && $data['rep_approved'] == 'REVIEW')
    {
        require_once 'Source/libs/Email.php';

        // Get users that have this record Organisation on their location
        $sql = "
            SELECT
                recordid,
                sta_title,
                sta_forenames,
                sta_surname,
                initials,
                email,
                fullname,
                con_dod,
                login
            FROM
                staff
            WHERE
                con_hier_location like '" . $data['ati_location'] . "'
                OR
                con_hier_location like '" . $data['ati_location'] . " %'
                OR
                con_hier_location like '% " . $data['ati_location'] . "'
                OR
                con_hier_location like '% " . $data['ati_location'] . " %'
        ";

        $Users = DatixDBQuery::PDO_fetch_all($sql);

        // Field needed for default e-mail template text
        $data['loc_name'] = getLocationName($data['ati_location']);

        $userFactory = new \src\users\model\UserModelFactory();

        foreach ($Users as $User)
        {
            if ($User['email'] != '' && empty($User['con_dod']) && bYN(GetUserParm($User['login'], 'ATI_REVIEWED_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('Assigned assessment template ' . $data['recordid'] . ' reviewed. E-mailing user ' . $User['login'] . ' that has this record Organisation on their location.');

                $emailSender = EmailSenderFactory::createEmailSender('ATI', 'ReviewedInstances');
                $recipient = $userFactory->getMapper()->find($User['recordid']);
                $emailSender->addRecipient($recipient);
                $emailSender->sendEmails($data);
            }
        }
    }
}

function getUserPerLocation($location)
{
    $sql = '
        SELECT
            recordid,
            sta_title,
            sta_forenames,
            sta_surname,
            initials,
            email,
            fullname,
            con_dod,
            login,
            con_hier_location
        FROM
            staff
        WHERE
            con_hier_location LIKE \'' . $location . ' %\'
            OR
            con_hier_location LIKE \'% ' . $location . '\'
            OR
            con_hier_location LIKE \'% ' . $location . ' %\'
            OR
            con_hier_location LIKE \'' . $location . '\'
    ';

    return DatixDBQuery::PDO_fetch_all($sql, array());
}

function emailUsersPerLocation($usersDetails, $data)
{
    require_once 'Source/libs/Email.php';

    $userFactory = new UserModelFactory();

    foreach ($usersDetails as $user)
    {
        if ($user['email'] != '' && empty($user['con_dod']) && bYN(GetUserParm($user['login'], 'ATI_LOC_EMAIL', 'N')))
        {
            Registry::getInstance()->getLogger()->logEmail('Assigned assessment template ' . $data['recordid'] . ' assigned to location. E-mailing user ' . $user['login'] . ' that has this location assigned.');

            $recipient = $userFactory->getMapper()->findById($user['recordid']);

            $emailSender = EmailSenderFactory::createEmailSender('ATI', 'NewLocation');
            $emailSender->addRecipient($recipient);
            $emailSender->sendEmails(array_merge($recipient->getVars(), $data));
        }
    }
}

function getLocationName($locationId)
{
    if (!empty($locationId))
    {
        $sql = '
            SELECT
                loc_name
            FROM
                vw_locations_main
            WHERE
                recordid = :recordid
        ';

        return DatixDBQuery::PDO_fetch($sql, array('recordid' => $locationId), PDO::FETCH_COLUMN);
    }
}

function generateInstances()
{
    global $scripturl, $yySetLocation;

    if (!(CanSeeModule('LOC') && (CanSeeModule('ATM') || CanSeeModule('ATI')) && bYN(GetParm('ATM_CAI', 'N'))))
    {
        fatal_error(_tk('no_permission_loc'), 'Notification', 'ADM', 'info_div');
    }
    
    // Initialize checkboxes if needed
    if (!isset($_SESSION['ATI']['RECORDLIST']))
    {
        $_SESSION['ATI']['RECORDLIST'] = RecordLists_RecordListShell::CreateForModule('ATI', '');
    }

    $Record = (isset($_POST['recordid']) ? $_POST['recordid'] : $_POST['assigned_assessment_instance_id']);

    $_POST['modules'][] = $Record;

    // Unflag all records before selecting this one
    $_SESSION['ATI']['RECORDLIST']->UnFlagAllRecords();
    $_SESSION['ATI']['RECORDLIST']->FlagRecord($Record);

    $LocationsFilter = Container::getFilter('LOC', true, 'ACR', true);
    $LocationsFilter->createWhereClause(array('records' => array($Record)));

    $yySetLocation = $scripturl . '?action=list&module=LOC&listtype=search&sidemenu=ACR&btn=create';
    redirectexit();
}



/**
 * Function to validate that it is not possible to generate instances for locations above (and in the same path as)
 * locations for which an assigned template has been created when generating instances from an Assigned Assessment Template
 *
 * @param $data
 * @return bool
 */
function validateExistingRecords($data)
{
    $parentLocations = getParentLocations($data['assigned_assessment_instance_id']);

    if (count(array_intersect($data['locations'], $parentLocations)) > 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * Funtion to filter a set of locations based on the location tier depth of a previously selected Assigned Assessment Template
 *
 * @param $locations
 * @param $locTierDepth
 * @return mixed
 */
function filterLocations($locations, $recordId)
{
    $parentLocations = getParentLocations($recordId);

    foreach ($locations as $id => $locArray)
    {
        if (in_array($locArray['recordid'], $parentLocations))
        {
            $locations[$id]['greyed_out'] = true;
        }
    }

    return $locations;
}

function getParentLocations ($recordid)
{
    // Get selected assigned assessment template location
    $sql = '
        SELECT
            ati_location
        FROM
            asm_templates
        WHERE
            recordid = :recordid
    ';

    $location = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid), PDO::FETCH_COLUMN);

    $locHierarchy = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);
    $parentLocations = array();

    // retireve a list of ancestors for each selected location
    $parentLocations = array_keys($locHierarchy->getAncestors($location));

    // Remove the selected assigned assessment template location
    array_pop($parentLocations);

    return $parentLocations;
}



function ValidateAdditionalLocations($data)
{
    if (isset($data['ati_location_additional']))
    {
        $currentlocations = DatixDBQuery::PDO_fetch('SELECT ati_location_additional FROM asm_template_instances WHERE recordid = :recordid',array('recordid' => $data['recordid']),PDO::FETCH_COLUMN);
        $data['ati_location_additional'] = AddRestrictedLocations($data['ati_location_additional'], $currentlocations);
    }

    return $data;
}

