<?php
  //Assessment Template Instances
$FieldDefs["ATI"] = array(
    "recordid" => array("Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_module_template_id" => array("Type" => "number",
        "Title" => "Module template ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "atm_version" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Version",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_category" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Category",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_title" => array("Type" => "string",
        "ReadOnly" => true,
        "MaxLength" => 255,
        'Width' => 70,
        'BlockFromReports' => true, //Otherwise we end up with duplicates with atm_title_code
        "Title" => "Title",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_title_code" => array(
        "Type" => "ff_select",
        "Title" => "Title",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix'),
        'NoListCol_Override' => true
    ),
    "atm_ref" => array("Type" => "string",
        "MaxLength" => 64,
        "Width" => 15,
        "ReadOnly" => true,
        "Title" => "Reference",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_description" => array("Type" => "textarea",
        "ReadOnly" => true,
        "Title" => "Description",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_notes" => array("Type" => "textarea",
        "ReadOnly" => true,
        "Title" => "Notes",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    'atm_staff' => array(
        'Type' => 'multilistbox',
        'Title' => 'Staff',
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix'),
    	'MaxLength' => 254
    ),
    "ati_location" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Organisation"),
    "ati_cycle" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Cycle year"),
    "rep_approved" => array(
        "Type"  => "ff_select",
        "Title" => "Approval status"
    ),
    "adm_year" => array("Type" => "ff_select",
        "Title" => "Assessment year"
    ),
    'ati_due_date' => array(
        'Type'     => 'date',
        'Title'    => 'Due date'
    ),
    'ati_staff' => array(
        'Type' => 'multilistbox',
        'Title' => 'Staff',
    	'MaxLength' => 8000
    ),
    'ati_location_additional' => array(
        'Type' => 'multilistbox',
        'Title' => 'Locations',
    	'MaxLength' => 8000
    ),
    'loc_name' => array(
        'Type' => 'string',
        'MaxLength' => 254,
        'Title' => 'Name'
    ),
    'loc_type' => array(
        'Type' => 'ff_select',
        'Title' => 'Location type'
    ),
    'loc_address' => array(
        'Type' => 'textarea',
        'Title' => 'Address',
        'Rows' => 7,
        'Columns' => 70
    ),
    'loc_active' => array(
        'Type' => 'yesno',
        'Title' => 'Active?',
        'BlockFromReports' => true
    ),
    'loc_tier_name' => array(
        'Type' => 'string',
        'Title' => 'Tier',
        'ReadOnly' => true
    ),
    'ati_comments' => array(
        'Type' => 'textarea',
        'Title' => 'Review comments',
        'Rows' => 7,
        'Columns' => 70
    ),
    'ati_rank' => array(
        'Type' => 'ff_select',
        'Title' => 'Initial assessment grade'
    ),
    'ati_priority' => array(
        'Type' => 'ff_select',
        'Title' => 'Assessment priority'
    ),
    'adm_percentage' => array(
        'Type' => 'number',
        'Title' => 'Compliance %',
        'ReadOnly' => true
    ),
    "ati_sub_rank" => array("Type" => "ff_select",
        "Title" => "Subscriber risk rank"),
);
?>
