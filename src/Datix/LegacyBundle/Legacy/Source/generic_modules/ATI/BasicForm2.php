<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "assessment_details" => array(
        "Title" => _tk('AMONameTitle')." details",
        "Rows" => array(
            array('Name' => 'atm_ref', 'ReadOnly' => true),
            array('Name' => 'atm_version', 'ReadOnly' => true),
            array('Name' => 'atm_category', 'ReadOnly' => true),
            array('Name' => 'atm_title', 'ReadOnly' => true),
            array('Name' => 'atm_description', 'ReadOnly' => true),
            array('Name' => 'atm_notes', 'ReadOnly' => true),
            array('Name' => 'atm_staff', 'ReadOnly' => true),
        )
    ),
    'questions' => array(
        'Title' => _tk('AQUNamesTitle'),
        'NotModes' => array('New', 'Search'),
        'Special' => 'LinkedATIQuestions',
        'NoFieldAdditions' => true,
        'Listings' => array('ATQ' => array('module' => 'ATQ', 'Label' => 'Select a listing to use for the list of '.$ModuleDefs['ATQ']['REC_NAME_PLURAL'])),
        'Rows' => array()
    ),
    "assignment_details" => array(
        "Title" => 'Assignment details',
        "NotModes" => array('New', 'Search'),
        "Rows" => array(
           'ati_location',
            array('Name' => 'ati_location_additional', 'Type' => 'custom', 'HTML' => getLocationControl($data, $FormType, $FormDesign)),
            'ati_cycle',
            'adm_year',
            'ati_due_date',
            'adm_percentage',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'ATI',
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => 'ATI',
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            )),
            'ati_comments',
            'ati_staff',
            'ati_priority',
            'ati_rank',
            'ati_sub_rank'
        )
    ),
    "instances" => array(
        "Title" => _tk('AMONameTitle').' instances',
        "NotModes" => array('New', 'Search'),
        'ControllerAction' => [
            'linkedModuleSection' => [
                'controller' => 'src\\assessments\\controllers\\AssessmentController'
            ]
        ],
        "NoFieldAdditions" => true,
        "Rows" => array(
        )
    )
);
