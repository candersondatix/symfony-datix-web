<?php

// Assesment module template instance records
$ModuleDefs['ATI'] = array(
    'MOD_ID'  => MOD_ASSESSMENT_MODULE_TEMPLATE_INSTANCES, // Module identifier. Used in licensing checks and db tables, such as udf_groups. See Source/libs/constants.php for value of constant
    'CODE'    => 'ATI', // not sure what this is for
    'GENERIC' => true,
    'GENERIC_FOLDER' => 'ATI',
    'NAME'    => _tk('ATINamesTitle'),
    'REC_NAME'  => 'assigned assessment',
    'REC_NAME_PLURAL' => 'assigned assessment templates',
	'REC_NAME_TITLE'  => 'Assigned Assessment',
	'REC_NAME_PLURAL_TITLE' => 'Assigned Assessment Templates',
    'TABLE' => 'asm_template_instances',
    'VIEW' => 'asm_templates',
    'TEMPLATE' => '',
    'ACTION' => 'record&module=ATI',

    'MODULE_GROUP' => 'ACR',
    'APPROVAL_LEVELS' => false,
    'USES_APPROVAL_STATUSES' => true,
    'NO_REP_APPROVED' => true,
    'FORMCODE' => 'ATI',
    'PERM_GLOBAL' => 'ATI_PERMS',
    'ICON' => '',
    'FK' => '',
    'FIELD_ARRAY' => array(
        'asm_module_template_id',
        'ati_location',
        'ati_location_additional',
        'ati_cycle',
        'rep_approved',
        'adm_year',
        'ati_due_date',
		'ati_staff',
        'ati_comments',
        'ati_rank',
		'ati_sub_rank',
        'ati_priority'
    ),
    'VIEW_FIELD_ARRAY' => array(
        'asm_template_instance_id',
        'asm_module_template_id',
        'ati_location',
        'ati_location_additional',
        'atm_version',
        'atm_category',
        'atm_title',
        'atm_title_code',
        'atm_description',
        'atm_notes',
        'ati_cycle',
        'rep_approved',
        'adm_year',
        'ati_due_date',
    	'ati_staff',
    	'atm_ref',
        'atm_staff',
        'loc_name',
        'loc_type',
        'loc_address',
        'loc_active',
        'loc_tier_name',
        'ati_comments',
        'ati_rank',
		'ati_sub_rank',
        'ati_priority',
        'adm_percentage'
    ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'ATI', 'HIDECODE' => true)
    ),
    'FORMCODE1' => 'ATI',
    'FORM_DESIGN_ARRAY_FILE' => 'UserATIForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserATISettings',
    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,
    'LISTING_FILTERS' => true,
    'BREADCRUMBS' => false,
    'LISTING_WHERE' => ' recordid IS NOT NULL',
    'HAS_HOMESCREEN' => false,   // Variable to decide if this module will appear as a Homescreen
    'EMAIL_AFTER_SAVE_FUNCTIONS' => array(
        'EmailStaff',
        'EmailMasterStaff',
        'EmailAdditionalLocations',
        'EmailReviewed'
    ),
    'BEFORE_SAVE_FUNCTIONS' => array(
        'ValidateAdditionalLocations'
    ),
    'EXTRA_FORM_ACTIONS' => array(
        'generateInstances' => array(
            'title'      => 'generate_assessment_instances',
            'sourceFile' => 'Source/generic_modules/ATI/ModuleFunctions.php',
            'callback'   => 'generateInstances',
            'condition'  => bYN(GetParm('ATM_CAI', 'N')),
            'js'         => '', // using js file instead of inline js, see CUSTOM_JS below
            'action'     => 'CREATE'
        )
    ),
    'CUSTOM_JS' => '<script type="text/javascript" src="js_functions/ati_instances' . ($MinifierDisabled ? '' : '.min') . '.js"></script>',
    'NO_NAV_ARROWS' => true,
    'COMBO_LINK_WHERE' => "AND fmt_field NOT IN
        ('rep_approved', 'loc_type',
        'ati_location_additional', 'ati_location', 'ati_staff',
        'atm_version', 'atm_staff', 'atm_title_code', 'atm_category')"
);

if(bYN(GetParm('ASM_MANAGE_CYCLES', 'N')))
{
    $ModuleDefs['ATI']['LISTING_EXTRA_LINKS'] = array(array('label' => _tk('ATIRedistribute'), 'link' => 'action=asmredistributetemplates'));
}
