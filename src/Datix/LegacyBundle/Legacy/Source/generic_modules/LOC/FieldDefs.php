<?php
// Locations
$FieldDefs["LOC"] = array(
    "recordid" => array("Type" => "number",
        "IdentityCol" => true,
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5),
    "loc_name" => array("Type" => "string",
        "MaxLength" => 254,
        "Title" => "Name"),
    "loc_type" => array("Type" => "ff_select",
        "Title" => "Location type" ),
    "loc_address" => array("Type" => "textarea",
        "Title" => "Address",
        "Rows" => 7,
        "Columns" => 70),
    "loc_tier_name" => array("Type" => "string",
        "Title" => "Tier",
        "ReadOnly" => true),
    "loc_active" => array("Type" => "yesno",
        "Title" => "Active?"),
        );
?>
