<?php
$ModuleDefs['LOC'] = array(
    'MOD_ID' => MOD_LOCATIONS,
    'CODE' => 'LOC',
    'GENERIC' => true,
    'GENERIC_FOLDER' => 'LOC',
    'NAME' => _tk("LOCNamesTitle"),
    'REC_NAME' => _tk("LOCName"),
    'REC_NAME_PLURAL' => _tk("LOCNames"),
	'REC_NAME_TITLE' => _tk("LOCNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("LOCNamesTitle"),
    'TABLE' => 'locations_main',
    'VIEW' => 'vw_locations_main',
  //  'TEMPLATE' => 'cqc_template_outcomes',
    'ACTION' => 'record&module=LOC',

    'APPROVAL_LEVELS' => false,
    'USES_APPROVAL_STATUSES' => false,
    'NO_REP_APPROVED' => true,

    'FORMCODE' => 'LOC',
    'PERM_GLOBAL' => 'LOC_PERMS',
    'ICON' => 'icons/icon_CQO.png',
    'FK' => 'loc_id',

    'DEFAULT_ORDER' => 'loc_name',
    'DEFAULT_ORDER_DIRECTION' => 'ASC',

    'FIELD_ARRAY' => array('loc_name', 'loc_type', 'loc_address', 'loc_active'),

    'VIEW_FIELD_ARRAY' => array('loc_name', 'loc_type', 'loc_address', 'loc_tier_name', 'loc_tier_depth', 'loc_active'),

    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'LOC', 'HIDECODE' => true)
        ),
    'FORMCODE1' => 'LOC',

    'FORM_DESIGN_ARRAY_FILE' => 'UserLOCForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserLOCSettings',

    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,

    'LISTING_FILTERS' => true,
   // 'BREADCRUMB_FIELD' => 'loc_name',
   // 'BREADCRUMB_LEVEL' => 0,

    'LISTING_WHERE' => ' recordid IS NOT NULL AND (loc_active != \'N\' OR  loc_active is null) AND (loc_active_inherit != \'N\' OR  loc_active_inherit is null)',
    'HAS_HOMESCREEN' => false,   // Variable to decide if this module will appear as a Homescreen
    'NO_NAV_ARROWS' => true,
    'BROWSELIST_PRE_RENDER' => array('ASM_Location', 'filterLocations')
);
