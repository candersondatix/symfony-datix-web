<?php

$LowestTier = DatixDBQuery::PDO_fetch('SELECT MAX(loc_tier_depth) FROM vw_locations_main', array(), PDO::FETCH_COLUMN);

$FormArray = array(
    'Parameters' => array(
        'Panels' => true,
        'Condition' => false),
    'location' => array(
        'Title' => 'Location Details',
        'Rows' => array(
            'recordid',
            'loc_name',
            'loc_tier_name',
            'loc_type',
            'loc_address',
            array('Name' => 'loc_active', 'Condition' => $data['loc_tier_depth'] == $LowestTier)
        )
    ),
    'outcomes' => array(
        'Title' => 'Outcomes',
        'Condition' => (ModIsLicensed('CQO')),
        'NotModes' => array('New', 'Search'),
        'Function' => array('CQC_Location', 'ListOutcomes'),
    ),
    'assessments' => array(
        'Title' => _tk('AMONamesTitle'),
        'Condition' => (ModIsLicensed('AMO')),
        'NotModes' => array('New', 'Search'),
        'NoFieldAdditions' => true,
        'ControllerAction' => array(
            'ListModules' => array(
                'controller' => 'src\\assessments\\controllers\\AssessmentController'
            )
        ),
        'Listings' => array('AMO' => array('module' => 'AMO')),
        'LinkedForms' => array('assessments' => array('module' => 'AMO'))
    ),
);
