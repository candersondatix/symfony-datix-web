<?php

// Field definitions for assessment modules module
$FieldDefs["AMO"] = array(
    "recordid" => array("Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_template_instance_id" => array("Type" => "number",
        "Title" => "Assigned assessment ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "asm_module_template_id" => array("Type" => "number",
        "Title" => "Assessment template ID",
        "ReadOnly" => true,
        "Width" => 5,
        "NoListCol_Override" => true),
    "atm_version" => array("Type" => "ff_select",
        "Title" => "Version",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_category" => array("Type" => "ff_select",
        "Title" => "Category",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_title_code" => array(
        "Type" => "ff_select",
        "Title" => "Title",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix'),
        'NoListCol_Override' => true
    ),
    "atm_title" => array("Type" => "string",
        "MaxLength" => 255,
        'Width' => 70,
        'BlockFromReports' => true, //Otherwise we end up with duplicates with atm_title_code
        "Title" => "Title",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_ref" => array("Type" => "string",
        "MaxLength" => 64,
        "Width" => 15,
        "Title" => "Reference",
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_description" => array("Type" => "textarea",
        "Title" => "Description",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "atm_notes" => array("Type" => "textarea",
        "Title" => "Notes",
        "Rows" => 7,
        "Columns" => 70,
        "SubmoduleSuffix" => _tk('ATMSubmoduleSuffix')),
    "adm_year" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Assessment year",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')),
    "adm_location" => array("Type" => "ff_select",
        "Title" => "Site"),
    "adm_coordinator" => array("Type" => "ff_select",
        "Title" => "Coordinator"),
    "adm_sub_coordinator" => array("Type" => "ff_select",
        "Title" => "Sub-coordinator"),
    "adm_end_user" => array("Type" => "ff_select",
        "Title" => "End user"),
    "ati_sub_rank" => array("Type" => "ff_select",
        "Title" => "Post-review assessment grade",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix'),
    ),
    "adm_comments" => array(
        "Type"    => "textarea",
        "Title"   => "Comments",
        "Rows"    => 7,
        "Columns" => 70
    ),
    "adm_compliance" => array("Type" => "ff_select",
        "Title" => "Compliance level",
        "ReadOnly" => true),
    "adm_percentage" => array("Type" => "number",
        "Title" => "Compliance %",
        "ReadOnly" => true),

    "ati_location" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Organisation",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')),
    "ati_cycle" => array("Type" => "ff_select",
        "ReadOnly" => true,
        "Title" => "Cycle year",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')),
    "ati_due_date" => array(
        "Type"     => "date",
        'ReadOnly' => true,
        "Title"    => "Due date",
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')
    ),
    "rep_approved" => array(
        'Type'     => 'ff_select',
        'Title'    => "Status"),
    'ati_location_additional' => array(
        'Type' => 'multilistbox',
        'Title' => 'Locations',
    	'MaxLength' => 8000,
        "SubmoduleSuffix" => _tk('ATISubmoduleSuffix')
    ),
    'loc_name' => array(
        'Type' => 'string',
        'MaxLength' => 254,
        'Title' => 'Name'
    ),
    'loc_type' => array(
        'Type' => 'ff_select',
        'Title' => 'Location type'
    ),
    'loc_address' => array(
        'Type' => 'textarea',
        'Title' => 'Address',
        'Rows' => 7,
        'Columns' => 70
    ),
    'loc_active' => array(
        'Type' => 'yesno',
        'Title' => 'Active?',
        'BlockFromReports' => true,
    ),
    'loc_tier_name' => array(
        'Type' => 'string',
        'Title' => 'Tier',
        'ReadOnly' => true
    ),
    'ati_rank' => array(
        'Type' => 'ff_select',
        'Title' => 'Risk rank',
        'SubmoduleSuffix' => _tk('ATISubmoduleSuffix'),
        'ReadOnly' => true
    ),
    'ati_priority' => array(
        'Type' => 'ff_select',
        'Title' => 'Assessment priority',
        'SubmoduleSuffix' => _tk('ATISubmoduleSuffix'),
        'ReadOnly' => true
    ),
);
?>
