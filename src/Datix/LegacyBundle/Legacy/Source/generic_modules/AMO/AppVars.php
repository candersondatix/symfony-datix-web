<?php

// Assesment module instance records
$ModuleDefs['AMO'] = array(
    'MOD_ID'  => MOD_ASSESSMENT_MODULES, // Module identifier. Used in licensing checks and db tables, such as udf_groups. See Source/libs/constants.php for value of constant
    'CODE'    => 'AMO', // not sure what this is for
    'GENERIC' => true,
    'GENERIC_FOLDER' => 'AMO',
    'NAME'    => _tk("AMONamesTitle"),
    'MENU_NAME' => _tk('accreditation'),
    'REC_NAME'  => _tk("AMOName"),
    'REC_NAME_PLURAL' => _tk("AMONames"),
	'REC_NAME_TITLE'  => _tk("AMONameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("AMONamesTitle"),
    'TABLE' => 'asm_data_modules',
    'VIEW' => 'asm_modules',
    'TEMPLATE' => '',
    'ACTION' => 'record&module=AMO',
    'FIELD_NAMES' => array(
        'NAME' => 'atm_title',
        'HANDLER' => '',
        'MANAGER' => '',
        'REF' => ''
    ),
    'MODULE_GROUP' => 'ACR',
    'APPROVAL_LEVELS' => true,
    'USES_APPROVAL_STATUSES' => true,
    'NO_REP_APPROVED' => true,
    'FORMCODE' => 'AMO',
    'PERM_GLOBAL' => 'AMO_PERMS',
    'ICON' => '',
    'FK' => '',
    'FIELD_ARRAY' => array(
        'adm_location',
        'adm_coordinator',
        'adm_sub_coordinator',
        'adm_end_user',
        'adm_comments',
        'rep_approved'
    ),
    'VIEW_FIELD_ARRAY' => array(
        'atm_version',
        'atm_category',
        'atm_title',
        'atm_title_code',
        'atm_ref',
        'atm_description',
        'atm_notes',
        'adm_year',
        'adm_location',
        'adm_coordinator',
        'adm_sub_coordinator',
        'adm_end_user',
        'ati_sub_rank',
        'adm_comments',
        'adm_compliance',
        'adm_percentage',
        'ati_cycle',
        'ati_location',
        'ati_location_additional',
        'rep_approved',
        'ati_due_date',
        'loc_name',
        'loc_type',
        'loc_address',
        'loc_active',
        'loc_tier_name',
        'ati_rank',
        'ati_priority'
    ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'AMO', 'HIDECODE' => true)
    ),
    'FORMCODE1' => 'AMO',
    'FORM_DESIGN_ARRAY_FILE' => 'UserAMOForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserAMOSettings',
    'ACTION_LINK_LIST_FIELDS' => array(
        array('field' => 'recordid', 'width' => '3'),
        array('field' => 'atm_version', 'width' => '6'),
        array('field' => 'atm_category', 'width' => '6'),
        array('field' => 'atm_title', 'width' => '6'),
        array('field' => 'adm_year', 'width' => '6'),
        array('field' => 'rep_approved', 'width' => '6'),
        array('field' => 'adm_compliance', 'width' => '6'),
        array('field' => 'adm_location', 'width' => '6'),
        array('field' => 'adm_coordinator', 'width' => '6')
    ),
    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,
    'LISTING_FILTERS' => true,
    'BREADCRUMBS' => false,
    'LISTING_WHERE' => ' recordid IS NOT NULL',
    'EMAIL_AFTER_SAVE_FUNCTIONS' => array(
        'EmailCoordinator',
        'EmailStaff'
    ),
    'CUSTOM_ACCESS_LEVEL_FUNCTION' => 'GetAssessmentAccessLevel',
    'NO_NAV_ARROWS' => true,
    'COMBO_LINK_WHERE' => "AND fmt_field NOT IN
        ('rep_approved', 'loc_type', 'ati_location_additional', 'ati_location', 'atm_title_code', 'ati_sub_rank',
        'ati_cycle', 'ati_priority', 'ati_rank', 'atm_category', 'atm_version',
        'adm_coordinator', 'adm_sub_coordinator', 'adm_location', 'adm_end_user', 'adm_year')"
);
