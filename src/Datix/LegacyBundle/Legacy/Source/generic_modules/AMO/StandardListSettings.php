<?php
$list_columns_standard = array(
    "ati_location" => array(
        'width' => '3'),
    "adm_location" => array(
        'width' => '3'),
    "ati_cycle" => array(
	    'width' => '2'),
    "adm_year" => array(
        'width' => '2'),
    "atm_version" => array(
        'width' => '2'),
    "atm_category" => array(
        'width' => '3'),
    "atm_title" => array(
        'width' => '10'),
    "adm_percentage" => array(
        'width' => '2'),
    "rep_approved" => array(
        'width' => '2'),
);