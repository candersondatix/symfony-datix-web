<?php

$FormArray = array(
    'Parameters' => array(
        'Panels' => true,
        'Condition' => false
    ),
    'module' => array(
        'Title' => _tk('AMONameTitle').' Details',
        'Rows' => array(
            array('Name' => 'atm_ref', 'ReadOnly' => true),
            array('Name' => 'atm_version', 'ReadOnly' => true),
            array('Name' => 'atm_category', 'ReadOnly' => true),
            array('Name' => 'atm_title', 'ReadOnly' => true),
            array('Name' => 'atm_description', 'ReadOnly' => true),
            array('Name' => 'atm_notes', 'ReadOnly' => true),
            'ati_location',
            array('Name' => 'ati_location_additional', 'ReadOnly' => true, 'Type' => 'custom', 'HTML' => getLocationControl($data, 'ReadOnly'), 'Condition' => $FormType != 'Search'),
            'ati_cycle',
            'adm_year',
            'ati_due_date',
            array('Name' => 'ati_priority', 'ReadOnly' => true),
            array('Name' => 'ati_rank', 'ReadOnly' => true),
            array('Name' => 'ati_sub_rank', 'ReadOnly' => true),
            array('Name' => 'adm_location', 'Type' => 'custom', 'HTML' => getLocationParentage($data['adm_location']), 'Condition' => $FormType != 'Search', 'ReadOnly' => true),
            'adm_coordinator',
            'adm_sub_coordinator',
            'adm_end_user',
            'adm_compliance',
            'adm_percentage',
            'adm_comments',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'AMO',
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => 'AMO',
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            ))
        )
    ),
    'asm_data_questions' => array(
        'Title' => _tk('AQUNamesTitle'),
        'NotModes' => array('New', 'Search'),
        'Special' => 'LinkedASMQuestions',
        'NoFieldAdditions' => true,
        'LinkedForms' => array('asm_data_questions' => array('module' => 'AQU')),
        'Listings' => array('AQU' => array('module' => 'AQU', 'Label' => 'Select a listing to use for the list of '.$ModuleDefs['AQU']['REC_NAME_PLURAL'])),
        'Rows' => array()
    ),
    'linked_actions' => array(
        'Title' => 'Actions',
        'NoFieldAdditions' => true,
        'Special' => 'LinkedActions',
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Listings' => array('ACT' => array('module' => 'ACT', 'Label' => 'Select a listing to use for the list of '.$ModuleDefs['ACT']['REC_NAME_PLURAL'])),
        'NotModes' => array('New', 'Search'),
        'Rows' => array()
    ),
);