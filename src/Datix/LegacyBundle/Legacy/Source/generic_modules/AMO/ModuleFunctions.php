<?php

use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

/**
 * Function that sends e-mail notifications when assessment instances are 'submitted' on which users are identified
 * as the 'Coordinator'.
 *
 * @param $data Assessment instance record data
 */
function EmailCoordinator($data)
{
    global $scripturl;

    // Field needed for default e-mail template text
    $data['scripturl'] = $scripturl . '?action=record&module=AMO&recordid=' . $data['recordid'];

    $module = 'AMO';

    // Get location name
    $sql = '
        SELECT
            vw_locations_main.loc_name
        FROM
            asm_modules,
            vw_locations_main
        WHERE
            asm_modules.adm_location = vw_locations_main.recordid
            AND
            asm_modules.recordid = :recordid
    ';

    // Field needed for default e-mail template text
    $data['loc_name'] = DatixDBQuery::PDO_fetch($sql, array('recordid' => $data['recordid']), PDO::FETCH_COLUMN);

    if ($data['CHANGED-rep_approved'] && $data['rep_approved'] != '')
    {
        require_once 'Source/libs/Email.php';

        if($data['rep_approved'] == 'SUBMIT' && $data['rep_approved_old'] != 'SUBMIT' && !empty($data['adm_coordinator']))
        {
            $Factory = new UserModelFactory();
            $Coordinator = $Factory->getMapper()->findByInitials($data['adm_coordinator']);

            if ($Coordinator->con_email != '' && empty($Coordinator->con_dod) && bYN(GetUserParm($Coordinator->login, 'AMO_SUBMIT_INSTANCE_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('Assessment instance ' . $data['recordid'] . ' submitted. E-mailing Coordinator.');

                $emailSender = EmailSenderFactory::createEmailSender($module, 'SubmittedAssessmentInstance');
                $emailSender->addRecipient($Coordinator);
                $emailSender->sendEmails(array_merge($Coordinator->getVars(), $data));
            }
        }
    }
}

/**
 * Function that sends e-mail notifications when users are identified in the 'Sub-Coordinator' or 'End user' fields
 * on an assessment instance.
 *
 * @param $data Assessment instance record data
 */
function EmailStaff($data)
{
    global $ModuleDefs, $scripturl;

    $module = 'AMO';

    // Field needed for default e-mail template text
    $data['scripturl'] = $scripturl . '?action=record&module=AMO&recordid=' . $data['recordid'];

    if (($data['CHANGED-adm_sub_coordinator'] && $data['adm_sub_coordinator'] != '') || ($data['CHANGED-adm_end_user'] && $data['adm_end_user'] != ''))
    {
        require_once 'Source/libs/Email.php';

        if ($data['CHANGED-adm_sub_coordinator'] && $data['adm_sub_coordinator'] != '')
        {
            $Field = 'adm_sub_coordinator';
        }
        elseif($data['CHANGED-adm_end_user'] && $data['adm_end_user'] != '')
        {
            $Field = 'adm_end_user';
        }

        $sql = '
            SELECT
                ' . $Field . '
            FROM
                ' . $ModuleDefs[$module]['VIEW'] . '
            WHERE
                recordid = :recordid
        ';

        $OriginalStaff = PDO_fetch($sql, array('recordid' => $data['recordid']), PDO::FETCH_COLUMN);

        if (empty($OriginalStaff) || $OriginalStaff != $data[$Field])
        {
            $Factory = new UserModelFactory();
            $User = $Factory->getMapper()->findByInitials($data[$Field]);

            if ($User->con_email != '' && empty($User->con_dod) && bYN(GetUserParm($User->login, 'AMO_STAFF_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('Assessment instance ' . $data['recordid'] . ' submitted. E-mailing Sub-Coordinator or End user.');

                $emailSender = EmailSenderFactory::createEmailSender($module, 'NewStaff');
                $emailSender->addRecipient($User);
                $emailSender->sendEmails(array_merge($User->getVars(), $data));
            }
        }
    }
}