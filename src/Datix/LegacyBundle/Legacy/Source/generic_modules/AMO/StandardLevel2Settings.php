<?php

$GLOBALS['FormTitle'] = _tk('AMONameTitle').' Form';
$GLOBALS["HideFields"] = array (
  'atm_notes' => true,
  'ati_location_additional' => true,
  'ati_priority' => true,
  'ati_rank'=> true,
  'ati_sub_rank' => true,
  'adm_sub_coordinator' => true,
  'adm_comments' => true,
  'linked_actions' => true,
);

$GLOBALS["NewPanels"] = array (
  'linked_actions' => true,
);