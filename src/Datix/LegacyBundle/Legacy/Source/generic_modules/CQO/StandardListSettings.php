<?php

$LowestTier = DatixDBQuery::PDO_fetch('SELECT MAX(loc_tier_depth) FROM vw_locations_main', array(), PDO::FETCH_COLUMN);

$list_columns_standard = array(
    "cto_ref" => array(
        'width' => '10'),
    "cto_desc" => array(
        'width' => '10'),
    "tier_".$LowestTier => array(
        'width' => '10'),
    "cdo_manager" => array(
	    'width' => '6'),
    "cdo_handlers" => array(
        'width' => '6'),
    "cdo_status" => array(
        'width' => '6'),
);

//if there's no tiers set up, adding this field will cause an error.
if($LowestTier == '')
{
    unset($list_columns_standard['tier_']);
}

?>
