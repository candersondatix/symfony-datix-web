<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "outcome" => array(
        "Title" => "Outcome Details",
        "Rows" => array(
            array('Name' => "cto_ref", 'ReadOnly' => true),
            array('Name' => "cto_desc_short", 'ReadOnly' => true),
            array('Name' => "cto_desc", 'ReadOnly' => true),
            array('Name' => "cto_theme", 'ReadOnly' => true),
            array('Name' => "cto_guidance", 'ReadOnly' => true),
            array('Name' => "cto_dpub", 'ReadOnly' => true),
            array('Name' => "cdo_location", 'ReadOnly' => $FormType != 'Search', 'CustomCodes' => CQC_Location::GetLowestLocations()),
            "cdo_manager",
            "cdo_handlers",
            array('Name' => "cdo_status_input", 'Condition' => !in_array(GetParm('CQC_ROLLUP_TYPE', 'NONE'), array('AVE', 'WORST'))),
            array('Name' => "cdo_status", 'ReadOnly' => true, 'Condition' => in_array(GetParm('CQC_ROLLUP_TYPE', 'NONE'), array('AVE', 'WORST'))),
            'cdo_dlastreview',
            'cdo_dnextreview',
        )
    ),
    "prompts" => array(
        "Title" => "Prompts",
        "NotModes" => array('New', 'Search'),
        "Special" => "LinkedCQCPrompts",
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "evidence" => array(
        "Title" => "Evidence" . ($libCount?" ($libCount)":""),
        "NoFieldAdditions" => true,
        "Function" => array('CQC_Outcome', 'listEvidence'),
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $act['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('ACT', $act['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        "NoFieldAdditions" => true,
        'Special' => 'ProgressNotes',
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "linked_records" => array(
        "Title" => _tk('linked_records'),
        "NoFieldAdditions" => true,
        "Special" => "LinkedRecords",
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
);
