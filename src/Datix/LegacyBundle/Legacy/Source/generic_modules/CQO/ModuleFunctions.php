<?php
function GetBreadCrumbs($data)
{
    global $ModuleDefs, $scripturl ;

    $trail = new BreadCrumbs();

    $trail->add(getFieldDataFromID($data['cdo_location'], 'loc_name', 'locations_main'), "$scripturl?action=record&module=LOC&fromsearch=1&recordid=" . $data['cdo_location'], 0); 
    $trail->add($data['cto_desc_short'], "$scripturl?action=record&module=CQO&fromsearch=1&recordid=" . $data['recordid'], 1);
    
    echo $trail->output();
}

function deleteOutcome()
{
    global $yySetLocation, $scripturl;
    
    if (!IsFullAdmin()) {
        AddSessionMessage('ERROR', _tk('no_permissions'));
        $yySetLocation = "$scripturl?action=record&module=CQO&recordid=$recordId&fromsearch=1";
    }
    
    $recordId = Sanitize::SanitizeInt($_POST['recordid']);
    $db = new DatixDBQuery('');
    
    $db->setSQL('SELECT recordid FROM cqc_data_prompts WHERE cqc_outcome_id = :recordid');
    $db->prepareAndExecute(array('recordid' => $recordId));
    $promptIds = $db->fetchAll(PDO::FETCH_COLUMN);
    $inClause = 'IN (' . \UnicodeString::trim(str_repeat('?,', count($promptIds)), ',') . ')';
    
    $db->beginTransaction();
    
    try {
        $db->setSQL('DELETE FROM cqc_data_subprompts WHERE cqc_prompt_id ' . $inClause);
        if (!$db->prepareAndExecute($promptIds))
        {
            throw new Exception('Sub-prompt delete failed');
        }
        
        $db->setSQL('DELETE FROM cqc_data_prompts WHERE cqc_outcome_id = :recordid');
        if (!$db->prepareAndExecute(array('recordid' => $recordId)))
        {
            throw new Exception('Prompt delete failed');
        }
        
        $db->setSQL('DELETE FROM cqc_data_outcomes WHERE recordid = :recordid');
        if (!$db->prepareAndExecute(array('recordid' => $recordId)))
        {
            throw new Exception('Outcome delete failed');
        }
    } catch (Exception $e) {
        $db->rollback();
        AddSessionMessage('ERROR', _tk('outcome_delete_fail'));
        $yySetLocation = "$scripturl?action=record&module=CQO&recordid=$recordId&fromsearch=1";
        redirectexit();
    }
    
    $db->commit();
    
    AddSessionMessage('INFO', _tk('outcome_deleted'));
    $yySetLocation = "$scripturl?action=list&module=CQO&listtype=all";
    redirectexit();
}