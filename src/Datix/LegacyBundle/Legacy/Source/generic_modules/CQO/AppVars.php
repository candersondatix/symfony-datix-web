<?php
$ModuleDefs['CQO'] = array(
    'MOD_ID' => MOD_CQC_OUTCOMES,
    'CODE' => 'CQO',
    'GENERIC' => true,
    'GENERIC_FOLDER' => 'CQO',
    'NAME' => 'CQC Outcomes',
    'MENU_NAME' => _tk('CQCNamesTitle'),
    'REC_NAME' => 'outcome',
    'REC_NAME_PLURAL' => 'outcomes',
	'REC_NAME' => 'Outcome',
	'REC_NAME_PLURAL' => 'Outcomes',
    'TABLE' => 'cqc_data_outcomes',
    'VIEW' => 'cqc_outcomes',
    'TEMPLATE' => 'cqc_template_outcomes',
    'ACTION' => 'record&module=CQO',
    'OURREF' => 'cto_ref',
    'FIELD_NAMES' => array(
        'NAME' => 'cto_desc_short',
        'HANDLER' => 'cdo_handlers',
        'MANAGER' => 'cdo_mangers',
        'REF' => 'cto_ref',
    ),
    'MODULE_GROUP' => 'CQC',
    'APPROVAL_LEVELS' => false,
    'USES_APPROVAL_STATUSES' => false,
    'NO_REP_APPROVED' => true,
    'FORMCODE' => 'CQO',
    'PERM_GLOBAL' => 'CQO_PERMS',
    'ICON' => 'icons/icon_CQO.png',
    'FK' => 'cqo_id',
    'FIELD_ARRAY' => array(
        'cqc_outcome_template_id',
        'cdo_location',
        'cdo_manager',
        'cdo_handlers',
        'cdo_dlastreview',
        'cdo_dnextreview',
        'cdo_status_input'
    ),
    'VIEW_FIELD_ARRAY' => array(
        'cqc_outcome_id',
        'cqc_outcome_template_id',
        'cdo_location',
        'cdo_manager',
        'cdo_handlers',
        'cdo_status',
        'cdo_status_input',
        'cdo_dlastreview',
        'cdo_dnextreview',
        'cto_ref',
        'cto_desc_short',
        'cto_desc',
        'cto_theme',
        'cto_guidance',
        'cto_dpub',
        'cto_active'
    ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'CQO', 'HIDECODE' => true)
    ),
    'FORMCODE1' => 'CQO',
    'FORM_DESIGN_ARRAY_FILE' => 'UserCQOForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserCQOSettings',
    'LINKED_CONTACTS' => false,
    'NOTEPAD' => false,
    'LISTING_FILTERS' => true,
    'BREADCRUMBS' => true,
    'LISTING_WHERE' => ' recordid IS NOT NULL',
    'EXTRA_RECORD_DATA_FUNCTION_INCLUDES' => array(
        'Source/generic_modules/CQO/ModuleFunctions.php'
    ),
    'EXTRA_FORM_ACTIONS' => array(
        'deleteOutcome' => array(
            'title'      => 'outcome_delete', // title is used as lookup for translation
            'sourceFile' => 'Source/generic_modules/CQO/ModuleFunctions.php',
            'callback'   => 'deleteOutcome',
            'condition'  => IsFullAdmin(),
            'js'         => '', // using js file instead of inline js, see CUSTOM_JS below
            'action'     => 'DELETE'
        )
    ),
    'BLOCK_LINKS' => array('CQP', 'CQS'),
    'CUSTOM_JS' => '<script type="text/javascript" src="js_functions/cqc_outcome' . ($MinifierDisabled ? '' : '.min') . '.js"></script>',
    'COMBO_LINK_WHERE' => "AND fmt_field NOT IN ('cto_ref', 'cdo_status', 'cto_theme')",

    'LOCAL_HELP' => 'WebHelp/index.html#CQCStandards/c_dx_cqc_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/CQCStandards/c_dx_cqc_guide.xml',

);