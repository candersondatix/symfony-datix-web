<?php

$GLOBALS['FormTitle'] = 'CQC Outcome Form';
$GLOBALS["NewPanels"] = array (
    'progress_notes' => true,
    'linked_actions' => true,
);
$GLOBALS["HideFields"] = array(
    'action_chains' => true,
);
