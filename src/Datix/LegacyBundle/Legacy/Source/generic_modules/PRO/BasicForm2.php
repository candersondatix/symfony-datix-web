<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => "True",
        "Condition" => false
    ),
    "element" => array(
        "Title" => "Element Details",
        "Type" => "Panel",
        "NewPanel" => true,
        "Function" => 'ElementDetailsSection',
        "NoFieldAdditions" => true,
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "details" => array(
        "Title" => "Compliance for Standard",
        "Rows" => array(
            array("Name" => "pro_code", "ReadOnly" => $ReadOnly),
            array("Name" => "pro_descr", "ReadOnly" => $ReadOnly),
            array("Name" => "pro_manager", "ReadOnly" => $ReadOnly),
            array("Name" => "pro_handler", "ReadOnly" => $ReadOnly),
            array("Name" => "pro_listorder", "ReadOnly" => $ReadOnly),
            array("Name" => "pro_level", "ReadOnly" => $ReadOnly),
            array("Name" => "pro_max_score", "ReadOnly" => $ReadOnly),
            array("Name" => "pro_status", "ReadOnly" => true, 'Type' => 'formfield', 'FormField' => $statusFieldObj, 'Condition' => ($FormMode == 'Design' || $Perms)),
            array("Name" => "pro_act_score", "ReadOnly" => $ReadOnly, 'Condition' => ($FormMode == 'Design' || $Perms)),
            array("Name" => "pro_comments", "ReadOnly" => $ReadOnly, 'Condition' => ($FormMode == 'Design' || $Perms))
        )
    ),
    "evidence" => array(
        "Title" => "Evidence",
        "Function" => 'ListEvidence',
        'Listings' => array('evidence' => array('module' => 'LIB')),
        "NoFieldAdditions" => true,
        "Condition" => ($FormMode == 'Design' || ($pro["recordid"] && $Perms))
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        "NotModes" => array("New", "Search"),
        "Special" => "LinkedActions"
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
);

?>
