<?php

// Standards
$ModuleDefs['PRO'] = array(
    'MOD_ID' => MOD_PROMPTS,
    'CODE' => 'PRO',
    'NAME' => _tk("mod_prompt_title"),
    'REC_NAME' => _tk("PROName"),
    'REC_NAME_PLURAL' => _tk("PRONames"),
	'REC_NAME_TITLE' => _tk("PRONameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("PRONamesTitle"),
    'FK' => 'pro_id',
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'PRO',
    'NO_GENERIC_FORM' => true,
    'NO_REP_APPROVED' => true,
    'NO_MAIN_MENU' => true,
    'OURREF' => 'pro_code',
    'TABLE' => 'standard_prompts',
    'ACTION' => 'prompt',
    'PERM_GLOBAL' => 'STN_PERMS',
    'NO_SECURITY_LEVEL' => true, //uses another module's sec level
    'MAIN_URL' => 'action=prompt',
    'MAIN_URL_OVERRIDE' => true,
   // 'LIBPATH' => 'Source/standards',
    'MODULE_GROUP' => 'STD',
    'FIELD_NAMES' => array(
        "NAME" => "pro_code",
        "HANDLER" => "pro_handler",
        ),
    'FORMS' => array(
        2 => array('LEVEL' => 2, 'CODE'=>'PRO', 'HIDECODE' => true)
        ),
    'FORM_DESIGN_ARRAY_FILE' => 'UserPROForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserPROSettings',
    'FIELD_ARRAY' => array('pro_code', 'pro_descr', 'pro_level', 'pro_max_score', 'pro_status', 'pro_comments', 'pro_act_score', 'pro_listorder',
                            'pro_manager', 'pro_handler'),

    'ICON' => 'icons/icon_STN.png',
    'ACTION_LINK_LIST_FIELDS' => array(
        array('field' => 'recordid', 'width' => '5'),
        array('field' => 'pro_code', 'width' => '10'),
        array('field' => 'pro_descr', 'width' => '85')
        ),

    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
);
