<?php
/**
* @desc Called from {@link GetExtraData()} for PALS module. Populates pal_subjects_linked field with correct data based
* on whether there are subjects linked or not. May be replaced in future when pal_subjects_linked becomes a proper db field.
* 
* @param int $recordid The recordid of the current record.
* 
* @return array Array containing data value for pal_subjects_linked.
*/
function GetYNPALSubjects($recordid)
{
    $sql = 
        'SELECT count(*) as NUM_SUBJECTS_LINKED
        FROM PALS_MAIN
        LEFT JOIN 
        PALS_SUBJECTS 
        ON PALS_MAIN.RECORDID = PALS_SUBJECTS.PSU_PAL_ID 
        WHERE PALS_MAIN.RECORDID = :recordid
        AND PALS_SUBJECTS.RECORDID IS NOT NULL 
        GROUP BY PALS_MAIN.RECORDID';

    $numSubjects = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid), PDO::FETCH_COLUMN);

    return array('pal_subjects_linked' => ($numSubjects > 0 ? 'Y' : 'N'));
}