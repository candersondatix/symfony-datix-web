<?php

$ModuleDefs['PAL'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'PAL',
    'LEVEL1_PERMS' => array('PAL1'),
    'AUDIT_TRAIL_PERMS' => array('PAL2, RM'),  //which permissions can see the audit trail?

    'MOD_ID' => MOD_PALS,
    'CODE' => 'PAL',
    'APPROVAL_LEVELS' => true,
    'NAME' => _tk('mod_pals_title'),
    'USES_APPROVAL_STATUSES' => true,
    'TABLE' => 'pals_main',
    'AUDIT_TABLE' => 'aud_pals_report',
    'REC_NAME' => _tk('PALSName'),
    'REC_NAME_PLURAL' => _tk('PALSNames'),
	'REC_NAME_TITLE' => _tk('PALSNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('PALSNamesTitle'),
    'FK' => 'pal_id',
    'ACTION' => 'record&module=PAL',
    'OURREF' => 'pal_ourref',
    'PERM_GLOBAL' => 'PAL_PERMS',
    'NO_LEVEL1_GLOBAL' => 'PAL_NO_OPEN',
    'FIELD_NAMES' => array(
        "NAME" => "pal_name",
        "HANDLER" => "pal_handler",
        'REF' => 'pal_ourref',
        ),
    'LOCATION_FIELD_PREFIX' => 'pal_',
    'FORMS' => array(
        1 => array('LEVEL' => 1, 'CODE'=>'PAL1'),
        2 => array('LEVEL' => 2, 'CODE'=>'PAL2')
        ),

    'EXTRA_RECORD_DATA_FUNCTIONS' => array(
        'GetYNPALSubjects'
    ),
    'EXTRA_RECORD_DATA_FUNCTION_INCLUDES' => array(
        'Source/generic/Subjects.php','Source/generic_modules/PAL/ModuleFunctions.php'),
    'LOGGED_OUT_LEVEL1' => true,
    'MAIN_MENU_ICON' => 'pals.gif',
    'ICON' => 'icons/icon_pals.png',
    'AGE_AT_DATE' => 'pal_dreceived',

    'LINKED_DOCUMENTS' => true,
    'DOCUMENT_SECTION_KEY' => 'documents',

    'LINKED_CONTACT_LISTING_COLS' => array('recordid', 'pal_name', 'pal_dreceived', 'pal_synopsis', 'pal_type'),

    'SEARCH_URL' => 'action=search&module=PAL',
    'DEFAULT_ORDER' => 'pal_dreceived',

    'DIF1_REP_APPROVED' => true,

    'LINKED_RECORDS' => array(
                        'pal_subject' => array(
                            'section' => 'subject',
                            'type' => 'pal_subject', 'table' => 'PALS_SUBJECTS', 'recordid_field' => 'recordid', 'save_listorder' => true,
                            'save_controller' => 'pal_subjects_linked',
                            'basic_form' => array('Rows' => array('psu_subject', 'psu_subsubject', 'psu_stafftype', 'psu_organisation',
                                            'psu_unit', 'psu_clingroup', 'psu_directorate', 'psu_specialty', 'psu_location', 'psu_loctype',
                                            'psu_consent', 'psu_outcome', 'psu_notes')),
                            'main_recordid_label' => 'PSU_PAL_ID',
                            'RunAfterSave' => array('SavePrimarySubject'),
                            'RunAfterSaveIncludes' => array('Source/generic/Subjects.php'))
                        ),

    'SUBJECT_TYPE' => 'pal_subject',

    'PRIMARY_SUBJECT_MAPPINGS' => array(
        'psu_subject' => 'pal_subject1',
        'psu_subsubject' => 'pal_subsubject1',
        'psu_stafftype' => 'pal_stafftype1',
        'psu_organisation' => 'pal_organisation1',
        'psu_unit' => 'pal_unit1',
        'psu_clingroup' => 'pal_clingroup1',
        'psu_directorate' => 'pal_directorate1',
        'psu_specialty' => 'pal_specialty1',
        'psu_location' => 'pal_location1',
        'psu_loctype' => 'pal_loctype1',
        'psu_outcome' => 'pal_outcome1',
        'psu_notes' => 'pal_notes1'
        ),

    'CONTACTTYPES' => array(
                "P" => array( "Type"=> "P", "Name"=>_tk('pal_enquirer'), "Plural"=>_tk('pal_enquirer_plural'), "None"=>_tk('no_pal_enquirer_plural'), "CreateNew"=>_tk('pal_enquirer_link'), "Max"=> 1),
                "A" => array( "Type"=> "A", "Name"=>_tk('pal_patient'), "Plural"=>_tk('pal_patient_plural'), "None"=>_tk('no_pal_patient_plural'), "CreateNew"=>_tk('pal_patient_link')),
                "E" => array( "Type"=> "E", "Name"=>_tk('pal_employee'), "Plural"=>_tk('pal_employee_plural'), "None"=>_tk('no_pal_employee_plural'), "CreateNew"=>_tk('pal_employee_link')),
                "N" => array( "Type"=> "N", "Name"=>_tk('pal_contact'), "Plural"=>_tk('pal_contact_plural'), "None"=>_tk('no_pal_contact_plural'), "CreateNew"=>_tk('pal_contact_link'))

    ),
    'LEVEL1_CON_OPTIONS' => array(
                        "P" => array("Title" => "Enquirer", "DivName" => 'contacts_type_P', "Max"=> 1),
                        "N" => array("Title" => "Contact", "DivName" => 'contacts_type_N', 'TriggerField' => 'show_other_contacts')
                        ),
    'RECORD_NAME_FROM_CONTACT' => 'P',
    'STAFF_EMPL_FILTER_MAPPINGS' => array('pal_organisation' => 'con_orgcode',
                                    'pal_unit' => 'con_unit',
                                    'pal_clingroup' => 'con_clingroup',
                                    'pal_directorate' => 'con_directorate',
                                    'pal_specialty' => 'con_specialty',
                                    'pal_loctype' => 'con_loctype',
                                    'pal_locactual' => 'con_locactual'),
    'FIELD_ARRAY' => array(
        "pal_name", "pal_ourref", "pal_handler", "pal_organisation", "pal_unit", "pal_clingroup",
        "pal_directorate", "pal_specialty", "pal_locactual", "pal_loctype", "pal_type",
        "pal_dreceived", "pal_synopsis", "pal_method", "pal_whereheard", "pal_timecode", "pal_outcomecode",
        "pal_outcome", "pal_dclosed", "pal_investigator", "pal_inv_dstart", "pal_inv_dcomp",
        "pal_inv_outcome", "pal_lessons_code", "pal_inv_lessons", "pal_action_code", "pal_inv_action",
        "rep_approved", 'pal_last_updated'
    ),

    'SECURITY' => array(
        'LOC_FIELDS' => array(
            'pal_organisation' => array(),
            'pal_unit' => array(),
            'pal_clingroup' => array(),
            'pal_directorate' => array(),
            'pal_specialty' => array(),
            'pal_loctype' => array('multi' => true),
            'pal_locactual' => array('multi' => true)
            ),
        'EMAIL_SELECT_GLOBAL' => 'PAL_EMAIL_SELECT',
        'EMAIL_NOTIFICATION_GLOBAL' => 'PAL_STA_EMAIL_LOCS'
        ),
    'SHOW_EMAIL_GLOBAL' => 'PAL_SHOW_EMAIL',
    'EMAIL_REPORTER_GLOBAL' => 'PAL_EMAIL_REPORTER',
    'EMAIL_HANDLER_GLOBAL' => 'PAL_EMAIL_MGR',
    'EMAIL_USER_PARAMETER' => 'PAL_STA_EMAIL_LOCS',

    'FORM_DESIGN_ARRAY_FILE' => 'UserPALSForms.php',
    'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserPAL1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserPAL2Settings',

    'ACTION_LINK_LIST_FIELDS' => array(
        array('field' => 'recordid', 'width' => '5'),
        array('field' => 'pal_name', 'width' => '17'),
        array('field' => 'pal_dreceived', 'width' => '10'),
        array('field' => 'pal_type', 'width' => '17'),
        array('field' => 'pal_synopsis', 'width' => '51')
        ),
    'TRAFFICLIGHTS_FIELDS' => array(
        'rep_approved', 'pal_method',  'pal_whereheard', 'pal_type'
        ),
    'HOME_SCREEN_STATUS_LIST' => true,
    'GENERATE_MAPPINGS' => array('COM' => array(
                                    'pal_name' => 'com_name',
                                    'pal_organisation' => 'com_organisation',
                                    'pal_unit' => 'com_unit',
                                    'pal_clingroup' => 'com_clingroup',
                                    'pal_directorate' => 'com_directorate',
                                    'pal_specialty' => 'com_specialty',
                                    'pal_loctype' => 'com_loctype',
                                    'pal_locactual' => 'com_locactual',
                                    'pal_handler' => 'com_mgr',
                                    'pal_synopsis' => 'com_detail',
                                    'pal_root_causes' => 'com_root_causes',
                                    'rep_approved' => 'rep_approved'),
                                 'INC' => array(
                                    'pal_name' => 'inc_name',
                                    'pal_organisation' => 'inc_organisation',
                                    'pal_unit' => 'inc_unit',
                                    'pal_clingroup' => 'inc_clingroup',
                                    'pal_directorate' => 'inc_directorate',
                                    'pal_specialty' => 'inc_specialty',
                                    'pal_loctype' => 'inc_loctype',
                                    'pal_locactual' => 'inc_locactual',
                                    'pal_handler' => 'inc_mgr',
                                    'pal_synopsis' => 'inc_notes',
                                    'pal_root_causes' => 'inc_root_causes',
                                    'rep_approved' => 'rep_approved'),
                                 'CLA' => array(
                                    'pal_name' => 'cla_name',
                                    'pal_organisation' => 'cla_organisation',
                                    'pal_unit' => 'cla_unit',
                                    'pal_clingroup' => 'cla_clingroup',
                                    'pal_directorate' => 'cla_directorate',
                                    'pal_specialty' => 'cla_specialty',
                                    'pal_loctype' => 'cla_location',
                                    'pal_locactual' => 'cla_locactual',
                                    'pal_handler' => 'cla_mgr',
                                    'pal_synopsis' => 'cla_synopsis',
                                    'pal_root_causes' => 'cla_root_causes', 
                                    'rep_approved' => 'rep_approved')
                                    ),
    'CAN_LINK_DOCS' => true,
    'CAN_LINK_CONTACTS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,
    'CAN_LINK_EQUPMENT' => true,
    'LOCAL_HELP' => 'WebHelp/index.html#Pals/c_dx_pals_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Pals/c_dx_pals_guide.xml',

);