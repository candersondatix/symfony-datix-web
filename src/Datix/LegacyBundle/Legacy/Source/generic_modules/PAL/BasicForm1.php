<?php

// If the user is logged in from the contacts table, they are the
// reporter.
if ($_SESSION["contact_login"] && $pal["pal_repname"] == "")
{
    $pal["pal_repname"] = $_SESSION["fullname"];
}


$FormArray = array(
    "Parameters" => array(
        "Condition" => false,
        "ExtraContacts" => 1
    ),
    "contacts_type_P" => array(
        "Title" => "Details of Enquirer",
        "module" => 'PAL',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'P',
        'LinkedForms' => array('P' => array('module' => 'CON')),
        "suffix" => 1,
        "Rows" => array()
    ),
    "details" => array(
        "Title" => "Details",
        "Rows" => array(
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => $module,
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => $module,
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            )),
            "pal_type",
            "pal_dreceived",
            "pal_synopsis",
            "pal_method",
            "pal_whereheard"
        )
    ),
    "location" => array(
        "Title" => "Location admitted",
        "Rows" => array(
            "pal_organisation",
            "pal_unit",
            "pal_clingroup",
            "pal_directorate",
            "pal_specialty",
            "pal_loctype",
            "pal_locactual"
        )
    ),
    "outcome" => array(
        "Title" => "Outcome",
        "Rows" => array(
            'pal_timecode',
            'pal_outcomecode',
            'pal_outcome',
            'pal_dclosed'
        )
    ),
    "additional" => array(
        "Title" => "Additional Information",
        "Rows" => array(
            "show_other_contacts",
            "show_document",
            array(
                'Title' => 'Do you want to add any subjects to this record?',
                'Name' => 'pal_subjects_linked',
                'Type' => 'yesno'
            )
        )
    ),
    "contacts_type_N" => array(
        "Title" => "Contacts",
        "module" => 'PAL',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'N',
        "suffix" => 2,
        'LinkedForms' => array('N' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && ($FormType != 'Print' && $FormType != 'ReadOnly'),
        "NoFieldAdditions" => true,
        "Special" => 'DynamicDocument',
        "Rows" => array()
    ),
    "linked_documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && $FormType == 'Print',
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "subject" => array(
        "Title" => "Subjects",
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoSubjectSection' => array(
                'controller' => 'src\\generic\\controllers\\SubjectsController'
            )
        ),
        "ExtraParameters" => array('subject_name' => 'pal_subject'),
        "Rows" => array()
    ),
    "subject_design" => array(
        "Title" => "Subjects",
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "NoSectionActions" => true,
        "AltSectionKey" => "subject",
        "Rows" => array(
            'psu_subject',
            'psu_subsubject',
            'psu_stafftype',
            'psu_organisation',
            'psu_unit',
            'psu_clingroup',
            'psu_directorate',
            'psu_specialty',
            'psu_location',
            'psu_loctype',
            'psu_consent',
            'psu_outcome',
            'psu_notes'
        )
    ),
    "your_manager" => array(
        "Title" => "Your Manager",
        "module" => 'PAL',
        "Condition" => bYN(GetParm("PAL_EMAIL_MGR", "N")),
        "Rows" => array(
            array(
                "Type" => "formfield",
                "Name" => "pal_handler",
                "Title" => 'Your Manager',
                "FormField" => MakeManagerDropdownGeneric('PAL', $data, $FormType, array('PAL2', 'RM'))
            )
        )
    ),
    "udf" => array(
        "ExtraFields" => $UDFGroups,
        "NoFieldAdditions" => true,
    )
);

?>