<?php

// If the user is logged in from the contacts table, they are the
// reporter.
if ($_SESSION["contact_login"] && $data["pal_repname"] == "")
{
    $data["pal_repname"] = $_SESSION["fullname"];
}

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "header" => array(
        "Title" => "Name and Reference",
        "Rows" => array(
            array(
                'Name' => 'recordid',
                'Condition' => ($data['recordid'] || $FormType == 'Design' || $FormType == 'Search')
            ),
            "pal_name",
            'pal_ourref',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'PAL',
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => 'PAL',
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            )),
            "pal_handler"
        )
    ),
    "details" => array(
        "Title" => "Details",
        "Rows" => array(
            ['Name' => 'pal_last_updated', 'ReadOnly' => true],
            'pal_type',
            'pal_dreceived',
            'pal_synopsis',
            'pal_method',
            'pal_whereheard'
        )
    ),
    "outcome" => array(
        "Title" =>"Outcome",
        "Rows" => array(
            'pal_timecode',
            'pal_outcomecode',
            'pal_outcome',
            'pal_dclosed'
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $data['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('PAL', $data['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "location" => array("Title" =>"Location admitted",
        "Rows" => array(
            'pal_organisation',
            'pal_unit',
            'pal_clingroup',
            'pal_directorate',
            'pal_specialty',
            'pal_loctype',
            'pal_locactual'
        )
    ),
    "subject_header" => array(
        "Title" => "Linked Subjects",
        "NotModes" => array('Search'),
        "Rows" => array(
            array(
                'Title' => 'Do you want to add any subjects to this record?',
                'Name' => 'pal_subjects_linked',
                'Type' => 'yesno'
            )
        )
    ),
    "subject" => array(
        "Title" => "Subjects",
        "NotModes" => array('Search'),
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoSubjectSection' => array(
                'controller' => 'src\\generic\\controllers\\SubjectsController'
            )
        ),
        "ExtraParameters" => array('subject_name' => 'pal_subject'),
        "Rows" => array()
    ),
    "subject_design" => array(
        "Title" => "Subjects",
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "NoSectionActions" => true,
        "AltSectionKey" => "subject",
        "Rows" => array(
            'psu_subject',
            'psu_subsubject',
            'psu_stafftype',
            'psu_organisation',
            'psu_unit',
            'psu_clingroup',
            'psu_directorate',
            'psu_specialty',
            'psu_location',
            'psu_loctype',
            'psu_consent',
            'psu_outcome',
            'psu_notes'
        )
    ),
    "feedback" => array(
        "Title" => _tk('feedback_title'),
        "Special" => "Feedback",
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        'NoFieldRemoval' => true,
        'NoReadOnly' => true,
        "Rows" => [
            [
                'Name'        => 'dum_fbk_to',
                'Title'       => 'Staff and contacts attached to this record',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_gab',
                'Title'       => 'All users',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_email',
                'Title'       => 'Additional recipients',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_subject',
                'Title'       => 'Subject',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ],
            [
                'Name'        => 'dum_fbk_body',
                'Title'       => 'Body of message',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ]
        ]
    ),
    "investigation" => array("Title" => "Details of investigation",
        "Rows" => array(
            "pal_investigator",
            "pal_inv_dstart",
            "pal_inv_dcomp",
            "pal_inv_outcome",
            "pal_lessons_code",
            "pal_inv_lessons",
            "pal_action_code",
            "pal_inv_action"
        )
    ),
    'notepad' => array(
        "Title" => "Notepad",
        'NotModes' => array('New','Search'),
        "Rows" => array(
            'notes'
        )
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController'
            )
        ),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    'linked_records' => array(
        'Title' => 'Linked Records',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Special' => 'LinkedRecords',
        'Rows' => array()
    ),
    "rejection" => GenericRejectionArray('PAL', $data),
    "rejection_history" => array(
        "Title" => _tk('reasons_history_title'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'SectionRejectionHistory' => array(
                'controller' => 'src\\reasons\\controllers\\ReasonsController'
            )
        ),
        "NotModes" => array("New","Search"),
        "Condition" =>  (bYN(GetParm("REJECT_REASON",'Y'))),
        "Rows" => array()
    ),
);

//add contact sections for each contact type.
foreach($ModuleDefs['PAL']['CONTACTTYPES'] as $ContactTypeDetails)
{
    $ContactArray['contacts_type_'.$ContactTypeDetails['Type']] = array(
        'Title' => $ContactTypeDetails['Plural'],
        'NoFieldAdditions' => true,
        'ControllerAction' => array(
            'ListLinkedContacts' => array(
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            )
        ),
        'ExtraParameters' => array('link_type' => $ContactTypeDetails['Type']),
        'NotModes' => array('New','Search'),
        'Listings' => array('contacts_type_'.$ContactTypeDetails['Type'] => array('module' => 'CON')),
        'LinkedForms' => array($ContactTypeDetails['Type'] => array('module' => 'CON')),
        'Condition' => (CanSeeContacts('PAL', $DIFPerms, $inc['rep_approved']) || !bYN(GetParm('DIF2_HIDE_CONTACTS', 'N'))),
        'Rows' => array()
    );
}

array_insert_datix($FormArray, 'feedback', $ContactArray);

require_once 'Source/libs/ModuleLinks.php';

foreach (getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array(
        'module' => $link_mod,
        'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']
    );
}

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}
