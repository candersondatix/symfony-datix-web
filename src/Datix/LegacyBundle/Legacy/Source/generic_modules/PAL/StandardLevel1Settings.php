<?php
$GLOBALS['FormTitle'] = 'Datix PALS Form';

$GLOBALS["ExpandSections"] = array (
  'pal_subjects_linked' =>
  array (
    0 =>
    array (
      'section' => 'subject',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_document' =>
  array (
    0 =>
    array (
      'section' => 'documents',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_other_contacts' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_N',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
);

$GLOBALS["HideFields"] = array (
    "rep_approved" => true,
);

$GLOBALS["DefaultValues"] = array (
    'rep_approved' => 'AWAREV',
);

$GLOBALS["UserExtraText"] = array (
		"pal_synopsis" => "Enter facts, not opinions.  Do not enter names of people",
		'pal_outcome' => "Enter facts, not opinions.  Do not enter names of people",
);
?>