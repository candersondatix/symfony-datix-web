<?php

$GLOBALS['FormTitle'] = 'Datix PALS Form';

$GLOBALS["HideFields"] = array (
    'progress_notes' => true,
    'rejection_history' => true,
    'action_chains' => true,
    'pal_last_updated' => true
);

$GLOBALS["ExpandSections"] = array (
  'pal_subjects_linked' =>
  array (
    0 =>
    array (
      'section' => 'subject',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'rep_approved' =>
  array (
    0 =>
    array (
      'section' => 'rejection',
      'alerttext' => 'Please complete the \'Details of rejection\' section before saving this form.',
      'values' =>
      array (
        0 => 'REJECT',
      ),
    ),
  ),
);

$GLOBALS["NewPanels"] = array (
  'contacts_type_P' => true,
  'location' => true,
  'subject_header' => true,
  'feedback' => true,
  'investigation' => true,
  'linked_records' => true,
  'documents' => true,
  'linked_actions' => true,
  'notepad' => true,
  'progress_notes' => true,
  'word' => true,
  'action_chains' => true,
);

$GLOBALS["UserExtraText"] = array (
    "pal_synopsis" => "Enter facts, not opinions.  Do not enter names of people",
    'pal_outcome' => "Enter facts, not opinions.  Do not enter names of people",
    'dum_fbk_to' => 'Only staff and contacts with e-mail addresses are shown.',
    'dum_fbk_gab' => 'Only users with e-mail addresses are shown.',
    'dum_fbk_email' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'
);

?>