<?php

$ModuleDefs['POL'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'POL',
    'MOD_ID' => MOD_POLICIES,
    'CODE' => 'POL',
    'NAME' => _tk('mod_policies_title'),
    'TABLE' => 'policies',
    'REC_NAME' => _tk('POLName'),
    'REC_NAME_PLURAL' => _tk('POLNames'),
	'REC_NAME_TITLE' => _tk('POLNameTitle'),
	'REC_NAME_PLURAL_TITLE' => _tk('POLNamesTitle'),
    'FK' => 'pol_id',
    'ACTION' => 'record&module=POL',
    'SEARCH_URL' => 'action=search&module=POL',
    'PERM_GLOBAL' => 'POL_PERMS',
    'FORMS' => array(2 => array('LEVEL' => 2, 'CODE'=>'POL2')),
    'ICON' => 'icons/icon_POL.png',
    'FIELD_ARRAY' => array('title', 'reference', 'description', 'aggregate_limit', 'event_limit', 'start_date', 'end_date', 'coverage_type', 'policy_basis', 'insurer', 'policy_type', 'costs_covered', 'attachment_point'),
    'FORM_DESIGN_ARRAY_FILE' => 'UserPOLForms.php',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserPOL2Settings',
    'LINKED_CONTACTS' => false,
    'LINKED_DOCUMENTS' => true,
    'NOTEPAD' => false,
	'NO_REP_APPROVED' => true,
    'ADD_NEW_RECORD_LEVELS' => array('POL_FULL'),
	'USES_APPROVAL_STATUSES' => false,
	'STANDARD_COPY_CHECK_OPTIONS' => false,

	'DATA_VALIDATION_INCLUDES' => array(
		'Source/generic_modules/POL/ModuleFunctions.php'
	),
	'DATA_VALIDATION_FUNCTIONS' => array(
		'validateCoverageDates'
	),
    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Title' => _tk("pol_listing"),
            'Link' => _tk("list_all_policy"),
            'Where' => '(1=1)'
        )
    ),
    'EXTRA_FORM_ACTIONS' => array(
        'generateInstances' => array(
            'title'     => 'unlink_policy',
            'condition' => $_GET['link_recordid'] != '',
            'js'        => "SendTo('app.php?action=editpolicylink&link_action=delete".
                               "&policy_id=".(int) $_GET['recordid'].
                               "&type=".(in_array($_GET['type'], ['CON','ORG']) ? $_GET['type'] : '').
                               "&main_recordid=".(int) $_GET['main_recordid'].
                               "&link_recordid=".(int) $_GET['link_recordid'].
                               "&org_id=".(int) $_GET['org_id']."');",
            'action'    => 'DELETE'
        )
    ),
    'LOCAL_HELP' => 'WebHelp/index.html#Policies/c_dx_policies_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Policies/c_dx_policies_guide.xml',
    'CONTACT_LINK_TABLE_ID' => array('link_respondents' => 'main_recordid')
);
