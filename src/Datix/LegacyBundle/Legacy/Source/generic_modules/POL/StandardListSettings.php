<?php

$list_columns_standard = array(
    "title" => array(
        'width' => '8'),
	"reference" => array(
		'width' => '8'),
    "insurer" => array(
        'width' => '8'),
    "coverage_type" => array(
        'width' => '4'),
    "attachment_point" => array(
        'width' => '4'),
	"event_limit" => array(
		'width' => '4')
);
