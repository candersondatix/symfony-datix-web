<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "policydetails" => array(
        "Title" => "Policy details",
        "Rows" => array( 
        	array( 'Name' => 'recordid', 'Condition' => ($data['recordid'] || $FormType == 'Design' || $FormType == 'Search') ),
            "title",
        	'reference',
            'description',
        	'insurer',
        	'coverage_type',
        	'policy_basis',
        	'policy_type',
        	'costs_covered'
        )
    ),
	"coveragedetails" => array(
		"Title" => "Coverage details",
		"Rows" => array(
			"start_date",
			"end_date",
			'attachment_point',
			"event_limit",
			"aggregate_limit"
		)
	),
    'respondents' => array(
        'Title' => _tk('respondents_title'),
        'NoFieldAdditions' => true,
        'NoReadOnly' => true,
        'Condition'  => $_GET['action'] == 'record' || $FormMode == 'Design',
        'ControllerAction' => array(
            'listrespondentsforpolicy' => array(
                'controller' => 'src\\respondents\\controllers\\RespondentsController'
            )
        ),
        'NotModes' => array('New','Search'),
        'Rows' => array()
    ),
    'claims' => array(
        'Title' => _tk('CLANamesTitle'),
        'NoFieldAdditions' => true,
        'NoReadOnly' => true,
        'Condition'  => $_GET['action'] == 'record' || $FormMode == 'Design',
        'ControllerAction' => array(
            'listclaimsforpolicy' => array(
                'controller' => 'src\\policies\\controllers\\PoliciesController'
            )
        ),
        'NotModes' => array('New','Search'),
        'Listings' => array('claims' => array('module' => 'CLA')),
        'Rows' => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $data['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('POL', $data['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    )
);

//require_once 'Source/libs/ModuleLinks.php';
//
//foreach (getModLinksArray() as $link_mod => $name)
//{
//    $FormArray['linked_records']['Listings'][$link_mod] = array(
//        'module' => $link_mod,
//        'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']
//    );
//}
//
//if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
//{
//    unset($FormArray['word']['ControllerAction']);
//    $FormArray['word']['Special'] = 'SectionWordMerge';
//}
