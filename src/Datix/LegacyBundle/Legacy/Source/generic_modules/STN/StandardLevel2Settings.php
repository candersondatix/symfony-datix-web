<?php
$GLOBALS['FormTitle'] = 'Details of Standard';

$GLOBALS["NewPanels"] = array (
    'details' => true,
    'evidence' => true,
    'risks' => true,
    'linked_actions' => true,
    'progress_notes' => true,
    'word' => true,
    'linked_records' => true,
    'action_chains' => true,
);

$GLOBALS["HideFields"] = array (
    'progress_notes' => true,
    'stn_ddue' => true,
    'documents' => true,
	'action_chains' => true,
);

?>
