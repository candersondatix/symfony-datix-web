<?php

if ($_GET['action'] != 'liststandards' & $_GET['action'] != 'reports' && $_GET['action'] != 'stnreports' && $FormType != 'Design')
{
    require_once 'Source/standards/StandardsBase.php';
    require_once 'Source/standards/StandardsForm.php';
    $libCount = CountLinkedEvidence('STN', $stn['recordid']);
}

$FormArray = array(
    "Parameters" => array(
        "Panels" => "True",
        "Condition" => false
    ),
    "details" => array(
        "Title" => "Standard Details",
        "NewPanel" => true,
        "Rows" => array(
            array('Name' => 'recordid', 'Condition' => ($stn['recordid'] || $FormType == 'Design' || $FormType == 'Search')),
            array("Name" => "stn_set", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_domain", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_name", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_version", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_ourref", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_descr", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_type", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_assessment", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_manager", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_handler", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_listorder", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_score", 'Condition' => ($FormType != 'Search' && ($FormType == 'Design' || $Perms))),
            array("Name" => "stn_status","Type" => "ff_select","FormField" => $obj_stn_status, 'Condition' => ($FormType != 'Search' && ($FormType == 'Design' || $Perms))),
            array("Name" => "stn_ddue", "ReadOnly" => $ReadOnly)
        )
    ),
    "location" => array(
        "Title" => "Location",
        "Rows" => array(
            array("Name" => "stn_organisation", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_unit", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_clingroup", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_directorate", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_specialty", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_loctype", "ReadOnly" => $ReadOnly),
            array("Name" => "stn_locactual", "ReadOnly" => $ReadOnly)
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $stn['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('STN', $stn['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "elements" => array(
        "Title" => "Elements",
        "Function" => 'ElementsSection',
        'Listings' => array('elements' => array('module' => 'ELE')),
        'LinkedForms' => array('elements' => array('module' => 'ELE')),
        "NoFieldAdditions" => true,
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "evidence" => array(
        "Title" => "Evidence" . ($libCount?" ($libCount)":""),
        "Function" => 'ListEvidence',
        'Listings' => array('evidence' => array('module' => 'LIB')),
        "NoFieldAdditions" => true,
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "child_evidence" => array(
        "Title" => "Linked Records Evidence",
        "Type" => "Panel",
        "Function" => 'ListRelatedEvidence',
        "NoFieldAdditions" => true,
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        "NotModes" => array("New", "Search"),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        "Special" => "LinkedActions"
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "Type" => "Panel",
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController')),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    'linked_records' => array(
        'Title' => 'Linked Records',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Special' => 'LinkedRecords',
        'Rows' => array()
    ),
);

require_once 'Source/libs/ModuleLinks.php';

foreach (getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array(
        'module' => $link_mod,
        'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']
    );
}

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}
