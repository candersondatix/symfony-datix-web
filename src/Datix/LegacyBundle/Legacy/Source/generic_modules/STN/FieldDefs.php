<?php
$FieldDefs["STN"] = array(
    "recordid" => array("Type" => "number",
        "Title" => "ID",
        "ReadOnly" => true,
        "Width" => 5),
    "stn_set" => array("Type" => "ff_select",
        "Title" => "Origin",
        "ReadOnly" => (!$IsAdminUser)),
    "stn_domain" => array("Type" => "ff_select",
        "Title" => "Domain",
        "ReadOnly" => (!$IsAdminUser)),
    "stn_name" => array("Type" => "string",
        "Title" => "Title",
        "Width" => 70,
        "MaxLength" => 254,
        "ReadOnly" => (!$IsAdminUser)),
    "stn_version" => array("Type" => "ff_select",
        "Title" => "Version",
        "ReadOnly" => (!$IsAdminUser)),
    "stn_ourref" => array("Type" => "string",
        "Title" => "Ref.",
        "Width" => 32,
        "MaxLength" => 32,
        "ReadOnly" => (!$IsAdminUser)),
    "stn_descr" => array("Type" => "textarea",
        "Title" => "Description",
        "Rows" => 7,
        "Columns" => 70,
        "ReadOnly" => (!$IsAdminUser)),
    "stn_type" => array("Type" => "ff_select",
        "Title" => "Type",
        "ReadOnly" => (!$IsAdminUser)),
    "stn_manager" => array("Type" => "ff_select",
        "Title" => "Manager",
        "ReadOnly" => (!$IsAdminUser)),
    "stn_handler" => array("Type" => "ff_select",
        "Title" => "Handler",
        "ReadOnly" => (!$IsAdminUser)),
    "stn_listorder" => array("Type" => "number",
        "Title" => "Sequence no.",
        "ReadOnly" => (!$IsAdminUser)),
    "stn_score" => array("Type" => "number",
        "Title" => "Score",
        'NoListCol' => true,
        "ReadOnly" => (True)),
    "stn_organisation" => array("Type" => "ff_select",
        "Title" => "Trust"),
    "stn_unit" => array("Type" => "ff_select",
        "Title" => "Unit"),
    "stn_clingroup" => array("Type" => "ff_select",
        "Title" => "Clinical Group"),
    "stn_directorate" => array("Type" => "ff_select",
        "Title" => "Directorate"),
    "stn_specialty" => array("Type" => "ff_select",
        "Title" => "Specialty"),
    "stn_loctype" => array("Type" => "ff_select",
        "Title" => "Location (type)"),
    "stn_locactual" => array("Type" => "ff_select",
        "Title" => "Location (exact)"),
    "stn_assessment" => array("Type" => "ff_select",
        "Title" => "Assessment"),
    "stn_status" => array("Type" => "string",
        "Title" => "Status",
        "Width" => 32,
        "MaxLength" => 32,
        'NoListCol' => true,
        "ReadOnly" => (true)),
    "stn_dec_dend" => array("Type" => "date",
        "Title" => "Start Date"),
    "stn_dec_dstart" => array("Type" => "date",
        "Title" => "Start Date"),
    "stn_declaration" => array("Type" => "ff_select",
        "Title" => "Declaration"),
    "stn_objectives" => array("Type" => "multilistbox",
        "Title" => "Declaration",
    	"MaxLength" => 254),
    "stn_ddue" => array("Type" => "date",
        "Title" => "Date due"),
    "link_notes" => array("Type" => "textarea",
        "Title" => "Link Notes",
        "Rows" => 5,
        "Columns" => 70,
        'LinkField' => true,
        "NoListCol_Override" => true),


    /* fields not used in the web, but default names might be needed for reporting if WEB_LABELS_ONLY is set */
    'stn_dec_issue' => array('Title' => 'Issue'),
    'stn_dec_actions' => array('Title' => 'Actions summary'),
    'ele_id' => array('Title' => 'ID (Element)', 'NoListCol_Override' => true),
    'pro_id' => array('Title' => 'ID (Compliance)', 'NoListCol_Override' => true),
    'lib_id' => array('Title' => 'ID (Evidence)', 'NoListCol_Override' => true),
    'ele_id' => array('Title' => 'ID (Element)', 'NoListCol_Override' => true),
    'pro_id' => array('Title' => 'ID (Compliance)', 'NoListCol_Override' => true),
    'ele_id' => array('Title' => 'ID (Element)', 'NoListCol_Override' => true),
    'lib_id' => array('Title' => 'ID (Evidence)', 'NoListCol_Override' => true),

        );
?>
