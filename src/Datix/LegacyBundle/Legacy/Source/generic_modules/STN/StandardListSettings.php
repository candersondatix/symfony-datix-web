<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '2'),
    "stn_set" => array(
        'width' => '10'),
    "stn_domain" => array(
        'width' => '10'),
    "stn_name" => array(
	    'width' => '6'),
    'stn_descr' => array (
	    'width' => '30'),
);

?>
