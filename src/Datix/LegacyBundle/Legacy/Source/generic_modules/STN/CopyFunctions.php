<?php
/**
* @desc Copies elements from one standards record to another with further options to copy child releated data..
* 
* @param string $module Module short name.
* @param int $SourceStandardID Recordid of source standard.
* @param int $DestStandardID Recordid of new standard record
* @param CopyOptions $aAdditionalParams CopyOptions object containing additional copy options.
*/
function CopyElements($module, $SourceStandardID, $DestStandardID, $aAdditionalParams)
{
    global $ModuleDefs;
    
    $field_array = $ModuleDefs['ELE']['FIELD_ARRAY']; 
      
    $field_array[] = "ele_stn_id";
    $CopyScores = bYN($aAdditionalParams->copy_scores);
    $CopyComments = bYN($aAdditionalParams->copy_comments); 
    $CopyEvidence = bYN($aAdditionalParams->copy_evidence);
    $copy_actions = bYN($aAdditionalParams->copy_actions); 
    
    $sql = "SELECT recordid, ".implode(', ', $field_array)." FROM standard_elements
             WHERE ele_stn_id = $SourceStandardID"; 
    $result = PDO_fetch_all($sql);

    foreach($result as $data)
    {
        $SourceElementID = $data["recordid"];
        $data["ele_stn_id"] = $DestStandardID; 
        
        if (!$CopyScores)
        {
            $data["ele_score"] = null;  
        }
        
        if (!$CopyComments)
        {
            $data["ele_comments"] = null;  
        }
    
        $recordid = GetNextRecordID($ModuleDefs['ELE']['TABLE'], true, 'recordid'); 
        $data["recordid"] = $recordid; 
             
        $sql = "UPDATE standard_elements SET "; 
        $sql .= GeneratePDOSQLFromArrays(array(
                            'FieldArray' => $field_array,
                            'DataArray' => $data,
                            'Module' => 'ELE')
                            , $PDOParams);

        $sql .= " WHERE recordid =:recordid";   
        
        $PDOParams["recordid"] = $recordid;  
        
        if (!PDO_query($sql, $PDOParams))
        {
            fatal_error("Cannot copy Elements data");
            return false;
        }
        
        CopyPrompts($DestStandardID, $SourceElementID, $recordid, $SourceStandardID,$aAdditionalParams);
        
        if ($CopyEvidence)
        {
            CopyEvidence($DestStandardID, $recordid, $SourcePromptID, null, $SourceStandardID, $SourceElementID, "ELE", $aAdditionalParams);  
        }
        
        if ($copy_actions)
        {
            CopyRecords::CopyActions("ELE", $SourceElementID, $recordid, $aAdditionalParams->copy_extra_fields);
        }
    } 
    return true;
}

/**
* @desc Copies prompts from one element record to another.
* 
* @param int $DestStandardID Recordid of new standard record         
* @param int $SourceElementID Recordid of source element. 
* @param int $DestElementID Recordid of new element record
* @param CopyOptions $aAdditionalParams CopyOptions object containing additional copy options.  
*/
function CopyPrompts($DestStandardID, $SourceElementID, $DestElementID, $SourceStandardID, $aAdditionalParams)
{
    global $ModuleDefs;
    $CopyScores = bYN($aAdditionalParams->copy_scores);
    $CopyComments = bYN($aAdditionalParams->copy_comments);
    $CopyEvidence = bYN($aAdditionalParams->copy_evidence);
    $copy_actions = bYN($aAdditionalParams->copy_actions); 
        
    $field_array = $ModuleDefs['PRO']['FIELD_ARRAY'];    
    
    $field_array[] = "pro_ele_id";
    
    $sql = "SELECT recordid, ".implode(', ', $field_array)." FROM standard_prompts
             WHERE pro_ele_id = $SourceElementID"; 
    $result = PDO_fetch_all($sql);

    foreach($result as $data)
    {      
        $SourcePromptID = $data["recordid"];
        $recordid = GetNextRecordID($ModuleDefs['PRO']['TABLE'], true, 'recordid'); 
        $data["recordid"] = $recordid; 
        
        $data["pro_ele_id"] = $DestElementID;
        if (!$CopyScores)
        {
            $data["pro_act_score"] = null;  
        }
        
        if (!$CopyComments)
        {
            $data["pro_comments"] = null;  
        }
        
        $sql = "UPDATE standard_prompts SET "; 
        $sql .= GeneratePDOSQLFromArrays(array(
                            'FieldArray' => $field_array,
                            'DataArray' => $data,
                            'Module' => 'PRO')
                            , $PDOParams);

        $sql .= " WHERE recordid =:recordid";   
        
        $PDOParams["recordid"] = $recordid;  
        
        if (!PDO_query($sql, $PDOParams))
        {
            fatal_error("Cannot copy Prompt data");
            return false;
        }
        
        if ($CopyEvidence)
        {
            CopyEvidence($DestStandardID, $DestElementID, $SourcePromptID, $recordid, $SourceStandardID, $SourceElementID, "PRO", $aAdditionalParams);  
        }
        
        if ($copy_actions)
        {
            CopyRecords::CopyActions("PRO", $SourcePromptID, $recordid, $aAdditionalParams->copy_extra_fields);
        }
    } 
    return true;
}

/**
* @desc Copies evidence from one prompt record to another.
* 
* @param int $DestStandardID Recordid of new standard record     
* @param int $DestElementID Recordid of new element record   
* @param int $SourcePromptID Recordid of source prompt.
* @param int $DestPromptID Recordid of new prompt record  
* @param CopyOptions $aAdditionalParams CopyOptions object containing additional copy options.  
*/
function CopyEvidence($DestStandardID, $DestElementID, $SourcePromptID, $DestPromptID, $SourceStandardID, $SourceElementID, $LinkModuleLevel, $aAdditionalParams)
{
    global $ModuleDefs;
    $CopyEvidenceLinkNotes = bYN($aAdditionalParams->copy_evidence_link_notes); 
    
    $sql = "SELECT lib_id, link_notes, link_type FROM link_library
             WHERE link_type = 'E'"; 
    
    switch ($LinkModuleLevel) {
        case 'STN':
            $sql .= " AND stn_id = $SourceStandardID AND (ele_id IS NULL OR ele_id = 0) AND (pro_id IS NULL OR pro_id = 0)";
            break;    
        case 'ELE':
            $sql .= " AND (stn_id = $SourceStandardID AND ele_id = $SourceElementID AND (pro_id IS NULL OR pro_id = 0))";
            break; 
        case 'PRO':
            $sql .= " AND (stn_id = $SourceStandardID AND ele_id = $SourceElementID AND pro_id = $SourcePromptID)";
            break;       
    }
    
    $result = PDO_fetch_all($sql);

    foreach($result as $data)
    {      
        if (!$CopyEvidenceLinkNotes)
        {
            $data["link_notes"] = null;  
        }
        
        $sql = "Insert into link_library (stn_id, ele_id, pro_id, lib_id, link_notes, link_type)
                values (:stn_id, :ele_id, :pro_id, :lib_id, :link_notes, :link_type)";
        
        if (!PDO_query($sql, array("stn_id" => $DestStandardID, "ele_id" => $DestElementID, "pro_id" => $DestPromptID, 
                                    "lib_id" => $data["lib_id"], "link_notes" => $data["link_notes"], "link_type" => $data["link_type"])))
        {
            fatal_error("Cannot copy evidence data");
            return false;
        }
    } 
    return true;
}  

/**
* @desc Copies evidence linked direclty from one standards record to another.
* 
* @param string $module Module short name.
* @param int $SourceStandardID Recordid of source standard.
* @param int $DestStandardID Recordid of new standard record
* @param CopyOptions $aAdditionalParams CopyOptions object containing additional copy options.  
*/
function CopyStandardsEvidence($module, $SourceStandardID, $DestStandardID, $aAdditionalParams)
{
    $CopyEvidence = bYN($aAdditionalParams->copy_evidence); 
    
    if ($CopyEvidence)
    {
        CopyEvidence($DestStandardID, $DestElementID, $SourcePromptID, $recordid, $SourceStandardID, $SourceElementID, "STN", $aAdditionalParams);  
    }
}
?>
