<?php
/**
* Creates the Hotspot Agent section of the hotspots form.
* 
* @param  array   $hot                          The existing form data.
* @param  string  $FormAction  Current Form action mode.
* 
* @return string  HTML for listing section
*/
function HotspotAgentSection($hot, $FormAction)
{   
    $Perms = GetParm("HOT_PERMS");
    $AdminUser = ($Perms == 'HOT_FULL');

    require_once 'Source/libs/ListingClass.php';
    $Listing = new Listing('HSA');
    
    $Listing->LoadColumnsFromDB();
    
    $Listing->WhereClause = ' recordid in (SELECT hsa_id FROM hotspots_main WHERE recordid = ' . $hot['recordid'] . ')';
 
    $Listing->LoadDataFromDB();
    
    echo $Listing->GetListingHTML($FormAction);
}