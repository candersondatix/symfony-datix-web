<?php
/**
* @desc Inserts a record into the hotspot_queue table for the relevant module with the details of the creator.
* 
* @param string $module The module code
*/
function InsertIntoHotspotQueue($module, $cas_id)
{   
    $data = array("createdby" => $_SESSION["initials"], "createddate" =>date('Y-m-d H:i:s'), "hsq_module" => $module, "cas_id" => $cas_id);
    $sql = 'INSERT INTO hotspots_queue (createdby,createddate,hsq_module,cas_id) VALUES (:createdby,:createddate,:hsq_module,:cas_id)';
    DatixDBQuery::PDO_insert($sql, $data);   
}

