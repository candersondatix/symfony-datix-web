<?php

require_once 'Source/generic_modules/HOT/FormFunctions.php';

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "header" => array(
        "Title" => "Name and Reference",
        "Rows" => array(
            array(
                'Name' => 'recordid',
                'Condition' => ($data['recordid'] || $FormType == 'Design' || $FormType == 'Search')
            ),
            "hot_name",
            'hot_ourref',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'HOT',
                'perms' => $Perms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $data,
                'module' => 'HOT',
                'perms' => $Perms,
                'approveobj' => $ApproveObj
            )),
            "hot_handler",
            "hot_opened_date",
            "hot_closed_date",
        )
    ),
    "details" => array(
        "Title" => "Details of "._tk('HOTNameTitle'),
        "Rows" => array(
            "hot_descr"
        )
    ),
    'notepad' => array(
        "Title" => "Notepad",
        'NotModes' => array('New','Search'),
        "Rows" => array(
            'notes'
        )
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    'linked_records' => array(
        'Title' => 'Linked Records',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Special' => 'LinkedRecords',
        'Rows' => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "hotspot_agent" => array(
        "Title" => _tk('HSANameTitle'),
        "Function" => 'HotspotAgentSection',
        'Listings' => array('hotspot_agent' => array('module' => 'HSA')),
        "Condition" => $_GET["recordid"] || $FormMode == 'Design'
    ),
    "permissions" => array(
        "Title" => "Permissions",
        "NoFieldAdditions" => true,
        "Function" => "RecordPermissionsGeneric",
        "ExtraParameters" => array('module' => 'HOT', 'type' => 'user'),
        "Condition" => bYN(GetParm('SEC_RECORD_PERMS', 'N')),
        "NotModes" => array("New","Search","Print"),
        "Rows" => array()
    ),
);

require_once 'Source/libs/ModuleLinks.php';

foreach (getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array('module' => $link_mod, 'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']);
}
