<?php
$ModuleDefs['HOT'] = array(
    'GENERIC' => TRUE,
    'GENERIC_FOLDER' => 'HOT',
    'LEVEL1_PERMS' => array(),
    'AUDIT_TRAIL_PERMS' => array('HOT2'),

    'MOD_ID' => MOD_HOTSPOTS,
    'CODE' => 'HOT',
    'APPROVAL_LEVELS' => true,
    'NAME' => _tk("mod_hotspots_title"),
    'USES_APPROVAL_STATUSES' => true,
    'NO_REP_APPROVED' => true,
    'TABLE' => 'hotspots_main',
    'AUDIT_TABLE' => 'aud_hotspots_main',
    'REC_NAME' => _tk("HOTName"),
    'REC_NAME_PLURAL' => _tk("HOTNames"),
	'REC_NAME_TITLE' => _tk("HOTNameTitle"),
	'REC_NAME_PLURAL_TITLE' => _tk("HOTNamesTitle"),
    'FK' => 'hot_id',
    'OURREF' => 'hot_ourref',
    'PERM_GLOBAL' => 'HOT_PERMS',
    'NO_LEVEL1_GLOBAL' => 'HOT_NO_OPEN',
    'ACTION' => 'record&module=HOT',
    'FIELD_NAMES' => array(
        'NAME' => 'hot_name',
        'HANDLER' => 'hot_handler',
        'REF' => 'hot_ourref',
        ),
//    'LOCATION_FIELD_PREFIX' => 'com_',
    'FORMS' => array(
   //     1 => array('LEVEL' => 1, 'CODE'=>'HOT1'),
        2 => array('LEVEL' => 2, 'CODE'=>'HOT2')
        ),
 /*
    'LOGGED_OUT_LEVEL1' => true,
    'FORMCODE1' => 'HOT1',                   */
    'FORMCODE2' => 'HOT2',
    'MAIN_MENU_ICON' => 'hotspot_24.png',
    'ADD_NEW_RECORD_LEVELS' => array('HOT_FULL'),  
    'ICON' => 'icons/icon_HOT_34x32.png',
    'LINKED_CONTACTS' => false,
    'CAN_CREATE_EMAIL_TEMPLATES' => true,
/*    'LINKED_DOCUMENTS' => true,
    'DOCUMENT_SECTION_KEY' => 'documents',

    'LINKED_CONTACT_LISTING_COLS' => array('recordid', 'com_name', 'com_ourref', 'com_detail', 'com_type'),     */

    'SEARCH_URL' => 'action=search',
    'DEFAULT_ORDER' => 'hot_opened_date',
   // 'OVERDUE_CHECK_FIELD' => 'hot_opened_date',
   // 'OVERDUE_STATUSES' => array('AWAREV', 'STCL', 'INREV', 'AWAFA', 'INFA'),

/*
    'STAFF_EMPL_FILTER_MAPPINGS' => array('com_organisation' => 'con_orgcode',
                                    'com_unit' => 'con_unit',
                                    'com_clingroup' => 'con_clingroup',
                                    'com_directorate' => 'con_directorate',
                                    'com_specialty' => 'con_specialty',
                                    'com_loctype' => 'con_loctype',
                                    'com_locactual' => 'con_locactual'),        */
    'FIELD_ARRAY' => array(
        "rep_approved", "hot_name", "hot_ourref", "hot_handler", "hot_descr", "hot_opened_date", "hot_closed_date", "hsa_id"
        ),

    'EXTRA_SAVE_INCLUDES' => array(
        'Source/generic_modules/HOT/ModuleFunctions.php'
    ),
   /* 'EXTRA_SAVE_FUNCTIONS' => array(
        'saveInvestigationStarted', 'saveInvestigationCompleted'
    ),       */

 /*   'SECURITY' => array(
        'LOC_FIELDS' => array(
            'com_organisation' => array(),
            'com_unit' => array(),
            'com_clingroup' => array(),
            'com_directorate' => array(),
            'com_specialty' => array(),
            'com_loctype' => array('multi' => true),
            'com_locactual' => array('multi' => true)
            ),
        'EMAIL_SELECT_GLOBAL' => 'HOT_EMAIL_SELECT',
        'EMAIL_NOTIFICATION_GLOBAL' => 'HOT_STA_EMAIL_LOCS'
        ),
    'SHOW_EMAIL_GLOBAL' => 'HOT_SHOW_EMAIL',
    'EMAIL_REPORTER_GLOBAL' => 'HOT_EMAIL_REPORTER',
    'EMAIL_HANDLER_GLOBAL' => 'HOT_EMAIL_MGR',
    'EMAIL_USER_PARAMETER' => 'HOT_STA_EMAIL_LOCS',                    */

    'FORM_DESIGN_ARRAY_FILE' => 'UserHOTForms.php',
 //   'FORM_DESIGN_LEVEL_1_FILENAME' => 'UserHOT1Settings',
    'FORM_DESIGN_LEVEL_2_FILENAME' => 'UserHOT2Settings',
    
    'HARD_CODED_LISTINGS' => array(
        'all' => array(
            'Title' => _tk("hot_open_listing_title"),
            'Link' => _tk("hot_open_listing_link"),
            'Where' => 'hotspots_main.hot_closed_date is null'
            ),
    ),

    'ACTION_LINK_LIST_FIELDS' => array(
        array('field' => 'recordid', 'width' => '5'),
        array('field' => 'hot_name', 'width' => '17'),
        array('field' => 'hot_ourref', 'width' => '10'),
        array('field' => 'hot_handler', 'width' => '17'),
        array('field' => 'hot_descr', 'width' => '51')
        ),

    'DISABLE_SEC_WHERE_CLAUSE' => true,

    'CAN_LINK_DOCS' => true,
    'CAN_LINK_MODULES' => true,
    'CAN_LINK_NOTES' => true,

    'LOCAL_HELP' => 'WebHelp/index.html#Hotspots/c_dx_hotspots_guide.html',
    'REMOTE_HELP' => 'http://help.datix.co.uk/?topicUri=/Content/en-GB/DatixWebHelp/Hotspots/c_dx_hotspots_guide.xml',
    );
