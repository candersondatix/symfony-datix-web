<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '2'),
    "hot_handler" => array(
        'width' => '10'),
    "hot_name" => array(
        'width' => '10'),
    "hot_opened_date" => array(
	    'width' => '6'),
    "hot_closed_date" => array(
        'width' => '6'),
    "hot_descr" => array (
	    'width' => '30'),
);

$list_columns_mandatory = array("recordid");

$list_columns_extra = array(
    "hot_ourref" => array(
        'condition' => ($ListType == "approved"),
        'showextra' => 'condition: $ListType == approved')
);
?>
