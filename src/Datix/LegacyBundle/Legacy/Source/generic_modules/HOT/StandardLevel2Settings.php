<?php

$GLOBALS['FormTitle'] = _tk('hot_title"');

$GLOBALS["DefaultValues"] = array (
  'hot_opened_date' => 'TODAY',
  'hot_inv_status' => 'NOTSTA'
);

$GLOBALS["NewPanels"] = array (
    'notepad' => true,
    'progress_notes' => true,
    'linked_actions' => true,
    'linked_records' => true, 
    'documents' => true,
    'hotspot_agent' => true,
    'permissions' => true,
    'action_chains' => true,
);

$GLOBALS["HideFields"] = array(
    'action_chains' => true,
);

/*$GLOBALS["ReadOnlyFields"] = array(
    'hot_inv_started' => true,
    'hot_inv_completed' => true,
);*/
?>