<?php

function ListContacts($data, $con, $ReadOnly = false, $FormType)
{
    require_once 'Source/contacts/GenericContactForm.php';
    ListContactsSection(array(
        'data' => $data,
        'module' => 'INC',
        'formtype' => $FormType
    ));
}

function SectionSecureMessages($inc, $FormType = "Main", $Module = "INC")
{
    ListContactType($inc, $FormType, "", "Recipients", true, "contact");

    $CTable = new FormTable($FormType, 'INC');
    $FieldObj = new FormField();

    $CTable->MakeRow("<b>" . _tk('subject_of_message') . "</b>",
    $FieldObj->MakeInputField('msg_subject', 40, 80, $msg['msg_subject']));

    $CTable->MakeRow("$error[msg_message]<b>" . _tk('message') . "</b>",
    $FieldObj->MakeTextAreaField('msg_message', 7, 70, 254, $msg["msg_message"]));

    $CTable->MakeRow($error["msg_status"] . "<b>" . _tk('message_status') . "</b>",
    $FieldObj->MakeDropdown("MSG", "msg_status", $msg["msg_status"]));

    echo $CTable->GetFormTable();

    echo '<tr>
    <td class="windowbg2" colspan="2">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="windowbg2" align="left">
                <input type="submit" value="'
                . _tk('send_msg_to_selected') . '" class="button" '.
                'onclick="document.forms[0].rbWhat.value=\'SendMessage\';submitClicked=true;" name="btnSendMsg"/>
                </td>
            </tr>
        </table>
    </td>
    </tr>';
}

function ListContactType($inc, $FormType = "Main", $ConType, $Header, $EmailCheckBox = false, $ConItemName = "contact")
{
    global $scripturl;

    $AdminUser = $_SESSION["AdminUser"];
    $DIFPerms = $_SESSION["Globals"]["DIF_PERMS"];

    if ($DIFPerms == "DFI2_READ_ONLY")
    {
        $EmailCheckBox = false;
    }

    if ($EmailCheckBox)
    {
        $ColSpan = 8;
    }
    else
    {
        $ColSpan = 7;
    }

    $LinkPerms = (!$_GET["print"]);

    $IncID = $inc["recordid"];

    $sql = "SELECT con_id, link_type, con_title, con_forenames, con_surname,
        con_type, con_subtype, con_specialty
        FROM contacts_main, link_contacts
        WHERE link_contacts.inc_id = $IncID
        AND contacts_main.recordid = link_contacts.con_id";

    $ContactArray = DatixDBQuery::PDO_fetch_all($sql);

    foreach ($ContactArray as $con1)
    {
        $con[] = $con1;
    }
       // $con[$con1["link_type"]][] = $con1;

echo '
<tr><td colspan="2" class="windowbg2">
    <table class="titlebg" cellspacing="1" cellpadding="0" width="100%" align="center" border="0">
<tr><td class="titlebg">';

    echo '
<tr>
<td>
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';
    echo'
<tr>
    <td class="titlebg" colspan="' . $ColSpan . '">
    <b>' . $Header . '</b>
    </td>
</tr>
</table>';

    $IncludeColsList = "con_subtype,con_specialty";
    DoContactSection('Contacts', $ConType, $IncID, $con, $ColSpan, $EmailCheckBox, $FormType, $IncludeColsList);

    echo "</td></tr></table></td></tr>";
}

function DoContactSection($Header, $ConType, $IncID, $con, $ColSpan, $EmailCheckBox, $FormType = "Main",
                          $IncludeColsList)
{
    $Div = $Header;

    $IncludeCols = explode(",", $IncludeColsList);

    if ($_GET["print"])
    {
        $EmailCheckBox = false;
        $ColSpan--;
    }

    echo'
<tr height="20">
    <td class="titlebg" colspan="' . $ColSpan . '">
    <a href="Javascript:ToggleTwistyExpand(\'' . $ConType . '_' . $Div . '\', \'twisty_' . $ConType . '_' . $Div .
        '\',\'Images/expand.gif\', \'Images/collapse.gif\')">
    <img hspace="2" id="twisty_'. $ConType . '_' . $Div . '" src="Images/expand.gif" alt="*" border="0" /></a>
    <b>' . $Header . ' (' . count($con) . ')</b>
    </td>
</tr>
<tr>
<td>
<div id="' . $ConType . '_' . $Div .'" style="display:none">
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
<tr>
';
    if ($EmailCheckBox)
    {
        echo '<td class="windowbg" width="1%">
                    <input type="checkbox" id = "email_check" name="email_check" '.
                    'onclick="ToggleCheckAll(\'email_include_' . $ConType . '_' . $Div . '\', this.checked)"/>
                </td>';
    }

    echo '<td class="windowbg" width="25%">
                <b>' . _tk('name') . '</b>
            </td>';

    echo '</tr>';

    if (!$con)
    {
        echo '
<tr>
    <td class="windowbg2" colspan="' . $ColSpan . '">
    <b>' . _tk('no_contacts') . '</b>
    </td>
</tr>';
    }
    else
    {
        foreach ($con as $contact)
        {
            echo'
<tr>';
            if ($EmailCheckBox)
            {
                echo '<td class="windowbg2" width="1%">
                <input type="checkbox" id="email_include_' . $ConType . '_' . $Div . '" name="email_include_' .
                    $contact['con_id'] . '" onclick="" />
                </td>';
            }

            echo '<td class="windowbg2" valign="middle">';
            echo "$contact[con_title] $contact[con_forenames] $contact[con_surname]";
            if (!$_GET["print"])
            {
                echo "</a>";
            }
            echo '</td>';
            echo '</tr>';
        }
    }
    echo '</td></tr></table></div>';
}

function SectionSecureMessageHistory($inc, $FormType = "Main", $Module = "INC")
{
    require_once 'Source/messages/MessageHistory.php';
    ListMessageHistory(MOD_INCIDENTS, $inc, $FormType = "Main", "Secure message history");
}

function getDIF1OldValuesUDF($inc_num)
{
	$sql = "
	
		SELECT
			REPLACE(a.AUD_ACTION,'WEB:',''),
			(	SELECT TOP 1 AUD_DETAIL
				FROM full_audit 
				WHERE AUD_MODULE = 'INC' AND AUD_RECORD = :aud_record AND AUD_ACTION = a.AUD_ACTION
				ORDER BY AUD_DATE
			) as AUD_DETAIL
		FROM (SELECT DISTINCT AUD_ACTION, AUD_RECORD, AUD_MODULE FROM full_audit) a
		WHERE a.AUD_RECORD = :aud_record2 AND a.AUD_MODULE = 'INC' AND a.AUD_ACTION LIKE 'WEB:UDF_%'
	
	";
	return DatixDBQuery::PDO_fetch_all($sql, array(
        'aud_record' => $inc_num,
        'aud_record2' => $inc_num
    ), PDO::FETCH_KEY_PAIR);
}

?>