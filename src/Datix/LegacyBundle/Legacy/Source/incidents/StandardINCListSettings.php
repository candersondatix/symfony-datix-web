<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '4'),
    "inc_ourref" => array(
        'width' => '6'),
    "inc_name" => array(
        'width' => '10'),
    "inc_mgr" => array(
        'width' => '4'),
    "inc_dincident" => array(
        'width' => '7'),
    "inc_time" => array(
        'width' => '2'),
    "inc_locactual" => array(
        'width' => '10'),
    "inc_carestage" => array(
        'width' => '10'),
    "inc_type" => array(
        'width' => '10'),
    "inc_notes" => array(
        'width' => '48'),

);

$list_columns_mandatory = array("recordid", "inc_dincident");

$list_columns_extra = array(
    "recordid" => array(
        'dataalign' => 'left',
        'showextra' => 'dataalign: left',
    ),
);

