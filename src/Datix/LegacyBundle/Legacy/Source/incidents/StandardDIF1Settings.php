<?php
$GLOBALS['FormTitle'] = 'Datix Incident Form (DIF1)';

$GLOBALS["MandatoryFields"] = array(
	"inc_type" => "details",
	"inc_dincident" => "details",
	"inc_locactual" => "details",
	"inc_notes" => "details",
	"con_type" => "person",
	"con_type_1" => "witness",
	"con_type_2" => "witness2",
	"inc_repname" => "reporter",
    "link_type_4" => "contacts_type_L",
	"link_type_5" => "contacts_type_P",
);

$GLOBALS["HideFields"] = array(
    "rep_approved" => true,
    "inc_organisation" => true,
    "inc_unit" => true,
	"inc_clingroup" => true,
	"inc_directorate" => true,
	"inc_specialty" => true,
	"inc_loctype" => true,
	"Grading" => true,
	"con_dod" => true,
	"inc_result" => true,
	"inc_is_riddor" => true,
	"link_role" => true,
    "link_npsa_role_1" => true,
    "link_npsa_role_2" => true,
    "link_status_1" => true,
    "link_status_2" => true,
    "link_role_1" => true,
	"link_role_2" => true,
	"link_type_1" => true,
	"link_type_2" => true,
	"investigation" => true,
	"notepad" => true,
    "riddor" => true,
    "nrls" => true,
    "inc_report_npsa" => true,
    "inc_rc_required" => true,
    "pars" => true,
    "show_pars" => true,
    "inc_cnstitype" => true,
    "inc_notify" => true,
    "inc_injury" => true,
    "inc_bodypart" => true,
    "inc_report_npsa" => true,
    "inc_severity" => true,
    "inc_grade" => true,
    "severity" => true,
    "causes" => true,
    "feedback" => true,
    "linked_actions" => true,
    "history" => true,
    "pas" => true,
    'tprop' => true,
    'inc_time_band' => true,
    'causal_factor_header' => true,
    'causal_factor' => true,
    'dum_inc_grading_initial' => true,
    'dum_inc_grading' => true,
    'imed_price_admin' => true,
    'imed_price_correct' => true,
    'imed_reference_admin' => true,
    'imed_reference_correct' => true,
    'ccs2' => true,
    'inc_level_intervention' => true,
    'inc_level_harm' => true,
    'inc_never_event' => true,
    'openness_transparency' => true,
    "imed_other_factors" => true,
    "imed_right_wrong_medicine_admin" => true,
    "imed_right_wrong_medicine_correct" => true,
    "imed_manufacturer_special_admin" => true
);

$GLOBALS["UserExtraText"] = array (
  'inc_notes' => 'Enter facts, not opinions.  Do not enter names of people',
  'inc_actiontaken' => 'Enter action taken at the time of the incident',
  'inc_rep_email' => 'Please enter a valid e-mail address so that an acknowledgment can be sent to you',
);

$GLOBALS["DefaultValues"] = array(
	"link_role_1" => "WITN",
	"link_role_2" => "WITN",
	"link_type_1" => "N",
	"link_type_2" => "N",
    "link_type_5" => "N",
    'rep_approved' => 'AWAREV',
);

$GLOBALS["HelpTexts"] = array (
  'inc_type' => '<b>Field: Incident Type</b><br /><br />Please make a selection from the dropdown list.<br /><br />The Incident Type indicates the type of party affected by the incident.',
  'inc_dincident' => '<b>Field: Incident Date</b><br /><br />Please enter the date on which the incident occurred in the format '. $_SESSION["DATE_FORMAT"] .'.  You can click on the Calendar button if you want to choose the date from a calendar window.',
  'show_person' => 'Tick this box if anyone was injured or affected by the incident, or if this was a near miss that would have affected someone.',
);

$GLOBALS["ExpandSections"] = array (
  'show_person' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_A',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_witness' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_W',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_employee' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_E',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_other_contacts' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_N',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_equipment' =>
  array (
    0 =>
    array (
      'section' => 'equipment',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_medication' =>
  array (
    0 =>
    array (
      'section' => 'medication',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_document' =>
  array (
    0 =>
    array (
      'section' => 'extra_document',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_pars' =>
  array (
    0 =>
    array (
      'section' => 'pars',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_causal_factors_linked' =>
  array (
    0 =>
    array (
      'section' => 'causal_factor',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_report_npsa' =>
  array (
    0 =>
    array (
      'section' => 'nrls',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_is_riddor' =>
  array (
    0 =>
    array (
      'section' => 'riddor',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_pol_attend' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_P',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'show_assailant' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_L',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_tprop_damaged' =>
  array (
    0 =>
    array (
      'section' => 'tprop',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
);

$GLOBALS["ExpandFields"] = array (
    'inc_ot_q1' => array (
        0 => array (
            'field' => 'inc_ot_q2',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        1 => array (
            'field' => 'inc_ot_q3',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        2 => array (
            'field' => 'inc_ot_q4',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        3 => array (
            'field' => 'inc_ot_q5',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        4 => array (
            'field' => 'inc_ot_q6',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        5 => array (
            'field' => 'inc_ot_q7',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        6 => array (
            'field' => 'inc_ot_q8',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        7 => array (
            'field' => 'inc_ot_q9',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        8 => array (
            'field' => 'inc_ot_q10',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        9 => array (
            'field' => 'inc_ot_q11',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        10 => array (
            'field' => 'inc_ot_q12',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        11 => array (
            'field' => 'inc_ot_q13',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        12 => array (
            'field' => 'inc_ot_q14',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        13 => array (
            'field' => 'inc_ot_q15',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
                1 => 'N',
            ),
        ),
        14 => array (
            'field' => 'inc_ot_q16',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        15 => array (
            'field' => 'inc_ot_q17',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        16 => array (
            'field' => 'inc_ot_q18',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        17 => array (
            'field' => 'inc_ot_q19',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        18 => array (
            'field' => 'inc_ot_q20',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
    ),
);

?>