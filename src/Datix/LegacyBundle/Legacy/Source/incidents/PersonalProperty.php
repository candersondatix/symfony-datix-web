<?php

/**
 * @desc This function is called when a form containing the personal property section is loaded. It
 * contstructs the table and fills it with any data present.
 *
 * @param array $data Data array for current record.
 * @param string $FormType Type of form (e.g. Print, ReadOnly, Search...)
 * @param string $module Current module
 * @param int $suffix Suffix of contact this section will be attached to
 * @param string $section Not currently used, but may be needed for form design options for this section
 *
 */
function SectionPropertyDetails($data, $FormType, $module, $suffix, $section = '')
{
    global $HideFields, $UserLabels;

    $HTML = '
    <div class="new_windowbg2">
';

    $RowSuffix = 1;
    $SuffixString = (is_integer((int) $suffix) && $suffix > 0 ? '_'.$suffix : '');

    $Table = new FormTable($FormType);

    if(!$data["property_table"])
    {
        //from post, rather than database
        if(count($data['ipp_description'.$SuffixString])>1 || $data['ipp_description'.$SuffixString][0] != '')
        {
            $PropertyArray = array();
            foreach($data['ipp_description'.$SuffixString] as $i => $desc)
            {
                if($desc!="")
                {
                    $PropertyArray[] = array(
                        'ipp_damage_type' => Sanitize::SanitizeString($data['ipp_damage_type'.$SuffixString][$i]), //For use, see below @ foreach($data["property_table"] as $Property)
                        'ipp_description' => Sanitize::SanitizeString($data['ipp_description'.$SuffixString][$i]), //For use, see below @ foreach($data["property_table"] as $Property)
                        'ipp_value' => Sanitize::SanitizeString($data['ipp_value'.$SuffixString][$i]), //For use, see below @ foreach($data["property_table"] as $Property)
                    );
                }
            }
            $data["property_table"] = $PropertyArray;
        }
    }

    if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked' && !($injuryReadOnly && $bodyReadOnly))
    {
        $DescTitle = FirstNonNull(array($UserLabels['dum_description'.$SuffixString], Labels_FormLabel::GetFormFieldLabel('ipp_description')));
        $TypeTitle = FirstNonNull(array($UserLabels['dum_property_type'.$SuffixString], Labels_FormLabel::GetFormFieldLabel('ipp_damage_type')));
        $ValueTitle = FirstNonNull(array($UserLabels['dum_value'.$SuffixString], Labels_FormLabel::GetFormFieldLabel('ipp_value')));

        $HTML .= '
        <table id="property_table'.$SuffixString.'" class="bordercolor" cellspacing="1" cellpadding="5" align="left" border="0">
        <tr>';

        if (!$HideFields['dum_description'.$SuffixString])
        {
            $HTML .= '
            <td class="titlebg" align="left" width="250">
            <b>' . $DescTitle . '</b>
            </td>';
        }

        if (!$HideFields['dum_property_type'.$SuffixString])
        {
            $HTML .= '
            <td class="titlebg" align="left" width="250">
            <b>' . $TypeTitle . '</b>
            </td>';
        }

        if (!$HideFields['dum_value'.$SuffixString])
        {
            $HTML .= '
            <td class="titlebg" align="left" width="80">
            <b>' . $ValueTitle . '</b>
            </td>';
        }

        $HTML .= '
            <td class="titlebg" align="left" width="50">
            </td>
        </tr>
        ';
    }

    //ifthere is data

    if(!empty($data["property_table"]) && is_array($data["property_table"]))
    {
        foreach($data["property_table"] as $Property)
        {
            if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked')
            {
                $HTML .= MakePropertyRow(array(
                    'row_suffix' => $RowSuffix,
                    'person_suffix' => $suffix,
                    'formtype' => $FormType,
                    'data' => $Property,
                ));
            }
            else
            {
                $HTML .= '<div style="padding-bottom:5px">'.code_descr('INC', 'ipp_damage_type', $Property["ipp_damage_type"]);
                if($Property["ipp_value"])
                {
                    $HTML .= ' (' . FormatMoneyVal($Property["ipp_value"]).')';
                }
                $HTML .= '<div>'.$Property["ipp_description"].'</div></div>';
            }
            $RowSuffix++;
        }
    }
    else if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked')
    {
        $HTML .= MakePropertyRow(array(
            'row_suffix'=>1,
            'person_suffix'=>$suffix,
            'formtype' => $FormType,
            //'hidden' => array('injury' => $injuryHidden, 'body' => $bodyHidden),
            //'readonly' => array('injury' => $injuryReadOnly, 'body' => $bodyReadOnly)
        ));
    }

    if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked' && !(($injuryReadOnly && $bodyReadOnly) || ($injuryReadOnly && $bodyHidden) || ($injuryHidden && $bodyReadOnly)))
    {
        $HTML .= '<tr><td colspan="4" class="windowbg2"><input type="button" value="'._tk('add_another_item').'" style="align:right" nextSuffix="'.($RowSuffix+1).'" onclick="addProperty(this, \''.$suffix.'\', \'E\')" /></td></tr>';
    }

    if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked' && !($injuryReadOnly && $bodyReadOnly))
    {
        $HTML .= '</table>';
    }

    $HTML .= '</div>';

    $PropertyObj = new FormField($FormType);
    $PropertyObj->MakeCustomField($HTML);
    $Table->MakeRow('<b>'._tk('personal_property').'</b>', $PropertyObj);

    echo $Table->GetFormTable();
}

/**
 * @desc Collates HTML for a table row for the property table. Called on pageload.
 *
 * @param array $aParams Array of parameters
 * @param string $aParams['person_suffix'] Suffix of the contact this property is linked to.
 * @param string $aParams['row_suffix'] Suffix of this piece of property.
 *
 * @return string The HTML for a property table row
 */
function MakePropertyRow($aParams)
{
    $SuffixString = (is_integer($aParams['person_suffix']) ? '_'.$aParams['person_suffix'].'_'.$aParams['row_suffix'] : '_'.$aParams['row_suffix']);

    $RowHTML = '<tr id="property_row'.$SuffixString.'" name="property_row'.$SuffixString.'" class="property_row">
        <td class="windowbg2">';

    $RowHTML .= implode('</td><td class="windowbg2">',constructPropertyCells($aParams));

    $RowHTML .= '</td>
    </tr>';

    return $RowHTML;
}

/**
 * @desc Pushes HTML for each field in a property table row into an array to return.
 *
 * @param array $aParams Array of parameters
 * @param string $aParams['person_suffix'] Suffix of the contact this property is linked to.
 * @param string $aParams['row_suffix'] Suffix of this piece of property.
 * @param string $aParams['data'] Data array for current record
 *
 * @return array Array with one entry for each table cell - contains the HTML for the control within each one.
 */
function constructPropertyCells($aParams)
{
    global $MandatoryFields, $HideFields, $ReadOnlyFields, $JSFunctions, $UserLabels;

    $PropertyCells = array();

    $row_suffix = $aParams['row_suffix'];
    $person_suffix = $aParams['person_suffix'];

    $ShortSuffixString = ($person_suffix ? '_'.$person_suffix : '');
    $SuffixString = ($person_suffix ? '_'.$person_suffix.'_'.$row_suffix : '_'.$row_suffix);

    $DescTitle = FirstNonNull(array($UserLabels['dum_description'.$ShortSuffixString], Labels_FormLabel::GetFormFieldLabel('ipp_description')));
    $TypeTitle = FirstNonNull(array($UserLabels['dum_property_type'.$ShortSuffixString], Labels_FormLabel::GetFormFieldLabel('ipp_damage_type')));
    $ValueTitle = FirstNonNull(array($UserLabels['dum_value'.$ShortSuffixString], Labels_FormLabel::GetFormFieldLabel('ipp_value')));

    if(!$HideFields['dum_description'.$ShortSuffixString])
    {
        if (!$ReadOnlyFields['dum_description'.$ShortSuffixString])
        {
            $DescObj = new FormField($aParams['formtype']);
            //need a different name and ID to allow collection of data from discrete fields.
            $DescObj->MakeCustomField('<textarea id="ipp_description'.$SuffixString.'" name="ipp_description'.$ShortSuffixString.'[]" rows="2" cols="31">'.$aParams['data']['ipp_description'].'</textarea>');
            $DescField = $DescObj->GetField();
        }
        else
        {
            $DescField = $aParams['data']['ipp_description'];
            if(!$DescField)
            {
                $DescField = '&nbsp;';
            }
        }

        if($MandatoryFields['dum_description'.$ShortSuffixString])
        {
            $DescField = '<img src="Images/warning.gif" style="float:left" /> '.$DescField.'<div class="field_error" id="erripp_description'.$SuffixString.'" style="display:none">'._tk('mandatory_err').'</div>';
            $JSFunctions[] = 'if(mandatoryArray){mandatoryArray.push(new Array("ipp_description'.$SuffixString.'","property_section'.$ShortSuffixString.'","'.$DescTitle.($person_suffix ? ' (Contact #'.$person_suffix.')' : '' ).'"))};';
        }

        $PropertyCells[] = $DescField;
    }

    if(!$HideFields['dum_property_type'.$ShortSuffixString])
    {
        if (!$ReadOnlyFields['dum_property_type'.$ShortSuffixString])
        {
            $TypeObj = Forms_SelectFieldFactory::createSelectField('ipp_damage_type', 'INC', $aParams['data']['ipp_damage_type'], $aParams['formtype'], false, $DescTitle, 'property_section', $aParams['data']['CHANGED-ipp_damage_type'], $SuffixString);
            $TypeObj->setAltFieldName('ipp_damage_type'.$SuffixString);
            $TypeObj->setOnChangeExtra('jQuery(\'#hidden_damage_type'.$SuffixString.'\').val(jQuery(\'#ipp_damage_type'.$SuffixString.'\').val());');
            $TypeField = $TypeObj->GetField();
        }
        else
        {
            $TypeField = code_descr('INC', 'ipp_damage_type', $aParams['data']['ipp_damage_type']);
        }

        $TypeField .= '<input type="hidden" id="'."hidden_damage_type".$SuffixString.'" name="ipp_damage_type'.$ShortSuffixString.'[]" value="'.$aParams['data']['ipp_damage_type'].'" />';

        if($MandatoryFields['dum_property_type'.$ShortSuffixString])
        {
            $TypeField = '<img src="Images/warning.gif" style="float:left" /> '.$TypeField.'<div class="field_error" id="erripp_damage_type'.$SuffixString.'" style="display:none">'._tk('mandatory_err').'</div>';
            $JSFunctions[] = 'if(mandatoryArray){mandatoryArray.push(new Array("ipp_damage_type'.$SuffixString.'","property_section'.$ShortSuffixString.'","'.$TypeTitle.($person_suffix ? ' (Contact #'.$person_suffix.')' : '' ).'"))};';
        }

        $PropertyCells[] = $TypeField;
    }

    if(!$HideFields['dum_value'.$ShortSuffixString])
    {
        if (!$ReadOnlyFields['dum_value'.$ShortSuffixString])
        {
            $ValueObj = new FormField($aParams['formtype']);
            $ValueObj->MakeCustomField('<input type="text" name="ipp_value'.$ShortSuffixString.'[]" id="ipp_value'.$SuffixString.'" size="5" maxlength="11" value="' . $aParams['data']['ipp_value'] . '" />');
            $ValueField = $ValueObj->GetField();
        }
        else
        {
            $ValueField = $aParams['data']['ipp_value'];
            if(!$ValueField)
            {
                $ValueField = '&nbsp;';
            }
        }

        if($MandatoryFields['dum_value'.$ShortSuffixString])
        {
            $ValueField = '<img src="Images/warning.gif" style="float:left" /> '.$ValueField.'<div class="field_error" id="erripp_value'.$SuffixString.'" style="display:none">'._tk('mandatory_err').'</div>';
            $JSFunctions[] = 'if(mandatoryArray){mandatoryArray.push(new Array("ipp_value'.$SuffixString.'","property_section'.$ShortSuffixString.'","'.$ValueTitle.($person_suffix ? ' (Contact #'.$person_suffix.')' : '' ).'"))};';
        }

        $PropertyCells[] = $ValueField;
    }

    $HiddenID = '<input type="hidden" name="ipp_recordid'.$ShortSuffixString.'[]" value="'.$aParams['data']['recordid'].'" />';

    if($row_suffix != 1 && !($aParams['readonly']['injury'] && $aParams['readonly']['body']))
    {
        $PropertyCells[] = $HiddenID.'<input type="button" name="btnAdd" value="Delete" onclick="deletePropertyRow('.$row_suffix.', '.($person_suffix ? $person_suffix : '0').')" />';
    }
    else
    {
        $PropertyCells[] = $HiddenID;
    }

    return $PropertyCells;
}

/**
 * @desc Called from httprequest.php when we need to insert a new property row via ajax. Echos the html for the new
 * row in json format.
 */
function printPropertyRow()
{
    global $ContactForms;
    require_once 'Source/libs/FormClasses.php';

    // This is not passed on a request (maybe we can remove it?)
    $aParams['data']['link_injury'] = $_GET["default_inj"];
    // This is not passed on a request (maybe we can remove it?)
    $aParams['data']['link_bodypart'] = $_GET["default_bpt"];
    $aParams['row_suffix'] = Sanitize::SanitizeInt($_GET['row_suffix']);
    $aParams['person_suffix'] = Sanitize::SanitizeInt($_GET['person_suffix']);
    $aParams['linktype'] = Sanitize::SanitizeString($_GET['linktype']);

    header('Content-type: application/x-json');

    if (empty($ContactForms) && file_exists($_SESSION['LASTUSEDFORMDESIGN']))
    {
        include($_SESSION['LASTUSEDFORMDESIGN']);
    }

    //bit of a hack, but we need to know whether we're looking at a dif1 or dif2
    if (mb_substr_count($_SESSION['LASTUSEDFORMDESIGN'], 'DIF2'))
    {
        $FormLevel = 2;
    }
    else
    {
        $FormLevel = 1;
    }

    $FileName = GetContactFormSettings($aParams['linktype'], $FormLevel, 'INC');

    AddSuffixToFormDesign(array('filename' => $FileName, 'suffix' => $aParams['person_suffix'], 'module' => ''));

    $SuffixString = ($aParams['person_suffix'] ? '_'.$aParams['person_suffix'] : '');

    $JSONdata['cells'] = constructPropertyCells($aParams);
    $JSONdata['row_suffix'] = $aParams['row_suffix'];
    $JSONdata['person_suffix'] = $aParams['person_suffix'];
    $JSONdata['js'] = getJSFunctions();

    //encode and return json data...
    echo json_encode($JSONdata);
}

/**
 * @desc Called when contact is saved - pushes data from $_POST into the inc_personal_property table.
 */
function SaveProperty($aParams)
{
    if($aParams['module'] == 'INC') //no support for other modules yet.
    {
        $SuffixString = ($aParams['suffix'] ? '_'.$aParams['suffix'] : '');

        $UpdatedRecords = array(); //keeps track of ids which have been re-saved. Any not in this list (i.e. not posted) will be deleted.

        if ($_POST["ipp_description".$SuffixString] != "")
        {
            foreach($_POST["ipp_description".$SuffixString] as $ref => $Desc)
            {
                if (empty($_POST["ipp_value".$SuffixString][$ref]))
                {
                    $_POST["ipp_value".$SuffixString][$ref] = null;
                }

                if($Desc)
                {
                    $Listorder++;

                    if($_POST["ipp_recordid".$SuffixString][$ref])
                    {
                        $sql = 'UPDATE inc_personal_property SET
                                listorder = :listorder,
                                ipp_description = :ipp_description,
                                ipp_damage_type = :ipp_damage_type,
                                ipp_value = :ipp_value,
                                updateddate = :updateddate,
                                updatedby = :updatedby
                                WHERE
                                recordid = :recordid';

                        DatixDBQuery::PDO_query($sql,
                            array(
                                'listorder' => $Listorder,
                                'ipp_description' => Sanitize::SanitizeString($_POST["ipp_description".$SuffixString][$ref]),
                                'ipp_damage_type' => Sanitize::SanitizeString($_POST["ipp_damage_type".$SuffixString][$ref]),
                                'ipp_value' => Sanitize::SanitizeString($_POST["ipp_value".$SuffixString][$ref]),
                                'updateddate' => date('Y-m-d H:i:s.000'),
                                'updatedby' => $_SESSION['initials'],
                                'recordid' => Sanitize::SanitizeInt($_POST["ipp_recordid".$SuffixString][$ref]),
                            )
                        );

                        $UpdatedRecords[] = Sanitize::SanitizeInt($_POST["ipp_recordid".$SuffixString][$ref]);
                    }
                    else
                    {
                        $recordid = GetNextRecordID("inc_personal_property");

                        DatixDBQuery::PDO_build_and_insert('inc_personal_property',
                            array(
                                'recordid' => $recordid,
                                'inc_id' => $aParams['main_recordid'],
                                'con_id' => $aParams['recordid'],
                                'listorder' => $Listorder,
                                'ipp_description' => $_POST["ipp_description".$SuffixString][$ref],
                                'ipp_damage_type' => $_POST["ipp_damage_type".$SuffixString][$ref],
                                'ipp_value' => $_POST["ipp_value".$SuffixString][$ref],
                                'updateddate' => date('Y-m-d H:i:s.000'),
                                'updatedby' => $_SESSION['initials'],

                            )
                        );

                        $UpdatedRecords[] = $recordid;
                    }
                }
            }
        }

        $sql = 'DELETE FROM inc_personal_property
        WHERE inc_id = :inc_id
        AND con_id = :con_id';

        if(!empty($UpdatedRecords))
        {
            $sql .= ' AND recordid NOT IN ('.implode(', ',$UpdatedRecords).')';
        }

        DatixDBQuery::PDO_query($sql, array('inc_id' => $aParams['main_recordid'], 'con_id' => $aParams['recordid']));
    }
}