<?php

$GLOBALS['FormTitle'] = 'Datix Incident Form (DIF2)';
$GLOBALS["MandatoryFields"] = array();

$GLOBALS["ReadOnlyFields"] = array (
  'reportedby' => true,
);
$GLOBALS["ExpandSections"] = array (
  'rep_approved' =>
  array (
    0 =>
    array (
      'section' => 'rejection',
      'alerttext' => 'Please complete the \'Details of rejection\' section before saving this form.',
      'values' =>
      array (
        0 => 'REJECT',
      ),
    ),
  ),
  'inc_report_npsa' =>
  array (
    0 =>
    array (
      'section' => 'nrls',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_is_riddor' =>
  array (
    0 =>
    array (
      'section' => 'riddor',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_tprop_damaged' =>
  array (
    0 =>
    array (
      'section' => 'tprop',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
  'inc_causal_factors_linked' =>
  array (
    0 =>
    array (
      'section' => 'causal_factor',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
);

$GLOBALS["HideFields"] = array(
    "inc_injury" => true,
    "inc_bodypart" => true,
    "additional" => true,
    "pas" => true,
    'progress_notes' => true,
    'rejection_history' => true,
    'inc_time_band' => true,
    'causal_factor_header' => true,
    'causal_factor' => true,
    'payments' => true,
    'action_chains' => true,
    'dum_inc_grading_initial' => true,
    'dum_inc_grading' => true,
    'imed_price_admin' => true,
    'imed_price_correct' => true,
    'imed_reference_admin' => true,
    'imed_reference_correct' => true,
    'ccs2' => true,
    'inc_level_intervention' => true,
    'inc_level_harm' => true,
    'inc_submittedtime' => true,
    'inc_never_event' => true,
    'inc_last_updated' => true,
    'openness_transparency' => true,
    "imed_other_factors" => true,
    "imed_right_wrong_medicine_admin" => true,
    "imed_right_wrong_medicine_correct" => true,
    "imed_manufacturer_special_admin" => true
);

$GLOBALS["UserExtraText"] = array (
    'inc_notes' => 'Enter facts, not opinions.  Do not enter names of people',
    'inc_actiontaken' => 'Enter action taken at the time of the incident',
    'dum_fbk_to' => 'Only staff and contacts with e-mail addresses are shown.',
    'dum_fbk_gab' => 'Only users with e-mail addresses are shown.',
    'dum_fbk_email' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'
);

$GLOBALS["DefaultValues"] = array();

$GLOBALS["HelpTexts"] = array (
  'inc_type' => '<b>Field: Incident Type</b><br /><br />Please make a selection from the dropdown list.<br /><br />The Incident Type indicates the type of party affected by the incident.',
  'inc_dincident' => '<b>Field: Incident Date</b><br /><br />Please enter the date on which the incident occurred in the format '. $_SESSION["DATE_FORMAT"] .'.  You can click on the Calendar button if you want to choose the date from a calendar window.',
);

$GLOBALS["NewPanels"] = array (
    'medication' => true,
    'investigation' => true,
    'feedback' => true,
    'linked_records' => true,
    'contacts_type_A' => true,
    'causes' => true,
    'notepad' => true,
    'progress_notes' => true,
    'linked_actions' => true,
    'documents' => true,
    'equipment' => true,
    'pars' => true,
    'pas' => true,
    'history' => true,
    'message' => true,
    'word' => true,
    'permissions_user' => true,
    'rejection' => true,
    'payments' => true,
    'action_chains' => true,
);

$GLOBALS["ExpandFields"] = array (
    'inc_ot_q1' => array (
        0 => array (
            'field' => 'inc_ot_q2',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        1 => array (
            'field' => 'inc_ot_q3',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        2 => array (
            'field' => 'inc_ot_q4',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        3 => array (
            'field' => 'inc_ot_q5',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        4 => array (
            'field' => 'inc_ot_q6',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        5 => array (
            'field' => 'inc_ot_q7',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        6 => array (
            'field' => 'inc_ot_q8',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        7 => array (
            'field' => 'inc_ot_q9',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        8 => array (
            'field' => 'inc_ot_q10',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        9 => array (
            'field' => 'inc_ot_q11',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        10 => array (
            'field' => 'inc_ot_q12',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        11 => array (
            'field' => 'inc_ot_q13',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        12 => array (
            'field' => 'inc_ot_q14',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        13 => array (
            'field' => 'inc_ot_q15',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
                1 => 'N',
            ),
        ),
        14 => array (
            'field' => 'inc_ot_q16',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        15 => array (
            'field' => 'inc_ot_q17',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        16 => array (
            'field' => 'inc_ot_q18',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        17 => array (
            'field' => 'inc_ot_q19',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
        18 => array (
            'field' => 'inc_ot_q20',
            'alerttext' => '',
            'values' => array (
                0 => 'Y',
            ),
        ),
    ),
);

?>
