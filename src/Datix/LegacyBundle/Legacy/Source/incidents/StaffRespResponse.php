<?php
// output
function StaffRespResponse()
{
    global $scripturl,$FieldDefs;

    $initial = $_REQUEST["initial"];
    if($_REQUEST["print"] == "1")
        $PrintMode = true;

    // Filter incidents by the given (clicked) user's where clause
    $where = MakeSecurityWhereClause("", "INC", $initial, "", "");

    // Filter incidents by the logged in user's where clause
    //Since we now allow UDFs in default listings, the quick fix for this area works as such: UDF fields are manually
    //removed from the listing, if the lsting then only has 0 or 1 columns left then we use 4 default fields: Recordid,
    //approval status, manager & description.
    $selectfields = preg_replace('/UDF_[A-Z]__[0-9]+[,]*/', '', $_SESSION["INC"]["staffrespresponse"]["selectfields"]);
    $selectfields = rtrim($selectfields,', ');
    $list_columns = $_SESSION["INC"]["staffrespresponse"]["list_columns"];
    foreach ($list_columns as $key => $val)
    {
        if (preg_match('/UDF_[A-Z]__[0-9]+/', $key) == 1)
        {
            unset($list_columns[$key]);
        }
    }
    if(count($list_columns) < 2)
    {
        $selectfields = ('recordid, rep_approved, inc_mgr, inc_description');
        $list_columns = array('recordid' => 'recordid', 'rep_approved' => 'rep_approved', 'inc_mgr' => 'inc_mgr', 'inc_notes' => 'inc_notes');
    }

    $output .= '<table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">';

    if($where)
    {
        $query = new \src\framework\query\Query;
        $query->select(explode(', ', $selectfields))->from('incidents_main');

        $sql = "SELECT $selectfields FROM incidents_main WHERE";
        $wherestring = " rep_approved IN ('AWAREV')";
        $wherestring .= ($where != "") ? " AND " . $where : '';
        $wherestring .= ($inc_where != "") ? " AND " . $inc_where : '';

        $query->setWhereStrings(array($wherestring));
        $query->orderBy(array("recordid"));

        $sqlWriter = new \src\framework\query\SqlWriter();
        list($sql, $params) = $sqlWriter->writeStatement($query);
        $resultArray = DatixDBQuery::PDO_fetch_all($sql, $params);

        // Columns
        if (count($resultArray) == 0)
        {
            $output .= '<tr><td class="windowbg2" colspan="'.count($list_columns).'">No access to incidents or no incidents in the holding area.</td></tr>';
        }
        else
        {
            $output .= '
            <tr>
                <td class="windowbg" colspan="'.count($list_columns).'">
                <b>' . count($resultArray) . ' records found.</b>
                </td>
            </tr>
            <tr class="tableHeader head2">';

            if (is_array($list_columns))
            {
                $module = "INC";

                foreach ($list_columns as $col_field => $col_info)
                {
                    $output .=  '<th class="windowbg"' . ($col_info['width'] ? 'width="' . $col_info['width'] . '%"' : '') . '>';
                    if($col_field == "recordid")
                    {
                        if($col_fieldinfo_extra['colrename'])
                            $currentCols[$col_field] = $col_fieldinfo_extra['colrename'];
                        else
                            $currentCols[$col_field] = GetFieldLabel($col_field, $FieldDefs[$module][$col_field]["Title"]);
                    }
                    else if(is_array($GLOBALS["UserLabels"]) && array_key_exists($col_field, $GLOBALS["UserLabels"]))
                    {
                        $currentCols[$col_field] = $GLOBALS["UserLabels"][$col_field];
                    }
                    else
                    {
                        $currentCols[$col_field] = GetFieldLabel($col_field, $FieldDefs[$module][$col_field]["Title"]);
                    }
                    $output .=  '<b>' . $currentCols[$col_field] . '</b>';
                    if ($FieldDefs[$module][$col_field]['Type'] != 'textarea')
                        $output .=  '</a>';
                    $output .=  '</th>';
                }
            }

            $output .=  '</tr><tr>';

            foreach ($resultArray as $row)
            {
                foreach ($list_columns as $col_field => $col_info_extra )
                {
                            $output .= '<td class="windowbg2" valign="middle"';

                            if ($col_info_extra['dataalign'])
                                 $output .= ' align="' . $col_info_extra['dataalign'] . '"';
                            $output .= '>';

                            if (!$PrintMode)
                                $output .= '<a href="'.$scripturl.'?action=incident&recordid='.$row["recordid"].'">';

                            if ($col_field == 'recordid')
                            {
                                if ($_SESSION["Globals"]["DIF_1_REF_PREFIX"])
                                    $output .= $_SESSION["Globals"]["DIF_1_REF_PREFIX"];
                                $output .= $row[$col_field];
                            }
                            elseif ($FieldDefs[$module][$col_field]['Type'] == 'ff_select')
                            {
                                $output .= code_descr($module, $col_field, $row[$col_field]);
                            }
                            elseif ($FieldDefs[$module][$col_field]['Type'] == 'date')
                            {
                                $output .= FormatDateVal($row[$col_field]);
                            }
                            elseif ($FieldDefs[$module][$col_field]['Type'] == 'textarea')
                            {
                                $output .= htmlspecialchars($row[$col_field]);
                            }
                            elseif ($FieldDefs[$module][$col_field]['Type'] == 'time')
                            {
                                if (\UnicodeString::strlen($row[$col_field]) == 4)
                                    $row[$col_field] = $row[$col_field]{0} . $row[$col_field]{1} . ":" . $row[$col_field]{2} . $row[$col_field]{3};

                                $output .= $row[$col_field];
                            }
                            else
                            {
                                $output .= $row[$col_field];
                            }

                            if (!$PrintMode)
                                $output .= '</a>';

                            $output .= '</td>';
                }
                $output .= '</tr>';
            }
        }
    }   // end of if($where)
    // staff can see all the unapproved incidents in the system
    else
    {
        $output .= '<tr><td class="windowbg2" colspan="'.count($list_columns).'">Access to all incidents</td></tr>';
    }

    $output .= '</table>';
    
    echo $output;
}

?>
