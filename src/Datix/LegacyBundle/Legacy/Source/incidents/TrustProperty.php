<?php

/**
* @desc Similar to other functions for contacts and documents - this works out the suffixes amd echos
* at least one dynamic equipment form, with an "add another" button with which to generate more.
*
* @param array $aParams array of parameters.
* @param array $aParams['data'] Current record data.
* @param string $aParams['module'] Current module.
*/
function MakeDynamicEquipmentSection($aParams)
{
    global $MaxSuffixField_AST, $ModuleDefs, $MaxSuffix_AST;

    $Data = $aParams['data']; //need to check whether there are multiple sections to put into the form.
    $module = $aParams['module'];

    if (!empty($Data['ast']) && !$Data['temp_record']) // logged in
    {
        $NumToShow = count($Data['ast'][$aParams['contacttype']]);

        if (!$MaxSuffixField_AST)
        {
            $MaxSuffixField_AST = true;
            echo '<input type="hidden" name="equipment_max_suffix" id="equipment_max_suffix" value="'.(count($Data['ast'])).'" />';
        }

        if (!$NumToShow)
        {
            if ($aParams['FormType'] != 'Print' && $aParams['FormType'] != 'ReadOnly')
            {
                $NumToShow = 1;
            }
        }
    }
    else
    {
        if ($Data['equipment_max_suffix'])
        {
            $EquipmentToShow = GetPostedEquipment($Data);
            $aParams['suffixpresent'] = true; //prevent additional suffix being added.
            $NumToShow = count($EquipmentToShow);
        }
        elseif ($Data['temp_record'])
        {
            $EquipmentToShow = $Data['ast'][$aParams['equipmenttype']];
            $NumToShow = count($EquipmentToShow);
        }

        if (!$NumToShow)
        {
            $EquipmentToShow[] = $aParams['suffix'];

            if ($aParams['FormType'] != 'Print' && $aParams['FormType'] != 'ReadOnly')
            {
                $NumToShow = 1;
            }
        }

        if (!$MaxSuffixField_AST)
        {
            $MaxSuffixField_AST = true;
            echo '<input type="hidden" name="equipment_max_suffix" id="equipment_max_suffix" value="'.($Data['equipment_max_suffix'] ? Sanitize::SanitizeInt($Data['equipment_max_suffix']) : '2').'" />';
        }
    }

    if ($MaxSuffix_AST)
    {
        $MaxSuffix_AST += $NumToShow;
    }
    else
    {
        $MaxSuffix_AST = 1;
    }

    for ($i = 0; $i < $NumToShow; $i++)
    {
        if (($_SESSION['logged_in'] && !isset($Data['error']) && !isset($Data['dif1_snapshot'])) || $Data['temp_record'])
        {
            $aParams['data'] = $Data['ast'][$aParams['equipmenttype']][$i];

            if ($i != 0 || !$aParams['suffix']) //first contact should take the default suffix
            {
                $aParams['suffix'] = $i + $MaxSuffix;
            }
        }
        else
        {
            $aParams['suffix'] = $EquipmentToShow[$i];
        }

        if ($i == 0)
        {
            $aParams['clear'] = true;
        }

        echo '

        <div id="equipment_section_div_'.$aParams['suffix'].'">';

        get_equipment_section($aParams);

        echo '</div>';
    }

    if ($aParams['FormType'] != 'ReadOnly' && $aParams['FormType'] != 'Print' )
    {
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        echo '
        <li class="new_windowbg" id="add_another_equipment_button_list">
            <input type="button" id="add_another_equipment_button" value="'._tk('btn_add_another').'" onclick="AddSectionToForm(\'equipment\', \'\', $(\'add_another_equipment_button_list\'), \''.htmlspecialchars($aParams['module']).'\', \'\', false, '.$spellChecker.', '.(isset($_REQUEST['form_id']) ? Sanitize::SanitizeInt($_REQUEST['form_id']) : 'null').');">
        </li>';
    }
}

/**
* @desc Similar to other functions for contacts and documents - this constructs a self-contained equipment form
* which can be inserted into the DIF1 either on load, or with data already populated.
*
* @param array $aParams array of parameters.
* @param array $aParams['data'] Current record data.
* @param string $aParams['module'] Current module.
* @param string $aParams['FormType'] Current form type (e.g. New, Edit, ReadOnly, Print...).
* @param string $aParams['suffix'] Suffix to give to the fields of this equipment section to distinguish it from others on the same page.
* @param bool $aParams['clear'] Indicates that this section should have an option to remove all data from it, while keeping the section in place.
*/
function get_equipment_section($aParams = array())
{
    global $ClientFolder, $FormDesigns, $ModuleDefs, $JSFunctions;

    $FormType = $aParams['FormType'];
    $data = ($aParams['data']? $aParams['data'] : array());
    $AJAX = (empty($aParams));

    if ($AJAX)
    {
        $aParams = Sanitize::SanitizeRawArray($_POST);
    }

    $Module = Sanitize::getModule($aParams['module']);
    $Suffix = Sanitize::SanitizeString($aParams["suffix"]);
    $clear = Sanitize::SanitizeString($aParams["clear"]);

    if ($Suffix && !$aParams['suffixpresent'] && !empty($data))
    {
        $data = AddSuffixToData($data, $Suffix,
            array(
                'Rows'=>array(
                    'link_damage_type',
                    'link_other_damage_info',
                    'link_replaced',
                    'link_replace_cost',
                    'link_repaired',
                    'link_repair_cost',
                    'link_written_off',
                    'link_disposal_cost',
                    'link_sold',
                    'link_sold_price',
                    'link_decommissioned',
                    'link_decommission_cost',
                    'link_residual_value',
                    'recordid', 'updateid','ast_name', 'ast_ourref', 'ast_organisation', 'ast_unit',
                    'ast_clingroup', 'ast_directorate', 'ast_directorate', 'ast_specialty',
                    'ast_loctype', 'ast_locactual', 'ast_type', 'ast_product',
                    'ast_model', 'ast_manufacturer', 'ast_supplier', 'ast_catalogue_no',
                    'ast_batch_no', 'ast_serial_no', 'ast_dmanufactured', 'ast_dputinuse',
                    'ast_cemarking', 'ast_location', 'ast_quantity', 'ast_dlastservice',
                    'ast_dnextservice', 'ast_descr', 'ast_category', 'ast_cat_other_info', 'notes'
                )
            )
        );
    }

    $Attributes['Title'] = 'Equipment';

    $SectionName = 'equipment_'.$Suffix;

    $equipmentFormDesign = Forms_FormDesign::GetFormDesign(array(
        'module' => 'AST',
        'level' => $aParams['level'],
        'parent_level' => $aParams['level'],
        'parent_module' => $Module,
        'link_type' => 'tprop'
    ));
    $equipmentFormDesign->AddSuffixToFormDesign($Suffix, $ModuleDefs[$Module]['LEVEL1_CON_OPTIONS'][$Type]['Title']);

    $Table = new FormTable($aParams['FormType'], 'AST', $equipmentFormDesign);

    $Title = '<div>'.$Attributes["Title"].'</div>';

    if ($aParams['FormType'] != 'ReadOnly' && $aParams['FormType'] != 'Print')
    {
        if (!$clear)
        {
           $RightHandLink['onclick'] = 'var DivToDelete=$(\'equipment_section_div_'.$Suffix.'\');DivToDelete.parentNode.removeChild(DivToDelete)';
           $RightHandLink['text'] = _tk('delete_section2');
        }
        else
        {
            if (is_numeric($_REQUEST['form_id']))
            {
                $parentFormId = Sanitize::SanitizeInt($_REQUEST['form_id']);
            }
            elseif (is_numeric($_REQUEST['parent_form_id']))
            {
                $parentFormId = Sanitize::SanitizeInt($_REQUEST['parent_form_id']);
            }
            else
            {
                $parentFormId = '';    
            }
            
           $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
           $RightHandLink['onclick'] = 'ReplaceSection(\''.$Module.'\','.$Suffix.', \'equipment\', \''.$Type.'\', '.$spellChecker.', \''.$parentFormId.'\')';
           $RightHandLink['text'] = _tk('clear_section');
        }
    }

    include("Source/assets/BasicForm_NoPanels.php");

    $Table->MakeForm($FormArray, $data, $Module, array('dynamic_section' => true));

    $returnHTML = $Table->GetFormTable();

    $returnHTML .= '<input type="hidden" id="link_recordid_'.$Suffix.'" name="link_recordid_'.$Suffix.'" value="'.Sanitize::SanitizeInt($data['link_recordid_'.$Suffix]).'" />';
    $returnHTML .= '<input type="hidden" id="updateid_'.$Suffix.'" name="updateid_'.$Suffix.'" value="'. Sanitize::SanitizeInt($data['updateid_'.$Suffix]) .'" />';
    $returnHTML .= '<input type="hidden" id="ast_id_'.$Suffix.'" name="ast_id_'.$Suffix.'" value="" />';

    if ($Attributes["DivName"])
    {
        $returnHTML .= '<input type="hidden" id="equipment_div_name_'.$Suffix.'" name="equipment_div_name_'.$Suffix.'" value="'.$Attributes["DivName"].'" />';
    }

    if ($data['link_exists_'.$Suffix])
    {
        $returnHTML .= '<input type="hidden" id="link_exists_'.$Suffix.'" name="link_exists_'.$Suffix.'" value="1" />';
    }

    $JSFunctions[] = $mandatoryJSChanges;

    echo $returnHTML;

    unset($GLOBALS["AST"]["form_id"]);
}

/**
* @desc Constructs a list of equipment linked to a particular record.
*
* @param string $module Module the maim record belongs to.
* @param string $link_id ID of the main record.
* @param string $FormType Current form type (e.g. New, Edit, ReadOnly, Print...).
*/
function MakePropertyPanel($module, $link_id, $FormType) {

    global $scripturl, $dtxdebug, $FormType, $ModuleDefs, $UserLabels, $UserExtraText;

    if ($_GET["print"] == 1)
    {
        $FormType = "Print";
    }

    // If user doesn't have access to the module he should not see any data
    if (CanSeeModule('AST') && $link_id)
    {
        // get equipment
        $sql = 'SELECT recordid, ast_name, ast_descr, ast_serial_no, link_assets.link_recordid, rep_approved
        FROM assets_main
        LEFT JOIN link_assets on assets_main.recordid = link_assets.ast_id
        WHERE ' . $ModuleDefs[$module]['FK'] . ' = '.$link_id;

        // Apply security where clause to linked contacts so that users see the appropriate linked data
        $securityWhere = MakeSecurityWhereClause('', 'AST', $_SESSION['initials']);

        if ($securityWhere != '')
        {
            $sql .= ' AND '.$securityWhere;
        }

        $sql .= ' ORDER BY recordid DESC';

        $resultArray = DatixDBQuery::PDO_fetch_all($sql);

        if (count($resultArray) > 0)
        {
            echo '
                <li name="documents_row" id="documents_row">
                    <input type="hidden" name="equipment_url" value="" />
                <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
                <tr>
                    <td class="windowbg" width="15%" align="left"><b>Name</b></td>
                    <td class="windowbg" width="60%" align="left"><b>Description</b></td>
                    <td class="windowbg" width="15%" align="left"><b>Serial number</b></td>
                </tr>
            ';
            foreach ($resultArray as $ast)
            {
                $sLink = '<a href="Javascript:if(CheckChange()){SendTo(\''.$scripturl.'?action=linkequipment&amp;module='.$module.'&amp;link_recordid='.$ast['link_recordid'].'&amp;main_recordid='.$link_id.'&amp;ast_recordid='.$ast["recordid"].'\');}">';

                echo '
                    <tr>
                        <td class="windowbg2">
                ';

                if (!$ast["ast_name"] && !$ast["ast_descr"] && !$ast["ast_serial_no"])
                {
                    //No info, so need to provide something to link with
                    $ast["ast_name"] = 'Unknown';
                }

                if ($FormType != "Print" && $FormType != "ReadOnly" && GetAccessLevel('AST') != 'AST_INPUT_ONLY')
                {
                    echo $sLink;

                    if ($ast["rep_approved"] == "UN")
                    {
                        echo '<img border="0" src="Images/busy1.gif" alt="" />&nbsp;';
                    }

                    echo $ast["ast_name"] ."</a>";
                }
                else
                {
                    echo $ast["ast_name"];
                }

                echo '
                </td>
                <td class="windowbg2">
                ';

                if ($FormType != "Print" && $FormType != "ReadOnly" && GetAccessLevel('AST') != 'AST_INPUT_ONLY')
                {
                    echo $sLink. $ast["ast_descr"] ."</a>";
                }
                else
                {
                    echo $ast["ast_descr"];
                }

                echo '
                </td>
                <td class="windowbg2">
                ';

                if ($FormType != "Print" && $FormType != "ReadOnly" && GetAccessLevel('AST') != 'AST_INPUT_ONLY')
                {
                    echo $sLink. $ast["ast_serial_no"] ."</a>";
                }
                else
                {
                    echo $ast["ast_serial_no"];
                }

                echo '
                </td>
                ';
                echo '
                </tr>
                ';
            }

            echo '
            </table></li>';
        }
    }

    if (count($resultArray) == 0)
    {
        echo '
        <li class="new_windowbg padded_div">
            <b>No equipment linked.</b>
        </li>';
    }

    if (!($FormType == "Print" || $FormType == "ReadOnly" || $FormType == "Locked") && GetAccessLevel('AST') != 'AST_READ_ONLY')
    {
        echo '
        <li class="section_title_row row_below_table">
            <b>Link new equipment</b>
        </li>
        <li class="section_link_row">
        ';

         $url = "$scripturl?action=linkequipment&module=$module&main_recordid=$link_id";
         echo '<a href="Javascript:if(CheckChange()){SendTo(\''. $url. '\');}" ><b>Attach a new piece of equipment</b></a>';
         echo '
        </li>
        ';
    }
}

/**
* @desc Retrieves data from link_assets and assets_main tables before displaying a linked equipment record.
* Once complete, calls ShowAssetForm()
*/
function EquipmentLink()
{
    global $ModuleDefs;

    $module = $_GET['module'];
    $main_recordid = $_GET['main_recordid'];

    $equipment_fields = implode(",", $ModuleDefs['AST']['FIELD_ARRAY']);

    LoggedIn();

    if ($_POST["link_action"] == "Cancel")
    {
        ReshowContactLink();
    }

    if ($_GET["link_recordid"] != "")  // An already-linked piece of equipment
    {
        $sql = 'SELECT link_recordid, '.implode(', ', $ModuleDefs['AST']['LINK_FIELD_ARRAY']).', ast_id, '.$ModuleDefs[$module]['FK'].'
            FROM link_assets
            WHERE link_recordid = :link_recordid';

        $ast = DatixDBQuery::PDO_fetch($sql, array("link_recordid" => $_GET['link_recordid']));
        $ast["link_exists"] = true;

        $sql = 'SELECT assets_main.recordid, assets_main.updateid, notes,' . $equipment_fields . ' FROM assets_main
                left join notepad on assets_main.recordid = notepad.ast_id
                WHERE recordid = :ast_id';

        $row = DatixDBQuery::PDO_fetch($sql, array("ast_id" => $ast['ast_id']));

        $ast = array_merge($ast, $row);
    }
    elseif ($_GET["ast_recordid"] != "")  // A piece of equipment not yet linked (post-match)
    {
        $sql = 'SELECT recordid, assets_main.recordid as ast_id, assets_main.updateid, notes,' . $equipment_fields . ' FROM assets_main
                left join notepad on assets_main.recordid = notepad.ast_id
                WHERE recordid = :ast_recordid';

        $ast = DatixDBQuery::PDO_fetch($sql, array("ast_recordid" => $_GET['ast_recordid']));
    }
    else
    {
        //New piece of equipment - No hard-coded default values
    }

    // If the equipment might have changed we need to get rid of the link details which may now be innaccurate
    if (!$_GET['from_equipment_match'])
    {
        unset($_SESSION["AST"]["LINK_DETAILS"]);
    }

    // Check if we need to show full audit trail
    if ($ast['ast_id'] && $_GET['full_audit'])
    {
        $FullAudit = GetFullAudit(array('Module' => 'AST', 'recordid' => $ast['ast_id']));

        if ($FullAudit)
        {
            $ast['full_audit'] = $FullAudit;
        }
    }

    $loader = new src\framework\controller\Loader();
    $controller = $loader->getController(array('controller' => 'src\ast\controllers\ShowAssetFormTemplateController'));
    $controller->setRequestParameter('ast', $ast);
    $controller->setRequestParameter('FormType', $FormType);
    $controller->setRequestParameter('LinkMode', 'linkequipment');
    $controller->setRequestParameter('module', $module);
    $controller->setRequestParameter('main_recordid', $main_recordid);
    echo $controller->doAction('showassetform');
    obExit();
}

function GetPostedEquipment($Data)
{
    $TotalEquipment = $Data['equipment_max_suffix'];

    for ($i = 1; $i < $TotalEquipment; $i++ )
    {
        $EquipmentSuffixes[] = $i;
    }

    return $EquipmentSuffixes;
}

function StoreEquipmentLabels()
{
    //Store equipment labels in session for later use in dropdown popups
    if (!is_array($_SESSION["EQUIPMENTLABELS"]))
    {
        $_SESSION['EQUIPMENTLABELS'] = array();
    }

    if (is_array($GLOBALS["UserLabels"]))
    {
        $_SESSION['EQUIPMENTLABELS'] = array_merge($_SESSION['EQUIPMENTLABELS'], $GLOBALS["UserLabels"]);
    }
}

