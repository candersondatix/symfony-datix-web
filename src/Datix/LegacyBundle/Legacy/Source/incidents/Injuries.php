<?php
function SectionInjuryDetails($inc, $FormType, $module, $suffix = 0, $section = '', $Type = '')
{
    global $txt, $scripturl, $ReadOnlyFields, $HideFields, $UserLabels, $MandatoryFields, $JSFunctions, $ModuleDefs;

    $SuffixString = (is_numeric($suffix) && $suffix > 0 ? '_'.$suffix : '');

    if (!empty($section) && is_array($ReadOnlyFields) && array_key_exists($section, $ReadOnlyFields))
    {
        $FormType = 'ReadOnly';
    }

    if (is_array($HideFields))
    {
        if (array_key_exists('dum_injury'.$SuffixString, $HideFields))
        {
            $injuryHidden = true;
        }
        if (array_key_exists('dum_bodypart'.$SuffixString, $HideFields))
        {
            $bodyHidden = true;
        }
        if (array_key_exists('dum_treatment'.$SuffixString, $HideFields))
        {
            $treatmentHidden = true;
        }
    }

    if (is_array($ReadOnlyFields))
    {
        if (array_key_exists('dum_injury'.$SuffixString, $ReadOnlyFields))
        {
            $injuryReadOnly = true;
        }
        if (array_key_exists('dum_bodypart'.$SuffixString, $ReadOnlyFields))
        {
            $bodyReadOnly = true;
        }
        if (array_key_exists('dum_treatment'.$SuffixString, $ReadOnlyFields))
        {
            $treatmentReadOnly = true;
        }
    }

    $Table = new FormTable($FormType, 'INC');

    $InjuryKey = "injury".$SuffixString;
    $BodyPartKey = "bodypart".$SuffixString;

    $Injuries = '
    <div style="overflow:visible">
';
    $RetainComboChildren = bYN(GetParm('RETAIN_COMBO_CHILDREN', 'Y'));
    $Injuries .= '<input type="hidden" id="RETAIN_COMBO_CHILDREN" value="' . $RetainComboChildren .'"/>';

    if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked' && !($injuryReadOnly && $bodyReadOnly))
    {
        $InjuryTitle = FirstNonNull(array($UserLabels['dum_injury'.$SuffixString], Labels_FieldLabel::GetFieldLabel('inc_injury', '', 'inc_injuries', 'INC', '', 'INCINJ'), _tk('injury')));
        $BodyTitle = FirstNonNull(array($UserLabels['dum_bodypart'.$SuffixString], Labels_FieldLabel::GetFieldLabel('inc_bodypart', '', 'inc_injuries', 'INC', '', 'INCINJ'),_tk('body_part')));

        $Injuries .= '
        <table id="injury_table'.$SuffixString.'" class="bordercolor" cellspacing="1" cellpadding="5" align="left" border="0" style="min-width:570px">
        <tr>';

        if (!$injuryHidden)
        {
            $Injuries .= '
            <td class="titlebg" align="left" width="250">
            <b>' . $InjuryTitle . '</b>
            </td>';
        }

        if (!$bodyHidden)
        {
            $Injuries .= '
            <td class="titlebg" align="left" width="250">
            <b>' . $BodyTitle . '</b>
            </td>';
        }

        $Injuries .= '
            <td class="titlebg" align="left" width="50">
            </td>
        </tr>
        ';
    }

    if($inc["injury_table"]) //from database
    {
        $inc['injury'] = Sanitize::SanitizeStringArray($inc["injury_table"]);
    }
    else //from post
    {
        if(is_array($inc[$InjuryKey]))
        {
            foreach($inc[$InjuryKey] as $i => $inj)
            {
                $injArray[] = array($inj,$inc[$BodyPartKey][$i]);
            }
            $inc["injury"] = $injArray;
        }
    }

    $children = GetChildren(array('module' => 'INC', 'field' => 'inc_injury'));

    $RowSuffix = 1;
    if($inc["injury"] != "")
    {
        if(is_array($inc["injury"]))
        {
            foreach($inc["injury"] as $Injury)
            {
                if (!array_key_exists("inc_injury", $Injury))
                {
                    $Injury["inc_injury"] = Sanitize::SanitizeString($Injury[0]);//Injected to page later
                    $Injury["inc_bodypart"] = Sanitize::SanitizeString($Injury[1]);//Injected to page later
                    $Injury["recordid"] = Sanitize::SanitizeString($Injury[2]);//Injected to page later
                    $Injury["listorder"] = Sanitize::SanitizeString($Injury[3]);//Injected to page later
                }

                if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked')
                {
                    $Injuries .= getInjuryRow(array(
                            'data' => array('link_injury' => $Injury["inc_injury"], 'link_bodypart' => $Injury["inc_bodypart"]),
                            'person_suffix'=>$suffix,
                            'row_suffix'=>$RowSuffix,
                            'formtype' => $FormType,
                            'hidden' => array('injury' => $injuryHidden, 'body' => $bodyHidden),
                            'readonly' => array('injury' => $injuryReadOnly, 'body' => $bodyReadOnly)));
                }
                else
                {
                    $Injuries .= code_descr('INC', 'link_injury1', $Injury["inc_injury"]);
                    if($Injury["inc_bodypart"])
                        $Injuries .= ' - ' . code_descr('INC', 'link_bodypart1', $Injury["inc_bodypart"]);
                    $Injuries .= '<br />';
                }

                if((!($FormType == 'Search' && bYN(GetParm('NO_SEARCH_ALERTS', 'N')))) || !empty($children[0]))
                {
                    $OnChangeFunc = 'function OnChange_injury_' . $RowSuffix .'() {
                    ';

                    foreach($children as $childField)
                    {
                        if($childField != '')
                        {
                            if ($childField == 'inc_bodypart')
                            {
                                $childField = 'bodypart';
                            }
                            $OnChangeFunc .= 'if (jQuery(\'#'.$childField. '_' .$RowSuffix.'_title\').length){jQuery(\'#'.$childField. '_' .$RowSuffix.'_title\').checkClearChildField('.($RetainComboChildren ? 'true' : 'false').');}';
                        }
                    }

                    $OnChangeFunc .= '}';

                    $JSFunctions[] = $OnChangeFunc;
                }

                $RowSuffix++;
            }
        }
    }
    else if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked')
    {
        $Injuries .= getInjuryRow(array(
                        'module'=>$module,
                        'type'=>$Type,
                        'row_suffix'=>1,
                        'person_suffix'=>$suffix,
                        'formtype' => $FormType,
                        'hidden' => array('injury' => $injuryHidden, 'body' => $bodyHidden),
                        'readonly' => array('injury' => $injuryReadOnly, 'body' => $bodyReadOnly)));

        if((!($FormType == 'Search' && bYN(GetParm('NO_SEARCH_ALERTS', 'N')))) || !empty($children[0]))
        {
            $OnChangeFunc = 'function OnChange_injury'. ($suffix ? '_' .$suffix : '') .'_1() {
            ';

            foreach($children as $childField)
            {
                if($childField != '')
                {
                    if ($childField == 'inc_bodypart')
                    {
                        $childField = 'bodypart';
                    }
                    $OnChangeFunc .= 'if (jQuery(\'#'.$childField. ($suffix ? '_' .$suffix : '').'_1_title\').length){jQuery(\'#'.$childField. ($suffix ? '_' .$suffix : '').'_1_title\').checkClearChildField('.($RetainComboChildren ? 'true' : 'false').');}';
                }
            }

            $OnChangeFunc .= '}';

            $JSFunctions[] = $OnChangeFunc;
        }
    }


    if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked' && !(($injuryReadOnly && $bodyReadOnly) || ($injuryReadOnly && $bodyHidden) || ($injuryHidden && $bodyReadOnly)))
    {
        $Injuries .= '<tr><td colspan="3" class="windowbg2"><input type="button" value="'._tk("add_another_injury").'" style="align:right" nextSuffix="'.($RowSuffix+1).'" onclick="addInjury(this, \''.$suffix.'\', \''.$Type.'\', '.(is_numeric($_REQUEST['form_id']) ? Sanitize::SanitizeInt($_REQUEST['form_id']) : 'null').')" /></td></tr>';
    }

    if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked' && !($injuryReadOnly && $bodyReadOnly))
    {
        $Injuries .= '</table>';
    }

    $Injuries .= '
        <input type="hidden" name="CHANGED-link_treatment" id="CHANGED-link_treatment" />
    </div>
    ';

    if (!($injuryHidden && $bodyHidden))
    {
        $InjuriesObj = new FormField($FormType);
        $InjuriesObj->MakeCustomField($Injuries);
        $Table->MakeRow('<b>' . _tk('injuries') . '</b><br/>'.(GetValidationErrors($inc, 'injury'.($suffix ? '_' .$suffix : ''))), $InjuriesObj);
    }

    $TreatmentLabel = FirstNonNull(array($UserLabels['dum_treatment'.$SuffixString], _tk('treatment_received')));
    $TreatmentTitle = '<b>'.$TreatmentLabel.'</b>';

    if($MandatoryFields['dum_treatment'.$SuffixString] && $FormType != 'Print')
    {
        //This is a bit of a hack - we should think about making the injury sections on DIF1 and DIF2 have the same name.
        if(!$suffix) //implies we're on DIF2
        {
            $sectionPrefix = 'injury';
        }
        else
        {
            $sectionPrefix = 'injury_section';
        }

        $MandatoryFields["link_treatment".$SuffixString] = $sectionPrefix.$SuffixString;
        $JSFunctions[] = 'if(mandatoryArray){mandatoryArray.push(new Array("link_treatment'.$SuffixString.'","'.$sectionPrefix.$SuffixString.'","'.$TreatmentLabel.' ('.$ModuleDefs[$module]['LEVEL1_CON_OPTIONS'][$Type]['Title'].')"))};';
        $TreatmentTitle = '<img src="Images/warning.gif" /> '.$TreatmentTitle.'<div class="field_error" id="errlink_treatment'.$SuffixString.'" style="display:none">'._tk('mandatory_err').'</div>';
    }

    if ($treatmentReadOnly)
    {
        $FormType = 'ReadOnly';
    }

    if (!$treatmentHidden)
    {
        $TreatmentObj = Forms_SelectFieldFactory::createSelectField('link_treatment', 'INC', Sanitize::SanitizeString($inc['link_treatment'.$SuffixString]), $FormType, false, $TreatmentLabel, 'injuries', Sanitize::SanitizeInt($inc['CHANGED-link_treatment'.$SuffixString]), $SuffixString);
        $TreatmentObj->setAltFieldName('link_treatment'.$SuffixString);
        $Table->MakeRow($TreatmentTitle, $TreatmentObj);
        echo '
            <input type="hidden" name="show_field_link_treatment'.$SuffixString . '" id="show_field_link_treatment'.$SuffixString . '" value="1" />';
    }

    $Table->MakeTable();

    echo '<li>'.$Table->GetFormTable().'</li>';
} // SectionInjuryDetails()

function PrintedInjuriesSection($aParams)
{
    global $ModuleDefs;

    $con = $aParams['con'];

    $main_recordid = $con[$ModuleDefs[$aParams['module']]['FK']];

    $con_id = $con['con_id'];

    $html = '';

    if($con_id && $main_recordid)
    {
        $sql = 'SELECT inc_injury, inc_bodypart from inc_injuries
            WHERE inc_id = '.$main_recordid.' AND con_id = '.$con_id;

        $injuries = DatixDBQuery::PDO_fetch_all($sql, array('inc_id' => $main_recordid, 'con_id' => $con_id));

        foreach ($injuries as $injury)
        {
            $html .= '<div>'.code_descr('INC','link_injury1',$injury["inc_injury"]).' - '.code_descr('INC','link_bodypart1',$injury["inc_bodypart"]).'</div>';
        }
    }

    return $html;
}

function constructInjuryCells($aParams)
{
    global $MandatoryFields, $JSFunctions, $txt, $UserLabels, $txt, $ModuleDefs;

    $InjuryCells = array();

    $row_suffix = $aParams['row_suffix'];
    $person_suffix = $aParams['person_suffix'];

    //This is a bit of a hack - we should think about making the injury sections on DIF1 and DIF2 have the same name.
    if(!$person_suffix) //implies we're on DIF2
    {
        $sectionPrefix = 'injury';
    }
    else
    {
        $sectionPrefix = 'injury_section';
    }

    $ShortSuffixString = ($person_suffix ? '_'.$person_suffix : '');
    $SuffixString = ($person_suffix ? '_'.$person_suffix.'_'.$row_suffix : '_'.$row_suffix);

    $InjuryName = FirstNonNull(array($UserLabels['dum_injury'.$ShortSuffixString], _tk('injury')));
    $BodyPartName = FirstNonNull(array($UserLabels['dum_bodypart'.$ShortSuffixString], _tk('body_part')));

    if (!$aParams['readonly']['injury'])
    {
        $Injury1 = Forms_SelectFieldFactory::createSelectField('link_injury1', 'INC', $aParams['data']['link_injury'], $FormType, false, $InjuryName, 'injuries', $aParams['data']['CHANGED-link_injury']);
        $Injury1->setChildren(array('bodypart'.$SuffixString));
        $Injury1->setAltFieldName('injury'.$SuffixString);
        $Injury1->setOnChangeExtra('jQuery(\'#hidden_injury'.$SuffixString.'\').val(jQuery(\'#injury'.$SuffixString.'\').val())');
        $InjuryField = $Injury1->getField();
        $InjuryField .= '
            <input type="hidden" name="show_field_injury'.$SuffixString . '" id="show_field_injury'.$SuffixString . '" value="1" />';
    }
    else
    {
        $InjuryField = code_descr('INC', 'link_injury1', $aParams['data']['link_injury']);
    }

    $InjuryField .= '<input type="hidden" id="'."hidden_injury".$SuffixString.'" name="injury'.$ShortSuffixString.'[]" value="'.$aParams['data']['link_injury'].'" />';

    if($MandatoryFields['dum_injury'.$ShortSuffixString])
    {
        $InjuryField = '<img src="Images/warning.gif" style="float:left" /> '.$InjuryField.'<div class="field_error" id="errinjury'.$SuffixString.'" style="display:none">'._tk('mandatory_err').'</div>';
        $JSFunctions[] = 'if(mandatoryArray){mandatoryArray.push(new Array("injury'.$SuffixString.'","'.$sectionPrefix.$ShortSuffixString.'","'.$InjuryName.' ('.$ModuleDefs[$aParams['module']]['LEVEL1_CON_OPTIONS'][$aParams['type']]['Title'].')"))};';
    }

    if (!$aParams['hidden']['injury'])
    {
        $InjuryCells[] = $InjuryField;
    }

    if (!$aParams['readonly']['body'])
    {
        $Bodypart1 = Forms_SelectFieldFactory::createSelectField('link_bodypart1', 'INC', $aParams['data']['link_bodypart'], $FormType, false, $BodyPartName, 'injuries', $aParams['data']['CHANGED-link_bodypart']);
        $Bodypart1->setParents(array('injury'.$SuffixString));
        $Bodypart1->setAltFieldName('bodypart'.$SuffixString);
        $Bodypart1->setOnChangeExtra('jQuery(\'#hidden_bodypart'.$SuffixString.'\').val(jQuery(\'#bodypart'.$SuffixString.'\').val())');
        $BodyField .= $Bodypart1->getField();
        $BodyField .= '
            <input type="hidden" name="show_field_bodypart'.$SuffixString . '" id="show_field_bodypart'.$SuffixString . '" value="1" />';
    }
    else
    {
        if ($aParams['readonly']['injury'])
        {
            $BodyField = ' - ';
        }
        $BodyField .= code_descr('INC', 'link_bodypart1', $aParams['data']['link_bodypart']);
    }

    $BodyField .= '<input type="hidden" id="'."hidden_bodypart".$SuffixString.'" name="bodypart'.$ShortSuffixString.'[]" value="'.$aParams['data']['link_bodypart'].'" />';

    if($MandatoryFields['dum_bodypart'.$ShortSuffixString])
    {
        $BodyField = '<img src="Images/warning.gif" style="float:left" /> '.$BodyField.'<div class="field_error" id="errbodypart'.$SuffixString.'" style="display:none">'._tk('mandatory_err').'</div>';
        $JSFunctions[] = 'if(mandatoryArray){mandatoryArray.push(new Array("bodypart'.$SuffixString.'","'.$sectionPrefix.$ShortSuffixString.'","'.$BodyPartName.' ('.$ModuleDefs[$aParams['module']]['LEVEL1_CON_OPTIONS'][$aParams['type']]['Title'].')"))};';
    }

    if (!$aParams['hidden']['body'])
    {
        $InjuryCells[] = $BodyField;
    }

    if($row_suffix != 1 && !($aParams['readonly']['injury'] && $aParams['readonly']['body']))
    {
        $InjuryCells[] = '<input type="button" name="btnAdd" value="Delete" onclick="deleteInjuryRow('.$row_suffix.', '.($person_suffix ? $person_suffix : '0').')" />';
    }
    else
    {
        $InjuryCells[] = '';
    }

    return $InjuryCells;
}

function getInjuryRow($aParams)
{
    $SuffixString = (is_integer($aParams['person_suffix']) ? '_'.$aParams['person_suffix'].'_'.$aParams['row_suffix'] : '_'.$aParams['row_suffix']);

    $RowHTML = '<tr id="injury_row'.$SuffixString.'" name="injury_row'.$SuffixString.'"><td class="windowbg2">';

    $RowHTML .= implode('</td><td class="windowbg2">',constructInjuryCells($aParams));

    $RowHTML .= '</td></tr>';

    return $RowHTML;
}

function printInjuryRow()
{
    global $ClientFolder, $MandatoryFields;

    require_once 'Source/libs/FormClasses.php';

    $aParams['data']['link_injury'] = Sanitize::SanitizeString($_GET["default_inj"]);
    $aParams['data']['link_bodypart'] = Sanitize::SanitizeString($_GET["default_bpt"]);
    $aParams['row_suffix'] = Sanitize::SanitizeInt($_GET["row_suffix"]);
    $aParams['person_suffix'] = Sanitize::SanitizeInt($_GET["person_suffix"]);
    $aParams['linktype'] = Sanitize::SanitizeString($_GET["linktype"]);

    header('Content-type: application/x-json');

    //bit of a hack, but we need to know whether we're looking at a dif1 or dif2
    if(mb_substr_count($_SESSION['LASTUSEDFORMDESIGN'], 'DIF2'))
    {
        $FormLevel = 2;
    }
    else
    {
        $FormLevel = 1;
    }

    $FileName = GetContactFormSettings($aParams['linktype'], $FormLevel, 'INC');

    AddSuffixToFormDesign(array('filename' => $FileName, 'suffix' => $aParams['person_suffix'], 'module' => ''));

    $SuffixString = ($aParams['person_suffix'] ? '_'.$aParams['person_suffix'] : '');

    if (is_array($GLOBALS['HideFields']))
    {
        if (array_key_exists('dum_injury'.$SuffixString, $GLOBALS['HideFields']))
        {
            $aParams['hidden']['injury'] = true;
        }
        if (array_key_exists('dum_bodypart'.$SuffixString, $GLOBALS['HideFields']))
        {
            $aParams['hidden']['body'] = true;
        }
    }

    if (is_array($GLOBALS['ReadOnlyFields']))
    {
        if (array_key_exists('dum_injury'.$SuffixString, $GLOBALS['ReadOnlyFields']))
        {
            $aParams['readonly']['injury'] = true;
        }
        if (array_key_exists('dum_bodypart'.$SuffixString, $GLOBALS['ReadOnlyFields']))
        {
            $aParams['readonly']['body'] = true;
        }
    }

    $JSONdata['cells'] = constructInjuryCells($aParams);
    $JSONdata['row_suffix'] = $aParams['row_suffix'];
    $JSONdata['person_suffix'] = $aParams['person_suffix'];
    $JSONdata['js'] = getJSFunctions();

    //encode and return json data...
    echo json_encode($JSONdata);

    //echo implode('</td><td class="windowbg2">',constructInjuryCells($aParams));
}

/**
* @desc Gets the injuries linked to a particlar contact and incident.
*
* @param array $aParams Array of parameters
* @param int $aParams['data'] Record data array
*
* @return array Record data array with injury data included
*/
function GetLinkedInjuries($aParams)
{
    $sql = 'SELECT inc_injury, inc_bodypart, recordid, listorder,
        con_id, inc_id
        FROM inc_injuries
        WHERE con_id = :con_id
        AND inc_id = :inc_id
        ORDER BY listorder ASC';

    $injuries = DatixDBQuery::PDO_fetch_all($sql, array('con_id' => $aParams['data']['con_id'], 'inc_id' => $aParams['data']['inc_id']));

    foreach ($injuries as $injury)
    {
        $aParams['data']['injury_table'][$injury["listorder"]] = $injury;
    }

    if (!empty($aParams['data']['injury_table']))
    {
        $aParams['data']['show_injury'] = "Y";
    }

    return $aParams['data'];
}