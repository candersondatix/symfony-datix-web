<?php

if (!$ManagerDropdownField && empty($GLOBALS['HideFields']['inc_mgr']) && bYN(GetParm('DIF_EMAIL_MGR', 'N')))
{
    $ManagerDropdownField = \src\incidents\controllers\IncidentFormTemplateController::MakeManagerDropdown($inc, $FormMode);
}

$UseReporterFields = CheckUseReporterFields();

// If the user is logged in from the contacts table, they are the
// reporter of the incident.
if ($_SESSION["contact_login"] && $inc["inc_repname"] == "")
{
    $inc["inc_repname"] = $_SESSION["fullname"];
}

if ($FormMode == "Print" || $FormMode == "ReadOnly")
{
    $gradereadonly = true;
}

if ($FormMode == "Search")
{
    $Searchmode = true;
}

if ($FormType && !$FormMode)
{
    $FormMode = $FormType;
}

$Table = new FormTable($FormMode, 'INC');
$UDFSection = new FormTable($FormMode, 'INC');

$FormArray = array(
    "Parameters" => array(
        "ExtraContacts" => 5,
        "Condition" => false
    ),
    "details" => array(
        "Title" => "Incident details",
        "Rows" => array(
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'INC',
                'perms' => $DIFPerms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $inc,
                'module' => 'INC',
                'perms' => $DIFPerms,
                'approveobj' => $ApproveObj
            )),
            "inc_type",
            "inc_dincident",
            "inc_time",
            "inc_time_band",
            "inc_organisation",
            "inc_unit",
            "inc_clingroup",
            "inc_directorate",
            "inc_specialty",
            "inc_loctype",
            "inc_locactual",
            "inc_notes",
            "inc_rc_required",
            "inc_report_npsa",
            "inc_is_riddor",
            "inc_actiontaken",
            array(
                "Name" => "inc_cnstitype",
                "Condition" => !($_SESSION["Globals"]["DIF_1_CCS"] == "TCS" || $_SESSION["Globals"]["DIF_1_CCS"] == "TCS_CCS")
            ),
            'inc_notify',
            array('Name' => 'inc_injury', 'Type' => 'ff_select', 'Title' => "Loss/damage", 'Module' => 'INC'),
            array('Name' => 'inc_bodypart', 'Type' => 'ff_select', 'Title' => "Item", 'Module' => 'INC'),
            array('Name' => 'inc_level_intervention', 'Condition' =>  (bYN(GetParm('CCS2_INC', 'N')))),
            array('Name' => 'inc_level_harm', 'Condition' =>  (bYN(GetParm('CCS2_INC', 'N')))),
            "inc_never_event"
        )
    ),
    "tcs" => array(
        "Title" => "Incident Coding",
        "Condition" => ($_SESSION["Globals"]["DIF_1_CCS"] == "TCS" || $_SESSION["Globals"]["DIF_1_CCS"] == "TCS_CCS"),
        "Rows" => array(
            "inc_category",
            "inc_subcategory",
            "inc_cnstitype"
        )
    ),
    "ccs" => array(
        "Title" => "Datix Common Classification System (CCS)",
        "Condition" => ($_SESSION["Globals"]["DIF_1_CCS"] == "CCS" || $_SESSION["Globals"]["DIF_1_CCS"] == "TCS_CCS"),
        "Rows" => array(
            "inc_carestage",
            "inc_clin_detail",
            "inc_clintype"
        )
    ),
    "ccs2" => array(
        "Title" =>"Datix CCS2",
        "Condition" => (bYN(GetParm("CCS2_INC",'N'))),
        "Rows" => array(
            'inc_affecting_tier_zero',
            'inc_type_tier_one',
            'inc_type_tier_two',
            'inc_type_tier_three'
        )
    ),
    "severity" => array(
        "Title" => ("Incident Severity" . ($HideFields["inc_result"] ? "" : " and " . GetFieldLabel("inc_result", "Result"))),
        "Rows" => array(
            "inc_result",
            "inc_severity",
            "dum_inc_grading_initial",
            "dum_inc_grading"
        )
    ),
    "riddor" => array(
        "Title" => "RIDDOR",
        "Rows" => array(
            "inc_dnotified",
            "inc_riddor_ref",
            "inc_ridloc",
            "inc_address",
            "inc_localauth",
            "inc_acctype",
            "inc_riddorno"
        )
    ),
    "nrls" => array(
        "Title" => "NPSA NRLS",
        "Rows" => array(
            "inc_n_effect",
            "inc_n_patharm",
            "inc_n_nearmiss",
            "inc_n_actmin",
            "inc_n_paedward",
            "inc_n_paedspec",
            "inc_dnpsa",
            "inc_n_clindir",
            "inc_n_clinspec"
        )
    ),
    "additional" => array(
        "Title" => "Additional Information",
        "Rows" => array(
            "show_person",
            "show_witness",
            "show_employee",
            "show_other_contacts",
            "show_equipment",
            "show_medication",
            "show_pars",
            "show_document"
        )
    ),
    "equipment" => array(
        "Title" => "Equipment (legacy)",
        "Rows" => array(
                "inc_eqpt_type",
                "inc_equipment",
                "inc_serialno",
                "inc_manufacturer",
                "inc_description",
                "inc_supplier",
                "inc_servrecords",
                "inc_model",
                "inc_location",
                "inc_qdef",
                "inc_dmanu",
                "inc_lastservice",
                "inc_dputinuse",
                "inc_batchno",
                "inc_cemarking",
                "inc_outcomecode",
                "inc_dmda",
                "inc_outcome",
                "inc_defect"
        )
    ),
    "medication" => array(
        "Title" => "Medication incident details",
        "Rows" => array(
            "inc_med_stage",
            "inc_med_error",
            "inc_med_drug",
            "inc_med_drug_rt",
            "inc_med_form",
            "inc_med_form_rt",
            "inc_med_dose",
            "inc_med_dose_rt",
            "inc_med_route",
            "inc_med_route_rt"
        )
    ),
    "multi_medication_record" => array(
        "Title" => "Medications",
        "Condition" => (bYN(GetParm("MULTI_MEDICATIONS", "N")) && $FormType != 'Design'),
        "NoFieldAdditions" => true,
        "Include" => "Source/medications/MainMedications.php",
        "Function" => "DoMedicationSection",
        "ExtraParameters" => array('medication_name' => 'inc_medication'),
        "Rows" => array()
    ),
    "multi_medication_design" => array(
        "Title" => "Medications",
        "Condition" => (bYN(GetParm("MULTI_MEDICATIONS", "N")) && $FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "NoSectionActions" => true,
        "AltSectionKey" => "multi_medication_record",
        "Listings" => array("MED" => array('module' => "MED", 'Label' => 'Select a listing to use for the list of '.$ModuleDefs["MED"]['REC_NAME_PLURAL'])),
            'FieldFormatsTable' => 'INCMED',
        "Rows" => array(
            'dummy_imed_search_admin',
            'imed_name_admin',
            'imed_brand_admin',
            'imed_manu_admin',
            'imed_class_admin',
            'imed_type_admin',
            'imed_route_admin',
            'imed_dose_admin',
            'imed_form_admin',
            'imed_controlled_admin',
            'imed_price_admin',
            'imed_reference_admin',
            'imed_right_wrong_medicine_admin',
            'imed_serial_no',
            'imed_batch_no',
            'imed_manufacturer_special_admin',
            'dummy_imed_search_correct',
            'imed_name_correct',
            'imed_brand_correct',
            'imed_manu_correct',
            'imed_class_correct',
            'imed_type_correct',
            'imed_route_correct',
            'imed_dose_correct',
            'imed_form_correct',
            'imed_controlled_correct',
            'imed_price_correct',
            'imed_reference_correct',
            'imed_right_wrong_medicine_correct',
            'imed_error_stage',
            'imed_error_type',
            'imed_notes',
            'imed_other_factors'
        )
    ),
    "notepad" =>  array(
        "Title" => "Notepad",
        "Rows" => array (
            "notes"
        )
    ),
    "investigation" => array(
        "Title" => "Details of investigation",
        "Rows" => array(
            "inc_investigator",
            "inc_inv_dstart",
            "inc_inv_dcomp",
            "inc_inv_outcome",
            "inc_root_causes",
            "inc_lessons_code",
            "inc_inv_lessons",
            "inc_action_code",
            "inc_inv_action"
        )
    ),
    "pars" => array(
        "Title" => "SIRS reporting information",
        "Rows" => array(
            "inc_pars_clinical",
            "inc_user_action",
            "inc_agg_issues",
            "inc_pars_pri_type",
            "inc_pars_sec_type",
            "inc_pol_called",
            "inc_pol_call_time",
            "inc_pol_attend",
            "inc_pol_att_time",
            "inc_pol_action",
            "inc_pol_crime_no",
            "inc_pars_address",
            "inc_postcode",
            "show_assailant",
            "inc_tprop_damaged",
        )
    ),
    "tprop" => array(
        "Title" => "Equipment",
        "module" => 'INC',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicEquipment',
        'LinkedForms' => array('linked_assets' => array('module' => 'AST')),
        'NoReadOnly' => true,
        "equipmenttype" => 'E',
        "suffix" => 1,
        "Rows" => array()
    ),
    "pas" => array(
        "Title" => "PAS",
        "Condition" => bYN(GetParm("SHOW_OLD_PAS", "N")),
        "Rows" => array(
            'inc_pasno1',
            'inc_pasno2',
            'inc_pasno3'
        )
    ),
    "causal_factor_header" => array(
        "Title" => "Causal factors for "._tk('INCNameTitle'),
        'NewPanel' => true,
        "Rows" => array(
            'inc_causal_factors_linked'
        )
    ),
    "causal_factor" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoCausalFactorsSection' => array(
                'controller' => 'src\\causalfactors\\controllers\\CausalFactorsController'
            )
        ),
        "ExtraParameters" => array('causal_factor_name' => 'inc_causal_factor'),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "causal_factor_design" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "AltSectionKey" => "causal_factor",
        "NoSectionActions" => true,
        "Rows" => array(
            'caf_level_1',
            'caf_level_2',
        )
    ),
    'openness_transparency' => [
        'Title' => _tk('openness_transparency_title'),
        'Rows' => [
            'inc_ot_q1',
            'inc_ot_q2',
            'inc_ot_q3',
            'inc_ot_q4',
            'inc_ot_q5',
            'inc_ot_q6',
            'inc_ot_q7',
            'inc_ot_q8',
            'inc_ot_q9',
            'inc_ot_q10',
            'inc_ot_q11',
            'inc_ot_q12',
            'inc_ot_q13',
            'inc_ot_q14',
            'inc_ot_q15',
            'inc_ot_q16',
            'inc_ot_q17',
            'inc_ot_q18',
            'inc_ot_q19',
            'inc_ot_q20'
        ],
        'NotModes' => ['Search']
    ],
    "contacts_type_L" => array(
        "Condition" => !($_GET['submitandprint'] == 1),
        "Title" => "Alleged assailant details",
        "ContactTitle" => "Alleged assailant",
        "module" => 'INC',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'L',
        "suffix" => 4,
        'LinkedForms' => array('L' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "contacts_type_P" => array(
        "Condition" => !($_GET['submitandprint'] == 1),
        "Title" => "Police officer details",
        "ContactTitle" => "Police officer",
        "module" => 'INC',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'P',
        "suffix" => 5,
        'LinkedForms' => array('P' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "reporter" => array(
        "Title" => "Details of person reporting the incident",
        "Condition" => $UseReporterFields,
        "Rows" => array(
            "inc_repname",
            "inc_rep_tel",
            array(
                "Name" => "inc_rep_email",
                "Title" => "Your e-mail address",
                "Condition" => bYN(GetParm("DIF_1_ACKNOWLEDGE", "N"))
            ),
            "inc_reportedby"
        )
    ),
    "contacts_type_R" => array(
        "Title" => "Details of person reporting the incident",
        "Condition" => !$UseReporterFields,
        "NoFieldAdditions" => true,
        "module" => 'INC',
        "Special" => 'DynamicContact',
        "contacttype" => 'R',
        "Role" => GetParm('REPORTER_ROLE', 'REP'),
        'LinkedForms' => array('R' => array('module' => 'CON')),
        "suffix" => 3,
        "Rows" => array()
    ),
    "your_manager" => array(
        "Title" => "Your Manager",
        "module" => 'INC',
        "Condition" => bYN(GetParm("DIF_EMAIL_MGR", "N")),
        "NoReadOnly" => true,
        "Rows" => array(
            array(
                "Type" => "formfield",
                "Name" => "inc_mgr",
                "NoReadOnly" => true,
                "Title" => 'Your Manager',
                "FormField" => $ManagerDropdownField
            )
        )
    ),
    "contacts_type_A" => array(
        "Title" => _tk('person_affected_plural'),
        "module" => 'INC',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'A',
        "suffix" => 6,
        'LinkedForms' => array('A' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "contacts_type_W" => array(
        "Title" => "Witnesses",
        "module" => 'INC',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'W',
        "suffix" => 8,
        'LinkedForms' => array('W' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "contacts_type_E" => array(
        "Title" => "Employees",
        "module" => 'INC',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'E',
        "suffix" => 7,
        'LinkedForms' => array('E' => array('module' => 'CON')),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "contacts_type_N" => array(
        "Title" => "Contacts",
        "module" => 'INC',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'N',
        'LinkedForms' => array('N' => array('module' => 'CON')),
        "suffix" => 9,
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "extra_document" => array(
        "Title" => "Documents",
        "NoFieldAdditions" => true,
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && ($FormType != 'Print' && $FormType != 'ReadOnly'),
        "NoReadOnly" => true,
        "Special" => 'DynamicDocument',
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "linked_documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && $FormType == 'Print',
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $inc['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('INC', $inc['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    )
);

if (is_array($RejectionArray) && bYN(GetParm("REJECT_REASON",'Y')))
{
    $FormArray["rejection"] = array("Title" => "Details of Rejection", "Rows" => $RejectionArray);
}