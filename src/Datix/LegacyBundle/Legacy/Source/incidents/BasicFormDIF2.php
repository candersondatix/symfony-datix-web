<?php

if ($FormType == "Print" || $FormType == "ReadOnly")
{
    $gradereadonly = true;
}

if ($FormType == "Search")
{
    $gradereadonly = false;
    $Searchmode = true;
}

//sections and fields available in design mode
$FormArray = array (
    "Parameters" => array("Panels" => "True", "Condition" => false),
    "name" => array(
        "Title" => "Name and reference",
        "Rows" => array(
            array(
                'Name' => 'recordid',
                'Condition' => ($inc['recordid'] || $FormType == 'Design' || $FormType == 'Search')
            ),
            "inc_name",
            array('Name' => 'inc_ourref', 'ReadOnly' => ($_GET["action"] != "addnewincident")),
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'INC',
                'perms' => $DIFPerms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $inc,
                'module' => 'INC',
                'perms' => $DIFPerms,
                'approveobj' => $ApproveObj
            )),
            "inc_dreported",
            "inc_dopened",
            "inc_submittedtime",
            array(
                'Name' => "inc_mgr",
                'ReadOnly' => !(GetParm("DIF_PERMS") == "RM" || (GetParm("DIF_PERMS") == "DIF2" && ((GetParm("REASSIGN_HANDLER","N") == "Y" && $_SESSION['initials'] == $inc["inc_mgr"])) || GetParm("REASSIGN_HANDLER","N") == "A") || $FormType == "Search")
            ),
            "inc_head"
        )
    ),
    "additional" => array(
        "Title" => "Additional Information",
        "Rows" => array(
            "show_person",
            "show_witness",
            "show_employee",
            "show_other_contacts",
            "show_equipment",
            "show_medication",
            "show_pars",
            "show_document"
        )
    ),
    "location" => array(
        "Title" => "Location",
        "Rows" => array(
            'inc_organisation',
            'inc_unit',
            'inc_clingroup',
            'inc_directorate',
            'inc_specialty',
            'inc_loctype',
            'inc_locactual'
        )
    ),
    "tcs" => array(
        "Title" => "Coding",
        "Condition" => ($_SESSION["Globals"]["DIF_2_CCS"] == "TCS_CCS"  || $_SESSION["Globals"]["DIF_2_CCS"] == "TCS" || $_SESSION["Globals"]["DIF_2_CCS"] == ""),
        "Rows" => array(
            'inc_type',
            'inc_category',
            'inc_subcategory',
            'inc_cnstitype'
        )
    ),
    "ccs" => array(
        "Title" => "Datix Common Classification System (CCS)",
        "Condition" => ($_SESSION["Globals"]["DIF_2_CCS"] == "TCS_CCS"  || $_SESSION["Globals"]["DIF_2_CCS"] == "CCS"),
        "Rows" => array(
            array(
                "Name" => "inc_type",
                "Condition" => $_SESSION["Globals"]["DIF_2_CCS"] == "CCS"
            ),
            "inc_carestage",
            "inc_clin_detail",
            "inc_clintype"
        )
    ),
    "ccs2" => array(
        "Title" =>"Datix CCS2",
        "Condition" =>  (bYN(GetParm("CCS2_INC",'N'))),
        "Rows" => array(
            'inc_affecting_tier_zero',
            'inc_type_tier_one',
            'inc_type_tier_two',
            'inc_type_tier_three'
        )
    ),
    "severity" => array(
        "Title" => ("Severity" . ($HideFields["inc_result"] ? "" : " and " . GetFieldLabel("inc_result", "Result"))),
        "Rows" => array(
            "inc_result",
            "inc_severity"
        )
    ),
    "details" => array(
        "Title" => "Details",
        "Rows" => array(
            ['Name' => 'inc_last_updated', 'ReadOnly' => true],
            'inc_dincident',
            'inc_time',
            'inc_time_band',
            'inc_notes',
            'inc_actiontaken',
            array(
                "Name" => "inc_cnstitype",
                "Condition" => !($_SESSION["Globals"]["DIF_2_CCS"] == "TCS_CCS"  || $_SESSION["Globals"]["DIF_2_CCS"] == "TCS" || $_SESSION["Globals"]["DIF_2_CCS"] == "")
            ),
            'inc_rc_required',
            'inc_report_npsa',
            'inc_is_riddor',
            'inc_dsched',
            'inc_notify',
            array('Name' => 'inc_injury', 'Type' => 'ff_select', 'Title' => "Loss/damage", 'Module' => 'INC'),
            array('Name' => 'inc_bodypart', 'Type' => 'ff_select', 'Title' => "Item", 'Module' => 'INC'),
            array('Name' => 'inc_level_intervention', 'Condition' =>  (bYN(GetParm('CCS2_INC', 'N')))),
            array('Name' => 'inc_level_harm', 'Condition' =>  (bYN(GetParm('CCS2_INC', 'N')))),
            "inc_never_event"
        )
    ),
    "riddor" => array(
        "Title" => "RIDDOR",
        "Rows" => array(
            "inc_dnotified",
            "inc_riddor_ref",
            "inc_ridloc",
            "inc_address",
            "inc_localauth",
            "inc_acctype",
            "inc_riddorno"
        )
    ),
    "nrls" => array(
        "Title" => "NPSA NRLS",
        "Rows" => array(
            "inc_n_effect",
            "inc_n_patharm",
            "inc_n_nearmiss",
            "inc_n_actmin",
            "inc_n_paedward",
            "inc_n_paedspec",
            "inc_dnpsa",
            "inc_n_clindir",
            "inc_n_clinspec"
        )
    ),
    "reportedby" => array(
        "Title" => "Reporter",
        "Condition" => (CanSeeContacts('INC', $DIFPerms, $inc['rep_approved']) || !bYN(GetParm("DIF2_HIDE_CONTACTS", "N"))),
        "Rows" => array(
            'inc_repname',
            'inc_reportedby',
            'inc_rep_email',
            'inc_rep_tel'
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormType == 'Design' || ($FormType != 'Search' && $FormType != 'Search' && $inc['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('INC', $inc['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    "medication" => array(
        "Title" => "Medication",
        "Rows" => array(
            "inc_med_stage",
            "inc_med_error",
            "inc_med_drug",
            "inc_med_drug_rt",
            "inc_med_form",
            "inc_med_form_rt",
            "inc_med_dose",
            "inc_med_dose_rt",
            "inc_med_route",
            "inc_med_route_rt"
        )
    ),
    "multi_medication_record" => array(
        "Title" => "Medications",
        "Condition" => (bYN(GetParm("MULTI_MEDICATIONS", "N")) && $FormType != 'Design'),
        "NoFieldAdditions" => true,
        "Include" => "Source/medications/MainMedications.php",
        "Function" => "DoMedicationSection",
        "ExtraParameters" => array('medication_name' => 'inc_medication'),
        "Rows" => array()
    ),
    "multi_medication_design" => array(
        "Title" => "Medications",
        "Condition" => (bYN(GetParm("MULTI_MEDICATIONS", "N")) && $FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        'NoSectionActions' => true,
        'AltSectionKey' => 'multi_medication_record',
        "Listings" => array("MED" => array('module' => "MED", 'Label' => 'Select a listing to use for the list of '.$ModuleDefs["MED"]['REC_NAME_PLURAL'])),
        'FieldFormatsTable' => 'INCMED',
        "Rows" => array(
            'dummy_imed_search_admin',
            'imed_name_admin',
            'imed_brand_admin',
            'imed_manu_admin',
            'imed_class_admin',
            'imed_type_admin',
            'imed_route_admin',
            'imed_dose_admin',
            'imed_form_admin',
            'imed_controlled_admin',
            'imed_price_admin',
            'imed_reference_admin',
            'imed_right_wrong_medicine_admin',
            'imed_serial_no',
            'imed_batch_no',
            'imed_manufacturer_special_admin',
            'dummy_imed_search_correct',
            'imed_name_correct',
            'imed_brand_correct',
            'imed_manu_correct',
            'imed_class_correct',
            'imed_type_correct',
            'imed_route_correct',
            'imed_dose_correct',
            'imed_form_correct',
            'imed_controlled_correct',
            'imed_price_correct',
            'imed_reference_correct',
            'imed_right_wrong_medicine_correct',
            'imed_error_stage',
            'imed_error_type',
            'imed_notes',
            'imed_other_factors'
        )
    ),
    "investigation" => array(
        "Title" => "Investigation",
        "Rows" => array(
            "inc_investigator",
            "inc_inv_dstart",
            "inc_inv_dcomp",
            "inc_cost",
            "dum_inc_grading_initial",
            "dum_inc_grading",
            "inc_inv_outcome",
            "inc_lessons_code",
            "inc_inv_lessons",
            "inc_inquiry",
            "inc_action_code",
            "inc_inv_action"
        )
    ),
    "feedback" => array(
        "Title" => _tk('feedback_title'),
        "Special" => "Feedback",
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        'NoFieldRemoval' => true,
        'NoReadOnly' => true,
        "Rows" => [
            [
                'Name'        => 'dum_fbk_to',
                'Title'       => 'Staff and contacts attached to this record',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_gab',
                'Title'       => 'All users',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_email',
                'Title'       => 'Additional recipients',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_subject',
                'Title'       => 'Subject',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ],
            [
                'Name'        => 'dum_fbk_body',
                'Title'       => 'Body of message',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ]
        ]
    ),
    "linked_records" => array(
        "Title" => _tk('linked_records'),
        "NoFieldAdditions" => true,
        "Special" => "LinkedRecords",
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "payments" => array(
        "Title" =>"Payments",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Listings' => array('payments' => array('module' => 'PAY')),
        'LinkedForms' => array('payments' => array('module' => 'PAY')),
        'ControllerAction' => [
            'listPayments' => [
                'controller' => 'src\\payments\\controllers\\PaymentController'
            ]
        ]
    ),
    "causes" => array(
        "Title" => "Causes",
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'SectionRootCauseDetails' => array(
                'controller' => 'src\\rootcauses\\controllers\\RootCausesController'
            )
        ),
        "Rows" => array()
    ),
    "notepad" => array(
        "Title" => "Notepad",
        "NotModes" => array("Search"),
        "Rows" => array('notes')
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        "Special" => "LinkedActions",
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController')),
        "Rows" => array()
    ),
    "equipment" => array(
        "Title" => "Equipment (legacy)",
        "Rows" => array(
            'inc_eqpt_type',
            'inc_equipment',
            'inc_serialno',
            'inc_manufacturer',
            'inc_description',
            'inc_supplier',
            'inc_servrecords',
            'inc_model',
            'inc_location',
            'inc_qdef',
            'inc_dmanu',
            'inc_lastservice',
            'inc_dputinuse',
            'inc_batchno',
            'inc_cemarking',
            'inc_outcomecode',
            'inc_dmda',
            'inc_outcome',
            'inc_defect'
        )
    ),
    "pars" => array("Title" => "SIRS reporting information",
        "Rows" => array(
            "inc_pars_clinical",
            "inc_user_action",
            "inc_agg_issues",
            "inc_pars_pri_type",
            "inc_pars_sec_type",
            "inc_pol_called",
            "inc_pol_call_time",
            "inc_pol_attend",
            "inc_pol_att_time",
            "inc_pol_action",
            "inc_pol_crime_no",
            "inc_pars_address",
            "inc_postcode",
            "show_assailant",
            "inc_tprop_damaged",
            "inc_pars_first_dexport",
            "inc_pars_dexport",
        )
    ),
    "tprop" => array(
        "Title" => 'Equipment',
        "NoFieldAdditions" => true,
        
		"NotModes" => array("New","Search"),
        'LinkedForms' => array('linked_assets' => array('module' => 'AST')),
        "ControllerAction" => array(
            "listLinkedEquipment" => array(
                "controller" => "src\\equipment\\controllers\\EquipmentController"
            )
        ),	
        "Rows" => array(),
        'Listings' => array('linked_assets' => array('module' => 'AST')),
    ),
    "pas" => array(
        "Title" => "PAS",
        "Condition" => bYN(GetParm("SHOW_OLD_PAS", "N")),
        "Rows" => array(
            'inc_pasno1',
            'inc_pasno2',
            'inc_pasno3'
        )
    ),
    "causal_factor_header" => array(
        "Title" => "Causal factors for " . _tk('INCNameTitle'),
        'NotModes' => array('Search'),
        'NewPanel' => true,
        "Rows" => array(
            'inc_causal_factors_linked'
        )
    ),
    "causal_factor" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType != 'Design'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'DoCausalFactorsSection' => array(
                'controller' => 'src\\causalfactors\\controllers\\CausalFactorsController'
            )
        ),
        "ExtraParameters" => array('causal_factor_name' => 'inc_causal_factor'),
        'NoReadOnly' => true,
        "Rows" => array()
    ),
    "causal_factor_design" => array(
        "Title" => _tk('causal_factors'),
        "Condition" => ($FormType == 'Design'),
        "NoFieldAdditions" => true,
        "NoFieldRemoval" => true,
        "AltSectionKey" => "causal_factor",
        "NoSectionActions" => true,
        "Rows" => array(
            'caf_level_1',
            'caf_level_2',
        )
    ),
    'openness_transparency' => [
        'Title' => _tk('openness_transparency_title'),
        'Rows' => [
            'inc_ot_q1',
            'inc_ot_q2',
            'inc_ot_q3',
            'inc_ot_q4',
            'inc_ot_q5',
            'inc_ot_q6',
            'inc_ot_q7',
            'inc_ot_q8',
            'inc_ot_q9',
            'inc_ot_q10',
            'inc_ot_q11',
            'inc_ot_q12',
            'inc_ot_q13',
            'inc_ot_q14',
            'inc_ot_q15',
            'inc_ot_q16',
            'inc_ot_q17',
            'inc_ot_q18',
            'inc_ot_q19',
            'inc_ot_q20'
        ],
        'NotModes' => ['Search']
    ],
    "history" => array(
        "Title" => "Notifications",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'MakeEmailHistoryPanel' => [
                'controller' => 'src\\email\\controllers\\EmailHistoryController'
            ]
        ],
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "message" => array(
        "Title" => "Messages",
        "Function" => "SectionSecureMessages",
        "Condition" => $_SESSION["licensedModules"][MOD_MESSAGES] && GetParm("MSG_PERMS"),
        "NotModes" => array("New","Search","Print"),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController')),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    "permissions_user" => array(
        "Title" => "User Permissions",
        "NoFieldAdditions" => true,
        "Function" => "RecordPermissions",
        "ExtraParameters" => array('module' => 'INC', 'type' => 'user'),
        "Condition" => bYN(GetParm('SEC_RECORD_PERMS', 'N')),
        "NotModes" => array("New","Search","Print"),
        "Rows" => array()
    ),
    "permissions_group" => array(
        "Title" => "Group Permissions",
        "NoFieldAdditions" => true,
        "Function" => "RecordPermissions",
        "ExtraParameters" => array('module' => 'INC', 'type' => 'group'),
        "Condition" => bYN(GetParm('SEC_RECORD_PERMS', 'N')),
        "NotModes" => array("New","Search","Print"),
        "Rows" => array()
    ),
    "rejection" => GenericRejectionArray('INC', $inc),
    "rejection_history" => array(
        "Title" => _tk('reasons_history_title'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'SectionRejectionHistory' => array(
                'controller' => 'src\\reasons\\controllers\\ReasonsController'
            )
        ),
        "NotModes" => array("New","Search"),
        "Condition" =>  (bYN(GetParm("REJECT_REASON",'Y'))),
        "Rows" => array()
    )
);

if (bYN(GetParm("READ_ONLY_REPORTER", "N")))
{
    $FormArray['reportedby']['ReadOnly'] = ($FormType != 'New');
}

// Add contact sections for each contact type.
foreach ($ModuleDefs['INC']['CONTACTTYPES'] as $ContactTypeDetails)
{
    $ContactArray['contacts_type_'.$ContactTypeDetails['Type']] = array(
        'Title' => $ContactTypeDetails['Plural'],
        'NoFieldAdditions' => true,
        'ControllerAction' => array(
            'ListLinkedContacts' => array(
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            )
        ),
        'ExtraParameters' => array('link_type' => $ContactTypeDetails['Type']),
        'NotModes' => array('New','Search'),
        'Listings' => array('contacts_type_'.$ContactTypeDetails['Type'] => array('module' => 'CON')),
        'LinkedForms' => array($ContactTypeDetails['Type'] => array('module' => 'CON')),
        'Condition' => (CanSeeContacts('INC', $DIFPerms, $inc['rep_approved']) || !bYN(GetParm('DIF2_HIDE_CONTACTS', 'N'))),
        'Rows' => array()
    );
}

array_insert_datix($FormArray, 'causes', $ContactArray);

require_once 'Source/libs/ModuleLinks.php';

foreach (getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array('module' => $link_mod, 'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']);
}

if(GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}