<?php

use src\framework\registry\Registry;
use src\framework\controller\Request;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\exceptions\ReportException;

function ExportToPDFOptions()
{
    if ('listing' == $_GET['report_type'])
    {
        // fetch the total row count for the report, used to judge the recommended export option
        $factory = new PackagedReportModelFactory();
        
        try
        {
            $packagedReport = $factory->getEntityFactory()->createCurrentPackagedReportFromRequest(new Request());
            $packagedReport->report->setReturnAllRecords(true);
            
            $totalRows = $factory->getReportEngine($packagedReport)->getRowCount($packagedReport);
            
            $suggestCSV = $totalRows >= 3000;
        }
        catch (ReportException $e)
        {
            Registry::getInstance()->getLogger()->logEmergency('Unable to determine total number of rows of listing report');
        }
    }
    
    $extraOptions = ($_GET['exportmode'] != 'graph' || GetParm('GRAPH_ENGINE', 'FUSIONCHARTS') == 'JPGRAPH');

    echo '<script language="JavaScript" type="text/javascript">
function getRadioValue(radioGroupName)
{
        radios = document.getElementsByName(radioGroupName);
        for (i = 0; i < radios.length; i++) {
            if (radios[i].checked) return radios[i].value;
        }
}
function setReturns(form_key)
{
    if(form_key == undefined)
    {
        form_key = 0;
    }
    '.($extraOptions ? '
    jQuery("#orientation_post").val(getRadioValue(\'orientation\'));
    jQuery("#papersize_post").val(document.getElementById(\'papersize\').value);' : '').'
    jQuery("#reportoutputformat").val(getRadioValue(\'reportoutputformat\'));

    return true;
}

function returnReturns()
{
    var rArray = new Array();'.($extraOptions ? '
    rArray[\'orientation_post\'] = getRadioValue(\'orientation\');
    rArray[\'papersize_post\'] = document.getElementById(\'papersize\').value;' : '').'
    rArray[\'reportoutputformat\'] = getRadioValue(\'reportoutputformat\');

    return rArray;
}';

if ($suggestCSV)
{
    echo '
jQuery(function(){
    suggestCsvExport();
})';
}

echo '
</script>';

    echo '
    Options: <br/><br/>
    <table style=\'
    border-width: 1px;
    border-spacing: 0px;
    border-style: none none none none;
    border-color: white white white white;
    border-collapse: collapse;
    \' >

    <tr>
    <td style=\'
    border-width: 1px;
    padding: 3px;
    border-style: inset inset inset inset;
    border-color: white white white white;
    width: 200px;
    \'>

          <input type=\'radio\' id="output-pdf" name=\'reportoutputformat\' value=\'pdf\'' . (!$suggestCSV ? 'checked="checked"' : '') . ''.($extraOptions ? 'onclick=document.getElementById(\'orientation1\').disabled=false;document.getElementById(\'orientation2\').disabled=false;document.getElementById(\'papersize\').disabled=false;' : '').'> <label for="output-pdf">PDF</label><br>';

    if ($_GET['noexcel'] != '1')
    {
        echo "<input type='radio' id=\"output-excel\" name='reportoutputformat' value='excel' ".($extraOptions ? "onclick=document.getElementById('orientation1').disabled=true;document.getElementById('orientation2').disabled=true;document.getElementById('papersize').disabled=false;" : "")."> <label for=\"output-excel\">Excel</label><br>";
        if ($_GET['nocsv'] != '1')
        {
            echo "<input type='radio' id=\"output-csv\" name='reportoutputformat' value='csv' " . ($suggestCSV ? " checked=\"checked\"" : "") . ($extraOptions ? "onclick=document.getElementById('orientation1').disabled=true;document.getElementById('orientation2').disabled=true;document.getElementById('papersize').disabled=true;" : "") . "> <label for=\"output-csv\">";

            if($suggestCSV)
            {
                echo "<span id=\"csv_label\" title=\"" . _tk('export_to_csv_recommend') . "\">Raw data (CSV)</span><br />";
            }
            else
            {
                echo "Raw data (CSV)<br />";
            }

            echo "</label>";
        }
    }

    echo'
    </td></tr>'.
        ($extraOptions ? '
    <tr>
    <td style=\'
    border-width: 1px;
    padding: 3px;
    border-style: inset inset inset inset;
    border-color: white white white white;
    width: 200px;
    \'>
            <input type=\'radio\' id=\'orientation1\' name=\'orientation\' value=\'portrait\' '.(GetParm('DEFAULT_PAPER_ORIENTATION', 'P') == 'P' ? 'checked' : '').'> <label for="orientation1">Portrait</label><br>
            <input type=\'radio\' id=\'orientation2\' name=\'orientation\' value=\'landscape\' '.(GetParm('DEFAULT_PAPER_ORIENTATION', 'P') == 'L' ? 'checked' : '').'> <label for="orientation2">Landscape</label><br>
    </td></tr>
    <tr>
    <td style=\'
    border-width: 1px;
    padding: 3px;
    border-style: inset inset inset inset;
    border-color: white white white white;
    width: 200px;
    \'>
    &nbsp Paper size: &nbsp<select id=\'papersize\' name=\'papersize\'>
        <option value=\'a4\' '.(GetParm('DEFAULT_PAPER_SIZE', 'A4') == 'A4' ? 'selected' : '').'>A4</option>
        <option value=\'a3\' '.(GetParm('DEFAULT_PAPER_SIZE', 'A4') == 'A3' ? 'selected' : '').'>A3</option>
        <option value=\'letter\' '.(GetParm('DEFAULT_PAPER_SIZE', 'A4') == 'LETTER' ? 'selected' : '').'>Letter</option>
    </select>
    </td></tr>' : '').'
    </table>';
}

function ExportSwitch()
{
    if ($_POST['reportoutputformat'])
    {
        $output_format = $_POST['reportoutputformat'];
    }
    else
    {
        $output_format = 'pdf';
    }

    if ($_POST['exportmode'] == 'listing')
    {
        $html = $_SESSION['listingreportstream'];
        ExportReport($html, 'listing', $output_format);
    }
    elseif ($_POST['exportmode'] == 'crosstab')
    {
        $html = $_SESSION['CurrentReport']->GetPDFExportHTML();
        ExportReport($html, 'crosstab', $output_format);
    }
    elseif ($_POST['exportmode'] == 'graph')
    {
        if ($output_format == 'pdf')
        {
            $tmpfile = tempnam("/tmp", "pn2");
            file_put_contents($tmpfile, $_SESSION['pngimgstream']);
            ExportReport('<h1>Header</h1>', 'graph', $output_format, $tmpfile);
            unlink($tmpfile);
        }
        elseif ($output_format == 'excel')
        {
            ExportReport('', 'graph', 'excel');
        }
    }
}
function ExportSwitchFromDashboard()
{
    if ($_GET['reportoutputformat'])
    {
        $output_format = $_GET['reportoutputformat'];
    }
    else
    {
        $output_format = 'pdf';
    }

    if ($_GET['widget_id'])
    {
        $widget_id = $_GET['widget_id'];
    }
    else
    {
        $widget_id = 0;
    }

    if ($_GET['exportmode'] == 'listing')
    {
        $ListingReport = unserialize($_SESSION['listingreportstream_dashboard'][$widget_id]);
        $html = $ListingReport;
        ExportReport($html, 'listing', $output_format);
    }
    elseif ($_GET['exportmode'] == 'crosstab')
    {
        $Crosstab = unserialize($_SESSION['SerializedReports'][$widget_id]);
        $html = $Crosstab->GetPDFExportHTML();
        ExportReport($html, 'crosstab', $output_format);
    }
    elseif ($_GET['exportmode'] == 'graph')
    {
        if ($output_format == 'pdf')
        {
            $tmpfile = tempnam("/tmp", "pn2");
            file_put_contents($tmpfile, $_SESSION['pngimgstream_dashboard'][$widget_id]);
            ExportReport('<h1>Header</h1>', 'graph', $output_format, $tmpfile);
            unlink($tmpfile);
        }
        elseif ($output_format == 'excel')
        {
            ExportReport('', 'graph', 'excel');
        }
    }
}
function ExportReport($html, $mode = 'graph', $output_format = 'pdf', $pngimg = '')
{
    global $dtxprint_footer, $ModuleDefs, $ClientFolder, $dtxpdf_footer;

    if ($_REQUEST['widget_id'])
    {
        $widget_id = $_REQUEST['widget_id'];
    }
    else
    {
        $widget_id = 0;
    }

    if (!isset($dtxpdf_footer) && $dtxprint_footer != '')
    {
        $dtxpdf_footer = $dtxprint_footer;
    }

    if ($output_format == 'pdf')
    {
        if ($mode == 'graph')
        {
            require_once __DIR__.'/../thirdpartylibs/MPDF/mpdf.php';

            switch ($_REQUEST['orientation_post'])
            {
                case 'portrait':
                    $orientation = 'P';
                    break;
                case 'landscape':
                    $orientation = 'L';
                    break;
                default:
                    $orientation = 'P';
                    break;
            }

            $mpdf = new mPDF('', $_REQUEST['papersize_post'], 7, 'DejaVuSans', 15, 15, 16, 16, 9, 9, $orientation);
            $mpdf->AliasNbPages();
            $mpdf->SetLeftMargin(15);
            $mpdf->SetFontSize(14);
            $mpdf->SetAutoPageBreak(true);
            $mpdf->AddPage();
            $mpdf->Image($pngimg, 0, 20, $mpdf->w, 0, 'PNG');
            $mpdf->Output('DatixWebReport.pdf', 'D');
            obExit();
        }
        elseif($mode == 'crosstab' || $mode == 'listing')
        {
            define('FPDF_FONTPATH','font/');
            require_once __DIR__.'/../export/PDFTableWrapper.php';

            $pdf = new PDFTableWrapper($_REQUEST['orientation_post'],'mm',$_REQUEST['papersize_post']);
            $pdf->SetHeaderTitle($_REQUEST['rep_name']);
            $pdf->AliasNbPages();
            $pdf->SetFont('Helvetica', '', 7);
            $pdf->SetMargins(20,20,20);
            $html = ParseHTMLTableForExport($html, $output_format, $pdf);
            $pdf = OutputPDFTable($html, $mode, $_REQUEST['orientation_post'], $_REQUEST['papersize_post'], $pdf);

            if (\UnicodeString::strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false)
            {
                // fix "file not found" issue when opening file directly from IE6
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            }

            $pdf->Output('DatixWebReport.pdf', 'D');
            obExit();
        }
    }
    elseif ($output_format == 'excel')
    {
        if (\UnicodeString::strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
        {
            ob_end_clean();
            header("Pragma: public");
            header("Cache-Control: max-age=0");
        }

        if ($mode == 'crosstab' || $mode == 'listing')
        {
            ini_set('include_path', ini_get('include_path').';../Classes/');
            require_once 'export/PHPExcel.php';
            require_once 'export/PHPExcel/Writer/Excel2007.php';
            require_once 'export/PHPExcel/IOFactory.php';

            // Note that the PHPExcel object already exists in the session with
            // all the required data. This is being built in Crosstab->GetData()

            if ($mode == 'crosstab') //we can generate the excel object at this point
            {
                if (is_array($_SESSION['SerializedReports']) && $widget_id > 0)
                {
                    $Crosstab = unserialize($_SESSION['SerializedReports'][$widget_id]);
                }
                else
                {
                    $Crosstab = $_SESSION['CurrentReport'];
                }

                $objPHPExcel = $Crosstab->GetExcelExportObject();
            }
            else
            {
                if (is_array($_SESSION['crosstabtablePHPExcel']) && $widget_id > 0)
                {
                    $objPHPExcel = unserialize($_SESSION['crosstabtablePHPExcel'][$widget_id]);
                }
                else
                {
                    $objPHPExcel = unserialize($_SESSION['crosstabtablePHPExcel']);
                }
            }

            header("Pragma: public");
            header("Cache-Control: max-age=0");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="DatixWebReport.xlsx"');
            header("Content-Encoding: none");
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
        }
        elseif ($mode == 'graph')
        {
            ExportGraphToExcelPHP();
            exit;
        }
    }
}

function ExportGraphToExcelPHP()
{
    if ($_GET['widget_id'])
    {
        $widget_id = $_GET['widget_id'];
        $_SESSION['excel'] = $_SESSION['excel_dashboard'][$widget_id];
    }

    $ydata_group = $_SESSION['excel']['ydata_group'];
    $graphtype = $_SESSION['excel']['graphtype'];
    $data_set = 1;

    if (is_array($ydata_group))
    {
        $data_sets = count($ydata_group);

        /** Include path **/
        ini_set('include_path', ini_get('include_path').';../Classes/');

        /** PHPExcel */
        require_once('export/PHPExcel.php');

        /** PHPExcel_Writer_Excel2007 */
        require_once 'export/PHPExcel/Writer/Excel2007.php';
        require_once 'export/PHPExcel/IOFactory.php';

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setTitle("DatixWeb Excel export");
        $objPHPExcel->getProperties()->setSubject("DatixWeb Excel export");
        $objPHPExcel->getProperties()->setDescription("DatixWeb Excel export");

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Data');

        foreach ($ydata_group as $key => $ydata)
        {
            $numdata = count($ydata['data']);

            if ($numdata  > 0)
            {
                if (empty($_SESSION['excel']['column_headers']))
                {
                    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'X-Axis');
                    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

                    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Data');
                    $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

                    if ($graphtype == 'spc')
                    {
                        if ($_SESSION['excel']['spc_chart_type'] == 'runchart')
                        {
                            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                        }

                        if ($_SESSION['excel']['spc_chart_type'] == 'movingrange')
                        {
                            $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Moving range');
                            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'RUCL');
                            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                        }

                        if ($_SESSION['excel']['spc_chart_type'] == 'cchart')
                        {
                            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'UCL');
                            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'LCL');
                            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'UWL');
                            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'LWL');
                            $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                        }

                        if ($_SESSION['excel']['spc_chart_type'] == 'ichart')
                        {
                            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'UCL');
                            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'LCL');
                            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                        }
                    }

                    if ($graphtype == 'pareto')
                    {
                        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Cumulative data');
                        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                        $pareto_datasum = 0;
                    }

                    if (\UnicodeString::strpos($graphtype, 'horizontal') !== false)
                    {
                        // data arrays need to be reversed so the ordering in Excel matches the web image
                        $ydata['data'] = array_reverse($ydata['data']);
                        $ydata['text'] = array_reverse($ydata['text']);
                    }

                    for ($i = 0; $i < $numdata; $i++)
                    {
                        $k = $i+2;
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('0', $k, str_replace('&#034', '"', $ydata['text'][$i]));
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('1', $k, $ydata['data'][$i]);

                        if ($graphtype == 'spc')
                        {
                            if ($_SESSION['excel']['spc_chart_type'] == 'runchart')
                            {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $k, $_SESSION['excel']['statdata']['average'][0]);
                            }
                            elseif ($_SESSION['excel']['spc_chart_type'] == 'movingrange')
                            {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $k, $_SESSION['excel']['statdata']['average'][0]);
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('3', $k, round($_SESSION['excel']['statdata']['upper_ctrl_limit'][0], 2));
                            }
                            elseif ($_SESSION['excel']['spc_chart_type'] == 'cchart')
                            {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $k, round($_SESSION['excel']['statdata']['average'][0], 2));
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('3', $k, round($_SESSION['excel']['statdata']['upper_ctrl_limit'][0], 2));
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('4', $k, round($_SESSION['excel']['statdata']['lower_ctrl_limit'][0], 2));
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('5', $k, round($_SESSION['excel']['statdata']['upper_warning_limit'][0], 2));
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('6', $k, round($_SESSION['excel']['statdata']['lower_warning_limit'][0], 2));
                            }
                            elseif ($_SESSION['excel']['spc_chart_type'] == 'ichart')
                            {
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $k, round($_SESSION['excel']['statdata']['average'][0], 2));
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('3', $k, round($_SESSION['excel']['statdata']['upper_ctrl_limit'][0], 2));
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('4', $k, round($_SESSION['excel']['statdata']['lower_ctrl_limit'][0], 2));
                            }
                        }

                        if ($graphtype == 'pareto')
                        {
                            $pareto_datasum += $ydata['data'][$i];
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $k, $pareto_datasum);
                        }
                    }
                }
                else
                {
                    $coord = 'A' . ($data_set);
                    $objPHPExcel->getActiveSheet()
                        ->setCellValueByColumnAndRow($data_set, 1, $_SESSION['excel']['column_headers'][$key]['title']);

                    $rh = 0;

                    if (is_array($_SESSION['excel']['row_headers']))
                    {
                        $rowHeaders = $_SESSION['excel']['row_headers'];

                        if (\UnicodeString::strpos($graphtype, 'horizontal') !== false)
                        {
                            // data arrays need to be reversed so the ordering in Excel matches the web image
                            $rowHeaders = array_reverse($rowHeaders);
                            $ydata['data'] = array_reverse($ydata['data']);
                        }

                        foreach ($rowHeaders as $rh_array)
                        {
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($rh+2), $rh_array['title']);
                            $coord = 'A' . ($rh+2);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($data_set, ($rh+2), $ydata['data'][$rh]);
                            $objPHPExcel->getActiveSheet()->getStyle($coord)->getFont()->setBold(true);
                            $rh++;
                        }
                    }

                    $colInLetters = ExcelConvertToLetter($data_set);

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($colInLetters)->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getStyle($colInLetters.'1')->getFont()->setBold(true);

                }
            }

            $data_set++;
        }

        /** Create new sheet for the chart */
        $objPHPExcel->createSheet(1);
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('Graph');

        $pChart = new PHPExcel_Chart();

        $pChart->setChartTitle($_SESSION['excel']['graphtitle']);
        $pChart->setChartType($graphtype);

        /** Defining chart settings via new Chart object*/
        if (empty($_SESSION['excel']['column_headers']))
        {
            if ($graphtype == 'spc')
            {
                $pChart->setLegend(true);
            }
            elseif ($graphtype != 'spc' && \UnicodeString::strpos($graphtype, 'pie') === false)
            {
                $pChart->setLegend(false);
            }
            elseif ($numdata > 15 && $graphtype != 'spc' && $graphtype != 'pie')
            {
                $pChart->setLegend(true);

                // decrease legend font size
                if ($numdata > 15 && $numdata <= 20)
                {
                    $pChart->setChartLegendFontSize(9);
                }
                elseif ($numdata > 20)
                {
                    $pChart->setChartLegendFontSize(8);
                }
            }

            $pChart->setDimensionC(1);
            $pChart->setDimensionR($numdata);
        }
        else
        {
            $pChart->setLegend(true);

            // decrease legend font size
            if ($data_sets > 15 && $data_sets <= 20)
            {
                $pChart->setChartLegendFontSize(10);
            }
            elseif ($data_sets > 20 && $data_sets <= 28)
            {
                $pChart->setChartLegendFontSize(9);
            }
            elseif ($data_sets > 28)
            {
                $pChart->setChartLegendFontSize(8);
                $pChart->setChartLegendExtended(true);
            }

            $pChart->setDimensionC($data_sets);
            $pChart->setDimensionR($numdata);
        }

        $objPHPExcel->getActiveSheet()->addChart($pChart);

        /** Stream it to the browser */
        header("Pragma: public");
        header("Cache-Control: max-age=0");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="DatixWebReport.xlsx"');
        header("Content-Encoding: none");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}

function ExportGraphToExcel()
{
    if ($_GET['widget_id'])
    {
        $widget_id = $_GET['widget_id'];
        $_SESSION['excel'] = $_SESSION['excel_dashboard'][$widget_id];
    }

    $ydata_group = $_SESSION['excel']['ydata_group'];
    $graphtype = $_SESSION['excel']['graphtype'];
    $html = "
    function excelColumnLetter(intCol)
    {
        if(intCol <= 26)
        {
            return String.fromCharCode(intCol+64);
        }
        else
        {
            var intFirstLetter = Math.floor(intCol/26) + 64;
            var firstLetter = String.fromCharCode(intFirstLetter);
            var secondLetter = String.fromCharCode((intCol % 26) + 64);
            return firstLetter + secondLetter;
        }
    }

    var chtypes = [];
        chtypes['pie'] = 5;
        chtypes['bar'] = 51;
        chtypes['line'] = 4; // basic line
        chtypes['linemarkers'] = 65; // small squares as markers
        chtypes['pareto'] = 51; // and 'line' for cumulative data

    function ExportGraphToExcelJS()
    {
        try
        {
            var Excel = new ActiveXObject('Excel.Application');
        }
        catch(err)
        {
            var errorMsg = '".str_replace("'", "\\'", _tk('export_activex_disabled'))."';
            alert(errorMsg);
            return false;
        }

        var excel_version = '';
        if(Excel.Version < '11.0')
        {
            excel_version = '2000';
        }
        else if(Excel.Version < '12.0')
        {
            excel_version = '2003';
        }
        else if(Excel.Version >= '12.0')
        {
            excel_version = '2007';
        }

        var wb = Excel.Workbooks.Add();
        wb.WorkSheets('Sheet2').Delete;
        wb.WorkSheets('Sheet3').Delete;
        var Sheet = wb.activeSheet;
        ";
    $data_set = 1;

    if (is_array($ydata_group))
    {
        $data_sets = count($ydata_group);

        foreach ($ydata_group as $key => $ydata)
        {
            $numdata = count($ydata['data']);

            if ($numdata  > 0)
            {
                if (empty($_SESSION['excel']['column_headers']))
                {
                    $html .= "
                        Sheet.Cells(1,1).NumberFormat = '@';
                        Sheet.Cells(1,1).Value = 'X-Axis';
                        Sheet.Cells(1,1).Font.Bold = true;

                        Sheet.Cells(1,2).NumberFormat = '@';
                        Sheet.Cells(1,2).Value = 'Data';
                        Sheet.Cells(1,2).Font.Bold = true;";

                    if ($graphtype == 'spc')
                    {
                        if ($_SESSION['excel']['spc_chart_type'] == 'runchart')
                        {
                            $html .= "
                                Sheet.Cells(1,3).NumberFormat = '@';
                                Sheet.Cells(1,3).Value = 'Mean';
                                Sheet.Cells(1,3).Font.Bold = true;";
                        }

                        if ($_SESSION['excel']['spc_chart_type'] == 'movingrange')
                        {
                            $html .= "
                                Sheet.Cells(1,2).NumberFormat = '@';
                                Sheet.Cells(1,2).Value = 'Moving range';
                                Sheet.Cells(1,3).NumberFormat = '@';
                                Sheet.Cells(1,3).Value = 'Mean';
                                Sheet.Cells(1,3).Font.Bold = true;
                                Sheet.Cells(1,4).NumberFormat = '@';
                                Sheet.Cells(1,4).Value = 'RUCL';
                                Sheet.Cells(1,4).Font.Bold = true;";
                        }

                        if ($_SESSION['excel']['spc_chart_type'] == 'cchart')
                        {
                            $html .= "
                                Sheet.Cells(1,3).NumberFormat = '@';
                                Sheet.Cells(1,3).Value = 'Mean';
                                Sheet.Cells(1,3).Font.Bold = true;
                                Sheet.Cells(1,4).NumberFormat = '@';
                                Sheet.Cells(1,4).Value = 'UCL';
                                Sheet.Cells(1,4).Font.Bold = true;
                                Sheet.Cells(1,5).NumberFormat = '@';
                                Sheet.Cells(1,5).Value = 'LCL';
                                Sheet.Cells(1,5).Font.Bold = true;
                                Sheet.Cells(1,6).NumberFormat = '@';
                                Sheet.Cells(1,6).Value = 'UWL';
                                Sheet.Cells(1,6).Font.Bold = true;
                                Sheet.Cells(1,7).NumberFormat = '@';
                                Sheet.Cells(1,7).Value = 'LWL';
                                Sheet.Cells(1,7).Font.Bold = true;";
                        }

                        if ($_SESSION['excel']['spc_chart_type'] == 'ichart')
                        {
                            $html .= "
                                Sheet.Cells(1,3).NumberFormat = '@';
                                Sheet.Cells(1,3).Value = 'Mean';
                                Sheet.Cells(1,3).Font.Bold = true;
                                Sheet.Cells(1,4).NumberFormat = '@';
                                Sheet.Cells(1,4).Value = 'UCL';
                                Sheet.Cells(1,4).Font.Bold = true;
                                Sheet.Cells(1,5).NumberFormat = '@';
                                Sheet.Cells(1,5).Value = 'LCL';
                                Sheet.Cells(1,5).Font.Bold = true;";
                        }
                    }

                    if ($graphtype == 'pareto')
                    {
                        $html .= "
                                Sheet.Cells(1,3).NumberFormat = '@';
                                Sheet.Cells(1,3).Value = 'Cumulative data';
                                Sheet.Cells(1,3).Font.Bold = true;";
                        $pareto_datasum = 0;
                    }

                    if (\UnicodeString::strpos($graphtype, 'horizontal') !== false)
                    {
                        // data arrays need to be reversed so the ordering in Excel matches the web image
                        $ydata['data'] = array_reverse($ydata['data']);
                        $ydata['text'] = array_reverse($ydata['text']);
                    }

                    for ($i = 0; $i < $numdata; $i++)
                    {
                        $k = $i+2;
                        $html .= "Sheet.Cells($k,1).NumberFormat = '@'; ";
                        $html .= "Sheet.Cells($k,1).Value = '".str_replace("'", "\\'", $ydata['text'][$i])."'; ";

                        $html .= "Sheet.Cells($k,2).Value = '".$ydata['data'][$i]."'; ";

                        if ($graphtype == 'spc')
                        {
                            if ($_SESSION['excel']['spc_chart_type'] == 'runchart')
                            {
                                $html .= "Sheet.Cells($k,3).Value = '".$_SESSION['excel']['statdata']['average'][0]."'; ";
                            }
                            elseif ($_SESSION['excel']['spc_chart_type'] == 'movingrange')
                            {
                                $html .= "Sheet.Cells($k,3).Value = '".$_SESSION['excel']['statdata']['average'][0]."'; ";
                                $html .= "Sheet.Cells($k,4).Value = '".round($_SESSION['excel']['statdata']['upper_ctrl_limit'][0], 2)."'; ";
                            }
                            elseif ($_SESSION['excel']['spc_chart_type'] == 'cchart')
                            {
                                $html .= "Sheet.Cells($k,3).Value = '".round($_SESSION['excel']['statdata']['average'][0], 2)."'; ";
                                $html .= "Sheet.Cells($k,4).Value = '".round($_SESSION['excel']['statdata']['upper_ctrl_limit'][0], 2)."'; ";
                                $html .= "Sheet.Cells($k,5).Value = '".round($_SESSION['excel']['statdata']['lower_ctrl_limit'][0], 2)."'; ";
                                $html .= "Sheet.Cells($k,6).Value = '".round($_SESSION['excel']['statdata']['upper_warning_limit'][0], 2)."'; ";
                                $html .= "Sheet.Cells($k,7).Value = '".round($_SESSION['excel']['statdata']['lower_warning_limit'][0], 2)."'; ";
                            }
                            elseif ($_SESSION['excel']['spc_chart_type'] == 'ichart')
                            {
                                $html .= "Sheet.Cells($k,3).Value = '".round($_SESSION['excel']['statdata']['average'][0], 2)."'; ";
                                $html .= "Sheet.Cells($k,4).Value = '".round($_SESSION['excel']['statdata']['upper_ctrl_limit'][0], 2)."'; ";
                                $html .= "Sheet.Cells($k,5).Value = '".round($_SESSION['excel']['statdata']['lower_ctrl_limit'][0], 2)."'; ";
                            }
                        }

                        if ($graphtype == 'pareto')
                        {
                            $pareto_datasum += $ydata['data'][$i];
                            $html .= "Sheet.Cells($k,3).Value = '$pareto_datasum'; ";
                        }
                    }
                }
                else
                {
                    $html .= "Sheet.Cells(1,($data_set+1)).NumberFormat = '@'; ";
                    $html .= "Sheet.Cells(1,($data_set+1)).Font.Bold = true;";
                    $html .= "Sheet.Cells(1,($data_set+1)).Value = '".str_replace("'", "\\'", $_SESSION['excel']['column_headers'][$key]['title'])."'; ";
                    $rh = 0;

                    if (is_array($_SESSION['excel']['row_headers']))
                    {
                        $rowHeaders = $_SESSION['excel']['row_headers'];

                        if (\UnicodeString::strpos($graphtype, 'horizontal') !== false)
                        {
                            // data arrays need to be reversed so the ordering in Excel matches the web image
                            $rowHeaders = array_reverse($rowHeaders);
                            $ydata['data'] = array_reverse($ydata['data']);
                        }

                        foreach ($rowHeaders as $rh_array)
                        {
                            $html .= "Sheet.Cells(".($rh+2).",1).NumberFormat = '@'; ";
                            $html .= "Sheet.Cells(".($rh+2).",1).Font.Bold = true;";
                            $html .= "Sheet.Cells(".($rh+2).",1).Value = '".str_replace("'", "\\'", $rh_array['title'])."'; ";
                            $html .= "Sheet.Cells(".($rh+2).",($data_set+1)).Value = '".$ydata['data'][$rh]."'; ";
                            $rh++;
                        }
                    }
                }
            }

            $data_set++;
        }
    }

    $html .= "
        Sheet.Name = 'Data';
        Sheet.Columns.Autofit();
        var awb = Excel.ActiveWorkbook;
        awb.Charts.Add();
        var ch = awb.ActiveChart;
        ch.Name = 'Graph';

        if(excel_version != '2007')
        {
            try
            {
                ch.PlotArea.Interior.ColorIndex = 2;
            }
            catch(err) { }
        }

        var rangsource;
        ";

    if (empty($_SESSION['excel']['column_headers']))
    {
        if ($graphtype != 'spc' && \UnicodeString::strpos($graphtype, 'pie') === false)
        {
            $html .= "ch.HasLegend = false;";
        }
        elseif ($numdata > 15 && $graphtype != 'spc')
        {
            $html .= "ch.Legend.Font.Size = '7';";
        }

        $html .= "rangsource = Sheet.Range('A2:B".($numdata+1)."');";
    }
    else
    {
        $html .= "
            var excelLastColumn = excelColumnLetter(".($data_sets+1).");
            rangsource = Sheet.Range('A1:'+excelLastColumn+'".($numdata+1)."');";
    }

    // http://msdn.microsoft.com/en-us/library/bb241008.aspx
    switch ($graphtype)
    {
        case 'bar':
            $html .= 'ch.ChartType = 51;';
            break;
        case 'horizontalbar':
            $html .= 'ch.ChartType = 57;';
            break;
        case 'stackedbar':
            $html .= 'ch.ChartType = 52;';
            break;
        case 'horizontalstackedbar':
            $html .= 'ch.ChartType = 58;';
            break;
        case 'line':
            $html .= 'ch.ChartType = 65;';
            break;
        case 'pie':
            $html .= 'ch.ChartType = 5;';
            break;
        case 'pieexploded':
            $html .= 'ch.ChartType = 69;';
            break;
        case 'spc':
            $html .= 'ch.ChartType = 65;';
            break;
        case 'pareto':
            $html .= 'ch.ChartType = 51;';
            break;
            break;
    }

    $html .= "
        ch.SetSourceData(rangsource, 2);
        try
        {
            ch.HasTitle = true;
            ch.ChartTitle.Caption = '".str_replace("'", "\\'", str_replace('\\', '\\\\', Escape::EscapeEntities($_SESSION['excel']['graphtitle'])))."';
        }
        catch(err) { }
    ";

    if ($graphtype == 'spc')
    {
        if ($_SESSION['excel']['spc_chart_type'] == 'runchart' || $_SESSION['excel']['spc_chart_type'] == 'movingrange')
        {
            $html .= "
                 var range_runchart = Sheet.Range('C2:C".($numdata+1)."');
                 var s1 = ch.SeriesCollection.Add(range_runchart);
                 //s1.Name = 'Mean'; // Excel 2007 only
                 //s1.ChartType = 4; // Excel 2007 only
                 awb.ActiveChart.SeriesCollection(1).Name = 'Data'; // Excel 2003 and 2007
                 awb.ActiveChart.SeriesCollection(2).Name = 'Mean'; // Excel 2003 and 2007
                 awb.ActiveChart.SeriesCollection(2).ChartType = 4; // Excel 2003 and 2007
                 ";
        }

        if ($_SESSION['excel']['spc_chart_type'] == 'movingrange')
        {
            $html .= "
                 var range_movingrange = Sheet.Range('D2:D".($numdata+1)."');
                 var s2 = ch.SeriesCollection.Add(range_movingrange);
                 awb.ActiveChart.SeriesCollection(1).Name = 'Moving range';
                 awb.ActiveChart.SeriesCollection(3).Name = 'RUCL';
                 awb.ActiveChart.SeriesCollection(3).ChartType = 4;";
        }

        if ($_SESSION['excel']['spc_chart_type'] == 'cchart')
        {
            $html .= "
                 var range_average = Sheet.Range('C2:C".($numdata+1)."');
                 var range_ucl = Sheet.Range('D2:D".($numdata+1)."');
                 var range_lcl = Sheet.Range('E2:E".($numdata+1)."');
                 var range_uwl = Sheet.Range('F2:F".($numdata+1)."');
                 var range_lwl = Sheet.Range('G2:G".($numdata+1)."');

                 var s1 = ch.SeriesCollection.Add(range_average);
                 var s2 = ch.SeriesCollection.Add(range_ucl);
                 var s3 = ch.SeriesCollection.Add(range_lcl);
                 var s4 = ch.SeriesCollection.Add(range_uwl);
                 var s5 = ch.SeriesCollection.Add(range_lwl);

                 awb.ActiveChart.SeriesCollection(1).Name = 'Data';
                 awb.ActiveChart.SeriesCollection(2).Name = 'Mean';
                 awb.ActiveChart.SeriesCollection(3).Name = 'UCL';
                 awb.ActiveChart.SeriesCollection(4).Name = 'LCL';
                 awb.ActiveChart.SeriesCollection(5).Name = 'UWL';
                 awb.ActiveChart.SeriesCollection(6).Name = 'LWL';

                 awb.ActiveChart.SeriesCollection(2).ChartType = 4;
                 awb.ActiveChart.SeriesCollection(3).ChartType = 4;
                 awb.ActiveChart.SeriesCollection(4).ChartType = 4;
                 awb.ActiveChart.SeriesCollection(5).ChartType = 4;
                 awb.ActiveChart.SeriesCollection(6).ChartType = 4;";
        }

        if ($_SESSION['excel']['spc_chart_type'] == 'ichart')
        {
            $html .= "
                 var range_average = Sheet.Range('C2:C".($numdata+1)."');
                 var range_ucl = Sheet.Range('D2:D".($numdata+1)."');
                 var range_lcl = Sheet.Range('E2:E".($numdata+1)."');

                 var s1 = ch.SeriesCollection.Add(range_average);
                 var s2 = ch.SeriesCollection.Add(range_ucl);
                 var s3 = ch.SeriesCollection.Add(range_lcl);

                 awb.ActiveChart.SeriesCollection(1).Name = 'Data';
                 awb.ActiveChart.SeriesCollection(2).Name = 'Mean';
                 awb.ActiveChart.SeriesCollection(3).Name = 'UCL';
                 awb.ActiveChart.SeriesCollection(4).Name = 'LCL';

                 awb.ActiveChart.SeriesCollection(2).ChartType = 4;
                 awb.ActiveChart.SeriesCollection(3).ChartType = 4;
                 awb.ActiveChart.SeriesCollection(4).ChartType = 4;";
        }

    }

    if ($graphtype == 'pareto')
    {
        $html .= "
            var range_pareto = Sheet.Range('C2:C".($numdata+1)."');
            var s1 = ch.SeriesCollection.Add(range_pareto);
            awb.ActiveChart.SeriesCollection(2).Name = 'Cumulative data';
            awb.ActiveChart.SeriesCollection(2).ChartType = 4;
            ";
    }

    if (\UnicodeString::strpos($graphtype, 'pie') === false)
    {
        $html .= "
            ch.Axes(1).TickLabelPosition = -4134; //xlLow";
    }

    $html .= "
        Excel.Visible = true;
        Excel = '';
    } // end of ExportGraphToExcelJS()
    ";
    echo $html;
}

function ExcelConvertToLetter($pColumnIndex = 0)
{
    if ($pColumnIndex < 26)
    {
        return chr(65 + $pColumnIndex);
    }

    return ExcelConvertToLetter((int)($pColumnIndex / 26) -1).chr(65 + $pColumnIndex%26);
}

/*
 * This function is used by listing reports to store the report HTML in the session to export to PDF.
 *
 * Note: Careful deleting it. We need to refactor the way we export listing reports to PDF before doing it.
 */
function output_handler_listingreport($html)
{
    echo '</table>';
    $_SESSION['listingreportstream'] = ob_get_contents();
    $_SESSION['listingreportstream'] = $_SESSION['listing_title'] . '<br/><br/>' . $_SESSION['listingreportstream'];
    return $html;
}
