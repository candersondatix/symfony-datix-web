var numContactQueries = 0;
var contactChoices = {};

function MergeWordDocument(recordid, module)
{
    if(jQuery('#select_template').val().empty())
    {
        alert(txt['select_word_template_err']);
    }
    else
    {
        //If the user has changed anything, need to alert them in case their changes are lost.
        if(GlobalChangeTracker)
        {
            AlertWindow = AddNewFloatingDiv('unsaved_changes');

            AlertWindow.setContents('You have made changes without saving, do you want to save the changes you have made before continuing?');
            AlertWindow.setTitle('Unsaved changes');

            AlertWindow.maxWidth = 300; //popups will be restricted to this width unless the buttons are wider than this.

            buttons = new Array();
            buttons.push({'value': 'Save', 'onclick': 'GetFloatingDiv(\'unsaved_changes\').CloseFloatingControl();DoJavascriptMergeContactCheck('+recordid+',\''+module+'\', true)'});
            buttons.push({'value': 'Don\'t Save', 'onclick': 'GetFloatingDiv(\'unsaved_changes\').CloseFloatingControl();DoJavascriptMergeContactCheck('+recordid+',\''+module+'\', false)'});
            buttons.push({'value': 'Cancel', 'onclick': 'GetFloatingDiv(\'unsaved_changes\').CloseFloatingControl()'});
            AlertWindow.setButtons(buttons);

            AlertWindow.display();
        }
        else
        {
            DoJavascriptMergeContactCheck(recordid, module, false);
        }
    }
}

function DoJavascriptMergeContactCheck(recordid, module, saveDataFirst)
{
    var doctemplate = jQuery('#select_template').val();

    jQuery.ajax(
        {
            url: scripturl + '?action=httprequest&type=getcontactchoicearray&responsetype=json',
            type: "POST",
            data: {'template_id': doctemplate,'module': module,'recordid': recordid},
            context: this,
            success: function(oReq)
            {
                if(oReq.evalJSON())
                {
                    try
                    {
                        var contactData = oReq.evalJSON();

                        messageArray = {};
                        jQuery.each(contactData,
                            function(role, contactList)
                            {
                                messageArray[role] = '';
                                jQuery.each(contactList,
                                    function(contactId, contactName)
                                    {
                                        messageArray[role] += '<div><input type="radio" value="'+contactId+'" name="contact_merge_radio_'+role+'" id="contact_merge_radio_'+role+contactId+'"> '+contactName+'</div>';
                                    }
                                );
                            }
                        );
                    }
                    catch(e){alert('Error when processing data for linked contacts :: ' + e.description);}

                    try
                    {
                        jQuery.each(messageArray,
                            function(role, message)
                            {
                                numContactQueries++;

                                AlertWindow = AddNewFloatingDiv('contact_merge_'+role);

                                AlertWindow.setContents(message);
                                AlertWindow.setTitle('Choose a contact');

                                AlertWindow.maxWidth = 300; //popups will be restricted to this width unless the buttons are wider than this.

                                buttons = new Array();
                                buttons.push({'value': 'Confirm', 'onclick': 'sendContactChoices(\''+role+'\',\''+module+'\','+recordid+','+doctemplate+', '+saveDataFirst+')'});
                                buttons.push({'value': 'Cancel', 'onclick': 'GetFloatingDiv(\'contact_merge_'+role+'\').CloseFloatingControl()'});
                                AlertWindow.setButtons(buttons);

                                AlertWindow.OnCloseAction = function(){numContactQueries--;};

                                AlertWindow.display();
                            }
                        );
                    }
                    catch(e){alert('Error when creating contact select popup :: ' + e.description);}

                    if(numContactQueries == 0)
                    {
                        SendDocForMerge(module, recordid, doctemplate, '', saveDataFirst)
                    }
                }
                else
                {
                    SendDocForMerge(module, recordid, doctemplate, '', saveDataFirst)
                }
            },
            failure: function()
            {
                alert('Error creating datasource.')
            }
        }
    );  
}

/**
 * Checks whether there are any choice windows still open and if not, posts all data to allow the merging to take place.
 * @param role
 * @param module
 * @param recordid
 * @param doctemplate
 */
function sendContactChoices(role, module, recordid, doctemplate, saveDataFirst)
{
    contactChoices[role]=jQuery('input[name=contact_merge_radio_'+role+']:checked').first().val();

    GetFloatingDiv('contact_merge_'+role).CloseFloatingControl();

    if(numContactQueries == 0)
    {
        validURL = true;
        choiceURL = '';
        jQuery.each(contactChoices,
            function(role, id)
            {
                if(id)
                {
                    choiceURL += '&selected_contact['+role+']='+id;
                }
                else
                {
                    validURL = false;
                }
            });
        if(validURL)
        {
            SendDocForMerge(module, recordid, doctemplate, choiceURL, saveDataFirst)
        }
    }
}

/**
 * Kicks off the merge process, either with or without a call to save the record first.
 * @param module
 * @param recordid
 * @param doctemplate
 * @param choiceURL
 * @param saveDataFirst
 * @constructor
 */
function SendDocForMerge(module, recordid, doctemplate, choiceURL, saveDataFirst)
{
    if(saveDataFirst)
    {
        document.forms[0].action += '&merge_document_after_save='+doctemplate+choiceURL;
        document.forms[0].submit();
    }
    else
    {
        SendTo('app.php?action=mergedocument&module='+module+'&recordid='+recordid+'&select_template='+doctemplate+choiceURL);
    }
}