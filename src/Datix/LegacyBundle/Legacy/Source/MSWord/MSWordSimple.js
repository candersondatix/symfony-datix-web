function DoJavascriptMergeContactCheck(recordid, module, serverfolder)
{
    if($('select_template').value.empty())
    {
        alert(txt["select_word_template_err"]);
    }
    else
    {
        var WordApp2;
        try
        {
            WordApp2 = new ActiveXObject('Word.Application');
        }
        catch(err)
        {
            var errorMsg = 'Running ActiveX controls in your browser is disabled. Please change your browser settings as explained below: <br/><br/> 1. From the menu select Tools -> Internet Options -> Security.  <br/><br/> 2. Select \'Local Intranet\' and click \'Custom Level...\'.  <br/><br/> 3. Enable the option \'Initialize and script ActiveX controls not marked as safe\' and click OK.  <br/><br/> 4. Try again to merge documents.';
            alert(errorMsg);
            if(WordApp2)
            {
                WordApp2.Quit(false);
                WordApp2 = null;
            }
            return;
        }

        WordApp2.Visible = false;
        var doctemplate = $('select_template').value;
        $('merge_contact').value = '';
        var doctemplatefullpath;
        var exit = false;

        var oAjaxCall = new Ajax.Request
        (
            scripturl + '?action=httprequest&type=copytemplatedoctoclient',
            {
                method: 'post',
                postBody: 'doctemplate=' + doctemplate,
                asynchronous: false,
                onSuccess: function(r)
                {
                    if(r.responseText.contains('error'))
                    {
                        alert('Cannot copy document to temporary folder. Please contact your IT department.');
                        WordApp2.Quit(false);
                        WordApp2 = null;
                        exit = true;
                    }
                    else
                    {
                        doctemplatefile = r.responseText.strip();
                        doctemplatefullpath = serverfolder + '\\' + doctemplatefile;
                    }
                },

                onFailure: function()
                {
                    alert('Error creating datasource.');
                    WordApp2.Quit(false);
                    WordApp2 = null;
                    exit = true;
                }
            }
        );

        if(exit)
            return;

        MailMergeDoc2 = WordApp2.Documents.Open(doctemplatefullpath);

        var Fs = MailMergeDoc2.MailMerge.Fields;
        var aMFArray;
        var postMMFs = '';

        for (var i = 1; i <= Fs.Count; i++)
        {
            var MMFName = Fs(i).Code.Text;
            var brokenstring = MMFName.split(" ");
            var brokenstring2 = MMFName.split("  ");
            var mergecodex;
            var mergecode;

            if(brokenstring[2].indexOf('\*') != -1)
            {
               mergecodex = brokenstring[2];
               var mcxlastindex =  mergecodex.indexOf('\*')-1;
               mergecode = mergecodex.substring(0, mcxlastindex);
            }
            else
            {
               mergecode = brokenstring2[1];
            }

            if(postMMFs == '')
                postMMFs = mergecode;
            else
                postMMFs = postMMFs + ", " + mergecode;
        }

        MailMergeDoc2.Close(false);
        WordApp2.Visible = false;
        WordApp2.Quit(false);
        WordApp2 = null;

        if(doctemplatefile !== undefined)
        {
            var oAjaxCall = new Ajax.Request
            (
                scripturl + '?action=httprequest&type=deletewordtmpfiles&responsetype=json',
                {
                    method: 'post',
                    postBody: 'doctemplatefile=' + doctemplatefile,
                    asynchronous: false,
                    onSuccess: function(r)
                    {
                        if(r.responseText.contains('error'))
                        {
                            alert('Cannot delete word merging temporary documents. Please contact your IT department.');
                        }
                        else
                        {
                            //alert('File has been deleted');
                        }
                    },

                    onFailure: function()
                    {
                        alert('Cannot delete word merging temporary documents. Please contact your IT department.');
                    }
                }
            );
        }

        var oAjaxCall = new Ajax.Request
        (
            scripturl + '?action=httprequest&type=checkmergecontactprompt',
            {
                method: 'post',
                postBody: 'postmmfs=' + postMMFs + '&module=' + module + '&recordid=' + recordid,
                asynchronous: false,
                onSuccess: function(oReq)
                {
                    if(oReq.responseText.strip() != '')
                    {
                        try
                        {
                            var data = eval(oReq.responseText);
                            var popUpMergeContactSourceCode = data[0];
                            var popUpMergeCode = data[1];
                        }
                        catch(e){alert('Error when processing data for linked contacts :: ' + e.description);}

                        if(popUpMergeContactSourceCode.strip() != '')
                        {
                            try
                            {
                                $('merge_code_contact').value = popUpMergeCode;
                                var NewControl = new CodeSelectionCtrl();
                                ControlIndex = GlobalCodeSelectControls.length;
                                NewControl.setIndex(ControlIndex);
                                GlobalCodeSelectControls.push(NewControl);
                                eval("overrideArray['merge_contact'] = " + popUpMergeContactSourceCode.strip());
                                GlobalCodeSelectControls[ControlIndex].create({module: '', field: 'merge_contact', fieldname: 'merge_contact', contactsuffix: '', multilistbox: false, mandatory: false, fieldmode: '', freetext: true, overrideArray: true, overrideTitle: 'Choose a contact' , nochoose: false , childcheck: false , codecheck: false});
                            }
                            catch(e){alert('Error when creating contact select popup :: ' + e.description);}
                        }
                        else
                        {
                            DoJavascriptMerge(recordid, module, serverfolder);
                        }
                    }
                    else
                    {
                        DoJavascriptMerge(recordid, module, serverfolder);
                    }
                },
                onFailure: function()
                {
                    alert('Error creating datasource.')
                }
            }
        );
    }
}
     
function DoJavascriptMerge(recordid, module, serverfolder)
{
    var WordApp = new ActiveXObject('Word.Application');
    WordApp.Visible = false;
    
    var doctemplate = $('select_template').value;
    var merge_contact = $('merge_contact').value;
    var merge_code_contact = $('merge_code_contact').value;
    var doctemplatefullpath;
    
    var oAjaxCall = new Ajax.Request
    (
        scripturl + '?action=httprequest&type=copytemplatedoctoclient',
        {
            method: 'post',
            postBody: 'doctemplate=' + doctemplate, 
            asynchronous: false,
            onSuccess: function(r)
            {
                if(r.responseText.contains('error'))
                {
                    alert('Cannot copy document to temporary folder. Please contact your IT department.')
                }
                else 
                {
                    doctemplatefile = r.responseText.strip();
                    doctemplatefullpath = serverfolder + '\\' + doctemplatefile;
                    
                }
            },

            onFailure: function()
            {
                alert('Cannot copy document to temporary folder. Please contact your IT department.')
            }
        }
    ); 
    
    MailMergeDoc = openWordDoc(doctemplatefullpath, WordApp);
    
    if (MailMergeDoc == false)
    {
    	alert('Unable to open merge document. Please contact your IT department.');
    	return false;
    }
                                   
    var Fs = MailMergeDoc.MailMerge.Fields;
    var aMFArray;
    var postMMFs = '';

    for (var i = 1; i <= Fs.Count; i++)
    {
        var MMFName = Fs(i).Code.Text;
        var brokenstring = MMFName.split(" ");
        var brokenstring2 = MMFName.split("  ");
        var mergecodex;
        var mergecode;
        
        if(brokenstring[2].indexOf('\*') != -1)
        {
           mergecodex = brokenstring[2];
           var mcxlastindex =  mergecodex.indexOf('\*')-1;
           mergecode = mergecodex.substring(0, mcxlastindex); 
        }
        else
        {
           mergecode = brokenstring2[1];
        }
        
        if(postMMFs == '')
            postMMFs = mergecode;
        else
            postMMFs = postMMFs + ", " + mergecode;
    }   
    
    var datasourcefile;
    var oAjaxCall = new Ajax.Request
    (
        scripturl + '?action=httprequest&type=returnmergesql',
        {
            method: 'post',
            postBody: 'postmmfs=' + postMMFs + '&module=' + module + '&recordid=' + recordid + '&merge_contact=' + merge_contact + '&merge_code_contact=' + merge_code_contact,
            asynchronous: false,
            onSuccess: function(r)
            {
                if(r.responseText)
                {
                    datasourcefile = r.responseText.strip();
                    datasourcefilefullpath = serverfolder + '\\' + datasourcefile;
                }
            },
            
            onFailure: function()
            {
                alert('Error creating datasource.');
            }  
        }
    );
    
    if(datasourcefilefullpath)
    {
        MailMergeDoc.MailMerge.MainDocumentType = 3; // 3 = wdCatalog (no section break)
        MailMergeDoc.MailMerge.OpenDataSource(datasourcefilefullpath);
        MailMergeDoc.MailMerge.Destination = 0; // 0 = wdSendToNewDocument

        // Execute the mail merge.
        try
        {
            MailMergeDoc.MailMerge.Execute();    
        }
        catch (e)
        {
            MailMergeDoc.MailMerge.MainDocumentType = 0; // 0 = wdFormLetters
            MailMergeDoc.MailMerge.Execute();    
        }
        
        MailMergeDoc.Close(false);
        WordApp.Visible = true;
        
        // word documents are now opened from the server templates directory (PATH_TEMPLATES) so 
        // save merged document to client machine with Word and then 
        // delete the temp files in PATH_TEMPLATES from the server
        WordApp.Application.Documents(1).SaveAs(globals['WORD_MERGE_TEMP_FILE_LOCATION']+'\\WM'+Math.random().toString(36).substring(7)+'.doc', 0);
        
        var oAjaxCall = new Ajax.Request
        (
            scripturl + '?action=httprequest&type=deletewordtmpfiles&responsetype=json',
            {
                method: 'post',
                postBody: 'doctemplatefile=' + doctemplatefile + '&datasrcfile=' + datasourcefile,
                asynchronous: false,
                onSuccess: function(r)
                {
                    if(r.responseText.contains('error'))
                    {
                        alert('Cannot delete word merging temporary documents. Please contact your IT department.');
                    }
                    else 
                    {
                        //alert('File has been deleted');
                    }
                },

                onFailure: function()
                {
                    alert('Cannot delete word merging temporary documents. Please contact your IT department.');
                }
            }
        );       
    }          
}

/**
 * Recursive function which continually attemps to open a word document until successful.
 * 
 * Prevents timing issues caused by attempting to open documents before they're fully saved to disk.
 * 
 * @param {string} 		  path 	  The file path.
 * @param {ActiveXObject} WordApp   The Word application object.
 * @param {int}    		  iteration The recursive iteration - used to prevent infinite loops.
 * 
 * @return DocumentObject|false doc The document object.
 */
function openWordDoc(path, WordApp, iteration)
{
	var doc;
	
	if (iteration == undefined)
	{
		iteration = 0;
	}
	
	if (iteration > 1000)
	{
		return false;
	}
	
	try
    {
		doc = WordApp.Documents.Open(path);
    }
    catch (e)
    {
    	iteration++;
    	doc = openWordDoc(path, WordApp, iteration);
    }
    return doc;
}
