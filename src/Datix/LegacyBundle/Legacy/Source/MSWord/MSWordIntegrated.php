<?php
require_once('src\security\Escaper.php');

use src\security\Escaper;

/**
* DATIXWeb Word Merge section placeholder
* 
* @param string $module Module code
* @param array $data All form data from the given module (e.g. $inc)
* @param string Form type (e.g. readonly, edit, print)
*/
function MSWordMain($module, $data, $FormType)
{
    global $FormType;	
    
    $ServerFolder = GetParm("PATH_TEMPLATES");
    $ServerFolder = addslashes($ServerFolder);
    
?>
<li name="msword_row" id="msword_row">
<script language="javascript" type="text/javascript" src="Source/MSWord/MSWordSimple.js"></script>
<script language="javascript" type="text/javascript">
function MergeContactSelectionDone()
{
    <?php 
    echo 'DoJavascriptMerge('.Escaper::escapeForJS($data['recordid']).', \''.$module.'\', \''.$ServerFolder.'\');';
    ?>
    
}
</script>

<input type="hidden" name="merge_contact" id="merge_contact" value=""/>
<input type='hidden' name='merge_code_contact' id='merge_code_contact' value=''/>

			
            <?php
    
        ///////////////////////////////////////////////////////////////////////////////
        // document templates are not accessible to anyone by default, the administrator 
        // has to give access to users and groups by the record level security panel
        // called 'permissions' in Document template administration.
        ///////////////////////////////////////////////////////////////////////////////
        
        $sql = "SELECT tm.recordid, tm.tem_notes, td.path
            FROM templates_main tm
            LEFT JOIN tem_documents td on tm.tem_id = td.recordid
            WHERE (tm.tem_module = '$module' OR tm.tem_module = '' OR tm.tem_module is null)";
        
        $tableSearch = 'templates_main';
        $DocTemplatesWhereClause = MakeSecurityWhereClause('', 'TEM', $_SESSION["initials"], $tableSearch, "", "", true, $module);
        
        $tableReplace = 'tm'; 
        $DocTemplatesWhereClause = str_replace($tableSearch . ".", $tableReplace . ".", $DocTemplatesWhereClause);
        
        if($DocTemplatesWhereClause != "")
            $sql .= " AND $DocTemplatesWhereClause";
        
        $sql .= " ORDER BY tm.tem_notes";
        
        $result = db_query($sql);
        $doctem_array = array();
        while ($row = db_fetch_array($result))
        {
            $doctem_array[$row['path']] = $row['tem_notes'];
        }
            
            
        $Table = new FormTable($FormType, $module);
        $FieldObj = new FormField($FormType);
        
        $field = Forms_SelectFieldFactory::createSelectField('select_template', '', '', $FormType);
        $field->setCustomCodes($doctem_array);
        $field->setSuppressCodeDisplay();
        $Table->MakeRow("<label for=\"select_template_title\"><b>Word template</b></label><br />Choose a document template", $field);

    $CustomField = '<input type="button" value="'._tk('btn_merge_in_msword').'"
            onclick="DoJavascriptMergeContactCheck('.Escaper::escapeForJS($data['recordid']).', \''.$module.'\', \''.$ServerFolder.'\');" />';
        $Table->MakeTitleRow($CustomField, "windowbg2");
        
        $Table->MakeTable();
        echo $Table->GetFormTable();          
                
                ?>
	</li>
    <?php
    
}       


/**
* AJAX codebehind function to calculate whether the user needs to be prompted to choose a contact for word merging
* 
* using:
* int $_POST[recordid] The id of the main record
* string $_POST[module] Module code
* string $_POST[postmmf] A string of merge fields, separated by comma (,)
*/      
function CheckMergeContactPrompt()
{
    global $ModuleDefs;
    
    $recordid = Sanitize::SanitizeInt($_POST['recordid']);
    $module = Sanitize::getModule($_POST['module']);
    $LinkedContactFunction = ($ModuleDefs[$module]['GET_LINKED_CONTACTS_FUNCTION'] ? $ModuleDefs[$module]['GET_LINKED_CONTACTS_FUNCTION'] : 'GetLinkedContacts'); 
    $con = $LinkedContactFunction(array('recordid' => $recordid, 'module' => $module, 'formlevel' => 2));  
    $linked_con_num = array();
    
    if(is_array($con))
    {
        $linked_con_num = getFetchedLinkedCons($con);
    }
    
    $con_sum = array_sum($linked_con_num);
     
    $selectfields = array();
    $selecttables = array();
    $selecttables_linkcon = array();
    // main table
    $selecttables[0] = $ModuleDefs[$module]['TABLE'];
    $postmmfs = array();
    $_SESSION['merge_contact_role'] = '';
    
    if($_POST['postmmfs'] != "")
    {
        $postmmfs = explode(', ', Sanitize::SanitizeString($_POST['postmmfs']));
        
        foreach($postmmfs as $p)
        {
            $ap = explode('__', $p);
            if($ap[1] != '')
            {
                // contact __ROLE is defined
                $apn = explode('_', $ap[1]);
                if($apn[1] != '')
                {
                    // contact numbering suffix is defined like __ROLE_2
                }
            }
            else
            {
                $apn = '';
            }
            
            $trimmedap0 = \UnicodeString::trim($ap[0]);
            $merge_code[$p] = array($trimmedap0, $apn[0], $apn[1], $p);
        
        
            $sql = "SELECT mer_code, mer_col, mer_table, mer_source, mer_format FROM mergecodes WHERE mer_code like '$trimmedap0'";
            $result = db_query($sql);
            
            if ($row = db_fetch_array($result))
            {
                if(($row['mer_table'] == 'link_contacts' || $row['mer_table'] == 'contacts_main' || $row['mer_table'] == 'link_compl') && $module != 'CON')
                {
                    CheckContactToPopUp($merge_code, $p, $con, $con_sum, $linked_con_num, $row, array('return' => 'JSON'));
                }   
            }    
        }                 
    }
} 
 
/**
* Creates a datasource file
* 
* AJAX codebehind function to create a datasource file to be used as a basis of word merging and
* to fill that datasource file with merge codes and their values.
* 
* using:
* int $_POST[recordid] The id of the main record
* string $_POST[module] Module code
* string $_POST[postmmf] A string of merge fields, separated by comma (,)
* int $_POST[merge_contact] The id of the contact that the user had choosen to use for data merging
* 
* @return Echos back to Javascript AJAX call the file name and extension of the datasource file created. This
* is a unique file name with .dat extension. 
*/
function ReturnMergeSQL()
{
    global $ServerFolder, $ModuleDefs;
    
    $recordid = Sanitize::SanitizeInt($_POST['recordid']);
    $module = Sanitize::SanitizeString($_POST['module']);
    $LinkedContactFunction = ($ModuleDefs[$module]['GET_LINKED_CONTACTS_FUNCTION'] ? $ModuleDefs[$module]['GET_LINKED_CONTACTS_FUNCTION'] : 'GetLinkedContacts'); 
    $con = $LinkedContactFunction(array('recordid' => $recordid, 'module' => $module, 'formlevel' => 2));
    $linked_con_num = array();
    
    if(is_array($con))
    {
        $linked_con_num = getFetchedLinkedCons($con);
    }
    
    $con_sum = array_sum($linked_con_num);
        
    $selectfields = array();
    $selecttables = array();
    $selectwhereclauses = array();  
    $selecttables_linkcon = array();
    $selecttables[0] = $ModuleDefs[$module]['TABLE']; // main table
    $postmmfs = array();
    
    if($_POST['postmmfs'] != "")
    {
        $postmmfs = explode(', ', Sanitize::SanitizeString($_POST['postmmfs'])); 
        
        foreach($postmmfs as $p)
        {
            $ap = explode('__', $p);
            if($ap[1] != '')
            {
                // contact __ROLE is defined
                $apn = explode('_', $ap[1]);
                if($apn[1] != '')
                {
                    // contact numbering suffix is defined like __ROLE_2
                }
            }
            else
            {
                $apn = '';
            }
            
            $trimmedap0 = \UnicodeString::trim($ap[0]);
            $merge_code[strtoupper($trimmedap0)] = array($trimmedap0, $apn[0], $apn[1], $p);
            
            if(\UnicodeString::strpos($trimmedap0, 'EXTRA') !== false)
            {
                // extra fields merge code
                $udffields[$p] = $p;   
            }
            else
            {
            
                $sql = "SELECT mer_code, mer_col, mer_table, mer_source, mer_format FROM mergecodes WHERE mer_code like '$trimmedap0'";
                $result = db_query($sql);

                if($row = db_fetch_array($result))
                {
                    if($row['mer_table'] != 'link_compl' && $row['mer_table'] != 'link_contacts' && ($row['mer_table'] != 'contacts_main' || $module == 'CON'))
                    //if($row['mer_table'] != 'link_contacts' && ($row['mer_table'] != 'contacts_main' || $module == 'CON'))
                    {
                        if ($row['mer_source'][0] != '!')
                        {
                            $selectfields[$row['mer_code']] = $row['mer_table'] . '.' . $row['mer_col'];
                            if(!in_array($row['mer_table'], $selecttables))
                            {
                                if ($row['mer_table'] = 'rev_main')
                                {
                                    $selecttables[] = $row['mer_table'] . " ON " . $row['mer_table'] . '.link_id = ' . $ModuleDefs[$module]['TABLE'] . '.recordid' .
                                                        " AND link_module = '$module'";
                                    
                                }
                                else
                                {
                                    $selecttables[] = $row['mer_table'] . " ON " . $row['mer_table'] . "." . $ModuleDefs[$module]['FK'] . ' = ' . 
                                                            $ModuleDefs[$module]['TABLE'] . '.recordid';
                                }
                            }
                        }
                        if($row['mer_format'] != '')
                        {
                            $merge_format[$row['mer_code']] = $row['mer_format'];
                        }
                        
                        if($row['mer_source'] != '')
                        {
                            $merge_source[$row['mer_code']] = $row['mer_source'];
                        }
                        
                        $datasourcefields[$row['mer_code']] = $row['mer_code'];
                    }
                    elseif($row['mer_table'] == 'link_compl')
                    {
                        if($row['mer_source'] != '')
                        {
                            if($row['mer_source'][0] != '!')
                            {
                                $merge_source_linkcon[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_source'];
                            }
                            else
                            {
                                // external merge function - not implemented yet
                                // e.g.: !MergeAgeAtIncident( 'INC_DINCIDENT','incidents_main','inc_dincident' )
                            }
                        }       
                        
                        $selectfields_linkcon[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_table'] . '.' . $row['mer_col'];
                        $datasourcefields_linkcon[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_col'];
                        
                        if($row['mer_table'] == 'link_compl')
                        {
                            if(!in_array('compl_main', $selecttables_linkcon))
                            {
                                $selecttables_linkcon[] = 'compl_main';    
                            }
                            if(!in_array('link_compl', $selecttables_linkcon))
                            {
                                $selecttables_linkcon[] = 'link_compl';    
                            }    
                        }
                        
                        if($row['mer_format'] != '')
                        {
                            $merge_format[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_format'];
                        }
                        
                        
                    }    
                    elseif(($row['mer_table'] == 'link_contacts' || $row['mer_table'] == 'contacts_main') && $module != 'CON')
                    {
                        
                        if($row['mer_source'] != '')
                        {
                            if($row['mer_source'][0] != '!')
                            {
                                $merge_source_linkcon[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_source'];
                            }
                            else
                            {
                                // external merge function - not implemented yet
                                // e.g.: !MergeAgeAtIncident( 'INC_DINCIDENT','incidents_main','inc_dincident' )
                            }
                        }       
                        
                        $selectfields_linkcon[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_table'] . '.' . $row['mer_col'];
                        $datasourcefields_linkcon[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_col'];
                        
                        if($row['mer_table'] == 'contacts_main')
                        {
                            if(!in_array('contacts_main', $selecttables_linkcon))
                            {
                                $selecttables_linkcon[] = 'contacts_main';    
                            }
                            if(!in_array('link_contacts', $selecttables_linkcon))
                            {
                                $selecttables_linkcon[] = 'link_contacts';    
                            }    
                        }
                        elseif($row['mer_table'] == 'link_contacts')
                        {
                            if(!in_array('link_contacts', $selecttables_linkcon))
                            {
                                $selecttables_linkcon[] = 'link_contacts';    
                            }
                        }
                        
                        if($row['mer_format'] != '')
                        {
                            $merge_format[$merge_code[strtoupper($row['mer_code'])][3]] = $row['mer_format'];
                        }
                    }
                }
                else // no results for sql select -> merge code is invalid
                {
                    // add blank entry into the datasource file for this merge code
                    $invalid_mm_fields[] = '"' . $p . '"';
                    $invalid_mm_fields_values[] = '""';
                }
            } // end of if check for extra fields else branch
        } // end of foreach
    } // end of if
    
    ///////////////////////////////////////////////////////////////////////////////////               
    
    if(is_array($invalid_mm_fields))
    {
        $doccol = implode(',', $invalid_mm_fields);
        $docval = implode(',', $invalid_mm_fields_values);
    }           
    
    if(!empty($datasourcefields))
    {
        getMainTableColVal($recordid, $selectfields, $selecttables, $selectwhereclauses, $datasourcefields, $merge_source, $merge_format, $doccol, $docval);
    }
    
    if(isset($selectfields_linkcon) && !empty($selectfields_linkcon))
    {
        getLinkedTableColVal($recordid, $module, $selectfields_linkcon, $selecttables, $selecttables_linkcon, $datasourcefields_linkcon, $merge_source_linkcon, $merge_format, $doccol, $docval);
    }   
    
    if(isset($udffields))
    {
        getExtraFieldsVal($udffields, $doccol, $docval);
    }
    
    $doc =  $doccol .",\n". $docval .",\r\n"; 
    
    $doc_unique_name = unique_filename('.dat');
    
    $ServerFolder = GetParm("PATH_TEMPLATES");
  //  $ServerFolder = addslashes($ServerFolder);
    
    $doc_dir = Sanitize::SanitizeFilePath($ServerFolder . "\\" . $doc_unique_name);
    $OutputFile = @fopen($doc_dir, "wb");
    
    if (!$OutputFile)
        fatal_error("Cannot create datasource file.  Please contact your IT department.");
    if (!fwrite($OutputFile, $doc))
        fatal_error("Cannot write to datasource file.  Please contact your IT department.");
    fclose($OutputFile);  
    
    echo $doc_unique_name;
}

/**
* Collects data for merge codes related to the main record table.
* 
* This funtion queries the database to collect data and organises the merge codes and the returned values 
* to be later processed and a datasource file to be created in function RunMergeSQL().
* 
* @param int $recordid The id of the main record
* @param array $selectfields The database fields to be selected in the SQL query. 
* @param array $selecttables The database tables to be included in the SQL query.
* @param array $datasourcefields An array structure containing the merge code and the database field it is linked to in table MERGECODES.
* @param array $merge_source An array structure containing the merge code and the source definition it is defined for in table MERGECODES.
* @param array $merge_format An array structure containing the merge code and the (date) format definition of the value of the data (defined in table MERGECODES). 
* @param string &$doccol A string containing the columns part of the datasource file.
* @param string &$docval A string containing the value part of the datasource file. 
* 
* @return (passed by reference) string &$doccol See above.
* @return (passed by reference) string &$docval See above.
*/                                                                                                                  
function getMainTableColVal($recordid, $selectfields = array(), $selecttables = array(), $selectwhere = array(), $datasourcefields, $merge_source, $merge_format, &$doccol, &$docval)
{
    
    if(!empty($selectfields) && !empty($selecttables))
    {                                     
        $sql = "SELECT ";
        $sql .= implode_select_as_key(' ,', $selectfields);
        $sql .= " FROM ";
        $sql .= implode(' LEFT JOIN ', array_unique($selecttables));
        $sql .= " WHERE ".$selecttables[0].".recordid = $recordid";
        if (!empty($selectwhere))
        {
            $sql .= " AND " . implode(" AND ", $selectwhere);
        }
        
        $result = db_query($sql);    
        $row = db_fetch_array($result);
    }
    
    if(is_array($datasourcefields))
    {
        foreach($datasourcefields as $key => $dsfield)
        {
            if (is_numeric($row[$key]) && $row[$key]{0} == '.')
            {
                // sqlsrv driver omits leading zero for decimals
                $row[$key] = '0'.$row[$key];   
            }
            
            $datasourcefields[$key] = '"'.$dsfield.'"';
            
            $row[$key] = str_replace('"', '""', $row[$key]);
            
            if(is_array($merge_format))
            {
                if(array_key_exists($key, $merge_format))
                {
                    switch($merge_format[$key])
                    {
                        case 'DL':
                        case 'D':
                            if($row[$key] == '')
                            {
                                $docval_main[$key] = '""'; 
                            }                                
                            else
                            {
                                $date = date_parse($row[$key]);
                                $format = GetParm("FMT_DATE_WEB") == "US" ? 'F j Y' : 'j F Y';
                                $docval_main[$key] = '"'.date($format, mktime(0, 0, 0, $date['month'], $date['day'], $date['year'])).'"';
                            }
                            break;
                        case 'DS':
                            $docval_main[$key] = '"'.FormatDateVal(($row[$key]), false).'"';
                            break;
                        default:
                            $docval_main[$key] = '"'.$row[$key].'"';
                            break;
                    }
                }
                else
                {
                    $docval_main[$key] = '"'.$row[$key].'"';
                }
            }
            else
            {
                $docval_main[$key] = '"'.$row[$key].'"';
            }
            
            ////////////////////////////////////////////////////////////
            // look up coded fields with description in other table
            if(is_array($merge_source))
            {
                if(isset($merge_source[$key]))
                { 
                    if($merge_source[$key][0] == '!')
                    {
                        switch($key)
                        {
                            case 'DATE_1':
                                $docval_main[$key] = '"'.date("d/m/Y").'"';
                                break;
                            case 'DATE_2':
                                $docval_main[$key] = '"'.date("l, j F Y").'"';
                                break;
                            case 'DATE_3':
                                $docval_main[$key] = '"'.date("j F Y").'"';
                                break;
                            case 'DATE_4':
                                $docval_main[$key] = '"'.date("j-M-Y").'"';
                                break;
                            case 'DATE_5':
                                $docval_main[$key] = '"'.date("j.m.Y").'"';
                                break;
                            case 'DATE_6':
                                $docval_main[$key] = '"'.date("j M Y").'"';
                                break;
                            case 'DATE_7':
                                $docval_main[$key] = '"'.date("j, F, Y").'"';
                                break;
                            case 'DATE_8':
                                $docval_main[$key] = '"'.date("F Y").'"';
                                break;
                            case 'DATE_9':
                                $docval_main[$key] = '"'.date("F-Y").'"';
                                break;
                            case 'DATE_10':
                                $docval_main[$key] = '"'.date("d/m/Y h:m").'"';
                                break;
                            case 'DATE_11':
                                $docval_main[$key] = '"'.date("d/m/Y h:m:s").'"';
                                break;
                        }
                    }
                    else
                    {
                        $mer_source_array = explode(';', $merge_source[$key]);
                        $description_f = $mer_source_array[0];
                        $codetable = $mer_source_array[1];
                        $code = $mer_source_array[2];
                        $codewhere = '';
                        if($mer_source_array[3] != '')
                            $codewhere = $mer_source_array[3];
                            
                        $sql = "SELECT $description_f as $key";
                        $sql .= " FROM $codetable";
                        $sql .= " WHERE $code = '$row[$key]'";
                        if($codewhere != '')
                            $sql .= " AND $codewhere";
                        $result2 = db_query($sql);    
                        $row2 = db_fetch_array($result2);
                        
                        if($row2[$key] != '')
                            $docval_main[$key] = '"'.str_replace('"', '""', $row2[$key]).'"';
                    }
                }
            } 
            ////////////////////////////////////////////////////////////
        }
        
        if($doccol != "")
            $doccol .= ',' . implode(',', $datasourcefields);
        else
            $doccol = implode(',', $datasourcefields);
        
    }
    
    if(is_array($docval_main))
    {
        if($docval != "")
            $docval .= ',' . implode(',', $docval_main);
        else
            $docval = implode(',', $docval_main);
    }
}

/**
* Collects data for merge codes related to linked tables.
* 
* This funtion queries the database to collect data and organises the merge codes and the returned values 
* to be later processed and a datasource file to be created in function RunMergeSQL().
* 
* @param int $recordid The id of the main record
* @param string $module Module code of the main record
* @param array $selectfields_linkcon The database fields to be selected in the SQL query. 
* @param array $selecttables The database tables to be included in the SQL query.
* @param array $selecttables_linkcon The database tables (linked tables) to be included in the SQL query.
* @param array $datasourcefields_linkcon An array structure containing the merge code and the database field it is linked to in table MERGECODES.
* @param array $merge_source An array structure containing the merge code and the source definition it is defined for in table MERGECODES.
* @param array $merge_format An array structure containing the merge code and the (date) format definition of the value of the data (defined in table MERGECODES). 
* @param string &$doccol A string containing the columns part of the datasource file.
* @param string &$docval A string containing the value part of the datasource file. 
* 
* @return (passed by reference) string &$doccol See above.
* @return (passed by reference) string &$docval See above.
*/ 
function getLinkedTableColVal($recordid, $module, $selectfields_linkcon, $selecttables, $selecttables_linkcon, $datasourcefields_linkcon, $merge_source, $merge_format, &$doccol, &$docval)
{
    global $ModuleDefs;
  
    if(is_array($selectfields_linkcon))
    {
        /*
        $sql = "SELECT ";
        
        if(is_array($selectfields_linkcon))
            $sql .= implode_select_as_key(' ,', $selectfields_linkcon);
        
        $sql .= ", link_contacts.link_role as LINK_ROLE_SWITCH";
        $sql .= ", contacts_main.recordid as CONTACT_RECORDID";
        
        $sql .= " FROM ";
        
        if(!in_array($selecttables[0], $selecttables_linkcon))
        {
            $selecttables_linkcon[] = $selecttables[0];    
        }                                     
        
        $sql .= implode(', ', $selecttables_linkcon);
        
        $sql .= " WHERE ".$selecttables[0].".recordid = $recordid ";
        if(in_array('link_contacts', $selecttables_linkcon))
        {
            $sql .= " AND ".$selecttables[0].".recordid = link_contacts.".$ModuleDefs[$module]['FK'];
        }
        if(in_array('link_compl', $selecttables_linkcon))
        {
            $sql .= " AND ".$selecttables[0].".recordid = link_compl.".$ModuleDefs[$module]['FK'];
        }
        if(in_array('contacts_main', $selecttables_linkcon))
        {
            $sql .= " AND link_contacts.con_id = contacts_main.recordid";            
            $sql .= " ORDER BY contacts_main.con_surname asc";
        }
        */
        
        $sql = "SELECT ";
        
        if(is_array($selectfields_linkcon))
            $sql .= implode_select_as_key(' ,', $selectfields_linkcon);
        
        $sql .= ", link_contacts.link_role as LINK_ROLE_SWITCH";
        $sql .= ", contacts_main.recordid as CONTACT_RECORDID";
        
        $sql .= " FROM ";
        
        if(!in_array($selecttables[0], $selecttables_linkcon))
        {
            $selecttables_linkcon[] = $selecttables[0];    
        }                                     
        
        $sql .= 'contacts_main';
        
        if(in_array('link_contacts', $selecttables_linkcon) || true)
        {
            $sql .= " LEFT JOIN link_contacts on contacts_main.recordid = link_contacts.con_id";
            $sql .= " LEFT JOIN ".$selecttables[0]." on ".$selecttables[0].".recordid = link_contacts.".$ModuleDefs[$module]['FK'];
        }
        if(in_array('link_compl', $selecttables_linkcon))
        {
            $sql .= " LEFT JOIN link_compl on compl_main.recordid = link_compl.com_id AND link_compl.con_id = link_contacts.con_id";
        }
        
        $sql .= " WHERE ".$selecttables[0].".recordid = $recordid ";
        
        if(in_array('contacts_main', $selecttables_linkcon))
        {
            $sql .= " ORDER BY contacts_main.con_surname asc";
        }                                           
        
        $result = db_query($sql);    
        
        $row_array = array();
        
        while($row = db_fetch_array($result))
        {
            $row_array[] = $row;
        }
               
        $con_sum = count($row_array);
        
        //$linked_con_num = $con_sum;
        $LinkedContactFunction = ($ModuleDefs[$module]['GET_LINKED_CONTACTS_FUNCTION'] ? $ModuleDefs[$module]['GET_LINKED_CONTACTS_FUNCTION'] : 'GetLinkedContacts'); 
        $con = $LinkedContactFunction(array('recordid' => $recordid, 'module' => $module, 'formlevel' => 2));  
        $linked_con_num = array();
        
        if(is_array($con))
        {
            $linked_con_num = getFetchedLinkedCons($con);
        }                               
                
        if(is_array($datasourcefields_linkcon))
        {
            foreach($datasourcefields_linkcon as $key => $dsfield)
            {
                $datasourcefields_linkcon_2[$key] = '"'.$key.'"';
                
                $row[$key] = str_replace('"', '""', $row[$key]);
                
                $ap = explode('__', $key);
                if($ap[1] != '')
                {
                    // contact __ROLE is defined
                    $apn = explode('_', $ap[1]);
                    if($apn[1] != '')
                    {
                        // contact numbering suffix is defined like __ROLE_2
                    }
                }   
                else
                {
                    $apn = '';
                }
                $merge_code = array(\UnicodeString::trim($ap[0]), $apn[0], $apn[1], $key);
                $trimmedap0 = \UnicodeString::trim($ap[0]);
                $p = $key;
                $merge_code2[$p] = array($trimmedap0, $apn[0], $apn[1], $p);

            
                $n = array();
                
                
                if(CheckContactToPopUp($merge_code2, $p, '', $con_sum, $linked_con_num, $row_array, array('return' => 'bool')) === true)
                {
                    foreach($row_array as $row)
                    {
                        if($row['CONTACT_RECORDID'] == $_POST['merge_contact'])
                        {
                            $datasourcefields_linkcon_2[$key] = '"'.$key.'"';
                            $docval_linkcon_2[$key] = '"'.str_replace('"', '""', $row[$key]).'"';
                            GetMergeCodeDescr($merge_source, $merge_format, $row, $key, $docval_linkcon_2);
                            break 1;
                        }
                    }
                }
                elseif(!empty($row_array))
                {
                    foreach($row_array as $row)
                    {
                        $row[$key] = str_replace('"', '""', $row[$key]);
                        
                        $n[$row['LINK_ROLE_SWITCH']]++;
                        
                        if($merge_code[1] != '')  
                        {
                            // e.g.: fieldname__LINKROLE is defined
                            // $merge_code[1] is the link role
                            // $merge_code[2] is the number indicator
                            if(!isset($merge_code[2]))
                            {
                                if($row['LINK_ROLE_SWITCH'] == $merge_code[1])
                                {
                                    $datasourcefields_linkcon_2[$key] = '"'.$key.'"';
                                    $docval_linkcon_2[$key] = '"'.$row[$key].'"';
                                    GetMergeCodeDescr($merge_source, $merge_format, $row, $key, $docval_linkcon_2);
                                    //break 1;
                                }
                            }
                            else
                            {
                                if($row['LINK_ROLE_SWITCH'] == $merge_code[1])
                                {
                                    if($n[$row['LINK_ROLE_SWITCH']] == $merge_code[2])
                                    {
                                        $datasourcefields_linkcon_2[$key] = '"'.$key.'"';  
                                        $docval_linkcon_2[$key] = '"'.$row[$key].'"';  
                                        GetMergeCodeDescr($merge_source, $merge_format, $row, $key, $docval_linkcon_2);
                                        //break 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            $datasourcefields_linkcon_2[$key] = '"'.$key.'"';
                            $docval_linkcon_2[$key] = '"'.$row[$key].'"';
                            GetMergeCodeDescr($merge_source, $merge_format, $row, $key, $docval_linkcon_2);
                        }
                       
                        if(!isset($docval_linkcon_2[$key]))
                            $docval_linkcon_2[$key] = '""'; 
                    } // end of foreach
                }// end of if $key == 
                else
                {
                    $datasourcefields_linkcon_2[$key] = '"'.$key.'"';
                    $docval_linkcon_2[$key] = '"'.$row[$key].'"';
                    GetMergeCodeDescr($merge_source, $merge_format, $row, $key, $docval_linkcon_2);
                    if(!isset($docval_linkcon_2[$key]))
                        $docval_linkcon_2[$key] = '""'; 
                }
            }
            if(is_array($datasourcefields_linkcon_2))
                $doccol_linkcon[] = implode(',', $datasourcefields_linkcon_2);
            if(is_array($docval_linkcon_2))
                $docval_linkcon[] = implode(',', $docval_linkcon_2);
        }
    } // end if is_array
    
    if(is_array($doccol_linkcon))
    {
        $doccol_linkcon = implode(',', $doccol_linkcon);
        if($doccol != "")
            $doccol .= ',' . $doccol_linkcon;
        else
            $doccol = $doccol_linkcon;
        
    }
    
    if(is_array($docval_linkcon))
    {
        $docval_linkcon = implode(',', $docval_linkcon);
        if($docval != "")
            $docval .= ',' . $docval_linkcon;
        else
            $docval = $docval_linkcon;
    }  
}
     
/**
* Converts merge code values coming from the database query based on the mergecode settings FORMAT and SOURCE.
* 
* FORMAT is being used to describe date formats 
* SOURCE is being used to define where (which other database table) to get the description of coded fields from.
* 
* @param array $merge_source An array structure containing the merge code and the source definition it is defined for in table MERGECODES.
* @param array $merge_format An array structure containing the merge code and the (date) format definition of the value of the data (defined in table MERGECODES). 
* @param array $row Return array of a database query.
* @param string $key The merge code.
* @param array &$docval_linkcon_2 An array structure containing the merge code as key and the processed data as value.
* 
* @return (passed by reference) array &$docval_linkcon_2 See above. 
*/
function GetMergeCodeDescr($merge_source, $merge_format, $row, $key, &$docval_linkcon_2)
{
    if(is_array($merge_format))
    {
        if(array_key_exists($key, $merge_format))
        {
            switch($merge_format[$key])
            {
                case 'DL':
                case 'D':
                    if($row[$key] == '')
                    {
                        $docval_linkcon_2[$key] = '""'; 
                    }                                
                    else
                    {
                        $date = date_parse($row[$key]);
                        $format = GetParm("FMT_DATE_WEB") == "US" ? 'F j Y' : 'j F Y';
                        $docval_linkcon_2[$key] = '"'.date($format, mktime(0, 0, 0, $date['month'], $date['day'], $date['year'])).'"';
                    }
                    break;
                case 'DS':
                    $docval_linkcon_2[$key] = '"'.FormatDateVal(($row[$key]), false).'"';
                    break;
                default:
                    $docval_linkcon_2[$key] = '"'.$row[$key].'"';
                    break;
            }
        }
    }
    else if(is_array($merge_source))
    {
        if(isset($merge_source[$key]))
        { 
            $mer_source_array = explode(';', $merge_source[$key]);
            $description_f = $mer_source_array[0];
            $codetable = $mer_source_array[1];
            $code = $mer_source_array[2];
            $codewhere = '';
            if($mer_source_array[3] != '')
                $codewhere = $mer_source_array[3];
                
            $sql = "SELECT $description_f as $key";
            $sql .= " FROM $codetable";
            $sql .= " WHERE $code = '$row[$key]'";
            if($codewhere != '')
                $sql .= " AND $codewhere";
            $result2 = db_query($sql);    
            $row2 = db_fetch_array($result2);
            
            if($row2[$key] != '')
                $docval_linkcon_2[$key] = '"'.$row2[$key].'"';
        }
    } 
}                   


/**
* Processes the linked contacts standard array ($con) and restrucutres it to be used in a javascript contact popup. 
* 
* @param array $con Linked contacts standard array.
* @param string $con_linke_role Filter the contacts by link_role to only show one kind of contacts.
* 
* @return array $fetch_con_array An array structure containing contacts processed to be used in a javascript contact popup.
*/
function fetchContacts($con, $con_link_role = '')
{
    $fetch_con_array = array();
    
    $module = $_POST['module'];
    
    switch($module)
    {
        case 'INC':
            $pat_type = GetParm('PAT_TYPE_DEF', 'PAT');
            break;
        case 'COM':
            $pat_type = GetParm('PAT_TYPE_DEF', 'PAT');
            $comp_type = GetParm('COMP_TYPE_DEF', 'COMP');
            $empl_type = GetParm('EMPL_TYPE_DEF', 'EMPL');
            break;
        case 'PAL':
            $enq_type = GetParm('ENQ_TYPE_DEF', 'ENQ');
            break;
    }               
    
    if(is_array($con))
    {
        foreach($con as $link_type => $contacts)
        {
            foreach($contacts as $contact)
            {
                $bAdd_con = false;
                if($contact['rep_approved'] == 'FA' && ($con_link_role == $contact['link_role'] || $con_link_role == '')) 
                {
                    if($pat_type != '' && $pat_type == $con_link_role)
                    {   
                        if($contact['link_type'] == 'A')  // Person 
                        {
                            $bAdd_con = true;
                        }
                    }  
                    elseif($comp_type != '' && $comp_type == $con_link_role)
                    {   
                        if($contact['link_type'] == 'C' || $contact['link_type'] == 'A')  // Complainant
                        {
                            $bAdd_con = true;
                        }
                    }  
                    elseif($empl_type != '' && $empl_type == $con_link_role)
                    {   
                        if($contact['link_type'] == 'E')  // Employee
                        {
                            $bAdd_con = true;
                        }
                    }
                    elseif($enq_type != '' && $enq_type == $con_link_role)
                    {   
                        if($contact['link_type'] == 'P')  // Enquirer
                        {
                            $bAdd_con = true;
                        }
                    }  
                    else
                    {
                        $bAdd_con = true;
                    }
                    
                    if($bAdd_con)
                        $fetch_con_array[$contact['con_id']] = '[' . $contact['link_role'] . '] ' . $contact['con_surname'] . ', ' . $contact['con_forenames'];
                }
            }
        }
    }
    
    asort($fetch_con_array);
    return $fetch_con_array;
}

/**
* Processes the linked contacts standard array ($con) and restrucutres it grouped by link_roles. 
* Needed to determine the number of contacts and the number of contacts with particular roles
* 
* @param array $con Linked contacts standard array
* 
* @return array $linked_con_num An array structure containing contacts grouped by link_role.
*/             
function getFetchedLinkedCons($con)
{
    if(is_array($con))
    {
        foreach($con as $link_type => $contacts)
        {
            if(is_array($contacts))
            {
                foreach($contacts as $contact)
                {
                    /**
                    * If the same contact id exists with the same link role, then do not add a new entry
                    * in order to cope with contacts linked as complainant AND person affected at the same time.
                    */
                    if(isset($linked_con__role_nums[$contact['link_role']]))
                    {
                        if(in_array($contact['recordid'], $linked_con__role_nums[$contact['link_role']]))
                        {
                            continue;
                        }
                    } 
                    $linked_con__role_nums[$contact['link_role']][] = $contact['recordid'];
                }
            }
        }
        
        if(is_array($linked_con__role_nums))
        {
            foreach($linked_con__role_nums as $link_role => $linked_con)
            {
                 $linked_con_num[$link_role] = count($linked_con);
            }
        }
    }

    return $linked_con_num;
}


/**
* Creates a copy of the document template and saves it to the same folder with a unique name.
* 
* @return Echos back a string to the Javascript AJAX call the file name and extension of the copy. If something went wrong it returns the string 'error'. 
*/
function CopyTemplateDoctoClient()
{
    $DocTemplateFile = $_POST['doctemplate'];
    $doc_unique_name = unique_filename('.doc');
    
    $ServerFolder = GetParm("PATH_TEMPLATES");
   // $ServerFolder = addslashes($ServerFolder);
    $FromFile = Sanitize::SanitizeFilePath($ServerFolder . "\\" . $DocTemplateFile);
    $ToFile =  Sanitize::SanitizeFilePath($ServerFolder . "\\" . $doc_unique_name);
    $ToFileName = $doc_unique_name;
    
    if (!copy($FromFile, $ToFile))
        echo 'error';
    else
        echo $ToFileName;
}   

/*function CopyTemplateDoctoClient()
{
    $DocTemplateFile = $_POST['doctemplate'];
    $doc_unique_name = unique_filename('.doc');
    
    $ServerFolder = GetParm("PATH_TEMPLATES");
    //$ServerFolder = addslashes($ServerFolder);
    $FromFile = $ServerFolder . "\\" . $DocTemplateFile;
    $ToFile = $ServerFolder . "\\" . $doc_unique_name;
    $ToFileName = $doc_unique_name;
    
    if (!copy($FromFile, $ToFile))
        echo 'error';
    else
        echo $ToFileName;
}  */


/**
* Deletes the temporary copy of the document template and the datasource file after the word merging has been finished.
*/
function DeleteWordTmpFile()
{
    $tmpFileName1 = $_POST['doctemplatefile'];
    $tmpFileName2 = $_POST['datasrcfile'];
    
    $ServerFolder = GetParm("PATH_TEMPLATES");
   // $ServerFolder = addslashes($ServerFolder);
    if($tmpFileName1 != '')
    {
        $tmpFile1 =  Sanitize::SanitizeFilePath($ServerFolder . "\\" . $tmpFileName1);
        @unlink($tmpFile1);
    }
    if($tmpFileName2 != '')
    {
        $tmpFile2 =  Sanitize::SanitizeFilePath($ServerFolder . "\\" . $tmpFileName2);
        @unlink($tmpFile2);
    } 
}

/**
* Creates a string containing contact data that will be embadded to the html page to be used by the javascript contact popup. 
* 
* @param array $con Linked contacts standard array.
* @param string $con_linke_role Filter the contacts by link_role to only show one kind of contacts.
* 
* @return string $overrideArrayString String that contains contact data to be used by the javascript contact popup. 
*/                                                                                                                
function MergeContactCodeInsert($con, $con_link_role = '')
{
    $overrideArrayString = "{";
        
    $fetch_con_array = fetchContacts($con, $con_link_role); 
    
    if(is_array($fetch_con_array))
    {
        foreach($fetch_con_array as $con_id => $con_descr)
        {
            $conlist .= "'" . $con_id . "': " . "'" . str_replace("'", "\'", $con_descr) . "',";
        }
        
        $conlist = \UnicodeString::substr($conlist, 0, -1);
        $overrideArrayString .= $conlist;
    }
    $overrideArrayString .= "}"; 
    
    return $overrideArrayString;
}


/**
* Calculates whether the user needs to be prompted to choose a contact for word merging or not
* 
* @param array $merge_code Array containing merge code and field codes.
* @param string $p The original merge code as defined in the document template (with suffixes like _F, etc).
* @param array $con Linked contacts standard array.
* @param int $con_sum Number of contacts
* @param array $linked_con_num An array structure containing contacts grouped by link_role.
* @param array $row Return array of a database query. 
* @param array $aParam Parameter to swich between result types: bool or JSON.
* 
* @return depending on $aParam: bool or JSON encoded echo string
*/                                                            
function CheckContactToPopUp($merge_code, $p, $con, $con_sum, $linked_con_num, $row, $aParam = array())
{
    if($aParam['return'] == 'JSON')
    {
        require_once 'thirdpartylibs/Services_JSON.php';
        $json = new Services_JSON();
    }
        
    if($merge_code[$p][1] != '')  
    {
        // e.g.: fieldname__LINKROLE is defined
        // $merge_code[$n][0] is the link role
        // $merge_code[$n][1] is the number indicator
        
        if(!isset($merge_code[$p][2]))
        {
            // only link role is set, no specific contact number
            if($aParam['return'] == 'JSON')
            {
                $bMergeCodeMatch = (\UnicodeString::strtoupper($merge_code[$p][0]) == \UnicodeString::strtoupper($row['mer_code']));
            }
            else
            {
                $bMergeCodeMatch = true;
            }
            if($linked_con_num[$merge_code[$p][1]] > 1 && $bMergeCodeMatch)
            {
                if($aParam['return'] == 'JSON')
                {
                    $data = array(MergeContactCodeInsert($con, $merge_code[$p][1]), $p);
                    header('Content-type: application/x-json');
                    echo $json->encode($data);
                    exit;
                }
                elseif($aParam['return'] == 'bool')
                {
                    return true;
                }
            }   
        }
        else
        {
            if($merge_code[$p][2] > $linked_con_num[$merge_code[$p][1]])
            {                        
                // defined number is higher than the number of contacts with that role
                if($aParam['return'] == 'JSON')
                {
                    $data = array(MergeContactCodeInsert($con, $merge_code[$p][1]), $p);
                    header('Content-type: application/x-json');
                    echo $json->encode($data);
                    exit;
                }
                elseif($aParam['return'] == 'bool')
                {
                    return true;
                }
            }
        }
    }
    else
    {
        // no specific link role is set, just contact field(s)
        // having more than one contact -> prompt user
        if($con_sum > 1)
        {
            if($aParam['return'] == 'JSON')
            {
                $data = array(MergeContactCodeInsert($con, ''), $p);
                header('Content-type: application/x-json');
                echo $json->encode($data);
                exit;
            }
            elseif($aParam['return'] == 'bool')
            {
                return true;
            }
        }
    }
    
    return false;
}


/**
* Collects data for extra field merge codes.
* 
* This funtion queries the database to collect data and organises the merge codes and the returned values 
* to be later processed and a datasource file to be created in function RunMergeSQL().
* 
* @param array $udffields The extra fields to be selected in the SQL query. 
* @param string &$doccol A string containing the columns part of the datasource file.
* @param string &$docval A string containing the value part of the datasource file. 
* 
* @return (passed by reference) string &$doccol See above.
* @return (passed by reference) string &$docval See above.
*/
function getExtraFieldsVal($udffields, &$doccol, &$docval)
{
    if(is_array($udffields))
    {
        $module = $_POST['module'];
        $recordid = $_POST['recordid'];
        $aExtraFields = array();
        $aExtraFields = GetAllMergeExtraFields($module, $recordid, array());
        
        foreach($udffields as $key => $val)
        {
            if(\UnicodeString::strpos($key, 'EXTRA') !== false)
            {
                $ap = explode('_', $key);
                if($ap[0] != '')  //EXTRA3
                {
                    $ap0num = \UnicodeString::substr($ap[0], 5);
                    
                    if($ap[1] != '')  //VALUE, VALUE_F, NAME
                    {
                        switch($ap[1])
                        {
                            case "VALUE": // get (code) value or description
                                if($ap[2] == 'F') // get (code) description (VALUE_F)
                                {
                                    $doccol_udf[$key] = '"'.$key.'"';
                                    $docval_udf[$key] = '"'.$aExtraFields[$ap0num-1]['VALUE_F'].'"';
                                }
                                else // get (code) value 
                                {
                                    $doccol_udf[$key] = '"'.$key.'"';
                                    $docval_udf[$key] = '"'.$aExtraFields[$ap0num-1]['VALUE'].'"';
                                }
                                break;
                            case "NAME": // get udf field name
                                $doccol_udf[$key] = '"'.$key.'"';
                                $docval_udf[$key] = '"'.$aExtraFields[$ap0num-1]['NAME'].'"';
                                break;
                            default: // do nothing
                                break;
                        }
                    }
                }
            }                                   
        }                                       
    
        if(is_array($doccol_udf))
        {
            $doccol_udf = implode(',', $doccol_udf);
            if($doccol != "")
                $doccol .= ',' . $doccol_udf;
            else
                $doccol = $doccol_udf;
            
        }
        
        if(is_array($docval_udf))
        {
            $docval_udf = implode(',', $docval_udf);
            if($docval != "")
                $docval .= ',' . $docval_udf;
            else
                $docval = $docval_udf;
        }  
    }                        
}
         
             
/**
* @desc Retrieving all the extra fields which have data in the same order (ish) as they appear in the form and the Main application.
* 
* @param string $Mod Module code.
* @param int $RecordID The recordid of the main record.
* 
* @return array $udfArray Returns an array of extra fields with data, description and extra field name. 
*/                                                          
function GetAllMergeExtraFields($Mod, $RecordID)
{
    global $ModuleDefs;

    $ModID = $ModuleDefs[$Mod]['MOD_ID'];
    
    /* Retrieve the order of UDF fields the same as the rich client order*/
    $sql ='SELECT udf_links.field_id as fld_id, 
                udf_links.group_id as grp_id, 
                fld_name, 
                fld_type, 
                fld_format, 
                listorder, 
                fld_code_like, 
                0 as field_order
            FROM udf_links, udf_fields
            LEFT OUTER JOIN UDF_VALUES ON udf_fields.RECORDID = UDF_VALUES.FIELD_ID AND UDF_VALUES.CAS_ID = :cas_id and UDF_VALUES.MOD_ID = :mod_id1
            WHERE udf_links.field_id = udf_fields.recordid and udf_links.group_id in (SELECT GROUP_ID FROM udf_record_links WHERE MOD_ID = :mod_id2 AND CAS_ID = :cas_id2)
            UNION
            SELECT field_id, 
                group_id, 
                fld_name, 
                fld_type, 
                fld_format, 
                0, 
                \'\',  
                (CASE WHEN udf_values.group_id = 0 THEN 1 ELSE 0 END) as field_order
            FROM udf_values, udf_fields
            WHERE udf_values.mod_id = :mod_id3 and cas_id = :cas_id3 and udf_values.field_id = udf_fields.recordid
            AND group_id not in (SELECT GROUP_ID FROM udf_record_links WHERE MOD_ID = :mod_id4 AND CAS_ID = :cas_id4)
            ORDER BY field_order, grp_id, listorder';
            
    $result = DatixDBQuery::PDO_fetch_all($sql, array('mod_id1' => $ModID, 'mod_id2' => $ModID, 'mod_id3' => $ModID, 'mod_id4' => $ModID, 
                                                        'cas_id' => $RecordID, 'cas_id2' => $RecordID, 'cas_id3' => $RecordID, 'cas_id4' => $RecordID));     

    foreach ($result as $UDFFieldrow)
    {
        /* Get the data for the field seperately (due to problems retrieving distinct values via UNION with text data type*/
        $sql = '
        SELECT
            udf_values.field_id as fld_id,
           udf_values.udv_string,
            udf_values.udv_number,
            udf_values.udv_date,
            udf_values.udv_money,
            udf_values.udv_text,
            udf_fields.fld_name,
            udf_fields.fld_type,
            udf_fields.fld_format
        FROM
            udf_values
        INNER JOIN
            udf_fields ON udf_values.field_id = udf_fields.recordid 
        WHERE
            udf_values.mod_id = :mod_id1 AND
            udf_values.cas_id = :cas_id
            AND udf_values.field_id = :fld_id
            AND udf_values.group_id = :grp_id
        ';
        
        $row = DatixDBQuery::PDO_fetch($sql, array('mod_id1' => $ModID, 
                                                        'cas_id' => $RecordID,
                                                        'fld_id' => $UDFFieldrow['fld_id'],
                                                        'grp_id' => $UDFFieldrow['grp_id']));
        
        $UDFValues = array(
                'udv_string' => $row['udv_string'],
                'udv_number' => $row['udv_number'],
                'udv_date' => $row['udv_date'],
                'udv_money' => $row['udv_money'],
                'udv_text' => $row['udv_text'],
                );

        $UDFProperties = array(
            'fld_name' => $UDFFieldrow['fld_name'],
            'grp_id' => $UDFFieldrow['grp_id'],
            'fld_id' => $UDFFieldrow['fld_id'],
            'fld_type' => $UDFFieldrow['fld_type'],
            'fld_format' => $UDFFieldrow['fld_format']
            );

        $udfArray[] = GetUDFFieldValue($UDFProperties, $UDFValues);
    }
    
    return $udfArray;
}


/**
* @desc Gets the value, the description and the field name of an extra field.  
* A rough copy of MakeUDFField() method of FormTable class.
* 
* @param array $UDFProperties Properties of the extra field e.g. name, id, type and format.
* @param array $UDFValues Values by type of the extra field as it is in the database.
* 
* @return array Value, description and the name of the extra field in an array format. 
*/
function GetUDFFieldValue($UDFProperties, $UDFValues) 
{
    $HasValue = false;
    if (!empty($UDFValues) && is_array($UDFValues))
    {
        foreach ($UDFValues as $Value)
        {
            if ($Value != '')
            {
                $HasValue = true;
                break;
            }
        }
    }  
    
    $FieldValue = '';
    $FieldDescr = '';
    $FieldName = $UDFProperties['fld_name'];
    $module = $_POST['module'];
    
    switch ($UDFProperties['fld_type'])
    {
        case "S":
        case "E":
            if ($HasValue === true)
            {
                $FieldValue = $UDFValues["udv_string"];
                $FieldDescr = $FieldValue;
            }
            break;
        case "D":
            if ($HasValue === true)
            {
                $FieldValue = FormatDateVal($UDFValues["udv_date"]);
                $FieldDescr = $FieldValue;
            }
            break;
        case "C":
            if ($HasValue === true)
            {
                $FieldValue = $UDFValues["udv_string"];
                $FieldDescr = code_descr_udf($UDFProperties["fld_id"], $FieldValue, $module);
            }
            break;
        case "N":
            if ($HasValue === true)
            {
                //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                if ($Format != "")
                {
                    $FormattedNumber = GuptaFormatEmulate($UDFValues["udv_number"], $Format);
                }
                else
                {
                    $FormattedNumber = number_format($UDFValues["udv_number"], 2, '.', '');
                }
                $FieldValue = $FormattedNumber;
                $FieldDescr = $FieldValue;
            }
            break;
        case "M":
            if ($HasValue === true)
            {
                //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                if ($Format != "")
                {
                    $FormattedNumber = GuptaFormatEmulate($UDFValues["udv_money"], $Format);
                }
                else
                {
                    $FormattedNumber = number_format($UDFValues["udv_money"], 2, '.', '');
                }
                $FieldValue = $FormattedNumber;
                $FieldDescr = $FieldValue;
            }
            break;
        case "T":
        case "Y":
            if ($HasValue === true)
            {
                $FieldValue = $UDFValues["udv_string"];
                $FieldDescr = code_descr_udf($UDFProperties["fld_id"], $FieldValue, $module);
            }
            break;
        case "L":
            if ($HasValue === true)
            {
                $FieldValue = $UDFValues["udv_text"];
                $FieldDescr = $FieldValue;
            }
            break;
    }  
    
    return array('VALUE' => $FieldValue,
                 'VALUE_F' => $FieldDescr,
                 'NAME' => $FieldName);
}
?>      