<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "action" => array(
        "Title" => "Action Details",
        "NewPanel" => true,
        "Rows" => array(
            array(
                "Title" => "Action ID",
                "Name" => "recordid",
                "Type" => "formfield",
                "FormField" => $IDFieldObj
            ),
            array(
                "Title" => "Module",
                "Name" => "act_module",
                "Type" => "formfield",
                "FormField" => $ModuleFieldObj
            ),
            array(
                "Type" => "string",
                "Name" => "act_recordname",
                "Title" => "Record",
                //Pretty hacky, but I don't want to put a sql call in this file.
                "Condition" => $FormType != 'Search' && CanSeeModule($act['act_module']) && $act['permissions_to_linked_record'] === true,
                "ReadOnly" => true
            ),
            array(
                "Type" => "string",
                "Name" => "ach_title",
                "Title" => "Action chain title",
                "Condition" => $FormType != 'Search' && $act['ach_title'],
                "ReadOnly" => true
            ),
            array(
                "Type" => "string",
                "Name" => "act_step_no",
                "Title" => "Step number",
                "Condition" => $FormType != 'Search' && $act['act_step_no'],
                "ReadOnly" => true
            ),
            "act_cas_id",
            "act_priority",
            "act_type",
            "act_organisation",
            "act_unit",
            "act_clingroup",
            "act_directorate",
            "act_specialty",
            "act_loctype",
            "act_locactual",
            "act_descr",
            "act_dstart",
            "act_ddue",
            "act_ddone",
            "act_from_inits",
            "act_to_inits",
            "act_by_inits",
            "act_cost",
            "act_cost_type",
            "act_synopsis",
            "act_resources",
            "act_monitoring",
            "act_progress",
            "act_score"
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $act['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('ACT', $act['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NewPanel' => true,
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "NoReadOnly" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController')),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    "links" => array(
        "Title" => "Links",
        "NewPanel" => true,
        "NoFieldAdditions" => true,
        "Include" => 'Source/actions/ActionForm.php',
        'ControllerAction' => array(
            'ListModuleLinks' => array(
                'controller' => 'src\\actions\\controllers\\ActionsController'
            )
        ),
        "NotModes" => array("New", "Search"),
        "Rows" => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController')),
        "NotModes" => array("New", "Search"),
        "Rows" => array()
    )
);

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}
