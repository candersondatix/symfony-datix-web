<?php

// File contains functions specifically for actions linked to a record in
// another module, e.g. actions linked to safety alerts

/**
* @desc Updates ram_open_actions field in ra_main for use in listings in the main app.
*
* @param int $recordid ID of the risk record to be updated.
*/
function UpdateRAMActionsOpen($aParams)
{
    if($aParams['action_id'] && !$aParams['risk_id'])
    {
        $aParams['risk_id'] = DatixDBQuery::PDO_fetch('SELECT act_cas_id FROM ca_actions WHERE recordid = :act_id AND act_module = \'RAM\'', array('act_id' => $aParams['action_id']), PDO::FETCH_COLUMN);
    }
    if($aParams['risk_id'])
    {
        $NumActions = DatixDBQuery::PDO_fetch('SELECT ram_act_count_open FROM ram_links_act_open WHERE ram_id = :ram_id', array('ram_id' => $aParams['risk_id']), PDO::FETCH_COLUMN);

        if (isset($NumActions))
        {
            DatixDBQuery::PDO_query('UPDATE ra_main SET ram_open_actions = :num_actions WHERE recordid = :ram_id', array('num_actions' => intval($NumActions), 'ram_id' => $aParams['risk_id']));
        }
    }
}

