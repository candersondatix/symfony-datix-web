<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '4'),
    "act_to_inits" => array(
        'width' => '4'),
    "act_from_inits" => array(
        'width' => '4'),
    "act_module" => array(
        'width' => '6'),
    "act_descr" => array(
        'width' => '20'),
    "act_ddue" => array(
        'width' => '4'),
    "act_ddone" => array(
	'width' => '4'),
    "act_priority" => array(
        'width' => '7'),
);

$list_columns_mandatory = array("recordid");

$list_columns_extra = array(
);
?>

