<?php

// ==========================================================================//
// Initialise conditions                                                     //
// ==========================================================================//

$record_url_part = 'fromsearch=1';

if ($_SESSION['ACT']['DRILL_QUERY'] instanceof \src\framework\query\Query &&
    (isset($_GET['from_report']) && $_GET['from_report'] == '1' || isset($_GET['fromcrosstab']) && $_GET['fromcrosstab'] == '1'))
{
    $drillIntoReport = true;
    $record_url_part = 'from_report=1';
}

$_SESSION["ACT"]["EXTRAWHERE"] = '';

$From = $_REQUEST["from"];
$To = $_REQUEST["to"];
$Timescale = $_REQUEST["timescale"];
$DateField = $_REQUEST["whichdate"];

$_SESSION["ReadOnlyMode"] = false;

$ListType = $_GET["listtype"];

// ==========================================================================//
// Build where clause according to access level, permission, timescales      //
// and criteria specific to the type of listing                              //
// ==========================================================================//

$ListTitle = "Actions Listing";

// Only list actions for modules which are licensed
$LicModules = getActionLinkModules();

if (empty($LicModules) === false)
{
	$ExtraWhere[] = "(act_module IN ('" . implode("', '", getActionLinkModules()) . "'))";
}

if ($ListType != 'search') // make sure we trigger the message to allow reporting on session data.
{
    $_SESSION["ACT"]["WHERE"] = $ModuleDefs['ACT']['HARD_CODED_LISTINGS'][$ListType]['Where'];
}

if ($_GET['overdue'] || $ListType == 'overdue')
{
    $ExtraWhere[] = $ModuleDefs['ACT']['HARD_CODED_LISTINGS']['overdue']['OverdueWhere'];
	$ListTitle = $ModuleDefs['ACT']['HARD_CODED_LISTINGS']['overdue']['Title'];
    $ListType = 'overdue';
}
else {
	$ListTitle = "Actions Listing";
}

if ($DateField == "")
{
    $DateField = "act_ddue";
}
if ($Timescale == "")
{
    $Timescale = "month";
}

if ($_REQUEST["drillwhere"] != "")
{
	$ActionsWhere[] = Sanitize::SanitizeRaw($_REQUEST["drillwhere"]);
}
else if ($drillIntoReport)
{
    $query = $_SESSION['ACT']['DRILL_QUERY'];

    $writer = new \src\framework\query\SqlWriter();

    list($statement, $parameters) = $writer->writeStatement($query);

    // Replace all occurrences of ? with values on the where clause
    $needle = '?';

    foreach ($parameters as $parameter)
    {
        $parameterPos = UnicodeString::strpos($statement, $needle);

        if ($parameterPos !== false)
        {
            $statement = UnicodeString::substr_replace($statement, DatixDBQuery::quote($parameter), $parameterPos, UnicodeString::strlen($needle));
        }
    }

    $ActionsWhere[] = $ModuleDefs['ACT']['TABLE'].'.recordid IN ('.$statement.')';

    $_SESSION['ACT']['current_drillwhere'] = $ActionsWhere[0];
}
else
{
    if ($_GET['fromcrosstab'] == '1')
    {
        $ExtraWhere[] =  "(" . $_SESSION['CurrentReport']->report->ConstructCellWhereClause($_GET) . ")";
    }
    else if ($_SESSION["ACT"]["WHERE"] && $ListType == "search")
    {
        $ExtraWhere[] =  "(" . $_SESSION["ACT"]["WHERE"] . ")";
    }
}

if ($ExtraWhere)
{
    $ActionsWhere[] =  implode(" and ", $ExtraWhere);
    $_SESSION["ACT"]["EXTRAWHERE"] = implode(" and ", $ExtraWhere);
}


$WhereClause = MakeSecurityWhereClause($ActionsWhere, "ACT", $_SESSION["initials"]);

$orderby = $_SESSION["LIST"]["ACT"]["ORDERBY"];
$order = $_SESSION["LIST"]["ACT"]["ORDER"];

if (!$orderby){
    $orderby = "act_ddue";
    $order = "DESC";
}

$listnum = $listnum2;

if($_POST["listnum"])
    $listnum = Sanitize::SanitizeInt($_POST["listnum"]);

$dbtable = array("ca_actions");

// ==========================================================================//
// Image file to display at the top left of the form                         //
// ==========================================================================//

$img = "Images/actionstitle.gif";


// ==========================================================================//
// Fields to be available in the timescales section                          //
// ==========================================================================//

$timescales_datefields = array(
    "act_dstart" => "Start",
    "act_ddue" => "Due",
    "act_ddone" => "Done");


// ==========================================================================//
// Build the URL to be used when clicking on a record                        //
// ==========================================================================//

$timescales_url = $scripturl . '?action=list&amp;module=ACT&amp;whichdate='.$WhichDate.'&amp;listtype='.$ListType.(($_GET["timescale"]=="range" || ($_REQUEST["from"] && $_REQUEST["to"])) ? "&amp;from=$_REQUEST[from]&amp;to=$_REQUEST[to]" : "")
.($_GET["timescale"]=="overdue" ? "&amp;timescale=overdue" : "");
// linking from a graphic report
if ($_REQUEST["rows"] != '')
{
    $timescales_url = "$scripturl?". htmlspecialchars($_SERVER[QUERY_STRING]);
}
if($_GET['fromcrosstab'] == '1')
{
    $timescales_url .= '&amp;fromcrosstab=1&amp;row='.$_GET['row'].'&amp;col='.$_GET['col'];
}
$record_url = "$scripturl?action=action&".$record_url_part;

// ==========================================================================//
// Columns to be displayed                                                   //
// ==========================================================================//

require_once 'Source/libs/ListingClass.php';
$list_columns = Listings_ModuleListingDesign::GetListingColumns($module);

if (is_array($list_columns))
{
    foreach ($list_columns as $col_name => $col_info)
    {
        if(is_array($list_columns_extra[$col_name])){
            if(!array_key_exists("condition", $list_columns_extra[$col_name]) || $list_columns_extra[$col_name]["condition"]){
                $selectfields[$col_name] = $col_name;
            }
            else{
                unset($list_columns[$col_name]);
            }
            if(array_key_exists("prefix", $list_columns_extra[$col_name]))
                $col_info_extra['prefix'] = $list_columns_extra[$col_name]["prefix"];
            if(array_key_exists("dataalign", $list_columns_extra[$col_name]))
                $col_info_extra['dataalign'] = $list_columns_extra[$col_name]["dataalign"];
        }
        else
            $selectfields[$col_name] = $col_name;
    }
}

$list_columns_count = count($list_columns);

$list_request = GetSQLResultsTimeScale($selectfields, $dbtable, $order, $orderby,
            $WhereClause, $listnum, $listdisplay, $listnum2, $listnumall, $sqla, null, $module);

//needed in BrowseList to create RecordList session object.
$ListWhereClause = $WhereClause;