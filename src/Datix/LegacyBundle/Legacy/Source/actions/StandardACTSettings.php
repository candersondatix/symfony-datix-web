<?php
$GLOBALS["FormTitle"] = "Action Form";
$GLOBALS["MandatoryFields"] = array(
    "act_descr" => "action",
    "act_to_inits" => "action"
);

$GLOBALS["HideFields"] = array(
    "act_organisation" => true,
    "act_unit" => true,
    "act_clingroup" => true,
    "act_directorate" => true,
    "act_specialty" => true,
    "act_loctype" => true,
    "act_locactual" => true,
    "progress_notes" => true,
    "documents" => true,

);

?>
