<?php

use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

function EmailAction($recordid, $Act, $Type)
{
    global $txt, $ModuleDefs;

    require_once 'Source/libs/Email.php';

    if ($Type != 'New')
    {
        $OriginalAction = GetActionRecord($recordid);
    }

    if ($Act == '')
    {
        $Act = $OriginalAction;
    }

    // Check to see if the action has been completed.
    if ($Type == 'Update' && $OriginalAction['act_ddone'] == ''
        && $Act['act_ddone'] != '' && $Act['act_ddone'] != 'NULL')
    {
        $Type = 'Complete';
    }

    $MainModuleDetails = GetModuleDetails($Act['act_module'], $Act['act_cas_id']);

    $Act = array_merge($Act, $MainModuleDetails);

    $Factory = new UserModelFactory();

    $result = 1;

    $StaffBy   = $Factory->getMapper()->findByInitials($Act['act_by_inits']);
    $StaffFrom = $Factory->getMapper()->findByInitials($Act['act_from_inits']);
    $StaffTo   = $Factory->getMapper()->findByInitials($Act['act_to_inits']);

    $Act['from_fullname'] = $StaffFrom->fullname;
    $Act['to_fullname']   = $StaffTo->fullname;
    $Act['by_fullname']   = $StaffBy->fullname;

    // If the record is completed
    // E-mail the From user (always).
    // E-mail the To user if they are not the same as the From user
    // E-mail the By user if they are not the same as the From or To users and
    // are not the currently logged in user.
     if ($Type == 'Complete')
     {
         // Email users as long as they are not the one currently logged in.
         if ($StaffFrom && $Act['act_from_inits'] != $_SESSION['initials'] && empty($StaffFrom->con_dod))
         {
             Registry::getInstance()->getLogger()->logEmail('E-mailing the From user, as long as they are not the one currently logged in, for completed Action/Action Chain ' . $Act['recordid'] . '.');

             $emailSender = EmailSenderFactory::createEmailSender('ACT', 'Complete');
             $emailSender->addRecipient($StaffFrom);
             $result = $emailSender->sendEmails(array_merge($StaffFrom->getVars(), $Act));

             foreach ($emailSender->getFailedDomainValidation() as $failedRecipient)
             {
                $blockedUsers[] = $failedRecipient->fullname;
             }
         }

         // E-mail the "To" user only if they are not the same as the "From" user
         if ($StaffTo && $Act['act_to_inits'] != $Act['act_from_inits'] && $Act['act_to_inits'] != $_SESSION['initials'] && empty($StaffTo->con_dod))
         {
             Registry::getInstance()->getLogger()->logEmail('E-mailing the To user, only if they are not the same as the From user, for completed Action/Action Chain ' . $Act['recordid'] . '.');

             $emailSender = EmailSenderFactory::createEmailSender('ACT', 'Complete');
             $emailSender->addRecipient($StaffTo);
             $result = $emailSender->sendEmails(array_merge($StaffTo->getVars(), $Act));

             foreach ($emailSender->getFailedDomainValidation() as $failedRecipient)
             {
                 $blockedUsers[] = $failedRecipient->fullname;
             }
         }

         // E-mail the "By" user only if they are not the same as the "From" user or the "To" user.
         if ($StaffBy && $Act['act_by_inits'] != $Act['act_from_inits'] &&
             $Act['act_to_inits'] != $Act['act_by_inits']
             && $Act['act_by_inits'] != $_SESSION['initials'] && empty($StaffBy->con_dod))
         {
             Registry::getInstance()->getLogger()->logEmail('E-mail the By user, only if they are not the same as the From user or the To user, for completed Action/Action Chain ' . $Act['recordid'] . '.');

             $emailSender = EmailSenderFactory::createEmailSender('ACT', 'Complete');
             $emailSender->addRecipient($StaffBy);
             $result = $emailSender->sendEmails(array_merge($StaffBy->getVars(), $Act));

             foreach ($emailSender->getFailedDomainValidation() as $failedRecipient)
             {
                 $blockedUsers[] = $failedRecipient->fullname;
             }
         }
     }
     // Update:
     // e-mail From user (unless they are the same as the logged-in user)
     // e-mail To user (unless they are the same as the logged-in user)
     // Only e-mail if the record has been changed
     elseif ($Type == 'Update' && IsChanged($Act))
     {
         // Don't send to the From user if they are the current user
         if ($StaffFrom && $StaffFrom->initials != $_SESSION['initials'] && empty($StaffFrom->con_dod))
         {
             Registry::getInstance()->getLogger()->logEmail('E-mailing the From user, unless they are the same as the logged-in user, for updated Action/Action Chain ' . $Act['recordid'] . '.');

             $emailSender = EmailSenderFactory::createEmailSender('ACT', 'Update');
             $emailSender->addRecipient($StaffFrom);
             $result = $emailSender->sendEmails(array_merge($StaffFrom->getVars(), $Act));

             foreach ($emailSender->getFailedDomainValidation() as $failedRecipient)
             {
                 $blockedUsers[] = $failedRecipient->fullname;
             }
         }

         // Don't send to the To user if they are the current user
         // or they are the same as the From user
         if ($StaffTo && $StaffTo->initials != $_SESSION['initials']
            && $StaffTo->initials != $StaffFrom->initials && empty($StaffTo->con_dod) && !empty($StaffTo->con_email))
         {
             Registry::getInstance()->getLogger()->logEmail('E-mailing the To user, unless they are the same as the logged-in user, for updated Action/Action Chain ' . $Act['recordid'] . '.');

             $emailSender = EmailSenderFactory::createEmailSender('ACT', 'Update');
             $emailSender->addRecipient($StaffTo);
             $result = $emailSender->sendEmails(array_merge($StaffTo->getVars(), $Act));

             foreach ($emailSender->getFailedDomainValidation() as $failedRecipient)
             {
                 $blockedUsers[] = $failedRecipient->fullname;
             }
         }
     }
     // New action:
     // e-mail To user (unless they are the logged-in user)
     elseif ($Type == 'New')
     {
         // Don't send to the To user if they are the current user
         if ($StaffTo && $StaffTo->initials != $_SESSION['initials'] && empty($StaffTo->con_dod) && !empty($StaffTo->con_email))
         {
             Registry::getInstance()->getLogger()->logEmail('E-mailing the To user, unless they are the same as the logged-in user, for new Action/Action Chain ' . $Act['recordid'] . '.');

             $emailSender = EmailSenderFactory::createEmailSender('ACT', 'New');
             $emailSender->addRecipient($StaffTo);
             $result = $emailSender->sendEmails(array_merge($StaffTo->getVars(), $Act));

             foreach ($emailSender->getFailedDomainValidation() as $failedRecipient)
             {
                 $blockedUsers[] = $failedRecipient->fullname;
             }
         }
     }

     if ($result == 0)
     {
        // some error ocurred during send
        return array ('result' => false, 'blocked' => implode(', ', $blockedUsers));
     }
     else
     {
        return array ('result' => true);
     }
}

// GetActionRecord($recordid) returns an array containing information retrieved
// from the Actions (ca_actions) table
function GetActionRecord($recordid)
{
    $sql = "
        SELECT
            recordid, act_module,
   			act_cas_id, act_ddue, act_dstart,
   			act_ddone, act_cost, act_descr, act_type, act_priority, act_from_inits,
   			act_to_inits, act_by_inits, act_synopsis, act_cost_type, act_score,
   			act_resources, act_monitoring, act_progress
        FROM
            ca_actions
        WHERE
            recordid = :recordid
    ";

    $action = PDO_fetch($sql, array('recordid' => $recordid));

    return $action;
}

// IsChanged($Act) takes a POST array and returns TRUE if any of the values
// have been changed (i.e. there is a CHANGED- field present).
function IsChanged($Act)
{
    foreach ($Act as $Name => $Value)
    {
        $Field = explode('-', $Name);

        if ($Field[0] == 'CHANGED' && $Value)
        {
            return true;
        }
    }

    return false;
}

// Takes a module code and a main record ID.
// Returns the "Our ref", name and handler for the main record
function GetModuleDetails($Module, $ModuleRecordID)
{
    global $ModuleDefs;

    $Table = $ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE'];
    $OurRef = $ModuleDefs[$Module]['OURREF'];
    $Name = $ModuleDefs[$Module]['FIELD_NAMES']['NAME'];
    $Handler = $ModuleDefs[$Module]['FIELD_NAMES']['HANDLER'];

    if ($Name)
    {
        $SelectFields[] = $Name;
    }

    if ($OurRef)
    {
        $SelectFields[] = $OurRef;
    }

    if ($Handler)
    {
        $SelectFields[] = $Handler;
    }

    if (!empty($SelectFields))
    {
        $sql = "SELECT
            ".implode(', ', $SelectFields)."
            FROM $Table
            WHERE recordid = :recordid";

        $ModuleDetails = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $ModuleRecordID));
    }

    $ModuleReturn['mod_ourref'] = $ModuleDetails[$OurRef];
    $ModuleReturn['mod_name'] = $ModuleDetails[$Name];
    $ModuleReturn['mod_handler'] = $ModuleDetails[$Handler];
    $ModuleReturn['record_name'] = $ModuleDefs[$Module]['REC_NAME'];

    return $ModuleReturn;
}

?>