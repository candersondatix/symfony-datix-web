<?php

$EmailText['NewStaff']['Subject'] = 'Notification of assigned assessment: ' . $data['atm_title'];
$EmailText['NewStaff']['Body'] = '
    You have been selected to review the ' . $data['atm_title'] . ' assessment. Select the link below to view the record.

    Link to assessment: ' . $data['scripturl'];

$EmailText['NewLocation']['Subject'] = 'Notification of assigned assessment: ' . $data['atm_title'];
$EmailText['NewLocation']['Body'] = '
    The ' . $data['atm_title'] . ' assessment has been assigned to your location (' . $data['loc_name'] . '). Select the link below to view the record.

    Link to assessment: ' . $data['scripturl'];

$EmailText['NewMasterStaff']['Subject'] = 'Submission of assigned assessment: ' . $data['atm_title'];
$EmailText['NewMasterStaff']['Body'] = '
    The ' . $data['atm_title'] . ' assessment assigned to ' . $data['loc_name'] . ' has been submitted and is ready for review. Select the link below to view the record.

    Link to assessment: ' . $data['scripturl'];

$EmailText['ReviewedInstances']['Subject'] = 'Review of assessment completed: ' . $data['atm_title'];
$EmailText['ReviewedInstances']['Body'] = '
    The ' . $data['atm_title'] . ' assessment assigned to ' . $data['loc_name'] . ' has been reviewed. Select the link below to view the record.

    Link to assessment: ' . $data['scripturl'];

?>