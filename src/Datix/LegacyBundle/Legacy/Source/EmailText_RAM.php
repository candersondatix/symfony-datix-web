<?php

$EmailText["Acknowledge"]["Subject"] = "Acknowledgment of risk report submission";

$EmailText["Acknowledge"]["Body"] = "You reported a risk on $data[ram_dreported] using DatixWeb.
The risk has been given the number $data[recordid].\n
We would like to thank you for taking the time to report this risk.\n
The Datix administrators.";

$EmailText["Notify"]["Subject"] = "Datix Risk Report Number $data[recordid]";

$EmailText["Notify"]["Body"] = "A risk report has been submitted via DatixWeb.

The details are:

Form number: $data[recordid]

Description:

$data[ram_description]

Please go to $scripturl?action=risk&recordid=$data[recordid] to view it.
";

$EmailText["Feedback"]["Subject"] = "DatixWeb feedback message";

$message = "This is a feedback message from $_SESSION[fullname]. The "._tk("RAMNameTitle")." reference is ";
$message .= $data['recordid'];
$message .= ".\n\nThe feedback is: \n\n\n";
$message .= "Please go to ".$scripturl."?action=risk&recordid=".$data['recordid']." to view it.";

$EmailText["Feedback"]["Body"] = $message;

?>