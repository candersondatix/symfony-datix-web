<?php
/**
* Creates the HTML for the popup dialogue which allows you to choose options for the Risk Grade Tracker report.
* 
* @global string $scripturl
* @global array  $txt
* 
* @param  string $_GET['recordid'] The Risk record ID.
*/
function getOptionsHtml()
{
    global $scripturl, $txt;

    require_once 'Source/libs/FormClasses.php';
    $startDate = new FormField();
    $endDate   = new FormField();
    
    $startDate->MakeDateField('startdate');
    $endDate->MakeDateField('enddate');
    
    $html = '
        <form name="riskGradeTrackerForm" method="POST" action="'.$scripturl.'?action=riskTrackerReport&token='.CSRFGuard::getCurrentToken().'">
            <div style="width:300px; height:30px; clear:both">
                <div style="float:left; font-weight:bold; margin-top:3px; width: 100px;">'._tk('report_on').'</div>
                <div style="float:left">
                    <label for="grade">'.$txt['level'].'</label><input type="radio" id="level" name="reporton" value="level" checked="checked" />
                    <label for="rating">'.$txt['rating'].'</label><input type="radio" id="rating" name="reporton" value="rating" />
                </div>
            </div>
            <div style="width:300px; height:30px; clear:both">
                <div style="float:left; font-weight:bold; margin-top:3px; width: 100px;">'._tk('start_date_colon').'</div>
                <div style="float:left">
                    '.$startDate->GetField().'    
                </div>
            </div>
            <div style="width:300px; height:30px; clear:both">
                <div style="float:left; font-weight:bold; margin-top:3px; width: 100px;">'._tk('end_date_colon').'</div>
                <div style="float:left">
                    '.$endDate->GetField().'    
                </div>
            </div>
            <input type="hidden" name="recordid" value="'.Sanitize::SanitizeInt($_GET['recordid']).'" />
            <input type="hidden" id="screenwidth" name="screenwidth" />
            <input type="hidden" id="screenheight" name="screenheight" />
            <input type="hidden" name="backbuttontext" value="'.$txt['btn_back_to_record'].'" />
        </form>';
        
    echo $html;   
}
