<?php

$list_columns_standard = array(
    "recordid" => array(
        'width' => '4'),
    "ram_ourref" => array(
        'width' => '6'),
    "ram_handler" => array(
        'width' => '4'),
    "ram_name" => array(
        'width' => '15'),
    "ram_dcreated" => array(
        'width' => '7'),
    "ram_risk_type" => array(
        'width' => '10'),
    "ram_cur_level" => array(
        'width' => '6'),
    "ram_after_level" => array(
        'width' => '6'),
);

?>