<?php

$GLOBALS['FormTitle'] = 'Datix Risk Form';

$GLOBALS["MandatoryFields"] = array(
	"ram_name" => "header",
	"ram_handler" => "header"
);

$GLOBALS["HideFields"] = array (
    'progress_notes' => true,
    'rejection_history' => true,
    'action_chains' => true,
    'ram_last_updated' => true
);

$GLOBALS["ExpandSections"] = array (
  'rep_approved' =>
  array (
    0 =>
    array (
      'section' => 'rejection',
      'alerttext' => _tk('alert_details_rejection'),
      'values' =>
      array (
        0 => 'REJECT',
      ),
    ),
  ),
);

$GLOBALS["UserExtraText"] = array (
    'ram_description' => 'Enter facts, not opinions.  Do not enter names of people',
    'dum_fbk_to' => 'Only staff and contacts with e-mail addresses are shown.',
    'dum_fbk_gab' => 'Only users with e-mail addresses are shown.',
    'dum_fbk_email' => 'Enter e-mail addresses of other recipients not listed above. You can<br />enter multiple addresses, separated by commas.'
);

$GLOBALS["NewPanels"] = array (
  'contacts_type_N' => true,
  'ass_ram' => true,
  'feedback' => true,
  'linked_records' => true,
  'documents' => true,
  'linked_actions' => true,
  'notepad' => true,
  'progress_notes' => true,
  'linked_standards' => true,
  'history' => true,
  'word' => true,
  'action_chains' => true,
);
?>
