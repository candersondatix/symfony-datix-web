<?php

$FormArray = array(
    "Parameters" => array(
        "Panels" => true,
        "Condition" => false
    ),
    "header" => array(
        "Title" => "Risk Description",
        "Rows" => array(
            array(
                'Name' => 'recordid',
                'Condition' => ($ram['recordid'] || $FormType == 'Design' || $FormType == 'Search')
            ),
            "ram_name",
            'ram_ourref',
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'RAM',
                'perms' => $RISKPerms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $ram,
                'module' => 'RAM',
                'perms' => $RISKPerms,
                'approveobj' => $ApproveObj
            )),
            "ram_handler",
            "ram_responsible",
            "ram_description",
            "ram_synopsis",
            ['Name' => 'ram_last_updated', 'ReadOnly' => true]
        )
    ),
    "dates" => array(
        "Title" => "Key Dates",
        "Rows" => array(
            "ram_dcreated",
            "ram_dreview",
            "ram_dclosed"
        )
    ),
    "coding" => array(
        "Title" => "Risk Coding",
        "Rows" => array(
            "ram_risk_type",
            "ram_risk_subtype"
        )
    ),
    "location" => array(
        "Title" => "Risk Location",
        "Rows" => array(
            "ram_organisation",
            "ram_unit",
            "ram_clingroup",
            "ram_directorate",
            "ram_specialty",
            "ram_location",
            "ram_locactual"
        )
    ),
    "grading" => array(
        "Title" => "Risk Grading",
        'Condition' => !bYN(GetParm('RAM_RATE_CONTROLS', 'N')),
        "Rows" => array(
            "dum_ram_initial",
            "dum_ram_current",
            "dum_ram_residual",
            "ram_adeq_controls"
        )
    ),
    'ram_control_grading' => array(
        'Title' => 'Risk rating',
        'Condition' => (bYN(GetParm('RAM_RATE_CONTROLS', 'N'))),
        'Rows' => array(
            'ram_cur_conseq',
            'ram_cur_likeli',
            'ram_adeq_controls',
            'ram_cur_rating',
            'ram_cur_level'
        )
    ),
    "orphanedudfs" => array(
        "Title" => "Other Extra Fields",
        "NotModes" => array('New', 'Search'),
        "Special" => "OrphanedUDFSection",
        'Condition' => ($FormMode == 'Design' || ($FormMode != 'Search' && $FormType != 'Search' && $ram['recordid'] && count(Fields_ExtraField::getOrphanedUDFs('RAM', $ram['recordid'], $FormDesign)) > 0)),
        "NoFieldAdditions" => true,
        "Rows" => array()
    ),
    'ass_ram' => array(
        "NoFieldAdditions" => true,
        'Title' => 'Assurance',
        'Rows' => array('ram_objectives')
    ),
    'ass_object' => array(
        'Title' => 'Detailed objectives',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Function' => 'ShowDetailedObjectives',
        'Rows' => array()
    ),
    'ass_ctrl' => array(
        'Title' => 'Controls',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Function' => 'ShowControls',
        'Rows' => array()
    ),
    'ass_gctrl' => array(
        'Title' => 'Gaps in controls',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Function' => 'ShowGapsInControls',
        'Rows' => array()
    ),
    'ass_assure' => array(
        'Title' => 'Assurances',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Function' => 'ShowAssurances',
        'Rows' => array()
    ),
    'ass_gapass' => array(
        'Title' => 'Gaps in assurances',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Function' => 'ShowGapsInAssurances',
        'Rows' => array()
    ),
    "feedback" => array(
        "Title" => _tk('feedback_title'),
        "Special" => "Feedback",
        "NoFieldAdditions" => true,
        "NotModes" => array("New","Search"),
        'NoFieldRemoval' => true,
        'NoReadOnly' => true,
        "Rows" => [
            [
                'Name'        => 'dum_fbk_to',
                'Title'       => 'Staff and contacts attached to this record',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_gab',
                'Title'       => 'All users',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_email',
                'Title'       => 'Additional recipients',
                'NoReadOnly'  => true,
                'NoMandatory' => true
            ],
            [
                'Name'        => 'dum_fbk_subject',
                'Title'       => 'Subject',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ],
            [
                'Name'        => 'dum_fbk_body',
                'Title'       => 'Body of message',
                'NoReadOnly'  => true,
                'NoMandatory' => true,
                'NoHide'      => true,
                'NoOrder'     => true
            ]
        ]
    ),
    'linked_records' => array(
        'Title' => 'Linked Records',
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Special' => 'LinkedRecords',
        'Rows' => array()
    ),
    "documents" => array(
        "Title" => "Documents",
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')),
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        "Rows" => array()
    ),
    "linked_actions" => array(
        "Title" => "Actions",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'LinkedForms' => array('linked_actions' => array('module' => 'ACT')),
        'Special' => 'LinkedActions',
        "Rows" => array()
    ),
    "action_chains" => array(
        "Title" => "Action chains",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'getActionChains' => [
                'controller' => 'src\\actionchains\\controllers\\ActionChainController'
            ]
        ],
        'LinkedForms' => array('action_chains' => array('module' => 'ACT')),
        "NotModes" => array("New","Search")
    ),
    'notepad' => array(
        "Title" => "Notepad",
        'NotModes' => array('Search'),
        "Rows" => array('notes')
    ),
    'progress_notes' => array(
        'Title' => 'Progress notes',
        'NotModes' => array('New','Search'),
        'Special' => 'ProgressNotes',
        "NoFieldAdditions" => true,
        'Rows' => array()
    ),
    "linked_standards" => array(
        "Title" => "Standards",
        "NoFieldAdditions" => true,
        'NotModes' => array('New','Search'),
        'Function' => 'ListStandards',
        "Rows" => array()
    ),
    "history" => array(
        "Title" => "Notifications",
        "NoFieldAdditions" => true,
        'ControllerAction' => [
            'MakeEmailHistoryPanel' => [
                'controller' => 'src\\email\\controllers\\EmailHistoryController'
            ]
        ],
        "NotModes" => array("New","Search"),
        "Rows" => array()
    ),
    "word" => array(
        "Title" => _tk('mod_templates_title'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'wordmergesection' => array(
                'controller' => 'src\\wordmergetemplate\\controllers\\WordMergeTemplateController'
            )
        ),
        "Condition" => (bYN(GetParm('WORD_MERGE_WEB', 'N'))),
        "NotModes" => array('New','Search','Print'),
        "Rows" => array()
    ),
    "rejection" => GenericRejectionArray('RAM', $ram),
    "rejection_history" => array(
        "Title" => _tk('reasons_history_title'),
        "NoFieldAdditions" => true,
        'ControllerAction' => array(
            'SectionRejectionHistory' => array(
                'controller' => 'src\\reasons\\controllers\\ReasonsController'
            )
        ),
        "NotModes" => array("New","Search"),
        "Condition" =>  (bYN(GetParm("REJECT_REASON",'Y'))),
        "Rows" => array()
    ),
);

foreach ($ModuleDefs['RAM']['CONTACTTYPES'] as $ContactTypeDetails)
{
    $ContactArray['contacts_type_'.$ContactTypeDetails['Type']] = array(
        'Title' => $ContactTypeDetails['Plural'],
        'NoFieldAdditions' => true,
        'ControllerAction' => array(
            'ListLinkedContacts' => array(
                'controller' => 'src\\contacts\\controllers\\ContactsController'
            )
        ),
        'ExtraParameters' => array('link_type' => $ContactTypeDetails['Type']),
        'NotModes' => array('New','Search'),
        'LinkedForms' => array($ContactTypeDetails['Type'] => array('module' => 'CON')),
        'Listings' => array('contacts_type_'.$ContactTypeDetails['Type'] => array('module' => 'CON')),
        'Rows' => array()
    );
}

array_insert_datix($FormArray, 'feedback', $ContactArray);

require_once 'Source/libs/ModuleLinks.php';

foreach (getModLinksArray() as $link_mod => $name)
{
    $FormArray['linked_records']['Listings'][$link_mod] = array(
        'module' => $link_mod,
        'Label' => 'Select a listing to use for the list of '.$ModuleDefs[$link_mod]['REC_NAME_PLURAL']
    );
}

if (GetParm('WORD_MERGE_ENGINE', 'OLD') == 'OLD')
{
    unset($FormArray['word']['ControllerAction']);
    $FormArray['word']['Special'] = 'SectionWordMerge';
}