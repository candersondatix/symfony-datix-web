<?php

$GLOBALS['FormTitle'] = 'Datix Risk Form';

$GLOBALS["MandatoryFields"] = array(
	"ram_name" => "header",
	"ram_handler" => "header"
);
$GLOBALS["DefaultValues"] = array (
    "ram_dcreated" => 'TODAY',
    'rep_approved' => 'AWAREV',
);
$GLOBALS["HideFields"] = array (
    "rep_approved" => true,
    "extra_document" => true,

);
$GLOBALS["UserExtraText"] = array (
  'ram_description' => 'Enter facts, not opinions.  Do not enter names of people',
);

$GLOBALS["ExpandSections"] = array (
  'show_contacts_RAM' =>
  array (
    0 =>
    array (
      'section' => 'contacts_type_N',
      'alerttext' => '',
      'values' =>
      array (
        0 => 'Y',
      ),
    ),
  ),
);

?>
