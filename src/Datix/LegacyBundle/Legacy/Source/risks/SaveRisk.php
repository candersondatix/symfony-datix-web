<?php

function ValidateRiskData()
{
    // This is "failover" validation as, in order to get this far,
    // the risk must have passed the JavaScript mandatory
    // field validation.

    require_once 'Date.php';

    $Today = new Date;

    //Date risk record was created cannot be later than today.
    if ($_POST["ram_dcreated"] != "")
    {
        $RiskDate = new Date(UserDateToSQLDate($_POST["ram_dcreated"]));

        if ($Today->Compare($RiskDate, $Today) == 1)
        {
            $error["Validation"]["ram_dcreated"] = 'Date cannot be later than today';
        }
    }

    //Date risk record was closed cannot be later than today.
    if ($_POST["ram_dclosed"] != "")
    {
        $RiskDate = new Date(UserDateToSQLDate($_POST["ram_dclosed"]));

        if ($Today->Compare($RiskDate, $Today) == 1)
        {
            $error["Validation"]["ram_dclosed"] = 'Date cannot be later than today';
        }
    }

    //Date risk record was closed cannot be before date it was opened
    if ($_POST["ram_dclosed"] != "" && $_POST["ram_dcreated"]!="")
    {
        $RiskOpenedDate = new Date(UserDateToSQLDate($_POST["ram_dcreated"]));
        $RiskClosedDate = new Date(UserDateToSQLDate($_POST["ram_dclosed"]));

        if ($Today->Compare($RiskClosedDate, $RiskOpenedDate) == -1)
        {
            $error["Validation"]["ram_dclosed"] = 'Date cannot be earlier than ' . GetFieldLabel("ram_dcreated", "Opened date");
        }
    }

    return $error;
}

function getRiskFieldArray()
{
    $FieldArray = array(
        "ram_name", "ram_risk_type", "ram_risk_subtype", "ram_organisation", "ram_unit", "ram_clingroup",
        "ram_directorate", "ram_specialty", "ram_assurance", "ram_location", "ram_locactual", "ram_objectives",
        "ram_handler", "ram_responsible", "ram_description", "ram_synopsis", "ram_consequence", "ram_likelihood",
        "ram_level", "ram_after_conseq", "ram_after_likeli", "ram_after_level", "ram_cur_conseq", "ram_cur_likeli",
        "ram_cur_level", "ram_dreview", "ram_ourref", "ram_dcreated", "ram_dclosed", "ram_dreported",
        "ram_rating", "ram_after_rating", "ram_cur_rating", "rep_approved",
        'ram_adeq_controls', 'show_document');

    return $FieldArray;
}

// Not currently called?
function getRiskCostBenefit()
{
    $Rating = $_POST["ram_rating"];
    $CurRating = $_POST["ram_cur_rating"];
    $AfterRating = $_POST["ram_after_rating"];

    if ($AfterRating != "" && $Rating != "")
    {
        $Ram_reduction = $AfterRating - $Rating;

        if ($Ram_reduction != 0 && $Ram_cost != "")
        {
            $Ram_cb = $Ram_cost / $Ram_reduction;
        }
    }

    return $Ram_cb;
}

// Used for 5 + 5 + 5 matrix (i.e. Salford Royal).
// Parameters:
// $Consequence:    Consequence code
// $Likelihood:     Likelihood code
// $Controls:       "Adequacy of controls" code
// Returns: associative array of ram_cur_rating and ram_cur_level
function CalcRatingControlsValues($Consequence, $Likelihood, $Controls)
{
    if ($Consequence == '' || $Likelihood == '' || $Controls == '')
    {
        return '';
    }

    $ConsequenceValue = DatixDBQuery::PDO_fetch('SELECT listorder FROM code_ra_conseq WHERE code = :code', array('code' => $Consequence), PDO::FETCH_COLUMN);
    $LikelihoodValue = DatixDBQuery::PDO_fetch('SELECT listorder FROM code_ra_likeli WHERE code = :code', array('code' => $Likelihood), PDO::FETCH_COLUMN);
    $ControlsValue = DatixDBQuery::PDO_fetch('SELECT cod_listorder FROM code_types WHERE cod_code = :code AND cod_type = \'RAMADC\'', array('code' => $Controls), PDO::FETCH_COLUMN);

    $RamCurRating = $ConsequenceValue + $LikelihoodValue + $ControlsValue;

    $LevelValue = DatixDBQuery::PDO_fetch('SELECT code FROM code_ra_levels WHERE lower_rating <= :lower_rating AND upper_rating >= :upper_rating', array('lower_rating' => $RamCurRating, 'upper_rating' => $RamCurRating), PDO::FETCH_COLUMN);

    return array('ram_cur_rating' => $RamCurRating, 'ram_cur_level' => $LevelValue);
}

function AuditRating($AuditRatingType, $recordid)
{
    $AuditDate = date('Y-m-d H:i:s');

    switch ($AuditRatingType)
    {
        case "I":
            $SelectList = 'ram_consequence,ram_likelihood,ram_rating,ram_level';
            break;
        case "C":
            $SelectList = 'ram_cur_conseq,ram_cur_likeli,ram_cur_rating,ram_cur_level';
            break;
        case "T":
            $SelectList = 'ram_after_conseq,ram_after_likeli,ram_after_rating,ram_after_level';
            break;
    }

    $FieldValueArray = explode(',', $SelectList);

    // If the $FieldValueArray is set, use this to determine which fields to
    // audit.  This is so we can audit forms where two tables are on the same
    // page, e.g. linked contacts.
    if ($FieldValueArray)
    {
        foreach ($FieldValueArray as $i => $FieldName)
        {
            if ($_POST["CHANGED-$FieldName"] != '')
            {
                $ChangedArray[] = $FieldName;
            }
        }
    }

    // Now select old values from main table
    if ($ChangedArray)
    {
        $sql = "SELECT $SelectList
            FROM ra_main
            WHERE recordid = :recordid";

        $row = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

        switch ($AuditRatingType)
        {
            case "I":
                $AuditDetail = EscapeQuotes('INITIAL: Consequence: ' . $row["ram_consequence"] . '; Likelihood: ' . $row["ram_likelihood"] . '; Rating: ' . $row["ram_rating"] . '; Level: ' . $row["ram_level"]);
                break;
            case "C":
                $AuditDetail = EscapeQuotes('CURRENT: Consequence: ' . $row["ram_cur_conseq"] . '; Likelihood: ' . $row["ram_after_likeli"] . '; Rating: ' . $row["ram_cur_rating"] . '; Level: ' . $row["ram_cur_level"]);
                break;
            case "T":
                $AuditDetail = EscapeQuotes('TARGET: Consequence: ' . $row["ram_after_conseq"] . '; Likelihood: ' . $row["ram_after_likeli"] . '; Rating: ' . $row["ram_after_rating"] . '; Level: ' . $row["ram_after_level"]);
                break;
        }

        DatixDBQuery::PDO_build_and_insert('full_audit',
            array(
                'aud_module' => 'RAM',
                'aud_record' => $recordid,
                'aud_login' => $_SESSION['initials'],
                'aud_date' => $AuditDate,
                'aud_action' => 'RATINGSAUDIT',
                'aud_detail' => $AuditDetail,
            )
        );
    }
}