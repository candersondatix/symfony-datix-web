<?php

function ApplyAssuranceItems($ram_new)
{
    global $scripturl, $yySetLocation;

    // Look for items to delete
    if (is_array($ram_new))
    {
        foreach ($ram_new as $pkey => $pval)
        {
            // Delete existing, ticked records
            $pos = \UnicodeString::strpos($pkey, "assurance_include");

            if ($pos === false){}
            else
            {
                $index = \UnicodeString::substr($pkey, 18);

                if ($index && $ram_new["insId_".$index])
                {
                    $deletethisitem[] = $ram_new["insId_".$index];
                }
                elseif ($index)
                {
                    $donotsavethis[] = $index;
                }
            }

            // Delete existing but now blanked, not ticked records
            $pos = \UnicodeString::strpos($pkey, "insId");

            if ($pos === false){}
            else
            {
                $index = \UnicodeString::substr($pkey, 6);

                if ($index && $ram_new["insId_".$index] && !$ram_new["insText_".$index])
                {
                    $deletethisitem[] = $ram_new["insId_".$index];
                }
            }
        }
    }

    // Delete marked items
    if (is_array($deletethisitem))
    {
        $sql = 'DELETE FROM assurance
                WHERE recordid in (?'.str_repeat(',?', count($deletethisitem)-1).')';

        if (!DatixDBQuery::PDO_query($sql, $deletethisitem))
        {
            fatal_error("Could not delete assurance item <br />" . $sql);
        }
    }

    // Collect items to be saved
    if (is_array($ram_new))
    {
        foreach ($ram_new as $pkey => $pval)
        {
            $pos = \UnicodeString::strpos($pkey, "insText");

            if ($pos === false) {}
            else
            {
                $index = \UnicodeString::substr($pkey, 8);

                if ($index)
                {
                    if (!is_array($deletethisitem))
                    {
                        $deletethisitem = array();
                    }

                    if (!is_array($donotsavethis))
                    {
                        $donotsavethis = array();
                    }

                    if (!in_array($index, $deletethisitem) && !in_array($index, $donotsavethis) && $ram_new["insText_".$index])
                    {
                        $savethisitem[] = $index;
                    }
                }
            }
        }
    }

    $ass_type = \UnicodeString::strtoupper(Sanitize::SanitizeString($_POST["ass_type"]));

    // Save records
    if (is_array($savethisitem))
    {
        foreach ($savethisitem as $saveindex)
        {
            if (!$ram_new["insId_".$saveindex])
            {
                $recordid = GetNextRecordID("assurance", true);
            }
            else
            {
                $recordid = $ram_new["insId_".$saveindex];
            }

            $insText = $ram_new["insText_".$saveindex];
            $insSource = $ram_new["insSource_".$saveindex];
            $insOrder = intval($ram_new["insOrder_".$saveindex]);

            $sql = "UPDATE assurance SET
                    ass_link_id = :ass_link_id,
                    ass_text = :ass_text,
                    ass_type = :ass_type,
                    ass_listorder = :ass_listorder,
                    ass_module = :ass_module,
                    ass_source = :ass_source
                    WHERE recordid = :recordid";

            $result = DatixDBQuery::PDO_query($sql,
                array(
                    'ass_link_id' => $ram_new['ram_id'],
                    'ass_text' => $insText,
                    'ass_type' => $ass_type,
                    'ass_listorder' => ($insOrder == 0 ? null : $insOrder),
                    'ass_module' => 'RAM',
                    'ass_source' => $insSource,
                    'recordid' => $recordid,
                ));

            if (!$result)
            {
                success_message("An error has occurred when trying to update the assurance records.  Please report the following to the Datix administrator: $sql");
            }
        }
    }

    // Update record last updated
    $loader = new \src\framework\controller\Loader();
    $controller = $loader->getController(['controller' => 'src\\generic\\controllers\\RecordController']);
    $controller->setRequestParameter('module', 'RAM');
    $controller->setRequestParameter('recordId', $ram_new['ram_id']);
    $controller->doAction('updateRecordLastUpdated');

    $yySetLocation = "$scripturl?action=editassurance&ram_id=$ram_new[ram_id]&table=main&type=$ass_type&savemessage=1";
    redirectexit();
}

function SavePrincipalObjectives($ram_new)
{
    global $scripturl, $yySetLocation;

    $ass_type = \UnicodeString::strtoupper($_POST["ass_type"]);

    if (is_array($ram_new) && $ass_type == "PRINOBJECT")
    {
        if ($ram_new["ram_objectives"])
        {
            $ram_objects = implode(" ", $ram_new["ram_objectives"]);
        }
        else
        {
            $ram_objects = "";
        }

        $sql_main = 'UPDATE ra_main SET
                     ram_objectives = :ram_objectives
                     WHERE recordid = :recordid';

        $result = DatixDBQuery::PDO_query($sql_main, array('ram_objectives' => $ram_objects, 'recordid' => $ram_new['ram_id']));

        if (!$result)
        {
            $error = "An error has occurred when trying to save the risk.  Please report the following to the Datix administrator: $sql";
        }
        elseif (affected_rows($result) == 0)
        {
            $error = _tk('cannot_save_err');
        }
    }

    $yySetLocation = "$scripturl?action=risk&recordid=$ram_new[ram_id]&table=main&panel=ass_ram&savemessage=4";
    redirectexit();
}

?>