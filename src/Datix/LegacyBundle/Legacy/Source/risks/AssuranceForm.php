<?php

use src\security\Escaper;

function ShowAssuranceForm($ass_row)
{
    global $scripturl, $dtxdebug, $FormType, $ModuleDefs, $UserLabels, $JSFunctions;

    LoggedIn();
    $RISKPerms = GetParm("RISK_PERMS");
    $dtxtitle = 'Risks - Assurance Framework details';

    GetPageTitleHTML(array(
         'title' => $dtxtitle,
         'module' => 'RAM'
         ));

    GetSideMenuHTML(array('module' => 'RAM'));

	template_header_nopadding();

    $a_type = \UnicodeString::strtoupper(Sanitize::SanitizeString($_GET["type"]));

    // Load RISK2 form settings
    IncludeCurrentFormDesign("RAM", 2);

    $a_section = $ModuleDefs['RAM']['ASSURANCE'][$a_type]['section'];
    $a_title = ($UserLabels[$a_section] ? $UserLabels[$a_section] : $ModuleDefs['RAM']['ASSURANCE'][$a_type]['title']);

?>
<script language="javascript" type="text/javascript">

    var jsrownum;

    function addTblRow(a_type, rownum)
    {
        var tbl = document.getElementById('tblobject').tBodies[0];
        var lastRow = tbl.rows.length;
        var newRow = tbl.insertRow(lastRow);
        var htmlrow;
        if(!jsrownum)
            jsrownum = rownum;
        else jsrownum++;
        //oCell = newRow.insertCell(0);
        //oCell.innerHTML = ' <img src="Images/Error_24.png" align="middle" hspace="5" border="0" width="14" height="14" id="delRow" onclick="javascript: delTblRow(this);" />';
        //oCell.innerHTML = "";
        if(a_type == "ASSURE" || a_type == "GAPASS")
        {
            <?php
            $FieldObj = Forms_SelectFieldFactory::createSelectField('ass_source', 'RAM', '', $FormType);
            $FieldObj->setAltFieldName('insSource_x');
            $phpstring = '<div><img src="Images/spacer.png" style="width:230px; height:1px;"></div>'.$FieldObj->GetField();
            $phpstring = str_replace("'", "\'", $phpstring);
            $phpstring = str_replace("\n\r", "", $phpstring);
            $phpstring = str_replace("\n", "", $phpstring);
            ?>
            oCell = newRow.insertCell(0);
            oCell.className = "windowbg2";
            var ihtml = '<?=$phpstring?>';
            ihtml = ihtml.replace(/insSource_x/g, 'insSource_'+jsrownum);
            oCell.innerHTML = ihtml;
            <?php
            // need to add javascript to build the field here too
                foreach ($JSFunctions as $script)
                {
                    if (\UnicodeString::strpos($script, 'insSource_x') !== false)
                    {
                        $script = str_replace("'", "\'", $script);
                        $script = str_replace("\n\r", "", $script);
                        $scriptStr = str_replace("\n", "", $script);
                        break;
                    }
                }
            ?>
            var extraJS = '<?=$scriptStr?>';
            extraJS = extraJS.replace(/insSource_x/g, 'insSource_'+jsrownum);
            eval(extraJS);
        }
        if(a_type == "ASSURE" || a_type == "GAPASS")
            oCell = newRow.insertCell(1);
        else
        {
            oCell = newRow.insertCell(0);
            oCell.colSpan = "2";
        }

        oCell.className = "windowbg2";
        oCell.innerHTML = '<input type="text" id="insText" name="insText_'+jsrownum+'" size="80" value=""/><input type="hidden" id="insId" name="insId_'+jsrownum+'" value=""/>';

        if(a_type == "ASSURE" || a_type == "GAPASS")
            oCell = newRow.insertCell(2);
        else
            oCell = newRow.insertCell(1);

        oCell.className = "windowbg2";
        oCell.innerHTML = '<input type="text" id="insOrder" name="insOrder_'+jsrownum+'" size="5" value="" />';
        if(a_type == "ASSURE" || a_type == "GAPASS")
            oCell = newRow.insertCell(3);
        else
            oCell = newRow.insertCell(2);
        oCell.className = "windowbg2";
        oCell.style.width = "3%";
        oCell.align = "center";
        oCell.innerHTML = '<input type="checkbox" id="assurance_include" name="assurance_include_'+jsrownum+'" />';
    }

    function delTblRow(src)
    {
        var oRow = src.parentNode.parentNode;
        if (document.all)
        {
            document.all("tblobject").deleteRow(oRow.rowIndex);
        }
        else if (document.getElementsByTagName)
        {
            document.getElementById("tblobject").deleteRow(oRow.rowIndex);
        }
    }


</script>

<?php

    echo '
<form method="post" name="frmAssurance" action="' . $scripturl .
        '?action=saveassurance">
<input type="hidden" name="rbWhat" id="rbWhat" value="SaveChanges" />
<input type="hidden" name="ram_id" id="ram_id" value="'. Sanitize::SanitizeInt($_GET['ram_id']) . '" />
<input type="hidden" name="ass_type" id="ass_type" value="' . Escaper::escapeForHTMLParameter($_GET['type']) . '" />
            <table id="tblobject" width="100%" class="gridlines" cellpadding="4" cellspacing="1">
            <tr>
    	        <td class="titlebg" colspan="5" width="100%"><b>'.Escape::EscapeEntities($a_title).'</b></td>
            </tr>';

            $savemessage = $_GET["savemessage"];
            if($savemessage == 1)
                echo'
                <tr>
                    <td class="windowbg" colspan="5"><font color="red"><b>Changes have been applied.</b></font></td>
                </tr>';


    if($a_type == "PRINOBJECT")
    {
        $Table = new FormTable($FormType, 'RAM');
        $Table->MakeRow('<b>Principal objectives:</b>',
                Forms_SelectFieldFactory::createSelectField('ram_objectives', 'RAM', $ass_row['ram_obj'], $FormType, true));
        $Table->MakeTable();
        echo '</table>
        <table width="100%" class="titlebg" cellpadding="0" cellspacing="0">
        <tr><td class="windowbg2" colspan="4">' . $Table->GetFormTable() . '
        </td></tr></table>
        <table class="gridlines" cellspacing="0" cellpadding="4" width="100%" align="center" border="0">';
    }

    if($alabel["type"] == "OBJECT")
    {
        echo '
        <table id="tblbject" class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';
    }

    if($a_type != "PRINOBJECT")
    {
        $rownum = 1;
        if (!$ass_row[$a_type] && $alabel["type"] != "PRINOBJECT"){
            echo '
            <tr name="'.$a_section.'_row" id="'.$a_section.'_row">
                <td class="windowbg2" colspan="5" width="100%">
                <b>No '.$a_title.'.</b>
                </td>
            </tr>
            <tr>';
                if($a_type == "ASSURE" || $a_type == "GAPASS")
                    echo '<td class="windowbg" width="20%"><b>Source</b></td>';
                if($a_type == "ASSURE" || $a_type == "GAPASS")
                    echo '<td class="windowbg">';
                else
                    echo '<td class="windowbg" colspan="2">';
                echo '
                <b>'.$a_title.'</b></td>
                <td class="windowbg" width="3%"><b>Order</b></td>
                <td class="windowbg" width="3%"><b>Delete?</b></td>
            </tr>';
        }
        else
        {
            echo '
            <tr>';
            if($a_type == "ASSURE" || $a_type == "GAPASS")
                echo '<td class="windowbg" width="20%"><b>Source</b></td>';
            if($a_type == "ASSURE" || $a_type == "GAPASS")
                echo '<td class="windowbg">';
            else
                echo '<td class="windowbg" colspan="2">';
            echo'
            <b>'.$a_title.'</b></td>
            <td class="windowbg" width="5%"><b>Order</b></td>
            <td class="windowbg" width="3%">
            <b>Delete?</b>
            </td>
        </tr>';
            if(is_array($ass_row[$a_type]))
            {
                foreach ($ass_row[$a_type] as $ass)
                {
                    echo'
                    <tr>';
                    if($a_type == "ASSURE" || $a_type == "GAPASS")
                    {
                        $FieldObj = Forms_SelectFieldFactory::createSelectField('ass_source', 'RAM', $ass['ass_source'], $FormType);
                        $FieldObj->setAltFieldName('insSource_'.$rownum);
                        echo '<td class="windowbg2"><div><img src="Images/spacer.png" style="width:230px; height:1px;"></div>'.$FieldObj->GetField().'</td>';
                    }
                    if($a_type == "ASSURE" || $a_type == "GAPASS")
                        echo '<td class="windowbg2">';
                    else
                        echo '<td class="windowbg2" colspan="2">';
                    echo '
                        <input type="text" id="insText" name="insText_'.$rownum.'" size="60" value="'.htmlspecialchars($ass["ass_text"]).'"/>
                        <input type="hidden" id="insId" name="insId_'.$rownum.'" value="'.$ass["recordid"].'"/>
                        </td>
                        <td class="windowbg2">
                        <input type="text" id="insOrder" name="insOrder_'.$rownum.'" size="5" value="'.$ass["ass_listorder"].'" />
                        </td>
                        <td class="windowbg2" width="3%" align="center">
                        <input type="checkbox" id="assurance_include" name="assurance_include_'.$rownum.'" />
                        </td>
                    </tr>';
                    $rownum++;
                }
            }
        }
    }

    echo '
</table>

<div class="button_wrapper">
';

    if ($a_type != "PRINOBJECT")
    {
        echo '
            <input type="button" value="Add" class="button" onclick="javascript:addTblRow(\''.Escape::EscapeEntities($a_type).'\', '.$rownum.');" id="btnAdd" name="btnAdd" />
            <input type="submit" value="Apply changes" onclick="Javascript:document.frmAssurance.rbWhat.value = \'ApplyChanges\';submitClicked=true;" class="button" id="btnSaveApprove" name="btnSaveApprove" />
        ';
    }
    elseif ($a_type == "PRINOBJECT")
    {
        echo '
            <input type="submit" value="Save changes" onclick="Javascript:selectAllMultiple(document.getElementById(\'ram_objectives\'));document.frmAssurance.rbWhat.value = \'SaveChanges\';submitClicked=true;" class="button" id="btnSaveApprove" name="btnSaveApprove" />';
    }
    echo '
            <input type="button" value="'._tk('back_to_assurance_framework').'" class="button" id="btnAssuranceBack" name="btnAssuranceBack"';?>onclick="SendTo('<?= "$scripturl?action=risk&amp;recordid=" . Sanitize::SanitizeInt($_GET['ram_id']). "&amp;table=" . Sanitize::SanitizeString($_GET['table']) . "&amp;panel=ass_ram"?>');" <?php echo '/>
       ';


echo '

</td>
</tr>
</table>


</form>';

    footer();
    obExit();
}

?>
