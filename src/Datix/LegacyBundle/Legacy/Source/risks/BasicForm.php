<?php

// If the user is logged in from the contacts table, they are the reporter of the incident.
if ($_SESSION["contact_login"] && $ram["ram_repname"] == "")
{
    $ram["ram_repname"] = $_SESSION["fullname"];
}

$FormArray = array(
    "header" => array(
        "Title" => "Risk Description",
        "Rows" => array(
            array(
                'Name' => 'recordid',
                'Condition' => ($ram['recordid'] || $FormType == 'Design')
            ),
            "ram_name",
            RepApprovedDisplayBasicFormArray(array(
                'formtype' => $FormType,
                'module' => 'RAM',
                'perms' => $RISKPerms,
                'currentapproveobj' => $CurrentApproveObj
            )),
            RepApprovedBasicFormArray(array(
                'formtype' => $FormType,
                'data' => $ram,
                'module' => 'RAM',
                'perms' => $RISKPerms,
                'approveobj' => $ApproveObj
            )),
            "ram_handler",
            "ram_description",
            "ram_synopsis"
        )
    ),
    'ass_ram' => array(
        'Title' => 'Assurance',
        "NoFieldAdditions" => true,
        'Rows' => array('ram_objectives')
    ),
    "dates" => array(
        "Title" => "Key Dates",
        "Rows" => array(
            "ram_dcreated",
            "ram_dreview",
            "ram_dclosed"
        )
    ),
    "coding" => array(
        "Title" => "Risk Coding",
        "Rows" => array(
            "ram_risk_type",
            "ram_risk_subtype"
        )
    ),
    "location" => array(
        "Title" => "Risk Location",
        "Rows" => array(
            "ram_organisation",
            "ram_unit",
            "ram_clingroup",
            "ram_directorate",
            "ram_specialty",
            "ram_location",
            "ram_locactual"
        )
    ),
    'grading' => array(
        'Title' => 'Risk rating',
        'Condition' => (!bYN(GetParm('RAM_RATE_CONTROLS', 'N'))),
        "Rows" => array(
            "dum_ram_initial",
            "dum_ram_current",
            "dum_ram_residual",
            "ram_adeq_controls"
        )
    ),
    'ram_control_grading' => array(
        'Title' => 'Risk rating',
        'Condition' => (bYN(GetParm('RAM_RATE_CONTROLS', 'N'))),
        'Rows' => array(
            'ram_cur_conseq',
            'ram_cur_likeli',
            'ram_adeq_controls',
            array(
                'Name' => 'ram_cur_rating',
                'Condition' => $FormMode == 'Design' || $ram['ram_cur_rating'] != ''
            ),
            array(
                'Name' => 'ram_cur_level',
                'Condition' => $FormMode == 'Design' || $ram['ram_cur_level'] != ''
            )
        )
    ),
    "additional" => array(
        "Title" => "Additional Information",
        "Rows" => array(
            "show_contacts_RAM",
            'show_document'
        )
    ),
    "contacts_type_N" => array(
        "Title" => "Contacts",
        "module" => 'RAM',
        "NoFieldAdditions" => true,
        "Special" => 'DynamicContact',
        "contacttype" => 'N',
        'LinkedForms' => array('N' => array('module' => 'CON')),
        "suffix" => 2,
        "Rows" => array()
    ),
    "contacts_type_R" => array(
        "Title" => "Details of person reporting the risk",
        "module" => 'RAM',
        "Special" => 'DynamicContact',
        "contacttype" => 'R',
        'LinkedForms' => array('R' => array('module' => 'CON')),
        "Role" => GetParm('REPORTER_ROLE', 'REP'),
        "suffix" => 1,
        "Rows" => array()
    ),
    "extra_document" => array(
        "Title" => "Documents",
        "NoFieldAdditions" => true,
        "Condition" => bYN(GetParm('WEB_DOCUMENTS', 'N')) && ($FormType != 'Print' && $FormType != 'ReadOnly'),
        "NoReadOnly" => true,
        "Special" => 'DynamicDocument',
        "Rows" => array()
    ),
    'linked_documents' => array(
        'Title' => 'Documents',
        'Condition' => bYN(GetParm('WEB_DOCUMENTS', 'N')) && $FormType == 'Print',
        'ControllerAction' => array(
            'listlinkeddocuments' => array(
                'controller' => 'src\\documents\\controllers\\DocumentController'
            )
        ),
        'Rows' => array()
    )
);

if (is_array($RejectionArray) && bYN(GetParm("REJECT_REASON",'Y')))
{
    $FormArray["rejection"] = array("Title" => "Details of Rejection", "Rows" => $RejectionArray);
}

?>