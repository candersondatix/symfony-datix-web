<?php

/* Section Risk Grading now takes an array as a parameter so that it
 * can be called from FieldDefs when displaying the risk grading section
 * Instead of printing out the table information itself, it now returns
 * the contents of the right hand cell to be used in FormClasses.
 */
function SectionRiskGrading($paramArray)
{
    if(bYN(GetParm('WEB_RISK_MATRIX', 'N')) && $paramArray[1] != 'Search')
    {
        require_once 'Source/libs/RiskRatingBase.php';
        return GetRiskGradingMatrix($paramArray);
    }
    else
    {
        return SectionRiskGradingStandard($paramArray);
    }
}

/**
* Creates the risk grading section of the Risk form when global WEB_RISK_MATRIX = N
*
* @global array    $txt
* @global array    $JSFunctions
* @global boolean  $riskGradingJavascriptSet
*
* @param  string   $paramArray[0]  The form data.
* @param  string   $paramArray[1]  The form mode.
* @param  string   $paramArray[2]  The row of the risk grading section (initial/current/target).
*
* @return string   $section        The section HTML.
*/
function SectionRiskGradingStandard($paramArray)
{
	global $txt, $JSFunctions, $riskGradingJavascriptSet;

    $ram              = $paramArray[0];
    $FormAction       = $paramArray[1];
    $RiskRow          = $paramArray[2];
    $riskMatrix       = bYN(GetParm('RISK_MATRIX', 'N'));
    $ratingField      = new FormField($FormAction);

    switch ($RiskRow)
    {
        case 'initial':
            $consequenceFieldName = 'ram_consequence';
            $likelihoodFieldName  = 'ram_likelihood';
            $ratingFieldName      = 'ram_rating';
            $levelFieldName       = 'ram_level';
            break;
        case 'current':
            $consequenceFieldName = 'ram_cur_conseq';
            $likelihoodFieldName  = 'ram_cur_likeli';
            $ratingFieldName      = 'ram_cur_rating';
            $levelFieldName       = 'ram_cur_level';
            break;
        case 'target':
            $consequenceFieldName = 'ram_after_conseq';
            $likelihoodFieldName  = 'ram_after_likeli';
            $ratingFieldName      = 'ram_after_rating';
            $levelFieldName       = 'ram_after_level';
            break;
    }

    $consequenceFieldTitle = Labels_FieldLabel::GetFieldLabel($consequenceFieldName, $txt["consequence"]);
    $likelihoodFieldTitle  = Labels_FieldLabel::GetFieldLabel($likelihoodFieldName, $txt["likelihood"]);
    $ratingFieldTitle      = Labels_FieldLabel::GetFieldLabel($ratingFieldName, $txt["rating"]);
    $levelFieldTitle       = Labels_FieldLabel::GetFieldLabel($levelFieldName, $txt["level"]);
    $onChange              = $FormAction == 'Search' ? '' : 'setRatingAndLevel(\''.$consequenceFieldName.'\', \''.$likelihoodFieldName.'\', \''.$ratingFieldName.'\', \''.$levelFieldName.'\', false)';

    // build consequence and likelihood fields
    $consequenceField = Forms_SelectFieldFactory::createSelectField($consequenceFieldName, 'RAM', $ram[$consequenceFieldName], $FormAction, false, $consequenceFieldTitle, '', $ram['CHANGED-'.$consequenceFieldName]);
    $consequenceField->setOnChangeExtra($onChange);
    $consequenceField->setSelectFunction('getRamConsequence');
    $likelihoodField = Forms_SelectFieldFactory::createSelectField($likelihoodFieldName, 'RAM', $ram[$likelihoodFieldName], $FormAction, false, $likelihoodFieldTitle, '', $ram['CHANGED-'.$likelihoodFieldName]);
    $likelihoodField->setOnChangeExtra($onChange);
    $likelihoodField->setSelectFunction('getRamLikelihood');

    // build rating and level fields
    $ratingFieldContents = '
                <input type="hidden" name="'.$ratingFieldName.'" id="'.$ratingFieldName.'" value="'.$ram[$ratingFieldName].'" />';
    if ($FormAction == 'Search')
    {
        // display rating and level as standard fields to allow searching
        $ratingField->MakeInputField($ratingFieldName, 10, 254, $ram[$ratingFieldName]);
        $ratingFieldContents .= $ratingField->GetField();
        $levelField = Forms_SelectFieldFactory::createSelectField($levelFieldName, 'RAM', $ram[$levelFieldName], $FormAction, false, $levelFieldTitle, '', $ram['CHANGED-'.$levelFieldName]);
    }
    else
    {
        // rating and level are custom fields whose values are dependent on consequence/likelihood
        $class = ($FormAction == 'ReadOnly' || $FormAction == 'Print') ? 'style="padding:3px"' : 'class="codefield"';

        $ratingFieldContents .= '
            <div '.$class.' id="'.$ratingFieldName.'_Descr">'.code_descr('RAM', $ratingFieldName, $ram[$ratingFieldName]).'</div>';
        $levelFieldContents = '
            <input type="hidden" name="'.$levelFieldName.'" id="'.$levelFieldName.'" value="'.$ram[$levelFieldName].'" />
            <div '.$class.' id="'.$levelFieldName.'_Descr">'.code_descr($module, $levelFieldName, $ram[$levelFieldName]).'</div>';
        $levelField = new FormField($FormAction);
        $levelField->MakeCustomField($levelFieldContents);
    }
    $ratingField->MakeCustomField($ratingFieldContents);

    // build section
    $section = '
    <div style="clear:both">
        <div style="float:left">
            <div style="height:26px">
            ' . $consequenceFieldTitle . ':&nbsp;
            </div>
            <div style="height:26px">
            ' . $likelihoodFieldTitle . ':&nbsp;
            </div>
            <div style="height:26px">
            ' . $ratingFieldTitle . ':&nbsp;
            </div>
            <div style="height:26px">
            ' . $levelFieldTitle . ':&nbsp;
            </div>
        </div>
        <div style="float:left">
            <div style="height:26px">
            ' . $consequenceField->GetField() . '
            </div>
            <div style="height:26px">
            ' . $likelihoodField->GetField() . '
            </div>
            <div style="height:26px">
            ' . $ratingField->GetField() . '
                <input type="hidden" name="CHANGED-'.$ratingFieldName.'" id="CHANGED-'.$ratingFieldName.'" />
            </div>
            <div style="height:26px">
            ' . $levelField->GetField() . '
                <input type="hidden" name="CHANGED-'.$levelFieldName.'" id="CHANGED-'.$levelFieldName.'" />
            </div>
        </div>
    </div>';

    // handle audit
    if ($_GET['full_audit'])
    {
        require_once 'Source/libs/RiskRatingBase.php';
        $section .= GetAuditLine(array(
            'module'    => 'RAM',
            'fieldName' => $ratingFieldName,
            'recordid'  => $ram['recordid'],
            'fields'    => array(
                $consequenceFieldTitle => $consequenceFieldName,
                $likelihoodFieldTitle  => $likelihoodFieldName,
                $ratingFieldTitle      => $ratingFieldName,
                $levelFieldTitle       => $levelFieldName,
            ),
        ));
    }

    // construct javascript used to update the rating and level fields
    if ($FormAction != 'Search')
    {
        if (!$riskGradingJavascriptSet)
        {
            if (bYN(GetParm('RISK_MATRIX', 'N')))
            {
                // use incidents risk grading
                $sql = 'SELECT
                            r.consequence, r.likelihood, r.rating,
                            g.description, g.cod_web_colour,
                            c.listorder AS conslistorder,
                            l.listorder AS likelistorder
                        FROM
                            ratings_map r
                        INNER JOIN
                            code_inc_grades g ON g.code = r.rating
                        INNER JOIN
                            code_inc_conseq c ON c.code = r.consequence
                        INNER JOIN
                            code_inc_likeli l ON l.code = r.likelihood';

                $levelMap = PDO_fetch_all($sql);

                $javascript = 'var levelMap = new Array();';
                foreach ($levelMap as $row)
                {
                    $javascript .= '
                    levelMap["'.$row['consequence'].'-'.$row['likelihood'].'"] = {"rating" : "'.$row['rating'].'", "description" : "'.$row['description'].'",
                        "colour" : "#'.$row['cod_web_colour'].'", "conslistorder" : "'.$row['conslistorder'].'", "likelistorder" : "'.$row['likelistorder'].'"};';
                }

                $javascript .= '
                    function setRatingAndLevel(consFieldName, likeFieldName, ratingFieldName, levelFieldName, onInitialLoad)
                    {
                        var consValue = jQuery("#"+consFieldName).val();
                        var likeValue = jQuery("#"+likeFieldName).val();
                        if (consValue != "" && likeValue != "" && levelMap[consValue+"-"+likeValue] != undefined)
                        {
                            jQuery("#"+ratingFieldName).val(levelMap[consValue+"-"+likeValue]["conslistorder"] * levelMap[consValue+"-"+likeValue]["likelistorder"]);
                            jQuery("#"+ratingFieldName+"_Descr").html(levelMap[consValue+"-"+likeValue]["conslistorder"] * levelMap[consValue+"-"+likeValue]["likelistorder"]);
                            jQuery("#"+levelFieldName).val(levelMap[consValue+"-"+likeValue]["rating"]);
                            jQuery("#"+levelFieldName+"_Descr").html(levelMap[consValue+"-"+likeValue]["description"]);
                            jQuery("#"+levelFieldName+"_Descr").css("background-color", levelMap[consValue+"-"+likeValue]["colour"]);
                        }
                        else
                        {
                            jQuery("#"+ratingFieldName).val("");
                            jQuery("#"+ratingFieldName+"_Descr").html("");
                            jQuery("#"+levelFieldName).val("");
                            jQuery("#"+levelFieldName+"_Descr").html("");
                            ' . (($FormAction == 'ReadOnly' || $FormAction == 'Print') ? '' : 'jQuery("#"+levelFieldName+"_Descr").css("background-color", "#FFFFFF");') . '
                        }

                        if (!onInitialLoad)
                        {
                            jQuery("#CHANGED-"+ratingFieldName).val("1");
                            jQuery("#CHANGED-"+levelFieldName).val("1");
                        }
                    }';
            }
            else
            {
                // use standard risk grading
                $sql = 'SELECT
                            lower_rating, upper_rating, code, description, cod_web_colour
                        FROM
                            code_ra_levels
                        WHERE
                            lower_rating IS NOT NULL AND
                            upper_rating IS NOT NULL
                        ORDER BY
                            lower_rating, upper_rating';

                $levelMap = DatixDBQuery::PDO_fetch_all($sql);

                $javascript = 'var levelMap = new Array();';
                $levelMapLength = count($levelMap);
                for ($i = 0; $i < $levelMapLength; $i++)
                {
                    $javascript .= '
                    levelMap['.$i.'] = {"lower_rating" : "'.$levelMap[$i]['lower_rating'].'", "upper_rating" : "'.$levelMap[$i]['upper_rating'].'",
                        "code" : "'.$levelMap[$i]['code'].'", "description" : "'.$levelMap[$i]['description'].'", "cod_web_colour" : "#'.$levelMap[$i]['cod_web_colour'].'"};';
                }

                $javascript .= '
                    function setRatingAndLevel(consFieldName, likeFieldName, ratingFieldName, levelFieldName, onInitialLoad)
                    {
                        var consValue = jQuery("#"+consFieldName).val();
                        var likeValue = jQuery("#"+likeFieldName).val();
                        if (consValue != "" && likeValue != "")
                        {
                            var ratingValue = consValue * likeValue;
                            jQuery("#"+ratingFieldName).val(ratingValue);
                            jQuery("#"+ratingFieldName+"_Descr").html(ratingValue);
                            for (var i = 0; i < levelMap.length; i++)
                            {
                                if (ratingValue >= levelMap[i]["lower_rating"] && ratingValue <= levelMap[i]["upper_rating"])
                                {
                                    jQuery("#"+levelFieldName).val(levelMap[i]["code"]);
                                    jQuery("#"+levelFieldName+"_Descr").html(levelMap[i]["description"]);
                                    jQuery("#"+levelFieldName+"_Descr").css("background-color", levelMap[i]["cod_web_colour"]);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            jQuery("#"+ratingFieldName).val("");
                            jQuery("#"+ratingFieldName+"_Descr").html("");
                            jQuery("#"+levelFieldName).val("");
                            jQuery("#"+levelFieldName+"_Descr").html("");
                            ' . (($FormAction == 'ReadOnly' || $FormAction == 'Print') ? '' : 'jQuery("#"+levelFieldName+"_Descr").css("background-color", "#FFFFFF");') . '
                        }

                        if (!onInitialLoad)
                        {
                            jQuery("#CHANGED-"+ratingFieldName).val("1");
                            jQuery("#CHANGED-"+levelFieldName).val("1");
                        }
                    }';
            }

            // set global variable so Javascript not repeated in subsequent risk grading sections
            $riskGradingJavascriptSet = true;
        }

        $javascript .= '
            jQuery(function(){setRatingAndLevel("'.$consequenceFieldName.'", "'.$likelihoodFieldName.'", "'.$ratingFieldName.'", "'.$levelFieldName.'", true);});';

        $JSFunctions[] = $javascript;
    }

    return $section;
}
?>
