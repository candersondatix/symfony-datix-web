<?php

function ListStandards($ram, $FormAction, $Module)
{
    global $UserLabels, $UserExtraText;

    ?>
    <li>
    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <?php

    if ($ram['recordid'])
    {
        $sql = "SELECT standards_main.recordid as stn_id, standards_main.stn_name, standards_main.stn_ourref, standards_main.stn_set,
        standards_main.stn_domain, standard_elements.recordid as ele_id, standard_elements.ele_code, standard_elements.ele_descr
        from link_modules, standard_elements, standards_main
        where link_modules.ele_id = standard_elements.recordid
	    and link_modules.stn_id = standards_main.recordid
        and link_modules.ram_id = :recordid";

        $standards = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $ram['recordid']));
        ?>
        <tr>
            <td class="titlebg" colspan="7">
        <?php

        if ($UserLabels["linked_standards"])
        {
            echo '<b>'.$UserLabels["linked_standards"].'</b>';
        }
        else
        {
            echo '<b>Standards</b>';
        }

        if ($UserExtraText["linked_standards"])
        {
            echo '<br />'.$UserExtraText["linked_standards"];
        }

        echo '</td>
            </tr>';
        ?>
        <tr>
		    <td class="windowbg" width="10%"  align="center"><b>Origin</b></td>
		    <td class="windowbg" width="10%"  align="center"><b>Domain</b></td>
		    <td class="windowbg" width="35%" align="center"><b>Title</b></td>
		    <td class="windowbg" width="10%"  align="center"><b>Ref.</b></td>
		    <td class="windowbg" width="35%"  align="center"><b>Element</b></td>
        </tr>
    <?php
	    foreach ($standards as $row)
        {
            if ($FormAction == 'ReadOnly')
            {
    ?>
        <tr>
            <td class="windowbg2" valign="middle" align="left"><?= $row["stn_set"]?></td>
            <td class="windowbg2" valign="middle" align="left"><?= $row["stn_domain"] ?></td>
            <td class="windowbg2" valign="middle" align="left"><?= $row["stn_name"] ?></td>
            <td class="windowbg2" valign="middle" align="left"><?= $row["stn_ourref"] ?></td>
            <td class="windowbg2" valign="middle" align="left"><?= "$row[ele_code] - $row[ele_descr]" ?></td>
        </tr>
    <?php
            }
            else
            {
    ?>
        <tr>
            <td class="windowbg2" valign="middle" align="left"><a href="<?= "$scripturl?action=standard&amp;recordid=$row[stn_id]" ?>"><?= $row["stn_set"]?></a></td>
            <td class="windowbg2" valign="middle" align="left"><a href="<?= "$scripturl?action=standard&amp;recordid=$row[stn_id]" ?>"><?= $row["stn_domain"] ?></a></td>
            <td class="windowbg2" valign="middle" align="left"><a href="<?= "$scripturl?action=standard&amp;recordid=$row[stn_id]" ?>"><?= $row["stn_name"] ?></a></td>
            <td class="windowbg2" valign="middle" align="left"><a href="<?= "$scripturl?action=standard&amp;recordid=$row[stn_id]" ?>"><?= $row["stn_ourref"] ?></a></td>
            <td class="windowbg2" valign="middle" align="left"><a href="<?= "$scripturl?action=element&amp;recordid=$row[ele_id]" ?>"><?= "$row[ele_code] - $row[ele_descr]" ?></a></td>
        </tr>
    <?php
            }
        }
    }
    else
    {
    ?>
        <tr>
            <td class="windowbg2" valign="middle" align="left">No linked standards</td>
        </tr>
    <?php
    }
?>
</table>
</li>
<?php
}

function ShowPrincipalObjectives($ram, $FormType, $Module)
{
    ListAssurance($ram, $FormType, $Module, 'PRINOBJ');
}

function ShowDetailedObjectives($ram, $FormType, $Module)
{
    ListAssurance($ram, $FormType, $Module, 'OBJECT');
}

function ShowControls($ram, $FormType, $Module)
{
    ListAssurance($ram, $FormType, $Module, 'CTRL');
}

function ShowGapsInControls($ram, $FormType, $Module)
{
    ListAssurance($ram, $FormType, $Module, 'GCTRL');
}

function ShowAssurances($ram, $FormType, $Module)
{
    ListAssurance($ram, $FormType, $Module, 'ASSURE');
}

function ShowGapsInAssurances($ram, $FormType, $Module)
{
    ListAssurance($ram, $FormType, $Module, 'GAPASS');
}

function ListAssurance($ram, $FormType, $Module, $alabel)
{
    global $scripturl, $ModuleDefs;

    $a_section = $ModuleDefs[$Module]['ASSURANCE'][$alabel]['section'];

    $RISKPerms = GetParm("RISK_PERMS");
    $print = ($_GET["print"] == 1);

    $ass_row = $ram['assurance'];

    if ($FormType != "Print" && $FormType != "ReadOnly")                                                                                                                                                                                  //. 'document.' . $formName . '.rbWhat.value=\'' . $rbWhat . '\';'
    {
        $EditButton = '&nbsp;&nbsp;<input type="button" id="btnEdit" name="btnEdit" value="Edit" onclick="if(CheckChange()){$(\'assurance_framework_clicked\').value=\'1\';selectAllMultiple(document.getElementById(\'ram_objectives\'));document.RAMform.rbWhat.value=\''.BTN_SAVE.'\';document.RAMform.submit();SendTo(\'' . $scripturl
            . '?action=editassurance&amp;ram_id=' . $ram['recordid']
            . '&amp;table=' . Sanitize::SanitizeString($_GET['table']) . '&amp;type=' . $alabel . '\');}" />';
    }

    echo '
    <li><table class="gridlines" cellspacing="1" cellpadding="4" width="99%" align="center" border="0">';

    if (!$ass_row[$alabel])
    {
        echo '
        <tr name="'.$a_section.'_row" id="'.$a_section.'_row">
        <td class="windowbg2" colspan="3">
        <b>No values</b>' . $EditButton . '
        </td>
        </tr>';
    }
    else
    {
        echo'
        <tr name="'.$a_section.'_row" id="'.$a_section.'_row">';

        if ($alabel == 'ASSURE' || $alabel == 'GAPASS')
        {
            echo '<td class="windowbg" width="10%"><b>Source</b></td>';
            echo'
                <td class="windowbg"><b>Value</b>' . $EditButton . '</td>';
        }
        else
        {
            echo'
                <td class="windowbg"><b>Value</b>' . $EditButton . '</td>';
        }

        echo '</tr>';

        if (is_array($ass_row[$alabel]))
        {
            foreach ($ass_row[$alabel] as $ass)
            {
                echo '
                <tr name="'.$a_section.'_row" id="'.$a_section.'_row">';

                if ($alabel == 'ASSURE' || $alabel == 'GAPASS')
                {
                    echo '<td class="windowbg2">'.$ass["ass_source"].'</td>';
                    echo '<td class="windowbg2">'.Escape::EscapeEntities($ass["ass_text"]).'</td>';
                }
                else
                {
                    echo '
                        <td class="windowbg2" colspan="3">'.Escape::EscapeEntities($ass["ass_text"]).'</td>';
                }

                echo '
                </tr>';
            }
        }
    }

    echo '</table></li>';
}

function RecordPermissionsRAM($ram, $doc, $FormAction)
{
    require_once 'Source/security/SecurityRecordPerms.php';
	MakeRecordSecPanel(
        'RAM',
		'ra_main',
		$ram['recordid'],
		$FormAction
    );
}

function ListContacts($data, $con, $ReadOnly = false, $FormType)
{
    require_once 'Source/contacts/GenericContactForm.php';
    ListContactsSection(array(
        'data' => $data,
        'module' => 'RAM',
        'formtype' => $FormType
    ));
}

?>