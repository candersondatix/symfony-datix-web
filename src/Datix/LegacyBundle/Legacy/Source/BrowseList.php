<?php

use Source\classes\Filters\Container;
use Source\classes\Filters\Controller;
use src\reports\model\report\Report;
use src\security\Escaper;

function BrowseList()
{
	global $user, $unapproved, $scripturl, $num_unapproved, $yySetLocation, $dtxtitle, $dtxdebug, $FieldDefs,
           $FieldDefsExtra, $NoMainMenu, $ModuleDefs;

	LoggedIn();

    if (isset($_GET['creatingassignments']) && $_GET['creatingassignments'] == '1' && !CanSeeModule('LOC'))
    {
        AddSessionMessage('ERROR', _tk('no_permission_loc'));
        $yySetLocation = $scripturl . '?action=list&module=ATM';
        redirectexit();
    }

    require_once 'Source/libs/QueriesBase.php';

    $module = Sanitize::getModule($_GET['module']);
    $overdue = Sanitize::SanitizeString($_GET['overdue']);
    $_SESSION[$module]['OVERDUE'] = $overdue;
    $sideMenuModule = Sanitize::SanitizeString($_GET['sidemenu']);

    if ($_POST["orderby"])
    {
        $_SESSION["LIST"][$module]["ORDERBY"] = Sanitize::SanitizeString($_POST["orderby"]);
    }

    if ($_POST["order"])
    {
        $_SESSION["LIST"][$module]["ORDER"] = Sanitize::SanitizeString($_POST["order"]);
    }

    $orderby = $_SESSION["LIST"][$module]["ORDERBY"];
    $order = $_SESSION["LIST"][$module]["ORDER"];

    $LinkMode = ($_GET['link'] == '1');

    if ($LinkMode)
    {
        $NoMainMenu = true;
    }

    // Make sure that we don't ask for values again when a field has @PROMPT
    if (isset($_POST['promptsubmitted']) && $_POST['promptsubmitted'] == '1')
    {
        $_SESSION[$module]['PROMPT']['PROMPTSUBMITTED'] = true;
    }

    switch ($module)
    {
        case "ACT":
            $includefile = "Source/actions/browselistactions.php";
            break;
        case "SAB":
            $includefile = "Source/sabs/browselistsabs.php";
            break;
        case "AST":
            $includefile = "Source/assets/browselistassets.php";
            break;
        case "TOD":
            $includefile = "Source/todo/browselisttodo.php";
            break;
        default:
            $includefile = "Source/libs/BrowseListGeneric.php";
    }

    require_once $includefile;

//=============================================================================
// Included file needs to set:
// $_SESSION[]["TABLES"]
// $_SESSION[]["EXTRAWHERE"]
// $ListTitle
// $ListTitleExtra
// $ActionType
// $timescales_url
// $ListWhereClause
//=============================================================================

    // TODO ideally would use the Observer pattern here to allow modules to plug in custom functionality
    // But this is a little hard to achieve right now, so using AppVars instead
    if ($ModuleDefs[$module]['BROWSELIST_PRE_RENDER'])
    {
        $callback = $ModuleDefs[$module]['BROWSELIST_PRE_RENDER'];

        if (is_array($callback) && method_exists($callback[0], $callback[1]))
        {
            call_user_func($callback, $list_request);
        }
        elseif (is_string($callback) && function_exists($callback))
        {
            call_user_func($callback, $list_request);
        }
        elseif (is_callable($callback)) // for lambda functions
        {
            $callback($list_request);
        }
    }

    $listnum = $listnum2;
	$dtxtitle = $ListTitle;

    if ($listnumall > 0)
    {
        $TitleExtra = $listnumall.' '._tk('record').($listnumall == 1 ? "" : "s")._tk('found_displaying') . ($listnum-$listdisplay+1) . '-' . (($listnum<$listnumall) ? $listnum : $listnumall) .'.';
    }

    getPageTitleHTML(array(
        'module' => $module,
        'image' => 'Images/icons/icon_'.$module.'_list.png',
        'title' => $ListTitle,
        'subtitle' => $TitleExtra
    ));

	if (!$LinkMode)
    {
        if (!$PrintMode)
        {
            GetSideMenuHTML(array(
                'module' => $module,
                'menumodule' => $sideMenuModule,
                'listing' => ($listnumall > 0),
                'extra_links' => $ModuleDefs[$module]['LISTING_EXTRA_LINKS']
            ));
        }

        template_header('','','','',new Template(array('no_padding' => true)));
    }
    else
    {
        echo getBasicHeader();
    }
    /* TODO - lets find a better way to add section specific css and js than injecting it at a relatively random point in the code */
?>
<style type="text/css">
/* display fix for IE6 - needs to be specific to listings, so can't go in main CSS file */
.content
{
    _overflow-x: auto;
    _width: 99%;
    _padding-bottom: 20px;
}
</style>
<script language="JavaScript" type="text/javascript">
    function sortList(orderBy)
    {
        if (orderBy != "")
        {
            var order = 'ASC';

            if (document.timescaleform.order.value == 'ASC')
            {
                order = 'DESC';
            }

            document.timescaleform.orderby.value = orderBy;
            document.timescaleform.order.value = order;
            document.timescaleform.listnum.value = '';
            document.timescaleform.submit();
        }
    }

    function pageing(orderby, order, listnum)
    {
         document.timescaleform.orderby.value = orderby;
         document.timescaleform.order.value = order;
         document.timescaleform.listnum.value = listnum;
         document.timescaleform.submit();
    }

</script>
<?php if($dtxdebug) echo $sqla;

    //Redirect if we are creating assignments without selecting templates
    if (isset($_GET['creatingassignments']) && (!$_SESSION['ATM']['RECORDLIST'] || empty($_SESSION['ATM']['RECORDLIST']->FlaggedRecords)))
    {
        AddSessionMessage('ERROR', 'You must select at least one record');
        $yySetLocation = $scripturl . '?action=list&module=ATM';
        redirectexit();
    }

    //Redirect if we are generating instances without selecting assessments
    if ((isset($_GET['btn']) && $_GET['btn'] == 'create') && (!$_SESSION['ATI']['RECORDLIST'] || empty($_SESSION['ATI']['RECORDLIST']->FlaggedRecords)))
    {
        AddSessionMessage('ERROR', 'You must select at least one record');
        $yySetLocation = $scripturl . '?action=list&module=ATI';
        redirectexit();
    }

    //Set the listing to be readonly if we are selecting locations as part of the process of creating assignments
    if (isset($_GET['creatingassignments']) && $module == 'LOC')
    {
        $PrintMode = true;
    }

    //Set the listing to be readonly if we are selecting locations as part of the process of generating instances
    if (isset($_GET['btn']) && $_GET['btn'] == 'create' && $module == 'LOC')
    {
        $PrintMode = true;
    }

    // Present informational message if we are on the instance generation process and we have records to show
    if ($listnumall > 0)
    {
        if ((isset($_GET['btn']) && $_GET['btn'] == 'create') || isset($_GET['creatingassignments']))
        {
            AddSessionMessage('INFO', _tk('greyed_out_locations_info'));
            echo GetSessionMessages();
        }
    }
    else
    {
        if ((isset($_GET['btn']) && $_GET['btn'] == 'create') || isset($_GET['creatingassignments']))
        {
            AddSessionMessage('INFO', _tk('no_locations_info'));
            echo GetSessionMessages();
        }
    }

    $list_columns_count = count($list_columns);

    if ($module=='INC')
    {
        $list_columns_count++;
    }

    // Take into account extra choose column
    if ($LinkMode)
    {
        $list_columns_count++;
    }


    if (bYN(GetParm("SAVED_QUERIES", "Y")) && !$LinkMode && !$ModuleDefs[$module]['LISTING_FILTERS'] && $module != 'TOD')
    {
        $saved_queries = get_saved_queries($module);

?>
       <table width="100%">

        <tr><td><div style="float:left">
            <form method="post" name="queryform" action="<?= "$scripturl?action=executequery&module=$module"?>" style="display: inline">
                 <a name="datix-content" id="datix-content"></a>
				 <label for="qry_name"><b><?php echo _tk('query_colon')?></b></label>
                    <select id="qry_name" name="qry_name" <?php if(!is_array($saved_queries) || empty($saved_queries)) { echo 'style="width:'.DROPDOWN_WIDTH_DEFAULT.'px"'; } ?> onchange="Javascript:if (document.queryform.qry_name.options[document.queryform.qry_name.selectedIndex].value != 'range' && document.queryform.qry_name.options[document.queryform.qry_name.selectedIndex].value != '') {document.queryform.submit()}">
<?php
        $query_recordid = Sanitize::SanitizeInt($_GET["query"]);

        if (is_array($saved_queries))
        {
            echo '<option value=""'
                    . ($query_recordid == $query ? ' selected="selected"' : '') . '>'
                    . 'Choose</option>';

            foreach ($saved_queries as $query => $querytitle)
            {
                echo '<option value="' . $query . '"'
                    . ($query_recordid == $query ? ' selected="selected"' : '') . '>'
                    . $querytitle . '</option>';
            }
        }
    echo '
                        </select>
                <input type="hidden" name="orderby" value="'. $orderby .'" />
                <input type="hidden" name="order" value="'. $order .'" />
                <input type="hidden" name="listnum" value="'. $listnum .'" />
            </form>
        </div>
<div style="float:right">
  <div class="list_replacer gbutton">
                            <a href="' . $scripturl . '?action=savequery&amp;module='.$module.'&amp;form_action=new&amp;table=' . Sanitize::SanitizeString($_REQUEST["table"]) . '">' . _tk('save_as_query') . '</a>

                    </div>
            </div>
  </td></tr></table>';

    }

    if ($ModuleDefs[$module]['LISTING_FILTERS'] && !$_REQUEST["drillwhere"] && $DrillIntoReport === false)
    {
        if (isset($_GET['creatingassignments']) && $_GET['creatingassignments'] == '1')
        {
            // Unset the recordlist when loading the page for the first time to avoid caching issues
            if( ! $_POST['listnum'])
            {
                unset($_SESSION[$module]['RECORDLIST']);
            }

            if ($_GET['listtype'] != 'search')
            {
                //Not great, but the aim of this is that when we load the locations listing for the first time (i.e. not
                //having run a filter), that we should clear any previous where clause to prevent the recordlist getting out
                //of sync with the listing.
                //TODO: We need to rationalise how we pass these where clauses around.
                unset($_SESSION[$module]['WHERE']);
            }
        }

        if (!isset($_SESSION[$module]['RECORDLIST']) && isset($_GET['btn']) && $_GET['btn'] == 'create')
        {
            $_SESSION[$module]['RECORDLIST'] = RecordLists_RecordListShell::CreateForModule($module, $_SESSION[$module]['WHERE']);
        }
        elseif (!isset($_SESSION[$module]['RECORDLIST']) && isset($_GET['creatingassignments']) && $_GET['creatingassignments'] == '1')
        {
            $_SESSION[$module]['RECORDLIST'] = RecordLists_RecordListShell::CreateForModule($module, $_SESSION[$module]['WHERE']);
        }
        elseif (!isset($_SESSION[$module]['RECORDLIST']))
        {
            $_SESSION[$module]['RECORDLIST'] = RecordLists_RecordListShell::CreateForModule($module, '', '', 'DESC', '', true);
        }

        if (!isset($_GET['btn']) && $_GET['btn'] != 'create')
        {
            $filter = Container::getFilter($module, true, $sideMenuModule);
            echo $filter->display();
        }
    }
    else if ($module != 'TOD')
    {
        $ListWhereClause = ReplacePrompts($ListWhereClause, $module);
        $_SESSION[$module]['RECORDLIST'] = RecordLists_RecordListShell::CreateForModule($module, $ListWhereClause, $orderby, $order, $overdueSQL['join']);
    }

    $listing = Listings_ListingFactory::getListing($module);

    if ($listnumall == 0)
    {
        echo '<div class="section_link_row row_above_table"><b>'._tk('no_records_found').'</b></div>';
    }
    else
    {
        if ($listnumall > $listdisplay)
        {
            PageNumbers($listnum, $listnumall, $listdisplay);
        }

        // Output column headers
        echo '<table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%"> <tr class="tableHeader head2">';

        if (is_array($list_columns))
        {
            // Take into account extra choose column
            if ($LinkMode)
            {
                echo '<th class="windowbg" width="6%" align="center"></th>';
            }
            elseif ($module == 'INC' || $module == 'TOD')  //need column for overdue row marker.
            {
                echo '<th class="windowbg" width="1%" align="center"></th>';
            }
            elseif (in_array($module, array('AMO', 'ATI', 'ATM', 'AQU', 'ATQ', 'LOC')))  //need column for checkbox
            {
                echo '<th class="windowbg" width="1%" align="center">
                    <script language="Javascript">
                    jQuery(window).on(\'beforeunload\', function(e) {
                        jQuery.ajax({url:\''.$scripturl.'?action=httprequest&type=setsessionflagstate&module='.$module.'\'+getListingCheckboxStateString(), async:false});
                    });</script>
                    <input type="hidden" name="listing_checkbox_select_all" id="listing_checkbox_select_all" value="'.($_SESSION[$module]['RECORDLIST']->allRecordsFlagged() ? '1' : '').'">
                    <input type="checkbox" name="listing_checkbox_all" id="listing_checkbox_all" '.($_SESSION[$module]['RECORDLIST']->allRecordsFlagged() ? ' checked="checked"' : '').' />
                </th>';
            }

            foreach ($list_columns as $col_field => $col_info)
            {
                echo '<th class="windowbg" ' . ($col_info['width'] ? 'width="' . $col_info['width'] . '%"' : '') . '>';

                if ($FieldDefs[$module][$col_field]['Type'] != 'textarea' && !$col_info['no_sort'] && !preg_match('/^UDF.*_([0-9]+)$/ui', $col_field))
                {
                    echo '<a href="Javascript:sortList(\'' . $col_field . '\');">';
                }

                if ($col_field == "mod_title")
                {
                    $currentCols["mod_title"] = "Module";
                }
                elseif ($col_field == "recordid")
                {
                    if ($col_info_extra['colrename'])
                    {
                        $currentCols[$col_field] = $col_info_extra['colrename'];
                    }
                    else
                    {
                        $currentCols[$col_field] = $listing->GetColumnLabel($col_field, $FieldDefsExtra[$module][$col_field]["Title"], false, $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE'], $module);
                    }
                }
                else
                {
                    $Long = false;
                    if ($module != 'LOC' && preg_match('/tier_(\d)/', $col_field))
                    {
                        $Long = true;
                    }
                    $currentCols[$col_field] = $listing->GetColumnLabel($col_field, $FieldDefsExtra[$module][$col_field]["Title"], $Long, $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE']);
                }

                if (isset($FieldDefs[$module][$col_field]['SubmoduleSuffix']))
                {
                    $currentCols[$col_field] .= ' '.$FieldDefs[$module][$col_field]['SubmoduleSuffix'];
                }

                echo $currentCols[$col_field];

                if ($FieldDefs[$module][$col_field]['Type'] != 'textarea')
                {
                    echo '</a>';
                }

                echo '</th>';
            }

            if (in_array($module, array('CQO','CQP','CQS')))
            {
                // add evidence column
                echo '<th class="windowbg" width="1%"></th>';
            }
        }

        echo '</tr>';

        $baserecord_url = $record_url;

        $isAnyGreyedOut = (isset($_GET['showinfodiv']) && $_GET['showinfodiv'] == '1' ? true : false);

        if ($module == 'AST' && $_SESSION['AST']['LINK']['LINK_ID']['VALUE'] && $_SESSION['AST']['LINK']['MODULE'])//gets a list of equipment that cannot be linked (ie is already linked)
        {
            $disallowedRecords = GetDisallowedEquipment($_SESSION['AST']['LINK']['LINK_ID']['VALUE'], $_SESSION['AST']['LINK']['MODULE']);
        }

        // Get organisations that were already linked as respondents
        if ($module == 'ORG' && $_SESSION['ORG']['LAST_SEARCH']['main_recordid'])
        {
            $disallowedRecords = (new \src\organisations\model\OrganisationModelFactory())->getMapper()->getDisallowedOrganisations($_SESSION['ORG']['LAST_SEARCH']['main_recordid']);
        }

        while ($row = db_fetch_array($list_request, MSSQL_ASSOC))
        {
            $recordids[] = $row['recordid'];
            $data[] = $row;
        }

        foreach ($list_columns as $col_field => $width)
        {
            if (preg_match('/^UDF.*_([0-9]+)$/ui', $col_field, $matches))
            {
                $udf_field_ids[] = $matches[1];
            }
        }

        $udfData = array();

        if (!empty($udf_field_ids))
        {
            $udfData = getUDFDisplayData($module, $recordids, $udf_field_ids);
        }

        if ($module == 'INC' && bYN(GetParm('OVERDUE_HIGHLIGHT', 'Y')))
        {
            $overdueRecords = getOverdueRecords($data);
        }

        // Output data
        foreach ($data as $row)
        {
            if ($orderby && ($LastGroup != $row[$orderby]) && ($FieldDefs[$module][$orderby]['Type'] == 'ff_select' || $orderby == "inc_grade"))
            {
                if($orderby == 'rep_approved')
                {
                    $desc = GetApprovalStatusDescription($module, $row[$orderby]);
                }
                else if ($orderby == 'ram_cur_level' || $orderby == 'ram_after_level' || $orderby == 'ram_level')
                {
                    if (bYN(GetParm('RISK_MATRIX', 'N')))
                    {
                        $desc = DatixDBQuery::PDO_fetch('SELECT description FROM code_inc_grades WHERE code = :code', array('code' => $row[$orderby]), PDO::FETCH_COLUMN);
                    }
                    else
                    {
                        $desc = DatixDBQuery::PDO_fetch('SELECT description FROM code_ra_levels WHERE code = :code', array('code' => $row[$orderby]), PDO::FETCH_COLUMN);
                    }
                }
                else
                {
                    $desc = code_descr($module, $orderby, $row[$orderby]);
                }

                echo '
                    <tr>
                        <td class="titlebg" colspan="' . $list_columns_count . '">
                            <b>' . $desc . '</b>
                        </td>
                    </tr>';
            }

            $LastGroup = $row[$orderby];

            echo '<tr class="listing-row">';

            if ($module == "TOD")
            {
                $baserecord_url = "{$scripturl}?action=". $ModuleDefs[$row["tod_mod"]]["ACTION"] ."&fromsearch=1";
            }

            $record_url = $baserecord_url . "&recordid={$row['recordid']}" . ($sideMenuModule == 'ACR' ? '&sidemenu=ACR' : '');

            if (is_array($list_columns))
            {
                if ($LinkMode) {
                	if (is_array($disallowedRecords) && in_array($row['recordid'], $disallowedRecords))
                	{
                		// This equipment is already linked with the relevent link_type, and so cannot be re-selected to link again.
                		echo '<td class="windowbg2" style="font-color:grey">'
                		. '<i>Link already exists</i></td>';
                	}
                	else 
                	{
                        if ($module == 'ORG')
                        {
                            echo '<td class="windowbg2">'
                                . '<input type="button" value="Choose" onclick="opener.location.href=\''
                                . $_SESSION['ORG']['LINK']['URL']
                                . "&from_match=1&match_link_id={$row[recordid]}"
                                . "&token=".CSRFGuard::getCurrentToken().'\';window.open(\'\', \'_self\');window.close();" /></td>';
                        }
                        else
                        {
                            // Extra choose column
                            // e.g. url format to link an asset record to a SABS record:
                            // action=asset&module=SAB&link_id=1&ast_id=14
                            // Link information is stor in $_SESSION['MOD']['LINK'], e.g.:
                            // $_SESSION["AST"]["LINK"]["MODULE"] = $_POST["module"];
                            // $_SESSION["AST"]["LINK"]["LINK_ID"]["NAME"] = "link_id";
                            // $_SESSION["AST"]["LINK"]["LINK_ID"]["VALUE"] = $_POST["link_id"];
                            // $_SESSION["AST"]["LINK"]["MAIN_ID"]["NAME"] = "ast_id";
                            echo '<td class="windowbg2">'
                                . '<input type="button" value="Choose" onclick="opener.location.href=\''
                                . $baserecord_url
                                . "&from_equipment_match=1&equipment_match_link_id=".$_SESSION["AST"]["LINK"]["LINK_RECORDID"]["VALUE"]
                                . "&module={$_SESSION[AST][LINK][MODULE]}"
                                . "&{$_SESSION[AST][LINK][LINK_ID][NAME]}={$_SESSION[AST][LINK][LINK_ID][VALUE]}"
                                . "&{$_SESSION[AST][LINK][MAIN_ID][NAME]}={$row[recordid]}"
                                . "&token=".CSRFGuard::getCurrentToken()
                                . '\';window.close();" /></td>';
                        }
                	}
                }
                elseif ($module == 'INC')
                {
                    if (in_array($row['recordid'],$overdueRecords))
                    {
                        echo '<td class="windowbg2" style="padding:0px;text-align:center" valign="middle"><img src="Images/icons/icon_incidents20x20.png" align="center" alt="' . _tk('alt_overdue') . '" title="' . _tk('alt_overdue') . '" height="20" width="20" style="vertical-align:middle"/></td>';
                    }
                    else
                    {
                        echo '<td class="windowbg2"></td>';
                    }
                }
                elseif ($module == 'TOD')
                {
                    $DueDate = strtotime($row['tod_due']);
                    $Today = strtotime(date(F));

                    if ($DueDate == $Today)
                    {
                        echo '<td class="windowbg2" style="padding:0px;text-align:center" valign="middle"><img src="Images/icons/todo_duetoday.png" align="center" alt="' . _tk('alt_overdue') . '" height="20" width="20" style="vertical-align:middle"/></td>';
                    }
                    elseif ($DueDate < $Today)
                    {
                        echo '<td class="windowbg2" style="padding:0px;text-align:center" valign="middle"><img src="Images/icons/todo_overdue.png" align="center" alt="' . _tk('alt_overdue') . '" height="20" width="20" style="vertical-align:middle"/></td>';
                    }
                    else
                    {
                        echo '<td class="windowbg2"></td>';
                    }
                }
                elseif (in_array($module, array('AMO', 'ATI', 'ATM', 'AQU', 'ATQ', 'LOC')))
                {
                    // Disable checkboxes and grey-out record if we are on the instance generation process
                    if (isset($_GET['btn']) && $_GET['btn'] == 'create')
                    {
                        if (isset($_SESSION['LOC']['FILTEREDLOCATIONS'][$row['recordid']]))
                        {
                            $Checked = $_SESSION['LOC']['FILTEREDLOCATIONS'][$row['recordid']]['CHECKED'];
                            $Disabled = $_SESSION['LOC']['FILTEREDLOCATIONS'][$row['recordid']]['DISABLED'];
                            $GreyedOut = $_SESSION['LOC']['FILTEREDLOCATIONS'][$row['recordid']]['GREYEDOUT'];

                            if ($Disabled && $GreyedOut)
                            {
                                $isAnyGreyedOut = true;
                            }
                        }
                        else
                        {
                            if (ASM_Template::validateAssignedAssessmentsPath($_SESSION['ATI']['RECORDLIST']->FlaggedRecords, array(0 => $row['recordid'])))
                            {
                                if (ASM_Template::validateGeneratedInstances($_SESSION['ATI']['RECORDLIST']->FlaggedRecords, $row['recordid']))
                                {
                                    if ($_SESSION['LOC']['RECORDLIST']->isFlagged(array('recordid' => $row['recordid'])))
                                    {
                                        $Checked = true;
                                    }
                                    else
                                    {
                                        $Checked = false;
                                    }

                                    $Disabled = false;
                                    $GreyedOut = false;
                                }
                                else
                                {
                                    $Checked = false;
                                    $Disabled = true;
                                    $GreyedOut = true;
                                    $isAnyGreyedOut = true;
                                }
                            }
                            else
                            {
                                $Checked = false;
                                $Disabled = true;
                                $GreyedOut = true;
                                $isAnyGreyedOut = true;
                            }

                            if ($Disabled && $GreyedOut)
                            {
                                $_SESSION[$module]['RECORDLIST']->UnFlagRecord($row['recordid']);
                                $_SESSION[$module]['RECORDLIST']->markDisabled($row['recordid']);
                            }
                        }
                    }
                    elseif (isset($_GET['creatingassignments']))
                    { // Disable checkboxes and grey-out record if we are on the createing assignments process

                    	$validator = new Source\classes\ASM\Validator();

                    	if ($validator->validateLocation($row['recordid']))
                    	{
                            $GreyedOut = false;
                            $Disabled = false;
                    	}
                    	else
                    	{
                    		$isAnyGreyedOut = true;
                            $GreyedOut = true;
                            $Disabled = true;
                    	}

                        if ($Disabled && $GreyedOut)
                        {
                            $_SESSION[$module]['RECORDLIST']->UnFlagRecord($row['recordid']);
                            $_SESSION[$module]['RECORDLIST']->markDisabled($row['recordid']);
                        }

                        $Checked = ($_SESSION[$module]['RECORDLIST']->isFlagged(array('recordid' => $row['recordid'])) ? true : false);
                    }
                    else
                    {
                        $Checked = ($_SESSION[$module]['RECORDLIST']->isFlagged(array('recordid' => $row['recordid'])) ? true : false);
                    }
                    

                    echo '<td class="windowbg2"><input type="checkbox" class="listing_checkbox" name="listing_checkbox_'.$row['recordid'].'" id="listing_checkbox_'.$row['recordid'].'"'.($Checked ? ' checked="checked"' : '').' '.($Disabled ? ' disabled="disabled"' : '').' /></td>';
                }

                foreach ($list_columns as $col_field => $col_info )
                {
                    if (in_array($module, array('CQO','CQP','CQS')) && preg_match('/tier_(\d)/u', $col_field))
                    {
                        $row[$col_field] = array_shift($listing->getNodeAtTier($col_field, $row['recordid']));
                    }

                    // special columns
                    if ($col_field == 'ram_cur_level' || $col_field == 'ram_after_level' || $col_field == 'ram_level')
                    {
                        if ($col_field == "ram_cur_level")
                        {
                            $col = $row["ram_cur_level"];
                        }

                        if ($col_field == "ram_after_level")
                        {
                            $col = $row["ram_after_level"];
                        }

                        if ($col_field == "ram_level")
                        {
                            $col = $row["ram_level"];
                        }

                        if (bYN(GetParm('RISK_MATRIX', 'N')))
                        {
                            $sql = "SELECT cod_web_colour, description FROM code_inc_grades WHERE code = '$col'";
                        }
                        else
                        {
                            $sql = "SELECT cod_web_colour, description FROM code_ra_levels WHERE code = '$col'";
                        }

                		$request2 = db_query($sql);
            			$level = db_fetch_array($request2);

                        if (!isset($level['description']))
                        {
                            $level['description'] = $col;
                        }

                        $colour = $level["cod_web_colour"];
                        ?>
                        <td align="left" <?= (empty($colour)) ? "class=\"windowbg2\">" : "bgcolor=\"#$colour\">" ?><?= $level["description"] ?></td>
                        <?php
                    }
                    elseif ($col_field == 'inc_grade')
                    {
                        // Get grade map information
                        $sql = "SELECT cod_web_colour, description FROM code_inc_grades WHERE code = '{$row['inc_grade']}'";

                        $result = db_query($sql);

                        if ($incgrade = db_fetch_array($result))
                        {
                            echo '<td align="left" ' . ($incgrade["cod_web_colour"] ? 'bgcolor="#' . $incgrade["cod_web_colour"] .'"' : "") . '>' . $incgrade["description"] . '</td>';
                        }
                        else
                        {
                            echo '<td align="left" class="rowfieldbg">' . $row['inc_grade'] . ' </td>';
                        }
                    }
                    // regular columns
                    else
                    {
                        if ($FieldDefs[$module][$col_field]['Type'] == 'ff_select' || $FieldDefs[$module][$col_field]['Type'] == 'multilistbox')
                        {
                            $codeinfo = get_code_info($module, $col_field, $row[$col_field]);

                            $colour = '';

                            if ($codeinfo["cod_web_colour"])
                            {
                                $colour = $codeinfo["cod_web_colour"];
                            }

                            if ($colour)
                            {
                                echo '<td valign="left" style="background-color:#' . Escape::EscapeEntities($colour). '"';
                            }
                            else
                            {
                                echo '<td class="windowbg2" valign="top"';
                            }
                        }
                        else
                        {
                            echo '<td class="windowbg2" valign="top"';
                        }

                        // split long words if larger than 50 chars
                        $row[$col_field] = preg_replace ('/(\S{50})/u', '$1 ', $row[$col_field]);
                        
                        if ($col_info_extra['dataalign'])
                        {
                            echo ' align="' . $col_info_extra['dataalign'] . '"';
                        }

                        echo '>';

                        if ($BoldRowField && $row[$BoldRowField] == $BoldRowConditionValue)
                        {
                            echo '<b>';
                        }

                        if (!$PrintMode && !$LinkMode && ($row[$col_field] || (preg_match('/^UDF.*_([0-9]+)$/', $col_field, $matches) && $udfData[$row['recordid']][$matches[1]])))
                        {
                            echo '<a href="' . $record_url . '" ' . ($GreyedOut ? 'style="color: #999999"' : '') . '>';
                        }

                        if ($GreyedOut)
                        {
                            echo '<span style="color: #999999">';
                        }

                        $row[$col_field] = htmlspecialchars($row[$col_field]);

                        //extra field hack
                        if (preg_match('/^UDF.*_([0-9]+)$/', $col_field, $matches))
                        {
                            echo $udfData[$row['recordid']][$matches[1]];
                        }
                        else if ($col_field == 'recordid')
                        {
                            if ($col_info_extra['prefix'])
                            {
                                echo $col_info_extra['prefix'];
                            }

                            echo $row[$col_field];
                        }
                        elseif ($col_field == "act_module")
                        {
                            echo $ModuleDefs[$row[$col_field]]["NAME"];
                        }
                        elseif ($col_field == 'rep_approved')
                        {
                            echo FirstNonNull(array(_tk('approval_status_'.$module.'_'.$row[$col_field]), $codeinfo['description']));
                        }
                        elseif ($FieldDefs[$module][$col_field]['Type'] == 'ff_select' || $FieldDefs[$module][$col_field]['Type'] == 'multilistbox')
                        {
                            echo Escape::EscapeEntities($codeinfo['description']);
                        }
                        elseif ($FieldDefs[$module][$col_field]['Type'] == 'date')
                        {
                            echo FormatDateVal($row[$col_field]);
                        }
                        elseif ($FieldDefs[$module][$col_field]['Type'] == 'money' && is_numeric($row[$col_field]))
                        {
                            echo FormatMoneyVal($row[$col_field]);
                        }
                        elseif ($FieldDefs[$module][$col_field]['Type'] == 'time')
                        {
                            if (\UnicodeString::strlen($row[$col_field]) == 4)
                            {
                                $row[$col_field] = $row[$col_field]{0} . $row[$col_field]{1} . ":" . $row[$col_field]{2} . $row[$col_field]{3};
                            }

                            echo $row[$col_field];
                        }
                        else
                        {
                            // Display full location hierarchy for each record
                            if ($module == 'LOC' && $sideMenuModule == 'ACR' && ((isset($_GET['btn']) && $_GET['btn'] == 'create' && bYN(GetParm('ATM_CAI', 'N'))) || ($_GET['creatingassignments'] && bYN(GetParm('ATM_AAT', 'N')))))
                            {
                                if ($col_field == 'loc_name')
                                {
                                    $sql = '
                                        SELECT
                                            parent_id
                                        FROM
                                            vw_locations_main
                                        WHERE
                                            recordid = :recordid
                                    ';

                                    $row['parent_id'] = DatixDBQuery::PDO_fetch($sql, array('recordid' => $row['recordid']), PDO::FETCH_COLUMN);

                                    $locationArray = array();
                                    $locationArray[] = array(
                                        'recordid' => $row['recordid'],
                                        'loc_name' => $row['loc_name'],
                                        'parent_id' => $row['parent_id']
                                    );

                                    $upperTierInfo = addUpperTierInfo($locationArray);
                                    $row[$col_field] = $upperTierInfo[0]['loc_name'];
                                }
                            }

                            echo $row[$col_field];
                        }

                        if ($GreyedOut)
                        {
                            echo '</span>';
                        }

                        if (!$PrintMode && !$LinkMode && ($row[$col_field] || (preg_match('/^UDF.*_([0-9]+)$/', $col_field, $matches) && $udfData[$row['recordid']][$matches[1]])))
                        {
                            echo '</a>';
                        }

                        if ($BoldRow)
                        {
                            echo '</b>';
                        }

                        echo '</td>';
                    }
                }

                if (in_array($module, array('CQO','CQP','CQS')) && is_array($ModuleDefs['LIB']['ADD_NEW_RECORD_LEVELS']) && in_array(GetParm($ModuleDefs['LIB']['PERM_GLOBAL']), $ModuleDefs['LIB']['ADD_NEW_RECORD_LEVELS']))
                {
                    echo '<td class="windowbg2" valign="top"><input type="button" value="' . _tk('cqc_add_evidence') . '" onclick="SendTo(\''.$scripturl.'?action=evidence&main_module='.$module.'&main_recordid='.$row['recordid'].'\');" /></td>';
                }

            }

            echo '</tr>';
        }

        echo '</table>';

        if (!$isAnyGreyedOut && $module != 'AST')
        {
	        echo '
                <script>
                    jQuery(document).ready(function() {
                      jQuery(\'.info_div\').hide();
                    });
                </script>
	        ';
        }


        if ($listnumall > $listdisplay)
        {
            PageNumbers($listnum, $listnumall, $listdisplay);
        }
    }

    if (!$LinkMode && ($ListType == "" || $ListType == "search") && (bYN(GetParm("SAVED_QUERIES", "Y"))) && !isset($overdue) && !$ModuleDefs[$module]['LISTING_FILTERS'] && $module != 'TOD') {
                echo '
            <div style="float:right">
              <div class="list_replacer gbutton">
                            <a href="' . $scripturl . '?action=savequery&amp;module='.$module.'&amp;form_action=new&amp;table=' . Sanitize::SanitizeString($_REQUEST["table"]) . '">' . _tk('save_as_query') . '</a>
                    </div>
            </div><div class="clearfix"></div>';
    }
?>
                            <div class="button_wrapper">
<?php
    if ($LinkMode)
    {
        echo '<input type="button" value="'._tk('btn_cancel').'" onClick="window.open(\'\', \'_self\');window.close();">';
    }
    elseif (!$ModuleDefs[$module]['LISTING_FILTERS'] || $_REQUEST['from_report'] == '1' || $_REQUEST['fromcrosstab'] == '1')
    {
        echo '<input type="button" value="'._tk('back').'" ';

        if ($_REQUEST['fromcrosstab'] == '1')
        {
            echo 'onclick="SendTo(scripturl+\'?action=reportdesigner&form_action=show_report&fromcrosstab=1&overdue='.Sanitize::SanitizeInt($_GET['overdue']).'&module='.$module.'&drilllevel='.$_SESSION['crosstab_current_drill_level'].'\')"';
        }
        elseif ($_REQUEST["from_report"] == '1')
        {
            echo 'onclick="SendTo(scripturl+\'?action=reportdesigner&module='.$module.'&form_action=show_report&from_report=1&overdue='.Sanitize::SanitizeInt($_GET['overdue']).'&from_dashboard='.Sanitize::SanitizeInt($_GET['from_dashboard']).'\')"';
        }
        elseif ($_GET["incident"])
        {
            echo 'onclick="SendTo(scripturl+\'?action=incident&amp;recordid=' . Sanitize::SanitizeInt($_GET["incident"]) . '\')"';
        }
        elseif ($_GET["query"])
        {
            if ($_GET['fromhomescreen'])
            {
                echo 'onclick="SendTo(scripturl+\'?action=home&module='.$module.'\')"';
            }
            else
            {
                echo 'onclick="SendTo(scripturl+\'?action=savedqueries&amp;module=' . $module . '\')"';
            }
        }
        elseif ($module == 'SAB' && !isset($_GET['listtype']))
        {
            echo 'onclick="SendTo(scripturl+\'?action=sabssearch&searchtype=lastsearch\')"';
        }
        elseif ($_GET["listtype"] == "search")
        {
            echo 'onclick="SendTo(scripturl+\'?'.  ($ModuleDefs[$module]["SEARCH_URL"] ? $ModuleDefs[$module]["SEARCH_URL"]  : 'action=search') . '&amp;module='.$module.'&amp;searchtype=lastsearch\')"';
        }
        elseif ($module)
        {
            if ($ModuleDefs[$module]['MODULE_GROUP'])
            {
                echo 'onclick="SendTo(scripturl+\'?module='.$ModuleDefs[$module]['MODULE_GROUP'].'\')"';
            }
            else
            {
                echo 'onclick="SendTo(scripturl+\'?module='.$module.'\')"';
            }
        }
        else
        {
            echo 'onclick="SendTo(scripturl+\'?module=INC\')">';
        }

        echo '/>';
    }

    // Create "Generate instances" button
    if ($module == 'ATI' && bYN(GetParm('ATM_CAI', 'N')))
    {
        echo '<input type="button" value="' . _tk('generate_assessment_instances') . '" onclick="SendTo(scripturl+\'?action=filterlocationscreateinstances\');" />';
    }

    // Create "Create instances" button
    if ($module == 'LOC' && $sideMenuModule == 'ACR' && isset($_GET['btn']) && $_GET['btn'] == 'create' && bYN(GetParm('ATM_CAI', 'N')) && $listnumall > 0)
    {
        echo '<input type="button" value="' . _tk('create_assessment_instances') . '" onclick="SendTo(scripturl+\'?action=newcreateinstances\');" />';
    }

    // Create "Create assignments" button for assessment templates
    if ($module == 'ATM' && bYN(GetParm('ATM_AAT', 'N')))
    {
        echo '<input type="button" value="' . _tk('create_assigned_assessments') . '" onclick="PromptForAdmYear()" />';
    }

    // Create "Create assignments" button for locations
    if ($module == 'LOC' && bYN(GetParm('ATM_AAT', 'N')) && $_GET['creatingassignments'] == 1)
    {
        $adm_year = Sanitize::SanitizeString($_GET['adm_year']);
        echo '<input type="button" value="' . _tk('create_assigned_assessments') . '" onclick="SendTo(scripturl+\'?action=asmcreateassignments&adm_year='.$adm_year.'\');" />';
    }
?>
                                </div>
            <form method="post" name="timescaleform" action="<?= $timescales_url . "&query=$query_recordid".($LinkMode ? "&link=1" : "") . ($overdue ? "&overdue=1" : "") . (isset($_GET['btn']) && $_GET['btn'] == 'create' ? "&btn=create" : "")  ?>" style="display:inline">
                <input type="hidden" name="orderby" value="<?= $orderby ?>" />
                <input type="hidden" name="order" value="<?= $order ?>" />
                <input type="hidden" name="listnum" value="<?= $listnum ?>" />
                <input type="hidden" name="drillfield" value="<?php echo Escaper::escapeForHTMLParameter($_REQUEST['drillfield']); ?>" />
                <input type="hidden" name="drillvalue" value="<?php echo Escaper::escapeForHTMLParameter($_REQUEST['drillvalue']) ?>" />
                <input type="hidden" name="drilldatatype" value="<?php echo Escaper::escapeForHTMLParameter($_REQUEST['drilldatatype']) ?>" />
                <input type="hidden" name="drilldateoption" value="<?php echo Escaper::escapeForHTMLParameter($_REQUEST['drilldateoption']) ?>" />
                <input type="hidden" name="drillwhere" value="<?php echo Escaper::escapeForHTMLParameter($_REQUEST["drillwhere"]) ?>" />
                <input type="hidden" name="rows" value="<?php echo Escaper::escapeForHTMLParameter($_REQUEST["rows"]) ?>" />
                <input type="hidden" name="saved_query" value="<?php echo Escaper::escapeForHTMLParameter($_REQUEST['saved_query']) ?>" />
                <?php if ($_REQUEST['fromcrosstab'] == '1') : ?>
			        <input type="hidden" name="fromcrosstab" value="1" />			        
			    <?php elseif ($_REQUEST["from_report"] == '1'): ?>
			     	<input type="hidden" name="from_report" value="1" />
			    <?php elseif ($_REQUEST["overdue"] == '1'): ?>
			     	<input type="hidden" name="overdue" value="1" />
			    <?php endif; ?>
                
            </form>
<?php
    if (!$LinkMode)
    {
	    footer();
    }
    else
    {
        echo getBasicFooter();
    }

    $_SESSION['LAST_PAGE'] = $scripturl . '?' . $_SERVER['QUERY_STRING'];

    // set the predefined listing type in the session, so we can subsequently set the property on SavedQuery objects
    if (!$ModuleDefs[$module]['LISTING_FILTERS'])
    {
        if (isset($ListRef))
        {
            $_SESSION[$module]['list_type'] = $ListRef;
        }
        else if ($ListType != 'search' && $ListType !== '')
        {
            $_SESSION[$module]['list_type'] = $ListType;
        }
        
        if (isset($_SESSION[$module]['list_type']))
        {
            // ensure any previous built where objects are removed when viewing a predefined listing
            $_SESSION[$module]['NEW_WHERE'] = null;
        }    
    }
    
	obExit();
}

function PageNumbers($listnum, $listnumall, $listdisplay)
{
        echo '<table width="100%" class="page-numbers">
            <tr>
                <td class="windowbg" width="5%" nowrap="nowrap">';

                if (($listnum - $listdisplay) > 0)
                {
                    echo '<a href="Javascript:pageing(\''.$orderby.'\',\''.$order.'\', \''.($listnum-$listdisplay).'\');"><b>'._tk('previous_page').'</b></a>';
                }

                echo'
                </td>
                <td class="windowbg" width="90%">';

                if ($listnumall > ($listnum - $listdisplay))
                {
                    $tmplistnum = $listnum;

                    if ($listdisplay != 0)
                    {
                        $pagenumber = ($tmplistnum/$listdisplay);
                    }

                    $CurrentPage = $pagenumber;
                    $maxpagenum = $pagenumber + 10;

                    if ($pagenumber > 10)
                    {
                        $pagenumber = $pagenumber - 10;
                        $tmplistnum = $listnum - (10 * $listdisplay);
                    }
                    else
                    {
                        $pagenumber = 1;
                        $tmplistnum = $listdisplay;
                    }

                    if ($CurrentPage > 11)
                    {
                        echo '<a href="Javascript:pageing(\''.$orderby.'\',\''.$order.'\', \''.($tmplistnum - $listdisplay).'\');"><b><< </b></a>';
                    }

                    while ($pagenumber <= $maxpagenum && $tmplistnum < ($listnumall + $listdisplay))
                    {
                        if ($pagenumber == $CurrentPage)
                        {
                            echo '<font size="3"><b>' . $pagenumber . ' </b></font>';
                        }
                        else
                        {
                            echo '<a href="Javascript:pageing(\''.$orderby.'\',\''.$order.'\', \''.($tmplistnum).'\');">'. ($pagenumber == $CurrentPage ? '<b>' . $pagenumber . '</b>' : $pagenumber) . ' </a>';
                        }

                        $pagenumber = $pagenumber + 1;
                        $tmplistnum = $tmplistnum + $listdisplay;
                    }

                    if ($tmplistnum < ($listnumall + $listdisplay))
                    {
                            echo '<a href="Javascript:pageing(\''.$orderby.'\',\''.$order.'\', \''.($tmplistnum).'\');"><b>>> </b></a>';
                    }

                    echo '</td>';

                    if ($listnumall > ($listnum))
                    {
                        echo '<td class="windowbg" width="5%" nowrap="nowrap">';
                        echo '<a href="Javascript:pageing(\''.$orderby.'\',\''.$order.'\', \''.($listnum+$listdisplay).'\');"><b>'._tk('next_page').'</b></a>';
                    }
                }

                echo '
                </td>
            </tr>';
echo '</table>';
}

/**
 * Finds the subset of a set of incident records that are overdue
 */
function getOverdueRecords($data)
{
    global $ModuleDefs;

    require_once 'Date.php';

    $recordIds = array();

    foreach ($data as $record)
    {
        $approvalStatuses[] = $record['rep_approved'];
        $recordIds[$record['rep_approved']][] = $record['recordid'];
        $recordData[$record['recordid']] = $record;
    }

    $approvalStatuses = array_unique($approvalStatuses);

    foreach ($approvalStatuses as $approvalStatus)
    {
        $OverdueDays = GetOverdueDays('INC', $approvalStatus);
        $RecordDate = new Date;

        //values cached in $_SESSION['CachedValues']['can_be_overdue'][$module][$approvalStatus][GetWorkflowID()]
        if ($_SESSION['CachedValues']['can_be_overdue']['INC'][$approvalStatus][GetWorkflowID('INC')])
        {
            $row = $_SESSION['CachedValues']['can_be_overdue']['INC'][$approvalStatus][GetWorkflowID('INC')];
        }
        else
        {
            $row = DatixDBQuery::PDO_fetch('SELECT can_be_overdue FROM code_approval_status WHERE module = :module AND code = :code AND workflow = :workflow', array('module' => 'INC', 'code' => $approvalStatus, 'workflow' => GetWorkflowID('INC')));
            $_SESSION['CachedValues']['can_be_overdue']['INC'][$approvalStatus][GetWorkflowID('INC')] = $row;
        }

        if ($row)
        {
            if ($row['can_be_overdue'] == 1)
            {
                if ($OverdueDays[$approvalStatus] >= 0)
                {
                    // we have an overdue timescale specific to this approval status
                    if ($OverdueDays[$approvalStatus . '_type'] == 'W')
                    {
                        // we're calculating the overdue date based on working days
                        $OverdueDays[$approvalStatus] = CalculateWorkingDays($OverdueDays[$approvalStatus], new DateTime(), true);
                    }

                    $OverdueDate = SubtractDays($OverdueDays[$approvalStatus]);
                    $OverdueDate = new Date(removeTimeFromDate($OverdueDate->getDate()));

                    if (empty($OverdueDays['overdue_date_field']))
                    {
                        $minMax = 'MAX'; // may eventually be made user-configurable (see function getOverdueSQL)
                        $sql = 'SELECT recordid, DATEADD(day, DATEDIFF(day, \'20000101\', ' . $minMax . '([date])), \'20000101\') AS auditdate
                                FROM inc_status_audit
                                WHERE recordid IN (' . implode(',', $recordIds[$approvalStatus]) . ')
                                AND [status] = :status
                                GROUP BY [recordid]';

                        $overdueData = DatixDBQuery::PDO_fetch_all($sql, array('status' => $approvalStatus), PDO::FETCH_KEY_PAIR);

                        foreach ($recordIds[$approvalStatus] as $recordid)
                        {
                            if (empty($overdueData[$recordid]) && $recordData[$recordid][$ModuleDefs['INC']['OVERDUE_CHECK_FIELD']] != '')
                            {
                                // the record has no audit entry, so we need to ensure we're checking against the OVERDUE_CHECK_FIELD
                                $overdueData[$recordid] = $recordData[$recordid][$ModuleDefs['INC']['OVERDUE_CHECK_FIELD']];
                            }

                            if (!empty($overdueData[$recordid]))
                            {
                                $ConvertedDate = strtotime($overdueData[$recordid]);
                                $RecordDate->setDate($ConvertedDate);

                                if (($OverdueDays[$approvalStatus . '_type'] == 'W' && $OverdueDate->compare($RecordDate, $OverdueDate) < 0) ||
                                    ($OverdueDays[$approvalStatus . '_type'] != 'W' && $OverdueDate->compare($RecordDate, $OverdueDate) <= 0)                                )
                                {
                                    $overdueRecords[] = $recordid;
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach ($recordIds[$approvalStatus] as $recordid)
                        {
                            // we've specified a field to calculate the overdue date from for this approval status
                            if ($record[$OverdueDays['overdue_date_field']] != '')
                            {
                                $ConvertedDate = strtotime($record[$OverdueDays['overdue_date_field']]);
                                $RecordDate->setDate($ConvertedDate);

                                if (($OverdueDays[$approvalStatus . '_type'] == 'W' && $OverdueDate->compare($RecordDate, $OverdueDate) < 0) ||
                                    ($OverdueDays[$approvalStatus . '_type'] != 'W' && $OverdueDate->compare($RecordDate, $OverdueDate) <= 0))
                                {
                                    $overdueRecords[] = $recordid;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if ($OverdueDays['overall'] >= 0)
    {
        if ($OverdueDays['overall_type'] == 'W')
        {
            // we're calculating the overdue date based on working days
            $OverdueDays['overall'] = CalculateWorkingDays($OverdueDays['overall'], new DateTime(), true);
        }

        $OverdueDate = SubtractDays($OverdueDays['overall']);
        $OverdueDate = new Date(removeTimeFromDate($OverdueDate->getDate()));

        foreach ($recordData as $recordid => $record)
        {
            $row = $_SESSION['CachedValues']['can_be_overdue']['INC'][$recordData[$recordid]['rep_approved']][GetWorkflowID('INC')];

            if ($row && $row['can_be_overdue'] == '1')
            {
                if ($record[$ModuleDefs['INC']['OVERDUE_CHECK_FIELD']] != '')
                {
                    $ConvertedDate = strtotime($record[$ModuleDefs['INC']['OVERDUE_CHECK_FIELD']]);
                    $RecordDate->setDate($ConvertedDate);

                    if (($OverdueDays['overall_type'] == 'W' && $OverdueDate->compare($RecordDate, $OverdueDate) < 0) ||
                        ($OverdueDays['overall_type'] != 'W' && $OverdueDate->compare($RecordDate, $OverdueDate) <= 0)
                    )
                    {
                        $overdueRecords[] = $record['recordid'];
                    }
                }

            }
        }
    }
    return array_unique($overdueRecords);
}
