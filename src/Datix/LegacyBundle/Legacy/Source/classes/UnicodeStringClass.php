<?php

class UnicodeString
{
    /**
     * Multibyte safe version of ucfirst
     * @param string $string
     * @return string
     */
    public static function ucfirst ($string)
    {
        $firstChar = self::strtoupper (self::substr ($string, 0, 1, 'UTF-8'), 'UTF-8');
        return $firstChar . self::substr ($string, 1, self::strlen($string) - 1, 'UTF-8'); 
    }

    /**
     * Multibyte safe version of ucwords
     * @param string $string
     * @return string
     */
    public static function ucwords ($string)
    {
        return mb_convert_case ($string, MB_CASE_TITLE, 'UTF-8');
    }

    /**
     * Multibyte safe version of str_split
     * @param string $string
     * @param integer $split_length
     * @return array
     */
    public static function str_split ($string, $split_length = -1) 
    {
        if ($split_length == -1) {
            $split_length = 1;
        }

        for ($i = 0, $len = self::strlen ($string, 'UTF-8'); $i < $len; $i += $split_length) {
            $array[] = self::substr ($string, $i, $split_length, 'UTF-8');
        }

        return $array;
    } 

    /**
     * Multibyte safe version of str_ireplace
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @param string $replacements
     * @return string
     */
    public static function str_ireplace ($search, $replace, $subject, &$replacements = null) 
    {
        if (is_array ($search)) 
        {
            foreach ($search as $key => $val) {
                $val = str_replace ('/', '\/', preg_quote ($val));
                $search[$key] = '/'. $val.'/ui'; 
            }
        } 
        else 
        {
            $search = str_replace ('/', '\/', preg_quote ($search));
            $search = '/'.$search.'/ui'; 
        }

        return preg_replace ($search, $replace, $subject, -1, $replacements);
    } 

    /**
     * Multibyte safe version of strcasecmp
     * @param string $str1
     * @param string $str2
     * @return string
     */
    public static function strcasecmp ($str1, $str2) 
    {
        return strcmp (self::strtoupper ($str1, 'UTF-8'), self::strtoupper($str2, 'UTF-8'));
    }

    /**
     * Multibyte safe version of trim
     * @param string $string
     * @param string $charlist
     * @return string
     */
    public static function trim ($string, $charlist = '') 
    {
        if (empty($charlist)) {
            return trim ($string);
        } else {
            $charlist = str_replace ('/', '\/', preg_quote ($charlist));
            return preg_replace ("/(^[$charlist]+)|([$charlist]+$)/us", '', $string);
        }
    }

    /**
     * Multibyte safe version of ltrim
     * @param string $string
     * @param string $charlist
     * @return string
     */
    public static function ltrim ($string, $charlist = '') 
    {
        if (empty($charlist)) {
            return ltrim ($string);
        } else {
            $charlist = str_replace ('/', '\/', preg_quote ($charlist));
            return preg_replace ("/^[$charlist]+/us", '', $string);
        }
    }

    /**
     * Multibyte safe version of rtrim
     * @param string $string
     * @param string $charlist
     * @return string
     */
    public static function rtrim ($string, $charlist = '') 
    {
        if (empty($charlist)) {
            return rtrim ($string);
        } else {
            $charlist = str_replace ('/', '\/', preg_quote ($charlist));
            return preg_replace ("/[$charlist]+$/us", '', $string);
        }
    }

    /**
     * Multibyte safe version of substr_replace
     * @param mixed $string
     * @param mixed $replacement
     * @param mixed $length
     * @return mixed
     */
    public static function substr_replace ($string, $replacement, $start, $length = null) 
    {
        if (is_array($string)) 
        {
            foreach ($string as $i => $val)
            {
                $repl = is_array ($replacement) ? $replacement[$i] : $replacement;
                $st   = is_array ($start) ? $start[$i] : $start;
                $len  = is_array ($length) ? $length[$i] : $length;
                
                $string[$i] = self::substr_replace ($val, $repl, $st, $len);
            }

            return $string;
        }

        if (is_null($length)) {
            $length = self::strlen ($string);
        }

        // negative start
        if ($start < 0) {
            $start = $length + $start;
        }

        $result  = self::substr ($string, 0, $start, 'UTF-8');
        $result .= $replacement;

        $result .= self::substr ($string, ($start+$length), self::strlen($string, 'UTF-8'), 'UTF-8');

        return $result;
    }

    /**
     * Multibyte safe version of wordwrap
     * @param string $string
     * @param int $width
     * @param char $break
     * @param bool $cut
     * @return string
     */
    public static function wordwrap ($string, $width = 75, $break = "\n", $cut = false)
    {
        // TODO: Edge cases not supported: 
        //  * $break with multiple characters
        //  * multiple spaces in strings aren't kept
        
        $words = preg_split ("/ /u", $string);

        $new_string = '';
        $line = '';

        foreach ($words as $word) 
        {
            $separator = empty ($line) ? '' : ' ';
            $word = empty ($word) ? ' ' : $word;

            // word has new lines
            if (preg_match ("/$break/u", $word)) {

                // split word
                $chunks     = preg_split ("/$break/u", $word);
                $last_chunk = array_pop ($chunks);
                $line      .= $break. implode ($break, $chunks);

                $word = $last_chunk;
            }

            // long words
            if (self::strlen ($word, 'UTF-8') > $width) {

                // split word
                if ($cut) {
                    $chunks     = self::str_split($word, $width);
                    $last_chunk = array_pop($chunks);
                    $line      .= (empty($line) ? '' : $break) . implode($break, $chunks);

                    $word = $last_chunk;
                } 

                if (!empty($line)) {
                    $new_string .= $line . $break;
                }

                $line = $word;
            }

            // word doesn't fit? wrap
            elseif (self::strlen ($line.$separator.$word, 'UTF-8') > $width) {
                $new_string .= $line . $break;
                $line = $word;
            } 

            // add space
            else {
                $line .= $separator . $word;
            }
        }

        $new_string .= $line;

        $result = self::rtrim ($new_string, $break);
        return $result;
    }

    /** 
     * Alias for mb_substr function
     */
    public static function substr ($string, $start, $length = NULL)
    {
        return mb_substr ($string, $start, $length, 'UTF-8');
    }

    /** 
     * Alias for mb_convert_case function
     */
    public static function convert_case ($string, $mode)
    {
        return mb_convert_case ($string, $mode, 'UTF-8');
    }

    /** 
     * Alias for mb_strtolower function
     */
    public static function strtolower ($string)
    {
        return mb_strtolower ($string, 'UTF-8');
    }

    /** 
     * Alias for mb_strtoupper function
     */
    public static function strtoupper ($string)
    {
        return mb_strtoupper ($string, 'UTF-8');
    }

    /** 
     * Alias for mb_strstr function
     */
    public static function strstr ($haystack, $needle, $before_needle = false)
    {
        return mb_strstr ($haystack, $needle, $before_needle, 'UTF-8');
    }

    /** 
     * Alias for mb_strpos function
     */
    public static function strpos ($haystack, $needle, $offset = 0)
    {
        return mb_strpos ($haystack, $needle, $offset, 'UTF-8');
    }

    /** 
     * Alias for mb_stripos function
     */
    public static function stripos ($haystack, $needle, $offset = 0)
    {
        return mb_stripos ($haystack, $needle, $offset, 'UTF-8');
    }

    /** 
     * Alias for mb_strlen function
     */
    public static function strlen ($string)
    {
        return mb_strlen ($string, 'UTF-8');
    }

    /**
     * Alias for mb_strrpos function
     */
    public static function strrpos ($string, $needle, $offset = 0)
    {
        return mb_strrpos ($string, $needle, $offset, 'UTF-8');
    }

    /** 
     * Alias for mb_send_mail function
     *
     * Note: We are using the default mail function because function mb_send_mail is causing problems with the
     * base64 encoding of the body. As long as the header UTF-8 goes in the mail all characters are displayed.
     */
    public static function mail ($to, $subject, $message, $additional_headers = null, $additional_parameter = null)
    {
        return mail($to, $subject, $message, $additional_headers, $additional_parameter);
    }
    
    /**
     * Wrapper for the multibyte module functions 
     */
    public static function __callStatic ($name, $arguments)
    {
        return call_user_func_array ('mb_'.$name, $arguments);
    }


    /**
     * Multibyte safe version of ord
     * @param string $char
     * @return int decimal unicode code
     */
    public static function ord ($char) 
    {
        // just get the first character
        $char = mb_substr ($char, 0, 1, 'UTF-8');

        $hex_code = bin2hex ($char);
        return hexdec ($hex_code);
    }

    /**
     * Multibyte safe version of chr
     * @param int $code decimal unicode code
     * @return string char
     */
    public static function chr ($code) 
    {
        return pack('H*', dechex($code));
    }
}

