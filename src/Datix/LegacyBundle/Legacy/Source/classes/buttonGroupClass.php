<?php

/**
* @desc Models a group of buttons, as you might see at the bottom of a form.
*/
class ButtonGroup
{
    //Array of Button{} objects representing the buttons in this group
    protected $Buttons = array();

    /**
    * @var array
    * Button objects used for Next/Previous links.
    */
    protected $NavigationButtons = array();

    protected $Module;

    /**
    * @desc Empty constructor - nothing to set
    */
    public function __construct($Parameters = array())
    {
        $this->Module = $Parameters['module'];
    }

    /**
    * @desc Used to add a button object to the Button array
    *
    * @param array $Parameters Array of parameters to use when creating the new button object.
    */
    public function AddButton($Parameters)
    {
        $this->Buttons[] = new Button($Parameters);
    }

    /**
    * @desc Checks whether navigation buttons exist - needed to determine height of the toolbar.
    *
    * @return bool
    */
    public function NavigationButtonsExist()
    {
        return !empty($this->NavigationButtons);
    }

    /**
     * @param $module
     * @param $recordid
     *
     * Constructs recordlist object for the navigation buttons where specific behaviour is required (i.e. not just cycling
     * through the current search results).
     */
    public function AddModuleSpecificNavigationButtons($module, $recordid)
    {
        if($module == 'AQU')
        {
            $RecordList = new RecordLists_ModuleRecordList();
            $RecordList->Module = 'AQU';
            $RecordList->WhereClause = 'asm_module_id = (SELECT asm_module_id FROM asm_data_questions WHERE recordid = '.Sanitize::SanitizeInt($recordid).')';
            $OrderByField = new Listings_ListingColumn('recordid', 'asm_questions');
            $OrderByField->setAscending();
            $RecordList->OrderBy[] = $OrderByField;
            $RecordList->RetrieveRecords();

            $this->AddNavigationButtons($RecordList, $recordid, $module);
        }
        if($module == 'ATQ')
        {
            $RecordList = new RecordLists_ModuleRecordList();
            $RecordList->Module = 'ATQ';
            $RecordList->WhereClause = 'asm_module_template_id = (SELECT asm_module_template_id FROM asm_question_templates WHERE recordid = '.Sanitize::SanitizeInt($recordid).')';
            $OrderByField = new Listings_ListingColumn('recordid', 'asm_question_templates');
            $OrderByField->setAscending();
            $RecordList->OrderBy[] = $OrderByField;
            $RecordList->RetrieveRecords();

            $this->AddNavigationButtons($RecordList, $recordid, $module);
        }
    }

    /**
    * @desc Used to add navigation buttons (First/Previous/Next/Last) to the object.
    *
    * @param array $RecordList List of records to model.
    */
    public function AddNavigationButtons($RecordList, $recordid, $module = '')
    {
        global $ModuleDefs;

        $CurrentRecordIndex = $RecordList->getRecordIndex(array('recordid' => $recordid));

        if($CurrentRecordIndex !== false)
        {
            require_once('StepButton.php');

            $href = "javascript:if(CheckChange()){UnlockRecord('".$ModuleDefs[$module]['TABLE']."', ".$recordid.");SendTo('" .getRecordURL(array('module' => $module, 'recordid' => $RecordList->getRecordidByIndex(0))).($_GET['from_parent_record'] ? '&from_parent_record=1' : '').($_REQUEST['fromsearch'] ? '&fromsearch=1' : '').($_GET['from_report'] ? '&from_report=1' : '')."');}";
            $this->NavigationButtons[] = new StepButton(
                $href,
                array(
                    'label' => 'First',
                    'action' => 'first',
                    'module' => $module,
                    'recordid' => $recordid
                ),
                ($CurrentRecordIndex != 0) ? true : false
            );

            $PreviousRecordid = $RecordList->getRecordidByIndex($CurrentRecordIndex - 1);
            $href = "javascript:if(CheckChange()){UnlockRecord('".$ModuleDefs[$module]['TABLE']."', ".$recordid.");SendTo('" .getRecordURL(array('module' => $module, 'recordid' => $PreviousRecordid)).($_GET['from_parent_record'] ? '&from_parent_record=1' : '').($_REQUEST['fromsearch'] ? '&fromsearch=1' : '').($_GET['from_report'] ? '&from_report=1' : '')."');}";
            $this->NavigationButtons[] = new StepButton(
                $href,
                array(
                    'label' => 'Previous',
                    'action' => 'prev',
                    'module' => $module,
                    'recordid' => $recordid
                ),
                (isset($PreviousRecordid)) ? true : false
            );

            $NextRecordid = $RecordList->getRecordidByIndex($CurrentRecordIndex + 1);
            $href = "javascript:if(CheckChange()){UnlockRecord('".$ModuleDefs[$module]['TABLE']."', ".$recordid.");SendTo('" .getRecordURL(array('module' => $module, 'recordid' => $NextRecordid)).($_GET['from_parent_record'] ? '&from_parent_record=1' : '').($_REQUEST['fromsearch'] ? '&fromsearch=1' : '').($_GET['from_report'] ? '&from_report=1' : '')."');}";
            $this->NavigationButtons[] = new StepButton(
                $href,
                array(
                    'label' => 'Next',
                    'action' => 'next',
                    'module' => $module,
                    'recordid' => $recordid
                ),
                (isset($NextRecordid)) ? true : false
            );

            $href = "javascript:if(CheckChange()){UnlockRecord('".$ModuleDefs[$module]['TABLE']."', ".$recordid.");SendTo('" .getRecordURL(array('module' => $module, 'recordid' => $RecordList->getRecordidByIndex(count($RecordList->Records)-1))).($_GET['from_parent_record'] ? '&from_parent_record=1' : '').($_REQUEST['fromsearch'] ? '&fromsearch=1' : '').($_GET['from_report'] ? '&from_report=1' : '')."');}";
            $this->NavigationButtons[] = new StepButton(
                $href,
                array(
                    'label' => 'Last',
                    'action' => 'last',
                    'module' => $module,
                    'recordid' => $recordid
                ),
                ($CurrentRecordIndex != count($RecordList->Records) -1) ? true : false
            );
        }
    }

    /**
    * @desc Gets the HTML for a div containing HTML button elements. Used at the bottom of forms.
    *
    * @return string The HTML for a div containing the buttons from this group.
    */
    public function getHTML()
    {
        $HTML = '<div class="button_wrapper">';

        foreach ($this->Buttons as $Button)
        {
            $HTML .= $Button->getHTML();
        }

        if (!empty($this->NavigationButtons)) {
            $HTML .= '<ul class="stepped">';

            foreach ($this->NavigationButtons as $Button)
            {
                $HTML .= '<li>' . $Button->getHTML() . '</li>';
            }

            $HTML .= '</ul>
            <script type="text/javascript">
            jQuery(document).ready(function()
            {
                jQuery(".stepped > li > a > img").hover(
                    function()
                    {
                        jQuery(this).css("border", "1px solid #828282");
                        if (jQuery(this).closest("li").next().length)
                        {
                            jQuery(this).closest("li").next().find("img").css("border-left", "none");
                        }
                        if (jQuery(this).closest("li").prev().length)
                        {
                            jQuery(this).closest("li").prev().find("img").css("border-right", "none");
                        }
                    },
                    function()
                    {
                        jQuery(this).css("border", "1px solid #bfbebe");
                    }
                );
            });
            </script>';
        }

        $HTML .= '</div>';

        return $HTML;
    }

    /**
    * @desc Gets the HTML for a collection of buttons represented by clickable images. Used in the menu floating to the left of forms.
    *
    * @return string The HTML for a set of images representing the buttons in this group.
    */
    public function getSideMenuHTML()
    {
        foreach ($this->Buttons as $Button)
        {
            $HTML .= $Button->getSideMenuHTML();
        }

        if (!empty($this->NavigationButtons))
        {
            $HTML .= '<div style="padding-top:3px">';

            foreach ($this->NavigationButtons as $Button)
            {
                $HTML .= $Button->getSideMenuHTML();
            }

            $HTML .= '</div>';
        }

        return $HTML;
    }

    /**
    * @desc Generic function used to create a standard button group for standard forms based on the formtype and module. Returns nothing, but
    * populates the Buttons array for this object.
    *
    * @param array $aParams Array of parameters
    * @param string $aParams[form_id] The "id" attribute of the HTML form that these buttons relate to.
    * @param string $aParams[formtype] The type of form we are looking at ("New", "Edit", "Search", "ReadOnly", "Print"...).
    * @param array $aParams[data] Data array available for the current form.
    */
    public function PopulateWithStandardFormButtons($aParams)
    {
        global $ModuleDefs, $scripturl;

        $FormIdentifier = ($aParams['form_id'] ? 'jQuery(\'#'.$aParams['form_id'].'\')' : 'document.forms[0]');

        $aParams['perms'] = GetParm($ModuleDefs[$aParams['module']]['PERM_GLOBAL']);

        if ($aParams['data']['rep_approved'] == 'REJECT' && $aParams['formtype'] != 'Search' && (CanEditRecord($aParams) || CanMoveRecord($aParams)))
        {
            // not an ideal solution, but some proper work needs to be done on re-structuring the
            // formtype/formmode options available, since they are not currently particularly useful.
            $aParams['formtype'] = 'Edit';
        }

        switch ($aParams['formtype'])
        {
            case 'ReadOnly':
            case 'Locked':
                if($ModuleDefs[$aParams['module']]['LINKED_MODULE']['parent_ids'])
                {
                    $this->AddButton(array('label' => _tk('back_to').$ModuleDefs[$aParams['link_module']]['REC_NAME'], 'name' => 'btnCancel', 'id' => 'btnCancel', 'onclick' => 'SendTo(\''.getRecordURL(array('module' => $aParams['link_module'], 'recordid' => $aParams['main_recordid'], 'panel' => $ModuleDefs[$aParams['module']]['LINKED_MODULE']['panel'])).'\')', 'action' => 'BACK'));
                }
                $this->AddButton(array('label' => _tk('btn_cancel'), 'name' => 'btnCancel', 'id' => 'btnCancel', 'onclick' => 'submitClicked=true;jQuery(\'#rbWhat\').val(\'Cancel\');'.$FormIdentifier.'.submit();', 'action' => 'CANCEL'));
                break;

            case 'Search':
                $this->AddButton(array('label' => _tk('btn_search'), 'name' => 'btnSearch', 'id' => 'btnSearch', 'onclick' => 'submitClicked=true;selectAllMultiCodes();jQuery(\'#rbWhat\').val(\'Search\');'.$FormIdentifier.'.submit();', 'action' => 'SEARCH'));
                $this->AddButton(array('label' => _tk('btn_cancel'), 'name' => 'btnCancel', 'id' => 'btnCancel', 'onclick' => 'submitClicked=true;jQuery(\'#rbWhat\').val(\'Cancel\');'.$FormIdentifier.'.submit();', 'action' => 'CANCEL'));
                break;
            case 'Print': // no buttons
                break;

            default:

                $this->AddButton(array('label' => ButtonGroup::GetSaveLabel($aParams), 'name' => 'btnSave', 'id' => 'btnSave', 'onclick' => 'submitClicked=true;selectAllMultiCodes();if(validateOnSubmit()){ jQuery(\'#rbWhat\').val(\'Save\');writeSuffixLimits();'.$FormIdentifier.'.submit(); }', 'action' => 'SAVE'));

                if (!bYN(GetParm('DIF_1_NO_PRINT')) && !$_SESSION['logged_in'] && !(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()))
                {
                    $this->AddButton(array('label' => _tk('btn_submit_and_print_incident'), 'name' => 'btnSubmitPrint', 'id' => 'btnSubmitPrint', 'onclick' => '$(\'printAfterSubmit\').value = 1;jQuery(\'#rbWhat\').val(\'Save\'); submitClicked=true;selectAllMultiCodes();if(validateOnSubmit()){writeSuffixLimits();'.$FormIdentifier.'.submit();}', 'action' => 'SAVE'));
                }

                if($ModuleDefs[$aParams['module']]['LINKED_MODULE']['parent_ids'])
                {
                    if(CanDeleteRecord($aParams['module']) && $aParams['formtype'] != 'New')
                    {
                        $this->AddButton(array('label' => 'Delete '.$ModuleDefs[$aParams['module']]['REC_NAME'], 'name' => 'btnDelete', 'id' => 'btnDelete', 'onclick' => 'if(confirm(\'Are you sure you want to delete this record?\')){SendTo(\''.$scripturl.'?action=deleterecord&module='.$aParams['module'].'&recordid='.$aParams['data']['recordid'].'&link_module='.$aParams['link_module'].'&main_recordid='.$aParams['main_recordid'].'\')}', 'action' => 'DELETE'));
                    }
                    $this->AddButton(array('label' => _tk('btn_go_to').$ModuleDefs[$aParams['link_module']]['REC_NAME'], 'name' => 'btnGoTo', 'id' => 'btnGoTo', 'onclick' => 'javascript:if(CheckChange()){SendTo(\''.getRecordURL(array('module' => $aParams['link_module'], 'recordid' => $aParams['main_recordid'], 'panel' => $ModuleDefs[$aParams['module']]['LINKED_MODULE']['panel'])).'\');}', 'action' => 'BACK'));

                    if (isset($_GET['from_parent_record']) && $_GET['from_parent_record'] == '1')
                    {
                        $this->AddButton(array('label' => _tk('btn_cancel'), 'name' => 'btnCancel', 'id' => 'btnCancel', 'onclick' => 'if(confirm(\'Press \\\'OK\\\' to confirm or \\\'Cancel\\\' to return to the form\')){SendTo(\'' . getRecordURL(array('module' => $aParams['link_module'], 'recordid' => $aParams['main_recordid'], 'panel' => $ModuleDefs[$aParams['module']]['LINKED_MODULE']['panel'])) . '\')}', 'action' => 'CANCEL'));
                    }
                    else {
                        if ($this->hasKey($aParams, 'query_string'))
                        {
                            $queryString = http_build_query($aParams['cancel']['query_string']);
                            $this->AddButton([
                                'label' => _tk('btn_cancel'),
                                'name' => 'btnCancel',
                                'id' => 'btnCancel',
                                'onclick' => 'if(confirm(\'Press \\\'OK\\\' to confirm or \\\'Cancel\\\' to return to the form\')){SendTo(\'' . $scripturl . "?{$queryString}" . '\')}',
                                'action' => 'CANCEL'
                            ]);
                        }
                        else
                        {
                            $this->AddButton([
                                'label' => _tk('btn_cancel'),
                                'name' => 'btnCancel',
                                'id' => 'btnCancel',
                                'onclick' => 'if(confirm(\'Press \\\'OK\\\' to confirm or \\\'Cancel\\\' to return to the form\')){SendTo(\'' . $scripturl . '?action=list&module=' . $aParams['module'] . '&listtype=search\')}',
                                'action' => 'CANCEL'
                            ]);
                        }
                    }
                }
                else
                {
                    $this->AddButton(array('label' => _tk('btn_cancel'), 'name' => 'btnCancel', 'id' => 'btnCancel', 'onclick' => getConfirmCancelJavascript($aParams['form_id']), 'action' => 'CANCEL'));
                }
                break;
        }
		
        if ($_GET['fromlisting'])
        {
        	$this->AddButton(array('label' => _tk('btn_back_to_report'), 'name' => 'btnBack', 'id' => 'btnBack', 'onclick' => 'submitClicked=true;selectAllMultiCodes();jQuery(\'#rbWhat\').val(\'BackToListing\');'.$FormIdentifier.'.submit();', 'action' => 'BACK'));
        }
    }

    public static function GetSaveLabel($Parameters)
    {
        if($Parameters['level'] == 1)
        {
            $SaveLabel = FirstNonNull(array(_tk('btn_submit_'.$Parameters['module']), _tk('btn_submit')));
        }
        else
        {
            $SaveLabel = FirstNonNull(array(_tk('btn_save_'.$Parameters['module']), _tk('btn_save')));
        }

        return $SaveLabel;
    }

    private function hasKey(Array $array, $key)
    {
        if(array_key_exists($key, $array))
        {
            return true;
        }
        foreach($array as $k => $v)
        {
            if(!is_array($v))
            {
                continue;
            }
            if(array_key_exists($key, $v))
            {
                return true;
            }
        }
        return false;
    }
}

