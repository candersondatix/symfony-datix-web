<?php

use Source\classes\ASM\TemplateInstance;
use Source\classes\Filters\Container;
use src\framework\registry\Registry;
use src\email\EmailSenderFactory;

class ASM_Template extends Records_NestedRecord
{
    /**
    * Allows users to select whether they want to create assessment instances from
    * distributed assessments, or directly from the assessment templates themselves.
    */
    public static function SelectTemplateType()
    {
        global $scripturl, $DatixView;


        if (CanSeeModule('ATM') && !CanSeeModule('ATI'))
        {
            static::SelectLocations('ATM');
        }
        else if (!CanSeeModule('ATM') && CanSeeModule('ATI'))
        {
            static::SelectLocations('ATI');
        }
        else
        {
            GetPageTitleHTML(array(
                'title' => _tk('select_template_type'),
                'module' => 'AMO'
            ));

            GetSideMenuHTML(array('module' => 'AMO'));

            template_header_nopadding();

            $DatixView->typeField = Forms_SelectFieldFactory::createSelectField('template_type', '', '', '');
            $DatixView->typeField->setCustomCodes(array('ATM' => _tk('ATMNamesTitle'), 'ATI' => _tk('ATINamesTitle')));
            $DatixView->typeField->setSuppressCodeDisplay();

            echo $DatixView->render('classes/ASM/TemplateClass/SelectTemplateType.php');

            footer();
            obExit();
        }
    }

    public static function SelectLocations($templateType = '')
    {
        global $scripturl, $DatixView;

        if ($templateType == '')
        {
            if (isset($_POST['template_type']))
            {
                $templateType = $_POST['template_type'];
            }
            else
            {
                static::SelectTemplateType();
            }
        }


        GetPageTitleHTML(array(
             'title' => _tk('select_assessment_locations'),
             'module' => 'AMO'
             ));
        GetSideMenuHTML(array('module' => 'AMO'));

        template_header_nopadding();

        if ($templateType == 'ATM')
        {
            $DatixView->cycleYearField = Forms_SelectFieldFactory::createSelectField('ati_cycle', 'ATI', $_POST['ati_cycle'], '');
            $DatixView->cycleYearField->setSuppressCodeDisplay();

            $DatixView->dueDateField = new \FormField;
            $DatixView->dueDateField->MakeDateField('due_date', $_POST['due_date']);
        }

        $activeWhere = "(loc_active != 'N' OR  loc_active is null) AND (loc_active_inherit != 'N' OR  loc_active_inherit is null)";


        $secWhere = MakeSecurityWhereClause($activeWhere, 'LOC', $_SESSION['initials']) ?: '1=1';

        $locations = DatixDBQuery::PDO_fetch_all('SELECT recordid, loc_name, parent_id FROM vw_locations_main WHERE ' . $secWhere . ' ORDER BY lft', array());
        $locations = addUpperTierInfo($locations);

        $DatixView->Locations = $locations;
        $DatixView->scripturl = $scripturl;
        $DatixView->templateType = Sanitize::SanitizeString($templateType);

        echo $DatixView->render('classes/ASM/TemplateClass/SelectLocations.php');

        footer();
        obExit();
    }

    public static function SelectModules()
    {
        global $DatixView;

        // sanitize locations
        if (is_array($_POST['locations']))
        {
            foreach ($_POST['locations'] as $key => $value)
            {
                if (!ctype_digit($value))
                {
                    unset($_POST['locations'][$key]);
                }
            }
        }

        if (empty($_POST['template_type']))
        {
            static::SelectTemplateType();
        }

        if (isset($_POST['due_date']))
        {
            $dueDate = DateStrToUnixTimestamp($_POST['due_date']);
        }

        if ($_POST['template_type'] == 'ATI' && empty($_POST['locations']))
        {
            AddSessionMessage('ERROR', _tk('locations_error'));
            static::SelectLocations();
        }

        if (!validateMultipleTiers($_POST['locations']))
        {
            AddSessionMessage('ERROR', _tk('multiple_tiers_error'));
            static::SelectLocations();
        }

        if ($_POST['template_type'] == 'ATM' && (empty($_POST['locations']) || empty($_POST['ati_cycle']) || !$dueDate || $dueDate < time()))
        {
            AddSessionMessage('ERROR', _tk('cycle_locations_error'));
            static::SelectLocations();
        }


        GetPageTitleHTML(array(
             'title' => _tk('select_assessment_modules'),
             'module' => 'AMO'
             ));
        GetSideMenuHTML(array('module' => 'AMO'));

        template_header_nopadding();

        $DatixView->Modules = array();

        switch ($_POST['template_type'])
        {
            case 'ATM':
                $DatixView->Modules = static::getMasterTemplates($_POST['locations']);
                $DatixView->cycleYear = \Sanitize::SanitizeInt($_POST['ati_cycle']);
                $DatixView->dueDate = $dueDate;
                break;

            case 'ATI':
                $DatixView->Modules = static::getAssignedTemplates($_POST['locations']);
                break;
        }

        $DatixView->Locations = DatixDBQuery::PDO_fetch_all('SELECT recordid, loc_name FROM locations_main WHERE recordid IN('.implode(', ',$_POST['locations']).')', array(), PDO::FETCH_KEY_PAIR);
        $DatixView->templateType = $_POST['template_type'];

        echo $DatixView->render('classes/ASM/TemplateClass/SelectModules.php');

        footer();
        obExit();
    }

    /**
    * Retrieves master templates for a set of specified locations.
    *
    * The master templates returned are those which haven't yet been used to generate
    * assigned assessments or assessment instances for any of the selected locations.
    *
    * @param array $locations
    * @return array The master templates
    */
    public static function getMasterTemplates(array $locations)
    {
        $secWhere = MakeSecurityWhereClause('', 'LOC', $_SESSION['initials']) ?: '1=1';

        return DatixDBQuery::PDO_fetch_all("
            SELECT
                asm_template_modules.recordid, asm_template_modules.atm_title
            FROM
                asm_template_modules
            LEFT JOIN
                asm_templates ON asm_template_modules.recordid = asm_templates.asm_module_template_id AND asm_templates.ati_location IN (SELECT recordid FROM vw_locations_main WHERE recordid IN (".implode(',', $locations).") AND ".$secWhere.")
            LEFT JOIN
                asm_modules ON asm_template_modules.recordid = asm_modules.asm_template_instance_id AND asm_modules.ati_location IN (SELECT recordid FROM vw_locations_main WHERE recordid IN (".implode(',', $locations).") AND ".$secWhere.")
            WHERE
                asm_templates.recordid IS NULL
            AND
                asm_modules.recordid IS NULL
            ORDER BY
                asm_templates.asm_module_template_id
            ", array(), PDO::FETCH_KEY_PAIR);
    }

    /**
    * Retrieves assigned templates for a set of specified locations.
    *
    * The specified locations are either the common ancestors to all locations selected or, if
    * only one location has been selected, that location itself and all its ancestors.
    *
    * @param array $locations
    * @return array $templates The assigned templates
    */
    public static function getAssignedTemplates(array $locations)
    {
        $locHierarchy = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);
        $parentLocations = array();

        foreach ($locations as $location)
        {
            // retireve a list of ancestors for each selected location
            $parentLocations[] = array_keys($locHierarchy->getAncestors($location));
        }

        if (count($parentLocations) > 1)
        {
            // compile a list of locations which are common across all sets of ancestors
            $parentLocations = call_user_func_array('array_intersect', $parentLocations);
        }
        else
        {
            // only one location selected originally
            $parentLocations = array_shift($parentLocations);
        }

        if (empty($parentLocations))
        {
            $templates = array();
        }
        else
        {
            // retieve template instances assigned to each of the locations
            $secWhere = MakeSecurityWhereClause('', 'LOC', $_SESSION['initials']) ?: '1=1';
            $templates = DatixDBQuery::PDO_fetch_all("
                SELECT
                    asm_templates.recordid, asm_templates.atm_title
                FROM
                    asm_templates
                INNER JOIN
                    vw_locations_main ON asm_templates.ati_location = vw_locations_main.recordid
                LEFT JOIN
                    asm_modules ON asm_templates.recordid = asm_modules.asm_template_instance_id
                WHERE
                    ".$secWhere."
                AND
                    vw_locations_main.recordid IN (".implode(',', $parentLocations).")
                AND
                    asm_modules.recordid IS NULL
                ORDER BY
                    asm_templates.asm_module_template_id
                ", array(), PDO::FETCH_KEY_PAIR);
        }

        return $templates;
    }

    public static function CreateInstances($redirect = false, $redirectUrl = '')
    {
        global $yySetLocation;

        $yySetLocation = $redirectUrl;

        // sanitize modules
        if (is_array($_POST['modules']))
        {
            foreach ($_POST['modules'] as $key => $value)
            {
                if (!ctype_digit($value))
                {
                    unset($_POST['modules'][$key]);
                }
            }
        }

        // sanitize locations
        if (is_array($_POST['locations']))
        {
            foreach ($_POST['locations'] as $key => $value)
            {
                if (!ctype_digit($value))
                {
                    unset($_POST['locations'][$key]);
                }
            }
        }

        $moduleCount = static::createInstancesOnly($_POST['modules'], $_POST['locations']);

        unset($_POST);

        if ($moduleCount == 0)
        {
            AddSessionMessage('ERROR', _tk('locations_error'));
            $yySetLocation = 'app.php?action=list&module=LOC&listtype=search&sidemenu=ACR&btn=create';
        }
        else
        {
            AddSessionMessage('INFO', _tk('number_of_modules_created').$moduleCount);
        }

        if ($redirect && $redirectUrl != '')
        {
            redirectexit();
        }
        else
        {
            static::SelectLocations();
        }
    }


    /**
    * Creates assessment instances from a set of assigned assessments.
    *
    */
    public static function createInstancesOnly($modules, $locations)
    {
        global $yySetLocation;

        $insertModuleQuery = '
            INSERT INTO
                asm_data_modules (asm_template_instance_id, adm_location, rep_approved, updatedby, updateddate, adm_coordinator, generatedby)
            VALUES
                (:asm_template_instance_id, :adm_location, :rep_approved,  :updatedby, :updateddate, :adm_coordinator, :generatedby)';

        $insertQuestionQuery = '
            INSERT INTO
                asm_data_questions (asm_question_template_id, asm_module_id, updatedby, updateddate)
            VALUES
                (:asm_question_template_id, :asm_module_id, :updatedby, :updateddate)';

        $selectQuestionsQuery = '
            SELECT
                asm_template_questions.recordid
            FROM
                asm_template_questions
            INNER JOIN
                asm_templates ON asm_template_questions.asm_module_template_id = asm_templates.asm_module_template_id
            WHERE
                asm_templates.recordid = :id';

        $selectTemplateTitle = '
            SELECT
                atm_title
            FROM
                asm_templates
            WHERE
                recordid = :recordid
        ';

        $selectLocationName = '
            SELECT
                loc_name
            FROM
                vw_locations_main
            WHERE
                recordid = :recordid
        ';

        $moduleCount = 0;
        $updatedDate = GetTodaysDate();
        $instancesIds = '';

        if (is_array($modules))
        {
            foreach ($modules as $module)
            {
                $questions = DatixDBQuery::PDO_fetch_all($selectQuestionsQuery, array('id' => $module), PDO::FETCH_COLUMN);

                if (is_array($locations) && static::validateExistingRecords(array('locations' => $locations, 'assigned_assessment_instance_id' => $module, 'table' => 'asm_templates')))
                {
                    foreach ($locations as $location)
                    {
                        // Only generate instances for locations that are enabled
                        if (static::isLocationEnabled($location))
                        {
                            $templateTitle = DatixDBQuery::PDO_fetch($selectTemplateTitle, array('recordid' => $module), PDO::FETCH_COLUMN);
                            $locationName = DatixDBQuery::PDO_fetch($selectLocationName, array('recordid' => $location), PDO::FETCH_COLUMN);

                            $moduleInstanceID = DatixDBQuery::PDO_insert($insertModuleQuery, array(
                                'asm_template_instance_id' => $module,
                                'adm_location' => $location,
                                'rep_approved' => 'OPEN',
                                'updatedby' => $_SESSION['initials'],
                                'updateddate' => $updatedDate,
                                'adm_coordinator' => $_SESSION['initials'],
                                'generatedby' => $_SESSION['initials']
                            ));

                            foreach ($questions as $question)
                            {
                                DatixDBQuery::PDO_insert($insertQuestionQuery, array(
                                    'asm_question_template_id' => $question,
                                    'asm_module_id' => $moduleInstanceID,
                                    'updatedby' => $_SESSION['initials'],
                                    'updateddate' => $updatedDate,
                                ));
                            }

                            static::emailUsers($location, $templateTitle, $locationName, $moduleInstanceID);

                            $moduleCount++;
                            $instancesIds.= $moduleInstanceID . ',';
                        }
                    }
                }
            }

            $instancesIds = \UnicodeString::substr($instancesIds, 0, -1);
            $yySetLocation.= '&instances=' . $instancesIds;
        }

        return $moduleCount;
    }


	/**
	 * Function to validate that it is not possible to generate instances for locations above (and in the same path as)
	 * locations for which an assigned template has been created when generating instances from an Assigned Assessment Template
	 *
	 * @param $data
	 * @return bool
	 */
	static function validateExistingRecords($data)
	{
	    $parentLocations = static::getParentLocations($data['assigned_assessment_instance_id'], $data['table']);

        // Safeguard for when there's no records
        if (count($parentLocations) == 0)
        {
            return true;
        }
	    else if (count(array_intersect($data['locations'], $parentLocations)) > 0)
	    {
	        return false;
	    }
	    else
	    {
	        return true;
	    }
	}



	function getParentLocations ($recordid, $table)
	{
	    // Get selected assigned assessment template location
	    $sql = '
	        SELECT
	            ati_location
	        FROM
	            ' . $table . '
	        WHERE
	            recordid = :recordid
	    ';

	    $location = DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid), PDO::FETCH_COLUMN);


        if (!empty($location))
        {
            $locHierarchy = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);
            $parentLocations = array();

            // retireve a list of ancestors for each selected location
            $parentLocations = array_keys($locHierarchy->getAncestors($location));

            // Remove the selected assigned assessment template location
            array_pop($parentLocations);

            return $parentLocations;
        }
        else
        {
            return array();
        }
	}

    public static function emailUsers($location, $templateTitle, $locationName, $recordid)
    {
        global $scripturl;

        require_once 'Source/libs/Email.php';

        $location = Sanitize::SanitizeString($location);
        $data = array();

        // Fields needed for default e-mail template text
        $data['scripturl'] = $scripturl . '?action=record&module=AMO&recordid=' . $recordid;
        $data['template_title'] = $templateTitle;
        $data['loc_name'] = $locationName;

        // Get users that have $location assigned
        $sql = "
            SELECT
                recordid,
                sta_title,
                sta_forenames,
                sta_surname,
                initials,
                email,
                fullname,
                con_dod,
                login
            FROM
                staff
            WHERE
                con_hier_location like '" . $location . "'
                OR
                con_hier_location like '" . $location . " %'
                OR
                con_hier_location like '% " . $location . "'
                OR
                con_hier_location like '% " . $location . " %'
        ";

        //TODO: Convert this sql to use a userCollection
        $Users = DatixDBQuery::PDO_fetch_all($sql);

        $userFactory = new \src\users\model\UserModelFactory();

        foreach ($Users as $User)
        {
            if ($User['email'] != '' && empty($User['con_dod']) && bYN(GetUserParm($User['login'], 'ASM_CREATE_INSTANCES_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('Assessment ' . $recordid . ' created to location ' . $data['loc_name'] . '. E-mailing users that have the assessment location.');

                $recipient = $userFactory->getMapper()->findById($User['recordid']);

                $emailSender = EmailSenderFactory::createEmailSender('AMO', 'NewInstances');
                $emailSender->addRecipient($recipient);
                $emailSender->sendEmails(array_merge($User, $data));
            }
        }
    }

    /**
     * @desc Function to validate that it is not possible to generate instances for locations above (and in the same path as)
     * locations for which an assigned template has been created when generating instances from an Assigned Assessment Template
     *
     * @param $modules
     * @param $locations
     * @return bool
     */
    public static function validateAssignedAssessmentsPath($modules, $locations)
    {
        $data = array();
        $data['locations'] = array();

        foreach($locations as $locationId)
        {
            array_push($data['locations'], $locationId);
        }

        foreach($modules as $assignedAssessmentId)
        {
            $data['assigned_assessment_instance_id'] = $assignedAssessmentId;
            $data['table'] = 'asm_templates';
            $data['locations'] = $locations;

            if (!static::validateExistingRecords($data))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * @desc Function to validate that it is not possible to generate instances for locations above (and in the same path as)
     * for which an instance has been created when generating instances from an Assigned Assessment Template
     *
     * @param $modules
     * @param $locations
     * @return bool
     */
    public static function validateAssessmentPath($modules, $locations)
    {
        $data = array();
        $data['locations'] = array();

        foreach($locations as $locationId)
        {
            array_push($data['locations'], $locationId);
        }

        foreach($modules as $assessmentId)
        {
            $data['assigned_assessment_instance_id'] = $assessmentId;
            $data['table'] = 'asm_modules';
            $data['locations'] = $locations;

            if (!static::validateExistingRecords($data))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * @desc Function that validates any location on the same path in the hierarchy as a location for which an instance
     * has already been generated for the same assessment year AND cycle year (both of which are pulled through from
     * the assigned assessment)
     *
     * @static
     * @param $modules
     * @param $location
     * @return bool
     */
    public static function validateGeneratedInstances($modules, $location)
    {
        foreach ($modules as $module)
        {
            // Get selected Assigned Assessment Template record data
            $sql = '
                SELECT
                    ati_cycle,
                    adm_year
                FROM
                    asm_templates
                WHERE
                    recordid = :recordid
            ';

            $assignedAssessmentData = DatixDBQuery::PDO_fetch($sql, array('recordid' => $module));

            // Get Assessment instances location for the selected Assigned Assessment Template
            $sql = '
                SELECT
                    adm_location
                FROM
                    asm_modules
                WHERE
                    asm_template_instance_id = :asm_template_instance_id
                    AND
                    ati_cycle = :ati_cycle
                    AND
                    adm_year = :adm_year
            ';

            $PDOParams = array(
                'asm_template_instance_id' => $module,
                'ati_cycle' => $assignedAssessmentData['ati_cycle'],
                'adm_year' => $assignedAssessmentData['adm_year']
            );

            $assessmentInstancesLocations = DatixDBQuery::PDO_fetch_all($sql, $PDOParams, PDO::FETCH_COLUMN);

            if (!empty($assessmentInstancesLocations))
            {
                $locHierarchy = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);

                foreach ($assessmentInstancesLocations as $assessmentInstancesLocation)
                {
                    $ancestors = $locHierarchy->getAncestors($assessmentInstancesLocation);
                    $descendants = $locHierarchy->getDescendants($assessmentInstancesLocation);

                    if (array_key_exists($location, $ancestors))
                    {
                        return false;
                    }

                    if (array_key_exists($location, $descendants))
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @desc Function that shows a location if it's a descendant of all the selected Assigned Assessment Templates
     *
     * @static
     * @param $assignedAssessments The selected Assigned Assessment Templates
     * @param $locationId The location to show
     * @return bool Show the location or not
     */
    public static function showLocationIfIsDescendant($assignedAssessments, $locationId)
    {
        $showLocation = array();
        $locHierarchy = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);

        foreach ($assignedAssessments as $assignedAssessmentId)
        {
            $sql = '
                SELECT
                    ati_location
                FROM
                    asm_templates
                WHERE
                    recordid = :recordid
            ';

            $assignedAssessmentLocationId = DatixDBQuery::PDO_fetch($sql, array('recordid' => $assignedAssessmentId), PDO::FETCH_COLUMN);
            $descendants = $locHierarchy->getDescendants($assignedAssessmentLocationId);

            if (array_key_exists($locationId, $descendants))
            {
                $showLocation[] = true;
            }
            else
            {
                $showLocation[] = false;
            }
        }

        if (in_array(false, $showLocation))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Checks if location is enabled.
     *
     * @param string $locationId The id of the location.
     * @return array
     */
    public static function isLocationEnabled($locationId)
    {
        $sql = "SELECT 1 FROM vw_locations_main WHERE recordid = :recordid AND (loc_active = 'Y' OR loc_active IS NULL)";

        $result = DatixDBQuery::PDO_fetch($sql, ['recordid' => $locationId]);

        return $result;
    }

}
