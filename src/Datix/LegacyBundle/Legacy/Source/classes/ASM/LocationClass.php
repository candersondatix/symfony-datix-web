<?php
/**
* Provides Assessment Module specific Location module functionality. 
*/
class ASM_Location
{
    public function filterLocations(PDOStatement $listingStatement)
    {
    	if ($_GET['btn'] != 'create')
    	{
    		// no need to execute this function if we're not in the process of generating records
    		return false;	
    	}
    	
        unset($_SESSION['LOC']['FILTEREDLOCATIONS']);

        $list_columns = Listings_ModuleListingDesign::GetListingColumns('LOC');

        $sql = 'SELECT ' . implode(', ', array_keys($list_columns)) . ' FROM vw_locations_main';

        if ($_SESSION['LOC']['WHERE'])
        {
            $sql.= '  WHERE ' . $_SESSION['LOC']['WHERE'];
        }

        $locations = DatixDBQuery::PDO_fetch_all($sql, array());

        foreach ($locations as $locationDetails)
        {
            if (ASM_Template::validateAssignedAssessmentsPath($_SESSION['ATI']['RECORDLIST']->FlaggedRecords, array(0 => $locationDetails['recordid'])))
            {
                if (ASM_Template::validateGeneratedInstances($_SESSION['ATI']['RECORDLIST']->FlaggedRecords, $locationDetails['recordid']))
                {
                    if ($_SESSION['LOC']['RECORDLIST']->isFlagged(array('recordid' => $locationDetails['recordid'])))
                    {
                        $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['CHECKED'] = true;
                    }
                    else
                    {
                        $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['CHECKED'] = false;
                    }

                    $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['DISABLED'] = false;
                    $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['GREYEDOUT'] = false;
                }
                else
                {
                    $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['CHECKED'] = false;
                    $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['DISABLED'] = true;
                    $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['GREYEDOUT'] = true;
                }
            }
            else
            {
                $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['CHECKED'] = false;
                $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['DISABLED'] = true;
                $_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['GREYEDOUT'] = true;
            }

            if ($_SESSION['LOC']['FILTEREDLOCATIONS'][$locationDetails['recordid']]['DISABLED'])
            {
                $_SESSION['LOC']['RECORDLIST']->UnFlagRecord($locationDetails['recordid']);
            }
        }
    }
}