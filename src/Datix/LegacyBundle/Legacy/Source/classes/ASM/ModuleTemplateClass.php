<?php

class ASM_ModuleTemplate extends ASM_Template
{
    /* the recordid of the outcome template record */
    public $recordid;

    // TODO: This function needs to be reviewed. I don't understand the purpose of it.
    public function __construct($recordid)
    {
        $this->recordid = $recordid;

        $this->Data = DatixDBQuery::PDO_fetch('SELECT recordid, atm_version, atm_category, atm_title, atm_description, atm_notes FROM asm_template_modules WHERE recordid = :recordid', array('recordid' => $this->recordid));
    }

    public function GetID()
    {
        return $this->recordid;
    }

    public static function LinkedQuestionSection($ID, $FormType)
    {
        global $DatixView, $scripturl;

        $ReadOnly = ($FormType == 'ReadOnly' || $FormType == 'Print');

        $Design = Listings_ModuleListingDesignFactory::getListingDesign(array('module' => 'ATQ', 'parent_module' => 'ATM', 'link_type' => 'asm_template_questions'));
        $Design->LoadColumnsFromDB(null, true);

        foreach ($Design->getColumns() as $key => $column)
        {
            if (preg_match('/^UDF.*_([0-9]+)$/ui', $column, $matches))
            {
                $udfFieldIDs[$column] = $matches[1];
                $listingColumns[] = 'null as '.$column;
            }
            else
            {
                $listingColumns[] = $column;
            }
        }

        $questions = DatixDBQuery::PDO_fetch_all('SELECT recordid, '.implode(', ', $listingColumns).' FROM asm_question_templates WHERE asm_module_template_id = :asm_module_template_id ORDER BY atq_order ASC, recordid DESC', array('asm_module_template_id' => $ID));

        foreach ($questions as $row)
        {
            $recordids[] = $row['recordid'];
        }

        $udfData = array();

        if (!empty($udfFieldIDs))
        {
            $udfData = getUDFData('ATQ', $recordids, $udfFieldIDs);
        }

        foreach ($questions as $id => $question)
        {
            foreach ($udfFieldIDs as $udf_field => $udf_id)
            {
                $questions[$id][$udf_field] = $udfData[$question['recordid']][$udf_id];
            }
        }

        $RecordList = new RecordLists_ModuleRecordList();
        $RecordList->Module = 'ATQ';
        $RecordList->Paging = false;
        $RecordList->AddRecordData($questions);

        $DatixView->ListingDisplay = new Listings_ListingDisplay($RecordList, $Design, '', $ReadOnly);
        $DatixView->ListingDisplay->RecordURLSuffix = '&from_parent_record=1';
        $DatixView->ID = $ID;
        $DatixView->scripturl = $scripturl;
        $DatixView->ReadOnly = $ReadOnly;

        echo $DatixView->render('classes/ASM/ModuleTemplateClass/LinkedQuestionSection.php');
   }
}