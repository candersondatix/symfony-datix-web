<?php
namespace Source\classes\ASM;

class Validator
{
	/**
	 * @desc
     * Takes a set of flagged records from an assessment template listing and attempts to combine
     * them with a given location to create a set of assigned assessments records.
	 * @param int $location
	 */
     public function validateLocation($location)
     {
        if (is_array($_SESSION['ATM']['RECORDLIST']->FlaggedRecords) && !empty($_SESSION['ATM']['RECORDLIST']->FlaggedRecords))
        {
        	$sql_check = '
            SELECT
                recordid
            FROM
                asm_template_instances
            WHERE
                asm_module_template_id = :asm_module_template_id AND ati_location = :ati_location AND ati_cycle = :ati_cycle AND adm_year = :adm_year';
        	
        	
        	$Cycle = \DatixDBQuery::PDO_fetch('SELECT TOP 1 code FROM code_asm_cycle WHERE cod_priv_level IS NULL OR cod_priv_level = \'\' OR cod_priv_level = \'Y\' ORDER BY cod_listorder asc, code asc', array(), \PDO::FETCH_COLUMN);
        	
            foreach ($_SESSION['ATM']['RECORDLIST']->FlaggedRecords as $TemplateRecordid)
            {
                if (!empty($location))
                {
                		$LocationRecordid = $location;

                        $check_array = array(
                            'asm_module_template_id' => $TemplateRecordid,
                            'ati_cycle' => $Cycle,
                            'ati_location' => $LocationRecordid,
                        	'adm_year' => $_GET['adm_year'],
                        );

                        //check no matching assignments above or below location
                        $assignmentsBelow = \DatixDBQuery::PDO_fetch(
                            'SELECT count(*) as num FROM asm_templates
                            JOIN locations_main ON ati_location = locations_main.recordid
                            WHERE asm_module_template_id = :asm_module_template_id AND ati_cycle = :ati_cycle AND adm_year = :adm_year
                            AND
                            (
                                (
                                    locations_main.lft > (SELECT lft FROM locations_main WHERE recordid = :loc_recordid)
                                    AND locations_main.rght < (SELECT rght FROM locations_main WHERE recordid = :loc_recordid2)
                                )
                            OR
                            locations_main.recordid IN
                                (SELECT T2.recordid
                                FROM locations_main AS T1, locations_main AS T2
                                WHERE T1.lft BETWEEN T2.lft AND T2.rght
                                AND T1.recordid=:loc_recordid3)
                            )',
                            array('loc_recordid' => $LocationRecordid, 'loc_recordid2' => $LocationRecordid, 'loc_recordid3' => $LocationRecordid, 'asm_module_template_id' => $TemplateRecordid, 'ati_cycle' => $Cycle, 'adm_year' => $_GET['adm_year']),
                            \PDO::FETCH_COLUMN
                            );

                        if($assignmentsBelow != 0)
                        {
          					return false;
                        }

                		//check no matching instances above or below location
                        $instancesBelow = \DatixDBQuery::PDO_fetch(
                            'SELECT count(*) as num FROM asm_modules
                            JOIN locations_main ON adm_location = locations_main.recordid
                            WHERE asm_module_template_id = :asm_module_template_id AND ati_cycle = :ati_cycle AND adm_year = :adm_year
                            AND
                            (
                                (
                                    locations_main.lft > (SELECT lft FROM locations_main WHERE recordid = :loc_recordid)
                                    AND locations_main.rght < (SELECT rght FROM locations_main WHERE recordid = :loc_recordid2)
                                )
                            OR
                            locations_main.recordid IN
                                (SELECT T2.recordid
                                FROM locations_main AS T1, locations_main AS T2
                                WHERE T1.lft BETWEEN T2.lft AND T2.rght
                                AND T1.recordid=:loc_recordid3)
                            )',
                            array('loc_recordid' => $LocationRecordid, 'loc_recordid2' => $LocationRecordid, 'loc_recordid3' => $LocationRecordid, 'asm_module_template_id' => $TemplateRecordid, 'ati_cycle' => $Cycle, 'adm_year' => $_GET['adm_year']),
                            \PDO::FETCH_COLUMN
                            );

                        if($instancesBelow != 0)
                        {
                        	return false;
                        }
                        
                        
                        
                        //check no duplicates present
                        $loc_check = \DatixDBQuery::PDO_fetch($sql_check, $check_array, \PDO::FETCH_COLUMN);

                        if($loc_check)
                        {
                            return false;
                        }

                }
            }
            return true;
            
        }
     	
     }

}