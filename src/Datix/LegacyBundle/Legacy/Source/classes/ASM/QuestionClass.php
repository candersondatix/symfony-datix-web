<?php

class ASM_Question
{
    public function storeListing(PDOStatement $listingStatement)
    {
        if ($results = $listingStatement->fetchAll())
        {
            $recordIds = array();
            
            foreach($results as $result)
            {
                $recordIds[] = $result['recordid'];
            }
            
            $cachedListing = new RecordSet('AQU', $recordIds);
            
            // reset and re-execute the statement for the use in the listing code
            $listingStatement->closeCursor();
            $listingStatement->execute();
        }
    }
    
    public function redirect($recordId)
    {
        global $ModuleDefs, $yySetLocation;
        
        AddSectionMessage('question', 'INFO', htmlspecialchars(_tk('AQU_2_record_saved')));
        $yySetLocation = $scripturl . '?action=' . $ModuleDefs['AQU']['ACTION'] .'&recordid=' . $recordId.($_GET['from_parent_record'] ? '&from_parent_record=1' : '');
        redirectexit();
    }
}