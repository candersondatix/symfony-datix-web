<?php
/**  
* Helper class for all the low level NPSA data extraction and xml generation.
*/
class NPSA
{
    public $XMLErrors;
    
    public function __construct()
    {
        $this->XMLErrors = array();    
    }
    
    /**
    * This helper method is the core function in this class which calls all the submethods to extract the correct ItemCode for the given parameters.
    * 
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int $EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string $lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return array $XMLResponseTag The array stucture that will be written into the next line of the xml file
    * array('ReferenceCode' => $QuestionCode, 
    *       'ItemCode' => $ItemCode,
    *       'EntitySequence' => $EntitiySeq,
    *       'EntityDescription' => $EntityDescription,
    *       'TextArea' => $TextArea);
    */
    public function GetNPSAAnswer($nIncident, $QuestionCode, $EntitiySeq = 1, $EntityDescription = '', $lsTextArea = '', $nLinkID = '')
    {
        $EntityType = \UnicodeString::substr($QuestionCode, 0, 2);
        $TextArea = '';
        
        switch($EntityType)
        {
            case 'IN':
                $EntityDescription = 'Incident';
                $ItemCode = $this->GetXMLAnswerLine_IN($nIncident, $QuestionCode, $EntitiySeq, $EntityDescription, $lsTextArea, $nLinkID);
                break;
    
            case 'RP':
                $EntityDescription = 'Incident';
                $ItemCode = $this->GetXMLAnswerLine_RP($nIncident, $QuestionCode, $EntitiySeq, $EntityDescription, $lsTextArea, $nLinkID);
                break;
    
            case 'PD':
                $EntityDescription = 'Individual Patient';
                $ItemCode = $this->GetXMLAnswerLine_PD($nIncident, $QuestionCode, $EntitiySeq, $EntityDescription, $lsTextArea, $nLinkID);
                break;
    
            case 'MD':
                $EntityDescription = 'Incident';
                $ItemCode = $this->GetXMLAnswerLine_MD($nIncident, $QuestionCode, $EntitiySeq = 1, $EntityDescription, $lsTextArea, $nLinkID);
                break;
    
            case 'PG':
                $EntityDescription = 'Patient Group';
                $ItemCode = $this->GetXMLAnswerLine_PG($nIncident, $QuestionCode, $EntitiySeq = 1, $EntityDescription, $lsTextArea, $nLinkID);
                break;
    
            case 'DE':
                $EntityDescription = 'Incident';
                $ItemCode = $this->GetXMLAnswerLine_DE($nIncident, $QuestionCode, $EntitiySeq = 1, $EntityDescription, $lsTextArea, $nLinkID);
                break;
                    
            case 'ST':
                $EntityDescription = 'Staff';
                $ItemCode = $this->GetXMLAnswerLine_ST($nIncident, $QuestionCode, $EntitiySeq = 1, $EntityDescription, $lsTextArea, $nLinkID);
                break;
    
            default:
                break;
        }
        
        $XMLResponseTag = array('ReferenceCode' => $QuestionCode, 
                                'ItemCode' => $ItemCode,
                                'EntitySequence' => $EntitiySeq,
                                'EntityDescription' => $EntityDescription,
                                'TextArea' => $lsTextArea);
        
        if($ItemCode != '') 
        {
            return $XMLResponseTag;
        }
        else
        {
            return false;
        }      
    }
    
    /**
    * This helper method is the sub method of GetNPSAAnswer and in particular dealing with Incident fields (IN QuestionCodes)
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int &$EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return string $ItemCode The NPSA code for the given Datix code.
    */
    public function GetXMLAnswerLine_IN($nIncident, $QuestionCode, &$EntitiySeq = 1, $EntityDescription = '', &$lsTextArea, $nLinkID = '')
    {
        $lsTextArea = '';
        if($QuestionCode == 'IN01')
        {
            $lsTextArea = $this->GetDateData($nIncident, $lsTextArea, 'inc_dincident', 'incidents_main', $QuestionCode);
            $lsTextArea != '' ? $ItemCode = 'X' : $ItemCode = 'U';
            
        }
        elseif($QuestionCode == 'IN02' && $EntitiySeq == 1)
        {
            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_time', 'incidents_main', $QuestionCode);
            if($lsTextArea != '')
            {
                $ItemCode = $this->GetTimeHoursCode($lsTextArea);
                $lsTextArea = '';
            }
        }
        elseif($QuestionCode == 'IN02' && $EntitiySeq == 2)
        {
            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_time', 'incidents_main', $QuestionCode);
            if($lsTextArea != '')
            {
                $ItemCode = $this->GetTimeMinutesCode($lsTextArea);
                $lsTextArea = '';
            }
            $EntitiySeq = 1;
        }
        elseif($QuestionCode == 'IN02' && $EntitiySeq == 0)
        {
            $ItemCode = 'CU';
            $EntitiySeq = 1;
        }
        elseif($QuestionCode == 'IN03')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_loctype', 'incidents_main', $QuestionCode);
            $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_loctype');
        }
        elseif($QuestionCode == 'IN04')
        {
            $CountryValue = GetParm('COUNTRY','ENGLAND');
            switch($CountryValue)
            {
                case 'ENGLAND':
                    $ItemCode = 'A';
                    break;
                case 'NORTHERN IRELAND':
                    $ItemCode = 'B';
                    break;
                case 'SCOTLAND':
                    $ItemCode = 'C';
                    break;
                case 'WALES':
                    $ItemCode = 'D';
                    break;
                default:
                    $ItemCode = 'Z';
                    $lsTextArea = $CountryValue;
            }
        }                   
        elseif($QuestionCode == 'IN05')
        {
            if (bYN(GetParm('EXPORT_CCS2','N')))
            {
                // use ccs2 tier 3
                $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_type_tier_three', 'incidents_main', $QuestionCode);
                $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_type_tier_three');
            }
            else
            {
                // use ccs adverse event
                $ItemCode = $this->GetMultiCodeAnswerIN05($nIncident, $lsTextArea, 'inc_clintype', 'incidents_main', $QuestionCode);
                $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_clintype', 'See also Stage of Care and Detail fields');
            }
        }                   
        elseif($QuestionCode == 'IN06')
        {
            $ItemCode = $this->GetRootCodeAnswer($nIncident, $lsTextArea, 'inc_root_causes', 'incidents_main', $QuestionCode, $nLinkID);
        }                   
        elseif($QuestionCode == 'IN07')
        {
            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_notes', 'incidents_main', $QuestionCode);
            $ItemCode = 'A';
        }                   
        elseif($QuestionCode == 'IN10')
        {
            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_inv_action', 'incidents_main', $QuestionCode);
            $ItemCode = 'A';
        }                   
        elseif($QuestionCode == 'IN11')
        {
            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_inv_lessons', 'incidents_main', $QuestionCode);
            $ItemCode = 'A';
        }
        elseif($QuestionCode == 'IN27')
        {
            $ItemCode = 'Z';
            $lsTextArea = 'never event';
        }                             
                           
        $ItemCode == '' ? $ItemCode = 'C' : $ItemCode;
        return $ItemCode;
    }
                               
    /**
    * This helper method is the sub method of GetNPSAAnswer and in particular dealing with Incident fields (RP QuestionCodes)
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int &$EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return string $ItemCode The NPSA code for the given Datix code.
    */                             
    public function GetXMLAnswerLine_RP($nIncident, $QuestionCode, &$EntitiySeq = 1, $EntityDescription = '', &$lsTextArea = '', $nLinkID = '')
    {
        $ItemCode = '';

        if($QuestionCode == 'RP02')
        {
            $ItemCode = $this->GetStringData($nIncident, $lsTextArea, 'inc_unit_type', 'incidents_main', $QuestionCode);
        }

        $ItemCode == '' ? $ItemCode = 'C' : $ItemCode;

        return $ItemCode;
    }
    
    /**
    * This helper method is the sub method of GetNPSAAnswer and in particular dealing with Patient fields (PD QuestionCodes)
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int &$EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return string $ItemCode The NPSA code for the given Datix code.
    */ 
    public function GetXMLAnswerLine_PD($nIncident, $QuestionCode, &$EntitiySeq = 1, $EntityDescription = '', &$lsTextArea = '', $nLinkID = '')
    {
        $ItemCode = '';
        if($QuestionCode == 'PD01')
        {
            $lsTextArea = $this->GetDateData($nLinkID, $lsTextArea, 'con_dob', 'contacts_main', $QuestionCode);
            $lsTextArea != '' ? $ItemCode = 'X' : $ItemCode = 'U';
        }
        elseif($QuestionCode == 'PD02')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nLinkID, $lsTextArea, 'con_gender', 'contacts_main', $QuestionCode);
            if($ItemCode == '') 
            {
                $ItemCode = 'U';
            }
        }
        elseif($QuestionCode == 'PD04')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_paedspec', 'incidents_main', $QuestionCode);
            if($ItemCode == '') 
            {
                $ItemCode = $this->GetMultiCodeAnswerExtra($nIncident, $lsTextArea, 'inc_specialty', 'incidents_main', $QuestionCode, 'cod_npsa_pd04');
            }
            $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_n_paedspec', 'Alternatively fill in specialty field if mapped to NPSA codes');
        }
        elseif($QuestionCode == 'PD05')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_clinspec', 'incidents_main', $QuestionCode);
            if($ItemCode == '') 
            {
                $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_specialty', 'incidents_main', $QuestionCode);
            }
            $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_n_clinspec', 'Alternatively fill in specialty field if mapped to NPSA codes');                                       
        }
        elseif($QuestionCode == 'PD06')
        {
            $ItemCode = $this->GetLinkCodeAnswer($nIncident, 'inc_id', $lsTextArea, 'link_mhcpa', 'link_contacts', $QuestionCode, $nLinkID, 'con_id');
        }
        elseif($QuestionCode == 'PD07')
        {
            $lsTextArea = $this->getLinkStringData($nIncident, 'inc_id', 'link_mhact_section', 'link_contacts', $nLinkID, 'con_id');    
            $lsTextArea == '' ? $ItemCode = 'A' : $ItemCode = 'U'; 
        }
        elseif($QuestionCode == 'PD09')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_severity', 'incidents_main', $QuestionCode);
            /* ! Only if the patient is harmed i.e. PD16 is 'A', then PD09 is made mandatory */
            if (
                $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_patharm', 'incidents_main', $QuestionCode) == 'A' 
                || 
                (
                $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_patharm', 'incidents_main', $QuestionCode) == '' 
                    && 
                $this->GetMultiCodeAnswerExtra($nIncident, $lsTextArea, 'inc_result', 'incidents_main', 'PD16', 'cod_npsa_pd16') == 'A'
                )
            )
            {
                $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_severity');
            }
        }
        elseif($QuestionCode == 'PD10')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_effect', 'incidents_main', $QuestionCode);
            if($ItemCode == '')
            {
                $ItemCode = $this->GetMultiCodeAnswerExtra($nIncident, $lsTextArea, 'inc_result', 'incidents_main', $QuestionCode, 'cod_npsa_pd10');
            }
        }
        elseif($QuestionCode == 'PD11')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nLinkID, $lsTextArea, 'con_ethnicity', 'contacts_main', $QuestionCode);
        }
        elseif($QuestionCode == 'PD12')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_nearmiss', 'incidents_main', $QuestionCode);
            if($ItemCode == '')
            {
                $ItemCode = $this->GetMultiCodeAnswerExtra($nIncident, $lsTextArea, 'inc_result', 'incidents_main', $QuestionCode, 'cod_npsa_pd12');
            }
        }
        elseif($QuestionCode == 'PD14')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_actmin', 'incidents_main', $QuestionCode);
        }
        elseif($QuestionCode == 'PD16')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_patharm', 'incidents_main', $QuestionCode);
            if($ItemCode == '')
            {
                $ItemCode = $this->GetMultiCodeAnswerExtra($nIncident, $lsTextArea, 'inc_result', 'incidents_main', $QuestionCode, 'cod_npsa_pd16');
            }
            $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_n_patharm', 'Alternatively fill in the Result field if mapped to NPSA codes');
        }
        elseif($QuestionCode == 'PD20')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_paedward', 'incidents_main', $QuestionCode);
        }
        
        return $ItemCode;
    }
    
    /**
    * This helper method is the sub method of GetNPSAAnswer and in particular dealing with Incident fields (RP QuestionCodes)
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int &$EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return string $ItemCode The NPSA code for the given Datix code.
    */ 
    public function GetXMLAnswerLine_MD($nIncident, $QuestionCode, &$EntitiySeq = 1, $EntityDescription = '', &$lsTextArea = '', $nLinkID = '')
    {
        $medLinkId = $nLinkID;

        if ($medLinkId == '')
        {
            $sql = 'SELECT TOP 1 recordid FROM inc_medications WHERE inc_id = :inc_id ORDER BY listorder, recordid';
            $medLinkId = DatixDBQuery::PDO_fetch($sql, array('inc_id' => $nIncident), PDO::FETCH_COLUMN);
        }

        // determine if multi meds should be used (i.e. if global is set, and there is a medication record linked to this incident)
        $useMultiMeds = bYN(GetParm("MULTI_MEDICATIONS", "N")) && $medLinkId != '';
        
        // equivalent field names for single/multi meds
        $fieldNames = array(
            'single' => array(
                'MD01'   => 'inc_med_stage',
                'MD02'   => 'inc_med_error',
                'MD05'   => 'inc_med_drug',
                'MD07'   => 'inc_med_form',
                'MD07-B' => 'inc_med_form_rt',
                'MD08'   => 'inc_med_dose',
                'MD08-B' => 'inc_med_dose_rt',
                'MD16'   => 'inc_med_route',
                'MD16-B' => 'inc_med_route_rt',
                'MD30'   => 'inc_med_drug',
                'MD32'   => 'inc_med_form',
                'MD32-B' => 'inc_med_form_rt',
                'MD33'   => 'inc_med_dose',
                'MD33-B' => 'inc_med_dose_rt',
                'MD34'   => 'inc_med_route',
                'MD34-B' => 'inc_med_route_rt',
            ),
            'multi' => array(
                'MD01'   => 'imed_error_stage',
                'MD02'   => 'imed_error_type',
                'MD03'   => 'imed_other_factors',
                'MD04'   => 'imed_right_wrong_medicine_admin',
                'MD05'   => 'imed_name_admin',
                'MD06'   => 'imed_brand_admin',
                'MD07'   => 'imed_form_admin',
                'MD07-B' => 'imed_form_correct',
                'MD08'   => 'imed_dose_admin',
                'MD08-B' => 'imed_dose_correct',
                'MD10'   => 'imed_manu_admin',
                'MD11'   => 'imed_batch_no',
                'MD12'   => 'imed_manufacturer_special_admin',
                'MD16'   => 'imed_route_admin',
                'MD16-B' => 'imed_route_correct',
                'MD30'   => 'imed_name_admin',
                'MD31'   => 'imed_brand_admin',
                'MD32'   => 'imed_form_admin',
                'MD32-B' => 'imed_form_correct',
                'MD33'   => 'imed_dose_admin',
                'MD33-B' => 'imed_dose_correct',
                'MD34'   => 'imed_route_admin',
                'MD34-B' => 'imed_route_correct',
                'MD36'   => 'imed_manufacturer_special_admin',
                'MD37'   => 'imed_manu_correct',
                'MD38'   => 'imed_batch_no'
            ),
        );
        
        // determine which field names to use
        $fields = $useMultiMeds ? $fieldNames['multi'] : $fieldNames['single'];
        
        $ItemCode = '';
        
        switch ($QuestionCode)
        {
            case 'MD01':
            case 'MD02':
                if ($useMultiMeds)
                {
                    $ItemCode = $this->GetLinkCodeAnswer($nIncident, 'inc_id', $lsTextArea, $fields[$QuestionCode], 'inc_medications', $QuestionCode, $medLinkId, 'inc_medications.recordid');
                }
                else
                {
                    $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, $fields[$QuestionCode], 'incidents_main', $QuestionCode);
                }

                $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, $fields[$QuestionCode]);
                break;
                
            case 'MD05':
            case 'MD30':
                if ($useMultiMeds)
                {
                    $this->GetLinkCodeAnswer($nIncident, 'inc_id', $lsTextArea, $fields[$QuestionCode], 'inc_medications', $QuestionCode, $medLinkId, 'inc_medications.recordid', true);
                }
                else
                {
                    $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, $fields[$QuestionCode], 'incidents_main', $QuestionCode);
                }
                
                if (GetParm('XMLDATASET', '1') == '2')
                {
                    $this->checkAndLogMissingData($lsTextArea, $nIncident, $QuestionCode, $fields[$QuestionCode]);
                }

                $ItemCode = 'A';
                break;
                
            case 'MD06':
            case 'MD07':
            case 'MD07-B':
            case 'MD08':
            case 'MD08-B':
            case 'MD10':
            case 'MD31':
            case 'MD32':
            case 'MD32-B':
            case 'MD33':
            case 'MD33-B':
            case 'MD37':
                if ($useMultiMeds)
                {
                    $this->GetLinkCodeAnswer($nIncident, 'inc_id', $lsTextArea, $fields[$QuestionCode], 'inc_medications', $QuestionCode, $medLinkId, 'inc_medications.recordid', true);
                }
                else
                {
                    // This can be removed when these fields are added to the non multi medication array
                    if (!in_array($QuestionCode, ['MD06', 'MD10']))
                    {
                        $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, $fields[$QuestionCode], 'incidents_main', $QuestionCode);
                    }
                    else
                    {
                        $lsTextArea = '';
                    }
                }

                $ItemCode = 'A';
                break;

            case 'MD11':
            case 'MD38':
                // This can be removed when these fields are added to the non multi medication array
                if (!in_array($QuestionCode, ['MD11']))
                {
                    $lsTextArea = $this->GetStringData($medLinkId, $lsTextArea, $fields[$QuestionCode], 'inc_medications', $QuestionCode);
                }
                else
                {
                    $lsTextArea = '';
                }

                $ItemCode = 'A';
                break;

            case 'MD03':
            case 'MD04':
            case 'MD12':
            case 'MD16':
            case 'MD16-B':
            case 'MD34':
            case 'MD34-B':
            case 'MD36':
                if ($useMultiMeds)
                {
                    $ItemCode = $this->GetLinkCodeAnswer($nIncident, 'inc_id', $lsTextArea, $fields[$QuestionCode], 'inc_medications', $QuestionCode, $medLinkId, 'inc_medications.recordid');
                }
                else
                {
                    // This can be removed when these fields are added to the non multi medication array
                    if (!in_array($QuestionCode, ['MD03', 'MD04', 'MD12']))
                    {
                        $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, $fields[$QuestionCode], 'incidents_main', $QuestionCode);
                    }
                    else
                    {
                        $ItemCode = '';
                    }
                }
                break;
        }
                         
        $ItemCode == '' ? $ItemCode = 'C' : $ItemCode;
        return $ItemCode;
    }
    
    /**
    * This helper method is the sub method of GetNPSAAnswer and in particular dealing with Patient fields (PG QuestionCodes)
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int &$EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return string $ItemCode The NPSA code for the given Datix code.
    */
    public function GetXMLAnswerLine_PG($nIncident, $QuestionCode, &$EntitiySeq = 1, $EntityDescription = '', &$lsTextArea = '', $nLinkID = '')
    {
        $ItemCode = '';
        if($QuestionCode == 'PG06')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_n_patharm', 'incidents_main', $QuestionCode);
            if($ItemCode == '')
            {
                $ItemCode = $this->GetMultiCodeAnswerExtra($nIncident, $lsTextArea, 'inc_result', 'incidents_main', $QuestionCode, 'cod_npsa_pg06');
            }
            $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_n_patharm', 'Alternatively fill in the result field');
        }
        
        return $ItemCode;
    }
    
    /**
    * This helper method is the sub method of GetNPSAAnswer and in particular dealing with Incident fields (DE QuestionCodes)
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int &$EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return string $ItemCode The NPSA code for the given Datix code.
    */
    public function GetXMLAnswerLine_DE($nIncident, $QuestionCode, &$EntitiySeq = 1, $EntityDescription = '', &$lsTextArea = '', $nLinkID = '')
    {
        $ItemCode = '';

        if ($QuestionCode == 'DE01')
        {
            //Use linked incidents OR old legacy equipment screen, not both
            if ($nLinkID != '')
            {
                $ItemCode = $this->GetMultiCodeAnswer($nLinkID, $lsTextArea, 'ast_type', 'assets_main', $QuestionCode);
            }
            else
            {
                $ItemCode = $this->GetMultiCodeAnswer($nIncident, $lsTextArea, 'inc_eqpt_type', 'incidents_main', $QuestionCode);
            }

            $this->checkAndLogMissingData($ItemCode, $nIncident, $QuestionCode, 'inc_eqpt_type');
        }
        elseif ($QuestionCode == 'DE02')
        {
            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_location', 'incidents_main', $QuestionCode);
            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE03')
        {
            /*
             * Check if equipment section has data
             *  - If yes use it
             *  - Otherwise use equipment legacy data
             */
            $sql = 'SELECT ast_product FROM assets_main WHERE recordid = :recordid';
            $result = DatixDBQuery::PDO_fetch($sql, ['recordid' => $nLinkID]);

            if ($result)
            {
                $tableName = 'assets_main';
                $fieldName = 'ast_product';
                $nIncident = $nLinkID;

                $this->GetNPSAMultiCode($nIncident, $tableName, $fieldName, $result[$fieldName], $CodeDescr, 'cod_code');

                $lsTextArea = code_descr('AST', $fieldName, $result[$fieldName]);
            }
            else
            {
                $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_equipment', 'incidents_main', $QuestionCode);
            }

            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE04')
        {
            /*
             * Check if equipment section has data
             *  - If yes use it
             *  - Otherwise use equipment legacy data
             */
            $sql = 'SELECT ast_model FROM assets_main WHERE recordid = :recordid';
            $result = DatixDBQuery::PDO_fetch($sql, ['recordid' => $nLinkID]);

            if ($result)
            {
                $tableName = 'assets_main';
                $fieldName = 'ast_model';
                $nIncident = $nLinkID;

                $this->GetNPSAMultiCode($nIncident, $tableName, $fieldName, $result[$fieldName], $CodeDescr, 'cod_code');

                $lsTextArea = code_descr('AST', $fieldName, $result[$fieldName]);
            }
            else
            {
                $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_model', 'incidents_main', $QuestionCode);
            }

            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE05')
        {
            $lsTextArea = $this->GetStringData($nLinkID, $lsTextArea, 'ast_catalogue_no', 'assets_main', $QuestionCode);
            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE06')
        {
            /*
            * Check if equipment section has data
            *  - If yes use it
            *  - Otherwise use equipment legacy data
            */
            $sql = 'SELECT ast_serial_no FROM assets_main WHERE recordid = :recordid';
            $result = DatixDBQuery::PDO_fetch($sql, ['recordid' => $nLinkID]);

            if ($result)
            {
                $tableName = 'assets_main';
                $fieldName = 'ast_serial_no';
                $nIncident = $nLinkID;
            }
            else
            {
                $tableName = 'incidents_main';
                $fieldName = 'inc_serialno';
            }

            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, $fieldName, $tableName, $QuestionCode);
            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE07')
        {
            /*
             * Check if equipment section has data
             *  - If yes use it
             *  - Otherwise use equipment legacy data
             */
            $sql = 'SELECT ast_manufacturer FROM assets_main WHERE recordid = :recordid';
            $result = DatixDBQuery::PDO_fetch($sql, ['recordid' => $nLinkID]);

            if ($result)
            {
                $tableName = 'assets_main';
                $fieldName = 'ast_manufacturer';
                $nIncident = $nLinkID;

                $this->GetNPSAMultiCode($nIncident, $tableName, $fieldName, $result[$fieldName], $CodeDescr, 'cod_code');

                $lsTextArea = code_descr('AST', $fieldName, $result[$fieldName]);
            }
            else
            {
                $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_manufacturer', 'incidents_main', $QuestionCode);
            }

            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE08')
        {
            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, 'inc_supplier', 'incidents_main', $QuestionCode);
            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE09')
        {
            /*
            * Check if equipment section has data
            *  - If yes use it
            *  - Otherwise use equipment legacy data
            */
            $sql = 'SELECT ast_batch_no FROM assets_main WHERE recordid = :recordid';
            $result = DatixDBQuery::PDO_fetch($sql, ['recordid' => $nLinkID]);

            if ($result)
            {
                $tableName = 'assets_main';
                $fieldName = 'ast_batch_no';
                $nIncident = $nLinkID;
            }
            else
            {
                $tableName = 'incidents_main';
                $fieldName = 'inc_batchno';
            }

            $lsTextArea = $this->GetStringData($nIncident, $lsTextArea, $fieldName, $tableName, $QuestionCode);
            $ItemCode = 'A';
        }
        elseif ($QuestionCode == 'DE11')
        {
            $lsTextArea = $this->GetDateData($nIncident, $lsTextArea, 'inc_dmanu', 'incidents_main', $QuestionCode);
            $ItemCode = 'X';
        }
        elseif ($QuestionCode == 'DE12')
        {
            $lsTextArea = $this->GetNumberData($nIncident, $lsTextArea, 'inc_qdef', 'incidents_main', $QuestionCode);
            $ItemCode = 'A';
        }
              
        $ItemCode == '' ? $ItemCode = 'C' : $ItemCode;
        return $ItemCode;
    }
    
    /**
    * This helper method is the sub method of GetNPSAAnswer and in particular dealing with Staff fields (ST QuestionCodes)
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int &$EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param string $EntityDescription The entity (section) description (e.g. Incident, Patient Group or Staff)
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $nLinkID The record id of the linked record
    * 
    * @return string $ItemCode The NPSA code for the given Datix code.
    */
    public function GetXMLAnswerLine_ST($nIncident, $QuestionCode, &$EntitiySeq = 1, $EntityDescription = '', &$lsTextArea = '', $nLinkID = '')  
    {
        $ItemCode = '';
        if($QuestionCode == 'ST01')
        {
            $ItemCode = $this->GetLinkCodeAnswer($nIncident, 'inc_id', $lsTextArea, 'link_status', 'link_contacts', $QuestionCode, $nLinkID, 'con_id');
        }
        elseif($QuestionCode == 'ST02')
        {
            $ItemCode = $this->GetMultiCodeAnswer($nLinkID, $lsTextArea, 'con_empl_grade', 'contacts_main', $QuestionCode);
        }
        elseif($QuestionCode == 'ST04')
        {
            $ItemCode = $this->GetLinkCodeAnswer($nIncident, 'inc_id', $lsTextArea, 'link_npsa_role', 'link_contacts', $QuestionCode, $nLinkID, 'con_id');
        }
        
        return $ItemCode;
    }                    
      
    /**
    * This helper method is used to get Date data from the database for a given field
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $FieldName The database column name of the field we need the value of
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param string $MainTable The database table name in question (typically incidents_main)
    * 
    * @return string $strDate Date value in string format ready to include in xml
    */
    public function GetDateData($nIncident, $lsTextArea, $FieldName, $MainTable = 'incidents_main', $QuestionCode)
    {
        $strDate = '';
        $sql = "SELECT $FieldName as field_name FROM $MainTable WHERE recordid = $nIncident";
        if($row = DatixDBQuery::PDO_fetch($sql))
        {
            if ($row["field_name"] == '')
            {
                $strDate == '';   
            }
            else
            {                
                $RecordDate = new \DateTime($row["field_name"]);
                $strDate = $RecordDate->format('Y-m-d');
            }
        }
        return $strDate;
    }
    
    /**
    * This helper method is used to get the value of a given field from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $FieldName The database column name of the field we need the value of
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param string $MainTable The database table name in question (typically incidents_main)
    * 
    * @return string $row[field_mane] Field value in string format ready to include in xml
    */
    public function GetStringData($nIncident, $lsTextArea, $FieldName, $MainTable = 'incidents_main', $QuestionCode)
    {
        $sql = "SELECT $FieldName as field_name FROM $MainTable WHERE recordid = $nIncident";
        $row = DatixDBQuery::PDO_fetch($sql);
        return $row["field_name"];
    }
    
    /**
    * This helper method is used to get the value of a given linked field from the database.
    *  
    * @param  int     $id           The recordid
    * @param  string  $mainIDField  The name of the ID field (e.g. recordid)
    * @param  string  $fieldName    The name of the database field we're after
    * @param  string  $mainTable    The database table name
    * @param  int     $linkID       The database table name in question (typically incidents_main)
    * @param  string  $linkField    The database table name in question (typically incidents_main)
    * 
    * @return string
    */
    public function getLinkStringData($id, $mainIDField, $fieldName, $mainTable, $linkID, $linkField)
    {
        return DatixDBQuery::PDO_fetch(
            'SELECT ' . $fieldName . ' AS field_name FROM ' . $mainTable . ' WHERE ' . $mainIDField . ' = :id AND ' . $linkField . ' = :linkid', 
            array('id' => $id, 'linkid' => $linkID), 
            PDO::FETCH_COLUMN);
    }
    
    /**
    * This helper method is used to get the value of a given number field from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string &$lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $FieldName The database column name of the field we need the value of
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param string $MainTable The database table name in question (typically incidents_main)
    * 
    * @return int $row[field_mane] Field value in string format ready to include in xml
    */
    public function GetNumberData($nIncident, $lsTextArea, $FieldName, $MainTable = 'incidents_main', $QuestionCode)
    {
        $sql = "SELECT $FieldName as field_name FROM $MainTable WHERE recordid = $nIncident";
        $row = DatixDBQuery::PDO_fetch($sql);
        return $row["field_name"] == '' ? 0 : $row["field_name"];
    }
    
    /**
    * This helper method is used to get the NPSA code for a given hour (24 hrs system)
    *  
    * @param string $lsTextArea The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * 
    * @return string NPSA code for the given hour
    * 
    * @codeCoverageIgnore
    */                                                                      
    public function GetTimeHoursCode($lsTextArea)
    {
        $Hour = \UnicodeString::substr($lsTextArea, 0, 2);
        
        switch($Hour)
        {
            case '00':
                return 'A';
                break;
            case '01':
                return 'B';
                break;
            case '02':
                return 'C';
                break;
            case '03':
                return 'D';
                break;
            case '04':
                return 'E';
                break;
            case '05':
                return 'F';
                break;
            case '06':
                return 'G';
                break;
            case '07':
                return 'H';
                break;
            case '08':
                return 'I';
                break;
            case '09':
                return 'J';
                break;
            case '10':
                return 'K';
                break;
            case '11':
                return 'L';
                break;
            case '12':
                return 'M';
                break;
            case '13':
                return 'N';
                break;
            case '14':
                return 'O';
                break;
            case '15':
                return 'P';
                break;
            case '16':
                return 'Q';
                break;
            case '17':
                return 'R';
                break;
            case '18':
                return 'S';
                break;
            case '19':
                return 'T';
                break;
            case '20':
                return 'AA';
                break;
            case '00':
                return 'AB';
                break;
            case '00':
                return 'AC';
                break;
            case '00':
                return 'AD';
                break;
            default:
                return 'CU';
                break;
        }
    }
    
    /**
    * This helper method is used to get the NPSA code for a given minutes
    *  
    * @param string $lsTextArea The information stored for the given field, will be stored in the TextArea attribute in the xml file
    * 
    * @return string NPSA code for the given minutes
    * 
    * @codeCoverageIgnore
    */     
    public function GetTimeMinutesCode($lsTextArea)
    {
        $Minutes = \UnicodeString::substr($lsTextArea, -2);
        switch($Minutes)
        {
            case $Minutes <= 14:
                return 'BA';
                break;
            case $Minutes <= 29:
                return 'BB';
                break;
            case $Minutes <= 44:
                return 'BC';
                break;
            case $Minutes <= 59:
                return 'BD';
                break;
            default:
                return 'CU';
                break;
        }
    }
    
     
    /**
    * This helper method is used to get the value of a given coded field from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string &$DatixCodeDescr The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $CodeField The database column name of the field we need the value of
    * @param string $MainTable The database table name in question (typically incidents_main)
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * 
    * @return string $NPSACodeValue NPSA code for the given Datix coded field value
    */
    public function GetMultiCodeAnswer($nIncident, &$DatixCodeDescr = '', $CodeField, $MainTable = 'incidents_main', $QuestionCode = '')
    {
        $CodeDescr = '';
        
        $sql = "SELECT $CodeField as code_field FROM $MainTable WHERE recordid = $nIncident";
        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row["code_field"] != '')
        {
            $NPSACodeValue = $this->GetNPSAMultiCode($nIncident,$MainTable, $CodeField, $row["code_field"], $CodeDescr, 'cod_npsa');
            
            if(\UnicodeString::substr($NPSACodeValue, 0, 1) == 'Z')
            {
                $DatixCodeDescr = code_descr('INC', $CodeField, $row["code_field"]);
            }
        }   
        
        return $NPSACodeValue;
    }
    
    /**
    * This helper method is used to get the value of a given coded field from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string &$DatixCodeDescr The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $CodeField The database column name of the field we need the value of
    * @param string $MainTable The database table name in question (typically incidents_main)
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * 
    * @return string $NPSACodeValue NPSA code for the given Datix coded field value
    */
    public function GetMultiCodeAnswerIN05($nIncident, &$DatixCodeDescr = '', $CodeField, $MainTable = 'incidents_main', $QuestionCode = '')
    {
        $CodeDescr = '';
        
        $sql = "SELECT $CodeField as code_field, inc_subcategory FROM incidents_main WHERE recordid = $nIncident";
        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row["code_field"] != '')
        {
            $NPSACodeValue = $this->GetNPSAMultiCode($nIncident,$MainTable, $CodeField, $row["code_field"], $CodeDescr, 'cod_npsa');
            
            if(\UnicodeString::substr($NPSACodeValue, 0, 1) == 'Z')
            {
                if($row['inc_subcategory'] != '')
                {
                    $DatixCodeDescr = code_descr('INC', 'inc_subcategory', $row["inc_subcategory"]);
                }
                
                if($DatixCodeDescr == '')
                {
                    $DatixCodeDescr = code_descr('INC', $CodeField, $row["code_field"]);
                }
            }
        }   
        
        return $NPSACodeValue;
    }
    
    /**
    * This helper method is used to get the value of a given coded field from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string &$DatixCodeDescr The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $CodeField The database column name of the field we need the value of
    * @param string $MainTable The database table name in question (typically incidents_main)
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param string $ExtraField The database column containing the NPSA code
    * 
    * @return string $NPSACodeValue NPSA code for the given Datix coded field value
    */
    public function GetMultiCodeAnswerExtra($nIncident, &$DatixCodeDescr = '', $CodeField, $MainTable = 'incidents_main', $QuestionCode = '', $ExtraField = 'cod_npsa')
    {
        $CodeDescr = '';
        
        $sql = "SELECT $CodeField as code_field FROM $MainTable WHERE recordid = $nIncident";
        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row["code_field"] != '')
        {
            $NPSACodeValue = $this->GetNPSAMultiCode($nIncident,$MainTable, $CodeField, $row["code_field"], $CodeDescr, $ExtraField);
            
            if(\UnicodeString::substr($NPSACodeValue, 0, 1) == 'Z')
            {
                $DatixCodeDescr = code_descr('INC', $CodeField, $row["code_field"]);
            }
        }   
        
        return $NPSACodeValue;    
    }
    
    /**
    * This helper method is used to get the root causes data for the given incident from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string &$DatixCodeDescr The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $CodeField The database column name of the field we need the value of
    * @param string $MainTable The database table name in question (typically incidents_main)
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int $nRCIndex The index of the root cause in question
    * 
    * @return string $NPSACodeValue NPSA code for the given root cause value
    */
    public function GetRootCodeAnswer($nIncident, &$DatixCodeDescr, $CodeField, $MainTable = 'incidents_main', $QuestionCode = '', $nRCIndex = 0)
    {
        $CodeDescr = '';
        
        $sql = "SELECT $CodeField as code_field FROM $MainTable WHERE recordid = $nIncident";
        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row["code_field"] != '')
        {
            $RootCArray = explode(' ', $row["code_field"]);
            $NPSACodeValue = $this->GetNPSAMultiCode($nIncident,$MainTable, $CodeField, $RootCArray[$nRCIndex], $CodeDescr, 'cod_npsa');
            $DatixCodeDescr = $CodeDescr;
        }   
        
        return $NPSACodeValue;
    }
    
    /**
    * This helper method is used to get the linked table data for the given incident from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $MainIDField The database column containing the foreign key in the linked table
    * @param string &$DatixCodeDescr The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $CodeField The database column name of the field we need the value of
    * @param string $LinkTable The linked database table name in question (Main is typically incidents_main)
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int $nLinkID The record id of the linked record
    * @param string $LinkField The database column name in the linked table we need the value of
    * @param bool   $setDescription Forces $DatixCodeDescr to be populated (i.e. even if the NPSA code does not start with 'Z') 
    * 
    * @return string $NPSACodeValue NPSA code for the given root cause value
    */                     
    public function GetLinkCodeAnswer($nIncident, $MainIDField = 'inc_id', &$DatixCodeDescr = '', $CodeField = 'link_mhcpa', $LinkTable = 'link_contacts', $QuestionCode, $nLinkID, $LinkField = 'con_id', $setDescription = false)
    {   
        $MainTable = 'incidents_main, ' . $LinkTable;
        $sql = "SELECT $CodeField as code_field FROM $MainTable WHERE $MainIDField = $nIncident AND $LinkField = $nLinkID";
        $row = DatixDBQuery::PDO_fetch($sql);

        if ($row["code_field"] != '')
        {
            $NPSACodeValue = $this->GetNPSAMultiCode($nIncident,$MainTable, $CodeField, $row["code_field"], $CodeDescr, 'cod_npsa');
            
            if(\UnicodeString::substr($NPSACodeValue, 0, 1) == 'Z' || $setDescription)
            {
                $DatixCodeDescr = code_descr('INC', $CodeField, $row["code_field"]);
            }
        }   
        
        return $NPSACodeValue;
    }
                         
    /**
    * This helper method is used to get the value of a given coded field from the database
    *  
    * @param int $nIncident The recordid of the given incident
    * @param string $MainTable The database table name in question (typically incidents_main) 
    * @param string $CodeField The database column name of the field we need the value of
    * @param string $DatixCode The Datix code value for the given field
    * @param string &$NPSACodeDescr The text information stored for the given field, will be stored in the TextArea attribute in the xml file
    * @param string $NPSACodeField The database column containing the NPSA code
    * 
    * @return string $NPSACodeFieldValue NPSA code for the given Datix coded field value
    */
    public function GetNPSAMultiCode($nIncident, $MainTable = 'incidents_main', $CodeField, $DatixCode = '', &$NPSACodeDescr = '', $NPSACodeField = '')
    {
        $fformat = getFieldFormat($CodeField, 'INC');
        $table   = $fformat['fmt_code_table'];
        $parent  = $fformat['fmt_code_parent'];

        if ($table{0} == '!')
        {
            $code_type  = \UnicodeString::substr($table, 1);
            $table      = 'code_types';
            $field      = 'cod_code';
            $where      = ' cod_type = \'' . $code_type . '\' and ' . $field . ' = \'' . $DatixCode . '\'';
            $descrField = 'cod_descr';
        }
        else
        {
            $field      = $fformat['fmt_code_field'];
            $descrField = $fformat['fmt_code_descr'];
            $where      = $fformat['fmt_code_where'];
            
            if ($DatixCode != '')
            {
                if ($where != '')
                {
                    $where .= ' AND ' . $field . ' = \'' . $DatixCode . '\'';
                }
                else
                {
                    $where = $field . ' = \'' . $DatixCode . '\'';    
                }    
            }
        }
        
        $sql = 'SELECT ' . $NPSACodeField . ', cod_parent, ' . $descrField . ' FROM ' . $table;
        if ($where != '')
        {
            $sql .= ' WHERE ' . $where;        
        }
        
        $row         = DatixDBQuery::PDO_fetch($sql);
        $parentCodes = explode(' ', $row['cod_parent']);
        $npsaCodes   = explode(' ', $row[$NPSACodeField]);
        
        if (count($npsaCodes) > 1)
        {
            $parentValue = $this->GetStringData($nIncident, '', $parent, $MainTable, '');
        
            // find parent value in parent code array and get corresponding npsa code from its position in the npsa code array
            $key = array_search($parentValue, $parentCodes); 
            if ($key !== false)
            {
                $NPSACodeFieldValue = $npsaCodes[$key];            
            }
            else
            {
                $NPSACodeFieldValue = null;    
            }   
        }
        else if (count($npsaCodes) == 1)
        {
            $NPSACodeFieldValue = $npsaCodes[0];       
        }
        
        $NPSACodeDescr = $row[$descrField];
        return $NPSACodeFieldValue;
    }
                           
    /**
    * This helper method is used to get the Care Setting NPSA code from the given unit type
    *  
    * @param string $UnitType The unit type of the incident record
    * 
    * @return string NPSA code for the given unit type
    * 
    * @codeCoverageIgnore
    */  
    public function GetCareSettingFromUnitType($UnitType)
    {
        switch($UnitType)
        {
            case 'A':
                return 'AM';
                break;
            case 'B':
                return 'PH';
                break;
            case 'C':
                return 'PCO';
                break;
            case 'D':
                return 'DE';
                break;
            case 'E':
                return 'GP';
                break;
            case 'F':
                return 'AC';
                break;
            case 'G':
                return 'LD';
                break;
            case 'H':
                return 'MH';
                break;
            case 'I':
                return 'OP';
                break;
            default:
                return '';
                break;
        }
    }
    
    /**
    * Populates the XMLErrors array if the data is missing.
    * 
    * @param mixed $data
    * @param mixed $incidentID
    * @param mixed $questionNo
    * @param mixed $sourceField
    * @param mixed $otherInfo
    */
    public function checkAndLogMissingData($data, $incidentID, $questionNo, $sourceField, $otherInfo = '')
    {
        if ($data == '')
        {
            $this->XMLErrors[] = 'Incident: ' . $incidentID . '; NPSA Code: ' . $questionNo . '; Field: ' . Labels_FieldLabel::GetFieldLabel($sourceField) . '; ' . $otherInfo;
        }   
    }
}
