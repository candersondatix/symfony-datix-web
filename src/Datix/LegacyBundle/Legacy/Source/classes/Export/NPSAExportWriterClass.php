<?php
/**
* Export_NPSAExportWriter class is used for Exporting NPSA codes to xml files.        
*/
                                                   
class Export_NPSAExportWriter 
{
    protected $xml;
    protected $CareSetting;
    protected $IncidentID;
    protected $batchxml;
    protected $strToday;
    protected $npsa;
    
    /**
    * An array of unique values containing NPSA codes mapped to root causes.
    * 
    * @var array
    */
    protected $RootCNPSAArray;
    
    /**
    * Constructor - gets, sets required information 
    * 
    * @global array $txt
    * 
    * @param array $parameters Optional parameters to pass information to this class regarding XML header attributes
    * 
    * XMLSchemaVersion
    * DataSetVersion
    * Alias
    * TrustName
    * RiskManagementVendor
    * ActionName
    * npsa
    */
    public function __construct($parameters)
    {
        if (bYN(GetParm('EXPORT_CCS2','N')) && GetParm('XMLDATASET', '1') == '1')
        {
            // cannot use XMLDATASET 1 when exporting CCS2 codes
            $_SESSION['fileExportError'] = json_encode(array(
                'title'    => _tk('npsa_export_invalid_config_title'),
                'contents' => _tk('npsa_export_invalid_config_message'),
            ));
            setcookie('fileExportToken', Sanitize::SanitizeString($_POST['export_token_value']).'|fail', 0, '', '', ($_SERVER["HTTPS"] == "on" ? true : false));
            obExit();
        }
        
        $XMLVersion = $parameters['XMLVersion'] ? $parameters['XMLVersion'] : GetParm('XMLVERSION', '1.0');
        $Encoding = $parameters['Encoding'] ? $parameters['Encoding'] : GetParm('XMLENCODING', 'UTF-8');
        $XMLSchemaVersion = $parameters['XMLSchemaVersion'] ? $parameters['XMLSchemaVersion'] : GetParm('XMLSCHVER', '1');
        $DataSetVersion = $parameters['DataSetVersion'] ? $parameters['DataSetVersion'] : GetParm('XMLDATASET', '1');
        $Alias = $parameters['Alias'] ? $parameters['Alias'] : GetParm('ALIAS', '');
        $TrustName = $parameters['TrustName'] ? $parameters['TrustName'] : GetParm('XMLTRUST', '');
        $RiskManagementVendor = $parameters['RiskManagementVendor'] ? $parameters['RiskManagementVendor'] : GetParm('XMLVENDOR', 'DATIX');
        $ActionName = $parameters['ActionName'] ? $parameters['ActionName'] : GetParm('XMLACTION', 'Insert');
        $this->npsa = $parameters['npsa'];
        $this->RootCNPSAArray = array();
        
        $xmlstr = '<?xml version="'.$XMLVersion.'" encoding="'.$Encoding.'"?><Batch></Batch>';
        $this->batchxml = new SimpleXMLElement($xmlstr);

        if (GetParm('XMLDATASET', '1') == '2')
        {
            $this->batchxml->addAttribute('LRMSVersion', GetVersion());    
        }

        $this->batchxml->addAttribute('XMLSchemaVersion', $XMLSchemaVersion);
        $this->batchxml->addAttribute('DataSetVersion', $DataSetVersion);
        $this->batchxml->addAttribute('Alias', $Alias);
        $this->batchxml->addAttribute('TrustName', $TrustName);
        $this->batchxml->addAttribute('RiskManagementVendor', $RiskManagementVendor);
        $this->batchxml->addAttribute('ActionName', $ActionName);
        
        $this->GetIncidentsSections();        

        $this->strToday = date('Y-m-d Hi');
        
        if (!empty($this->npsa->XMLErrors))
        {
            // error is retrieved from session via ajax
            $_SESSION['fileExportError'] = '<div style="padding:5px;">'._tk('npsa_export_error_message').'</div>'.
                '<div style="border:solid 1px #808080; background-color:#fff; height:500px; overflow-y:auto; overflow-x:hidden; padding:5px;">'.implode('<br />', $this->npsa->XMLErrors).'</div>'.
                '<div style="text-align:center"><input style="width:80px; margin-left:auto; margin-right:auto; margin-top:10px;" type="button" value="Export" onclick="SendTo(\''.$scripturl.'?service=export&event=getNPSAExportError\');" /></div>';
                
            // the error can also be exported in CSV format
            $_SESSION['npsaXMLErrors'] = array();

            foreach ($this->npsa->XMLErrors as $error)
            {
                $_SESSION['npsaXMLErrors'][] = explode('; ', $error);    
            }

            setcookie('fileExportToken', Sanitize::SanitizeString($_POST['export_token_value']).'|fail', 0, '', '', ($_SERVER["HTTPS"] == "on" ? true : false));
            obExit();
        }
        else
        {
            // output XML file
        	$this->UpdateIncidentNPSAReportedDate();
            $this->OutputNPSAExport('download');   
        }
    }
    
    /**
    * Core method that loops through all the records in the search result set
    */
    protected function GetIncidentsSections()
    {
        require_once 'Source/libs/SavedQueries.php';

        $sql = "SELECT recordid, inc_dopened, inc_unit_type, inc_root_causes, inc_time, inc_never_event FROM incidents_main";

        if (!empty($_SESSION['INC']['WHERE']))
        {
            $whereString = $_SESSION['INC']['WHERE'];

            // We need to replace special keywords from the search query before executing it.
            $whereString = replaceAtPrompts($whereString, 'INC');
            $whereString = TranslateWhereCom($whereString, $_SESSION["initials"], 'INC');

            $sql .= " WHERE ".$whereString;
        }

        $sql .= " ORDER BY incidents_main.recordid";

        $resultcodes = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($resultcodes as $inc_data)
        {
            $this->GetIncidentHeader($inc_data);
        }
    }
    
    /**
    * This method is called for each and every record and creates the record specific XML attributes. Then it calls the data collector and 
    * XML writer methods divided into sections based on NPSA code Question codes (e.g. IN, PD, MD).
    * 
    * @param array $inc_data Contains the basic data fields for the incident record from GetIncidentsSections()
    */
    protected function GetIncidentHeader($inc_data)
    {
        // $incxml is a given incident node in the xml document tree
        $incxml = $this->batchxml->addChild('Incident');

        require_once "Date.php";
        // $ReportedDate is actually opened date in the main app source code, despite the attribute name 
        $ReportedDate = new Date();
        $strReportedDate = $ReportedDate->getDate(DATE_FORMAT_NPSA_XML);
        
        $this->CareSetting = $this->npsa->GetCareSettingFromUnitType($inc_data['inc_unit_type']);
        
        $incxml->addAttribute('ReportedDate', $strReportedDate);
        $incxml->addAttribute('CareSetting', $this->CareSetting);
        $incxml->addAttribute('IncidentID', $inc_data['recordid']);
        
        $this->getIncidentRPINAnswers($incxml, $inc_data);
        $this->getNPSAIncRootCauses($incxml, $inc_data);
        $this->getNPSAPDAnswers($incxml, $inc_data);
        $this->getNPSAPatients($incxml, $inc_data);
        $this->getNPSAEmployees($incxml, $inc_data);
        $this->getNPSAMDAnswers($incxml, $inc_data);
        $this->getNPSADEAnswers($incxml, $inc_data);
    }
    
    /**
    * XML tag writer applying the given parameters as attributes.
    * @param &SimpleXMLElement incxml Incident node in the xml tree.
    * @param string ChildName The child tag name in the xml tree
    * @param $XMLResponseTag The parameters containing data to fill the attribute in the child tag.
    */
    protected function WriteToXMLAddChild(&$incxml, $ChildName = 'Response', $XMLResponseTag)
    {
        /**********************************************
        *   'ReferenceCode' => $code, 
            'ItemCode' => $ItemCode,
            'EntitySequence' => $EntitiySeq,
            'EntityDescription' => $EntityDescription,
            'TextArea' => $TextArea
        **********************************************/ 
        if($XMLResponseTag)
        {
            $responsexml = $incxml->addChild($ChildName);  
            $responsexml->addAttribute('ReferenceCode',     $XMLResponseTag['ReferenceCode']);
            $responsexml->addAttribute('ItemCode',          $XMLResponseTag['ItemCode']);
            $responsexml->addAttribute('EntitySequence',    $XMLResponseTag['EntitySequence']);
            $responsexml->addAttribute('EntityDescription', $XMLResponseTag['EntityDescription']);
            $responsexml->addAttribute('TextArea',          $XMLResponseTag['TextArea']);
        }
    }
    
    /**
    * Incident and RP Answers writer
    * 
    * @param &SimpleXMLElement incxml Incident node in the xml tree
    * @param array $inc_data Array that contains the basic data fields for the incident record
    */
    protected function getIncidentRPINAnswers($incxml, $inc_data)
    {
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'RP02', 1, null, true));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN01', 1, null, true));

        if (!empty($inc_data['inc_time']))
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN02', 1, null, true));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN02', 2, null, true));
        }
        else
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN02', 0, null, true));
        }

        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN03', 1, null, true));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN04', 1, null, true));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN05', 1, null, true));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN07', 1, null, true));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN10', 1, null, false));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN11', 1, null, false));

        if ($inc_data['inc_never_event'] == 'Y')
        {
             $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN27', 1, null, false));   
        }
    }
    
    /**
    * Root Causes Answers writer
    * 
    * @param &SimpleXMLElement incxml Incident node in the xml tree
    * @param array $inc_data Array that contains the basic data fields for the incident record
    */ 
    protected function getNPSAIncRootCauses($incxml, $inc_data)
    {
        $EntitySeq = 1;
        $nIndexCount = 0;

        if ($inc_data['inc_root_causes'] != '')
        {
            $RootCArray = explode(' ', $inc_data['inc_root_causes']);
            $nIncRootCNumber = count($RootCArray);
            $nIndexCount = 0;

            foreach ($RootCArray as $RootCCode)
            {
                $CurrentRootCCodeNPSA = $this->npsa->GetRootCodeAnswer($inc_data['recordid'], $RootCCode, 'inc_root_causes', 'incidents_main', 'IN06', $nIndexCount);

                if ($CurrentRootCCodeNPSA != '')
                {
                    if (!in_array($CurrentRootCCodeNPSA, $this->RootCNPSAArray))
                    {
                        $this->RootCNPSAArray[] = $CurrentRootCCodeNPSA;
                        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'IN06', $EntitySeq, $nIndexCount, false));
                    }
                }

                $nIndexCount++;
                $EntitySeq++;
            }
        }
    }
    
    /**
    * PD Answers writer
    * 
    * @param &SimpleXMLElement incxml Incident node in the xml tree
    * @param array $inc_data Array that contains the basic data fields for the incident record
    */
    protected function getNPSAPDAnswers($incxml, $inc_data)
    {
        if ($this->CareSetting == 'AC')
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD04', 1, null, false));                
        }
        
        if ($this->npsa->GetMultiCodeAnswer($inc_data['recordid'], $DatixCodeDescr, 'inc_n_patharm', 'incidents_main', 'PD16') == 'A'
            ||
            (
                $this->npsa->GetMultiCodeAnswer($inc_data['recordid'], $DatixCodeDescr, 'inc_n_patharm', 'incidents_main', 'PD16') == ''
                    &&
                $this->npsa->GetMultiCodeAnswerExtra($inc_data['recordid'], $DatixCodeDescr, 'inc_result', 'incidents_main', 'PD16', 'cod_npsa_pd16') == 'A'
            )
            )
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD09', 1, null, false));
        }
        else
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD12', 1, null, false));
        }
        
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD05', 1, null, true));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD10', 1, null, false));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD14', 1, null, false));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD16', 1, null, true));
        $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD20', 1, null, false));
    }
    
    /**
    * Patient Data Answers writer
    * 
    * @param &SimpleXMLElement incxml Incident node in the xml tree
    * @param array $inc_data Array that contains the basic data fields for the incident record
    */
    protected function getNPSAPatients($incxml, $inc_data)
    {
        $EntitySeq = 1;
        
        $sql = "SELECT recordid from link_contacts,contacts_main where link_contacts.link_type = 'A' AND link_contacts.inc_id = $inc_data[recordid] AND link_contacts.con_id = contacts_main.recordid";
        $resultArray = DatixDBQuery::PDO_fetch_all($sql);
        
        foreach ($resultArray as $row)
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD01', $EntitySeq, $row['recordid'], false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD02', $EntitySeq, $row['recordid'], true));

            if ($this->CareSetting == 'MH')
            {
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD06', $EntitySeq, $row['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD07', $EntitySeq, $row['recordid'], true));
            }

            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'PD11', $EntitySeq, $row['recordid'], false));
            $EntitySeq++;
        }
    }
    
    /**
    * Employees Answers writer
    * 
    * @param &SimpleXMLElement incxml Incident node in the xml tree
    * @param array $inc_data Array that contains the basic data fields for the incident record
    */
    protected function getNPSAEmployees($incxml, $inc_data)
    {
        $EntitySeq = 1;
        
        $sql = "SELECT recordid from link_contacts, contacts_main where link_contacts.link_type = 'E' AND link_contacts.inc_id = $inc_data[recordid] AND link_contacts.con_id = contacts_main.recordid";
        $resultArray = DatixDBQuery::PDO_fetch_all($sql);
        
        foreach ($resultArray as $row)
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'ST01', $EntitySeq, $row['recordid'], false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'ST02', $EntitySeq, $row['recordid'], false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'ST04', $EntitySeq, $row['recordid'], false));
            $EntitySeq++;
        }   
    }   
    
    /**
    * MD Answers writer
    * 
    * @param &SimpleXMLElement incxml Incident node in the xml tree
    * @param array $inc_data Array that contains the basic data fields for the incident record
    */
    protected function getNPSAMDAnswers($incxml, $inc_data) 
    {
        if ($this->npsa->GetMultiCodeAnswer($inc_data['recordid'], $DatixCodeDescr, 'inc_clintype', 'incidents_main', 'IN05') == 'J'
            || $this->npsa->GetMultiCodeAnswer($inc_data['recordid'], $DatixCodeDescr, 'inc_type_tier_three', 'incidents_main', 'IN05') == 'J'
            || in_array('D', $this->RootCNPSAArray))
        {
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD01', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD02', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD03', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD04', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD05', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD06', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD07', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD07-B', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD08', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD08-B', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD10', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD11', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD12', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD16', 1, '', false));
            $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD16-B', 1, '', false));

            // When second drug is added and  MD02 = K (wrong drug) or MD02 = I (wrong/ transposed /omitted medicine label) then map the following from the second drug
            $sql = 'SELECT recordid FROM inc_medications WHERE inc_id = :inc_id ORDER BY listorder, recordid';
            $result = DatixDBQuery::PDO_fetch_all($sql, ['inc_id' => $inc_data['recordid']]);

            $md02 = $this->GetXMLAnswerLine($inc_data['recordid'], 'MD02', 1, '', false);

            if (count($result) >= 2 && ($md02['ItemCode'] == 'K' || $md02['ItemCode'] == 'I'))
            {
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD30', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD31', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD32', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD32-B', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD33', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD33-B', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD34', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD34-B', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD36', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD37', 1, $result[1]['recordid'], false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'MD38', 1, $result[1]['recordid'], false));
            }
        }
    }              
    
    /**
    * DE Answers writer
    * 
    * @param &SimpleXMLElement incxml Incident node in the xml tree
    * @param array $inc_data Array that contains the basic data fields for the incident record
    */
    protected function getNPSADEAnswers($incxml, $inc_data)
    {
        $EntitySeq = 1;
        
        if (\UnicodeString::substr($this->npsa->GetMultiCodeAnswer($inc_data['recordid'], $DatixCodeDescr, 'inc_clintype', 'incidents_main', 'IN05'), 0, 1) == 'I'
            || $this->npsa->GetMultiCodeAnswer($inc_data['recordid'], $DatixCodeDescr, 'inc_clintype', 'incidents_main', 'IN05') == 'ZI'
            || in_array('C', $this->RootCNPSAArray)) 
        {
            $sql = "SELECT TOP 1 recordid as asset_recordid from link_assets, assets_main where link_assets.link_type = 'E' AND link_assets.inc_id = $inc_data[recordid] AND link_assets.ast_id = assets_main.recordid ORDER BY link_assets.link_recordid";
            $resultArray = DatixDBQuery::PDO_fetch_all($sql);
            
            if (empty($resultArray))
            {
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE01', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE02', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE03', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE04', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE06', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE07', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE08', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE09', 1, '', false));
                $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE11', 1, '', false));
            }
            else
            {
                foreach ($resultArray as $row)
                {
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE01', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE02', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE03', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE04', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE05', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE06', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE07', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE08', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE09', $EntitySeq, $row['asset_recordid'], false));
                    $this->WriteToXMLAddChild($incxml, 'Response', $this->GetXMLAnswerLine($inc_data['recordid'], 'DE11', $EntitySeq, $row['asset_recordid'], false)); 
                    $EntitySeq++;
                }    
            }                            
        }
    }    
    
    /**
    * NPSA Reported date update for the incident records 
    * 
    */
    protected function UpdateIncidentNPSAReportedDate()
    {
        if ($_SESSION['INC']['WHERE'] != '')
        {
            $whereString = $_SESSION['INC']['WHERE'];

            // We need to replace special keywords from the search query before executing it.
            $whereString = replaceAtPrompts($whereString, 'INC');
            $whereString = TranslateWhereCom($whereString, $_SESSION["initials"], 'INC');

            $sql = 'UPDATE incidents_main SET inc_dnpsa = :inc_dnpsa WHERE '.$whereString;
            DatixDBQuery::PDO_query($sql, array('inc_dnpsa' => (new DateTime())->format('Y-m-d H:i:s')));
        }
    }
        
    /**
    * Get data and parameters for the given Question Code
    * 
    * @param int $nIncident The recordid of the given incident
    * @param string $QuestionCode The NPSA question code (e.g. 'IN04' or 'PD01')
    * @param int $EntitiSeq Entitiy sequence counter in case multiple entries will be created for the one given incident record
    * @param int $nLinkID The record id of the linked record
    * @param bool $boolean not used
    * 
    * @return array $XMLResponseTag The array stucture that will be written into the next line of the xml file
    * array('ReferenceCode' => $QuestionCode, 
    *       'ItemCode' => $ItemCode,
    *       'EntitySequence' => $EntitiySeq,
    *       'EntityDescription' => $EntityDescription,
    *       'TextArea' => $TextArea);
    */        
    protected function GetXMLAnswerLine($nIncident, $QuestionCode, $EntitiySeq = 0, $nLinkID = '', $boolean = '')
    {
        $EntityDescription = '';
        $lsTextArea = '';
        $XMLResponseTag = $this->npsa->GetNPSAAnswer($nIncident, $QuestionCode, $EntitiySeq, $EntityDescription, $lsTextArea, $nLinkID);

        return $XMLResponseTag;
    }

    /**
    * File writer / file downloader method
    * 
    * @param string $mode The output mode ('download' = prompt the user for download |'dfile' = dump file)
    */
    protected function OutputNPSAExport($mode = 'download')
    {
        if ($mode == 'download')
        {
            setcookie('fileExportToken', Sanitize::SanitizeString($_POST['export_token_value']).'|success', 0, '', '', ($_SERVER["HTTPS"] == "on" ? true : false));
            header('Content-Type: text/xml');
            header('Content-Disposition: attachment; filename=NPSA '.$this->strToday.'.xml');
            header('Content-Encoding: none');
            echo $this->batchxml->asXML();
            exit; 
        }
        elseif ($mode == 'dfile')
        {
            $this->batchxml->asXML('NPSA '.$this->strToday.'.xml');
        }
    }
}