<form>
    <div style="clear:both;">
        <div style="float:left; width:150px; font-weight:bold">&nbsp;</div>
        <div style="float:left; font-weight:bold"><?php echo $GLOBALS['txt']['isd_month'] ?></div>
        <div style="float:left; font-weight:bold; margin-left:20px"><?php echo $GLOBALS['txt']['isd_year'] ?></div>
    </div>
    <div style="clear:both; height:5px"></div>
    <div style="clear:both">
        <div style="float:left; width:150px; font-weight:bold"><?php echo $GLOBALS['txt']['isd_ref_date'] ?></div>
        <div style="float:left"><?php echo $month->getField() ?></div>
        <div style="float:left; margin-left:10px"><?php echo $year->getField() ?></div>
    </div>
    <div style="clear:both; height:10px"></div>
    <div style="clear:both">
        <div style="float:left; width:150px; font-weight:bold"><?php echo $GLOBALS['txt']['isd_edition'] ?></div>
        <div style="float:left"><?php echo $edition->getField() ?></div>
    </div>
    <div style="clear:both; height:10px"></div>
    <div style="text-align:center">
        <input type="button" value="<?php echo _tk('btn_ok')?>" onclick="if (isdFormValidated()){exportFile(scripturl+'?service=export&event=isd', 'displayExportError(txt[\'isd_export_error_title\'])', {'isd_month' : jQuery('#isd_month').val(), 'isd_year' : jQuery('#isd_year').val(), 'isd_edition' : jQuery('#isd_edition').val()});var div=GetFloatingDiv('isd_dialogue');div.CloseFloatingControl();displayLoadPopup();}" />
        <input type="button" value="<?php echo _tk('btn_cancel')?>" onclick="var div=GetFloatingDiv('isd_dialogue');div.CloseFloatingControl()" />
    </div>
</form>
<?php echoJSFunctions() ?>