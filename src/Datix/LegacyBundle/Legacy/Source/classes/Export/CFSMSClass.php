<?php
/**
* Handles the CFSMS XML export.
*/
class Export_CFSMS
{
    /**
    * The XML file object.
    * 
    * @var Files_XML
    */
    protected $xmlFile;
    
    /**
    * The object used to create the XML.
    * 
    * @var DOMDocument
    */
    protected $xml;
    
    /**
    * The where clause for the records we're exporting.
    * 
    * @var string
    */
    protected $where;
    
    /**
    * The value for the SIRS_TRUST global.  Must be set for data to be exported.
    * 
    * @var string
    */
    protected $sirsTrust;
    
    /**
    * Constructor - initialise $xmlFile/$xml parameters, creates security where clause used in export and sets SIRS Trust value.
    * 
    * @param Files_XML   $xmlFile
    * @param DOMDocument $xml
    */
    public function __construct(Files_XML $xmlFile, DOMDocument $xml)
    {
        require_once 'Source/libs/SavedQueries.php';

        $this->xmlFile   = $xmlFile;
        $this->xml       = $xml;
        $this->where     = MakeSecurityWhereClause($_SESSION['INC']['WHERE'], 'INC', $_SESSION["initials"]);

        // We need to replace special keywords from the search query before executing it.
        $this->where = replaceAtPrompts($this->where, 'INC');
        $this->where = TranslateWhereCom($this->where, $_SESSION["initials"], 'INC');

        $this->sirsTrust = getParm('SIRS_TRUST', '', true);    
    }
    
    /**
    * Controls the export process.
    */
    public function doExport()
    {
        $this->checkSirsTrust();
        $this->setExportDates();
        $root = $this->setHeaders();
        
        $incidents = $this->getIncidents();
        foreach ($incidents as $incident)
        {
            $incElement = $root->appendChild($this->xml->createElement('Incident'));
            $incKeyElement = $incElement->appendChild($this->xml->createElement('nid:New_Incident'));        
            $incKeyElement->setAttribute('Incident_Key', $incident['recordid']);
            
            $this->addIncidentDetails($incKeyElement, $incident);
            $this->addIncidentPerpetrators($incKeyElement, $incident);
            $this->addIncidentVictims($incKeyElement, $incident);
            $this->addPersonalProperty($incKeyElement, $incident);        
            $this->addTrustProperty($incKeyElement, $incident);
            $this->addIncidentPolice($incKeyElement, $incident);
            $this->addAggrFactors($incKeyElement, $incident);        
        }
        
        $this->xmlFile->setContents($this->xml->saveXML());
        $this->xmlFile->decodeLineBreaks(); // SIRS parser can't handle encoded line breaks.
        $this->xmlFile->export();    
    }
    
    /**
    * Checks that the SIRS_TRUST global has been set.  If not, 
    * the user is prompted to set this before continuing with the export.
    */
    protected function checkSirsTrust()
    {
        if ($this->sirsTrust == '')
        {
            $this->xmlFile->failExport();                
        }
    }
    
    /**
    * Sets the first/most recent export dates for the incident records in this export.
    */
    protected function setExportDates()
    {
        // set most recent export date
        $sql = 'UPDATE incidents_main SET inc_pars_dexport = \''.date('Y-m-d').'\'';
        if ($this->where != '')
        {
            $sql .= ' WHERE '.$this->where;        
        }
        DatixDBQuery::PDO_query($sql);
        
        // set first date for records which have never been exported
        $sql = 'UPDATE incidents_main SET inc_pars_first_dexport = \''.date('Y-m-d').'\' WHERE ';
        if ($this->where != '')
        {
            $sql .= $this->where . ' AND ';       
        }
        $sql .= '(inc_pars_first_dexport IS NULL)';
        DatixDBQuery::PDO_query($sql);    
    }
    
    /**
    * Sets header elements for XML.
    * 
    * @return DOMElement $element The root element.
    */
    protected function setHeaders()
    {
        $element = $this->xml->createElementNS('http://www.incidentdata.com', 'icd:Incident_Data');
        $element->setAttribute('xsi:schemaLocation', 'http://www.incidentdata.com incidentData_v1.2.xsd');
        $element->setAttribute('xmlns:nid', 'http://www.newincidentdata.com');
        $element->setAttribute('xmlns:upd', 'http://www.userdefinedtypes.com');
        $element->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $element->appendChild($this->xml->createElement('Schema_Version', '1.2'));
        $element->appendChild($this->xml->createElement('RMS_Version', DatixDBQuery::PDO_fetch('SELECT version + \'.\' + patchlevel FROM aaa_version', array(), PDO::FETCH_COLUMN)));
        
        $this->xml->appendChild($element);
        return $element;
    }
    
    /**
    * Retrieves the incident records to be exported.
    * 
    * @return array The incident records.
    */
    protected function getIncidents()
    {
        $sql = 'SELECT
                    inc_name, inc_ourref, recordid, inc_organisation, inc_unit, inc_clingroup, inc_directorate,
                    inc_specialty, inc_loctype, inc_locactual, inc_type, inc_category, inc_subcategory, inc_carestage,
                    inc_clin_detail, inc_clintype, inc_result, inc_severity, inc_cnstitype, inc_dincident, inc_time,
                    inc_dreported, inc_dopened, inc_dsched, inc_notify, inc_notes, inc_actiontaken, inc_unit_type,
                    inc_clinical, inc_mgr, inc_head, inc_agg_issues, inc_pol_call_time, inc_pol_attend, inc_pol_action,
                    inc_inv_action, inc_is_riddor, inc_inv_lessons, inc_pol_att_time, inc_pol_crime_no, inc_user_action,
                    inc_pol_called, inc_pars_pri_type, inc_pars_sec_type, inc_pars_clinical, inc_address, inc_pars_address,
                    inc_postcode, inc_pars_dexport, inc_pars_first_dexport, inc_tprop_damaged
                FROM
                    incidents_main'.
                ($this->where != '' ? '
                WHERE '.$this->where : '').'
                ORDER BY
                    recordid';
                    
        return DatixDBQuery::PDO_fetch_all($sql);      
    }
    
    /**
    * Adds main incident record info to XML output.
    * 
    * @param DOMElement $element    The parent (incident) element
    * @param array      $incRecord  The incident record data
    */
    protected function addIncidentDetails(DOMElement $element, array $incRecord)
    {            
        $element->appendChild($this->xml->createElement('Trust_Number', $this->xmlFile->XMLEntities($this->sirsTrust)));            
        $element->appendChild($this->xml->createElement('Record_Source', 'Datix'));            
        $element->appendChild($this->xml->createElement('Trust_Incident_Reference', $incRecord['recordid']));            
        $element->appendChild($this->xml->createElement('trustNHSfreeFormatName', $this->xmlFile->XMLEntities(getParm('CLIENT'))));            
        $element->appendChild($this->xml->createElement('Incident_Date', $this->formatDate($incRecord['inc_dincident'], 'Y-m-d')));            
        $element->appendChild($this->xml->createElement('Incident_Site', code_descr('INC', 'inc_unit', $this->xmlFile->XMLEntities($incRecord['inc_unit']))));            
        $element->appendChild($this->xml->createElement('Incident_Department', $this->xmlFile->XMLEntities(code_descr('INC', 'inc_loctype', $incRecord['inc_loctype']))));            
        $element->appendChild($this->xml->createElement('Incident_Directorate', $this->xmlFile->XMLEntities(code_descr('INC', 'inc_directorate', $incRecord['inc_directorate']))));            
        $element->appendChild($this->xml->createElement('Incident_Address1', $this->xmlFile->XMLEntities($incRecord['inc_pars_address'])));            
        $element->appendChild($this->xml->createElement('Incident_Postcode', $this->xmlFile->XMLEntities($incRecord['inc_postcode'])));            
        $element->appendChild($this->xml->createElement('Incident_Description', $this->xmlFile->XMLEntities($incRecord['inc_notes'])));
        $element->appendChild($this->xml->createElement('Reported_to_Police', $this->xmlFile->XMLEntities(code_descr('INC', 'inc_pol_called', $incRecord['inc_pol_called']))));            
        $element->appendChild($this->xml->createElement('Police_Attended', $this->xmlFile->XMLEntities(code_descr('INC', 'inc_pol_attend', $incRecord['inc_pol_attend']))));            
        $element->appendChild($this->xml->createElement('Incident_Created_Datetime_Stamp', $this->formatDate($incRecord['inc_dreported'], 'Y-m-d H:i:s')));            
    }
    
    /**
    * Adds perpetrator info to XML output.
    * 
    * @param DOMElement $element    The parent (incident) element
    * @param array      $incRecord  The incident record data
    */
    protected function addIncidentPerpetrators(DOMElement $element, array $incRecord)
    {
        $perps = $this->getPerpetrators($incRecord['recordid']);
        foreach ($perps as $perp)
        {
            $perpElement = $element->appendChild($this->xml->createElement('Perpetrator'));    
            $perpElement->setAttribute('Perpetrator_Key', $perp['recordid']);
            
            $perpElement->appendChild($this->xml->createElement('Incident_Key', $incRecord['recordid']));
            $perpElement->appendChild($this->xml->createElement('Specific_Age', $perp['link_age']));
            $perpElement->appendChild($this->xml->createElement('Ethnicity', $this->xmlFile->XMLEntities(code_descr('CON', 'con_ethnicity', $perp['con_ethnicity']))));
            
            $perpType = $this->getSIRSCode('con_type', 'CON', $perp['con_type'], 'SMI00164', $sirsDesc);
            $perpElement->appendChild($this->xml->createElement('Perpetrator_Type', $perpType));
            if ($perpType == 'OTHER')
            {
                $perpElement->appendChild($this->xml->createElement('Other_Perpetrator_Type_Description', $this->xmlFile->XMLEntities(code_descr('INC', 'link_status', $perp['link_status']))));    
            }
            
            $perpElement->appendChild($this->xml->createElement('Perpetrator_First_Name', $this->xmlFile->XMLEntities($perp['con_forenames'])));
            $perpElement->appendChild($this->xml->createElement('Perpetrator_Last_Name', $this->xmlFile->XMLEntities($perp['con_surname'])));
            $perpElement->appendChild($this->xml->createElement('Gender', $this->xmlFile->XMLEntities($perp['con_gender'])));
            $perpElement->appendChild($this->xml->createElement('Perpetrator_Address_Freeformat', $this->xmlFile->XMLEntities($perp['con_address'])));
            $perpElement->appendChild($this->xml->createElement('Perpetrator_Postcode', $this->xmlFile->XMLEntities($perp['con_postcode'])));
            $perpElement->appendChild($this->xml->createElement('Perpetrator_DOB', $this->formatDate($perp['con_dob'], 'Y-m-d')));
            $perpElement->appendChild($this->xml->createElement('Perpetrator_Telephone', $this->xmlFile->XMLEntities($perp['con_tel1'])));
            
            $this->addPerpetratorIncTypes($perpElement, $incRecord['inc_pars_pri_type'], $perp['recordid']);
        }    
    }
    
    /**
    * Retrieves the perpetrator records to be exported.
    * 
    * @param  int   $incID
    * 
    * @return array
    */
    protected function getPerpetrators($incID)
    {
        $perpRoleCodes = array_map('trim',explode(',', getParm('SIRS_PERP_LINK_ROLE', 'PERP')));

        $sql = 'SELECT
                    recordid, con_surname, con_forenames, con_address, con_postcode, con_type, con_subtype, con_dob,
                    con_dod, con_notes, con_email, con_tel1, con_tel2, con_number, con_nhsno, con_ethnicity, con_language,
                    con_jobtitle, con_gender, con_police_number, link_status, link_role, link_daysaway, link_treatment,
                    link_become_unconscious, link_req_resuscitation, link_hospital_24hours, link_worked_alone, link_age
                FROM
                    link_contacts, contacts_main
                WHERE
                    inc_id = :incID and con_id = contacts_main.recordid and link_contacts.link_type = \'N\'
                    and link_role in (\''.implode('\',\'', $perpRoleCodes).'\')
                ORDER BY
                    con_surname, con_forenames';
                    
        return DatixDBQuery::PDO_fetch_all($sql, array('incID' => $incID));
    }
    
    /**
    * Adds incident type info to each perpetrator record in the XML output.
    * 
    * @param DOMElement $element The perp element we're appending to.
    * @param string     $incType The incident type.
    * @param int        $conID   The contact ID.
    */
    protected function addPerpetratorIncTypes(DOMElement $element, $incType, $conID)
    {
        if ($incType != '')
        {
            $types = explode(' ', $incType);
            foreach ($types as $type)
            {
                $perpToPerpElement = $element->appendChild($this->xml->createElement('Perp_to_Perp_Incident'));
                $perpToPerpElement->appendChild($this->xml->createElement('Perpetrator_Key', $conID));
                $perpToPerpElement->appendChild($this->xml->createElement('Incident_Type', $this->getSIRSCode('inc_pars_pri_type', 'INC', $type, 'SIRSTYPE', $sirsDescr)));
            }    
        }        
    }
    
    /**
    * Adds victim info to XML output.
    * 
    * @param DOMElement $element    The parent (incident) element
    * @param array      $incRecord  The incident record data
    */
    protected function addIncidentVictims(DOMElement $element, array $incRecord)
    {
        $victims = $this->getVictims($incRecord['recordid']);
        foreach ($victims as $victim)
        {
            $victimElement = $element->appendChild($this->xml->createElement('Victim'));    
            $victimElement->setAttribute('Trust_Victim_Key', $victim['recordid']);
            
            $victimElement->appendChild($this->xml->createElement('Incident_Key', $incRecord['recordid']));
            $victimElement->appendChild($this->xml->createElement('Clinical_Factor', $this->getSirsMultiCode('link_clin_factors', 'INC', $victim['link_clin_factors'], 'SMI00083')));
            $victimElement->appendChild($this->xml->createElement('Ethnicity', $this->xmlFile->XMLEntities(code_descr('CON', 'con_ethnicity', $victim['con_ethnicity']))));
            
            $this->getSIRSCode('con_empl_grade', 'INC', $victim['con_empl_grade'], 'SMI00064', $sirsDesc);
            $victimElement->appendChild($this->xml->createElement('Staff_Grade', $this->xmlFile->XMLEntities($sirsDesc)));
            
            $this->getSIRSCode('link_status', 'INC', $victim['link_status'], 'SMI00066', $sirsDesc);
            $victimElement->appendChild($this->xml->createElement('Staff_Role', $this->xmlFile->XMLEntities($sirsDesc)));
            
            $victimElement->appendChild($this->xml->createElement('Victim_First_Name', $this->xmlFile->XMLEntities($victim['con_forenames'])));
            $victimElement->appendChild($this->xml->createElement('Victim_Surname', $this->xmlFile->XMLEntities($victim['con_surname'])));
            $victimElement->appendChild($this->xml->createElement('Victim_DOB', $this->formatDate($victim['con_dob'], 'Y-m-d')));
            $victimElement->appendChild($this->xml->createElement('Victim_Telephone_Number', $this->xmlFile->XMLEntities($victim['con_tel1'])));
            $victimElement->appendChild($this->xml->createElement('Physical_Contact_Made', $victim['link_direct_indirect']));
            $victimElement->appendChild($this->xml->createElement('Physical_Injury', $victim['link_injury_caused']));
            $victimElement->appendChild($this->xml->createElement('Personal_Discomfort', $victim['link_discomfort_caused']));
            $victimElement->appendChild($this->xml->createElement('Verbal_Abuse', $victim['link_verbal_abuse']));
            $victimElement->appendChild($this->xml->createElement('Attempted_Assualt', $victim['link_attempted_assault']));
            $victimElement->appendChild($this->xml->createElement('Harrassment_or_MAL_Comms', $victim['link_harassment']));
            $victimElement->appendChild($this->xml->createElement('Lone_Worker', $victim['link_worked_alone']));
            $victimElement->appendChild($this->xml->createElement('Lone_Worker_Risk_Assessed', $victim['con_work_alone_assessed']));
            $victimElement->appendChild($this->xml->createElement('Unconcious', $victim['link_become_unconscious']));
            $victimElement->appendChild($this->xml->createElement('Resuscitation', $victim['link_req_resuscitation']));
            $victimElement->appendChild($this->xml->createElement('Hospitalisation_24H', $victim['link_hospital_24hours']));
            $victimElement->appendChild($this->xml->createElement('Number_Working_Days_Absent', $victim['link_daysaway']));
            $victimElement->appendChild($this->xml->createElement('Gender', $victim['con_gender']));
            $victimElement->appendChild($this->xml->createElement('Persued_by_Police', $victim['link_police_pursue']));
            $victimElement->appendChild($this->xml->createElement('Police_Non_Persued_Reason', $victim['link_police_persue_reason']));
            $victimElement->appendChild($this->xml->createElement('Public_Disorder_Caused', $victim['link_public_disorder']));
            $victimElement->appendChild($this->xml->createElement('Is_Personal_Property_LOD', $victim['link_pprop_damaged']));
                
            $this->addContactInjuries($incRecord['recordid'], $victim['recordid'], $victim['link_treatment'], $victimElement);
        }    
    }
    
    /**
    * Retrieves the victim records to be exported.
    * 
    * @param  int   $incID
    * 
    * @return array
    */
    protected function getVictims($incID)
    {
        $victimRoleCodes = array_map('trim',explode(',', getParm('SIRS_VICTIM_LINK_ROLE', 'VICT')));

        $sql = 'SELECT
                    recordid, con_surname, con_forenames, con_address, con_postcode, con_type, con_subtype, con_dob,
                    con_dod, con_notes, con_email, con_tel1, con_tel2, con_number, con_nhsno, con_ethnicity, con_language,
                    con_jobtitle, con_gender, con_police_number, con_work_alone_assessed, con_empl_grade, link_status, link_role,
                    link_daysaway, link_treatment, link_become_unconscious, link_req_resuscitation, link_hospital_24hours,
                    link_worked_alone, link_age, link_pprop_damaged, link_clin_factors, link_direct_indirect, link_injury_caused,
                    link_discomfort_caused, link_attempted_assault, link_public_disorder, link_harassment, link_police_pursue, 
                    link_police_persue_reason, link_verbal_abuse
                FROM
                    link_contacts, contacts_main
                WHERE
                    inc_id = :incID and con_id = contacts_main.recordid and link_contacts.link_type = \'A\'
                    and link_role in (\''.implode('\',\'', $victimRoleCodes).'\')
                ORDER BY
                    con_surname, con_forenames';


        return DatixDBQuery::PDO_fetch_all($sql, array('incID' => $incID));
    }
    
    /**
    * Adds injury info to XML output.
    * 
    * @param int        $incID      The incident recordid
    * @param int        $conID      The contact recordid
    * @param string     $treatment  The link_treatment data for this incident/contact
    * @param DOMElement $element    The parent element the injury details are being appended to
    */
    protected function addContactInjuries($incID, $conID, $treatment, DOMElement $element)
    {
        $injuries = $this->getContactInjuries($incID, $conID);   
        foreach ($injuries as $injury)
        {
            $injuryElement = $element->appendChild($this->xml->createElement('Injury'));    
            $injuryElement->setAttribute('Trust_Injury_Key', $injury['recordid']);
            
            $injuryElement->appendChild($this->xml->createElement('Trust_Victim_Key', $conID));
            
            $this->getSIRSCode('inc_bodypart', 'INC', $injury['inc_bodypart'], 'SMI00096', $sirsDesc, 'INCINJ');
            $injuryElement->appendChild($this->xml->createElement('Body_Part', $this->xmlFile->XMLEntities($sirsDesc)));
            
            $this->getSIRSCode('inc_severity', 'INC', $injury['inc_severity'], 'SMI00102', $sirsDesc);
            $injuryElement->appendChild($this->xml->createElement('Injury_Severity', $this->xmlFile->XMLEntities($sirsDesc)));
            
            $injuryElement->appendChild($this->xml->createElement('Treatment_Applied', $this->xmlFile->XMLEntities(code_descr('INC', 'link_treatment', $treatment))));
            
            $injuryTypeElement = $injuryElement->appendChild($this->xml->createElement('Injury_type'));
            $injuryTypeElement->appendChild($this->xml->createElement('Trust_Victim_Key', $conID));
            
            $this->getSIRSCode('inc_injury', 'INC', $injury['inc_injury'], 'SMI00097', $sirsDesc, 'INCINJ');
            $injuryTypeElement->appendChild($this->xml->createElement('Injury_Type', $this->xmlFile->XMLEntities($sirsDesc)));       
        }    
    }
    
    /**
    * Retrieves the injury info to be exported fo a given contact.
    * 
    * @param  int   $incID
    * @param  int   $conID
    * 
    * @return array
    */
    protected function getContactInjuries($incID, $conID)
    {
        $sql = 'SELECT
                    inc_injuries.recordid, inc_injuries.inc_bodypart, inc_injuries.inc_injury, inc_severity
                FROM
                    inc_injuries, incidents_main
                WHERE
                    inc_id = :incID and con_id = :conID and inc_id = incidents_main.recordid 
                ORDER BY
                    listorder';
                    
        return DatixDBQuery::PDO_fetch_all($sql, array('incID' => $incID, 'conID' => $conID));    
    }
    
    /**
    * Adds personal property info to XML output.
    * 
    * @param DOMElement $element    The parent (incident) element
    * @param array      $incRecord  The incident record data
    */
    protected function addPersonalProperty(DOMElement $element, array $incRecord)
    {
        $properties = $this->getPersonalProperty($incRecord['recordid']);
        foreach ($properties as $property)
        {
            $propertyElement = $element->appendChild($this->xml->createElement('Personal_Property'));    
            $propertyElement->setAttribute('Personal_Property_Key', $property['recordid']);
            
            $propertyElement->appendChild($this->xml->createElement('Incident_Key', $incRecord['recordid']));        
            $propertyElement->appendChild($this->xml->createElement('Trust_Victim_Key', $property['con_id']));        
            $propertyElement->appendChild($this->xml->createElement('Property_Description', $this->xmlFile->XMLEntities($property['ipp_description'])));        
            $propertyElement->appendChild($this->xml->createElement('Property_Value', number_format(floatval($property['ipp_value']), 2)));
            
            $propertyTypeElement = $propertyElement->appendChild($this->xml->createElement('Loss_or_Damage_Type'));
            $propertyTypeElement->appendChild($this->xml->createElement('Personal_Property_Key', $property['recordid']));
            $propertyTypeElement->appendChild($this->xml->createElement('LOD_Type', $this->getSIRSCode('ipp_damage_type', 'INC', $property['ipp_damage_type'], 'SMI00109', $sirsDescr)));        
        }
    }
    
    /**
    * Retrieves the personal property records to be exported.
    * 
    * @param  int   $incID
    * 
    * @return array
    */
    protected function getPersonalProperty($incID)
    {
        $victimRoleCodes = array_map('trim',explode(',', getParm('SIRS_VICTIM_LINK_ROLE', 'VICT')));

        $sql = 'SELECT
                    recordid, ipp_damage_type, ipp_description, ipp_value, inc_personal_property.con_id
                FROM
                    inc_personal_property, link_contacts
                WHERE
                    inc_personal_property.inc_id = :incID AND inc_personal_property.inc_id = link_contacts.inc_id AND inc_personal_property.con_id = link_contacts.con_id
                    and link_role in (\''.implode('\',\'', $victimRoleCodes).'\')
                ORDER BY
                    listorder';
                    
        return DatixDBQuery::PDO_fetch_all($sql, array('incID' => $incID));
    }
    
    /**
    * Adds trust property info to XML output.
    * 
    * @param DOMElement $element    The parent (incident) element
    * @param array      $incRecord  The incident record data
    */
    protected function addTrustProperty(DOMElement $element, array $incRecord)
    {
        $properties = $this->getTrustProperty($incRecord['recordid']);
        foreach ($properties as $property)
        {
            $propertyElement = $element->appendChild($this->xml->createElement('TrustProperty'));    
            $propertyElement->setAttribute('Trust_Property_Key', $property['recordid']);
            
            $propertyElement->appendChild($this->xml->createElement('Incident_Key', $incRecord['recordid']));        
            $propertyElement->appendChild($this->xml->createElement('Items_Replaced', $property['link_replaced']));        
            $propertyElement->appendChild($this->xml->createElement('Replacement_Cost', number_format(floatval($property['link_replace_cost']), 0)));        
            $propertyElement->appendChild($this->xml->createElement('Items_Repaired', $property['link_repaired']));
            $propertyElement->appendChild($this->xml->createElement('Repair_Cost', number_format(floatval($property['link_repair_cost']), 0)));
            $propertyElement->appendChild($this->xml->createElement('Items_Written_Off', $property['link_written_off']));
            $propertyElement->appendChild($this->xml->createElement('Disposal_Cost', number_format(floatval($property['link_disposal_cost']), 0)));
            $propertyElement->appendChild($this->xml->createElement('Items_Sold', $property['link_sold']));
            $propertyElement->appendChild($this->xml->createElement('Sale_Price', number_format(floatval($property['link_sold_price']), 0)));
            $propertyElement->appendChild($this->xml->createElement('Items_Decommissioned', $property['link_decommissioned']));
            $propertyElement->appendChild($this->xml->createElement('Decommissioned_Cost', number_format(floatval($property['link_decommission_cost']), 0)));
            $propertyElement->appendChild($this->xml->createElement('Residual_Value', number_format(floatval($property['link_residual_value']), 0)));
            
            $incTPElement = $propertyElement->appendChild($this->xml->createElement('Incident_TP_Category'));
            $incTPElement->appendChild($this->xml->createElement('Trust_Property_Key', $property['recordid']));
            $incTPElement->appendChild($this->xml->createElement('Property_Category', $this->getSIRSCode('ast_category', 'INC', $property['ast_category'], 'SMI00117', $sirsDescr)));
            
            $lodElement = $propertyElement->appendChild($this->xml->createElement('Trust_Property_LOD'));
            $lodElement->appendChild($this->xml->createElement('Trust_Property_Key', $property['recordid']));
            $lodElement->appendChild($this->xml->createElement('LOD_Type', $this->getSIRSCode('link_damage_type', 'INC', $property['link_damage_type'], 'SMI00109', $sirsDescr)));        
            $lodElement->appendChild($this->xml->createElement('Other_LOD_Type_Desc', $this->xmlFile->XMLEntities($property['link_other_damage_info'])));        
        }
    }
    
    /**
    * Retrieves the trust property records to be exported.
    * 
    * @param  int   $incID
    * 
    * @return array
    */
    protected function getTrustProperty($incID)
    {
        $sql = 'SELECT 
                    recordid, ast_descr, ast_category, ast_cat_other_info, link_damage_type, link_other_damage_info,
                    link_replaced, link_replace_cost, link_repaired, link_repair_cost, link_written_off, link_disposal_cost,
                    link_sold, link_sold_price, link_decommissioned, link_decommission_cost, link_residual_value
                FROM
                    link_assets, assets_main
                WHERE
                    inc_id = :incID and ast_id = assets_main.recordid and link_assets.link_type = \'E\'
                ORDER BY
                    link_assets.link_recordid' ;
                    
        return DatixDBQuery::PDO_fetch_all($sql, array('incID' => $incID));
    }
    
    /**
    * Adds police info to XML output.
    * 
    * @param DOMElement $element    The parent (incident) element
    * @param array      $incRecord  The incident record data
    */
    protected function addIncidentPolice(DOMElement $element, array $incRecord)
    {
        $policeInvolvement = $element->appendChild($this->xml->createElement('Police_Incident_Involvement'));
        $policeInvolvement->setAttribute('Police_Incident_Key', $incRecord['recordid']);
        $policeInvolvement->appendChild($this->xml->createElement('Incident_Key', $incRecord['recordid']));
        $policeInvolvement->appendChild($this->xml->createElement('Reported_to_Police', $this->xmlFile->XMLEntities(code_descr('INC', 'inc_pol_called', $incRecord['inc_pol_called']))));
        $policeInvolvement->appendChild($this->xml->createElement('Police_Attended', $this->xmlFile->XMLEntities(code_descr('INC', 'inc_pol_attend', $incRecord['inc_pol_attend']))));
        $policeInvolvement->appendChild($this->xml->createElement('Police_Log_Number', $incRecord['inc_pol_crime_no']));
        
        $policeRecords = $this->getIncidentPolice($incRecord['recordid']);
        $getPoliceAddress = false;
        foreach ($policeRecords as $police)
        {
            if (!$getPoliceAddress)
            {
                // get the first police contact address only for incident
                $policeInvolvement->appendChild($this->xml->createElement('Police_Telephone_Number', $this->xmlFile->XMLEntities($police['con_tel1'])));
                $policeInvolvement->appendChild($this->xml->createElement('Police_Address_Freeformat', $this->xmlFile->XMLEntities($police['con_address'])));
                $policeInvolvement->appendChild($this->xml->createElement('Police_Postcode', $this->xmlFile->XMLEntities($police['con_postcode'])));
                $getPoliceAddress = true;   
            }
            
            $policeElement = $policeInvolvement->appendChild($this->xml->createElement('Involved_Police_Officers'));    
            
            $policeElement->appendChild($this->xml->createElement('Police_Incident_Key', $incRecord['recordid']));              
            $policeElement->appendChild($this->xml->createElement('Officer_First_Name', $this->xmlFile->XMLEntities($police['con_forenames'])));        
            $policeElement->appendChild($this->xml->createElement('Officer_Surname', $this->xmlFile->XMLEntities($police['con_surname'])));        
            $policeElement->appendChild($this->xml->createElement('Officer_Collar_Number', $this->xmlFile->XMLEntities($police['con_police_number'])));        
            $policeElement->appendChild($this->xml->createElement('Officer_Identity', $this->xmlFile->XMLEntities($police['recordid'])));    
        }
    }
    
    /**
    * Retrieves the police records to be exported.
    * 
    * @param  int   $incID
    * 
    * @return array
    */
    protected function getIncidentPolice($incID)
    {
        $policeRoleCodes = array_map('trim',explode(',', getParm('SIRS_POLICE_LINK_ROLE', 'POLICE')));

        $sql = 'SELECT
                    recordid, con_surname, con_forenames, con_address, con_postcode, con_type, con_subtype,
                    con_dob, con_dod, con_notes, con_email, con_tel1, con_tel2, con_number, con_nhsno, con_ethnicity,
                    con_language, con_jobtitle, con_gender, con_police_number, link_status, link_role, link_daysaway,
                    link_treatment, link_become_unconscious, link_req_resuscitation, link_hospital_24hours, 
                    link_worked_alone, link_age
                FROM
                    link_contacts, contacts_main
                WHERE
                    inc_id = :incID and con_id = contacts_main.recordid and link_contacts.link_type = \'N\'
                    and link_role in (\''.implode('\',\'', $policeRoleCodes).'\')
                ORDER BY
                    link_recordid';
                    
        return DatixDBQuery::PDO_fetch_all($sql, array('incID' => $incID));
    }
    
    /**
    * Adds aggravating factors info to XML output.
    * 
    * @param DOMElement $element    The parent (incident) element
    * @param array      $incRecord  The incident record data
    */
    protected function addAggrFactors(DOMElement $element, array $incRecord)
    {
        if ($incRecord['inc_agg_issues'] != '')
        {
            $factors = explode(' ', $incRecord['inc_agg_issues']);
            foreach ($factors as $factor)
            {
                $factorsElement = $element->appendChild($this->xml->createElement('Incident_Aggr_Factors'));    
                
                $factorsElement->appendChild($this->xml->createElement('Incident_Key', $incRecord['recordid']));
                
                $code = $this->getSIRSCode('inc_agg_issues', 'INC', $factor, 'SMI00021', $sirsDesc);
                $factorsElement->appendChild($this->xml->createElement('Aggravating_Factor', $this->xmlFile->XMLEntities($sirsDesc)));
                
                if ($code == 'OTHER')
                {
                    $factorsElement->appendChild($this->xml->createElement('OtherDesc', $this->xmlFile->XMLEntities(code_descr('INC', 'inc_agg_issues', $factor))));    
                }
            }    
        }    
    }
           
    /**
    * Retrieves the mapped SIRS code/description for a given datix code.
    * 
    * @param  string $field           The datix field name
    * @param  string $module          The module the field belongs to
    * @param  string $value           The current value of the field
    * @param  string $sirsQuestionNo  The SIRS code type
    * @param  string $sirsDescr       The mapped SIRS descr (assigned by reference)
    * @param  string $table           fmt_table in field_formats - needed for fields where there are duplicate definitions (e.g. inc_bodypart/inc_injury)
    * 
    * @return string $sirsCode        The mapped SIRS code
    */
    protected function getSIRSCode($field, $module, $value, $sirsQuestionNo, &$sirsDescr, $table = '')
    {
        $fformat = getFieldFormat($field, $module, $table);
        $table   = $fformat['fmt_code_table'];
        
        if ($table{0} == '!')
        {
            $code_type  = \UnicodeString::substr($table, 1);
            $table      = 'code_types';
            $field      = 'cod_code';
            $where      = ' cod_type = \'' . $code_type . '\' and ' . $field . ' = \'' . $value . '\'';
            $descrField = 'cod_descr';       
        }
        else
        {
            $field      = $fformat['fmt_code_field'];
            $descrField = $fformat['fmt_code_descr'];
            $where      = $fformat['fmt_code_where'];
            
            if ($value != '')
            {
                if ($where != '')
                {
                    $where .= ' AND ' . $field . ' = \'' . $value . '\'';
                }
                else
                {
                    $where = $field . ' = \'' . $value . '\'';    
                }    
            }         
        }
        
        $sql = 'SELECT cod_sirs FROM ' . $table;
        if ($where != '')
        {
            $sql .= ' WHERE ' . $where;        
        }
        
        $sirsCode = DatixDBQuery::pdo_fetch($sql, array(), PDO::FETCH_COLUMN);
        if ($sirsCode != '')
        {
            $sirsDescr = DatixDBQuery::pdo_fetch('SELECT cod_descr FROM code_sirs_types WHERE cod_type = :sirsQuestionNo AND cod_code = :sirsCode', array('sirsQuestionNo' => $sirsQuestionNo, 'sirsCode' => $sirsCode), PDO::FETCH_COLUMN);        
        }
        else
        {
            $sirsDescr = null;    
        }
        
        return $sirsCode;
    }
    
    /**
    * Retrieves the mapped SIRS codes/descriptions for a given datix multicode.
    * 
    * @param  string $field           The datix field name
    * @param  string $module          The module the field belongs to
    * @param  string $value           The current value of the field
    * @param  string $sirsQuestionNo  The SIRS code type
    * 
    * @return string $sirsCodes        The mapped SIRS codes
    */
    protected function getSirsMultiCode($field, $module, $value, $sirsQuestionNo)
    {
        if ($value == '')
        {
            $sirsCodes = '';   
        }
        else
        {
            $codes = explode(' ', $value);
            foreach ($codes as $code)
            {
                if (!isset($sirsCodes))
                {
                    $sirsCodes = $this->getSIRSCode($field, $module, $code, $sirsQuestionNo, $sirsDescr);    
                }
                else
                {
                    $sirsCodes .= ' ' . $this->getSIRSCode($field, $module, $code, $sirsQuestionNo, $sirsDescr);    
                }           
            }   
        }
        return $sirsCodes;    
    }
    
    /**
    * Returns a date string based on the format provided, or an empty string if there's no date value.
    * 
    * @param  string $date
    * @param  string $format
    * 
    * @return string $formattedDate
    */
    protected function formatDate($date, $format)
    {
        $formattedDate = $date;
        if ($formattedDate != '')
        {
            $formattedDate = date($format, strtotime($date));        
        }
        return $formattedDate;   
    }
}
