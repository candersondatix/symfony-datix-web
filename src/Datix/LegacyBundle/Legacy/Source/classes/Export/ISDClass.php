<?php
/**
* Handles the ISD CSV export.
*/
class Export_ISD
{
    /**
    * The CSV file object.
    * 
    * @var Files_CSV
    */
    protected $csvFile;
    
    /**
    * The data to export in the CSV file.
    * 
    * @var array
    */
    protected $csvData;
    
    /**
    * The where clause for the records we're exporting.
    * 
    * @var string
    */
    protected $where;
    
    /**
    * Whether or not the data is validated, as controlled by the ISD_VALIDATION global.
    * 
    * @var string
    */
    protected $validate;
    
    /**
    * Validation errors for this export.
    * 
    * @var array
    */
    protected $errors;
    
    /**
    * Constructor - initialises csv/error variables, creates security where clause used in export and sets validate value.
    * 
    * @param Files_CSV $csvFile
    */
    public function __construct(Files_CSV $csvFile)
    {
        $this->csvFile  = $csvFile;
        $this->csvData  = array();
        $this->where    = MakeSecurityWhereClause($_SESSION['COM']['WHERE'], 'COM', $_SESSION["initials"]);
        $this->validate = bYN(getParm('ISD_VALIDATION', 'Y'));    
        $this->errors   = array();    
    }
    
    /**
    * Creates the dialogue which prompts users to enter a value for the ISD_AGENT global.
    */
    public function displayAgentDialogue()
    {
        $agent = new FormField();
        $agent->MakeInputField('isd_agent', '20', '', '');
        
        require_once dirname(__FILE__) . '/views/ISDAgentDialogue.php';
        obExit();    
    }
    
    /**
    * Creates the dialogue which allows users to enter month/year/edition values prior to export.
    */
    public function displayExportDialogue()
    {
        $month = Forms_SelectFieldFactory::createSelectField('isd_month', '', '', '');
        $month->setSelectFunction('getIsdMonth');
        $month->setSuppressCodeDisplay();
        $month->setNoResize();
        
        $year = Forms_SelectFieldFactory::createSelectField('isd_year', '', '', '');
        $year->setSelectFunction('getIsdYear');
        $year->setSuppressCodeDisplay();
        $year->setNoResize();
        
        $edition = new FormField();
        $edition->MakeInputField('isd_edition', '15', '', '');

        require_once dirname(__FILE__) . '/views/ISDDialogue.php';
        obExit();
    }
    
    /**
    * Controls the export process.
    */
    public function doExport()
    {
        $this->setHeader();
        
        $complaints = $this->getComplaints();
        foreach ($complaints as $complaint)
        {
            $complainant = $this->getComplainants($complaint['recordid']);
            $isdIssues = $this->getISDIssues($complaint['recordid']);
            
            $line = array();
            
            // A2 - Reference No / ID
            $ref = GetParm('ISD_A2', 'REF') == 'ID' ? 'recordid' : 'com_ourref';
            $line[] = $complaint[$ref];
            $this->checkAndLogMissingData($complaint[$ref], $complaint['recordid'], 'A2', $ref);
            
            // A6 - Received method
            $comMethod = $this->getISDCode('com_method', $complaint['com_method'], 'cod_isd');
            $line[] = $comMethod;
            $this->checkAndLogMissingData($comMethod, $complaint['recordid'], 'A6', 'com_method');
            
            // A11 - Received date
            $receivedDate = $this->formatDate($complaint['com_dreceived'], 'd/m/Y');
            $line[] = $receivedDate;
            $this->checkAndLogMissingData($complaint['com_dreceived'], $complaint['recordid'], 'A11', 'com_dreceived');
            
            // A12 - Acknowledged date
            $acknowledgedDate = $this->formatDate($complainant['lcom_dack'], 'd/m/Y');
            $line[] = $acknowledgedDate;
            $this->checkISDDates($complaint['com_dreceived'], $complainant['lcom_dack'], $complaint['recordid'], 'A12', 'lcom_dack', 'Date the complaint was acknowledged is earlier than the date the complaint was received');
            
            // A10 - Health sector
            $isdUnit = $this->getISDCode('com_isd_unit', $complaint['com_isd_unit'], 'cod_isd');
            $line[] = $isdUnit;
            $this->checkAndLogMissingData($isdUnit, $complaint['recordid'], 'A10', 'com_isd_unit');
            
            // B19 - Consent required?
            $line[] = $complaint['com_isd_consent'];
            $this->checkAndLogMissingData($complaint['com_isd_consent'], $complaint['recordid'], 'B19', 'com_isd_consent');
            
            // B21 - Received with consent date
            $consentReceived = $this->formatDate($complaint['com_isd_dconsent_rec'], 'd/m/Y');
            $line[] = $consentReceived;
            if ($complaint['com_isd_consent'] == 'Y')
            {
                $this->checkISDDates($complaint['com_dreceived'], $complaint['com_isd_dconsent_rec'], $complaint['recordid'], 'B21', 'com_isd_dconsent_rec', 'Date consent was recieved is earlier than the date the complaint was received');            
            }
            
            // A14 - Equality diversity form sent
            $line[] = $complaint['com_isd_div_sent'];
            $this->checkAndLogMissingData($complaint['com_isd_div_sent'], $complaint['recordid'], 'A14', 'com_isd_div_sent');
            
            // B22 - Location code
            $line[] = $complaint['com_isd_locactual'];
            $this->checkAndLogMissingData($complaint['com_isd_locactual'], $complaint['recordid'], 'B22', 'com_isd_locactual');
            
            $serviceArea = array();
            $admissionType = array();
            foreach ($isdIssues as $issueNo => $issue)
            {
                // C2 - Issues raised
                $line[] = $issue['cisd_subcategory'];
                
                // C3 - Staff Position
                $line[] = $issue['cisd_staff_position'];
                
                // C4 - Service area 1
                $serviceArea[$issueNo] = $this->getISDCode('cisd_service_area', $issue['cisd_service_area'], 'cod_isd');
                $line[] = $serviceArea[$issueNo];
                
                // C5 - Patient admission type
                $admissionType[$issueNo] = $this->getISDCode('cisd_pat_adm_type', $issue['cisd_pat_adm_type'], 'cod_isd');
                $line[] = $admissionType[$issueNo];
                
                // C6 - Specialty
                $line[] = $issue['cisd_specialty'];
            }
            
            // log missing data for first issue only
            $this->checkAndLogMissingData($isdIssues[0]['cisd_subcategory'], $complaint['recordid'], 'C2', 'cisd_subcategory');    
            $this->checkAndLogMissingData($isdIssues[0]['cisd_staff_position'], $complaint['recordid'], 'C3', 'cisd_staff_position');    
            $this->checkAndLogMissingData($serviceArea[0], $complaint['recordid'], 'C4', 'cisd_service_area');    
            $this->checkAndLogMissingData($admissionType[0], $complaint['recordid'], 'C5', 'cisd_pat_adm_type');    
            $this->checkAndLogMissingData($isdIssues[0]['cisd_specialty'], $complaint['recordid'], 'C6', 'cisd_specialty');
            
            // need to fill in gaps if fewer than three issue records
            if (count($isdIssues) != 3)
            {
                for ($i = 0; $i < (3 - count($isdIssues)) * 5; $i++)
                {
                    $line[] = '';   
                }   
            }
            
            // D7 - Outcome
            $outcome = $this->getISDCode('com_outcome', $complaint['com_outcome'], 'cod_isd');
            $line[] = $outcome;
            
            // D1 - Response Date
            $responseDate = $this->formatDate($complainant['lcom_dreplied'], 'd/m/Y');
            $line[] = $responseDate;
            $this->checkISDDates($complaint['com_dreceived'], $complainant['lcom_dreplied'], $complaint['recordid'], 'D1', 'lcom_dreplied', 'Date of response is earlier than the date the complaint was received');
            $this->checkISDDates($complainant['lcom_dack'], $complainant['lcom_dreplied'], $complaint['recordid'], 'D1', 'lcom_dreplied', 'Date of response is earlier than the date the complaint was acknowledged');
            $this->checkISDDates($complaint['com_isd_dconsent_rec'], $complainant['lcom_dreplied'], $complaint['recordid'], 'D1', 'lcom_dreplied', 'Date of response is earlier than the date consent was received');

            // D10 - Action type 1,2 and 3
            $actions = explode(' ', $complaint['com_isd_actions']);
            for ($i = 0; $i < 3; $i++)
            {
                $line[] = isset($actions[$i]) ? $this->getISDCode('com_isd_actions', $actions[$i], 'cod_isd') : '';        
            }
            
            $this->csvData[] = $line;    
        }
        
        $this->sendResponse();    
    }
    
    /**
    * Sets the header line for the CSV file.
    */
    protected function setHeader()
    {
        $this->csvData[] = array(
            'HEADER',
            'COMP',
            GetParm('ISD_AGENT', '', true),
            $_POST['isd_year'].$_POST['isd_month'],
            $_POST['isd_edition']
        );        
    }
    
    /**
    * Retrieves the complaint records to be exported.
    * 
    * @return array The complaint records.
    */
    protected function getComplaints()
    {
        $sql = 'SELECT
                    recordid, com_ourref, com_dreceived, com_kosubject, com_koservarea, com_koprof,
                    com_outcome, com_drequest, com_ddecision, com_dclosed, com_dreopened,
                    com_recir, com_outcome1, com_dinform, com_ircode, com_method, com_unit, com_locactual,
                    com_isd_unit, com_isd_locactual, com_isd_actions, com_isd_div_sent, com_isd_consent, com_isd_dconsent_rec
                FROM
                    compl_main'.
                ($this->where != '' ? '
                WHERE '.$this->where : '');
                    
        return DatixDBQuery::PDO_fetch_all($sql);    
    }
    
    /**
    * Retrieves the complainant info for this complaint record.
    * 
    * N.B. fetches the earliest dates for the primary complainant. 
    * 
    * @param int $comID The complaint recordid.
    * 
    * @return array The complainant info.       
    */
    protected function getComplainants($comID)
    {
        // get the primary complainant
        $sql = 'SELECT 
                    link_compl.con_id, \'primary_order\' = CASE WHEN link_compl.lcom_primary = \'Y\' THEN \'1\' ELSE \'2\' END
                FROM 
                    link_compl, link_contacts
                WHERE 
                    link_compl.com_id = link_contacts.com_id AND link_compl.com_id = :comID AND link_compl.con_id = link_contacts.con_id
                ORDER BY 
                    primary_order, link_compl.recordid, lcom_dreopened';
                    
        $conID = DatixDBQuery::PDO_fetch($sql, array('comID' => $comID), PDO::FETCH_COLUMN);
        
        // select the earliest dates for the primary complainant
        $sql = 'SELECT
                    link_compl.con_id, lcom_dresponse, lcom_dreceived, lcom_dreplied, lcom_dack, link_status
                FROM 
                    link_compl, link_contacts
                WHERE 
                    link_compl.com_id = link_contacts.com_id AND link_compl.com_id = :comID AND link_compl.con_id = link_contacts.con_id AND link_compl.con_id = :conID
                ORDER BY 
                    link_compl.recordid';
                    
        return DatixDBQuery::PDO_fetch($sql, array('comID' => $comID, 'conID' => $conID));    
    }
    
    /**
    * Retrieves the top 3 issue records for this complaint. 
    * 
    * @param int $comID The complaint recordid.
    * 
    * @return array The issue records.       
    */
    protected function getISDIssues($comID)
    {
        $sql = 'SELECT TOP 3 
                    cisd_type, cisd_category, cisd_subcategory, cisd_pat_adm_type, cisd_service_area, 
                    cisd_specialty, cisd_staff_group, cisd_staff_position
                FROM 
                    compl_isd_issues
                WHERE 
                    com_id = :comID 
                ORDER BY 
                    listorder, recordid';
                    
        return DatixDBQuery::PDO_fetch_all($sql, array('comID' => $comID));   
    }
    
    /**
    * Will either output the CSV file or fail the export if there were errors encountered.
    */
    protected function sendResponse()
    {
        if (empty($this->errors) || !$this->validate)
        {
            // set the export date for these records
            $sql = 'UPDATE compl_main SET com_isd_dexport = \''.date('Y-m-d').'\''.($this->where != '' ? ' WHERE '.$this->where : '');
            DatixDBQuery::PDO_query($sql);
            
            // export to CSV file
            $this->csvFile->setData($this->csvData);
            $this->csvFile->export();    
        }
        else
        {
            // error dialogue markup retrieved via ajax
            ob_start();
            require_once dirname(__FILE__) . '/views/ISDErrorDialogue.php';
            $this->csvFile->setExportError(ob_get_contents());
            ob_end_clean();
            
            // the error can also be exported in CSV format
            $_SESSION['isdErrors'] = array();
            foreach ($this->errors as $error)
            {
                $_SESSION['isdErrors'][] = explode('; ', $error);    
            }
            
            $this->csvFile->failExport();    
        }    
    } 
    
    /**
    * Gets the mapped ISD code for a given Datix field/value.
    * 
    * @param string $field
    * @param string $value
    * @param string $isdCode
    * 
    * @return string
    */
    function getISDCode($field, $value, $isdCode)
    {
        $fformat = getFieldFormat($field, 'COM');
        $table   = $fformat['fmt_code_table'];
        
        if ($table{0} == '!')
        {
            $code_type  = \UnicodeString::substr($table, 1);
            $table      = 'code_types';
            $field      = 'cod_code';
            $where      = ' cod_type = \'' . $code_type . '\' and ' . $field . ' = \'' . $value . '\'';      
        }
        else
        {
            $field      = $fformat['fmt_code_field'];
            $where      = $fformat['fmt_code_where'];
            
            if ($where != '')
            {
                $where .= ' AND ' . $field . ' = \'' . $value . '\'';
            }
            else
            {
                $where = $field . ' = \'' . $value . '\'';    
            }           
        }
        
        $sql = 'SELECT '. 
                    $isdCode.' 
                FROM '.
                    $table.
                ($where != '' ? '
                WHERE '.$where : '');
                
        return DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN);    
    }
    
    /**
    * Populates the errors array if the data is missing.
    * 
    * @param mixed $data
    * @param mixed $complaintID
    * @param mixed $questionNo
    * @param mixed $sourceField
    * @param mixed $otherInfo
    */
    protected function checkAndLogMissingData($data, $complaintID, $questionNo, $sourceField, $otherInfo = '')
    {
        if ($data == '')
        {
            $this->errors[] = 'Complaint: ' . $complaintID . ';  ISD Code: ' . $questionNo . '; Field: ' . GetFieldLabel($sourceField) . '; ' . $otherInfo;    
        }   
    }
    
    /**
    * Compares ISD dates and logs an error if necessary (i.e. if date2 is before date1).
    * 
    * @param string $date1
    * @param string $date2
    * @param int    $comID
    * @param string $questionNo
    * @param string $sourceField
    * @param string $otherInfo
    */
    protected function checkISDDates($date1, $date2, $comID, $questionNo, $sourceField, $otherInfo = '')
    {
        if ($date1 != '' && $date2 != '' && strtotime($date1) > strtotime($date2))
        {
            $this->checkAndLogMissingData('', $comID, $questionNo, $sourceField, $otherInfo);   
        }    
    }
    
    /**
    * Returns a date string based on the format provided, or an empty string if there's no date value.
    * 
    * @param  string $date
    * @param  string $format
    * 
    * @return string $formattedDate
    */
    protected function formatDate($date, $format)
    {
        $formattedDate = $date;
        if ($formattedDate != '')
        {
            $formattedDate = date($format, strtotime($date));        
        }
        return $formattedDate;   
    }
}
