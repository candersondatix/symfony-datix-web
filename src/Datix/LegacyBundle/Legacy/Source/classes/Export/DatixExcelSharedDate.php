<?php

require_once __DIR__.'\..\..\..\export\PHPExcel\Shared\Date.php';

/**
 * Class used to overwrite the methods to transform a PHP date to Excel.
 * We need this to allow us to not output the time as part of a date.
 */
class DatixExcelSharedDate extends PHPExcel_Shared_Date
{
    private static $ExcelBaseDate = self::CALENDAR_WINDOWS_1900;

    /**
     * Convert a date from PHP to Excel
     *
     * @param int  $dateValue   PHP serialized date/time or date object.
     * @param bool $outputTime  Used to decide if the time should be outputted as part of the timestamp.
     *
     * @return bool|float|mixed Excel date/time value or boolean False on failure
     */
    public static function PHPToExcel($dateValue = 0, $outputTime = true)
    {
        $saveTimeZone = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $retValue = False;

        if ((is_object($dateValue)) && ($dateValue instanceof self::$dateTimeObjectType))
        {
            $retValue = self::FormattedPHPToExcel($outputTime, $dateValue->format('Y'), $dateValue->format('m'), $dateValue->format('d'),
                $dateValue->format('H'), $dateValue->format('i'), $dateValue->format('s'));
        }
        elseif (is_numeric($dateValue))
        {
            $retValue = self::FormattedPHPToExcel($outputTime, date('Y',$dateValue), date('m',$dateValue), date('d',$dateValue),
                date('H',$dateValue), date('i',$dateValue), date('s',$dateValue));
        }

        date_default_timezone_set($saveTimeZone);

        return $retValue;
    }

    public static function FormattedPHPToExcel($outputTime = true, $year, $month, $day, $hours=0, $minutes=0, $seconds=0)
    {
        if (self::$ExcelBaseDate == self::CALENDAR_WINDOWS_1900)
        {
            //
            //	Fudge factor for the erroneous fact that the year 1900 is treated as a Leap Year in MS Excel
            //	This affects every date following 28th February 1900
            //
            $excel1900isLeapYear = True;
            if (($year == 1900) && ($month <= 2))
            {
                $excel1900isLeapYear = False;
            }

            $myExcelBaseDate = 2415020;
        }
        else
        {
            $myExcelBaseDate = 2416481;
            $excel1900isLeapYear = False;
        }

        //	Julian base date Adjustment
        if ($month > 2)
        {
            $month = $month - 3;
        }
        else
        {
            $month = $month + 9;
            --$year;
        }

        //	Calculate the Julian Date, then subtract the Excel base date (JD 2415020 = 31-Dec-1899 Giving Excel Date of 0)
        $century = substr($year,0,2);
        $decade = substr($year,2,2);
        $excelDate = floor((146097 * $century) / 4) + floor((1461 * $decade) / 4) + floor((153 * $month + 2) / 5) + $day + 1721119 - $myExcelBaseDate + $excel1900isLeapYear;

        if ($outputTime === true)
        {
            $excelTime = (($hours * 3600) + ($minutes * 60) + $seconds) / 86400;
        }
        else
        {
            $excelTime = 0;
        }

        return (float) $excelDate + $excelTime;
    }
}