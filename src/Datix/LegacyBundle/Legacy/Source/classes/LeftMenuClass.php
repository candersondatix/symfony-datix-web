<?php
/**
* @desc Models the left hand menu on forms.
*/
class LeftMenu
{
    //The module this menu belongs to
    protected $Module;

    //The module to actually display (e.g. sometimes ACR in LOC)
    protected $MenuModule;

    //The FormTable object representing the form that this menu serves. Used to find panels.
    protected $Table;

    //Array of panels if this is a non-standard menu and so does not have a FormTable object to reference.
    protected $Panels;

    //Key identifying the currently selected panel for use when drawing the menu.
    protected $CurrentPanel;

    //Key identifying a field that is erroring and needs focus.
    protected $ErrorField;

    //Array containing config information about the form - used for default panel names.
    protected $FormArray;

    //ButtonGroup object used when creating the floating left hand menu, which contains button icons.
    protected $Buttons;

    //Array of menu items
    protected $MenuArray;

    //ID of the current record (if applicable)
    protected $Recordid;

    //Bool storing whether a print link should be displayed
    protected $PrintLink;

    //Bool storing whether an audit link should be displayed
    protected $AuditLink;

    //Bool storing whether the current page is associated with a record listing
    protected $Listing;

    //Bool storing whether an NPSA export link should be displayed
    protected $NPSAExportLink;

    //Additional custom links
    public $ExtraLinks;

    //Is this record a linked record?
    protected $isLinkedRecord = false;


    /**
    * @desc Constructor: Assigns parameter values to object properties.
    *
    * @param array $Parameters The array of parameters to be assigned.
    */
    public function __construct($Parameters)
    {
        $this->Module = $Parameters['module'];
        $this->MenuModule = $Parameters['menumodule'];
        $this->Table = $Parameters['table'];
        $this->Panels = $Parameters['panels'];
        $this->CurrentPanel = $Parameters['current_panel'];
        $this->ErrorField = $Parameters['error_field'];
        $this->FormArray = $Parameters['formarray'];
        $this->Buttons = $Parameters['buttons'];
        $this->MenuArray = $Parameters['menu_array'];
        $this->PrintLink = (!$Parameters['no_print'] && !(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()));
        $this->PrintLinkParams = array_key_exists('print_link_params', $Parameters) ? $Parameters['print_link_params'] : false;
        $this->AuditLink = (!$Parameters['no_audit']);
        $this->Listing = $Parameters['listing'];
        $this->Action = $Parameters['action'];
        $this->show_excluded = $_SESSION['reports_administration:show_excluded'];
        $this->isLinkedRecord = $Parameters['isLinkedRecord'];

        $this->SetCurrentPanel();

        //tries to find the current recordid
        $this->Recordid = ($Parameters['recordid'] ? $Parameters['recordid'] : $_GET['recordid']);
    }

    public function SetCurrentPanel()
    {
        global $FieldSectionReference;

        if($this->ErrorField)
        {
            $this->CurrentPanel = $FieldSectionReference[$this->ErrorField];
        }

        if (!isset($this->CurrentPanel) && isset($_GET['panel']))
        {
            $this->CurrentPanel = $_GET['panel'];
        }

        $PanelArray = $this->GetPanelArray();

        if(is_array($PanelArray))
        {
            foreach ($PanelArray as $PanelName => $Sections)
            {
                if (in_array($this->CurrentPanel, $Sections) || $this->CurrentPanel == $PanelName)
                {
                    $ValidPanel = true;
                    $this->CurrentPanel = $PanelName;
                }
            }
        }

        if (!$ValidPanel)
        {
            $this->CurrentPanel = null;
        }
    }

    /**
    * @desc Gets the HTML for the left hand side menu (not the floating one).
    *
    * @return string HTML for the left hand side menu.
    */
    public function GetHTML()
    {
        global $scripturl;

        $registry = src\framework\registry\Registry::getInstance();

        $HTML = '';

        if ($this->Module)
        {
            //Use table object to generate panel links
            if ($this->Table || $this->Panels || $this->FormArray)
            {
                $PanelLinks = $this->GetPanelMenuHTML();
            }

            //Construct static module-based links.
            $OtherLinks = $this->GetOtherLinkHTML();

            $HTML .= '<div class="col" style="width:201px; z-index:2;"> ' .
            ($registry->getParm('ENHANCED_ACCESSIBILITY', 'N', false, true) ? '<a class="skip-nav" href="#main-content">Skip side navigation</a>' : '') .
            $PanelLinks .
            $OtherLinks .
            '</div>
            ';
        }

        return $HTML;
    }

    /**
    * @desc Constructs and returns the static links that appear below the panels (e.g. Print)
    *
    * @return string HTML for these links.
    */
    function GetOtherLinkHTML()
    {
        global $scripturl, $ModuleDefs;

        $LinkArray = array();

        if ($this->Table && $this->Table->FormMode != 'New' && $this->Table->FormMode != 'Link' && $this->Table->FormMode != 'Search' && $this->Table->FormMode != 'Design')
        {
            if ($this->PrintLink)
            {
            	if ($this->Module == 'AST')
            	{
            		$LinkArray[] = array(
            				array(
            						'label'   => _tk('print'),
            						'onclick' => 'SendTo(\''.getRecordURL(array('module' => $this->Module, 'recordid' => $this->Recordid, 'linkMode' => $this->isLinkedRecord)).'&print=1'.($_GET['full_audit'] ? '&full_audit=1' : '') . ( $this->PrintLinkParams ? '&'. http_build_query( $this->PrintLinkParams ) : '' ) .'&token='.CSRFGuard::getCurrentToken().'\', \'_blank\')'
            				));
            	}
            	else
            	{
                	$LinkArray[] = array(
                    	array(
                        	'label'   => _tk('print'),
                        	'onclick' => 'SendTo(\''.getRecordURL(array('module' => $this->Module, 'recordid' => $this->Recordid)).'&print=1'.($_GET['full_audit'] ? '&full_audit=1' : '') . ( $this->PrintLinkParams ? '&'. http_build_query( $this->PrintLinkParams ) : '' ) .'&token='.CSRFGuard::getCurrentToken().'\', \'_blank\')'
                    	));
            	}
            }

            if ($this->NPSAExportLink && $this->Module == 'INC' && bYN(GetParm('SHOW_NPSA_SETTINGS', 'Y')) && $_SESSION["AdminUser"] == true)
            {
                $LinkArray[] = array(array('label' => 'Export to NPSA', 'onclick' => 'exportFile(\''.$scripturl.'?action=npsaexport\', \'displayExportError(\\\''.str_replace('\'', '\\\'', _tk('npsa_export_error_title')).'\\\')\')'));
            }

            if ($this->Module == 'INC' && bYN(GetParm('SAVE_DIF1_SNAPSHOT', 'N')) && IsFullAdmin())
            {
                //need to check whether a snapshot exists for this record.
                $snapshotId = DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM inc_submission_snapshot WHERE recordid = :recordid', array('recordid' => $this->Recordid), PDO::FETCH_COLUMN);

                if ($snapshotId == 1)
                {
                    $AuditArray[] = array('label' => _tk("show_dif1_snapshot"), 'new_window' =>true, 'link' => 'action=showsnapshot&recordid='.$this->Recordid);
                }
            }

            if (bYN(GetParm($this->Module.'_SHOW_AUDIT', 'Y')) && $this->AuditLink)
            {
                if ($this->Module == 'INC')
                {
                    $AuditArray[] = array('label' => _tk("show_dif1_values"), 'external_link' => true, 'link' => getRecordURL(array('module' => $this->Module, 'recordid' => $this->Recordid)).'&show_dif1_values=1');
                }

                if ($this->Module == 'AST' && $_GET['action'] == 'linkequipment')
                {
                    $AuditArray[] = array('label' => _tk("audit_trail"), 'external_link' => true, 'link' => 'app.php?action=linkequipment&module='.$_GET['module'].'&link_recordid='.$_GET['link_recordid'].'&main_recordid='.$_GET['main_recordid'].'&ast_recordid='.$_GET['ast_recordid'].'&full_audit=1');
                }
                else
                {
                    $AuditArray[] = array('label' => _tk("audit_trail"), 'external_link' => true, 'link' => getRecordURL(array('module' => $this->Module, 'recordid' => $this->Recordid)).'&full_audit=1'.(isset($_REQUEST['frommainrecord']) && $_REQUEST['frommainrecord'] != '' ? '&frommainrecord='.$_REQUEST['frommainrecord'] : '').(isset($_REQUEST['main_recordid']) && $_REQUEST['main_recordid'] != '' ? '&main_recordid='.$_REQUEST['main_recordid'] : '').(isset($_REQUEST['module']) && $_REQUEST['module'] != '' ? '&module='.$_REQUEST['module'] : '').(isset($_REQUEST['action']) && $_REQUEST['action'] == 'linkequipment' ? '&LinkMode='.$_REQUEST['action'] : ''));
                }
                $LinkArray[] = $AuditArray;
            }

            if ($this->Module == 'RAM')
            {
                $LinkArray[] = array(array('label' => _tk('risk_grade_tracker'), 'onclick' => 'riskGradeTracker('.$this->Recordid.')'));
            }

            if ($this->Module == 'CQO')
            {
                $LinkArray[] = array(array('label' => _tk("cqc_view_outcome_summary_report"), 'link' => 'action=cqcoutcomesummaryreport&packaged_report=report8&outcome_id='.$this->Recordid));
            }

        }

        if ($this->MenuArray)
        {
            $menuArray = $this->MenuArray;
        }
        else
        {
            $menuArray = GetMenuItems(array('module' => $this->MenuModule ? $this->MenuModule : $this->Module));
        }

        if(!empty($menuArray))
        {
            $LinkArray[] = $menuArray;
        }

        //add batch delete link
        if($this->Listing && !in_array($this->Module, array('TOD', 'CQO', 'CQP', 'CQS','LOC')))
        {
            if(!($this->Module == 'CON' && $_SESSION["CON"]["DUPLICATE_SEARCH"]))
            {
                if (bYN(GetParm('ENABLE_BATCH_DELETE', 'N')) && !in_array($this->Module, array('ATQ', 'AQU')))
                {
                    $LinkArray[] = array(array('label' => _tk('batch_delete'), 'link' => 'service=batchdelete&event=collectparameters&module='.$this->Module));
                }
                if (bYN(GetParm('ENABLE_BATCH_UPDATE', 'N')))
                {
                    $LinkArray[] = array(array('label' => _tk('batch_update'), 'link' => 'service=batchupdate&event=collectparameters&module='.$this->Module));
                }
            }
            if ((IsFullAdmin() || bYN(GetParm('CON_ALLOW_MERGE_DUPLICATES', 'N'))) && $_SESSION["CON"]["DUPLICATE_SEARCH"] && $this->Module == 'CON')
            {
                $LinkArray[] = array(array('label' => _tk('batch_merge'), 'link' => 'service=batchmerge&event=collectparameters&module='.$this->Module));
            }
        }

        if(!empty($this->ExtraLinks))
        {
            $LinkArray[] = $this->ExtraLinks;
        }

        if ($this->Module == 'ADM')
        {
            if ($this->Action == 'designabasereport' || 'reportsadminaction' == $this->Action)
            {
                parse_str($_SERVER['QUERY_STRING'], $query_output);

                $query_output['show_excluded'] = $_SESSION['reports_administration:show_excluded'] == '1' ? '0' : '1' ;
                unset($query_output['token']);

                $LinkArray[] = array(array('label' => ($_SESSION['reports_administration:show_excluded'] == '1' ? _tk('hide_excluded_forms_fields') : _tk('show_excluded_forms_fields')), 'link' => http_build_query($query_output)));
            }

            $LinkArray[] = array(array('label' => _tk('back_to_admin'), 'link' => 'action=home&module=ADM', 'bold' => true));
        }

        if ($this->Module == 'TOD')
        {
            /*if(!\src\framework\registry\Registry::getInstance()->getSessionCache()->exists('help.online-help-available'))
            {
                //here be the superhack that checks if the remote help can be accessed
                \src\framework\registry\Registry::getInstance()->getSessionCache()->set('help.online-help-available', (explode(' ', get_headers('http://help.datix.co.uk', 1)[0])[1] == 200));
            }
            $canHelpRemote = \src\framework\registry\Registry::getInstance()->getSessionCache()->get('help.online-help-available');
            */

            $LinkArray[] = array(array('label' => _tk('all_todo'), 'link' => 'action=list&module=TOD', 'bold' => true));
            $LinkArray[] = array(array('label' => _tk('overdue_todo'), 'link' => 'action=list&module=TOD&listtype=overdue', 'bold' => true));
            $LinkArray[] = array(array('label' => _tk('due_today_todo'), 'link' => 'action=list&module=TOD&listtype=duetoday', 'bold' => true));
            $LinkArray[] = array(array('label' => _tk('due_this_week_todo'), 'link' => 'action=list&module=TOD&listtype=duethisweek', 'bold' => true));
            $LinkArray[] = array(array('label' => _tk('due_this_month_todo'), 'link' => 'action=list&module=TOD&listtype=duethismonth', 'bold' => true));
            $LinkArray[] = array( array(
            	'label' => _tk('help_alt'),
                'link' => 'app.php?action=helpfinder&module=TOD',
            	'new_window' => true,
            	'external_link' => true
            ));
        }

        if (is_array($LinkArray))
        {
            foreach ($LinkArray as $LinkList)
            {
                if (is_array($LinkList))
                {
                    $ItemHTML = '';

                    foreach ($LinkList as $id => $menuitem)
                    {
                        if($menuitem['condition'] !== false)
                        {
                            if (isset($menuitem['menu_name']))
                            {
                                foreach ($menuitem['items'] as $Item)
                                {
                                    if (\UnicodeString::strpos($Item['link'], $this->Module) || (isset($Item['display_always']) && $Item['display_always'] == true))
                                    {
                                        $ItemHTML .= $this->buildMenuItems($Item, $id);
                                    }
                                }
                            }
                            else
                            {
                                $ItemHTML .= $this->buildMenuItems($menuitem, $id);
                            }
                        }
                    }

                    if($ItemHTML != '')
                    {

                        $HTML.= '
                            <div class="option-nav">
                                <ul>
                        ';

                        $HTML .= $ItemHTML;

                        $HTML.= '
                                </ul>
                            </div>
                        ';
                    }
                }
            }
        }

        return $HTML;
    }

    /**
     * @desc Builds a menu item for left hand side menu.
     *
     * @param $Item Menu item with all parameters
     * @param $ItemId Menu item id
     * @return string HTML for this menu item
     */
    protected function buildMenuItems($Item, $ItemId)
    {
        global $scripturl;

        $html = '';

        if ($Item['condition'] !== false)
        {
            if ($Item['link'])
            {
                $html.= '<li id="'.$ItemId.'"><a href="javascript:if(CheckChange()){SendTo(\''.($Item['external_link'] ? '' : $scripturl.'?').$Item['link'].'\', \'' . ($Item['new_window'] ? '_blank' : '') . '\');}">'.
                    ($Item['icon_blue'] ? '<img src="'.$Item['icon_blue'].'" alt="'.$Item['alt'].'" /> ' : '').
                    ($Item['bold'] ? '<b>'. (!empty($Item['lhsm_label']) ? $Item['lhsm_label'] : $Item['label']) .'</b>' : (!empty($Item['lhsm_label']) ? $Item['lhsm_label'] : $Item['label']) ).
                    '</a></li>
                ';
            }
            else if ($Item['onclick'])
            {
                $html.= '<li id="'.$ItemId.'"><a href="javascript:if(CheckChange()){'.$Item['onclick'].';}">'.
                    ($Item['icon_blue'] ? '<img src="'.$Item['icon_blue'].'" alt="'.$Item['alt'].'" /> ' : '').
                    ($Item['bold'] ? '<b>'. (!empty($Item['lhsm_label']) ? $Item['lhsm_label'] : $Item['label']) .'</b>' : (!empty($Item['lhsm_label']) ? $Item['lhsm_label'] : $Item['label']) ).
                    '</a></li>
                ';
            }
        }

        return $html;
    }

    /**
    * @desc Finds the array of panels to use for the panel displays on both left hand menus.
    *
    * @return array An array of panels.
    */
    protected function GetPanelArray()
    {
        $PanelArray = array();

        if ($this->Table)
        {
            $PanelArray = $this->Table->GetPanelArray();
            $FormArray = $this->Table->GetFormArray();
        }
        else if ($this->FormArray)
        {
            foreach ($this->FormArray as $key => $details)
            {
                if ($details['NewPanel'])
                {
                    $PanelArray[$key] = array('Label' => $details['Title']);
                }
            }
        }
        else if ($this->Panels)
        {
            $PanelArray = $this->Panels;
        }

        return $PanelArray;
    }

    /**
    * @desc Constructs the HTML for the "Current record" section of the side menu, which contains links to the form panels.
    *
    * @return string The HTML of the menu options.
    */
    public function GetPanelMenuHTML()
    {
        global $Show_all_section, $UserLabels;

        if ($this->Table)
        {
            $FormArray = $this->Table->GetFormArray();
        }

        $PanelArray = $this->GetPanelArray();

        $Menu .= '<div class="form-nav">
        ';

        $FirstPanelDone = false;


        if(is_array($PanelArray) && (!$this->Table || $this->Table->getFormLevel() != 1 || $this->Module == 'SAB'))
        {
            foreach ($PanelArray as $PanelName => $PanelDetails)
            {
                $Classes = array('list-item');

                if (!$FirstPanelDone)
                {
                    $Classes[] = 'top';
                    $FirstPanelDone = true;

                    if (!isset($this->CurrentPanel))
                    {
                        $Classes[] = 'selected';
                    }
                }

                if (isset($this->CurrentPanel) && $this->CurrentPanel == $PanelName)
                {
                    $Classes[] = 'selected';
                }

                $Menu .= '
                    <div id="menu-' . $PanelName . '" class="'.implode(' ', $Classes).'">';

                if (bYN($Show_all_section))
                {
                    $Menu .= '
                        <a href="#panel-' . $PanelName . '" onClick="jQuery(\'.list-item\').removeClass(\'selected\');jQuery(\'#menu-'.$PanelName.'\').addClass(\'selected\');">';
                }
                else
                {
                    $Menu .= '
                        <a href="JavaScript:void(showFormPanel(\'panel-'. $PanelName . '\'))" onClick="jQuery(\'.list-item\').removeClass(\'selected\');jQuery(\'#menu-'.$PanelName.'\').addClass(\'selected\');">';
                }

                if (is_array($UserLabels) && array_key_exists($PanelName, $UserLabels))
                {
                    $Label = $UserLabels[$PanelName];
                }
                elseif ($FormArray[$PanelName]["MenuTitle"])
                {
                    $Label = $FormArray[$PanelName]["MenuTitle"];
                }
                else if ($FormArray[$PanelName]["Title"])
                {
                    $Label = $FormArray[$PanelName]["Title"];
                }
                else
                {
                    $Label = $PanelDetails['Label'];
                }

                $Menu .= _t($Label).'</a>
                    </div>';
            }
        }

        $Menu .= '</div>
        ';

        return $Menu;
    }

    /**
    * @desc Constructs and returns the HTML for the floating left hand menu.
    *
    * @return string The HTML for this menu.
    */
    public function GetFloatingMenuHTML()
    {
        global $Show_all_section, $UserLabels, $JSFunctions;

        if ($this->Table)
        {
            $FormArray = $this->Table->GetFormArray();
        }

        if ($_GET['action'] != 'newdif1')
        {
        	$PanelArray = $this->GetPanelArray();
        }
        $FloatingMenu = '
        <div id="site-bottom-bar" class="fixed-position" '.($this->Buttons->NavigationButtonsExist() ? 'style="height: 60px" ': '').'>
            <div id="site-bottom-bar-frame">
                <div id="site-bottom-bar-content">';

        if(!empty($PanelArray))
        {
            $FloatingMenu .='<a id="menu-root" href="##" title="'._tk('btn_menu').'"><img src="images/icons/icon_footer_menu.png" alt="'._tk('btn_menu').'" class="side-menu-icon first"/></a>';
        }

        if ($this->Buttons)
        {
            $FloatingMenu .= $this->Buttons->getSideMenuHTML();
        }

        $FloatingMenu .= '
                    <div id="menu-2">
                        <ul>
        ';

        if(is_array($PanelArray))
        {
            foreach ($PanelArray as $PanelName => $PanelDetails)
            {
                if (bYN($Show_all_section))
                {
                    $FloatingMenu .= '
                    <li>
                        <a href="#panel-' . $PanelName . '" onClick="jQuery(\'.list-item\').removeClass(\'selected\');jQuery(\'#menu-'.$PanelName.'\').addClass(\'selected\');">';
                }
                else
                {
                    $FloatingMenu .= '
                    <li>
                        <a href="JavaScript:void(showFormPanel(\'panel-'. $PanelName . '\'))" onClick="jQuery(\'.list-item\').removeClass(\'selected\');jQuery(\'#menu-'.$PanelName.'\').addClass(\'selected\');jQuery(\'html,body\').scrollTop(0);">';
                }

                if (is_array($UserLabels) && array_key_exists($PanelName, $UserLabels))
                {
                    $Label = $UserLabels[$PanelName];
                }
                elseif ($FormArray[$PanelName]["MenuTitle"])
                {
                    $Label = $FormArray[$PanelName]["MenuTitle"];
                }
                else if ($FormArray[$PanelName]["Title"])
                {
                    $Label = $FormArray[$PanelName]["Title"];
                }
                else
                {
                    $Label = $PanelDetails['Label'];
                }

                $FloatingMenu .= $Label.'</a></li>';
            }
        }

        $FloatingMenu .= '
                           </ul>
                    </div>

                </div>

            </div>
        </div>';

        if ((\UnicodeString::strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false))
        {
            $JSFunctions[] = '
                function positionFloatingMenu()
                {
                    jQuery("#site-bottom-bar").position({
                        of: window,
                        my: "left bottom",
                        at: "left bottom",
                        offset: "20 0",
                        collision: "none"
                    });
                }
                jQuery(document).ready(function(){jQuery("#site-bottom-bar").show();positionFloatingMenu();});
                jQuery(window).scroll(function(){positionFloatingMenu();});';
        }

        return $FloatingMenu;
    }
}
