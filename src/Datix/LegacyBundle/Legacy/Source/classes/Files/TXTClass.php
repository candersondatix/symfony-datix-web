<?php
/**
* TXT file class.
*/
class Files_TXT extends Files_File
{
    /**
    * Constructor: Allows setting of the file extension as well as filename.
    * 
    * @param string $name
    * @param string $extension
    */
    public function __construct($name, $extension = 'txt')
    {
        $this->extension = $extension;
        parent::__construct($name);    
    }
    
    /**
    * Export the file.
    * 
    * @codeCoverageIgnore
    */
    public function export()
    {
        parent::export();
        header('Content-Type: application/txt');
        echo $this->contents;
        obExit();    
    }
}