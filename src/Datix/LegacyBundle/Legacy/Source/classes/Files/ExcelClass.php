<?php
/**
* Base class for Excel Files - uses a PHPExcel object to puill out the file data.
*/
class Files_Excel extends Files_File
{

    /**
    * @desc The PHPExcel object representing the excel file
    * @var object
    */
    protected $PHPExcelObj;

    /**
    * The line number of the current line (when reading in from file).
    * @var int
    */
    protected $lineNumber;

    /**
    * Number of populated columns in the file.
    * @var int
    */
    protected $numberOfColumns;


    public function __construct($name)
    {
        $this->extension = 'xls';
        $this->lineNumber = 1;
        parent::__construct($name);
    }

    /**
    * Checks if the file exists and is readble.
    *
    * @return boolean
    */
    public function isReadable()
    {
        return true;
    }

    public function loadFile()
    {
        if(!file_exists($this->name))
        {
            throw new FileNotFoundException(_tk('cannot_read_file'));
        }

        include_once('thirdpartylibs/PHPExcel/PHPExcel.php');
        include_once('thirdpartylibs/PHPExcel/PHPExcel/IOFactory.php');
        
        /**  Identify the type of $inputFileName  **/
        $inputFileType = PHPExcel_IOFactory::identify($this->name);
        /**  Create a new Reader of the type that has been identified  **/
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $this->PHPExcelObj = $objReader->load($this->name);

        //need to find first worksheet with data:
        for($i=0; $i<100; $i++)
        {
            $this->PHPExcelObj->setActiveSheetIndex($i);
            if($this->PHPExcelObj->getActiveSheet()->getHighestColumn() != 'A' || $this->PHPExcelObj->getActiveSheet()->getHighestRow() != 1)
            {
                //this worksheet has data, so we can import from it.
                break;
            }
        }
    }

    /**
    * Retrieves the next line in the file.  Generates the PHPExcel Object if necessary.
    *
    * @return array $this->line
    */
    public function getNextLine()
    {
        if(!$this->PHPExcelObj)
        {
            $this->loadFile();
        }

        $BlankRow = true;

        //Run through each column of the current row, adding the data to an array indexed by the column letter.
        for($col = 0; $col< PHPExcel_Cell::columnIndexFromString($this->PHPExcelObj->getActiveSheet()->getHighestColumn()); $col++)
        {
            $Value = $this->PHPExcelObj->getActiveSheet()->getCellByColumnAndRow($col, $this->lineNumber)->getValue();

            if($Value != null)
            {
                $BlankRow = false;
            }

            $CurrentLineData[PHPExcel_Cell::stringFromColumnIndex($col)] = $Value;
        }

        $this->lineNumber++;

        //If we have reached a blank row, we assume we are at the end of the file, and so need to return false.
        if($BlankRow)
        {
            return false;
        }

        return $CurrentLineData;
    }

}