<?php
/**
* XML file class.
*/
class Files_XML extends Files_File
{
    /**
    * Constructor: defines extension and filename.
    * 
    * @param  string  $name
    */
    public function __construct($name)
    {
        $this->extension = 'xml';
        parent::__construct($name);       
    }
        
    /**
    * Export the file.
    * 
    * @codeCoverageIgnore
    */
    public function export()
    {
        parent::export();
        header('Content-Type: text/xml');
        header('Content-Encoding: none');
        echo $this->contents;
        obExit();
    }
    
    /**
    * Makes a string safe for XML by converting special characters to XML entities.
    * 
    * @param  string $string  The input string
    * 
    * @return string $strout The escaped output string
    */
    public function XMLEntities($string)
    {
        $strout = null;

        for ($i = 0, $len = \UnicodeString::strlen($string); $i < $len; $i++)
        {
            $char = \UnicodeString::substr ($string, $i, 1);
            $ord = \UnicodeString::ord ($char);

            if (($ord > 0 && $ord < 32) || ($ord >= 127))
            {
                $strout .= "&amp;#{$ord};";
            }
            else
            {
                switch ($char)
                {
                    case '<':
                        $strout .= '&lt;';
                        break;
                    case '>':
                        $strout .= '&gt;';
                        break;
                    case '&':
                        $strout .= '&amp;';
                        break;
                    case '"':
                        $strout .= '&quot;';
                        break;
                    default:
                        $strout .= $char;
                }
            }
        }

        return $strout;
    }
    
    /**
    * Decodes line breaks in the XML contents.
    */
    public function decodeLineBreaks()
    {
        $this->contents = str_replace(array('&amp;#13;', '&amp;#10;'), array("\r", "\n"), $this->contents);    
    }
}
