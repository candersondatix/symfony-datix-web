<?php
/**
* CSV file class.
*/
class Files_CSV extends Files_File
{
    /**
    * The delimiter for the csv file.
    * 
    * @var string
    */
    protected $delimiter;
    
    /**
    * Maximum line length for the input file - improves efficiency while parsing when defined.
    * 
    * @var int
    */
    protected $maxLineLength;
    
    /**
    * The data contained in the current line (when reading in from file).
    * 
    * @var array
    */
    protected $line;
    
    /**
    * The line number of the current line (when reading in from file).
    * 
    * @var int
    */
    protected $lineNumber;
    
    /**
    * The contents of the CSV file (as a 2D array).
    * 
    * @var array
    */
    protected $data;
    
    /**
    * Constructor: defines extension, filename, CSV delimiter and max line length.
    * 
    * @param  string  $name
    * @param  string  $delimiter
    * @param  int     $length
    */
    public function __construct($name = '', $delimiter = ',', $length = 4096)
    {
        $this->extension = 'csv';
        $this->delimiter = $delimiter;       
        $this->maxLineLength = $length;
        $this->lineNumber = 0;
        parent::__construct($name);       
    }
    
    /**
    * Rewinds the file pointer and sets the current line number to 0.
    */
    public function resetPointer()
    {
        if (is_resource($this->filePointer))
        {
            rewind($this->filePointer);   
        }
        $this->lineNumber = 0;    
    }
    
    /**
    * Retrieves the next line in the file.  Opens the file pointer if necessary.
    * 
    * @return array $this->line
    */
    public function getNextLine()
    {
        if (!is_resource($this->filePointer))
        {
            $this->openFile();        
        }
        
        $this->line = fgetcsv($this->filePointer, $this->maxLineLength, $this->delimiter);
        $this->lineNumber++;
        
        return $this->getCurrentLine();       
    }
    
    /**
     * Export the file. Ths is use either a bespoke csv encoding system
     * (default - appropriate for pre v5 php, note it doesn't use enclosures),
     * or use fputcsv to encode the data
     *
     * @param boolean $useFputcsv If true, use fputcsv to encode, otherwise use bespoke function
     *
     * @codeCoverageIgnore
     */
    public function export($useFputcsv = false)
    {
        parent::export();
        header('Content-type: application/octet-stream');

        if ($useFputcsv)
        {
            //Have the library output to screen via fputcsv without changing too much else.
            if (is_null($this->filePointer))
            {
                $this->setName('php://output');
            }
            $this->openFile();
            foreach ($this->data as $row)
            {
                fputcsv($this->filePointer, $row, $this->delimiter);
            }
        } else {
            $this->setContents($this->contents);
            echo $this->contents;
        }
        obExit();
    }
    
    /**
    * Getter for the data contained in the current line.
    * 
    * @return array $this->line
    * 
    * @codeCoverageIgnore
    */
    public function getCurrentLine()
    {
        return $this->line;
    }
    
    /**
    * Getter for current line number.
    * 
    * @return int $this->lineNumber
    * 
    * @codeCoverageIgnore
    */
    public function getLineNumber()
    {
        return $this->lineNumber;    
    }
    
    /**
    * Setter for the data.
    * 
    * @param array $data
    * 
    * @codeCoverageIgnore   
    */
    public function setData(array $data)
    {
        $this->data = $data;    
    }

    /**
    * Setter for the contents.  Converts the info stored in $data if no string passed.
    *
    * @param string $contents
    */
    public function setContents($contents = null)
    {
        if ($contents == null)
        {
            foreach ($this->data as $data)
            {
                $this->contents .= implode($this->delimiter, $data)."\r\n";
            }
        }
        else
        {
            $this->contents = $contents;
        }
    }
}
