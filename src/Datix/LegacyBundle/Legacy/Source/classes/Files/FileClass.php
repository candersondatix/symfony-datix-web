<?php
/**
* Base class for Files.
*/
abstract class Files_File
{
    /**
    * The file name.
    * 
    * @var string
    */
    protected $name;
    
    /**
    * The pointer to the file.
    * 
    * @var resource
    */
    protected $filePointer;
    
    /**
    * The file contents
    * 
    * @var string
    */
    protected $contents;
    
    /**
    * The file extension
    * 
    * @var string
    */
    protected $extension;
    
    /**
    * Constructor.
    * 
    * @param string $name
    */
    public function __construct($name)
    {
        $this->name = $name;   
    }
    
    /**
    * Closes the file pointer if open.
    */
    public function __destruct()
    {
        if (is_resource($this->filePointer))
        {
            fclose($this->filePointer);        
        }    
    }
    
    /**
    * Opens the file resource.
    * 
    * @throws FileNotFoundException
    */
    public function openFile()
    {
        $this->filePointer = @fopen($this->name, 'r');
        if ($this->filePointer === false)
        {
            throw new FileNotFoundException(_tk('cannot_read_file'));
        }       
    }
    
    /**
    * Checks if the file exists and is readble.
    * 
    * @return boolean 
    */
    public function isReadable()
    {
        if (($stream = @fopen($this->name, 'r')) === false)
        {
            return false;    
        }
        else
        {
            fclose($stream);
            return true;
        }
    }
    
    /**
    * Sets a generic header for the export and can also set a cookie used to track the successful export of a file.
    */
    public function export()
    {
        $name = $this->removeDirFromName($this->addExtensionToName($this->name));
        
        header('Content-Disposition: attachment; filename="'.$name.'"');
        if (isset($_POST['export_token_value']))
        {
            setcookie('fileExportToken', Sanitize::SanitizeRaw($_POST['export_token_value']).'|success', 0, '', '', ($_SERVER["HTTPS"] == "on" ? true : false));   
        }   
    }
    
    /**
    * Sets a cookie used to track a failed export attempt (e.g. because of validation rules etc).
    */
    public function failExport()
    {
        if (isset($_POST['export_token_value']))
        {
            setcookie('fileExportToken', Sanitize::SanitizeRaw($_POST['export_token_value']).'|fail', 0, '', '', ($_SERVER["HTTPS"] == "on" ? true : false));
        }
        obExit();
    }

    /**
     * Setter for the name of the file being managed.
     *
     * @param string $name
     *
     * @codeCoverageIgnore
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
    * Setter for the contents.
    * 
    * @param string $contents
    * 
    * @codeCoverageIgnore   
    */
    public function setContents($contents)
    {
        $this->contents = $contents; 
    }
    
    /**
    * Setter for the contents directly from an existing file.
    * 
    * @param string $filename
    * 
    * @throws FileNotFoundException
    */
    public function setContentsFromFile($filename)
    {
        $this->contents = @file_get_contents($filename);
        
        if ($this->contents === false)
        {
            throw new FileNotFoundException(_tk('cannot_read_file'));
        }
    }
    
    /**
    * Returns the file download error set in the session.
    * 
    * @return string
    */
    public static function getExportError()
    {
        return $_SESSION['fileExportError'];
    }
    
    /**
    * Saves the file export error in the session (for retrieveal later e.g. via ajax).
    * 
    * @param string $error
    */
    public static function setExportError($error)
    {
        $_SESSION['fileExportError'] = $error;
    }
    
    /**
    * Delete the file from the filesystem.
    * 
    * @throws FileNotFoundException
    */
    public function delete()
    {
        $result = @unlink($this->name);
        if ($result === false)
        {
            throw new FileNotFoundException(_tk('cannot_read_file'));
        }   
    }
    
    /**
    * Adds the extension to filename if not already defined.
    * 
    * @param  string $name
    * 
    * @return string $name
    */
    public function addExtensionToName($name)
    {
        if (isset($this->extension) && \UnicodeString::substr($this->name, -1 - \UnicodeString::strlen($this->extension)) != '.'.$this->extension)
        {
            $name .= '.'.$this->extension;               
        }
        return $name;
    }
    
    /**
    * Removes the directory structure prefix from the file name.
    * 
    * @param  string $name
    * 
    * @return string $name
    */
    public function removeDirFromName($name)
    {
        $name = explode('/', $name);
        $name = $name[count($name)-1];
        return $name;
    }    
}