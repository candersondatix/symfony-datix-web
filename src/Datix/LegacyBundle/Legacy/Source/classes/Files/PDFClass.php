<?php
/**
* PDF file class.
*/
class Files_PDF extends Files_File
{
    /**
    * Constructor: defines extension and filename.
    * 
    * @param  string  $name
    */
    public function __construct($name)
    {
        $this->extension = 'pdf';
        parent::__construct($name);       
    }
    
    /**
    * Export the file.
    * 
    * @codeCoverageIgnore
    */
    public function export()
    {
        parent::export();
        header('Content-Type: application/pdf');
        
        if (\UnicodeString::strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false)
        {
            // fix "file not found" issue when opening file directly from IE6
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");    
        }
        
        echo $this->contents;
        obExit();    
    }
}