<?php
/**
* CSV load file class.
*/
class Files_CSVLoadFile extends Files_CSV
{
    /**
    * Constructor: sets file name and delimiter to |.
    * 
    * @param string $name
    */
    public function __construct($name)
    {
        parent::__construct($name, '|');    
    }
    
    /**
    * Gets the header info from the load file, defined on the first line.
    *
    * @returns array $header
    * 
    * @throws  FileHeaderNotFoundException
    */
    public function getHeader()
    {
        if ($this->lineNumber != 0)
        {
            $this->resetPointer();       
        }
        
        $header = $this->getNextLine();
        if ($header[0]{0} != '!')
        {
            // header line must begin with !
            throw new FileHeaderNotFoundException(_tk('invalid_load_file'));
        }
        else
        {
            // remove ! from header
            $header[0] = \UnicodeString::substr($header[0], 1);
        }
        
        return $header;     
    }
    
    /**
    * Gets the field names from the load file, defined on the second line.
    * 
    * @return $this->line
    */
    public function getFieldNames()
    {
        if ($this->lineNumber != 1)
        {
            $this->resetPointer();
            $this->getNextLine();        
        }
        
        return $this->getNextLine();   
    }    
}
