<?php
/**
* Utility functions for retrieving data from nested set hierarchies.
*/
class Location_NestedSetMapper extends System_NestedSetMapper
{

    /**
     * Inserts a record into the locations table
     *
     * @param string $name The name of the new location
     * @param int $parentNode The id of the parent node of the location. Defaults to null
     * @return int|false
     */
    public function insert($name, $parentNode = null)
    {
        $data = array_merge(array(
            'desc' => $name,
            'parent_id' => $parentNode
        ), $this->_getLeftRight($parentNode));

        $this->query->setSQL("INSERT INTO ".$this->table." (".$this->descField.", lft, rght, parent_id) VALUES (:desc, :lft, :rght, :parent_id)");
        $result = $this->query->prepareAndExecute($data);

        if ($result)
        {
            return $this->query->PDO->lastInsertId();
        }

        return false;
    }
}