<?php

class ATI_Module
{
    public function LinkedQuestionSection($id, $listingid = null)
    {
        global $DatixView, $scripturl;

        $Design = Listings_ModuleListingDesignFactory::getListingDesign(array('module' => 'ATQ', 'parent_module' => 'ATI', 'link_type' => 'questions'));
        $Design->LoadColumnsFromDB(null, true);

        $sql = '
            SELECT
                asm_question_templates.recordid,
                ' . implode(', ', $Design->getColumns(true)) . '
            FROM
                asm_question_templates,
                asm_templates
            WHERE
                asm_templates.asm_module_template_id = asm_question_templates.asm_module_template_id
                AND
                asm_templates.recordid = :recordid
        ';

        $Questions = DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $id));

        $RecordList = new RecordLists_ModuleRecordList();
        $RecordList->Module = 'ATQ';
        $RecordList->Paging = false;
        $RecordList->AddRecordData($Questions);

        $DatixView->ListingDisplay = new Listings_ListingDisplay($RecordList, $Design, '', true);
        $DatixView->ListingDisplay->RecordURLSuffix = '&from_parent_record=1';
        $DatixView->id = $id;
        $DatixView->scripturl = $scripturl;

        echo $DatixView->render('classes/ASM/ModuleClass/LinkedQuestionSection.php');
    }
}