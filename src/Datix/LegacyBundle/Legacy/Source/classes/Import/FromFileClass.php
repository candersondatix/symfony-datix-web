<?php
/**
* Base class for importing data from file.
*/
abstract class Import_FromFile extends Import_Import
{
    /**
    * The file object.
    * 
    * @var Files_File
    */
    protected $file;
    
    /**
    * Constructor: Checks the file is readable and assigns file property.
    * 
    * @param  string $filename
    * 
    * @throws FileNotFoundException
    */
    public function __construct(Files_File $file, DatixDBQuery $query)
    {
        if (!$file->isReadable())
        {
            throw new FileNotFoundException(_tk('cannot_read_file'));
        }
        $this->file = $file;
        parent::__construct($query);
    }  
}
