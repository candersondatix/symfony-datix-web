<?php
/**
* Base import class
*/
abstract class Import_Import
{
    /**
    * The table we're importing data into.
    *
    * @var string
    */
    protected $table;

    /**
    * Defines how to handle existing data (overwrite/update/append).
    *
    * @var string
    */
    protected $existingRecordsAction;

    /**
    * A list of fields we're inserting data into.
    *
    * @var array
    */
    protected $fieldNames;

    /**
    * Contains all of the data (correctly formatted) that we're imported into the DB.
    *
    * @var array
    */
    protected $importArray;

    /**
    * The object we're using to carry out the data transaction.
    *
    * @var DatixDBQuery
    */
    protected $query;

    /**
    * Array of fields used as keys, used to form the where clause when updating records.
    *
    * @var array
    */
    protected $Keys = array();

    /**
    * Defines the field to use when removing specific records prior to import.
    *
    * @var string
    */
    protected $deleteField;

    /**
    * Defines the field value to use when removing specific records prior to import.
    *
    * @var string
    */
    protected $deleteCode;

    /**
    * A list of numeric table fields.  Used to prevent SQL errors when binding empty strings.
    *
    * @var array
    */
    protected $numericFields;

    /**
    * Variable tracking the number of records updated during the import
    * @var int
    */
    protected $updatedRecords;

    /**
    * Variable tracking the number of records added during the import
    * @var int
    */
    protected $addedRecords;

    /**
    * Constructor.
    *
    * @param  DatixDBQuery  $query
    */
    public function __construct(DatixDBQuery $query)
    {
        $this->query         = $query;
        $this->importArray   = array();
        $this->numericFields = array();
    }

    /**
    * Closes the DB connection.
    */
    public function __destruct()
    {
        unset($this->query);
    }

    /**
    * Resets the table by removing all of the current data.
    */
    protected function resetTable()
    {
        $this->query->setSQL('DELETE FROM '.$this->table);
        $this->query->prepareAndExecute();
    }

    /**
    * @desc If we're inserting into a table without an index, we neeed to generate the recordid manually.
    */
    protected function checkIndexesForInsert(&$row)
    {
        $Table = new Table($this->table);
        $SQLIndex = $Table->getSQLIndex();

        if(!$SQLIndex && $Table->hasRecordid())
        {
            if(!in_array('recordid', $this->fieldNames))
            {
                $this->fieldNames[] = 'recordid';
            }
            if(!isset($row['recordid']) || $row['recordid'] == '' || $row['recordid'] == null)
            {
                $row['recordid'] = GetNextRecordID($this->table, false);
            }
        }
    }

    /**
    * Writes the data in importArray to the DB.
    */
    protected function insertData()
    {
        $this->validateData();
        $this->addedRecords = 0;
        $this->updatedRecords = 0;

        try
        {
            $this->query->beginTransaction();
            $this->handleExistingRecords();

            switch ($this->existingRecordsAction)
            {
                case 'UPDATE':
                    foreach ($this->importArray as $row)
                    {
                        if ($this->recordExists($row))
                        {
                            // update the row if it exists
                            $this->query->setSQL($this->createUpdateSQL($row));
                            $this->updatedRecords++;
                        }
                        else
                        {
                            // otherwise insert a new row
                            $this->checkRequiredFieldsForInsert($row);
                            $this->checkIndexesforInsert($row);
                            $this->query->setSQL($this->createInsertSQL($row));
                            $this->addedRecords++;
                        }
                        $this->query->prepareAndExecute($row);
                    }
                    break;

                case 'APPEND':
                    foreach ($this->importArray as $row)
                    {
                        if (!$this->recordExists($row))
                        {
                            $this->checkRequiredFieldsForInsert($row);
                            $this->query->setSQL($this->createInsertSQL($row));
                            // only insert row if it doesn't already exist
                            $this->checkIndexesforInsert($row);
                            $this->query->prepareAndExecute($row);
                            $this->addedRecords++;
                        }
                    }
                    break;

                default:
                    foreach ($this->importArray as $row)
                    {
                        $this->checkRequiredFieldsForInsert($row);
                        $this->query->setSQL($this->createInsertSQL($row));
                        // insert a new row
                        $this->checkIndexesforInsert($row);
                        $this->query->prepareAndExecute($row);
                        $this->addedRecords++;
                    }
                    break;
            }

            $this->query->commit();
        }
        catch (DatixDBQueryException $e)
        {
            $this->query->rollback();
            throw $e;
        }
    }

    /**
    * Handles existing records prior to import, if required.
    */
    protected function handleExistingRecords()
    {
        if ($this->existingRecordsAction == 'OVERWRITE')
        {
            $this->resetTable();
        }
        else if ($this->deleteField != null && $this->deleteCode != null)
        {
            $this->query->setSQL('DELETE FROM '.$this->table.' WHERE '.$this->deleteField.' = :deleteCode');
            $this->query->prepareAndExecute(array('deleteCode' => $this->deleteCode));
        }
    }

    /**
    * Creates the SQL for inserting a new record.
    *
    * @return string
    */
    protected function createInsertSQL($row)
    {
        //Don't want to include any fieldname entries that don't have a value set.
        $FieldsToInclude = array();
        foreach($this->fieldNames as $Field)
        {
            if(isset($row[$Field]))
            {
                $FieldsToInclude[] = $Field;
            }
        }

        return 'INSERT INTO '.$this->table.' ('.implode(',', $FieldsToInclude).') VALUES ('.implode(',', array_map(array($this, 'addColon'), $FieldsToInclude)).')';
    }

    /**
    * Creates the SQL for updating an existing record.
    *
    * NB - some drivers don't allow you to bind the same variable twice,
    * which is why we have to add the suffix '2' to the primary/secondary keys.
    *
    * @param  array  $row  The row of data we're using to update.
    *                      Passed by reference so we can add primary/secondary key data to be bound when executing SQL.
    *
    * @return string $sql
    */
    protected function createUpdateSQL(&$row)
    {
        $this->checkPrimaryKey();

        //Don't want to include any fieldname entries that don't have a value set.
        $FieldsToInclude = array();
        foreach($this->fieldNames as $Field)
        {
            if(isset($row[$Field]))
            {
                $FieldsToInclude[] = $Field;
            }
        }

        $sql = 'UPDATE '.$this->table.' SET '.implode(',', array_map(array($this, 'createUpdateBindSQL'), $FieldsToInclude));

        $keysql = '';

        foreach($this->Keys as $KeyField)
        {
            if (!empty($KeyField))
            {
                $keysql[] = $KeyField.' = '.$this->addColon($KeyField).'2';
                $row[$KeyField.'2'] = $row[$KeyField];
            }
        }

        $sql .= ' WHERE '.implode(' AND ', $keysql);

        return $sql;
    }

    /**
    * Used when creating the SET clause for the update SQL statement.
    *
    * @param  string $value
    *
    * @return string
    */
    protected function createUpdateBindSQL($value)
    {
        return $value.' = :'.$value;
    }

    /**
    * Checks to see whether or not the record already exists in the data table.
    * Record can then be updated/skipped accordingly when updating/appending.
    *
    * @param  array   $row  The row of data we're attempting to update/append.
    *
    * @return boolean
    */
    protected function recordExists($row)
    {
        $this->checkPrimaryKey();

        $sql = 'SELECT COUNT(*) FROM '.$this->table;

        $keysql = '';

        if(empty($this->Keys))
        {
            // There are no keys set, so there is no way to match the record.
            return false;
        }

        $bindParams = array();

        foreach($this->Keys as $KeyField)
        {
            if (!empty($KeyField))
            {
                if($row[$KeyField] != '')
                {
                    $keysql[] = $KeyField.' = '.$this->addColon($KeyField);
                    $bindParams[$KeyField] = $row[$KeyField];
                }
                else
                {
                    $keysql[] = $KeyField.' = \'\' OR '.$KeyField.' IS NULL';
                }
            }
        }

        $sql .= ' WHERE '.implode(' AND ', $keysql);

        return (boolean) DatixDBQuery::pdo_fetch($sql, $bindParams, PDO::FETCH_COLUMN);
    }

    /**
    * Validates the data provided in the importArray against the data type of the field. If any mismatches are found, the data is blanked out.
    */
    protected function validateData()
    {
        $checkCCS2 = true;

        if (isset($_SESSION['ccs2_verified']) && $_SESSION['ccs2_verified'] == 'verified')
        {
            $checkCCS2 = false;
        }

        foreach($this->fieldNames as $fieldname)
        {
            try
            {
                $FieldObjects[$fieldname] = new Fields_Field($fieldname);
            }
            catch (Exception $e)
            {
                continue;
            }
        }

        foreach($this->importArray as $index => $row)
        {
            if ($checkCCS2)
            {
                $this->checkCCS2Version($row);
            }

            foreach ($row as $fieldname => $value)
            {
                if (isset($FieldObjects[$fieldname]))
                {
                    switch($FieldObjects[$fieldname]->getFieldType())
                    {
                        case 'N':
                            if(!is_numeric($value))
                            {
                                $this->importArray[$index][$fieldname] = 0;
                            }
                        break;

                        case 'D':
                            if(preg_match('/[a-zA-Z]/u', $value))
                            {
                                $this->importArray[$index][$fieldname] = null;
                            }
                        break;

                        default:

                        break;
                    }
                }
            }
        }
    }

    /**
    * Adds a colon to the front of a string.  Used when binding parameters in PDO queries.
    *
    * @param  string $value
    *
    * @return string
    */
    protected function addColon($value)
    {
        return ':'.$value;
    }

    public function getNumRecordsAdded()
    {
        return intval($this->addedRecords);
    }

    public function getNumRecordsUpdated()
    {
        return intval($this->updatedRecords);
    }

    /**
    * @desc Some fields, specifically rep_approved, cannot be left blank when inserting, since the web relies on them being present.
    */
    protected function checkRequiredFieldsForInsert(&$row)
    {
        global $ModuleDefs;

        $ModApprovalMap = array(
            'CON' => 'FA',
            'COM' => 'UN',
            'INC' => 'AWAREV',
            'RAM' => 'AWAREV',
            'PAL' => 'AWAREV',
            'AST' => 'UN'
            );

        foreach($ModuleDefs as $mod => $details)
        {
            if($this->table == $details['TABLE'])
            {
                $Module = $mod;
            }
        }

        if(!$Module || !$ModuleDefs[$Module]['USES_APPROVAL_STATUSES'] || !$ModApprovalMap[$Module])
        {
            return;
        }

        if(!in_array('rep_approved', $this->fieldNames))
        {
            $this->fieldNames[] = 'rep_approved';
        }

        foreach($this->importArray as $key => $recordData)
        {
            $row['rep_approved'] = $ModApprovalMap[$Module];
        }
    }

    protected function checkCCS2Version(&$row)
    {
        if (\UnicodeString::strpos($this->table, 'ccs2'))
        {
            $sql = 'SELECT TOP(1) ccs2_version FROM ' . $this->table;
            $ccs2StoredVersion = DatixDBQuery::PDO_fetch($sql, array(), PDO::FETCH_COLUMN);

            if (intval($row['ccs2_version']) < intval($ccs2StoredVersion))
            {
                throw new Exception('CCS2');
            }
        }

        return;
    }


    /**
    * Handles the import process.
    */
    abstract public function importData();

    /**
    * Creates the field names array.
    */
    abstract protected function setFieldNames();

    /**
    * Creates the import array.
    *
    * The array needs to be created in the format expected by insertData() - importArray[row number][field name] = field value
    */
    abstract protected function buildImportArray();

    /**
    * Checks that a primary key has been defined before attempting to import data.
    */
    abstract protected function checkPrimaryKey();
}