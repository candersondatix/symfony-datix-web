<?php
/**
* Class for handling import of table data using an import profile.
*/
class Import_UsingProfile extends Import_FromFile
{
    /**
    * @desc Holds the profile object that we are using to define this import.
    * @var ImportProfile
    */
    protected $Profile;

    /**
    * @desc Array of transformations to be applied to values found in the source file.
    * @var array
    */
    protected $Mappings;

    /**
    * @desc Becomes true when valid data is found in the file.
    * @var bool
    */
    protected $FileContainsData = false;

    /**
    * @desc Used to track whether there are validation errors with the import.
    * @var array
    */
    protected $ValidationErrors = array();

    /**
    * Constructor.
    *
    * @param Files_CSVLoadFile $file
    * @param DatixDBQuery      $query
    * @param ImportProfile     $profile - the profile used to format the imported data.
    */
    public function __construct(Files_File $file, DatixDBQuery $query, ImportProfile $profile)
    {
        $this->Profile = $profile;

        $cols = $this->Profile->getColumnConfig();

        if(!$cols || empty($cols))
        {
            throw new MissingParameterException('Your profile needs to contain some columns for an import to work.');
        }
        if($file instanceof Files_CSV && $this->Profile->getType() != 'CSV')
        {
            throw new IncorrectFileTypeException('This profile will only work with CSV files');
        }
        if($file instanceof Files_Excel && $this->Profile->getType() == 'CSV')
        {
            throw new IncorrectFileTypeException('This profile will only work with Excel files');
        }

        $this->table = $this->Profile->getForm();
        $this->existingRecordsAction = 'UPDATE';

        parent::__construct($file, $query);

    }

    /**
    * Handles the import process.
    */
    public function importData()
    {
        $this->setFieldNames();
        $this->defineMappings();
        $this->buildImportArray();

        if(!empty($this->ValidationErrors))
        {
            foreach($this->ValidationErrors as $Error)
            {
                AddSessionMessage('ERROR', $Error);
            }
            throw new InvalidDataException('There were validation errors while processing the import. Please fix these errors before retrying the import.');
        }

        if($this->FileContainsData)
        {
            $this->insertData();
        }
        else
        {
            throw new InvalidDataException('There is no data to import');
        }
    }


    /**
    * Retrieves a list of field names from the load file.
    */
    protected function setFieldNames()
    {
        $ColumnConfig = $this->Profile->getColumnConfig();

        foreach($ColumnConfig as $Column)
        {
            $this->fieldNames[] = $Column->Data['exc_dest_field'];

            if(bYN($Column->Data['exc_id_field']))
            {
                $this->Keys[] = $Column->Data['exc_dest_field'];
            }
        }

        $this->fieldNames = array_unique($this->fieldNames);



    }

    /**
    * Set up an array of data-value mappings to use.
    */
    protected function defineMappings()
    {
        $ColumnConfig = $this->Profile->getColumnConfig();

        foreach($ColumnConfig as $Column)
        {
            $this->Mappings[$Column->Data['exc_col_letter']] = $Column->getMappingConfig();
        }

    }

    /**
    * Builds the import array.
    */
    protected function buildImportArray()
    {
        global $FieldDefs;

        $recordCount = 0;

        for($i = 1; $i < $this->Profile->getStartAt(); $i++)
        {
            $line = $this->file->getNextLine();
        }

        $OrderArray = $this->Profile->getColumnOrderArray();

        ksort($OrderArray);

        while (($line = $this->file->getNextLine()) !== false)
        {
            $importValues = array();

            if($this->CheckLineIsValid($line))
            {
                foreach ($line as $index => $value)
                {
                	$real_index = $index;
                	if( $this->Profile->getType() == 'CSV' )
                	{
                		//csv profile indexing starts with 1,2,3... rather than 0,1,2
                		$real_index = intval($index) + 1;
                	}
                	
                    if($Column = $this->Profile->getColumnFromKey( $real_index )->Data['exc_dest_field'])
                    {
                        $Field = new Fields_Field($this->Profile->getColumnFromKey( $real_index )->Data['exc_dest_field'], $this->Profile->getForm());

                        //date validation needs to happen before dates are converted
                        if ($Field->getFieldType() == 'D' && $value != '')
                        {
                            if(!is_numeric($value))
                            {
                                $this->ValidationErrors[] = 'The value "'.$value.'" is not valid for the field "'.Labels_FieldLabel::GetFieldLabel($Field->getName()).'"';
                            }
                        }

                        switch($this->Profile->getType())
                        {
                            case 'CSV':                                
                                $importValues[ $real_index ] = $value;
                                break;
                                
                            case 'EXC':
                            default:
                                if(in_array($index, $OrderArray, true))
                                {
                                    if($Field->getFieldType() == 'D')
                                    {
                                        $value = PHPExcel_Shared_Date::ExcelToPHP($value);

                                        $value = TimeStampToSQLDateStr($value);
                                    }
                                    else if($FieldDefs[$this->Profile->getModule()][$Field->getName()]['Type'] == 'time')
                                    {
                                        //can't use PHPExcel_Shared_Date::ExcelToPHP() here because it gets confused about BST for some reason.
                                        $value = $value - intval($value);

                                        $hours = round($value * 24);
                                        $mins = round($value * 24 * 60) - round($hours * 60);

                                        $hours = str_pad((int) $hours,2,"0",STR_PAD_LEFT);
                                        $mins = str_pad((int) $mins,2,"0",STR_PAD_LEFT);

                                        $value = (string) $hours.$mins;
                                    }

                                    $importValues[$index] = $value;
                                }
                                break;
                        }

                    }
                }

                foreach($OrderArray as $key)
                {
                    if(!bYN($this->Profile->getColumnFromKey($key)->Data['exc_ignore_nulls']) || $importValues[$key] != '')
                    {
                        $Value = $this->MapValue($importValues[$key], $key);

                        if($Value)
                        {
                            $this->FileContainsData = true;
                        }

                        $this->importArray[$recordCount][$this->Profile->getColumnFromKey($key)->Data['exc_dest_field']] .= $Value;
                    }
                }

                foreach ($this->importArray[$recordCount] as $fieldName => $value)
                {
                    $Field = new Fields_Field($fieldName, $this->Profile->getForm());

                    //coded field validation needs to happen after mapping.
                    if($Field->getFieldType() == 'C' && $value != '')
                    {
                        $CodeTable = $Field->getCodeTable();

                        //special case for EMPLOYEE_MULTI_CODES, since the values these fields will accept change with a global
                        if (in_array($Field->getName(), array('con_organisation', 'con_unit', 'con_directorate', 'con_clingroup', 'con_specialty', 'con_location', 'con_locactual'))
                            && !bYN(GetParm('EMPLOYEE_MULTI_CODES'))
                            && count(explode(' ', $value)) > 1
                        )
                        {
                            $this->ValidationErrors[] = 'The code "'.$value.'" is not valid for the field "'.Labels_FieldLabel::GetFieldLabel($Field->getName()).'"';
                        }
                        else
                        {
                            foreach (explode(' ', $value) as $codeValue)
                            {
                                if ($CodeTable{0} == '!')
                                {
                                    $sql = 'SELECT count(*) as num
                                        FROM code_types
                                        WHERE cod_code = :value
                                        AND cod_type = :code_type';

                                    $PDOArray = array('value' => $codeValue, 'code_type' => \UnicodeString::substr($Field->getCodeTable(), 1));
                                }
                                elseif ($Field->getCodeDescription() != '' && $Field->getCodeTable() != '' && $Field->getCodeTableField() != '')
                                {
                                    $sql = 'SELECT count(*) as num
                                            FROM ' . $Field->getCodeTable() . '
                                            WHERE ' . $Field->getCodeTableField() . ' = :value' . ($Field->getCodeWhere() ? ' AND ' . $Field->getCodeWhere() : '');
                                    $PDOArray = array('value' => $codeValue);
                                }

                                if ($sql)
                                {
                                    $row = PDO_fetch($sql, $PDOArray);
                                }
                                if (intval($row['num']) == 0)
                                {
                                    $this->ValidationErrors[] = 'The code "' . $codeValue . '" is not valid for the field "' . Labels_FieldLabel::GetFieldLabel($Field->getName()) . '"';
                                }
                            }
                        }
                    }
                }
                $recordCount++;
            }
        }
    }

    /**
    * @desc Checks that the line passed is valid to be imported - at the moment this just
    * involves making sure that the header value is correct if one has been specified.
    */
    protected function CheckLineIsValid($line)
    {
        if($this->Profile->getRowHeader())
        {
            switch($this->Profile->getType())
            {
                case 'CSV':
                    $key = 0;
                    break;
                case 'EXC':
                default:
                    $key = 'A';
                    break;
            }

            if($line[$key] != $this->Profile->getRowHeader())
            {
                return false;
            }
        }

        return true;
    }

    /**
    * @desc Transforms a value according to the transformations set in the profile.
    * Before the value is mapped, we have to replace two @codes
    */
    protected function MapValue($value, $key)
    {
        $Transformations = $this->Mappings[$key];

        if(isset($Transformations) && !empty($Transformations))
        {
            foreach($Transformations as $old => $new)
            {
                // @MYVALUE is replaced with the value we are currently mapping
                $old2 = preg_replace('/@MYVALUE/u', $value, $old);
                $new2 = preg_replace('/@MYVALUE/u', $value, $new);
                // @RETURN is replaced with a carriage return.
                $new2 = preg_replace('/@RETURN/u', "\r\n", $new2);
                $Mappings[$old2] = $new2;
            }

            if(isset($Mappings[$value]) && !empty($Mappings[$value]))
            {
                return $Mappings[$value];
            }
        }

        return $value;
    }

    /**
    * Not implemented in this class.
    */
    protected function checkPrimaryKey(){}
}
