<?php
/**
* Class for handling import of table data from load files.
*/
class Import_LoadFile extends Import_FromFile
{
    /**
    * Constructor.
    *
    * @param Files_CSVLoadFile $file
    * @param DatixDBQuery      $query
    */
    public function __construct(Files_CSVLoadFile $file, DatixDBQuery $query)
    {
        parent::__construct($file, $query);
    }

    /**
    * Handles the import process.
    */
    public function importData()
    {
        $this->setHeader();
        $this->setFieldNames();
        $this->buildImportArray();
        $this->insertData();
    }

    /**
    * Retrieves header information from the load file.
    */
    protected function setHeader()
    {
        list(
            $this->table,
            $this->existingRecordsAction,
            $key1,
            $key2,
            $this->deleteField,
            $this->deleteCode
        ) = $this->file->getHeader();

        if($key1)
        {
            $this->Keys[] = $key1;
        }
        if($key2)
        {
            $this->Keys[] = $key2;
        }
    }

    /**
    * Sets the first value in the field names as the array if no primary key has been explicitly set in the load file.
    */
    protected function checkPrimaryKey()
    {
        if (empty($this->Keys))
        {
            $this->Keys[] = $this->fieldNames[0];
        }
    }

    /**
    * Retrieves a list of field names from the load file.
    */
    protected function setFieldNames()
    {
        $this->fieldNames = $this->file->getFieldNames();

        // fields are defined in the format <field name>:<type>
        foreach ($this->fieldNames as $index => $field)
        {
            $field = explode(':', $field);
            $this->fieldNames[$index] = $field[0];
            if ($field[1] == 'N')
            {
                // need to keep a note of numeric fields to avoid SQL errors when binding empty strings.
                $this->numericFields[] = $field[0];
            }
        }
    }

    /**
    * Builds the import array.
    */
    protected function buildImportArray()
    {
        $recordCount = 0;

        while (($line = $this->file->getNextLine()) !== false)
        {
            foreach ($line as $index => $value)
            {
                if (in_array($this->fieldNames[$index], $this->numericFields) && $value == '')
                {
                    $value = 0;
                }
                $this->importArray[$recordCount][$this->fieldNames[$index]] = $value;
            }
            $recordCount++;
        }
    }
}