<?php

class Table
{
    protected $Name;
    protected $Index;
    protected $Description;

    public function __construct($TableName)
    {
        $this->Name = $TableName;

        if(!$_SESSION['CachedValues']['table_directory'][$TableName])
        {
            $Data = DatixDBQuery::PDO_fetch('SELECT tdr_index, tdr_desc FROM table_directory WHERE tdr_name = :table_name', array('table_name' => $TableName));

            $_SESSION['CachedValues']['table_directory'][$TableName]['Index'] = $Data['tdr_index'];
            $_SESSION['CachedValues']['table_directory'][$TableName]['Description'] = $Data['tdr_desc'];
        }

        $this->Index = $_SESSION['CachedValues']['table_directory'][$TableName]['Index'];
        $this->Description = $_SESSION['CachedValues']['table_directory'][$TableName]['Description'];
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getIndex()
    {
        return $this->Index;
    }

    public function getDescription()
    {
        return $this->Description;
    }

    public function getSQLIndex()
    {
        $IndexColumn = DatixDBQuery::PDO_fetch('
            SELECT SC.NAME as col FROM dbo.sysobjects SO
            INNER JOIN dbo.syscolumns SC ON SO.id = SC.id
            LEFT JOIN dbo.syscomments SM ON SC.cdefault = SM.id
            WHERE SO.xtype = \'U\' AND COLUMNPROPERTY(SO.id, SC.name, \'isIdentity\') = 1
            AND SO.NAME = :table_name', array('table_name' => $this->Name), PDO::FETCH_COLUMN);

        return $IndexColumn;
    }

    /**
    * Check to see if the table uses a recordid column.
    *
    * @return boolean
    */
    public function hasRecordid()
    {
        return (bool) DatixDBQuery::PDO_fetch('
            SELECT SC.NAME FROM dbo.sysobjects SO
            INNER JOIN dbo.syscolumns SC ON SO.id = SC.id
            WHERE SO.xtype = \'U\' AND SC.NAME = \'recordid\'
            AND SO.NAME = :table_name', array('table_name' => $this->Name), PDO::FETCH_COLUMN);
    }

    public static function getTableIndex($TableName)
    {
        $Table = new Table($TableName);
        return $Table->getIndex();
    }

    /**
    * Check if column is an identitiy column
    * 
    * @param string $table Table name
    * @param string $column Column name
    */
    public static function isIdentityColumn($table, $column)
    {
        $sql = "SELECT is_identity FROM sys.columns JOIN sys.tables ON sys.columns.object_id = sys.tables.object_id
                WHERE sys.columns.name = :column
                AND sys.tables.name = :table";   
            
        $row = DatixDBQuery::PDO_fetch($sql, array("table" => $table, "column" => $column));
        if ($row["is_identity"] == 1)
        {
            return true;
        }
        else
        {
            return false;   
        }
    }
}
