<?php

/**
* @desc Dummy class to replace Zend_Translate when no language files are provided, since otherwise notices get thrown.
*/
class View_NullTranslate
{
    public function translate($Parameters)
    {
        global $txt;
        //if this is a keyed value, it will already have a translation/replacement ready
        if($txt[$Parameters['key']])
        {
            return $txt[$Parameters['key']];
        }

        //otherwise translate the text.
        return $Parameters['text'];
    }
}
