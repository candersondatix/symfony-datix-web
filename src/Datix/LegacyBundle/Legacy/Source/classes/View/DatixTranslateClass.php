<?php

/**
* @desc Custom adapter for Zend_Translate to allow us to manage text replacements alongside translations.
* For now, this is just a shell, since the translation work has not been started.
*/
class View_DatixTranslate extends Zend_Translate_Adapter_Csv
{
    public function translate($Parameters)
    {
        global $txt;
        //if this is a keyed value, it will already have a translation/replacement ready
        if($txt[$Parameters['key']])
        {
            return $txt[$Parameters['key']];
        }

        //otherwise translate the text.
        return parent::translate($Parameters['text']);
    }

    public function toString()
    {
        return 'View_DatixTranslate';
    }

}
