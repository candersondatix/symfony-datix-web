<?php
namespace Source\classes\Filters\Layer;

use Source\classes\Filters\Layer\Layer; 

/**
* Holds a collection of Layer objects.
*/
class LayerCollection implements \Iterator, \Countable
{
    /**
    * An array of Layer objects.
    * 
    * @var array
    */
    protected $layers;
    
    /**
    * Used to track the current position when iterating.
    * 
    * @var int
    */
    protected $position;
    
    /**
    * Constructor.
    */
    public function __construct()
    {
        $this->layers = array();    
        $this->position = 0;    
    }
    
    /**
    * Adds a Layer object to the $layers array.
    * 
    * @param Layer $layer
    */
    public function addLayer(Layer $layer)
    {
        $this->layers[] = $layer;    
    }
    
    /**
    * Iterator implementation.
    * 
    * @codeCoverageIgnore
    */
    public function rewind() 
    {
        $this->position = 0;
    }

    /**
    * Iterator implementation.
    * 
    * @codeCoverageIgnore
    */
    public function current() 
    {
        return $this->layers[$this->position];
    }

    /**
    * Iterator implementation.
    * 
    * @codeCoverageIgnore
    */
    public function key() 
    {
        return $this->position;
    }

    /**
    * Iterator implementation.
    * 
    * @codeCoverageIgnore
    */
    public function next() 
    {
        ++$this->position;
    }

    /**
    * Iterator implementation.
    * 
    * @codeCoverageIgnore
    */
    public function valid() 
    {
        return isset($this->layers[$this->position]);
    }

    /**
    * Countable implementation.
    * 
    * @codeCoverageIgnore
    */
    public function count()
    {
        return count($this->layers);    
    }    
}