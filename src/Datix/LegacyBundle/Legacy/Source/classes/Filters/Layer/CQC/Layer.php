<?php
namespace Source\classes\Filters\Layer\CQC;

/**
* Layer extended to accomodate CQC-specific functionality.
*/
class Layer extends \Source\classes\Filters\Layer\Layer
{
    /**
    * Fetches an array of codes/descriptions for the value field.
    * 
    * Handles non-standard code retrieval required for CQC.
    * 
    * @param string $term The search term used to filter the code results.
    * 
    * @return array $codes
    */
    public function getFilterValues($term)
    {
        $codes = array();
        
        switch ($this->field)
        {
            // select "hardcoded" fields
            case 'cto_ref':
            case 'cto_desc_short':
            case 'ctp_ref':
            case 'ctp_desc_short':
            case 'cts_ref':
            case 'cts_desc_short':
                $codes = $this->getHardcodedFieldValues($term);
                break;
            
            // location tiers    
            case (preg_match('/tier_(\d)/u', $this->field, $matches) ? $this->field : !$this->field):
                $codes = $this->getLocations($matches[1], \System_NestedSetMapperFactory::getNestedSetMapper(\System_NestedSetMapperFactory::LOCATIONS), $term);
                break;
            
            // standard coded/multicoded fields    
            default:
                $codes = parent::getFilterValues($term);
                break;   
        }
        
        return $codes;
    }
    
    /**
    * Retrieves the values for a set of "hardcoded" (i.e. non-standard) fields.
    * 
    * These fields are free-text fields on the template records.
    * 
    * @param string       $field  The name of the field we're returning values for.
    * @param string       $term   The term used to filter the results.
    * @param DatixDBQuery $query  The DB query object.
    * 
    * @return array
    */
    protected function getHardcodedFieldValues($term)
    {
        $codes[] = $this->getNoValueCode();
        
        switch ($this->field)
        {
            case 'cto_ref':
            case 'cto_desc_short':
                $table = 'cqc_template_outcomes';
                break;
                
            case 'ctp_ref':
            case 'ctp_desc_short':
                $table = 'cqc_template_prompts';
                break;
                
            case 'cts_ref':
            case 'cts_desc_short':
                $table = 'cqc_template_subprompts';
                break;    
        }
        
        $this->query->setSQL('SELECT DISTINCT '.$this->field.' AS value, '.$this->field.' AS description FROM '.$table.' WHERE '.$this->field.' LIKE :term');
        $this->query->prepareAndExecute(array('term' => '%'.$term.'%'));
        return array_merge($codes, $this->query->fetchAll(\PDO::FETCH_ASSOC)); 
    }
    
    /**
    * Retrieves a list of leaf nodes under a certain depth in the location hierarchy.
    * 
    * @param int                    $depth     The depth of the hierarchy to search under.
    * @param System_NestedSetMapper $hierarchy The mapper for the loactions hierarchy.
    * @param string                 $term      The search term used to filter the code results.
    * 
    * @return array $locations
    */
    protected function getLocations($depth, \System_NestedSetMapper $hierarchy, $term)
    {
        $locations = array();
        $params = array();
        
        $where = MakeSecurityWhereClause('vw_locations_main.loc_name LIKE :term', 'LOC', $_SESSION['initials']);
        $params['term'] = '%'.$term.'%';
        
        $nodes = $hierarchy->getNodesByDepth($depth, $where, $params);
        foreach ($nodes as $node)
        {
            $locations[] = array('value' => $node['recordid'], 'description' => htmlfriendly($node['loc_name']));           
        }
        return $locations;                
    }
    
    /**
    * Creates the codes used to populate this layer's "field" dropdown.
    * 
    * @return array $codes The "field" dropdown codes.
    */
    protected function getFieldCodes()
    {
        // define field names available for filtering at each level
        $CQOFields = array(
            'cdo_manager',
            'cdo_handlers',
            'cto_theme',
            'cto_ref',
            'cto_desc_short'    
        );
        
        $CQPFields = array(
            'ctp_ref',    
            'ctp_desc_short',    
            'ctp_element',    
            'cdp_manager',    
            'cdp_handlers'
        );
        
        $CQSFields = array(
            'cts_ref',
            'cts_desc_short',
            'cds_status',
            'cds_manager',
            'cds_handlers'
        );
        
        // status field used depends on rollup configuration
        // also, only the current level's status is available for filtering (i.e. you can't filter using the parent level's status)
        if (in_array(GetParm('CQC_ROLLUP_TYPE', 'NONE'), array('AVE', 'WORST')))
        {
            if ($this->module == 'CQO')
            {
                $CQOFields[] = 'cdo_status';    
            }
            
            if ($this->module == 'CQP')
            {
                $CQPFields[] = 'cdp_status';   
            }
        }
        else
        {
            if ($this->module == 'CQO')
            {
                $CQOFields[] = 'cdo_status_input';    
            }
            
            if ($this->module == 'CQP')
            {
                $CQPFields[] = 'cdp_status_input';   
            }   
        }
        
        $codes = array();
        
        // add CQO filter fields to dropdown array        
        foreach ($CQOFields as $field)
        {
            $label = \Fields_Field::getFieldLabel($field);
            if ($this->module == 'CQP' || $this->module == 'CQS')
            {
                $label .= ' ('._tk('cqc_outcome').')';
            }
            
            $codes[$field] = $label;               
        }
        
        // add CQP filter fields to dropdown array
        if ($this->module == 'CQP' || $this->module == 'CQS')
        {
            foreach ($CQPFields as $field)
            {
                $label = \Fields_Field::getFieldLabel($field);
                if ($this->module == 'CQS')
                {
                    $label .= ' ('._tk('cqc_prompt').')';
                }
                
                $codes[$field] = $label;   
            }    
        }
        
        // add CQS filter fields to dropdown array
        if ($this->module == 'CQS')
        {
            foreach ($CQSFields as $field)
            {
                $codes[$field] = \Fields_Field::getFieldLabel($field);   
            }    
        }
        
        // fetch tiers from loaction hierarchy and add to dropdown array
        $this->query->setSQL("SELECT 'tier_' + CAST(lti_depth AS varchar), lti_name FROM location_tiers");
        $this->query->prepareAndExecute();
        $codes = array_merge($codes, $this->query->fetchAll(\PDO::FETCH_KEY_PAIR));
        
        asort($codes);
        return $codes;   
    }
    
    /**
    * Creates the "value" dropdown for this layer.
    * 
    * Extended to handle location values.
    *
    * @param int $id The number used to identify this filter layer.
    * 
    * @return Forms_SelectField $dropdown
    */
    protected function createValueDropdown($id)
    {
        $dropdown = parent::createValueDropdown($id);
        
        if (preg_match('/tier_(\d)/u', $this->field))
        {
            $dropdown->setFieldFormatsName('cdo_location');    
        }
        
        return $dropdown;        
    }    
}