<?php
namespace Source\classes\Filters\Layer;

/**
* Represents an individual layer within a filter (i.e. a field/value pairing).
*/
class Layer
{
    const ADD = 1;
    const DELETE = 2;
    
    /**
    * The name of the module we're filtering on.
    * 
    * @var string
    */
    protected $module;
    
    /**
    * The name of the field we're filtering on.
    * 
    * @var string
    */
    protected $field;
    
    /**
    * The value of the field we're filtering on.
    * 
    * @var string
    */
    protected $value;
    
    /**
    * The DB query object.
    * 
    * @var DatixDBQuery
    */
    protected $query;
    
    /**
    * Constructor.
    * 
    * @param string       $module
    * @param string       $field
    * @param string       $value
    * @param DatixDBQuery $query
    * 
    * @throws MissingParameterException
    */
    public function __construct($module, $field, $value, \DatixDBQuery $query)
    {
        if ($module == '')
        {
            throw new \MissingParameterException('Missing module parameter');      
        }
                
        if ($field == '')
        {
            throw new \MissingParameterException('Missing field parameter');      
        }
        
        $this->module = \Sanitize::getModule($module);
        $this->field = $field;    
        $this->value = $value;
        $this->query = $query;  
    }
    
    /**
    * Constructs the HTML for the filter layer.
    * 
    * @global Zend_View $DatixView
    *
    * @param int $id     The number used to identify this filter layer.
    * @param int $action The action for the right-hand link (i.e. delete layer/add another).
    * 
    * @return string
    * 
    * @throws MissingParameterException
    */
    public function display($id, $action)
    {
        global $DatixView;
        
        if (!is_numeric($id))
        {
            throw new \MissingParameterException('Missing id parameter');
        }
        
        $DatixView->field = $this->createFieldDropdown($id);
        $DatixView->value = $this->createValueDropdown($id);
        $DatixView->value->setOnChangeExtra($DatixView->render('classes/Filters/Layer/ValueOnChange.php'));
        $DatixView->id = $id;
        $DatixView->module = $this->module;
        $DatixView->fieldValue = addslashes($this->field);
        $DatixView->addAnother = _tk('filter_add_another');
        $DatixView->delete = _tk('filter_delete');
        $DatixView->deleteDisplay = false;
        $DatixView->addDisplay = false;
        
        switch ($action)
        {
            case self::ADD:
                $DatixView->addDisplay = true;
                break;
                
            case self::DELETE:
                $DatixView->deleteDisplay = true;
                break;
        }
        
        return $DatixView->render('classes/Filters/Layer/Display.php');   
    }
    
    /**
    * Uses the standard code retrival method to return an array of codes/descriptions.
    * 
    * @param string $term The search term used to filter the code results.
    * 
    * @return array $codes
    * 
    * @codeCoverageIgnore
    */
    public function getFilterValues($term)
    {
        // the first value in the list allows users to select "no value"
        $codes[] = $this->getNoValueCode();

        require_once 'Source/libs/CodeSelectionCtrl.php';
        $bindArray = array();
        $this->query->setSQL(GetCodeListSQL($this->module, $this->field, '', true, (\UnicodeString::substr($this->field, 0, 3) == 'UDF'), false, '', $title, $bindArray, $term));
        $this->query->prepareAndExecute($bindArray);
        $codes = array_merge($codes, $this->query->fetchAll(\PDO::FETCH_ASSOC));
        
        return $codes;
    }
    
    /**
    * Controls access to class properties.
    * 
    * @param string $name The name of the property being accessed.
    * 
    * @throws InaccessibleParameterException
    */
    public function __get($name)
    {
        $accessible = array(
            'field',    
            'value'    
        );
        
        if (in_array($name, $accessible))
        {
            return $this->name;    
        }
        
        throw new \InaccessibleParameterException('You do not have permission to access this property');    
    }
    
    /**
    * Returns the "<No value>" code for filter values.
    * 
    * @return array
    */
    protected function getNoValueCode()
    {
        return array('value' => '!NO_VALUE!', 'description' => _tk('filter_no_value'));    
    }
    
    /**
    * Creates the "field" dropdown for this layer.
    * 
    * @global Zend_View $DatixView
    * 
    * @param int $id The number used to identify this filter layer.
    * 
    * @return Forms_SelectField $dropdown
    * 
    * @codeCoverageIgnore
    */
    protected function createFieldDropdown($id)
    {
        global $DatixView;
        
        $dropdown = \Forms_SelectFieldFactory::createSelectField('filter_field_'.$id, $this->module, $this->field, '');
        $dropdown->setSuppressCodeDisplay();
        $dropdown->setCustomCodes($this->getFieldCodes());
        
        $DatixView->id = $id;  
        $dropdown->setOnChangeExtra($DatixView->render('classes/Filters/Layer/CreateFieldDropdown.php'));
        
        return $dropdown;    
    }
    
    /**
    * Creates the codes used to populate this layer's "field" dropdown.
    * 
    * @global array $ModuleDefs
    * @global array $FieldDefs
    * 
    * @return array $codes The "field" dropdown codes.
    */
    protected function getFieldCodes()
    {
        global $ModuleDefs, $FieldDefs;
        
        $fields = $ModuleDefs[$this->module]['VIEW_FIELD_ARRAY'] ?: $ModuleDefs[$this->module]['FIELD_ARRAY'];
        $validFields = array();
        $codes = array();
        
        foreach ($fields as $field)
        {
            if (in_array($FieldDefs[$this->module][$field]['Type'], array('ff_select', 'multilistbox')))
            {
                $validFields[] = $field;       
            }    
        }
        
        foreach ($validFields as $field)
        {
            $codes[$field] = \Fields_Field::getFieldLabel($field, ($ModuleDefs[$this->module]['VIEW'] ? $ModuleDefs[$this->module]['VIEW'] : $ModuleDefs[$this->module]['TABLE']));
        }
        
        $codes = array_merge($codes, $this->getFieldExtraFields());
        
        asort($codes);
        return $codes;    
    }
    
    /**
    * Retrieves the extra fields assigned to this module to populate this layer's "field" dropdown.
    * 
    * @return array
    */
    protected function getFieldExtraFields()
    {
        $this->query->setSQL("
            SELECT ('UDF_' + udf_fields.fld_type + '_0_' + CAST(udf_fields.recordid AS VARCHAR(32))) AS fmt_field, fld_name AS fmt_new_label
            FROM udf_fields
            LEFT JOIN udf_mod_link ON udf_mod_link.uml_id = udf_fields.recordid
            WHERE fld_type IN ('C','T') AND (udf_mod_link.uml_module = :module)");
        $this->query->prepareAndExecute(array('module' => $this->module));
        return $this->query->fetchAll(\PDO::FETCH_KEY_PAIR);   
    }
    
    /**
    * Creates the "value" dropdown for this layer.
    *
    * @param int $id The number used to identify this filter layer.
    * 
    * @return Forms_SelectField $dropdown
    * 
    * @codeCoverageIgnore
    */
    protected function createValueDropdown($id)
    {
        $dropdown = \Forms_SelectFieldFactory::createSelectField('filter_value_'.$id, $this->module, $this->value, '');
        $dropdown->setSuppressCodeDisplay();
        $dropdown->setSelectFunction('getFilterValues', array('fields' => 'fieldValues', 'module' => '"'.$this->module.'"', 'id' => $id));
        $dropdown->setFieldFormatsName($this->field);
        
        if ($this->value == '!NO_VALUE!')
        {
            $dropdown->setDescription(_tk('filter_no_value'));    
        }
        
        return $dropdown;        
    }
}
