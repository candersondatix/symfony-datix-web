<?php
namespace Source\classes\Filters\Layer\LOC;

/**
* Layer extended to accomodate Location module-specific functionality.
*/
class Layer extends \Source\classes\Filters\Layer\Layer
{
    /**
    * Fetches an array of codes/descriptions for the value field.
    * 
    * Handles non-standard code retrieval required for LOC module.
    * 
    * @param string $term The search term used to filter the code results.
    * 
    * @return array $codes
    */
    public function getFilterValues($term)
    {
        $codes = array();
        
        switch ($this->field)
        {
            // location name field
            case 'loc_name':
                $codes[] = $this->getNoValueCode();
                $codes = array_merge($codes, $this->getLocationNames($term));
                break;
            
            // location tiers    
            case 'tier':
                $codes = $this->getTiers($term);
                break;
            
            // standard coded/multicoded fields    
            default:
                $codes = parent::getFilterValues($term);
                break;   
        }
        
        return $codes;
    }
    
    /**
    * Retrieves a list of location names.
    *
    * @param string $term The term used to filter the results.
    * 
    * @return array
    */
    protected function getLocationNames($term)
    {
        $this->query->setSQL('SELECT DISTINCT loc_name AS value, loc_name AS description FROM locations_main WHERE loc_name LIKE :term');
        $this->query->prepareAndExecute(array('term' => '%'.$term.'%'));
        return $this->query->fetchAll(\PDO::FETCH_ASSOC); 
    }
    
    /**
    * Retrieves a list of location hierarchy tiers.
    * 
    * @param string $term The search term used to filter the code results.
    * 
    * @return array
    */
    protected function getTiers($term)
    {
        $this->query->setSQL('SELECT lti_depth AS value, lti_name AS description FROM location_tiers WHERE lti_name LIKE :term');
        $this->query->prepareAndExecute(array('term' => '%'.$term.'%'));
        return $this->query->fetchAll(\PDO::FETCH_ASSOC);                
    }
    
    /**
    * Creates the codes used to populate this layer's "field" dropdown.
    * 
    * @return array $codes The "field" dropdown codes.
    * 
    * @codeCoverageIgnore
    */
    protected function getFieldCodes()
    { 
        $codes = array(
            'loc_name' => \Fields_Field::getFieldLabel('loc_name'),
            'tier' => 'Tier',
            'loc_type' => \Fields_Field::getFieldLabel('loc_type'),
        );
          
        asort($codes);
        return $codes;   
    }
    
    /**
    * Creates the "value" dropdown for this layer.
    * 
    * Extended to handle tier values.
    *
    * @param int $id The number used to identify this filter layer.
    * 
    * @return Forms_SelectField $dropdown
    */
    protected function createValueDropdown($id)
    {
        $dropdown = parent::createValueDropdown($id);
        
        if ($this->field == 'tier')
        {
            $this->query->setSQL('SELECT lti_name FROM location_tiers WHERE lti_depth = :depth');
            $this->query->prepareAndExecute(array('depth' => $this->value));
            $description = $this->query->fetch(\PDO::FETCH_COLUMN);
            $dropdown->setDescription($description);
        }
        
        return $dropdown;        
    }    
}