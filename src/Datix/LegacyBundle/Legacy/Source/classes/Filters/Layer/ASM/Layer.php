<?php
namespace Source\classes\Filters\Layer\ASM;

/**
* Layer extended to accomodate Accreditation-specific functionality.
*/
class Layer extends \Source\classes\Filters\Layer\Layer
{
    /**
    * Adds template title to the standard list of fields.
    * 
    * @return array $codes The "field" dropdown codes.
    */
    protected function getFieldCodes()
    {
        $codes = parent::getFieldCodes();
        $codes['atm_title'] = \Fields_Field::getFieldLabel('atm_title');
        
        unset($codes['atm_title_code']); // need to ignore dummy field def added for reporting, as filters handles this differently
        
        // case insensitive sort
        uasort($codes, array($this, 'sortFieldCodes'));   
        
        return $codes;   
    }
    
    /**
     * Sort function called to order "field" dropdown codes.
     * 
     * Factored out here to prevent issues using an anonymous function with the ionCube encryption engine.
     * 
     * @param string $a
     * @param string $b
     * 
     * @return string
     */
    protected function sortFieldCodes($a, $b)
    {
        return \UnicodeString::strcasecmp($a, $b);
    }
    
    /**
    * Retrieves template titles in addition to standard value codes.
    * 
    * @param string $term The search term used to filter the code results.
    * 
    * @return array $codes
    */
    public function getFilterValues($term)
    {
        if ($this->field == 'atm_title')
        {
            $codes[] = $this->getNoValueCode();
            
            $this->query->setSQL('SELECT DISTINCT atm_title AS value, atm_title AS description FROM asm_template_modules WHERE atm_title LIKE :term AND atm_title != \'\'');
            $this->query->prepareAndExecute(array('term' => '%'.$term.'%'));
            $codes = array_merge($codes, $this->query->fetchAll(\PDO::FETCH_ASSOC));
            
            foreach ($codes as $key => $code)
            {
                if (\UnicodeString::strlen($code['description']) > 100)
                {
                    $codes[$key]['description'] = \UnicodeString::substr($code['description'], 0, 100) . '...';    
                }       
            }
            
            return $codes;    
        }
        else
        {
            return parent::getFilterValues($term);
        }
    }
}