<?php
namespace Source\classes\Filters;

use Source\classes\Filters\Filter\Filter;
use Source\classes\Filters\Filter\CQC\Filter as CQCFilter;
use Source\classes\Filters\Filter\LOC\Filter as LOCFilter;
use Source\classes\Filters\Filter\LOC\GenerateInstancesFilter;
use Source\classes\Filters\Layer\Layer;
use Source\classes\Filters\Layer\CQC\Layer as CQCLayer;
use Source\classes\Filters\Layer\LOC\Layer as LOCLayer;
use Source\classes\Filters\Layer\ASM\Layer as ASMLayer;
use Source\classes\Filters\Layer\LayerCollection;

/**
* DI Container for Filter classes.
* 
* @codeCoverageIgnore
*/
class Container
{
    /**
    * Constructs and returns a filter object.
    * 
    * @global array $ModuleDefs
    * @global array $FieldDefs
    * 
    * @param string  $module         The module we're in.
    * @param boolean $populateLayers Whether we need to populate the filter layers from the session.
    * @param string  $sideMenuModule The module to show on the left hand side menu
    * 
    * @return Filter $filter
    */
    public static function getFilter($module, $populateLayers = true, $sideMenuModule = '', $generateInstances = false)
    {
        global $ModuleDefs, $FieldDefs;
        
        $module = \Sanitize::getModule($module);
        $layers = new LayerCollection();
        
        if ($populateLayers)
        {
            static::populateLayers($module, $layers);
        }
        
        switch ($module)
        {
            case 'CQO':    
            case 'CQP':    
            case 'CQS':
                $filter = new CQCFilter($module, $layers, \System_NestedSetMapperFactory::getNestedSetMapper(\System_NestedSetMapperFactory::LOCATIONS), 'cdo_location');
                break;
                
            case 'LOC':
                if ($generateInstances)
                {
                    $filter = new GenerateInstancesFilter($module, $layers, \System_NestedSetMapperFactory::getNestedSetMapper(\System_NestedSetMapperFactory::LOCATIONS), $sideMenuModule);
                }
                else
                {
                    $filter = new LOCFilter($module, $layers, \System_NestedSetMapperFactory::getNestedSetMapper(\System_NestedSetMapperFactory::LOCATIONS), $sideMenuModule);
                }

                break;
                
            default:
                $filter = new Filter($module, $layers);
                break;    
        }
        
        return $filter;
    }
    
    /**
    * Constructs and returns a layer object.
    * 
    * @param string $module
    * @param string $field
    * @param string $value
    * 
    * @return Layer $layer
    */
    public static function getLayer($module, $field, $value = '')
    {
        switch ($module)
        {
            case 'CQO':    
            case 'CQP':    
            case 'CQS':
                $layer = new CQCLayer($module, $field, $value, new \DatixDBQuery(''));
                break;
            
            case 'LOC':
                $layer = new LOCLayer($module, $field, $value, new \DatixDBQuery(''));
                break;
            
            case 'AMO':
            case 'AQU':
            case 'ATI':
            case 'ATM':
            case 'ATQ':
                $layer = new ASMLayer($module, $field, $value, new \DatixDBQuery(''));
                break;                
                
            default:
                $layer = new Layer($module, $field, $value, new \DatixDBQuery(''));
                break;    
        }
        
        return $layer;           
    }
    
    /**
    * Populates the filter layer collection.
    * 
    * @param string          $module
    * @param LayerCollection $layers
    */
    protected static function populateLayers($module, LayerCollection $layers)
    {
        if (is_array($_SESSION[$module]['FILTER']))
        {
            foreach ($_SESSION[$module]['FILTER'] as $layer)
            {
                $layers->addLayer(static::getLayer($module, $layer['field'], $layer['value']));
            }    
        }
    }   
}