<?php
namespace Source\classes\Filters\Filter\LOC;

use Source\classes\Filters\Layer\LayerCollection;

/**
* Filter extended to accomodate filtering location records.
*/
class Filter extends \Source\classes\Filters\Filter\Filter
{
    /**
    * Mapper for the location hierarchy
    * 
    * @var System_NestedSetMapper
    */
    protected $hierarchy;
    
    /**
    * Constructor.
    * 
    * @param string                 $module
    * @param LayerCollection        $layers
    * @param System_NestedSetMapper $hierarchy
    * @param string                 $sideMenuModule
    */
    public function __construct($module, LayerCollection $layers, \System_NestedSetMapper $hierarchy, $sideMenuModule = '')
    {
        parent::__construct($module, $layers, $sideMenuModule);
        $this->hierarchy = $hierarchy;
    }

    protected function buildFilterArray($rawParams)
    {
        global $ModuleDefs;
        
        $validFields = $ModuleDefs[$this->module]['FIELD_ARRAY'];
        $formattedParams = array();
        
        for ($i = 0; $i < $rawParams['filter_max_suffix']; $i++)
        {
            if (in_array($rawParams['filter_field_'.$i], $validFields))
            {
                $formattedParams[$rawParams['filter_field_'.$i]][] = $rawParams['filter_value_'.$i];    
            }
            elseif ($rawParams['filter_field_'.$i] == 'tier' && $rawParams['filter_value_'.$i] != '')
            {
                // the value is a depth in the location hierarchy, so we need to retrieve all nodes at that depth
                $locations = $this->hierarchy->getNodesByDepth($rawParams['filter_value_'.$i]);
                foreach ($locations as $location)
                {
                    $formattedParams['recordid'][] = $location['recordid'];           
                }   
            }
        }
        
        return $formattedParams;    
    }

    /**
    * Constructs an array which represents the current filter design.
    * 
    * Extended to handle selection of a tiers in the location hierarchy as filter values.
    * 
    * @global array $ModuleDefs
    * 
    * @param array $filterParams The parameters (fields/values) which make up the filter, and the number of layers (e.g. array('filter_field_1' => 'field1', 'filter_value_1' => 'value1', 'filter_max_suffix' => '1'))
    * 
    * @return array $filter The filter design as a set of fields/values.
    */
    public function filterDesignToArray($filterParams)
    {
        global $ModuleDefs;
        
        $filter = array();
        $validFields = $ModuleDefs[$this->module]['FIELD_ARRAY'];
        
        for ($i = 0; $i < $filterParams['filter_max_suffix']; $i++)
        {
            if (in_array($filterParams['filter_field_'.$i], $validFields) || $filterParams['filter_field_'.$i] == 'tier')
            {
                $filter[] = array('field' => $filterParams['filter_field_'.$i], 'value' => $filterParams['filter_value_'.$i]);
            }  
        }
        
        return $filter;    
    }
}