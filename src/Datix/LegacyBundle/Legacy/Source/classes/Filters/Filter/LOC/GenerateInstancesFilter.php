<?php

namespace Source\classes\Filters\Filter\LOC;

use Source\classes\Filters\Layer\LayerCollection;

class GenerateInstancesFilter extends \Source\classes\Filters\Filter\Filter
{
    /**
     * Mapper for the location hierarchy
     *
     * @var System_NestedSetMapper
     */
    protected $hierarchy;

    /**
     * Constructor.
     *
     * @param string                 $module
     * @param LayerCollection        $layers
     * @param System_NestedSetMapper $hierarchy
     * @param string                 $sideMenuModule
     */
    public function __construct($module, LayerCollection $layers, \System_NestedSetMapper $hierarchy, $sideMenuModule = '')
    {
        parent::__construct($module, $layers, $sideMenuModule);
        $this->hierarchy = $hierarchy;
    }

    public function createWhereClause($filterParams)
    {
        global $ModuleDefs, $FieldDefs;

        $module = 'LOC';
        $WhereClause = array();
        $descendants = array();

        foreach($filterParams['records'] as $assignedAssessmentId)
        {
            $sql = '
                SELECT
                    ati_location
                FROM
                    asm_templates
                WHERE
                    recordid = :recordid
            ';

            $assignedAssessmentLocationId = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $assignedAssessmentId), \PDO::FETCH_COLUMN);

            $descendants[$assignedAssessmentLocationId] = $this->hierarchy->getDescendants($assignedAssessmentLocationId);

            $WhereClause[$assignedAssessmentLocationId] = '
                vw_locations_main.recordid IN (
                    SELECT
                        vw_locations_main.recordid
                    FROM
                        vw_locations_main
                    WHERE
                        vw_locations_main.lft >= (SELECT lft FROM vw_locations_main WHERE recordid = ' . $assignedAssessmentLocationId . ')
                        AND
                        vw_locations_main.rght <= (SELECT rght FROM vw_locations_main WHERE recordid = ' . $assignedAssessmentLocationId . ')
                )
            ';
        }

        $WhereClauseAux = implode('OR ', $WhereClause);
        $WhereClauseAux = MakeSecurityWhereClause($WhereClauseAux, $module, $_SESSION["initials"]);

        require_once 'Source/libs/ListingClass.php';
        $list_columns = \Listings_ModuleListingDesign::GetListingColumns($module);

        if (is_array($list_columns))
        {
            foreach ($list_columns as $col_name => $col_info)
            {
                if(is_array($list_columns_extra[$col_name]))
                {
                    if(!array_key_exists("condition", $list_columns_extra[$col_name]) || $list_columns_extra[$col_name]["condition"])
                    {
                        $selectfields[$col_name] = $col_name;
                    }
                    else
                    {
                        unset($list_columns[$col_name]);
                    }

                    if(array_key_exists("prefix", $list_columns_extra[$col_name]))
                    {
                        $col_info_extra['prefix'] = $list_columns_extra[$col_name]["prefix"];
                    }

                    if(array_key_exists("dataalign", $list_columns_extra[$col_name]))
                    {
                        $col_info_extra['dataalign'] = $list_columns_extra[$col_name]["dataalign"];
                    }

                    if(array_key_exists("colrename", $list_columns_extra[$col_name]))
                    {
                        $col_info_extra['colrename'] = $list_columns_extra[$col_name]["colrename"];
                    }
                }
                else
                {
                    $selectfields[$col_name] = $col_name;
                }
            }
        }

        if (!in_array('recordid', $selectfields))
        {
            $selectfields[] = 'recordid';
        }

        if (is_array($selectfields))
        {
            $selectfields = implode(", ", $selectfields);
        }

        $sql = 'SELECT ' . $selectfields . ' FROM ' . ($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']) . ' WHERE ' . $WhereClauseAux;
        $result = \DatixDBQuery::PDO_fetch_all($sql, array());

        $CanUseRecordList = false;

        if(isset($_SESSION['LOC']['RECORDLIST']))
        {
            $CanUseRecordList = true;
        }

        if ($CanUseRecordList)
        {
            $_SESSION['LOC']['RECORDLIST']->UnFlagAllRecords();
        }

        foreach ($result as $locationDetails)
        {
            $showLocation = array();

            foreach ($descendants as $descendantsArray)
            {
                if (array_key_exists($locationDetails['recordid'], $descendantsArray))
                {
                    $showLocation[] = true;

                    if ($CanUseRecordList)
                    {
                        $_SESSION['LOC']['RECORDLIST']->FlagRecord($locationDetails['recordid']);
                    }
                }
                else
                {
                    $showLocation[] = false;

                    if ($CanUseRecordList)
                    {
                        $_SESSION['LOC']['RECORDLIST']->UnFlagRecord($locationDetails['recordid']);
                    }
                }
            }

            if (in_array(false, $showLocation))
            {
                if (isset($WhereClause[$locationDetails['recordid']]))
                {
                    unset($WhereClause[$locationDetails['recordid']]);
                }
            }
        }

        $WhereClause = (empty($WhereClause) ? '' : implode('OR ', $WhereClause));

        if(!isset($_SESSION[$module]['RECORDLIST']) || ($WhereClause != $_SESSION[$module]['WHERE']))
        {
            $_SESSION[$module]['RECORDLIST'] = \RecordLists_RecordListShell::CreateForModule($module, $WhereClause);
            $_SESSION[$module]['RECORDLIST']->FlagAllRecords();
        }

        $_SESSION[$module]['WHERE']  = $WhereClause;
    }
}