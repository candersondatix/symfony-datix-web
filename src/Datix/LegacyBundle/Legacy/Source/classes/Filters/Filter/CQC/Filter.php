<?php
namespace Source\classes\Filters\Filter\CQC;

use Source\classes\Filters\Layer\LayerCollection;

/**
* Filter extended to accomodate filtering CQC records using the location hierarchy.
* 
* Could potentially be reused for modules other than CQC if the records are filtered in the same way,
* i.e. selecting a tier in the location hierarchy as a field and a node from that tier as the value; the resulting listing
* contains records linked to leaf nodes in the location hierarchy which are children of the node value selected.
*/
class Filter extends \Source\classes\Filters\Filter\Filter
{
    /**
    * Mapper for the location hierarchy
    * 
    * @var System_NestedSetMapper
    */
    protected $hierarchy;
    
    /**
    * The record's DB field which holds the location id.
    * 
    * @var string
    */
    protected $locationField;
    
    /**
    * Constructor.
    * 
    * @param string                 $module
    * @param LayerCollection        $layers
    * @param System_NestedSetMapper $hierarchy
    * @param string                 $locationField
    * 
    * @throws MissingParameterException
    */
    public function __construct($module, LayerCollection $layers, \System_NestedSetMapper $hierarchy, $locationField)
    {
        if ($locationField == '')
        {
            throw new \MissingParameterException('Missing location field parameter');
        }
        
        parent::__construct($module, $layers);
        $this->hierarchy = $hierarchy;    
        $this->locationField = $locationField;    
    }
    
    /**
    * Formats a set of filter parameters into an array that used to build the filter where clause.
    * 
    * Extended to handle selection of a tiers in the location hierarchy as filter fields.
    * 
    * @global array $ModuleDefs
    *
    * @param array $rawParams The unformatted field names/values that make up the filter, as well as the number of layers (filter_max_suffix).
    * 
    * @return array $formattedParams The formatted (grouped by field) filter parameters.
    */
    protected function buildFilterArray($rawParams)
    {
        global $ModuleDefs;
        
        $validFields = $ModuleDefs[$this->module]['VIEW_FIELD_ARRAY'];
        $formattedParams = array();
        $suffix = 0;
        
        for ($i = 0; $i < $rawParams['filter_max_suffix']; $i++)
        {
            if (in_array($rawParams['filter_field_'.$i], $validFields))
            {
                $formattedParams[$rawParams['filter_field_'.$i]][] = $rawParams['filter_value_'.$i];
            }
            elseif (preg_match('/tier_(\d)/u', $rawParams['filter_field_'.$i]) && $rawParams['filter_value_'.$i] != '')
            {
                // the value is the id of a node in the location hierarchy, so we need to fetch all subordinate leaf nodes
                $locations = $this->hierarchy->getLeafNodes($rawParams['filter_value_'.$i]);
                foreach ($locations as $location)
                {
                    $formattedParams[$this->locationField.'_'.$suffix][] = $location['recordid'];
                }
                $suffix++;
            }
        }
        
        return $formattedParams;
    }
    
    /**
    * Constructs an array which represents the current filter design.
    * 
    * Extended to handle selection of a tiers in the location hierarchy as filter fields.
    * 
    * @global array $ModuleDefs
    * 
    * @param array $filterParams The parameters (fields/values) which make up the filter, and the number of layers (e.g. array('filter_field_1' => 'field1', 'filter_value_1' => 'value1', 'filter_max_suffix' => '1'))
    * 
    * @return array $filter The filter design as a set of fields/values.
    */
    public function filterDesignToArray($filterParams)
    {
        global $ModuleDefs;
        
        $filter = array();
        $validFields = $ModuleDefs[$this->module]['VIEW_FIELD_ARRAY'];
        
        for ($i = 0; $i < $filterParams['filter_max_suffix']; $i++)
        {
            if (in_array($filterParams['filter_field_'.$i], $validFields) || preg_match('/tier_(\d)/u', $filterParams['filter_field_'.$i], $matches))
            {
                $filter[] = array('field' => $filterParams['filter_field_'.$i], 'value' => $filterParams['filter_value_'.$i]);
            }  
        }
        
        return $filter;        
    }
}
