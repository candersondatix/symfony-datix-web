<?php
namespace Source\classes\Filters\Filter;

use Source\classes\Filters\Container;
use Source\classes\Filters\Layer\Layer;
use Source\classes\Filters\Layer\LayerCollection;
use src\framework\query\FieldCollection;
use src\framework\query\Where;

/**
* Enables users to filter listings by defining a set of field/value parings.
*/
class Filter
{
    /**
    * The name of the module we're filtering on.
    *
    * @var string
    */
    protected $module;

    /**
    * A collection of layers (field/value pairs) used in this filter.
    *
    * @var LayerCollection
    */
    protected $layers;

    /**
     * The module to show on the left hand side menu
     *
     * @var string
     */
    protected $sideMenuModule;

    /**
    * Constructor.
    *
    * @param string                 $module
    * @param LayerCollection $layers
    *
    * @throws MissingParameterException
    */
    public function __construct($module, LayerCollection $layers, $sideMenuModule = '')
    {
        if ($module == '')
        {
            throw new \MissingParameterException('Missing module parameter');
        }

        $this->module = \Sanitize::getModule($module);
        $this->layers = $layers;
        $this->sideMenuModule = $sideMenuModule;
    }

    /**
    * Constructs the HTML for the filter.
    * @global Zend_View $DatixView
    * @return string
    * @codeCoverageIgnore
    */
    public function display()
    {
        global $DatixView;

        $this->layers->addLayer($this->createBlankLayer());

        $filterRows = array();
        foreach ($this->layers as $id => $layer)
        {
            $filterRows[] = $layer->display($id, ($id == count($this->layers) - 1 ? Layer::ADD : Layer::DELETE));
        }

        $DatixView->maxSuffix = count($this->layers);
        $DatixView->module = $this->module;
        $DatixView->filterRows = $filterRows;
        $DatixView->filterButtonLabel = _tk('filter_button');
        $DatixView->resetLinkLabel = _tk('filter_reset');
        $DatixView->saveFilterLinkLabel = _tk('filter_save');
        $DatixView->chooseField = addslashes(_tk('filter_choose_field'));
        $DatixView->chooseValue = addslashes(_tk('filter_choose_value'));
        $DatixView->sideMenuModule = $this->sideMenuModule;

        return $DatixView->render('classes/Filters/Filter/Display.php');
    }

    /**
    * Creates the default "blank" layer used for new filters/when adding a new layer.
    * @return Layer
    * @codeCoverageIgnore
    */
    protected function createBlankLayer()
    {
        return Container::getLayer($this->module, _tk('filter_choose_field'), _tk('filter_choose_value'));
    }

    /**
    * returns a new blank layer row - used when adding a row via ajax.
    *
    * @param int    $id     The ID (suffix) for the new layer.
    * @param string $module The module we're in.
    *
    * @return string The new layer HTML.
    *
    * @codeCoverageIgnore
    */
    public function addAnother($id, $module)
    {
        $layer = $this->createBlankLayer($module);
        return $layer->display($id, Layer::ADD);
    }

    /**
    * Constructs the WHERE clause for the filter.
    *
    * @global array $FieldDefs
    *
    * @param array $filterParams The parameters (fields/values) which make up the filter, and the number of layers (e.g. array('filter_field_1' => 'field1', 'filter_value_1' => 'value1', 'filter_max_suffix' => '1'))
    *
    * @return string
    */
    public function createWhereClause($filterParams)
    {
        global $FieldDefs;

        $where = array();
        $fields = $this->buildFilterArray($filterParams);

        $newWhereParams = array();
        
        foreach ($fields as $field => $values)
        {
            // field suffix can be used if we want to create an AND condition between values of the same field
            if (\UnicodeString::substr($field, 0, 3) != 'UDF')
            {
                $field = RemoveSuffix($field);
            }
            
            //need to include table to prevent problems when joining to views with identical columns (e.g. CQC)
            $table = GetTableViewForField($field, $this->module);
            $fieldAndTable = ($table) ? $table.'.'.$field : $field;
            
            $fieldWhere = '';
            foreach ($values as $value)
            {
                if ($fieldWhere != '')
                {
                    // use OR for multiples of the same field
                    $fieldWhere .= ' OR ';
                }

                if (\UnicodeString::substr($field, 0, 3) == 'UDF')
                {
                    $udfParts = explode('_', $field);
                    if ($value == '!NO_VALUE!')
                    {
                        // searching for blank/null values
                        $fieldWhere .= "recordid NOT IN (SELECT cas_id FROM udf_values WHERE field_id = ".intval($udfParts[3])." AND mod_id = ".ModuleCodeToID($this->module).")";
                    }
                    else if ($udfParts[1] == 'T') // multiselect
                    {
                        $fieldWhere .= "recordid IN (SELECT cas_id FROM udf_values WHERE field_id = ".intval($udfParts[3])."  AND mod_id = ".ModuleCodeToID($this->module)." AND ".
                            "(udv_string LIKE '".\EscapeQuotes($value)."' ".
                            "OR udv_string LIKE '% ".\EscapeQuotes($value)."' ".
                            "OR udv_string LIKE '".\EscapeQuotes($value)." %' ".
                            "OR udv_string LIKE '% ".\EscapeQuotes($value)." %'))";        
                    }
                    else
                    {
                        $fieldWhere .= "recordid IN (SELECT cas_id FROM udf_values WHERE field_id = ".intval($udfParts[3]).
                            " AND mod_id = ".ModuleCodeToID($this->module)." AND udv_string = '".\EscapeQuotes($value)."')";        
                    }
                }
                else
                {
                    if ($value == '!NO_VALUE!')
                    {
                        // searching for blank/null values
                        $fieldWhere .= "(".$fieldAndTable." = '' OR ".$fieldAndTable." IS NULL)";
                    }
                    else if ($FieldDefs[$this->module][$field]['Type'] == 'multilistbox')
                    {
                        $fieldWhere .= "(".$fieldAndTable." LIKE '".\EscapeQuotes($value)."' ".
                            "OR ".$fieldAndTable." LIKE '% ".\EscapeQuotes($value)."' ".
                            "OR ".$fieldAndTable." LIKE '".\EscapeQuotes($value)." %' ".
                            "OR ".$fieldAndTable." LIKE '% ".\EscapeQuotes($value)." %')";
                    }
                    else
                    {
                        $fieldWhere .= "(".$fieldAndTable." = '".\EscapeQuotes($value)."')";
                    }   
                }
            }
            $where[] = '('.$fieldWhere.')';
            
            // build field collection for "new" where session object, used for reporting
            $cleanValues = array_diff($values, array('!NO_VALUE!'));
            $conditions  = array((new FieldCollection)->field($fieldAndTable)->in($cleanValues));
            
            if (count($cleanValues) != count($values))
            {
                $conditions[] = (new FieldCollection)->field($fieldAndTable)->isEmpty();
                $conditions[] = (new FieldCollection)->field($fieldAndTable)->isNull();
            }
            
            $newWhereParams[] = array('condition' => 'OR', 'parameters' => $conditions);
        }
        
        $newWhere = new Where();
        $newWhere->addArray($newWhereParams);
        $_SESSION[$this->module]['NEW_WHERE'] = $newWhere;

        return implode(' AND ', $where);
    }

    /**
    * Formats a set of filter parameters into an array that used to build the filter where clause.
    *
    * @global array $ModuleDefs
    *
    * @param array $rawParams The unformatted field names/values that make up the filter, as well as the number of layers.
    *
    * @return array $formattedParams The formatted (grouped by field) filter parameters.
    */
    protected function buildFilterArray($rawParams)
    {
        global $ModuleDefs;

        $validFields = $ModuleDefs[$this->module]['VIEW_FIELD_ARRAY'] ?: $ModuleDefs[$this->module]['FIELD_ARRAY'];
        $formattedParams = array();

        for ($i = 0; $i < $rawParams['filter_max_suffix']; $i++)
        {
            if (in_array($rawParams['filter_field_'.$i], $validFields) || preg_match('/^UDF_[C|T]_0_\d+$/u', $rawParams['filter_field_'.$i]))
            {
                $formattedParams[$rawParams['filter_field_'.$i]][] = $rawParams['filter_value_'.$i];
            }
        }

        return $formattedParams;
    }

    /**
    * Constructs an array which represents the current filter design.
    *
    * @global array $ModuleDefs
    *
    * @param array $filterParams The parameters (fields/values) which make up the filter, and the number of layers (e.g. array('filter_field_1' => 'field1', 'filter_value_1' => 'value1', 'filter_max_suffix' => '1'))
    *
    * @return array $filter The filter design as a set of fields/values.
    */
    public function filterDesignToArray($filterParams)
    {
        global $ModuleDefs;

        $filter = array();
        $validFields = $ModuleDefs[$this->module]['VIEW_FIELD_ARRAY'] ?: $ModuleDefs[$this->module]['FIELD_ARRAY'];

        for ($i = 0; $i < $filterParams['filter_max_suffix']; $i++)
        {
            if (in_array($filterParams['filter_field_'.$i], $validFields) || preg_match('/^UDF_[C|T]_0_\d+$/u', $filterParams['filter_field_'.$i]))
            {
                $filter[] = array('field' => $filterParams['filter_field_'.$i], 'value' => $filterParams['filter_value_'.$i]);
            }
        }

        return $filter;
    }
}
