<?php
namespace Source\classes\LIB;

/**
* Display and functionality of the Manage CQC Links section of the library form.
*/
class ManageCQCLinks
{
    /**
    * Creates and displayes the section.
    * 
    * @global ZendView $DatixView
    * @global array    $ModuleDefs
    * 
    * @param array        $data      The form data.
    * @param string       $formType  The mode the form is in.
    * @param string       $module    The module code (not used, but included because of the way this function is called).
    * @param DatixDBQuery $query     The DB query object.
    */
    public static function display($data, $formType, $module, \DatixDBQuery $query)
    {
        global $DatixView, $ModuleDefs;
        
        $readonly = in_array($formType, array('ReadOnly','Locked','Print'));
        
        $Design = new \Listings_ListingDesign(array('columns' => array(
            new \Fields_DummyField(array('name' => 'link', 'label' => 'CQC Links', 'type' => 'X')),
            new \Fields_DummyField(array('name' => 'unlink', 'label' =>'', 'type' => 'X')),
            )
        ));
        
        // do not show links to records that the user doesn't have access to, either because they have no access 
        // to the module at all, or because their security where clause prevents them from seeing it.
        $accessWhereClauses = array(
            'CQO' => CanSeeModule('CQO') ? '' : '1=2',
            'CQP' => CanSeeModule('CQP') ? '' : '1=2',
            'CQS' => CanSeeModule('CQS') ? '' : '1=2',
        );
        
        $securityWhereClauses = array(
            'CQO' => MakeSecurityWhereClause('', 'CQO', $_SESSION['initials']) ?: $accessWhereClauses['CQO'] ?: '1=1',
            'CQP' => MakeSecurityWhereClause('', 'CQP', $_SESSION['initials']) ?: $accessWhereClauses['CQP'] ?: '1=1',
            'CQS' => MakeSecurityWhereClause('', 'CQS', $_SESSION['initials']) ?: $accessWhereClauses['CQS'] ?: '1=1',
        );
        
        // select all the links to this library record in one go
        // the ordering logic is based on the location hierarchy first, and then the CQC hierarchy as defined by the templates (asssuming the template IDs are in order)
        $sql = "
            SELECT 
                cqo_id, cqp_id, cqs_id, link_recordid,
                'link' =
                    CASE 
                        WHEN cqo_id IS NOT NULL THEN REPLACE((SELECT parent.loc_name AS 'data()' FROM locations_main AS node, locations_main AS parent WHERE node.lft BETWEEN parent.lft AND parent.rght AND node.recordid = (SELECT cdo_location FROM cqc_outcomes WHERE recordid = cqo_id) ORDER BY node.lft FOR XML PATH ('span')), '</span>', '</span> > ') + (SELECT '"._tk("cqc_outcome")." ' + cto_ref FROM cqc_outcomes WHERE recordid = cqo_id)
                        WHEN cqp_id IS NOT NULL THEN REPLACE((SELECT parent.loc_name AS 'data()' FROM locations_main AS node, locations_main AS parent WHERE node.lft BETWEEN parent.lft AND parent.rght AND node.recordid = (SELECT cdo_location FROM cqc_prompts WHERE recordid = cqp_id) ORDER BY node.lft FOR XML PATH ('span')), '</span>', '</span> > ') + (SELECT '"._tk("cqc_outcome")." ' + cto_ref + ' > "._tk("cqc_prompt")." ' + ctp_ref FROM cqc_prompts WHERE recordid = cqp_id)
                        WHEN cqs_id IS NOT NULL THEN REPLACE((SELECT parent.loc_name AS 'data()' FROM locations_main AS node, locations_main AS parent WHERE node.lft BETWEEN parent.lft AND parent.rght AND node.recordid = (SELECT cdo_location FROM cqc_subprompts WHERE recordid = cqs_id) ORDER BY node.lft FOR XML PATH ('span')), '</span>', '</span> > ') + (SELECT '"._tk("cqc_outcome")." ' + cto_ref + ' > "._tk("cqc_prompt")." ' + ctp_ref + ' > "._tk("cqc_subprompt")." ' + cts_ref FROM cqc_subprompts WHERE recordid = cqs_id)
                    END,
                'left' =
                    CASE 
                        WHEN cqo_id IS NOT NULL THEN (SELECT lft FROM locations_main INNER JOIN cqc_outcomes ON cqc_outcomes.cdo_location = locations_main.recordid WHERE cqc_outcomes.recordid = cqo_id)
                        WHEN cqp_id IS NOT NULL THEN (SELECT lft FROM locations_main INNER JOIN cqc_prompts ON cqc_prompts.cdo_location = locations_main.recordid WHERE cqc_prompts.recordid = cqp_id)
                        WHEN cqs_id IS NOT NULL THEN (SELECT lft FROM locations_main INNER JOIN cqc_subprompts ON cqc_subprompts.cdo_location = locations_main.recordid WHERE cqc_subprompts.recordid = cqs_id)
                    END,
                'cto_id' = 
                    CASE 
                        WHEN cqo_id IS NOT NULL THEN (SELECT cqc_outcome_template_id FROM cqc_outcomes WHERE recordid = cqo_id)
                        WHEN cqp_id IS NOT NULL THEN (SELECT cqc_outcome_template_id FROM cqc_prompts WHERE recordid = cqp_id)
                        WHEN cqs_id IS NOT NULL THEN (SELECT cqc_outcome_template_id FROM cqc_subprompts WHERE recordid = cqs_id)
                    END,
                'ctp_id' = 
                    CASE 
                        WHEN cqo_id IS NOT NULL THEN NULL
                        WHEN cqp_id IS NOT NULL THEN (SELECT cqc_prompt_template_id FROM cqc_prompts WHERE recordid = cqp_id)
                        WHEN cqs_id IS NOT NULL THEN (SELECT cqc_prompt_template_id FROM cqc_subprompts WHERE recordid = cqs_id) 
                    END,
                'cts_id' = 
                    CASE 
                        WHEN cqo_id IS NOT NULL THEN NULL
                        WHEN cqp_id IS NOT NULL THEN NULL
                        WHEN cqs_id IS NOT NULL THEN (SELECT cqc_subprompt_template_id FROM cqc_subprompts WHERE recordid = cqs_id) 
                    END 
            FROM LINK_LIBRARY
            WHERE lib_id = :id
            AND (cqo_id IN (SELECT recordid FROM cqc_outcomes WHERE ".$securityWhereClauses['CQO'].") OR cqp_id IN (SELECT recordid FROM cqc_prompts WHERE ".$securityWhereClauses['CQP'].") OR cqs_id IN (SELECT recordid FROM cqc_subprompts WHERE ".$securityWhereClauses['CQS']."))
            ORDER BY 'left', 'cto_id', 'ctp_id', 'cts_id'
        ";
        
        $query->setSQL($sql);
        $query->prepareAndExecute(array('id' => $data['recordid']));
        $links = $query->fetchAll();
        
        $DatixView->unlink = _tk('cqc_links_unlink');
        
        if (!$readonly)
        {
            // create unlink buttons
            foreach ($links as $key => $value)
            {
                $DatixView->disabled = '';
                $DatixView->title = '';
                
                if ($value['link_recordid'] == $data['link_recordid'])
                {
                    // cannot unlink if the link listed here is the main link for the page - you have to use the unlink button at the bottom of the form
                    $DatixView->disabled = 'disabled="disabled"';       
                    $DatixView->title = \Escape::EscapeEntities(_tk('cqc_links_use_main_unlink'));       
                }
                
                if (($value['cqo_id'] != '' && GetParm($ModuleDefs['CQO']['PERM_GLOBAL']) == 'CQO_READ_ONLY') || ($value['cqp_id'] != '' && GetParm($ModuleDefs['CQP']['PERM_GLOBAL']) == 'CQP_READ_ONLY') || ($value['cqs_id'] != '' && GetParm($ModuleDefs['CQS']['PERM_GLOBAL']) == 'CQS_READ_ONLY'))
                {
                    // only full access users can unlink, read-only cannot
                    $DatixView->disabled = 'disabled';       
                    $DatixView->title = \Escape::EscapeEntities(_tk('cqc_links_read_only'));    
                }
                
                $DatixView->linkId = $value['link_recordid'];
                $links[$key]['unlink'] = $DatixView->render('classes/LIB/ManageCQCLinks/UnlinkButton.php');
            }   
        }
        
        $RecordList = new \RecordLists_RecordList();
        $RecordList->AddRecordData($links);
        
        $listing = new \Listings_ListingDisplay($RecordList, $Design);
        
        $DatixView->listing = $listing->GetListingHTML();
        $DatixView->addLink = \Escape::EscapeEntities(_tk('cqc_links_add'));
        $DatixView->unlinkConfirm = \Escape::EscapeEntities(_tk('cqc_links_unlink_confirm'));
        $DatixView->libId = \Sanitize::SanitizeInt($data['recordid']);
        $DatixView->callback = $_SERVER['QUERY_STRING'];
        $DatixView->canAdd = (
            (
                GetParm($ModuleDefs['CQO']['PERM_GLOBAL']) == 'CQO_FULL' || 
                GetParm($ModuleDefs['CQP']['PERM_GLOBAL']) == 'CQP_FULL' || 
                GetParm($ModuleDefs['CQS']['PERM_GLOBAL']) == 'CQS_FULL'
            ) && !$readonly);
        
        if (\UnicodeString::strpos($DatixView->callback, 'panel=') !== false)
        {
            $DatixView->callback = preg_replace('/panel=[a-z_]+/u', 'panel=cqc_links', $DatixView->callback);                
        }
        else
        {
            $DatixView->callback .= '&panel=cqc_links';
        }
        
        $DatixView->callback = addslashes(urlencode(urlencode($DatixView->callback)));
        
        echo $DatixView->render('classes/LIB/ManageCQCLinks/display.php');
    }
    


    /**
    * Creates the library link records.
    * 
    * @param string       $module    The CQC module code.
    * @param array        $locations The recordids of the locations used to determine which instances we're linking to.
    * @param array        $templates The recordids of the templates used to determine which instances we're linking to.
    * @param DatixDBQuery $query The DB query object.
    */
    public static function createCQCLinks($module, array $locations, array $templates, \DatixDBQuery $query)
    {
        switch ($module)
        {
            case 'CQO':
                $table = 'cqc_outcomes';
                $templateField = 'cqc_outcome_template_id';
                $linkField = 'cqo_id';
                break;
                
            case 'CQP':
                $table = 'cqc_prompts';
                $templateField = 'cqc_prompt_template_id';
                $linkField = 'cqp_id';
                break;
                
            case 'CQS':
                $table = 'cqc_subprompts';
                $templateField = 'cqc_subprompt_template_id';
                $linkField = 'cqs_id';
                break;
                
            default:
                return false;
        }
        
        // sanitize the template ids
        $validTemplates = array();
        foreach ($templates as $template)
        {
            if (ctype_digit($template))
            {
                $validTemplates[] = $template;    
            }    
        }
        
        if (!empty($validTemplates))
        {
            // get all records that match the posted template IDs and aren't already linked to this evidence record
            // use the user's security where clause to double-check that the POSTed ids are valid
            $secWhere = MakeSecurityWhereClause('', $module, $_SESSION['initials']);
            
            $sql = 'SELECT recordid
                    FROM '.$table.'
                    LEFT JOIN link_library ON '.$linkField.' = recordid AND lib_id = :lib_id
                    WHERE '.$templateField.' IN ('.implode(',', $templates).')
                    AND cdo_location IN ('.implode(',', $locations).')
                    AND '.$linkField.' IS NULL'.($secWhere != '' ? '
                    AND '.$secWhere : '');
            
            $query->setSQL($sql);
            $query->prepareAndExecute(array('lib_id' => $_POST['lib_id']));
            $records = $query->fetchAll(\PDO::FETCH_COLUMN);
            
            // create a library link for each record 
            foreach ($records as $record)
            {
                $params = array(
                    'updateddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $_SESSION['initials'],
                    'link_type' => 'E',
                    $linkField => $record,
                    'lib_id' => $_POST['lib_id'],
                    'link_notes' => $_POST['link_notes']
                );

                $fields = array_keys($params);
                $query->setSQL('INSERT INTO link_library (' . implode(',', $fields) . ') VALUES (' . implode(',', array_map('addColon', $fields)) . ')');
                $query->insert($params);   
            }   
        }    
    }        
}