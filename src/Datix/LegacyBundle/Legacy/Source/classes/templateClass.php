<?php

/**
* @desc Models a template used to output HTML. At the moment a lot of functionality is still
* handled procedurally, but it can be brought into this class over time.
*/
class Template
{
    //bool: True if no menu div should be drawn
    protected $NoMenu = false;

    //bool: True if no padding required in the main area of the screen.
    protected $NoPadding = false;

    /**
    * @desc Constructor: Assigns parameter values to object properties.
    *
    * @param array $Parameters The array of parameters to be assigned.
    */
    public function __construct($Parameters = array())
    {
        if ($Parameters['no_menu'])
        {
            $this->NoMenu = true;
        }
        if ($Parameters['no_padding'])
        {
            $this->NoPadding = true;
        }
    }

    /**
    * @desc Adds appropriate CSS files to the $dtx_system_css global, which holds the list of css
    * files to be included when the page is output.
    */
    private function addCss()
    {
        global $dtx_system_css, $MinifierDisabled;

        $addMinExtension = ($MinifierDisabled ? '' : '.min');

        if ($this->NoMenu)
        {
            $dtx_system_css .= '
<link rel="stylesheet" type="text/css" href="css/template_css/no-menu' . $addMinExtension . '.css"/>
';
        }
        if ($this->NoPadding)
        {
            $dtx_system_css .= '
<link rel="stylesheet" type="text/css" href="css/template_css/no-padding' . $addMinExtension . '.css"/>
';
        }
    }

    /**
    * @desc Gets the raw HTML (including mergecodes) from the template.php file ready for it to be manipulated.
    */
    public function getTemplateHTML()
    {
        $this->addCss();

        ob_start();
        include("template.php");
        $dtxtemplate = explode("\n", ob_get_contents());
        ob_end_clean();

        return $dtxtemplate;
    }
}
