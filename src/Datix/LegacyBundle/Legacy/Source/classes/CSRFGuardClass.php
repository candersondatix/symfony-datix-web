<?php

require_once __DIR__.'/../libs/Subs.php';

/**
 * @author Edson Medina <emedina@datix.co.uk>
 * Class to deal with CSRF attack prevention
 */

class CSRFGuard
{
    const SESSION_TOKEN_NAME = 'csrf_token';
    
    /**
     * Constants used when adding token to URLs.
     * 
     * @var int
     */
    const URL_WITH_QUERY    = 1;
    const URL_WITHOUT_QUERY = 2;
    const URL_DETECT_QUERY  = 3;

    private $whiteList = array ();

    /**
     * Class constructor
     * @param array $whitelist list of actions that should bypass the validation
     */
    public function __construct (array $whitelist = array ())
    {
        $this->whiteList = $whitelist;
    }

    /**
     * Generates (and stores) a new authentication token
     * @return string the new token hash
     */
    static function generateNewToken ()
    {
        // disabled? return empty token
        if (!bYN(GetParm('CSRF_PREVENTION', 'N'))) {
            return '';
        }

        $token = bin2hex(openssl_random_pseudo_bytes(16));

        self::setToken($token);

        return $token;
    }

    /**
     * Validate authentication. Checks the request type, blacklist and token.
     * @param $action action name
     * @param $token authentication token hash
     * @return bool
     */
    public function validateAuth ($action, $token)
    {
        // disabled? accept auth
        if (!bYN(GetParm('CSRF_PREVENTION', 'N'))) {
            return true;
        }

        if ($this->canThisActionBypassCRSFCheck($action)) {
            return true;
        }

        return ($token == self::getCurrentToken());
    }

    /**
     * Get current stored token hash
     * @return string authentication token hash
     */
    static function getCurrentToken ()
    {
        // not active? return empty token
        if (!bYN(GetParm('CSRF_PREVENTION', 'N'))) {
            return '';
        }

        $token = isset($_SESSION[self::SESSION_TOKEN_NAME]) ? $_SESSION[self::SESSION_TOKEN_NAME] : null;
        
        if (empty($token)) {
            $token = self::generateNewToken();
        }
        return $token;
    }

    /**
     * Set token hash
     * @param string $token auth token hash
     */
    static function setToken ($token)
    {
        $_SESSION[self::SESSION_TOKEN_NAME] = $token;
    }

    /**
     * Checks if this action (or request type) can skip token check
     * @param $action action name
     * @return bool
     */
    private function canThisActionBypassCRSFCheck ($action)
    {
        // if prevention is off bypass check
        if (!bYN(GetParm('CSRF_PREVENTION', 'N'))) {
            return true;
        }

        // White listed actions can skip this
        if (in_array($action, $this->whiteList)) {
            return true;
        }

        return false;
    }
    
    /**
     * Adds the CSRF token suffix to a URL if the application is configured to use CSRF protection.
     * 
     * @param string $url       The initial URL string.
     * @param int    $separator Used to define which query string separator to use, depending on whether or not the
     *                          URL already contains query string parameters.
     * 
     * @return string The URL with the token added
     */
    public static function addTokenToURL($url, $separator = self::URL_DETECT_QUERY)
    {
        if (bYN(GetParm('CSRF_PREVENTION', 'N')))
        {
            switch ($separator)
            {
                case self::URL_WITH_QUERY:
                    $sep = '&';
                    break;
                    
                case self::URL_WITHOUT_QUERY:
                    $sep = '?';
                    break;
                    
                case self::URL_DETECT_QUERY:
                    // there is a performance overhead on parsing the URL, so it's better to explicitly
                    // define the separator if possible, especially when dealing with many URLs in one request
                    $sep = (parse_url($url, PHP_URL_QUERY) == null) ? '?' : '&';
                    break;
                    
                default:
                    throw new CSRFException('Invalid URL separator constant');
            }
            
            $url .= $sep.'token='.static::getCurrentToken();
        }
        
        return $url;
    }
}