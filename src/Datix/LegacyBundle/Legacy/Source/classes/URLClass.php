<?php
class URL
{
    protected $File;
    protected $QueryStringArray;
    protected $Root;

    public function __construct()
    {
        $ScriptNameExplode = explode("/", $_SERVER['SCRIPT_NAME']);
        $this->File = array_pop($ScriptNameExplode);

        $this->Root = $_SERVER['HTTP_HOST'].implode("/", $ScriptNameExplode);

        $QueryString = explode('&', Sanitize::SanitizeString($_SERVER['QUERY_STRING']));

        foreach($QueryString as $KeyVal)
        {
            $KeyValPair = explode('=', $KeyVal);
            $this->QueryStringArray[$KeyValPair[0]] = $KeyValPair[1];
        }
    }

    public function AddToQueryString($key, $val)
    {
        $this->QueryStringArray[$key] = $val;
    }

    protected function implodeQueryString()
    {
        foreach($this->QueryStringArray as $key => $val)
        {
            $QString[] = $key.'='.$val;
        }

        return implode('&', $QString);
    }

    public function GetURL()
    {
        return 'http://'. urlencode($this->Root) .'/'. urlencode($this->File) .'?'.$this->implodeQueryString();
    }
}
