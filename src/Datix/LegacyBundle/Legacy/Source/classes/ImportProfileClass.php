<?php

/**
* @desc Object representing an import profile as stored in the excel_profile table
*/
class ImportProfile
{

    protected $ID;
    protected $Form;
    protected $Module;
    protected $Name;
    protected $StartAt;
    protected $RowHeader;
    protected $Type;

    /**
    * @desc Object of type DatixDBQuery - used to connect to the database. Passed in to allow for unit test replacement.
    */
    protected $Query;

    protected $ColumnConfig = array();

    /**
    * @desc Constructor - pulls out profile data and sets up an array of column objects.
    *
    * @param int $recordid The id of the profile in the excel_profile table.
    * @param string $module The module that this profile relates to - will only be provided when building a new profile in the web.
    */
    public function __construct(DatixDBQuery $Query, $recordid = '', $module = '')
    {
        $this->Query = $Query;

        if($recordid)
        {
            $Data = DatixDBQuery::PDO_fetch('SELECT exp_profile_form, exp_profile_name, exp_start_row, exp_profile_type, exp_profile_rowheader FROM excel_profile WHERE exp_profile_id = :recordid', array('recordid' => $recordid), null, $this->Query);

            $this->ID = $recordid;
            $this->Form = $Data['exp_profile_form'];
            $this->Name = $Data['exp_profile_name'];
            $this->StartAt = $Data['exp_start_row'];
            $this->Type = ($Data['exp_profile_type'] ? $Data['exp_profile_type'] : 'EXC');
            $this->RowHeader = $Data['exp_profile_rowheader'];
        }
        else if($module)
        {
            $this->Module = $module;
        }

        $Columns = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM excel_import WHERE exc_profile = :profile ORDER BY exc_import_order', array('profile' => $this->ID), PDO::FETCH_COLUMN, $this->Query);

        if(is_array($Columns))
        {
            foreach($Columns as $ColumnRecordid)
            {
                $this->ColumnConfig[] = new ImportProfileColumn($ColumnRecordid);
            }
        }
    }

    public function getID()
    {
        return $this->ID;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getType()
    {
        return $this->Type;
    }

    public function getStartAt()
    {
        return $this->StartAt;
    }

    public function getRowHeader()
    {
        return $this->RowHeader;
    }

    /**
    * @desc Returns the "form" that this profile relates to. If there is no form defined, can take the form from a provided module.
    */
    public function getForm()
    {
        global $ModuleDefs;

        if(!$this->Form && $this->Module)
        {
            $this->Form = $ModuleDefs[$this->Module]['TABLE'];
        }

        return $this->Form;
    }

    /**
    * @desc Returns the module that this profile relates to. Since the form rather than the module is stored, we will often need to convert
    * this on the fly.
    */
    public function getModule()
    {
        global $ModuleDefs;

        if(!$this->Module)
        {
            foreach($ModuleDefs as $Module => $Details)
            {
                if($Details['TABLE'] == $this->getForm())
                {
                    $this->Module = $Module;
                }
            }
        }

        return $this->Module;
    }

    public function getColumnConfig()
    {
        return $this->ColumnConfig;
    }

    /**
    * @desc Returns an array of data values useful when displaying the edit profile form.
    */
    public function getDataArray()
    {
        $Data['exp_profile_form'] = $this->getForm();
        $Data['exp_profile_module'] = $this->getModule();
        $Data['exp_profile_id'] = $this->ID;
        $Data['exp_profile_name'] = $this->Name;
        $Data['exp_start_row'] = $this->StartAt;
        $Data['exp_profile_type'] = $this->Type;
        $Data['exp_profile_rowheader'] = $this->RowHeader;

        return $Data;
    }

    /**
    * @desc Returns a column object based on the unique identifier for that column (Excel column letter or CSV column number).
    */
    public function getColumnFromKey($key)
    {
        foreach($this->ColumnConfig as $Column)
        {
            if($Column->Data['exc_col_letter'] == $key)
            {
                return $Column;
            }
        }

        return false;
    }

    /**
    * @desc Returns an array of column identifiers keyed with an order
    */
    public function getColumnOrderArray()
    {
        $UnorderedCols = array();
        $OrderArray = array();

        foreach($this->ColumnConfig as $Column)
        {
            if($Column->Data['exc_import_order'])
            {
                $OrderArray[intval($Column->Data['exc_import_order'])] = $Column->Data['exc_col_letter'];
            }
            else
            {
                $UnorderedCols[] = $Column;
            }
        }

        foreach($UnorderedCols as $Column)
        {
            if(empty($OrderArray))
            {
                $OrderArray[1] = $Column->Data['exc_col_letter'];
            }
            else
            {
                $OrderArray[max(array_flip($OrderArray))+1] = $Column->Data['exc_col_letter'];
            }
        }
        return $OrderArray;
    }

    public function setName($value)
    {
        $this->Name = $value;
    }

    public function setForm($value)
    {
        $this->Form = $value;
    }

    public function setStartAt($value)
    {
        $this->StartAt = $value;
    }

    public function setType($value)
    {
        $this->Type = $value;
    }

    public function setRowHeader($value)
    {
        $this->RowHeader = $value;
    }

    public function SaveToDB()
    {
        if(!$this->ID)
        {
            //need to insert a new record
            $this->ID = GetNextRecordID('excel_profile', true, 'exp_profile_id');
        }

        $Data['exp_profile_form'] = $this->getForm();
        $Data['exp_profile_id'] = $this->ID;
        $Data['exp_profile_name'] = $this->Name;
        $Data['exp_start_row'] = $this->StartAt;
        $Data['exp_profile_type'] = $this->Type;
        $Data['exp_profile_rowheader'] = $this->RowHeader;

        $this->Query->setSQL('UPDATE excel_profile SET exp_profile_name = :exp_profile_name, exp_profile_form = :exp_profile_form, exp_start_row = :exp_start_row, exp_profile_type = :exp_profile_type, exp_profile_rowheader = :exp_profile_rowheader WHERE exp_profile_id = :exp_profile_id');
        $this->Query->prepareAndExecute($Data);
    }


    public function DeleteFromDB()
    {
        if($this->ID)
        {
            $this->Query->beginTransaction();

            try
            {
                //delete data transformations
                $cols = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM excel_import WHERE exc_profile = :exc_profile', array('exc_profile' => $this->ID), PDO::FETCH_COLUMN, $this->Query);

                if(!empty($cols))
                {
                    $InClause = DatixDBQuery::getINforPDO($cols);
                    DatixDBQuery::PDO_query('DELETE FROM excel_data_trans WHERE edt_import_field_id '.$InClause['SQL'], $InClause['Parameters'], $this->Query);

                    //delete data columns
                    DatixDBQuery::PDO_query('DELETE FROM excel_import WHERE exc_profile = :exc_profile', array('exc_profile' => $this->ID), null, $this->Query);
                }

                //delete profile
                DatixDBQuery::PDO_query('DELETE FROM excel_profile WHERE exp_profile_id = :exp_profile_id', array('exp_profile_id' => $this->ID), null, $this->Query);

            }
            catch(Exception $e)
            {
                $this->Query->rollBack();
                throw $e;
            }

            $this->Query->commit();
            return true;
        }

        throw new NoIDProvidedException('ID required');

    }


    function OutputAsXML()
    {
        global $DatixView;

        header("Content-Type: text/xml");
        header("Content-Disposition: attachment; filename=". $this->Name .".xml");
        header("Content-Encoding: none");

        $DatixView->ImportProfile = $this;
        echo '<?xml version="1.0" encoding="UTF-8"?>
';
        echo $DatixView->render('setups/ImportProfileClass/OutputAsXML.php'); //identify the particular view file you want to use and generate the output.
        exit;
    }
}
