<?php

/**
 * Represents a button in a stepped navigation UI element
 */
class StepButton extends Button
{
    /**
     * Whether the button is active or disabled
     *
     * @var boolean
     */
     private $_active;

     /**
      * Destination url
      *
      * @var string
      */
     private $_href;

    /**
     * Constructor
     *
     * @param string href Destination url for the button
     * @param array Construction options. See parent class
     * @param boolean Whether the button is active
     */
    function __construct($href, array $params = array(), $active = true)
    {
        $this->_href = $href;
        $this->_active = (bool) $active;

        parent::__construct($params);
    }

    /**
     * Returns the HTML used to render this button on a record details page
     *
     * @return string
     */
    public function getHTML()
    {
        $return = '';

        if ($this->_active) {
            $return = '<a href="' . $this->_href . '" title="' . $this->Label . '"><img style="width:22px; height:22px;" class="' . $this->Action . '" src="Images/nav_' . $this->Action . '.png" alt="' . $this->Action . ' record" /></a>';
        } else {
            $return = '<span title="' . $this->Label . ' (disabled)"><img style="width:22px; height:22px;" class="' . $this->Action . '" src="Images/nav_' . $this->Action . '_disabled.png" alt="' . $this->Action . ' record (disabled)" /></span>';
        }

        return $return;
    }

    /**
     * Renders the HTML used in a floating menu
     *
     * @return string
     */
    public function getSideMenuHTML()
    {
        if (!$this->_active) {
            return '';
        }

        $return = '';

        $return .= '<a href="' . $this->_href . '" title="' . $this->Label . '" id="icon_link_' . $this->ID . '">';
        $return .= '<img src="' . $this->getIcon() . '" alt="' . $this->Label . '" id="icon_' . $this->ID . '" class="side-menu-icon"/>';
        $return .= '</a>';

        return $return;
    }

    /**
     * Returns the relative path to the icon that should be used to represent this button
     *
     * @return string|void
     */
    public function getIcon()
    {
        if ($this->Icon) {
            return $this->Icon;
        }

        switch (\UnicodeString::strtolower($this->Action)) {
            case 'first':
                return 'Images/icons/toolbar-first.png';
                break;

            case 'prev':
                return 'Images/icons/toolbar-prev.png';
                break;

            case 'next':
                return 'Images/icons/toolbar-next.png';
                break;

            case 'last':
                return 'Images/icons/toolbar-last.png';
                break;
        }
    }
}
