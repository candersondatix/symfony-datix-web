<?php

/**
 * @desc Class which can be used for sanitizing data for security checks etc.
 */
class Sanitize
{
    /**
    * Static method to check if data is numeric and returns the data cast as an int
    * 
    * @param int $IntToCheck Data to cast as int
    * 
    * return int
    */
    static public function SanitizeInt($IntToCheck)
    {
        return is_numeric($IntToCheck) ? (int) $IntToCheck : "";   
    }
        
    /**
     * Wrapper function for htmlentities, but always encodes once.
     * 
     * @param string $string The input string. 
     * @param int $quote_style quote_style Constant
     * @param string $charset
     */
    static public function htmlentities_once($string, $quote_style = ENT_COMPAT, $charset = "UTF-8")
    {
        return htmlspecialchars($string, $quote_style, $charset, false);    
    }
    
    /**
    * Sanitize function for FilePaths. (In dev. Extra logic can be added)
    * 
    * @param string $File File path to sanitize.
    * @return string File path
    */
    static public function SanitizeFilePath($File)
    {
        $CleanFileName = filter_var($File, FILTER_SANITIZE_URL);
        return $CleanFileName;  
    }
    
    /**
     * Sanitize function for strings
     * Strip tags, optionally strip or encode special characters.
     * 
     * @param string $string data to sanitize.
     * @param string $options Filter options applied to filter_var
     * @return mixed Sanitized string
     */
    static public function SanitizeString($string, $options = "")
    {
        if (is_null($string))
        {
            return null;
        }
        else
        {
            return filter_var($string, FILTER_SANITIZE_STRING, $options);
        }
    }

    
    /**
     * Sanitize function for string arrays
     * 
     * @param array $StringArray array data to sanitize.
     * @return mixed Sanitized array
     */

    static public function SanitizeStringArray($StringArray)
    {
        foreach($StringArray as $key => $val)
        {
            if(is_array($val))
            {
                $StringArray[$key] = Sanitize::SanitizeStringArray($val);
            }
            else
            {
                $StringArray[$key] = Sanitize::SanitizeString($val);
            }
        }
        return $StringArray;
    }
      
	static public function getModule($moduleName)
	{
		//exception needed for module groups, which are not handled as standard modules.
        $ModuleGroups = GetModuleGroups();
        if(in_array($moduleName, array_keys($ModuleGroups)))
        {
            return $moduleName;
        }

        $modules = GetModuleCodeIdList();
		$modules = array_flip($modules);
	
	  	$key = array_search($moduleName, $modules);
	  	if ($key !== false)
	  	{
	  		return $modules[$key];
	  	}
	}
    
    /**
     * Sanitize function for using FILTER_UNSAFE_RAW filter.
     * To be used when we need to prevent tags and quotes being stripped from the data unless option passed. 
     * Do nothing, optionally strip or encode special characters.
     * 
     * @param string $string data to sanitize.
     * @param int $options Filter options applied to filter_var
     * @return sanitized string
     * @deprecated
     */
    static public function SanitizeRaw($string, $options = "")
    {
        return $string;
    }
    
    /**
     * Sanitize function for RAW arrays
     * 
     * @param string $RawArray array data to sanitize.
     * @return sanitized array
     * @deprecated
     */
    static public function SanitizeRawArray($RawArray)
    {
        return $RawArray;
    }
    
    /**
    * Sanitize function for Email addresses.
    * Remove all characters except letters, digits and !#$%&'*+-/=?^_`{|}~@.[]
    * 
    * @param string $EmailAddress E-mail address to sanitize.
    * @return sting E-mail address
    */
    static public function SanitizeEmail($EmailAddress)
    {
        // FIXME: I believe this method should be refactored to throw an error
        // when the email is not valid (or contains non-valid characters), instead
        // of simply removing those characters as it currently does

        return filter_var($EmailAddress, FILTER_SANITIZE_EMAIL); 
    }
    
    /**
    * Sanitize function for URL.
    * Remove all characters except letters, digits and $-_.+!*'(),{}|\\^~[]`<>#%";/?:@&=. 
    * 
    * @param string $EmailAddress URL to sanitize.
    * @return sting URL
    */
    static public function SanitizeURL($Url)
    {
        // FIXME: I believe this method should be refactored to throw an error
        // when the email is not valid (or contains non-valid characters), instead
        // of simply removing those characters as it currently does

        return filter_var($Url, FILTER_SANITIZE_URL);
    }
}
  
