<?php
/**
* @desc Models a form button whether in the form of an icon on the floating menu to the left of the screen
* or an actual HTML button at the bottom of a form.
*/
class Button
{
    // The contents of the "id" attribute of the HTML button
    protected $ID;

    // The contents of the "name" attribute of the HTML button
    protected $Name;

    // The label for the HTML button and the alt text for the icon
    protected $Label;

    // The javascrpt to be executed when the button or icon is clicked
    protected $OnClickJS;

    // The image to use for the icon (not required: if not provided, will default to a generic image).
    protected $Icon;

    //general "type" of action being performed. Used as a quick way of deciding on default behaviour/appearance
    protected $Action;

    //specific input type
    protected $Type;

    /**
    * @desc Constructor: Assigns parameter values to object properties.
    *
    * @param array $Parameters The array of parameters to be assigned.
    */
    public function __construct($Parameters = array())
    {
        $this->ID = $Parameters['id'];
        $this->Name = $Parameters['name'];
        $this->Label = $Parameters['label'];
        $this->OnClickJS = $Parameters['onclick'];
        $this->Icon = $Parameters['icon'];
        $this->Action = $Parameters['action'];
        $this->Type = $Parameters['type'] ?: 'button';
    }

    /**
    * @desc Gets the HTML for an HTML button defined by this object.
    *
    * @return string The HTML for the button.
    */
    public function getHTML()
    {
        return '<input type="'.$this->Type.'" value="'.$this->Label.'" border="10" class="button" name="'.$this->Name.'" id="'.$this->ID.'"' . ($this->OnClickJS ? ' onClick="'.$this->OnClickJS.'"' : '') .'>';
    }

    /**
    * @desc Gets the HTML for a clickable icon defined by this object.
    *
    * @return string The HTML for the icon.
    */
    public function getSideMenuHTML()
    {
        if ($this->getIcon())
        {
        	return '<a href="'.(\UnicodeString::strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false ? '#' : 'Javascript:void(0);').'" title="'.$this->Label.'" name="icon_link_'.$this->Name.'" id="icon_link_'.$this->ID.'"><img src="'.$this->getIcon().'" alt="'.$this->Label.'" onclick="'.$this->OnClickJS.'" name="icon_'.$this->Name.'" id="icon_'.$this->ID.'" class="side-menu-icon"/></a>';
        } else {
        	return '';
        }
    }

    /**
    * @desc Gets the location of the image file to be used for this button when displayed as an icon.
    *
    * @return string The location of the image.
    */
    protected function getIcon()
    {
        //manual override
        if ($this->Icon)
        {
            return $this->Icon;
        }

        //image selected according to the general type of action the button performs.
        switch ($this->Action)
        {
            case 'SAVE':
                return 'images/icons/icon_footer_save.png';
                break;
            case 'SEARCH':
                return 'images/icons/icon_footer_search.png';
                break;
            case 'DELETE':
                return 'images/icons/icon_footer_delete.png';
                break;
            case 'BACK':
                return 'images/icons/icon_footer_back.png';
                break;
            case 'CANCEL':
                return 'images/icons/icon_footer_cancel.png';
                break;
            default:
            	return '';
        }
    }
}

