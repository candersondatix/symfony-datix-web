<?php

class ImportProfileColumnMapping
{

    protected $ID;
    public $Data;
    protected $OldVal;
    protected $NewVal;

    protected $MappingConfig;

    public function __construct($recordid)
    {
        $this->ID = $recordid;

        $this->Data = DatixDBQuery::PDO_fetch(
            'SELECT
            edt_old_string_val, edt_new_string_val,
            edt_old_text_val, edt_new_text_val,
            edt_old_number_val, edt_new_number_val,
            edt_old_date_val, edt_new_date_val
            FROM excel_data_trans
            WHERE recordid = :recordid', array('recordid' => $this->ID));

        if($this->Data['edt_old_string_val'] != '')
        {
            $this->OldVal = $this->Data['edt_old_string_val'];
            $this->NewVal = $this->Data['edt_new_string_val'];
        }
        else if($this->Data['edt_old_text_val'] != '')
        {
            $this->OldVal = $this->Data['edt_old_text_val'];
            $this->NewVal = $this->Data['edt_new_text_val'];
        }
        else if($this->Data['edt_old_number_val'] != '')
        {
            $this->OldVal = $this->Data['edt_old_number_val'];
            $this->NewVal = $this->Data['edt_new_number_val'];
        }
        else if($this->Data['edt_old_date_val'] != '')
        {
            $this->OldVal = $this->Data['edt_old_date_val'];
            $this->NewVal = $this->Data['edt_new_date_val'];
        }
    }

    public function getOldVal()
    {
        return $this->OldVal;
    }

    public function getNewVal()
    {
        return $this->NewVal;
    }
}
