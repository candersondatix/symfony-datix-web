<?php
// Thrown when querying for an action chain which doesn't exist.
class ActionChainNotExistException extends Exception{}