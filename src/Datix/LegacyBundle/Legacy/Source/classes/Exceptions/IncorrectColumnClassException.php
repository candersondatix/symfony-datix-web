<?php
// Thrown when constructing a listing if the wrong type of object is passed in for a column.
class IncorrectColumnClassException extends Exception{}