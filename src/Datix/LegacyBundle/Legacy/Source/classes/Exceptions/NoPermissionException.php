<?php
class NoPermissionException extends Exception {

    public $userVisible = true;
}