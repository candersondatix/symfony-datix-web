<?php
// Thrown when the system is incorrectly configured
class InvalidConfigException extends Exception{}