<?php
// Thrown when invalid data is provided.
class InvalidDataException extends Exception{}