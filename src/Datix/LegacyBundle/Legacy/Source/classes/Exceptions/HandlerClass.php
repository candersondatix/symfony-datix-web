<?php

use src\framework\registry\Registry;

class Exceptions_Handler
{
    /**
    * @desc Handles any exceptions that get through other error-catching.
    *
    * @param Exception $e The exception object that was just thrown.
    */
    static function handle(Exception $e)
    {
        global $dtxdebug;

        Registry::getInstance()->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');

        switch (get_class($e))
        {
            case 'URLNotFoundException':
                header("HTTP/1.0 404 Not Found");
                fatal_error('404: Not Found');
                break;
            
            default:
                if ($dtxdebug)
                {
                    $ErrorArray = Exceptions_Handler::getTraceArray($e->getTrace());
                    array_unshift($ErrorArray,  "A system error has occurred: ".$e->getMessage()." in ".$e->getFile()." line ".$e->getLine());
                    fatal_error($ErrorArray);
                }
                else
                {
                    // default message
                    $msg = _tk('system_error');

                    // if exception has userVisible flag, display it
                    if (!empty($e->userVisible)) {
                        $msg = $e->getMessage();
                    }
                    
                    fatal_error($msg);
                }
                break;
        }
    }

    static function getTraceArray($trace)
    {
        $ErrorArray = array();

        foreach($trace as $traceelement)
        {
            $ErrorHTML = $traceelement['file'].' ('.$traceelement['line'].') ';

            if($traceelement['class'])
            {
                $ErrorHTML .= $traceelement['class'].' '.$traceelement['type'];
            }

            $ErrorHTML .= $traceelement['function'];

            $ErrorArray[] = $ErrorHTML;
        }

        return $ErrorArray;
    }
}