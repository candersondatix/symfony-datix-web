<?php
// Thrown when an expected parameter is invalid.
class InvalidParameterException extends Exception{}