<?php
// Thrown when expected metadata is missing.
class MissingMetaDataException extends Exception{}