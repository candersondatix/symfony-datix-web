<?php
// Thrown when an expected parameter is missing.
class MissingParameterException extends Exception{}