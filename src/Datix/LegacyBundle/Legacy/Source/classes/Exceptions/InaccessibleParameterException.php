<?php
// Thrown when a parameter required for a method is missing.
class InaccessibleParameterException extends Exception{}