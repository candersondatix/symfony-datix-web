<?php
// Thrown when an expected parameter is missing.
class IncorrectFileTypeException extends Exception{}