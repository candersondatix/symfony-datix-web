<?php

/**
* @desc Object holding information about a generic data record.
*/
class Records_Record
{
    public $Data = array();
    public $RowNumber = 1;

    public function __construct($Data)
    {
        global $ModuleDefs;

        $this->PopulateData($Data);
    }

    protected function PopulateData($Data)
    {
        $this->Data = $Data;
    }

    public function CalculateRowNumber()
    {
        return true;
    }

    public function populateListingArray(&$ListingArray, $ColumnList)
    {
        foreach($ColumnList as $Column)
        {
            $ListingArray[$Column->GetTable().'.'.$Column->GetName()][] = clone($this);
        }
    }
}
