<?php

/**
* @desc Object holding information about a generic data record containing linked data as other record objects.
*/
class Records_NestedRecord extends Records_Record
{
    public $Data = array();
    public $LinkedData = array(); //contains record objects
    public $Table;
    public $TableStructure = '';

    public $RowNumber; //used for listings
    public $ListingProgress = 0; //used for listings

    public function __construct($Table, $TableStructure)
    {
        global $ModuleDefs;

        $this->Table = $Table;
        $this->TableStructure = $TableStructure;

        //$this->Data = $Data;
    }

    public function PopulateFromMultipleTables($Data)
    {
        foreach($Data as $DataToMerge)
        {
            $this->AddToRecord($DataToMerge);
        }
    }

    /**
    * @desc Takes a flat record containing linked data and adds it to the appropriate nested level of this record.
    */
    public function AddToRecord($DataToMerge)
    {
        foreach($DataToMerge as $Field => $Value)
        {
            if(count($FieldParts = explode('.', $Field)) > 1) //contains a table definition
            {
                if($Value)
                {
                    $TableData[$FieldParts[0]][$FieldParts[1]] = $Value;
                }

                if($ModuleDefs[$this->Module]['TABLE'] == $FieldParts[0]) //main table data.
                {


                }
                else //linked data
                {

                }
            }
            else
            {
                //need to check the table.
            }
        }


        if(is_array($TableData[$this->TableStructure->BaseTable]))
        {
            foreach($TableData[$this->TableStructure->BaseTable] as $Key => $Val)
            {
                $this->Data[$Key] = $Val;
            }
        }

        if(is_array($this->TableStructure->TableArray['children']))
        {
            foreach($this->TableStructure->TableArray['children'] as $Table => $Children)
            {
                if($TableData[$Table])
                {
                    $TableIndex = DatixDBQuery::PDO_fetch('SELECT TDR_INDEX FROM TABLE_DIRECTORY WHERE TDR_NAME = :Table', array('Table' => $Table), PDO::FETCH_COLUMN);

                    if(!isset($this->LinkedData[$Table][$TableData[$Table][$TableIndex]])) //linked record doesn't exise
                    {
                        $newRecord = new Records_NestedRecord($Table, $this->TableStructure->getSubStructure($Table));
                        $newRecord->AddToRecord($DataToMerge);
                        $this->LinkedData[$Table][$TableData[$Table][$TableIndex]] = $newRecord;
                    }
                    else
                    {
                        $this->LinkedData[$Table][$TableData[$Table][$TableIndex]]->AddToRecord($DataToMerge);
                    }
                }
            }
        }

    }

    public function SplitStructureIntoLevels($StructArray, $level, $result)
    {
        if(is_array($StructArray['children']))
        {
            foreach($StructArray['children'] as $Table => $ChildArrays)
            {
                $result[$level][] = $Table;
                $result = $this->SplitStructureIntoLevels($ChildArrays, $level+1, $result);
            }
        }

        return $result;
    }

    /**
    * @desc Works out the number of records contained in this record - i.e the number of rows that will be needed to
    * be spanned in a Listing Table.
    */
    public function CalculateRowNumber()
    {
        if(empty($this->LinkedData))
        {
            $this->RowNumber = 1;
            return 1;
        }
        else
        {
            foreach($this->LinkedData as $Table => $Records)
            {
                $RowNumber[$Table] = 0;
                foreach($Records as $Record)
                {
                    if(!$Record->RowNumber)
                    {
                        $Record->CalculateRowNumber();
                    }

                    $RowNumber[$Table] += $Record->RowNumber;
                }
            }

            $this->RowNumber = max($RowNumber);
        }
    }

    /**
    * @desc Creates an array containing an array of objects for each column in an array of columns.
    * Used when building listings.
    */
    public function populateListingArray(&$ListingArray, $ColumnList)
    {
        foreach($ColumnList as $Column)
        {
            if($Column->GetTable() == $this->Table)
            {
                $ListingArray[$Column->GetTable().'.'.$Column->GetName()][] = clone($this);
            }
        }

        if(is_array($this->LinkedData))
        {
            foreach($this->LinkedData as $Table => $Records)
            {
                foreach($Records as $Record)
                {
                    $Record->populateListingArray($ListingArray, $ColumnList);
                }
            }
        }
    }

    public function getRecordValues($field)
    {
        $CurrentRecords = array($this);

        if($this->Table != $field->getTable())
        {
            if(is_array($this->TableStructure->Map[$field->getTable()]))
            {
                foreach($this->TableStructure->Map[$field->getTable()] as $Step)
                {
                    $NextStepRecords = array();
                    foreach($CurrentRecords as $CurrentRecord)
                    {
                        if(is_array($CurrentRecord->LinkedData[$Step]))
                        {
                            foreach($CurrentRecord->LinkedData[$Step] as $NextStepRecord)
                            {
                                $NextStepRecords[] = $NextStepRecord;
                            }
                        }
                    }

                    $CurrentRecords = $NextStepRecords;
                }
            }
        }

        foreach($CurrentRecords as $CurrentRecord)
        {
            $ValueArray[] = $CurrentRecord->Data[$field->getName()];
        }

        return $ValueArray;


    }



}
