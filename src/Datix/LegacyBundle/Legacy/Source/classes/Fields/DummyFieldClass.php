<?php

/**
* @desc Object representing a dummy field (e.g. one without a database table)
*/
class Fields_DummyField
{
    protected $Name;
    protected $Type;
    protected $Label;

    protected $CodeArray;
    protected $CodeField;

    protected $Value;
    protected $Description;
    protected $Colour;

    protected $Width;
    
    /**
     * Defines the controller action responsible for rendering the output for this field.
     * 
     * @var array
     */
    protected $action;

    public function __construct($Parameters)
    {
        $this->Name = $Parameters['name'];
        $this->Type = $Parameters['type'];
        $this->Label = $Parameters['label'];
        $this->CodeArray = $Parameters['codes'];
        $this->CodeField = $Parameters['codefield'];
        $this->Width = $Parameters['width'];
    }

    public function getName()
    {
        return \UnicodeString::strtolower($this->Name);
    }

    public function getFieldType()
    {
        return $this->Type;
    }

    public function getLabel()
    {
        return $this->Label;
    }

    public function getDescription()
    {
        return $this->Description;
    }

    function setValue($value)
    {
        $this->Value = $value;
        $this->Description = NULL;
        $this->Colour = NULL;
    }

    /**
    * @desc Calculates and populates the "Description" and "Colour" properties with display-ready values.
    * 
    * @global array $txt
    */
    function getDisplayInfo()
    {
        if ($this->Value == '')
        {
            return '';
        }

        if ($_SESSION['CachedValues']['code_description']['custom'][$this->getName()][$this->Value])
        {
            $this->Description = $_SESSION['CachedValues']['code_description']['custom'][$this->getName()][$this->Value];
            $this->Colour = $_SESSION['CachedValues']['code_colour']['custom'][$this->getName()][$this->Value];
        }
        else
        {
            switch($this->getFieldType())
            {
                case 'C':
                    if($this->CodeArray[$this->Value])
                    {
                        $this->Description = $this->CodeArray[$this->Value];
                    }
                    else if ($this->CodeField)
                    {
                        $CodeInfo = get_code_info(getModuleFromField($this->CodeField), $this->CodeField, $this->Value);
                        $this->Description = $CodeInfo['description'];
                        $this->Colour = $CodeInfo['cod_web_colour'];
                    }
                    else
                    {
                        $this->Description = $this->Value;
                    }
                    break;

                case 'D':
                    $this->Description = FormatDateVal($this->Value);
                    break;

                case 'Y':
                    $YNArray = array('Y' => _tk('yes'), 'N' => _tk('N'));
                    $this->Description = $YNArray[$this->Value];
                    break;

                case 'T':
                    if (\UnicodeString::strlen($this->Value) == 4)
                    {
                        $this->Description = $this->Value{0} . $this->Value{1} . ":" . $this->Value{2} . $this->Value{3};
                    }
                    else
                    {
                        $this->Description = $this->Value;
                    }
                    break;

                default:
                    $this->Description = $this->Value;
            }

            $_SESSION['CachedValues']['code_description']['custom'][$this->getName()][$this->Value] = $this->Description;
            $_SESSION['CachedValues']['code_colour']['custom'][$this->getName()][$this->Value] = $this->Colour;
        }
    }

    public function getWidth()
    {
        return $this->Width;
    }

    public function getTable()
    {
        return '';
    }

    public function getColour()
    {
        return $this->Colour;
    }

    public function getModule()
    {
        return 'custom';
    }

    public static function getFieldTypesArray()
    {
        return array(
            'D' => 'Date',
            'C' => 'Coded',
            'Y' => 'Yes/No',
            'T' => 'Multicode',
            'S' => 'String',
            'N' => 'Number',
            'M' => 'Money',
            'L' => 'Text',
            );
    }

    /**
     * Define a controller action responsible for rendering the output for this listing column.
     * 
     * @param string $controller Controller class name.
     * @param string $action     Controller action (method) name.
     * 
     * @throws \InvalidArgumentException
     */
    public function setControllerAction($controller, $action, array $data = null)
    {
        if (!class_exists($controller))
        {
            throw new \InvalidArgumentException($controller.' is not a valid controller');
        }
        
        if (!method_exists($controller, $action))
        {
            throw new \InvalidArgumentException($action.' is not a valid action of controller '.$controller);
        }
        
        $this->action = ['controller' => $controller, 'action' => $action, 'data' => $data];
    }
    
    /**
     * Fetches the contents of a field that uses a controller action.
     * 
     * @throws \Exception If the field doesn't use a controller action 
     */
    public function getContents($data)
    {
        if (!$this->usesControllerAction())
        {
            throw new \Exception('getContents() is not supported by fields that don\'t use a controller action');
        }
        
        $loader = new src\framework\controller\Loader();
        $controller = $loader->getController(['controller' => $this->action['controller']]);
        $controller->setRequestParameter('data', $data);
        $controller->setRequestParameter('additional_data', $this->action['data']);
        return $controller->doAction($this->action['action']);
    }
    
    /**
     * Whether this field relies on a separate controller action to render its contents.
     * 
     * @return boolean
     */
    public function usesControllerAction()
    {
        return $this->getFieldType() == 'X' && is_array($this->action);
    }
}
