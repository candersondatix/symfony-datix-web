<?php
use src\framework\query\FieldCollection;
/**
* @desc Object representing a database field
*/
class Fields_ExtraField extends Fields_DummyField
{
    protected $ID;
    protected $GroupID;
    protected $Format;
    protected $Length;
    protected $CodeLike;
    protected $CodeLikeTable;
    protected $DesignLabel; // In design mode, extra fields have different labels.
    protected $CentralLocked;
    protected $QueryID;
    protected $CodeLikeModule;
    /**
    * @var array
    * @desc Array of modules that this extra field will appear in.
    */
    protected $Modules = array();

    /**
    * @desc Looks in FIELD_DIRECTORY table to find field meta-data.
    */
    public function __construct($ID, $GroupID = '')
    {
        $this->ID = $ID;
        $this->GroupID = $GroupID;

        if ($this->ID)
        {
            if ($this->GroupID)
            {
                if (isset($_SESSION["udf_fields"][$this->ID]["GROUP"][$GroupID]))
                {
                     $Data = $_SESSION["udf_fields"][$this->ID]["GROUP"][$GroupID];
                }
                else
                {
                    $sql = '
                        SELECT
                            fld_name as fld_name_basic,
                            fld_name + \' (\' + grp_descr + \')\' as fld_name,
                            fld_type,
                            fld_format,
                            fld_code_like,
                            fld_code_like_table,
                            fld_length,
                            central_locked,
                            fld_code_like_module
                        FROM
                            UDF_FIELDS
                        LEFT JOIN
                            udf_links on UDF_FIELDS.recordid = udf_links.field_id
                        LEFT JOIN
                            udf_groups on udf_links.group_id = udf_groups.recordid
                        WHERE
                            UDF_FIELDS.recordid = :ID and udf_groups.recordid = :group_id
                    ';

                    $Data = DatixDBQuery::PDO_fetch($sql, array('ID' => $this->ID, 'group_id' => $GroupID));
                    $_SESSION["udf_fields"][$this->ID]["GROUP"][$GroupID] = $Data;
                }
            }
            else
            {
                if (!isset($_SESSION["udf_fields"][$this->ID]["NO_GROUP"]))
                {
                    $sql = '
                        SELECT
                            recordid,
                            fld_name as fld_name_basic,
                            fld_name,
                            fld_type,
                            fld_format,
                            fld_code_like,
                            fld_code_like_table,
                            fld_length,
                            central_locked,
                            fld_code_like_module
                        FROM
                            UDF_FIELDS
                    ';

                    $Result = DatixDBQuery::PDO_fetch_all($sql, array());

                    foreach ($Result as $row)
                    {
                        $_SESSION["udf_fields"][$row["recordid"]]["NO_GROUP"] = $row;
                    }
                }

                $Data = $_SESSION["udf_fields"][$this->ID]["NO_GROUP"];
            }
            
            $this->Type = $Data['fld_type'];
            $this->Format = $Data['fld_format'];
            $this->Length = $Data['fld_length'];
            $this->Label = $Data['fld_name_basic'];
            $this->DesignLabel = $Data['fld_name'];
            $this->CodeLike = $Data['fld_code_like'];
            $this->CodeLikeTable = $Data['fld_code_like_table'];
            $this->CentralLocked = $Data['central_locked'];
            $this->CodeLikeModule = $Data['fld_code_like_module'];

            if (!isset($_SESSION["udf_mod_link"]))
            {
                $sql = '
                    SELECT
                        uml_id,
                        uml_module
                    FROM
                        udf_mod_link
                ';

                $Result = DatixDBQuery::PDO_fetch_all($sql, array());

                foreach ($Result as $row)
                {
                     $_SESSION["udf_mod_link"][$row["uml_id"]][] = $row["uml_module"];
                }
            }

            $this->Modules = $_SESSION["udf_mod_link"][$this->ID];
        }
    }

    public function GetDesignLabel()
    {
        return $this->DesignLabel;
    }

    public function GetID()
    {
        return $this->ID;
    }

    public function getName()
    {
        return 'UDF_' . $this->Type . '_' . $this->GroupID . '_' . $this->ID;
    }

    public function getFieldType()
    {
        return $this->Type;
    }

    public function getLabel()
    {
        return $this->Label;
    }

    public function getColour()
    {
        return $this->Colour;
    }

    public function getDescription()
    {
        return $this->Description;
    }

    public function getFormat()
    {
        return $this->Format;
    }
    
    public function getLength()
    {
    	return $this->Length;
    }
    
    public function getCodeLike()
    {
    	return $this->CodeLike;
    }
    
    public function getCentralLocked()
    {
    	return $this->CentralLocked;
    }
    
    public function getModules()
    {
    	return $this->Modules;
    }

    public function getMapFieldModule()
    {
        return $this->CodeLikeModule;
    }

    /**
    * @desc Returns whether or not this field is in a given table.
    */
    public function inTable($Table)
    {
        return (\UnicodeString::strtolower($this->getTable()) == \UnicodeString::strtolower($Table));
    }

    /**
    * @desc Returns the module this field belongs to (if any).
    */
    public function getModule()
    {
        global $ModuleDefs;

        foreach ($ModuleDefs as $module => $details)
        {
            if ($details['TABLE'] == $this->getTable() && !is_null($details['TABLE']) && !is_null($this->getTable()))
            {
                return $module;
            }
        }

        return '';
    }

    function setValue($value)
    {
        $this->Value = $value;
        $this->Description = NULL;
        $this->Colour = NULL;
    }

    /**
    * @desc Calculates and populates the "Description" and "Colour" properties with display-ready values.
    */
    function getDisplayInfo()
    {
        global $FieldDefs, $FieldDefsExtra;

        $module = $this->getModule();
        $field = $this->getName();
        $value = $this->Value;

        if ($value == '')
        {
            return '';
        }

        if ($_SESSION['CachedValues']['cod_info'][$module][$field][$value] && !$GLOBALS['devNoCache'])
        {
           $this->Description = $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['description'];
           $this->Colour = $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['cod_web_colour'];
        }
        else
        {
            switch($this->getFieldType())
            {
                case 'C':
                    $this->GetCodeDisplayInfo();
                    break;
                case 'T':
                    $values = explode(' ', $value);
                    foreach ($values as $mcValue)
                    {
                        $this->GetCodeDisplayInfo($mcValue);
                        $mcDescriptions[] = $this->Description;
                    }
                    $this->Description = implode(', ', $mcDescriptions);
                    break;
                case 'D':
                    $this->Description = FormatDateVal($this->Value);
                    break;
                case 'Y':
                    $YNArray = array('Y' => _tk('yes'), 'N' => _tk('no'));
                    $this->Description = $YNArray[$this->Value];
                    break;
                case "N":
                    //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                    if ($this->getFormat() != "")
                    {
                        $this->Description = GuptaFormatEmulate($this->Value, $this->getFormat());
                    }
                    else
                    {
                        $this->Description = number_format(floatval($this->Value), 2, '.', '');
                    }
                    break;
                case 'M':
                    $this->Description = $this->Value;
                    break;
                default:
                    $this->Description = $this->Value;
            }

            $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['description'] = $this->Description;
            $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['cod_web_colour'] = $this->Colour;
        }
    }

    /**
    * @desc Determines the correct code table for a coded field and pulls out the "Description" and "Colour" values.
    */
    protected function GetCodeDisplayInfo($value = '')
    {
        $module = $this->getModule();

        if ($value == '')
        {
            $value = $this->Value;
        }

        $this->Description = code_descr_udf($this->ID, $value, $module, false);
    }

    public function UpdateFromPost()
    {
        LoggedIn();
        CheckFullAdmin();

        $this->Label = $_POST['label'];
        $this->Type = $_POST['type'];

        //This is used exclusively for staff fields to be limited by profile
        if (isset($_POST['profile_select']))
        {
            $extraCodeField = \src\framework\registry\Registry::getInstance()->getFieldDefs()['UDF_'.$_POST['recordid']];
            $queryid = $extraCodeField->queryid;
            $profileQueryModelFactory = new \src\savedqueries\model\SavedQueryModelFactory();
            $profileQueryEntityFactory = $profileQueryModelFactory->getEntityFactory();
            $profileQueryMapper = $profileQueryModelFactory->getMapper();
            $profileWhere = new \src\framework\query\Where();

            foreach ($_POST['profile_select'] as $profile)
            {
                $whereParams[] = (new FieldCollection)->field('recordid')->eq($profile);
            }

            $newWhereParams[] = array('condition' => 'OR', 'parameters' => $whereParams);
            $profileWhere->addArray($newWhereParams);

            if ($queryid == null || $queryid < 1)
            {
                $profileQuery = $profileQueryEntityFactory->createObject(array('where' => $profileWhere));
                $this->savedQuery = $profileQuery;
                $profileQuery->createdby = $_SESSION['initials'];
                $profileQuery->createddate = date('Y-m-d H:i:s');
            }
            else
            {
                $profileQuery = $profileQueryMapper->find($queryid);
                $profileQuery->where = $profileWhere;
            }

            $profileQuery->updatedby = $_SESSION['initials'];
            $profileQuery->updateddate = date('Y-m-d H:i:s');
            $profileQueryMapper->save($profileQuery);
            $this->queryid = $profileQuery->recordid;
        }
        if($_POST['fld_code_like'] != null && $_POST['fld_code_like_module'] == null)
        {
            $_POST['fld_code_like_module'] = getModuleFromField($_POST['fld_code_like']);
        }
        else if($_POST['fld_code_like'] == null && $_POST['fld_code_like_module'] == null)
        {
            $this->CodeLike = null;
            $this->CodeLikeTable = null;
            $this->CodeLikeModule = null;
        }

        $this->SaveConfigurationToDatabase();
    }

    protected function SaveConfigurationToDatabase()
    {
        LoggedIn();
        CheckFullAdmin();

        if (!$this->ID)
        {
            $this->ID = GetNextRecordID('udf_fields', true);
        }

        $sql = '
            UPDATE
                udf_fields
            SET
                fld_name = :fld_name,
                fld_type = :fld_type,
                fld_format = :fld_format,
                fld_length = :fld_length,
                central_locked = :central_locked,
                queryid = :queryid,
                fld_code_like = :fld_code_like,
                fld_code_like_table = :fld_code_like_table,
                fld_code_like_module = :fld_code_like_module
            WHERE
                recordid = :recordid
        ';

        if (isset($_POST['fld_code_like_module']) && $_POST['fld_code_like_module'] != '' && isset($_POST['fld_code_like']) && $_POST['fld_code_like'] != '')
        {
            $fieldTable = $this->getFieldTable($_POST['fld_code_like_module'], $_POST['fld_code_like']);
        }

        $PDOParams['recordid'] = $this->ID;
        $PDOParams['fld_name'] = $_POST['fld_name'];
        $PDOParams['fld_type'] = $_POST['fld_type'];
        $PDOParams['fld_format'] = $_POST['fld_format'];
        $PDOParams['fld_length'] = ($_POST['fld_length'] != '' ? $_POST['fld_length'] : null);
        $PDOParams['central_locked'] = ($_POST['central_locked'] != '' ? $_POST['central_locked'] : null);
        $PDOParams['queryid'] = $this->queryid;
        $PDOParams['fld_code_like'] = $_POST['fld_code_like'] ?: '';
        $PDOParams['fld_code_like_table'] = $fieldTable ?: '';
        $PDOParams['fld_code_like_module'] = $_POST['fld_code_like_module'] ?: '';

        DatixDBQuery::PDO_query($sql, $PDOParams);

        $sql = '
            DELETE FROM
                udf_mod_link
            WHERE
                uml_id = :recordid
        ';

        DatixDBQuery::PDO_query($sql, array('recordid' => $this->ID));

        if (is_array($_POST['fld_modules']))
        {
            foreach ($_POST['fld_modules'] as $module)
            {
                DatixDBQuery::PDO_build_and_insert('udf_mod_link', array('uml_id' => $this->ID, 'uml_module' => $module));
            }
        }

        $this->updateCache();
    }

    /**
    * Updates the cached values for this UDF stored in the session (e.g. when updating the field)
    */
    protected function updateCache()
    {
        $newValues = array(
            'fld_name'   => $_POST['fld_name'],
            'fld_name_basic' => $_POST['fld_name'],     
            'fld_type'   => $_POST['fld_type'],
            'fld_format' => $_POST['fld_format'],
            'fld_length' => $_POST['fld_length'],
        	'central_locked' => $_POST['central_locked'],
            'fld_code_like_module' => $_POST['fld_code_like_module'],
            'fld_code_like' => $_POST['fld_code_like']

        );

        if ($this->GroupID)
        {
            $_SESSION["udf_fields"][$this->ID]["GROUP"][$this->GroupID] = array_merge($_SESSION["udf_fields"][$this->ID]["GROUP"][$this->GroupID], $newValues);
        }
        else
        {
            if (is_array($_SESSION["udf_fields"][$this->ID]["NO_GROUP"]))
            {
                $_SESSION["udf_fields"][$this->ID]["NO_GROUP"] = array_merge($_SESSION["udf_fields"][$this->ID]["NO_GROUP"], $newValues);
            }
            else
            {
                $_SESSION["udf_fields"][$this->ID]["NO_GROUP"] = $newValues;
            }
        }

        $_SESSION['udf_mod_link'][$this->ID] = $_POST['fld_modules'];
    }

    static public function getOrphanedUDFs($Mod, $RecordID, $FormDesign)
    {
        global $ModuleDefs;

        $sExclUDFsSQL = '';

        $ModID = $ModuleDefs[$Mod]['MOD_ID'];

        $Table = $ModuleDefs[$Mod]['TABLE'];

        // Do not display UDF Fields defined in the DIF2 formdesign - those are displayed already
        if ($FormDesign && is_array($FormDesign->ExtraFields) && !empty($FormDesign->ExtraFields))
        {
            $sExclUDFs = "('" . implode("' ,'", array_keys($FormDesign->ExtraFields)) . "')";

            $sExclUDFsSQL = " AND cast(udf_fields.recordid as varchar) + '_' + cast(udf_groups.recordid as varchar) NOT IN $sExclUDFs ";
            $msExclUDFsSQL = " AND cast(udf_fields.recordid as varchar) + '_' + cast(udf_values.group_id as varchar) NOT IN $sExclUDFs ";
        }

        // This query will get UDF fields that have a valid group_id
        $sql =
        "SELECT
            udf_fields.recordid as recordid,
            udf_fields.fld_name + CASE WHEN udf_groups.grp_descr = ''  THEN ''
                   ELSE ' (' + udf_groups.grp_descr + ')' END as fld_name,
            udf_fields.recordid as fld_id,
            udf_values.group_id as grp_id,
            udf_fields.fld_type,
            udf_values.udv_string,
            udf_values.udv_number,
            udf_values.udv_date,
            udf_values.udv_money,
            udf_values.udv_text,
            udf_fields.fld_format
        FROM
            udf_fields
            LEFT JOIN udf_links ON UDF_LINKS.field_id = udf_fields.recordid
            LEFT JOIN udf_groups ON UDF_groups.recordid = UDF_LINKS.group_id
            LEFT JOIN udf_values ON udf_values.field_id = udf_fields.recordid
        WHERE
            udf_values.mod_id = {$ModID}
            AND udf_values.group_id = UDF_groups.recordid
            AND udf_values.cas_id = " . (int) $RecordID
            . "{$sExclUDFsSQL}";

        $UDFFields = DatixDBQuery::PDO_fetch_all($sql);

        // This query will get UDF fields that as a group_id = 0
        $sql = '
            SELECT
                udf_fields.recordid,
                udf_fields.fld_name,
                udf_fields.recordid as fld_id,
                udf_values.group_id as grp_id,
                udf_fields.fld_type,
                udf_values.udv_string,
                udf_values.udv_number,
                udf_values.udv_date,
                udf_values.udv_money,
                udf_values.udv_text,
                udf_fields.fld_format
            FROM
                udf_values,
                udf_fields
            WHERE
                udf_values.field_id = udf_fields.recordid
                AND
                udf_values.mod_id = ' . $ModID . '
                AND
                udf_values.cas_id = ' . $RecordID . '
                AND
                udf_values.group_id = 0
        ' . $msExclUDFsSQL;

        $MoreUDFFields = DatixDBQuery::PDO_fetch_all($sql);

        $UDFFields = array_merge($UDFFields, $MoreUDFFields);

        return ($UDFFields ? $UDFFields : array());
    }

    private function getFieldTable($module, $fieldName)
    {
        global $ModuleDefs;

        $registry = \src\framework\registry\Registry::getInstance();
        $fieldDefs = $registry->getFieldDefs();

        if ($module == 'STD')
        {
            $module = 'STN';
        }

        $moduleGroups = GetModuleGroups();
        $setupModuleList = [];

        if (in_array($module, array_keys($moduleGroups)))
        {
            foreach (getModArray() as $subModule => $name)
            {
                if ($ModuleDefs[$subModule]['MODULE_GROUP'] == $module)
                {
                    $setupModuleList[] = $subModule;
                }
            }

            foreach ($setupModuleList as $mod)
            {
                $tableOrView = ($ModuleDefs[$mod]['VIEW'] ?: $ModuleDefs[$mod]['TABLE']);

                if ($tableOrView !== null)
                {
                    $fieldDef = $fieldDefs[$tableOrView.'.'.$fieldName];

                    if ($fieldDef !== null)
                    {
                        break;
                    }
                }
            }
        }
        else
        {
            $tableOrView = ($ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE']);

            // Hack to get the table fot stn_status in the standards module because this field is in a view and a view doesn't exist in the standards module
            if ($module == 'STN' && $fieldName == 'stn_status')
            {
                $tableOrView = 'standards_status';
            }

            $fieldDef = $fieldDefs[$tableOrView.'.'.$fieldName];
        }

        return  $fieldDef->getCodeTable();
    }
}