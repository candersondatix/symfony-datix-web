<?php
/**
 * TODO: Replace this with the new Field object
 */

use src\framework\registry\Registry;

/**
* @desc Object representing a database field
*/
class Fields_Field extends Fields_DummyField
{
    protected $Table;
    protected $Data;
    protected $Long;
    protected $Module;

    /**
    * @desc Looks in FIELD_DIRECTORY table to find field meta-data.
    */
    public function __construct($Name, $Table = '', $long = false)
    {
        $this->Name = \UnicodeString::strtolower($Name);
        $this->Long = $long;

        if ($Table)
        {
            $this->Data = $_SESSION['field_directory:'.$this->Name.':'.$Table];

            if (!isset($this->Data))
            {
                $sql = 'SELECT fdr_name, fdr_label, fdr_table, fdr_code_table, fdr_code_descr, fdr_code_field, fdr_data_type, fdr_width, fdr_data_length, fdr_format, fdr_required, fdr_validation, fdr_code_where, fdr_message, fdr_code_parent, fdr_code_order, fdr_code_parent2, fdr_list_type, fdr_colour, fdr_setup, fdr_custom_code from FIELD_DIRECTORY WHERE FDR_NAME = :Name AND FDR_TABLE = :Table';
                $this->Data = DatixDBQuery::PDO_fetch($sql, array('Name' => $this->Name, 'Table' => $Table));

                $_SESSION['field_directory:'.$this->Name.':'.$Table] = $this->Data;
            }
        }
        else
        {
            $this->Data = $_SESSION['field_directory:'.$this->Name];

            if (!isset($this->Data))
            {
                $sql = 'SELECT fdr_name, fdr_label, fdr_table, fdr_code_table, fdr_code_descr, fdr_code_field, fdr_data_type, fdr_width, fdr_data_length, fdr_format, fdr_required, fdr_validation, fdr_code_where, fdr_message, fdr_code_parent, fdr_code_order, fdr_code_parent2, fdr_list_type, fdr_colour, fdr_setup, fdr_custom_code from FIELD_DIRECTORY WHERE FDR_NAME = :Name';
                $this->Data = DatixDBQuery::PDO_fetch($sql, array('Name' => $this->Name));

                $_SESSION['field_directory:'.$this->Name] = $this->Data;
            }

            $Table = $this->Data['fdr_table'];
        }

        $this->Table = \UnicodeString::strtolower($Table);

        if (empty($this->Data))
        {
            throw new Exception('Field "'.$Name.'" not found in field_directory');
        }

        $this->setFieldLabel();
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getFieldType()
    {
        return $this->Data['fdr_data_type'];
    }

    public function getTable()
    {
        return $this->Table;
    }

    public function getLabel()
    {
        return  $this->Data['fdr_label'];
    }

    public function isCustomCodedField()
    {
        return bYN($this->Data['fdr_custom_code']);
    }

    private function setFieldLabel()
    {
        if ($this->Long == true)
        {
            $FieldLabel = new Labels_FieldLabel($this->getName(), $this->getTable(), '', $this->Long);
        }
        else
        {
            $FieldLabel = new Labels_FieldLabel($this->getName(), $this->getTable());
        }

        $this->Data['fdr_label'] = $FieldLabel->getLabel();
    }

    public static function getFieldLabel($Name, $Table = '')
    {
        $field = new Fields_Field($Name, $Table);
        return  $field->getLabel();
    }

    public function getColour()
    {
        return  $this->Colour;
    }

    public function getDescription()
    {
        return $this->Description;
    }

    public function getCodeDescription()
    {
        return  $this->Data['fdr_code_descr'];
    }


    /**
    * @desc Returns whether or not this field is in a given table.
    */
    public function inTable($Table)
    {
        return ($this->getTable() == \UnicodeString::strtolower($Table));
    }

    /**
    * @desc Returns the module this field belongs to (if any).
    */
    public function getModule()
    {
        global $ModuleDefs;

        if($this->module == null)
        {
            foreach($ModuleDefs as $module => $details)
            {
                if($details['TABLE'] == $this->getTable())
                {
                    $this->module = $module;
                }
            }

            if($module == null)
            {
                $this->module = '';
            }
        }

        return $this->module;
    }

    function setValue($value)
    {
        $this->Value = $value;
        $this->Description = NULL;
        $this->Colour = NULL;
    }

    /**
    * @desc Calculates and populates the "Description" and "Colour" properties with display-ready values.
    */
    function getDisplayInfo()
    {
        global $FieldDefs, $FieldDefsExtra;

        $module = $this->getModule();
        $field = $this->getName();
        $value = $this->Value;

        if ($value == '')
        {
            return '';
        }

        if ($_SESSION['CachedValues']['cod_info'][$module][$field][$value] && !$GLOBALS['devNoCache'])
        {
           $this->Description = $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['description'];
           $this->Colour = $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['cod_web_colour'];
        }
        else
        {
            switch($this->getFieldType())
            {
                case 'C':
                case 'M':
                    $this->GetCodeDisplayInfo();
                    break;

                case 'D':
                    $this->Description = FormatDateVal($this->Value);
                    break;

                case 'Y':
                    $YNArray = array('Y' => _tk('yes'), 'N' => _tk('N'));
                    $this->Description = $YNArray[$this->Value];
                    break;

                case 'T':
                    if (\UnicodeString::strlen($this->Value) == 4)
                    {
                        $this->Description = $this->Value{0} . $this->Value{1} . ":" . $this->Value{2} . $this->Value{3};
                    }
                    else
                    {
                        $this->Description = $this->Value;
                    }
                    break;

                default:
                    $this->Description = $this->Value;
            }

            $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['description'] = $this->Description;
            $_SESSION['CachedValues']['cod_info'][$module][$field][$value]['cod_web_colour'] = $this->Colour;
        }
    }

    /**
    * @desc Determines the correct code table for a coded field and pulls out the "Description" and "Colour" values.
    */
    protected function GetCodeDisplayInfo()
    {
        global $FieldDefs;

        $registry = Registry::getInstance();
        $moduleDefs = $registry->getModuleDefs();

        $module = $this->getModule();
        $field = $this->getName();
        $value = $this->Value;

        if ($this->getName() == 'rep_approved' && _tk('approval_status_'.$this->getModule().'_'.$this->Value))
        {
            $this->Description = _tk('approval_status_'.$this->getModule().'_'.$this->Value);
            return true;
        }

        if ($this->Data['fdr_code_table']{0} == '!')
        {
            $sql = 'SELECT cod_descr AS description, cod_web_colour as colour
                FROM code_types';

            if (\UnicodeString::strpos($value, ' '))
            {
                $where = ' WHERE cod_code IN (';
                $values = explode(' ', $value);
                $PDOArray = array();

                foreach ($values as $id => $val)
                {
                    $where.= ':val' . $id . ', ';
                    $PDOArray['val' . $id] = $val;
                }

                $where = \UnicodeString::substr($where, 0, -2);
                $where.= ')';
            }
            else
            {
                $where = ' WHERE cod_code = :value';
                $PDOArray = array('value' => $value);
            }

            $sql .=  $where . ' AND cod_type = :code_type';

            $PDOArray['code_type'] = \UnicodeString::substr($this->Data['fdr_code_table'], 1);
        }
        elseif ($this->Data['fdr_code_descr'] != '' && $this->Data['fdr_code_table'] != '' && $this->Data['fdr_code_field'] != '')
        {
            $sql = 'SELECT '.$this->Data['fdr_code_descr'].' AS description'.($this->Data['fdr_custom_code'] != 'Y' ? ', cod_web_colour as colour' :'').'
                    FROM '.$this->Data['fdr_code_table'];

            if (\UnicodeString::strpos($value, ' ') && $this->getName() != 'link_patrelation') //'link_patrelation' is a legacy field where the "codes" are actually descriptions and so might have spaces.
            {
                $where = ' WHERE ' . $this->Data['fdr_code_field'] . ' IN (';
                $values = explode(' ', $value);
                $PDOArray = array();

                foreach ($values as $id => $val)
                {
                    $where.= ':val' . $id . ', ';
                    $PDOArray['val' . $id] = $val;
                }

                $where = \UnicodeString::substr($where, 0, -2);
                $where.= ')' . ($this->Data['fdr_code_where'] ? ' AND ' . $this->Data['fdr_code_where'] : '');
            }
            else
            {
                $where = ' WHERE ' . $this->Data['fdr_code_field'] . ' = :value' . ($this->Data['fdr_code_where'] ? ' AND ' . $this->Data['fdr_code_where'] : '');
                $PDOArray = array('value' => $value);
            }

            $sql.= $where;
        }

        if ($sql)
        {
            $rows = PDO_fetch_all($sql, $PDOArray);
        }

        if (count($rows) == 0)
        {
            $this->Description = $value;
        }
        else if(count($rows) == 1)
        {
            $this->Description = $rows[0]['description'];
            if($rows[0]['colour'])
            {
                $this->Colour = $rows[0]['colour'];
            }
        }
        else
        {
            foreach ($rows as $row)
            {
                $descriptionArray[] = $row['description'];
            }
            $this->Description = implode(', ', $descriptionArray);
        }

        if($field === 'act_module')
        {
            $this->Description = $moduleDefs[$value]['NAME'] ?: $this->Description;
        }
    }

    public function getCodeTable()
    {
        return \UnicodeString::strtolower($this->Data['fdr_code_table']);
    }

    public function getCodeWhere()
    {
        return \UnicodeString::strtolower($this->Data['fdr_code_where']);
    }

    public function getCodeTableField()
    {
        return \UnicodeString::strtolower( $this->Data['fdr_code_field']);
    }

    /**
    * Getter for the Colour property.
    *
    * @codeCoverageIgnore
    */
    public function getCodeColour()
    {
        return \UnicodeString::strtolower($this->Colour);
    }

    public function getCodeOrder()
    {
        return \UnicodeString::strtolower( $this->Data['fdr_code_order']);
    }

    /**
    * Fecthes the equivalent to this field in another module, if one exists.
    *
    * Can be used to map e.g. system-wide location fields.
    *
    * @param  string $module The module we're looking for the mapped field in.
    *
    * @return string         The name of the mapped field, or false if not found.
    */
    public function getFieldMapping($module)
    {
        global $ModuleDefs;

        $sql = 'SELECT fdr_name FROM field_directory WHERE fdr_table = :table AND fdr_code_table = :code_table ORDER BY fdr_name';
        $PDOParams = array('table' => $ModuleDefs[$module]['TABLE'], 'code_table' => $this->Data['fdr_code_table']);
        return DatixDBQuery::PDO_fetch($sql, $PDOParams, PDO::FETCH_COLUMN);
    }

    /**
     * @desc Function that determines if a field is calculated or not
     *
     * @static
     * @param string $module The module to where the field belongs to
     * @param string $fieldName The name of the field
     * @return bool true if is a calculated field false otherwise
     */
    public static function isFieldCalculated($module, $fieldName)
    {
        if ($module != '' && $fieldName != '')
        {
            $sql = '
                SELECT
                    fmt_calculated_field
                FROM
                    field_formats
                WHERE
                    fmt_module = :fmt_module
                    AND
                    fmt_field = :fmt_field
            ';

            $PDOParams = array(
                'fmt_module' => $module,
                'fmt_field' => $fieldName
            );

            return (DatixDBQuery::PDO_fetch($sql, $PDOParams, PDO::FETCH_COLUMN) == 'Y' ? true : false);
        }

        return false;
    }
}
