<?php
/**
 * Class used to copy Datix module records and associated data
 *
 */
class CopyRecords
{
    private $CopyOptions;

    /**
    * @desc Constructor: Assigns parameter values to object properties.
    *
    * @param array $Parameters The array of parameters to be assigned.
    */
    public function __construct($Parameters)
    {
        $this->CopyOptions = new CopyOptions($Parameters);
    }

    /**
    * @desc Selects main module record data for copying and copies other data based on options chosen.
    *
    */
    public function CopyMainTable()
    {
        global $ModuleDefs;

        $Module = $this->CopyOptions->Module;

        if (!empty($ModuleDefs[$Module]["ADDITIONAL_COPY_LIB"]))
        {
            require_once $ModuleDefs[$Module]["ADDITIONAL_COPY_LIB"];
        }

        $ModID = $ModuleDefs[$Module]["MOD_ID"];

        $sql = "SELECT recordid, ".implode(', ',$ModuleDefs[$Module]['FIELD_ARRAY'])." FROM ".$ModuleDefs[$Module]['TABLE'];

        if (!empty($this->CopyOptions->join))
        {
           $sql .= " " . $this->CopyOptions->join;
        }
        
        if (!empty($this->CopyOptions->where))
        {
           $sql .= " WHERE " . $this->CopyOptions->where;
        }

        $RecordsToCopy = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($RecordsToCopy as $data)
        {
            if (!empty($ModuleDefs[$Module]["ADDITIONAL_COPY_OVERRIDE_FIELD_OPTIONS"]))
            {
                foreach ($ModuleDefs[$Module]["ADDITIONAL_COPY_OVERRIDE_FIELD_OPTIONS"] as $fieldname)
                {
                    if (!empty($this->CopyOptions->override_values[$fieldname]))
                    {
                        $data[$fieldname] = $this->CopyOptions->override_values[$fieldname];
                    }
                }
            }

            for ($i = 0;$i < $this->CopyOptions->copies; $i++)
            {
                if (self::InsertRecords($Module, $NewRecordID, $data))
                {
                    if ($this->CopyOptions->copy_extra_fields == "Y")
                    {
                        $this->CopyUDFData($ModID, $data["recordid"], $NewRecordID);
                    }
                    if ($this->CopyOptions->copy_actions == "Y")
                    {
                        $this->CopyActions($Module, $data["recordid"], $NewRecordID, $this->CopyOptions->copy_extra_fields);
                    }
                    if ($this->CopyOptions->copy_documents == "Y")
                    {
                         $this->CopyDocuments($Module, $data["recordid"], $NewRecordID);
                    }
                    if ($this->CopyOptions->copy_linked_contacts == "Y")
                    {
                         $this->CopyLinkedContacts($Module, $data["recordid"], $NewRecordID, $Module);
                         $this->CopyInjuries($data["recordid"], $NewRecordID);
                    }
                    if ($this->CopyOptions->copy_notepad == "Y")
                    {
                         $this->CopyNotepad($Module, $data["recordid"], $NewRecordID);
                    }
                    if ($this->CopyOptions->copy_link_to_master == "Y")
                    {
                         $this->LinkToMaster($Module, $data["recordid"], $NewRecordID, "Linked via copy option");
                    }

                    if (!empty($ModuleDefs[$Module]["ADDITIONAL_COPY_FUNCTION_CALLS"]))
                    {
                        foreach($ModuleDefs[$Module]["ADDITIONAL_COPY_FUNCTION_CALLS"] as $additional_func)
                        {
                            $additional_func($Module, $data["recordid"], $NewRecordID, $this->CopyOptions);
                        }
                    }

                    InsertFullAuditRecord($Module, $NewRecordID, 'COPY', "Copied from record: " . $data["recordid"] . " By: $_SESSION[initials]"); 
                }
            }
        }
    }

    /**
    * @desc Performs insert into main module table with data for each field passed as an array of values.
    *
    * @param string $Module Module to insert into.
    * @param int $recordid Return value of new record's recordid.
    * @param array $data contains values for fields for inserted record.
    */
    static function InsertRecords($Module, &$recordid, $data)
    {
        global $ModuleDefs, $scripturl, $yySetLocation;

        $registry = \src\framework\registry\Registry::getInstance();
        $tableDefs = $registry->getTableDefs();

        $ModuleTable = $ModuleDefs[$Module]['TABLE'];

        if ($tableDefs[$ModuleTable]->is_identity == 1)
        {
            if($data['recordid'])
            {
                unset($data['recordid']);
            }

            $sql = GeneratePDOInsertSQLFromArrays(array(
                'FieldArray' => $ModuleDefs[$Module]['FIELD_ARRAY'],
                'DataArray' => $data,
                'Module' => $Module,
                'end_comma' => false
            ));

            $sql = "INSERT INTO $ModuleTable ".$sql;

            $recordid = DatixDBQuery::PDO_insert($sql, $data);
        }
        else
        {
            $recordid = GetNextRecordID($ModuleTable, true, 'recordid', $_SESSION["initials"]);

            $data["updatedby"] = $_SESSION[initials];
            $data["updateddate"] = date('d-M-Y H:i:s');

            // Populate 'First received' and 'Opened' dates for Complaints
            if ($Module == 'COM')
            {
                if (!in_array('com_dreceived', $ModuleDefs[$Module]['FIELD_ARRAY']))
                {
                    $ModuleDefs[$Module]['FIELD_ARRAY'][] = 'com_dreceived';
                }

                if (!in_array('com_dopened', $ModuleDefs[$Module]['FIELD_ARRAY']))
                {
                    $ModuleDefs[$Module]['FIELD_ARRAY'][] = 'com_dopened';
                }

                $data['com_dreceived'] = date('d-M-Y H:i:s');
                $data['com_dopened'] = date('d-M-Y H:i:s');
            }

            $sql = "UPDATE $ModuleTable SET ";

            $sql .= GeneratePDOSQLFromArrays(array(
                'FieldArray' => $ModuleDefs[$Module]['FIELD_ARRAY'],
                'DataArray' => $data,
                'Module' => $Module,
                'end_comma' => false
            ), $PDOParams);
            $sql .= " WHERE recordid = $recordid";

            $result = DatixDBQuery::PDO_query($sql, $PDOParams);
        }

        if (($tableDefs[$ModuleTable]->is_identity == 1 && !$recordid) || ($tableDefs[$ModuleTable]->is_identity != 1 && !$result))
        {
            $error = "An error has occurred when trying to save the record.  Please report the following to the Datix administrator: $sql";
        }

        if ($error != "")
        {
            SaveError($error);
            return false;
        }

        return true;
    }

    /**
    * @desc Copies UDF data from one module record to another.
    *
    * @param int $ModID Module ID.
    * @param int $SourceCasID
    * @param int $DestCasID
    * @return bool
    */
    static function CopyUDFData($ModID, $SourceCasID, $DestCasID)
    {
        $sql = "insert into UDF_VALUES (MOD_ID, cas_id, GROUP_ID, FIELD_ID, UDV_STRING, UDV_NUMBER, UDV_DATE, udv_money, udv_text)
                (
                select MOD_ID, $DestCasID, GROUP_ID, FIELD_ID, UDV_STRING, UDV_NUMBER, UDV_DATE, udv_money, udv_text from UDF_VALUES where MOD_ID = $ModID and CAS_ID = $SourceCasID
                )";
        $result = DatixDBQuery::PDO_query($sql);

        if (!$result)
        {
            return false;
        }

        return true;
    }

    /**
     * Copies Actions from one module record to another.
     *
     * @param string $Act_module Module short name.
     * @param $SourceCasID
     * @param $DestCasID
     * @param string $copy_extra_fields
     * @return bool
     */
    static function CopyActions($Act_module, $SourceCasID, $DestCasID, $copy_extra_fields = "N")
    {
        global $ModuleDefs;

        // Get record actions
        $sql = "SELECT recordid, act_chain_id, act_chain_instance, act_step_no, ".implode(', ',$ModuleDefs['ACT']['FIELD_ARRAY'])."
                FROM ca_actions
                WHERE act_module = :act_module and act_cas_id = :act_cas_id
                ORDER BY recordid";

        $result = DatixDBQuery::PDO_fetch_all($sql, [
            'act_module' => $Act_module,
            'act_cas_id' => $SourceCasID
        ]);

        // Copy record actions
        foreach ($result as $data)
        {
            $OldActionRecordid = $data["recordid"];
            $data["act_cas_id"] = $DestCasID;

            $recordid = GetNextRecordID($ModuleDefs['ACT']['TABLE'], false, 'recordid', $_SESSION["initials"]);
            $data["recordid"] = $recordid;

            $InsertSql = GeneratePDOInsertSQLFromArrays(
                array(
                    'FieldArray' => array_merge(array('recordid', 'act_chain_id', 'act_chain_instance', 'act_step_no'), $ModuleDefs['ACT']['FIELD_ARRAY']),
                    'DataArray' => $data,
                    'Module' => 'ACT',
                    'Extras' => array('updatedby' => $_SESSION["initials"], 'updateddate' => date('d-M-Y H:i:s'))
                ),
                $PDOParams
            );

            $sql = "INSERT INTO ca_actions $InsertSql";

            if (!DatixDBQuery::PDO_query($sql, $PDOParams))
            {
                fatal_error("Cannot copy Actions data");
                return false;
            }

            if ($copy_extra_fields == "Y")
            {
                self::CopyUDFData(MOD_ACTIONS, $OldActionRecordid, $recordid);
            }
        }

        return true;
    }

    /**
     * Copies Notepad data from one module record to another.
     *
     * @param string $Module Module short name.
     * @param $SourceCasID
     * @param $DestCasID
     * @return bool
     */
    static function CopyNotepad($Module, $SourceCasID, $DestCasID)
    {
        global $ModuleDefs;

        $FK_field = $ModuleDefs[$Module]["FK"];

        $sql = "insert into NOTEPAD  ($FK_field ,REMINDER_DATE ,REMINDER_NOTE ,NOTES)
                (
                    select $DestCasID, REMINDER_DATE, REMINDER_NOTE, NOTES from NOTEPAD where $FK_field = $SourceCasID
                )";

        $result = DatixDBQuery::PDO_Query($sql);

        if (!$result)
        {
            fatal_error("Cannot copy Notepad data");
            return false;
        }

        return true;
    }

    /**
    * @desc Copies linked contacts data from one module record to another.
    *
    * @param string $Module Module short name.
    * @param int $SourceCasID Recordid of source record
    * @param int $DestCasID Recordid of destination record
    * @param int $DestModule Destination Module short name.
    * @return bool
    */
    static function CopyLinkedContacts($Module, $SourceCasID, $DestCasID, $DestModule)
    {
        global $ModuleDefs;

        $FK_field = $ModuleDefs[$Module]["FK"];
        $Dest_FK_field = $ModuleDefs[$DestModule]["FK"];

        $sql = "insert into LINK_CONTACTS  ($Dest_FK_field
          ,[CON_ID]
          ,[LINK_TYPE]
          ,[LINK_ROLE]
          ,[LINK_DEAR]
          ,[LINK_REF]
          ,[LINK_SPECIALTY]
          ,[LINK_RESP]
          ,[LINK_STATUS]
          ,[LINK_MARRIAGE]
          ,[LINK_DOD]
          ,[LINK_NDEPENDENTS]
          ,[LINK_AGEDEPENDENTS]
          ,[LINK_TOCNST]
          ,[LINK_OCCUPATION]
          ,[LINK_PLAPAT]
          ,[LINK_DECEASED]
          ,[LINK_PATRELATION]
          ,[LINK_LIP]
          ,[LINK_RIDDOR]
          ,[LINK_DAYSAWAY]
          ,[LINK_IS_RIDDOR]
          ,[LINK_TREATMENT]
          ,[LINK_INJURY1]
          ,[LINK_BODYPART1]
          ,[LINK_LEGALAID]
          ,[LINK_INJURIES]
          ,[UPDATEDDATE]
          ,[UPDATEDBY]
          ,[LINK_NOTES]
          ,[LINK_MHACT]
          ,[link_age]
          ,[link_npsa]
          ,[link_mhact_section]
          ,[link_mhcpa]
          ,[link_npsa_role]
          ,[link_mh_observe]
          ,[link_primary]
          ,[link_abs_start]
          ,[link_abs_end]
          ,[link_assigned_act]
          ,[link_actual_act]
          ,[link_become_unconscious]
          ,[link_req_resuscitation]
          ,[link_hospital_24hours]
          ,[link_worked_alone]
          ,[link_pprop_damaged]
          ,[link_clin_factors]
          ,[link_direct_indirect]
          ,[link_injury_caused]
          ,[link_attempted_assault]
          ,[link_discomfort_caused]
          ,[link_public_disorder]
          ,[link_harassment]
          ,[link_police_pursue]
          ,[link_police_persue_reason]
          ,[link_verbal_abuse]
          ,[link_notify_progress]
          ,[link_date_admission]
          )
          (
            select $DestCasID
          ,[CON_ID]
          ,[LINK_TYPE]
          ,[LINK_ROLE]
          ,[LINK_DEAR]
          ,[LINK_REF]
          ,[LINK_SPECIALTY]
          ,[LINK_RESP]
          ,[LINK_STATUS]
          ,[LINK_MARRIAGE]
          ,[LINK_DOD]
          ,[LINK_NDEPENDENTS]
          ,[LINK_AGEDEPENDENTS]
          ,[LINK_TOCNST]
          ,[LINK_OCCUPATION]
          ,[LINK_PLAPAT]
          ,[LINK_DECEASED]
          ,[LINK_PATRELATION]
          ,[LINK_LIP]
          ,[LINK_RIDDOR]
          ,[LINK_DAYSAWAY]
          ,[LINK_IS_RIDDOR]
          ,[LINK_TREATMENT]
          ,[LINK_INJURY1]
          ,[LINK_BODYPART1]
          ,[LINK_LEGALAID]
          ,[LINK_INJURIES]
          ,[UPDATEDDATE]
          ,[UPDATEDBY]
          ,[LINK_NOTES]
          ,[LINK_MHACT]
          ,[link_age]
          ,[link_npsa]
          ,[link_mhact_section]
          ,[link_mhcpa]
          ,[link_npsa_role]
          ,[link_mh_observe]
          ,[link_primary]
          ,[link_abs_start]
          ,[link_abs_end]
          ,[link_assigned_act]
          ,[link_actual_act]
          ,[link_become_unconscious]
          ,[link_req_resuscitation]
          ,[link_hospital_24hours]
          ,[link_worked_alone]
          ,[link_pprop_damaged]
          ,[link_clin_factors]
          ,[link_direct_indirect]
          ,[link_attempted_assault]
          ,[link_injury_caused]
          ,[link_discomfort_caused]
          ,[link_public_disorder]
          ,[link_harassment]
          ,[link_police_pursue]
          ,[link_police_persue_reason]
          ,[link_verbal_abuse]
          ,[link_notify_progress]
          ,[link_date_admission] from LINK_CONTACTS where $FK_field = $SourceCasID
          )";
        $result = DatixDBQuery::PDO_query($sql);

        if ($Module != $DestModule)
        {
            $FromLinkType = '';

            switch ($Module)
            {
                case 'PAL':
                    $FromLinkType = 'P';
                    break;
                case 'COM':
                    $FromLinkType = 'C';
                    break;
            }

            if (!empty($FromLinkType))
            {
                $sql = "UPDATE link_contacts SET link_type = 'N' WHERE $Dest_FK_field = $DestCasID and link_type = '$FromLinkType'";
                $result = DatixDBQuery::PDO_query($sql);
            }
        }

        if (!$result)
        {
            fatal_error("Cannot copy linked contacts data");
            return false;
        }

        return true;
    }

    /**
    * @desc Copies document data from one module record to another. This includes copying the physical file as well.
    *
    * @param string $Module Module short name.
    * @param int $SourceCasID
    * @param int $DestCasID
     * @return bool
    */
    static function CopyDocuments($Module, $SourceID, $DestID)
    {
        global $ModuleDefs;

        $FK_field = $ModuleDefs[$Module]["FK"];

        $sql = 'SELECT recordid FROM documents_main WHERE '.$ModuleDefs[$Module]['FK'].' = :source_id';
        $DocumentsToCopy = \DatixDBQuery::PDO_fetch_all($sql, array('source_id' => $SourceID), \PDO::FETCH_COLUMN);

        $Factory = new \src\documents\model\DocumentModelFactory();

        foreach ($DocumentsToCopy as $recordid)
        {
            $Document = $Factory->getMapper()->find($recordid);

            if ($Document->doc_id)
            {
                $DocLocation = $Document->GetPath();
            }

            $Document->recordid = null;
            $Document->doc_id = null;
            $Document->$ModuleDefs[$Module]['FK'] = $DestID;

            if ($Document->Exists($DocLocation))
            {
                $Factory->getMapper()->save($Document, $DocLocation, $Document->doc_extension);
            }
        }

        return true;
    }

    /**
    * @desc Creates link from one module record to another.
    *
    * @param string $Module Module short name.
    * @param int $SourceCasID Recordid of master record
    * @param int $DestCasID Recordid of copied record
    * @param string $Link_notes link notes
    * @return bool
    */
    static function LinkToMaster($Module, $SourceCasID, $DestCasID, $Link_notes = "")
    {
        $PDOParams["lnk_modid1"] = $Module;
        $PDOParams["lnk_id1"] = $SourceCasID;
        $PDOParams["lnk_modid2"] = $Module;
        $PDOParams["lnk_id2"] = $DestCasID;
        $PDOParams["link_notes"] = $Link_notes;

        $sql = "INSERT INTO links (lnk_mod1 ,lnk_id1 , lnk_mod2 , lnk_id2, link_notes)
                    VALUES (:lnk_modid1, :lnk_id1, :lnk_modid2, :lnk_id2, :link_notes)";

        $result = DatixDBQuery::PDO_Query($sql, $PDOParams);

        if (!$result)
        {
            fatal_error("Cannot create link to master record");
            return false;
        }

        return true;
    }

    /**
    * @desc Copies linked equipment data from one module record to another.
    *
    * @param string $Module Module short name.
    * @param int $SourceCasID Recordid of source record
    * @param int $DestCasID Recordid of destination record
    * @param int $DestModule Destination Module short name.
     * @return bool
    */
    static function CopyLinkedAssets($Module, $SourceCasID, $DestCasID, $DestModule)
    {
        global $ModuleDefs;

        $FK_field = $ModuleDefs[$Module]["FK"];
        $Dest_FK_field = $ModuleDefs[$DestModule]["FK"];

        $sql = "insert into LINK_ASSETS  ($Dest_FK_field
          ,[LINK_TYPE]
          ,[AST_ID]
          ,[link_notes]
          ,[link_damage_type]
          ,[link_other_damage_info]
          ,[link_replaced]
          ,[link_replace_cost]
          ,[link_repaired]
          ,[link_repair_cost]
          ,[link_written_off]
          ,[link_disposal_cost]
          ,[link_sold]
          ,[link_sold_price]
          ,[link_decommissioned]
          ,[link_decommission_cost]
          ,[link_residual_value]
          )
          (
            select $DestCasID
          ,[LINK_TYPE]
          ,[AST_ID]
          ,[link_notes]
          ,[link_damage_type]
          ,[link_other_damage_info]
          ,[link_replaced]
          ,[link_replace_cost]
          ,[link_repaired]
          ,[link_repair_cost]
          ,[link_written_off]
          ,[link_disposal_cost]
          ,[link_sold]
          ,[link_sold_price]
          ,[link_decommissioned]
          ,[link_decommission_cost]
          ,[link_residual_value] from LINK_ASSETS where $FK_field = $SourceCasID
          )";
        $result = DatixDBQuery::PDO_query($sql);

        if (!$result)
        {
            fatal_error("Cannot copy linked equipment data");
            return false;
        }

        return true;
    }

    /**
     * Creates link from one module record to another record in a different module.
     *
     * @param string $Module Module short name.
     * @param int $SourceCasID Recordid of master record
     * @param $DestModule
     * @param int $DestCasID Recordid of copied record
     * @param string $Link_notes link notes
     * @return bool
     */
    static function LinkToMasterCrossModule($Module, $SourceCasID, $DestModule, $DestCasID, $Link_notes = "")
    {
        global $ModuleDefs;

        $FK_field = $ModuleDefs[$Module]["FK"];
        $Dest_FK_field = $ModuleDefs[$DestModule]["FK"];

        $PDOParams["FK_field"] = $SourceCasID;
        $PDOParams["Dest_FK_field"] = $DestCasID;
        $PDOParams["link_notes"] = $Link_notes;

        $sql = "INSERT INTO link_modules ($FK_field, $Dest_FK_field, link_notes)
                    VALUES (:FK_field, :Dest_FK_field, :link_notes)";

        $result = DatixDBQuery::PDO_Query($sql, $PDOParams);

        if (!$result)
        {
            fatal_error("Cannot create cross module link to master record");
            return false;
        }

        return true;
    }

    /**
    * @desc Copies injury links from one incident record to another.
    *
    * @param int $SourceCasID
    * @param int $DestCasID
     * @return bool
    */
    static function CopyInjuries($SourceCasID, $DestCasID)
    {
        global $ModuleDefs;

        $InjuriesFieldArray = array('recordid', 'inc_id', 'con_id', 'inc_injury', 'inc_bodypart', 'listorder');
        $sql = "SELECT " . implode(", ", $InjuriesFieldArray) .
                " FROM inc_injuries
                WHERE inc_id = " . $SourceCasID;
        $request = db_query($sql);

        while ($data = db_fetch_array($request))
        {
            $data["inc_id"] = $DestCasID;

            $recordid = GetNextRecordID('inc_injuries', false, 0, 'recordid', "", $_SESSION["initials"]);
            $data["recordid"] = $recordid;

            $InsertSql = GeneratePDOInsertSQLFromArrays(array(
                                'FieldArray' => $InjuriesFieldArray,
                                'DataArray' => $data,
                                ), $PDOParams);

            $sql = "INSERT INTO inc_injuries $InsertSql";

            if (!PDO_query($sql, $PDOParams))
            {
                fatal_error("Cannot copy Injuries data");
                return false;
            }
        }

        return true;
    }
}

?>