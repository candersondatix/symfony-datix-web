<?php
 /**
 * Class used for code setups.
 *
 */
class Setups_CodeSetup
{
    private $fieldname;
    private $fieldtable;

    var $code_table;
    var $code_field;
    var $code_type;
    var $code_where;
    var $code_label;
    var $code_description;

    var $ErrorMsgs = array();   //Array used to store error messages.

    /**
    * @desc constructor - sets/gets required info about code setup
    */
    function __construct($fieldname, $fieldtable)
    {
        $this->fieldname  = $fieldname;
        $this->fieldtable = $fieldtable;
    }

    /**
    * @desc Outputs setup codes into XML format load file.
    *
    */
    function OutputCodeSetup()
    {
        $this->getCodeInfo();
        $CodeSetupExport = new Setups_ExportCodes($this->code_table, $this->code_where);
        $setup_xml = $CodeSetupExport->getCodeSetupsXML();

        header("Content-Type: text/xml");
        header("Content-Disposition: attachment; filename=". $this->code_label .".xml");
        header("Content-Encoding: none");
        echo $setup_xml;
        exit;
    }

    /**
    * @desc Imports data in XML format outputted via OutputCodeSetup method.
    *
    * @param string $RawXMLData XML data string to be imported
    * @param string $import_action Import method to use (APPEND|UPDATE|OVERWRITE)
    */
    function ImportCodeSetup($RawXMLData, $import_action)
    {
        $this->getCodeInfo();
        $DatixCodesImport = new Setups_ImportCodes($this->code_table, $this->code_field, $this->code_where, $this->code_description, $import_action);
        if (!$DatixCodesImport->LoadXMLDoc($RawXMLData))
        {
            $this->ErrorMsgs = $DatixCodesImport->ErrorMsgs;
            return false;
        }

        if ($DatixCodesImport->importCodesXMLdata())
        {
            return true;
        }
        else
        {
            $this->ErrorMsgs = $DatixCodesImport->ErrorMsgs;
            return false;
        }
    }

    /**
    * @desc Gets code related info for use in other class methods.
    *
    */
    private function getCodeInfo()
    {
        $FDR_info = new Fields_Field($this->fieldname,  $this->fieldtable);
        $this->code_table = $FDR_info->getCodeTable();
        $this->code_field = $FDR_info->getCodeTableField();
        $this->code_label = $FDR_info->getLabel();
        $this->code_description = $FDR_info->getCodeDescription();

        if ($this->code_table[0] == "!")
        {
            $this->code_type = \UnicodeString::substr($this->code_table, 1, \UnicodeString::strlen($this->code_table));
            $this->code_where = "cod_type = '$this->code_type'";
            $this->code_table = "code_types";
            $this->code_field = "cod_code";
        }
    }



    public function lockCodeSetup()
    {
        if (!IsCentrallyAdminSys())
        {
            throw new NoPermissionException ("Only centrally administered systems can be locked");
        }
        
        // we don't know if theres any previous record, so we can't 
        // just update. Let's delete first.
        $this->deleteLockStatus();

        $params = array (
            'field' => $this->fieldname, 
            'table' => $this->fieldtable
        );

        DatixDBQuery::PDO_query('INSERT INTO locked_code_fields (code_field, code_table, locked) VALUES (:field, :table, 1)', $params);
    }



    public function unlockCodeSetup()
    {
        if (!IsCentrallyAdminSys())
        {
            throw new NoPermissionException ("Only centrally administered systems can be locked");
        }
        
        $this->deleteLockStatus();
    }



    private function deleteLockStatus()
    {
        $params = array (
            'field' => $this->fieldname, 
            'table' => $this->fieldtable
        );

        DatixDBQuery::PDO_query('DELETE FROM locked_code_fields WHERE code_field = :field AND code_table = :table', $params);
    }


    
    /**
     * Was code field locked by central administrator?
     * @param string $code_field_name
     * @return bool true if field is locked, false otherwise
     */
    public function isCodeFieldLocked()
    {
        if (!IsCentrallyAdminSys())
        {
            // system without central administration 
            // has no locked code fields
            return false;
        }

        $sql = "SELECT locked FROM locked_code_fields WHERE code_table = :table AND code_field = :field";

        $params = array (
            'field' => $this->fieldname, 
            'table' => $this->fieldtable
        );

        $result = DatixDBQuery::PDO_fetch_all ($sql, $params);

        if (!empty($result[0]['locked'])) 
        {
            return true;
        }

        return false;
    }


    static function getSystemWideFieldsList ()
    {
        $list = array (
            '!trust' => array (
                'inc_organisation', 'ram_organisation',
                'pal_organisation', 'com_organisation', 
                'cla_organisation', 'stn_organisation', 
                'lib_organisation', 'sab_organisation', 
                'act_organisation', 'con_orgcode',
                'ast_organisation'
            ),
            '!unit' => array (
                'inc_unit', 'ram_unit', 
                'pal_unit', 'com_unit', 
                'cla_unit', 'stn_unit', 
                'lib_unit', 'sab_unit', 
                'act_unit', 'con_unit', 
                'ast_unit'
            ),
            '!clinical_group' => array (
                'inc_clingroup', 'ram_clingroup',
                'pal_clingroup', 'com_clingroup',
                'cla_clingroup', 'stn_clingroup', 
                'lib_clingroup', 'sab_clingroup', 
                'act_clingroup', 'con_clingroup', 
                'ast_clingroup', 
            ),
            '!directorate' => array (
                'inc_directorate', 'ram_directorate',
                'pal_directorate', 'com_directorate', 
                'cla_directorate', 'stn_directorate', 
                'lib_directorate', 'sab_directorate', 
                'act_directorate', 'con_directorate',
                'ast_directorate'
            ),
            '!specialty' => array (
                'inc_specialty', 'ram_specialty',
                'pal_specialty', 'com_specialty', 
                'cla_specialty', 'stn_specialty', 
                'lib_specialty', 'sab_specialty', 
                'act_specialty', 'con_specialty',
                'ast_specialty'
            ),
            '!location_exact' => array (
                'inc_locactual', 'ram_locactual',
                'pal_locactual', 'com_locactual',
                'cla_locactual', 'stn_locactual',
                'lib_locactual', 'sab_locactual', 
                'act_locactual', 'con_locactual', 
                'ast_locactual'
                
            ),
            '!location_type' => array (
                'inc_loctype', 'ram_location', 
                'pal_loctype', 'com_loctype',
                'cla_location', 'stn_loctype',
                'lib_loctype', 'sab_loctype', 
                'act_loctype', 'con_loctype', 
                'ast_loctype'
            ),
            '!affecting_tier_zero' => array (
                'inc_affecting_tier_zero',
                'cla_affecting_tier_zero',
                'com_affecting_tier_zero'
            ),
            '!type_tier_one' => array (
                'inc_type_tier_one',
                'cla_type_tier_one',
                'com_type_tier_one'
            ),
            '!type_tier_two' => array (
                'inc_type_tier_two',
                'cla_type_tier_two',
                'com_type_tier_two'
            ),
            '!type_tier_three' => array (
                'inc_type_tier_three',
                'cla_type_tier_three',
                'com_type_tier_three'
            ),
            '!level_harm' => array (
                'inc_level_harm',
                'cla_level_harm'
            ),
            '!level_intervention' => array (
                'inc_level_intervention',
                'cla_level_intervention'
            )
        );

        return $list;
    }

    
    /* If this is a system wide field, change the fieldname
     * @return void 
     */
    public function useSystemWideFieldNames ()
    {
        $list = self::getSystemWideFieldsList();

        foreach ($list as $sysField => $fields) 
        {
            if (in_array ($this->fieldname, $fields)) 
            {
                $this->fieldname  = $sysField;
                $this->fieldtable = '';
            }
        }
    }
}
