<?php
 /**
 * Class used for importing code setups data in DATIX using XML data via files.
 * Similar to the importing option found in the Rich client.
 *
 */
class Setups_ImportCodes
{

    var $XMLDoc; //XML document to be loaded

    var $CodeTableName;     //Actual Code table name
    var $CodeFieldName;     //Code field in code table e.g. cod_code or code.
    var $CodeWhere;         //Where clause for code if using code table such as code_types.
    var $ImportMethod;      //Sets method of import: APPEND|UPDATE|OVERWRITE
    var $ErrorMsgs = array();   //Array used to store error messages whilst importing.

    /**
    * @desc Constructor - sets the details of the code setup table.
    *
    * @param string $Module The module this listing represents
    * @param int $ListingId The recordid of the listing in the database
    */
    function __construct($CodeTableName, $CodeFieldName, $CodeWhere, $CodeDescription, $ImportMethod)
    {
        $this->CodeTableName   = $CodeTableName;
        $this->CodeFieldName   = $CodeFieldName;
        $this->CodeWhere       = $CodeWhere;
        $this->CodeDescription = $CodeDescription;
        $this->ImportMethod    = $ImportMethod;
    }

    /**
     * Loads an XML document from a string.
     *
     * Takes a string containing an XML structure and loads this into a DOMDocument object
     * @param string $XMLdata String containing XML structure.
     */
    function LoadXMLDoc($XMLData)
    {
        $this->XMLDoc = new DOMDocument();
        if (!empty($XMLData) && preg_match('/\<CODES[^\>]*>/iu', $XMLData))// stripos($XMLData, "<CODES>") !== false)
        {
            libxml_use_internal_errors(true);
            if ($this->XMLDoc->loadXML(\UnicodeString::trim($XMLData)))
            {
                return true;
            }
        }

        $this->ErrorMsgs[] = "Invalid data file";
        return false;
    }

    /**
    * Function used to import data into main Datix tables
    *
    * Takes the XML object and imports data according to structure/elements/attributes of the XML object.
    */
    function importCodesXMLdata()
    {
        $xml = simplexml_import_dom($this->XMLDoc);

        foreach($xml as $table)
        {
            $code_table = $table->getName();

            if ($code_table != \UnicodeString::strtoupper($this->CodeTableName))
            {
                $this->ErrorMsgs[] = "No valid data for " . $this->CodeTableName . " table found in XML file";
                return false;
            }

            if ($this->ImportMethod == 'OVERWRITE')
            {
                $this->deletecodes();
            }

            foreach($table->CODE_TYPE as $record)
            {
                if (!$this->savecoderecord($record))
                {
                    $this->ErrorMsgs[] = "Unable to save table record for " . $this->CodeTableName;
                    return false;
                }
            }
        }
        return true;
    }
    /**
    * Function to delete codes before import when import method set to OVERWRITE
    *
    */
    function deletecodes()
    {
        $sql = "DELETE FROM " . $this->CodeTableName;
        if (!empty($this->CodeWhere))
        {
            $sql .= " WHERE " . $this->CodeWhere;
        }
        if ($this->CodeTableName == "code_types" && empty($this->CodeWhere))
        {
            return false; //Safety net for code_types if no WHERE clause has been set.
        }

        return DatixDBQuery::PDO_fetch($sql);
    }

    /**
    * Function to perform the insertion/update of code records depending on import method chosen,
    *
    * @param simpleXMLElement $record simpleXMLElement containing the data for a single code record.
    */
    function savecoderecord($record)
    {
        $columns = array();
        $values  = array();

        foreach($record as $column => $value)
        {
            $columns[] = $column;

            if ($column == 'COD_COST' && (string) $value == '')
            {
                $values[$column] = null;
            }
            else
            {
                $values[$column] = (string) $value;
            }

            if ($column == \UnicodeString::strtoupper($this->CodeFieldName))
            {
                $code_field_value = $value;
            }
        }

        // Code field value should not be more than 6 characters long
        // (Unless for codes without a code field)
        if (($this->CodeDescription != $this->CodeFieldName) && \UnicodeString::strlen($code_field_value) > 6)
        {
            return false;
        }

        $sql = "SELECT count(*) as checkcode FROM " . $this->CodeTableName . " WHERE " . $this->CodeFieldName . " = '" . $code_field_value . "'";
        if ($this->CodeTableName == "code_types")
        {
            $sql .= " AND cod_type = '" . $values['COD_TYPE'] . "'";
        }

        $checkrow = DatixDBQuery::PDO_fetch($sql);

        if ($checkrow["checkcode"] == 0)
        {
            // insert a new record
            $sql = 'INSERT INTO ' . $this->CodeTableName . ' ';
            $sql .= GeneratePDOInsertSQLFromArrays(
                    array(
                        'FieldArray' => $columns,
                        'DataArray' => $values
                    ),
                    $PDOParamsArray
                );
            DatixDBQuery::PDO_query($sql, $PDOParamsArray);
        }
        else if ($this->ImportMethod == 'UPDATE')
        {
            // update an existing record
            $sql = 'UPDATE ' .  $this->CodeTableName . ' SET ' ;
            $sql .= GeneratePDOSQLFromArrays(array(
                        'FieldArray' => $columns,
                        'DataArray' => $values
                    ),
                    $PDOParamsArray
                );

            $sql .= ' WHERE ' . $this->CodeFieldName . " = '" . $code_field_value . "'";
            if ($this->CodeTableName == "code_types")
            {
                $sql .= " AND cod_type = '" . $values['COD_TYPE'] . "'";
            }
            DatixDBQuery::PDO_query($sql, $PDOParamsArray);
        }

        return true;
    }
}
