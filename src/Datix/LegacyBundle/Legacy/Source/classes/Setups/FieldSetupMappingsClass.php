<?php
 /**
 * Class used for retrieving code setup mapping info from field_setup_mappings table
 *
 */
class Setups_FieldSetupMappings
{
    var $fsm_setup_table;
    var $fsm_setup_type;
    var $SetupMappings;

    function __construct($fsm_setup_table, $fsm_setup_type = "")
    {
        $this->fsm_setup_table = $fsm_setup_table;
        $this->fsm_setup_type = $fsm_setup_type;

        $sql = "SELECT * FROM field_setup_mappings WHERE fsm_setup_table = :fsm_setup_table";
        $PDOParmsArray['fsm_setup_table'] = $this->fsm_setup_table;

        if (!empty($this->fsm_setup_type))
        {
            $sql .= " AND fsm_setup_type = :fsm_setup_type";
            $PDOParmsArray['fsm_setup_type'] = $this->fsm_setup_type;
        }

        if(!bYN(GetParm('SHOW_NPSA_SETTINGS', 'Y')))
        {
            $sql .= " AND fsm_map_type != 'NRLS'";
        }

        if(!bYN(GetParm('SHOW_CFSMS_SETTINGS', 'Y')))
        {
            $sql .= " AND fsm_map_type != 'SIRS'";
        }

        $sql .= " ORDER BY fsm_order";

        $this->SetupMappings = DatixDBQuery::PDO_fetch_all($sql, $PDOParmsArray);
    }

}
