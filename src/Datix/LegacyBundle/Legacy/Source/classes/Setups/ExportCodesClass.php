<?php
 /**
 * Class used for exporting code setups data in DATIX using XML data via files.
 * Similar to the exporting option found in the Rich client.
 * 
 */
class Setups_ExportCodes 
{
    var $code_table;
    var $code_where;
    function __construct($code_table, $code_where)
    {
        $this->code_table = $code_table;
        $this->code_where = $code_where; 
    }
    /**
    * @desc returns an XML string in the same format used by the rich client for exporting codes.
    *
    * @param string $code_table Code table for field.
    * @param string $code_where WHERE clause if any used to select codes from code table
    *
    * @returns string Exported code data into XML format used by the Rich client for re-importing into a Datix databse.
    */
    function getCodeSetupsXML()
    {
        $sql = "SELECT * FROM " . $this->code_table;

        if (!empty($this->code_where))
        {
                $sql .= " WHERE " . $this->code_where;
        }
        $sql .= " ORDER BY 1 ASC";

        $resultcodes = DatixDBQuery::PDO_fetch_all($sql);

        $xmlstr = '<?xml version="1.0" encoding="UTF-8"?>
                    <CODES>
                    </CODES>';
        $xml = new SimpleXMLElement($xmlstr);

        $code_table_ele = $xml->addChild(\UnicodeString::strtoupper($this->code_table));

        foreach ($resultcodes as $row => $code_data)
        {
            $record_ele = $code_table_ele->addChild('CODE_TYPE');
            foreach ($code_data as $code_col => $code_col_val)
            {
                $code_col = \UnicodeString::strtoupper(Escape::EscapeEntities($code_col));
                $code_col_val = Escape::EscapeEntities($code_col_val, ENT_QUOTES);
                $code_col_ele = $record_ele->addChild($code_col, $code_col_val);
            }
        }

        return $xml->asXML();
    }
}
