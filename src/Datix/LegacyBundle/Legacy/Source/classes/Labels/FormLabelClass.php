<?php
/**
* @desc Object representing a Form label
*/
class Labels_FormLabel extends Labels_FieldLabel
{
    /**
    * Constructor - Calls the parent contstructor to set the base field label text before and 
    * then checks for a re-label in form design. If still no label found, then checks if field is UDF and 
    * retrieves UDF label.
    *
    * @param string $field The field to create the Label object for.
    * @param string $table The table name of field. Required for exact retrieval of field label.
    */
    public function __construct($FieldName, $FormFieldName, $Module, $table = '')
    {   
        parent::__construct($FieldName, $table, $Module);

        $tmpLabel = $this->getFormLabel($FieldName, $FormFieldName, $Module);
        
        //Check for default UDF label if none found in form design.
        if (empty($tmpLabel))
        {
            if (\UnicodeString::substr($FieldName,0,3) == 'UDF')
            {
                $UDFArray = explode('_',$FieldName);

                $tmpLabel = Labels_UDFLabel::GetUDFFieldLabel($UDFArray[3]);       
            }
        }
        
        if (!empty($tmpLabel))
        {       
            $this->setLabel($tmpLabel);  
        }     
    }
    /**
    * Check for form design label loaded into $GLOBALS["UserLabels"]
    * 
    * @param string $FieldName Field name 
    * @param string $FormFieldName Form field name
    */
    static function getFormLabel($FieldName, $FormFieldName = "")
    { 
        if ($FormFieldName)
        {
             $sTitle = $GLOBALS["UserLabels"][$FormFieldName];
        }
        else
        {
             $sTitle = $GLOBALS["UserLabels"][$FieldName];
        }
                 
        return $sTitle;   
    }
    
    /**
    * Replacement function for old GetFormFieldLabel found in subs.php, which still reamins as a wrapper function
    * for this static method.
    * 
    * @param string $FieldName Field name
    * @param string $DefaultTitle Default title if no label found
    * @param string $module Module short name
    * @param string $FormFieldName Form field name
    */
    static public function GetFormFieldLabel($FieldName, $DefaultTitle = "", $module = '', $FormFieldName = '', $table = '')
    {
       $FormLabelObj = new Labels_FormLabel($FieldName, $FormFieldName, $module, $table);
       $Label = $FormLabelObj->getLabel();
       
       return ($Label ? $Label : $DefaultTitle);  
    }
}  
