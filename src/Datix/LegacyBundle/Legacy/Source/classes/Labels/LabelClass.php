<?php
/**
* Base class for Labels
*/
abstract class Labels_Label
{
    protected $LabelText;  
    
    /**
    * Retrieves the LabelText set
    * 
    */
    public function getLabel()
    {
        return $this->LabelText;
    }
    
    /**
    * Sets the LabelText property.
    * 
    * @param string $NewLabelText Text for field label
    */
    public function setLabel($NewLabelText)
    {
        $this->LabelText = $NewLabelText;
    }
}  
