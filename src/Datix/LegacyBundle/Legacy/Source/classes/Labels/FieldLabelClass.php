<?php

use src\framework\registry\Registry;
use src\system\database\extrafield\ExtraField;
use src\system\database\FieldInterface;

/**
* @desc Object representing a field label
*/
class Labels_FieldLabel extends Labels_Label
{
    private $field; //Field name
    private $table; //Table name for field

    /**
    * @desc Constructor - Sets the label based on parameters passed and whether 'WEB_LABELS_ONLY' global set
    *
    * @param string $field The field to create the Label object for.
    * @param string $table The table name of field. Required for exact retrieval of field label.
    * @param bool $full If true, the "longer" name from field_formats/field_directory will be used, otherwise the shorter name from fielddefs.php
    */
    public function __construct($field, $table = '', $module = '', $long = false, $fftable = '')
    {
        $this->field = \UnicodeString::strtolower($field);
        $this->table = \UnicodeString::strtolower($table);

        //Check re-labeled fields in vw_field_formats
        if (!$_SESSION["WEB_LABELS_ONLY"])
        {
            $tmpLabel = $this->getVWFieldFormatsLabel($field, $module, $fftable);
        }

        //Check field_directory
        if (empty($tmpLabel))
        {
            $tmpLabel = $this->getFDRLabel($field, $table, $long);
        }

        //Check FieldDefs
        if (empty($tmpLabel))
        {
            $tmpLabel = $this->getFieldDefLabel($field, $module);
        }

        //stupid exception for link_treatment
        if ($field == 'link_treatment' && _tk('treatment_received') != '')
        {
            $tmpLabel = _tk('treatment_received');
        }

        $this->setLabel($tmpLabel);

    }

    /**
    * Retrieves label for field from field_directory table using the field and table names.
    *
    * @param string $Field The field to create the Label object for.
    * @param string $Table The table name of field. Required for exact retrieval of field label.
    * @return string Field label
    */
    static function getFDRLabel($Field, $Table, $Long = false)
    {
        //need to split any fieldnames of the form <table>.<field> such as incidents_main.recordid
        if(mb_substr_count($Field, '.') != 0)
        {
            $FieldNameSplit = explode('.', $Field, 2);
            $Field = $FieldNameSplit[1];
            $Table = $FieldNameSplit[0];
        }

        if(!isset($_SESSION['FDRLabels']))
        {
            $sql = 'SELECT fdr_label, LOWER(fdr_name) AS fdr_name, LOWER(fdr_table) AS fdr_table, tdr_desc
                    FROM field_directory JOIN table_directory ON fdr_table = tdr_name';

            $Labels = DatixDBQuery::PDO_fetch_all($sql);

            foreach($Labels as $Label)
            {
                $_SESSION['FDRLabels'][$Label['fdr_table']][$Label['fdr_name']] = $Label['fdr_label'];
                $_SESSION['FDRLabels']['NONE'][$Label['fdr_name']] = $Label['fdr_label'];

                $_SESSION['FDRLabelsLong'][$Label['fdr_table']][$Label['fdr_name']] = $Label['fdr_label'] . " (" . _tk($Label['tdr_desc']) . ")";
                $_SESSION['FDRLabelsLong']['NONE'][$Label['fdr_name']] = $Label['fdr_label'] . " (" . _tk($Label['tdr_desc']) . ")";
            }
        }

        if(empty($Table))
        {
            if ($Long === true)
            {
                return $_SESSION['FDRLabelsLong']['NONE'][$Field];
            }
            else
            {
                return $_SESSION['FDRLabels']['NONE'][$Field];
            }
        }
        else
        {
            if ($Long === true)
            {
                return $_SESSION['FDRLabelsLong'][$Table][$Field];
            }
            else
            {
                return $_SESSION['FDRLabels'][$Table][$Field];
            }
        }
    }

    /**
    * Retieves labels from vw_field_formats and caches values for specified module.
    * NB/ Only includes fields which have been re-labelled
    *
    * @param string $FieldName The field to create the Label object for.
    * @param sting $Module The Module name for the field
    * @return string
    */
    static function getVWFieldFormatsLabel($FieldName, $Module = "", $Table = "")
    {
        //Some module labels are stored under other module subforms (e.g Elements under STNELE).
        //Hardcoded here until we can get relabelling into a proper structure on the web side.
        if($Module == 'ELE' || ($Module == '' && preg_match('/^ele_/', $FieldName)))
        {
            $Table = 'STNELE';
            $Module = 'STN';
        }
        if($Module == 'PRO')
        {
            $Table = 'STNPRO';
            $Module = 'STN';
        }
        if($Module == 'COM' && substr($FieldName,0,4) == 'csu_')
        {
            $Table = 'COMSUB';
            $FieldName = 'com_'.substr($FieldName,4);
        }

        $ModuleKey = ($Module? $Module : 'NONE');
        $TableKey = ($Table? $Table : 'NONE');

        if (!isset($_SESSION["MAIN_RELABEL"][$ModuleKey][$TableKey]))
        {
            $sql = "SELECT fmt_field, fmt_new_label
                FROM vw_field_formats
                WHERE fmt_title!=fmt_new_label COLLATE SQL_Latin1_General_CP1_CS_AS";
            $PDOParams = array();

            if ($Module)
            {
                $sql .= " AND fmt_module = :fmt_module";
                $PDOParams["fmt_module"] = $Module;
            }

            if ($Table)
            {
                $sql .= " AND fmt_table = :fmt_table";
                $PDOParams["fmt_table"] = $Table;
            }

            $result = PDO_fetch_all($sql, $PDOParams);

            $_SESSION["MAIN_RELABEL"][$ModuleKey][$TableKey] = array(); //make sure we don't run this query repeatedly if there are no labels set.

            foreach ($result as $row )
            {
                $_SESSION["MAIN_RELABEL"][$ModuleKey][$TableKey][$row["fmt_field"]] = $row["fmt_new_label"];
            }
        }

        return $_SESSION["MAIN_RELABEL"][$ModuleKey][$TableKey][$FieldName];
    }

    /**
    * Gets field label for field by table/subform values
    *
    * @param string $FieldName Field name
    * @param string $Subform Subform/table for field
    * @return string Field label
    */
    static function getFieldFormatsLabelByForm($FieldName, $Subform)
    {
        static $cache = array ();

        // This is a top hack and should be removed when we drop the Rich Client or when we have a centralized
        // place for field definitions and labels on the web
        if ($FieldName == 'fin_estdamages' && $Subform == 'claims_main')
        {
            $sql = 'SELECT fdr_label FROM field_directory WHERE fdr_name = :fdr_name and fdr_table = :fdr_table';
            $result = DatixDBQuery::PDO_fetch($sql, array('fdr_name' => $FieldName, 'fdr_table' => $Subform));

            if ($result)
            {
                return $result['fdr_label'];
            }
        }

        if (empty($cache))
        {
            $sql = "SELECT fmt_table, fmt_field, fmt_new_label FROM vw_field_formats";
            $result = DatixDBQuery::PDO_fetch_all($sql);

            foreach ($result as $row )
            {
                $cache[$row['fmt_table']][$row["fmt_field"]] = $row["fmt_new_label"];
            }
        }

        return $cache[$Subform][$FieldName];
    }

    /**
    * Retrieves label for field from FieldDefs global array using the field and table names.
    *
    * @param string $FieldName The field to create the Label object for.
    * @param sting $Module The Module name for the field
    * @return sting $sTitle The label defined in FieldDefs
    */
    static function getFieldDefLabel($sFieldName, $Module = "")
    {
        global $FieldDefs;
        //need to split any fieldnames of the form <table>.<field> such as incidents_main.recordid
        if(mb_substr_count($sFieldName, '.') != 0)
        {
            $FieldNameSplit = explode('.', $sFieldName, 2);
            $FieldName = $FieldNameSplit[1];
        }
        else
        {
            $FieldName = $sFieldName;
        }

        if ($Module)
        {
            $sTitle = $FieldDefs[$Module][$FieldName]["Title"];
        }
        else
        {
            foreach ($FieldDefs as $Module => $Fields)
            {
                if ($Fields[$FieldName])
                {
                    $sTitle = $Fields[$FieldName]["Title"];
                    break;
                }
            }
        }
        return $sTitle;
    }

    /**
    * This static function can be used as a replacement for the old GetFieldLabel function (Still found in subs.php as a wrapper function for this one).
    *
    * @param mixed $FieldName The field to create the Label object for.
    * @param mixed $DefaultTitle Default title if no other is found
    * @param mixed $Table The table name of field. Required for exact retrieval of field label.
    * @param mixed $Module The Module name for the field
    * @param bool $full If true, the "longer" name from field_formats/field_directory will be used, otherwise the shorter name from fielddefs.php
    *
    * @return The label set for the Label object if not empty, otherwise the label specified as the default.
    */
    static public function GetFieldLabel($FieldName, $DefaultTitle = "", $Table = '', $Module = '', $long = false, $fftable = '')
    {
        $fieldDefs = Registry::getInstance()->getFieldDefs();
        if (null !== ($fieldDef = $fieldDefs[$FieldName]) && $fieldDef instanceof ExtraField)
        {
            $Label = $fieldDef->fld_name;
        }
        else
        {
            $FormLabelObj = new Labels_FieldLabel($FieldName, $Table, $Module, $long, $fftable);
            $Label = $FormLabelObj->getLabel();
        }
        
        return ($Label ? $Label : $DefaultTitle);
    }

    /**
    * Retrieves the table for a field based on the Subform entry
    *
    * @param string $Field Field name
    * @param string $Subform Subform value
    */
    static function getTableViaSubform($Field, $Subform)
    {
        $Field = \UnicodeString::strtolower($Field);
        $Table = "";

        //need to split any fieldnames of the form <table>.<field> such as incidents_main.recordid
        if(mb_substr_count($Field, '.') != 0)
        {
            $FieldNameSplit = explode('.', $Field, 2);
            $Field = $FieldNameSplit[1];
          //  $Table = $FieldNameSplit[0];
        }

        $sql = "SELECT sfm_tables FROM subforms WHERE sfm_form = :sfm_form";
        $result = DatixDBQuery::PDO_fetch($sql, array("sfm_form" => $Subform));

        if (!empty($result['sfm_tables']))
        {
            $tables_exploded = explode(",", $result['sfm_tables']);
            $fdr_tables = implode("','", $tables_exploded);

            $sql = "SELECT TOP 1 LOWER(fdr_table) AS fdr_table FROM field_directory
                WHERE fdr_name = :fdr_name AND fdr_table in ('" . $fdr_tables . "')";
            $FDRRecord = DatixDBQuery::PDO_fetch($sql, array("fdr_name" => $Field));
            $Table = $FDRRecord['fdr_table'];
        }

        return $Table;
    }
    
    /**
     * Uses a "FieldDef" object to get a field label.
     * 
     * @param FieldInterface $field
     */
    public static function getLabelFromFieldDef(FieldInterface $field)
    {
        if ($field instanceof ExtraField)
        {
            return static::GetFieldLabel($field->getName());
        }
        else
        {
            return static::GetFieldLabel($field->getName(), '', $field->fdr_table);
        }
    }
}
