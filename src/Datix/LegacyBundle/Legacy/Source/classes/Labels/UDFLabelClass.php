<?php
/**
* @desc Object representing a UDF label
*/
class Labels_UDFLabel
{       
    /**
    * Retrieves the label for a UDF field via the UDF ID
    * 
    * @param int $field_id ID of UDF field
    */
    static function getUDFLabelByID($field_id)
    { 
        $sql = 'SELECT fld_name FROM udf_fields WHERE recordid = :recordid';           
        $PDOParams = array('recordid' => $field_id);
    
        $LabelRow = DatixDBQuery::PDO_fetch($sql, $PDOParams);  

        $sTitle = $LabelRow['fld_name'];

        return $sTitle; 
    }
    
    /**
    * Replaces GetUDFFieldLabel in sub.php which checks if label for UDF field has been cached and uses that 
    * if found, but otherwise retrieves the default label defined in the udf tables.
    * 
    * @param int $id ID of UDF field  
    */
    static public function GetUDFFieldLabel($id)
    {
        //Use cached data if available
        if (isset($_SESSION["udf_fields"][$id]["fld_name"]))
        {
            $Title = $_SESSION["udf_fields"][$id]["fld_name"];
        }
        else
        {
            $_SESSION["udf_fields"][$id]["fld_name"] = $Title = self::getUDFLabelByID($id);
        }

        return $Title;
    }
} 
