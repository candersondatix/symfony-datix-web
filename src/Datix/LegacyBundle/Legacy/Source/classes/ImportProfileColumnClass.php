<?php

class ImportProfileColumn
{

    protected $ID;
    public $Data;
    public $MappingData;

    protected $MappingConfig;

    public function __construct($recordid)
    {
        $this->ID = $recordid;

        $this->Data = DatixDBQuery::PDO_fetch('SELECT exc_col_letter, exc_dest_field, exc_id_field, exc_import_order, exc_ignore_nulls FROM excel_import WHERE recordid = :recordid', array('recordid' => $this->ID));

        $Mappings = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM excel_data_trans WHERE edt_import_field_id = :recordid', array('recordid' => $this->ID), PDO::FETCH_COLUMN);

        foreach($Mappings as $MappingRecordid)
        {
            $Mapping = new ImportProfileColumnMapping($MappingRecordid);
            $this->MappingConfig[$Mapping->getOldVal()] = $Mapping->getNewVal();
            $this->MappingData[] = $Mapping->Data;
        }
    }

    public function getMappingConfig()
    {
        return $this->MappingConfig;
    }
}
