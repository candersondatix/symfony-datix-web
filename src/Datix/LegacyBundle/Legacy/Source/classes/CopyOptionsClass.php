<?php

/**
* @desc Models the options available for copying modules records.
*/
class CopyOptions
{
    public $Module;   //Module short name
    public $join;
    public $where;
    public $copies;
    public $copy_linked_contacts;
    public $copy_actions;
    public $copy_documents;
    public $copy_extra_fields;
    public $copy_notepad;
    public $copy_link_to_master;
    public $copy_scores;
    public $copy_comments;
    public $copy_evidence;
    public $copy_evidence_link_notes;
    public $copy_linked_assets;
    public $copy_medications;
    public $copy_causal_factors;

    /**
    * @desc Constructor: Assigns parameter values to object properties.
    *
    * @param array $Parameters The array of parameters to be assigned.
    */
    public function __construct($Parameters)
    {
        $this->Module = $Parameters['module'];
        $this->join = $Parameters['join'];
        $this->where = $Parameters['where'];
        $this->copies = ($Parameters['copies'] > 0 ? $Parameters['copies'] : 1);
        $this->copy_linked_contacts = $Parameters['copy_linked_contacts'];
        $this->copy_actions = $Parameters['copy_actions'];
        $this->copy_documents = $Parameters['copy_documents'];
        $this->copy_extra_fields = $Parameters['copy_extra_fields'];
        $this->copy_notepad = $Parameters['copy_notepad'];
        $this->copy_link_to_master = $Parameters['copy_link_to_master'];
        $this->copy_scores = $Parameters['copy_scores'];
        $this->copy_comments = $Parameters['copy_comments'];
        $this->copy_evidence = $Parameters['copy_evidence'];
        $this->copy_evidence_link_notes = $Parameters['copy_evidence_link_notes'];
        $this->copy_linked_assets = $Parameters['copy_linked_assets'];
        $this->copy_medications = $Parameters['copy_medications'];
        $this->override_values = $Parameters['OVERRIDE_VALUES'];
        $this->copy_causal_factors = $Parameters['copy_causal_factors'];
    }
}

?>