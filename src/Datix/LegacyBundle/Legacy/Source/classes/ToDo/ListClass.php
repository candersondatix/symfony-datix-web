<?php

use src\framework\registry\Registry;

/**
* Represents a ToDo List
*/
class ToDo_List
{
    /**
    * Stores SQL to generate ToDo List
    *
    * @var string
    */
    private $ToDoSQL;

    /**
    * Array of SQL required for generating list of ToDo items for each module.
    *
    * @var string array
    */
    private $dbtablelist = array();
    
    /**
     * The application registry.
     * 
     * @var Registry
     */
    protected $registry;

    /**
    * Constructor. Generates and sets SQL in ToDoSQL property
    *
    * @param string array $modules List of module short names required for the ToDo list
    * @param string $ListType List type specified applies filters on the ToDo list results.
    * @return ToDo_List
    */
    public function __construct(array $modules, $ListType, Registry $registry = null)
    {
        $this->registry = $registry ?: Registry::getInstance();
        
        if (is_array($modules))
        {
            foreach ($modules as $module)
            {
                self::getModuleToDoSQL($module, $ListType);
            }
        }

        if (!empty($this->dbtablelist))
        {
            $this->ToDoSQL = "(" . implode(" UNION ALL ", $this->dbtablelist) . ") todolist";
        }
        else
        {
            //Provide dummy table SQL if no config set for ToDo list.
            $this->ToDoSQL = "(SELECT NULL as recordid, NULL as tod_descr, NULL as tod_module, NULL as tod_type, NULL as tod_mod, NULL as tod_due WHERE 1=2) todolist";
        }
    }

    /**
    * Returns a SQL to return all ToDo items for specified modules
    *
    */
    public function getToDoSQL()
    {
        return $this->ToDoSQL;
    }

    /**
    * Routes generation of SQL for module ToDo lists
    *
    * @param string $mod Modules short name
    * @param string $listType List type filter
    */
    private function getModuleToDoSQL($mod, $listType)
    {
        switch($mod)
        {
            case 'ACT':
                self::getActionsToDo($listType);
                break;
            case 'INC':
                self::getIncidentsToDo($listType);
                break;
            case 'COM':
                self::getComplaintsToDo($listType);
                break;
            case 'RAM':
                self::getRisksToDo($listType);
                break;
            case 'CQO':
                self::getCQCToDo('CQO', $listType);
                break;
            case 'CQP':
                self::getCQCToDo('CQP', $listType);
                break;
            case 'CQS':
                self::getCQCToDo('CQS', $listType);
                break;
        }

    }

    /**
    * Generates SQL for Actions part of ToDo list and appends to dbtablelist array property
    *
    * @param string $ListType List type filter
    */
    public function getActionsToDo($ListType)
    {
        require_once 'Source/libs/SearchSymbols.php';

        $TOD_USERS_ACT = GetParm("TOD_USERS_ACT", "");

        //If not set exit
        if (empty($TOD_USERS_ACT))
        {
            return;
        }

        //Only include licensed modules
        $ACTWhereClause[] = "(act_module IN ('" . implode("', '", getActionLinkModules()) . "'))";
        if ($ListType == "overdue")
        {
            $ACTWhereClause[] = "act_ddone is null and act_ddue is not null and act_ddue < '" . date('Y-m-d') . "'";
        }
        else if ($ListType == "duetoday")
        {
            $ACTWhereClause[] = "act_ddone is null and act_ddue is not null and act_ddue <= '" . date('Y-m-d') . "'";
        }
        else if ($ListType == "duethisweek")
        {
            GetWeekDateRange(getdate(), $dtStart, $dtEnd);
            $WeekEndDate = strftime("%Y-%m-%d", $dtEnd);
            $ACTWhereClause[] = "act_ddone is null and act_ddue is not null and act_ddue <= '" . $WeekEndDate . "'";
        }
        else if ($ListType == "duethismonth")
        {
            GetMonthDateRange(getdate(), $dtStart, $dtEnd);
            $MonthEndDate = strftime("%Y-%m-%d", $dtEnd);
            $ACTWhereClause[] = "act_ddone is null and act_ddue is not null and act_ddue <= '" . $MonthEndDate . "'";
        }
        else
        {
            $ACTWhereClause[] = "act_ddone is null and act_ddue is not null";
        }
        $ACTUserFields = explode(",", $TOD_USERS_ACT);
        foreach ($ACTUserFields as $i => $ToInitsField)
        {
            $ACTUserFieldsSQLArray[] = "$ToInitsField = '". $_SESSION["initials"] . "'";
        }
        $ACTUserFieldsSQL = implode(" OR ", $ACTUserFieldsSQLArray);
        $ACTWhereClause[] = "(" . $ACTUserFieldsSQL . ")";
        $ACTWhereClause = MakeSecurityWhereClause($ACTWhereClause, "ACT", $_SESSION["initials"]);

        $this->dbtablelist[] = "(SELECT recordid, act_descr as tod_descr,
                             '" . _tk('ACTNamesTitle') . "' as tod_module,
                            ((SELECT description FROM code_ra_acttype WHERE code = ca_actions.act_type) + ' ('
                            + (SELECT mod_title FROM modules WHERE mod_module = act_module) + ')') as tod_type,
                            'ACT' as tod_mod,
                             act_ddue as tod_due
                            FROM ca_actions WHERE " . $ACTWhereClause. ")";

    }

    /**
    * Generates SQL for Incidents part of ToDo list and appends to dbtablelist array property
    *
    * @param string $ListType List type filter
    */
    public function getIncidentsToDo($ListType)
    {
        global $FieldDefs;
        
        $ModuleDefs    = $this->registry->getModuleDefs();
        $TOD_USERS_INC = $this->registry->getParm('TOD_USERS_INC');

        // If not set exit
        if (empty($TOD_USERS_INC))
        {
            return;
        }

        $dtToday = getdate();

        $Statuses = getOverdueStatuses(array('module' => 'INC'));
        
        if ($this->registry->getParm($ModuleDefs['INC']['OVERDUE_DAY_GLOBAL'], 14) != -1)
        {
            $Statuses[''] = 'None';
        }
        
        foreach ($Statuses as $Status => $name)
        {
            $IncidentsOverDueSQL  = null;
            $OverdueDays[$Status] = GetOverdueDays('INC', $Status);
            
            //getOverdueStatuses still returns statuses with an overdue of -1 if the "overall" overdue days setting is not -1
            //so we need to filter them out here to prevent them from always appearing on the todo list.
            if ($OverdueDays[$Status][$Status] != -1)
            {
                $workingDays = false;
                if ((empty($Status) && $OverdueDays['']['overall_type'] == 'W') || $OverdueDays[$Status][$Status.'_type'] == 'W')
                {
                    // we're calculating the overdue date based on working days
                    $workingDays = true;
                }
                
                $overdueOn = new DateTime();
                
                switch ($ListType)
                {
                    case 'overdue':
                    case 'duetoday':
                        if ($workingDays)
                        {
                            // records cannot become overdue on the weekend if using working days, so refer to previous friday
                            $today = date('w');
                            if ($today == 6) // saturday
                            {
                                $overdueOn->modify('friday this week');
                            }
                            if ($today == 0) // sunday
                            {
                                // sunday is considered the start of the week, so we have to look at last friday
                                $overdueOn->modify('friday last week');
                            }
                        }
                        break;
                        
                    case 'duethisweek':
                        $today = date('w');
                        if ($workingDays)
                        {
                            // if we're using working days then the latest the record can be overdue is friday this week
                            if ($today == 0)
                            {
                                // sunday is considered the start of the week, so we have to look at last friday
                                $overdueOn->modify('friday last week');
                            }
                            else
                            {
                                $overdueOn->modify('friday this week');
                            }
                        }
                        else
                        {
                            if ($today != 0)
                            {
                                // gives you next sunday if today is sunday, for some reason...
                                $overdueOn->modify('sunday this week');
                            }
                        }
                        break;
                        
                    case 'duethismonth':
                        if ($workingDays)
                        {
                            // if we're using working days then the latest the record can be overdue is the last Friday of the month
                            $overdueOn->modify('last friday of this month');
                        }
                        else
                        {
                            $overdueOn->modify('last day of this month');
                        }
                        $overdueOn->modify('last day of this month');
                        break;
                }
                
                $IncidentsOverDueSQL = getOverdueSQL('INC', $Status, $overdueOn, $Status != '');

                if ($ListType == '')
                {
                    // displaying all records
                    $IncidentsOverDueSQL["where"] = "incidents_main.inc_dsched is null";
                }
                elseif ($ListType == 'overdue')
                {
                    // Make sure that records with closed date populated don't appear in the todo list
                    $IncidentsOverDueSQL['where'] = 'incidents_main.inc_dsched IS NULL AND '.$IncidentsOverDueSQL['where'];
                }
            }

            if (!empty($IncidentsOverDueSQL))
            {
                if (!empty($Status))
                {
                    $IncidentsOverDueSQL["where"] = "incidents_main.rep_approved like '$Status' AND " . $IncidentsOverDueSQL["where"];
                }
                $INCOverdueWhereClause[$Status] = $IncidentsOverDueSQL["where"];
                $INCOverdueJoin[$Status] = $IncidentsOverDueSQL["join"];
            }
        }

        $INCUserFields = explode(",", $TOD_USERS_INC);
        foreach ($INCUserFields as $i => $ToInitsField)
        {
            if ($FieldDefs['INC'][$ToInitsField]["Type"] = "multilistbox")
            {
                $initials = $_SESSION["initials"];
                // searching on multicoded fields
                $search = "($ToInitsField = '$initials'";
                $search .= " OR $ToInitsField like '$initials %'";
                $search .= " OR $ToInitsField like '% $initials'";
                $search .= " OR $ToInitsField like '% $initials %')";
                $INCUserFieldsSQLArray[] = $search;
            }
            else
            {
                $INCUserFieldsSQLArray[] = "$ToInitsField = '". $_SESSION["initials"] . "'";
            }
        }
        $INCUserFieldsSQL = implode(" OR ", $INCUserFieldsSQLArray);

        if (!empty($INCOverdueWhereClause))
        {
            foreach ($INCOverdueWhereClause as $Status => $INC_OverdueWhere)
            {
                $OverdueDaysLimit = $OverdueDays[$Status][$Status] ? $OverdueDays[$Status][$Status] : $OverdueDays[$Status]["overall"];
                
                $workingDays = false;
                if ((empty($Status) && $OverdueDays['']['overall_type'] == 'W') || $OverdueDays[$Status][$Status.'_type'] == 'W')
                {
                    // we're calculating the overdue date based on working days
                    $workingDays = true;
                }
                
                $INCWhereClause = array();
                $INCWhereClause[] = "(" . $INCUserFieldsSQL . ")";
                $INCWhereClause[] = $INC_OverdueWhere;
                $INCWhereClause[] = "incidents_main.rep_approved != 'REJECT' AND incidents_main.rep_approved != 'FA'";

                $INCWhereClause = MakeSecurityWhereClause($INCWhereClause, "INC", $_SESSION["initials"]);
                if (!empty($Status))
                {
                    if ($INCOverdueJoin[$Status]) // due date is calculated from the date the record was moved to the current approval status
                    {
                        if ($workingDays)
                        {
                            $dueDateField = "CASE WHEN auditdate IS NULL THEN dbo.CalculateWorkingDays(inc_dreported, $OverdueDaysLimit, 'ADD') ELSE dbo.CalculateWorkingDays(auditdate, $OverdueDaysLimit, 'ADD') END AS tod_due";
                        }
                        else
                        {
                            $dueDateField = "CASE WHEN auditdate IS NULL THEN inc_dreported + $OverdueDaysLimit ELSE auditdate + $OverdueDaysLimit END AS tod_due";
                        }
                    }
                    else // due date calculated from reported date
                    {
                        if ($workingDays)
                        {
                            $dueDateField = "dbo.CalculateWorkingDays(inc_dreported, $OverdueDaysLimit, 'ADD') AS tod_due";
                        }
                        else
                        {
                            $dueDateField = "inc_dreported + $OverdueDaysLimit AS tod_due";
                        }
                    }

                    //We need to provide relabelled names for approval status codes.
                    $approvalStatusCodes = Registry::getInstance()->getFieldDefs()['incidents_main.rep_approved']->getCodes();
                    $approvalStatusSelect = 'SELECT CASE incidents_main.rep_approved ';
                    foreach($approvalStatusCodes as $code)
                    {
                        $approvalStatusSelect .= 'WHEN \''.$code->code.'\' THEN \''.$code->description.'\' ';
                    }
                    $approvalStatusSelect .= 'END';

                    $this->dbtablelist[] = "(SELECT recordid, inc_name as tod_descr, '". _tk('INCNamesTitle') . "' as tod_module,
                            (".$approvalStatusSelect.") as tod_type,
                            'INC' as tod_mod,
                            $dueDateField
                            FROM incidents_main " . ($INCOverdueJoin[$Status] ? $INCOverdueJoin[$Status] : "") . " WHERE " . $INCWhereClause. ")";
                }
                else
                {
                    if ($workingDays)
                    {
                        $dueDateField = "dbo.CalculateWorkingDays(inc_dreported, $OverdueDaysLimit, 'ADD') AS tod_due";
                    }
                    else
                    {
                        $dueDateField = "inc_dreported + $OverdueDaysLimit AS tod_due";
                    }
                    
                    $this->dbtablelist[] = "(SELECT recordid, inc_name as tod_descr, '". _tk('INCNamesTitle') . "' as tod_module,
                            'Overdue Overall' as tod_type,
                            'INC' as tod_mod,
                            $dueDateField
                            FROM incidents_main " . ($INCOverdueJoin[$Status] ? $INCOverdueJoin[$Status] : "") . " WHERE " . $INCWhereClause. ")";
                }
            }
        }
    }

    /**
    * Generates SQL for Complaints part of ToDo list and appends to dbtablelist array property
    *
    * @param string $ListType List type filter
    */
    public function getComplaintsToDo($ListType)
    {
        global $ModuleDefs, $FieldDefs;

        $TOD_USERS_COM = GetParm("TOD_USERS_COM", "");

        //If not set exit
        if (empty($TOD_USERS_COM))
        {
            return;
        }

        $COMUserFields = explode(",", $TOD_USERS_COM);
        foreach ($COMUserFields as $i => $ToInitsField)
        {
            if ($FieldDefs['COM'][$ToInitsField]["Type"] = "multilistbox")
            {
                $initials = $_SESSION["initials"];
                // searching on multicoded fields
                $search = "($ToInitsField = '$initials'";
                $search .= " OR $ToInitsField like '$initials %'";
                $search .= " OR $ToInitsField like '% $initials'";
                $search .= " OR $ToInitsField like '% $initials %')";
                $COMUserFieldsSQLArray[] = $search;
            }
            else
            {
                $COMUserFieldsSQLArray[] = "$ToInitsField = '". $_SESSION["initials"] . "'";
            }
        }
        $COMUserFieldsSQL = implode(" OR ", $COMUserFieldsSQLArray);

        $ModuleToDisplay = "COM";
        if (is_array($ModuleDefs[$ModuleToDisplay]['HARD_CODED_LISTINGS']))
        {
            foreach ($ModuleDefs[$ModuleToDisplay]['HARD_CODED_LISTINGS'] as $Listing => $Details)
            {
                $OverdueDateField = $Details['OverdueDateField'];
                $COMWhereClause = array();
                //$NumRecords = CountRecordsGeneric($ModuleToDisplay, $Details['Where']);

              //  $NumRecordsOverdue = 0;
                if (!empty($Details['OverdueWhere']))
                {
                    if ($ListType == "overdue")
                    {
                        $Details['OverdueWhere'] = str_replace('@TODAY', date('Y-m-d'), $Details['OverdueWhere']);
                    }
                    else if ($ListType == "duetoday")
                    {
                        $date = new DateTime();
                        date_add($date, new DateInterval("P1D"));
                        $Details['OverdueWhere'] = str_replace('@TODAY', $date->format('Y-m-d'), $Details['OverdueWhere']);
                    }
                    else if ($ListType == "duethisweek")
                    {
                        GetWeekDateRange(getdate(), $dtStart, $dtEnd);
                        $WeekEndDate = strftime("%Y-%m-%d", strtotime('+1 day', $dtEnd));
                        $Details['OverdueWhere'] = str_replace('@TODAY', $WeekEndDate, $Details['OverdueWhere']);
                    }
                    else if ($ListType == "duethismonth")
                    {
                        GetMonthDateRange(getdate(), $dtStart, $dtEnd);
                        $MonthEndDate = strftime("%Y-%m-%d", strtotime('+1 day', $dtEnd));
                        $Details['OverdueWhere'] = str_replace('@TODAY', $MonthEndDate, $Details['OverdueWhere']);
                    }
                    else
                    {
                        $Details['OverdueWhere'] = str_replace('< \'@TODAY\'', "IS NOT NULL", $Details['OverdueWhere']);
                       // $Details['OverdueWhere'] = "$OverdueDateField is not null";
                    }
                 //   $COMWhereClause[] = "(" . $COMUserFieldsSQL . ")";
                    $COMWhereClause[] = $Details['OverdueWhere'];

                    $COMWhereClause = MakeSecurityWhereClause($COMWhereClause, "COM", $_SESSION["initials"]);
                    $this->dbtablelist[] = "(SELECT compl_main.recordid, com_name as tod_descr, '". _tk('COMNamesTitle') . "' as tod_module,
                            '". str_replace ("'", "''", $Details['Title']) . "' as tod_type,
                            'COM' as tod_mod,
                            $OverdueDateField as tod_due
                            FROM compl_main LEFT JOIN link_compl ON compl_main.recordid = link_compl.com_id AND link_compl.lcom_current = 'Y'
                            AND link_compl.recordid = (SELECT top 1 link_compl.recordid FROM link_compl WHERE
                                                                            link_compl.com_id = compl_main.recordid AND link_compl.lcom_current='Y'
                                                                            order by lcom_primary desc, link_compl.recordid)
                            AND con_id in (SELECT recordid FROM contacts_main)
                            WHERE ($COMUserFieldsSQL) AND " .
                            $COMWhereClause. ")";
                }
            }
        }
    }

    /**
    * Generates SQL for Risks part of ToDo list and appends to dbtablelist array property
    *
    * @param string $ListType List type filter
    */
    public function getRisksToDo($ListType)
    {
        $TOD_USERS_RAM = GetParm("TOD_USERS_RAM", "");

        //If not set exit
        if (empty($TOD_USERS_RAM))
        {
            return;
        }

        if ($ListType == "overdue")
        {
            $RAMWhereClause[] = "ram_dreview is not null and ram_dreview < '" . date('Y-m-d') . "'";
        }
        else if ($ListType == "duetoday")
        {
            $RAMWhereClause[] = "ram_dreview is not null and ram_dreview <= '" . date('Y-m-d') . "'";
        }
        else if ($ListType == "duethisweek")
        {
            GetWeekDateRange(getdate(), $dtStart, $dtEnd);
            $WeekEndDate = strftime("%Y-%m-%d", $dtEnd);
            $RAMWhereClause[] = "ram_dreview is not null and ram_dreview <= '" . $WeekEndDate . "'";
        }
        else if ($ListType == "duethismonth")
        {
            GetMonthDateRange(getdate(), $dtStart, $dtEnd);
            $MonthEndDate = strftime("%Y-%m-%d", $dtEnd);
            $RAMWhereClause[] = "ram_dreview is not null and ram_dreview <= '" . $MonthEndDate . "'";
        }
        else
        {
            $RAMWhereClause[] = "ram_dreview is not null";
        }
        $RAMWhereClause[] = "ram_dclosed is null";
        $RAMWhereClause[] = "ra_main.rep_approved != 'REJECT'";
        $RAMUserFields = explode(",", $TOD_USERS_RAM);
        foreach ($RAMUserFields as $i => $ToInitsField)
        {
            $RAMUserFieldsSQLArray[] = "$ToInitsField = '". $_SESSION["initials"] . "'";
        }
        $RAMUserFieldsSQL = implode(" OR ", $RAMUserFieldsSQLArray);
        $RAMWhereClause[] = "(" . $RAMUserFieldsSQL . ")";
        $RAMWhereClause = MakeSecurityWhereClause($RAMWhereClause, "RAM", $_SESSION["initials"]);

        $this->dbtablelist[] = "(SELECT recordid, ram_description as tod_descr, '". _tk('RAMNamesTitle') . "' as tod_module,
                            'Review Risk' as tod_type,
                            'RAM' as tod_mod,
                             ram_dreview as tod_due
                            FROM ra_main WHERE " . $RAMWhereClause. ")";
    }

    /**
    * Generates SQL for CQC module part of To-Do list and appends to dbtablelist array property
    * SQL will be based on the submodule contained in $cqcModule
    *
     * @param string $cqcModule Will contain CQO, CQP or CQS depending on which portion of the cqc module to-do list is being constructed.
    * @param string $listType List type filter
    */
    public function getCQCToDo($cqcModule, $listType)
    {
        $moduleDefs = Registry::getInstance()->getModuleDefs();

        $cqcUsers = GetParm("TOD_USERS_CQC", "");

        //If not set, CQC modules should not appear on the to-do list.
        if (empty($cqcUsers))
        {
            return;
        }

        $fieldReference = [
            'CQO' => [
                'comparisonDate' => 'cdo_dnextreview',
                'handlers' => 'cdo_handlers',
                'manager' => 'cdo_manager',
                'description' => 'cto_desc',
            ],
            'CQP' => [
                'comparisonDate' => 'cdp_dnextreview',
                'handlers' => 'cdp_handlers',
                'manager' => 'cdp_manager',
                'description' => 'ctp_desc',
            ],
            'CQS' => [
                'comparisonDate' => 'cds_dnextreview',
                'handlers' => 'cds_handlers',
                'manager' => 'cds_manager',
                'description' => 'cts_desc',
            ],
        ];

        //restrict the cqc records returned to particular categories of "overdue"
        if ($listType == "overdue")
        {
            $cqcWhereClause[] = $fieldReference[$cqcModule]['comparisonDate']." is not null and ".$fieldReference[$cqcModule]['comparisonDate']." < '" . date('Y-m-d') . "'";
        }
        else if ($listType == "duetoday")
        {
            $cqcWhereClause[] = $fieldReference[$cqcModule]['comparisonDate']." is not null and ".$fieldReference[$cqcModule]['comparisonDate']." <= '" . date('Y-m-d') . "'";
        }
        else if ($listType == "duethisweek")
        {
            GetWeekDateRange(getdate(), $dtStart, $dtEnd);
            $weekEndDate = strftime("%Y-%m-%d", $dtEnd);
            $cqcWhereClause[] = $fieldReference[$cqcModule]['comparisonDate']." is not null and ".$fieldReference[$cqcModule]['comparisonDate']." <= '" . $weekEndDate . "'";
        }
        else if ($listType == "duethismonth")
        {
            GetMonthDateRange(getdate(), $dtStart, $dtEnd);
            $monthEndDate = strftime("%Y-%m-%d", $dtEnd);
            $cqcWhereClause[] = $fieldReference[$cqcModule]['comparisonDate']." is not null and ".$fieldReference[$cqcModule]['comparisonDate']." <= '" . $monthEndDate . "'";
        }
        else
        {
            $cqcWhereClause[] = $fieldReference[$cqcModule]['comparisonDate']." is not null";
        }

        //depending on a global, handlers or managers may need to see items in the to-do list.
        $cqcStaffClause = [];
        $cqcUserFields = explode(",", $cqcUsers);
        if (in_array('cqc_handler', $cqcUserFields))
        {
            $cqcStaffClause[] = '('.
                $fieldReference[$cqcModule]['handlers']." LIKE '% ". $_SESSION["initials"] . " %' OR ".
                $fieldReference[$cqcModule]['handlers']." LIKE '% ". $_SESSION["initials"] . "' OR ".
                $fieldReference[$cqcModule]['handlers']." LIKE '". $_SESSION["initials"] . " %' OR ".
                $fieldReference[$cqcModule]['handlers']." = '". $_SESSION["initials"] . "')";
        }
        if (in_array('cqc_manager', $cqcUserFields))
        {
            $cqcStaffClause[] = $fieldReference[$cqcModule]['manager']." = '". $_SESSION["initials"] . "'";
        }
        $cqcWhereClause[] = '('.implode(' OR ', $cqcStaffClause).')';

        $cqcWhereClause = implode(' AND ', $cqcWhereClause);

        $cqcWhereClause = MakeSecurityWhereClause($cqcWhereClause, $cqcModule, $_SESSION["initials"]);

        $this->dbtablelist[] = "(SELECT recordid, ".$fieldReference[$cqcModule]['description']." as tod_descr, '". $moduleDefs[$cqcModule]['NAME'] . "' as tod_module,
                            'Review Needed' as tod_type,
                            '".$cqcModule."' as tod_mod,
                             ".$fieldReference[$cqcModule]['comparisonDate']." as tod_due
                            FROM ".$moduleDefs[$cqcModule]['VIEW']." WHERE " . $cqcWhereClause. ")";

    }
}
