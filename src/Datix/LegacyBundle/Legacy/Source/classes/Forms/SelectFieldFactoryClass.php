<?php
class Forms_SelectFieldFactory
{
    /**
    * Creates and returns either a new single- or multi-select field object.
    *
    * @param string  $name
    * @param string  $module
    * @param string  $value
    * @param string  $mode
    * @param boolean $multi
    * @param string  $label
    * @param string  $formSection
    * @param string  $changedValue
    * @param string  $suffix
    *
    * @return Forms_SelectField/Forms_MultiSelectField
    */
    public static function createSelectField($name, $module, $value, $mode, $multi = false, $label = '', $formSection = '', $changedValue = '', $suffix = '', $newId = '')
    {
    	if($name == 'ati_location_additional' || $name == 'con_hier_location')
        {
    		return new Forms_MultiSelectFieldLocations($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix, $newId);
    	}
    	
        if ($multi || $mode == 'Search')
        {
            return new Forms_MultiSelectField($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix, $newId);
        }
        else
        {
            return new Forms_SelectField($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix, $newId);
        }
    }
}