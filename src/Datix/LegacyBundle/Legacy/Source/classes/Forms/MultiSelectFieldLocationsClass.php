<?php
class Forms_MultiSelectFieldLocations extends Forms_MultiSelectField
{

    /**
    * Retrieves the description for fields with tiers hierarchy
    *
    * @param  string  $code         The code we're fetching the description for.
    *
    * @return string  $description
    */
	protected function getDescription($code)
	{
		return getLocationParentage($code);
	}
}