<?php
class Forms_RadioButtonsFieldFactory
{
    /**
    * Creates and returns either a new radio buttons field object.
    *
    * @param string  $name
    * @param string  $module
    * @param string  $value
    * @param string  $mode
    * @param boolean $multi
    * @param string  $label
    * @param string  $formSection
    * @param string  $changedValue
    * @param string  $suffix
    * @param boolean $mandatory
    *
    * @return Forms_RadioButtonsField
    */
    public static function createField($OriginalName, $module, $value, $mode, $label = '', $formSection = '', $changedValue = '', $suffix = '', $mandatory = false)
    {
        // get data for radio buttons
        require_once __DIR__.'/../../libs/CodeSelectionCtrl.php';
        $udf = (\UnicodeString::substr($OriginalName, 0, 3) == 'UDF');
        $bindArray = array ();
        $name = CheckFieldMappings($module, $OriginalName, true);
        $sql = GetCodeListSQL($module, $name, '', false, $udf, false, array(), $label, $bindArray);

        $result = PDO_fetch_all($sql, $bindArray);

        $list = array ();
        foreach ($result as $row) {
            $list[$row['value']] = $row['description'];
        }

        // create field and inject data
        $field = new Forms_RadioButtonsField ($OriginalName, $module, $value, $mode, $label, $formSection, $changedValue, $suffix);
        $field->setValues ($list);
        $field->setMandatory ($mandatory);

        return $field;
    }
    
    
    public static function MakeYesNoSelect ($name, $module, $value, $mode, $label = '', $formSection = '', $changedValue = '', $suffix = '', $mandatory = false)
    {
        
        $list = array('Y' => _tk('yes'), 'N' => _tk('no'));
        
        // create field and inject data
        $field = new Forms_RadioButtonsField ($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix);
        $field->setValues ($list);
        $field->setMandatory ($mandatory);
        
        return $field;
        
    }
    
}