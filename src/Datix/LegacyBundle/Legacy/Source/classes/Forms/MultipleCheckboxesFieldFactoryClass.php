<?php
class Forms_MultipleCheckboxesFieldFactory
{
    /**
    * Creates and returns either a new multiple-checkbox field object.
    *
    * @param string  $name
    * @param string  $module
    * @param string  $value
    * @param string  $mode
    * @param string  $label
    * @param string  $formSection
    * @param string  $changedValue
    * @param string  $suffix
    *
    * @return Forms_MultiSelectField
    */
    public static function createField ($name, $module, $value, $mode, $label = '', $formSection = '', $changedValue = '', $suffix = '')
    {
        // get data for checkboxes
        require_once __DIR__.'/../../libs/CodeSelectionCtrl.php';
        $udf = (\UnicodeString::substr($name, 0, 3) == 'UDF'); 
        $bindArray = array ();
        $sql = GetCodeListSQL($module, $name, '', false, $udf, false, array(), $label, $bindArray);

        $result = PDO_fetch_all($sql, $bindArray);

        $list = array ();
        foreach ($result as $row) {
            $list[$row['value']] = $row['description'];
        }

        // create field and inject data
        $field = new Forms_MultipleCheckboxesField ($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix);
        $field->setValues ($list);

        return $field;
    }
}