<?php
/**
 * Multiple checkboxes field class.
 */
class Forms_MultipleCheckboxesField extends Forms_FormField
{
    protected $values  = array ();
    
    /**
    * @param mixed  $name
    * @param mixed  $module
    * @param string $value
    * @param mixed  $mode
    * @param mixed  $label
    * @param mixed  $formSection
    * @param mixed  $changedValue
    * @param mixed  $suffix
    * 
    * @return Forms_MultiSelectField
    */
    public function __construct($name, $module, $value, $mode, $label = '', $formSection = '', $changedValue = '', $suffix = '')
    {
        if (is_array($value))
        {
            // parent still needs a concatenated string
            $value = implode(' ', $value);
        }

        parent::__construct($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix);    
    }
    
    /**
    * Creates the HTML for this field.
    */
    protected function createHTML($readonly = false)
    {
        $this->html =   '<div id="'.$this->fieldId.'_values">'.
                            '<ul>';

        $this->html .= '<input type="hidden" name="'.$this->fieldId.'" id="'.$this->fieldId.'" value="'.$this->value.'"/>';

        $selected = preg_split ('/ /u', $this->value);

        foreach ($this->values as $code => $desc)
        {
            $extra = ' onclick="updateFieldWithCheckboxesValues(\''.$this->fieldId.'\'); FieldChanged(\''.$this->fieldId.'\');"';

            // is it checked?
            if (in_array((string) $code, $selected, true)) {
                $extra .= ' checked="checked"';
            }

            // read-only?
            if ($this->mode == 'readonly' || $readonly)
            {
                $extra .= ' disabled';
            }

            $fieldDomId = $this->fieldId.'_'.rand();
            $this->html .= '<li><input type="checkbox" name="'.$this->fieldId.'_options[]" id="'.$fieldDomId.'" value="'.$code.'"'.$extra.'/> <label class="field_label" for="'.$fieldDomId.'">'.$desc.'</label></li>';
        }

        $this->html .=      '</ul>'.
                        '</div>';
    }
    
    /**
     * Set values for checkboxes
     * @param array ($key => label, $key => $label, ...)
     */
    public function setValues (array $values)
    {
        $this->values = $values;
    }    


    protected function build ()
    {
        if ($this->defaultHidden)
        {
            $this->createDefaultHiddenHTML();
        }
        else
        {
            $this->createHTML($readonly);
        }
    }


    /**
    * Creates the HTML for this field if it's hidden with a default value.
    */
    protected function createDefaultHiddenHTML()
    {
        $this->html = '<input type="hidden" name="'.$this->fieldId.'" id="'.$this->fieldId.'" value="'.$this->defaultHidden.'"/>';
    }
}
