<?php

/**
* @desc Class representing a form design.
*/
class Forms_FormDesign
{

    public $ID; //The id of this form design
    public $Module; //The module that this form design falls into
    public $Level; //The form design level (1 or 2)
    public $FormType; //The form mode (New, Edit, Search...)

    // parameters used for linked form designs.
    protected $ParentModule; //the module the this form design sits under. (e.g. an actions form linked to an incident)
    protected $ParentLevel; // the level of the form this form design sits under.
    protected $LinkType; // Identifier in case there are multiple forms linked to the parent form.

    /**
    * Determines how the object should handle a non-existent file.
    *
    * If "IGNORE", will do nothing.
    * If "DEFAULT", will change the file pointed to to a default file.
    * If blank, will throw an exception.
    */
    public $NonExistentFile;

    // controls whether the form can be edited.
    public $ReadOnly = false;

    //form design parameters
    public $FormTitle;
    public $FormTitleDescr;
    public $Show_all_section;
    public $HideFields;
    public $ReadOnlyFields;
    public $MandatoryFields;
    public $OrderSections;
    public $UserLabels;
    public $UserExtraText;
    public $DefaultValues;
    public $TextareaMaxChars;
    public $FieldOrders;
    public $DIF1UDFGroups;
    public $ExpandSections;
    public $ExpandFields;
    public $HelpTexts;
    public $UDFGroups;
    public $NewPanels;
    public $ContactMatch;
    public $FormDesigns;
    public $ContactForms;
    public $ListingDesigns;
    public $MoveFieldsToSections;
    public $ExtraSections = array();
    public $ExtraFields = array();
    public $DisplayAsCheckboxes;
    public $DisplayAsRadioButtons;
    public $TimestampFields;
    public $LockFieldAtribs = array();
    public $OrganisationMatch;

    /**
    * @desc Basic constructor - sets up module/level information. No parameters are required, though some methods will throw
    * exceptions if module and level are not provided.
    */
    public function __construct($Parameters = array())
    {
        $this->Module = $Parameters['module'];
        $this->FormType = $Parameters['form_type'];
        $this->Level = $Parameters['level'];

        if (isset($Parameters['id']))
        {
            $this->ID = Sanitize::SanitizeInt($Parameters['id']);
        }

        $this->ParentModule = $Parameters['parent_module'];
        $this->ParentLevel = isset($Parameters['parent_level']) ? $Parameters['parent_level'] : $this->Level;
        $this->LinkType = $Parameters['link_type'];

        if (isset($Parameters['readonly']))
        {
            $this->ReadOnly = $Parameters['readonly'];
        }
    }

    public function getParentModule()
    {
        return $this->ParentModule;
    }
    public function getParentLevel()
    {
        return $this->ParentLevel;
    }
    public function getLinkType()
    {
        return $this->LinkType;
    }

    /**
    * @desc Finds the correct form design file and includes it, storing the information in the parameters property.
    */
    public function PopulateFormDesign($Parameters = array())
    {
        $savedSettings = saveFormDesignSettings();
        unsetFormDesignSettings();

        $file = $this->GetFilename($Parameters);
        include $file;

        foreach (Forms_FormDesign::getFormDesignGlobals() as $DesignParameter)
        {
            if(isset($GLOBALS[$DesignParameter]))
            {
                $this->$DesignParameter = $GLOBALS[$DesignParameter];
            }
        }

        loadFormDesignSettings($savedSettings);
    }

    /**
    * @desc Populates the parameter array from design globals.
    */
    public function PopulateFromCurrentDesignGlobals()
    {
        $savedSettings = saveFormDesignSettings();

        foreach($savedSettings as $Parameter => $Value)
        {
            $this->$Parameter = $Value;
        }
    }

    protected function RequireModule()
    {
        if(!$this->Module)
        {
            throw new Exception('No module provided.');
        }
    }

    protected function RequireLevel()
    {
        if(!$this->Level)
        {
            throw new Exception('No level provided.');
        }
    }

    /**
    * @desc Gets the filename associated with this form design.
    *
    * @param array $Parameters Array of parameters
    * @param bool NoCheckExists If set to true, a check will not be done for whether the file exists before returning it.
    * This allows this function to be used when creating new designs.
    *
    * @return string The path to this form design file.
    */
    public function GetFilename($Parameters = array())
    {
        global $ModuleDefs;

        $Filename = $this->GenerateFileName();

        $NonExistentFileAction = ($Parameters['NonExistentFile'] ? $Parameters['NonExistentFile'] : $this->NonExistentFile);

        //Ensure that we are always returning a filename that exists.
        if (!file_exists($Filename))
        {
            if ($NonExistentFileAction == 'DEFAULT')
            {
                if ($ModuleDefs[Sanitize::getModule($this->Module)]['GENERIC'])
                {
                    $Filename = 'Source/generic_modules/'.Sanitize::getModule($this->Module).'/StandardLevel' . $this->getLevel() . 'Settings.php';
                }
                else
                {
                    $Filename = $ModuleDefs[Sanitize::getModule($this->Module)]['LIBPATH'].'/Standard'.$ModuleDefs[Sanitize::getModule($this->Module)]['FORMS'][$this->getLevel()]['CODE'].'Settings.php';
                }
            }
            else if ($NonExistentFileAction != 'IGNORE')
            {
                throw new FileNotFoundException('Form design file does not exist');
            }
        }

        return $Filename;
    }

    public function GenerateFileName()
    {
        global $ClientFolder, $ModuleDefs;

        $this->RequireModule();
        $this->RequireLevel();

        if (!is_numeric($this->ID))
        {
            $this->CalculateFormID();
        }

        if ($this->Level == 1)
        {
            $FilePrefix = $ModuleDefs[Sanitize::getModule($this->Module)]['FORM_DESIGN_LEVEL_1_FILENAME'];
        }
        else
        {
            $FilePrefix = $ModuleDefs[Sanitize::getModule($this->Module)]['FORM_DESIGN_LEVEL_2_FILENAME'];
        }

        if (intval($this->ID) > 0)
        {
            $Filename = $ClientFolder.'/'.$FilePrefix.'_' . Sanitize::SanitizeInt($this->ID) . '.php';
        }
        else
        {
            $Filename = $ClientFolder.'/'.$FilePrefix.'.php';
        }

        return $Filename;
    }

    public function FileExists()
    {
        $Filename = $this->GenerateFileName();

        return file_exists($Filename);
    }


    /*
     * Returns the level if its been set to the object (passes definition from array for fortify security check)
     */
    public function getLevel()
    {
    	$possibleLevels = array('1','2');
    	if (isset($this->Level) && in_array($this->Level, $possibleLevels))
    	{
    		return $possibleLevels[($this->Level - 1)];
    	}
    }

    public function GetID()
    {
        if (!isset($this->ID))
        {
            $this->CalculateFormID();
        }

        return $this->ID;
    }

    /**
    * @desc Gets the id of this form, based on the module and level. Takes into account linked forms.
    * Doesn't return anything, but sets the $ID property.
    */
    protected function CalculateFormID()
    {
        global $ModuleDefs;

        $this->RequireModule();
        $this->RequireLevel();

        // in logged out level 1 forms, the URL overrides, but only if the id is for the module we are trying to load.
        if ($this->Level == 1 && !$_SESSION['logged_in'] && ($_REQUEST['form_id'] != '' || $_REQUEST['parent_form_id'] != '') && ((isset($_REQUEST['module']) && ($_REQUEST['module'] == $this->Module || ($_REQUEST['module'] == 'DIF1' && $this->Module == 'INC'))) || (!isset($_REQUEST['module']) && $this->Module == 'INC')))
        {
            $this->ID = intval($_REQUEST['form_id']) ?: intval($_REQUEST['parent_form_id']); // parent_form_id used when replacing a dynamic section via the "Clear section" link
            return true;
        }
        elseif ($this->Level == 1 && $_SESSION['logged_in'] && GetCurrentUserFormLevel($this->Module) == 1)
        {
            $Level1OnlyID = GetParm($ModuleDefs[$this->Module]['LEVEL_1_ONLY_FORM_GLOBAL']);

            if($Level1OnlyID)
            {
                $this->ID = (Int) $Level1OnlyID;
                return true;
            }
        }

        //if this is a linked form, we need to check the parent form design. Can't do this with actions in search mode, because we don't know which module is the parent
        if (
            (($this->Module == 'ACT' || $ModuleDefs[$this->Module]['LINKED_MODULE']) && $this->FormType != 'Search' && ($this->Module != 'ORG' || $this->getLevel() != '2'))
            || ($this->Module == 'ORG' && $this->ParentModule == 'CLA')
            || ($this->Module != 'ACT' && !$ModuleDefs[$this->Module]['LINKED_MODULE'] && ((in_array($this->Module, array('PRO', 'ELE'))) || (isset($this->LinkType) && $this->LinkType != '')))
            || ($this->Module == 'AMO' && $this->ParentModule == 'LOC') || ($this->Module == 'CON' && $this->ParentModule == 'CLA' && $this->LinkType == 'O'))
        {
            switch($this->Module)
            {
                case 'PRO':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => 'ELE', 'level' => 2, 'link_type' => 'elements'));
                    break;
                case 'ELE':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => 'STN', 'level' => 2));
                    break;
                case 'AQU':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => 'AMO', 'level' => 2));
                    break;
               case 'ATQ':
                      if(isset($_REQUEST['from_parent_record']) && $_REQUEST['from_parent_record'] == '1')
                      {
                         $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => $this->ParentModule, 'level' => $this->ParentLevel));
                      } else {
                         $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => 'AMO', 'level' => 2));     
                      }
                   break; 
                default:
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => $this->ParentModule, 'level' => $this->ParentLevel));
            }

            $FormDesigns = $ParentDesign->FormDesigns;
            switch($this->Module)
            {
                case 'CON':
                    if (isset($FormDesigns['contacts_type_'.$this->LinkType][$this->LinkType]) && $FormDesigns['contacts_type_'.$this->LinkType][$this->LinkType] != '')
                    {
                        $FormID = intval($FormDesigns['contacts_type_'.$this->LinkType][$this->LinkType]);
                    }
                    elseif(isset($FormDesigns['respondents'][$this->LinkType]) && $FormDesigns['respondents'][$this->LinkType] != '')
                    {
                        $FormID = intval($FormDesigns['respondents'][$this->LinkType]);
                    }
                    break;
                case 'ORG':
                    if (isset($FormDesigns['respondents_organisation'][$this->LinkType]) && $FormDesigns['respondents_organisation'][$this->LinkType] != '')
                    {
                        $FormID = intval($FormDesigns['respondents_organisation'][$this->LinkType]);
                    }
                    if (isset($FormDesigns['respondents'][$this->LinkType]) && $FormDesigns['respondents'][$this->LinkType] != '')
                    {
                        $FormID = intval($FormDesigns['respondents'][$this->LinkType]);   
                    }
                    break;
                case 'AST':
                    if (isset($FormDesigns[$this->LinkType]['linked_assets']) && $FormDesigns[$this->LinkType]['linked_assets'] != '')
                    {
                        $FormID = intval($FormDesigns[$this->LinkType]['linked_assets']);
                    }
                    break;
                case 'ACT':
                    if (isset($FormDesigns[$this->LinkType][$this->LinkType]) && $FormDesigns[$this->LinkType][$this->LinkType] != '')
                    {
                        $FormID = intval($FormDesigns[$this->LinkType][$this->LinkType]);
                    }
                    break;
                default:
                    if (isset($FormDesigns[$this->LinkType][$this->LinkType]) && $FormDesigns[$this->LinkType][$this->LinkType] != '')
                    {
                        $FormID = intval($FormDesigns[$this->LinkType][$this->LinkType]);
                    }
            }
        }
        //Now we just get the form from the appropriate global. This takes into account user, profile and global config options.
        if (!isset($FormID))
        {
            if ($this->FormType == 'Search')
            {
                $FormID = GetParm($ModuleDefs[$this->Module]["FORMS"][$this->Level]['CODE'].'_SEARCH_DEFAULT');
            }
            if($this->FormType != 'Search' || !is_numeric($FormID)) //even in search mode, we want to fall back on the global default if no specific default set.
            {
                if ($this->Module == 'INC' && $this->Level == 1 && isset($_SESSION['login']) && $_SESSION['login'] != '' && GetParm($ModuleDefs[$this->Module]["FORMS"][$this->Level]['CODE'].'_ONLY_FORM'))
                {
                    $FormID = GetParm($ModuleDefs[$this->Module]["FORMS"][$this->Level]['CODE'].'_ONLY_FORM');
                }
                else
                {
                    $FormID = GetParm($ModuleDefs[$this->Module]["FORMS"][$this->Level]['CODE'].'_DEFAULT');
                }
            }
        }

        $this->ID =  Sanitize::SanitizeInt($FormID);
    }

    /**
    * @desc Loads the form design parameters into $GLOBALS variables to fit in with legacy code.
    */
    public function LoadFormDesignIntoGlobals()
    {
        unsetFormDesignSettings();

        foreach(Forms_FormDesign::getFormDesignGlobals() as $Parameter)
        {
            $ParametersToLoad[$Parameter] = $this->$Parameter;
        }

        loadFormDesignSettings($ParametersToLoad);
    }

    function getParameter($Param)
    {
        return $this->$Param;
    }

    /**
    * @desc Saves all parameter property data to the appropriate file.
    */
    public function SaveToFile()
    {
        // Create output file.
        $OutputFile = @fopen(Sanitize::SanitizeFilePath($this->GetFilename(array('NonExistentFile' => 'IGNORE'))), "wb");

        if (!$OutputFile)
        {
            fatal_error("Cannot create settings file.  Please contact your IT department.");
        }

        if (!fwrite($OutputFile,"<?php\n"))
        {
            fatal_error("Cannot write to settings file.  Please contact your IT department.");
        }

        if ($this->FormTitle)
        {
            fwrite($OutputFile, '$GLOBALS["FormTitle"] = "' . EscapeQuotestoFile(str_replace('\\', '\\\\', $this->FormTitle)) . "\";\n");
        }

        if ($this->FormTitleDescr)
        {
            fwrite($OutputFile, '$GLOBALS["FormTitleDescr"] = "' . EscapeQuotestoFile(str_replace('\\', '\\\\', $this->FormTitleDescr)) . "\";\n");
        }

        if ($this->Show_all_section)
        {
            fwrite($OutputFile, '$GLOBALS["Show_all_section"] = "' . $this->Show_all_section . "\";\n");
        }

        if ($this->getParameter('HideFields'))
        {
            fwrite($OutputFile, '$GLOBALS["HideFields"] = ' . var_export($this->getParameter('HideFields'), true) . ";\n");
        }

        if ($this->getParameter('ReadOnlyFields'))
        {
            fwrite($OutputFile, '$GLOBALS["ReadOnlyFields"] = ' . var_export($this->getParameter('ReadOnlyFields'), true) . ";\n");
        }

        if ($this->getParameter('MandatoryFields'))
        {
            fwrite($OutputFile, '$GLOBALS["MandatoryFields"] = ' . var_export($this->getParameter('MandatoryFields'), true) . ";\n");
        }
        if ($this->getParameter('DisplayAsCheckboxes'))
        {
            fwrite($OutputFile, '$GLOBALS["DisplayAsCheckboxes"] = ' . var_export($this->getParameter('DisplayAsCheckboxes'), true) . ";\n");
        }

        if ($this->getParameter('DisplayAsRadioButtons'))
        {
            fwrite($OutputFile, '$GLOBALS["DisplayAsRadioButtons"] = ' . var_export($this->getParameter('DisplayAsRadioButtons'), true) . ";\n");
        }

        if ($this->getParameter('TimestampFields'))
        {
            fwrite($OutputFile, '$GLOBALS["TimestampFields"] = ' . var_export($this->getParameter('TimestampFields'), true) . ";\n");
        }

        if ($this->getParameter('OrderSections'))
        {
            fwrite($OutputFile, '$GLOBALS["OrderSections"] = ' . var_export($this->getParameter('OrderSections'), true) . ";\n");
        }

        if ($this->getParameter('UserLabels'))
        {
            fwrite($OutputFile, '$GLOBALS["UserLabels"] = ' . var_export($this->getParameter('UserLabels'), true) . ";\n");
        }

        if ($this->getParameter('UserExtraText'))
        {
            fwrite($OutputFile, '$GLOBALS["UserExtraText"] = ' . var_export($this->getParameter('UserExtraText'), true) . ";\n");
        }

        if ($this->getParameter('DefaultValues'))
        {
            fwrite($OutputFile, '$GLOBALS["DefaultValues"] = ' . var_export($this->getParameter('DefaultValues'), true) . ";\n");
        }

        if ($this->getParameter('TextareaMaxChars'))
        {
            fwrite($OutputFile, '$GLOBALS["TextareaMaxChars"] = ' . var_export($this->getParameter('TextareaMaxChars'), true) . ";\n");
        }

        if ($this->getParameter('FieldOrders'))
        {
            fwrite($OutputFile, '$GLOBALS["FieldOrders"] = ' . var_export($this->getParameter('FieldOrders'), true) . ";\n");
        }

        if ($this->getParameter('DIF1UDFGroups'))
        {
            fwrite($OutputFile, '$GLOBALS["DIF1UDFGroups"] = ' . var_export($this->getParameter('DIF1UDFGroups'), true) . ";\n");
        }

        if ($this->getParameter('ExpandSections'))
        {
            fwrite($OutputFile, '$GLOBALS["ExpandSections"] = ' . var_export($this->getParameter('ExpandSections'), true) . ";\n");
        }

        if ($this->getParameter('ExpandFields'))
        {
            fwrite($OutputFile, '$GLOBALS["ExpandFields"] = ' . var_export($this->getParameter('ExpandFields'), true) . ";\n");
        }

        if ($this->getParameter('HelpTexts'))
        {
            fwrite($OutputFile, '$GLOBALS["HelpTexts"] = ' . var_export($this->getParameter('HelpTexts'), true) . ";\n");
        }

        if ($this->getParameter('NewPanels'))
        {
            fwrite($OutputFile, '$GLOBALS["NewPanels"] = ' . var_export($this->getParameter('NewPanels'), true) . ";\n");
        }

        if ($this->getParameter('ContactMatch'))
        {
            fwrite($OutputFile, '$GLOBALS["ContactMatch"] = ' . var_export($this->getParameter('ContactMatch'), true) . ";\n");
        }

        if ($this->getParameter('ContactForms'))
        {
            fwrite($OutputFile, '$GLOBALS["ContactForms"] = ' . var_export($this->getParameter('ContactForms'), true) . ";\n");
        }

        if ($this->getParameter('FormDesigns'))
        {
            fwrite($OutputFile, '$GLOBALS["FormDesigns"] = ' . var_export($this->getParameter('FormDesigns'), true) . ";\n");
        }

        if ($this->getParameter('EquipmentForms'))
        {
            fwrite($OutputFile, '$GLOBALS["EquipmentForms"] = ' . var_export($this->getParameter('EquipmentForms'), true) . ";\n");
        }

        if ($this->getParameter('ListingDesigns'))
        {
            fwrite($OutputFile, '$GLOBALS["ListingDesigns"] = ' . var_export($this->getParameter('ListingDesigns'), true) . ";\n");
        }

        if ($this->getParameter('MoveFieldsToSections'))
        {
            fwrite($OutputFile, '$GLOBALS["MoveFieldsToSections"] = ' . var_export($this->getParameter('MoveFieldsToSections'), true) . ";\n");
        }

        if ($this->ExtraSections)
        {
            fwrite($OutputFile, '$GLOBALS["ExtraSections"] = ' . var_export($this->getParameter('ExtraSections'), true) . ";\n");
        }

        if ($this->ExtraFields)
        {
            fwrite($OutputFile, '$GLOBALS["ExtraFields"] = ' . var_export($this->getParameter('ExtraFields'), true) . ";\n");
        }

        if ($this->getParameter('LockFieldAtribs'))
        {
            fwrite($OutputFile, '$GLOBALS["LockFieldAtribs"] = ' . var_export($this->getParameter('LockFieldAtribs'), true) . ";\n");
        }

        if ($this->getParameter('OrganisationMatch'))
        {
            fwrite($OutputFile, '$GLOBALS["OrganisationMatch"] = ' . var_export($this->getParameter('OrganisationMatch'), true) . ";\n");
        }

        fwrite($OutputFile, '?>');
        fclose($OutputFile);

        return $OutputFile;
    }

    public function GetGlobalName()
    {
        global $ModuleDefs;

        $this->RequireModule();
        $this->RequireLevel();

        if ($this->FormType == 'Search')
        {
            return $ModuleDefs[$this->Module]["FORMS"][$this->Level]['CODE'].'_SEARCH_DEFAULT';
        }
        else
        {
            return $ModuleDefs[$this->Module]["FORMS"][$this->Level]['CODE'].'_DEFAULT';
        }
    }

    public function ClearDesign()
    {
        global $ModuleDefs;

        //delete design file
        $DesignFile = Sanitize::SanitizeFilePath($this->GetFilename(array('NonExistentFile' => 'IGNORE')));

        if (file_exists($DesignFile))
        {
            unlink($DesignFile);
        }

        if ($this->GetID() != 0) //Need to do some clean-up, since we are deleting rather than resetting.
        {
            if ($this->GetGlobalName() != '')
            {
                //any user,profile or global settings with this id need to be cleared, since they are no longer valid.
                DatixDBQuery::PDO_query('UPDATE GLOBALS SET PARMVALUE = :parmvalue WHERE PARAMETER = :parameter AND PARMVALUE = :current_parmvalue', array('parmvalue' => '0', 'parameter' => $this->GetGlobalName(), 'current_parmvalue' => $this->GetID()));
                DatixDBQuery::PDO_query('DELETE FROM USER_PARMS WHERE PARMVALUE = :parmvalue AND PARAMETER = :parameter', array('parmvalue' => $this->GetID(), 'parameter' => $this->GetGlobalName()));
                DatixDBQuery::PDO_query('DELETE FROM LINK_PROFILE_PARAM WHERE LPP_VALUE = :parmvalue AND LPP_PARAMETER = :parameter', array('parmvalue' => $this->GetID(), 'parameter' => $this->GetGlobalName()));
            }

            $this->DeleteFromFormsFile();
        }
    }

    /**
    * @desc Deletes the current form from the forms header file.
    */
    protected function DeleteFromFormsFile()
    {
        $FormsFileName = GetFormsFilename($this->Module);

        if (file_exists($FormsFileName))
        {
            require $FormsFileName;

            if ($this->Level == 2)
            {
                unset($FormFiles2[$this->GetID()]);
            }
            else
            {
                unset($FormFiles[$this->GetID()]);
            }

            Forms_FormDesign::WriteFormsFile($FormFiles, $FormFiles2, $this->Module);
        }
    }

    /**
    * @desc Pulls form design parameters out of the POST variable and assigns them to appropriate parameter keys.
    */
    public function PopulateFromPOST()
    {
        $params = Sanitize::SanitizeRawArray($_POST);
        
        $OLDExtraFields = $this->ExtraFields;

        while (list($name, $value) = each($params))
        {
            $NamePair = explode("-", $name);

            if ($NamePair[2] != "")
            {
                switch ($NamePair[0])
                {
                	case 'lock':
                        if ($value != '0')
                        {
                            $this->LockFieldAtribs[$NamePair[3]][$NamePair[1]] = true;
                        }
                        else
                        {
                            unset($this->LockFieldAtribs[$NamePair[3]][$NamePair[1]]);
                        }
                        
                		break;
                    case 'HIDE':
                        if ($value == 'off')
                        {
                            unset($this->HideFields[$NamePair[2]]);
                        }
                        else
                        {
                            $this->HideFields[$NamePair[2]] = true;
                        }

                        break;
                    case 'READONLY':
                        if ($value == 'off')
                        {
                            unset($this->ReadOnlyFields[$NamePair[2]]);
                        }
                        else
                        {
                            $this->ReadOnlyFields[$NamePair[2]] = true;
                        }

                        break;
                    case 'MANDATORY':
                        if ($value == 'off')
                        {
                            unset($this->MandatoryFields[$NamePair[2]]);
                        }
                        else
                        {
                            $this->MandatoryFields[$NamePair[2]] = Escape::EscapeEntities($NamePair[1]);
                        }

                        break;
                    case 'DISPLAYASCHECKBOXES':
                        if ($value == 'off')
                        {
                            unset($this->DisplayAsCheckboxes[$NamePair[2]]);
                        }
                        else
                        {
                            $this->DisplayAsCheckboxes[$NamePair[2]] = true;
                        }

                        break;
                    case "DISPLAYASRADIOBUTTONS":
                        if ($value == 'off')
                        {
                            unset($this->DisplayAsRadioButtons[$NamePair[2]]);
                        }
                        else
                        {
                            $this->DisplayAsRadioButtons[$NamePair[2]] = true;
                        }

                        break;
                    case "TIMESTAMP":
                        if ($value == 'off')
                        {
                            unset($this->TimestampFields[$NamePair[2]]);
                        }
                        else
                        {
                            $this->TimestampFields[$NamePair[2]] = true;
                        }
                        break;
                    case "ORDER":
                        if ($value)
                        {
                            //remove any previous orders for this field;
                            foreach ($this->OrderSections as $order => $section)
                            {
                                if ($section == Escape::EscapeEntities($NamePair[1]))
                                {
                                    unset($this->OrderSections[$order]);
                                }
                            }

                            $this->OrderSections[intval($value)] = Escape::EscapeEntities($NamePair[1]);
                            $this->OrderSections = array_unique($this->OrderSections);
                        }
                        else
                        {
                            if (in_array(Escape::EscapeEntities($NamePair[1]), $this->OrderSections))
                            {
                                $this->OrderSections = array_diff($this->OrderSections, array(Escape::EscapeEntities($NamePair[1])));
                            }
                        }

                        break;
                    case 'LABEL':
                        if ($value)
                        {
				        	//todo WHITELIST CANDIDATE
                            $this->UserLabels[$NamePair[2]] = $value;
                        }
                        else
                        {
                            unset($this->UserLabels[$NamePair[2]]);
                        }

                        break;
                    case 'EXTRA':
                        if ($value)
                        {
				        	//todo WHITELIST CANDIDATE
                            $this->UserExtraText[$NamePair[2]] = $value;
                        }
                        else
                        {
                            unset($this->UserExtraText[$NamePair[2]]);
                        }

                        break;
                    case 'FIELDORDER':
                        if ($value)
                        {
                            if (!$this->IsFieldLocked($NamePair[1], $this->FieldOrders[$NamePair[1]][$value], 'FIELDORDER') || IsCentralAdmin())
                            {
                                //remove any previous orders for this field;
                                foreach ($this->FieldOrders[$NamePair[1]] as $order => $fieldName)
                                {
                                    if ($fieldName == Escape::EscapeEntities($NamePair[2]))
                                    {
                                        unset($this->FieldOrders[$NamePair[1]][$order]);
                                    }
                                }

                                $this->FieldOrders[$NamePair[1]][$value] = Escape::EscapeEntities($NamePair[2]);
                                $this->FieldOrders[$NamePair[1]] = array_unique($this->FieldOrders[$NamePair[1]]);
                                ksort($this->FieldOrders[$NamePair[1]]);
                            }
                        }
                        else
                        {
                            if (in_array(Escape::EscapeEntities($NamePair[2]), $this->FieldOrders[$NamePair[1]]))
                            {
                                $this->FieldOrders[$NamePair[1]] = array_diff($this->FieldOrders[$NamePair[1]], array(Escape::EscapeEntities($NamePair[2])));
                            }
                        }

                        break;
                    case 'DEF':
                        if ($_POST[$NamePair[2]] != '')
                        {
                            $this->DefaultValues[$NamePair[2]] = Escape::EscapeEntities($_POST[$NamePair[2]]);
                        }
                        else
                        {
                            unset($this->DefaultValues[$NamePair[2]]);
                        }
                        break;
                    case 'TEXTAREAMAXCHARS':
                        if ($value)
                        {
                            $this->TextareaMaxChars[$NamePair[2]] = Escape::EscapeEntities($value);
                        }
                        else
                        {
                            unset($this->TextareaMaxChars[$NamePair[2]]);
                        }
                        break;
                    case 'HELPTEXT':
                        if ($value)
                        {
                        	// Unable to filter on helptext as the application allows tag entry (including script tags)
				        	//todo WHITELIST CANDIDATE
                            $this->HelpTexts[$NamePair[2]] = $value;
                        }
                        else
                        {
                            unset($this->HelpTexts[$NamePair[2]]);
                        }

                        break;
                    case 'EXPANDSECTION':
                        if ($value)
                        {
                            $this->ExpandSections[$NamePair[2]][$NamePair[1]]
                                = array('section' => Escape::EscapeEntities($value),
                                'alerttext' => $_POST["EXPANDALERTTEXT-"
                                    . $NamePair[1] . "-" . $NamePair[2]],
                                'values' => explode(' ', \UnicodeString::trim(filter_var($_POST["EXPANDVALUES-"
                                    . $NamePair[1] . "-" . $NamePair[2]], FILTER_SANITIZE_SPECIAL_CHARS))));
                        }
                        else
                        {
                            unset($this->ExpandSections[$NamePair[2]][(int) $NamePair[1]]);
                        }

                        break;
                    case 'EXPANDALERTTEXT':
                        // Only set this if it has not already been set by EXPANDSECTION
                        if ($value)
                        {
                            if ($_POST["EXPANDSECTION-". $NamePair[1] . "-" . $NamePair[2]] == "")
                            {
                                $this->ExpandSections[$NamePair[2]][$NamePair[1]] = array(
                                    'alerttext' => $_POST["EXPANDALERTTEXT-"
                                    . $NamePair[1] . "-" . $NamePair[2]],
                                'values' => explode(' ', \UnicodeString::trim(filter_var($_POST["EXPANDVALUES-"
                                 . $NamePair[1] . "-" . $NamePair[2]], FILTER_SANITIZE_SPECIAL_CHARS))));
                            }
                        }
                        break;
                    case 'EXPANDFIELD':
                        if ($value)
                        {
                            $this->ExpandFields[$NamePair[2]][$NamePair[1]]
                                = array('field' => $value,
                                'alerttext' => $_POST["EXPANDFIELDALERTTEXT-"
                                    . $NamePair[1] . "-" . $NamePair[2]],
                                'values' => explode(' ', \UnicodeString::trim(filter_var($_POST["EXPANDFIELDVALUES-"
                                    . $NamePair[1] . "-" . $NamePair[2]], FILTER_SANITIZE_SPECIAL_CHARS))));
                        }
                        else
                        {
                            unset($this->ExpandFields[$NamePair[2]][(int) $NamePair[1]]);
                        }

                        break;
                    case 'EXPANDFIELDALERTTEXT':
                        // Only set this if it has not already been set by EXPANDSECTION
                        if ($value)
                        {
                            if ($_POST["EXPANDFIELD-" . $NamePair[1] . "-" . $NamePair[2]] == "")
                            {
                                $this->ExpandFields[$NamePair[2]][$NamePair[1]] = array(
                                    'alerttext' => $_POST["EXPANDFIELDALERTTEXT-"
                                    . $NamePair[1] . "-" . $NamePair[2]],
                                'values' => explode(' ', \UnicodeString::trim(filter_var($_POST["EXPANDFIELDVALUES-"
                                    . $NamePair[1] . "-" . $NamePair[2]], FILTER_SANITIZE_SPECIAL_CHARS))));
                            }
                        }
                        break;
                    case 'NEWPANEL':
                        if ($value == 'off')
                        {
                            unset($this->NewPanels[$NamePair[2]]);
                        }
                        else
                        {
                            $this->NewPanels[$NamePair[2]] = true;
                        }

                        break;
                    case 'MATCH':
                        if ($value == 'off')
                        {
                            unset($this->ContactMatch[$NamePair[2]]);
                        }
                        else
                        {
                            $this->ContactMatch[$NamePair[2]] = true;
                        }
                        
                        break;
                    case 'MATCHORG':
                        if ($value == 'off')
                        {
                            unset($this->OrganisationMatch[$NamePair[2]]);
                        }
                        else
                        {
                            $this->OrganisationMatch[$NamePair[2]] = true;
                        }

                        break;
                    case 'FORMDESIGN':
                        $this->FormDesigns[$NamePair[1]][$NamePair[2]] = $value;
                        break;
                    case 'EQUIPFORM': //dynamic equipment form to use for particular section
                        $this->EquipmentForms = $value;
                        break;
                    case 'LISTINGDESIGN': //listing design to use for a listing section
                        $this->ListingDesigns[$NamePair[1]][$NamePair[2]] = $value;
                        break;
                    case 'EXTRASECTIONS': //User defined sections to add to the form
                        if ($value)
                        {
                            $this->ExtraSections[$NamePair[1]] = $value;                            
                        }
                        else
                        {
                            unset($this->ExtraSections[$NamePair[1]]);
                            $this->deleteSectionRelations($NamePair[1]);
                        }
                        break;
                    case 'EXTRAFIELDS': //User defined fields to add to the form
                        $this->ExtraFields = array_diff($this->ExtraFields, array($NamePair[1]));
                        foreach ($value as $UDF_id)
                        {
                            $this->ExtraFields[$UDF_id] = $NamePair[1];
                        }
                        $_SESSION['EXTRAFIELDS'][$NamePair[2]] = '1';
                        break;
                    case 'CHANGEDSECTION':
                        $DoNotMoveField = false;

                        if ($value)
                        {
                            // remove existing field order data if a field is being moved to a new section
                            if (!isset($this->MoveFieldsToSections[$NamePair[2]]) || $this->MoveFieldsToSections[$NamePair[2]]['New'] != $value)
                            {
                                $currentSection = ''; // this var will be set if the field being moved has an existing field order definition
                                
                                if (is_array($this->MoveFieldsToSections[$NamePair[2]]) && is_array($this->FieldOrders[$this->MoveFieldsToSections[$NamePair[2]]['New']]))
                                {
                                    // the field has already been moved from its default section
                                    $currentSection = $this->MoveFieldsToSections[$NamePair[2]]['New'];
                                }
                                elseif (is_array($this->FieldOrders[$NamePair[1]]))
                                {
                                    // assume the field is being moved from its default section
                                    $currentSection = $NamePair[1];
                                }
                                
                                if ($currentSection != '')
                                {
                                    // remove field order values in the original section for this field
                                    $this->removeFieldOrderFromSection($NamePair[2], $currentSection);
                                }
                            }
                            
                            if(\UnicodeString::substr($NamePair[2], 0, 4) == 'UDF_')
                            {
                                $DoNotMoveField = true;

                                $UDFParts = explode('_', $NamePair[2]);

                                $this->ExtraFields[$UDFParts[3].'_'.$UDFParts[2]] = $value;

                                //in case we haven't saved the extrafield setting yet we need to edit the posted data.
                                foreach($_POST['EXTRAFIELDS-'.$NamePair[1].'-'.$NamePair[1]] as $key => $UDF_id)
                                {
                                    if($UDF_id == $UDFParts[3].'_'.$UDFParts[2])
                                    {
                                        unset($_POST['EXTRAFIELDS-'.$NamePair[1].'-'.$NamePair[1]][$key]);
                                    }
                                }

                                $params['EXTRAFIELDS-'.$value.'-'.$value][] = $UDFParts[3].'_'.$UDFParts[2];
                                $_SESSION['EXTRAFIELDS'][$value] = '1';
                            }

                            if ($NamePair[1] == $value) //moved back to default section manually.
                            {
                                $DoNotMoveField = true;
                                unset($this->MoveFieldsToSections[$NamePair[2]]);
                            }

                            if (!$DoNotMoveField)
                            {
                                $this->MoveFieldsToSections[$NamePair[2]] = array('Original' => $NamePair[1], 'New' => $value);

                                // any mandatory flag for this field needs to be removed if the field is being moved to a read only section
                                if (is_array($this->ReadOnlyFields) && $this->ReadOnlyFields[$value] && is_array($this->MandatoryFields) && array_key_exists($NamePair[2], $this->MandatoryFields))
                                {
                                    unset($this->MandatoryFields[$NamePair[2]]);
                                }
                            }
                        }
                        else
                        {
                            if (isset($this->MoveFieldsToSections[$NamePair[2]]))
                            {
                                // we arrive here when using the "Return field to Default section" button
                                $this->removeFieldOrderFromSection($NamePair[2], $this->MoveFieldsToSections[$NamePair[2]]['New']);
                                unset($this->MoveFieldsToSections[$NamePair[2]]);
                            }
                        }

                        break;
                    case 'NEWCHANGEDSECTION':
                        if(!bYN(GetParm('ALLOW_CROSS_SECTION_ACTIONS', 'N')) && $value == '1')
                        {
                            $DeleteFieldActions[] = $NamePair[2];
                        }
                        break;
                    case 'SECTIONEXPANDED':
                        if ($value == '1')
                        {
                            $_SESSION['SECTIONEXPANDED'][$NamePair[2]] = $value;
                        }
                        else
                        {
                            unset($_SESSION['SECTIONEXPANDED'][$NamePair[2]]);
                        }

                        break;
                    case 'CHANGED':
                        // Blank fields because we are now populating the form with previous data
                        switch ($NamePair[1])
                        {
                            case 'EXTRAFIELDS':
                                if (!isset($_SESSION['EXTRAFIELDS'][$NamePair[2]]))
                                {
                                    if (in_array(Escape::EscapeEntities($NamePair[2]), $this->ExtraFields))
                                    {
                                        $this->ExtraFields = array_diff($this->ExtraFields, array($NamePair[2]));
                                    }
                                }
                                else
                                {
                                    unset($_SESSION['EXTRAFIELDS'][$NamePair[2]]);
                                }

                                break;
                        }

                        break;
                }
            }
        }

        
        
        $deleted = array_diff(array_keys($OLDExtraFields), array_keys($this->ExtraFields));
        if (!empty($deleted))
        {
        	$this->deleteUDFrelations($deleted);
        }
            
        if(is_array($DeleteFieldActions))
        {
            foreach($DeleteFieldActions as $Field)
            {
                unset($this->ExpandFields[$Field]);

                if(is_array($this->ExpandFields))
                {
                    foreach($this->ExpandFields as $FieldName => $ExpandArray)
                    {
                        foreach($ExpandArray as $key => $ExpandDetails)
                        {
                            if($ExpandDetails['field'] == $Field)
                            {
                                unset($this->ExpandFields[$FieldName][$key]);

                                if(empty($this->ExpandFields[$FieldName]))
                                {
                                    unset($this->ExpandFields[$FieldName]);
                                }
                            }
                        }
                    }
                }
            }
        }


        if ($_POST["DIF_1_UDF_GROUPS"])
        {
            $this->DIF1UDFGroups = filter_var($_POST["DIF_1_UDF_GROUPS"], FILTER_SANITIZE_SPECIAL_CHARS);
        }

        if ($_POST["form_title"])
        {
        	//todo WHITELIST CANDIDATE
            $this->FormTitle = $_POST["form_title"];

        }
        if (!empty($params))
        {
        	//todo WHITELIST CANDIDATE
            $this->FormTitleDescr = $_POST["form_title_descr"];
        }
        if ($_POST["show_all_section"])
        {
            $this->Show_all_section = filter_var($_POST["show_all_section"], FILTER_SANITIZE_SPECIAL_CHARS);
        }
    }
    
    /**
     * Removes a field order definition from a given section.
     * 
     * Used when moving fields between sections to clear up old data.
     * 
     * @param string $field
     * @param string $section
     */
    protected function removeFieldOrderFromSection($field, $section)
    {
        $orderKey = array_search($field, $this->FieldOrders[$section]);
        if ($orderKey !== false)
        {
            if (count($this->FieldOrders[$section]) == 1)
            {
                // if this is the only value we remove the entire section array
                unset($this->FieldOrders[$section]);
            }
            else
            {
                unset($this->FieldOrders[$section][$orderKey]);
            }
        }
    }
    
    public function deleteUDFrelationsTable($deleted, $table)
    {
        foreach ($this->{$table} as $key => $row) {
            if(substr($key, 0, 4) == 'UDF_')
            {
                $UDFParts = explode('_', $key);
                if (in_array($UDFParts[3].'_'.$UDFParts[2], $deleted))
                {
                    unset($this->{$table}[$key]);
                    
                }
            }
        }
    }
    
    
    public function deleteSectionRelations($SectionID)
    {
    
        $toClear = array('ReadOnlyFields', 'HideFields', 'UserLabels', 'UserExtraText');
    
        foreach ($toClear as $row)
        {
            if (isset($this->{$row}['section' . $SectionID]))
            {
                unset($this->{$row}['section' . $SectionID]);
            }
        }
        
        if (isset($this->OrderSections))
        {
            foreach ($this->OrderSections as $key => $sectionName)
            {
                if ($sectionName == 'section' . $SectionID)
                {
                    unset($this->OrderSections[$key]);
                }
            }
        }
        
    }
    
    
    public function deleteUDFrelations($deleted)
    {
        
        $toClear = array('HideFields', 'ReadOnlyFields', 'MandatoryFields', 'UserLabels', 'UserExtraText', 'DefaultValues', 'TextareaMaxChars', 'HelpTexts', 'DisplayAsCheckboxes', 'DisplayAsRadioButtons', 'TimestampFields', 'ExpandSections', 'ExpandFields');
        
        foreach ($toClear as $row)
        {
            $this->deleteUDFrelationsTable($deleted, $row);
        }
        
        if (isset($this->FieldOrders))
        {
            foreach ($this->FieldOrders as $key => $section)
            {
                foreach ($section as $order => $field)
                {
                    if(substr($field, 0, 4) == 'UDF_')
                    {
                        $UDFParts = explode('_', $field);
                        if (in_array($UDFParts[3].'_'.$UDFParts[2], $deleted))
                        {
                            unset($this->FieldOrders[$key][$order]);
                        } 
                    }
                }
            }
        }
    }
    
    /**
    * @desc Returns a URL link to the design page for this form.
    */
    public function getDesignLink()
    {
        $this->RequireModule();
        $this->RequireLevel();

        return '?action=formdesignsetup&module='.$this->Module.'&form_id='.$this->GetID().'&formlevel='.$this->Level;
    }

    public function CopyToNew()
    {
        global $ModuleDefs;

        $this->RequireModule();
        $this->RequireLevel();

        $FormsFileName = GetFormsFilename($this->Module);

        if (file_exists($FormsFileName))
        {
            require $FormsFileName;
        }
        else
        {
            $FormFiles = "";
            $FormFiles2 = "";
        }

        if (!is_array($FormFiles))
        {
            $Code = GetFormCode(array('module' => $this->Module, 'level' => 1));
            $FormFiles = array(0 => "Default {$ModuleDefs[$this->Module][REC_NAME]}".($Code ? ' ('.$Code.')' : '')." form");
        }
        if (!is_array($FormFiles2))
        {
            $Code = GetFormCode(array('module' => $this->Module, 'level' => 2));
            $FormFiles2 = array(0 => "Default {$ModuleDefs[$this->Module][REC_NAME]}".($Code ? ' ('.$Code.')' : '')." form");
        }

        // Get the highest form number from the array
        $MaxFormNo = 0;
        if($this->Level == 2)
        {
            $NewFormNo = max(array_keys($FormFiles2)) + 1;

            $FormFiles2[$NewFormNo] = $_POST["form_name"];
        }
        else
        {
            $NewFormNo = max(array_keys($FormFiles)) + 1;

            $FormFiles[$NewFormNo] = $_POST["form_name"];
        }

        Forms_FormDesign::WriteFormsFile($FormFiles, $FormFiles2, $this->Module);

        $FromFile = Sanitize::SanitizeFilePath($this->GetFilename(array('NonExistentFile' => 'DEFAULT')));
        $NewFormDesign = new Forms_FormDesign(array('module' => $this->Module, 'level' => $this->Level, 'id' => $NewFormNo));
        $NewFormDesign->NonExistentFile = 'IGNORE';
        $ToFile = Sanitize::SanitizeFilePath($NewFormDesign->GetFilename());

        if (!$FromFile || !$ToFile || !copy($FromFile, $ToFile))
        {
            fatal_error("Cannot copy settings to new file.  Please contact your IT department.");
        }

        return $NewFormDesign;
    }

    public function AddSuffixToFormDesign($Suffix, $typeTitle = '')
    {
        global $ModuleDefs, $JSFunctions;

        if ($Suffix)
        {
            foreach (array("HideFields", "UserExtraText", 'DefaultValues', 'UserLabels', 'ContactMatch', 'ReadOnlyFields', 'OrganisationMatch') as $Key)
            {
                if (is_array($this->$Key))
                {
                    $NewArray = array();
                    foreach ($this->$Key as $FieldName => $value)
                    {
                        unset($this->$Key[$FieldName]);
                        $NewArray[$FieldName."_".$Suffix] = $value;
                    }
                    $this->$Key = $NewArray;
                }
            }
            if (is_array($this->ExpandSections))
            {
                $NewArray = array();
                foreach ($this->ExpandSections as $FieldName => $SectionArray)
                {
                    unset($this->ExpandSections[$FieldName]);

                    $NewArray[$FieldName."_".$Suffix] = $SectionArray;

                    foreach ($SectionArray as $index => $value)
                    {
                        if ($value['section'])
                        {
                            $NewArray[$FieldName."_".$Suffix][$index]['section'] = $value['section'].'_'.$Suffix;
                        }
                    }
                }
                $this->ExpandSections = $NewArray;
            }
            if (is_array($this->ExpandFields))
            {
                $NewArray = array();
                foreach ($this->ExpandFields as $FieldName => $FieldArray)
                {
                    unset($this->ExpandSections[$FieldName]);

                    $NewArray[$FieldName."_".$Suffix] = $FieldArray;

                    foreach ($FieldArray as $index => $value)
                    {
                        if ($value['field'])
                        {
                            $NewArray[$FieldName."_".$Suffix][$index]['field'] = $value['field'].'_'.$Suffix;
                        }
                    }
                }
                $this->ExpandFields = $NewArray;
            }
            if (is_array($this->HelpTexts))
            {
                $NewArray = array();
                foreach ($this->HelpTexts as $FieldName => $value)
                {
                    unset($this->HelpTexts[$FieldName]);
                    $NewArray[$FieldName."_".$Suffix] = $value;
                }
                $this->HelpTexts = $NewArray;
                //$_SESSION['HelpTexts'] = array_merge((is_array($_SESSION['HelpTexts']) ? $_SESSION['HelpTexts'] : array()) , $NewArray);
            }
            if (is_array($this->MandatoryFields))
            {
                $NewArray = array();
                foreach ($this->MandatoryFields as $FieldName => $value)
                {
                    $FieldnameWithSuffix = $FieldName.($Suffix ? "_".$Suffix : '');
                    unset($this->MandatoryFields[$FieldName]);
                    $NewArray[$FieldnameWithSuffix] = $value;

                    // If rows are provided, make sure we are returning appropriate mandatory fields.
                    if (!(is_array($aIssueRows['Rows']) && !in_array($FieldName, $aIssueRows['Rows'])))
                    {
                        if ($this->Module == 'CON' && in_array($FieldName, $ModuleDefs[$this->Module]['LINKED_FIELD_ARRAY']))
                        {
                            $table = 'link_contacts';
                        }
                        else
                        {
                            $table = $ModuleDefs[$this->Module]['TABLE'];
                        }

                        $FieldLabel = ($this->UserLabels[$FieldnameWithSuffix] ? $this->UserLabels[$FieldnameWithSuffix] : Labels_FormLabel::GetFormFieldLabel($FieldName, "", $this->Module, $FieldnameWithSuffix, $table));

                        $mandatoryJSChanges .= 'if (mandatoryArray){mandatoryArray.push(new Array("'.$FieldName."_".$Suffix.'","'.$value.(!$aParams['no_section_suffix'] ? "_".$Suffix : '').'","'.

                        \UnicodeString::str_ireplace('"', '\"', $FieldLabel).

                        (($Suffix > 0 && !empty($typeTitle))? ' ('.($typeTitle).')' : '').'"))};
                        ';
                    }
                }
                $this->MandatoryFields = $NewArray;
            }
            if (is_array($this->FieldOrders))
            {
                $NewArray = array();
                foreach ($this->FieldOrders as $id => $Section)
                {
                    foreach ($Section as $value => $FieldName)
                    {
                        unset($this->FieldOrders[$id][$value]);
                        $NewArray[$id."_".$Suffix][$value] = $FieldName."_".$Suffix;
                    }
                }
                $this->FieldOrders = $NewArray;
            }
            if (is_array($this->ExtraFields))
            {
                $NewArray = array();
                foreach ($this->ExtraFields as $id => $Section)
                {
                    unset($this->ExtraFields[$id]);
                    $NewArray[$id] = $Section."_".$Suffix;
                }
                $this->ExtraFields = $NewArray;
            }

        }

        $JSFunctions[] = $mandatoryJSChanges;
    }

    /**
    * @desc Returns a Forms_FormDesign object populated correctly according to provided parameters
    *
    * @return obj Forms_FormDesign object representing a form design.
    */
    public static function GetFormDesign($Parameters)
    {
        $Design = new Forms_FormDesign($Parameters);
        $Design->NonExistentFile = 'DEFAULT';
        $Design->PopulateFormDesign(array('NonExistentFile' => 'DEFAULT'));

        return $Design;
    }

    /**
    * @desc Returns the ID of a form design according to provided parameters
    *
    * @return obj Forms_FormDesign object representing a form design.
    */
    public static function GetFormDesignID($Parameters)
    {
        $Design = new Forms_FormDesign($Parameters);
        return $Design->GetID();
    }

    /**
    * @desc Returns an array containing all form design globals.
    *
    * @return array Array of form design global names.
    */
    public static function getFormDesignGlobals()
    {
        return array(
            'FormTitle',
            'FormTitleDescr',
            'Show_all_section',
            'HideFields',
            'ReadOnlyFields',
            'MandatoryFields',
            'OrderSections',
            'UserLabels',
            'UserExtraText',
            'DefaultValues',
            'TextareaMaxChars',
            'FieldOrders',
            'DIF1UDFGroups',
            'ExpandSections',
            'ExpandFields',
            'HelpTexts',
            'UDFGroups',
            'NewPanels',
            'ContactMatch',
            'FormDesigns',
            'ContactForms',
            'ListingDesigns',
            'ExtraSections',
            'ExtraFields',
            'MoveFieldsToSections',
            'DisplayAsCheckboxes',
            'DisplayAsRadioButtons',
            'TimestampFields',
            'LockFieldAtribs',
            'OrganisationMatch'
        );
    }

    /**
    * @desc Gets the global form design id, ignoring user settings. This is needed for the form
    * design admin page.
    */
    public static function getGlobalFormDesignID($Module, $Level, $FormType = '')
    {
        global $ModuleDefs;

        if ($FormType == 'Search')
        {
            return GetParm($ModuleDefs[$Module]['FORMS'][$Level]['CODE'] .'_SEARCH_DEFAULT', 0, true);
        }
        else
        {
            return GetParm($ModuleDefs[$Module]['FORMS'][$Level]['CODE'] .'_DEFAULT', 0, true);
        }
    }

    public static function ShowFormDesign($Module, $FormID, $FormLevel)
    {
        global $ModuleDefs, $scripturl, $dtxtitle;

        $FormType = "Design";
        $FormMode = $FormType;

        // Makes sure that the "Linked details" sections of contact forms are always displayed.
        $ShowLinkDetails = true;

        // Make the form read only if the form id is zero and the user is not a central administrator in a centrally administered system
        if (IsCentrallyAdminSys())
        {
            if ($FormID == 0 && !IsCentralAdmin())
            {
                $ReadOnly = true;
            }
            else
            {
                $ReadOnly = false;
            }
        }
        else
        {
            $ReadOnly = false;
        }

        $FormDesign = Forms_FormDesign::GetFormDesign(array(
            'module'   => $Module,
            'id'       => $FormID,
            'level'    => $FormLevel,
            'readonly' => $ReadOnly
        ));
        $FormDesign->LoadFormDesignIntoGlobals();

        require GetBasicFormFileName($Module, $FormLevel);

        $FormsFileName = GetFormsFilename($Module);

        if (file_exists($FormsFileName))
        {
            include($FormsFileName);
        }

        ValidateFormDesign();

        $buttonGroup = new ButtonGroup();

        if (IsCentrallyAdminSys())
        {
            if ($FormID == 0 && IsCentralAdmin())
            {
                $buttonGroup->AddButton(array(
                    'id' => 'btnSaveDesign',
                    'name' => 'btnSaveDesign',
                    'label' => 'Save settings',
                    'onclick' => 'submitClicked = true;selectAllMultiCodes();checkDatixSelects();setSectionsExpanded();document.frmDesign.rbWhat.value=\'SaveDesign\';document.frmDesign.submit();',
                    'action' => 'SAVE'
                ));

                if (file_exists($FormDesign->getFilename(array('NonExistentFile' => 'IGNORE'))))
                {
                    $buttonGroup->AddButton(array(
                        'id' => 'btnDefaultDesign',
                        'name' => 'btnDefaultDesign',
                        'label' => 'Reset to default',
                        'onclick' => 'if(confirm(\'' . _tk("reset_default_form_confirm") . '\')){document.frmDesign.rbWhat.value=\'ClearDesign\';document.frmDesign.submit();}',
                        'action' => 'BACK'
                    ));
                }
            }

            if ($FormID > 0)
            {
                $buttonGroup->AddButton(array(
                    'id' => 'btnSaveDesign',
                    'name' => 'btnSaveDesign',
                    'label' => 'Save settings',
                    'onclick' => 'submitClicked = true;selectAllMultiCodes();checkDatixSelects();setSectionsExpanded();document.frmDesign.rbWhat.value=\'SaveDesign\';document.frmDesign.submit();',
                    'action' => 'SAVE'
                ));
                $buttonGroup->AddButton(array(
                    'id' => 'btnDeleteDesign',
                    'name' => 'btnDeleteDesign',
                    'label' => _tk('btn_delete_form'),
                    'onclick' => 'if(confirm(\'' . _tk("delete_confirm") . '\')){document.frmDesign.rbWhat.value=\'ClearDesign\';document.frmDesign.submit();}',
                    'action' => 'DELETE'
                ));
            }
        }
        else
        {
            $buttonGroup->AddButton(array(
                'id' => 'btnSaveDesign',
                'name' => 'btnSaveDesign',
                'label' => 'Save settings',
                'onclick' => 'submitClicked = true;selectAllMultiCodes();checkDatixSelects();setSectionsExpanded();document.frmDesign.rbWhat.value=\'SaveDesign\';document.frmDesign.submit();',
                'action' => 'SAVE'
            ));

            if ($FormID <= 0 && file_exists($FormDesign->getFilename(array('NonExistentFile' => 'IGNORE'))))
            {
                $buttonGroup->AddButton(array('id' => 'btnDefaultDesign', 'name' => 'btnDefaultDesign', 'label' => 'Reset to default', 'onclick' => 'if(confirm(\'' . _tk("reset_default_form_confirm") . '\')){document.frmDesign.rbWhat.value=\'ClearDesign\';document.frmDesign.submit();}', 'action' => 'BACK'));
            }
            elseif ($FormID > 0)
            {
                $buttonGroup->AddButton(array('id' => 'btnDeleteDesign', 'name' => 'btnDeleteDesign', 'label' => _tk('btn_delete_form'), 'onclick' => 'if(confirm(\'' . _tk("delete_confirm") . '\')){document.frmDesign.rbWhat.value=\'ClearDesign\';document.frmDesign.submit();}', 'action' => 'DELETE'));
            }
        }

        $buttonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => 'Cancel', 'onclick' => 'if(confirm(\'' . _tk("cancel_confirm") . '\')){ document.forms[0].rbWhat.value=\'CancelDesign\';document.frmDesign.submit();}', 'action' => 'CANCEL'));

        getPageTitleHTML(array(
            'title'  => 'Design - ' . $FormDesign->FormTitle,
            'module' => $Module,
         ));

        GetSideMenuHTML(array(
            'module' => 'ADM',
            'buttons' => $buttonGroup
        ));

        template_header_nopadding();

        echo '
            <form method="post" name="frmDesign" action="' . $scripturl . '?action=savedesignsettings">
                <input type="hidden" name="formlevel" value="' . $FormLevel . '">
                <input type="hidden" name="module" value="' . $Module . '">
                <input type="hidden" name="usersettingsfile" value="' . $FormDesign->getFilename() . '">
                <input type="hidden" name="settingsfile" value="' . $SettingsFile . '">
                <input type="hidden" name="form_id" value="' . $FormID . '">
                <input type="hidden" name="rbWhat" value="Save">
        ';

        // Here is where we make up the form
        $FormTable = new FormTable('Design', $Module, $FormDesign);
        $FormTable->MakeForm($FormArray, array(), $Module);

        $FormTable->AddExpandCollapseAllSectionsJS();
        $FormTable->MakeTable();
        echo $FormTable->GetFormTable();

        //no extra fields allowed in medications module.
        if($Module != 'MED' && (!IsCentrallyAdminSys() || $FormID != 0 || IsCentralAdmin()))
        {
            $NumExtraSections = (!empty($FormDesign->ExtraSections) ? max(array_keys($FormDesign->ExtraSections)) : 0);
            echo '<div><input type="button" value="Add new section" onclick="addNewFormDesignSection('.($NumExtraSections + 1).')"></div>';
        }

        echo $buttonGroup->getHTML();

        echo '</form>';

        footer();
    }

    public static function WriteFormsFile($FormFiles, $FormFiles2, $module)
    {
        $FormsFileName = GetFormsFilename($module);
        // Create output file.
        $OutputFile = @fopen($FormsFileName, "wb");

        if (!$OutputFile)
        {
            fatal_error("Cannot create form settings file.  Please contact your IT department.");
        }
        if (!fwrite($OutputFile,"<?php\n"))
        {
            fatal_error("Cannot write to form settings file.  Please contact your IT department.");
        }

        fwrite($OutputFile, '$FormFiles = ' . var_export($FormFiles, true) . ";\n");

        fwrite($OutputFile, '$FormFiles2 = ' . var_export($FormFiles2, true) . ";\n");

        fwrite($OutputFile, "?>");

        fclose($OutputFile);
    }

    function UnsetDefaultValues()
    {
        if (is_array($this->DefaultValues))
        {
            foreach ($this->DefaultValues as $Field => $DefaultVal)
            {
                if (!$this->HideFields[$Field])
                {
                    unset($this->DefaultValues[$Field]);
                }
            }
        }
    }

    /**
     * @desc Function that determines if a field is locked on a centrally administered system
     *
     * @param string $Section The name of the form design section
     * @param string $FieldName The name of the field
     * @param string $FieldType The type of the field (e.g. EXPANDFIELD)
     * @return bool True if field is locked on a centrally adminnistered system and viewed by a local admin, otherwise false
     */
    public function IsFieldLocked($Section, $FieldName, $FieldType)
    {
        if (IsCentrallyAdminSys())
        {
        	if(IsCentralAdmin())
        	{
        		return false;
        	}
        	
            if ($this->LockFieldAtribs[$Section]["SECTION"] ||
                $this->LockFieldAtribs[$FieldName]["ALLFIELD"] ||
                $this->LockFieldAtribs[$FieldName][$FieldType])
            {
                return true;
            }
        }

        return false;
    }

    /**
     * @desc Function to determine if a section has a field with the property Hidden locked.
     *
     * @param string $Section Name of the section
     * @param array $FormArray The form settings
     * @return bool True if section has at least one field hidden, false otherwise
     */
    public function HasSectionLockedHiddenFields($Section, $FormArray)
    {
        if (IsCentralAdmin())
        {
            return false;
        }

        if (IsCentrallyAdminSys())
        {
            if (isset($FormArray[$Section]['Rows']))
            {
                foreach ($FormArray[$Section]['Rows'] as $Field)
                {
                    if (is_array($Field))
                    {
                        $FieldName = $Field['Name'];
                    }
                    else
                    {
                        $FieldName = $Field;
                    }

                    if (isset($this->LockFieldAtribs[$FieldName]['HIDE']))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @desc Function to determine if a section has a fully locked field.
     *
     * @param string $Section Name of the section
     * @param array $FormArray The form settings
     * @return bool True if section has at least one fully locked field, false otherwise
     */
    public function HasSectionFullyLockedField($Section, $FormArray)
    {
        if (IsCentralAdmin())
        {
            return false;
        }

        if (IsCentrallyAdminSys())
        {
            if (isset($FormArray[$Section]['Rows']))
            {
                foreach ($FormArray[$Section]['Rows'] as $Field)
                {
                    if (is_array($Field))
                    {
                        $FieldName = $Field['Name'];
                    }
                    else
                    {
                        $FieldName = $Field;
                    }

                    if (isset($this->LockFieldAtribs[$FieldName]['ALLFIELD']))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}

