<?php
class Forms_MultiSelectField extends Forms_SelectField
{
	/**
	 * Can be flagged in order to ignore maxlength for fields which don't
	 * require it (i.e. fields which aren't persisting data in the DB).
	 * 
	 * @var boolean
	 */
	protected $ignoreMaxLength;
    
    /**
    * Used when passing in a locked set of codes where array keys are codes.
    * Those codes will be locked on the filed with padlock icons with ho hovering and clicking action
    * @var array
    */
    public $lockedCodes;
    
    /**
     * Custom javascript check when filtering available codes for the select list.
     * 
     * @var string
     */
    protected $customIncludeListOption;
	
    /**
    * Constructor: converts current value to a string before invoking parent constructor.
    * 
    * @param mixed  $name
    * @param mixed  $module
    * @param string $value
    * @param mixed  $mode
    * @param mixed  $label
    * @param mixed  $formSection
    * @param mixed  $changedValue
    * @param mixed  $suffix
    * 
    * @return Forms_MultiSelectField
    */
    public function __construct($name, $module, $value, $mode, $label = '', $formSection = '', $changedValue = '', $suffix = '')
    {
        if (is_array($value))
        {
            $value = implode(' ', $value);
        }
        $this->ignoreMaxLength = false;
        parent::__construct($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix);    
    }
    
    /**
     * Set the ignoreMaxLength flag.
     */
    public function setIgnoreMaxLength()
    {
    	$this->ignoreMaxLength = true;	
    }
    
    /**
    * Creates the HTML for this field.
    */
    protected function createHTML($readonly = false)
    {
        $maxLength = $this->getMaxLength();
        
        if ($this->mode == 'search')
        {
            $this->html = '<div class="dropdown-wrapper" style="clear:both"><input id="'.$this->fieldId.'_title" name="'.$this->fieldId.'" type="text" class="codefield multi_select" '.
            				'maxLength="'.$maxLength.'" value="'.htmlspecialchars($this->value).'" autocomplete="off" onfocus="initDropdown(jQuery(this));" />';

            if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
            {
                $this->html .= '<a href="javascript:jQuery(\'#'.$this->fieldId.'_title\').dropdownButton();" id="img_add_'.$this->fieldId.'" name="img_add_' . $this->fieldId . '" class="dropdown_button_image_mobile"></a>';
            }
            else
            {
                $this->html .= '<a href="javascript:jQuery(\'#'.$this->fieldId.'_title\').dropdownButton();" id="img_add_'.$this->fieldId.'" name="img_add_' . $this->fieldId . '" class="dropdown_button_image"></a>';
            }

            $this->html .= '</div>';
        }
        else
        {
            $values = array();
            $codes = $this->value != '' ? explode(' ', $this->value) : array();
            foreach ($codes as $code)
            {
                $values[$code] = htmlspecialchars($this->getDescription($code), ENT_COMPAT, 'UTF-8', false);            
            }
            natsort($values);
            
            if ($this->mode == 'readonly' || $readonly)
            {
                $description = implode('<br/>', $values);
                $this->html = '<div id="'.$this->fieldId.'_title">'.str_replace(' ', '&nbsp;', $description).'</div>';
                foreach ($values as $key => $value)
                {
                	 $options .= '<option value="'.$key.'" selected="selected"></option>';    
                }
            }
            else
            {
                $description = '';

                foreach ($values as $key => $value)
                {
                    $description .= '<li id="'.$key.'" class="datixSelectOption" ';
                    if (!isset($this->lockedCodes[$key]))
                    {
                        $description .=
                        'onmouseover="jQuery(\'#'.$this->fieldId.'_title\').activate(this);" ' .
                        'onmouseout="jQuery(\'#'.$this->fieldId.'_title\').deactivate(this);" ' .
                        'onmousedown="jQuery(\'#'.$this->fieldId.'_title\').selectItem(this);"';
                    }
                    $description .= '>'.str_replace(' ', '&nbsp;', $value);
                    if(isset($this->lockedCodes[$key]))
                    {
                    	$description .= ' <img height="12" src="Images/icons/lock.png" alt="'._tk('locked').'" title="'._tk('locked').'"/>';
                    }
                    $description .= '</li>';
                    
                    
                    $options .= '<option value="'.$key.'" selected="selected"></option>';            
                }
                $this->html = '<div>'.
                                  '<div id="'.$this->fieldId.'_values" class="codefield multiSelectList">'.
                                      '<ul>'.
                                          $description.            
                                      '</ul>'.
                                  '</div>'.
                                  '<a href="javascript:jQuery(\'#'.$this->fieldId.'_title\').deleteSelectedItems();" id="img_add_'.$this->fieldId.'" name="img_add_'.$this->fieldId.'" class="mutlilistbox_image"></a>'.
                              '</div>'.
                              '<div class="dropdown-wrapper" style="clear:both">'.
                                  '<input id="'.$this->fieldId.'_title" type="text" class="codefield multi_select" maxLength="'.$maxLength.'" autocomplete="off" onfocus="initDropdown(jQuery(this));" title="' . $this->label . '" />';

                                  if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
                                  {
                                      $this->html .= '<a href="javascript:jQuery(\'#'.$this->fieldId.'_title\').dropdownButton();" id="img_add_'.$this->fieldId.'" name="img_add_' . $this->fieldId . '" class="dropdown_button_image_mobile"></a>';
                                  }
                                  else
                                  {
                                      $this->html .= '<a href="javascript:jQuery(\'#'.$this->fieldId.'_title\').dropdownButton();" id="img_add_'.$this->fieldId.'" name="img_add_'.$this->fieldId.'" class="dropdown_button_image">'.
                                  '</a>';
                                  }
                $this->html .= '</div>';
            }
            $this->html .= '<select style="display:none" name="'.$this->fieldId.'[]" id="'.$this->fieldId.'" multiple="multiple" title="hidden select field">'.$options.'</select>';
        }  
    }
    
    /**
    * Extends parent function by adding multiselect-specific config options.
    * 
    * @global array $JSFunctions
    */
    protected function setGlobalJavascript()
    {
        global $JSFunctions;
        
        parent::setGlobalJavascript();
        
        if ($this->mode == 'search')
        {
            $JSFunctions[] = 'jQuery(\'#'.$this->fieldId.'_title\').data("search", true);';
        }
        
        if (isset($this->customIncludeListOption))
        {
            $JSFunctions[] = 'jQuery(\'#'.$this->fieldId.'_title\').data("customIncludeListOption", "'.\UnicodeString::str_ireplace('"', '\"', $this->customIncludeListOption).'");';
        }
    }
    
    /**
     * Retrieves the max length for this field, used to prevent truncation errors
     * when adding too many codes.
     * 
     * @global array $FieldDefsExtra
     * 
     * @return string The max length for this field
     * 
     * @throws InvalidConfigException
     */
    protected function getMaxLength()
    {
    	global $FieldDefsExtra;
    	
    	if ($this->mode == 'search' || $this->ignoreMaxLength)
    	{
    		return '';	
    	}
    	
    	if ($this->isUDF())
    	{
    		return '254';	
    	}
    	
    	if (isset($FieldDefsExtra[$this->module][$this->name]['MaxLength']))
    	{
    		return $FieldDefsExtra[$this->module][$this->name]['MaxLength'];	
    	}
    	
    	throw new InvalidConfigException('The MaxLength attibute for '.$this->name.' is missing in FieldDefs');
    }

    /**
    * Setter for the locked code list. Expects an array $GLOBALS["LockFieldAtribs"]
    * @param array $LockFieldAtribs
    */
    public function setLockedCodes(array $LockFieldAtribs)
    {
    	$LockedFields = array();

        if (!empty($LockFieldAtribs))
        {
            foreach ($LockFieldAtribs as $FieldName => $Attributes)
            {
                if (\UnicodeString::strpos($FieldName, 'UDF') !== false)
                {
                    if (sizeof($Attributes) > 0)
                    {
                        $FielNameArray = explode('_', $FieldName);
                        $FieldNameNew = $FielNameArray[3] . '_' . $FielNameArray[2];
                        $LockedFields[$FieldNameNew] = true;
                    }
                }
            }
        }

        $this->lockedCodes = $LockedFields;
    }
    
    /**
     * Setter for customIncludeListOption.
     * 
     * @param string $customIncludeListOption
     */
    public function setCustomIncludeListOption($customIncludeListOption)
    {
        $this->customIncludeListOption = $customIncludeListOption;
    }
}