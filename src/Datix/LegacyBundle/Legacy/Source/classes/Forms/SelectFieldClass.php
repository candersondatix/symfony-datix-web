<?php
/**
* Single select coded field class.
*/
class Forms_SelectField extends Forms_FormField
{
    /**
    * The field's parent fields.
    *
    * @var array
    */
    protected $parents;

    /**
    * The field's child fields.
    *
    * @var array
    */
    protected $children;

    /**
    * Used when passing in a custom set of codes, rather than using standard code retrieval.
    *
    * @var array
    */
    protected $customCodes;

    /**
    * Used to retrieve descriptions in a special case in which the field is using another field's codes.
    *
    * @var string
    */
    protected $mimicField;

    /**
    * Used to retrieve descriptions in a special case in which the field is using another field's codes.
    *
    * @var string
    */
    protected $mimicModule;

    /**
    * Custom function for retrieving codes.
    *
    * @var string
    */
    protected $selectFunction;

    /**
    * Parameters for select function, passed as POST variables in AJAX call.
    *
    * @var array
    */
    protected $selectFunctionParameters;

    /**
    * Contains the keys of values in the parents array which should ignore suffix values
    * (e.g. when the parent field is outside of a dynamic section).
    *
    * @var array
    */
    protected $noParentSuffix;

    /**
    * If set then the field is hidden and set to this value.
    *
    * @var string
    */
    protected $defaultHidden;

    /**
    * Can be set to prevent codes being displayed if WEB_SHOW_CODES = Y.
    *
    * @var boolean
    */
    protected $suppressCodeDisplay;

    /**
    * Can be set to prevent FieldChanged() from being called onchange.
    *
    * @var boolean
    */
    protected $suppressFieldChangedFlag;

    /**
    * Used in special cases where we're using mapped fields so we can use the correct field name to retrieve descriptions.
    *
    * @var string
    */
    protected $fieldFormatsName;

    /**
    * True if we don't want the field to be dynamically resized.
    *
    * @var boolean
    */
    protected $noResize;

    /**
    * Free text fields allow users to input any value but also search from a list of defined values (e.g. old medication fields).
    *
    * @var boolean
    */
    protected $freeText;

    /**
    * Can be set to true so codes are always retrieved from the DB.
    *
    * @var boolean
    */
    protected $disableCache;

    /**
    * Width of the input field (in pixels)
    *
    * @var int
    */
    protected $width;

    /**
    * Can be set to manually determine the description displayed when the dropdown is first rendered.
    *
    * @var string
    */
    protected $description;
    
    /**
     * The DB table that this field maps to.
     * 
     * Used when retrieving codes.
     * 
     * @var string
     */
    protected $table;

    /**
    * Retrieves the description corresponding to the currently selected code.
    *
    * @param  string  $code         The code we're fetching the description for.
    *
    * @return string  $description
    */
    protected function getDescription($code)
    {
        if (isset($this->description))
        {
            return $this->description;
        }

        if (isset($this->customCodes))
        {
            if (isset($this->customCodes[\UnicodeString::strtoupper($code)]) || isset($this->customCodes[$code]))
            {
                if(!isset($this->customCodes[$code]))
                {
                    $code = \UnicodeString::strtoupper($code);
                }

                if (is_array($this->customCodes[$code]))
                {
                    // may be an array containing description and code colour
                    $description = $this->customCodes[$code]['description'];
                }
                else
                {
                    $description = $this->customCodes[$code];
                }
            }
            else
            {
                $description = $code;
            }
        }
        else
        {
            // determine the field name used to lookup description
            if (isset($this->fieldFormatsName))
            {
                $fieldName = $this->fieldFormatsName;
            }
            else if (isset($this->mimicField))
            {
                $fieldName = $this->mimicField;
            }
            else
            {
                $fieldName = $this->name;
            }

            // determime the module used to lookup description
            $module = isset($this->mimicModule) ? $this->mimicModule : $this->module;

            // use appropriate function to lookup description
            if ($this->isUDF())
            {
                // UDF format either UDF_<Type>_<GroupID>_<FieldID> or UDF_<FieldID>
                $udfParts = explode('_', $fieldName);
                $fieldId = $udfParts[3] ? $udfParts[3] : $udfParts[1];

                $description = code_descr_udf($fieldId, $code, $module);
            }
            else
            {
                $description = code_descr($module, $fieldName, $code, '', false);
            }
        }

        if ($description == $code && isset($this->selectFunction) && $this->selectFunction != '')
        {
            require_once 'Source/libs/SelectFunctions.php';

            $function = $this->selectFunction;
            $codes = $function();

            if ($codes[$code] != '')
            {
                $description = $codes[$code];
            }
            else
            {
                $description = $code;
            }
        }

        if (!$this->suppressCodeDisplay && !$this->freeText && $description != '' && bYN(GetParm('WEB_SHOW_CODE', 'N')))
        {
            // display code as well as description
            $description .= ': '.$code;
        }

        return $description;
    }

    /**
    * Retrieves the colour assigned to the current code (if any).
    *
    * Colours will only be displayed if COMBO_COLOUR = Y.
    *
    * @return string $colour The colour in hex format.
    */
    protected function getColour()
    {
        $colour = '';

        if ($this->value != '' && bYN(GetParm('COMBO_COLOUR', 'Y')))
        {
            if (is_array($this->customCodes[$this->value]) && isset($this->customCodes[$this->value]['colour']))
            {
                $colour = $this->customCodes[$this->value]['colour'];
            }
            else
            {
                try
                {
                    $FDR_info = new Fields_Field($this->fieldFormatsName ?: $this->name, $this->table);
                    $FDR_info->setValue($this->value);
                    $FDR_info->getDisplayInfo();
                    $colour = $FDR_info->getCodeColour();
                }
                catch (Exception $e) {}
            }
        }

        if ($colour == '')
        {
            $colour = $this->mode == 'readonly' ? 'F5F9FF' : 'FFF';
        }

        return $colour;
    }

    /**
    * Used to determine whether or not this is a User Defined Field.
    *
    * Overrides parent function so mimic fields can be taken into account.
    *
    * @return boolean
    */
    protected function isUDF()
    {
        if (isset($this->fieldFormatsName))
        {
            return \UnicodeString::substr($this->fieldFormatsName, 0, 3) == 'UDF';    
        }
        if (isset($this->mimicField))
        {
            return \UnicodeString::substr($this->mimicField, 0, 3) == 'UDF';
        }
        else
        {
            return \UnicodeString::substr($this->name, 0, 3) == 'UDF';
        }
    }

    /**
    * Creates the HTML for this field.
    */
    protected function createHTML($readonly = false)
    {
        global $FieldDefs;

        $isTablet = \src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet();

        $description = $this->getDescription($this->value);
        $colour = $this->getColour();

        if ($this->mode == 'readonly' || $readonly)
        {
            $this->html = '<span id="'.$this->fieldId.'_title"'.($colour != '' ? ' style="background-color:#'.$colour.';padding:1px;"' : '').'>'.str_replace(' ', '&nbsp;', htmlspecialchars($description, ENT_COMPAT, 'UTF-8', false)).'</span>';
        }
        else
        {
            $fieldRequiresMinCharacters = $FieldDefs[$this->module][$this->name]['requireMinChars'];
            $fieldIsDrugField = (in_array($this->fieldId, array('inc_med_drug', 'inc_med_drug_rt')) || preg_match('/imed_name_(admin|correct)_[0-9]+/i', $this->fieldId) > 0);

            if($fieldRequiresMinCharacters || ($isTablet && $fieldIsDrugField))
            {
                $this->data .= ' data-search-min-chars="4"';

                if($isTablet)
                {
                    $wrapper_class = ' disable-button';
                }
            }

            $this->html = '<span role="status" class="aria" aria-live="polite"></span>' .
                '<span class="dropdown-wrapper' . $wrapper_class . '"><input id="'.$this->fieldId.'_title" type="text" class="codefield ff_select"' . $this->data . ' value="'.htmlspecialchars($description, ENT_COMPAT, 'UTF-8', false).'" autocomplete="off" style="background-color:#' . $colour . ';' . (isset($this->width) ? ' min-width:0px; width:' . $this->width . 'px;' : '') . '" onfocus="initDropdown(jQuery(this));" title="' . $this->label . '" />';

            if($isTablet)
            {
                $this->html .= '<a href="javascript:jQuery(\'#'.$this->fieldId.'_title\').dropdownButton();" id="img_add_'.$this->fieldId.'" name="img_add_' . $this->fieldId . '" class="dropdown_button_image_mobile"></a>';
            }
            else
            {
                $this->html .= '<a href="javascript:jQuery(\'#'.$this->fieldId.'_title\').dropdownButton();" id="img_add_'.$this->fieldId.'" name="img_add_' . $this->fieldId . '" class="dropdown_button_image"></a>';
            }

            $this->html .= '</span>'; // close .dropdown-wrapper
        }
        $this->html .= '<input type="hidden" name="'.$this->fieldId.'" id="'.$this->fieldId.'" value="'.htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8', false).'" />';
    }

   /**
    * Creates the HTML for this field if it's hidden with a default value.
    * 
    * @param boolean $createHiddenTitle Whether we need to create a hidden "_title" element for this field to check combo-links.
    */
    protected function createDefaultHiddenHTML($createHiddenTitle = false)
    {
        $this->html = '<input type="hidden" name="'.$this->fieldId.'" id="'.$this->fieldId.'" value="'.htmlspecialchars($this->defaultHidden, ENT_COMPAT, 'UTF-8', false).'"/>';
        if ($createHiddenTitle)
        {
            $this->html .= '<input type="hidden" class="ff_select" id="'.$this->fieldId.'_title" value="'.htmlspecialchars($this->defaultHidden, ENT_COMPAT, 'UTF-8', false).'"/>'; // value set here so checkDatixSelects() doesn't remove the value on submit
        }
    }

    /**
    * Creates the javascript necesary for the functioning of this field.
    *
    * @global array $JSFunctions
    */
    protected function setGlobalJavascript()
    {
        global $JSFunctions, $FieldDefs;

        // define javascript used in non-standard code retrieval
        if (isset($this->customCodes))
        {
            // define custom code list as javascript array
            $codes = array();
            foreach($this->customCodes as $code => $description)
            {
                if (is_array($description))
                {
                    // each element in the custom codes array could be an array containing both the description and the code's colour
                    $codes[] = '{"value":"'.$code.'","description":"'.str_replace('"','\\"', htmlfriendly($description['description'])).'","colour":"'.($description['colour'] == '' ? 'FFF' : $description['colour']).'"}';
                }
                else
                {
                    $codes[] = '{"value":"'.$code.'","description":"'.str_replace('"','\\"', htmlfriendly($description)).'"}';
                }
            }
            $JSFunctions[] = 'customCodes["'.$this->fieldId.'"] = ['.implode(',', $codes).'];';
        }
        else if (!empty($this->selectFunctionParameters))
        {
            // define select function parameters as javascript array
            $parameters = 'var '.$this->fieldId.'SelectFunctionParameters = {};';
            foreach ($this->selectFunctionParameters as $key => $value)
            {
                $parameters .= $this->fieldId.'SelectFunctionParameters["' . $key . '"] = ' . $value . ';';
            }
            $JSFunctions[] = $parameters;
        }

        // define any additional onchange functions for this field
        if (isset($this->onChangeExtra))
        {
            $JSFunctions[] = 'function OnChange_Extra_' . $this->fieldId . '(){' . $this->onChangeExtra . '}';
        }

        // define parents
        if (!empty($this->parents))
        {
            $parentJS = ", parents: ['".implode("','", $this->parents)."']";
        }

        // define children
        if (!empty($this->children))
        {
            $childrenJS = ", children: ['".implode("','", $this->children)."']";
        }
        
        // define the table this field maps to
        if (!isset($this->table))
        {
            $this->table = GetTableForField($this->name, $this->module);
        }

        // define config options for this field
        $JSFunctions[] = 'jQuery(\'#'.$this->fieldId.'_title\').data({'.
                            'field: "'.(isset($this->fieldFormatsName) ? $this->fieldFormatsName : $this->name).'", '.
                            'fieldId: "'.$this->fieldId.'", '.
                            'module: "'.$this->module.'", '.
                            'fieldmode: "'.$this->mode.'"'.
                            $parentJS.
                            $childrenJS.
                            (\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet() ? ', mobile: true' :'').
                            (isset($this->customCodes) ? ', customCodes: true' : '').
                            (isset($this->selectFunction) ? ', selectFunction: "'.$this->selectFunction.'"' : '').
                            (isset($this->selectFunctionParameters) ? ', selectFunctionParameters: '.$this->fieldId.'SelectFunctionParameters' : '') .
                            (bYN(GetParm('RETAIN_COMBO_CHILDREN', 'Y')) ? ', childcheck: true' : '').
                            (bYN(GetParm('CACHE_CODES', 'Y')) && !$this->disableCache ? ', cacheCodes: true' : '').
                            (!$this->suppressCodeDisplay && bYN(GetParm('WEB_SHOW_CODE', 'N')) ? ', showCodes: true' : '').
                            ($this->noResize ? ', noresize: true' : '').
                            (bYN(GetParm('COMBO_COLOUR', 'Y')) ? ', showColour: true' : '').
                            ($this->freeText ? ', freeText: true' : '').
                            ($this->suppressFieldChangedFlag ? ', suppressFieldChangedFlag: true' : '').
                            ', table: "'.$this->table.'"'.
                         '});';
    }

    /**
    * Retrieves parent/child field values from session (if not manually configured)
    * and adds a suffix to these values where appropriate.
    */
    protected function initialiseParenting()
    {
        if (!isset($this->parents))
        {
            $this->parents = array_filter(GetParents(array('field' => $this->name, 'module' => $this->module)));
        }

        if (!isset($this->children))
        {
            $this->children = array_filter(GetChildren(array('field' => $this->name, 'module' => $this->module)));
        }

        if ($this->suffix)
        {
            foreach ($this->parents as $key => $value)
            {
                if (empty($this->noParentSuffix) || !in_array($key, $this->noParentSuffix))
                {
                    $this->parents[$key] = $value.'_'.$this->suffix;
                }
            }

            foreach ($this->children as $key => $value)
            {
                $this->children[$key] = $value.'_'.$this->suffix;
            }
        }
    }

    /**
    * Builds the field (initialises parent/child fields and creates javascript/html).
    *
    * @return string
    */
    protected function build($readonly = false)
    {
        if ($this->defaultHidden)
        {
            $createHiddenTitle = false;
            
            if (bYN(GetParm('RETAIN_COMBO_CHILDREN', 'Y')))
            {
                $this->initialiseParenting();
                if (!empty($this->parents))
                {
                    $this->setGlobalJavascript();
                    $createHiddenTitle = true;
                }
            }
            
            $this->createDefaultHiddenHTML($createHiddenTitle);
        }
        else
        {
            $this->initialiseParenting();
            $this->setGlobalJavascript();
            $this->createHTML($readonly);
        }
    }

    /**
    * Setter for parent field values, can be used if not set in field_formats/combo_links.
    *
    * @param array $parents
    *
    * @codeCoverageIgnore
    */
    public function setParents(array $parents)
    {
        $this->parents = $parents;
    }

    /**
    * Setter for child field values, can be used if not set in field_formats/combo_links.
    *
    * @param array $children
    *
    * @codeCoverageIgnore
    */
    public function setChildren(array $children)
    {
        $this->children = $children;
    }

    /**
    * Setter for noParentSuffix parameters.
    *
    * @param array $keys The keys that relate to the values in the parents array that should ignore the suffix.
    *
    * @codeCoverageIgnore
    */
    public function setNoParentSuffix(array $keys)
    {
        $this->noParentSuffix = $keys;
    }

    /**
    * Setter for the custom code list.
    *
    * Expects an array with the codes as keys and descriptions as values.
    *
    * @param array $codes
    *
    * @codeCoverageIgnore
    */
    public function setCustomCodes(array $codes)
    {
        $this->customCodes = $codes;
    }
    
    /**
    * Setter for the custom code list.
    *
    * Expects an array with the codes as keys and descriptions as values.
    *
    * @param array $codes
    *
    * @codeCoverageIgnore
    */
    public function setCustomCodeCollection(\src\system\database\code\CodeCollection $codeCollection)
    {
        foreach ($codeCollection as $codeObject)
        {
            $this->customCodes[$codeObject->code] = $codeObject->description;
        }
    }

    /**
    * Allows the setting of a custom value for the field's name/id attribute if different from the field name.
    *
    * @param string $altFieldName
    *
    * @codeCoverageIgnore
    */
    public function setAltFieldName($altFieldName)
    {
        $this->fieldId = $altFieldName;
    }

    /**
    * Setter for the mimic properties which are used to retrieve descriptions when the field is using another's codes.
    *
    * @param string $field
    * @param string $module
    *
    * @codeCoverageIgnore
    */
    public function setMimic($field, $module)
    {
        $this->mimicField   = $field;
        $this->mimicModule  = $module;
    }

    /**
    * Define a 'select' (custom on-the-fly code retrieval) function for this field.
    *
    * @param string $name
    * @param array  $parameters Literal strings should be passed in surrounded by quotes.
    *
    * @codeCoverageIgnore
    */
    public function setSelectFunction($name, array $parameters = null)
    {
        $this->selectFunction = $name;
        $this->selectFunctionParameters = $parameters;
    }

    /**
    * Setter for default hidden value.
    *
    * @param string $value
    *
    * @codeCoverageIgnore
    */
    public function setDefaultHidden($value)
    {
        $this->defaultHidden = $value;
    }

    /**
    * Setter for suppression of code display.
    *
    * @codeCoverageIgnore
    */
    public function setSuppressCodeDisplay()
    {
        $this->suppressCodeDisplay = true;
    }

    /**
    * Setter for suppression of code display.
    *
    * @codeCoverageIgnore
    */
    public function setSuppressFieldChangedFlag()
    {
        $this->suppressFieldChangedFlag = true;
    }

    /**
    * Setter for FieldFormatsName.
    *
    * @codeCoverageIgnore
    */
    public function setFieldFormatsName($name)
    {
        $this->fieldFormatsName = $name;
    }

    /**
    * Setter for noResize.
    *
    * @codeCoverageIgnore
    */
    public function setNoResize()
    {
        $this->noResize = true;
    }

    /**
    * Setter for freeText.
    *
    * @codeCoverageIgnore
    */
    public function setFreeText()
    {
        $this->freeText = true;
    }

    /**
    * Setter for disableCache.
    *
    * @codeCoverageIgnore
    */
    public function setDisableCache()
    {
        $this->disableCache = true;
    }

    /**
    * Setter for width.
    *
    * @param $width
    *
    * @codeCoverageIgnore
    */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
    * Setter for description.
    *
    * @param $description
    *
    * @codeCoverageIgnore
    */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * Setter for table.
     * 
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }
}
