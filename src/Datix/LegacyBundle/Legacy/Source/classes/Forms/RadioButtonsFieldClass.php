<?php
/**
 * Multiple checkboxes field class.
 */
class Forms_RadioButtonsField extends Forms_SelectField
{
    protected $values  = array ();
    protected $mandatory = false;
    
    /**
    * @param mixed  $name
    * @param mixed  $module
    * @param string $value
    * @param mixed  $mode
    * @param mixed  $label
    * @param mixed  $formSection
    * @param mixed  $changedValue
    * @param mixed  $suffix
    * 
    * @return Forms_MultiSelectField
    */
    public function __construct($name, $module, $value, $mode, $label = '', $formSection = '', $changedValue = '', $suffix = '')
    {
        if (is_array($value))
        {
            // parent still needs a concatenated string
            $value = implode(' ', $value);
        }

        parent::__construct($name, $module, $value, $mode, $label, $formSection, $changedValue, $suffix);    
    }
    
    /**
     * Setter for mandatory option
     * @param boolean $value
     */
    public function setMandatory ($value)
    {
        $this->mandatory = (bool) $value;
    }
    
    /**
    * Creates the HTML for this field.
    */
    protected function createHTML($readonly = false)
    {
        $this->html =   '<div id="'.$this->fieldId.'_values">'.
                            '<ul>';

        //This is a 'temporary' hack, see refactoring ticket DW-12290 if you want to fix it properly - CA
        if($this->name === 'inc_reportedby')
        {
            $selected = [$this->value];
        } else {
            $selected = preg_split ('/ /u', $this->value);
        }


        // non-mandatory fields need a "none" option
        if (!$this->mandatory) 
        {
            $extra = ' onclick="FieldChanged(\''.$this->fieldId.'\')"';

            // is it checked?
            if (empty($this->value))
            {
                $extra .= ' checked="checked"';
            }

            // read-only?
            if ($this->mode == 'readonly' || $readonly)
            {
                $extra .= ' disabled';
            }

            $this->html .= '<li><input type="radio" name="'.$this->fieldId.'" id="'.$this->fieldId.'_none" value=""'.$extra.'/> <label class="field_label" for="'.$this->fieldId.'_none">'._tk('none').'</label></li>';
        }

        // build options
        foreach ($this->values as $code => $desc)
        {
            //$extra = ' onchange="setChanged(\''.$this->fieldId.'\')"';
            $extra = ' onclick="FieldChanged(\''.$this->fieldId.'\')"';
            
            // is it checked?
            if (in_array((string) $code, $selected, true)) {
                $extra .= ' checked="checked"';
            }

            // read-only?
            if ($this->mode == 'readonly' || $readonly)
            {
                $extra .= ' disabled';
            }

            $fieldDomId = $this->fieldId.'_'.rand();
            $this->html .= '<li><input type="radio" name="'.$this->fieldId.'" id="'.$fieldDomId.'" value="'.$code.'"'.$extra.'/> <label class="field_label" for="'.$fieldDomId.'">'.$desc.'</label></li>';
        }

        $this->html .=      '</ul>'.
                        '</div>';
    }
    
    /**
     * Set values for checkboxes
     * @param array ($key => label, $key => $label, ...)
     */
    public function setValues (array $values)
    {
        $this->values = $values;
    }    


    protected function build ()
    {
        if ($this->defaultHidden)
        {
            $this->createDefaultHiddenHTML();
        }
        else
        {
            $this->createHTML($readonly);
        }
    }
    
}
