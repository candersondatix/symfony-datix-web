<?php
/**
* Base class for all form fields.
*/
abstract class Forms_FormField
{
    /**
    * The field name.
    * 
    * @var string
    */
    protected $name;
    
    /**
    * Used for id/name attribute on input tag.
    * 
    * Can differ from the field name if we're using a suffix.
    * 
    * @var string
    */
    protected $fieldId;
    
    /**
    * The module this field belongs to.
    * 
    * @var string
    */
    protected $module;
    
    /**
    * The current value for this field.
    * 
    * @var string
    */
    protected $value;
    
    /**
    * The current value for this field's changed flag.
    * 
    * @var string
    */
    protected $changedValue;
    
    /**
    * The field label.
    * 
    * @var mixed
    */
    protected $label;
    
    /**
    * The field mode (search/readonly/edit).
    * 
    * @var string
    */
    protected $mode;
    
    /**
    * The section of the form this field is in.
    * 
    * @var string
    */
    protected $formSection;
    
    /**
    * Additional identifier when the field contains linked data
    * and can appear on a form multiple times.
    * 
    * @var string
    */
    protected $suffix;
    
    /**
    * Custom onchange event function(s).
    * 
    * @var string
    */
    protected $onChangeExtra;
    
    /**
    * The HTML markup for this field.
    * 
    * @var string
    */
    protected $html;

    /**
     * Flag to control if we should output the CHANGED- input hidden field
     *
     * @var bool
     */
    protected $addChangedFlag = true;

    protected $data;
    
    /**
    * Constructor: initialises properties common to all form fields.
    * 
    * @param mixed $name
    * @param mixed $module
    * @param mixed $value
    * @param mixed $mode
    * @param mixed $label
    * @param mixed $formSection
    * @param mixed $changedValue
    * @param mixed $suffix
    * 
    * @return Forms_FormField
    */
    public function __construct($name, $module, $value, $mode, $label = '', $formSection = '', $changedValue = '', $suffix = '')
    {
        $this->name         = $name;
        $this->fieldId      = $suffix != '' ? $name.'_'.$suffix : $name;    
        $this->module       = $module;
        $this->value        = $value;
        $this->label        = $label;
        $this->formSection  = $formSection;
        $this->changedValue = $changedValue;
        $this->suffix       = $suffix;

        $this->setMode($mode);
    }

    /**
     * Getter for name property
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Getter for module property
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
    * Sets the field mode (search/readonly/edit).
    * 
    * @param string $mode
    */
    protected function setMode($mode)
    {
        switch ($mode)
        {
            case 'Search':
                $this->mode = 'search';
                break;    
            case 'Print':
            case 'ReadOnly':
            case 'Locked':
                $this->mode = 'readonly';
                break;     
            default:
                $this->mode = 'edit';
                break;    
        }  
    }
    
    /**
    * Define function for creating the HTML markup for this field.
    */
    abstract protected function createHTML();
    
    /**
    * Define function for carrying out all tasks necessary to fully create this field.
    *
    * This may include creating the HTML, adding to $JSFunctions global, etc.
    */
    abstract protected function build();
    
    /**
    * Used to determine whether or not this is a User-Defined Field.
    * 
    * @return boolean
    */
    protected function isUDF()
    {
        return \UnicodeString::substr($this->name, 0, 3) == 'UDF';    
    }
    
    /**
    * Builds field and returns the HTML.  Also constructs and returns changed flag field.
    * 
    * @return string $this->html
    * 
    * @codeCoverageIgnore
    */
    public function getField($readonly = false)
    {
        $this->build($readonly);

        if ($this->addChangedFlag)
        {
            return $this->html . '<input type="hidden" name="CHANGED-'.$this->fieldId.'" id="CHANGED-'.$this->fieldId.'" value="'.$this->changedValue.'" />';
        }
        else
        {
            return $this->html;
        }
    }
    
    /**
    * Setter for custom onchange event functions.
    * 
    * @param string $onChangeExtra
    * 
    * @codeCoverageIgnore
    */
    public function setOnChangeExtra($onChangeExtra)
    {
        $this->onChangeExtra = $onChangeExtra;   
    }

    /**
     * Setter for add change flag that if we should output the CHANGED- input hidden field.
     *
     * @param bool $addChangedFlag
     */
    public function setAddChangedFlag($addChangedFlag)
    {
        $this->addChangedFlag = $addChangedFlag;
    }
    
    public function setData($data)
    {
        foreach($data as $key => $val)
        {
            if(($key != 'duplicate-check' && $key != 'can-add') ||
                ($key == 'duplicate-check' && isset($_SESSION["logged_in"]) && bYN(GetParm('CLA_SHOW_DUPLICATES', 'N'))) ||
                ($key == 'can-add' && bYN(GetParm('PAY_ADD_NEW_RECIPIENT', 'N')))
            )
            {
                $this->data .= ' data-' . $key . '="' . $val . '"';
            }
        }
    }
}