<?php

use src\framework\model\RecordEntity;

/**
* @desc Object to display a list of records. Uses the RecordList object as a data source.
*/
class Listings_ListingDisplay
{
    /**
    * @var obj
    * @desc Object representing the list of records to display in this listing.
    */
    public $RecordList;

    /**
    * @var obj
    * @desc Object representing the design of this listing.
    */
    public $ListingDesign;

    /**
    * @var bool
    * @desc If true, this listing will be readonly.
    */
    public $ReadOnly = false;

    /**
    * @var bool
    * @desc used for SABS (for example) where we need a checkbox next to every item.
    */
    public $Checkbox = false;

    /**
    * @var string
    * @desc the "action" that these listing items should link to. Only required if no module provided by RecordList.
    */
    public $Action;

    /**
    * @var string
    * @desc the "service" and "event" that these listing items should link to, if items on the list are interpreted by a service class.
    */
    public $Service;
    public $Event;

    /**
    * @var array
    * @desc array of keys that need to be included in the url that each item links to. By default, this will be "recordid"
    */
    public $URLParameterArray;

    /**
     * The direction in which this listing is sorted
     *
     * @var string
     */
    private $_direction = 'ASC';

    /**
     * The column name the listing is being sorted by
     *
     * @var string
     */
    private $_sortField;
    
    /**
     * The value of the action query parameter to be used when the listed is sorted
     *
     * @var string
     */
     private $_sortAction;

    /**
     * The "parent" module that the records being listed are linked to
     *
     * @var string
     */
    public $LinkModule;

    /**
     * Message to display if no rows are found.
     *
     * @var string
     */
    public $EmptyMessage = 'No records found.';

    /**
     * String to add to the end of the url for a listed record when constructing the link.
     *
     * @var string
     */
    public $RecordURLSuffix;

    public function __construct($RecordList, $ListingDesign, $LinkModule = '', $ReadOnly = false)
    {
        $this->RecordList = $RecordList;
        $this->ListingDesign = $ListingDesign;
        $this->LinkModule = $LinkModule;
        $this->ReadOnly = $ReadOnly;
        $this->EmptyMessage = _tk('no_records_found');

        //If the user doesn't have access to the listed records, the listing should be readonly.
        if($this->ListingDesign->Module && $this->ListingDesign->Module != 'ADM' && !GetAccessLevel($this->ListingDesign->Module))
        {
            $this->ReadOnly = true;
        }
        
        if (!is_bool($this->ReadOnly))
        {
            throw new \InvalidArgumentException('ReadOnly must be a Boolean value');
        }
    }

    public function getColumns()
    {
        if(!empty($this->ListingDesign->Columns))
        {
            return $this->ListingDesign->getColumnObjects();
        }

        if(!empty($this->RecordList->Columns))
        {
            return $this->RecordList->Columns;
        }

        return array();
    }

    /**
    * @desc Top level function to call - generates and returns the HTML for this listing.
    */
    public function GetListingHTML()
    {
        global $FieldDefs, $FieldDefsExtra, $scripturl, $ModuleDefs;

        /* TODO
        //Check if Read only override already set.
        if ($this->ReadOnly !== true)
        {
            $this->setFormAccess($this->FormType);
        }
        */

        //Check we are in a position to print a listing.

        if (count($this->getColumns()) == 0)
        {
            return '<div class="padded_div"><b>This listing has no columns. Please contact your Datix administrator</b></div>';
        }

        if (count($this->RecordList->Records) == 0)
        {
            return '<div class="padded_div windowbg2"><b>'.$this->EmptyMessage.'</b></div>';
        }

        $HTML = '<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';

        $HTML .= $this->getHeaderRowHTML();

        $HTML .= $this->getDataRowsHTML();

        $HTML .= '</table>';

        return $HTML;
    }


    /**
    * @desc Gets the HTML for the main data area of the listing.
    */
    private function getDataRowsHTML()
    {
        foreach ($this->RecordList->Records as $Record)
        {
            $HTML .= $this->GetDataRowHTML($Record);
        }

        return $HTML;
    }

    /**
    * @desc Gets the HTML for a single record row in the listing.
    */
    protected function GetDataRowHTML($Record)
    {
        global $ModuleDefs;
        
        $HTML .= '
            <tr class="listing-row">';

        if ($this->Checkbox)
        {
            $HTML .= '<td class="windowbg2" width="1%">
                    <input type="checkbox" id="checkbox_id_' . $this->CheckboxType . '" name="checkbox_include_' . $Record->Data[$this->CheckboxIDField] . '" onclick="" />
                    </td>';
        }

        foreach ($this->getColumns() as $Column)
        {
            $HTML .= $this->GetDataCellHTML($Record, $Column);
        }

        // options
        
        $recordid = $Record->Data instanceof RecordEntity ? $Record->Data->recordid : $Record->Data['recordid'];
        $options = $this->ListingDesign->getOptions($recordid);
        $maxNumOptions = $this->ListingDesign->getMaxNumOfOptions();

        if ( ! empty($options) || $maxNumOptions > 0)
        {
            // add extra fields depending on what options exist for other reports
            if ($maxNumOptions > count ($options))
            {
                // go through the options array and add a blank option for any options not present that are for other reports
                for ($i = 1; $i <= $maxNumOptions; $i++) {

                    if($options[$i] == null)
                    {
                        $options[$i] = '&nbsp;';
                    }
                }
            }

            // sort the array by key high to low so that the first item generated will be the one used to set the max number of options needed
            krsort($options);

            foreach ($options as $option) {
                $HTML .= '<td class="windowbg2" width="5%" style="text-align:center; white-space:nowrap;">'.$option.'</td>';
            }
        }

        $HTML .= '</tr>';

        return $HTML;
    }

    /**
    * @desc Gets the HTML for a single cell in the listing.
    */
    protected function getDataCellHTML($Record, $PassedColumn)
    {
        $Column = clone($PassedColumn);

        $col_field = $Column->getName();
        $FieldType = $Column->getFieldType();
        $row = $Record->Data;
        
        if ($Column->usesControllerAction())
        {
            $value = $Column->getContents($row);
        }
        else if ($Column instanceof Listings_UDFListingColumn && $row instanceof src\framework\model\Entity) //workaround while udf data is not accessible though entities.
        {
            $value = $this->RecordList->udfData[$row->recordid][$Column->GetID()];
        }
        else
        {
            $value = $row instanceof src\framework\model\Entity ? $row->$col_field : $row[$col_field];
        }
        
        $Column->setValue($value);
        $Column->getDisplayInfo();

		$HtmlClass = ($this->_sortField && $this->_sortField == $col_field) ? 'sort' : '';
        $HTML .= '<td class="windowbg2 ' . $HtmlClass . '" valign="top"';

        if($Record->RowNumber > 1)
        {
             $HTML .= ' rowspan="' . $Record->RowNumber . '"';
        }

        if($Column->getColour())
        {
             $HTML .= ' style="background-color:#' . $Column->getColour(). '"';
        }

        $HTML .= '>';

        if (isset($value) && !$this->ReadOnly && $FieldType != 'X')
        {
            // Encode single and double quotes
            $url = str_replace("'", '\\&#39;', $this->getRecordUrl($Record->Data));
            $url = str_replace('"', '\\&#34;', $url);

            $HTML .= '<a href="javascript:if(CheckChange()){SendTo(\''. $url .'\');}" >';
        }

        if($FieldType != 'X')
        {
            $HTML .= htmlspecialchars($this->getFormattedData($FieldType, $Column->getDescription()));
        }
        else
        {
            $HTML .= $this->getFormattedData($FieldType, $Column->getDescription());
        }

        if (isset($value) && !$this->ReadOnly && $FieldType != 'X')
        {
            $HTML .= '</a>';
        }

        $HTML .= '</td>';

        return $HTML;
    }


    private function getFormattedData ($FieldType, $value)
    {
        if (!is_numeric($value) && empty ($value))
        {
            return '';
        }
        
        switch ($FieldType)
        {
            case 'M': // money
                return FormatMoneyVal($value, false);
                break;
            case 'DT':
                return FormatDateVal($value, true);
                break;
            case 'PCT':
                return is_numeric($value) ? $value.'%' : '';
                break;

            default:
                return $value;
                break;
        }
    }

    
    /**
    * @desc Gets the HTML for the header row of the listing, containing the field labels.
    */
    private function getHeaderRowHTML()
    {
        global $FieldDefsExtra, $scripturl, $ModuleDefs;

        $HTML .= '<tr name="element_section_row" id="element_section_row" class="tableHeader head2">'; //title row

        if ($this->Checkbox)
        {
             $HTML .= '<th class="windowbg" width="1%">
                <input type="checkbox" id = "check_all_checkbox" name="check_all_checkbox" onclick="ToggleCheckAll(\'checkbox_id_' . $this->CheckboxType . '\', this.checked)"/>
            </th>';
        }

        foreach ($this->getColumns() as $Column)
        {
            $HTML .= '<th class="windowbg"' . ($Column->getWidth() ? 'width="' . $Column->getWidth() . '%"' : '') . '>';

            $col_field = $Column->getName();

            if ($Column->GetFieldType != 'T' && $this->_sortField && !$col_info['custom'])
            {
				$HtmlClass  = ($this->_sortField && $this->_sortField == $Column->getName()) ? 'sort ' . \UnicodeString::strtolower($this->_direction) : '';
				$sortAction = (!is_null($this->_sortAction)) ? $this->_sortAction : Sanitize::SanitizeURL($_GET['action']);
				$sortUrl    = $scripturl . '?action=' . $sortAction . '&amp;orderby=' . $Column->getName() . '&amp;order=' . (($this->_direction == 'ASC') ? 'DESC' : 'ASC');
				
                $HTML .= '<a class="' . $HtmlClass . '" href="' . $sortUrl . '">';
            }

            $columnLabel = $Column->getLabel();

            if (preg_match('/^.*\((\D*)\)/iu', $columnLabel, $result))
            {
                if (isset($FieldDefsExtra[$this->ListingDesign->Module][$col_field]['SubmoduleSuffix']))
                {
                    $submoduleSuffix = \UnicodeString::substr($FieldDefsExtra[$this->ListingDesign->Module][$col_field]['SubmoduleSuffix'], 0, -1);
                    $submoduleSuffix = \UnicodeString::substr($submoduleSuffix, 1);

                    if (strcmp($submoduleSuffix, $this->ListingDesign->Module) != 0)
                    {
                        $columnLabel = str_replace($result[1], $submoduleSuffix, $columnLabel);
                    }
                }
                else
                {
                    if (strcmp($result[1], $ModuleDefs[$this->ListingDesign->Module]['NAME']) == 0)
                    {
                        $columnLabel = str_replace($result[1], '', $columnLabel);
                        $columnLabel = \UnicodeString::substr($columnLabel, 0, -2);
                    }
                }
            }

            $HTML.= $columnLabel;

            if ($FieldDefs[$Column->getModule()][$col_field]['Type'] != 'textarea' && $this->_sortField && !$col_info['custom'])
            {
                $HTML .= '</a>';
            }

            $HTML .= '</th>';
        }

        // add extra columns for options
        $numOptions = $this->ListingDesign->getMaxNumOfOptions();
        if ($numOptions)
        {
            $HTML .= '<td colspan="'.$numOptions.'">&nbsp;</td>';
        }

        $HTML .= '</tr>';

        return $HTML;
    }

    /**
    * @desc Constructs the url to link the user to a particular record referenced in this listing.
    *
    * @param array $datarow Array of data from this record
    *
    * @return string The url of the record.
    */
    function getRecordUrl($datarow)
    {
        global $ModuleDefs, $scripturl;

        if(!empty($this->URLParameterArray))
        {
            foreach($this->URLParameterArray as $key => $Param)
            {
                if (!is_int($key))
                {
                    $ParamString .= '&'.$key.'='.$Param;
                }
                else
                {
                    $ParamString .= '&'.$Param.'='.$this->getField($datarow, $Param);
            	    if ($Param == 'frommainrecord') $ParamString .= '1';
                }
            }
        }
        else
        {
            $ParamString = '&recordid='.$this->getField($datarow, 'recordid');
        }

        if ($this->LinkModule) //linking to a linked record.
        {
            if($this->RecordList->Module == 'CON' && $this->LinkModule == 'SAB')
            {
                //exception because SABS contacts are non-standard.
                $URL = $scripturl.'?action=sabslinkcontact&con_id='.$this->getField($datarow, 'con_id').'&sab_id='.$this->getField($datarow, 'sab_id').'&formtype=Edit';
            }
            else
            {
                // return $scripturl.'?action='.$ModuleDefs[$this->Module]['ACTION'].'&module='.$this->Module.'&link_recordid='.$datarow['link_recordid'];
                $URL = $scripturl.'?action='.$ModuleDefs[$this->RecordList->Module]['ACTION'].'&link_module='.$this->LinkModule.'&recordid='.$this->getField($datarow, $ModuleDefs[$this->RecordList->Module]['LINKED_MODULE']['link_recordid']).'&main_recordid=' . $this->getField($datarow, $ModuleDefs[$this->RecordList->Module]['LINKED_MODULE']['parent_ids'][$this->LinkModule]) . '&from_parent_record=1';
            }
        }
        else if ($this->RecordList->Module)
        {
            $URL = $scripturl.'?action='.$ModuleDefs[$this->RecordList->Module]['ACTION'].'&module='.$this->RecordList->Module.'&'.FirstNonNull(array($ModuleDefs[$this->RecordList->Module]['URL_RECORDID'], 'recordid')).'='.$this->getField($datarow, 'recordid').(!empty($this->URLParameterArray) ? $ParamString : '');
        }
        else if ($this->Service && $this->Event)
        {
            $URL = $scripturl.'?service='.$this->Service.'&event='.$this->Event.$ParamString;
        }
        else
        {
            $URL = $scripturl.'?action='.$this->Action.$ParamString;
        }

        if($this->RecordURLSuffix)
        {
            $URL .= $this->RecordURLSuffix;
        }

        return $URL;
    }
    
    /**
     * Bit of a hack this - because I want RecordLists to consume EntityCollections as well as raw data arrays, the argument
     * passed to Listings_ListingDisplay::getRecordUrl() will be either an array or an instance of Entity.  So this method 
     * accesses the desired field from this argument be it of either type.
     * 
     * @param array|Entity $data The variable we're getting the field from.
     * @param string       $name The name of the field we want.
     * 
     * @throws InvalidArgumentException If data is not an array or an instance of Entity.
     * 
     * @return mixed
     */
    protected function getField($data, $name)
    {
        if (!is_array($data) && !($data instanceof src\framework\model\Entity))
        {
            throw new \InvalidArgumentException('data must be an array or an instance of Entity');
        }
        
        return is_array($data) ? $data[$name] : $data->$name;
    }

    static function getRecordsPerPage()
    {
        return GetParm("LISTING_DISPLAY_NUM", 20);
    }


    function getPageNumbersHTML()
    {
        $perPage = Listings_ListingDisplay::getRecordsPerPage();

        if($perPage < $this->RecordList->NumRecords)
        {
            $pageStart = 0;

            while($pageStart < $this->RecordList->NumRecords)
            {
                $pages[] = $pageStart;
                $pageStart += $perPage;

                if(!isset($startPage) && $this->RecordList->StartAt < $pageStart)
                {
                    $startPage = count($pages)-1;
                }
            }

            $URL = new URL();


            $HTML .= '<table width="100%" class="page-numbers">
                <tr>
                    <td class="windowbg" width="5%" nowrap="nowrap">';
                    if($startPage != 0)
                    {
                        $newURL = clone($URL);
                        $newURL->AddToQueryString('start', $this->RecordList->StartAt-$perPage);

                        $HTML .= '<a href="'.$newURL->GetURL().'"><b>'._tk('previous_page').'</b></a>';
                    }
                    $HTML .= '
                    </td>
                    <td class="windowbg" width="90%">';

                    if ($startPage > 10)
                    {
                        $newURL = clone($URL);
                        $newURL->AddToQueryString('start', $pages[$startPage-11]);

                        $HTML .= '<a href="'.$newURL->GetURL().'"><b><< </b></a>';
                    }

                    foreach($pages as $pagenumber => $startAt)
                    {
                        if(abs($startPage - $pagenumber) <= 10)
                        {
                            if($pagenumber == $startPage)
                            {
                                $HTML .= '<font size="3"><b>' . ($pagenumber + 1) . ' </b></font>';
                            }
                            else
                            {
                                $newURL = clone($URL);
                                $newURL->AddToQueryString('start', $startAt);

                                $HTML .= '<a href="'.$newURL->GetURL().'">'. ($pagenumber + 1) . '</a> ';
                            }
                        }
                    }
                    if ($startPage < count($pages)-11)
                    {
                        $newURL = clone($URL);
                        $newURL->AddToQueryString('start', $pages[$startPage+11]);

                        $HTML .= '<a href="'.$newURL->GetURL().'"><b>>> </b></a>';
                    }
                    $HTML .= '</td>';
                    $HTML .= '<td class="windowbg" width="5%" nowrap="nowrap">';
                    if($startPage < count($pages)-1)
                    {
                        $newURL = clone($URL);
                        $newURL->AddToQueryString('start', $this->RecordList->StartAt + $perPage);

                        $HTML .= '<a href="'.$newURL->GetURL().'"><b>'._tk('next_page').'</b></a>';
                    }
                    $HTML .= '
                    </td>
                </tr>';
            $HTML .= '</table>';
        }

        return $HTML;
    }

    /**
     * Sets the column name by which this listing is sorted
     *
     * @param string $columnName The name of the field the display is being sorted by
     * @return void
     */
    public function setOrder($columnName)
    {
        $this->_sortField = $columnName;
    }

    /**
     * Sets the direction in which the listing has been sorted (either ASC or DESC)
     *
     * @param string $dir The direction. Either ASC or DESC
     * @return void
     * @throws Exception if the direction is anything but ASC or DESC
     */
    public function setDirection($dir)
    {
        if (!in_array($dir, array('ASC', 'DESC'))) {
            throw new Exception('Direction needs to be either ASC or DESC');
        }

        $this->_direction = $dir;
    }
    
    /**
     * Sets the value of the action query parameter when the user sorts the listing
     *
     * @param string $action The value to use for the action query parameter
     * @return void
     */
    public function setSortAction($action)
    {
        $this->_sortAction = $action;
    }

    /**
    * Adds the listing to an excel object, starting at the row and column provided.
    *
    * @param obj $objPHPExcel PHPExcel object to be modified
    * @param mixed $excelCol Excel column to begin from
    * @param mixed $excelRow Excel row to begin from
    */
    public function addToExcel(&$objPHPExcel, $excelCol, &$excelRow)
    {
        $startCol = $excelCol;

        $BorderStart = $excelCol.$excelRow;

        foreach ($this->getColumns() as $Column)
        {
            $objPHPExcel->getActiveSheet()->SetCellValue($excelCol.$excelRow, $Column->getLabel());
            $objPHPExcel->getActiveSheet()->getStyle($excelCol.$excelRow)->getFont()->setBold(true);

            $excelCol++;
        }

        $excelRow++;

        foreach ($this->RecordList->Records as $Record)
        {
            $excelCol = $startCol;

            $Record->CalculateRowNumber();

            for($i=0; $i<$Record->RowNumber; $i++)
            {
                foreach ($this->getColumns() as $col_id => $Column)
                {
                    $col_field = $Column->getName();
                    $FieldType = $Column->getFieldType();
                    $row = $Record->Data;

                    $Column->setValue($Record->Data[$Column->getName()]);
                    $Column->getDisplayInfo();

                    $objPHPExcel->getActiveSheet()->SetCellValue($excelCol.$excelRow, $Column->getDescription());

                    if($col_id != count($this->getColumns())-1)
                    {
                        $excelCol++;
                    }
                }
            }
        }

        $BorderEnd = $excelCol.$excelRow;

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    ),
                ),
            );

        $objPHPExcel->getActiveSheet()->getStyle($BorderStart.':'.$BorderEnd)->applyFromArray($styleArray);

    }

}