<?php
/**
* Contact listings when searching for matching contacts on level one forms. 
*/
class Listings_LevelOneConMatchListing extends Listings_ModuleListingDesign
{
    /**
    * Returns either user defined (if set) or default columns.
    *
    * @param string $module The module listing to return
    *
    * @return array An array of columns
    */
    static function GetListingColumns($module)
    {
        $Listing = new Listings_LevelOneConMatchListing(array('module' => 'CON', 'listing_id' => self::GetDefaultListing($module)));
        
        if ($Listing->ListingId == '')
        {
            $Listing->Columns = array(
                new Listings_ListingColumn('con_surname'),
                new Listings_ListingColumn('con_forenames'),
                new Listings_ListingColumn('link_age'),
            );                
        }
        else
        {
            $Listing->LoadColumnsFromDB();   
        }

        return $Listing->Columns;
    }

    /**
    * @desc Returns the default listing for a given module.
    *
    * @return int The id of the default listing for this module.
    */
    static function GetDefaultListing($module)
    {
        return GetParm($module.'_L1_CON_MATCH_LISTING_ID', '');
    }
}