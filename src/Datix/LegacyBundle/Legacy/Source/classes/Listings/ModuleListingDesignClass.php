<?php

/**
* @desc Class representing the design of a record listing.
* This class is module-specific, and sits on top of a module independent class.
*
* Used when designing listings and when using those designs to display listings.
* This class should not contain any methods to display listings, or to collect data.
*/
class Listings_ModuleListingDesign extends Listings_ListingDesign
{
    var $Module; //module the listing belongs to

    var $ListingId; //id of the listing

    var $OverrideLinkType; // Link type to use instead of the link type provided in the data. Needed because of the weird data structure in complaints.

    // parameters used for linked form designs.
    protected $ParentModule; //module the records in the listing are linked to (usually the module the listing appears in).
    protected $LinkType; // Identifier in case there are multiple forms linked to the parent form.

    /**
    * @desc Constructor - sets a couple of basic variables.
    *
    * @param string $Module The module this listing represents
    * @param int $ListingId The recordid of the listing in the database
    */
    public function __construct($Parameters)
    {
    	// Call getModule from the Subs.php file for sanitizing
        $this->Module = Sanitize::getModule($Parameters['module']);
        $this->ParentModule = Sanitize::getModule($Parameters['parent_module']);
        $this->LinkType = $Parameters['link_type'];
        $this->ListingId = $Parameters['listing_id'];
    }

    /**
    * @desc Returns the contents of the module parameter.
    */
    protected function getModule()
    {
        return $this->Module;
    }

    /**
    * @desc Returns the table that the records this design will list are from.
    */
    protected function getTable()
    {
        global $ModuleDefs;

        if($this->Table)
        {
            return $this->Table;
        }
        else
        {
            return ($ModuleDefs[$this->getModule()]['VIEW'] ? $ModuleDefs[$this->getModule()]['VIEW'] : $ModuleDefs[$this->getModule()]['TABLE']);
        }
    }

    /**
    * @desc Gets the id of this form, based on the module and level. Takes into account linked forms.
    * Doesn't return anything, but sets the ListingId property.
    */
    protected function CalculateListingID()
    {
        global $ModuleDefs;

        //if this is a linked listing, we need to check the parent form design. Actions doesn't always have a parent form design (since it may be a list of actions from multiple modules)
        if (in_array($this->Module, array('PRO', 'ELE', 'AQU', 'ATQ', 'AMO')) || (in_array($this->Module, ['ACT','PAY','POL','CLA','INC']) && !empty($this->ParentModule)))
        {
            switch($this->Module)
            {
                case 'PRO':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => 'ELE', 'level' => 2, 'link_type' => 'elements'));
                    break;
                case 'ELE':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => 'STN', 'level' => 2));
                    break;
                case 'AQU':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => (!empty($this->ParentModule) ? $this->ParentModule : 'AMO'), 'level' => 2));
                    break;
                case 'ATQ':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => (!empty($this->ParentModule) ? $this->ParentModule : 'ATM'), 'level' => 2));
                    break;
                case 'AMO':
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => 'LOC', 'level' => 2));
                    break;
                default:
                    $ParentDesign = Forms_FormDesign::GetFormDesign(array('module' => $this->ParentModule, 'level' => 2));
            }

            $ListingDesigns = $ParentDesign->ListingDesigns;
            switch($this->Module)
            {
                case 'CQP':
                case 'CQS':
                case 'PAY':
                case 'ELE':
                case 'POL':
                case 'CLA':
                case 'INC':
                    if (isset($ListingDesigns[$this->LinkType][$this->LinkType]) && $ListingDesigns[$this->LinkType][$this->LinkType] != '')
                    {
                        $FormID = intval($ListingDesigns[$this->LinkType][$this->LinkType]);
                    }
                    break;
                default:
                    if (isset($ListingDesigns[$this->LinkType][$this->Module]) && $ListingDesigns[$this->LinkType][$this->Module] != '')
                    {
                        $FormID = intval($ListingDesigns[$this->LinkType][$this->Module]);
                    }
            }
        }
        //Now we just get the form from the appropriate global. This takes into account user, profile and global config options.
        if (!isset($FormID))
        {
            $FormID = Listings_ModuleListingDesign::GetDefaultListing($this->Module);
        }

        $this->ListingId =  Sanitize::SanitizeInt($FormID);
    }

    /**
    * @desc Loads column definitions from the database into this object.
    *
    * @param int $ListingId the recordid of the listing in the database, in case it has not already been provided.
    */
    public function LoadColumnsFromDB($ListingId = null, $long = false)
    {
        global $ModuleDefs;

        if (isset($ListingId))
        {
            $this->ListingId = $ListingId;
        }
        if (!isset($this->ListingId))
        {
            $this->CalculateListingID();
        }

        if ($this->ListingId != 0)
        {
            //this could almost certainly sit in memory, since it is going to change very infrequently
            if (!\src\framework\registry\Registry::getInstance()->getSessionCache()->exists('listing-design.modules-for-listings'))
            {
                //need to check that the listing given is for the appropriate module
                $ListingModules = DatixDBQuery::PDO_fetch_all('SELECT recordid, lst_module FROM WEB_LISTING_DESIGNS', array(), PDO::FETCH_KEY_PAIR);
                \src\framework\registry\Registry::getInstance()->getSessionCache()->set('listing-design.modules-for-listings', $ListingModules);
            }

            $ListingModule = \src\framework\registry\Registry::getInstance()->getSessionCache()->get('listing-design.modules-for-listings')[$this->ListingId];
        }

        if ($this->ListingId == 0 || $ListingModule != $this->getModule()) //use the default design - not stored in the database.
        {
            $this->LoadFromDefaultFile();
        }
        else
        {
            $sql = 'SELECT lcl_table, lcl_field, lcl_width FROM WEB_LISTING_COLUMNS WHERE listing_id = :listing_id ORDER BY LCL_order';

            $Columns = DatixDBQuery::PDO_fetch_all($sql, array('listing_id' => $this->ListingId));

            foreach ($Columns as $ColumnDetails)
            {
                if(!$ColumnDetails['lcl_table'])
                {
                    $ColumnDetails['lcl_table'] = $ModuleDefs[$this->getModule()]['VIEW'] ?: $ModuleDefs[$this->getModule()]['TABLE'];
                }

                try
                {
                    $NewColumn = Listings_ListingColumnFactory::getColumn($this->getModule(), $ColumnDetails['lcl_field'], $ColumnDetails['lcl_table'], $long);
                }
                catch(Exception $e)
                {
                    $NewColumn = Listings_ListingColumnFactory::getColumn($this->getModule(), $ColumnDetails['lcl_field'], '', $long);
                }

                $NewColumn->setWidth($ColumnDetails['lcl_width']);
                $this->Columns[] = $NewColumn;
            }
        }
    }

    /**
    * @desc Determines whether the listing represented by this object is the listing to be used by default in the system.
    *
    * @return bool true if this listing is the default, false otherwise.
    */
    protected function isDefault()
    {
        return (System_Globals::GetCurrentValue_GlobalOnly($this->getModule().'_LISTING_ID', 0) == $this->ListingId);
    }

    /**
    * @desc Loads columns from the default listing file - if this listing is then displayed in the listing designer,
    * it will be read-only, since you can't change the datix default listing.
    */
    protected function LoadFromDefaultFile()
    {
        global $ModuleDefs;

        $ColSettingsFile = $this->GetDefaultFile();

        include($ColSettingsFile);

        foreach ($list_columns_standard as $field => $ColumnDetails)
        {
            $NewColumn = Listings_ListingColumnFactory::getColumn($this->getModule(), $field, $this->getTable());
            $NewColumn->setWidth($ColumnDetails['width']);
            $ColumnsToAdd[] = $NewColumn;
        }

        $this->LoadColumnsFromArray($ColumnsToAdd);
    }

    /**
     * Override used when getting columns for staff module sql.
     * @param bool $includeTablePrefix
     * @return array
     */
    public function getColumns($includeTablePrefix = false)
    {
        $columns = parent::getColumns($includeTablePrefix);

        if($this->Module == 'ADM')
        {
            foreach($columns as $index => $column)
            {
                //We want to move away from using the sta_... properties that simply duplicate con_... properties, so we substitute them here:
                $PropertyMapping = array(
                    'sta_title' => 'con_title',
                    'sta_forenames' => 'con_forenames',
                    'sta_surname' => 'con_surname',
                    'jobtitle' => 'con_jobtitle',
                    'email' => 'con_email',
                    'sta_sid' => 'con_sid',
                );

                if($includeTablePrefix)
                {
                    $SplitCol = explode('.', $column);
                    $ColWithoutPrefix = $SplitCol[1];
                }
                else
                {
                    $ColWithoutPrefix = $column;
                }

                if (isset($PropertyMapping[$ColWithoutPrefix]))
                {
                    unset($columns[$index]);

                    if($includeTablePrefix)
                    {
                        $columns[$index] = $SplitCol[0].'.'.$PropertyMapping[$ColWithoutPrefix];
                    }
                    else
                    {
                        $columns[$index] = $PropertyMapping[$ColWithoutPrefix];
                    }
                }
            }
        }
        ksort($columns);

        return $columns;
    }

    /**
     * Override used when getting columns for staff module sql.
     * @param bool $includeTablePrefix
     * @return array
     */
    public function getColumnObjects()
    {
        $columns = parent::getColumnObjects();

        if($this->Module == 'ADM')
        {
            foreach($columns as $index => $column)
            {
                //We want to move away from using the sta_... properties that simply duplicate con_... properties, so we substitute them here:
                $PropertyMapping = array(
                    'sta_title' => 'con_title',
                    'sta_forenames' => 'con_forenames',
                    'sta_surname' => 'con_surname',
                    'jobtitle' => 'con_jobtitle',
                    'email' => 'con_email',
                    'sta_sid' => 'con_sid',
                );

                if (isset($PropertyMapping[$column->getName()]))
                {
                    unset($columns[$index]);

                    try
                    {
                        $NewColumn = Listings_ListingColumnFactory::getColumn($this->getModule(), $PropertyMapping[$column->getName()], 'staff', false);
                    }
                    catch(Exception $e)
                    {
                        $NewColumn = Listings_ListingColumnFactory::getColumn($this->getModule(), $PropertyMapping[$column->getName()], '', false);
                    }

                    $columns[$index] = $NewColumn;
                }
            }
        }

        ksort($columns);

        return $columns;
    }

    /**
    * @desc Gets the filename of the listing template file for this module.
    */
    protected function GetDefaultFile()
    {
        global $ModuleDefs;

        if ($ModuleDefs[$this->getModule()]['GENERIC'])
        {
            if (file_exists('Source/generic_modules/'.$ModuleDefs[$this->getModule()]['GENERIC_FOLDER'].'/StandardListSettings.php'))
            {
                $SettingsFilename = 'Source/generic_modules/'.$ModuleDefs[$this->getModule()]['GENERIC_FOLDER'].'/StandardListSettings.php';
            }
            else
            {
                $SettingsFilename = 'Source/generic/StandardListSettings.php';
            }
        }
        else
        {
            $SettingsFilename = "{$ModuleDefs[$this->getModule()][LIBPATH]}/Standard{$this->getModule()}ListSettings.php";
        }

        return $SettingsFilename;
    }

    /**
    * @desc Deletes the current listing design from the database.
    */
    public function DeleteFromDB()
    {
        DatixDBQuery::PDO_query('DELETE FROM WEB_LISTING_COLUMNS WHERE listing_id = :listing_id', array('listing_id' => $this->ListingId));
        DatixDBQuery::PDO_query('DELETE FROM WEB_LISTING_DESIGNS WHERE recordid = :listing_id', array('listing_id' => $this->ListingId));

        if ($this->isDefault()) //we need to reassign the default design to one that will still exist.
        {
            SetGlobal($this->getModule().'_LISTING_ID', 0);
            $_SESSION["Globals"][$_GET['global']] = $_GET['value']; //make sure the change is picked up straight away.
        }
    }

    /**
    * @desc Saves the current listing design to the database.
    */
    public function SaveToDB()
    {
        if (!$this->ListingId)
        {
            $this->ListingId = DatixDBQuery::PDO_build_and_insert('WEB_LISTING_DESIGNS',
                array('lst_title' => $this->Title, 'lst_module' => $this->getModule(), 'createddate' => GetTodaysDate(), 'createdby' => $_SESSION['initials']));
        }
        else
        {
            DatixDBQuery::PDO_query('UPDATE WEB_LISTING_DESIGNS SET updateddate = :updateddate, updatedby = :updatedby WHERE recordid = :recordid',
                array('recordid' => $this->ListingId, 'updateddate' => GetTodaysDate(), 'updatedby' => $_SESSION['initials']));
        }

        DatixDBQuery::PDO_query('DELETE FROM WEB_LISTING_COLUMNS WHERE listing_id = :listing_id', array('listing_id' => $this->ListingId));

        $InsertColumnQuery = new DatixDBQuery('INSERT INTO WEB_LISTING_COLUMNS (listing_id, LCL_table, LCL_field, LCL_width, LCL_order, updatedby, updateddate) VALUES (:listing_id, :table, :field, :width, :order, :updatedby, :updateddate)');
        $InsertColumnQuery->prepare();

        $Order = 0;

        if (is_array($this->Columns))
        {
            foreach ($this->Columns as $Column)
            {
                $InsertColumnQuery->execute(array('listing_id' => $this->ListingId, 'table' => $Column->getTable(), 'field' => $Column->getName(), 'width' => $Column->getWidth(), 'order' => $Order++, 'updatedby' => $_SESSION['initials'], 'updateddate' => date('Y-m-d H:i:s')));
            }
        }
    }

    /**
    * Set the listing's ReadOnly property based on the current permissions/form type.
    *
    * @global array $ModuleDefs
    *
    * @param string $formType The current form type.
    */
    protected function setFormAccess($formType)
    {
        global $ModuleDefs;

        $this->ReadOnly = (GetParm($ModuleDefs[$this->getModule()]['PERM_GLOBAL']) == '' || $formType == 'Print');
    }

    /**
    * @desc Loads the default listing from the database and returns its columns as an array.
    *
    * @param string $module The module listing to return
    *
    * @return array An array of columns
    */
    public static function GetListingColumns($module)
    {
        $Listing = Listings_ModuleListingDesignFactory::getListingDesign(array('module' => $module));
        $Listing->LoadColumnsFromDB();

        return $Listing->GetColumnArray();
    }

    /**
    * @desc Returns the default listing for a given module.
    *
    * @return int The id of the default listing for this module.
    */
    public static function GetDefaultListing($module)
    {
        return GetUserParm($_SESSION['login'], $module.'_LISTING_ID', 0);
    }

    public static function EditColumns($module = '', $listing_id = null)
    {
        global $scripturl, $dtxtitle, $dtxdebug, $dbtype, $FieldDefs, $FieldDefsExtra, $ModuleDefs;

        LoggedIn();
        if (!$_SESSION["AdminUser"])
        {
            $yySetLocation = $scripturl;
            redirectexit();
        }

        $dtxtitle = "Edit columns for listing page";

        if (!$module)
        {
            $module = Sanitize::getModule($_REQUEST["module"]);
        }
        if (!$module)
        {
            $module = "INC";
        }

        if(!$listing_id)
        {
            $listing_id = Sanitize::SanitizeInt($_GET['form_id']);
        }

        getPageTitleHTML(array(
             'title' => 'Listings design',
             'module' => 'ADM',
             ));

        GetSideMenuHTML(array('module' => 'ADM'));

        template_header_nopadding();

                echo '
        <script language="javascript" type="text/javascript">
            var NS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) < 5);


            function addToListing()
            {
                jQuery(\'#\'+jQuery(\'#table_list\').val()+\'_field_list option:selected\').remove().appendTo(\'#columns_list2\').attr(\'selected\', false);
                reDrawPreview();
            }

            function removeFromListing()
            {
                optionsToSplit = jQuery(\'#columns_list2 option:selected\').remove().each(function(i, option) {

                    //OldAlert(option);
                    var table_field = option.value.split(\'-\');
                    option.selected = false;
                    jQuery(\'#\'+table_field[0]+\'_field_list\').append(option);
                    sortListBox(document.getElementById(table_field[0]+\'_field_list\'));
                });

                reDrawPreview();
            }


        </script>';

        require_once 'Source/libs/ListingClass.php';
        $Listing = new Listings_ModuleListingDesign(array('module' => $module, 'listing_id' => $listing_id));
        $Listing->LoadColumnsFromDB();
        if ($listing_id == 0)
        {
            $Listing->ReadOnly = true;
        }

        $list_columns = $Listing->Columns;

        echo '
            <form id="editcol" name="editcol" method="post" action="' . $scripturl . '?action=editcoloptions">
        <input type="hidden" id="module" name="module" value="'.$module.'" />
        <input type="hidden" id="form_id" name="form_id" value="'.$listing_id.'" />
        <input type="hidden" id="rbWhat" name="rbWhat" value="Save" />


        <ol>';
        echo '<li class="section_title_row"><b>Preview:</b></li>';

        echo '<li id="listing_preview_table_wrapper">'.$Listing->GetListingPreviewHTML().'</li>';

        echo '<li class="section_title_row"><b>Edit Listing:</b></li>';

        echo '<li>';

        echo '
        <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">

        ';

        echo '
        <tr>
        <td class="windowbg2" align="left">
        Available columns:
        </td>
        <td class="windowbg2" align="left">
        </td>
        <td class="windowbg2" align="left">
        Current columns:
        </td>
        </tr>
        <tr>
            <td class="windowbg2" align="center">';


        $Tables = DatixDBQuery::PDO_fetch_all(
            'SELECT TLK_TABLE1 as table_name FROM TABLE_LINKS WHERE TLK_TABLE2 = :table
            UNION SELECT TLK_TABLE2 as table_name FROM TABLE_LINKS WHERE TLK_TABLE1 = :table
            UNION SELECT TRT_TABLE1 as table_name FROM TABLE_ROUTES WHERE TRT_TABLE2 = :table
            UNION SELECT TRT_TABLE2 as table_name FROM TABLE_ROUTES WHERE TRT_TABLE1 = :table', array('table' => $ModuleDefs[$module]['TABLE']), PDO::FETCH_COLUMN);

        array_unique($Tables);

        array_unshift($Tables, $ModuleDefs[$module]['TABLE']);

        foreach($Tables as $Table)
        {
            $TableObjs[] = new Table($Table);
        }

        $TableSelectHTML = '<select id="table_list" style="width: 300px" onchange="jQuery(\'.field_list_select\').hide();jQuery(\'#\'+jQuery(\'#table_list\').val()+\'_field_list\').show();">';

        foreach($Listing->Columns as $Column)
        {
            $ListingCols[$Column->getTable()][] = $Column->getName();
        }

        foreach($TableObjs as $TableObj)
        {
            $TableSelectHTML .= '<option value="'.$TableObj->getName().'">'.$TableObj->getDescription().'</option>';

            $sql = 'SELECT FDR_NAME, FDR_LABEL FROM field_directory WHERE FDR_TABLE = :table';

            if($ListingCols[$TableObj->getName()])
            {
                $sql .= ' AND FDR_NAME NOT IN (\''.implode('\', \'', $ListingCols[$TableObj->getName()]).'\')';
            }

            $sql .= ' ORDER BY FDR_LABEL';

            $Fields = DatixDBQuery::PDO_fetch_all($sql, array('table' => $TableObj->getName()));

            $FieldSelectHTML[$TableObj->getName()] = '<select id="'.$TableObj->getName().'_field_list" multiple="multiple" size="12" class="field_list_select" id="columns_list" '.($Listing->ReadOnly ? 'disabled ' : '').'
            style="width: 300px;'.($ModuleDefs[$module]['TABLE'] == $TableObj->getName() ? '' : 'display:none').'">';

            foreach($Fields as $Field)
            {
                $FieldSelectHTML[$TableObj->getName()] .= '<option value="'.$TableObj->getName().'-'.$Field['FDR_NAME'].'">'.$Field['FDR_LABEL'].'</option>';
            }

           $FieldSelectHTML[$TableObj->getName()] .= '</select>';
        }

       $TableSelectHTML .= '</select><br>';

       echo $TableSelectHTML;

       foreach($FieldSelectHTML as $Table => $HTML)
       {
           echo $HTML;
       }

       /*echo '
            <select id="columns_list" name="columns_list[]" multiple="multiple" size="12" id="columns_list" '.($Listing->ReadOnly ? 'disabled ' : '').'
            style="width: 300px">
            ';
        asort($availCols);
        foreach ($availCols as $Code => $Val){
            if(!array_key_exists($Code, $currentCols))
            {
                if(is_array($list_columns_mandatory) && in_array($Code, $list_columns_mandatory))
                    echo '<option value="' . $Code . '" style="color: red;">'.$Val.' *</option>';
                else
                    echo '<option value="' . $Code . '">'.$Val.'</option>';
            }
        }
        echo '
            </select>  */
        echo '
            </td>
            <td class="windowbg2" align="center">
            '.($Listing->ReadOnly ? '' : '
            <input type="button" value="Add     >>>"
                onclick="addToListing();" /><br /><br />
            <input type="button" value="<<<Remove"
                onclick="removeFromListing()" />
                ').'
            </td>
            <td class="windowbg2" align="center">
            <table>
            <tr>';

            if(!$Listing->ReadOnly)
            {
                echo '<td>
                <img src="Images/up_windowbg2.gif" border="0" onclick="moveUp(\'columns_list2\');"/> <br />
                <img src="Images/collapse_windowbg2.gif" border="0" onclick="moveDown(\'columns_list2\');"/>
                </td>';
            }

            echo '
            <td>
            <select id="columns_list2" name="columns_list2[]" multiple="multiple" size="12" '.($Listing->ReadOnly ? 'disabled ' : '').'
            style="width: 300px">';
        foreach($Listing->Columns as $Column)
        {
            echo '<option value="' . $Column->getTable() . '-' . $Column->getName() . '">'.$Column->getLabel().'</option>';
        }
            echo'
            </select>';
        echo '
            </td>
            </tr>
            </table>
            </td>
        </tr>';

        echo '
        </table>

        </li>
        <li id="default_listing_row" class="padded_div">';

        if($Listing->isDefault())
        {
            echo 'This is the default listing';
        }
        else
        {
            echo '<a href="#" onclick="javascript:ForceSetGlobal(\''.$module.'_LISTING_ID\', '.$listing_id.');jQuery(\'#default_listing_row\').html(\'This is the default listing\');">Make this the default listing</a>';
        }

        echo '
        </li>
        <li>
        <div class="button_wrapper">
            '.($Listing->ReadOnly ? '' : '
            <input type="submit" value="Save settings" border="10" class="button" name="btnSaveDesign" onClick="selectAllMultiCodes();this.form.rbWhat.value=\'Save\'">
            <input type="button" value="Reset to template" border="10" class="button" name="btnReset" onClick="if(confirm(\'Are you sure you want to reset this form?\')){selectAllMultiCodes();this.form.rbWhat.value=\'Reset\';this.form.submit()}">
            <input type="button" value="Delete" border="10" class="button" name="btnReset" onClick="if(confirm(\'Are you sure you want to delete this form?\')){selectAllMultiCodes();this.form.rbWhat.value=\'Delete\';this.form.submit()}">
            ').'
            <input type="button" value="'._tk('btn_back').'" border="10" class="button" name="btnSaveDesign" onClick="if(confirm(\'Are you sure you want to cancel?\')){SendTo(\''.$scripturl.'?action=listlistingdesigns&module='.$module.'\');}">
        </div>
        </li>
        </ol>

        </form>
        ';

        footer();
        obExit();
    }
}

