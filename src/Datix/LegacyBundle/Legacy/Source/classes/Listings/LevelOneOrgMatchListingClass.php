<?php

/**
 * Organisation listings when searching for matching organisations on level one forms.
 */
class Listings_LevelOneOrgMatchListing extends Listings_ModuleListingDesign
{
    /**
     * Returns either user defined (if set) or default columns.
     *
     * @param string $module The module listing to return
     *
     * @return array An array of columns
     */
    static function GetListingColumns($module)
    {
        $Listing = new Listings_LevelOneOrgMatchListing(array('module' => 'ORG', 'listing_id' => self::GetDefaultListing($module)));

        if ($Listing->ListingId == '')
        {
            $Listing->Columns = array(
                new Listings_ListingColumn('org_name'),
                new Listings_ListingColumn('org_reference'),
                new Listings_ListingColumn('org_email'),
            );
        }
        else
        {
            $Listing->LoadColumnsFromDB();
        }

        return $Listing->Columns;
    }

    /**
     * Returns the default listing for a given module.
     *
     * @param $module
     *
     * @return int|string The id of the default listing for this module.
     */
    static function GetDefaultListing($module)
    {
        return GetParm($module.'_L1_ORG_MATCH_LISTING_ID', '');
    }
}