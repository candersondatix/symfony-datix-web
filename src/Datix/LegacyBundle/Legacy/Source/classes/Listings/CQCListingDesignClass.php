<?php
/**
* Module listing class extended for CQC-specific functionality (i.e. adding location hierarchy tiers as columns).
*/
class Listings_CQCListingDesign extends Listings_ModuleListingDesign
{
    /**
    * Gets an array of columns in the style of the old file-based listing design.
    * 
    * Flags columns based on location tiers as not sortable.
    */
    protected function GetColumnArray()
    {
        foreach ($this->Columns as $Column)
        {
            $properties = array('width' => $Column->getWidth());
            
            if (preg_match('/tier_(\d)/u', $Column->getName()))
            {
                $properties['no_sort'] = true;        
            }
            
            $ColumnArray[$Column->getName()] = $properties;
        }

        return $ColumnArray;
    }
}