<?php

/**
* @desc Object representing a listing column (and hence really a database field).
*/
class Listings_ListingColumn extends Fields_Field
{
    protected $Width;
    protected $Descending = true;

    public function setWidth($width)
    {
        $this->Width = $width;
    }
    public function getWidth()
    {
        return $this->Width;
    }

    public function setDescending()
    {
        $this->Descending = true;
    }
    public function setAscending()
    {
        $this->Descending = false;
    }
    public function getOrder()
    {
        if ($this->Descending)
        {
            return 'DESC';
        }
        else
        {
            return 'ASC';
        }
    }
}