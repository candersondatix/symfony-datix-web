<?php
/**
* Factory class for Listings_ListingColumn classes.
*/
class Listings_ListingColumnFactory
{
    /**
    * Returns the correct ListingColumn object for this module.
    * 
    * @param string $module The module code.
    * @param string $field  The field name.
    * @param string $table  The table name.
    */
    public static function getColumn($module, $field, $table = '', $long = false)
    {
        if (preg_match('/^UDF_([0-9]+)$/ui', $field, $matches) != 0)
        {
            $column = new Listings_UDFListingColumn($matches[1]);
        }
        else
        {
            switch ($module)
            {
                case 'CQO':
                case 'CQP':
                case 'CQS':
                    $column = new Listings_CQCListingColumn($field, $table);
                    break;

                default:
                    $column = new Listings_ListingColumn($field, $table, $long);
            }
        }

        return $column;
    }    
}