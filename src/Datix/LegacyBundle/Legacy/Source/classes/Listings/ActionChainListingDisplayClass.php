<?php

class Listings_ActionChainListingDisplay extends Listings_ListingDisplay
{
    protected function GetDataRowHTML($record)
    {
        global $ModuleDefs;
        
        $html = '';
        $listingArray = array();
        
        $record->populateListingArray($listingArray, $this->getColumns());
        
        $html .= '<tr class="listing-row' . (($record->Data['active'] == 'Y') ? '' : ' inactive') . '">';

        foreach ($this->getColumns() as $column)
        {
            $columnId = $column->getTable() . '.' . $column->getName();
            
            if ($listingArray[$columnId][0]->ListingProgress == 0)
            {
                $html .= $this->GetDataCellHTML($listingArray[$columnId][0], $column, $record);
                $listingArray[$columnId][0]->ListingProgress = $listingArray[$columnId][0]->RowNumber;
            }

            $listingArray[$columnId][0]->ListingProgress--;

            if ($listingArray[$columnId][0]->ListingProgress == 0)
            {
                array_shift($listingArray[$columnId]);
            }
        }

        $html .= '</tr>';

        return $html;
    }
}