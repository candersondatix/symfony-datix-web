<?php
/**
* Factory class for listings.
* 
* @codeCoverageIgnore
*/
class Listings_ListingFactory
{
    /**
    * Returns the correct Listing object for this module.
    * 
    * @param string $module      The module code.
    * @param int    $listing_id  The ID for the listing design.
    * 
    * @return Listing
    */
    public static function getListing($module, $listing_id = null)
    {
        switch ($module)
        {
            case 'CQO':    
            case 'CQP':    
            case 'CQS':
                require_once 'Source/classes/Listings/CQCListing.php';
                return new CQCListing($module, $listing_id);
                break;
                
            default:
                require_once 'Source/libs/ListingClass.php';
                return new Listing($module, $listing_id);
                break;    
        }    
    }    
}