<?php

/**
* @desc Class representing the design of a record listing.
* This class is module-independent.
*
* Used to create listings not associated with a particular module.
* This class should not contain any methods to display listings, or to collect data.
*/
class Listings_ListingDesign
{
    /**
     * Constants used when adding additional columns.
     */
    const APPEND  = 1;
    const PREPEND = 2;

    /**
     * @var string
     * @desc The 3 letter code for module that the records in this listing belong to.
     */
    var $Module;

    /**
     * @var string
     * @desc The Table that the records in this listing belong to.
     */
    var $Table;

    /**
    * @var array
    * @desc Array of Listings_ListingColumn objects, representing the columns of the listing.
    */
    var $Columns = array(); //Array of Listings_ListingColumn objects, representing the columns of the listing.

    /**
    * @var string
    * @desc the name of the form on the listings listing page.
    */
    var $Title;

    /**
    * @var bool
    * @desc controls whether the design can be edited.
    */
    var $ReadOnly = false;

    /**
    * @var array
    * @desc Array of option links for each record
    */
    private $options = array();

    /**
     * @var int
     * @desc maximum number of options
     */
    private $maxNumberOptions = 0;


    /**
    * @desc Constructor - sets a couple of basic variables.
    *
    * @param array $Parameters Array of parameters
    * @param string $Parameters[table] The table that the records in this listing will belong to.
    * @param array $Parameters[columns] The columns to use in this listing.
    */
    public function __construct($Parameters)
    {
        $this->Table = $Parameters['table'];
        $this->Module = $Parameters['module'];

        if (is_array($Parameters['columns']) && !empty($Parameters['columns']))
        {
            $this->LoadColumnsFromArray($Parameters['columns']);
        }
    }

    /**
    * @desc Returns the contents of the Table parameter
    */
    protected function getTable()
    {
        return $this->Table;
    }

    /**
     * @desc Returns the contents of the Module parameter
     */
    protected function getModule()
    {
        return $this->Module;
    }

    /**
    * @desc Returns the contents of the Table parameter
    */
    public function getColumnObjects()
    {
        return $this->Columns;
    }

    /**
    * @desc Gets an array of columns in the style of the old file-based listing design. Used while not all code is converted to use the new classes.
    */
    protected function GetColumnArray()
    {
        foreach ($this->Columns as $Column)
        {
            $ColumnArray[$Column->getName()] = array('width' => $Column->getWidth());
        }

        return $ColumnArray;
    }

    /**
     * @desc Sets the array of columns on this object (using this to set null in some confusing circumstances)
     */
    public function SetColumnArray($newColumns)
    {
        $this->Columns = $newColumns;
    }

    /**
    * @desc Loads column definitions directly from an array
    *
    * @param array $Columns Array of Listings_ListingColumn objects.
    */
    public function LoadColumnsFromArray($Columns)
    {
        Listings_ListingDesign::CheckColumns($Columns);

        $this->Columns = $Columns;
    }

    /**
    * @desc Appends additional (probably hard coded) columns to the end of the existing column list.
    *
    * @param array $AdditionalColumns An array of Fields_DummyField objects.
    * @param int   $option            Whether to add the column to the front or the end of the listing.
    */
    public function AddAdditionalColumns(array $AdditionalColumns, $option = self::APPEND)
    {
        Listings_ListingDesign::CheckColumns($AdditionalColumns);

        if ($option == self::PREPEND)
        {
            $this->Columns = SafelyMergeArrays(array($AdditionalColumns, $this->Columns));
        }
        else
        {
            $this->Columns = SafelyMergeArrays(array($this->Columns, $AdditionalColumns));
        }
    }

    /**
    * @desc Checks that the objects contained by an array are all of the correct type to be used as columns in this listing.
    */
    protected static function CheckColumns($ColumnArray)
    {
        foreach ($ColumnArray as $Column)
        {
            if (!($Column instanceof Fields_DummyField))
            {
                throw new IncorrectColumnClassException('Object passed of type '.get_class($Column).' rather than Fields_DummyField');
            }
        }
    }


    /**
    * @desc Similar to the listing HTML generated when viewing a listing, this generates HTML for use in the listing designer,
    * representing a preview of the columns, with fields to set their widths.
    *
    * @return string HTML representing a preview of the listing.
    * No unit test, since it deals with constructing HTML rather than providing any testable return value.
    */
    public function GetListingPreviewHTML()
    {
        global $FieldDefs;

        $HTML = '<table id="listing_preview_table" class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">';

        $HTML .= '<tr name="element_section_row" id="element_section_row">'; //title row
        if (is_array($this->Columns))
        {
            // Table headers
            foreach ($this->Columns as $Column)
            {
                $HTML .= '<th id="col_heading_'.$Column->getTable().'-'.$Column->getName().'" class="windowbg"' . ($Column->getWidth() ? ' width="' . $Column->getWidth() . '%"' : '') . '>';

                if ($Column->getName() == "mod_title")
                {
                    $currentCols["mod_title"] = "Module";
                }
                elseif ($Column->getName() == "recordid")
                {
                    if ($col_info_extra['colrename'])
                    {
                        $currentCols[$Column->getName()] = $col_info_extra['colrename'];
                    }
                    else
                    {
                       //todo: need to define local getModule method
                       $currentCols[$Column->getName()] = GetColumnLabel($Column->getName(), $FieldDefs[$this->getModule()][$Column->getName()]["Title"]);
                    }
                }
                else
                {
                    $currentCols[$Column->getName()] = GetColumnLabel($Column->getName(), $FieldDefs[$this->getModule()][$Column->getName()]["Title"]);
                }
                $HTML .= '<b>' . $currentCols[$Column->getName()] . '</b>';

                $HTML .= '</th>';
            }

            $HTML .= '</tr>';
            $HTML .= '<tr>';

            foreach ($this->Columns as $Column)
            {
                //$HTML .= '<th class="windowbg"' . ($col_info['width'] ? 'width="' . $col_info['width'] . '%"' : '') . '>';

                if ($this->ReadOnly)
                {
                    $HTML .= '<td class="windowbg" style="text-align:center">'.$Column->getWidth().'%</td>';
                }
                else
                {
                    $HTML .= '<td class="windowbg" style="text-align:center"><input id="col_width_'.$Column->getTable().'-'.$Column->getName().'" name="col_width_'.$Column->getTable().'-'.$Column->getName().'" type="string" size="2" maxlength="3" value="'.$Column->getWidth().'" onchange="jQuery(\'#col_heading_'.$Column->getTable().'-'.$Column->getName().'\').width(this.value.toString()+\'%\')"></td>';
                }

                //$HTML .= '</th>';
            }
        }

        $HTML .= '</tr>';

        $HTML .= '</table>';

        return $HTML;

    }

    // @codeCoverageIgnoreEnd
    public function getColumns($includeTablePrefix = false)
    {
        $colArray = array();

        foreach ($this->Columns as $column)
        {
            if($includeTablePrefix)
            {
                $colArray[] = $column->getTable().'.'.$column->getName();
            }
            else
            {
                $colArray[] = $column->getName();
            }
        }

        return $colArray;
    }


    public function setOptions ($recordid, array $options)
    {
        $this->options[$recordid] = $options;

        // keep the max number of options based on the highest array key
        $numOptions = max(array_keys($options));
        $this->maxNumberOptions = max ($numOptions, $this->maxNumberOptions);
    }

    public function getOptions ($recordid)
    {
        return $this->options[$recordid];
    }

    public function getMaxNumOfOptions ()
    {
        return $this->maxNumberOptions;
    }
}

