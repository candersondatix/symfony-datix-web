<?php
require_once 'Source/libs/ListingClass.php';

/**
* Listing class extended for CQC-specific functionality (i.e. adding location hierarchy tiers as columns).
*/
class CQCListing extends Listing
{
    /**
    * Retrieves the label for a listing column header.
    * 
    * @global array $ModuleDefs
    * 
    * @param string $FieldName    The name of the field (in the DB).
    * @param string $DefaultTitle The label to use if no other is found.
    * @param bool   $FullName     Whether or no to use the "long" name from field_formats/field_directory (as opposed to the short one in FieldDefs).
    * @param string $table        The table (DB) the field belongs to.
    * 
    * @return string
    */
    public function GetColumnLabel($FieldName, $DefaultTitle = "", $FullName = false, $table = '')
    {
        global $ModuleDefs;
        
        if (preg_match('/tier_(\d)/u', $FieldName))
        {
            $FieldName = explode('_', $FieldName);

            if ($FullName)
            {
                $sql = "SELECT lti_name + ' (".$ModuleDefs['LOC']['NAME'].")' FROM location_tiers WHERE lti_depth = :depth";
            }
            else
            {
                $sql = 'SELECT lti_name FROM location_tiers WHERE lti_depth = :depth';    
            }
            return DatixDBQuery::PDO_fetch($sql, array('depth' => $FieldName[1]), PDO::FETCH_COLUMN);
        }
        else
        {
            return parent::GetColumnLabel($FieldName, $DefaultTitle, $FullName, $table);    
        }
    }
    
    /**
    * Retrieves the name of the node in the location hierarchy from a specified tier, 
    * which is the ancestor of the location the outcome instance is attached to. 
    * 
    * @global array $ModuleDefs
    * 
    * @param string $tier     The column name in the format tier_<depth>
    * @param int    $recordid The ID of the CQC record.
    * 
    * @return string
    */
    public function getNodeAtTier($tier, $recordid)
    {
        global $ModuleDefs;
        
        $tier = explode('_', $tier);
        $depth = $tier[1];
        
        $node = DatixDBQuery::PDO_fetch('SELECT cdo_location FROM '.$ModuleDefs[$this->Module]['VIEW'].' WHERE recordid = :id', array('id' => $recordid), PDO::FETCH_COLUMN);
        
        $hierarchy = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);
        return $hierarchy->getAncestors($node, $depth);    
    }    
}