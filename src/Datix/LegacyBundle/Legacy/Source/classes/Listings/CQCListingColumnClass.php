<?php
/**
* ListingColumn class extended for CQC-specific functionality (i.e. adding location hierarchy tiers as columns).
*/
class Listings_CQCListingColumn extends Listings_ListingColumn
{
    /**
    * Used to flag that we need a custom select statement when retrieving data for this column.
    * 
    * @var bool
    */
    public $customSelect;
    
    /**
    * Constructor - bypasses DB field def lookups for tier columns (which aren't real fields).
    * 
    * @param string $field
    * @param string $table
    */
    public function __construct($field, $table = '')
    {
        if (preg_match('/tier_(\d)/u', $field))
        {
            $this->Name = $field; 
            $this->Table = $table;
            $this->Data['fdr_label'] = $this->getTierLabel(new DatixDBQuery(''));
            $this->customSelect = true; 
        }
        else
        {
            parent::__construct($field, $table);
        }    
    }
    
    /**
    * Creates the sub select used to retrieve the tier name for this record.
    * 
    * @return string
    * 
    * @throws InvalidParameterException
    */
    public function getCustomSelect()
    {
        $depth = explode('_', $this->Name);
        
        if (!ctype_digit($depth[1]))
        {
            throw new InvalidParameterException('Depth must be an integer');    
        }
        
        return '
            (SELECT parent.loc_name
             FROM vw_locations_main AS node, vw_locations_main AS parent, '.$this->Table.' AS cqc
             WHERE node.lft BETWEEN parent.lft AND parent.rght
             AND node.recordid = cqc.cdo_location
             AND cqc.recordid = '.$this->Table.'.recordid
             AND parent.loc_tier_depth = '.$depth[1].') AS \''.$this->Table.'.'.$this->Name.'\'';
    }
    
    /**
    * Retrieves the name for this tier in the location hierarchy, used for the column header.
    * 
    * @param DatixDBQuery $query
    * 
    * @return string
    */
    protected function getTierLabel(DatixDBQuery $query)
    {
        $depth = explode('_', $this->Name);
        $query->setSQL('SELECT lti_name FROM location_tiers WHERE lti_depth = :depth');
        $query->prepareAndExecute(array('depth' => $depth[1]));
        return $query->fetch(PDO::FETCH_COLUMN);        
    } 
}