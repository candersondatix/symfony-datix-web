<?php
/**
* Factory class for Listings_ModuleListingDesign classes.
*/
class Listings_ModuleListingDesignFactory
{
    /**
    * Returns the correct ListingDesign object for this module.
    *
    * @param string $parameters['module'] The module code.
    */
    public static function getListingDesign($parameters)
    {
        switch ($parameters['module'])
        {
            case 'CQO':
            case 'CQP':
            case 'CQS':
                return new Listings_CQCListingDesign(array('module' => $parameters['module'], 'listing_id' => Listings_ModuleListingDesign::GetDefaultListing($parameters['module'])));
                break;

            default:
                return new Listings_ModuleListingDesign(array('module' => $parameters['module'], 'parent_module' => $parameters['parent_module'], 'link_type' => $parameters['link_type'], 'listing_id' => $listingId));
                break;
        }
    }
}