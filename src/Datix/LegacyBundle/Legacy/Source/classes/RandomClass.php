<?php
/**
* @desc Class which can be used for random data etc.
*/
class Random
{
    /**
    * Replacement for rand() using OpenSSL as your PRNG
    *
    * @param int $min Minimum value
    * @param int $max Maximum value
    *
    * return int
    */
    static public function openssl_rand($min,$max)
    {
        $range = $max - $min;
        if ($range == 0)
        {
            return $min;
        }
        $length = (int) (log($range,2) / 8) + 1;
        return $min + (hexdec(bin2hex(openssl_random_pseudo_bytes($length,$s))) % $range);
    }

    /**
    * Random number generator for non-security related code. Required because openssl_random_pseudo_bytes is extremely slow.
    *
    * @param int $min Minimum value
    * @param int $max Maximum value
    *
    * return int
    */
    static public function unsecure_rand($min,$max)
    {
        return mt_rand($min,$max);
    }

    /**
    * generates a random p_a_s_s_w_o_r_d, uses base64: 0-9a-zA-Z/+
    * @param int [optional] $length length of p_a_s_s_w_o_r_d, default 24 (144 Bit)
    * @return string p_a_s_s_w_o_r_d
    */
    static public function Random_Password($length = 24) {
            if(function_exists('openssl_random_pseudo_bytes')) {
                $sPasswd = base64_encode(openssl_random_pseudo_bytes($length, $strong));
                if($strong == TRUE)
                    return \UnicodeString::substr($sPasswd, 0, $length); //base64 is about 33% longer, so we need to truncate the result
            }

            //fallback to mt_rand if php < 5.3 or no openssl available
            $characters = '0123456789';
            $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+';
            $charactersLength = \UnicodeString::strlen($characters)-1;
            $sPasswd = '';

            //select some random characters
            for ($i = 0; $i < $length; $i++) {
                $sPasswd .= $characters[Random::openssl_rand(0, $charactersLength)];
            }

            return $sPasswd;
    }
}
