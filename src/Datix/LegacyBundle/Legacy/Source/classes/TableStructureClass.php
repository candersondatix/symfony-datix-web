<?php

/**
* @desc Holds the structure of the relationship between a set of tables, holding information about
* they way they can be linked together.
*/
class TableStructure
{
    public $TableArray;
    public $BaseTable;
    public $ParentList;

    public $Map;

    public function __construct($BaseTable)
    {
        $this->BaseTable = $BaseTable;
        $this->TableArray = array();
    }

    /**
    * @desc Adds a new table to the structure.
    */
    public function addTable($Table, $parentTable, $dataArray)
    {
        if($parentTable == $this->BaseTable)
        {
            if(!isset($this->TableArray['children'][$Table]))
            {
                $this->TableArray['children'][$Table] = $dataArray;
                $this->Map[$Table] = array($Table);
            }
        }
        else
        {
            $this->addToTable($Table, $parentTable, $dataArray, $this->TableArray);
        }
    }

    /**
    * @desc Iterative function used in addTable().
    */
    protected function addToTable($Table, $parentTable, $dataArray, &$targetTable)
    {
        if(isset($targetTable['children'][$parentTable]))
        {
            if(!isset($targetTable['children'][$parentTable]['children'][$Table]))
            {
                $targetTable['children'][$parentTable]['children'][$Table] = $dataArray;
                $this->Map[$Table] = $this->Map[$parentTable];
                $this->Map[$Table][] = $Table;
            }
        }
        else
        {
            $this->addToTable($Table, $parentTable, $dataArray, $targetTable['children'][$parentTable]);
        }
    }

    /**
    * @desc Returns a TableStructure object representing a subset of the tables included in this object.
    */
    public function getSubStructure($Table)
    {
        if($Table == $this->BaseTable)
        {
            return $this;
        }
        else
        {
            return $this->getSubStructureFrom($Table, $this->TableArray);
        }
    }

    /**
    * @desc Iterative function used in getSubStructure().
    */
    protected function getSubStructureFrom($Table, $targetTable)
    {
        if(is_array($targetTable['children']))
        {
            foreach($targetTable['children'] as $childTable => $children)
            {
                if($childTable == $Table)
                {
                    $newTableStructure = new TableStructure($Table);
                    $newTableStructure->TableArray = $children;
                }
                else
                {
                    $newTableStructure = $this->getSubStructureFrom($Table, $children);
                }

                if($newTableStructure)
                {
                    return $newTableStructure;
                }
            }
        }
    }

}
