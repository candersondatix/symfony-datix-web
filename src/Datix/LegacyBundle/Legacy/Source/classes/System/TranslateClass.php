<?php

/**
* Singleton class that calls a Zend_Translate object
*/
class System_Translate
{
    public static function getTranslator()
    {
        //No translation file, so just use a dummy class which applies a trivial translation.
        return new View_NullTranslate();
    }
}
