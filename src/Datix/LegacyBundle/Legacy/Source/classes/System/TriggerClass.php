<?php

/**
* Represents a trigger - an action assigned to a where clause.
*/
class System_Trigger
{
    protected $ID;

    protected $Name;

    protected $Expression;

    protected $Module;

    protected $QueryName;

    protected $ActionChain;

    /**
    * Picks up trigger information from triggers_main table.
    *
    * @param int $ID The id of the trigger in triggers_main
    * @param char(3) $module The module that this trigger affects (needed if no id provided)
    */
    public function __construct($ID, $module = '')
    {
        $this->ID = $ID;

        if($ID)
        {
            $TriggerDetails = DatixDBQuery::PDO_fetch('SELECT trg_expression, trg_message, mod_id, trg_query, trg_action_chain from triggers_main WHERE recordid = :recordid', array('recordid' => $this->ID));

            $this->Name = $TriggerDetails['trg_message'];
            $this->Expression = $TriggerDetails['trg_expression'];
            $this->Module = ModuleIDToCode($TriggerDetails['mod_id']);
            $this->QueryName = $TriggerDetails['trg_query'];

            $this->ActionChain = $TriggerDetails['trg_action_chain'];
        }
        else
        {
            $this->Module = $module;
        }
    }

    /**
    * Getter for the trigger ID
    */
    public function getID()
    {
        return $this->ID;
    }

    /**
    * Getter for the trigger name
    */
    public function getName()
    {
        return $this->Name;
    }

    /**
    * Getter for the where clause associated with the trigger
    */
    public function getExpression()
    {
        return $this->Expression;
    }

    /**
    * Getter for the action chain associated with the trigger
    */
    public function getActionChain()
    {
        return $this->ActionChain;
    }

    /**
    * Getter for the module that this trigger affects
    */
    public function getModule()
    {
        return $this->Module;
    }

    /**
    * Getter for the name of the saved query that was used to set up this trigger.
    */
    public function getQueryName()
    {
        return $this->QueryName;
    }

    /**
    * Fills the trigger object with values from the $_POST variable. Used when adding a new trigger or updating an existing trigger.
    */
    public function UpdateFromPost()
    {
        $this->Name = $_POST['trg_message'];
        $this->Expression = $_POST['trg_expression'];
        $this->QueryName = $_POST['trg_query'];
        $this->ActionChain = $_POST['trg_action_chain'];

        if (!$this->Module)
        {
            $this->Module = $_POST['module'];
        }

        $this->SaveConfigurationToDatabase();
    }

    /**
    * Takes the values from the trigger object and updates the relevent db record with them.
    */
    protected function SaveConfigurationToDatabase()
    {
        $params = array(
            'trg_message' => $this->Name,
            'mod_id' => ModuleCodeToID($this->Module),
            'trg_expression' => $this->Expression,
            'trg_query' => $this->QueryName,
            'trg_action_chain' => $this->ActionChain    
        );
        
        if (!$this->ID)
        {
            $this->ID = GetNextRecordID('triggers_main');
            
            $params['trg_type'] = 'AC';
            $params['trg_app_show'] = 'WEB';
            $params['recordid'] = $this->ID;
            
            $fields = array_keys($params);
            $sql = 'INSERT INTO triggers_main (' . implode(',', $fields) . ') VALUES (' . implode(',', array_map('addColon', $fields)) . ')'; 
        }
        else
        {
            $params['recordid'] = $this->ID;
            $sql = 'UPDATE triggers_main SET trg_message = :trg_message, trg_expression = :trg_expression, trg_query = :trg_query, trg_action_chain = :trg_action_chain, mod_id = :mod_id WHERE recordid = :recordid';  
        }
        
        DatixDBQuery::PDO_query($sql, $params);
    }

    /**
    * Deletes the record representing the trigger object from the db.
    */
    public function DeleteTrigger()
    {
        DatixDBQuery::PDO_query('DELETE FROM triggers_main WHERE recordid = :recordid', array('recordid' => $this->getID()));
    }
}

