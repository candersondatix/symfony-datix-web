<?php

//Class representing a set of system parameters (global, profile and user).

class System_Globals
{
    protected $GlobalParameters;
    protected $ProfileParameters;
    protected $UserParameters;

    /**
    * @desc Constructor. Sets up arrays of key-value pairs for each set of parameters used in the application.
    *
    * @param string $username The username of a user, if one is logged in.
    */
    public function __construct($username = '')
    {
        // Get global parameters
        $sql = "SELECT parameter, parmvalue as [value] FROM globals";
        $this->GlobalParameters = PDO_fetch_all($sql, array(), PDO::FETCH_KEY_PAIR);

        if ($username)
        {
            //Get profile parameters
            $sql = 'SELECT lpp_parameter as parameter, lpp_value as [value] FROM link_profile_param, contacts_main
                    WHERE link_profile_param.lpp_profile = contacts_main.sta_profile
                    AND contacts_main.login = :username';
            $this->ProfileParameters = PDO_fetch_all($sql, array('username' => $username), PDO::FETCH_KEY_PAIR);

            // Get user parameters
            $sql = 'SELECT parameter, parmvalue as [value] FROM user_parms WHERE login = :username';
            $this->UserParameters = PDO_fetch_all($sql, array('username' => $username), PDO::FETCH_KEY_PAIR);
        }
    }

    /**
    * @desc Gets a parameter value from user, profile or global parameters in order.
    *
    * @param string $key The name of the parameter we want to find a value for.
    * @param string $default The default value to fall back on if nothing is set in the system.
    */
    public function GetValue($key, $default)
    {
        if($this->UserParameters[$key])
        {
            return $this->UserParameters[$key];
        }
        else if($this->ProfileParameters[$key])
        {
            return $this->ProfileParameters[$key];
        }
        else if($this->GlobalParameters[$key])
        {
            return $this->GlobalParameters[$key];
        }

        return $default;
    }


    /**
    * @desc Gets a global value, ignoring user and profile parameters.
    *
    * @param string $key The name of the parameter we want to find a value for.
    * @param string $default The default value to fall back on if nothing is set in the system.
    */
    public function GetGlobalValueOnly($key, $default)
    {
        if($this->GlobalParameters[$key])
        {
            return $this->GlobalParameters[$key];
        }

        return $default;
    }

    /**
    * @desc Gets a profile value, ignoring user and global parameters.
    *
    * @param string $key The name of the parameter we want to find a value for.
    * @param string $default The default value to fall back on if nothing is set in the system.
    */
    public function GetProfileValueOnly($key, $default)
    {
        if($this->ProfileParameters[$key])
        {
            return $this->ProfileParameters[$key];
        }

        return $default;
    }

    /**
    * @desc Gets a user value, ignoring profile and global parameters.
    *
    * @param string $key The name of the parameter we want to find a value for.
    * @param string $default The default value to fall back on if nothing is set in the system.
    */
    public function GetUserValueOnly($key, $default)
    {
        if($this->UserParameters[$key])
        {
            return $this->UserParameters[$key];
        }

        return $default;
    }

    /**
    * @desc Sets a session global to a particular value.
    *
    * @param string $key The name of the parameter we want to set a value for.
    * @param string $value The value to set.
    */
    public function SetGlobalValue($key, $value)
    {
        $this->GlobalParameters[$key] = $value;
    }

    /**
    * @desc Initialises a session object holding parameter information for the currently logged in user.
    */
    public static function InitialiseSessionObject()
    {
        $Globals = new System_Globals($_SESSION['login']);

        $_SESSION['GlobalObj'] = $Globals;
    }

    /**
    * @desc Gets a parameter value from the session from user, profile or global parameters in order.
    *
    * @param string $key The name of the parameter we want to find a value for.
    * @param string $default The default value to fall back on if nothing is set in the system.
    */
    public static function GetCurrentValue($key, $default)
    {
        if(!isset($_SESSION['GlobalObj']))
        {
            System_Globals::InitialiseSessionObject();
        }

        return $_SESSION['GlobalObj']->GetValue($key, $default);
    }

    /**
    * @desc Gets a global value from the session, ignoring user and profile parameters.
    *
    * @param string $key The name of the parameter we want to find a value for.
    * @param string $default The default value to fall back on if nothing is set in the system.
    */
    public static function GetCurrentValue_GlobalOnly($key, $default)
    {
        if(!isset($_SESSION['GlobalObj']))
        {
            System_Globals::InitialiseSessionObject();
        }

        return $_SESSION['GlobalObj']->GetGlobalValueOnly($key, $default);
    }

}

