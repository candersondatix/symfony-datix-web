<?php
/**
* System_NestedSetMapper Factory class.
*
* @codeCoverageIgnore
*/
class System_NestedSetMapperFactory
{
    const LOCATIONS = 1;

    /**
    * Constructs and returns a System_NestedSetMapper object.
    *
    * @param int $type The object type.
    *
    * @return System_NestedSetMapper
    */
    public static function getNestedSetMapper($type)
    {
        switch ($type)
        {
            case self::LOCATIONS:
                return new Location_NestedSetMapper(new DatixDBQuery(''), 'vw_locations_main', 'loc_name', 'recordid', 'loc_tier_depth');
                break;
        }

        return null;
    }
}