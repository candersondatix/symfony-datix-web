<?php
/**
* Utility functions for retrieving data from nested set hierarchies.
*/
class System_NestedSetMapper
{
    /**
    * The DB query object.
    * 
    * @var DatixDBQuery
    */
    protected $query;
    
    /**
    * The DB table the hierarchy data is stored in.
    * 
    * @var string
    */
    protected $table;
    
    /**
    * The field in the DB table for the record description.
    * 
    * @var string
    */
    protected $descField;
    
    /**
    * The field in the DB table for the record id.
    * 
    * @var string
    */
    protected $idField;
    
    /**
    * The field in the DB table for the tier depth of the current node.
    * 
    * @var string
    */
    protected $depthField;
    
    /**
    * Constructor.
    * 
    * @param DatixDBQuery $query
    * @param string       $table
    * @param string       $descField
    * @param string       $idField
    */
    public function __construct(DatixDBQuery $query, $table, $descField, $idField = 'recordid', $depthField = '')
    {
        $this->query = $query;    
        $this->table = $table;    
        $this->descField = $descField;    
        $this->idField = $idField;
        $this->depthField = $depthField;     
    }
    
    /**
     * Inserts a record into the locations table
     *
     * @param string $name The name of the new location
     * @param int $parentNode The id of the parent node of the location. Defaults to null
     * @return int|false
     */
    public function insert($name, $parentNode = null)
    {
        $data = array_merge(array(
            'desc' => $name
        ), $this->_getLeftRight($parentNode));
        
        $this->query->setSQL("INSERT INTO ".$this->table." (".$this->descField.", lft, rght) VALUES (:desc, :lft, :rght)");
        $result = $this->query->prepareAndExecute($data);
        
        if ($result)
        {
            return $this->query->PDO->lastInsertId();
        }
        
        return false;
    }
    
    /**
     * Finds all locations, ordered by their left value
     *
     * @return array
     */
    public function findAll()
    {
        $this->query->setSQL('
            SELECT t1.'.$this->descField.', t1.'.$this->idField.', (COUNT(t2.'.$this->idField.') - 1) AS depth, t1.loc_active, t1.loc_active_inherit
            FROM '.$this->table.' t1,'.$this->table.' t2
            WHERE t1.lft BETWEEN t2.lft AND t2.rght
            GROUP BY t1.'.$this->idField.', t1.lft, t1.'.$this->descField.', t1.loc_active, t1.loc_active_inherit
            ORDER BY t1.lft'
        );
        
        $this->query->prepareAndExecute();
        return $this->query->fetchAll();
    }
    
    protected function _getLeftRight($parentId)
    {
        $return = array();
        $lft = null;
        $rgt = null;
        
        if ($parentId)
        {
            $this->query->setSQL("SELECT lft, rght FROM ".$this->table." WHERE recordid = ?");
            $this->query->prepareAndExecute(array($parentId));
            
            $result = $this->query->fetch();
            
            if ($result)
            {
                $lft = (int) $result['lft'];
                $rgt = (int) $result['rght'];
            }
        }
        
        if ($lft !== null && $rgt !== null)
        {
            $this->query->setSQL("UPDATE ".$this->table." SET rght = rght + 2 WHERE rght >= $rgt");
            $this->query->prepareAndExecute();
            $this->query->setSQL("UPDATE ".$this->table." SET lft = lft + 2 WHERE lft > $rgt");
            $this->query->prepareAndExecute();
            
            $return['lft'] = $rgt;
            $return['rght'] = $rgt + 1;
        }
        else
        {
            $this->query->setSQL("SELECT MAX(rght) FROM ".$this->table);
            $this->query->prepareAndExecute();
            $max_rgt = $this->query->fetch(PDO::FETCH_COLUMN);

            if ($max_rgt) 
            {
                $return['lft'] = $max_rgt + 1;
            }
            else
            {
                //No data? First node...
                $return['lft'] = 1;
            }

            $return['rght'] = $return['lft'] + 1;
        }
        
        return $return;
    }
    
    /**
    * Fetches all the nodes at a specified depth.
    * 
    * @param int    $depth  The hierarchy depth.
    * @param string $where  An optional additional where clause for filtering results.
    * @param array  $params Extra bound parameters if needed with additional where clause.
    * 
    * @return array
    * 
    * @throws InvalidParameterException 
    */
    public function getNodesByDepth($depth, $where = '', array $params = array())
    {
        if (!ctype_digit($depth))
        {
            throw new InvalidParameterException('Depth must be an integer');    
        }
        
        $this->query->setSQL('
            SELECT '.$this->idField.', '.$this->descField.', lft FROM 
            (
                SELECT node.'.$this->idField.', node.'.$this->descField.', node.lft, (COUNT(parent.'.$this->idField.') - 1) AS depth
                FROM '.$this->table.' AS node, '.$this->table.' AS parent
                WHERE node.lft BETWEEN parent.lft AND parent.rght'.($where != '' ? '
                AND node.'.$this->idField.' IN (SELECT '.$this->table.'.'.$this->idField.' FROM '.$this->table.' WHERE '.$where.')' : '').'
                GROUP BY node.'.$this->idField.', node.'.$this->descField.', node.lft
            ) as q
            WHERE q.depth = :depth
            ORDER BY lft
        ');
        
        $this->query->prepareAndExecute(array_merge(array('depth' => $depth), $params));
        return $this->query->fetchAll();        
    }
    
    /**
    * Fetches all the leaf nodes under a specified node.
    * 
    * @param int $nodeId
    * 
    * @return array
    * 
    * @throws InvalidParameterException 
    */
    public function getLeafNodes($nodeId)
    {
        if (!ctype_digit($nodeId))
        {
            throw new InvalidParameterException('Node ID must be an integer');    
        }
        
        $this->query->setSQL('
            SELECT node.'.$this->idField.', node.'.$this->descField.'
            FROM '.$this->table.' AS node, '.$this->table.' AS parent
            WHERE node.lft BETWEEN parent.lft AND parent.rght
            AND node.rght = node.lft + 1
            AND parent.'.$this->idField.' = :id
            ORDER BY node.lft
        ');
        
        $this->query->prepareAndExecute(array('id' => $nodeId));
        return $this->query->fetchAll();
    }
    
    /**
    * Retrieves the ancestor(s) of a given node. 
    * 
    * @param int $nodeId The ID of the starting node.
    * @param int $depth  The depth of the returned ancestor node (can only be used if the tier depth is defined in the DB schema).
    * 
    * @return array
    * 
    * @throws InvalidParameterException 
    */
    public function getAncestors($nodeId, $depth = '')
    {
        if (!ctype_digit($nodeId))
        {
            throw new InvalidParameterException('Node ID must be an integer');    
        }
        
        if ($depth != '' && $this->depthField == '')
        {
            throw new InvalidParameterException('No depth field specified for this hierarchy');    
        }
        
        $this->query->setSQL('
            SELECT parent.'.$this->idField.', parent.'.$this->descField.'
            FROM '.$this->table.' AS node, '.$this->table.' AS parent
            WHERE node.lft BETWEEN parent.lft AND parent.rght
            AND node.'.$this->idField.' = :id
            '.($depth != '' ? 'AND parent.'.$this->depthField.' = :depth' : '').'
            ORDER BY node.lft
        ');
        
        $params = array('id' => $nodeId);
        if ($depth != '')
        {
            $params['depth'] = $depth;    
        }
        
        $this->query->prepareAndExecute($params);
        return $this->query->fetchAll(\PDO::FETCH_KEY_PAIR);        
    }
    
    /**
    * Retrieves the descendants of a given node. 
    * 
    * @param int $nodeId The ID of the starting node.
    * 
    * @return array
    */
    public function getDescendants($nodeId)
    {
        if (!ctype_digit($nodeId))
        {
            throw new InvalidParameterException('Node ID must be an integer');    
        }
        
        $this->query->setSQL('
            SELECT child.'.$this->idField.', child.'.$this->descField.'
            FROM '.$this->table.' AS node, '.$this->table.' AS child
            WHERE child.lft BETWEEN node.lft AND node.rght
            AND node.'.$this->idField.' = :id
            ORDER BY child.lft
        ');

        $params = array('id' => $nodeId);
        
        $this->query->prepareAndExecute($params);
        return $this->query->fetchAll(\PDO::FETCH_KEY_PAIR);    
    }
    
    
    /**
     * Deletes a node and all its child nodes
     *
     * @param int $nodeId The id of the node to delete
     * @return boolean Success of the delete operation
     * @throws InvalidParameterException if the node id is not numeric
     */
    public function deleteNode($nodeId)
    {
        if (!is_int($nodeId))
        {
            throw new InvalidParameterException('Node ID must be an integer');    
        }
        
        $this->query->setSQL('SELECT lft, rght FROM locations_main WHERE recordid = :id');
        $result = $this->query->prepareAndExecute(array('id' => $nodeId));

        if (!$result) {
            return false;

        }
        
        $data = $this->query->fetch();
        
        $sql = 'DELETE FROM ' . $this->table . '
                WHERE lft BETWEEN :lft
                AND :rght';
                
        $this->query->setSQL($sql);
        return $this->query->prepareAndExecute($data);
    }
}