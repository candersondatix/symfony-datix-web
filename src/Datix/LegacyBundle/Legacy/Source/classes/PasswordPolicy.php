<?php

require_once(dirname(__FILE__) . '/Exceptions/PasswordPolicy/InvalidValue.php');
require_once(dirname(__FILE__) . '/Exceptions/PasswordPolicy/MinRequiredLength.php');

/**
 * Represents the PW policy for a particular Datix installation
 * Used to set the PW policy settings and validate PWs against
 *
 * @author James Nicholls <jnicholls@datix.co.uk>
 */
class PasswordPolicy
{
	/**
	 * The minimum length a PW needs to be
	 *
	 * @var int
	 */
	private $_minLength;
	
	/**
	 * The minimum amount of numbers a PW needs to contain
	 *
	 * @var int
	 */
	private $_numbers;
	
	/**
	 * The minimum amount of symbols a PW must contain
	 *
	 * @var int
	 */
	private $_symbols;
	
	/**
	 * The minimum amount of uppercase letters a PW must contain
	 *
	 * @var int
	 */
	private $_ucLetters;
	
	/**
	 * The minimum amount of lowercase letters a PW must contain
	 *
	 * @var int
	 */
	private $_lcLetters;
	
	/**
	 * The length, in days, after which a PW is considered expired
	 *
	 * @var int
	 */
	private $_expiry;
	
	/**
	 * The amount of PWs that a new PW will need to be validated against to determine if it's unique
	 *
	 * @var int
	 */
	private $_uniquePasswordAmount;
	
	/**
	 * Sets the minimum length a PW needs to be
	 *
	 * @param int $length The minimum length a PW needs to be
	 * @return self
	 * @throws Exception_PwdPolicy_InvalidValue if the length is not an integer
	 * @throws Exception_PwdPolicy_InvalidValue if the length is greater than 999 or less than 0
	 * @throws Exception_PwdPolicy_MinRequiredLength if the length is less than the combined length of the policy's required symbols, numbers, uppercase and lowercase letters
	 */
	public function setMinLength($length)
	{
		$requiredLength = $this->_calculateRequiredMinLength();
		
		if (!is_numeric($length)) {
			throw new Exception_PwdPolicy_InvalidValue('Minimum length must be numeric');
		}
		
		if ($length < 0 || $length > 999) {
			throw new Exception_PwdPolicy_InvalidValue('Minimum length must be greater than 0 and less 1000');
		}
		
		if ($length < $requiredLength) {
			throw new Exception_PwdPolicy_MinRequiredLength('Minimum length is less than the combined length of required symbols, numbers and uppercase and lowercase letters');
		}
		
		return $this->_minLength = (int) $length;
	}
	
	/**
	 * Gets the minimum length a PW needs to be
	 *
	 * @return int
	 */
	public function getMinLength()
	{
		return $this->_minLength;
	}
	
	/**
	 * Sets the minimum amount of numbers a PW needs to contain
	 *
	 * @param int $amount The amount of numbers a PW needs to contain
	 * @return self
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of numbers is not numeric
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of numbers is greater than 999 or less than 0
	 * @throws Exception_PwdPolicy_MinRequiredLength if the combined length of numbers, symbols and uppercase and lowercase letters is greater than the policy's minimum length
	 */
	public function setNumbers($amount)
	{
		if (!is_numeric($amount)) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of numbers required must be numeric');
		}
		
		if ($amount < 0 || $amount > 999) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of numbers must be greater than 0 and less 1000');
		}
		
		$old = $this->_numbers;
		$this->_numbers = (int) $amount;
		$requiredLength = $this->_calculateRequiredMinLength();
		
		if ($this->_minLength < $requiredLength) {
			$this->_numbers = $old;
			throw new Exception_PwdPolicy_MinRequiredLength('Minimum length is less than the combined length of required symbols, numbers and uppercase and lowercase letters');
		}
		
		return $this;
	}
	
	/**
	 * Gets the minimum amount of numbers a PW needs to contain
	 *
	 * @return int
	 */
	public function getNumbers()
	{
		return $this->_numbers;
	}
	
	/**
	 * Sets the minimum amount of symbols a PW needs to contain
	 *
	 * @param int $amount The minimum amount of symbols a PW needs to contain
	 * @return self
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of symbols is not numeric
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of symbols is greater than 999 or less than 0
	 * @throws Exception_PwdPolicy_MinRequiredLength if the combined length of numbers, symbols and uppercase and lowercase letters is greater than the policy's minimum length
	 */
	public function setSymbols($amount)
	{
		if (!is_numeric($amount)) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of symbols required must be numeric');
		}
		
		if ($amount < 0 || $amount > 999) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of symbols must be greater than 0 and less 1000');
		}
		
		$old = $this->_symbols;
		$this->_symbols = (int) $amount;
		$requiredLength = $this->_calculateRequiredMinLength();
		
		if ($this->_minLength < $requiredLength) {
			$this->_symbols = $old;
			throw new Exception_PwdPolicy_MinRequiredLength('Minimum length is less than the combined length of required symbols, numbers and uppercase and lowercase letters');
		}
		
		return $this;
	}
	
	/**
	 * Gets the minimum amount of symbols a PW must contain
	 *
	 * @return int
	 */
	public function getSymbols()
	{
		return $this->_symbols;
	}
	
	/**
	 * Sets the minimum amount of uppercase letters a PW must contain
	 *
	 * @param int $amount The minimum amount of uppercase letters a PW must contain
	 * @return self
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of uppercase letters is not numeric
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of uppercase letters is greater than 999 or less than 0
	 * @throws Exception_PwdPolicy_MinRequiredLength if the combined length of numbers, symbols and uppercase and lowercase letters is greater than the policy's minimum length
	 */
	public function setUcLetters($amount)
	{
		if (!is_numeric($amount)) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of uppercase letters required must be numeric');
		}
		
		if ($amount < 0 || $amount > 999) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of uppercase letters must be greater than 0 and less 1000');
		}
		
		$old = $this->_ucLetters;
		$this->_ucLetters = (int) $amount;
		$requiredLength = $this->_calculateRequiredMinLength();
		
		if ($this->_minLength < $requiredLength) {
			$this->_ucLetters = $old;
			throw new Exception_PwdPolicy_MinRequiredLength('Minimum length is less than the combined length of required symbols, numbers and uppercase and lowercase letters');
		}
		
		return $this;
	}
	
	/**
	 * Gets the minimum amount of uppercase letters a PW must contain
	 *
	 * @return int
	 */
	public function getUcletters()
	{
		return $this->_ucLetters;
	}
	
	/**
	 * Sets the minimum amount of lowercase letters a PW must contain
	 *
	 * @param int $amount The minimum amount of lowercase letters a PW must contain
	 * @return self
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of lowercase letters is not numeric
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of lowercase letters is greater than 999 or less than 0
	 * @throws Exception_PwdPolicy_MinRequiredLength if the combined length of numbers, symbols and uppercase and lowercase letters is greater than the policy's minimum length
	 */
	public function setLcLetters($amount)
	{
		if (!is_numeric($amount)) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of lowercase letters required must be numeric');
		}
		
		if ($amount < 0 || $amount > 999) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of lowercase letters must be greater than 0 and less 1000');
		}
		
		$old = $this->_lcLetters;
		$this->_lcLetters = (int) $amount;
		$requiredLength = $this->_calculateRequiredMinLength();
		
		if ($this->_minLength < $requiredLength) {
			$this->_lcLetters = $old;
			throw new Exception_PwdPolicy_MinRequiredLength('Minimum length is less than the combined length of required symbols, numbers and uppercase and lowercase letters');
		}
		
		return $this;
	}
	
	/**
	 * Gets the minimum amount of lowercase letters a PW must contain
	 *
	 * @return int
	 */
	public function getLcLetters()
	{
		return $this->_lcLetters;
	}
	
	/**
	 * Sets the length, in days, after which a PW is considered expired
	 *
	 * @param int $expiration The amount of days after which a PW is considered expired
	 * @return self
	 * @throws Exception_PwdPolicy_InvalidValue if the given expiration is not numeric
	 * @throws Exception_PwdPolicy_InvalidValue if the expiration amount is greater than 999 or less than 0
	 */
	public function setExpiry($expiration)
	{
		if (!is_numeric($expiration)) {
			throw new Exception_PwdPolicy_InvalidValue('The expiry must be numeric');
		}
		
		if ($expiration < 0 || $expiration > 999) {
			throw new Exception_PwdPolicy_InvalidValue('The expiration amount must be greater than 0 and less 1000');
		}
		
		$this->_expiry = (int) $expiration;
		
		return $this;
	}
	
	/**
	 * Gets the length, in days, after which a PW is considered expired
	 *
	 * @return int
	 */
	public function getExpiry()
	{
		return $this->_expiry;
	}
	
	/**
	 * Sets the amount of previous PWs that a given PW will be validated against during validation
	 * If set to a number less than 1, PWs will not be validated against previous PWs
	 *
	 * @param int $amount The amount of previous PWs to validate against
	 * @return self
	 * @throws Exception_PwdPolicy_InvalidValue if the given amount is not numeric
	 * @throws Exception_PwdPolicy_InvalidValue if the amount of unique PWs is greater than 999 or less than 0
	 */
	public function setUniquePasswordAmount($amount)
	{
		if (!is_numeric($amount)) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of unique passwords must be numeric');
		}
		
		if ($amount < 0 || $amount > 999) {
			throw new Exception_PwdPolicy_InvalidValue('The amount of unique passwords must be greater than 0 and less 1000');
		}
		
		$this->_uniquePasswordAmount = (int) $amount;
		
		return $this;
	}
	
	/**
	 * Gets the amount of PWs that a new PW will need to be validated against
	 *
	 * @return int
	 */
	public function getUniquePasswordAmount()
	{
		return $this->_uniquePasswordAmount;
	}
	
	/**
	 * @todo Will probably need a method for setting the previous PWs before running validation
	 */
	
	/**
	 * Validates a string against the PW policy
	 *
	 * @param string $string The string to validate
	 * @return boolean
	 * @todo Implement me!
	 */
	public function validate($string)
	{
		return true;
	}
	
	/**
	 * Calculates the minimum length a PW would need to be in order to satisfy the other policy requirements (eg min. number of uppercase letters)
	 *
	 * @return int
	 */
	private function _calculateRequiredMinLength()
	{
		return ($this->_ucLetters + $this->_lcLetters + $this->_symbols + $this->_numbers);
	}
}