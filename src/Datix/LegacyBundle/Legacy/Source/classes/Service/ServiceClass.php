<?php

/**
* @desc Services are collections of methods that can be called
* from a url. Act as an alternative to ?action=, and don't require
* large arrays of function mappings.
*
* In addition, by putting all of these classes under the "service" folder, we can
* restrict the functionality that users can access via the url.
*
* Public methods accept one parameter, which will be an array of parameters.
* This array will be pre-populated with the $_GET array when called from the url,
* making the execution of these functions identical whether called from url or
* from internal code.
*
* This class is a base class that can be extended for actual implementable services.
*/
abstract class Service_Service
{
    protected $Parameters = array();

    public function __construct($Parameters = array())
    {
        $this->Parameters = $Parameters;
    }

}
