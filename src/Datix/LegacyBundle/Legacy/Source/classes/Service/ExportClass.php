<?php
/**
* Service class for export functionality.
*/
class Service_Export extends Service_Service
{
    /**
    * Export data from Rico tables.
    */
    public function rico()
    {
        switch ($this->Parameters['format'])
        {
            case 'csv':
                $file = new Files_CSV($this->Parameters['id']);
                break;    
        }
        
        $PDOParams = array();
        if (is_array($_SESSION[$this->Parameters['id'].'_PDOParams']))
        {
            $PDOParams = $_SESSION[$this->Parameters['id'].'_PDOParams'];
        }

        //If pagesize and startpos are not defined in the GET request for the exported document, then assume that the request was for all the results in the table
        $sql = ($this->Parameters['pagesize'] || $this->Parameters['startpos']) ? ($_SESSION[$this->Parameters['id'].'_query']) : ($_SESSION[$this->Parameters['id'].'_query_unlimited']);

        $data = DatixDBQuery::PDO_fetch_all($sql, $PDOParams);
        
        foreach ($data as $key => $row)
        {
            // remove row numbers
            unset($data[$key]['rico_rownum']);    
        }
        
        if (is_numeric($this->Parameters['startpos']) && is_numeric($this->Parameters['pagesize']))
        {
            // return visible rows
            $data = array_slice($data, $this->Parameters['startpos'] - 1, $this->Parameters['pagesize']);    
        }
        
        $file->setData($data);
        $file->export(true);
    }
    
    /**
    * Prints the last export error message.
    */
    public function getExportError()
    {
        echo Files_File::getExportError();
    }
    
    /**
    * Outputs NPSA export errors in CSV format.
    */
    public function getNPSAExportError()
    {
        $csv = new Files_CSV('npsa_export_errors');
        $csv->setData($_SESSION['npsaXMLErrors']);
        $csv->export();
    }
    
    /**
    * XML export for CFSMS.
    */
    public function cfsms()
    {
        if (isset($_POST['sirs_trust']) && DatixDBQuery::PDO_fetch('SELECT parmvalue FROM globals WHERE parameter = \'SIRS_TRUST\'', array(), PDO::FETCH_COLUMN) == '')
        {
            // set the SIRS_TRUST global before proceeding with export       
            DatixDBQuery::PDO_query('DELETE FROM globals WHERE parameter = \'SIRS_TRUST\'');       
            DatixDBQuery::PDO_query('INSERT INTO globals (parameter, parmvalue) VALUES (\'SIRS_TRUST\', :sirs_trust)', array('sirs_trust' => $_POST['sirs_trust']));       
        }
        
        $exporter = new Export_CFSMS(new Files_XML(getParm('SIRS_TRUST', '', true).'_'.(new DateTime)->format('Ymd')), new DOMDocument('1.0', 'UTF-8'));
        $exporter->doExport();    
    }
    
    /**
    * CSV export for ISD.
    */
    public function isd()
    {
        if ($_GET['agent'] != '' && DatixDBQuery::PDO_fetch('SELECT parmvalue FROM globals WHERE parameter = \'ISD_AGENT\'', array(), PDO::FETCH_COLUMN) == '')
        {
            // set the ISD_AGENT global before proceeding with export       
            DatixDBQuery::PDO_query('DELETE FROM globals WHERE parameter = \'ISD_AGENT\'');       
            DatixDBQuery::PDO_query('INSERT INTO globals (parameter, parmvalue) VALUES (\'ISD_AGENT\', :agent)', array('agent' => $_GET['agent']));       
        }
        
        $exporter = new Export_ISD(new Files_CSV('isd_export'));
        if (GetParm('ISD_AGENT', '', true) == '')
        {
            // need to set this global before export
            $exporter->displayAgentDialogue();    
        }
        elseif ($_POST['isd_year']  == '' || $_POST['isd_edition'] == '')
        {
            // users need to submit month/year/edition values before running the ISD export
            $exporter->displayExportDialogue();       
        }
        else
        {
            $exporter->doExport($_POST['isd_month'], $_POST['isd_year'], $_POST['isd_edition']);
        }        
    }
    
    /**
    * Outputs ISD export errors in CSV format.
    */
    public function getISDExportError()
    {
        $csv = new Files_CSV('isd_export_errors');
        $csv->setData($_SESSION['isdErrors']);
        $csv->export();
    }
    
    /**
    * Outputs config files (SQLErrors, form designs etc).
    */
    public function exportConfigFile()
    {
        // extract name and extension, and create new file object
        $ext = \UnicodeString::substr(\UnicodeString::strrchr($this->Parameters['file'], '.'), 1);
        $filename = \UnicodeString::substr($this->Parameters['file'], 0, \UnicodeString::strlen($this->Parameters['file']) - (\UnicodeString::strlen($ext) + 1));
        
        switch ($ext)
        {
            case 'xml':
                $file = new Files_XML($filename);
                break;
            
            default:
                $file = new Files_TXT($filename, 'php');
                break;
        }
        
        // check for admin user 
        LoggedIn();
        if (!$_SESSION["AdminUser"])
        {
            $file->setExportError('Error');
            $file->failExport();
        }
        
        // whitelist the file
        if (!in_array($this->Parameters['file'], array('index.php', 'config.xml', 'SQLErrors.xml')) && \UnicodeString::substr($this->Parameters['file'], 0, 4) != 'User')
        {
            $file->setExportError('Error');
            $file->failExport();
        }
        
        // add directory to filename
        if ($this->Parameters['file'] != 'index.php')
        {
            $this->Parameters['file'] = $GLOBALS['ClientFolder'] . '/' . $this->Parameters['file'];
        } 
        
        // export the file
        try
        {
            $file->setContentsFromFile($this->Parameters['file']);    
        }
        catch (FileNotFoundException $e)
        {
            $file->setExportError($e->getMessage());
            $file->failExport();       
        }

        $file->export();   
    }
    
    public function exportChartPdf()
    {
        global $ClientFolder;
        
        $file = str_replace('ExportedImages', $ClientFolder, Sanitize::SanitizeFilePath($_GET['file']));
        $pdf = new Files_PDF($file);
        $pdf->setContentsFromFile($file);
        $pdf->delete();
        $pdf->export();   
    }
}
