<?php
class Service_BatchMerge extends Service_Service
{
    /**
    * @desc Displays a screen that allows users to confirm the deletion and choose whther documents will be merged
    *
    * @param array $Parameters Array of parameters.
    * @param string $Parameters['module'] Module we are working in.
    */
    public function CollectParameters($Parameters)
    {
        global $dtxtitle, $yySetLocation, $scripturl, $ModuleDefs;

        $this->VerifyBatchMergeCredentials($Parameters);

        $dtxtitle = 'Batch Merge';

        GetPageTitleHTML(array(
             'title' => $dtxtitle,
             'module' => $Parameters['module']
             ));
        GetSideMenuHTML(array('module' => $Parameters['module']));
        template_header_nopadding();

        echo GetSessionMessages();

        //Build WHERE Clause for flagged records.
        foreach($_SESSION[$Parameters['module']]['RECORDLIST']->FlaggedRecords as $FlaggedRecord)
        {
            if ($FlaggedRecord->Data['recordid'])
            {
                $FlaggedRecordIDs[] = $FlaggedRecord->Data['recordid'];
            }
            else
            {
                $FlaggedRecordIDs[] = $FlaggedRecord;
            }
        }
        if (is_array($FlaggedRecordIDs))
        {
            $_SESSION[$Parameters['module']]['BATCH_MERGE_WHERE'] = $ModuleDefs[$Parameters['module']]['TABLE'] . ".recordid IN (" . implode(",", $FlaggedRecordIDs) . ")";
        }
        else
        {
            AddSessionMessage("ERROR", _tk('no_selected_records'));
            $yySetLocation = $_SESSION[$Parameters['module']]["SEARCHLISTURL"];
            redirectexit();
        }
        $_SESSION[$Parameters['module']]['BATCH_MERGE_FLAGGED_IDS'] = $FlaggedRecordIDs;
        $WhereClause = $_SESSION[$Parameters['module']]['BATCH_MERGE_WHERE'];
        $WhereClause = MakeSecurityWhereClause($WhereClause, $Parameters['module'], $_SESSION['initials']);
        $NumRecords = DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM '.$ModuleDefs[$Parameters['module']]['TABLE'].($WhereClause ? ' WHERE '.$WhereClause : ''), array(), PDO::FETCH_COLUMN);
        $NumRecords = filter_var($NumRecords, FILTER_SANITIZE_NUMBER_INT);


        echo '<form method="POST" action="'.$scripturl.'?service=batchmerge&event=executefrompost&module='.$Parameters['module'].'">';

        echo '<div class="padded_div">';

        $this->MergeOptionsSection($Parameters['module']);

        echo '</br>';

        echo '
        <div class="button_wrapper">
            <input type="button" class="button" value="Merge '.$NumRecords.' records now" onclick="if(confirm(\'This action will merge '.$NumRecords.' records. Are you sure you want to do this?\')){this.form.submit()}">
            <input type="button" class="button" value="'._tk('btn_cancel').'" onclick="SendTo(\''.$scripturl.'?action=home&module='.$Parameters['module'].'\');">
        </div>';

        echo '</form>';

        footer();
        obExit();
    }

    /**
    * @desc Outputs Merge options section for Merge options form.
    */
    private function MergeOptionsSection($module) {

        global $ModuleDefs;

        $CTable = new FormTable();
        $FieldObj = new FormField();
        $CTable->MakeTitleRow('<b>' . _tk('merge_options') . '</b>');

        if ($error)
            $CTable->Contents .= '<font color="red"><b>' . $error["message"] . '</b></font><br /><br />';

        $CTable->MakeRow("<b>" . _tk('merge_dest_id') . "</b>", $FieldObj->MakeNumberField("merge_dest_id", "", 5));
        $CTable->MakeRow("<b>" . _tk('merge_documents') . "</b>", $FieldObj->MakeDivCheckbox("merge_documents", "", true));
        $CTable->MakeRow("<b>" . _tk('merge_linked_records') . "</b>", $FieldObj->MakeDivCheckbox("merge_linked_records", "", true));
        $CTable->MakeRow("<b>" . _tk('delete_after_merge') . "</b>", $FieldObj->MakeDivCheckbox("delete_after_merge", "", true));

        $CTable->MakeTable();

        echo '
            <tr>
                <td>' . $CTable->GetFormTable() . '</td>
            </tr>
        ';
    }

    /**
    * @desc Checks that the current user is permitted to run the batch merge operation.
    * @param array $Parameters Array of parameters.
    * @param string $Parameters['module'] Module we are working in.
    */
    protected function VerifyBatchMergeCredentials($Parameters)
    {
        LoggedIn();

        if (!(IsFullAdmin() || bYN(GetParm('CON_ALLOW_MERGE_DUPLICATES'))) || !$Parameters['module'] || !ModIsLicensed($Parameters['module']))
        {
            $yylocation = $scripturl;
            redirectexit();
        }

    }

    /**
    * @desc Executes a batch update operation using data passed from the setup form in the $_POST variable.
    * @param array $Parameters Array of parameters.
    * @param string $Parameters['module'] Module we are working in.
    */
    public function ExecuteFromPost($Parameters)
    {
        global $yylocation, $scripturl;

        $this->VerifyBatchMergeCredentials($Parameters);

        //Check that record selected for merge into is not part of the list of records to merge FROM.
        if (is_array( $_SESSION[$Parameters['module']]['BATCH_MERGE_FLAGGED_IDS']) &&
            in_array($_POST['merge_dest_id'], $_SESSION[$Parameters['module']]['BATCH_MERGE_FLAGGED_IDS']))
        {
            AddSessionMessage("ERROR", _tk('merge_id_validation'));
            $this->CollectParameters($Parameters);
        }

        //Check that destination ID has been entered.
        if (empty($_POST['merge_dest_id']))
        {
            AddSessionMessage("ERROR", _tk('merge_id_missing'));
            $this->CollectParameters($Parameters);
        }

        //Check that destination ID exists.
        $DestRecordExists = $this->CheckRecordExists($Parameters['module'], $_POST['merge_dest_id']);
        if ($DestRecordExists == false)
        {
            AddSessionMessage("ERROR", _tk('merge_id_not_exist'));
            $this->CollectParameters($Parameters);
        }

        $Parameters['merge_dest_id'] = Sanitize::SanitizeInt($_POST['merge_dest_id']);
        $Parameters['merge_documents'] = ($_POST['merge_documents'] == "Y");
        $Parameters['merge_linked_records'] = ($_POST['merge_linked_records'] == "Y");
        $Parameters['delete_after_merge'] = ($_POST['delete_after_merge'] == "Y");

        $this->ExecuteAndReport($Parameters);
    }


    /**
    * @desc Executes the batch update operation based on parameters recieved.
    *
    * @param array $Parameter Array of parameter values.
    * @param string $Parameters['module'] Module we are working in.
    * @param array $Parameter['merge_docs'] If true, then linked documents will be merged. Otherwise only the links will be removed.
    */
    public function Execute($Parameters)
    {
        $this->VerifyBatchMergeCredentials($Parameters);

        $RecordList = new RecordLists_ModuleRecordList();
        $RecordList->Module = $Parameters['module'];
        $RecordList->Paging = false;
        $RecordList->Columns[] = new Listings_ListingColumn('rep_approved', 'contacts_main');
        $RecordList->WhereClause = MakeSecurityWhereClause($_SESSION[$Parameters['module']]['BATCH_MERGE_WHERE'], $Parameters['module'], $_SESSION['initials']);
        $Result = $RecordList->BatchMerge($Parameters);

        return $Result;
    }


    /**
    * @desc Executes the batch update operation based on parameters recieved and then outputs the result to the browser.
    * Can be called from POST via ExecuteFromPost(), or from within the application.
    *
    * @param array $Parameter Array of parameter values.
    * @param string $Parameters['module'] Module we are working in.
    * @param array $Parameter['merge_docs'] If true, then linked documents will be merged. Otherwise only the links will be removed.
    */
    protected function ExecuteAndReport($Parameters)
    {
        global $dtxtitle, $yylocation, $scripturl, $ModuleDefs;

        $this->VerifyBatchMergeCredentials($Parameters);

        $Result = $this->Execute($Parameters);

        $dtxtitle = 'Batch merge: Results';

        GetPageTitleHTML(array(
             'title' => $dtxtitle,
             'module' => $Parameters['module']
             ));
        GetSideMenuHTML(array('module' => $Parameters['module']));
        template_header_nopadding();

        echo '<div class="padded_div">';

        if (count($Result['merged']) > 0)
        {
            echo count($Result['merged']).' records have been merged into record '. $Parameters['merge_dest_id'] . '.';

            if (count($Result['not_merged']) > 0)
            {
                echo 'Some records could not be merged ('.implode(', ', $Result['not_merged']).')';
            }
        }
        else if (count($Result['merged']) == 0)
        {
            echo 'Problems occurred during this process. No records were merged.';
        }

        echo '</div>';

        echo '<div class="button_wrapper">';
        echo '<input TYPE="button" VALUE="'. _tk('btn_goto_merged_record') .'" name="btnGotoMerged" onclick="SendTo(\''.$scripturl. '?action=' . $ModuleDefs[$Parameters['module']]['ACTION'] . '&fromsearch=1&recordid=' . $Parameters['merge_dest_id'] . '\');" />';
        echo '</div>';

        footer();
        obExit();

    }

    /**
    * Deletes links that would lead to a duplicate being created when the update to the new contact ID is carried out.
    * (Ported from Rich client)
    *
    * @param int $ModLinkIDField Link field for module
    * @param string $Link_contact_table Link table name
    * @param string $Link_contact_keyfield Contact link ID field
    * @param int $FromConID Source contact ID
    * @param int $ToConID Destination contact ID
    * @return bool
    */
    static public function DeleteLinkContactsForMod($ModLinkIDField, $Link_contact_table, $Link_contact_keyfield, $FromConID, $ToConID)
    {

        $sql = 'DELETE FROM ' . $Link_contact_table . '
                    WHERE ' . $Link_contact_keyfield . ' IN (SELECT L1.' . $Link_contact_keyfield .
                                                            ' FROM ' . $Link_contact_table . ' L1, ' . $Link_contact_table . ' L2
                                                                WHERE L1.con_id = ' . $FromConID . ' AND L2.con_id = ' . $ToConID . '
                                                                AND L1.' . $ModLinkIDField . ' = L2.' . $ModLinkIDField . ' AND L2.link_type = L1.link_type
                                                                AND L1.' . $ModLinkIDField . ' > 0 )';

        return DatixDBQuery::PDO_query($sql);

    }
    /**
    * Performs check if record exists for the main record to be merged into
    *
    * @param string $module Module short name
    * @param int $recordid Record id for main table for module.
    */
    private function CheckRecordExists($module, $recordid)
    {
        global $ModuleDefs;

        $CountData = DatixDBQuery::PDO_fetch('SELECT count(*) as RecordCount FROM ' . $ModuleDefs[$module]['TABLE'] . ' WHERE recordid = :recordid',
                                array('recordid' => $recordid));

        if ($CountData["RecordCount"] > 0)
        {
            return true;
        }

        return false;
    }

    /**
    * Merges/Transfers all related data for records selected in this list from the database.
    *
    * @param bool $aParam["merge_dest_id"] Destination ID.
    * @param bool $aParam["merge_documents"] If true, the documents linked to this record will be transfered.
    * @param bool $aParam["merge_linked_records"] If true, linked records will be transferred to the destination record.
    * @param bool $aParam["delete_after_merge"] If true, the source records will be deleted after the transfer.
    * @param RecordLists_ModuleRecordList $ModRecordList Record List with selected records to merge.
    */
    public static function BatchMerge($aParam, $ModRecordList)
    {
        global $ModuleDefs;

        $DestID = $aParam["merge_dest_id"];
        $ModRecordList->RetrieveRecords();

        $NoErrors = true;

        //If we're merging contacts, we need to check whether any contacts are approved so we can make the target contact approved too.
        $ApprovedContact = false;

        foreach($ModRecordList->Records as $Record)
        {
            if ($ModRecordList->Module == 'CON' && $Record->Data['rep_approved'] == 'FA')
            {
                $ApprovedContact = true;
            }

            DatixDBQuery::static_beginTransaction();

            $SrcID = $Record->Data['recordid'];

            if($ModuleDefs[$ModRecordList->Module]['CAN_LINK_DOCS'])
            {
                if($aParam['merge_documents'])
                {
                    //Transfer Documents
                    $DocsToMerge = DatixDBQuery::PDO_fetch_all('SELECT recordid, doc_id from documents_main WHERE '.$ModuleDefs[$ModRecordList->Module]['FK'].' = '.$Record->Data['recordid']);

                    foreach($DocsToMerge as $DocDetails)
                    {

                        try
                        {
                            $NoErrors = DatixDBQuery::PDO_query("UPDATE documents_main SET " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = $DestID WHERE recordid = ".$DocDetails['recordid']);
                        }
                        catch (DatixDBQueryException $e)
                        {
                            $NoErrors = false;
                        }

                        InsertFullAuditRecord($ModRecordList->Module, $DocDetails['recordid'], 'UPDATE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] (documents_main)');
                    }
                }
            }

            if($NoErrors && $aParam['merge_linked_records'])
            {
                if ($ModRecordList->Module = 'CON')
                {
                    $LinkFKeyfields = array('inc_id', 'cla_id', 'com_id', 'ram_id', 'pal_id', 'rfi_id', 'stn_id', 'inq_id', 'sab_id');
                    foreach($LinkFKeyfields as $FKeyfield)
                    {
                        try
                        {
                            //The following deletes are required for removing links that would lead to a duplicate being created when the update to the new contact ID is carried out.
                            $NoErrors = Service_BatchMerge::DeleteLinkContactsForMod($FKeyfield, 'link_contacts', 'link_recordid', $SrcID, $DestID);
                        }
                        catch (DatixDBQueryException $e)
                        {
                            $NoErrors = false;
                            continue;
                        }
                    }

                    //special case of the above for SABs
                    try
                    {
                        $NoErrors = Service_BatchMerge::DeleteLinkContactsForMod('sab_id', 'sab_link_contacts', 'recordid', $SrcID, $DestID);
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                        continue;
                    }

                    try
                    {
                        //The following deletes are required for removing links that would lead to a duplicate being created when the update to the new contact ID is carried out.
                        $sql = 'DELETE FROM link_compl
                                WHERE recordid IN (SELECT L1.recordid
                                                    FROM link_compl L1,link_compl L2
                                                                WHERE L1.con_id = :SrcID AND L2.con_id = :DestID
                                                                AND L1.com_id = L2.com_id
                                                                AND L1.com_id > 0 )';
                        $NoErrors = DatixDBQuery::PDO_query($sql, array("SrcID" => $SrcID, "DestID" => $DestID));
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                        continue;
                    }
                }

                if ($NoErrors)
                {
                    try
                    {
                        $NoErrors = DatixDBQuery::PDO_query("UPDATE link_contacts SET " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = $DestID WHERE " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = ". $SrcID);
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                    }
                }

                InsertFullAuditRecord($ModRecordList->Module, $SrcID, 'UPDATE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] (link_contacts)');

                if ($NoErrors)
                {
                    //Update age and age bands for link_contact records
                    $LinkContacts = DatixDBQuery::PDO_fetch_all('SELECT link_recordid, con_id, inc_id, cla_id, com_id, ram_id, pal_id, rfi_id, inq_id, crs_id, con_dob
                                                                   FROM link_contacts, contacts_main where link_contacts.con_id = contacts_main.recordid AND ' .
                                                                   $ModuleDefs[$ModRecordList->Module]['FK'] . ' = '. $DestID, array());

                    foreach ($LinkContacts as $LinkCon)
                    {
                        foreach ($ModuleDefs as $ModDef)
                        {
                            $ModFKey = $ModDef['FK'];
                            //Check if age at date specified
                            if(isset($ModDef['AGE_AT_DATE']) && !empty($LinkCon[$ModFKey]))
                            {
                                $Age = getAgeAtDateForModule($ModDef['CODE'], $LinkCon[$ModFKey], $LinkCon['con_dob'], $Age);
                            }
                        }

                        if (!empty($Age))
                        {
                            $AgeBand = getAgeBand($Age);
                            $sql = 'update link_contacts set link_age = :Age, link_age_band = :AgeBand where link_recordid = :Link_recordid';

                            try
                            {
                                $NoErrors = DatixDBQuery::PDO_query($sql, array('Age' => $Age, 'AgeBand' => $AgeBand, 'Link_recordid' => $LinkCon['link_recordid']));
                            }
                            catch (DatixDBQueryException $e)
                            {
                                $NoErrors = false;
                            }
                        }
                    }
                }

                //Update linked complainant data
                if ($NoErrors)
                {
                    try
                    {
                        $NoErrors = DatixDBQuery::PDO_query("UPDATE link_compl SET " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = $DestID WHERE " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = ". $SrcID);
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                    }

                    InsertFullAuditRecord($ModRecordList->Module, $SrcID, 'UPDATE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] (link_compl)');
                }

                //Update linked sab data
                if ($NoErrors)
                {
                    try
                    {
                        $NoErrors = DatixDBQuery::PDO_query("UPDATE sab_link_contacts SET " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = $DestID WHERE " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = ". $SrcID);
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                    }

                    InsertFullAuditRecord($ModRecordList->Module, $SrcID, 'UPDATE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] (sab_link_contacts)');
                }

                //Update linked injury data
                if ($NoErrors)
                {
                    try
                    {
                        $NoErrors = DatixDBQuery::PDO_query("UPDATE inc_injuries SET " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = $DestID WHERE " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = ". $SrcID);
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                    }

                    InsertFullAuditRecord($ModRecordList->Module, $SrcID, 'UPDATE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] (inc_injuries)');
                }
                
                //Update linked property data
                if ($NoErrors)
                {
                    try
                    {
                        $NoErrors = DatixDBQuery::PDO_query("UPDATE inc_personal_property SET " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = $DestID WHERE " . $ModuleDefs[$ModRecordList->Module]['FK'] . " = ". $SrcID);
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                    }

                    InsertFullAuditRecord($ModRecordList->Module, $SrcID, 'UPDATE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] (inc_personal_property)');
                }

                //Update linked dashboard data
                if ($NoErrors)
                {
                    try
                    {
                        $NoErrors = DatixDBQuery::PDO_query("UPDATE reports_dashboards SET dash_owner_id = $DestID WHERE dash_owner_id = ". $SrcID);
                    }
                    catch (DatixDBQueryException $e)
                    {
                        $NoErrors = false;
                    }

                    InsertFullAuditRecord($ModRecordList->Module, $SrcID, 'UPDATE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] (reports_dashboards)');
                }

            }

            if($NoErrors && $aParam['delete_after_merge'])
            {
                //Delete main record
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM '.$ModRecordList->getTable().' WHERE recordid = '.$Record->Data['recordid']);
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }

                InsertFullAuditRecord($ModRecordList->Module, $SrcID, 'DELETE', 'WEB: Batch Transfer ['.$ModRecordList->Module.'] ('.$ModRecordList->getTable().')');
            }

            // If we've had any errors, then don't commit this deletion, and mark the record as one that hasn't been deleted.
            // Otherwise commit the change.
            if($NoErrors)
            {
                DatixDBQuery::static_commit();
                $result['merged'][] = $Record->Data['recordid'];
            }
            else
            {
                DatixDBQuery::static_rollBack();
                $result['not_merged'][] = $Record->Data['recordid'];
            }
        }

        if ($ModRecordList->Module == 'CON' && $ApprovedContact)
        {
            DatixDBQuery::PDO_query('UPDATE contacts_main SET rep_approved = \'FA\' WHERE recordid = :recordid', array('recordid' => $DestID));
        }

        return $result;
    }
}
