<?php

class Service_BatchUpdate extends Service_Service
{
    /**
     * @desc Displays a screen that allows users to choose which fields should be updated
     *
     * @param array $Parameters Array of parameters.
     * @throws RequiredParameterMissingException
     *
     * @codeCoverageIgnoreStart
     * No unit test, since it deals with constructing HTML
     */
    public function CollectParameters($Parameters)
    {
        global $dtxtitle, $yySetLocation, $scripturl, $ModuleDefs;

        if (!$Parameters['module'])
        {
            throw new RequiredParameterMissingException('No module provided.');
        }

        $this->VerifyBatchUpdateCredentials($Parameters);

        //These modules require that records are selected with checkboxes
        if (in_array($Parameters['module'], array('ATM', 'AMO', 'ATI', 'ATQ', 'AQU')) &&
            (!isset($_SESSION[$Parameters['module']]['RECORDLIST']) || empty($_SESSION[$Parameters['module']]['RECORDLIST']->FlaggedRecords)))
        {
            AddSessionMessage('ERROR', 'You must select at least one record to update');
            $yySetLocation = $scripturl . '?action=list&module='. $Parameters['module'];
            redirectexit();
        }

        $dtxtitle = 'Batch Update';

        GetPageTitleHTML(array(
            'title' => $dtxtitle,
            'module' => $Parameters['module']
        ));

        GetSideMenuHTML(array('module' => $Parameters['module']));
        template_header_nopadding();

        //to prevent additional searches within the same session from affecting this operation.
        $_SESSION[$Parameters['module']]['BATCH_UPDATE_WHERE'] = $_SESSION[$Parameters['module']]['WHERE'];

        $_SESSION[$Parameters['module']]['BATCH_UPDATE_REFERRER'] = $_SESSION['LAST_PAGE'];

        $WhereClause = $_SESSION[$Parameters['module']]['BATCH_UPDATE_WHERE'];

        // Replace @prompt on Where clause
        require_once 'Source/libs/SavedQueries.php';
        $WhereClause = ReplacePrompts($WhereClause, $Parameters['module']);

        //Apply current user's security settings to the records selected.
        $WhereClause = MakeSecurityWhereClause($WhereClause, $Parameters['module'], $_SESSION['initials']);

        //Apply any checkbox-selected record filters
        if ($_SESSION[$Parameters['module']]['RECORDLIST'] && !empty($_SESSION[$Parameters['module']]['RECORDLIST']->FlaggedRecords))
        {
            if ($WhereClause)
            {
                $WhereClause = '('.$WhereClause.') AND ';
            }

            $WhereClause .= 'recordid IN ('.implode(', ', $_SESSION[$Parameters['module']]['RECORDLIST']->FlaggedRecords).')';
        }

        $recordsToUpdate = DatixDBQuery::PDO_fetch_all('SELECT recordid as num FROM ' . ($ModuleDefs[$Parameters['module']]['VIEW'] ? $ModuleDefs[$Parameters['module']]['VIEW'] : $ModuleDefs[$Parameters['module']]['TABLE']) . ($WhereClause ? ' WHERE '.$WhereClause : ''), array(), PDO::FETCH_COLUMN);
        $NumRecords = count($recordsToUpdate);

        $_SESSION[$Parameters['module']]['BATCH_UPDATE_RECORDS'] = $recordsToUpdate;

        //On submission, this data will be pushed directly to the batch update operation
        echo '<form method="POST" action="'.$scripturl.'?service=batchupdate&event=executefrompost&module='.$Parameters['module'].'">';

        //holds the number of batch operations that will be performed.
        echo '<input type="hidden" name="max_suffix" id="max_suffix" value="0">';

        echo '<div class="padded_div">';
        echo '<div>This process will update any chosen fields in these records. This operation cannot be undone. If you do not wish perform this operation, please press "cancel" now.</div>';
        echo '<div>
            <table id="batch_update_table">
                <tr>
                    <th>Field</th>
                    <th width="250px">Old value</th>
                    <th width="250px">New value</th>
                </tr>';

        echo $this->getBatchUpdateRow($Parameters['module'], '0');

        echo '
                <tr>
                    <td colspan="3">
                        <input type="button" value="'._tk('btn_add_another').'" onClick="jQuery(\'#max_suffix\').val(parseInt(jQuery(\'#max_suffix\').val()) + 1);AddAnotherBatchUpdateRow(\''.$Parameters['module'].'\', jQuery(\'#max_suffix\').val())">
                    </td>
                </tr>
            </table>
        </div>';

        echo '<div>You are about to attempt to update '.Sanitize::SanitizeInt($NumRecords).' records</div></div>';

        echo '
        <div class="button_wrapper">
            <input type="button" class="button" value="Update '.$NumRecords.' records now" onclick="if(checkBatchUpdateInfo()){ if(window.confirm(\'Are you sure you want to perform this action?\')) {this.form.submit()}}">
            <input type="button" class="button" value="'._tk('btn_cancel').'" onclick="SendTo(\''.$scripturl.'?action=home&module='.Sanitize::getModule($Parameters['module']).'\');">
        </div>';

        echo '</form>';

        footer();
        obExit();
    }

    /**
     * @desc Executes a batch update operation using data passed from the setup form in the $_POST variable.
     *
     * @param array $Parameters Array of parameters.
     */
    public function ExecuteFromPost($Parameters)
    {
        global $yySetLocation, $scripturl;

        $this->VerifyBatchUpdateCredentials($Parameters);

        //loops through the different batch operations that have been submitted.
        for ($i = 0; $i <= intval($_POST['max_suffix']); $i++)
        {
            if ($_POST['field_select_'.$i] && ($_POST[$_POST['field_select_'.$i].'_'.$i.'_old'] || $_POST[$_POST['field_select_'.$i].'_'.$i.'_new']))
            {
                $Parameters['batch_operations'][] = array('field' => $_POST['field_select_'.$i], 'old' => $_POST[$_POST['field_select_'.$i].'_'.$i.'_old'], 'new' => $_POST[$_POST['field_select_'.$i].'_'.$i.'_new']);
            }
        }

        if (empty($Parameters['batch_operations']))
        {
            AddSessionMessage('ERROR', _tk('batch_update_error'));
            $yySetLocation = $scripturl.'?service=batchupdate&event=collectparameters&module='.$Parameters['module'];
            redirectexit();
        }

        //if rep_approved is one of the fields being updated we need to check that the transition is valid
        foreach ($Parameters['batch_operations'] as $Operation)
        {
            if ($Operation['field'] == 'rep_approved' && !checkApprovalStatusTransferLegitimate(array('from' => $Operation['old'], 'to' => $Operation['new'], 'module' => $Parameters['module'], 'perm' => GetAccessLevel($Parameters['module']))))
            {
                AddSessionMessage('ERROR', 'The approval status transition you defined is not permitted.');
                $yySetLocation = $scripturl.'?service=batchupdate&event=collectparameters&module='.$Parameters['module'];
                redirectexit();
            }
        }

        $this->ExecuteAndReport($Parameters);
    }

    // @codeCoverageIgnoreEnd

    /**
     * @desc Checks that the current user is permitted to run the batch update operation.
     *
     * @param array $Parameters Array of parameters.
     */
    protected function VerifyBatchUpdateCredentials($Parameters)
    {
        LoggedIn();

        if (!bYN(GetParm('ENABLE_BATCH_UPDATE', 'N')) || !$Parameters['module'] || !ModIsLicensed($Parameters['module']))
        {
            $yylocation = $scripturl;
            redirectexit();
        }
    }

    /**
     * @desc Executes the batch update operation based on parameters received.
     *
     * @param array $Parameter Array of parameter values.
     * @param string $Parameters['module'z] Module we are working in.
     * @param array $Parameter['batch_operations'] An array containing the batch operations to run, in the form array('field' => field to update, 'old' => old value, 'new' => new value).
     * @return array Returns array with following keys:
     *  * updatedRecords: an array of unique record ids updated in the process
     *  * updates: an array of results for each batch operation performed. Each result is an array containing the following keys:
     *   - success: Whether the operation was successful or not (boolean)
     *   - field: The name of the field updated in the operation (string)
     *   - numRecords: The number of records updated in the operation
     *   - oldValue: The old value of the updated field
     *   - newValue: The new value of the updated field
     */
    protected function Execute($Parameters)
    {
        global $FieldDefs, $ModuleDefs;

        $return = array(
            'updatedRecords' => array(),
            'updates' => array()
        );
        //Record which records were updated by the batch update
        $updatedRecords = array();


        // loops through the different batch operations that have been submitted.
        // i.e. each of the fields to be updated
        foreach ($Parameters['batch_operations'] as $UpdateDetails) {

            $FieldToUpdate = new Fields_Field($UpdateDetails['field'], ($ModuleDefs[$Parameters['module']]['VIEW'] ? $ModuleDefs[$Parameters['module']]['VIEW'] : $ModuleDefs[$Parameters['module']]['TABLE']));
            //Session value originally set on the Medications Search Listing page
            //Pass the IDs of the records to be batch updated into a temporary var which will be spliced
            $recordsToUpdateTemp = $_SESSION[$Parameters['module']]['BATCH_UPDATE_RECORDS'];

            $numRecordsUpdated = 0;
            $successfulUpdate = null;

            // If there are more than 2100 records that need to be updated, we need to break the batch up so that
            // PDO can digest all the records peicemeal
            // See https://datixltd.atlassian.net/browse/DW-8034
            while($recordsToUpdate = array_splice($recordsToUpdateTemp, 0, 2000)) {

                //This if is just in case the spliced array is empty, there's no need to do another update query
                if(count($recordsToUpdate)) {

                    $recordsWhereClause = 'recordid IN (' . \UnicodeString::trim(str_repeat('?,', count($recordsToUpdate)), ',') . ')';

                    $PDOArray = array();
                    $Result = true;

                    if ($FieldDefs[$Parameters['module']][$FieldToUpdate->getName()]['Type'] == 'multilistbox') //multicode fields need to be treated differently.
                    {
                        MakeFieldWhere($Parameters['module'], $FieldToUpdate->getName(), $UpdateDetails['old'], $FieldWhereClause, '');

                        $WhereClause = ($FieldWhereClause ? $FieldWhereClause . ' AND ' : '') . $recordsWhereClause;
                        $PDOArray = array_merge($PDOArray, $recordsToUpdate);

                        $RecordsToChange = DatixDBQuery::PDO_fetch_all('SELECT recordid, ' . $FieldToUpdate->getName() . ' FROM ' . $FieldToUpdate->getTable() . ' WHERE (' . $WhereClause . ')', $PDOArray);

                        foreach ($RecordsToChange as $Record) {
                            if ($UpdateDetails['old'] == '') {
                                $newValue = ($Record[$FieldToUpdate->getName()] ? $Record[$FieldToUpdate->getName()] . ' ' . $UpdateDetails['new'] : $UpdateDetails['new']);
                            } else {
                                $newValue = str_replace($UpdateDetails['old'], $UpdateDetails['new'], $Record[$FieldToUpdate->getName()]);
                            }

                            $SingleResult = DatixDBQuery::PDO_query('UPDATE ' . $FieldToUpdate->getTable() . ' SET ' . $FieldToUpdate->getName() . ' = :newValue WHERE recordid = :recordid', array('recordid' => $Record['recordid'], 'newValue' => $newValue));

                            if (!$SingleResult) {
                                $Result = false;
                            }
                        }

                        $numRecordsUpdated = count($RecordsToChange);
                    } else {
                        if ($UpdateDetails['old']) {
                            MakeFieldWhere($Parameters['module'], $FieldToUpdate->getName(), '?', $FieldWhereClause, '', array('?'));

                            if ($FieldToUpdate->getFieldType() == 'D') {
                                $PDOArray[] = UserDateToSQLDate($UpdateDetails['old']);
                            } else if (in_array($FieldToUpdate->getName(), GetAllFieldsByType($Parameters['module'], 'time'))) {
                                $timeWithNoColon = str_replace(':', '', $UpdateDetails['old']);
                                $PDOArray[] = $timeWithNoColon;
                                $PDOArray[] = substr($timeWithNoColon, 0, 2) . ':' . substr($timeWithNoColon, 2);
                            } elseif ($UpdateDetails['old']) {
                                $PDOArray[] = $UpdateDetails['old'];
                            }
                        } else {
                            MakeFieldWhere($Parameters['module'], $FieldToUpdate->getName(), '=', $FieldWhereClause, '');
                        }

                        $PDOArray = array_merge($PDOArray, $recordsToUpdate);
                        $WhereClause = $FieldWhereClause . ' AND ' . $recordsWhereClause;

                        $RecordsToChange = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM ' . $FieldToUpdate->getTable() . ' WHERE (' . $WhereClause . ')', $PDOArray, PDO::FETCH_COLUMN);
                        $numRecordsUpdatedTemp = count($RecordsToChange); //Needs to be seperate var due to the evaulation below
                        $numRecordsUpdated += $numRecordsUpdatedTemp;

                        if ($numRecordsUpdatedTemp > 0) {
                            $sql = 'UPDATE ' . $FieldToUpdate->getTable() . ' SET ' . $FieldToUpdate->getName() . ' = ? WHERE (' . $WhereClause . ')';

                            if ($FieldToUpdate->getFieldType() == 'D') {
                                array_unshift($PDOArray, UserDateToSQLDate($UpdateDetails['new']));
                            } else {
                                if ($UpdateDetails['new']) {
                                    array_unshift($PDOArray, $UpdateDetails['new']);
                                } else {
                                    array_unshift($PDOArray, null);
                                }
                            }
                            $Result = DatixDBQuery::PDO_query($sql, $PDOArray);
                        }
                    }

                    if ($Result) {
                        $updatedRecords = array_merge($RecordsToChange, $updatedRecords);
                        // Update record last updated
                        $loader = new \src\framework\controller\Loader();
                        $controller = $loader->getController(['controller' => 'src\\generic\\controllers\\RecordController']);

                        foreach ($recordsToUpdate as $recordId) {
                            $controller->setRequestParameter('module', $this->Parameters['module']);
                            $controller->setRequestParameter('recordId', $recordId);
                            $controller->doAction('updateRecordLastUpdated');
                        }
                    }

                    //Only say the update has been successful if it hasn't failed in one of the prevous splices of the batch operation
                    if ( $Result && $successfulUpdate !== false ) {
                        $successfulUpdate = true;
                    } else {
                        $successfulUpdate = false;
                    }
                }
            }

            //provide feedback to the user on how each batch operation went
            $update = array(
                'field' => Labels_FieldLabel::GetFieldLabel($FieldToUpdate->getName(), '', $FieldToUpdate->getTable()),
                'numRecords' => $numRecordsUpdated,
                'success' => $successfulUpdate,
                'oldValue' => code_descr($FieldToUpdate->getModule(), $FieldToUpdate->getName(), $UpdateDetails['old'], $FieldToUpdate->getTable()),
                'newValue' => code_descr($FieldToUpdate->getModule(), $FieldToUpdate->getName(), $UpdateDetails['new'], $FieldToUpdate->getTable())
            );

            $return['updates'][] = $update;
        }

        // We need to make sure that the updated record ids are unique and we cannot use array_unique because it
        // doesn't work on multi dimensional arrays.
        foreach ($updatedRecords as $record)
        {
            $updatedRecordExists = false;

            foreach ($return['updatedRecords'] as $updatedRecord)
            {
                if ($updatedRecord['recordid'] == $record['recordid'])
                {
                    $updatedRecordExists = true;
                    break;
                }
            }

            if (!$updatedRecordExists)
            {
                $return['updatedRecords'][] = $record;
            }
        }

        return $return;
    }

    /**
     * @desc Executes the batch update operation based on parameters received and then outputs the result to the browser.
     *
     * @param array $Parameter Array of parameter values.
     * @param string $Parameters['module'] Module we are working in.
     * @param array $Parameter['batch_operations'] An array containing the batch operations to run, in the form array('field' => field to update, 'old' => old value, 'new' => new value).
     */
    protected function ExecuteAndReport($Parameters)
    {
        global $dtxtitle, $yylocation, $scripturl, $ModuleDefs, $FieldDefs;

        $this->VerifyBatchUpdateCredentials($Parameters);

        $results = Sanitize::SanitizeRawArray($this->Execute($Parameters));
        $html = '<div class="batch-update-results">';

        if (count($results['updates']) > 0)
        {
            $html .= '<ul>';

            foreach ($results['updates'] as $update)
            {
                if ($update['success'])
                {
                    $html .= '<li><span class="field">' . $update['field'] . ':</span>Updated from <em>"' . $update['oldValue'] . '"</em> to <em>"' . $update['newValue'] . '"</em> on <em>' . $update['numRecords'] . ' record(s)</em>.</li>';
                }
                else
                {
                    $html .= '<li><span class="field"></span>There was an error updating this field.</li>';
                }

            }

            $html .= '</ul></div>';
        }

        AddSessionMessage('INFO', $html);
        header('Location: '.$_SESSION[$Parameters['module']]['BATCH_UPDATE_REFERRER'].'&showinfodiv=1&token='.CSRFGuard::getCurrentToken());
        exit();
    }

    /**
     * @desc Gets the HTML for the "old" and "new" fields for inputting batch update values. Called from AJAX, and echoes the result as a JSON string.
     *
     * @param array $Parameter Array of parameter values.
     * @param string $Parameters['module'] Module we are working in.
     * @param string $Parameters['field'] The field we are looking to update.
     * @param string $Parameters['suffix'] The suffix number of the batch operation, since we can be displaying multiple operations at once.
     */
    public function getFieldHTML_JSON($Parameters)
    {
        global $JSFunctions;

        require_once 'Source/libs/FormClasses.php';

        $OldField = $this->GetFormFieldObjectForBatchUpdate($Parameters['module'], $Parameters['field'], $Parameters['suffix'].'_old');
        $NewField = $this->GetFormFieldObjectForBatchUpdate($Parameters['module'], $Parameters['field'], $Parameters['suffix'].'_new');

        $JSONdata = Sanitize::SanitizeStringArray($_GET);
        $JSONdata['html_old'] = $OldField->getField();
        $JSONdata['html_new'] = $NewField->getField();
        $JSONdata['js'] = $JSFunctions;

        echo json_encode($JSONdata);
    }

    /**
     * @desc Gets a field object for a dropdown containing fields for a particular module.
     *
     * @param string $module The module we are working in.
     * @param string $suffix The suffix number of the batch operation, since we can be displaying multiple operations at once.
     *
     * @return obj Forms_SelectField object containing list of fields for a given module.
     */
    protected function getFieldDropdown($module, $suffix)
    {
        global $FieldDefs, $ModuleDefs;

        $fieldArray = array();

        // Get list of excluded fields
        $sql = "SELECT bex_field
	        FROM batch_exclusions
	        WHERE bex_module = :module";

        $excludedFields = DatixDBQuery::PDO_fetch_all($sql, array('module' => $module),PDO::FETCH_COLUMN);

        if($module == 'CLA')
        {
            //TODO may want some more generic way to stop fields being batch updated other than user configurable
            $excludedFields[] = 'indem_dclosed';
            $excludedFields[] = 'expen_dclosed';
        }

        foreach ($ModuleDefs[$module]['FIELD_ARRAY'] as $field)
        {
            if ((!in_array($field, $excludedFields) || bYN(GetParm("IGNORE_BATCH_UPDATE_SETTINGS","N"))) && !Fields_Field::isFieldCalculated($module, $field))
            {
                $label = Labels_FieldLabel::GetFieldLabel($field);

                if ($label != '')
                {
                    $fieldArray[$field] = $label;
                }
            }
        }

        natcasesort($fieldArray);

        $FieldDropDown = Forms_SelectFieldFactory::createSelectField('field_select', 'ADM', '', '', '', '', '', '', $suffix);
        $FieldDropDown->setCustomCodes($fieldArray);
        $FieldDropDown->setSuppressCodeDisplay();
        $FieldDropDown->setOnChangeExtra('getFieldForBatchUpdate(\''.$module.'\', \'field_select_'.$suffix.'\', '.$suffix.')');

        return $FieldDropDown;
    }

    /**
     * @desc Constructs the HTML for each cell in a table row used to define a batch update operation.
     *
     * @param string $module The module we are working in.
     * @param string $suffix The suffix number of the batch operation, since we can be displaying multiple operations at once.
     *
     * @return array Array of HTML corresponding to each cell in a table row.
     */
    protected function getBatchUpdateRowFields($module, $suffix)
    {
        global $ModuleDefs;

        $FieldDropDown = $this->getFieldDropdown($module, $suffix);

        $RowFields[] = '<div id="field_select_wrapper_'.$suffix.'">'.$FieldDropDown->getField().'</div>';

        $RowFields[] = '<div id="old_field_select_wrapper_'.$suffix.'"></div>';
        $RowFields[] = '<div id="new_field_select_wrapper_'.$suffix.'"></div>';

        return $RowFields;
    }

    /**
     * @desc Constructs the HTML for a row of fields used to define a batch update operation and echos it as JSON.
     *
     * @param array $Parameter Array of parameter values.
     * @param string $Parameters['module'] Module we are working in.
     * @param string $Parameters['suffix'] The suffix number of the batch operation, since we can be displaying multiple operations at once.
     */
    public function getBatchUpdateRow_JSON($Parameters)
    {
        global $JSFunctions;

        $JSONdata = $Parameters;
        $JSONdata['html'] = $this->getBatchUpdateRow($Parameters['module'], $Parameters['suffix']);
        $JSONdata['js'] = $JSFunctions;

        echo json_encode($JSONdata);
    }

    /**
     * @desc Constructs the HTML for a row of fields used to define a batch update operation.
     *
     * @param string $module The module we are working in.
     * @param string $suffix The suffix number of the batch operation, since we can be displaying multiple operations at once.
     */
    protected function getBatchUpdateRow($module, $suffix)
    {
        $HTML = '<tr><td>';

        $HTML .= implode('</td><td>', $this->getBatchUpdateRowFields($module, $suffix));

        $HTML .= '</td></tr>';

        return $HTML;
    }

    /**
     * @desc Gets an object representing a field for any fieldname and module, independent of a form context. This was copied from FormClasses and whittled down to those
     * field types that will be updateable.
     *
     * @param string $module The module we are working in.
     * @param string $fieldName The field we want to represent.
     * @param string $suffix The suffix number of the batch operation, since we can be displaying multiple operations at once.
     */
    protected function GetFormFieldObjectForBatchUpdate($module, $fieldName, $Suffix)
    {
        global $FieldDefs;

        $FieldDef = $FieldDefs[$module][$fieldName];

        if (!$FieldDef)
        {
            $FieldDef = $FieldDefs[getModuleFromField($fieldName)][$fieldName];
        }

        $OriginalFieldName = $fieldName;
        $InputFieldName = $fieldName;

        $FieldObj = new FormField();

        switch ($FieldDef["Type"])
        {
            case "string_search":
                $field = Forms_SelectFieldFactory::createSelectField(
                    $OriginalFieldName,
                    $FieldDef['Module'],
                    $Data[$InputFieldName],
                    $mode,
                    false,
                    $FieldDef['Title'],
                    '',
                    $Data['CHANGED-'.$InputFieldName],
                    $Suffix
                );

                $field->setFreeText();

                if ($FieldDef['CustomCodes'])
                {
                    $field->setCustomCodes($FieldDef['CustomCodes']);
                }

                if ($FieldDef["DefaultHidden"])
                {
                    $field->setDefaultHidden($FieldDef["DefaultHidden"]);
                }

                if ($FieldDef["FieldFormatsName"])
                {
                    $field->setFieldFormatsName($FieldDef["FieldFormatsName"]);
                }


                break;
            case "multilistbox": //for batch update purposes, we display multicodes as single codes.
            case "ff_select":
                $field = Forms_SelectFieldFactory::createSelectField($OriginalFieldName, $FieldDef['Module'], $Data[$InputFieldName],
                    $mode, false, $FieldDef['Title'], '', $Data['CHANGED-'.$InputFieldName], $Suffix);

                if ($FieldDef['CustomCodes'])
                {
                    $field->setCustomCodes($FieldDef['CustomCodes']);
                }

                if ($OriginalFieldName == 'rep_approved')
                {
                    $CustomCodeData = DatixDBQuery::PDO_fetch_all('SELECT code, description FROM code_approval_status WHERE module = :module AND workflow = :workflow ORDER BY cod_listorder', array('module' => $module, 'workflow' => GetWorkflowID($module)));

                    foreach ($CustomCodeData as $Details)
                    {
                        if ($Details['code'] != 'NEW')
                        {
                            // handle relabelled approval statuses
                            $CustomCodes[$Details['code']] = ( $new_desc = _tk( 'approval_status_'.$module.'_'.$Details['code'] ) ) ? $new_desc : $Details['description'];
                        }
                    }

                    $field->setCustomCodes($CustomCodes);
                }

                if ($FieldDef["DefaultHidden"])
                {
                    $field->setDefaultHidden($FieldDef["DefaultHidden"]);
                }

                if ($FieldDef["FieldFormatsName"])
                {
                    $field->setFieldFormatsName($FieldDef["FieldFormatsName"]);
                }

                break;
            case "array_select":
                $field = Forms_SelectFieldFactory::createSelectField($InputFieldName, $FieldDef['Module'], $Data[$InputFieldName],
                    $mode, false, $FieldDef['Title'], '', $Data['CHANGED-'.$InputFieldName], $Suffix);
                $field->setCustomCodes($FieldDef["SelectArray"]);

                if ($FieldDef["DefaultHidden"])
                {
                    $field->setDefaultHidden($FieldDef["DefaultHidden"]);
                }

                break;
            case "date":
                $InputFieldName .= '_'.$Suffix;
                $FieldObj->MakeDateField($InputFieldName, $Data[$InputFieldName], $FieldDef);
                break;
            case "time":
                $InputFieldName .= '_'.$Suffix;
                $FieldObj->MakeTimeField($InputFieldName, $Data[$InputFieldName]);
                break;
            case "number":
                $InputFieldName .= '_'.$Suffix;
                $FieldObj->MakeNumberField($InputFieldName, $Data[$InputFieldName], $FieldDef["Width"], $FieldDef['MaxLength']);
                break;
            case "money":
                $InputFieldName .= '_'.$Suffix;
                $FieldObj->MakeMoneyField($InputFieldName, $Data[$InputFieldName], $FieldDef["Width"]);
                break;
            case "textarea":
                $MyMaxLength = $FieldDef['MaxLength'];
                $FieldObj->MakeTextAreaField($InputFieldName, $FieldDef["Rows"],
                    35, $MyMaxLength,
                    $Data[$InputFieldName], ($FieldDef["NoSpellcheck"] == "" ? true : false), true, '', $Label, $Data['CURRENT_'.$InputFieldName], $Suffix);
                break;
            case "string":
                $InputFieldName .= '_'.$Suffix;
                $FieldObj->MakeInputField($InputFieldName, $FieldDef["Width"],
                    $FieldDef["MaxLength"], $Data[$InputFieldName], $FieldDef);
                break;
            case "email":
                $InputFieldName .= '_'.$Suffix;
                $FieldObj->MakeEmailField($InputFieldName, $FieldDef["Width"],
                    $FieldDef["MaxLength"], $Data[$InputFieldName]);
                break;
            case "checkbox":
                $InputFieldName .= '_'.$Suffix;

                if (array_key_exists($InputFieldName, $Data))
                {
                    if ($Data[$InputFieldName] === true)
                    {
                        $Value = "Y";
                    }
                    elseif ($Data[$InputFieldName] === false)
                    {
                        $Value = "N";
                    }
                    else
                    {
                        $Value = $Data[$InputFieldName];
                    }
                }
                elseif ($Data["recordid"] != "")
                {
                    $Value = "N";
                }
                else
                {
                    $Value = "";
                }

                $field = $FieldObj->MakeDivYesNo($InputFieldName, $FieldDef["Div"], $Value);
                break;
            case "yesno":
                $InputFieldName .= '_'.$Suffix;

                if ($FieldDef["Div"])
                {
                    $field = $FieldObj->MakeDivYesNo($InputFieldName, $FieldDef["Div"], $Data[$InputFieldName], $FieldDef['DefaultHidden']);
                }
                else
                {
                    $field = $FieldObj->MakeYesNoSelect($InputFieldName, $FieldDef["Module"], $Data[$InputFieldName], $mode,
                        $FieldDef["ExtraChange"].$OnChangeJS, GetFormFieldLabel($InputFieldName, $FieldDef["Title"]), $FieldDef['DefaultHidden']);
                }

                break;
            case "formfield":
                if ($FieldDef["FormField"] instanceof Forms_FormField)
                {
                    $field = $FieldDef["FormField"];
                }
                else
                {
                    $FieldObj = $FieldDef["FormField"];
                }

                break;
            case "grading":
                $InputFieldName .= '_'.$Suffix;
                $FieldObj->MakeRatingFields(
                    false,
                    $FieldDef["Consequence"], $FieldDef["Likelihood"], $InputFieldName,
                    "", "", "",
                    _tk('consequence'), _tk('likelihood'), _tk('level'),
                    false);
                break;
        }

        if ($field)
        {
            return $field;
        }
        else
        {
            return $FieldObj;
        }
    }
}