<?php

class Service_DuplicateSearch extends Service_Service
{
    public function CollectParameters()
    {
        global $dtxtitle, $DatixView;

        $this->VerifyDuplicateSearchCredentials();

        $DatixView->Fields = array(
            'con_title',
            'con_forenames',
            'con_surname',
            'con_organisation',
            'con_email',
            'con_postcode',
            'con_tel1',
            'con_tel2',
            'con_dob',
            'con_number',
            'con_nhsno',
            'con_type',
            'con_subtype'
        );

        $DatixView->SelectedFields = $_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS'];

        $dtxtitle = 'Duplicate contacts search options';

        GetPageTitleHTML(array(
            'title' => $dtxtitle,
            'module' => 'CON'
        ));

        GetSideMenuHTML(array('module' => 'CON'));

        template_header_nopadding();

        echo $DatixView->render('classes/Service/DuplicateSearchClass/CollectParameters.php');

        footer();
        obExit();
    }

    /**
    * Takes fields provided in $_POST and searches db for contacts with these fields in common. Presents results to user.
    */
    public function PerformSearch()
    {
        global $dtxtitle, $DatixView;

        $this->VerifyDuplicateSearchCredentials();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $fields = $_POST['duplicate_fields'];
            unset ($_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS']);
        } else {
            $fields = $_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS'];
        }


        if (empty($fields))
        {
            AddSessionMessage('ERROR', 'No fields provided.');
            $this->CollectParameters();
        }
        else
        {
            $FieldsToSearchOn = Sanitize::SanitizeStringArray( isset($_POST['duplicate_fields']) ? $_POST['duplicate_fields'] : $_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS'] );
            $_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS'] = $FieldsToSearchOn;

            foreach ($FieldsToSearchOn as $Field)
            {
                // Create field object to get type
                $ContactsField = new Fields_Field($Field, 'contacts_main');

                // This is not a listing of contact records, so we need to treat the fields as dummy fields.
                $ListingFields[] = new Listings_ListingColumn($Field, 'contacts_main');

                if ($ContactsField->getFieldType() == 'D')
                {
                    $SQLFieldsToSearchOn[] = $Field;
                }
                else
                {
                    $SQLFieldsToSearchOn[] = 'ISNULL('.$Field.', \'\') as '.$Field;
                }
            }

            $ListingFields[] = new Fields_DummyField(array('name' => 'num', 'label' => 'Number of matches', 'type' => 'N'));

            $Design = new Listings_ListingDesign(array('columns' => $ListingFields));

            $SecurityWhere = MakeSecurityWhereClause('', 'CON', $_SESSION['initials']);

            $Duplicates = DatixDBQuery::PDO_fetch_all(
            'SELECT '.implode(', ', $FieldsToSearchOn).', num FROM
            (SELECT '.implode(', ', $FieldsToSearchOn).', count(*) as num FROM
            (SELECT '.implode(', ', $SQLFieldsToSearchOn).' FROM contacts_main'.($SecurityWhere ? ' WHERE '.$SecurityWhere : '').') g
            GROUP BY '.implode(', ', $FieldsToSearchOn).') f
            WHERE f.num > 1 ORDER BY f.num DESC');

            $RecordList = new RecordLists_RecordList();
            $RecordList->AddRecordData($Duplicates);

            $DatixView->Listing = new Listings_ListingDisplay($RecordList, $Design);
            $DatixView->Listing->Service = 'DuplicateSearch';
            $DatixView->Listing->Event = 'viewduplicatelisting';
            $DatixView->Listing->URLParameterArray = $FieldsToSearchOn;

            $dtxtitle = 'Possible duplicate contacts';

            GetPageTitleHTML(array(
                'title' => $dtxtitle,
                'module' => 'CON'
            ));

            GetSideMenuHTML(array('module' => 'CON'));

            template_header_nopadding();

            echo $DatixView->render('classes/Service/DuplicateSearchClass/PerformSearch.php');

            footer();
            obExit();
        }
    }

    public function ViewDuplicateListing()
    {
        global $ModuleDefs;

        foreach ($_GET as $key => $val)
        {
            if ($key != 'event' && $key != 'service' && isset($val))
            {
                // format date fields
                if (in_array($key, $ModuleDefs['CON']['FIELD_ARRAY']))
                {
                    $ContactsField = new Fields_Field($key, 'contacts_main');

                    if ($ContactsField->getFieldType() == 'D')
                    {
                        $val = FormatDateVal($val);
                    }
                }

                if ($val == '')
                {
                    $val = '='; //need to pretend the data has come from a search.
                }

                $_POST[$key] = $val;
            }
        }

        $_POST['from_duplicate_search'] = true;

        $loader = new \src\framework\controller\Loader();
        $controller = $loader->getController(
            array('controller' => 'src\\contacts\\controllers\\ContactsDoSelectionController')
        );

        foreach ($_POST as $key => $value)
        {
            $controller->setRequestParameter($key, $value);
        }

        echo $controller->doAction('searchcontact');
        obExit();
    }

    /**
    * @desc Checks that the current user is permitted to run the duplicate search operation.
    */
    protected function VerifyDuplicateSearchCredentials()
    {
        LoggedIn();

        return true;
    }
}

?>