<form enctype="multipart/form-data" method="POST" id="importfilewithprofle" name="importfilewithprofle" action="<?php echo $scripturl ?>?service=importexcel&event=doimport">
    <ol>
    <li class="new_titlebg">
        <div class="title_text_wrapper"><b>Select file to import</b></div>
    </li>
    <?php echo GetDivFieldHTML('Choose File', $FileSelect->getField()); ?>
    <li class="new_titlebg">
        <div class="title_text_wrapper"><b>Select profile to use for this import</b></div>
    </li>
    <?php echo GetDivFieldHTML('Module', $ModuleSelect->getField()); ?>
    <?php foreach($Modules as $Module => $Name): ?>
    <li id="<?= $Module ?>-div-wrapper" class="profile-div-wrapper">
    <ol>
    <?php echo GetDivFieldHTML('Choose Profile', $ProfileSelect[$Module]->getField()); ?>
    </ol>
    </li>
    <?php endforeach; ?>
    <li class="button_wrapper">
            <button type="button" value="upload" onclick="jQuery('#importfilewithprofle').submit()"><?php echo _tk('btn_import') ?></button>
            <button type="button" value="cancel" onclick="SendTo('<?= $scripturl ?>?action=home&module=ADM')"><?php echo _tk('btn_cancel') ?></button>
    </li>
    </ol>
</form>
<script language="Javascript">jQuery(".profile-div-wrapper").hide();</script>
<?php if($Parameters['module']): ?>
<script language="Javascript">jQuery("#<?= $Parameters['module'] ?>-div-wrapper").show();</script>
<?php endif; ?>

