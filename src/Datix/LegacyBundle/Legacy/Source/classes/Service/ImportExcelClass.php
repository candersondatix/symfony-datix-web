<?php


class Service_ImportExcel extends Service_Service
{

    public function DoImport()
    {
        LoggedIn();
        CheckFullAdmin();

        try
        {
            if (!file_exists($_FILES['import_file']['tmp_name']))
            {
                throw new MissingParameterException('You must select a file to import');
            }
            else if (!$_POST['module'])
            {
                throw new MissingParameterException('You must select a module');
            }
            else if (!$_POST['profile_'.$_POST['module']])
            {
                throw new MissingParameterException('You must select a profile');
            }

            $Profile = new ImportProfile(new DatixDBQuery(''), intval($_POST['profile_'.$_POST['module']]));

            // Save file for future use and for PHPExcel library be able to determine the correct file type
            $filePath = $this->saveFile($_FILES);

            switch ($Profile->getType())
            {
                case 'CSV':
                    $FileParts = explode('.', $_FILES['import_file']['name']);

                    if($FileParts[count($FileParts)-1] != 'csv')
                    {
                        throw new Exception('That profile requires a CSV file');
                    }

                    $File = new Files_CSV($_FILES['import_file']['tmp_name']);
                    break;

                case 'EXC':
                    $FileParts = explode('.', $_FILES['import_file']['name']);

                    if(!in_array($FileParts[count($FileParts)-1], array('xls', 'xlsx')))
                    {
                        throw new Exception('That profile requires an Excel file');
                    }

                    $File = new Files_Excel($filePath);
                    break;
            }

            $ImportHandler = new Import_UsingProfile($File, new DatixDBQuery(''), $Profile);

            $ImportHandler->importData();

            // Delete file after process
            $this->deleteFile($filePath);

            $message = 'Import complete: '.$ImportHandler->getNumRecordsAdded().' records added. '.$ImportHandler->getNumRecordsUpdated().' records updated.';

            AddSessionMessage('INFO', $message);

        }
        catch(Exception $e)
        {
            AddSessionMessage('ERROR', $e->getMessage());
        }

        $this->SetupImport($_POST);

    }


    public function SetupImport($Parameters = array())
    {
        LoggedIn();
        CheckFullAdmin();

        global $ModuleDefs;

        $FileSelect = new FormField();
        $FileSelect->MakeCustomField('<input name="import_file" id="import_file" type="file" size="50"/>');

        $Modules = getModArray();
        unset($Modules['DAS']);
        unset($Modules['ADM']);

        $DBProfiles = DatixDBQuery::PDO_fetch_all('SELECT exp_profile_form, exp_profile_id, exp_profile_name FROM excel_profile ORDER BY exp_profile_id', array(), PDO::FETCH_GROUP|PDO::FETCH_ASSOC);

        foreach ($Modules as $Module => $Name)
        {
            $Value = '';
            $ModuleProfiles = array();
            if (isset($ModuleDefs[$Module]['TABLE']) && is_array($DBProfiles[$ModuleDefs[$Module]['TABLE']]))
            {
                foreach ($DBProfiles[$ModuleDefs[$Module]['TABLE']] as $Details)
                {
                    $ModuleProfiles[$Details['exp_profile_id']] = $Details['exp_profile_name'];
                }

                if($Parameters['module'] && $Parameters['module'] == $Module)
                {
                    $Value = $Parameters['profile_'.$Module];
                }

                $ProfileSelect[$Module] = Forms_SelectFieldFactory::createSelectField('profile_'.$Module, 'ADM', $Value, '');
                $ProfileSelect[$Module]->setCustomCodes($ModuleProfiles);
            }
            else
            {
                unset($Modules[$Module]);
            }
        }

        $ModuleSelect = Forms_SelectFieldFactory::createSelectField('module', 'ADM', $Parameters['module'], '');
        $ModuleSelect->setCustomCodes($Modules);
        $ModuleSelect->setOnChangeExtra('jQuery(".profile-div-wrapper").hide();jQuery("#"+jQuery("#module").val()+"-div-wrapper").show();');

        getPageTitleHTML(array(
             'title' => _tk('import-using-profile'),
             'module' => 'ADM',
        ));

        GetSideMenuHTML(array('module' => 'ADM'));

        template_header();

        include (dirname(__FILE__) . '/views/ChooseFiles.php');

        footer();
        obExit();
    }

    /**
     * Saves a file.
     *
     * @param string $file The uploaded array $_FILE.
     *
     * @return string $toFile The path where the file was saved.
     * @throws Exception
     */
    private function saveFile($file)
    {
        $toFile = \Sanitize::SanitizeFilePath(\src\framework\registry\Registry::getInstance()->getParm('PATH_TEMPLATES', '', true) . "\\" . $file['import_file']['name']);

        if ($_FILES['import_file']['size'] == '0' || !@move_uploaded_file(\Sanitize::SanitizeFilePath($file['import_file']['tmp_name']), $toFile))
        {
            throw new Exception('Cannot copy file to server. Please contact your IT department.');
        }

        return $toFile;
    }

    /**
     * Deletes a file.
     *
     * @param string $file The path of the file to be deleted.
     */
    private function deleteFile($file)
    {
        unlink($file);
    }
}
