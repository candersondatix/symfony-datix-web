<?php

class Service_BatchDelete extends Service_Service
{
    /**
    * @desc Displays a screen that allows users to confirm the deletion and choose whther documents will be deleted
    *
    * @param array $Parameters Array of parameters.
    * @param string $Parameters['module'] Module we are working in.
    *
    * @codeCoverageIgnoreStart
    * No unit test, since it deals with constructing HTML
    */
    public function CollectParameters($Parameters)
    {
        global $dtxtitle, $yySetLocation, $scripturl, $ModuleDefs;

        $this->VerifyBatchDeleteCredentials($Parameters);

        //These modules require that records are selected with checkboxes
        if (in_array($Parameters['module'], array('ATM', 'AMO', 'ATI', 'ATQ', 'AQU')) &&
            (!isset($_SESSION[$Parameters['module']]['RECORDLIST']) || empty($_SESSION[$Parameters['module']]['RECORDLIST']->FlaggedRecords)))
        {
            AddSessionMessage('ERROR', 'You must select at least one record to delete');
            $yySetLocation = $scripturl . '?action=list&module='. $Parameters['module'];
            redirectexit();
        }
        
        $dtxtitle = 'Batch Delete';

        GetPageTitleHTML(array(
             'title' => $dtxtitle,
             'module' => $Parameters['module']
             ));
        GetSideMenuHTML(array('module' => $Parameters['module']));
        template_header_nopadding();

        //to prevent additional searches within the same session from affecting this operation.
        $_SESSION[$Parameters['module']]['BATCH_DELETE_WHERE'] = $_SESSION[$Parameters['module']]['WHERE'];

        $WhereClause = $_SESSION[$Parameters['module']]['BATCH_DELETE_WHERE'];
        $WhereClause = MakeSecurityWhereClause($WhereClause, $Parameters['module'], $_SESSION['initials']);
        
        //Apply any checkbox-selected record filters
        if ($_SESSION[$Parameters['module']]['RECORDLIST'] && !empty($_SESSION[$Parameters['module']]['RECORDLIST']->FlaggedRecords))
        {
            if ($WhereClause)
            {
                $WhereClause = '('.$WhereClause.') AND ';
            }

            $WhereClause .= 'recordid IN ('.implode(', ', $_SESSION[$Parameters['module']]['RECORDLIST']->FlaggedRecords).')';
        }
        
        $recordsToDelete = DatixDBQuery::PDO_fetch_all('SELECT recordid as num FROM ' . ($ModuleDefs[$Parameters['module']]['VIEW'] ? $ModuleDefs[$Parameters['module']]['VIEW'] : $ModuleDefs[$Parameters['module']]['TABLE']) . ($WhereClause ? ' WHERE '.$WhereClause : ''), array(), PDO::FETCH_COLUMN);
        $NumRecords = count($recordsToDelete);

        $_SESSION[$Parameters['module']]['BATCH_DELETE_RECORDS'] = $recordsToDelete;

        echo '<form method="POST" action="'.$scripturl.'?service=batchdelete&event=executefrompost&module='.$Parameters['module'].'">';

        echo '<div class="padded_div">';

        echo '<div>This process will delete all information related to these records, including any linked actions. If you do not wish to delete this information, please press "cancel" now.</div>';

        switch ($Parameters['module'])
        {
            case 'ATM' :
                
                $NumRelatedRecords = RecordLists_ModuleRecordList::CountATMRelatedRecords($recordsToDelete);
                $ATINumRecords = $NumRelatedRecords['ATI'];
                $AMONumRecords = $NumRelatedRecords['AMO'];

                echo '<div>
                	- You are about to delete ' . $NumRecords . ' ' . _tk('ATMName') . ' records<br />
                	- You are about to delete ' . $ATINumRecords . ' ' . _tk('ATIName') . ' records<br />
                	- You are about to delete ' . $AMONumRecords . ' ' . _tk('AMOName') . ' records<br />
                	Plus all linked ' . _tk('AQUNames') . '
                </div></div>';
                break;
            case 'ATI' :
                
                $AMONumRecords = RecordLists_ModuleRecordList::CountATIRelatedRecords($recordsToDelete);
                
                echo '<div>
                	- You are about to delete ' . $NumRecords . ' ' . _tk('ATIName') . ' records<br />
                	- You are about to delete ' . $AMONumRecords . ' ' . _tk('AMOName') . ' records<br />
                	Plus all linked ' . _tk('AQUNames') . '
                </div></div>';
                break;
            case 'AMO' :
                echo '<div>
                	- You are about to delete ' . $NumRecords . ' ' . _tk('AMOName') . ' records<br />
                	Plus all linked ' . _tk('AQUNames') . '
                </div></div>';
                break;
            default:
                echo '<div><input type="checkbox" name="doc_delete" value="1"> Delete all associated documents?</div>';
                echo '<div>You are about to delete '.$NumRecords.' records</div></div>';
        }
        

        echo '
        <div class="button_wrapper">
            <input type="button" class="button" value="Delete '.$NumRecords.' records now" onclick="if(confirm(\'This action will permanently delete '.$NumRecords.' records. Are you sure you want to do this?\')){this.form.submit()}">
            <input type="button" class="button" value="'._tk('btn_cancel').'" onclick="SendTo(\''.$scripturl.'?action=home&module='.$Parameters['module'].'\');">
        </div>';

        echo '</form>';

        footer();
        obExit();
    }

    // @codeCoverageIgnoreEnd

    /**
    * @desc Checks that the current user is permitted to run the batch delete operation.
    * @param array $Parameters Array of parameters.
    * @param string $Parameters['module'] Module we are working in.
    */
    protected function VerifyBatchDeleteCredentials($Parameters)
    {
        global $yySetLocation, $scripturl;

        LoggedIn();

        if (!bYN(GetParm('ENABLE_BATCH_DELETE', 'N')) || !$Parameters['module'] || !ModIsLicensed($Parameters['module']))
        {
            $yySetLocation = $scripturl;
            redirectexit();
        }
    }

    /**
    * @desc Executes a batch update operation using data passed from the setup form in the $_POST variable.
    * @param array $Parameters Array of parameters.
    * @param string $Parameters['module'] Module we are working in.
    */
    public function ExecuteFromPost($Parameters)
    {
        $this->VerifyBatchDeleteCredentials($Parameters);

        $Parameters['delete_docs'] = ($_POST['doc_delete'] == 1);

        $this->ExecuteAndReport($Parameters);
    }


    /**
    * @desc Executes the batch update operation based on parameters recieved.
    *
    * @param array $Parameter Array of parameter values.
    * @param string $Parameters['module'] Module we are working in.
    * @param array $Parameter['delete_docs'] If true, then linked documents will be deleted. Otherwise only the links will be removed.
    */
    protected function Execute($Parameters)
    {
        global $ModuleDefs;
        
        $this->VerifyBatchDeleteCredentials($Parameters);

        $RecordList = new RecordLists_ModuleRecordList();
        $RecordList->Module = $Parameters['module'];
        $RecordList->Paging = false;
        // We need to set the table first otherwise Batch delete can't delete the records if a view is set.
        $RecordList->Table = $ModuleDefs[$RecordList->Module]['TABLE'] ?: $ModuleDefs[$RecordList->Module]['VIEW'];

        if ($ModuleDefs[$Parameters['module']]['VIEW'] && $Parameters['module'] != 'PAY')
        {
            $RecordList->WhereClause = MakeSecurityWhereClause($_SESSION[$Parameters['module']]['BATCH_DELETE_WHERE'], $Parameters['module'], $_SESSION['initials'],
                '', '', false, false);
        }
        elseif ($ModuleDefs[$Parameters['module']]['VIEW'] && $Parameters['module'] == 'PAY')
        {
            $RecordList->WhereClause = MakeSecurityWhereClause($_SESSION[$Parameters['module']]['WHERE'], $Parameters['module'], $_SESSION['initials']);
        }
        else
        {
            $RecordList->WhereClause = MakeSecurityWhereClause($_SESSION[$Parameters['module']]['BATCH_DELETE_WHERE'], $Parameters['module'], $_SESSION['initials']);
        }
        
        $recordsToDelete = $_SESSION[$Parameters['module']]['BATCH_DELETE_RECORDS'];
        
        if (!empty($recordsToDelete))
        {
            $recordsWhereClause = 'recordid IN (' . implode(', ',$recordsToDelete) . ')';

            // We need a sub-select to deal with modules that uses a view.
            if (!empty($RecordList->WhereClause) && $ModuleDefs[$Parameters['module']]['VIEW'])
            {
                $RecordList->WhereClause = $recordsWhereClause.' AND recordid IN (SELECT recordid FROM '.$ModuleDefs[$Parameters['module']]['VIEW'].' WHERE '.$RecordList->WhereClause.')';
            }
            elseif (!empty($RecordList->WhereClause))
            {
                $RecordList->WhereClause = '(' . $RecordList->WhereClause . ') AND ';
            }

            if ($ModuleDefs[$Parameters['module']]['VIEW'] == '' || $ModuleDefs[$Parameters['module']]['VIEW'] == null)
            {
                $RecordList->WhereClause .= $recordsWhereClause;
            }
        }

        $Result = $RecordList->BatchDelete($Parameters['delete_docs']);

        // We need to clear the record list from the session because it's causing cache problems in the Accreditation module
        if (in_array($Parameters['module'], array('ATM', 'AMO', 'ATI', 'ATQ', 'AQU')))
        {
            unset($_SESSION[$Parameters['module']]['RECORDLIST']);
        }

        return $Result;
    }


    /**
    * @desc Executes the batch update operation based on parameters recieved and then outputs the result to the browser.
    * Can be called from POST via ExecuteFromPost(), or from within the application.
    *
    * @param array $Parameter Array of parameter values.
    * @param string $Parameters['module'] Module we are working in.
    * @param array $Parameter['delete_docs'] If true, then linked documents will be deleted. Otherwise only the links will be removed.
    *
    * @codeCoverageIgnoreStart
    * No unit test, since it deals with constructing HTML
    */
    protected function ExecuteAndReport($Parameters)
    {
        global $dtxtitle, $ModuleDefs;

        $this->VerifyBatchDeleteCredentials($Parameters);

        $Result = $this->Execute($Parameters);

        $dtxtitle = 'Batch delete: Results';

        GetPageTitleHTML(array(
             'title' => $dtxtitle,
             'module' => $Parameters['module']
             ));
        GetSideMenuHTML(array('module' => $Parameters['module']));
        template_header_nopadding();

        echo '<div class="padded_div">';

        if (count($Result['deleted']) > 0)
        {
            echo count($Result['deleted']).' records have been deleted.';

            if (count($Result['not_deleted']) > 0)
            {
                echo 'Some records could not be deleted ('.implode(', ', $Result['not_deleted']).')';
            }
        }
        else if (count($Result['deleted']) == 0)
        {
            echo 'Problems occurred during this process. No records were deleted.';
        }

        echo '</div>';

        footer();
        obExit();

    }

    // @codeCoverageIgnoreEnd
}
