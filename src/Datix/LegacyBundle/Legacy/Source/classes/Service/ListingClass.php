<?php

/**
* @desc Describes a service which allows the display of a listing of records based on a search,
* a hard-coded query or a drill down into a graphical report.
*/
class Service_Listing extends Service_Service
{
    protected $ListingDesign;
    protected $RecordList;


    public function GetListingDesign($Parameters)
    {
        $ListingDesign = new Listings_ModuleListingDesign(array('module' => $Parameters['module']));
        $ListingDesign->LoadColumnsFromDB();

        return $ListingDesign;
    }

    /**
    * @desc Returns a where clause for the listing being constructed based on information passed in the url and the session.
    */
    public function GetWhereClause($Parameters)
    {
        /**
        * We need to find the where clause to pass to the recordlist object. This where clause depends on what situation
        * we are in - we could be looking at the results of a search, a saved query, a hard-coded listing or the results from
        * drilling into a report.
        */
        global $ModuleDefs;

        if (isset($Parameters['listref'])) //we are looking at a hard coded listing based on rep_approved.
        {
            $Parameters['listtype'] = "search";

            $WhereClause = $ModuleDefs[$Parameters['module']]['TABLE'] . '.rep_approved like \''.$Parameters['listref'].'\'';

            $Where[] = $WhereClause;
            $_SESSION[$Parameters['module']]["WHERE"] = $WhereClause;

            if ($Parameters['overdue'] && !is_array($ModuleDefs[$Parameters['module']]['HARD_CODED_LISTINGS']))
            {
                $overdueSQL = getOverdueSQL($Parameters['module'], $Parameters['listref']);
                $join = $overdueSQL['join'];
                $Where[] = $overdueSQL['where'];
            }

        }
        else if ($ModuleDefs[$Parameters['module']]['HARD_CODED_LISTINGS'][$Parameters['listtype']]) //we are looking at a hard coded listing based on more complex inputs
        {
            if($Parameters['overdue'])
            {
                $Where[] = $ModuleDefs[$Parameters['module']]['HARD_CODED_LISTINGS'][$ListType]['OverdueWhere'];
            }
            else
            {
                $Where[] = $ModuleDefs[$Parameters['module']]['HARD_CODED_LISTINGS'][$ListType]['Where'];
            }
        }
        else if ($_REQUEST["drillwhere"] != '') //we are looking at a drill down from a graphical report.
        {
            $Where = array(stripslashes(Sanitize::SanitizeRaw($_REQUEST['drillwhere'])));
            $_SESSION[$Parameters['module']]["current_drillwhere"] = Sanitize::SanitizeRawArray($_REQUEST["drillwhere"]);
        }
        else if($Parameters['fromcrosstab'] == '1') //we are looking at a drill down from a crosstab.
        {
            $Where[] =  "(" . $_SESSION['CurrentReport']->ConstructCellWhereClause($Parameters) . ")";
        }
        else if ($ListType == "search" && $_SESSION[$Parameters['module']]["WHERE"]) //normal search - retrieve the where clause from the session.
        {
            $Where[] =  "(" . $_SESSION[$Parameters['module']]["WHERE"] . ")";
        }

        if ($Parameters['listtype'] == 'all')
        {
            $_SESSION[$Parameters['module']]["WHERE"] = '1=1';
        }
        elseif ($Parameters['listtype'] != "search")
        {
            $_SESSION[$Parameters['module']]["WHERE"] = '';
        }

        return MakeSecurityWhereClause($Where, $Parameters['module'], $_SESSION["initials"]);
    }

    public function GetRecordList($Parameters)
    {
        global $ModuleDefs;

        $orderby = $_SESSION["LIST"][$Parameters['module']]["ORDERBY"];

        if (!$orderby)
        {
            $orderby = $ModuleDefs[$Parameters['module']]['DEFAULT_ORDER'];
        }

        $OrderCol = new Listings_ListingColumn($orderby);

        $order = $_SESSION["LIST"][$Parameters['module']]["ORDER"];

        if (!$_SESSION["LIST"][$Parameters['module']]["ORDER"] &&  $module == 'INC')
        {
            if(bYN(GetParm('LISTINGS_ORDER_DESC', 'N')))
            {
                $OrderCol->setDescending();
            }
            else
            {
                $OrderCol->setAscending();
            }
        }
        else if(!$order)
        {
            $OrderCol->setDescending();
        }

        $RecordList = new RecordLists_ModuleRecordList();
        $RecordList->Module = $Parameters['module'];
        $RecordList->Columns = $this->ListingDesign->Columns;
        $RecordList->WhereClause = $this->GetWhereClause($Parameters);
        if($orderby)
        {
            $RecordList->OrderBy = array($OrderCol);
        }
        $RecordList->StartAt = $Parameters['start'];

        $RecordList->RetrieveRecords();

        return $RecordList;
    }

    protected function SavedQueryRow($HideDropdown = false)
    {
        require_once 'Source/libs/QueriesBase.php';

        if (bYN(GetParm("SAVED_QUERIES", "Y")) && !$LinkMode)
        {
            $saved_queries = get_saved_queries($Parameters['module']);

    ?>
           <table width="100%">

            <tr><td>
    <?php
        if(!$HideDropdown)
        {
    ?>
            <div style="float:left">
                <form method="post" name="queryform" action="<?php echo "{$scripturl}?action=executequery&amp;module={$Parameters['module']}"?>" style="display: inline">
                     <a name="datix-content" id="datix-content"></a>
                     <b><?php echo _tk('query_colon') ?></b>
                        <select name="qry_name" <?php if(!is_array($saved_queries) || empty($saved_queries)) { echo 'style="width:'.DROPDOWN_WIDTH_DEFAULT.'px"'; } ?> onchange="Javascript:if (document.queryform.qry_name.options[document.queryform.qry_name.selectedIndex].value != 'range' && document.queryform.qry_name.options[document.queryform.qry_name.selectedIndex].value != '') {document.queryform.submit()}">
    <?php
            $query_recordid = $Parameters["query"];
            if (is_array($saved_queries))
            {
                echo '<option value=""'
                        . ($query_recordid == $query ? ' selected="selected"' : '') . '>'
                        . 'Choose</option>';

                foreach ($saved_queries as $query => $querytitle)
                {
                    echo '<option value="' . $query . '"'
                        . ($query_recordid == $query ? ' selected="selected"' : '') . '>'
                        . $querytitle . '</option>';
                }
            }
        echo '
                            </select>
                    <input type="hidden" name="orderby" value="'. $orderby .'" />
                    <input type="hidden" name="order" value="'. $order .'" />
                    <input type="hidden" name="listnum" value="'. $listnum .'" />
                </form>
            </div>';

        }

        echo '
    <div style="float:right">
      <div class="list_replacer gbutton">
                                <a href="' . $scripturl . '?action=savequery&amp;module='.$Parameters['module'].'&amp;form_action=new&amp;table=' . Sanitize::SanitizeString($_REQUEST["table"]) . '">'._tk('save_as_query').'</a>

                        </div>
                </div>
      </td></tr></table>';

        }    //$list_request = GetSQLResultsTimeScale($selectfields, $ModuleDefs[$module]['TABLE'], $order, $orderby, $ListWhereClause, $listnum, $listdisplay, $listnum2, $listnumall, $sqla, $join, $module);

    }

    protected function getBackButtonHTML()
    {
        global $ModuleDefs;

        $HTML = '<input type="button" value="'._tk('btn_back').'" ';

        if($this->Parameters['fromcrosstab'] == '1')
        {
            $HTML .= 'onclick="SendTo(scripturl+\'?action=crosstab\')"';
        }
        elseif ($this->Parameters["from_report"] == '1')
        {
            $HTML .= 'onclick="SendTo(scripturl+\'?action=displaycurrentreport&width='.$this->Parameters['width'].'&height='.$this->Parameters['height'].'&from_dashboard='.$this->Parameters['from_dashboard'].'\')"';
        }
        elseif($this->Parameters["incident"])
        {
            $HTML .= 'onclick="SendTo(scripturl+\'?action=incident&amp;recordid=' . $this->Parameters["incident"] . '\')"';
        }
        elseif($this->Parameters["query"])
        {
            $HTML .= 'onclick="SendTo(scripturl+\'?action=savedqueries&amp;module='.$this->Parameters['module'].'\')"';
        }
        elseif($this->Parameters["listtype"] == "search")
        {
            $HTML .= 'onclick="SendTo(scripturl+\'?'.  $ModuleDefs[$this->Parameters['module']]["SEARCH_URL"] . '&amp;module='.$this->Parameters['module'].'&amp;searchtype=lastsearch\')"';
        }
        elseif($this->Parameters['module'])
        {
            $HTML .= 'onclick="SendTo(scripturl+\'?module='.$this->Parameters['module'].'\')"';
        }
        else
        {
            $HTML .= 'onclick="SendTo(scripturl+\'?module=INC\')">';
        }

        $HTML .= '/>';

        return $HTML;
    }
}
?>
