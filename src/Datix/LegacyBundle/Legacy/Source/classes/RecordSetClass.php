<?php

class RecordSet
{
    private $_module;

    private $_storage;

    public function __construct($module, array $recordIds = array())
    {
        $this->_module = $module;

        if (!empty($recordIds))
        {
            $_SESSION[$this->_module]['RECORDSET'] = $recordIds;
        }
    }

    public function getRecordIds()
    {
        return $_SESSION[$this->_module]['RECORDSET'];
    }

    // have to use method signature from RecordLists_RecordList::getRecordIndex, which is why it is so clunky
    public function getRecordIndex($recordId)
    {
        $recordIds = $this->getRecordIds();

        if (!is_array($recordIds) || empty($recordIds))
        {
            return false;
        }

        foreach ($recordIds as $index => $id)
        {
            if ($id == $recordId['recordid'])
            {
                return $index;
            }
        }

        return false;
    }

    // Using __get magic method to work with buttonGroup class. Once that class is updated to use this class, the magic method can be removed
    public function __get($property)
    {
        if ($property == 'Module')
        {
            return $this->_module;
        }

        if ($property == 'Records')
        {
            $records = array();

            foreach($this->getRecordIds() as $recordId)
            {
                $r = new StdClass;
                $r->Data = array(
                    'recordid' => $recordId
                );

                $records[] = $r;
            }

            return $records;
        }

        if ($property == 'NumRecords')
        {
            return count($this->getRecordIds());
        }
    }

    public function getStorage()
    {
        if (is_null($this->_storage))
        {
            $_SESSION[$this->_module]['RECORDSET'] = array();
            $this->_storage = $_SESSION[$this->_module]['RECORDSET'];
        }

        return $this->_storage;
    }

    public function setStorage()
    {
        $this->_storage;
    }
}