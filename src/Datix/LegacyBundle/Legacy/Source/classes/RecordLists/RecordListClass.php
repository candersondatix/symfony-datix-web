<?php

use src\framework\model\EntityCollection;

/**
* @desc Object to construct and hold lists of records for use with ListingClass and reports.
*
* Holds records as an array of Record objects, which at the moment are just data array wrappers.
*/
class RecordLists_RecordList extends RecordLists_RecordListShell
{
    /**
    * @var string
    * @desc The name of the table from which to retrieve records
    */
    public $Table;

    /**
    * @var array
    * @desc An array of Listings_ListingColumn objects representing the columns for which to retrieve data
    */
    public $Columns = array();

    /**
    * @var string
    * @desc A where clause, limiting the records to retrieve
    */
    public $WhereClause;

    /**
    * @var array
    * @desc An array of Listings_ListingColumn objects identifying the order in which they should be used to sort the retrieved data
    */
    public $OrderBy = array();

    /**
    * @var array
    * @desc Array to use with the PDO functions when retrieving records - contains mapped values for identifiers within the SQL statement.
    */
    public $PDOParamArray = array();

    /**
    * @var obj
    * @desc TableStructure object providing a map of the table structure of the system, to allow joins to be constructed.
    */
    public $TableStructure;

    public $TableColumnList;
    public $TableColumnMap;

    /**
    * @var array
    * @desc Array of Records_NestedRecord objects. This array is the core of the record list - the actual list of records.
    */
    public $Records = array();

    /**
    * @var int
    * @desc Number of records held in this recordlist TODO: check whether this is required as a parameter.
    */
    public $NumRecords = 0;

    /**
    * @var bool
    * @desc This will restrict records retrieved in this record list to only those that will fit on a page to reduce processing time
    */
    public $Paging = true;

    /**
    * @var int
    * @desc The location in the list to begin listing records from. 0, the default will start with the first record. 10 would start with the 11th etc.
    */
    public $StartAt = 0;

    /**
    * @var array
    * @desc Picks out particular records in a recordlist using pointers.
    */
    public $FlaggedRecords = array();

    /**
    * @desc Manually adds record objects to the list. Used if we are manually populating this object with data from a SQL query.
    *
    * @param array|EntityCollection $DataArray Nested array, containing results of a SQL query in the form array(array('field_name' => 'value'...), array(...))
    * 
    * @throws InvalidArgumentException If DataArray is not an array or an instance of EntityCollection.
    */
    public function AddRecordData($DataArray)
    {
        if (!is_array($DataArray) && !($DataArray instanceof EntityCollection))
        {
            throw new \InvalidArgumentException('DataArray must be an array or an instance of EntityCollection');
        }

        if( ! $_SESSION[$this->Module]['RECORDLIST'])
        {
            $_SESSION[$this->Module]['RECORDLIST'] = new RecordLists_RecordListShell();
        }

        $recordIdArray = array();

        foreach($DataArray as $RecordData)
        {
            $this->Records[] = new Records_Record($RecordData);

            $recordIdArray[] =  is_object($RecordData) ? $RecordData->recordid : $RecordData['recordid'];

        }

        $_SESSION[$this->Module]['RECORDLIST']->AddRecordData($recordIdArray, $_SESSION[$this->Module]['RECORDLIST']->check_all);
    }

    /**
    * @desc Simple getter to return the current base table.
    *
    * @return string Returns the name of the table from which the records listed are drawn
    */
    public function getTable()
    {
        return $this->Table;
    }

    /**
    * @desc Processes the OrderBy property to produce and return a SQL "ORDER BY" statement.
    *
    * @param bool $Reverse If true, reverses the sort order (asc/desc) for each field. Required when using "TOP X" in SQL to
    * pull out specific ranges of records.
    * @param string $Alias The alias given to the table being ordered in the SQL statement being produced.
    *
    * @return string A SQL "ORDER BY" statement that can be used in a SELECT query.
    */
    protected function getOrderBy($Reverse = false, $Alias = '')
    {
        foreach($this->OrderBy as $Field)
        {
            if(!$Alias)
            {
                $Alias = $Field->getTable();
            }

            $OrderByField = $Alias.'.'.$Field->getName();

            if(($Field->getOrder() == 'DESC' && !$Reverse) || ($Field->getOrder() != 'DESC' && $Reverse))
            {
                $OrderByField .= ' DESC';
            }
            else
            {
                $OrderByField .= ' ASC';
            }

            $OrderBy[] = $OrderByField;
        }

        return (isset($OrderBy) && count($OrderBy) > 0) ? ' ORDER BY '.implode(', ', $OrderBy) : ' ';
    }

    /**
    * @desc Processes the OrderBy property to produce an array of the fieldnames. Used to include these fields in SQL queries,
    * or when checking whether a particular field is in the order by statement.
    *
    * @return array An array of string field names.
    */
    protected function getOrderByFieldNames()
    {
        $Fields = array();

        foreach($this->OrderBy as $Field)
        {
            $Fields[] = $Field->getName();
        }

        return $Fields;
    }

    /**
    * @desc Creates an array of record objects by building a sql statement connecting all provided tables.
    */
    public function RetrieveRecords()
    {
        global $ModuleDefs;
        
        $this->NumRecords = array_shift(DatixDBQuery::PDO_fetch_all('SELECT count(*) as num FROM '.$this->getTable(). ($this->WhereClause ? ' WHERE ' . $this->WhereClause : ''), $this->PDOParamArray, PDO::FETCH_COLUMN));

        //if it is not included already, add the primary key of the source table to the end of the ordering array.
        if(Table::getTableIndex($this->getTable()) != '' && !in_array(Table::getTableIndex($this->getTable()), $this->getOrderByFieldNames()))
        {
            $this->OrderBy[] = new Listings_ListingColumn(Table::getTableIndex($this->getTable()), $this->getTable());
        }

        $OrderByInside = $this->getOrderBy();
        $OrderByOutside = $this->getOrderBy(true, 's');
        $OrderByFinal = $this->getOrderBy(false, 'r');

        if($this->Paging)
        {
            $numRecords = Listings_ListingDisplay::getRecordsPerPage();
        }
        else
        {
            $numRecords = $this->NumRecords;
        }

        $endAt = $this->StartAt + $numRecords;

        $OrderFields = $this->getOrderByFieldNames();

        //This first SQL statement pulls out the recordids of all the records we will be listing
        //Doing this first makes the subsequent SQL less complex, at the cost of an additional query.
        $sql = 'SELECT TOP ' . $numRecords . ' recordid FROM ';

        if (count($OrderFields) > 0)
        {
            $sql.= '(
                    SELECT TOP '.$numRecords.' '.implode(', ', $OrderFields).' FROM (
                        SELECT TOP '.$endAt.' '.implode(', ', $OrderFields).' FROM
                        '.$this->getTable().' WHERE '.$this->WhereClause.' '.$OrderByInside.'
                        ) s '.$OrderByOutside.'
                    ) r '.$OrderByFinal;
        }
        else
        {
            $sql.= $this->getTable() . ' WHERE '.$this->WhereClause;
        }

        $Records = DatixDBQuery::PDO_fetch_all($sql, $this->PDOParamArray, PDO::FETCH_COLUMN);

        if(count($Records) > 0)
        {
            //We know the records we are looking for, so we can simplify the where clause here.
            $this->WhereClause = $this->getTable().'.recordid IN ('.implode(', ',$Records).')';

            foreach($this->Columns as $Field)
            {
                if($Field->getName() == Table::getTableIndex($this->getTable()))
                {
                    $IndexPresent = true;
                }
            }

            if(!$IndexPresent && Table::getTableIndex($this->getTable()))
            {
                $this->Columns[] = Listings_ListingColumnFactory::getColumn($this->Module, Table::getTableIndex($this->getTable()), $this->getTable());
            }

            foreach($this->Columns as $Field)
            {
                if ($Field->getTable() != '')
                {
                    $this->TableColumnList[\UnicodeString::strtolower($Field->getTable())][] = $Field;
                }
            }


            //TODO:add an entry if the main module table is not included.


            $this->TableStructure = new TableStructure($this->getTable());

            if(count($this->TableColumnList) > 1) //check whether we have any linked tables
            {
                $TableList = array();
                foreach($this->TableColumnList as $Table => $Fields)
                {
                    if($Table != $this->getTable())
                    {
                        //This function will populate $this->TableStructure with linked tables.
                        $this->PopulateTableStructure($Table, $Fields);
                    }
                }

                //Get the SQL used to join to any linked tables.
                $JoinClause .= ' '.$this->GetJoinSQL();
            }

            //The top-level select statement needs to gather and alias all of the joined data - we need to make sure we have proper mappings
            //for all the field names:
            foreach($this->TableColumnList as $TableName => $Table)
            {
                foreach($Table as $column)
                {
                    if ($column->customSelect)
                    {
                        $TopFields[] = $column->getCustomSelect();
                    }
                    elseif($this->TableColumnMap[$TableName])
                    {
                        $TopFields[] = $this->TableColumnMap[$TableName].'.'.$column->getTable().'_'.$column->getName().' as \''.$column->getTable().'.'.$column->getName().'\'';
                    }
                    else
                    {
                        $TopFields[] = $column->getTable().'.'.$column->getName().' as \''.$column->getTable().'.'.$column->getName().'\'';
                    }
                }
            }

            //We have all of the parts of the SQL statement now, so we can start putting it together:

            // Fallback if there's no $TopFields
            if (!isset($TopFields))
            {
                $TopFields[] = '*';
            }

            $sql = 'SELECT '.implode(', ', $TopFields).' FROM '.$this->getTable();

            if($JoinClause)
            {
                $sql .= ' '.$JoinClause;
            }

            if($this->WhereClause)
            {
                $sql .= ' WHERE '.$this->WhereClause;
            }

            $sql .= $this->getOrderBy();

            $result = DatixDBQuery::PDO_fetch_all($sql, $this->PDOParamArray);

            // we need to choose one of the below methods:
            //create one record for each row = duplication due to link data.
            /*foreach($result as $row)
            {
                $this->Records[] = new Records_Record($ModuleDefs[$this->Module]['TABLE'], $row, $TableStructure);
            } */

            //combine data
            foreach($result as $row)
            {
                $RecordData[$row[$this->getTable().'.'.Table::getTableIndex($this->getTable())]][] = $row;
            }

            foreach($RecordData as $Recordid => $Data)
            {
                $NextRecord = new Records_NestedRecord($this->getTable(), $this->TableStructure);
                $NextRecord->PopulateFromMultipleTables($Data);
                $this->Records[] = $NextRecord;
            }
        }
    }

    /**
    * @desc Constructs the where clause to use when filtering linked tables.
    *
    * @return string The where clause to use for a particular table join.
    */
    protected function GetWhereSQL($order, $TableArray)
    {
        $wheres = array();

        foreach($TableArray['children'] as $Table => $TableInfo)
        {
            if($TableInfo['link'][$order][0]['where'])
            {
                $wheres[] = '('.$TableInfo['link'][$order][0]['where'].')';
            }
        }

        if(!empty($wheres))
        {
            return ' WHERE '.implode(' AND ', $wheres);
        }
    }

    /**
    * @desc Constructs the join clause to use when linking to other tables by calling the iterative
    * join function with current object property values.
    */
    protected function GetJoinSQL()
    {
        return $this->GetJoinSQLIterate(0, $this->TableStructure->TableArray);
    }

    /**
    * @desc Iterative function: Cycles through the "children" of the current table, creating joins to each of them and
    * calling itself on their children.
    *
    * @param int $order The "level" of iteration we are currently at. Used to help distinguish between aliases in complex joins.
    * @param array $TableArray Array describing the table structure below this point, identifying the tables to join to from the current table.
    *
    * @return string A SQL JOIN statement.
    */
    protected function GetJoinSQLIterate($order, $TableArray)
    {
        foreach($TableArray['children'] as $Table => $TableInfo)
        {
            $JoinClause = '';

            if(!empty($TableInfo['children']))
            {
                foreach($TableInfo['children'] as $ChildTable => $ChildInfo)
                {
                    $IndexField = Table::getTableIndex($ChildTable);
                    $Indexes[] = $ChildTable.'.'.$IndexField.' as '.$ChildTable.'_'.$IndexField;

                    foreach($this->TableColumnList[$ChildTable] as $column)
                    {
                        $Indexes[] = $column->getTable().'.'.$column->getName().' as '.$column->getTable().'_'.$column->getName();
                    }

                    $this->TableColumnMap[$ChildTable] = $TableInfo['table'].$order;
                }

                $IndexField = Table::getTableIndex($TableInfo['table']);
                $Indexes[] = $TableInfo['table'].'.'.$IndexField.' as '.$TableInfo['table'].'_'.$IndexField;
                foreach($this->TableColumnList[$TableInfo['table']] as $column)
                {
                    $Indexes[] = $column->getTable().'.'.$column->getName().' as '.$column->getTable().'_'.$column->getName();
                }
                $this->TableColumnMap[$TableInfo['table']] = $TableInfo['table'].$order;

                foreach($TableInfo['link'][$order] as $linkjoins)
                {
                    $joins[] = $linkjoins['currentTable'].'.'.$linkjoins['currentField'].' = '.$TableInfo['table'].$order.'.'.$linkjoins['targetTable'].'_'.$linkjoins['targetField'];
                    $Indexes[] = $linkjoins['targetTable'].'.'.$linkjoins['targetField'] .' as '.$linkjoins['targetTable'].'_'.$linkjoins['targetField'];
                }

                $Indexes = array_unique($Indexes);

                $JoinClause = ' LEFT JOIN
                (SELECT '.implode(', ', $Indexes).' FROM link_contacts
    '.$this->GetJoinSQLIterate($order+1, $TableInfo).
    $this->GetWhereSQL($order+1, $TableInfo).
    '
    )'.$TableInfo['table'].$order.' ON ';

                $JoinClause .= implode(' AND ', $joins);
            }
            else
            {
                $JoinClause = ' LEFT JOIN '.$TableInfo['link'][$order][0]['targetTable'].' ON ';

                foreach($TableInfo['link'][$order] as $linkjoins)
                {
                    $joins[] = $linkjoins['currentTable'].'.'.$linkjoins['currentField'].' = '.$linkjoins['targetTable'].'.'.$linkjoins['targetField'];
                }


                $JoinClause .= implode(' AND ', $joins);
            }

            $OverallJoinClause .= ' '.$JoinClause;
        }

        return $OverallJoinClause;
    }

    /**
    * @desc Determines and stores the series of tables to traverse between the main table and a different table.
    */
    protected function PopulateTableStructure($Table, $Fields)
    {
        global $ModuleDefs;

        $sql = 'SELECT TLK_LINK_ID, TLK_TABLE1, TLK_TABLE2, TLK_WHERE FROM TABLE_LINKS
                WHERE (TLK_TABLE1 = :MainTable AND TLK_TABLE2 = :TargetTable) OR (TLK_TABLE2 = :MainTable AND TLK_TABLE1 = :TargetTable)';
        $links = DatixDBQuery::PDO_fetch_all($sql, array('MainTable' => $this->getTable(), 'TargetTable' => $Table));

        if(empty($links)) //no direct links
        {
            $sql = 'SELECT TLK_LINK_ID, TLK_TABLE1, TLK_TABLE2, TLK_WHERE FROM TABLE_LINKS
                    JOIN TABLE_ROUTES ON TRT_LINK = TLK_LINK_ID
                    WHERE TRT_TABLE1 = :MainTable AND TRT_TABLE2 = :TargetTable ORDER BY TRT_ORDER ASC';
            $links = DatixDBQuery::PDO_fetch_all($sql, array('MainTable' => $this->getTable(), 'TargetTable' => $Table));
        }

        if(empty($links)) //no routed links forwards
        {
            $sql = 'SELECT TLK_LINK_ID, TLK_TABLE1, TLK_TABLE2, TLK_WHERE FROM TABLE_LINKS
                    JOIN TABLE_ROUTES ON TRT_LINK = TLK_LINK_ID
                    WHERE TRT_TABLE2 = :MainTable AND TRT_TABLE1 = :TargetTable ORDER BY TRT_ORDER DESC';
            $links = DatixDBQuery::PDO_fetch_all($sql, array('MainTable' => $this->getTable(), 'TargetTable' => $Table));
        }

        $currentTable = $this->getTable();

        foreach($links as $order => $link)
        {
            if($link['TLK_TABLE1'] == $currentTable)
            {
                $LinkInfo = DatixDBQuery::PDO_fetch_all('SELECT TKY_FIELD2 as targetField, TKY_FIELD1 as currentField FROM TABLE_LINK_KEYS WHERE TKY_LINK_ID = :link_id', array('link_id' => $link['TLK_LINK_ID']));

                foreach($LinkInfo as $infoId => $LinkInfoRecord)
                {
                    $LinkArray[$order][$infoId] = $LinkInfoRecord;
                    $LinkArray[$order][$infoId]['currentTable'] = $link['TLK_TABLE1'];
                    $LinkArray[$order][$infoId]['targetTable'] = $link['TLK_TABLE2'];
                    $LinkArray[$order][$infoId]['where'] = $link['TLK_WHERE'];
                }

                $currentTable = $link['TLK_TABLE2'];
            }
            else
            {
                $LinkInfo = DatixDBQuery::PDO_fetch_all('SELECT TKY_FIELD2 as currentField, TKY_FIELD1 as targetField FROM TABLE_LINK_KEYS WHERE TKY_LINK_ID = :link_id', array('link_id' => $link['TLK_LINK_ID']));

                foreach($LinkInfo as $infoId => $LinkInfoRecord)
                {
                    $LinkArray[$order][$infoId] = $LinkInfoRecord;
                    $LinkArray[$order][$infoId]['currentTable'] = $link['TLK_TABLE2'];
                    $LinkArray[$order][$infoId]['targetTable'] = $link['TLK_TABLE1'];
                    $LinkArray[$order][$infoId]['where'] = $link['TLK_WHERE'];
                }

                $currentTable = $link['TLK_TABLE1'];
            }

            $this->TableStructure->addTable($LinkArray[$order][$infoId]['targetTable'], $LinkArray[$order][$infoId]['currentTable'], array('table' => $LinkArray[$order][$infoId]['targetTable'], 'link' => $LinkArray));
        }

        foreach($LinkArray as $Link)
        {
            $TableList[] = $Link[0]['targetTable'];
        }

        //We take all of the tables that will be the target of joins and make sure that their indexes are included in the list of columns to be retrieved.
        $TableList = array_unique($TableList);

        $TableIndexData = DatixDBQuery::PDO_fetch_all('SELECT TDR_INDEX, TDR_NAME FROM TABLE_DIRECTORY WHERE TDR_NAME IN(\''.implode('\', \'', $TableList).'\')');
        foreach($TableIndexData as $IndexData)
        {
            $TableIndexes[$IndexData['TDR_NAME']] = $IndexData['TDR_INDEX'];
        }

        foreach($TableList as $Table)
        {
            $IndexPresent = false;
            if(is_array($this->TableColumnList[$Table]))
            {
                foreach($this->TableColumnList[$Table] as $Field)
                {
                    if($Field->getName() == $TableIndexes[$Table])
                    {
                        $IndexPresent = true;
                    }
                }
            }
            if(!$IndexPresent)
            {
                $IndexField = new Listings_ListingColumn($TableIndexes[$Table], $Table);
                $this->Columns[] = $IndexField;
                $this->TableColumnList[$Table][] = $IndexField;
            }
        }
    }


   /* public function getRecordValues($field)
    {
        $ValueArray = array();

        foreach($this->Records as $Record)
        {
            $ValueArray = array_merge($ValueArray, $Record->getRecordValues($field));
        }

        return array_unique($ValueArray);
    } */

    /**
    * @desc Used when displaying a human-readable count of the number of records contained in a paged listing.
    *
    * @return int The list index of the first record on the current page.
    */
    public function getStartingRecordIndex()
    {
        return $this->StartAt + 1;
    }

    /**
    * @desc Used when displaying a human-readable count of the number of records contained in a paged listing.
    *
    * @return int The list index of the last record on the current page.
    */
    public function getEndingRecordIndex()
    {
        return $this->StartAt + count($this->Records);
    }


    public function getRecordIndex($Parameters)
    {
        foreach($this->Records as $index => $Record)
        {
            foreach($Parameters as $key => $val)
            {
                if($Record->Data[$key] == $val)
                {
                    return $index;
                }
            }
        }

        return false;
    }

    /**
    * Gets the recordid for a single record in the list based on list index.
    */
    public function getRecordidByIndex($Index)
    {
        return $this->Records[$Index]->Data['recordid'];
    }
}
