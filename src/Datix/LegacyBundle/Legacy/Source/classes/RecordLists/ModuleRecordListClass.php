<?php

/**
* @desc Object to construct and hold lists of records linked to specific modules.
* Can be used in the same way as vanilla recordlists, but can handle a lot of processes independently
* because the module is known.
*/
class RecordLists_ModuleRecordList extends RecordLists_RecordList
{
    /**
    * @var string
    * @desc The module to which records in this list belong.
    */
    public $Module;

    /**
     * @var array
     * @desc If we're storing a collection of records that do not yet support extra field data, we need an additional place to hold that data.
     * This can be removed once these entities can hold udf data
     */
    public $udfData;

    /**
    * @desc Replaces the generic getTable() method in RecordLists_RecordList with one checking for the
    * module as a fallback and using the ModuleDefs default table.
    *
    * @return string The name of a database table
    */
    public function getTable()
    {
        global $ModuleDefs;

        if($this->Table)
        {
            return $this->Table;
        }
        else if($this->Module)
        {
            return $ModuleDefs[$this->Module]['VIEW'] ?: $ModuleDefs[$this->Module]['TABLE'];
        }

        return '';
    }

    /**
    * @desc Deletes all records included in this list from the database. Deletes any links and any orphaned linked records.
    *
    * @param bool $DeleteDocs If true, the documents linked to this record will be deleted, otherwise the documents will remain, and only the links will be deleted.
    */
    public function BatchDelete($DeleteDocs = false)
    {
        global $ModuleDefs;
        
        $this->RetrieveRecords();

        $NoErrors = true;

        foreach($this->Records as $Record)
        {
            DatixDBQuery::static_beginTransaction();

            InsertFullAuditRecord($this->Module, $Record->Data['recordid'], 'DELETE', 'WEB: Batch Delete ['.$this->Module.'] ('.$this->getTable().')');

            if($ModuleDefs[$this->Module]['CAN_LINK_DOCS'])
            {
                if($DeleteDocs)
                {
                    $Factory = new \src\documents\model\DocumentModelFactory();

                    //Delete Documents
                    $DocsToDelete = DatixDBQuery::PDO_fetch_all('SELECT recordid from documents_main WHERE '.$ModuleDefs[$this->Module]['FK'].' = :recordid', array('recordid' => $Record->Data['recordid']), \PDO::FETCH_COLUMN);

                    foreach($DocsToDelete as $recordid)
                    {
                        $Document = $Factory->getMapper()->find($recordid);

                        if(is_file($Document->getPath()))
                        {
                            unlink($Document->getPath());
                        }

                        try
                        {
                            $NoErrors = DatixDBQuery::PDO_query('DELETE FROM doc_documents WHERE recordid = :doc_id', array('doc_id' => $Document->doc_id));
                        }
                        catch (DatixDBQueryException $e)
                        {
                            $NoErrors = false;
                        }

                        InsertFullAuditRecord('Document', $Document->doc_id, 'DELETE', 'WEB: Batch Delete ['.$this->Module.'] (doc_documents)');
                        InsertFullAuditRecord('Document', $recordid, 'DELETE', 'WEB: Batch Delete ['.$this->Module.'] (documents_main)');
                    }
                }

                //Delete Document links.
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM documents_main WHERE '.$ModuleDefs[$this->Module]['FK'].' = :recordid', array('recordid' => $Record->Data['recordid']));
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors && $ModuleDefs[$this->Module]['CAN_LINK_NOTES'])
            {
                //Delete Notepad
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM notepad WHERE '.$ModuleDefs[$this->Module]['FK'].' = '.$Record->Data['recordid']);
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors)
            {
                //Delete Actions
                try
                {
                    $ActionsToDelete = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM ca_actions WHERE act_module = \''.$this->Module.'\' AND act_cas_id = '.$Record->Data['recordid'], array(), PDO::FETCH_COLUMN);
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM ca_actions WHERE act_module = \''.$this->Module.'\' AND act_cas_id = '.$Record->Data['recordid']);
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }

                if(!empty($ActionsToDelete))
                {
                    foreach($ActionsToDelete as $ActionID)
                    {
                        InsertFullAuditRecord('ACT', $ActionID, 'DELETE', 'WEB: Batch Delete ['.$this->Module.'] (ca_actions)');
                    }
                }
                
            }

            if($NoErrors && $ModuleDefs[$this->Module]['CAN_LINK_CONTACTS'])
            {
                //Delete contact links
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM link_contacts WHERE '.$ModuleDefs[$this->Module]['FK'].' = '.$Record->Data['recordid']);
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors && $ModuleDefs[$this->Module]['CAN_LINK_EQUPMENT'])
            {
                //Delete equipment links
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM link_assets WHERE '.$ModuleDefs[$this->Module]['FK'].' = '.$Record->Data['recordid']);
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors && $ModuleDefs[$this->Module]['CAN_LINK_MODULES'])
            {
                //Delete module links
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM link_modules WHERE '.$ModuleDefs[$this->Module]['FK'].' = '.$Record->Data['recordid']);
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors && $ModuleDefs[$this->Module]['CAN_LINK_MODULES'])
            {
                //Delete extra field data
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM udf_values WHERE mod_id = :mod_id AND cas_id = :recordid', array('recordid' => $Record->Data['recordid'], 'mod_id' => ModuleCodeToID($this->Module)));
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors && in_array($this->Module, array('ATM', 'ATI', 'AMO')))
            {
                try
                {
                    switch ($this->Module)
                    {
                        case 'ATM' : $NoErrors = $this->DeleteATMRelatedRecords($Record->Data['recordid']); break;
                        case 'ATI' : $NoErrors = $this->DeleteATIRelatedRecords($Record->Data['recordid']); break;
                        case 'AMO' : $NoErrors = $this->DeleteAMORelatedRecords($Record->Data['recordid']); break;
                    }
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors && $this->Module == 'INC')
            {
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM inc_medications WHERE inc_id = :inc_id', array('inc_id' => $Record->Data['recordid']));
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            if($NoErrors)
            {
                
                //Delete main record
                try
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM '.$this->getTable().' WHERE recordid = '.$Record->Data['recordid']);
                }
                catch (DatixDBQueryException $e)
                {
                    $NoErrors = false;
                }
            }

            // If we've had any errors, then don't commit this deletion, and mark the record as one that hasn't been deleted.
            // Otherwise commit the change.
            if($NoErrors)
            {
                DatixDBQuery::static_commit();
                $result['deleted'][] = $Record->Data['recordid'];
            }
            else
            {
                DatixDBQuery::static_rollBack();
                $result['not_deleted'][] = $Record->Data['recordid'];
            }
        }

        return $result;
    }

    /**
    * Merges/Transfers all related data for records selected in this list from the database.
    *
    * @param bool $aParam["merge_dest_id"] Destination ID.
    * @param bool $aParam["merge_documents"] If true, the documents linked to this record will be transfered.
    * @param bool $aParam["merge_linked_records"] If true, linked records will be transferred to the destination record.
    * @param bool $aParam["delete_after_merge"] If true, the source records will be deleted after the transfer.
    */
    public function BatchMerge($aParam)
    {
        return Service_BatchMerge::BatchMerge($aParam, $this);
    }
    
    
    /**
     * Function deletes Criteria (AQU) for gived Assesment (AMO)
     * 
     * @param int $recordid
     * @return bool $NoErrors
     */
    private function DeleteAMORelatedRecords($recordid)
    {
        global $ModuleDefs;
        
        $NoErrors = true;
        
        $AQUToDelete = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM ' . $ModuleDefs['AQU']['TABLE'] . ' WHERE asm_module_id = :recordid', array('recordid' => $recordid), PDO::FETCH_COLUMN);
        
        // delete Criteria (AQU)
        $NoErrors =  DatixDBQuery::PDO_query('DELETE FROM ' . $ModuleDefs['AQU']['TABLE'] . ' WHERE asm_module_id = :recordid', array('recordid' => $recordid));
        
        // delete actions for AQU
        if ($NoErrors && !empty($AQUToDelete))
        {
            $NoErrors = DatixDBQuery::PDO_query('DELETE FROM ca_actions WHERE act_module = :module AND act_cas_id IN (' . implode(', ',$AQUToDelete) . ')', array('module' => 'AQU'));
        }
        
        
        return $NoErrors;
    }

    
    /**
     * Function deletes Criteria (AQU) and Assesments (AMO) for given Assigned Assessment Template (ATI)
     * 
     * @param int $recordid
     * @return bool $NoErrors
     */
    private function DeleteATIRelatedRecords($recordid)
    {
        global $ModuleDefs;
        
        $NoErrors = true;
        
        $AMOToDelete = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM ' . $ModuleDefs['AMO']['TABLE'] . ' WHERE asm_template_instance_id = :recordid', array('recordid' => $recordid), PDO::FETCH_COLUMN);
        
        if (!empty($AMOToDelete))
        {
            foreach($AMOToDelete as $AMORecordid)
            {
                $NoErrors = $this->DeleteAMORelatedRecords($AMORecordid);

                // delete actions for AMO
                if ($NoErrors)
                {
                    $NoErrors = DatixDBQuery::PDO_query('DELETE FROM ca_actions WHERE act_module = :module AND act_cas_id = :recordid', array('module' => 'AMO', 'recordid' => $AMORecordid));
                }
                
                if (!$NoErrors)
                {
                    return $NoErrors;
                }
            }
        }

        // Delete AMO records
        $NoErrors = DatixDBQuery::PDO_query('DELETE FROM ' . $ModuleDefs['AMO']['TABLE'] . ' WHERE asm_template_instance_id = :recordid', array('recordid' => $recordid));
        
        return $NoErrors;
    }
    
    /**
     * Function deletes Criteria (AQU), Assesments (AMO), Assigned Assessment Template (ATI) for given Assessment Template (ATM)
     * 
     * @param int $recordid
     * @return bool $NoErrors
     */
    private function DeleteATMRelatedRecords($recordid)
    {
        global $ModuleDefs;
        
        $NoErrors = true;
        
        $ATIToDelete = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM ' . $ModuleDefs['ATI']['TABLE'] . ' WHERE asm_module_template_id = :recordid', array('recordid' => $recordid), PDO::FETCH_COLUMN);
        
        if (!empty($ATIToDelete))
        {
            foreach($ATIToDelete as $ATIRecordid)
            {
                $NoErrors = $this->DeleteATIRelatedRecords($ATIRecordid);
                
                if (!$NoErrors)
                {
                    return $NoErrors;
                }
            }
        }
        
        
        // Delete ATQ records
        if($NoErrors)
        {
            $NoErrors = DatixDBQuery::PDO_query('DELETE FROM ' . $ModuleDefs['ATQ']['TABLE'] . ' WHERE asm_module_template_id = :recordid', array('recordid' => $recordid));   
        }
        
        // Delete ATI records
        if($NoErrors)
        {
            $NoErrors = DatixDBQuery::PDO_query('DELETE FROM ' . $ModuleDefs['ATI']['TABLE'] . ' WHERE asm_module_template_id = :recordid', array('recordid' => $recordid));   
        }
        
        
        return $NoErrors;
    }

    /**
     * return number of Assessments to be deleted for given array of ATI recordids
     * @param array $recordsToDelete
     * @return int
     */
    public static function CountATIRelatedRecords($recordsToDelete)
    {
        global $ModuleDefs;
        
        if (empty($recordsToDelete))
        {
            return 0;    
        }

        $recordsWhereClause = ' WHERE asm_template_instance_id IN (' . implode(', ',$recordsToDelete) . ')';
        
        // Count AMO
        $AMO = DatixDBQuery::PDO_fetch('SELECT count(recordid) as num FROM ' . $ModuleDefs['AMO']['TABLE'] . $recordsWhereClause, array(), PDO::FETCH_COLUMN);
        
        return $AMO;
    }
    
    /**
     * return array of numer of records of Assesments and assigned Assessments to be deleted for given array of ATM recordids
     * @param array $recordsToDelete
     * @return array 
     */
    public static function CountATMRelatedRecords($recordsToDelete)
    {
        global $ModuleDefs;
        
        if (!empty($recordsToDelete))
        {
            $recordsWhereClause = ' WHERE asm_module_template_id IN (' . implode(', ',$recordsToDelete) . ')';
        }

        // Count ATI
        $ATI_Records = DatixDBQuery::PDO_fetch_all('SELECT recordid as num FROM ' . $ModuleDefs['ATI']['TABLE'] . $recordsWhereClause, array(), PDO::FETCH_COLUMN);
        $ATI = count($ATI_Records);
        
        // Count AMO
        $AMO = self::CountATIRelatedRecords($ATI_Records);
        
        return array(
            'ATI' => $ATI,
        	'AMO' => $AMO
        );
    }
    
    
}
