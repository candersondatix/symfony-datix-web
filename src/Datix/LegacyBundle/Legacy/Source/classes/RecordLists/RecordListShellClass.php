<?php

/**
* @desc Object to hold lists of recordids for use as lightweight stored listings.
*
* Holds records as an array of integers.
*/
class RecordLists_RecordListShell
{
    /**
    * @var array
    * @desc Array of recordids.
    */
    public $Records = array();

    /**
    * @var array
    * @desc Picks out particular records in a recordlist.
    */
    public $FlaggedRecords = array();

    /**
     * @var array
     * @desc Used to track disabled rows so that the check for all records flagged can use this to determine if all
     * available records are flagged
     */
    public $disabledRecords = array();

    public $check_all = true;

    /**
     * @desc Manually adds record objects to the list. Used if we are manually populating this object with data from a SQL query.
     *
     * @param array $DataArray Nested array, containing results of a SQL query in the form array(array('field_name' => 'value'...), array(...))
     * @param bool $flagAllRecords If set to true will flag the records.
     */
    public function AddRecordData($DataArray, $flagAllRecords = false)
    {
        foreach($DataArray as $Recordid)
        {
            $this->Records[] = $Recordid;

            if ($flagAllRecords === true)
            {
                $this->FlaggedRecords[] = $Recordid;
            }
        }
    }

    public function getRecordIndex($DataArray)
    {
        return array_search($DataArray['recordid'], $this->Records);
    }

    public function isFlagged($DataArray)
    {
        $RecordIndex = $this->getRecordIndex($DataArray);

        if($RecordIndex !== false)
        {
            return isset($this->FlaggedRecords[$RecordIndex]);
        }

        return false;
    }

    /**
     * @desc Checks if the records matches the count of flagged records and disabled records
     * @return bool
     */
    public function allRecordsFlagged()
    {
        if(count($this->Records) == (count($this->FlaggedRecords) + count($this->disabledRecords)))
        {
            return true;
        }

        return false;
    }

    /**
    * Gets the recordid for a single record in the list based on list index.
    */
    public function getRecordidByIndex($Index)
    {
        return $this->Records[$Index];
    }

    /**
    * Sets a record as "flagged"
    */
    public function FlagRecord($recordid)
    {
        $RecordIndex = $this->getRecordIndex(array('recordid' => $recordid));

        if(isset($RecordIndex))
        {
            $this->FlaggedRecords[$RecordIndex] = $this->Records[$RecordIndex];

            /**
             * If it's being flagged then make sure it's not in the disabled array
             */
            $this->unMarkDisabled($recordid);

            return true;
        }

        return false;
    }

    /**
    * Sets all records as "flagged"
    */
    public function FlagAllRecords()
    {
        $this->FlaggedRecords = $this->Records;
        $this->disabledRecords = [];
    }

    /**
    * Sets a record as not "flagged"
    */
    public function UnFlagRecord($recordid)
    {
        $RecordIndex = $this->getRecordIndex(array('recordid' => $recordid));

        if(isset($RecordIndex))
        {
            unset($this->FlaggedRecords[$RecordIndex]);
            return true;
        }

        return false;
    }

    /**
     * Sets all records as "unflagged"
     */
    public function UnFlagAllRecords()
    {
        $this->FlaggedRecords = [];
    }

    /**
     * @desc Marks a record as disabled so that it can be tracked
     * @param $recordid
     * @return bool
     */
    public function markDisabled($recordid)
    {
        $recordIndex = $this->getRecordIndex(array('recordid' => $recordid));

        if(isset($recordIndex))
        {
            $this->disabledRecords[$recordIndex] = $this->Records[$recordIndex];

            return true;
        }

        return false;
    }

    /**
     * @desc unmarks a disabled record
     * @param $recordid
     * @return bool
     */
    public function unMarkDisabled($recordid)
    {
        $recordIndex = $this->getRecordIndex(array('recordid' => $recordid));

        if(isset($recordIndex))
        {
            unset($this->disabledRecords[$recordIndex]);

            return true;
        }

        return false;
    }

    /**
     * @desc resets disabeld record array
     */
    public function UnMarkAllDisabled()
    {
        $this->disabledRecords = array();
    }

    /**
    * Sets a series of records as either flagged or unflagged in one call
    *
    * @param string $module
    * @param array $flags
    */
    public static function SetSessionFlagState()
    {
        if (is_array($_GET['flags']) && isset($_SESSION[$_GET['module']]['RECORDLIST']))
        {
            $_SESSION[$_GET['module']]['RECORDLIST']->check_all = ($_GET['listing_checkbox_select_all'] == 1 ? true : false);

            if ($_GET['listing_checkbox_select_all'] == '1' || $_GET['listing_checkbox_select_all'] == '2')
            {
                $_SESSION[$_GET['module']]['RECORDLIST']->FlagAllRecords();
            }
            elseif ($_GET['listing_checkbox_select_all'] == '0')
            {
                $_SESSION[$_GET['module']]['RECORDLIST']->UnFlagAllRecords();
            }

            foreach ($_GET['flags'] as $recordid => $state)
            {
                if ($state == '1')
                {
                    $_SESSION[$_GET['module']]['RECORDLIST']->FlagRecord($recordid);
                }
                elseif ($state == '0')
                {
                    $_SESSION[$_GET['module']]['RECORDLIST']->UnFlagRecord($recordid);

                    /**
                     * If the check_all flag is set, then add these records as disabled as this would be the only reason
                     * they would be unflagged
                     */
                    if ($_SESSION[$_GET['module']]['RECORDLIST']->check_all)
                    {
                        $_SESSION[$_GET['module']]['RECORDLIST']->markDisabled($recordid);
                    }
                }
            }
        }
    }

    /**
    * "flags" all the records in this list. Used in checkboxes on listings.
    */
    public static function FlagAllSessionRecordList()
    {
        foreach($_SESSION[$_GET['module']]['RECORDLIST']->Records as $RecordId)
        {
             $_SESSION[$_GET['module']]['RECORDLIST']->FlagRecord($RecordId);
        }
    }

    /**
    * Finds a record in a session recordlist and "flags" it. Used in checkboxes on listings.
    */
    public static function FlagSessionRecordList()
    {
        if (!isset($_SESSION[$_GET['module']]['RECORDLIST']))
        {
            $_SESSION[$_GET['module']]['RECORDLIST'] = RecordLists_RecordListShell::CreateForModule($_GET['module'], 'recordid = '.(int) $_GET['recordid']);
        }
        $RecordIndex = $_SESSION[$_GET['module']]['RECORDLIST']->FlagRecord($_GET['recordid']);
    }


    /**
    * "flags" all the records in this list. Used in checkboxes on listings.
    */
    public static function UnFlagAllSessionRecordList()
    {
        foreach($_SESSION[$_GET['module']]['RECORDLIST']->Records as $RecordId)
        {
             $_SESSION[$_GET['module']]['RECORDLIST']->UnFlagRecord($RecordId);
        }
    }

    /**
    * Finds a record in a session recordlist and "flags" it. Used in checkboxes on listings.
    */
    public static function UnFlagSessionRecordList()
    {
        $RecordIndex = $_SESSION[$_GET['module']]['RECORDLIST']->UnFlagRecord($_GET['recordid']);
    }


    public static function ClearSessionRecordList()
    {
        unset($_SESSION[$_GET['module']]['RECORDLIST']);
    }

    public static function CreateForModule($module, $whereClause, $orderBy = '', $order = 'DESC', $overdueJoin = '', $flagAllRecords = false)
    {
        global $ModuleDefs;

        if(!$order)
        {
            $order = 'DESC';
        }

        $sql = 'SELECT recordid FROM '.($ModuleDefs[$module]['VIEW'] ? $ModuleDefs[$module]['VIEW'] : $ModuleDefs[$module]['TABLE']);

        if ($_GET['overdue'] == '1' && $overdueJoin != '')
        {
            $sql.= $overdueJoin;
        }

        $whereClause = MakeSecurityWhereClause($whereClause, $module);

        if($whereClause)
        {
            $sql .= ' WHERE '.$whereClause;
        }

        if($orderBy)
        {
            $orderByArray[] = $orderBy.' '.$order;
        }

        if($orderBy != 'recordid')
        {
            $orderByArray[] = 'recordid '.$order;
        }

        $sql .= ' ORDER BY '.implode(', ', $orderByArray);

        $RecordList = new RecordLists_RecordListShell();
        $RecordList->AddRecordData(DatixDBQuery::PDO_fetch_all($sql, array(), PDO::FETCH_COLUMN), $flagAllRecords);

        return $RecordList;
    }


}
