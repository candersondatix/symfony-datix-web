<?php

class CQC_OutcomeTemplate extends CQC_Template
{

    /* the recordid of the outcome template record */
    public $recordid;

    /* outcome template record data: */
    public $data;


    public function __construct($recordid)
    {
        $this->recordid = $recordid;

        $this->data = DatixDBQuery::PDO_fetch('SELECT recordid, cto_ref, cto_desc, cto_desc_short, cto_dpub, cto_guidance, cto_theme, cto_active FROM cqc_template_outcomes WHERE recordid = :recordid', array('recordid' => $this->recordid));
    }

    public function GetID()
    {
        return $this->recordid;
    }

    public static function ListPromptTemplates($Data)
    {
        $Design = new Listings_ListingDesign(array('columns' => array(
            new Fields_DummyField(array('name' => 'ctp_ref', 'label' => 'Reference', 'type' => 'S')),
            new Fields_DummyField(array('name' => 'ctp_desc', 'label' => 'Description', 'type' => 'S')),
            new Fields_DummyField(array('name' => 'ctp_element', 'label' => 'Element', 'type' => 'C', 'codes' => DatixDBQuery::PDO_fetch_all('SELECT cod_code, cod_descr FROM code_types WHERE cod_type = \'CQCELE\'', array(), PDO::FETCH_KEY_PAIR))),
            )
        ));

        $Prompts = DatixDBQuery::PDO_fetch_all('SELECT recordid, ctp_ref, ctp_desc, ctp_element FROM cqc_template_prompts WHERE cqc_outcome_template_id = :cqc_outcome_template_id ORDER BY recordid', array('cqc_outcome_template_id' => $Data['recordid']));

        $RecordList = new RecordLists_RecordList();
        $RecordList->AddRecordData($Prompts);

        $PromptListing = new Listings_ListingDisplay($RecordList, $Design);
        $PromptListing->Action = 'editcqcprompttemplate';

        echo $PromptListing->GetListingHTML();

   }

    public function UpdateFromPost()
    {
        LoggedIn();
        CheckFullAdmin();

        $aDateValues = CheckDatesFromArray(array('cto_dpub'), $_POST);
        $aTextAreaValues = CheckTextAreaFromArray(array('cto_desc', 'cto_guidance'), '', $_POST);

        $this->data = array_merge($_POST, $aTextAreaValues, $aDateValues);
    }

    public function SaveConfigurationToDatabase()
    {
        LoggedIn();
        CheckFullAdmin();

        if(!$this->recordid)
        {
            $this->recordid = GetNextRecordID('cqc_template_outcomes', true);
        }

        $sql = 'UPDATE cqc_template_outcomes SET ';

        $sql .= GeneratePDOSQLFromArrays(array(
                            'FieldArray' => array('cto_ref', 'cto_desc', 'cto_desc_short', 'cto_dpub', 'cto_guidance', 'cto_theme', 'cto_active'),
                            'DataArray' => $this->data,
                            'Module' => 'CQO',
                            'end_comma' => false
                            ), $PDOParamsArray);

        $sql .= ' WHERE recordid = :recordid';

        $PDOParamsArray['recordid'] = $this->recordid;

        DatixDBQuery::PDO_query($sql, $PDOParamsArray);
    }

}