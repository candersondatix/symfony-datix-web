<?php

class CQC_Subprompt extends Records_NestedRecord
{
    public function __construct($Recordid)
    {
        $Recordid = Sanitize::SanitizeInt($Recordid);
        $this->Data = DatixDBQuery::PDO_fetch('SELECT * FROM cqc_subprompts WHERE recordid = :recordid', array('recordid' => $Recordid));

    }


    public static function Display()
    {
        global $DatixView;

        if($_GET['print'])
        {
            $FormType = 'Print';
        }
        else
        {
            $FormType = 'Edit';
        }

        $SubPrompt = new CQC_Subprompt($_GET['recordid']);

        $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'CQS', 'level' => 2));

        $SubPromptForm = new FormTable($FormType, 'CQS', $FormDesign);

        include('Source/generic_modules/CQS/BasicForm2.php');

        $SubPromptForm->MakeForm($FormArray, $SubPrompt->Data, 'CQS');

        $DatixView->SubpromptForm = $SubPromptForm;



        // create page
        getPageTitleHTML(array(
             'title' => $FormDesign->FormTitle,
             'module' => 'CQS',
        ));

        GetSideMenuHTML(array(
            'module' => 'CQS',
            'table' => $SubPromptForm
        ));

        template_header();

        echo $DatixView->render('classes/CQC/SubpromptClass/Display.php');

        if ($FormType != "Print")
        {
            echo JavascriptPanelSelect($FormDesign->Show_all_section, Sanitize::SanitizeString($_GET['panel']), $SubPromptForm);
        }

        footer();

        obExit();
    }




}