<?php

class CQC_SubpromptTemplate extends CQC_Template
{

    /* the recordid of the outcome template record */
    public $recordid;

    /* outcome template record data: */
    public $data;


    public function __construct($recordid)
    {
        $this->recordid = $recordid;

        $this->data = DatixDBQuery::PDO_fetch('SELECT recordid, cqc_prompt_template_id, cts_desc, cts_desc_short, cts_guidance, cts_ref FROM cqc_template_subprompts WHERE recordid = :recordid', array('recordid' => $this->recordid));
    }

    public function GetID()
    {
        return $this->recordid;
    }

    public function UpdateFromPost()
    {
        LoggedIn();
        CheckFullAdmin();

        $this->data = $_POST;
    }

    public function SaveConfigurationToDatabase()
    {
        LoggedIn();
        CheckFullAdmin();

        if(!$this->recordid)
        {
            $this->recordid = GetNextRecordID('cqc_template_subprompts', true);
        }

        $sql = 'UPDATE cqc_template_subprompts SET ';

        $sql .= GeneratePDOSQLFromArrays(array(
                            'FieldArray' => array('cqc_prompt_template_id', 'cts_ref', 'cts_desc', 'cts_desc_short', 'cts_guidance'),
                            'DataArray' => $this->data,
                            'Module' => 'CQP',
                            'end_comma' => false
                            ), $PDOParamsArray);

        $sql .= ' WHERE recordid = :recordid';

        $PDOParamsArray['recordid'] = $this->recordid;

        DatixDBQuery::PDO_query($sql, $PDOParamsArray);
    }

}