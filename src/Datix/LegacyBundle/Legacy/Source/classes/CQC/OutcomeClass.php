<?php

class CQC_Outcome extends Records_NestedRecord
{
    public function __construct($Recordid)
    {
        $Recordid = Sanitize::SanitizeInt($Recordid);
        $this->Data = DatixDBQuery::PDO_fetch('SELECT * FROM cqc_outcomes WHERE recordid = :recordid', array('recordid' => $Recordid));

    }


    public static function Display()
    {
        global $DatixView;

        if($_GET['print'])
        {
            $FormType = 'Print';
        }
        else
        {
            $FormType = 'Edit';
        }

        $Outcome = new CQC_Outcome($_GET['recordid']);

        $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'CQO', 'level' => 2));

        $OutcomeForm = new FormTable($FormType, 'CQO', $FormDesign);

        include('Source/generic_modules/CQO/BasicForm2.php');

        $OutcomeForm->MakeForm($FormArray, $Outcome->Data, 'CQO');

        $DatixView->OutcomeForm = $OutcomeForm;



        // create page
        getPageTitleHTML(array(
             'title' => $FormDesign->FormTitle,
             'module' => 'CQO',
        ));

        GetSideMenuHTML(array(
            'module' => 'CQO',
            'table' => $OutcomeForm
        ));

        template_header();

        echo $DatixView->render('classes/CQC/OutcomeClass/Display.php');

        if ($FormType != "Print")
        {
            echo JavascriptPanelSelect($FormDesign->Show_all_section, Sanitize::SanitizeString($_GET['panel']), $OutcomeForm);
        }

        footer();

        obExit();
    }

    static public function LinkedPromptSection($ID)
    {
        global $DatixView;

        $Outcome = new CQC_Outcome($ID);

        require_once 'Source/libs/ListingClass.php';
        $Design = new Listings_ModuleListingDesign(array('module' => 'CQP', 'parent_module' => 'CQO', 'link_type' => 'prompts'));
        $Design->LoadColumnsFromDB();

        $RecordList = new RecordLists_ModuleRecordList();
        $RecordList->Module = 'CQP';
        $RecordList->Columns = $Design->Columns;
        $RecordList->WhereClause = MakeSecurityWhereClause('cqc_outcome_id = '.$Outcome->Data['recordid'], 'CQP');
        $RecordList->OrderBy = array(new Listings_ListingColumn('recordid', 'cqc_prompts'));
        $RecordList->Paging = false;
        $RecordList->RetrieveRecords();

        $DatixView->ListingDisplay = new Listings_ListingDisplay($RecordList, $Design);

        echo $DatixView->render('classes/CQC/OutcomeClass/LinkedPromptSection.php');
    }

    /**
    * Retrieves the evidence listing section of the form.
    *
    * @param array  $data     The form data.
    * @param string $formType The mode the form is in.
    * @param string $module   The module we're in.
    */
    public static function listEvidence($data, $formType, $module)
    {
        require_once 'Source/standards/EvidenceForm.php';
        ListLinkedEvidence($module, $data['recordid'], $formType);
    }
}