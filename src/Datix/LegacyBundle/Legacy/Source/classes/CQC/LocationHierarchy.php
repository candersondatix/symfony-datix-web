<?php

class CQC_LocationHierarchy
{

    static public function index()
    {
        LoggedIn();
        
        global $DatixView, $dtxtitle, $MinifierDisabled;

        $dtxtitle = _tk('cqc_location_setup');

        $addMinExtension = ($MinifierDisabled ? '' : '.min');

        $locationMapper = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);
        $locations = $locationMapper->findAll();

        if (count($locations) > 0)
        {
            $depth = max(array_map(function($node) {
                return $node['depth'];
            }, $locations));
        } else {
            $depth = 0;
        }

        $tiers = DatixDBQuery::PDO_fetch_all('SELECT * FROM location_tiers ORDER BY lti_depth ASC');

        $outcomeCounts = self::_getOutcomeCounts();

        $DatixView->outcomes_assigned = false;
        foreach ($locations as &$location) {
            if (array_key_exists($location['recordid'], $outcomeCounts)) {
                $location['deletable'] = false;
            } else {
                $location['deletable'] = true;
            }
            //Check if any location at the lowest tier have any outcomes assigned. Used to hide New tier option.
            if ($location['depth'] == $depth)
            {
               if ($outcomeCounts[$location['recordid']] > 0)
               {
                  $DatixView->outcomes_assigned = true;
               }
            }
        }

        foreach ($tiers as &$tier) {
            $tier['deletable'] = (count($locationMapper->getNodesByDepth($tier['lti_depth'])) < 1);
        }

        $DatixView->locations = self::_nest($locations);
        $DatixView->depth     = $depth;
        $DatixView->data      = json_encode($DatixView->locations);
        $DatixView->tiers     = $tiers;
        $DatixView->addMinExtension = $addMinExtension;

        getPageTitleHTML(array(
             'title' => $dtxtitle,
             'module' => 'CQO',
        ));

        GetSideMenuHTML(array(
            'module' => 'ADM',
        ));

        template_header();
        echo $DatixView->render('classes/CQC/LocationHierarchy/index.php');
        footer();
        obExit();
    }

    static public function save()
    {
        LoggedIn();
        
        $locationMapper = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);
        $name = Sanitize::SanitizeRaw($_POST['name']);

        if (!is_null($_POST['parentId']))
        {
            $parentId = Sanitize::SanitizeInt($_POST['parentId']);
        }

        $id = $locationMapper->insert($name, $parentId);

        if ($id)
        {
            echo json_encode(array(
                'success' => true,
                'id' => $id
            ));
        }
        else
        {
            echo json_encode(array('success' => false));
        }

        exit();
    }

    static public function newLocation()
    {
        LoggedIn();
        
        global $DatixView, $txt;

        $DatixView->txt = $txt;
        echo $DatixView->render('classes/CQC/LocationHierarchy/newNodeDialogue.php');
        exit();
    }

    static public function editLocation()
    {
        LoggedIn();
        
        $name  = Sanitize::SanitizeString($_POST['name']);
        $id = Sanitize::SanitizeInt($_POST['id']);

        $sql = 'UPDATE locations_main SET loc_name = :name WHERE recordid = :id';
        $success = DatixDBQuery::PDO_query($sql, array(
            'name' => $name,
            'id' => $id
        ));

        echo json_encode(array('success' => $success)); exit();
    }

    static public function addTier()
    {
        LoggedIn();
        
        $depth = Sanitize::SanitizeInt($_POST['depth']);
        $success = null;

        $sql = 'SELECT * FROM location_tiers WHERE lti_depth = :depth';
        $result = DatixDBQuery::PDO_fetch($sql, array('depth' => $depth));

        if (empty($result))
        {
            $sql = 'INSERT INTO location_tiers (lti_depth, lti_name) VALUES (:depth, :name)';
            $success = DatixDBQuery::PDO_query($sql, array(
                'depth' => $depth,
                'name'  => $_POST['name']
            ));
        }
        else
        {
            $sql = 'UPDATE location_tiers SET lti_name = :name WHERE lti_depth = :depth';
            $success = DatixDBQuery::PDO_query($sql, array(
                'depth' => $depth,
                'name'  => $_POST['name']
            ));
        }

        echo json_encode(array('success' => $success)); exit();
    }

    static public function deleteLocation()
    {
        LoggedIn();
        
        $id  = Sanitize::SanitizeInt($_POST['id']);
        $locationMapper = System_NestedSetMapperFactory::getNestedSetMapper(System_NestedSetMapperFactory::LOCATIONS);
        $success = $locationMapper->deleteNode($id);

        echo json_encode(array('success' => $success)); exit();
    }

    static public function deleteTier()
    {
        LoggedIn();
        
        $depth = Sanitize::SanitizeInt($_POST['depth']);
        $db = new DatixDBQuery('');
        $params = array('depth' => $depth);
        $success = false;

        // $db->beginTransaction();

        // delete tier
        $db->setSQL('DELETE FROM location_tiers WHERE lti_depth = :depth');
        $success = $db->prepareAndExecute($params);

        if (!$success) {
            echo json_encode(array('success' => $success)); exit();
        } else {
            // re-order any further tiers
            $db->setSQL('UPDATE location_tiers SET lti_depth = lti_depth - 1 WHERE lti_depth > :depth');
            $success = $db->prepareAndExecute($params);

            echo json_encode(array('success' => $success)); exit();
        }
    }

    static private function _getOutcomeCounts()
    {
        $outcomeCounts = DatixDBQuery::PDO_fetch_all('SELECT cdo_location, COUNT(*) AS \'outcomes\' FROM fn_cqc_rollup(null) GROUP BY cdo_location');
        $outcomeCounts2 = DatixDBQuery::PDO_fetch_all('SELECT cdo_location, COUNT(*) AS \'outcomes\' FROM cqc_outcomes GROUP BY cdo_location');

        $outcomeCounts = array_merge($outcomeCounts, $outcomeCounts2);

        $return = array();

        foreach($outcomeCounts as $c) {
            $return[$c['cdo_location']] = $c['outcomes'];
        }

        return $return;
    }

    static private function _nest(array $collection)
    {
        // Trees mapped
        $trees = array();
        $l = 0;

        if (count($collection) > 0) {
            // Node Stack. Used to help building the hierarchy
            $stack = array();

            foreach ($collection as $node) {
                $item = $node;
                $item['children'] = array();

                // Number of stack items
                $l = count($stack);

                // Check if we're dealing with different levels
                while($l > 0 && $stack[$l - 1]['depth'] >= $item['depth']) {
                    array_pop($stack);
                    $l--;
                }

                // Stack is empty (we are inspecting the root)
                if ($l == 0) {
                    // Assigning the root node
                    $i = count($trees);
                    $trees[$i] = $item;
                    $stack[] = & $trees[$i];
                } else {
                    // Add node to parent
                    $i = count($stack[$l - 1]['children']);
                    $stack[$l - 1]['children'][$i] = $item;
                    $stack[] = & $stack[$l - 1]['children'][$i];
                }
            }
        }

        return $trees;
    }
}
