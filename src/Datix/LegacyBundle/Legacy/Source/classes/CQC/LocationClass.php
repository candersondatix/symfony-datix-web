<?php

class CQC_Location
{

    protected $ID;
    protected $Name;
    protected $Depth;
    protected $OutcomeLevel = false;   //Whether this location belongs to the lowest tier.

    public function __construct($ID)
    {
        $this->ID = $ID;

        $LocationData = DatixDBQuery::PDO_fetch('SELECT loc_name, loc_tier_depth, max_tier_depth = (SELECT MAX(loc_tier_depth) from vw_locations_main) from vw_locations_main WHERE recordid = :recordid', array('recordid' => $this->ID));

        $this->Name = $LocationData['loc_name'];
        $this->Depth = $LocationData['loc_tier_depth'];
        $this->OutcomeLevel = ($LocationData['loc_tier_depth'] == $LocationData['max_tier_depth']);
    }

    public function GetID()
    {
        return $this->ID;
    }

    public function CanAttachOutcomes()
    {
        return $this->OutcomeLevel;
    }

    static public function ListOutcomes($Data, $FormType, $Module)
    {
        global $DatixView, $scripturl;

        $Design = new Listings_ListingDesign(array('columns' => array(
            new Fields_DummyField(array('name' => 'cto_ref', 'label' => 'Outcome', 'type' => 'S')),
            new Fields_DummyField(array('name' => 'cto_desc', 'label' => 'Description', 'type' => 'S')),
            new Fields_DummyField(array('name' => 'cdo_status', 'label' => 'Status', 'type' => 'C', 'codefield' => 'cdo_status')),
            )
        ));

        $Location = new CQC_Location($Data['recordid']);

        if ($Location->CanAttachOutcomes())
        {
            $Outcomes = DatixDBQuery::PDO_fetch_all('SELECT recordid, cto_ref, cto_desc, cdo_status FROM cqc_outcomes WHERE cdo_location = :location ORDER BY recordid',
                                                    array('location' => $Data['recordid']));

            $RecordList = new RecordLists_RecordList();
            $RecordList->AddRecordData($Outcomes);

            $OutcomeListing = new Listings_ListingDisplay($RecordList, $Design);
            $OutcomeListing->RecordList->Module = 'CQO';
        }
        else
        {
            $Outcomes = DatixDBQuery::PDO_fetch_all(
                    'SELECT cqc_outcome_template_id as recordid, cto_ref, cto_desc, CODE_CQC_STATUS.code as cdo_status, \'CQO\' as module,
                    \'tier_' . $Data["loc_tier_depth"] .'\' as filter_field_0, cdo_location as filter_value_0,
                    \'cto_ref\' as filter_field_1, cto_ref as filter_value_1, 2 as filter_max_suffix
                    FROM fn_cqc_rollup(:location) CQC_rollup
                    LEFT OUTER JOIN dbo.CQC_TEMPLATE_OUTCOMES AS CQC_T_O ON CQC_T_O.recordid = CQC_rollup.cqc_outcome_template_id
                    LEFT OUTER JOIN dbo.vw_locations_main ON dbo.vw_locations_main.recordid = CQC_rollup.cdo_location
                    LEFT OUTER JOIN dbo.CODE_CQC_STATUS ON dbo.CODE_CQC_STATUS.cod_score = CQC_rollup.score
                    WHERE cdo_location = :cdo_location ORDER BY recordid',
                    array('location' => $Data['recordid'], 'cdo_location' => $Data['recordid']));

            $RecordList = new RecordLists_RecordList();
            $RecordList->AddRecordData($Outcomes);

            $OutcomeListing = new Listings_ListingDisplay($RecordList, $Design);
            $OutcomeListing->Action = 'dofilter';
            $OutcomeListing->URLParameterArray = array('module', 'filter_max_suffix','filter_field_0', 'filter_value_0','filter_field_1', 'filter_value_1');
        }
        $DatixView->scripturl = $scripturl;
        $DatixView->Location = $Location;
        $DatixView->OutcomeListing = $OutcomeListing;

        echo $DatixView->render('classes/CQC/LocationClass/ListOutcomes.php');
    }

    public static function GetLowestLocations()
    {
        return DatixDBQuery::PDO_fetch_all('SELECT recordid, loc_name from vw_locations_main WHERE loc_tier_depth = (SELECT MAX(loc_tier_depth) from vw_locations_main)', array(), PDO::FETCH_KEY_PAIR);
    }
}

