<?php

class CQC_Prompt extends Records_NestedRecord
{
    public function __construct($Recordid)
    {
        $Recordid = Sanitize::SanitizeInt($Recordid);
        $this->Data = DatixDBQuery::PDO_fetch('SELECT * FROM cqc_prompts WHERE recordid = :recordid', array('recordid' => $Recordid));

    }


    public static function Display()
    {
        global $DatixView;

        if($_GET['print'])
        {
            $FormType = 'Print';
        }
        else
        {
            $FormType = 'Edit';
        }

        $Prompt = new CQC_Prompt($_GET['recordid']);

        $FormDesign = Forms_FormDesign::GetFormDesign(array('module' => 'CQP', 'level' => 2));

        $PromptForm = new FormTable($FormType, 'CQP', $FormDesign);

        include('Source/generic_modules/CQP/BasicForm2.php');

        $PromptForm->MakeForm($FormArray, $Prompt->Data, 'CQP');

        $DatixView->PromptForm = $PromptForm;



        // create page
        getPageTitleHTML(array(
             'title' => $FormDesign->FormTitle,
             'module' => 'CQP',
        ));

        GetSideMenuHTML(array(
            'module' => 'CQP',
            'table' => $PromptForm
        ));

        template_header();

        echo $DatixView->render('classes/CQC/PromptClass/Display.php');

        if ($FormType != "Print")
        {
            echo JavascriptPanelSelect($FormDesign->Show_all_section, Sanitize::SanitizeString($_GET['panel']), $PromptForm);
        }

        footer();

        obExit();
    }

    static public function LinkedSubpromptSection($ID)
    {
        global $DatixView;

        $Prompt = new CQC_Prompt($ID);

        require_once 'Source/libs/ListingClass.php';
        $Design = new Listings_ModuleListingDesign(array('module' => 'CQS', 'parent_module' => 'CQP', 'link_type' => 'subprompts'));
        $Design->LoadColumnsFromDB();

        $RecordList = new RecordLists_ModuleRecordList();
        $RecordList->Module = 'CQS';
        $RecordList->Columns = $Design->Columns;
        $RecordList->WhereClause = MakeSecurityWhereClause('cqc_prompt_id = '.$Prompt->Data['recordid'], 'CQS');
        $RecordList->OrderBy = array(new Listings_ListingColumn('recordid', 'cqc_subprompts'));
        $RecordList->Paging = false;
        $RecordList->RetrieveRecords();

        $DatixView->ListingDisplay = new Listings_ListingDisplay($RecordList, $Design);

        echo $DatixView->render('classes/CQC/PromptClass/LinkedSubpromptSection.php');
    }





}