<?php

class CQC_Template extends Records_NestedRecord
{
    /**
    * Having selected locations and outcome templates, this function checks whether we need to add a page warning about duplicates.
    */
    public static function CheckDuplicateOutcomes()
    {
        global $scripturl, $dtxtitle, $DatixView;

        if(empty($_POST['outcomes']))
        {
            AddSessionMessage('ERROR', 'Please select at least one outcome');
            
            // using here loader instead of standard redirect, becase of POST params
            $loader = new src\framework\controller\Loader();
            $controller = $loader->getController(array(
            	'controller' => 'src\\admin\\cqc\\controllers\\ApplyCqcOutcomesController',
            ));
            echo $controller->doAction('cqcselectoutcomes');
            
            // header('Location: app.php?action=cqcselectoutcomes');
            obExit();
        }

        if(empty($_POST['prompts']))
        {
            //shouldn't ever be triggered - just here for completeness
            AddSessionMessage('ERROR', 'Please select at least one prompt');
            header('Location: app.php?action=cqcselectoutcomes&token='.CSRFGuard::getCurrentToken());
            obExit();
        }

        foreach($_POST['locations'] as $Location)
        {
            foreach($_POST['outcomes'] as $Outcome)
            {
                $PairsToCreate[] = array('location' => $Location, 'outcome' => $Outcome);
                $SQLPairs[] = 'cdo_location = \''.$Location.'\' AND cqc_outcome_template_id = \''.$Outcome.'\'';
            }
        }

        $sql = 'SELECT recordid, cdo_location, cqc_outcome_template_id FROM cqc_data_outcomes WHERE ('.implode(') OR (', $SQLPairs).')';

        $Duplicates = DatixDBQuery::PDO_fetch_all($sql);

        if(!empty($Duplicates))
        {
            CQC_Template::AlertForDuplicates($Duplicates);
        }
        else
        {
            CQC_Template::GenerateOutcomes();
        }

    }

    /**
    * Displays a warning about duplicates
    *
    * @param array $Duplicates List of location-outcome combinations that already exist in the db.
    */
    public static function AlertForDuplicates($Duplicates)
    {
        global $DatixView, $dtxtitle, $scripturl;

        $dtxtitle = 'Duplicates found';

        GetPageTitleHTML(array(
             'title' => $dtxtitle,
             'module' => 'CQO'
             ));
        GetSideMenuHTML(array('module' => 'CQO'));

        template_header_nopadding();

        if(count($Duplicates) >= count($_POST['locations'])*count($_POST['outcomes']))
        {
            $DatixView->AllDuplicates = true;
        }

        $DatixView->Locations = DatixDBQuery::PDO_fetch_all('SELECT recordid, loc_name FROM locations_main WHERE recordid IN('.implode(', ',$_POST['locations']).')', array(), PDO::FETCH_KEY_PAIR);
        $DatixView->Outcomes = DatixDBQuery::PDO_fetch_all('SELECT recordid, cto_desc FROM cqc_template_outcomes WHERE recordid IN('.implode(', ',$_POST['outcomes']).')', array(), PDO::FETCH_KEY_PAIR);
        $DatixView->Prompts = $_POST['prompts'];
        $DatixView->Duplicates = $Duplicates;
        $DatixView->scripturl = $scripturl;

        echo $DatixView->render('classes/CQC/TemplateClass/AlertForDuplicates.php');

        footer();
        obExit();
    }

    /**
    * Generates a record in cqc_data_outcomes for each combination of location and outcome template
    * that is not a duplicate.
    * 
    * @global array $ModuleDefs
    */
    public static function GenerateOutcomes()
    {
    	global $ModuleDefs;

        $OutcomeSQL = 'INSERT INTO cqc_data_outcomes (cqc_outcome_template_id, cdo_location, updateddate, updatedby)
                ';

        $PromptSQL = 'INSERT INTO cqc_data_prompts (cqc_outcome_id, cqc_prompt_template_id, updateddate, updatedby)
                ';

        $SubpromptSQL = 'INSERT INTO cqc_data_subprompts (cqc_prompt_id, cqc_subprompt_template_id, updateddate, updatedby)
                ';
        
        foreach($_POST['locations'] as $Location)
        {
            foreach($_POST['outcomes'] as $Outcome)
            {
                if(empty($_POST['duplicates']) || !in_array($Location.'|'.$Outcome, $_POST['duplicates']))
                {
                    $OutcomeSelects[] = 'SELECT '.$Outcome.', '.$Location.', \''.GetTodaysDate().'\', \''.$_SESSION['initials'].'\'';
                    $OutcomePairs[] = 'cqc_data_outcomes.cdo_location = \''.$Location.'\' AND cqc_data_outcomes.cqc_outcome_template_id = \''.$Outcome.'\'';
                }
            }
        }

        if(!empty($OutcomeSelects))
        {
            $OutcomeSQL .= implode(' UNION ALL ', $OutcomeSelects);

            DatixDBQuery::PDO_query($OutcomeSQL);

            $OutcomeIDs = DatixDBQuery::PDO_fetch_all('SELECT cqc_template_prompts.recordid as prompt_template_id, cqc_data_outcomes.recordid as outcome_id FROM cqc_template_prompts JOIN cqc_data_outcomes ON cqc_template_prompts.cqc_outcome_template_id = cqc_data_outcomes.cqc_outcome_template_id WHERE ('.implode(') OR (', $OutcomePairs).')');

            $outcomes = array(); // store generated outcome IDs for use when generating triggers
            foreach ($OutcomeIDs as $PromptDetails)
            {
            	if (!in_array($PromptDetails['outcome_id'], $outcomes))
            	{
            		$outcomes[] = $PromptDetails['outcome_id'];
            	}
            	
                if(in_array($PromptDetails['prompt_template_id'], $_POST['prompts']))
                {
                    $PromptSelects[] = 'SELECT '.$PromptDetails['outcome_id'].', '.$PromptDetails['prompt_template_id'].', \''.GetTodaysDate().'\', \''.$_SESSION['initials'].'\'';
                    $PromptPairs[] = 'cqc_data_prompts.cqc_outcome_id = \''.$PromptDetails['outcome_id'].'\' AND cqc_data_prompts.cqc_prompt_template_id = \''.$PromptDetails['prompt_template_id'].'\'';
                }
            }

            $PromptSQL .= implode(' UNION ALL ', $PromptSelects);

            DatixDBQuery::PDO_query($PromptSQL);

            $PromptIDs = DatixDBQuery::PDO_fetch_all('SELECT cqc_template_subprompts.recordid as subprompt_template_id, cqc_data_prompts.recordid as prompt_id FROM cqc_data_prompts INNER JOIN cqc_template_subprompts ON cqc_template_subprompts.cqc_prompt_template_id = cqc_data_prompts.cqc_prompt_template_id WHERE ('.implode(') OR (', $PromptPairs).')');

            foreach ($PromptIDs as $SubpromptDetails)
            {
                $SubpromptSelects[] = 'SELECT '.$SubpromptDetails['prompt_id'].', '.$SubpromptDetails['subprompt_template_id'].', \''.GetTodaysDate().'\', \''.$_SESSION['initials'].'\'';
            }

            $SubpromptSQL .= implode(' UNION ALL ', $SubpromptSelects);

            DatixDBQuery::PDO_query($SubpromptSQL);
			
            // process triggers for all three modules
            if (bYN(GetParm("WEB_TRIGGERS","N")))
            {
            	require_once "Source/libs/Triggers.php";
            	foreach ($outcomes as $outcome)
            	{
            		ExecuteTriggers($outcome, array('recordid' => $outcome), $ModuleDefs['CQO']["MOD_ID"]);
            		
            		$prompts = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM cqc_prompts WHERE cqc_outcome_id = :outcomeid', array('outcomeid' => $outcome), PDO::FETCH_COLUMN);
            		foreach ($prompts as $prompt)
            		{
            			ExecuteTriggers($prompt, array('recordid' => $prompt), $ModuleDefs['CQP']["MOD_ID"]);
            		}
            		
            		$subprompts = DatixDBQuery::PDO_fetch_all('SELECT recordid FROM cqc_subprompts WHERE cqc_outcome_id = :outcomeid', array('outcomeid' => $outcome), PDO::FETCH_COLUMN);
            		foreach ($subprompts as $subprompt)
            		{
            			ExecuteTriggers($subprompt, array('recordid' => $subprompt), $ModuleDefs['CQS']["MOD_ID"]);
            		}
            	}
            }
            
            AddSessionMessage('INFO', count($OutcomeSelects).' outcomes have been successfully created');
            header('Location: app.php?action=cqcselectlocations&token='.CSRFGuard::getCurrentToken());
            obExit();
        }
        else
        {
            AddSessionMessage('INFO', 'No outcomes were created');
            header('Location: app.php?action=cqcselectlocations&token='.CSRFGuard::getCurrentToken());
            obExit();
        }
    }

}
