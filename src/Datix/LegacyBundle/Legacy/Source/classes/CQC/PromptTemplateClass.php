<?php

class CQC_PromptTemplate extends CQC_Template
{

    /* the recordid of the outcome template record */
    public $recordid;

    /* outcome template record data: */
    public $data;


    public function __construct($recordid)
    {
        $this->recordid = $recordid;

        $this->data = DatixDBQuery::PDO_fetch('SELECT recordid, cqc_outcome_template_id, ctp_desc, ctp_desc_short, ctp_element, ctp_guidance, ctp_ref FROM cqc_template_prompts WHERE recordid = :recordid', array('recordid' => $this->recordid));
    }

    public function GetID()
    {
        return $this->recordid;
    }

    public static function ListSubpromptTemplates($Data)
    {
        $Design = new Listings_ListingDesign(array('columns' => array(
            new Fields_DummyField(array('name' => 'cts_ref', 'label' => 'Reference', 'type' => 'S')),
            new Fields_DummyField(array('name' => 'cts_desc', 'label' => 'Description', 'type' => 'S')),
            )
        ));

        $Prompts = DatixDBQuery::PDO_fetch_all('SELECT recordid, cts_ref, cts_desc FROM cqc_template_subprompts WHERE cqc_prompt_template_id = :cqc_prompt_template_id ORDER BY recordid', array('cqc_prompt_template_id' => $Data['recordid']));

        $RecordList = new RecordLists_RecordList();
        $RecordList->AddRecordData($Prompts);

        $PromptListing = new Listings_ListingDisplay($RecordList, $Design);
        $PromptListing->Action = 'editcqcsubprompttemplate';

        echo $PromptListing->GetListingHTML();

   }

    public function UpdateFromPost()
    {
        LoggedIn();
        CheckFullAdmin();

        $this->data = $_POST;
    }

    public function SaveConfigurationToDatabase()
    {
        LoggedIn();
        CheckFullAdmin();

        if(!$this->recordid)
        {
            $this->recordid = GetNextRecordID('cqc_template_prompts', true);
        }

        $sql = 'UPDATE cqc_template_prompts SET ';

        $sql .= GeneratePDOSQLFromArrays(array(
                            'FieldArray' => array('ctp_desc', 'ctp_desc_short', 'ctp_element', 'ctp_guidance', 'ctp_ref'),
                            'DataArray' => $this->data,
                            'Module' => 'CQP',
                            'end_comma' => false
                            ), $PDOParamsArray);

        $sql .= ' WHERE recordid = :recordid';

        $PDOParamsArray['recordid'] = $this->recordid;

        DatixDBQuery::PDO_query($sql, $PDOParamsArray);
    }

}