<?php
/**
* @desc Class which can be used for escaping (encoding) data for prevention of XSS etc.
*/
class Escape
{
    /**
     * Escape function similar to htmlentities to encode special characters
     * 
     * @param string $string data to escape.
     * @return escaped/encoded string
     */
    static public function EscapeEntities($string )
    {
        return filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_ENCODE_AMP);
    } 

}