<?php
/*
* Breadcrumb trail class
*
* This class allows the output of a breadcrumb type trail to be displayed to the user for easy navigation when 
* there are several levels of hierarchy. The root level is 0 and as the user drills down a hierarchy, then another 
* level crumb should be added for the user to go back up to any level they have come from.
* If a level is inserted, all child levels under the current level is cleared.
* 
*/

class BreadCrumbs{

   var $output;
   var $crumbs = array();
   var $location;

   /**
   * Constructor
   * 
   */  
   function __construct()
   {   
      if ($_SESSION['breadcrumb'] != null)
      {
         $this->crumbs = $_SESSION['breadcrumb'];
      }   
   }

    /**
    * Add a crumb
    * 
    * @param string $label Label for the level
    * @param string $url URL for the level
    * @param int $level Level in the trail.
    */
   function add($label, $url, $level)
   {
      $crumb = array();
      $crumb['label'] = $label;
      $crumb['url'] = $url;

      if ($crumb['label'] != null && $crumb['url'] != null && isset($level))
      {                
         while(count($this->crumbs) > $level)
         {
            array_pop($this->crumbs); //clear levels below current
         }

         $this->crumbs[$level] = $crumb;  
               
      }

        $_SESSION['breadcrumb'] = $this->crumbs; //Persist the data
   }

   /**
   * Output breadcrumb trail.
   * 
   * @return string $html
   */
   function output()
   {
      $html = "<div id='breadcrumb' class='breadcrumb'><h1>";

      $lastlink = count($this->crumbs) - 1;
      $currentlink = 0;
      foreach ($this->crumbs as $crumb)
      {  
         if ($crumb['url'] != null)
         {
             if ($currentlink < $lastlink)
             {
                $exploded_crumbs[] = "<a href='".$crumb['url']."' title='".$crumb['label']."'>".$crumb['label']."</a>";
             }
             else
             {
                $exploded_crumbs[] = $crumb['label'];
             }
         } 
         $currentlink++;
      }
      
      if (is_array($exploded_crumbs))
      {
            $html .= implode(" >> ", $exploded_crumbs);
      }
      
      $html .= "</h1></div>";
      
      return $html;
   }
}