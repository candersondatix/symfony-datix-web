<?php


// English language string constants for DATIXWeb

// Modules

$txt["mod_incidents_title"] = "Incidents";
$txt["mod_risks_title"] = "Risk Register";
$txt["mod_claims_title"] = "Claims";
$txt["mod_complaints_title"] = "Complaints";
$txt["mod_pals_title"] = "PALS";
$txt["mod_sabs_title"] = "Safety Alerts";
$txt["mod_standards_title"] = "Standards";
$txt["mod_elements_title"] = "Elements";
$txt["mod_prompt_title"] = "Prompts for Compliance";
$txt["mod_library_title"] = "Library";
$txt["mod_actions_title"] = "Actions";
$txt["mod_distlists_title"] = "Distribution Lists";
$txt["mod_assets_title"] = "Equipment";
$txt["mod_contacts_title"] = "Contacts";
$txt["mod_messages_title"] = "Secure Messages";
$txt["mod_admin_short_title"] = "Admin";
$txt["mod_templates_title"] = "Templates";
$txt["mod_medications_title"] = "Medications";
$txt["mod_payments_title"] = "Payments";
$txt["mod_policies_title"] = "Policies";
$txt["mod_organisations_title"] = "Organisations";
$txt["mod_hotspotagents_title"] = "Hotspot Agents";
$txt["mod_hotspots_title"] = "Hotspots";
$txt["mod_todo_title"] = "To Do List";

//Top Menu
$txt['top_menu_new_form'] = 'New Form';
$txt['top_menu_login'] = 'Login';
$txt['top_menu_register'] = 'Register';
$txt['top_menu_logout'] = 'Logout';
$txt['top_menu_main_menu'] = 'Main Menu';

// Incidents module
//
$txt["INCName"] = "incident";
$txt["INCNames"] = "incidents";
$txt["INCNameTitle"] = "Incident";
$txt["INCNamesTitle"] = "Incidents";

$txt['INC_sections_label'] = 'List <datix INCNames> with status';
$txt["approval_status_INC_AWAREV"] = "In the holding area, awaiting review";
$txt["approval_status_INC_INREV"] = "Being reviewed";
$txt["approval_status_INC_AWAFA"] = "Awaiting final approval";
$txt["approval_status_INC_INFA"] = "Being approved";
$txt["approval_status_INC_FA"] = "Finally approved";
$txt['enable_saved_queries_home_screen'] = 'Pin queries to the main screen';
$txt['choose_saved_queries'] = 'Select up to 10 saved queries to appear as pinned queries on the main screen';
$txt['enable_saved_queries_num_records'] = 'Display record count against pinned queries';

// PALS module
//
$txt["PALSName"] = "PALS record";
$txt["PALSNames"] = "PALS records";
$txt["PALSNameTitle"] = "PALS Record";
$txt["PALSNamesTitle"] = "PALS Records";

$txt['PAL_sections_label'] = 'List <datix PALSNames> with status';

$txt['pal_enquirer'] = 'Enquirer';
$txt['pal_enquirer_plural'] = 'Enquirers';
$txt['pal_enquirer_link'] = 'Create a new <datix pal_enquirer> link';
$txt['no_pal_enquirer_plural'] = 'No <datix pal_enquirer_plural>';
$txt['pal_patient'] = 'Patient';
$txt['pal_patient_plural'] = 'Patients';
$txt['pal_patient_link'] = 'Create a new <datix pal_patient> link';
$txt['no_pal_patient_plural'] = 'No <datix pal_patient_plural>';
$txt['pal_employee'] = 'Employee';
$txt['pal_employee_plural'] = 'Employees';
$txt['pal_employee_link'] = 'Create a new <datix pal_employee> link';
$txt['no_pal_employee_plural'] = 'No <datix pal_employee_plural>';
$txt['pal_contact'] = 'Contact';
$txt['pal_contact_plural'] = 'Contacts';
$txt['pal_contact_link'] = 'Create a new <datix pal_contact> link';
$txt['no_pal_contact_plural'] = 'No <datix pal_contact_plural>';


$txt['pal_subjects'] = 'Subjects';

// SABS module
$txt["SABName"] = "safety alert";
$txt["SABNames"] = "safety alerts";
$txt["SABNameTitle"] = "Safety alert";
$txt["SABNamesTitle"] = "Safety alerts";

// Complaints module
//
$txt["COMName"] = "complaint";
$txt["COMNames"] = "complaints";
$txt["COMNameTitle"] = "Complaint";
$txt["COMNamesTitle"] = "Complaints";

$txt['COM_sections_label'] = 'List <datix COMNames> with status';
$txt['com_show_add_new'] = "Show 'Add a new <datix COMName>' option";

// Claims module
//
$txt["CLAName"] = "claim";
$txt["CLANames"] = "claims";
$txt["CLANameTitle"] = "Claim";
$txt["CLANamesTitle"] = "Claims";
$txt['CLA_sections_label'] = 'List <datix CLAName> with status';
$txt['CLA_reserve_text'] = 'Recording of reasons for incurred value adjustments';
$txt['CLA_reserve_extratext'] = 'This setting controls whether users are able to add reasons for adjustments to incurred values, and whether adding a reason is optional or mandatory. Reasons for incurred value adjustments are recorded in the loss adjustment history.';
$txt['value'] = 'value';
$txt['adjustment'] = 'adjustment';
$txt['changed_by'] = 'Changed by';
$txt['changed_on'] = 'Changed on';
$txt['reason_for_indem_change'] = 'Reason for indemnity incurred change';
$txt['reason_for_expen_change'] = 'Reason for expenses incurred change';
$txt['no_records_found'] = 'No records found';
$txt['reserve_reasons_N'] = 'Reasons are not captured';
$txt['reserve_reasons_O'] = 'Users have the option';
$txt['reserve_reasons_M'] = 'Users must add reasons';
$txt['no_change'] = '<No change>';

// Payments module
//
$txt["PAYName"] = "payment";
$txt["PAYNames"] = "payments";
$txt["PAYNameTitle"] = "Payment";
$txt["PAYNamesTitle"] = "Payments";
$txt['pay_recipient'] = 'Recipient';
$txt['pay_listing'] = 'Payments';

// Policies module
//
$txt["POLName"] = "policy";
$txt["POLNames"] = "policies";
$txt["POLNameTitle"] = "Policy";
$txt["POLNamesTitle"] = "Policies";

// Equipment module
//
$txt["ASTName"] = "equipment";
$txt["ASTNames"] = "equipment";
$txt["ASTNameTitle"] = "Equipment";
$txt["ASTNamesTitle"] = "Equipment";

// Contacts module
//
$txt["CONName"] = "contact";
$txt["CONNames"] = "contacts";
$txt["CONNameTitle"] = "Contact";
$txt["CONNamesTitle"] = "Contacts";

// Actions module
//
$txt["ACTName"] = "action";
$txt["ACTNames"] = "actions";
$txt["ACTNameTitle"] = "Action";
$txt["ACTNamesTitle"] = "Actions";

// Distribution lists
//
$txt["DSTName"] = "distribution list";
$txt["DSTNames"] = "distribution lists";
$txt["DSTNameTitle"] = "Distribution list";
$txt["DSTNamesTitle"] = "Distribution lists";

// Risks module
//
$txt["RAMName"] = "risk";
$txt["RAMNames"] = "risks";
$txt["RAMNameTitle"] = "Risk";
$txt["RAMNamesTitle"] = "Risks";

$txt['RAM_sections_label'] = 'List <datix RAMNames> with status';

$txt['ram_contact'] = 'Contact';
$txt['ram_contact_plural'] = 'Contacts';
$txt['no_ram_contact_plural'] = 'No <datix ram_contact_plural>';
$txt['ram_contact_link'] = 'Create a new <datix ram_contact> link';

// Standards module
//
$txt["STNName"] = "standard";
$txt["STNNames"] = "standards";
$txt["STNNameTitle"] = "Standard";
$txt["STNNamesTitle"] = "Standards";

// Elements module
//
$txt["ELEName"] = "element";
$txt["ELENames"] = "elements";
$txt["ELENameTitle"] = "Element";
$txt["ELENamesTitle"] = "Elements";

// Prompts module
//
$txt["PROName"] = "prompt";
$txt["PRONames"] = "prompts";
$txt["PRONameTitle"] = "Prompt";
$txt["PRONamesTitle"] = "Prompts";

// Library module
//
$txt["LIBName"] = "library record";
$txt["LIBNames"] = "library records";
$txt["LIBNameTitle"] = "Library record";
$txt["LIBNamesTitle"] = "Library records";

// Medication module
//
$txt["MEDName"] = "medication";
$txt["MEDNames"] = "medications";
$txt["MEDNameTitle"] = "Medication";
$txt["MEDNamesTitle"] = "Medications";

// Hotspot Agents module
//
$txt["HSAName"] = "hotspot agent";
$txt["HSANames"] = "hotspot agents";
$txt["HSANameTitle"] = "Hotspot Agent";
$txt["HSANamesTitle"] = "Hotspot Agents";
$txt["hsa_criteria_subheader"] = "Location values selected on this record are operating alongside conditions set below";

// Hotspot module
//
$txt["HOTName"] = "hotspot";
$txt["HOTNames"] = "hotspots";
$txt["HOTNameTitle"] = "Hotspot";
$txt["HOTNamesTitle"] = "Hotspots";

$txt['HOT_sections_label'] = 'List <datix HOTNames> with status';

// Dashboard module
//
$txt["DASName"] = "dashboard";
$txt["DASNames"] = "dashboards";
$txt["DASNameTitle"] = "Dashboard";
$txt["DASNamesTitle"] = "Dashboards";

// To Do List module
//
$txt["TODName"] = "to do list";
$txt["TODNames"] = "to do lists";
$txt["TODNameTitle"] = "To Do List";
$txt["TODNamesTitle"] = "To Do Lists";

//CQC Outcomes module
//
$txt["CQCNamesTitle"] = "CQC Standards";
$txt["CQOName"] = "CQC outcome";
$txt["CQONames"] = "CQC outcomes";
$txt["CQONameTitle"] = "CQC Outcome";
$txt["CQONamesTitle"] = "CQC Outcomes";
$txt["CQPName"] = "CQC prompt";
$txt["CQPNames"] = "CQC prompts";
$txt["CQPNameTitle"] = "CQC Prompt";
$txt["CQPNamesTitle"] = "CQC Prompts";
$txt["CQSName"] = "CQC subprompt";
$txt["CQSNames"] = "CQC subprompts";
$txt["CQSNameTitle"] = "CQC Subprompt";
$txt["CQSNamesTitle"] = "CQC Subprompts";
$txt["cqc_location_setup"] = "CQC Location Set-up";
$txt["cqc_location_name"] = "Name";
$txt["cqc_location_parent"] = "Parent";
$txt["cqc_view_outcome_summary_report"] = "View outcome summary report";
$txt["outcome_delete"] = "Delete outcome";
$txt["no_permissions"] = 'You do not have permission to perform this action';
$txt["outcome_delete_fail"] = 'A problem was encountered when attempting to delete this outcome';
$txt['outcome_deleted'] = 'Outcome deleted';
$txt['cqc_outcome_templates'] = 'CQC outcome templates';
$txt['new_outcome_template'] = 'New CQC outcome template';
$txt['edit_outcome_template'] = 'Edit CQC outcome template';
$txt['new_prompt_template'] = 'New CQC prompt template';
$txt['edit_prompt_template'] = 'Edit CQC prompt template';
$txt['new_subprompt_template'] = 'New CQC subprompt template';
$txt['edit_subprompt_template'] = 'Edit CQC subprompt template';
$txt['select_cqc_locations'] = 'Select CQC Locations';

//Accreditation module
$txt['accreditation'] = 'Accreditation';
$txt["AMOName"] = "assessment";
$txt["AMONames"] = "assessments";
$txt["AMONameTitle"] = "Assessment";
$txt["AMONamesTitle"] = "Assessments";
$txt["AMOSubmoduleSuffix"] = "(<datix AMONameTitle>)";

$txt["AQUName"] = "criterion";
$txt["AQUNames"] = "criteria";
$txt["AQUNameTitle"] = "Criterion";
$txt["AQUNamesTitle"] = "Criteria";

$txt["ATIName"] = "assigned <datix AMOName> template";
$txt["ATINames"] = "assigned <datix AMOName> templates";
$txt["ATINameTitle"] = "Assigned <datix AMOName> template";
$txt["ATINamesTitle"] = "Assigned <datix AMOName> templates";
$txt["ATISubmoduleSuffix"] = "(Assigned <datix AMOName>)";

$txt["ATMName"] = "<datix AMOName> template";
$txt["ATMNames"] = "<datix AMOName> templates";
$txt["ATMNameTitle"] = "<datix AMONameTitle> template";
$txt["ATMNamesTitle"] = "<datix AMONameTitle> templates";
$txt["ATMSubmoduleSuffix"] = "(<datix AMONameTitle> template)";

$txt["ATQName"] = "<datix AQUName> template";
$txt["ATQNames"] = "<datix AQUName> templates";
$txt["ATQNameTitle"] = "<datix AQUNameTitle> template";
$txt["ATQNamesTitle"] = "<datix AQUNameTitle> templates";
$txt["ATQSubmoduleSuffix"] = "(<datix AQUNameTitle> template)";

$txt['ATIFormUser'] = 'Assigned <datix AMOName> form for this user';
$txt['ATMFormUser'] = '<datix AMONameTitle> template form for this user';
$txt['AQUFormUser'] = '<datix AQUNameTitle> instance form for this user';
$txt['ATQFormUser'] = '<datix AQUNameTitle> template form for this user';
$txt['AMOFormUser'] = '<datix AMONameTitle> instance form for this user';
$txt['ATMMQTEMPNameTitle'] = 'Give this user the ability to create new <datix AMOName> templates';
$txt['ATMAATNameTitle'] = 'Give this user the ability to create assigned <datix AMONames>';
$txt['ATMCAINameTitle'] = 'Give this user the ability to generate <datix AMOName> instances';
$txt['ATI_STAFF_EMAIL_desc'] = 'E-mail this user when they are selected in a staff field on an assigned <datix AMOName>';
$txt['ATI_LOC_EMAIL_desc'] = 'E-mail this user when an assigned <datix AMOName> is made available for their location';
$txt['ATM_STAFF_EMAIL_desc'] = 'E-mail this user when assigned <datix AMONames> are submitted for <datix AMOName> templates on which they are selected';
$txt['ASM_MANAGE_CYCLES_desc'] = 'Allow user to generate records for new cycle years?';
$txt['ASM_CREATE_INSTANCES_EMAIL_desc'] = 'E-mail this user when an <datix AMOName> instance is generated for their location';
$txt['AMO_SUBMIT_INSTANCE_EMAIL_desc'] = 'E-mail this user when <datix AMOName> instances they coordinate have been submitted';
$txt['AMO_STAFF_EMAIL_desc'] = 'E-mail this user when they are selected in a staff field on an <datix AMOName> instance';
$txt['ATI_REVIEWED_EMAIL_desc'] = 'E-mail this user when assigned <datix AMONames> for their location have been reviewed';
$txt['ASM_READONLY_SUBMITTED_INFO'] = 'This record cannot be updated because the assigned <datix AMOName> from which it is generated is set to \'Submitted\'.';
$txt['ASM_READONLY_REVIEWED_INFO'] = 'This record cannot be updated because the assigned <datix AMOName> from which it is generated is set to \'Reviewed\'.';

$txt['ATIRedistribute'] = 'Re-distribute';

//Locations
$txt["LOCName"] = "location";
$txt["LOCNames"] = "locations";
$txt["LOCNameTitle"] = "Location";
$txt["LOCNamesTitle"] = "Locations";
$txt['LOCSubmoduleSuffix'] = '(<datix LOCNameTitle>)';

// Admin module
$txt['datixweb_config'] = 'DatixWeb configuration';
$txt['add_new_user'] = 'Add new user';
$txt['view_own_user'] = 'View own user record';
$txt['list_users'] = 'List users';
$txt['manage_profiles'] = 'Manage profiles';
$txt['list_groups'] = 'List groups';
$txt['overdue_email'] = 'Overdue email notification';
$txt['overdue_email_button'] = 'Send overdue emails';
$txt['reports_administration'] = 'Reports administration';
$txt['document_template_administration'] = 'Document template administration';
$txt['design_forms'] = 'Design forms';
$txt['design_listing_pages'] = 'Design listing pages';
$txt['manage_email_templates'] = 'Manage email templates';
$txt['approve_reg_reqs'] = 'Approve registration requests';
$txt['software_licensing'] = 'Software licensing';
$txt['send_config_to_datix'] = 'Send configuration to Datix support';
$txt['show_config_settings'] = 'Show configuration settings';
$txt['show_error_log'] = 'Show error log';
$txt['show_locked_records'] = 'Show locked records';
$txt['show_user_sessions'] = 'Show user sessions';
$txt['migrate_con_forms'] = 'Migrate contact forms';
$txt['migrate_form_designs'] = 'Migrate form designs';
$txt['import_xml_data'] = 'Import XML data';
$txt['user_access_report'] = 'User access report';
$txt['diagnose_system_problems'] = 'Diagnose system problems';
$txt['user_manual'] = 'User manual';
$txt['change_pwd'] = 'Change password';
$txt['user_perm_error'] = 'You do not have permission to view this user record.';
$txt['search_form_design'] = 'Search form design';
$txt['search_form_design_explanatory'] = 'Select the search form to be used for this module';
$txt['search_form_design_help'] = 'By default, each user\'s search form is a duplicate of their input form.  Select a form design here if you wish users to have a different search form.<br><br>The form design selected here will be the search form for all users.  To set a different search form for specific users, choose a form within the \'Configuration parameters\' section of the user record';
$txt['complainant_chain_days'] = 'Days calculation';
$txt['complainant_chain_days_help'] = 'Calculation of due dates in the complainant chain can be based on calendar or working days.';
$txt['current_user'] = 'Current User';
$txt['current_group'] = 'Current group';
$txt['code_setups'] = 'Code setups';
$txt['code_setup'] = 'Code setup';
$txt['import_table_data'] = 'Import table data';
$txt['lock_code_setups'] = 'Lock code setup';
$txt['unlock_code_setups'] = 'Unlock code setup';
$txt['export_code_setups'] = 'Export code setup';
$txt['import_code_setups'] = 'Import code setup';
$txt['import_options'] = 'Import action';
$txt['globals'] = 'Globals';
$txt['userparms'] = 'User parameters';
$txt['system_security'] = 'System Security';
$txt['in05'] = 'NPSA mapping for IN05';
$txt['batch_update'] = 'Batch Update';
$txt['batch_delete'] = 'Batch Delete';
$txt['batch_merge'] = 'Batch Merge';
$txt['full_audit_title'] = 'Full audit';
$txt['no-codes'] = 'No codes';
$txt['user_audit'] = 'User audit';
$txt['login_audit'] = 'Login audit';
$txt['field_labels'] = 'Field Labels';
$txt['enable_contact_searching'] = 'Enable contact searching on level 1 forms?';
$txt['con_match_listing'] = 'Choose listing design to use when matching contacts';
$txt['contact_searching_fields'] = 'Choose which fields to search on';
$txt['contact_searching_hint'] = 'You can only search on a maximum of 13 fields';
$txt['contact_searching_error'] = 'You can only search on a maximum of 13 fields when searching on DIF1 forms';
$txt['risk_grading'] = 'Risk grading';
$txt['rico_table_open_error'] = 'open failed';
$txt['claims_insurer_excess'] = 'Insurer Excess';
$txt['excess_start_date'] = 'Start date';
$txt['excess_end_date'] = 'End date';
$txt['excess'] = 'Excess';
$txt['user_limit_exceeded_err'] = 'User limit exceeded';
$txt['udf_length_alert'] = 'Length must be between 0 and 254';
$txt['permitted_domains'] = 'Permitted e-mail domains';
$txt['email_audit_title'] = 'E-mail audit';
$txt['permitted_domain_invalid'] = '<datix permitted_domains>: %s is not a valid domain';
$txt['minutes_to_timeout_lbl'] = 'Minutes to timeout';
$txt['identify_obsolete_nrls_mappings'] = 'Identify obsolete NRLS mappings';
$txt['form_design_save_confirm'] = 'The form design has been saved';
$txt['list_active_users_only'] = 'List active users only';
$txt['list_all_users'] = 'List active and inactive users';
$txt['active_users_only_title'] = ' (active users only)';
$txt['adm_view_own_user'] = 'Display link to user record on main Admin screen';
$txt['adm_no_admin_reports'] = 'Give user access to packaged report design via Reports administration';
$txt['import_dmd_data'] = 'Import DM+D data';
$txt['please_select_a_file'] = 'Please select a file';
$txt['must_be_zip_file'] = 'The file must be of type .zip';
$txt['loading_data'] = 'Loading Data';
$txt['file_import_error'] = 'File import error';
$txt['dmd_older_import'] = "You're trying to import an older version of the DM+D file. Are you sure you want to proceed?";
$txt['wrong_dmd_filename'] = 'Wrong DM+D filename';
$txt['dmd_import_successful'] = 'DM+D data successfully imported';
$txt['use_dmd'] = 'Enable DM+D database import';
$txt['use_dmd_tip'] = 'This will disable editing of medications';
$txt['dmd_max_upload_error'] = 'The maximum allowed upload limit is too low for DM+D import. Please contact your Datix Administrator and ask them to contact Datix Support for details of how to increase the allowed upload limit.';
$txt['record_update_email_error'] = 'The RECORD_UPDATE_EMAIL global parameter has not been correctly configured in this system. Please contact Datix Support for assistance.';

// Organisation module
$txt['ORGName'] = 'organisation';
$txt['ORGNames'] = 'organisations';
$txt['ORGNameTitle'] = 'Organisation';
$txt['ORGNamesTitle'] = 'Organisations';
$txt["org_read_only"] = "Read-only access";
$txt["org_full"] = "Full access";
$txt["ORG_2_record_saved"] = 'The <datix ORGName> has been saved.';
$txt["ORG_saved_title"] = '<datix ORGNameTitle> number ';
$txt['linked_ORG_saved'] = 'The <datix ORGName> has been saved.';
$txt['add_new_organisation'] = 'Add a new <datix ORGName> ';
$txt['list_all_organisations'] = 'List all <datix ORGNames> ';
$txt['unlink_organisation'] = 'Unlink <datix ORGName> ';
$txt['unlink_organisation_confirm'] = 'Are you sure you want to unlink this <datix ORGName>?';
$txt['unlink_organisation_successfully'] = '<datix ORGNameTitle> unlinked successfully';
$txt['enable_organisation_searching'] = 'Enable <datix ORGName> searching on level 1 forms?';
$txt['organisation_searching_fields'] = 'Choose which fields to search on';
$txt['organisation_searching_hint'] = 'You can only search on a maximum of 13 fields';
$txt['organisation_searching_error'] = 'You can only search on a maximum of 13 fields when searching on CLA1 forms';
$txt['add_another'] = 'Add another';
$txt['org_number_match_text'] = 'Show button for <datix ORGName> matching:';
$txt['invalid_organisation_error'] = 'It is not possible to create new <datix ORGNames> via this form. Please match an <datix ORGName> before saving the record.';
$txt['matching_organisations'] = 'Matching <datix ORGNames>';
$txt['no_matching_organisations'] = 'No matching <datix ORGNames> found';
$txt['claims_linked_to_organisation_number'] = 'Claims linked to <datix ORGName> number ';
$txt['new_organisation_defaults'] = 'Add \'<datix ORGName> <datix respondent>\' link to <datix CLAName>';
$txt['check_matching_organisations'] = 'Check for matching <datix ORGNames>';
$txt['invalid_organisation'] = 'Invalid <datix ORGNames>';
$txt['edit_organisation_defaults'] = 'Edit <datix ORGName> defaults';

//Tag sets
$txt['tag_set'] = 'tag set';
$txt['tag_sets'] = 'tag sets';
$txt['tag_set_title'] = 'Tag set';
$txt['tag_sets_title'] = 'Tag sets';
$txt['list_tag_sets'] = 'Tags Set-up';
$txt['new_tag_set'] = 'New <datix tag_set>';
$txt['edit_tag_set'] = 'Edit <datix tag_set>';
$txt['tag_set_details'] = '<datix tag_set_title> details';
$txt['tag_set_saved'] = '<datix tag_set_title> saved';
$txt['btn_delete_tag_set'] = 'Delete <datix tag_set>';
$txt['tag_set_deleted'] = '<datix tag_set_title> deleted';
$txt['tag_set_linked_fields'] = 'Fields using this <datix tag_set>';
$txt['tag_list'] = 'List of tags';
$txt['assign_tags'] = 'Assign tags';
$txt['no_tags'] = "No tags currently exist. Please create tags under 'Tags set-up'";
$txt['tag_field_saved'] = 'Tags saved';

// Login and Audit screen
$txt['login_settings-title'] = 'Login settings';
$txt['login_audit-max-logins-lbl'] = '<abbr title="Maximum">Max.</abbr> number of login attempts';
$txt['login_audit-max-logins-hint'] = 'The number of unsuccessful login attempts a user is allowed before the user&rsquo;s account is locked out';
$txt['login_audit-days-inactive-lbl'] = 'Days of inactivity before lock out';
$txt['login_audit-days-inactive-hint'] = 'The number of days allowed between login attempts before a user&rsquo;s account is locked';
$txt['login_audit-disable'] = 'Disable this feature';
$txt['login_audit-save'] = 'Save';
$txt['login_audit-max-logins-error'] = 'You may only enter numbers into the maximum number of login attempts';
$txt['login_audit-days-inactive-error'] = 'You may only enter numbers greater or equal to 0 into the days of inactivity before lock out or disable the feature';
$txt['login_audit-update-success'] = 'Login settings successfully updated';

// Locked out users
$txt['locked-users-title'] = 'Locked out users';
$txt['active_only'] = '(active users only)';
$txt['username'] = 'Username';
$txt['fullname'] = 'Full name';
$txt['unlock'] = 'Unlock';
$txt['user-cant-be-unlocked'] = 'The user can\'t be unlocked';
$txt['user-unlocked'] = 'The user has been unlocked';
$txt['users-unlocked'] = 'The users have been unlocked';
$txt['no-locked-out-users'] = 'There are no locked out users to display';
$txt['no-users-selected'] = 'There are no users selected';

//Form Design
$txt['rep_approved_field_hard_hide'] = 'PLEASE NOTE: The \'Approval status\' field is not displayed on the form and has no default value. This field should be either set with a default value or displayed on the form. Data may be lost if this is not done.';
$txt['rep_approved_field_soft_hide'] = 'PLEASE NOTE: This form has been designed so that the \'Approval status\' field may not be displayed.  Please note that if this field is not displayed on the form and completed by the user, data may be lost.';
$txt['rep_approved_section_hard_hide'] = 'PLEASE NOTE: The \'Approval status\' field is designed into a section which will not appear on the form. This field should be moved to a displayed section, even if the field itself will remain hidden. Data may be lost if this is not done.';
$txt['rep_approved_section_soft_hide'] = 'PLEASE NOTE: This form has been designed so that the section containing the \'Approval status\' field may not be displayed.  Please note that if this section is not displayed on the form, the approval status will not be saved and users may experience loss of data.';
$txt['expand_all'] = 'Expand all';
$txt['collapse_all'] = 'Collapse all';

// public holidays
$txt['public-holidays'] = 'Public holidays';
$txt['date'] = 'Date';
$txt['holiday'] = 'Holiday';
$txt['there-was-an-error-processing-your-request'] = 'There was an error processing your request';
$txt['publicholidays-holiday-set'] = 'A public holiday has already been set on that date';
$txt['publicholidays-weekend-set'] = 'This date is on a weekend. Please enter a valid date';

// P_a_s_s_w_o_r_d policy
$txt['password-policy'] = 'Password policy';
$txt['password-format'] = 'Password Format';
$txt['password-must-be-characters-long'] = 'Password must be at least %s characters long';
$txt['and-must-contain-at-least'] = 'And must contain at least:';
$txt['number-of-unique-passwords'] = 'Number of unique passwords';
$txt['disable-unique-passwords'] = 'Disable unique passwords';
$txt['numbers'] = 'numbers';
$txt['uppercase-letters'] = 'uppercase letters';
$txt['lowercase-letters'] = 'lowercase letters';
$txt['symbols'] = 'symbols';
$txt['password-settings'] = 'Password Settings';
$txt['expiry-in-days'] = 'Expiry in days';
$txt['you-may-only-enter-numbers-into-the-number-of-unique-passwords'] = 'You may only enter numbers into the number of unique passwords';
$txt['length-of-password-must-be-a-number'] = 'Length of password must be a number';
$txt['the-amount-of-numbers-must-be-numeric'] = 'The amount of numbers must be numeric';
$txt['the-amount-of-uppercase-letters-must-be-numeric'] = 'The amount of uppercase letters must be numeric';
$txt['the-amount-of-lowercase-letters-must-be-numeric'] = 'The amount of lowercase letters must be numeric';
$txt['the-amount-of-symbols-must-be-numeric'] = 'The amount of symbols must be numeric';
$txt['the-password-expiry-must-be-numeric'] = 'The password expiry must be numeric';
$txt['password-policy-settings-updated-successfully'] = 'Password policy settings updated successfully';
$txt['there-was-an-error-whilst-trying-to-update-the-password-policy-settings'] = 'There was a system error whilst trying to update the password policy settings';

// Change password
$txt['change_password_confirm'] = 'Password successfully updated';

//Workflows
$txt['workflow-administration'] = 'Workflow administration';

//Excel Import Mapping
$txt['excel-import-mapping-profiles'] = 'Excel import mapping profiles';
$txt['list-of-import-profiles'] = 'List of Import Profiles';
$txt['import-using-profile'] = 'Import using profile';
$txt['edit-import-profile'] = 'Edit Profile';
$txt['new-import-profile'] = 'New Profile';

// Field labels
$txt["inc_organisation"] = "Trust";
$txt["inc_unit"] = "Unit";
$txt["inc_clingroup"] = "Clinical group";
$txt["inc_directorate"] = "Directorate";
$txt["inc_specialty"] = "Specialty";
$txt["inc_loctype"] = "Location type";
$txt["inc_locactual"] = "Location (exact)";
$txt["inc_dopened"] = "Opened date";

$txt["dif1_title"] = "Datix <datix INCNameTitle> Form (DIF1)";
$txt["dif1_cannot_edit"] = "The <datix INCName> cannot be edited or printed.  This could be because you clicked 'Print and Finish' or 'Finish', or because the terminal has been left unattended for too long.  It could also be a result of the security settings in your browser.";
$txt["dif1_form_number"] = "Datix <datix INCNameTitle> Form number: ";
$txt["dif1_no_perm_view"] = "You do not have permission to view this <datix INCName>.";
$txt["number"] = " number: ";
$txt["dif2_title"] = "Datix <datix INCNameTitle> Form (DIF2)";
$txt["rejected_by"] = "rejected by";
$txt["form_errors"] = "The form you submitted contained the following errors.  Please correct them and try again.";
$txt["form_submit_expired"] = "Your report could not be submitted because your form has expired. This may indicate that the 'Back' or 'Refresh' button has been clicked after submission.";
$txt["search_errors"] = "Cannot understand the criteria entered.  Enter the search criteria in a valid format and retry.";
$txt['save_as_query'] = 'Save the current search as a query.';
$txt["form_general_errors"] = "The form you submitted contained errors.  Please correct them and try again.";
$txt["sql_error"] = "An unexpected error occurred whilst communicating with the database.";
$txt["delete_form_design"] = "Delete this form design?";
$txt["btn_delete_form"] = "Delete form";
$txt["btn_cancel"] = "Cancel";
$txt["btn_submit_incident"] = "Submit";
$txt["btn_submit_and_print_incident"] = "Submit and print";
$txt["btn_save_complete_later"] = "Save to complete later";
$txt["btn_restore_inc_to_holding"] = "Restore to holding area";
$txt["btn_import"] = "Import";
$txt["btn_new"] = "New";
$txt["btn_save"] = "Save";
$txt["btn_close"] = "Close";
$txt["btn_delete"] = "Delete";
$txt["btn_back"] = "Back";
$txt["btn_go_to"] = "Go to ";
$txt["btn_new_report"] = "New statistical report";
$txt["btn_new_listing_report"] = "New listing report";
$txt["injury"] = "Injury";
$txt["body_part"] = "Body part";
$txt["btn_add_to_table"] = "Add to table";
$txt["injuries"] = "Injuries";
$txt["treatment_received"] = "Treatment";
$txt["contacts_need_to_be_approved"] = "There are contacts for this record in the holding area that need to be approved";
$txt["btn_back_to_incident_and_approve"] = "Back to <datix INCName> and approve contacts";
$txt["btn_back_to_incident"] = "Back to <datix INCName>";
$txt["btn_new_template"] = "New template";
$txt["btn_copy"]= "Copy";

// DIF1 User Details section
$txt["con_type"] = "Type";
$txt["con_subtype"] = "Subtype";
$txt["con_title"] = "Title";
$txt["con_forenames"] = "Forenames";
$txt["con_surname"] = "Surname";
$txt["con_email"] = "E-mail";
$txt["con_number"] = "Payroll number";
$txt["con_tel1"] = "Telephone number";
$txt["con_status"] = "Status";
$txt["your_manager"] = "Your manager";

// FormClasses
$txt["choose"] = "Choose";
$txt["sqb_deleted_sqb"] = "[Deleted]";
$txt["male"] = "Male";
$txt["female"] = "Female";
$txt["m_male"] = "M"; // value for male/female dropdown - don't change
$txt["f_female"] = "F";
$txt["y_yes"] = "Y";
$txt["n_no"] = "N";
$txt["yes"] = "Yes";
$txt["no"] = "No";

$txt["consequence"] = "Consequence";
$txt["likelihood"] = "Likelihood";
$txt["grade"] = "Grade";
$txt["rating"] = "Rating";
$txt["level"] = "Level";

$txt["btn_add_selected_field"] = "Add selected field";
$txt["extra_field_group"] = "Extra Field Group";
$txt["title_of_form"] = "Title of form";
$txt["introductory_text"] = "Introductory text";
$txt["show_all_on_one_page"] = "Show all sections on one page?";
$txt["form_q"] = "form?";
$txt["section_name"] = "Section name";
$txt["hide_section_q"] = "Hide section?";
$txt["read_only_section_q"] = "Read-only?";
$txt['new_panel_q'] = 'New panel?';
$txt["advanced_q"] = "Advanced?";
$txt["extra_text"] = "Extra text:";
$txt["con_number_match_text"] = "Show button for contact matching:";
$txt["con_form_select_text"] = "Select contact form to use for this section:";
$txt["equip_form_select_text"] = "Select equipment form to use for this section:";
$txt["listing_section_select_text"] = "Select listing design to use for this section:";
$txt["form_section_select_text"] = "Select form design to use for this section:";
$txt['label_individual_respondent_form_design'] = 'Select <datix CONName> form used for <datix cla_individual_respondent> only';
$txt['label_organisation_respondent_form_design'] = 'Select <datix ORGName> form used for <datix cla_organisation_respondent> only';
$txt["extra_field_groups"] = "Extra Field Groups";
$txt["udfs_on_form"] = "Display these Extra Field Groups on the form";
$txt["hide_q"] = "Hide?";
$txt["read_only_q"] = "Read-only?";
$txt["mandatory_q"] = "Mandatory?";
$txt["timestamp_q"] = "Date/User stamped entries";
$txt["order"] = "Order:";
$txt["field_label"] = "Field label:";
$txt["help_text"] = "Help text:";
$txt["datix_help_title"] = 'Datix Help: ';
$txt["default_value"] = "Default value:";
$txt["dynamic_sizing_q"] = "Dynamic sizing?";
$txt["display_section"] = "Display section:";
$txt["display_field"] = "Display field:";
$txt["show_popup_msg"] = "Show popup message:";
$txt["trigger_values"] = "Trigger values:";
$txt["change_actions_btn"] = "Change actions";
$txt["change_field_actions_btn"] = "Change field actions";
$txt["change_fieldsection_btn"] = "Move Field";
$txt["delete_action_q"] = "Delete this action?";
$txt["delete_action_btn"] = "Delete action";
$txt['confirm_or_cancel'] = "Press <datix btn_ok> to confirm or <datix btn_cancel> to return to the form";
$txt["delete_field_action_btn"] = "Delete field action";
$txt["no_sections_linked"] = "No sections or popup messages are linked to this field.";
$txt["no_fields_linked"] = "No other fields are linked to this field.";
$txt["add_action_btn"] = "Add new action";
$txt["add_field_action_btn"] = "Add new field action";
$txt["no_value_audit"] = "no value";
$txt["mandatory_err"] = "You must enter a value in this field";
$txt["mandatory_alt"] = "Mandatory";
$txt["help_alt"] = "Help";
$txt["original_value"] = "Original value";
$txt['create_extra_field'] = 'Create a new extra field';
$txt['return_to_extra_fields_list'] = 'Return to extra fields list';

$txt['delete_section'] = 'Delete Section';
$txt['clear_section'] = 'Clear Section';

$txt['display_as_checkboxes'] = 'Display as check boxes';
$txt['display_as_radio_buttons'] = 'Display as radio buttons';
$txt['none'] = 'None';

// Login

$txt["login_title"] = "Log in to Datix";
$txt["user_name"] = "User name";
$txt["password"] = "Password";
$txt["domain"] = "Domain";
$txt["forgotten_password"] = "Forgotten password?";
$txt["btn_log_in"] = "Log in";
$txt["reset_password_title"] = "Reset Password";
$txt["reset_instructions"] = "If you have forgotten your password, Datix can send you an e-mail containing a link to change it.  Please enter your user name in the box below and click the 'Request password change' button.";
$txt["email_address"] = "Email address";
$txt["btn_reset_password"] = "Request password change";
$txt["missing_email_err"] = "You must enter an e-mail address.";
$txt["user_name_error"] = "Could not reset password for this user name.";

$txt["ie_6_alert"] = 'You are running Internet Explorer v6, which is no longer supported by Datix. Please contact your IT department to arrange an upgrade.';

// P_a_s_s_w_o_r_d reset e-mail
$txt["pwd_reset_title"] = "Datix username and password reminder";
$txt["pwd_reset_user_name"] = "Datix user name:";
$txt["pwd_reset_subject"] = "Datix Password Reset";
$txt["pwd_reset_email_1"] = "You have requested to change your Datix password.";
$txt["pwd_reset_email_2"] = "Please follow the link below to change your password";

$txt["pwd_reset_success"] = "If an e-mail address is associated with that user name, a link has been sent to it to enable you to set a new password.";
$txt["pwd_reset_email_sent"] = "A link has been sent to your e-mail address to enable you to set a new password.";
$txt['expired_pwd_reset_link'] = "The password reset link you have used is invalid. This may be because it has already been used to reset your password.";

$txt["pwd_rem_notfound_err"] = "E-mail address not found.";
$txt["no_user_name_err"] = "You must enter a user name.";
$txt["no_password_err"] = "You must enter a password.";

$txt["maintenance_mode_err"] = "Datix is in maintenance mode.  You may not log in.";
$txt["update_staff_LDAP_err"] = "Failed to update Datix staff record with LDAP details";
$txt["no_SID_err"] = "Unable to update Datix staff record with LDAP details: no SID set.";
$txt["ldap_auth_fail_err"] = "LDAP Authentication failed. Failed to create Datix staff record.";

$txt["login_failed_err"] = "Login attempt unsuccessful.";
$txt["login_failed_err_desc"] = "This may be due to an incorrect user name or password.  Note that an account will be locked after ".GetParm('LOGIN_TRY', LOGIN_TRY)." unsuccessful attempts to log in. Please contact your Datix administrator if you require assistance.";
$txt["wrong_user_pwd_err"] = "Incorrect user name or password. Please try again.";
$txt["lockout_err"] = "You are locked out.  Please contact your Datix Administrator.";
$txt["login_attempts_left"] = "Login attempts left";
$txt["account_inactive_err"] = "Your account is not active.  Please contact your Datix Administrator.";
$txt["password_expired_enter_new"] = "Your password has expired.  Please enter a new password.";
$txt["timed_out_title"] = "Session timed out";
$txt["timed_out_msg"] = "Your session has timed out because you have been inactive for a while.";

//LDAP
$txt["ldap_disable_lockout_reason"] = "The account has been locked out since it has been disabled via Active Directory LDAP integration.";
$txt["ldap_sync_all_group"] = "Sync All AD";
$txt["ldap_sync_existing_group"] = "Sync Existing";
$txt["ldap_sync_all_groups"] = "Sync all Datix group users with Active directory group mappings";
$txt["ldap_sync_existing_groups"] = "Sync only existing Datix group users with Active directory group mappings";
$txt["ldap_sync_all_profiles"] = "Sync all Datix profile users with Active directory group mappings";
$txt["ldap_sync_existing_profiles"] = "Sync only existing Datix profile users with Active directory group mappings";
$txt["ldap_sync_all_users"] = "Sync all Datix users with Active directory group mappings";
$txt["ldap_sync_existing_users"] = "Sync only existing Datix users with Active directory group mappings";
$txt["active_directory_mappings_section"] = "Active directory mappings";
$txt["sync_user_mappings_title"] = "Sync Datix Users with Directory group mappings";
$txt["sync_profile_mappings_title"] = "Sync Datix Profiles with Directory group mappings";
$txt["sync_group_mappings_title"] = "Sync Datix Groups with Directory group mappings";
$txt["edit_ldap_order"] = "Edit priorities";
$txt["save_ldap_order"] = "Save priorities";
$txt["save_ldap_order_unique_validation"] = "Priority values must be unique. Please amend before saving";
$txt["save_ldap_order_positive_validation"] = "Priority values must be greater than 0. Please amend before saving";
$txt["save_ldap_order_success"] = "Changes have been saved";

// Registration
$txt["registration_title"] = "New User Registration Application";
$txt["registration_review_title"] = "Review of User Registration Application";
$txt["choose_user_name"] = "Please choose a user name (maximum 20 letters)";
$txt["user_title"] = "Title";
$txt["user_forenames"] = "Forenames";
$txt["user_surname"] = "Surname";
$txt["user_org"] = "Trust/organisation";
$txt["choose_pwd"] = "Choose a password";
$txt["retype_pwd"] = "Retype password";
$txt["pwd_reminder"] = "Password reminder";
$txt["pls_enter_email"] = "Please enter your e-mail address";
$txt["instructions_will_be_sent"] = "Instructions on how to get started will be sent to this address.";
$txt["btn_submit_reg"] = "Submit registration request";
$txt["btn_submit_reg_review"] = "Submit";
$txt["reg_pending_message"] = "You have already registered for a DatixWeb account.  This account is currently pending approval.  You will receive an advisory e-mail once the account has been approved.";
$txt["reg_inactive_message"] = "You already have an account for DatixWeb, but this account is currently inactive.  Please contact the system administrator at <global DIF_ADMIN_EMAIL> to reactivate your account.";
$txt["reg_deactivated_message"] = "You already have an account for DatixWeb, but this account is currently inactive.  Please contact the system administrator at <global DIF_ADMIN_EMAIL> to reactivate your account.";
$txt["reg_active_message"] = "You already have an active account for DatixWeb.  Please select the 'Login' option to gain access to the system.";
$txt["please_register_message"] = "You have not yet registered for a DatixWeb user account. Please select the 'Register' option.";
$txt["sso_setup_error"] = "You are unable to access the DatixWeb application as your identity cannot be verified.  Please contact your Datix administrator at <global DIF_ADMIN_EMAIL>.";
$txt["sso_error_message"] = "You are unable to access the DatixWeb application as your identity cannot be verified.  Please contact your Datix administrator at <global DIF_ADMIN_EMAIL>.";
$txt["user_added"] = "User created successfully";
$txt["registration_submitted"] = "Thank you for your application.  Your registration request has been submitted and you will shortly receive an e-mail confirmation.";
$txt["registration_email_error"] = 'E-mails were not sent to the following recipients as their domain is not permitted:';
$txt["approve_reg_title"] = "Approve Registration Requests";
$txt["reg_requests"] = "Registration requests";
$txt["no_reg_requests_found"] = "No registration requests found.";
$txt["full_name_header"] = "Full Name";
$txt["user_name_header"] = "User Name";
$txt["e_mail_addr_header"] = "E-mail Address";
$txt["registration_action"] = "Action";
$txt["approve_registration"] = "Approve";
$txt["reject_registration"] = "Reject";
$txt["approval_options_defer"] = "Defer";
$txt["approval_options_approve"] = "Approve";
$txt["approval_options_discard"] = "Discard";
$txt["approval_options_reject"] = "Reject";
$txt["registration_reason"] = "Reason for Rejection";
$txt["btn_submit"] = "Submit";
$txt["reg_requests_updated"] = "Registration requests have been updated.";


// Change p_a_s_s_w_o_r_d

$txt["pwd_expired_title"] = "Password Expired";
$txt["pwd_change_title"] = "Change Password";
$txt["pwd_set_title"] = "Set Password";
$txt["btn_set_pwd"] = "Set password";
$txt["old_pwd"] = "Old password";
$txt["new_pwd"] = "New password";
$txt["pwd_mismatch_err"] = "New and retyped passwords do not match";
$txt["old_pwd_wrong_err"] = "Old password is incorrect";
$txt["wrong_username_pwd_err"] = "Username mismatch. Cannot change password for this user as they are not logged in.";

//Licence key
$txt['err_licence_expired'] = 'The DatixWeb software licence has expired';
$txt['err_licence_invalid'] = 'Invalid DatixWeb licence key';


// User List

$txt["user_list_title"] = "User List";
$txt["domain"] = "Domain";
$txt["job_title_header"] = "Job Title";
$txt["logins"] = "Logins";
$txt["last_login_header"] = "Last Login";
$txt["btn_add_new_user"] = "Add new user";
$txt["email_addr_header"] = "E-mail address";

// Group List

$txt["group_list_title"] = "Group List";


// Add/Edit user

$txt["add_user_title"] = "Add New User";
$txt["edit_user_title"] = "Edit User Details";
$txt["employee_user_title"] = "Employee Details";
$txt["SID"] = "SID";
$txt["initials"] = "Initials";
$txt["email"] = "E-mail address";
$txt["access_start"] = "Access start date";
$txt["access_end"] = "Access end date";
$txt["select_group_err"] = "Please select a security group";
$txt["security_group"] = "Security group";
$txt["add_security_groups"] = "Add Security groups";
$txt["add_profiles"] = "Add Profiles";
$txt["security_settings_title"] = "User Security Settings";
$txt["full_administrator"] = "Give this user full admin access";
$txt["access_level"] = "User access level";
$txt["dif1_input_only"] = "DIF1 input only";
$txt["dif2_read_only"] = "DIF2 read-only access";
$txt["dif2_no_approval"] = "DIF2 access only - no review of DIF1 forms";
$txt["dif2_access"] = "DIF2 access and review of DIF1 forms";
$txt["dif2_final"] = "Final approval of DIF2";
$txt["own_only"] = "Restrict user to approving and viewing their own records";
$txt["own_only_explain"] = "If you set this option, the user will only be able to see records where their name appears in the Handler dropdown.  Does not apply to input-only users.";
$txt["types_and_categories"] = "Types and categories";
$txt["inc_type_title"] = "Type";
$txt["inc_category_title"] = "Category";
$txt["btn_add_type_category"] = "Add type and category to list";
$txt["where_clause"] = "Security WHERE clause (from user's permissions):";
$txt["sql_generated"] = "SQL generated by these permissions:";
$txt["email_security_1"] = "User receives e-mails for reports if the WHERE clause above matches the record.";
$txt["security_explain_1"] = "  (When determining who to e-mail and which records the user can access, only the WHERE clause is used and not the location and type settings)";
$txt["email_security_2"] = "User receives e-mails for reports from the above locations and type/category combinations, if the WHERE clause above also matches.";
$txt["security_explain_2"] = "(When determinining who to e-mail and which records the user can access, the location, type and WHERE clause are all used)";
$txt["email_security_3"] = "User receives e-mails for reports from the above locations and type/category combinations only.";
$txt["security_explain_3"] = "(When determining who to e-mail, only the location and type are used, not the WHERE clause.  When determining which records the user can access, only the WHERE clause is used, not the location and type settings)'";
$txt["email_security_4"] = "User receives e-mails for reports from the above locations and type/category combinations only.";
$txt["security_explain_4"] = "(When determining who to e-mail, the location and type settings are used, not the WHERE clause. When determining which records the user can access, the location, type and WHERE clause are all used.)";
$txt["enable_reject_button"] = "Enable the reject status for this user";
$txt["hide_contacts_tab"] = "Hide Contacts tab for this user";
$txt["allow_user_grant_access"] = "Allow user to grant record level access permissions";
$txt["btn_delete_user"] = "Delete user";
$txt["missing_data_error"] = "Some of the information you entered is missing or incorrect.  Please correct the fields marked below and try again";
$txt["invalid_char_uname_err"] = "Invalid character in user name";
$txt["email_option_err"] = "You cannot set the email option unless the user has review or final approval permissions";
$txt["inc_email_option_err"] = '<datix email_option_err>';
$txt["ram_email_option_err"] = '<datix email_option_err>';
$txt["pal_email_option_err"] = '<datix email_option_err>';
$txt["com_email_option_err"] = '<datix email_option_err>';
$txt["cla_email_option_err"] = '<datix email_option_err>';
$txt['emails_not_set_domain_not_permitted'] = 'E-mails were not sent to the following recipients as their domain is not permitted: %s';
$txt['invalid_email_structure_error'] = 'The email address provided is invalid: %s';
$txt['unknown_email_error'] = 'An unforseen problem occurred when attempting to send this email';
$txt["invalid_char_initials_err"] = "Invalid character in initials";
$txt["initials_dup_err"] = "Initials already in use";
$txt["email_dup_err"] = "E-mail address is already in use by contacts";
$txt["create_user_err"] = "Cannot create new user";
$txt["update_user_err"] = "Cannot update user details";
$txt["user_update_successful"] = "User updated successfully";
$txt["user_security_group"] = "User security group";
$txt["new_user_security_group"] = "New user security group";
$txt["security_group"] = "Security group";
$txt["security_group_already_linked"] = "Security group already linked to user";
$txt["invalid_user_name"] = "Invalid user name or account not active";
$txt["user_security_group"] = "User security group";
$txt['pwd_change_next_login'] = 'User must change password at next login';
$txt['config_params'] = 'Configuration parameters';
$txt['no_config_params'] = 'No configuration parameters';
$txt['user_own_only'] = 'User can only access their own records';
$txt['access_to_all'] = 'Access to all records';
$txt['remove_user_q'] = 'Remove this user?';
$txt['remove_group_q'] = 'Remove this group?';
$txt['lockout'] = 'Locked out';

//User Listing settings
$txt['INC_listing_design_user'] = '<datix INCNameTitle> listing design for this user';
$txt['RAM_listing_design_user'] = '<datix RAMNameTitle> listing design for this user';
$txt['POL_listing_design_user'] = '<datix POLNameTitle> listing design for this user';
$txt['PAL_listing_design_user'] = '<datix PALSNameTitle> listing design for this user';
$txt['COM_listing_design_user'] = '<datix COMNameTitle> listing design for this user';
$txt['CLA_listing_design_user'] = '<datix CLANameTitle> listing design for this user';
$txt['SAB_listing_design_user'] = '<datix SABSNameTitle> listing design for this user';
$txt['STN_listing_design_user'] = '<datix STNNameTitle> listing design for this user';
$txt['HSA_listing_design_user'] = '<datix HSANameTitle> listing design for this user';
$txt['HOT_listing_design_user'] = '<datix HOTNameTitle> listing design for this user';
$txt['ACT_listing_design_user'] = '<datix ACTNameTitle> listing design for this user';
$txt['AST_listing_design_user'] = '<datix ASTNameTitle> listing design for this user';
$txt['CON_listing_design_user'] = '<datix CONNameTitle> listing design for this user';
$txt['MED_listing_design_user'] = '<datix MEDNameTitle> listing design for this user';
$txt['CQO_listing_design_user'] = '<datix CQONameTitle> listing design for this user';
$txt['CQP_listing_design_user'] = '<datix CQPNameTitle> listing design for this user';
$txt['CQS_listing_design_user'] = '<datix CQSNameTitle> listing design for this user';
$txt['LOC_listing_design_user'] = '<datix LOCNameTitle> listing design for this user';
$txt['ATI_listing_design_user'] = 'Assigned <datix AMOName> listing design for this user';
$txt['ATM_listing_design_user'] = '<datix AMONameTitle> template listing design for this user';
$txt['AQU_listing_design_user'] = '<datix AQUNameTitle> instance listing design for this user';
$txt['ATQ_listing_design_user'] = '<datix AQUNameTitle> template listing design for this user';
$txt['AMO_listing_design_user'] = '<datix AMONameTitle> instance listing design for this user';
$txt['ADM_listing_design_user'] = 'User listing design for this user';


//Profiles
$txt['profile_clear_check'] = 'Do you want to remove all user configuration, location/type settings and groups?<br><br>Any user-specific configuration settings will override the settings made in the profile.  Any user-specific security group settings will operate in addition to any security group settings made in the profile.';
$txt['profile_saved'] = 'The profile has been saved';

// P_a_s_s_w_o_r_d errors

$txt["new_pwd_unique_err"] = "New password cannot not be the same as any previous password";
$txt["new_pwd_old_err"] = "New password cannot be the same as the last password";
$txt["datix_pwd_err"] = "The word Datix cannot be used as a password";
$txt["pwd_user_same_err"] = "Password cannot be the same as the user name";
$txt["pwd_reminder_same_err"] = "Password reminder cannot be the same as the password";
$txt["pwd_fullname_same_err"] = "New password cannot be the same as the user's name";
$txt["pwd_length_err"] = "Minimum number of characters for password is ";
$txt["new_pwd_spaces_err"] = "New password cannot contain spaces.";

//errors
$txt['max_input_vars_error'] = 'You must set the max_input_vars setting in your php.ini file before using this version of DatixWeb. Please contact Datix support.';
$txt['no_codes_available'] = 'No codes available';

//Overdue Emails
$txt['overdue_email_title'] = 'Send Overdue Reminder Emails';

// Risk Register security settings
$txt["risk1_only"] = "RISK1 input only";
$txt["risk2_read_only"] = "RISK2 read-only access";
$txt["risk2_no_approval"] = "RISK2 access only - no review of RISK1 forms";
$txt["risk2_access"] = "RISK2 access and review of RISK1 forms";
$txt["risk2_final"] = "Final approval of RISK2";
$txt['risk2_user'] = 'RISK2 form for this user';
$txt['risk2_search_form_user'] = 'RISK2 search form for this user';

// PALS security settings
$txt["pals1_only"] = "PALS1 input only";
$txt["pals2_read_only"] = "PALS2 read-only access";
$txt["pals2_no_approval"] = "PALS2 access only - no review of PALS1 forms";
$txt["pals2_access"] = "PALS2 access and review of PALS1 forms";
$txt["pals2_final"] = "Final approval of PALS2";
$txt["pal2_user"] = "PALS2 form for this user";
$txt["pal2_search_form_user"] = "PALS2 search form for this user";

// Complaints security settings
$txt["com1_only"] = "COM1 input only";
$txt["com2_read_only"] = "Read-only access to COM2";
$txt["com2_access"] = "Full access to COM2";
$txt["com2_user"] = "COM2 form for this user";
$txt["com2_search_form_user"] = "COM2 search form for this user";

// Safety Alerts security settings
$txt["sabs_response_only"] = "Response only";
$txt["sabs_full_access"] = "Full access";

// Library security settings
$txt["lib_input"] = "Input only";
$txt["lib_read"] = "Read only";
$txt["lib_full"] = "Full access";

// Standards security settings for version 10.2 and above
$txt["stn_input_only"] = "Input only";
$txt["stn_read_only"] = "Read only";
$txt["stn_full"] = "Full access";
$txt["stn2_user"] = 'Standards form for this user';
$txt['stn2_search_form_user'] = 'Standards search form for this user';
$txt["stn_add_new"] = 'Enable users to add new records';

// Standards security settings
//$txt["stn_lvl_1"] = "Read/write own standards only";
//$txt["stn_lvl_2"] = "Read/write own standards, read-only others";
//$txt["stn_lvl_3"] = "Full access";

// Actions security settings
$txt["act_input_only"] = "Input only";
$txt["act_read_only"] = "Read only";
$txt["act_full"] = "Full access";
$txt["restrict_own_actions"] = "Restrict user to viewing their own actions";
$txt["restrict_own_actions_explain"] = "If you set this option, the user will only be able to see actions which have been assigned to them";

// Assets/Equipment security settings
$txt["ast_input_only"] = "Input only";
$txt["ast_read_only"] = "Read only";
$txt["ast_full"] = "Full access";

// Medications security settings
$txt["med_read_only"] = "Read only";
$txt["med_full"] = "Full access";

// Distribution lists security settings
$txt["dst_input_only"] = "Input only";
$txt["dst_read_only"] = "Read only";
$txt["dst_full_access"] = "Full access";

// Secure messages security settings
$txt["msg_read_only"] = "Read only";
$txt["msg_full_access"] = "Full access";

// Dashboard security settings
$txt["das_read_only"] = "Read only";
$txt["das_full_access"] = "Full access";

// Admin "module" security settings
$txt["adm_no_access"] = "No admin access";
$txt["adm_user_admin"] = "User administration";
$txt["adm_full"] = "Full administration";

// Contacts module security settings
$txt["con_input_only"] = "Input only";
$txt["con_read_only"] = "Read only";
$txt["con_full"] = "Full access";

// Hotspot Agents module security settings
$txt["hsa_read_only"] = "Read only";
$txt["hsa_full"] = "Full access";
$txt["hsa2_user"] = "HSA2 form for this user";
$txt["hsa2_search_form_user"] = "HSA2 search form for this user";

// Hotspot security settings
$txt["hot2_read_only"] = "Read-only access";
$txt["hot2_access"] = "Full access";
$txt["hot2_user"] = "HOT2 form for this user";
$txt["hot2_search_form_user"] = "HOT2 search form for this user";

// Contacts Registration
$txt["reg_link_err"] = "Invalid registration invitation link";
$txt["name"] = "Name";
$txt["confirm_email"] = "Please confirm your e-mail address.";
$txt["confirm_email_explain"] = "This must be the one to which your registration invite was sent";
$txt["email_match_err"] = "The e-mail address you entered does not match the address to which the registration invite was sent";
$txt["already_activated_err"] = "User registration has already been activated";
$txt["registration_activated"] = "Your registration for DatixWeb has been activated.  You may log in using the login name and password that you chose when you registered.";
$txt["invalid_activation_err"] = "Invalid activation link or the login has already been activated.";

// Users
$txt['edit_user_details'] = 'Edit User Details';
$txt['confirm_delete_user'] = 'Remove this user from the system?  Press OK to confirm or Cancel to return to the user screen.';
$txt['confirm_cancel_user'] = 'Are you sure you want to leave this record? Any changes will be lost.';
$txt['err_cannot_save_user'] = 'Cannot save user details.';
$txt['msg_user_saved'] = 'Details for user "%s" saved';
$txt["user_deleted"] = 'User "%s" deleted';
$txt["err_user_not_deleted"] = 'An error occurred when trying to delete user "%s". This user has not been deleted.';

//----------------------------------------------------------------

// Safety Alerts module
$txt["SABSName"] = "safety alert";
$txt["SABSNames"] = "safety alerts";
$txt["SABSNameTitle"] = "Safety Alert";
$txt["SABSNamesTitle"] = "Safety Alerts";
$txt["delete_weblink"] = "Delete weblink?";

$txt['add_new_sab'] = 'Add a new <datix SABSName>';
$txt['list_sab_response'] = 'List all my <datix SABSNames> which require a response';
$txt['list_all_sab'] = 'List all my <datix SABSNames>';
$txt['list_all_open_sab'] = 'List all open <datix SABSNames>';
$txt['list_all_closed_sab'] = 'List all closed <datix SABSNames>';

$txt['email_selected_recipients'] = 'E-mail selected recipients';
$txt['unlink_selected_recipients'] = 'Unlink selected recipients';
$txt['confirm_unlink_contacts_sab'] = 'Are you sure you wish to unlink selected contacts? Any response data will be deleted.';
$txt["unlink_select_a_contact"] = 'Please select at least 1 contact to unlink.';

$txt['sab_recipient'] = 'recipient';
$txt['sab_recipients'] = 'recipients';
$txt['sab_contact'] = 'contact';
$txt['sab_contacts'] = 'contacts';

$txt['sab_for_action_by'] = 'For action by';
$txt['sab_information_only'] = 'Information only';
$txt['sab_contact'] = 'Contact';
$txt['sab_contact_plural'] = 'Contacts';

$txt['sab_new_recipients'] = 'New <datix sab_recipients>';
$txt['sab_new_contacts'] = 'New <datix sab_contacts>';

$txt['sab_add_recipient'] = 'Add a <datix sab_recipient> for this <datix SABSName>';
$txt['sab_add_contact'] = 'Add a <datix sab_contact> for this <datix SABSName>';

$txt['sab_search_recipient'] = 'Search for contacts to link as <datix sab_recipients> for this <datix SABSName>';
$txt['sab_search_contact'] = 'Search for contacts to link as <datix sab_contacts> for this <datix SABSName>';

$txt['sab_add_dst_recipient'] = 'Add contacts from a <datix DSTName> as <datix sab_recipients> for this <datix SABSName>';
$txt['sab_add_dst_contact'] = 'Add contacts from a <datix DSTName> as <datix sab_contacts> for this <datix SABSName>';

//Standards module
$txt['add_new_standard'] = 'Add a new <datix STNName>';
$txt['list_all_standards'] = 'List all <datix STNNames>';
$txt['back_to_standard'] = 'Back to <datix STNName>';

$txt['add_new_element'] = 'Add a new <datix ELEName>';
$txt['list_my_elements'] = 'List my <datix ELENames>';
$txt['list_pending_elements'] = 'List pending <datix ELENames>';
$txt['list_my_pending_elements'] = 'List my pending <datix ELENames>';
$txt['element_save_message'] = '<datix ELENameTitle> saved';
$txt['back_to_element'] = 'Back to <datix ELEName>';

$txt['add_new_prompt'] = 'Add a new <datix PROName>';
$txt['prompt_save_message'] = '<datix PRONameTitle> saved';
$txt['back_to_prompt'] = 'Back to <datix PROName>';

//Location module
$txt['add_new_location'] = 'Add a new <datix LOCName>';

// Default listing titles
$txt["inc_listing"] = "Incidents Search Listing";
$txt["ram_listing"] = "Risk Register Search Listing";
$txt["pal_listing"] = "PALS Search Listing";
$txt["com_listing"] = "Complaints Search Listing";
$txt["loc_listing"] = "Location Listing";
$txt["cqo_listing"] = "CQC Outcomes Listing";
$txt["cqp_listing"] = "CQC Prompts Listing";
$txt["cqs_listing"] = "CQC Sub-prompts Listing";
$txt["med_listing"] = "Medications Listing";
$txt['ast_listing'] = "<datix ASTNameTitle> Listing";
$txt['atm_listing'] = "<datix ATMNameTitle> Listing";
$txt['atq_listing'] = "<datix ATQNameTitle> Listing";
$txt['ati_listing'] = "<datix ATINameTitle> Listing";
$txt['amo_listing'] = "<datix AMONameTitle> Listing";
$txt['aqu_listing'] = "<datix AQUNameTitle> Listing";
$txt['pol_listing'] = "<datix POLNameTitle> Listing";
$txt['org_listing'] = "<datix ORGNamesTitle> Listing";

//No permissions to view record
$txt["no_perms_view_INC"] = "You do not have the necessary permissions to view the record.";
$txt["no_perms_view_RAM"] = "<datix no_perms_view_inc>";
$txt["no_perms_view_COM"] = "<datix no_perms_view_inc>";
$txt["no_perms_view_PAL"] = "<datix no_perms_view_inc>";
$txt["no_perms_view_AST"] = "<datix no_perms_view_inc>";
$txt["no_perms_view_CON"] = "<datix no_perms_view_inc>";
$txt["no_perms_view"] = "You do not have the necessary permissions to view the record.";
$txt["no_perms_view_num"] = "You do not have the necessary permissions to view <datix numRecords>";

// MainIncident
$txt["not_found"] = "Record not found";
$txt["inc_rejected_err"] = "You cannot view this record as it has been rejected.";
$txt["search_invalid_chars_err"] = "<br />There are invalid characters in one of the fields in your search criteria.";
$txt["dash_example_incident"] = " - example <datix INCName>";
$txt["dash_search_incidents"] = " - Search for <datix INCName>";
$txt["inc_already_approved_err"] = "This <datix INCName> has already been approved.";
$txt["btn_continue"] = "Continue";
$txt["btn_search"] = "Search";
$txt["btn_approve"] = "Approve";
$txt["btn_reject"] = "Reject";
$txt["reject_incident_q"] = "Reject this <datix INCName>?";
$txt["inc_name_ref_section"] = "Name and reference";
$txt["incident_name"] = "Name";
$txt["no_id_assigned"] = "No ID assigned yet - still in holding area";
$txt["still_being_reviewed"] = "Still being reviewed - not yet sent for final approval";
$txt["incomplete"] = "Incomplete";
$txt["in_holding_area"] = "In holding area - not yet reviewed";
$txt["awaiting_final_approval"] = "Awaiting final approval";
$txt["rejected"] = "Rejected";
$txt["has_final_approval"] = "Has final approval";
$txt["approval_status"] = "Approval status";
$txt["inc_form_ref"] = "Form reference";
$txt["reported_date"] = "Reported date";
$txt["calendar"] = "Calendar";
$txt["opened_date"] = "Opened date";
$txt["transferred_dif1_by"] = "Transferred from DIF1 by";
$txt["not_added_via_dif1"] = "Not added via DIF1";
$txt["handler"] = "Handler";
$txt["cause_analysis_title"] = "Correctable Cause Analysis";
$txt["no_causes"] = "No correctable causes recorded";
$txt["reporter_section_title"] = "Reporter details";
$txt["full_name"] = "Full name";
$txt["additional_recipients"] = "Additional recipients";
$txt["additional_recipients_explain"] = "Enter e-mail addresses of other recipients not listed above.  You can enter multiple addresses, separated by commas.";
$txt["subject_of_message"] = "Subject of message";
$txt["btn_send_message"] = "Send message";
$txt["no_valid_emails_err"] = "You did not enter any valid email addresses. If you are trying to enter multiple addresses, please ensure you have separated them with a comma.";
$txt["email_address_missing_err"] = "E-mail address is missing";
$txt["message_history_title"] = "Message history";
$txt["date_time_header"] = "Date/Time";
$txt["sender"] = "Sender";
$txt["recipient"] = "Recipient";
$txt["body_of_message_header"] = "Body of Message";
$txt["no_messages_for_inc"] = "No messages";
$txt["record_saved"] = '<datix INCName> record saved';
$txt["link_contact_before_approve"] = "Contacts require approval.";
$txt["person_affected"] = "Person Affected";
$txt["person_affected_link"] = "Create a new <datix person_affected> link";
$txt["other_contact"] = "Other Contact";
$txt["no_other_contact_plural"] = "No <datix other_contact_plural>";
$txt["other_contact_link"] = "Create a new <datix other_contact> link";
$txt['create_new_link'] = 'Create new link';
$txt['save_contact'] = 'Save';
$txt['reject_contact'] = 'Reject Contact';
$txt['unlink_contact'] = 'Unlink contact';
$txt['unlink_contact_confirm'] = 'Are you sure you want to unlink this contact?';
$txt['approve_and_save_contact'] = 'Approve and Save';

$txt['person_affected_plural'] = 'People Affected';
$txt['no_person_affected_plural'] = 'No <datix person_affected_plural>';
$txt['employee'] = 'Employee';
$txt['employee_link'] = 'Create a new <datix employee> link';
$txt['employee_plural'] = 'Employees';
$txt['no_employee_plural'] = 'No <datix employee_plural>';
$txt['other_contact_plural'] = 'Other Contacts';
$txt['openness_transparency_title'] = 'Openness and Transparency';
$txt['inc_ot_q1_title'] = 'Was the patient/appropriate person informed that an incident occurred?';
$txt['inc_ot_q2_title'] = 'When was the patient /appropriate person informed?';
$txt['inc_ot_q3_title'] = 'Were the members of staff involved in the incident involved in informing the patient/appropriate person?';
$txt['inc_ot_q4_title'] = 'Please provide details of staff members who informed the patient/appropriate person.';
$txt['inc_ot_q5_title'] = 'Please provide details of the patient/appropriate person who was informed.';
$txt['inc_ot_q6_title'] = 'Was an apology provided to the patient/appropriate person?';
$txt['inc_ot_q7_title'] = 'Was a truthful account of the facts known at the time shared with the patient/appropriate person?';
$txt['inc_ot_q8_title'] = 'Was the patient/appropriate person advised about next investigative steps to be undertaken?';
$txt['inc_ot_q9_title'] = 'Was a written memo of the initial disclosure securely recorded?';
$txt['inc_ot_q10_title'] = 'Was a copy of this memo provided to the patient/appropriate person?';
$txt['inc_ot_q11_title'] = 'When was this memo provided to the patient/appropriate person?';
$txt['inc_ot_q12_title'] = 'Following a thorough investigation were details related to personnel or system insufficiencies/failures discussed';
$txt['inc_ot_q13_title'] = 'Were details of personnel or system insufficiencies/failures and/or errors included in a report?';
$txt['inc_ot_q14_title'] = 'Following investigation was a detailed written  investigation report securely recorded?';
$txt['inc_ot_q15_title'] = 'Was a copy of this detailed report provided in full to the patient/appropriate person?';
$txt['inc_ot_q16_title'] = 'Were unsuccessful attempts to contact the patient/appropriate person recorded?';
$txt['inc_ot_q17_title'] = 'Were support services offered to the patient/appropriate person affected by the incident?';
$txt['inc_ot_q18_title'] = 'Did the patient/appropriate person accept the support services offered?';
$txt['inc_ot_q19_title'] = 'What was the nature of support services provided?';
$txt['inc_ot_q20_title'] = 'Were follow-up discussions offered to the patient/appropriate person?';

// Actions
$txt["action_saved"] = "The action has been saved";
$txt["act_type"] = "Type";
$txt["act_priority"] = "Priority";
$txt["act_description"] = "Description";
$txt["act_responsible"] = "Responsible";
$txt["act_start"] = "Start";
$txt["act_due"] = "Due";
$txt["act_done"] = "Done";
$txt["act_cost"] = "Cost";
$txt["no_actions_for_inc"] = "No actions for this record.";
$txt["new_actions"] = "New actions";
$txt["create_new_action"] = "Create a new action";
$txt["act_no_actions"] = 'No actions';
$txt["action_complete"] = "Complete";
$txt['overdue_actions_listing'] = 'Overdue Actions Listing';

// SaveIncident
$txt["another_user_approved"] = "Another user has approved this <datix INCName> while you were viewing it.";
$txt["cannot_save_others_err"] = "You cannot save a record that has been assigned to someone else.";
$txt["date_in_future_err"] = "Date cannot be later than today";
$txt["date_earlier_than_err"] = "Date cannot be earlier than";
$txt["date_later_than_err"] = "Date cannot be later than";
$txt["large_money_err"] = "Money values cannot be larger than ".GetParm('CURRENCY_CHAR', '£')."99999999999.99";
$txt["time_in_future_err"] = "Time cannot be later than today";
$txt["time_earlier_than_err"] = "Time cannot be earlier than";
$txt["inc_being_submitted"] = "Your <datix INCName> is being submitted.  Please wait.";
$txt["title_save_incident"] = "Save <datix INCName>";
$txt["inc_being_submitted_sub"] = "Please do not click Back or Refresh in your browser or close the browser window until you receive confirmation that the <datix INCName> has been submitted successfully.";
$txt["inc_save_sql_err"] = "An error occurred when trying to save the <datix INCName>.  Please report the following to the Datix administrator:";
$txt["cannot_save_inc_err"] = "Cannot save the <datix INCName>.  This may be because you clicked Refresh in your browser.";
$txt["cannot_restore_rejected"] = "An error occurred when trying to restore the rejected <datix INCName>";
$txt["incident_id_number"] = "<datix INCNameTitle> Number";
$txt["incident_report_number"] = "<datix INCNameTitle> Number";
$txt["person_saved"] = "The person has been saved.";
$txt["person_not_saved"] = "The person has not been saved.";
$txt["incident_report_saved_holding"] = "The <datix INCName> has been saved.";
$txt["incident_report_saved_holding_pending"] = "The <datix INCName> has been saved.";
$txt["incident_report_rejected"] = "The <datix INCName> has been rejected.";
$txt['incident_report_rejected_reason'] = 'The reason for rejection has been saved.';
$txt["incident_report_restored"] = "The rejected <datix INCName> has been restored to the holding area.";
$txt["add_another_contact"] = "Add another contact";
$txt["btn_add_another_contact"] = "Add another contact";
$txt["add_another_contact_sub"] = "Use this option to add additional people affected, members of staff, witnesses or any other contact.";
$txt["btn_finish_adding_incident"] = "Finish";
$txt["btn_add_another_incident"] = "Add another <datix INCName>";
$txt["btn_edit_this_incident"] = "Edit";
$txt["invalid_nhs_num_err"] = "Please enter a valid NHS number";

// Messages
$txt["secure_message"] = "Secure message";
$txt["message"] = "Message";
$txt["message_status"] = "Message status";
$txt["send_msg_to_selected"] = "Send message to selected recipients";
$txt["date_sent"] = "Date sent";
$txt["date_read"] = "Date read";
$txt["comments"] = "Comments";
$txt["no_contacts"] = "No contacts";

// Security settings
$txt["user_record_access"] = "Record access for users";
$txt["group_record_access"] = "Record access for security groups";
$txt["user_report_access"] = "Report access for users";
$txt["group_report_access"] = "Report access for security groups";
$txt["profile_report_access"] = "Report access for profiles";
$txt["user"] = "User";
$txt["add_users"] = "Add Users";
$txt["user_already_linked"] = "This user is already linked to this record";
$txt["group_already_linked"] = "This group is already linked to this record";
$txt['form_design_override'] = 'This option overrides the setting in \'Design forms\'';

//Error messages
$txt["cannot_save_err"] = "Your changes could not be saved.  This may be because you have refreshed your browser or because another user saved the record while you were working on it.";
$txt["cannot_read_file"] = "The file cannot be read.";
$txt["invalid_load_file"] = "Incorrect file format. Header line not found.";
$txt["load_file_error"] = "Failed to import load file.";
$txt["npsa_export_error_title"] = "Incomplete/Missing Data";
$txt["npsa_export_error_message"] = "NPSA data is missing from the following incidents/fields.  Please complete the required fields and run the export again.";
$txt["npsa_export_invalid_config_title"] = "Invalid configuration";
$txt["npsa_export_invalid_config_message"] = "You are unable to create an XML file for the NRLS dataset 1 when you are exporting from CCS2.";

// Main menu
$txt['options'] = "Options";
$txt['status'] = "Status";
$txt['statuses'] = "Statuses";
$txt['searches'] = "Searches";
$txt["list_search"] = "List search results";
$txt["new_search"] = "New search";
$txt['clear_search'] = 'Clear the current search';
$txt["saved_queries"] = "Saved queries";
$txt["pinned_queries"] = "Pinned queries";
$txt["saved_filters"] = "Saved filters";
$txt["listings"] = "Listings";
$txt["current_record"] = "Current record";
$txt["overdue"] = "Overdue";
$txt["overdue_items"] = "There are overdue items";
$txt["act_overdue_items"] = "<datix overdue_items>";
$txt["inc_overdue_items"] = "<datix overdue_items>";
$txt["pal_overdue_items"] = "<datix overdue_items>";
$txt["civ_overdue_items"] = "<datix overdue_items>";
$txt["sab_overdue_items"] = "<datix overdue_items>";
$txt['overdue_records'] = 'overdue records';
$txt["unread_messages"] = "Unread messages:";
$txt["design_report"] = "Design a report";
$txt["my_reports"] = "My reports";
$txt["my_dashboard"] = "My Dashboard";
$txt["manage_duplicates"] = "Manage duplicates";

//General Menu Items
$txt["contacts"] = "Contacts";
$txt["equipment"] = "Equipment";
$txt["notepad"] = "Notepad";
$txt["actions"] = "Actions";
$txt["documents"] = "Documents";
$txt["linked_records"] = "Linked records";
$txt["causal_factors"] = "Causal factors";
$txt["add_a_new"] = "Add a new";
$txt["record"] = "record";
$txt["records"] = "records";


// Incidents menu
$txt["current_incident"] = "<datix current_record>";
$txt["incident_details"] = "<datix INCNameTitle> details";
$txt["investigation"] = "Investigation";
$txt["causes"] = "Causes";
$txt["contacts"] = "Contacts";
$txt["equipment"] = "Equipment";
$txt["medication"] = "Medication";
$txt["notepad"] = "Notepad";
$txt["actions"] = "Actions";
$txt["documents"] = "Documents";
$txt["violence_staff"] = "Violence against staff";
$txt["notifications_sent"] = "Notifications sent";
$txt["permissions"] = "Permissions";
$txt["print_title"] = "Print";
$txt["print"] = "Print";
$txt["secure_messages"] = "Secure messages";
$txt["new_secure_message"] = "New secure message";
$txt["secure_message_history"] = "Secure message history";
$txt["incident_listings"] = '<datix listings>';
$txt["list_all_incidents"] = "List all";
$txt["new_search_incidents"] = '<datix new_search>';
$txt["reports"] = "Reports";
$txt["new_incident"] = "New <datix INCName>";
$txt["security"] = "Security";
$txt["show_dif1_snapshot"] = "Show DIF1 snapshot";
$txt["show_dif1_values"] = "Show DIF1 values";
$txt["audit_trail"] = "Audit trail";
$txt["main_menu_title"] = "Main Menu";
$txt["new_user_security_group"] = "New user security group";
$txt["security_group"] = "Security group";
$txt["security_group_already_linked"] = "Security group already linked to user";
$txt["user_security_group"] = "User security group";
$txt['add_new_incident'] = 'Add a new <datix INCName>';
$txt['show_staff_responsibilities'] = 'Show staff responsibilities';

//Equipment Menu
$txt["new_asset"] = "New <datix ASTName>";
$txt["add_new_asset"] = "Add a new piece of <datix ASTName>";
$txt["list_all_equipment"] = "List all <datix ASTName>";
$txt["no_asset_records_found"] = "No <datix ASTName> linked.";

//Equipment buttons
$txt['btn_check_match_equipment'] = 'Check for matching <datix ASTName>';
$txt['btn_new_link_equipment'] = 'Create new link';
$txt['btn_unlink_equipment'] = 'Unlink <datix ASTName>';
$txt['btn_search_for_equipment'] = 'Search for existing <datix ASTName>';

//Risk menu ---------------------------

$txt['add_new_risk'] = 'Add a new <datix RAMName>';
$txt['assurance_framework'] = 'Assurance Framework';
$txt['back_to_assurance_framework'] = '<datix back_to> <datix assurance_framework>';

//Action menu ---------------------------

$txt['list_all_actions'] = 'List all <datix ACTNames>';

//Claims menu ---------------------------

$txt['add_new_claim'] = 'Add new <datix CLAName>';
$txt['list_open_claims'] = 'List open <datix CLANames>';
$txt['list_closed_claims'] = 'List closed <datix CLANames>';

//Medications menu --------------

$txt['add_new_medication'] = 'Add a new <datix MEDName>';
$txt['list_all_medication'] = 'List all <datix MEDNames>';

//Medications module
$txt['min_character_error'] = 'Please add a minimum of 4 characters before searching, or select a value from one of the following fields: %s';

// Contacts Module

$txt['back_to_contacts_list'] = 'Back to Contacts List';

$txt['create_new_contact'] = 'Create a new <datix CONName>';
$txt['list_all_contacts'] = 'List all <datix CONNames>';

$txt['contacts_require_approval'] = 'The following <datix CONNames> require approval:';

$txt['redefine_criteria'] = 'Redefine criteria';

$txt['automatically_merge_contacts'] = 'Automatically merge contacts';


// -----------------------------------

// Complaints Module

$txt['no_due_date_calc'] = 'Cannot automatically calculate due dates when done dates have been added.';

$txt['complaints_subjects'] = 'Subjects';

// Complaints listings
$txt["com_unapproved_listing_title"] = "Unapproved <datix COMNames>";
$txt["com_awatingacknowledgement_listing_title"] = "<datix COMNamesTitle> awaiting acknowledgement";
$txt["com_awatinginivestigation_listing_title"] = "<datix COMNamesTitle> awaiting investigation";
$txt["com_underinvestigation_listing_title"] = "<datix COMNamesTitle> under investigation";
$txt["com_awaitingholding_listing_title"] = "<datix COMNamesTitle> awaiting holding letter";
$txt["com_awaitingreply_listing_title"] = "<datix COMNamesTitle> awaiting final reply";
$txt["com_completed_listing_title"] = "Completed <datix COMNames>";
$txt["com_rejected_listing_title"] = "Rejected <datix COMNames>";

//complainant chain
$txt['com_chain_due'] = 'Due';
$txt['com_chain_done'] = 'Done';

$txt['com_chain_acknowledged'] = 'Acknowledged';
$txt['com_chain_actioned'] = 'Actioned';
$txt['com_chain_response'] = 'Response';
$txt['com_chain_holding'] = 'Holding';
$txt['com_chain_replied'] = 'Replied';

//contacts
$txt['com_complainant'] = "Complainant";
$txt['com_complainant_plural'] = "Complainants";
$txt['com_complainant_link'] = "Create a new <datix com_complainant> link";
$txt['no_com_complainant_plural'] = "No <datix com_complainant_plural>";
$txt['com_person_affected'] = "Person Affected";
$txt['com_person_affected_link'] = "Create a new <datix com_person_affected> link";
$txt['com_person_affected_plural'] = "People Affected";
$txt['no_com_person_affected_plural'] = "No <datix com_person_affected_plural>";
$txt['com_employee'] = "Employee";
$txt['com_employee_plural'] = "Employees";
$txt['com_employee_link'] = "Create a new <datix com_employee> link";
$txt['no_com_employee_plural'] = "No Employees";
$txt['com_other_contact'] = "Other contact";
$txt['com_other_contact_plural'] = "Other contacts";
$txt['com_other_contact_link'] = "Create a new <datix com_other_contact> link";
$txt['no_com_other_contact_plural'] = "No <datix com_other_contact_plural>";

// Field labels
$txt["com_detail"] = "Description of <datix COMName>";
$txt["com_method"] = "Method of <datix COMName>";
$txt["link_patrelation"] = "Relationship to person affected by the <datix COMName>";
$txt["link_complpat"] = "Is the <datix com_complainant> the person affected by the <datix COMName>?";

// Form Titles
$txt["com1_title"] = "Datix <datix COMNamesTitle> Form (COM1)";
$txt["com2_title"] = "Datix <datix COMNamesTitle> Form (COM2)";
$txt["hsa_title"] = "Datix <datix HSANamesTitle> Form";
$txt["hot_title"] = "Datix <datix HOTNamesTitle> Form";
$txt["cla1_title"] = "Datix <datix CLANamesTitle> Form (CLAIM1)";
$txt["cla2_title"] = "Datix <datix CLANamesTitle> Form (CLAIM2)";

//-----------------------------------

// Generic Modules

$txt['add_new_record_section'] = 'Add new record';
$txt['add_new_record'] = 'Add new';

// Post submission messages
$txt['btn_add_another_record'] = 'Add another record';
$txt['btn_back_to_record'] = 'Back to record';
$txt['btn_print_and_finish'] = 'Print and Finish';

$txt["INC_1_saved_title"] = '<datix INCNameTitle> reference ';
$txt["INC_saved_title"] = '<datix INCNameTitle> number ';
$txt["RAM_saved_title"] = '<datix RAMNameTitle> number ';
$txt["COM_saved_title"] = '<datix COMNameTitle> number ';
$txt["CLA_saved_title"] = '<datix CLANameTitle> number ';
$txt["PAL_saved_title"] = '<datix PALSNameTitle> number ';
$txt["HOT_saved_title"] = '<datix HOTNameTitle> number ';
$txt["HSA_saved_title"] = '<datix HSANameTitle> number ';
$txt["DST_saved_title"] = '<datix DSTNameTitle> number ';
$txt["AST_saved_title"] = '<datix ASTNameTitle> number ';
$txt["PAY_saved_title"] = '<datix PAYNameTitle> number ';
$txt["LIB_saved_title"] = '<datix LIBNameTitle> number ';
$txt["CQO_saved_title"] = '<datix CQONameTitle>';
$txt["CQP_saved_title"] = '<datix CQPNameTitle>';
$txt["CQS_saved_title"] = '<datix CQSNameTitle>';
$txt["LOC_saved_title"] = '<datix LOCNameTitle> number ';
$txt["ATM_saved_title"] = '<datix ATMNameTitle> number ';
$txt["ATQ_saved_title"] = '<datix ATQNameTitle> number ';
$txt["ATI_saved_title"] = '<datix ATINameTitle> number ';
$txt["AMO_saved_title"] = '<datix AMONameTitle> number ';
$txt["AQU_saved_title"] = '<datix AQUNameTitle> number ';

$txt["INC_report_saved"] = '<datix INCName> has been saved. The reference number is ';
$txt["RAM_report_saved"] = '<datix RAMName> has been saved. The record ID is ';
$txt["COM_report_saved"] = '<datix COMName> has been saved. The record ID is ';
$txt["CLA_report_saved"] = '<datix CLAName> has been saved. The record ID is ';
$txt["PAL_report_saved"] = '<datix PALSName> has been saved. The record ID is ';
$txt["HSA_report_saved"] = '<datix HSAName> has been saved. The record ID is ';
$txt["HOT_report_saved"] = '<datix HOTName> has been saved. The record ID is ';
$txt["DST_report_saved"] = '<datix DSTName> has been saved. The record ID is ';


$txt["PAY_record_saved"] = '<datix PAYNameTitle> record saved';

$txt["INC_2_record_saved"] = '<datix INCNameTitle> record saved ';
$txt["RAM_2_record_saved"] = '<datix RAMNameTitle> record saved';
$txt["COM_2_record_saved"] = '<datix COMNameTitle> record saved';
$txt["CLA_2_record_saved"] = '<datix CLANameTitle> record saved';
$txt["PAL_2_record_saved"] = '<datix PALSNameTitle> saved'; // PALSName has 'record' in it
$txt["HOT_2_record_saved"] = '<datix HOTNameTitle> record saved';
$txt["HSA_2_record_saved"] = '<datix HSANameTitle> record saved';
$txt["DST_2_record_saved"] = '<datix DSTNameTitle> record saved';
$txt["AST_2_record_saved"] = '<datix ASTNameTitle> record saved';
$txt["PAY_2_record_saved"] = '<datix PAYNameTitle> record saved';
$txt["LIB_2_record_saved"] = 'Evidence saved';
$txt["CQO_2_record_saved"] = '<datix CQONameTitle> record saved';
$txt["CQP_2_record_saved"] = '<datix CQPNameTitle> record saved';
$txt["CQS_2_record_saved"] = '<datix CQSNameTitle> record saved';
$txt["LOC_2_record_saved"] = '<datix LOCNameTitle> record saved';
$txt["ATM_2_record_saved"] = '<datix ATMNameTitle> record saved';
$txt["ATQ_2_record_saved"] = '<datix ATQNameTitle> record saved';
$txt["ATI_2_record_saved"] = '<datix ATINameTitle> record saved';
$txt["AMO_2_record_saved"] = '<datix AMONameTitle> record saved';
$txt["AQU_2_record_saved"] = '<datix AQUNameTitle> record saved';
$txt["SABS_2_record_saved"] = '<datix SABSNameTitle> record saved';

$txt['STN_save_message'] = '<datix STNNameTitle> record saved';
$txt['STN_evidence_save_message'] = 'Evidence saved';
$txt['CON_save_message'] = '<datix CONNameTitle> record saved';

$txt["level_2_record_ref"] = 'The record ID is ';
$txt['invalid_characters_in_fields'] = 'There are invalid characters in one of the fields you have searched on.';

// HSA listings
$txt["com_all_listing_title"] = "List all <datix HSANames>";
$txt["com_all_listing_link"] = "List all <datix HSANames>";

// HOT listings
$txt["hot_open_listing_title"] = "List open <datix HOTNames>";
$txt["hot_open_listing_link"] = "List open <datix HOTNames>";

// Payments listing
$txt["pay_all_listing_title"] = "List all <datix PAYNames>";
$txt["pay_all_listing_link"] = "List all <datix PAYNames>";

//Feedback
$txt["feedback_title"] = "Communication and feedback";
$txt["feedback_sent"] = "Message has been sent";
$txt["feedback_sent_full"] = "Feedback has been sent to the following addresses:";
$txt["feedback_err"] = "A problem was encountered when trying to send the e-mail.  The message has not been sent.";
$txt["recipients"] = "Recipients";
$txt["feedback_recipients_explain"] = "Only linked contacts with e-mail addresses are shown.";
$txt["fbk_email_check_recipients"] = "You have identified recipients for a feedback email but not sent it.  Please choose from the options below or select Cancel to return to the record.";
$txt["fbk_email_check_amend"] = "You have amended the feedback email text but not identified recipients.  Select Cancel to return to the record or save the record without sending the email.";
$txt["btn_send_and_save"] = "Send email and save record";
$txt["btn_save_without_sending"] = "Save without sending email";
$txt["btn_feedback_cancel"] = "Cancel";
$txt['staff_contacts'] = 'Staff and contacts attached to this record';
$txt['feedback_staff_contacts_explain'] = 'Only staff and contacts with e-mail addresses are shown.';
$txt['all_users'] = 'All users';
$txt['feedback_all_users_explain'] = 'Only users with e-mail addresses are shown.';
$txt['message_title'] = 'Message';
$txt['subject'] = 'Subject';

//Library module

$txt["lib_list_all"] = "List all <datix LIBNames>";
$txt["lib_list_all_link"] = "List all <datix LIBNames>";

//Distribution list module
$txt["dst_list_all"] = "List all <datix DSTNames>";
$txt["dst_list_all_link"] = "List all <datix DSTNames>";
$txt["dst_list_my"] = "List my <datix DSTNames>";
$txt["dst_list_my_link"] = "List my <datix DSTNames>";

$txt["dst_member"] = 'Member';
$txt["dst_member_plural"] = 'Members';

//Claims module
$txt["cla1_user"] = "CLAIM1 form for this user";
$txt["cla2_user"] = "CLAIM2 form for this user";
$txt["cla2_search_form_user"] = "CLAIM2 search form for this user";
$txt["cla1_input_only"] = "CLAIM1 Input only";
$txt["cla2_access"] = "Full access to CLAIM2";
$txt["cla2_read_only"] = "Read only access to CLAIM2";
$txt['potential_claims'] = "Potential claims";
$txt['confirmed_claims'] = 'Confirmed claims';
$txt['closed_claims'] = 'Closed claims';
$txt['rejected_claims'] = 'Rejected claims';
$txt['cla_claimant'] = 'Claimant';
$txt['cla_claimant_plural'] = 'Claimants';
$txt['cla_claimant_link'] = 'Create a new <datix cla_claimant> link';
$txt['no_cla_claimant_plural'] = 'No <datix cla_claimant_plural>';
$txt['cla_person'] = 'Person';
$txt['cla_person_plural'] = 'Persons';
$txt['cla_person_link'] = 'Create a new <datix cla_person> link';
$txt['no_cla_person_plural'] = 'No <datix cla_person_plural>';
$txt['cla_contact'] = 'Contact';
$txt['cla_reporter'] = 'Reporter';
$txt['cla_employee'] = 'Employee';
$txt['cla_employee_plural'] = 'Employees';
$txt['no_cla_employee_plural'] = 'No <datix cla_employee_plural>';
$txt['cla_employee_link'] = 'Create a new <datix cla_employee> link';
$txt['cla_other_contact'] = 'Other Contact';
$txt['cla_other_contact_plural'] = 'Other Contacts';
$txt['cla_other_contact_link'] = 'Create a new <datix cla_other_contact> link';
$txt['no_cla_other_contact_plural'] = 'No <datix cla_other_contact_plural>';
$txt['cla_organisation_respondent'] = 'organisation <datix respondent>';
$txt['cla_organisation_respondent_title'] = 'Organisation <datix respondent>';
$txt['cla_organisation_respondent_link'] = 'Create a new \'<datix ORGNameTitle>\' respondent link';
$txt['cla_individual_respondent'] = 'individual <datix respondent>';
$txt['cla_individual_respondent_plural'] = 'individual <datix respondents>';
$txt['cla_individual_respondent_link'] = "Create a new 'Individual' <datix respondent> link";
$txt['no_cla_individual_respondent_plural'] = 'No individual <datix respondents>';
$txt['cla_individual_respondent_title'] = 'Individual <datix respondent>';
$txt['cla_individual_respondent_plural_title'] = 'Individual <datix respondents>';

$txt['stage_history'] = 'Stage History';
$txt['stage_history_description'] = 'Description';
$txt['stage_history_date'] = 'Date';
$txt['stage_history_edit'] = '[edit]';
$txt['stage_history_delete'] = '[delete]';
$txt['stage_history_add_stage'] = 'Add stage';
$txt['stage_history_edit_stage'] = 'Edit stage';
$txt['stage_history_delete_confirm'] = 'Are you sure you wish to delete?';
$txt["approval_status_CLA_UN"] = "Potential";
$txt["approval_status_CLA_ACT"] = "Confirmed";
$txt["approval_status_CLA_CLOSED"] = "Closed";
$txt["approval_status_CLA_REJECT"] = "Rejected";
$txt['respondent'] = 'respondent';
$txt['respondents'] = 'respondents';
$txt['respondent_title'] = 'Respondent';
$txt['respondents_title'] = 'Respondents';
$txt['responsibility_not_100'] = 'Total responsibility does not equal 100%';
$txt['select_listing_for_duplicates'] = "Select listing design to use for duplicates matching";
$txt['payments_assigned_to_respondent'] = 'There are payments assigned to this <datix respondent>. You must reassign these payments before unlinking the <datix respondent>.';

//Payments module
$txt["pay_user"] = "Payment form for this user";
$txt["pay_search_form_user"] = "Payment search form for this user";
$txt["pay_input_only"] = "Input only access to <datix mod_payments_title>";
$txt["pay_read_only"] = "Read only access to <datix mod_payments_title>";
$txt["pay_full_access"] = "Full access to <datix mod_payments_title>";
$txt["linked_PAY_saved"] = "The <datix PAYName> has been saved";

//Policies module
$txt["pol_full"] = "Full access";
$txt["pol_listing"] = "Policies listing";
$txt["pol_read_only"] = "Read only";
$txt["POL_2_record_saved"] = 'The <datix POLName> has been saved';
$txt["POL_saved_title"] = '<datix POLNameTitle> number ';
$txt["add_new_policy"] = "Add a new <datix POLName>";
$txt["list_all_policy"] = "List all <datix POLNames>";
$txt['pol2_user'] = 'POL2 form for this user';
$txt['pol2_search_form_user'] = 'POL2 search form for this user';
$txt['create_a_new_policy_link'] = 'Create a new <datix POLName> link';
$txt['link_a_policy_to_a_respondent'] = 'Link a <datix POLName> to a <datix respondent_title>';
$txt['check_for_matching_policies'] = 'Check for matching <datix POLNames>';
$txt['select_a_policy'] = 'Select a <datix POLName>';
$txt['policy_inaccurate1'] = 'The <datix POLName> selection you have made appears to be inaccurate, based upon the <datix POLName> basis and the date of the <datix CLAName> or the <datix INCName> to which this <datix CLAName> relates. Confirm your selection or cancel to make a new selection.';
$txt['policy_inaccurate2'] = 'The <datix INCName> date is not set, and so the <datix POLName> selected may not cover this <datix CLAName>. Confirm your selection or cancel to make a new selection';
$txt['policy_inaccurate3'] = 'The <datix CLAName> date is not set, and so the <datix POLName> selected may not cover this <datix CLAName>. Confirm your selection or cancel to make a new selection';
$txt['policies_inaccurate'] = 'The coverage dates for one or more of the <datix POLNames> selected indicate that this <datix CLAName> is not covered';
$txt['policy_link_created'] = '<datix POLNameTitle> link created';
$txt['policy_link_deleted'] = '<datix POLNameTitle> link deleted';
$txt['unlink_policy'] = 'Unlink <datix POLName>';

//To DO List module
$txt["tod_full_access"] = "Full access to <datix mod_todo_title>";
$txt["linked_TOD_saved"] = "The <datix TODName> has been saved";
$txt["todo_list_staff_fields_act"] = "<datix ACTNameTitle> staff fields for To Do List";
$txt["todo_list_staff_fields_inc"] = "<datix INCNameTitle> staff fields for To Do List";
$txt["todo_list_staff_fields_com"] = "<datix COMNameTitle> staff fields for To Do List";
$txt["todo_list_staff_fields_ram"] = "<datix RAMNameTitle> staff fields for To Do List";
$txt["todo_list_staff_fields_cqc"] = "<datix CQCNamesTitle> staff fields for To Do List";
$txt["todo_list_staff_fields_hint"] = "Choose which staff fields are used to determine what appears on the To Do List";

//CQC module
$txt["cqc_outcome"] = "Outcome";
$txt["cqc_outcomes"] = "Outcomes";
$txt["cqc_prompt"] = "Prompt";
$txt["cqc_prompts"] = "Prompts";
$txt["cqc_subprompt"] = "Sub-prompt";
$txt["cqc_subprompts"] = "Sub-prompts";
$txt["cqc_list_outcomes"] = "List all <datix cqc_outcomes>";
$txt["cqc_list_prompts"] = "List all <datix cqc_prompts>";
$txt["cqc_list_subprompts"] = "List all <datix cqc_subprompts>";

//Locations
$txt["location"] = "Location";
$txt["locations"] = "Locations";
$txt["list_locations"] = "List all <datix locations>";

$txt["loc_full"] = "Full access";
$txt["loc_read_only"] = "Read only access";
$txt["cqo_full"] = "Full access";
$txt["cqo_read_only"] = "Read only access";
$txt["cqp_full"] = "Full access";
$txt["cqp_read_only"] = "Read only access";
$txt["cqs_full"] = "Full access";
$txt["cqs_read_only"] = "Read only access";

$txt["edit_cqc_templates"] = "Edit CQC templates";
$txt["apply_outcomes"] = "Apply outcomes";
$txt["select_outcomes"] = "Select outcomes";
$txt["apply_cqc_outcomes"] = "Apply CQC outcomes";
$txt["view_cqc_locations"] = "View CQC locations";

$txt['cqc_add_evidence'] = 'Add evidence';

// Assessment Modules
$txt['amo_assessment_template'] = "<datix ATMName>";
$txt['amo_assessment_templates'] = "<datix ATMNamesTitle>";
$txt['amo_criterion_template'] = "<datix ATQNameTitle>";
$txt['amo_criterion_templates'] = "<datix ATQNamesTitle>";
$txt['amo_assigned_assessment'] = "<datix ATINameTitle>";
$txt['amo_assigned_assessments'] = "<datix ATINamesTitle>";
$txt['amo_assessment'] = "<datix AMONameTitle>";
$txt['amo_assessments'] = "<datix AMONamesTitle>";
$txt['amo_criterion'] = "<datix AQUNameTitle>";
$txt['amo_criteria'] = "<datix AQUNamesTitle>";
$txt["amo_list_assessment_templates"] = "List <datix amo_assessment_templates>";
$txt["amo_list_criterion_templates"] = "List <datix amo_criterion_templates>";
$txt["amo_list_assigned_assessments"] = "List <datix amo_assigned_assessments>";
$txt["amo_list_assessments"] = "List <datix amo_assessments>";
$txt["amo_list_criteria"] = "List <datix amo_criteria>";
$txt['add_new_assessment_template'] = "Add a new <datix amo_assessment_template>";

$txt["atm_full"] = "Full access";
$txt["atq_full"] = "Full access";
$txt["ati_read_only"] = "Read Only";
$txt["ati_submit"] = "Submitter";
$txt["ati_review"] = "Reviewer";
$txt["amo_full"] = "Full access";
$txt["aqu_full"] = "Full access";
$txt["amo_edit_open"] = "Edit open <datix AMONames> and review submitted ones";
$txt['aqu_edit_open'] = 'Edit open <datix AQUNames> and review submitted ones';

$txt["linked_AQU_saved"] = "The <datix AQUName> has been saved";
$txt["linked_ATQ_saved"] = "The <datix ATQName> has been saved";

$txt["cycle_year"] = "Cycle year";
$txt["due_date"] = "Due date";

$txt["distribute_assessment_modules"] = "Distribute <datix AMONames>";
$txt["select_cycle_year_and_locations"] = "Select cycle year and locations";
$txt["select_assessment_modules"] = "Select <datix AMONames>";
$txt["select_all"] = "Select all";
$txt["deselect_all"] = "Deselect all";
$txt["cycle_locations_error"] = "Please select a cycle year, a due date in the future and at least one location";
$txt["selected_locations"] = "Selected locations";
$txt["number_of_assessments_distributed"] = "Number of <datix AMONames> distributed: ";
$txt["generate_assessment_instances"] = "Generate instances";
$txt["create_assessment_instances"] = "Create instances";
$txt["create_assigned_assessments"] = "Create assignments";
$txt["manage_new_cycles"] = "Manage new cycles";
$txt["select_assessment_locations"] = "Select <datix AMOName> locations";
$txt["select_modules"] = "Select <datix AMONames>";
$txt["no_modules_available"] = "No <datix AMONames> available";
$txt["locations_error"] = "Please select at least one location";
$txt["number_of_modules_created"] = "Number of <datix AMONames> created: ";
$txt["select_template_type"] = "Select template type";
$txt["template_type"] = "Template type";
$txt['manage_locations'] = 'Manage locations';
$txt['search_locations'] = 'Search Locations';

$txt["select"] = "Select";

$txt['success_generate_instances'] = 'Successfully generated instances.';
$txt['existing_records_error'] = 'It is not possible to generate instances for locations above (and in the same path as) for which an assigned template has been created.';
$txt['multiple_tiers_error'] = 'It is not possible to simultaneously select locations at multiple tiers.';
$txt['greyed_out_locations_info'] = 'Some locations are unavailable for selection due to assignments or instances already existing for them or for dependent locations';
$txt['invalid_locations_error'] = 'The selected <datix AMONames> have not been created due to assignments or instances already existing for them or for dependent locations';
$txt['no_locations_info'] = 'No common locations exist for all selected <datix ATINames>. Select another set and retry.';

$txt['cycle_controller_no_permission'] = 'You do not have permission to perform this action.';
$txt['cycle_controller_select_records'] = 'You must select some records to re-distribute.';
$txt['cycle_controller_no_review'] = 'Only assigned assessements that have been reviewed can be re-distributed.';
$txt['cycle_controller_final_year'] = 'Some of the assigned assessments you have selected are in their final cycle year, and so cannot be re-distributed.';
$txt['cycle_controller_final_period'] = 'Some of the assigned assessments you have selected are in their final assessment period, and so cannot be re-distributed.';
$txt['cycle_controller_missing_years'] = 'Some of the assigned assessments you have selected have no value for Assessment year or Cycle year. Both of these values must be set to use the re-distribution feature.';
$txt['cycle_controller_already_redistributed'] = 'Some of the assigned assessments you have selected have already been re-distributed.';

//Batch update
$txt['batch_update_error'] = 'Please choose a field to update and values to change.';

$txt['back_loc_hier'] = 'Back to Location Hierarchy';


//-----------------------------------
  //Documents

$txt['missing_document_alt_text'] = 'This document cannot be found.  This may be because it has been deleted or its location has been changed.  Please contact your Datix administrator.';
$txt['missing_document_explanation'] = 'This symbol identifies a document that cannot be found.  This may be because it has been deleted or its location has been changed.  Please contact your Datix administrator.';
$txt['large_document_upload_error'] = 'The size of the file(s) you have attempted to attach exceed the allowed upload limit and the information you have inputted has not been saved. Please input the information again but do not attach the file(s). Make a note of the reference number after submitting and send the document and this reference to your Datix system administrator. Please ask your administrator to contact Datix Support for details on how to increase the allowed upload limit.';
$txt['delete_document'] = 'Delete document?';
//-----------------------------------

//Reporting
$txt['listing_report_settings_title'] = "Listing report settings";
$txt['listing_report_view_settings_title'] = "Listing report view settings";
$txt['base_report_settings_title'] = "Base report settings";
$txt['packaged_report_settings_title'] = "Packaged report settings";
$txt['packaged_reports'] = "Packaged reports";
$txt['packaged_reports_title'] = "<datix packaged_reports>";
$txt['new_packaged_report'] = "New packaged report";
$txt['no_packaged_reports'] = "No packaged reports.";
$txt['package_report_header_name'] = "Package report name";
$txt['packaged_report_admin_menu_title'] = "Packaged report admin";
$txt['delete_base_report'] = "Delete base report";
$txt['export_report_design'] = "Export report design";
$txt['open_in_excel'] = "Open in Excel";
$txt['delete_report_q'] = "Delete this report?";
$txt['delete_packaged_report'] = "Delete packaged report";
$txt['delete_template'] = "Delete template";
$txt['delete_template_q'] = "Delete this template?";
$txt['delete_packaged_report_q'] = "Delete this packaged report?";
$txt['reports_administration_title'] = "Reports Administration";
$txt['graphical_reports'] = "Statistical reports";
$txt['listing_reports'] = "Listing reports";
$txt['reporting_fields_setup'] = "Reporting fields setup";
$txt['design_graphical_reports'] = "Manage statistical reports";
$txt['design_listing_reports'] = "Manage listing reports";
$txt['show_excluded_forms_fields'] = "Show excluded forms/fields";
$txt['hide_excluded_forms_fields'] = "Hide excluded forms/fields";
$txt['no_packaged_reports_available'] = "No DatixWeb packaged reports are available.";
$txt['no_listing_reports_available'] = "No DatixWeb listing reports are available.";
$txt['select_packaged_report'] = "Select packaged report";
$txt['select_packaged_report_description'] = "These packaged reports contain pre-defined record selection criteria.";
$txt['packaged_reports_selection_title'] = 'Packaged reports';
$txt['btn_run_packaged_report'] = 'Run packaged report';
$txt['packaged_report_saved'] = 'Packaged report saved.';
$txt['packaged_report_saved_permission_warning'] = "This report is currently accessible to all users.  To restrict access, identify in the Permissions section the specific users / groups who should have access to the report.";
$txt['reporting'] = "Reporting";
$txt['add_to_dashboard'] = 'Add to Dashboard';
$txt['add_to_my_reports'] = 'Add to My reports';
$txt['save_as_new'] = 'Save as new';
$txt['save_changes'] = 'Save changes';
$txt['my_report_settings_title'] = "My report settings";
$txt['dashboard_settings_title'] = "My Dashboard settings";
$txt['no_dashboards_message'] = "You have not been assigned any dashboards";
$txt['delete_my_report'] = "Delete My report";
$txt['my_report_deleted'] = "The report has been deleted from My reports.";
$txt['override_sec_warning'] = "Due to the design of this report, you are unable to access records by selecting values displayed within it";
$txt['btn_back_to_reports_menu'] = 'Back to My reports';
$txt['export_activex_disabled'] = "Running ActiveX controls in your browser is disabled. Please change your browser settings as explained below: <br/><br/> 1. From the menu select Tools -> Internet Options -> Security.  <br/><br/> 2. Select 'Local Intranet' and click 'Custom Level...'.  <br/><br/> 3. Enable the option 'Initialize and script ActiveX controls not marked as safe' and click OK.  <br/><br/> 4. Try again to export the report.";
$txt['btn_back_to_report'] = 'Back to report';
$txt['linegraph_tooltip_multiple'] = 'Multiple values - click to select';
$txt['at_prompt_setting'] = '@prompt setting';
$txt['edit_report'] = 'edit';
$txt['delete_report'] = 'delete';
$txt['query'] = 'Query';
$txt['report_type'] = 'Report type';
$txt['report_title'] = 'Custom title';
$txt['spc_report_type'] = 'Type';
$txt['rows'] = 'Rows';
$txt['columns'] = 'Columns';
$txt['x_axis'] = 'X axis';
$txt['y_axis'] = 'Y axis';
$txt['field_1'] = 'Field 1';
$txt['field_2'] = 'Field 2';
$txt['field'] = 'Field';
$txt['show_additional_options'] = 'Additional options';
$txt['select_base_listing_report'] = 'Base listing report';
$txt['select_spc_type'] = 'SPC report type';
//$txt['limit_rows_to_top_n'] = 'Limit rows to top (n)?';
$txt['display_top'] = 'Show top';
$txt['display_top_items'] = 'items'; // combines with ['display_top']
$txt['bar_orientation'] = 'Orientation';
$txt['chart_style'] = 'Chart style';
$txt['bar_style'] = 'Style';
$txt['pie_style'] = 'Style';
$txt['count_style'] = 'Count style';
$txt['value_only'] = 'Value only';
$txt['percentage_only'] = '% only';
$txt['percentage_and_value'] = '% and value';
$txt['number_of_records'] = 'Number of records';
$txt['sum_of_values'] = 'Sum of values';
$txt['ave_of_values'] = 'Average (mean) of values';
$txt['field_to_sum'] = 'Field to sum';
$txt['field_to_ave'] = 'Field to average';
$txt['field_value_at'] = 'Value at';
$txt['today'] = 'Today';
$txt['select_date'] = '(Select date)';
$txt['initial_levels'] = 'Include initial values';
$txt['initial_levels_help'] = 'Displays the initial value alongside the value on the date selected above, enabling comparison. Cannot be used in conjunction with a \'Field 2\' or \'Columns\' selection.';
$txt['show_as_percentages'] = 'Show %';
$txt['show_total_nulls'] = 'Count nulls';
$txt['null_help'] = 'Shows the number of records which have no value set for the selected field(s)';
$txt['additional_listing_options'] = 'Additional listing options';
$txt['include_elements'] = 'Include <datix ELENamesTitle>?';
$txt['include_compliance'] = 'Include Compliance?';
$txt['include_evidence'] = 'Include Evidence?';
$txt['include_documents'] = 'Include Documents?';
$txt['vertical'] = 'Vertical';
$txt['horizontal'] = 'Horizontal';
$txt['grouped'] = 'Grouped';
$txt['stacked'] = 'Stacked';
$txt['standard'] = 'Standard';
$txt['exploded'] = 'Exploded';
$txt['section_1'] = 'Section 1';
$txt['section_2'] = 'Section 2';
$txt['section_3'] = 'Section 3';
$txt['min_value'] = 'Min. value';
$txt['max_value'] = 'Max. value';
$txt['colour'] = 'Colour';
$txt['run_report'] = 'Run report';
$txt['update_report'] = 'Update report';
$txt['clear_settings'] = 'Clear settings';
$txt['clear_confirm'] = 'Are you sure you want to reset the report? Any unsaved changes will be lost.';
$txt['save_confirm'] = 'Are you sure you want to save this report? This will overwrite any previously saved data.';
$txt['changes_confirm'] = "You have made changes to this report but not clicked 'Update report', these changes will not be saved. Are you sure you want to continue?";
$txt['percentage_warning'] = 'Due to rounding of % values, the % total may not equal 100.';
$txt['form_label'] = 'Form';
$txt['field_label'] = 'Field';
$txt['date_label'] = 'Date option';
$txt['back_to_dashboard'] = 'Back to Dashboard';
$txt['initial_value'] = 'Initial value';
$txt['current_value'] = 'Current value';
$txt['datixweb_excel_export'] = 'DatixWeb Excel export';
$txt['datix_listing_report'] = 'Datix listing report';
$txt['merge_repeated_values'] = 'Merge repeated values';
$txt['error_loading_report_data'] = 'Error loading report data';
$txt['end_of_report'] = 'End of report';
$txt['retry_text'] = 'retry';

$txt['crosstab_report'] = 'Crosstab report';
$txt['listing_report'] = 'Listing report';
$txt['bar_chart'] = 'Bar chart';
$txt['bar_chart_horiz'] = 'Horizontal';
$txt['bar_chart_stacked_horiz'] = 'Stacked horizontal';
$txt['bar_chart_vert'] = 'Vertical';
$txt['bar_chart_stacked_vert'] = 'Stacked vertical';
$txt['line_graph'] = 'Line graph';
$txt['spc_chart'] = 'SPC chart';
$txt['pie_chart'] = 'Pie chart';
$txt['pie_chart_standard'] = 'Standard';
$txt['pie_chart_exploded'] = 'Exploded';
$txt['speedometer'] = 'Speedometer';
$txt['traffic_light_chart'] = 'Traffic light chart';
$txt['gauge_chart'] = 'Gauge chart';
$txt['pareto_graph'] = 'Pareto graph';
$txt['spc_chart_cchart'] = 'C-Chart';
$txt['spc_chart_ichart'] = 'I-Chart (X-Chart)';
$txt['spc_chart_movingrange'] = 'Moving range';
$txt['spc_chart_runchart'] = 'Run chart';

$txt['reporting_fields_available'] = 'Fields available for reporting';
$txt['reporting_fields_excluded'] = 'Fields excluded from reporting';
$txt['reporting_fields_exclude_button'] = 'Exclude >>>';
$txt['reporting_fields_include_button'] = '<<< Include';
$txt['reporting_fields_save_button'] = 'Save settings';
$txt['reporting_fields_cancel_button'] = 'Cancel';
$txt['reporting_fields_save_message'] = 'Settings have been saved';

//Entries referenced in the table_directory loadfile. (Used e.g. for the "Form" dropdown in the report designer).
$txt['table_compl_main'] = '<datix COMNamesTitle>';
$txt['table_pals_main'] = '<datix PALSNamesTitle>';
$txt['table_ra_main'] = '<datix RAMNamesTitle>';
$txt['table_sabs_main'] = '<datix SABNamesTitle>';
$txt['table_standard_prompts'] = '<datix PRONamesTitle>';
$txt['table_standards_main'] = '<datix STNNamesTitle>';
$txt['table_hotspots_agent'] = '<datix HSANamesTitle>';
$txt['table_library_main'] = '<datix LIBNamesTitle>';
$txt['table_distribution_lists'] = '<datix DSTNamesTitle>';
$txt['table_assets_main'] = '<datix ASTNamesTitle>';
$txt['table_link_assets'] = 'Linked <datix ASTNames>';
$txt['table_incidents_main'] = '<datix INCNamesTitle>';
$txt['table_contacts_main'] = '<datix CONNamesTitle>';
$txt['table_link_contacts'] = 'All <datix CONNames>';
$txt['table_sab_link_contacts'] = 'All <datix CONNames>';
$txt['table_standard_elements'] = '<datix ELENamesTitle>';
$txt['table_hotspots_main'] = '<datix HOTNamesTitle>';
$txt['table_medications_main'] = '<datix MEDNamesTitle>';
$txt['table_inc_medications'] = 'Linked <datix MEDNames>';
$txt['table_cqc_outcomes'] = '<datix CQONamesTitle>';
$txt['table_cqc_prompts'] = '<datix CQPNamesTitle>';
$txt['table_cqc_subprompts'] = '<datix CQSNamesTitle>';
$txt['table_payments'] = '<datix PAYNamesTitle>';
$txt['table_vw_payments'] = '<datix PAYNamesTitle>';
$txt['table_asm_template_modules'] = '<datix ATMNamesTitle>';
$txt['table_asm_template_questions'] = '<datix ATQNamesTitle>';
$txt['table_asm_templates'] = '<datix ATINamesTitle>';
$txt['table_asm_modules'] = '<datix AMONamesTitle>';
$txt['table_asm_questions'] = '<datix AQUNamesTitle>';
$txt['table_asm_question_templates'] = '<datix ATQNamesTitle>';
$txt['table_asm_data_modules'] = '<datix AMONamesTitle>';
$txt['table_asm_data_questions'] = '<datix AQUNamesTitle>';
$txt['table_asm_template_instances'] = '<datix ATINamesTitle>';
$txt['table_claims_main'] = '<datix CLANamesTitle>';
$txt['table_ca_actions'] = '<datix ACTNamesTitle>';
$txt['table_library_main'] = '<datix LIBNamesTitle>';
$txt['table_rev_main'] = 'Healthcare commission';
$txt['table_policies'] = '<datix POLNamesTitle>';
$txt['table_organisations_main_pol'] = '<datix respondents_title> (<datix ORGNamesTitle>)';
$txt['table_contacts_main_pol_resp'] = '<datix respondents_title> (<datix CONNamesTitle>)';
$txt['table_vw_respondents_joined_pol'] = '<datix respondents_title> (All)';
$txt['table_organisations_main_cla'] = '<datix respondents_title> (<datix ORGNamesTitle>)';
$txt['table_contacts_main_cla_resp'] = '<datix respondents_title> (<datix CONNamesTitle>)';
$txt['table_vw_respondents_joined_cla'] = '<datix respondents_title> (All)';

$txt['table_inc_injuries'] = '<datix injuries>';
$txt['table_vw_inc_injuries'] = '<datix injuries>';

$txt['table_staff'] = 'Users';
$txt['table_pals_subjects'] = '<datix pal_subjects>';
$txt['table_compl_subjects'] = '<datix complaints_subjects>';
$txt['table_compl_isd_issues'] = 'Issues';
$txt['table_aud_reasons'] = '<datix reasons_for_rejection>';
$txt['table_events_history'] = '<datix stage_history>';
$txt['table_causal_factors'] = '<datix causal_factors>';

$txt['table_link_contacts_COM'] = 'All <datix CONNames>';
$txt['table_link_contacts_COM_N'] = '<datix com_other_contact_plural>';
$txt['table_link_contacts_COM_E'] = '<datix com_employee_plural>';
$txt['table_link_contacts_COM_A'] = '<datix com_person_affected_plural>';
$txt['table_link_contacts_COM_C'] = '<datix com_complainant_plural>';
$txt['table_link_contacts_INC'] = 'All <datix CONNames>';
$txt['table_link_contacts_INC_A'] = '<datix person_affected_plural>';
$txt['table_link_contacts_INC_E'] = '<datix employee_plural>';
$txt['table_link_contacts_INC_N'] = '<datix other_contact_plural>';
$txt['table_link_contacts_RAM_N'] = '<datix ram_contact_plural>';
$txt['table_link_contacts_CLA'] = 'All <datix CONNames>';
$txt['table_link_contacts_CLA_M'] = '<datix cla_claimant_plural>';
$txt['table_link_contacts_CLA_A'] = '<datix cla_person_plural>';
$txt['table_link_contacts_CLA_E'] = '<datix cla_employee_plural>';
$txt['table_link_contacts_CLA_N'] = '<datix cla_other_contact_plural>';
$txt['table_link_contacts_PAL'] = 'All <datix CONNames>';
$txt['table_link_contacts_PAL_P'] = '<datix pal_enquirer_plural>';
$txt['table_link_contacts_PAL_N'] = '<datix pal_contact_plural>';
$txt['table_link_contacts_SAB'] = 'All <datix CONNames>';
$txt['table_link_contacts_SAB_S'] = '<datix sab_for_action_by>';
$txt['table_link_contacts_SAB_I'] = '<datix sab_information_only>';
$txt['table_link_contacts_SAB_N'] = '<datix sab_contact_plural>';

$txt['table_excel_profile'] = 'Import profiles';
$txt['table_email_templates'] = 'Email templates';
$txt['table_code_tag_groups'] = 'Tag sets';
$txt['table_documents_main'] = 'Documents';
$txt['table_action_chains'] = '<datix action_chains>';
$txt['table_action_chain_steps'] = 'Action chain steps';
$txt['web_reports'] = 'Reports';
$txt['web_packaged_reports'] = '<datix packaged_reports>';
$txt['web_queries'] = '<datix saved_queries>';


$txt['gauge_default_title'] = 'Gauge report for';

$txt['gauge_max_min_value_error'] = 'Max value must be equal or greater than min value';

$txt['report_details'] = 'Report details';
$txt['report_table'] = 'Table';
$txt['report_field'] = 'Field';
$txt['current_criteria'] = 'Current criteria';
$txt['report_criteria'] = 'Criteria saved against report';

$txt['reports_no_table_error'] = 'Please select a table.';
$txt['total'] = 'Total';

$txt['report_date_option'] = 'Date option';
$txt['day_of_week'] = 'Day of week';
$txt['week'] = 'Week number';
$txt['week_date'] = 'Week date';
$txt['month'] = 'Month';
$txt['month_and_year'] = 'Month and year';
$txt['quarter'] = 'Quarter';
$txt['year'] = 'Year';
$txt['financial_month'] = 'Financial month';
$txt['financial_quarter'] = 'Financial quarter';
$txt['financial_year'] = 'Financial year';

$txt['monday'] = 'Monday';
$txt['tuesday'] = 'Tuesday';
$txt['wednesday'] = 'Wednesday';
$txt['thursday'] = 'Thursday';
$txt['friday'] = 'Friday';
$txt['saturday'] = 'Saturday';
$txt['sunday'] = 'Sunday';

$txt['jan'] = 'Jan';
$txt['feb'] = 'Feb';
$txt['mar'] = 'Mar';
$txt['apr'] = 'Apr';
$txt['may'] = 'May';
$txt['jun'] = 'Jun';
$txt['jul'] = 'Jul';
$txt['aug'] = 'Aug';
$txt['sep'] = 'Sep';
$txt['oct'] = 'Oct';
$txt['nov'] = 'Nov';
$txt['dec'] = 'Dec';

$txt['cumulative_data'] = 'Cumulative data';
$txt['drill_down'] = 'Drill down';
$txt['reporting_no_value'] = 'No value';
$txt['ambiguous_column_message'] = 'The saved query used is not compatible with this report design.  Please either re-create the saved query (by running and saving a new search), or manually alter the existing query so that all field names are fully-qualified (i.e. they are prefixed with the appropriate table name).';
$txt['add_report_prompt_error'] = 'Error: unable to access the current report';
$txt['my_reports_save_error'] = 'Error saving to my reports';
$txt['atprompt_save_literal'] = 'Always use currently selected values';
$txt['atprompt_save_codes'] = 'Prompt for values when running report';

$txt['totals_based_on'] = 'totals based on';
$txt['average_based_on'] = 'average based on';

$txt['at_prompt_values'] = 'Filters';
$txt['at_prompt_start'] = 'Start';
$txt['at_prompt_end'] = 'End';

$txt['no_report_to_display'] = 'No report to display';
$txt['please_select_filter_options'] = 'Please select required filter options';
$txt['select_required_options'] = 'select required options';

$txt['traffic_light_no_options'] = 'No options to display';
$txt['traffic_light_field_label'] = 'Field value';
$txt['traffic_light_colour_label'] = 'Colour';

$txt['record_count'] = 'Record count';
$txt['error_retrieving_count'] = 'Error retrieving count';

//====================================================================================

// SAVED QUERIES

$txt['run_query'] = 'Run query';
$txt['edit_query'] = 'Edit';
$txt['query_save_message'] = 'Query \'%s\' saved successfully';
$txt['query_delete_message'] = 'Query deleted successfully';
$txt['query_delete_error'] = 'You don\'t have permissions to delete this saved query';


//====================================================================================

// DASHBOARD

$txt['widget_sidebar_my_reports'] = 'My reports';
$txt['widget_sidebar_other_reports'] = 'Other reports';

$txt['add_to_this_dashboard'] = 'Add to this dashboard:';

$txt['dashboard_settings'] = 'Settings';
$txt['dashboard_please_wait'] = 'Please wait...';
$txt['dashboard_menu_edit'] = 'Edit';
$txt['dashboard_menu_refresh'] = 'Refresh';
$txt['dashboard_menu_delete'] = 'Delete';
$txt['dashboard_menu_export'] = '<datix btn_export>';
$txt['dashboard_menu_moveto'] = 'Move To ...';



//====================================================================================

// GENERIC

$txt["reject_q"] = 'Are you sure you want to reject this record?';
$txt["remove_from_dashboard"] = 'Are you sure you want to remove this graph from the dashboard?';
$txt["cancel_confirm"] = 'Cancel changes?';
$txt["reset_default_form_confirm"] = 'Reset form to default design?';
$txt["delete_confirm"] = 'Delete?';
$txt["ajax_wait_msg"] = 'Please wait';
$txt["save_wait_msg"] = 'Please wait';
$txt["email_merge_wait_msg"] = 'Please wait while email data for this record is migrated. This may take an additional few seconds, but should only happen once per record';
$txt["logged_out_sso"] = 'You are currently logged out. Please choose one of the options above to continue.';
$txt["search_for_records"] = ' - Search for Records';
$txt['btn_help_close'] = 'Close';
$txt['confirm_unsaved_changes'] = 'You will lose any unsaved data. Press OK to continue or Cancel to return to record';

// IMAGE ALT TEXT:
$txt['alt_overdue'] = 'Overdue'; // used on overdue marker
$txt['alt_dropdown'] = 'Dropdown'; // used on dropdown arrow
$txt['alt_picklist_add'] = 'Add values'; // used on multipicklist 'add' image
$txt['alt_picklist_delete'] = 'Delete highlighted value(s)'; // used on multipicklist 'delete' image

//Distribution lists
$txt["unable_to_delete_DST"] = "Unable to delete distribution list";
$txt["deleted_DST"] = "Distribution list has been deleted.";
$txt["unable_to_delete_DST_contact_links"] = "Could not delete distribution list links to contacts";

$txt["add_dst"] = "Add a <datix DSTName>";
$txt["list_all_dst"] = "List all <datix DSTNames>";
$txt["list_my_dst"] = "List my <datix DSTNames>";

//Email
$txt["email_sent_confirm"] = "Emails were sent to the following users:";
$txt["email_select_a_contact"] = 'Please select at least 1 contact to email.';
$txt["email_problem_sending"] = "There was an error sending an email.";
$txt['recipient_name'] = 'Recipient Name';
$txt['recipient_email'] = 'Recipient E-mail';
$txt['date_time'] = 'Date/Time';
$txt['contact_id'] = 'Contact ID';
$txt['tel_number'] = 'Telephone Number';

// Document templates
$txt['document_template_saved'] = 'Template saved.';
$txt['document_template_saved_permission_warning'] = 'This template is currently accessible to all users.  To restrict access, identify in the Permissions section the specific users / groups who should have access to the template.';

$txt['btn_merge_in_msword'] = 'Merge in MS Word';
$txt['select_word_template_err'] = "You have not selected a template for Word merging.<br><br>Select a template before pressing the '<datix btn_merge_in_msword>' button";

//Copy options
$txt['copy_selected_records'] = "Copy selected record(s)";
$txt['copy_options'] = "Copy options";
$txt['copy_linked_contacts'] = "Copy links to contacts?";
$txt['copy_actions'] = "Copy actions?";
$txt['copy_documents'] = "Copy documents?";
$txt['copy_extra_fields'] = "Copy extra fields?";
$txt['copy_notepad'] = "Copy notepad?";
$txt['copies'] = "Copies";
$txt['copy'] = "Copy";
$txt['copy_confirm'] = "Are you sure you want to copy all of the selected records from your search?";
$txt['copy_link_to_master'] = "Link copied records to master record?";
//$txt['copy_link_to_modules'] = "Copy linked module records?";
$txt['copy_scores'] = "Copy scores?";
$txt['copy_comments'] = "Copy comments?";
$txt['copy_evidence'] = "Copy evidence?";
$txt['copy_evidence_link_notes'] = "Copy evidence link notes?";
$txt['copy_linked_assets'] = "Copy links to equipment?";
$txt['copy_medications'] = "Copy medications?";
$txt['allow_copy_option'] = "Allow user to copy records for this module?";
$txt['copy_causal_factors'] = 'Copy causal factors?';

//Merge options
$txt['merge_options'] = "Merge options";
$txt['merge_dest_id'] = "Merge into contact ID";
$txt['merge_documents'] = "Transfer documents?";
$txt['merge_linked_records'] = "Transfer links to other modules?";
$txt['delete_after_merge'] = "Delete records after transfer?";
$txt['merge_id_validation'] = "Cannot merge into a record selected as a record to merge from.";
$txt['btn_goto_merged_record'] = "Go to merged record";
$txt['no_selected_records'] = "No records selected.";
$txt['merge_id_not_exist'] = "The destination record does not exist.";
$txt['merge_id_missing'] = "Please enter a destination record ID.";

//Generate options
$txt['generate'] = "Generate from";
$txt['generate_selected_records'] = "Generate from selected records";
$txt["btn_generate"]= "Generate";
$txt['generate_confirm'] = "Are you sure you want to generate from the selected record?";
$txt['generate_options'] = "Generate options";
$txt['generate_module'] = "Module to generate record in: ";
$txt['generate_records'] = "Generate records";
$txt['btn_goto_generated_record'] = "Go to generated record";
$txt['btn_back_to_source_record'] = "Back to source record";
$txt['generate_link_to_master'] = "Link generated record to master record?";
$txt['btn_back_to_main_menu'] = "Back to main menu";
$txt['generate_truncate_message'] = "Please note that due to field length differences the data in the following fields has been truncated: %s";
$txt['generate_select_module'] = 'Please select a module before selecting approval status';
$txt['generate_select_module_no_perms'] = 'You do not have adequate write permissions for this module';
$txt['generate_default_approval'] = '';

//Reasons for Rejection
$txt['reasons_for_rejection'] = 'Reasons for <datix rejection>';
$txt['rejection'] = 'Rejection';
$txt['reasons_history_title'] = '<datix reasons_for_rejection> - History';

//Fatal errors
$txt['no_permission'] = "You do not have permission to perform this action.";
$txt['system_error'] = "A system error has occurred";
$txt['no_permission_loc'] = 'You do not have access to any locations. You are unable to complete this function.';
$txt['no_permission_asm'] = 'You do not have access to any templates. You are unable to complete this function.';

// ISD Export
$txt['isd_export_title'] = "ISD Export";
$txt["isd_export_error_title"] = "Incomplete/Missing Data";
$txt["isd_export_error_message"] = "ISD data is missing from the following complaints/fields.  Please complete the required fields and run the export again.";
$txt["isd_agent"] = "Please enter your ISD agent code";
$txt["isd_month"] = "Month (MM)";
$txt["isd_year"] = "Year (YYYY)";
$txt["isd_ref_date"] = "ISD Reference date";
$txt["isd_edition"] = "Edition number for this ISD export?";
$txt["isd_validate_year"] = "Please enter a valid year";
$txt["isd_validate_edition"] = "Please enter an Edition number";

// LDAP configiguration
$txt['ldap_configuration'] = "LDAP configuration";
$txt['ldap_username'] = "Username";
$txt['ldap_password'] = "Password";
$txt['ldap_btn_save'] = "Save";
$txt['ldap_configuration_saved'] = "LDAP configuration saved";

// Combo Linking
$txt['combo_linking'] = 'Combo linking';
$txt['combo_linking_child_error'] = 'Please enter a value in the child field';
$txt['combo_linking_parent_error'] = 'Please enter a value in the parent field';
$txt['combo_linking_same_error'] = 'Parent and child fields cannot be the same';

//Progress notes
$txt["no_progress_notes"] = "No progress notes.";

// Tasks
$txt['task-generator'] = 'Task generator';
$txt['no_tasks_created'] = 'No tasks have been set up for %s';
$txt['edit_add_task'] = '%s task';
$txt['add_new_task'] = 'Add a new task';
$txt['new_task'] = 'New';
$txt['edit_task'] = 'Edit';
$txt['tasks'] = 'Tasks';

// Action chains
$txt['action_chains'] = 'Action chains';
$txt['action_chain'] = 'action chain';
$txt['new_action_chain'] = 'Create a new action chain';
$txt['no_action_chains'] = 'There are no action chains to display';
$txt['add_action_chain'] = 'Add action chain';
$txt['edit_action_chain'] = 'Edit action chain';
$txt['action_chain_save_button'] = 'Save';
$txt['action_chain_delete_button'] = 'Delete';
$txt['action_chain_cancel_button'] = 'Cancel';
$txt['action_chain_delete_confirm'] = 'Are you sure you wish to delete?';
$txt['action_chain_delete_message'] = 'The action chain was successfully deleted';
$txt['action_chain_save_message'] = 'The action chain was successfully saved';
$txt['add_new_action_chain'] = 'Add a new action chain';
$txt['validate_step_numbers_error_msg'] = 'There are duplicate step numbers in the chain, please amend so that there are no duplicates and re-save the chain';
$txt['action_chain_all_modules'] = 'All Modules';
$txt['action_chain_step'] = 'Step';

// Filters
$txt['filter_choose_field'] = 'Choose field...';
$txt['filter_choose_value'] = 'Choose value...';
$txt['filter_add_another'] = 'Add another filter';
$txt['filter_delete'] = 'Delete';
$txt['filter_button'] = 'Filter';
$txt['filter_reset'] = 'Reset';
$txt['filter_save'] = 'Save filter criteria as a query';
$txt['filter_select_field_error'] = 'Please select a field';
$txt['filter_no_value'] = '<No value>';

// Manage CQC Links
$txt['cqc_links_add'] = 'Add link';
$txt['cqc_links_unlink'] = 'Unlink';
$txt['cqc_links_unlink_confirm'] = 'Are you sure you wish to unlink?';
$txt['cqc_links_use_main_unlink'] = 'Use the "Unlink evidence" button at the bottom of the form to unlink';
$txt['cqc_links_read_only'] = 'You have read-only access to this record so cannot unlink';
$txt['cqc_new_link_title'] = 'New evidence link';
$txt['cqc_new_link_subtitle'] = 'Link evidence record';
$txt['cqc_new_link_save'] = 'Save';
$txt['cqc_new_link_cancel'] = 'Cancel';
$txt['cqc_new_link_select_loc'] = 'Please select a value for the Locations field';

// Manage Accreditation
$txt["atm_create_assigned_templates"] = 'Create assignments for this <datix AMONames>';

// Email templates
$txt['add_email_template'] = 'Add Email Template';
$txt['edit_email_template'] = 'Edit Email Template';
$txt['email_contents'] = 'Email Contents';
$txt['email_template_saved'] = 'The template has been saved successfully';
$txt["email_template_deleted"] = 'The template has been deleted';
$txt['email_template_configured'] = 'Your changes have been saved, please log out and back in for these to take effect.';
$txt['form_data_lost_error'] = 'There was an error attempting to save the form.  Please re-enter your changes and save the form again.';

// Central admin
$txt['locked'] = "Locked";
$txt['unlocked'] = "Unlocked";
$txt['current_user_error'] = 'Error: unable to access current user';
$txt['module'] = 'Module';
$txt['personal_property'] = 'Personal Property';

$txt['confirm_save_batch_update'] = 'Press OK to leave this page WITHOUT saving, press Cancel to return';

$txt['no_records_found'] = 'No records found';
$txt['no_contacts_found'] = 'No contacts found.';

$txt['check_mapping_changed'] = 'Making this change will affect reporting.';
$txt['please_select_module'] = 'Please select a module';
$txt['show_mapped_code_setup'] = 'Show mapped code setup';
$txt['code_setup_read_only_msg'] = 'This code set up area and any entries listed here are not in use because this field is mapped to another field\'s code set.<br />Return to the field set up screen to view the mapped code set.';
$txt['mapped_code_read_only_msg'] = 'These codes come from a mapped field and cannot be altered here.';

//TO MOVE TO A NEW HOME
$txt['timeout'] = 'Timeout: ';
$txt['all'] = 'All';
$txt['overdue'] = 'Overdue';
$txt['due_today'] = 'Due today';
$txt['due_this_week'] = 'Due this week';
$txt['due_this_month'] = 'Due this month';
$txt['back'] = 'Back';
$txt['back_to'] = 'Back to';
//sabs
$txt['there_are'] = 'There are';
$txt['there_is'] = 'There is';
$txt['deadline_overdue'] = ' where the deadline for action underway is overdue.';
$txt['action_overdue'] = ' where the deadline for action completed is overdue.';
$txt['read_overdue'] = ' where the deadline to be read is overdue for some contacts.';

$txt['back_to_admin'] = 'Back to admin';
$txt['btn_back_distribution'] = 'Back to distribution list';
$txt['add_new_contact'] = 'Add new <datix CONName>';
$txt['back_to_medication'] = ' <datix MEDName>';
$txt['list_extra_fields'] = 'List extra fields';
$txt['batch_update_title'] = 'Batch update field set up';

$txt['sabs_history_record_found'] = ' history records found. Displaying ';
$txt["record"] = "record";
$txt['found_displaying'] = ' found. Displaying ' ;
$txt['query_colon'] = '<datix query>:';
$txt['next_page'] = 'Next page';
$txt['previous_page'] = 'Previous page';
$txt['report_designer'] = 'Report designer';
$txt['report_settings'] = 'Report settings';
$txt['btn_menu'] = 'Menu';
$txt['staff_responsibility_title'] = 'Staff responsibilities - incidents in the holding area';
$txt['btn_add_another'] = 'Add another';
$txt['btn_add_another_row'] = '<datix add_another> row';
$txt['details_of'] = 'Details of';
$txt['details_action_chain'] = '<datix details_of> <datix action_chain>';
$txt['details_rejection'] = '<datix details_of> <datix rejection>';
$txt['alert_details_rejection'] = 'Please complete the \'<datix details_rejection>\' section before saving this form.';
$txt['details_person_reporting'] = '<datix details_of > person reporting the risk';
$txt['additional_information'] = 'Additional Information';
$txt['risk_grade_tracker'] = 'Risk grade tracker';
$txt['risk_grade_tracker_report'] = 'Risk grade tracker report';
$txt['risk_grade_tracker_report_title'] = 'Risk Grade Tracker Report - Risk ID ';
$txt['date_update'] = 'Date of update';
$txt['report_on'] = 'Report on:';
$txt['start_date_colon'] = 'Start date:';
$txt['end_date_colon'] = 'End date:';
$txt['btn_add'] = 'Add';
$txt['btn_export'] = 'Export';
$txt['btn_ok'] = 'OK';
$txt['change_inc_workflow'] = 'You have changed the workflow for the <datix mod_incidents_title> module. Changing a workflow could mean that an approval status is removed and that any records at that approval status will no longer be displayed. Ensure that any records at affected approval statuses are moved to another status before proceeding with this change.\n\nPress OK to save your changes or press Cancel to return to the configuration area';
$txt['btn_save_new_listing'] = 'Save new listing';
$txt['copy_profile_confirm'] = 'You have chosen to copy this profile.  Press OK to save the current profile and create an identical copy.  Press Cancel to return to the current profile';
$txt['delete_profile_confirm'] = 'Delete profile?\nDeleting this profile will affect settings and access restrictions for any users attached to it';
$txt['btn_apply'] = 'Apply';
$txt['link_distribution_list_contacts'] = 'Link distribution list contacts';
$txt['export_to_csv_recommend'] = 'Due to its size this report could take a long time exporting to PDF or Excel. It is recommended that you export to CSV';

$txt['create_a_new_action'] = 'Create a new action';
$txt['no_notification_emails_sent'] = 'No notification e-mails sent';
$txt["job_title"] = "Job title";
$txt["not_found"] = ' not found';
$txt["attach_a_new_document"] = "Attach a new document";
$txt["no_documents"] = "No documents.";
$txt["done_date"] = "Done date";
$txt["overdue"] = "Overdue";
$txt["add_another_injury"] = "Add another injury";
$txt["add_another_item"] = "Add Another Item";
$txt["doc_link_as"] = "Link as";
$txt["doc_description"] = "Description";
$txt["doc_attach_this_file"] = "Attach this file";
$txt["doc_created"] = 'Created';
$txt["doc_type"] = 'Type';
$txt["doc_id"] = 'ID';
$txt["alert_message"] = "The following fields need to be filled in before you can submit the form:";
$txt["alert_title"] = "Alert";
$txt['all_todo'] = 'All';
$txt['overdue_todo'] = 'Overdue';
$txt['due_today_todo'] = 'Due today';
$txt['due_this_week_todo'] = 'Due this week';
$txt['due_this_month_todo'] = 'Due this month';
$txt['no_permission_to_access_module'] = 'You do not have permissions to access this module';

//come back to clean up
$txt['check_matching_contacts'] = 'Check for matching contacts';
$txt['access_level_required'] = 'Access level required if criteria for this security group entered.';
