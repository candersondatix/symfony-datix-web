<xsl:stylesheet version = "1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="errors">
        <xsl:for-each select="error">
            <xsl:sort select="position()" data-type="number" order="descending"/>
            <xsl:call-template name="error" />
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="error">

        <xsl:variable name="style">
            <xsl:choose>
                <xsl:when test="position() mod 2 = 1">windowbg2</xsl:when>
                <xsl:otherwise>windowbg</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <tr class="{$style}">
            <td style="padding:4px;vertical-align:top">
                <xsl:value-of select="date" />
            </td>
            <td style="padding:4px;vertical-align:top">
                <xsl:value-of select="time" />
            </td>
            <td width="80%" style="padding:2px;vertical-align:top">
                <div style="padding:2px">
                    <b><xsl:value-of select="message" /></b>
                </div>
                <div style="padding:2px">
                <xsl:value-of select="details" />
                </div>
                <div style="padding:2px">
                <b><xsl:text>SQL: </xsl:text></b> <xsl:value-of select="sql" />
                </div>
                <div style="padding:2px">
                <b><xsl:text>User: </xsl:text></b><xsl:value-of select="contact" /><xsl:text> : </xsl:text><xsl:value-of select="contact_initials" /><xsl:text> (</xsl:text><xsl:value-of select="contact_id" /><xsl:text>)</xsl:text>
                </div>
                <div style="padding:2px">
                <b><xsl:text>Page: </xsl:text></b><xsl:value-of select="page" />
                </div>
            </td>
        </tr>
    </xsl:template>

</xsl:stylesheet>

