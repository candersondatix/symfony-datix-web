<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="/">
		<div>
            <xsl:variable name="multiple" select="/codes/multiple" />
            <xsl:variable name="search" select="/codes/search" />
            <xsl:variable name="freetext" select="/codes/freetext" />
            <xsl:variable name="empty" select="/codes/empty" />
            <xsl:variable name="ctrlidx" select="/codes/ctrlidx" />
            
            <xsl:choose>
                <xsl:when test="not($empty = 'empty')">
			        <div id="codelist_search">
				        <input tabindex="1001" name="searchtext" id="searchtext" size="52" maxlength="254" value="" style="width: 274px;" onkeyup="GlobalCodeSelectControls[{$ctrlidx}].highlightSelection(this.value, document.getElementById('code_list_select'));" onkeypress="SelectCodeOnEnter({$ctrlidx})" type="text" />
			        </div>
			        <div id="codelist_select">
				        <xsl:choose>
					        <xsl:when test="$multiple = 'multiple'">
						        <xsl:call-template name="multi_select"/>
					        </xsl:when>
					        <xsl:otherwise>
						        <xsl:call-template name="single_select"/>
					        </xsl:otherwise>
				        </xsl:choose>
			        </div>
                </xsl:when>
                <xsl:otherwise>
                    <div id="codelist_select"> 
                        <div>There are no codes to display.</div>
                        <div style="display:none">
                            <select name="code_list_select" id="code_list_select_{$ctrlidx}" size="5" multiple="multiple">
                            </select>
                        </div>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
		</div>
		<input type="hidden" id="code_list_field" value=""/>
	</xsl:template>

	<xsl:template match="codes/code">
		<xsl:variable name="value" select="value" />
		<xsl:variable name="selected" select="selected" />
		<xsl:choose>
			<xsl:when test="$selected = 'selected'">
				<option value="{$value}" selected="selected"><xsl:value-of select="description" /></option>
			</xsl:when>
			<xsl:otherwise>
				<option value="{$value}"><xsl:value-of select="description" /></option>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="multi_select">
        <xsl:variable name="ctrlidx" select="codes/ctrlidx" /> 
		<select tabindex="1002" name="code_list_select" id="code_list_select_{$ctrlidx}" size="5" multiple="multiple" ctrlidx="{$ctrlidx}" ondblclick="GlobalCodeSelectControls[{$ctrlidx}].makeSelection(this);">
		<xsl:apply-templates select="codes/code" />
		</select>
	</xsl:template>

    <xsl:template name="single_select">
        <xsl:variable name="ctrlidx" select="codes/ctrlidx" />
        <select tabindex="1002" name="code_list_select" id="code_list_select_{$ctrlidx}" size="5" ctrlidx="{$ctrlidx}" onclick="GlobalCodeSelectControls[{$ctrlidx}].makeSelection(this);" onkeypress="SelectCodeOnEnter({$ctrlidx})">
        <xsl:apply-templates select="codes/code" />
        </select>
    </xsl:template>

</xsl:stylesheet>
