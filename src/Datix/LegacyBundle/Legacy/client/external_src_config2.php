<?php
  //Used to form where clause passed onto external web service for querying external contacts database
  $_SESSION["CON"]["EXTERNAL_WHERE"] = "con_number = '" . EscapeQuotes($_POST["con_number"]) . "'";
  if(bYN(GetParm('CON_PAS_CHK_CON_SURNAME', 'Y')))
  {
    $_SESSION["CON"]["EXTERNAL_WHERE"] .= " AND con_surname = '" . $_POST["con_surname"] . "'";
  }
  //Specifies external primary key for external contacts table and datatype.
  $_SESSION["CON"]["EXTERNAL_KEY"] = "con_number";
  $_SESSION["CON"]["EXTERNAL_KEY_DATATYPE"] = "string";
  //Location of the external wsdl file.
  $_SESSION["CON"]["EXTERNAL_WSDL"] = "$ClientFolder/external_interface.wsdl";
?>
