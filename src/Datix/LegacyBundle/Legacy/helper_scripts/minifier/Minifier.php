<table cellpadding="10" width="100%"><?php
$path = realpath (__DIR__.'/../../');

$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));

$changed = array();

$excludeDirs = array (
    'thirdpartylibs',
    'Zend',
    'PHPExcel',
    'graph-php5',
    'tests',
    'export',
    'graph',
    'client',
    'WebHelp',
    'scriptaculous',
    'MSWord',
    'cpaint',
    'rico',
    'qTip',
    'helper_scripts'
);

$hitlist = array ();

if($_POST['run'] == 'minify')
{
    if($_POST['action'] == 'minify selected')
    {
        $filename = $_POST['selected'];

        $pathParts = pathinfo($filename);

        $cmd = 'java -jar ' . $path . '\\helper_scripts\\minifier\\yuicompressor-2.4.7.jar --line-break 200 ' . $filename . ' -o ' . $pathParts['dirname'] . '\\' . $pathParts['filename'] . '.min.' . $pathParts['extension'];

        minify_file($filename, $min_file);
    }
    else{
        // Minify CSS files
        foreach ($files as $filename => $info)
        {
            $min_file = preg_replace('/\.(css|js)$/i', '.min.$1', $filename);

            // Skip non CSS files
            if (!preg_match('/.*\.css$/', $filename) || preg_match('/\.min\./i', $filename) || filemtime($filename) <= filemtime($min_file))
            {
                continue;
            }

            // Skip excluded dirs
            foreach ($excludeDirs as $dir)
            {
                $dir = '\\' . DIRECTORY_SEPARATOR . $dir . '\\' . DIRECTORY_SEPARATOR;

                if (preg_match ("/{$dir}/", $filename))
                {
                    continue (2);
                }
            }

            minify_file($filename, $min_file);
        }

        $min_file = preg_replace('/\.(css|js)$/i', '.min.$1', $filename);

        // Minify JS files
        foreach ($files as $filename => $info)
        {
            $min_file = preg_replace('/\.(css|js)$/i', '.min.$1', $filename);

            // Skip non JS files, .min files and if file has already been minified after last change to file
            if (!preg_match('/.*\.js$/', $filename) || preg_match('/\.min\./i', $filename) || filemtime($filename) <= filemtime($min_file))
            {
                continue;
            }

            // Skip excluded dirs
            foreach ($excludeDirs as $dir)
            {
                $dir = '\\' . DIRECTORY_SEPARATOR . $dir . '\\' . DIRECTORY_SEPARATOR;

                if (preg_match ("/{$dir}/", $filename))
                {
                    continue (2);
                }
            }

            minify_file($filename, $min_file);
        }
    }

    ?><tr><td>Minifier finished</td></tr><?php
}

function minify_file($filename, $min_file)
{
    global $path;

    $pathParts = pathinfo($filename);

    $cmd = 'java -jar ' . $path . '\\helper_scripts\\minifier\\yuicompressor-2.4.7.jar --line-break 200 ' . $filename . ' -o ' . $pathParts['dirname'] . '\\' . $pathParts['filename'] . '.min.' . $pathParts['extension'];

    exec($cmd." 2>&1", $output, $error);

    if($error == 0)
    {
        ?><tr><td><?php echo $min_file; ?> successfully created</td></tr><?php
    }
    else
    {
        ?><tr><td style="color: red">ERROR CREATING <?php echo $min_file; ?></td></tr>
        <tr><td><pre><?php echo implode(PHP_EOL, $output); ?></pre></td></tr><?php
    }
}

$dir_regexp = '';

foreach ($excludeDirs as $dir)
{
    if($dir_regexp != '')
    {
        $dir_regexp .= '|';
    }
    $dir_regexp .= '\\' . DIRECTORY_SEPARATOR . $dir . '\\' . DIRECTORY_SEPARATOR;
}

?><tr><td width="50%"><h4>Changed files:</h4><?php

foreach($files as $filename => $info)
{
    if (preg_match ("/({$dir_regexp})/", $filename) == 0)
    {
        if(preg_match('/.*(?<!\.min)\.(css|js)$/i', $filename))
        {
            $min_file = preg_replace('/\.(css|js)$/i', '.min.$1', $filename);

            if((is_file($min_file) && filemtime($filename) > filemtime($min_file)) || ( ! is_file($min_file)))
            {
                echo $filename . " last changed: " . date('d/m/Y H:i', filemtime($filename)) . "<br />";
                echo $min_file . " last changed: " . date('d/m/Y H:i', filemtime($min_file)) . "<br />";
                echo '---------------------------------------------------------------------' . "<br />";
                $changed[] = $filename;
            }
        }
    }
}

if(empty($changed))
{
    ?><p>All files up to date</p><?php
}

?></td><td width="50%" style="vertical-align: top;"><?php

if( ! empty($changed))
{
    ?><h3>Minify files</h3>
    <form method="post">
        <div><input type="submit" name="action" value = "minify all files" /></div>
        <select name="selected">
            <?php
            foreach($changed as $file)
            {
                ?><option value="<?php echo $file; ?>"><?php echo preg_replace('/^' . preg_quote($_SERVER['DOCUMENT_ROOT']) . '/', '', $file); ?></option><?php
            }
            ?>
        </select>
        <input type="hidden" name="run" value="minify" />
        <input type="submit" name="action" value = "minify selected" />
    </form><?php
}
?></td></tr></table>