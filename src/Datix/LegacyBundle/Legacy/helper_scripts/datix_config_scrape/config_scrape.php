<?php

use src\users\model\User;
use src\framework\registry\Registry;
use src\framework\controller\Loader;
use Symfony\Component\DomCrawler\Crawler;

chdir('../../');
session_start();

ob_start();
require_once 'tests/config.php';
require_once 'Source/libs/FormClasses.php';
ob_end_clean();

foreach ($txt as $code => $text)
{
    while (preg_match("/<datix\s+(\w+)>/", $txt[$code], $tags))
    {
        $txt[$code] = preg_replace("/<datix\s+$tags[1]>/",$txt[$tags[1]],$txt[$code]);
    }
    
    while (preg_match("/<global\s+(\w+)>/", $txt[$code], $tags))
    {
        $txt[$code] = preg_replace("/<global\s+$tags[1]>/",GetParm($tags[1]),$txt[$code]);
    }
}

$_SESSION['CurrentUser'] = new User();
$registry = Registry::getInstance();
$moduleDefs = $registry->getModuleDefs();

$loader = new Loader();
$controller = $loader->getController(['controller' => 'src\\admin\\controllers\\ShowDatixwebConfTemplateController']);

ob_start(); // unfortunately, template controllers still echo the response themselves, rather than returning a response string...
$controller->doAction('setup');
$crawler = new Crawler(ob_get_clean());

// get panel descriptions
foreach (array_keys($moduleDefs) as $module)
{
    $_SESSION['licensedModules'][$moduleDefs[$module]['MOD_ID']] = true;
}
$_SESSION['Globals']['MULTI_MEDICATIONS'] = 'Y';
$panels = getModArray([], true);
$panels['ALL'] = 'All Modules';

$contents = "DATIX WEB CONFIGURATION\n\n";
$contents .= "Module,Section,Field,Description,Global\n";

foreach ($crawler->filter('div[class="panel"]') as $panel)
{
    $panelCode = substr($panel->getAttribute('id'), 6, 3);
    $panelName = trim($panels[$panelCode]);
    
    $panel = new Crawler($panel->ownerDocument->saveHTML($panel));
    
    foreach ($panel->filter('li') as $row)
    {
        if ('section_title_row' == $row->getAttribute('class'))
        {
            $sectionName = trim((new Crawler($row->ownerDocument->saveHTML($row)))->text());
        }
        
        if ('field_div' == $row->getAttribute('class'))
        {
            $fieldLabel = (new Crawler($row->ownerDocument->saveHTML($row)))->filter('div[class="field_label_div"]');
            $fieldName = trim($fieldLabel->filter('b')->text());
            $subText = trim(str_ireplace($fieldLabel->filter('b')->text(), '', $fieldLabel->text()));
            
            try
            {
                $global = '';
                $global = (new Crawler($row->ownerDocument->saveHTML($row)))->filter('input')->attr('id');
                $global = '_title' == substr($global, -6) ? rtrim($global, '_title') : $global;
                $global = $global == '' ? (new Crawler($row->ownerDocument->saveHTML($row)))->filter('input')->attr('name') : $global;
            } 
            catch (Exception $e)
            {
                try
                {
                    $global = (new Crawler($row->ownerDocument->saveHTML($row)))->filter('textarea')->attr('id');
                } 
                catch (Exception $e)
                {
                    // dunno what the global is then...
                }
            }
        }
        
        if ('' != $fieldName && $global === strtoupper($global))
        {
            $contents .= '"'.str_replace('"', '""', $panelName).'","'.str_replace('"', '""', $sectionName).'","'.str_replace('"', '""', $fieldName).'","'.str_replace('"', '""', $subText).'","'.$global."\"\n";
        }
        
        $fieldName = '';
    }
}

$loader = new Loader();
$controller = $loader->getController(['controller' => 'src\\users\\controllers\\UserTemplateController']);

ob_start();
$controller->doAction('edituser');
$crawler = new Crawler(ob_get_clean());

$contents .= "\n\nUSER/PROFILE CONFIGURATION\n\n";
$contents .= "Module,Field,Description,Global\n";

foreach ($crawler->filter('li[id="panel-parameters"]')->filter('li[class="section_div"]') as $section)
{
    if ('parameters' == $section->getAttribute('id'))
    {
        continue;
    }
    
    $section = (new Crawler($section->ownerDocument->saveHTML($section)));
    
    $module = $section->filter('div[class="section_title"]')->text();
    $module = str_ireplace(' settings', '', $module);
    
    foreach ($section->filter('li[class="field_div"]') as $field)
    {
        $field = (new Crawler($field->ownerDocument->saveHTML($field)));
        $label = $field->filter('label');
        
        try
        {
            $extraText = $label->filter('div[class="field_extra_text"]')->text();
        }
        catch (Exception $e)
        {
            $extraText = '';
        }
        
        try
        {
            $fieldName = str_replace(_tk('mandatory_err'), '', $field->filter('label')->text());
            $fieldName = str_replace($extraText, '', $fieldName);
            $fieldName = trim($fieldName);
        }
        catch (Exception $e)
        {
            $fieldName = '';
        }
        
        try
        {
            $global = $label->attr('for');
            $global = str_replace('_title', '', $global);
        }
        catch (Exception $e)
        {
            $global = '';
        }
        
        if ('' != $fieldName && '' != $global)
        {
            $contents .= '"'.str_replace('"', '""', $module).'","'.str_replace('"', '""', $fieldName).'","'.str_replace('"', '""', $extraText).'","'.$global."\"\n";
        }
    }
}

$csv = new Files_CSV('DatixWeb Configuration');
$csv->setContents($contents);
$csv->export();