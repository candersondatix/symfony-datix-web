<?php
spl_autoload_register('__autoload');

class InsertService {

    public function __construct()
    {
        global $ClientFolder;
        require_once("../Source/classes/EscapeClass.php");
        require_once("../Source/libs/subs.php");
        require_once(getClientPath($ClientFolder)."/DATIXConfig.php");
        require_once("../Source/libs/DatixDBQueryClass.php");
        require_once("../Source/classes/SanitizeClass.php");
        require_once("WSTokens.php");
        require_once("WSAuthentication.php");

    }
  // Test function to simulate reteival of contact from an external source.
  function getContact($login, $password, $token, $whereclause)
  {
        WSAuthentication::doAuthentication($login, $password, $token);

        $XMLResponse = '<?xml version="1.0" encoding="UTF-8"?>';
        $XMLResponse .= '<DATIXDATA>';
        $XMLResponse .= '<TABLE name="contacts_main" module="CON">';

        $sql = "SELECT recordid, rep_approved, con_title, con_forenames, con_surname,
            con_type, con_gender, con_ethnicity,
            con_subtype, con_address, con_organisation,
            con_postcode, con_number, con_nhsno, con_tel1, con_tel2, con_fax,
            CONVERT ( varchar(23) , con_dob, 126 ) as con_dob,
            CONVERT ( varchar(23) , con_dod, 126 ) as con_dod,
            con_email, con_orgcode, con_clingroup, con_directorate, con_specialty,
            con_unit, con_loctype, con_locactual,
            con_empl_grade, con_payroll, con_jobtitle, con_language,
            CONVERT ( varchar(23) , con_dopened, 126 ) as con_dopened,
            CONVERT ( varchar(23) , con_dclosed, 126 ) as con_dclosed,
            con_notes,
            age = DATEDIFF(year, con_dob, GETDATE()), age_at_death = DATEDIFF(year, con_dob, con_dod)
            FROM contacts_main
            WHERE " . $whereclause .
            " ORDER BY con_name ASC";

        $result = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($result as $row)
        {
            $fieldnames = array_keys($row);
            $XMLResponse .= '<RECORD>';
            foreach ($fieldnames as $fieldname)
            {
                if ($row[$fieldname] != "")
                {
                    $row[$fieldname] = htmlspecialchars($row[$fieldname], ENT_QUOTES);
                    $row[$fieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $row[$fieldname]);
                    $XMLResponse .= '<COLUMN name="' . \UnicodeString::strtolower($fieldname) . '" value="' . $row[$fieldname] . '"/>';
                }
            }

            $XMLResponse .= '</RECORD>';
        }
        $XMLResponse .= '</TABLE>';
        $XMLResponse .= '</DATIXDATA>';

     /*   $handle=fopen("$ClientFolder/contact2.xml", 'w+');

        if (fwrite($handle, $XMLResponse) === FALSE)
        {
           echo "Cannot write to file contact2.xml";
           exit;
        }
        fclose($handle);    */

        return $XMLResponse;

  }

    // Function to allow insertion of records to main tables with optional linked data
    function insertRecord($login, $password, $token, $XMLData)
    {
        global $InterfaceErrorMsg, $createdRecordids;
        
        WSAuthentication::doAuthentication($login, $password, $token);
        
        if (substr(getcwd(), -4) != 'soap')
        {
            $prefix = 'soap/';
        }
        
        require_once $prefix.'interface.php';
        $XSDFile = $prefix.'insertrecord.xsd';
        
        return array(
            'Result'   => DoImportXMLdata($XMLData, $XSDFile),
            'ErrorMsg' => $InterfaceErrorMsg,
            'Ids'      => $createdRecordids
        );
    }

  //Generic function to allow external quereying of database tables via DATIXWeb webservice.
  function getRecordSet($login, $password, $token, $columns, $tablename, $whereclause, $orderby)
  {
    WSAuthentication::doAuthentication($login, $password, $token);

    $XMLResponse = '<?xml version="1.0" encoding="UTF-8"?>';
    $XMLResponse .= '<DATIXDATA>';
    $XMLResponse .= '<TABLE name="' . $tablename .'">';

    $sql = "SELECT " . $columns . "
            FROM " . $tablename . "
            WHERE " . $whereclause .
            " ORDER BY " . $orderby;

    $result = DatixDBQuery::PDO_fetch_all($sql);

    foreach ($result as $row)
    {
        $fieldnames = array_keys($row);
        $XMLResponse .= '<RECORD>';
        foreach ($fieldnames as $fieldname)
        {
            if ($row[$fieldname] != "")
            {
                $row[$fieldname] = htmlspecialchars($row[$fieldname], ENT_QUOTES);
                $row[$fieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $row[$fieldname]);
                $XMLResponse .= '<COLUMN name="' . \UnicodeString::strtolower($fieldname) . '" value="' . $row[$fieldname] . '"/>';
            }
        }
        $XMLResponse .= '</RECORD>';
    }
    $XMLResponse .= '</TABLE>';
    $XMLResponse .= '</DATIXDATA>';

    return $XMLResponse;

  }

  function getIncident($login, $password, $token, $whereclause)
  {
      global $ICISAuthOverride;

        //hack for Greater Manchester, who need to use web services without login
        //for their ICIS connection
        if(!$ICISAuthOverride)
        {
            WSAuthentication::doAuthentication($login, $password, $token);
        }

        $XMLResponse = '<?xml version="1.0" encoding="UTF-8"?>';

        $XMLResponse .= '<!DOCTYPE DATIXDATA[';
        $XMLResponse .= GetEntityCodes();
        $XMLResponse .= ']>';

        $XMLResponse .= '<DATIXDATA>';
        $XMLResponse .= '<TABLE name="incidents_main" module="INC" recordid_req="1">';

        $sql = "SELECT recordid, rep_approved, inc_mgr, inc_head,
        inc_type, inc_category, inc_subcategory,
        inc_dincident, inc_time, inc_locactual, inc_unit,
        inc_notes, inc_actiontaken, inc_result,
        inc_eqpt_type, inc_manufacturer, inc_serialno,
        inc_defect, inc_description, inc_supplier, inc_servrecords, inc_model,
        inc_location, inc_qdef, inc_dmanu, inc_lastservice, inc_dputinuse,
        inc_batchno, inc_cemarking, inc_outcomecode, inc_dmda, inc_outcome,
        inc_repname, inc_rep_tel, inc_rep_email, inc_reportedby,
        inc_impact, inc_inv_dstart, inc_consequence, inc_likelihood,
        inc_recommend, inc_inv_dcomp, inc_inv_outcome, inc_is_riddor,
        inc_root_causes, inc_reportedby, inc_repname, inc_dreported,
        inc_directorate, inc_specialty, inc_severity,
        inc_loctype, inc_name, inc_ourref, inc_consequence,
        inc_inv_action, inc_inv_lessons,
        inc_lessons_code, inc_action_code,
        inc_likelihood, inc_grade, inc_clin_detail, inc_clintype,
        inc_cnstitype, inc_carestage, inc_clingroup,
        inc_cost, inc_inquiry, inc_organisation, inc_equipment,
        inc_dopened, inc_dsched,
        inc_med_stage, inc_med_error, inc_med_drug, inc_med_drug_rt,
        inc_med_form, inc_med_form_rt, inc_med_dose, inc_med_dose_rt,
        inc_med_route, inc_med_route_rt,
        inc_dnotified, inc_ridloc, inc_riddor_ref, inc_address, inc_localauth, inc_pars_address, inc_acctype, inc_riddorno, inc_rc_required,
        inc_report_npsa, inc_n_effect, inc_n_patharm, inc_n_nearmiss, inc_n_actmin,
        inc_n_paedward, inc_n_paedspec, inc_dnpsa,
        inc_agg_issues, inc_user_action, inc_pol_crime_no, inc_pol_called,
        inc_pol_call_time, inc_pol_attend, inc_pol_att_time, inc_pol_action,
        inc_pars_pri_type, inc_pars_sec_type, inc_pars_clinical,
        inc_postcode, inc_investigator,
        inc_n_clindir, inc_n_clinspec, inc_notify, inc_injury, inc_bodypart,
        inc_pasno1, inc_pasno2, inc_pasno3, inc_problems
        FROM incidents_main
        WHERE " . $whereclause .
        " ORDER BY recordid ASC";

    $inc_rows = DatixDBQuery::PDO_fetch_all($sql);

    foreach ($inc_rows as $row)
    {
        $fieldnames = array_keys($row);
        $XMLResponse .= '<RECORD>';
        foreach ($fieldnames as $fieldname)
        {
                if ($fieldname == "inc_ourref")
                {
                    $XMLResponse .= $this->__ProcessColumn($fieldname, $row[$fieldname], 'INC', 'incidents_main', true);
                }
                else
                {
                    $XMLResponse .= $this->__ProcessColumn($fieldname, $row[$fieldname], 'INC', 'incidents_main');
                }
        }

        //Notes
        $sql = "SELECT notes
            FROM notepad
            WHERE inc_id = " . $row['recordid'];

        $notesrow = DatixDBQuery::PDO_fetch($sql);
        if (!empty($notesrow))
        {
            $notesfieldnames = array_keys($notesrow);
            foreach ($notesfieldnames as $notesfieldname)
            {
                if ($notesrow[$notesfieldname] != "")
                {
                    $notesrow[$notesfieldname] = htmlspecialchars($notesrow[$notesfieldname], ENT_QUOTES);
                    $notesrow[$notesfieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $notesrow[$notesfieldname]);
                    $ColResponse .= '<COLUMN name="' . \UnicodeString::strtolower($notesfieldname) . '" value="' . $notesrow[$notesfieldname] . '"/>';
                }
            }
            if($ColResponse)
            {
                $XMLResponse .= '<LINKTABLE name="notepad" module="INC" parent_link_field="inc_id">';
                $XMLResponse .= '<RECORD>';
                $XMLResponse .= $ColResponse;
                $XMLResponse .= '</RECORD>';
                $XMLResponse .= '</LINKTABLE>';
            }
        }

        //Extra fields
        $sql = "SELECT mod_id, group_id, field_id, udv_string, udv_number, udv_date, udv_money, udv_text
            FROM udf_values
            WHERE mod_id = " . ModuleCodeToID('INC') . ' AND cas_id = ' . $row['recordid'];

        $udfresultArray = DatixDBQuery::PDO_fetch_all($sql);
        if(count($udfresultArray) > 0)
        {
            $XMLResponse .= '<LINKTABLE name="udf_values" module="INC" parent_link_field="cas_id">';
            foreach ($udfresultArray as $udfrow)
            {
                    $udffieldnames = array_keys($udfrow);
                    $XMLResponse .= '<RECORD>';
                    foreach ($udffieldnames as $udffieldname)
                    {
                        if ($udfrow[$udffieldname] != "")
                        {
                            $udfrow[$udffieldname] = htmlspecialchars($udfrow[$udffieldname], ENT_QUOTES);
                            $udfrow[$udffieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $udfrow[$udffieldname]);
                            $KeyCol = in_array(\UnicodeString::strtolower($udffieldname), array('mod_id', 'group_id', 'field_id', 'mod_id'));
                            $XMLResponse .= '<COLUMN name="' . \UnicodeString::strtolower($udffieldname) . '" value="' . $udfrow[$udffieldname] . '" '. ($KeyCol ? 'iskeycolumn="1"' : '' ) .'/>';
                        }
                    }
                    $XMLResponse .= '</RECORD>';
            }
            $XMLResponse .= '</LINKTABLE>';
        }

        $sql = "SELECT recordid, contacts_main.rep_approved, con_title, con_forenames, con_surname,
            con_type, con_gender, con_ethnicity,
            con_subtype, con_address, con_organisation,
            con_postcode, con_number, con_nhsno, con_tel1, con_tel2, con_fax,
            con_dob, con_dod,
            con_email, con_orgcode, con_clingroup, con_directorate, con_specialty,
            con_unit, con_loctype, con_locactual,
            con_empl_grade, con_payroll, con_jobtitle, con_language,
            con_dopened, con_dclosed, con_notes
            FROM contacts_main, link_contacts
            WHERE inc_id = " . $row['recordid'] . " AND link_contacts.con_id = contacts_main.recordid" .
            " ORDER BY contacts_main.recordid ASC";

         $conrows = DatixDBQuery::PDO_fetch_all($sql);
         $lastconrow = 0;
         foreach ($conrows as $conrow)
        {
            //Make sure that a contact linked more than once is only output once per incident.
            if ($conrow["recordid"] === $lastconrow)
            {
                continue;
            }
            $lastconrow = $conrow["recordid"];
            $confieldnames = array_keys($conrow);
            $XMLResponse .= '<TABLE name="contacts_main" module="CON" recordid_req="1">';
            $XMLResponse .= '<RECORD>';
            foreach ($confieldnames as $confieldname)
            {
                $KeyColumn = in_array($confieldname, array('con_surname', 'con_number'));
                $XMLResponse .= $this->__ProcessColumn($confieldname, $conrow[$confieldname], 'CON', 'contacts_main');
            }

            //LINK_CONTACT
            $sql = "SELECT link_role, link_type, link_status, link_notes,
            link_treatment, link_injury1, link_bodypart1,
            link_ref, link_dear, link_npsa_role, link_deceased, link_age,
            link_occupation, link_riddor, link_is_riddor, link_daysaway,
            link_mhact_section, link_mhcpa,
            link_abs_start, link_abs_end
            FROM link_contacts
            WHERE inc_id = " . $row['recordid'] . " AND link_contacts.con_id = " . $conrow["recordid"] . "
            ORDER BY link_contacts.con_id ASC";

            $conlinkresult = DatixDBQuery::PDO_fetch_all($sql);
            foreach ($conlinkresult as $conlinkrow)
            {
                $conlinkfieldnames = array_keys($conlinkrow);
                $XMLResponse .= '<LINKTABLE name="link_contacts" module="INC" parent_link_field="inc_id" child_link_field="con_id">';
                $XMLResponse .= '<RECORD>';
                foreach ($conlinkfieldnames as $conlinkfieldname)
                {
                    $XMLResponse .= $this->__ProcessColumn($conlinkfieldname, $conlinkrow[$conlinkfieldname], 'INC','');
                }
                $XMLResponse .= '</RECORD>';
                $XMLResponse .= '</LINKTABLE>';
            }

            //INC_INJURIES
            $sql = "SELECT recordid, inc_injury, inc_bodypart, listorder
            FROM inc_injuries
            WHERE inc_id = " . $row['recordid'] . " AND con_id = " . $conrow["recordid"] . "
            ORDER BY inc_injuries.listorder ASC";

            $injurylinkresult = DatixDBQuery::PDO_fetch_all($sql);
            if (!empty($injurylinkresult))
            {
                $XMLResponse .= '<LINKTABLE name="inc_injuries" module="INC" parent_link_field="inc_id" child_link_field="con_id" recordid_req="1">';

                foreach ($injurylinkresult as $injurylinkrow)
                {
                    $injurylinkfieldnames = array_keys($injurylinkrow);
                    $XMLResponse .= '<RECORD>';
                    foreach ($injurylinkfieldnames as $injurylinkfieldname)
                    {
                        if ($injurylinkfieldname == "inc_injury" || $injurylinkfieldname == "inc_bodypart")
                        {
                            $XMLResponse .= $this->__ProcessColumn($injurylinkfieldname, $injurylinkrow[$injurylinkfieldname], 'INC', 'INCINJ', true);
                        }
                        else
                        {
                            $XMLResponse .= $this->__ProcessColumn($injurylinkfieldname, $injurylinkrow[$injurylinkfieldname], 'INC', 'INCINJ');
                        }
                    }
                    $XMLResponse .= '</RECORD>';
                }

                $XMLResponse .= '</LINKTABLE>';
            }

            $XMLResponse .= '</RECORD>';
            $XMLResponse .= '</TABLE>';
        }
        $XMLResponse .= '</RECORD>';
    }
    $XMLResponse .= '</TABLE>';
    $XMLResponse .= '</DATIXDATA>';

  /*      $handle=fopen("contact2.xml", 'w+');

        if (fwrite($handle, $XMLResponse) === FALSE)
        {
           echo "Cannot write to file contact2.xml";
           exit;
        }
        fclose($handle);           */

        return $XMLResponse;

  }

  function getRisk($login, $password, $token, $whereclause)
  {
        WSAuthentication::doAuthentication($login, $password, $token);

        $XMLResponse = '<?xml version="1.0" encoding="UTF-8"?>';
        $XMLResponse .= '<!DOCTYPE DATIXDATA[';
        $XMLResponse .= GetEntityCodes();
        $XMLResponse .= ']>';
        $XMLResponse .= '<DATIXDATA>';
        $XMLResponse .= '<TABLE name="ra_main" module="RAM" recordid_req="1">';

        $sql = "SELECT recordid, rep_approved, ram_name,ram_risk_type,ram_risk_subtype,
                    ram_organisation,ram_unit,ram_clingroup,ram_directorate,
                    ram_specialty,ram_assurance,ram_location,ram_locactual,
                    ram_objectives,ram_responsible,ram_description,
                    ram_synopsis,ram_consequence,ram_likelihood,
                    ram_rating,ram_cur_rating,ram_after_rating,ram_level,ram_after_conseq,ram_after_likeli,
                    ram_after_level,ram_cur_conseq,ram_cur_likeli,ram_cur_level,
                    ram_cur_cost,ram_dreview,ram_ourref,ram_dcreated,ram_dclosed,
                    ram_handler,rep_approved,ram_adeq_controls
                FROM ra_main
                WHERE " . $whereclause .
                " ORDER BY recordid ASC";

        $resultmain = DatixDBQuery::PDO_fetch_all($sql);

        foreach($resultmain as $row)
        {
            $fieldnames = array_keys($row);
            $XMLResponse .= '<RECORD>';
            foreach ($fieldnames as $fieldname)
            {
                    if ($fieldname == "ram_ourref")
                    {
                        $XMLResponse .= $this->__ProcessColumn($fieldname, $row[$fieldname], 'RAM', 'ra_main', true);
                    }
                    else
                    {
                        $XMLResponse .= $this->__ProcessColumn($fieldname, $row[$fieldname], 'RAM', 'ra_main');
                    }
            }

            //Notes
            $sql = "SELECT notes
                    FROM notepad
                    WHERE ram_id = " . $row['recordid'];

            $notesresult = DatixDBQuery::PDO_fetch_all($sql);
            foreach ($notesresult as $notesrow)
            {
                $notesfieldnames = array_keys($notesrow);
                foreach ($notesfieldnames as $notesfieldname)
                {
                    if ($notesrow[$notesfieldname] != "")
                    {
                        $notesrow[$notesfieldname] = htmlspecialchars($notesrow[$notesfieldname], ENT_QUOTES);
                        $notesrow[$notesfieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $notesrow[$notesfieldname]);
                        $ColResponse .= '<COLUMN name="' . \UnicodeString::strtolower($notesfieldname) . '" value="' . $notesrow[$notesfieldname] . '"/>';
                    }
                }
                if($ColResponse)
                {
                    $XMLResponse .= '<LINKTABLE name="notepad" module="RAM" parent_link_field="ram_id">';
                    $XMLResponse .= '<RECORD>';
                    $XMLResponse .= $ColResponse;
                    $XMLResponse .= '</RECORD>';
                    $XMLResponse .= '</LINKTABLE>';
                }
            }

            //Extra fields
            $sql = "SELECT mod_id, group_id, field_id, udv_string, udv_number, udv_date, udv_money, udv_text
                FROM udf_values
                WHERE mod_id = " . ModuleCodeToID('RAM') . ' AND cas_id = ' . $row['recordid'];

            $udfresultArray = DatixDBQuery::PDO_fetch_all($sql);

            if (count($udfresultArray) > 0)
            {
                $XMLResponse .= '<LINKTABLE name="udf_values" module="INC" parent_link_field="cas_id">';
                foreach ($udfresultArray as $udfrow)
                {
                        $udffieldnames = array_keys($udfrow);
                        $XMLResponse .= '<RECORD>';
                        foreach ($udffieldnames as $udffieldname)
                        {
                            if ($udfrow[$udffieldname] != "")
                            {
                                $udfrow[$udffieldname] = htmlspecialchars($udfrow[$udffieldname], ENT_QUOTES);
                                $udfrow[$udffieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $udfrow[$udffieldname]);
                                $KeyCol = in_array(\UnicodeString::strtolower($udffieldname), array('mod_id', 'group_id', 'field_id', 'mod_id'));
                                $XMLResponse .= '<COLUMN name="' . \UnicodeString::strtolower($udffieldname) . '" value="' . $udfrow[$udffieldname] . '" '. ($KeyCol ? 'iskeycolumn="1"' : '' ) .'/>';
                            }
                        }
                        $XMLResponse .= '</RECORD>';
                }
                $XMLResponse .= '</LINKTABLE>';
            }

            $sql = "SELECT recordid, contacts_main.rep_approved, con_title, con_forenames, con_surname,
                con_type, con_gender, con_ethnicity,
                con_subtype, con_address, con_organisation,
                con_postcode, con_number, con_nhsno, con_tel1, con_tel2, con_fax,
                con_dob, con_dod,
                con_email, con_orgcode, con_clingroup, con_directorate, con_specialty,
                con_unit, con_loctype, con_locactual,
                con_empl_grade, con_payroll, con_jobtitle, con_language,
                con_dopened, con_dclosed, con_notes
                FROM contacts_main, link_contacts
                WHERE ram_id = " . $row['recordid'] . " AND link_contacts.con_id = contacts_main.recordid" .
                " ORDER BY contacts_main.recordid ASC";

             $conresult = DatixDBQuery::PDO_fetch_all($sql);
             $lastconrow = 0;
             foreach ($conresult as $conrow)
            {
                //Make sure that a contact linked more than once is only output once per incident.
                if ($conrow["recordid"] === $lastconrow)
                {
                    continue;
                }
                $lastconrow = $conrow["recordid"];
                $confieldnames = array_keys($conrow);
                $XMLResponse .= '<TABLE name="contacts_main" module="CON" recordid_req="1">';
                $XMLResponse .= '<RECORD>';
                foreach ($confieldnames as $confieldname)
                {
                    $KeyColumn = in_array($confieldname, array('con_surname', 'con_number'));
                    $XMLResponse .= $this->__ProcessColumn($confieldname, $conrow[$confieldname], 'CON', 'contacts_main');
                }

                //LINK_CONTACT
                $sql = "SELECT link_role, link_type, link_status, link_notes,
                link_ref, link_dear, link_npsa_role, link_deceased, link_age,
                link_occupation, link_riddor, link_is_riddor, link_daysaway,
                link_mhact_section, link_mhcpa,
                link_abs_start, link_abs_end
                FROM link_contacts
                WHERE ram_id = " . $row['recordid'] . " AND link_contacts.con_id = " . $conrow["recordid"] . "
                ORDER BY link_contacts.con_id ASC";

                $conlinkresult = DatixDBQuery::PDO_fetch_all($sql);
                foreach ($conlinkresult as $conlinkrow)
                {
                    $conlinkfieldnames = array_keys($conlinkrow);
                    $XMLResponse .= '<LINKTABLE name="link_contacts" module="RAM" parent_link_field="ram_id" child_link_field="con_id">';
                    $XMLResponse .= '<RECORD>';
                    foreach ($conlinkfieldnames as $conlinkfieldname)
                    {
                        $XMLResponse .= $this->__ProcessColumn($conlinkfieldname, $conlinkrow[$conlinkfieldname], 'RAM','');
                    }
                    $XMLResponse .= '</RECORD>';
                    $XMLResponse .= '</LINKTABLE>';
                }

                $XMLResponse .= '</RECORD>';
                $XMLResponse .= '</TABLE>';
            }
            $XMLResponse .= '</RECORD>';
        }
        $XMLResponse .= '</TABLE>';
        $XMLResponse .= '</DATIXDATA>';

        return $XMLResponse;

  }

  private function __ProcessColumn($fieldname, $FieldValue, $module, $fmt_table, $iskeycolumn = false)
  {
    $ff_def = getFieldFormat($fieldname, $module, $fmt_table);

    if ($ff_def['fmt_data_type'] == 'D' && $FieldValue != "")
    {
        list($month, $day, $year, $time) = explode(" ", $FieldValue);
        $Timestamp = strtotime("$day $month $year $time");
        $FieldValue = date(GetParm("EXPORT_DATE_FMT", "Ymd", true), $Timestamp);
    }

    // Global EXPORT_CODE_DESCR set to 'Y' exports code descriptions instead of actual code values.
    if ($ff_def['fmt_data_type'] == 'C' && bYN(GetParm("EXPORT_CODE_DESCR", "N", true)))
    {
        $FieldValue = GetCodeDescriptions($module, $fieldname, $FieldValue, $fmt_table);
    }

    $FieldValue = htmlspecialchars($FieldValue, ENT_QUOTES);
    $FieldValue = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $FieldValue);

    $XMLResponse = '<COLUMN name="' . \UnicodeString::strtolower($fieldname) . '" value="' . $FieldValue . '"'. ($iskeycolumn === true ? ' iskeycolumn="1"' : '' ) .'/>';

    return $XMLResponse;
  }

  /**
  * Web service for Hotspots DotNet service to pass HotspotID and list of contacts to notify.
  *
  * @param  int $HotspotID Hotspot ID.
  *
  * @return string $ContactsWhereClause List of contact ID's to email.
  */
  function sendHotSpotEmails($userid, $token, $HotspotID, $ContactsWhereClause)
  {
        global $ClientFolder, $ModuleDefs, $FieldDefs, $TableDefs, $scripturl, $Locale, $LanguageFile;
        global $txt; // do not remove this, it's required by the languageFile below
        chdir('..');
        
        require_once 'soap/HotspotsService.php';
        require_once 'Source/classes/System/TranslateClass.php';
        require_once 'Source/classes/View/NullTranslateClass.php';
        require_once 'Zend/Locale.php';
        require_once 'Source/libs/constants.php';
        require_once 'Source/TableDefs.php';
        require_once 'Source/generic_modules/HSA/AppVars.php';
        require_once 'Source/generic_modules/HSA/FieldDefs.php';
        require_once 'Source/generic_modules/HOT/AppVars.php';
        require_once 'Source/generic_modules/HOT/FieldDefs.php';
        require_once 'Source/libs/EmailTemplateClass.php';
        require_once 'Source/libs/Email.php';
        require_once 'Source/classes/Labels/LabelClass.php';
        require_once 'Source/classes/Labels/FieldLabelClass.php';
        require_once 'Source/classes/EscapeClass.php';

        if (!WSTokens::ValidateTokenWithUserID($userid, $token))
        {
            throw new SOAPFault("Authentication failed - reason: invalid user id/token", 401);
        }

        if (isset($LanguageFile))
        {
            require_once($LanguageFile);
        }
        else
        {
            require_once("english.php");
        }
        
        $EmailsSuccess = ProcessHotSpotEmails($HotspotID, $ContactsWhereClause);

        $PermissionsSet = GiveHotSpotRecordPerms($HotspotID, $ContactsWhereClause);

        if ($EmailsSuccess && $PermissionsSet)
        {
            return true;
        }
        else
        {
            return false;
        }
  }

  function getReport($login, $password, $token, $whereclause)
  {
        WSAuthentication::doAuthentication($login, $password, $token);

        $XMLResponse = '<?xml version="1.0" encoding="UTF-8"?>';
        $XMLResponse .= '<!DOCTYPE DATIXDATA[';
        $XMLResponse .= GetEntityCodes();
        $XMLResponse .= ']>';
        $XMLResponse .= '<DATIXDATA>';
        $XMLResponse .= '<TABLE name="reports_extra" module="ADM" recordid_req="1">';

        $sql = "SELECT recordid, REP_NAME, REP_PATH, REP_TABLE, REP_JOIN, REP_FIELDS, REP_FORMAT, REP_GROUPBY, REP_WHERE, REP_ORDERBY, REP_OPTION,
                        REP_MODULE, REP_TYPE, REP_DCREATED, REP_CREATEDBY, REP_DUPDATED, REP_UPDATEDBY, REP_LOCKEDBY, REP_FILE,
                        rep_private, rep_source, REP_APP_SHOW, rep_subtype
                FROM reports_extra
                WHERE " . $whereclause .
                " ORDER BY recordid ASC";

        $resultmain = DatixDBQuery::PDO_fetch_all($sql);

        foreach ($resultmain as $row)
        {
            $fieldnames = array_keys($row);
            $XMLResponse .= '<RECORD>';
            foreach ($fieldnames as $fieldname)
            {
                if ($fieldname != "recordid")
                {
                    $XMLResponse .= $this->__ProcessColumn($fieldname, $row[$fieldname], 'ADM', 'reports_extra');
                }
            }

            //Reports_formats
            $sql = "SELECT RPF_FIELD, RPF_TITLE, RPF_ORDER, RPF_WIDTH, RPF_DATA_TYPE, RPF_FORMAT, RPF_DESCR, RPF_TOTAL,
                        RPF_LOGIN, RPF_MODULE, RPF_FORM, RPF_SECTION, RPF_LINENUM, RPF_CONDITIONS
                FROM reports_formats
                WHERE RPF_REP_ID = " . $row['recordid'];

            $RPFresultArray = DatixDBQuery::PDO_fetch_all($sql);
            if(count($RPFresultArray) > 0)
            {
                $XMLResponse .= '<LINKTABLE name="reports_formats" module="ADM" parent_link_field="rpf_rep_id">';
                foreach ($RPFresultArray as $RPFrow)
                {
                        $RPFfieldnames = array_keys($RPFrow);
                        $XMLResponse .= '<RECORD>';
                        foreach ($RPFfieldnames as $RPFfieldname)
                        {
                            if ($RPFrow[$RPFfieldname] != "")
                            {
                                $RPFrow[$RPFfieldname] = htmlspecialchars($RPFrow[$RPFfieldname], ENT_QUOTES);
                                $RPFrow[$RPFfieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $RPFrow[$RPFfieldname]);
                                $KeyCol = in_array(\UnicodeString::strtolower($RPFfieldname), array('rpf_field', 'rpf_title', 'rpf_module', 'rpf_form', 'rpf_section'));
                                $XMLResponse .= '<COLUMN name="' . \UnicodeString::strtolower($RPFfieldname) . '" value="' . $RPFrow[$RPFfieldname] . '" '. ($KeyCol ? 'iskeycolumn="1"' : '' ) .'/>';
                            }
                        }
                        $XMLResponse .= '</RECORD>';
                }
                $XMLResponse .= '</LINKTABLE>';
            }

            //Reports_packaged
            $sql = "SELECT rep_pack_name, rep_pack_tables, rep_pack_where, rep_pack_dupdated, rep_pack_updatedby, rep_pack_dcreated,
                         rep_pack_createdby, rep_pack_override_sec
                    FROM reports_packaged
                    WHERE rep_id = " . $row['recordid'];

            $RPFresultArray = DatixDBQuery::PDO_fetch_all($sql);
            if(count($RPFresultArray) > 0)
            {
                $XMLResponse .= '<LINKTABLE name="reports_packaged" module="ADM" recordid_req="1" parent_link_field="rep_id">';
                foreach ($RPFresultArray as $RPFrow)
                {
                        $RPFfieldnames = array_keys($RPFrow);
                        $XMLResponse .= '<RECORD>';
                        foreach ($RPFfieldnames as $RPFfieldname)
                        {
                            if ($RPFrow[$RPFfieldname] != "")
                            {
                                $RPFrow[$RPFfieldname] = htmlspecialchars($RPFrow[$RPFfieldname], ENT_QUOTES);
                                $RPFrow[$RPFfieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $RPFrow[$RPFfieldname]);
                                $KeyCol = in_array(\UnicodeString::strtolower($RPFfieldname), array('rep_pack_name'));
                                $XMLResponse .= '<COLUMN name="' . \UnicodeString::strtolower($RPFfieldname) . '" value="' . $RPFrow[$RPFfieldname] . '" '. ($KeyCol ? 'iskeycolumn="1"' : '' ) .'/>';
                            }
                        }
                        $XMLResponse .= '</RECORD>';
                }
                $XMLResponse .= '</LINKTABLE>';
            }

            //Reports_parameters
            $sql = "SELECT rpar_parameter, rpar_value
                    FROM report_parameters
                    WHERE rpar_report_id = " . $row['recordid'];

            $RPARresultArray = DatixDBQuery::PDO_fetch_all($sql);
            if(count($RPARresultArray) > 0)
            {
                $XMLResponse .= '<LINKTABLE name="report_parameters" module="ADM" parent_link_field="rpar_report_id">';
                foreach ($RPARresultArray as $RPARrow)
                {
                        $RPARfieldnames = array_keys($RPARrow);
                        $XMLResponse .= '<RECORD>';
                        foreach ($RPARfieldnames as $RPARfieldname)
                        {
                            if ($RPARrow[$RPARfieldname] != "")
                            {
                                $RPARrow[$RPARfieldname] = htmlspecialchars($RPARrow[$RPARfieldname], ENT_QUOTES);
                                $RPARrow[$RPARfieldname] = preg_replace('/(&|&amp;)#0*39;/u', '&apos;', $RPARrow[$RPARfieldname]);
                                $KeyCol = in_array(\UnicodeString::strtolower($RPFfieldname), array('rpar_parameter'));
                                $XMLResponse .= '<COLUMN name="' . \UnicodeString::strtolower($RPARfieldname) . '" value="' . $RPARrow[$RPARfieldname] . '" '. ($KeyCol ? 'iskeycolumn="1"' : '' ) .'/>';
                            }
                        }
                        $XMLResponse .= '</RECORD>';
                }
                $XMLResponse .= '</LINKTABLE>';
            }
            $XMLResponse .= '</RECORD>';
        }
        $XMLResponse .= '</TABLE>';
        $XMLResponse .= '</DATIXDATA>';

        return $XMLResponse;

  }

  function sendNotificationEmails($userid, $token, $Module, $RecID, $TemplateID, $ContactsToEmailList)
  {
        global $ClientFolder, $ModuleDefs, $FieldDefs, $TableDefs, $scripturl, $Locale;
        chdir('..');

        require_once 'Source/classes/System/TranslateClass.php';
        require_once 'Source/classes/View/NullTranslateClass.php';
        require_once 'Zend/Locale.php';
        require_once 'soap/EmailService.php';
        require_once 'Source/libs/constants.php';
        require_once 'Source/libs/subs.php';
        require_once 'Source/TableDefs.php';
        require_once 'Source/libs/AppVars.php';
        require_once 'Source/FieldDefs.php';
        require_once 'Source/libs/EmailTemplateClass.php';
        require_once 'Source/libs/Email.php';
        require_once 'Source/classes/Labels/LabelClass.php';
        require_once 'Source/classes/Labels/FieldLabelClass.php';
        
        getLanguageArray();

        $realModule = \DatixDBQuery::PDO_fetch('SELECT ACT_MODULE FROM CA_ACTIONS WHERE RECORDID = :recordid', array('recordid' => $RecID));

        if (!WSTokens::ValidateTokenWithUserID($userid, $token))
        {
            throw new SOAPFault("Authentication failed - reason invalid user id/token", 401);
        }

        $availableModules = getDatixWebLicensedModulesByModuleCode();

        if($availableModules[$realModule['ACT_MODULE']])
        {
            return ProcessNotificationEmails($Module, $RecID, $TemplateID, $ContactsToEmailList);
        }
        else
        {
            return false;
        }
  }

  /**
  * Can be used to return a session ID as a token
  *
  * @param string $login Login
  * @param string $userpwd Password
  * @return string Token
  */
  function getToken($login, $userpwd)
  {
      chdir('..');
      include_once("Source/security/SecurityBase.php");

      $sql = "SELECT recordid, initials, fullname, lockout, permission, email, tel1,
            login_tries, sta_clingroup, sta_orgcode, sta_directorate,
            sta_specialty, sta_unit, sta_forenames, sta_surname, sta_pwd_change,
            sta_last_login, sta_lockout_dt, sta_lockout_reason
            FROM staff
            WHERE login = :login AND (login is not null and login != '')
            AND (sta_user_type != 'MAIN' OR sta_user_type IS NULL)";

      $StaffRow = DatixDBQuery::PDO_fetch($sql, array("login" => $login));

      // User does not exist in the staff table
      if (!$StaffRow)
      {
        $return_message = 'Login failed for web service authentication: incorrect user name or password';
        //AuditLogin($User, $return_message);
        return $return_message;
      }

     $_SESSION["Globals"]["PWD_SHARED"] = "Y";
     if (!ValidatePassword($login, $userpwd))
     {
        $return_message = 'Login failed for web service authentication: incorrect user name or password';
        //AuditLogin($User, $return_message);
        return $return_message;
     }

      DatixDBQuery::CallStoredProcedure(array(
            "procedure" => "ClearExpiredRecords",
            "parameters" => array()
        ));

      DatixDBQuery::CallStoredProcedure(array(
            "procedure" => "NewSession",
            "parameters" => array(
                array("@application", "WEB", SQLVARCHAR),
                array("@userid", $StaffRow["recordid"], SQLINT4),
                array("@session_id", &$_SESSION["session_id"], SQLINT4, true),
                array("@hostname", getenv("REMOTE_ADDR"), SQLVARCHAR)
                )
            ));
      return $_SESSION["session_id"];
  }

}

require_once('../client/DATIXConfig.php');
require_once('../Source/classes/UnicodeStringClass.php');

if ($Locale == '')
{
    $Locale = 'enGB';
}

$server = new SoapServer(getClientPath($ClientFolder).'/datixinterface.wsdl');
$server->setClass("InsertService");
$server->handle();

function getClientPath($path)
{
    return $path == 'client' ? '../client' : $path;
}

function __autoload($class_name)
{
    if (substr(getcwd(), -4) == 'soap')
    {
        $prefix = '../';
    }
    
    if (substr($class_name, -9) == 'Exception' && file_exists($prefix.'Source/classes/Exceptions/'. $class_name . '.php'))
    {
        $fileToIncl = $prefix.'Source/classes/Exceptions/'. $class_name . '.php';
    }
    elseif (substr($class_name, 0, 5) == 'Zend_')
    {
        $fileToIncl = $prefix . str_replace('_', '/', $class_name) . '.php';
    }
    elseif (substr($class_name, 0, 8) == 'Symfony\\' || substr($class_name, 0, 8) == 'Monolog\\' || substr($class_name, 0, 4) == 'Psr\\')
    {
        $fileToIncl = $prefix . 'thirdpartylibs/' . str_replace('\\', '/', $class_name)  . '.php';
    }
    elseif (strpos($class_name, '\\') !== false)
    {
        $fileToIncl = $prefix . str_replace('\\', '/', $class_name)  . '.php';
    }
    else
    {
        $fileToIncl = $prefix.'Source/classes/'. str_replace('_', '/', $class_name) . 'Class.php';
    }

    if (file_exists($fileToIncl))
    {
        require_once $fileToIncl;
    }
}
