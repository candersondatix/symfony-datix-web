<?php

function DoImportXMLdata($RawXMLData, $ValidationSchema)
{
    global $dbcon, $dbtype, $ClientFolder, $InterfaceErrorMsg, $createdRecordids;

    include("$ClientFolder/DATIXConfig.php");
    include_once("interfaceclass.php"); 

    $DatixInterface = new DatixInterface();
    $DatixInterface->LoadXMLDoc($RawXMLData);

    if ($DatixInterface->ValidateWithSchema($ValidationSchema))
    {
        if ($DatixInterface->importXMLdata())
        {
            $InterfaceErrorMsg = "Import successful!";
            $DatixInterface->writeErrors($InterfaceErrorMsg);
            $createdRecordids = $DatixInterface->CreatedRecordids;
            return true;
        }
    }
    return false;
}

?>
