<?php
/**
 * Class used for importing/exporting data in DATIX using XML data via files or Web services.
 *
 * Includes functions for validation of data and error logging.
 */
class DatixInterface {

    var $XMLDoc; //XML document to be loaded
    var $OutputXML;

    /**
    * Recordids of records created when running insert statements based on XML
    *
    * @var array
    */
    public $CreatedRecordids = array();

/**
 * Loads an XML document from a string.
 *
 * Takes a string containing an XML structure and loads this into a DOMDocument object
 * @param string $XMLdata String containing XML structure.
 */
    function LoadXMLDoc($XMLdata)
    {
        global $XMLDoc;

        $XMLDoc = new DOMDocument();
        if (!empty($XMLdata) && \UnicodeString::stripos($XMLdata, "<DATIXDATA>") !== false)
        {
            $XMLDoc->loadXML($XMLdata);
        }
    }

/**
 * Opens a XML file for loading
 *
 * Opens the XML file specified and loads this into an XML object.
 * @param string $XMLfile The PATH to the XML file
 */
    function OpenXMLDoc($XMLfile)
    {
        global $XMLDoc;
        if (file_exists($XMLfile))
        {
            $XMLDoc = simplexml_load_file($XMLfile);
        }
    }

    //
/**
 * Function used to import data into main Datix tables
 *
 * Takes the XML object and imports data according to structure/elements/attributes of the XML object.
 */
    function importXMLdata()
    {
        global $XMLDoc, $InterfaceErrorMsg;

        $xml = simplexml_import_dom($XMLDoc);

        if (!$this->importTABLEData($xml))
        {
            return false;
        }
        return true;
    }

/**
 * Function used to import data contained in an XML object into the relevant database tables
 *
 * Recursive function used to process the XML object to determine how/where to import data including dynamic linking with recordids.
 * @param object $xml The PATH to the XML file
 * @param string $parent_module Short name for main parent record module
 * @param string $parent_table Table name of main parent record
 * @param int $parent_recordid Recordid of main parent record
 * @param boolean $recordid_req Flag to determine whether the main record requires a recordid.
 * @param boolean $link_table Whether or not this is a link table record
 */

    function importTABLEData($xml, $parent_module = "", $parent_table = "" , $parent_recordid = "", $recordid_req = true, $link_table = false)
    {
        global $InterfaceErrorMsg;

        foreach($xml->TABLE as $table )
        {
           $tablename = (string) $table->attributes()->name;
           $module = (string) $table->attributes()->module;

           if($table->attributes()->recordid_req != NULL)
           {
                if ($table->attributes()->recordid_req[0] == "1")
                {
                    $recordid_req = true;
                }
                else
                {
                    $recordid_req = false;
                }
           }
           foreach($table->RECORD as $record )
           {
                if (!$this->saverecord($table, $module, $record, $RecID, $recordid_req, $link_table))
                {
                    $InterfaceErrorMsg = "Unable to save table record for " . $tablename;
                    $this->writeErrors($InterfaceErrorMsg);
                    return false;
                }

                $parent_recordid_save = $parent_recordid;
                foreach($record->LINKTABLE as $linktable )
                {
                    $linktablename = (string) $linktable->attributes()->name;
                    $linkmodule = (string) $linktable->attributes()->module;
                    $parent_link_field = (string) $linktable->attributes()->parent_link_field;
                    $parent_notkeycolumn = (string) $linktable->attributes()->parent_notkeycolumn;
                    $child_link_field = (string) $linktable->attributes()->child_link_field;
                    $overwrite = (string) $linktable->attributes()->overwrite;
                    $linktable_recordid_req = (string) $linktable->attributes()->recordid_req;

                    //parent_recordid gets overwritten below under some circumstances. this is a low-impact hack for 12.1 to prevent this from happening
                    $parent_recordid = $parent_recordid_save;

                    if ($overwrite == "1" && !empty($parent_link_field) && !empty($RecID))
                    {
                        $sql = "DELETE
                            FROM $linktablename
                            WHERE $parent_link_field = " . $RecID;

                        $result = DatixDBQuery::PDO_query($sql);
                        if ($result == false)
                        {
                            $InterfaceErrorMsg = "Error clearing existing link records:";
                            $this->writeErrors($InterfaceErrorMsg);
                            return false;
                        }
                    }

                    foreach($linktable->RECORD as $linkrecord )
                    {
                        $col_count = count($linkrecord->COLUMN);

                        $linkrecord->addChild('COLUMN');
                        $linkrecord->COLUMN[$col_count]->addAttribute('name', $parent_link_field);
                        if ($child_link_field == ""/*$parent_recordid == ""*/)
                        {
                            $parent_recordid = $RecID;
                        }
                        $linkrecord->COLUMN[$col_count]->addAttribute('value', $parent_recordid);
                        if (!isset($parent_notkeycolumn) || $parent_notkeycolumn == '')
                        {
                            $linkrecord->COLUMN[$col_count]->addAttribute('iskeycolumn', '1');
                        }
                        if ($child_link_field != "")
                        {
                            $linkrecord->addChild('COLUMN');
                            $linkrecord->COLUMN[$col_count + 1]->addAttribute('name', $child_link_field);
                            $linkrecord->COLUMN[$col_count + 1]->addAttribute('value', $RecID);
                            $linkrecord->COLUMN[$col_count + 1]->addAttribute('iskeycolumn', '1');
                        }

                        $tmpxml = '<?xml version="1.0" encoding="UTF-8"?>';
                        $tmpxml .= '<!DOCTYPE DATIXDATA[';
                        $tmpxml .= GetEntityCodes();
                        $tmpxml .= ']>';
                        $tmpxml2 = $linkrecord->asXML();

                        $tmpxml .= "<DATIXDATA><TABLE name='". $linktablename . "' module='" . $linkmodule . "' parent_notkeycolumn='" . $parent_notkeycolumn . "'>" . $tmpxml2 . "</TABLE></DATIXDATA>";

                        $linktablexml = new SimpleXMLElement($tmpxml);

                        if (!$this->importTABLEData($linktablexml, "", "", "", $linktable_recordid_req, true))
                        {
                            $InterfaceErrorMsg = "Unable to import link table";
                            $this->writeErrors($InterfaceErrorMsg);
                                return false;
                        }
                    }
                }
            }
        }
        return true;
    }

/**
 * Function used to validate XML passed by the insert message to datix against the specified schema file.
 *
 * Opens the XML .xsd schema file specified and validates the current $XMLDoc DOMDocument object against this.
 * @param string $XMLSchema The PATH to the XML .xsd schema file
 */
    function ValidateWithSchema($XMLSchema)
    {
        global $XMLDoc, $InterfaceErrorMsg;

        if (!file_exists($XMLSchema))
        {
            $InterfaceErrorMsg = "Schema File " . $XMLSchema . " is missing";
            $this->writeErrors($InterfaceErrorMsg);
            return false;
        }

        libxml_use_internal_errors(true);

        if (!$XMLDoc->schemaValidate($XMLSchema))
        {
            $errors = libxml_get_errors();
            foreach ($errors as $InterfaceErrorMsg)
            {
                $this->writeErrors($XMLSchema . ":" . $InterfaceErrorMsg->code . " " . \UnicodeString::trim($InterfaceErrorMsg->message) .
                    "  Line: $InterfaceErrorMsg->line Column: $InterfaceErrorMsg->column");
            }

            return false;
        }
        else
        {
            $InterfaceErrorMsg = "XML data file validated successfully!";
            $this->writeErrors($InterfaceErrorMsg);
            return true;
        }

    }

 /**
 * Function to update/insert records into Datix based on defined structure
 *
 * Opens the XML .xsd schema file specified and validates the current $XMLDoc DOMDocument object against this.
 * @param SimpleXMLElement $table The XML object which represents this table record.
 * @param string $Module Module short name
 * @param object $record SimpleXML element
 * @param int $recordid Receive variable for recordid returned after a n insert/creation of a record
 * @param boolean $require_recordid Flag to determine whether the main record requires a recordid.
 */
    function saverecord($table, $Module, $record, &$recordid, $require_recordid = true, $link_table = false)
    {
        global $dbcon, $dbtype, $ClientFolder, $InterfaceErrorMsg;

        $tablename = (string) $table->attributes()->name;
        
        if ($_GET["action"] == "import" )
        {
            include_once("Source/libs/Subs.php");
        }
        else
        {
            include("$ClientFolder/DATIXConfig.php");

            if(file_exists("../Source/libs/Subs.php"))
            {
                include_once("../Source/libs/Subs.php");
            }
            else
            {
                include_once("Source/libs/Subs.php");
            }
        }

        $recordid = "";
        $excludecols = array();
        $this->TranformRecord($Module, $record, $excludecols);

        $usekeycolumns = true; //will not use keycolumns if one is blank to prevent accidental overwriting.
       foreach($record->COLUMN as $col)
       {
            $column = (string) $col->attributes()->name;
            $value = (string) $col->attributes()->value;
            $isKeyColumn = (string) $col->attributes()->iskeycolumn;

            if (!in_array ($column, $excludecols))
            {
                //Work around for &pound; being read in as Unicode unless we can get loadxml to read in the XML file with correct encoding
                //which it currenlty seems to ignore.
                $value = str_replace('£','�',$value);

                if (!$this->validateFieldData($column, $Module, $tablename, $value) && $isKeyColumn != "1")
                {
                    return false;
                }
                if ($isKeyColumn == "1")
                {
                    if (\UnicodeString::ltrim($value) == "")
                    {
                        //if we use keys here, records in the target DB will be overwritten when using nonmandatory keys (e.g. inc_ourref).
                        $usekeycolumns = false;
                        //$keycolumn_list[] = $column . "= NULL";  //We may need to put this back in if my change above interferes with XML import

                    }
                    else
                    {
                        //Build check for existing records based on key columns apart from recordid
                        $keycolumn_list[] = $column . "='" . EscapeQuotes($value) ."'";
                    }
                }
/*                if ($require_recordid == true)
                {
                    if (ltrim($value) == "")
                    {
                        $update_list[] = $column . "= NULL";
                    }
                    else
                    {
                        //Build update list (Need both lists due to not knowing whether to update or insert yet. Check done later.)
                        $update_list[] = $column . "='" . EscapeQuotes($value) ."'";
                        $insert_list[] = $column;
                        $insert_list_values[] = "'" .EscapeQuotes($value) . "'";
                    }
                }
                else
                {
                    //Build insert list without recordid
                    $update_list[] = $column;

                    if (ltrim($value) == "")
                    {
                        $update_list_values[] = "NULL";
                    }
                    else
                    {
                        $update_list_values[] = "'" .EscapeQuotes($value) . "'";
                    }
                }         */
                if (\UnicodeString::ltrim($value) == "")
                {
                    $update_list[] = $column . "= NULL";
                }
                else
                {
                    //Build update list (Need both lists due to not knowing whether to update or insert yet. Check done later.)
                    $update_list[] = $column . "='" . EscapeQuotes($value) ."'";
                    $insert_list[] = $column;
                    $insert_list_values[] = "'" .EscapeQuotes($value) . "'";
                }

                if (\UnicodeString::ltrim($value) == "")
                {
                    $update_list_with_keycolumns[] = $column . "=NULL";
                }
                else
                {
                    $update_list_with_keycolumns[] = $column . "='" . EscapeQuotes($value) ."'";
                }
            }
        }
        // Do check for existing records based on key columns specified
        $recordexists = false;
        if (!empty($keycolumn_list) && $usekeycolumns)
        {
            $keycols_where_clause = implode(" AND ", $keycolumn_list);

            $sql = "SELECT count(*) existscheck
            FROM $tablename
            WHERE " . $keycols_where_clause;

            $recordcheck = DatixDBQuery::PDO_fetch($sql);

            if ($recordcheck["existscheck"] > 0)
            {
                $recordexists = true;
                $update_list = $update_list_with_keycolumns;
            }

            if ($require_recordid == true)
            {
                $sql = "SELECT recordid
                FROM $tablename
                WHERE " . $keycols_where_clause;

                $row = DatixDBQuery::PDO_fetch($sql);
                $recordid = $row["recordid"];
            }

        }
        //$recordid = "";
        if (!empty($update_list))
        {
            if ($require_recordid == true  || ($recordexists == true && $keycols_where_clause != ""))
            {
                if ($recordid == "" && $recordexists == false)
                {
                    $indexField = $this->getTableIndex($tablename);
                    $recordid = GetNextRecordID($tablename, false, $indexField);

                    //Not all tables use "recordid" as the unique id, so we need to find the column name for this table
                     if (file_exists("../Source/classes/TableClass.php"))
                    {
                        require_once '../Source/classes/TableClass.php';
                    }
                    else
                    {
                        require_once 'Source/classes/TableClass.php';
                    }
                    $IdField = (Table::getTableIndex($tablename) ?: 'recordid');

                    //$sql = "UPDATE $tablename SET $update_fields WHERE recordid = $recordid";
                    $insert_fields = implode(",", $insert_list);
                    $insert_field_values = implode(",", $insert_list_values);
                    $sql = "INSERT INTO $tablename ($IdField, $insert_fields) VALUES ($recordid, $insert_field_values)";
                    
                    if (!$link_table)
                    {
                        // store main table recordids to be reported back in the soap response
                        $this->CreatedRecordids[] = $recordid;    
                    }
                }
                else
                {
                    $update_fields = implode(",", $update_list);
                    $sql = "UPDATE $tablename SET $update_fields WHERE " . $keycols_where_clause;
                }

                if (!DatixDBQuery::PDO_query($sql))
                {
                    $InterfaceErrorMsg = "Unable to update record.:" . $sql;
                    $this->writeErrors($InterfaceErrorMsg);
                    return false;
                }
            }
            else
            {
                $insert_list_values = parseReportDataForDate($tablename, $insert_list, $insert_list_values);

                //$update_values = implode(",", $update_list_values);
                //$sql = "INSERT INTO $tablename ($update_fields) VALUES ($update_values)";
                $insert_fields = implode(",", $insert_list);
                $insert_field_values = implode(",", $insert_list_values);
                $sql = "INSERT INTO $tablename ($insert_fields) VALUES ($insert_field_values)";

                try 
                {
                    $recordid = DatixDBQuery::PDO_insert($sql);
                } 
                catch (DatixDBQueryException $e)
                {
                    $InterfaceErrorMsg = "Unable to insert record.:" . $sql;
                    $this->writeErrors($InterfaceErrorMsg);
                    return false;
                }

                if (!$link_table)
                {
                    // store main table recordids to be reported back in the soap response
                    $this->CreatedRecordids[] = $recordid;    
                }
            }
        }
        foreach($record->TABLE as $childtable)
        {
            $xml = new SimpleXMLElement("<TABLE>" . $childtable->asXML(). "</TABLE>");
            if (!$this->importTABLEData($xml, $Module, $tablename, $recordid, true, true))
            {
                $InterfaceErrorMsg = "Unable to import subtable";
                $this->writeErrors($InterfaceErrorMsg);
                return false;
            }
        }
        return true;
    }

/**
 * Reads XML data into array structure.
 *
 * Converts the current XML $XMLDoc DOMDocument object to an array structure.
 * @param array string $rows receive variable for return array structure.
 */
    function parseXMLDataToArray(&$rows)
    {
        global $XMLDoc, $InterfaceErrorMsg, $FieldDefs;
        $rows = array();
        $row = array();

        $xml = simplexml_import_dom($XMLDoc);

        foreach($xml->TABLE as $table)
        {
            foreach($table->attributes() as $tableattribute => $tableattribvalue)
            {
                if ($tableattribute == "name")
                {
                    foreach($xml->TABLE->RECORD as $record)
                    {
                        foreach($record[0]->COLUMN as $col )
                        {
                            $column = (string) $col->attributes()->name;
                            $value = (string) $col->attributes()->value;

                            $row[$column] = $value;
                        }

                        $rows[] = $row;
                        unset($row);
                    }
                }
            }
        }
        return true;
    }

 /**
 *  Field data validation function
 *
 * Used for validating field data prior to import.
 * @param string $fieldname Field name.
 * @param string $module Module short name
 * @param string $tablename Table name
 * @param mixed $datatovalidate Data to validate
 */
    function validateFieldData($fieldname, $module, $tablename, $datatovalidate)
    {
        global $InterfaceErrorMsg;
        //return true;
        if ($_GET["action"] == "import" || !file_exists("../Source/libs/Subs.php"))
        {
            include_once("Source/libs/Subs.php");
        }
        else
        {
            include_once("../Source/libs/Subs.php");
        }

        $ff_def = getFieldFormat($fieldname, $module , $tablename);

        switch ($ff_def["fmt_data_type"])
        {
        case "N":
        case "M":
            if (!is_numeric($datatovalidate) && !empty($datatovalidate))
            {
                $InterfaceErrorMsg = $fieldname . " is not a valid number:". $datatovalidate;
                $this->writeErrors($InterfaceErrorMsg);
                return false;
            }
            break;
        case "S":
        case "L":
            if (!is_string($datatovalidate))
            {
                $InterfaceErrorMsg = $fieldname . " is not a valid string:" . $datatovalidate;
                $this->writeErrors($InterfaceErrorMsg);
                return false;
            }
            break;
        case "D":
            break;
        case "C":
            if (!is_string($datatovalidate))
            {
                $InterfaceErrorMsg = $fieldname . $fieldname . " is not a valid code string:" . $datatovalidate;
                $this->writeErrors($InterfaceErrorMsg);
                return false;
            }
            $datatovalidate = \UnicodeString::ltrim($datatovalidate);

            if ($datatovalidate != "")
            {
                $multicodedata = explode(" ", $datatovalidate );

                foreach ($multicodedata as $codedata)
                {
                    $code_table = $ff_def["fmt_code_table"];
                    $code_ok = false;
                    $coderow["code_exists"] = 0;
                    if ($code_table{0} == '!')
                    {
                        $code_type = \UnicodeString::substr($code_table, 1);

                        $sql = "SELECT count(*) as code_exists
                            FROM code_types
                            WHERE cod_code = '$codedata'
                            AND cod_type = '$code_type'";
                        $code_ok = true;
                    }
                    elseif($code_table != "" && $ff_def["fmt_code_field"] != "")
                    {
                        $sql = "SELECT count(*) as code_exists
                            FROM $code_table
                            WHERE $ff_def[fmt_code_field] = '$codedata'";
                        $code_ok = true;
                    }

                    if($code_ok)
                    {
                        $coderow = DatixDBQuery::PDO_fetch($sql);
                        if (empty($coderow))
                        {
                            $InterfaceErrorMsg = "Error validating code in the setup:". $fieldname. "=" . $codedata;
                            $this->writeErrors($InterfaceErrorMsg);
                            return false;
                        }
                    }
                    if ($coderow["code_exists"] < 1)
                    {
                        $InterfaceErrorMsg = $fieldname . " is not a valid code in the setup:" . $codedata;
                        $this->writeErrors($InterfaceErrorMsg);
                        return false;
                    }
                }
            }
            break;
        default:
          /*  $InterfaceErrorMsg = $fieldname . " is not a valid field:" . $datatovalidate;
            $this->writeErrors($InterfaceErrorMsg);
            return false;*/
            // Can only validate against fields defined in field_formats, but some fields
            // which are not defined will be let through since some like globals parameters etc
            // cannot be put into field_formats.
            return true;
            break;
    }

        $fmt_data_length = (integer) $ff_def["fmt_data_length"];
        if ($fmt_data_length > 0)
        {
            if (\UnicodeString::strlen($datatovalidate) > (integer) $ff_def["fmt_data_length"])
            {
                $InterfaceErrorMsg = $fieldname . " is not a valid length (max " . $fmt_data_length . "):" . $datatovalidate;
                $this->writeErrors($InterfaceErrorMsg);
                return false;
            }
        }

        return true;
    }

 /**
 *  Validation of coded field data
 *
 * Used for validating a code for a field
 * @param string $module Module short name
 * @param string $field Field name
 * @param string $value Code to validate
 */
    function checkvalidcode($module, $field, $value)
    {
        include_once("$ClientFolder/DATIXConfig.php");
        include("../Source/libs/DatixDBQueryClass.php");
        include("../Source/classes/SanitizeClass.php");

        if ($value == "")
            return true;

        $sql = "SELECT fmt_code_table, fmt_code_field, fmt_code_descr
            FROM field_formats
            WHERE fmt_module = '$module'
            AND fmt_field = '$field'";

        $row = DatixDBQuery::PDO_fetch($sql);

        $code_table = $row["fmt_code_table"];

        if ($code_table{0} == '!')
        {
            $code_type = \UnicodeString::substr($code_table, 1);

            $sql = "SELECT cod_descr AS description, cod_colour as colour
                FROM code_types
                WHERE cod_code = '$value'
                AND cod_type = '$code_type'";
            $code_ok = true;
        }
        elseif($row["fmt_code_descr"] != "" && $code_table != "" && $row["fmt_code_field"] != "")
        {
            $sql = "SELECT $row[fmt_code_descr] AS description, cod_colour as colour
                FROM $code_table
                WHERE $row[fmt_code_field] = '$value'";
            $code_ok = true;
        }

        if($code_ok)
        {
            $row = DatixDBQuery::PDO_fetch($sql);
        }

        if ($row == "")
            $row["description"] = $value;

        return $row;
    }

 /**
 *  Function used to output error messages to file.
 *
 * Outputs specified message to the interface_error_log.txt file
 * @param string $ErrorMessage Message to record
 * @param string $error_filename File name
 */
    function writeErrors($ErrorMessage, $error_filename = "")
    {
        global $ClientFolder;

        if ($error_filename == "")
        {
            $error_filename = $ClientFolder . "/interface_error_log.txt";
        }

        if (file_put_contents( $error_filename, date("r") . " $ErrorMessage \r\n", FILE_APPEND ) === FALSE)
        {
           echo "Cannot write to file " . $error_filename;
           exit;
        }
    }

 /**
 *  Used to perform any data transformations defined in the transformation.php and trnasformations_config.php files.
 *
 * Function is passsed record to be processed and also returns an exclusion list to exclude data in the XML file which does
 * not need to be included in the validation/insert directly.
 * @param string $Module Module short name
 * @param string &$rec SimpleXML element
 * @param string &$excludelist Receive variable to store fields to exclude from update/insert.
 */
    function TranformRecord($Module, &$rec, &$excludelist)
    {
        include_once("transformations.php");
        include("transformations_config.php");
    }

    /**
     * Gets the table index column from a table name.
     *
     * @param string $tableName The name of the table.
     *
     * @return array
     */
    private function getTableIndex($tableName)
    {
        $sql = "SELECT tdr_index FROM table_directory WHERE tdr_name = :tdr_name";

        return DatixDBQuery::PDO_fetch($sql, ['tdr_name' => $tableName], PDO::FETCH_COLUMN);
    }
}

function parseReportDataForDate($table, $nameArray, $valueArray)
{
    if ($table == 'web_packaged_reports' || $table == 'web_reports')
    {
        if (in_array('createddate', $nameArray))
        {
            $valueArray[array_search('createddate', $nameArray)] = "'".date("Y-m-d H:i:s").".000'";
        }
        if (in_array('updateddate', $nameArray))
        {
            $valueArray[array_search('updateddate', $nameArray)] = "'".date("Y-m-d H:i:s").".000'";
        }
        if ($_SESSION['CurrentUser'] instanceof \src\users\model\User)
        {
            if (in_array('createdby', $nameArray))
            {
                $valueArray[array_search('createdby', $nameArray)] = "'".$_SESSION['CurrentUser']->initials."'";
            }
            if (in_array('updatedby', $nameArray))
            {
                $valueArray[array_search('updatedby', $nameArray)] = "'".$_SESSION['CurrentUser']->initials."'";
            }
        }
    }

    return $valueArray;
}
?>
