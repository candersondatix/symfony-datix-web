<?php
 class WSAuthentication { 
    
    /**
    * This function authenticates the login and password provided and creates a session.
    * 
    * @param string $login User login
    * @param string $userpwd User password
    * @return int Session ID created for user.
    */
    public static function getLoginAuthentication($login, $userpwd)
    {
          chdir('..');
          include_once("Source/libs/DatixDBQueryClass.php"); 
          include_once("Source/libs/subs.php");
          include_once("Source/classes/SanitizeClass.php"); 
          include_once("Source/security/SecurityBase.php");
          
          $sql = "SELECT recordid, initials, fullname, lockout, permission, email, tel1,
                login_tries, sta_clingroup, sta_orgcode, sta_directorate,
                sta_specialty, sta_unit, sta_forenames, sta_surname, sta_pwd_change,
                sta_last_login, sta_lockout_dt, sta_lockout_reason
                FROM staff
                WHERE login = :login AND (login is not null and login != '')
                AND (sta_user_type != 'MAIN' OR sta_user_type IS NULL)";

            $StaffRow = DatixDBQuery::PDO_fetch($sql, array("login" => $login));

            // User does not exist in the staff table
            if (!$StaffRow)
            {
               return false;
            }      
             
            $_SESSION["Globals"]["PWD_SHARED"] = "Y"; 
            if (!ValidatePassword($login, $userpwd))
            {
                return false;
            }            
            
          DatixDBQuery::CallStoredProcedure(array(
                "procedure" => "ClearExpiredRecords",
                "parameters" => array()
            ));       
          
          DatixDBQuery::CallStoredProcedure(array(
                "procedure" => "NewSession",
                "parameters" => array(
                    array("@application", "WEB", SQLVARCHAR),
                    array("@userid", $StaffRow["recordid"], SQLINT4),
                    array("@session_id", &$_SESSION["session_id"], SQLINT4, true),
                    array("@hostname", getenv("REMOTE_ADDR"), SQLVARCHAR)
                    )
                ));
          return $_SESSION["session_id"];
    }
      
    /**
    * Wrapper function to perform authentication with eith login/password or login/session ID token.
    * Returns SOAPFault if fails.
    * 
    * @param string $login User login
    * @param string $password User password
    * @param int $token Session token ID
    */
    public static function doAuthentication($login, $password, $token)
    {
        include_once("WSTokens.php");
        
        if (!empty($token))
        {
            if (!WSTokens::ValidateTokenWithLogin($login, $token))
            {
                throw new SOAPFault("Authentication failed - reason: invalid login/token", 401);
            } 
        }         
        else 
        {
            if (!empty($login) && !empty($password))     
            {
                if (self::getLoginAuthentication($login, $password) == false)
                {
                    throw new SOAPFault("Authentication failed - reason: invalid username/password", 401);  
                }
            }
            else
            {
                throw new SOAPFault("Authentication failed - reason: missing username/password", 401);
            }
        }
    }
 } 
?>
