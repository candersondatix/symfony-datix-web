<?php
//This function can be used to convert multi line address data into the required single feild in Datix.
function ConvertMultiAddress(&$rec, &$excludelist)
{
        foreach($rec->COLUMN as $col)
       {
            $column = (string) $col->attributes()->name;
            $value = (string) $col->attributes()->value;

            $i = 1;
            while($i <= 6)
            {
                if ($column == "con_address_" . $i)
                {
                    $Con_address_lines[] = $value;
                    $excludelist[] = "con_address_" . $i;
                }
                $i++;
            }
       }
       if (is_array($Con_address_lines))
       {
            $Con_address = implode("\r\n", $Con_address_lines);
            if(!empty($Con_address))
            {
                $col_count = count($rec->COLUMN);
                $rec->addChild('COLUMN');
                $rec->COLUMN[$col_count]->addAttribute('name', 'con_address');
                $rec->COLUMN[$col_count]->addAttribute('value', $Con_address);
            }
       }
       return true;
}

?>
