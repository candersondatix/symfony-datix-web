<?php

function ImportData()
{
    global $scripturl, $yySetLocation, $ModuleDefs, $InterfaceErrorMsg;

    if ($_POST['rbWhat'] == 'cancel')
    {
        $yySetLocation = "$scripturl?module=ADM";
        redirectexit();
    }

    include_once 'interface.php';

    if (!empty($_FILES['userfile']['tmp_name']))
    {
        $RawXMLData = file_get_contents($_FILES['userfile']['tmp_name']);
    }
    else
    {
        $ImportResult === false;
        AddSessionMessage('ERROR', 'No import file selected');
        $yySetLocation = $scripturl . '?action=import_form';
        redirectexit();
    }

    $XSDFile = 'soap/insertrecord.xsd';
    $ImportResult =  DoImportXMLdata($RawXMLData, $XSDFile);

    if ($ImportResult === true)
    {
       AddSessionMessage('INFO', 'Import successful.');
    }
    else
    {
        $ErrorMessage = 'Could not import XML data. '; 
        
        if (is_string($InterfaceErrorMsg))
        {
           $ErrorMessage .= $InterfaceErrorMsg;
        }

        if (is_string($InterfaceErrorMsg->message))
        {
           $ErrorMessage .= $InterfaceErrorMsg->message;
        }
        
        AddSessionMessage('ERROR', $ErrorMessage);
    }

    $yySetLocation = $scripturl . '?action=import_form';
    redirectexit();
}

?>