<?php

use src\email\EmailSenderFactory;

/**
* Processes notifications with specified list of contacts to notify.    
* 
* @param string $Module Module
* @param int $RecID Module recordid
* @param int $TemplateID Template recordid
* @param string $ContactsList List of contact IDs to be emailed
*/
function ProcessNotificationEmails($Module, $RecID, $TemplateID, $ContactsList)
{
    //global $dbcon, $dbtype; 
    global $PDO, $Database, $ServerName, $UserName, $Password, $ModuleDefs, $FieldDefs, $TableDefs, $scripturl;

    \src\framework\registry\Registry::getInstance()->getLogger()->logEmail('ProcessNotificationEmails: Called with parameters '.$Module.', '.$RecID.', '.$TemplateID.', '.$ContactsList);

    if ($_SERVER["HTTPS"] == "on")
        $HTTP_Prefix = "https";
    else
        $HTTP_Prefix = "http";
    $scripturl = "$HTTP_Prefix://$_SERVER[SERVER_NAME]$_SERVER[SCRIPT_NAME]"; 
    $scripturl = \UnicodeString::str_ireplace("soap/datixserver", "index",$scripturl);     
        
    $sql = "SELECT recordid, " . implode(",", $ModuleDefs[$Module]["FIELD_ARRAY"]) . " FROM " . $ModuleDefs[$Module]["TABLE"] . " WHERE recordid = $RecID";
    $data = DatixDBQuery::PDO_fetch($sql);
     
    $sql = "SELECT recordid AS con_id, initials, con_email, con_jobtitle, fullname, contacts_main.login as login FROM contacts_main WHERE con_email IS NOT NULL AND con_email != '' AND recordid in (" . $ContactsList . ")";    
    $contactsToEmail = DatixDBQuery::PDO_fetch_all($sql);

    $contactFactory = new \src\contacts\model\ContactModelFactory();

    if ($data["recordid"] > 0)
    {
        $IndividualEmails = GetParm("INDIVIDUAL_EMAILS", "Y", true);
        \src\framework\registry\Registry::getInstance()->getLogger()->logEmail('ProcessNotificationEmails: Preparing to send emails to '.count($contactsToEmail).' addresses. Individual emails are set to '.($IndividualEmails ? '"on"' : '"off"'));

        $emailSender = EmailSenderFactory::createEmailSender($Module, '', $TemplateID);

        foreach ($contactsToEmail as $con)
        {
            $recipient = $contactFactory->getMapper()->find($con['con_id']);
            $emailSender->addRecipient($recipient);
        }

        $emailSender->sendEmails($data);
    }
     
    return true;
}   
?>
