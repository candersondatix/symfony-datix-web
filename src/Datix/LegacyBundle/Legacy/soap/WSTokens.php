<?php
class WSTokens {
    
    /**
    * Checks if session token id is valid
    * 
    * @param int $token
    * 
    * returns bool
    */
    public static function ValidateToken($token)
    {
        include_once("../Source/libs/subs.php");
        include_once("../Source/classes/SanitizeClass.php");
        include_once("../Source/libs/DatixDBQueryClass.php");        
        
        if (!is_numeric($token))
        {
            return false;    
        }
        
        $sql = 'SELECT count(*) as total FROM SESSIONS WHERE RECORDID = :recordid';
        $result = DatixDBQuery::PDO_fetch($sql, array(':recordid' => $token)); 
        
        if ($result['total'] == 0)
        {
            return false;
        }   
        
        return true;      
    }
    
    /**
    * Check session id is valid against a specific login
    * 
    * @param string $login User login
    * @param string $token Session token ID
    * 
    * returns bool
    */
    public static function ValidateTokenWithLogin($login, $token)
    {
        include_once("../Source/libs/subs.php");
        include_once("../Source/classes/SanitizeClass.php");
        include_once("../Source/libs/DatixDBQueryClass.php");        
        
        if (!is_numeric($token))
        {
            return false;    
        }
        
        $sql = 'SELECT count(*) as total FROM sessions JOIN staff ON sessions.ses_userid = staff.recordid 
                WHERE staff.login = :login AND sessions.recordid = :recordid';
        $result = DatixDBQuery::PDO_fetch($sql, array('login' => $login, 'recordid' => $token)); 
        
        if ($result['total'] == 0)
        {
            return false;
        }   
        
        return true;      
    }
    
    /**
    * Check session id is valid against a specific user ID
    * 
    * @param string $login User ID
    * @param string $token Session token ID
    * 
    * returns bool
    */
    public static function ValidateTokenWithUserID($userid, $token)
    {
        include_once("../Source/libs/subs.php");
        include_once("../Source/classes/SanitizeClass.php");
        include_once("../Source/libs/DatixDBQueryClass.php");        
        
        if (!is_numeric($token))
        {
            return false;    
        }
        
        $sql = 'SELECT count(*) as total FROM sessions 
                WHERE sessions.ses_userid = :userid AND sessions.recordid = :recordid';
        $result = DatixDBQuery::PDO_fetch($sql, array('userid' => $userid, 'recordid' => $token)); 
        
        if ($result['total'] == 0)
        {
            return false;
        }   
        
        return true;      
    }
}  
?>
