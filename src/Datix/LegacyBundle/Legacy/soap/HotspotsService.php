<?php

use src\framework\registry\Registry;
use src\email\EmailSenderFactory;
use src\contacts\model\ContactModelFactory;

  /**
  * Processes notifications for hotspot with specified list of contacts to notify.
  * 
  * @param  int $HotID Hotspot ID.
  * 
  * @return string $ContactsList List of contact ID's to email.
  */
function ProcessHotSpotEmails($HotID, $ContactsList)
{
    //global $dbcon, $dbtype; 
    global $PDO, $Database, $ServerName, $UserName, $Password, $ModuleDefs, $FieldDefs, $TableDefs, $scripturl;    
    
    if ($_SERVER["HTTPS"] == "on")
        $HTTP_Prefix = "https";
    else
        $HTTP_Prefix = "http";
    $scripturl = "$HTTP_Prefix://$_SERVER[SERVER_NAME]$_SERVER[SCRIPT_NAME]"; 
    $scripturl = \UnicodeString::str_ireplace("soap/datixserver", "index",$scripturl);     
    
    $sql = "SELECT hotspots_main.recordid as recordid, hsa_id, hsa_name, hsa_description, hsa_emt_recordid, hsa_handler FROM hotspots_main, hotspots_agent WHERE hotspots_main.hsa_id = hotspots_agent.recordid 
                AND hotspots_main.recordid = " . $HotID;    
    $hot = DatixDBQuery::PDO_fetch($sql);     
    
    $sql = "SELECT recordid, initials, con_email, con_jobtitle, fullname, contacts_main.login as login FROM contacts_main WHERE con_email IS NOT NULL AND con_email != '' AND recordid in (" . $ContactsList . ")";
    $contactsToEmail = DatixDBQuery::PDO_fetch_all($sql);    

    $contactFactory = new ContactModelFactory();

    $emailSender = EmailSenderFactory::createEmailSender('HOT', 'Notify', $hot["hsa_emt_recordid"]);

    Registry::getInstance()->getLogger()->logEmail('Sending notification e-mails when saving HOT with ID '.$HotID);
    foreach ($contactsToEmail as $con)
    {
        $recipient = $contactFactory->getMapper()->find($con['recordid']);
        $emailSender->addRecipient($recipient);
    }

    $emailSender->sendEmails($hot);

    return true;
} 

/**
* Give hotspot record level permissions with specified list of contacts who have been notified.
* 
* @param  int $HotID Hotspot ID.
* 
* @return string $ContactsList List of contact ID's to allocate record level permission.
*/
function GiveHotSpotRecordPerms($HotID, $ContactsList)
{
    global $PDO, $Database, $ServerName, $UserName, $Password;    
    
    $retval = false;
    $ContactIDs = explode(",", $ContactsList);
    
    foreach($ContactIDs as $i => $ConID)
    {
        // Check whether link already exists between user and record
        $sql = "SELECT COUNT(*) as link_count
            FROM sec_record_permissions
            WHERE
            module = :module
            AND tablename = :tablename
            AND link_id = :link_id
            AND con_id = :con_id";
        
        $row = DatixDBQuery::PDO_fetch($sql, array("module"=>"HOT", "tablename"=>"hotspots_main", "link_id"=>$HotID, "con_id"=> $ConID));

        if ($row["link_count"] == 0) 
        {    
            $sql = "insert into sec_record_permissions (module, tablename, link_id, con_id, updatedby, updateddate)
                values (:module, :tablename, :link_id, :con_id, :updatedby, :updateddate)";
                
            DatixDBQuery::PDO_query($sql, array("module"=>"HOT", "tablename"=>"hotspots_main", "link_id"=>$HotID, "con_id"=> $ConID, "updatedby"=> "ADM", "updateddate"=>date('d-M-Y H:i:s')));    
        }
        $retval = true;        
    }
    
    return $retval;
}
?>
