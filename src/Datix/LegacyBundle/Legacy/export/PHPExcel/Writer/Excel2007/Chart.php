<?php
/**
 * PHPExcel_Writer_Excel2007_Chart
 *
 * @category   PHPExcel
 * @package    PHPExcel_Worksheet
 * @copyright  Copyright (c) 2009 DATIX Ltd
 */                              

/** PHPExcel root directory */
if (!defined('PHPEXCEL_ROOT')) {
    /**
     * @ignore
     */
    define('PHPEXCEL_ROOT', dirname(__FILE__) . '/../../../');
}

/** PHPExcel */
require_once PHPEXCEL_ROOT . 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
require_once PHPEXCEL_ROOT . 'PHPExcel/Writer/Excel2007.php';

/** PHPExcel_Writer_Excel2007_WriterPart */
require_once PHPEXCEL_ROOT . 'PHPExcel/Writer/Excel2007/WriterPart.php';

/** PHPExcel_Shared_File */
require_once PHPEXCEL_ROOT . 'PHPExcel/Shared/File.php';

/** PHPExcel_Shared_XMLWriter */
require_once PHPEXCEL_ROOT . 'PHPExcel/Shared/XMLWriter.php';

require_once PHPEXCEL_ROOT . 'PHPExcel/Chart/Chart.php';


class PHPExcel_Writer_Excel2007_Chart extends PHPExcel_Writer_Excel2007_WriterPart
{

    public function writeChartXML(PHPExcel_Worksheet $pWorksheet = null)
    {
        // Create XML writer
        $objWriter = null;
        if ($this->getParentWriter()->getUseDiskCaching()) {
            $objWriter = new PHPExcel_Shared_XMLWriter(PHPExcel_Shared_XMLWriter::STORAGE_DISK, $this->getParentWriter()->getDiskCachingDirectory());
        } else {
            $objWriter = new PHPExcel_Shared_XMLWriter(PHPExcel_Shared_XMLWriter::STORAGE_MEMORY);
        }
        
        $pChart = new PHPExcel_Chart();
        $iterator =  $pWorksheet->getChartCollection()->getIterator();
        while($iterator->valid()){
            $pChart = $iterator->current();
            // only one chart is supported yet
            $iterator->next();              
        }
        
        // XML header
        $objWriter->startDocument('1.0','UTF-8','yes');
        
        $objWriter->startElement('c:chartSpace');
            $objWriter->writeAttribute('xmlns:c', 'http://schemas.openxmlformats.org/drawingml/2006/chart');
            $objWriter->writeAttribute('xmlns:a', 'http://schemas.openxmlformats.org/drawingml/2006/main');
            $objWriter->writeAttribute('xmlns:r', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships');
        
            $objWriter->startElement('c:lang');
                $objWriter->writeAttribute('val', 'en-GB');
            $objWriter->endElement(); 
            
            $objWriter->startElement('c:chart');
            
                $objWriter->startElement('c:title');
                    $objWriter->startElement('c:tx');
                        $objWriter->startElement('c:rich');
                            $objWriter->writeElement('a:bodyPr');
                            $objWriter->startElement('a:p');
                                $objWriter->startElement('a:r');
                                    $objWriter->startElement('a:rPr');
                                        $objWriter->writeAttribute('lang', 'en-GB');
                                    $objWriter->endElement();
                                    $objWriter->writeElement('a:t', $pChart->getChartTitle());
                                $objWriter->endElement();
                            $objWriter->endElement();
                        $objWriter->endElement();
                    $objWriter->endElement();
                    $objWriter->startElement('c:overlay');
                        $objWriter->writeAttribute('val', '0');
                    $objWriter->endElement();
                $objWriter->endElement();
                
                $this->_writePlotArea($objWriter, $pChart);
            
                $this->_writeLegend($objWriter, $pChart, $pChart);
            
                $objWriter->startElement('c:plotVisOnly');
                    $objWriter->writeAttribute('val', '1');
                $objWriter->endElement();
                
                if($pChart->getChartType() == 'pareto')
                {
                    $objWriter->startElement('c:dispBlanksAs');
                        $objWriter->writeAttribute('val', 'gap');
                    $objWriter->endElement();
                }
                
            $objWriter->endElement(); // chart
            
            $this->_writePrintSettings($objWriter);
        
        $objWriter->endElement(); // chartSpace
        
        return $objWriter->getData();         
    }
    
    
    public function _writePlotArea(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart)
    {
        $objWriter->startElement('c:plotArea');
        
        // resizing chart to fit more items on legend
        if($pChart->getChartLegendExtended() === true)
        {
            $objWriter->startElement('c:layout');
                $objWriter->startElement('c:manualLayout');
                    $objWriter->startElement('c:layoutTarget');
                        $objWriter->writeAttribute('val', 'inner');
                    $objWriter->endElement();
                    $objWriter->startElement('c:xMode');
                        $objWriter->writeAttribute('val', 'edge');
                    $objWriter->endElement();
                    $objWriter->startElement('c:yMode');
                        $objWriter->writeAttribute('val', 'edge');
                    $objWriter->endElement();
                    $objWriter->startElement('c:x');
                        $objWriter->writeAttribute('val', '0.12885556584838659');
                    $objWriter->endElement();
                    $objWriter->startElement('c:y');
                        $objWriter->writeAttribute('val', '8.667133275007291E-2');
                    $objWriter->endElement();
                    $objWriter->startElement('c:w');
                        $objWriter->writeAttribute('val', '0.45721099660336578');
                    $objWriter->endElement();
                    $objWriter->startElement('c:h');
                        $objWriter->writeAttribute('val', '0.56232312627588221');
                    $objWriter->endElement();
                $objWriter->endElement();
            $objWriter->endElement();
        }
        
        switch($pChart->getChartType())
        {
            case 'bar':
                $this->_writeBarChart($objWriter, $pChart);
                break;
            case 'pie':
                $this->_writePieChart($objWriter, $pChart);
                break;
            case 'line':
                $this->_writeLineChart($objWriter, $pChart);
                break;
            case 'pareto':
                $this->_writeParetoChart($objWriter, $pChart);
                break;
            case 'spc':
                switch($_SESSION['excel']['spc_chart_type'])
                {
                    case 'runchart':
                        $pChart->setDimensionC(2);
                        break;
                    case 'movingrange':
                        $pChart->setDimensionC(3);
                        break;
                    case 'ichart':
                        $pChart->setDimensionC(4);
                        break;
                    case 'cchart':
                        $pChart->setDimensionC(6);
                        break;
                    default:
                        break;
                }
                
                $this->_writeSPCChart($objWriter, $pChart);
                break;
            default:
                break;
        }
    
        $objWriter->endElement(); //plotArea
    }
    
    
    public function _writeLegend(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart = null)
    {
        if($pChart->getLegend() === true)
        {
            $objWriter->startElement('c:legend');
                $objWriter->startElement('c:legendPos');
                    $objWriter->writeAttribute('val', 'tr');
                $objWriter->endElement();
                
                // resizing legend to fit more items
                if($pChart->getChartLegendExtended() === true)
                {
                    $objWriter->startElement('c:layout');
                        $objWriter->startElement('c:manualLayout');
                            $objWriter->startElement('c:xMode');
                                $objWriter->writeAttribute('val', 'edge');
                            $objWriter->endElement();
                            $objWriter->startElement('c:yMode');
                                $objWriter->writeAttribute('val', 'edge');
                            $objWriter->endElement();
                            $objWriter->startElement('c:x');
                                $objWriter->writeAttribute('val', '0.60151159873398174');
                            $objWriter->endElement();
                            $objWriter->startElement('c:y');
                                $objWriter->writeAttribute('val', '7.722222222222222E-2');
                            $objWriter->endElement();
                            $objWriter->startElement('c:w');
                                $objWriter->writeAttribute('val', '0.39113546008954764');
                            $objWriter->endElement();
                            $objWriter->startElement('c:h');
                                $objWriter->writeAttribute('val', '0.84375590551181101');
                            $objWriter->endElement();
                        $objWriter->endElement();
                    $objWriter->endElement();
                }
                
                // Setting legend font size
                $objWriter->startElement('c:txPr');
                    $objWriter->writeElement('a:bodyPr');
                    $objWriter->startElement('a:p');
                        $objWriter->startElement('a:pPr');
                            $objWriter->startElement('a:defRPr');
                                $objWriter->writeAttribute('sz', ($pChart->getChartLegendFontSize()*100));
                                $objWriter->writeAttribute('baseline', '0');
                            $objWriter->endElement();
                        $objWriter->endElement();
                    $objWriter->endElement();
                $objWriter->endElement();
                
            $objWriter->endElement();
        }
    }
    
                              
    public function _writePrintSettings(PHPExcel_Shared_XMLWriter $objWriter = null)
    {
        $objWriter->startElement('c:printSettings');
            $objWriter->writeElement('c:headerFooter'); 
            $objWriter->startElement('c:pageMargins'); 
                $objWriter->writeAttribute('b', '0.75000000000000011');
                $objWriter->writeAttribute('l', '0.70000000000000007');
                $objWriter->writeAttribute('r', '0.70000000000000007');
                $objWriter->writeAttribute('t', '0.75000000000000011');
                $objWriter->writeAttribute('header', '0.30000000000000004');
                $objWriter->writeAttribute('footer', '0.30000000000000004');
            $objWriter->endElement();
            $objWriter->writeElement('c:pageSetup'); 
        $objWriter->endElement();          
    }
    
    
    public function _writeSerData(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart = null, $aParam = array())
    {
        $colDim = $pChart->getDimensionC();
        $rowDim = $pChart->getDimensionR();
        for($i=1;$i<=$colDim;$i++)
        {
            $objWriter->startElement('c:ser');
                $objWriter->startElement('c:idx');
                    if(isset($aParam['overwriteid']))
                    {
                        $objWriter->writeAttribute('val', $aParam['overwriteid']);
                    }
                    else
                    {
                        $objWriter->writeAttribute('val', ($i-1));
                    }
                $objWriter->endElement();
                $objWriter->startElement('c:order');
                    if(isset($aParam['overwriteid']))
                    {
                        $objWriter->writeAttribute('val', $aParam['overwriteid']);
                    }
                    else
                    {
                        $objWriter->writeAttribute('val', ($i-1));
                    }
                $objWriter->endElement();
                $objWriter->startElement('c:tx');
                    $objWriter->startElement('c:strRef');
                        $objWriter->writeElement('c:f', $pChart->getDataHeader($i));
                    $objWriter->endElement();
                $objWriter->endElement();
                if($pChart->getChartType() == 'line')
                {
                    $objWriter->startElement('c:marker');
                        $objWriter->startElement('c:symbol');
                            $objWriter->writeAttribute('val', 'square');
                        $objWriter->endElement();
                    $objWriter->endElement();
                }
                elseif($pChart->getChartType() == 'pareto')
                {
                    if($aParam['subchart'] == 'paretoLineVal')
                    {
                        $objWriter->startElement('c:marker');
                            $objWriter->startElement('c:symbol');
                                $objWriter->writeAttribute('val', 'none');
                            $objWriter->endElement();
                        $objWriter->endElement();
                    }
                }
                elseif($pChart->getChartType() == 'pie')
                {
                    if($pChart->getChartPieExploded() === true)
                    {
                        $objWriter->startElement('c:explosion');
                            $objWriter->writeAttribute('val', '8');
                        $objWriter->endElement();
                    }
                    $objWriter->startElement('c:dLbls');
                        $objWriter->startElement('c:dLblPos');
                            $objWriter->writeAttribute('val', 'bestFit');
                        $objWriter->endElement();
                        $objWriter->startElement('c:showCatName');
                            $objWriter->writeAttribute('val', '1');
                        $objWriter->endElement();
                        $objWriter->startElement('c:showLeaderLines');
                            $objWriter->writeAttribute('val', '1');
                        $objWriter->endElement();
                    $objWriter->endElement();
                }
                elseif($pChart->getChartType() == 'spc')
                {
                    if($i == 1)
                    {
                        $objWriter->startElement('c:marker');
                            $objWriter->startElement('c:symbol');
                                $objWriter->writeAttribute('val', 'square');
                            $objWriter->endElement();
                        $objWriter->endElement();
                    }
                    else
                    {
                        $objWriter->startElement('c:marker');
                            $objWriter->startElement('c:symbol');
                                $objWriter->writeAttribute('val', 'none');
                            $objWriter->endElement();
                        $objWriter->endElement();
                    }
                }                                             
                
                $objWriter->startElement('c:cat');
                    $objWriter->startElement('c:strRef');
                        $objWriter->writeElement('c:f', $pChart->getDataRange('cat', $i, $rowDim));
                    $objWriter->endElement();
                $objWriter->endElement(); //  c:cat
                $objWriter->startElement('c:val');
                    $objWriter->startElement('c:numRef');
                        if($pChart->getChartType() == 'pareto' && $aParam['subchart'] == 'paretoLineVal')
                        {
                            $objWriter->writeElement('c:f', $pChart->getDataRange('val', ($i+1), $rowDim)); // Dataset for pareto Cumulative Data column (line graph)
                        }
                        else
                        {
                            $objWriter->writeElement('c:f', $pChart->getDataRange('val', $i, $rowDim));
                        }
                    $objWriter->endElement();
                $objWriter->endElement(); //  c:val
            $objWriter->endElement(); //  c:ser
        }
    }           
    
    
    public function _writeBarChart(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart = null)
    {
        $objWriter->startElement('c:barChart');
            $objWriter->startElement('c:barDir');
                $objWriter->writeAttribute('val', $pChart->getChartBarDirection());
            $objWriter->endElement();
            $objWriter->startElement('c:grouping');
                $objWriter->writeAttribute('val', $pChart->getChartBarGrouping());
            $objWriter->endElement();
            
            $this->_writeSerData($objWriter, $pChart);
            
            if($pChart->getChartBarGrouping() == 'stacked')
            {
                $objWriter->startElement('c:overlap');
                    $objWriter->writeAttribute('val', '100');
                $objWriter->endElement();                
            }
            
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74029312');
            $objWriter->endElement();
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74047488');
            $objWriter->endElement();
        $objWriter->endElement(); //  c:barChart
        
        $objWriter->startElement('c:catAx');
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74029312');
            $objWriter->endElement();
            $objWriter->startElement('c:scaling');
                $objWriter->startElement('c:orientation');
                    $objWriter->writeAttribute('val', 'minMax');
                $objWriter->endElement();
            $objWriter->endElement();
            $objWriter->startElement('c:axPos');
                $objWriter->writeAttribute('val', 'b');
            $objWriter->endElement();
            $objWriter->startElement('c:tickLblPos');
                $objWriter->writeAttribute('val', 'nextTo');
            $objWriter->endElement();
            $objWriter->startElement('c:crossAx');
                $objWriter->writeAttribute('val', '74047488');
            $objWriter->endElement();
            $objWriter->startElement('c:crosses');
                $objWriter->writeAttribute('val', 'autoZero');
            $objWriter->endElement();
            $objWriter->startElement('c:auto');
                $objWriter->writeAttribute('val', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:lblAlgn');
                $objWriter->writeAttribute('val', 'ctr');
            $objWriter->endElement();
            $objWriter->startElement('c:lblOffset');
                $objWriter->writeAttribute('val', '100');
            $objWriter->endElement();
        $objWriter->endElement(); //catAx
            
        $objWriter->startElement('c:valAx');
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74047488');
            $objWriter->endElement();
            $objWriter->startElement('c:scaling');
                $objWriter->startElement('c:orientation');
                    $objWriter->writeAttribute('val', 'minMax');
                $objWriter->endElement();
            $objWriter->endElement();
            $objWriter->startElement('c:axPos');
                $objWriter->writeAttribute('val', 'l');
            $objWriter->endElement();
            $objWriter->writeElement('c:majorGridlines');
            $objWriter->startElement('c:numFmt');
                $objWriter->writeAttribute('formatCode', 'General');
                $objWriter->writeAttribute('sourceLinked', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:tickLblPos');
                $objWriter->writeAttribute('val', 'nextTo');
            $objWriter->endElement();
            $objWriter->startElement('c:crossAx');
                $objWriter->writeAttribute('val', '74029312');
            $objWriter->endElement();
            $objWriter->startElement('c:crosses');
                $objWriter->writeAttribute('val', 'autoZero');
            $objWriter->endElement();
            $objWriter->startElement('c:crossBetween');
                $objWriter->writeAttribute('val', 'between');
            $objWriter->endElement();
        $objWriter->endElement(); //valAx
    }
    
    
    public function _writePieChart(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart = null)
    {
        $objWriter->startElement('c:pieChart');
            $objWriter->startElement('c:varyColors');
                $objWriter->writeAttribute('val', '1');
            $objWriter->endElement();
            
            $this->_writeSerData($objWriter, $pChart);
          
            $objWriter->startElement('c:firstSliceAng');
                $objWriter->writeAttribute('val', '0');
            $objWriter->endElement();
        $objWriter->endElement(); //  c:pieChart
    }
    
    
    public function _writeLineChart(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart = null)
    {
        $objWriter->startElement('c:lineChart');
            $objWriter->startElement('c:grouping');
                        $objWriter->writeAttribute('val', 'standard');
            $objWriter->endElement();
            
            $this->_writeSerData($objWriter, $pChart);
            
            $objWriter->startElement('c:marker');
                $objWriter->writeAttribute('val', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '110573056');
            $objWriter->endElement();
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '106637952');
            $objWriter->endElement();
        $objWriter->endElement(); //  c:lineChart
        
        
        $objWriter->startElement('c:catAx');
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '110573056');
            $objWriter->endElement();
            $objWriter->startElement('c:scaling');
                $objWriter->startElement('c:orientation');
                    $objWriter->writeAttribute('val', 'minMax');
                $objWriter->endElement();
            $objWriter->endElement();
            $objWriter->startElement('c:axPos');
                $objWriter->writeAttribute('val', 'b');
            $objWriter->endElement();
            $objWriter->startElement('c:tickLblPos');
                $objWriter->writeAttribute('val', 'low');
            $objWriter->endElement();
            $objWriter->startElement('c:crossAx');
                $objWriter->writeAttribute('val', '106637952');
            $objWriter->endElement();
            $objWriter->startElement('c:crosses');
                $objWriter->writeAttribute('val', 'autoZero');
            $objWriter->endElement();
            $objWriter->startElement('c:auto');
                $objWriter->writeAttribute('val', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:lblAlgn');
                $objWriter->writeAttribute('val', 'ctr');
            $objWriter->endElement();
            $objWriter->startElement('c:lblOffset');
                $objWriter->writeAttribute('val', '100');
            $objWriter->endElement();
        $objWriter->endElement(); //catAx
            
        $objWriter->startElement('c:valAx');
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '106637952');
            $objWriter->endElement();
            $objWriter->startElement('c:scaling');
                $objWriter->startElement('c:orientation');
                    $objWriter->writeAttribute('val', 'minMax');
                $objWriter->endElement();
            $objWriter->endElement();
            $objWriter->startElement('c:axPos');
                $objWriter->writeAttribute('val', 'l');
            $objWriter->endElement();
            $objWriter->writeElement('c:majorGridlines');
            $objWriter->startElement('c:numFmt');
                $objWriter->writeAttribute('formatCode', 'General');
                $objWriter->writeAttribute('sourceLinked', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:tickLblPos');
                $objWriter->writeAttribute('val', 'nextTo');
            $objWriter->endElement();
            $objWriter->startElement('c:crossAx');
                $objWriter->writeAttribute('val', '110573056');
            $objWriter->endElement();
            $objWriter->startElement('c:crosses');
                $objWriter->writeAttribute('val', 'autoZero');
            $objWriter->endElement();
            $objWriter->startElement('c:crossBetween');
                $objWriter->writeAttribute('val', 'midCat'); //not between
            $objWriter->endElement();
        $objWriter->endElement(); //valAx
    }
    
    
    public function _writeSPCChart(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart = null)
    {
        $this->_writeLineChart($objWriter, $pChart);
    }                                   
    
    
    public function _writeParetoChart(PHPExcel_Shared_XMLWriter $objWriter = null, PHPExcel_Chart $pChart = null)
    {
        $objWriter->startElement('c:barChart');
            $objWriter->startElement('c:barDir');
                $objWriter->writeAttribute('val', 'col');
            $objWriter->endElement();
            $objWriter->startElement('c:grouping');
                $objWriter->writeAttribute('val', 'clustered');
            $objWriter->endElement();
            $this->_writeSerData($objWriter, $pChart, array('subchart' => 'paretoBarVal'));
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74235904');
            $objWriter->endElement();
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74217728');
            $objWriter->endElement();
        $objWriter->endElement(); //  c:barChart
        
        $objWriter->startElement('c:lineChart');
            $objWriter->startElement('c:grouping');
                        $objWriter->writeAttribute('val', 'standard');
            $objWriter->endElement();
            $this->_writeSerData($objWriter, $pChart, array('subchart' => 'paretoLineVal', 'overwriteid' => 1));
            $objWriter->startElement('c:marker');
                $objWriter->writeAttribute('val', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74235904');
            $objWriter->endElement();
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74217728');
            $objWriter->endElement();
        $objWriter->endElement(); //  c:lineChart
        
        $objWriter->startElement('c:catAx');
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74235904');
            $objWriter->endElement();
            $objWriter->startElement('c:scaling');
                $objWriter->startElement('c:orientation');
                    $objWriter->writeAttribute('val', 'minMax');
                $objWriter->endElement();
            $objWriter->endElement();
            $objWriter->startElement('c:axPos');
                $objWriter->writeAttribute('val', 'b');
            $objWriter->endElement();
            $objWriter->startElement('c:tickLblPos');
                $objWriter->writeAttribute('val', 'nextTo');
            $objWriter->endElement();
            $objWriter->startElement('c:crossAx');
                $objWriter->writeAttribute('val', '74217728');
            $objWriter->endElement();
            $objWriter->startElement('c:crosses');
                $objWriter->writeAttribute('val', 'autoZero');
            $objWriter->endElement();
            $objWriter->startElement('c:auto');
                $objWriter->writeAttribute('val', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:lblAlgn');
                $objWriter->writeAttribute('val', 'ctr');
            $objWriter->endElement();
            $objWriter->startElement('c:lblOffset');
                $objWriter->writeAttribute('val', '100');
            $objWriter->endElement();
        $objWriter->endElement(); //catAx
            
        $objWriter->startElement('c:valAx');
            $objWriter->startElement('c:axId');
                $objWriter->writeAttribute('val', '74217728');
            $objWriter->endElement();
            $objWriter->startElement('c:scaling');
                $objWriter->startElement('c:orientation');
                    $objWriter->writeAttribute('val', 'minMax');
                $objWriter->endElement();
            $objWriter->endElement();
            $objWriter->startElement('c:axPos');
                $objWriter->writeAttribute('val', 'l');
            $objWriter->endElement();
            $objWriter->writeElement('c:majorGridlines');
            $objWriter->startElement('c:numFmt');
                $objWriter->writeAttribute('formatCode', 'General');
                $objWriter->writeAttribute('sourceLinked', '1');
            $objWriter->endElement();
            $objWriter->startElement('c:tickLblPos');
                $objWriter->writeAttribute('val', 'nextTo');
            $objWriter->endElement();
            $objWriter->startElement('c:crossAx');
                $objWriter->writeAttribute('val', '74235904');
            $objWriter->endElement();
            $objWriter->startElement('c:crosses');
                $objWriter->writeAttribute('val', 'autoZero');
            $objWriter->endElement();
            $objWriter->startElement('c:crossBetween');
                $objWriter->writeAttribute('val', 'between');
            $objWriter->endElement();
        $objWriter->endElement(); //valAx
    }
     
     
    public function writeDrawing(PHPExcel $pPHPExcel = null)
    {
        /* chart size: 
         * From 0 0 to:
         * <xdr:col>17</xdr:col>
         * <xdr:row>36</xdr:row>
         */ 
        $content = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><xdr:wsDr xmlns:xdr="http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><xdr:twoCellAnchor><xdr:from><xdr:col>0</xdr:col><xdr:colOff>0</xdr:colOff><xdr:row>0</xdr:row><xdr:rowOff>0</xdr:rowOff></xdr:from><xdr:to><xdr:col>17</xdr:col><xdr:colOff>0</xdr:colOff><xdr:row>36</xdr:row><xdr:rowOff>0</xdr:rowOff></xdr:to><xdr:graphicFrame macro=""><xdr:nvGraphicFramePr><xdr:cNvPr id="2" name="Chart 1"/><xdr:cNvGraphicFramePr/></xdr:nvGraphicFramePr><xdr:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/></xdr:xfrm><a:graphic><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/chart"><c:chart xmlns:c="http://schemas.openxmlformats.org/drawingml/2006/chart" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" r:id="rId1"/></a:graphicData></a:graphic></xdr:graphicFrame><xdr:clientData/></xdr:twoCellAnchor></xdr:wsDr>';
        
        return $content;
    }
    
    
    public function writeDrawingRel(PHPExcel $pPHPExcel = null)
    {
        $content = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart" Target="../charts/chart1.xml"/></Relationships>';
        
        return $content;
    }
    
    
    public function writeWorksheet2(PHPExcel $pPHPExcel = null)
    {
        $content = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"><dimension ref="A1"/><sheetViews><sheetView tabSelected="1" workbookViewId="0"><selection activeCell="O11" sqref="O11"/></sheetView></sheetViews><sheetFormatPr defaultRowHeight="15"/><sheetData/><pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/><drawing r:id="rId1"/></worksheet>';
        
        return $content;
    }
    
    
    public function writeWorksheetDrawingRel(PHPExcel $pPHPExcel = null)
    {
        $content = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships"><Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing" Target="../drawings/drawing1.xml"/></Relationships>';
                
        return $content;
    }                          
    
    
    function ExcelConvertToLetter($pColumnIndex = 0)
    {
        if($pColumnIndex < 26) 
        {
            return chr(65 + $pColumnIndex);
        }
        return ExcelConvertToLetter((int)($pColumnIndex / 26) -1).chr(65 + $pColumnIndex%26);
    }
}
?>
