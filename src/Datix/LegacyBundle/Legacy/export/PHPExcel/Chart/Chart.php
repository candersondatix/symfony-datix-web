<?php

/**
 * PHPExcel_Chart
 *
 * @category   PHPExcel
 * @package    PHPExcel_Worksheet
 * @copyright  Copyright (c) 2009 DATIX Ltd
 */

/** PHPExcel root directory */
if (!defined('PHPEXCEL_ROOT')) {
    /**
     * @ignore
     */
    define('PHPEXCEL_ROOT', dirname(__FILE__) . '/../../');
}

class PHPExcel_Chart 
{    
    /**
     * Path
     *
     * @var string
     */
    private $_chartTitle;
    private $_chartType;
    private $_chartLegend;
    private $_chartDimensionC;
    private $_chartDimensionR;
    private $_chartBarGrouping;
    private $_chartBarDirection;
    private $_chartPieExploded;
    private $_chartLegendFontSize;
    private $_chartLegendExtended;
    
    public function __construct()
    {
        $this->_chartTitle = 'Chart title';
        $this->_chartType = 'pie';
        $this->_chartLegend = false;
        $this->_chartDimensionC = 1;
        $this->_chartDimensionR = 1;
        $this->_chartBarGrouping = 'clustered';
        $this->_chartBarDirection = 'col';
        $this->_chartPieExploded = false;
        $this->_chartLegendFontSize = 10;
        $this->_chartLegendExtended = false;
    }
    
    public function getChartTitle()
    {
        return $this->_chartTitle;
    }

    public function setChartTitle($chartTitle = '')
    {
        return $this->_chartTitle = $chartTitle;
    }
    
    public function getChartType()
    {
        return $this->_chartType;
    }

    public function setChartType($chartType)
    {
        switch($chartType)
        {
            case 'bar':
                break;
            case 'horizontalbar':
                $this->setChartBarDirection('bar');
                $chartType = 'bar';
                break;
            case 'stackedbar':
                $this->setChartBarGrouping('stacked');
                $chartType = 'bar';
                break;
            case 'horizontalstackedbar':
                $this->setChartBarGrouping('stacked');
                $this->setChartBarDirection('bar');
                $chartType = 'bar';
                break;
            case 'pieexploded':
                $this->setChartPieExploded(true);
                $chartType = 'pie';
                break;
        }
        
        return $this->_chartType = $chartType;
    }
    
    public function getLegend()
    {
        return $this->_chartLegend;
    }
    
    public function setLegend($bLegend)
    {
        $this->_chartLegend = $bLegend;
    }
    
    public function getDimensionC()
    {
        return $this->_chartDimensionC;
    }
    
    public function setDimensionC($nDimension)
    {
        $this->_chartDimensionC = $nDimension;
    }
    
    public function getDimensionR()
    {
        return $this->_chartDimensionR;
    }
    
    public function setDimensionR($nDimension)
    {
        $this->_chartDimensionR = $nDimension;
    }
        
    public function getDataRange($ref = 'cat', $colDim = 1, $rowDim = 1)
    {
        if($ref == 'cat')
        {
            return 'Data!$A$2:$A$' . ($rowDim+1);
        }
        elseif($ref == 'val')
        {
            $colLetter = ExcelConvertToLetter(($colDim));
            return 'Data!$'.$colLetter.'$2:$'.$colLetter.'$' . ($rowDim+1);
        }
    }
    
    public function getDataHeader($colDim)
    {
        $colLetter = ExcelConvertToLetter(($colDim));
        return 'Data!$'.$colLetter.'$1';
    }
    
    public function getChartBarGrouping()
    {
        return $this->_chartBarGrouping;
    }

    public function setChartBarGrouping($chartBarGrouping = '')
    {
        return $this->_chartBarGrouping = $chartBarGrouping;
    }
    
    public function getChartBarDirection()
    {
        return $this->_chartBarDirection;
    }

    public function setChartBarDirection($chartBarDirection = '')
    {
        return $this->_chartBarDirection = $chartBarDirection;
    }
    
    public function getChartPieExploded()
    {
        return $this->_chartPieExploded;
    }

    public function setChartPieExploded($chartPieExploded = false)
    {
        return $this->_chartPieExploded = $chartPieExploded;
    }
    
    function ExcelConvertToLetter($pColumnIndex = 0)
    {
        if($pColumnIndex < 26) 
        {
            return chr(65 + $pColumnIndex);
        }
        return ExcelConvertToLetter((int)($pColumnIndex / 26) - 1).chr(65 + $pColumnIndex%26);
    }
    
    public function getChartLegendFontSize()
    {
        return $this->_chartLegendFontSize;
    }

    public function setChartLegendFontSize($chartLegendFontSize = '')
    {
        $this->_chartLegendFontSize = $chartLegendFontSize;
    }
    
    public function getChartLegendExtended()
    {
        return $this->_chartLegendExtended;
    }

    public function setChartLegendExtended($chartLegendExtended = false)
    {
        $this->_chartLegendExtended = $chartLegendExtended;
    }
}
?>
