<?php

/* This class wrapper adds new custom functionality to the 
 * original third party PDFTable class
 */

require_once __DIR__.'/fpdf16/pdftable.inc.php';

class PDFTableWrapper extends PDFTable
{
    private $headerTitle;

    public function SetHeaderTitle ($title) 
    {
        $this->headerTitle = $title;
    }

    public function Header ()
    {
        if (!empty($this->headerTitle))
        {
            $this->SetXY($this->lMargin, $this->tMargin);
            $this->Cell(0,10,$this->headerTitle,0,0,'C');
            $this->Ln(10);
        }

        parent::Header();
    }
}

?>
