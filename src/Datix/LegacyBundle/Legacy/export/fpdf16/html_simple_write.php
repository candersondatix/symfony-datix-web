<?php
require('fpdf.php');
//require('fpdf.inc.php');
class PDF extends FPDF
    {
    var $B;
    var $I;
    var $U;
    var $HREF;

    function PDF($orientation='P',$unit='mm',$format='A4')
    {
        //Call parent constructor
        $this->FPDF($orientation,$unit,$format);
        //Initialization
        $this->B=0;
        $this->I=0;
        $this->U=0;
        $this->HREF='';
    }
    
    function WriteHTML($html)
    {
        //HTML parser
        
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                else
                    $this->Write(5,$e);
            }
            else
            {
                //Tag
                if($e{0}=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract attributes
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $attr=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $attr[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$attr);
                }
            }
        }
    }

    function OpenTag($tag,$attr)
    {
        //Opening tag
        if($tag=='B' or $tag=='I' or $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$attr['HREF'];
        if($tag=='BR')
            $this->Ln(5);
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' or $tag=='I' or $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }

    function Footer()
    {
        global $dtxprint_footer, $dtxpdf_footer;         
        
        if(!isset($dtxpdf_footer))
        {
            if($dtxprint_footer != '')
                $dtxpdf_footer = $dtxprint_footer;
        }
        
        if(!is_array($dtxpdf_footer)) // string
        {
            $dtxpdf_footer = array('middle' => array('type' => 'text', 'value' =>  $dtxpdf_footer));
        }
            
        if(is_array($dtxpdf_footer))
        {
            
            foreach($dtxpdf_footer as $dtxpdf_align => $dtxpdf_cell)
            {
                
                switch($dtxpdf_align)
                {
                    case 'left':
                        $dtxpdf_cell_align = 'L';
                        $x = $this->lMargin;
                        break;
                    case 'middle':
                        $dtxpdf_cell_align = 'C';
                        $x = null;
                        break;
                    case 'right':
                        $dtxpdf_cell_align = 'R';
                        $x = $this->w-$this->rMargin-33;
                        break;
                }
                
                $this->SetXY(15, -15);
                
                $y = -15;
                
                if($dtxpdf_cell['type'] == 'image')
                {
                    $this->Image($dtxpdf_cell['src'],$x, null,33);
                }
                            
                if($dtxpdf_cell['type'] == 'text')
                {
                    $this->SetY(-15);
                    $this->SetFont($dtxpdf_cell['font'], $dtxpdf_cell['style'], $dtxpdf_cell['size']);
                    $this->Cell($x,10,$dtxpdf_cell['value'],0,0,$dtxpdf_cell_align);
                    $this->SetFont('Helvetica');
                }
                
                if($dtxpdf_cell['type'] == 'pageno')
                {
                    $this->SetY(-15);
                    $this->SetFont($dtxpdf_cell['font'], $dtxpdf_cell['style'], $dtxpdf_cell['size']);
                    $this->Cell($x,10,'Page '.$this->PageNo().'/{nb}',0,0,$dtxpdf_cell_align);
                    $this->SetFont('Helvetica');
                } 
                
                $dtxpdf_cell = array();
            }
        }
    }       
}
?>