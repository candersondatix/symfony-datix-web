/*
 *  (c) 2005-2009 Matt Brown (http://dowdybrown.com)
 *
 *  Rico is licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 *  file except in compliance with the License. You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the
 *  License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

if(typeof Rico=='undefined') throw("LiveGridForms requires the Rico JavaScript framework");
if(typeof RicoUtil=='undefined') throw("LiveGridForms requires the RicoUtil object");
if(typeof RicoTranslate=='undefined') throw("LiveGridForms requires the RicoTranslate object");


Rico.TableEdit = Class.create(
/** @lends Rico.TableEdit# */
{
/**
 * @class Supports editing LiveGrid data.
 * @constructs
 */
  initialize: function(liveGrid) {
    Rico.writeDebugMsg('Rico.TableEdit initialize: '+liveGrid.tableId);
    this.grid=liveGrid;
    this.options = {
      maxDisplayLen    : 75,    // max displayed text field length
      panelHeight      : 200,   // size of tabbed panels
      panelWidth       : 500,
      hoverClass       : 'tabHover',
      selectedClass    : 'tabSelected',
      compact          : false,    // compact corners
      RecordName       : RicoTranslate.getPhraseById("record"),
      updateURL        : window.location.href, // default is that updates post back to the generating page
      readOnlyColor    : '#AAA',   // read-only fields displayed using this color
      showSaveMsg      : 'errors'  // disposition of database update responses (full - show full response, errors - show full response for errors and short response otherwise)
    };
    Object.extend(this.options, liveGrid.options);
    this.hasWF2=(document.implementation && document.implementation.hasFeature && document.implementation.hasFeature('WebForms', '2.0'));
    this.menu=liveGrid.menu;
    //if(!this.menu.options.dataMenuHandler)
    //{
        this.menu.options.dataMenuHandler=this.editMenu.bind(this);
    //}
    this.menu.ignoreClicks();
    RicoEditControls.atLoad();
    this.createEditDiv();
    this.createKeyArray();
    this.saveMsg=$(liveGrid.tableId+'_savemsg');
    Event.observe(document,"click", this.clearSaveMsg.bindAsEventListener(this), false);
    this.extraMenuItems=new Array();
    this.responseHandler=this.processResponse.bind(this);
    Rico.writeDebugMsg("Rico.TableEdit.initialize complete, hasWF2="+this.hasWF2);
  },

  canDragFunc: function(elem,event) {
    if (elem.componentFromPoint) {
      //Rico.writeDebugMsg('canDragFunc: '+elem.tagName+' '+elem.componentFromPoint(event.clientX,event.clientY));
      var c=elem.componentFromPoint(event.clientX,event.clientY);
      // for some reason, IE returns outside when this is called inside a frame
      if (c!='' && c!='outside') return false;
    }
    return (elem==this.editDiv || elem.tagName=='FORM');
  },

  createKeyArray: function() {
    this.keys=[];
    for (var i=0; i<this.grid.columns.length; i++) {
      if (this.grid.columns[i].format && this.grid.columns[i].format.isKey)
        this.keys.push(i);
    }
  },

  createEditDiv: function() {

    // create editDiv (form)

    this.requestCount=1;
    this.editDiv = this.grid.createDiv('edit',document.body);
    this.editDiv.style.display='none';
    this.editDiv.style.zIndex = "50";
    if (this.options.canEdit || this.options.canAdd) {
      this.startForm();
      this.createForm(this.form);
    } else {
      var buttonClose=this.createButton(RicoTranslate.getPhraseById("close"));
      Event.observe(buttonClose,"click", this.cancelEdit.bindAsEventListener(this), false);
      this.createForm(this.editDiv);
    }
    this.editDivCreated=true;
    this.formPopup=new Rico.Popup({ignoreClicks:true, hideOnClick:false, canDragFunc: this.canDragFunc.bind(this) }, this.editDiv);

    // create responseDialog

    this.responseDialog = this.grid.createDiv('editResponse',document.body);
    this.responseDialog.style.display='none';

    var buttonOK = document.createElement('button');
    buttonOK.appendChild(document.createTextNode('OK'));
    buttonOK.onclick=this.ackResponse.bindAsEventListener(this);
    this.responseDialog.appendChild(buttonOK);

    this.responseDiv = this.grid.createDiv('editResponseText',this.responseDialog);

    if (this.panelGroup) {
      Rico.writeDebugMsg("createEditDiv complete, requestCount="+this.requestCount);
      setTimeout(this.initPanelGroup.bind(this),50);
    }
  },

  initPanelGroup: function() {
    this.requestCount--;
    Rico.writeDebugMsg("initPanelGroup: "+this.requestCount);
    if (this.requestCount>0) return;
    var wi=parseInt(this.options.panelWidth,10);
    if (this.form) {
      this.form.style.width=(wi+10)+'px';
      if (Prototype.Browser.WebKit) this.editDiv.style.display='block';  // this causes display to flash briefly
      this.options.bgColor = Rico.Color.createColorFromBackground(this.form);
    }
    this.editDiv.style.display='none';
    this.options.panelHdrWidth=(Math.floor(wi / this.options.panels.length)-4)+'px';
    this.Accordion=new Rico.TabbedPanel(this.panelHdr.findAll(this.notEmpty), this.panelContent.findAll(this.notEmpty), this.options);
  },

  notEmpty: function(v) {
    return typeof(v)!='undefined';
  },

  startForm: function() {
    this.form = document.createElement('form');
    /** @ignore */
    this.form.onsubmit=function() {return false;};
    this.editDiv.appendChild(this.form);

    // hidden fields
    this.hiddenFields = document.createElement('div');
    this.hiddenFields.style.display='none';
    this.action = this.appendHiddenField(this.grid.tableId+'__action','');
    var i,fldSpec;
    for (i=0; i<this.grid.columns.length; i++) {
      fldSpec=this.grid.columns[i].format;
      if (fldSpec && fldSpec.FormView && fldSpec.FormView=="hidden")
        this.appendHiddenField(fldSpec.FieldName,fldSpec.ColData);
    }
    this.form.appendChild(this.hiddenFields);

 /*   var tab = document.createElement('table');
    var row = tab.insertRow(-1);
    var cell = row.insertCell(-1);
    var button=cell.appendChild(this.createButton(RicoTranslate.getPhraseById("saveRecord",this.options.RecordName)));
    Event.observe(button,"click", this.TESubmit.bindAsEventListener(this), false);
    cell = row.insertCell(-1);
    button=cell.appendChild(this.createButton(RicoTranslate.getPhraseById("cancel")));
    Event.observe(button,"click", this.cancelEdit.bindAsEventListener(this), false);
    this.form.appendChild(tab);  */
  },

  createButton: function(buttonLabel) {
    var button = document.createElement('button');
    button.innerHTML="<span style='text-decoration:underline;'>"+buttonLabel.charAt(0)+"</span>"+buttonLabel.substr(1);
    button.accessKey=buttonLabel.charAt(0);
    return button;
  },

  createPanel: function(i) {
    var hasFields=false;
    for (var j=0; j<this.grid.columns.length; j++) {
      var fldSpec=this.grid.columns[j].format;
      if (!fldSpec) continue;
      if (!fldSpec.EntryType) continue;
      if (fldSpec.EntryType=='H') continue;
      var panelIdx=fldSpec.panelIdx || 0;
      if (panelIdx==i) {
        hasFields=true;
        break;
      }
    }
    if (!hasFields) return false;
    this.panelHdr[i] = document.createElement('div');
    this.panelHdr[i].className='tabHeader';
    this.panelHdr[i].innerHTML=this.options.panels[i];
    this.panelHdrs.appendChild(this.panelHdr[i]);
    this.panelContent[i] = document.createElement('div');
    this.panelContent[i].className='tabContent';
    this.panelContents.appendChild(this.panelContent[i]);
    return true;
  },

  createForm: function(parentDiv) {
    var i,div,fldSpec,panelIdx,tables=[];
    this.panelHdr=[];
    this.panelContent=[];
    if (this.options.panels) {
      this.panelGroup = document.createElement('div');
      this.panelGroup.className='tabPanelGroup';
      this.panelHdrs = document.createElement('div');
      this.panelGroup.appendChild(this.panelHdrs);
      this.panelContents = document.createElement('div');
      this.panelContents.className='tabContentContainer';
      this.panelGroup.appendChild(this.panelContents);
      parentDiv.appendChild(this.panelGroup);
      if (this.grid.direction=='rtl') {
        for (i=this.options.panels.length-1; i>=0; i--) {
          if (this.createPanel(i))
            tables[i]=this.createFormTable(this.panelContent[i],'tabContent');
        }
      } else {
        for (i=0; i<this.options.panels.length; i++) {
          if (this.createPanel(i))
            tables[i]=this.createFormTable(this.panelContent[i],'tabContent');
        }
      }
      parentDiv.appendChild(this.panelGroup);
    } else {
      div=document.createElement('div');
      div.className='noTabContent';
      tables[0]=this.createFormTable(div);
      parentDiv.appendChild(div);
    }
    for (i=0; i<this.grid.columns.length; i++) {
      fldSpec=this.grid.columns[i].format;
      if (!fldSpec) continue;
      panelIdx=fldSpec.panelIdx || 0;
      if (tables[panelIdx]) this.appendFormField(this.grid.columns[i],tables[panelIdx]);
      if (typeof fldSpec.pattern=='string') {
        switch (fldSpec.pattern) {
          case 'email':
            fldSpec.regexp=/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/;
            break;
          case 'float-unsigned':
            fldSpec.regexp=/^\d+(\.\d+)?$/;
            break;
          case 'float-signed':
            fldSpec.regexp=/^[-+]?\d+(\.\d+)?$/;
            break;
          case 'int-unsigned':
            fldSpec.regexp=/^\d+$/;
            break;
          case 'int-signed':
            fldSpec.regexp=/^[-+]?\d+$/;
            break;
          default:
            fldSpec.regexp=new RegExp(fldSpec.pattern);
            break;
        }
      }
    }

    var buttonDiv = document.createElement('div');
    buttonDiv.align='center';
    var button=buttonDiv.appendChild(this.createButton(RicoTranslate.getPhraseById("saveRecord",this.options.RecordName)));
    Event.observe(button,"click", this.TESubmit.bindAsEventListener(this), false);
    button=buttonDiv.appendChild(this.createButton(RicoTranslate.getPhraseById("cancel")));
    Event.observe(button,"click", this.cancelEdit.bindAsEventListener(this), false);
    div.appendChild(buttonDiv);
  },

  createFormTable: function(div) {
    var tab=document.createElement('table');
    tab.border=0;
    div.appendChild(tab);
    return tab;
  },

  appendHiddenField: function(name,value) {
    var field=RicoUtil.createFormField(this.hiddenFields,'input','hidden',name,name);
    field.value=value;
    return field;
  },

  appendFormField: function(column, table) {
    var fmt=column.format;
    if (!fmt.EntryType) return;
    if (fmt.EntryType=="H" || fmt.ForceHide) return;
    if (fmt.FormView) return;
    Rico.writeDebugMsg('appendFormField: '+column.displayName+' - '+fmt.EntryType);
    var row = table.insertRow(-1);
    var hdr = row.insertCell(-1);
    column.formLabel=hdr;
    if (hdr.noWrap) hdr.noWrap=true;
    var entry = row.insertCell(-1);
    if (entry.noWrap) entry.noWrap=true;
    hdr.innerHTML=column.displayName;
    hdr.id='lbl_'+fmt.FieldName;
    if (fmt.Help) {
      hdr.title=fmt.Help;
      hdr.className='ricoEditLabelWithHelp';
    } else {
      hdr.className='ricoEditLabel';
    }
    var field, name=fmt.FieldName;
    switch (fmt.EntryType) {
      case 'TA':
      case 'tinyMCE':
        field=RicoUtil.createFormField(entry,'textarea',null,name);
        field.cols=fmt.TxtAreaCols;
        field.rows=fmt.TxtAreaRows;
        field.innerHTML=fmt.ColData;
        hdr.style.verticalAlign='top';
        break;
      case 'R':
      case 'RL':
        field=RicoUtil.createFormField(entry,'div',null,name);
        if (fmt.isNullable) this.addSelectNone(field);
        this.selectValuesRequest(field,fmt);
        break;
      case 'N':
        field=RicoUtil.createFormField(entry,'select',null,name);
        if (fmt.isNullable) this.addSelectNone(field);
        field.onchange=this.checkSelectNew.bindAsEventListener(this);
        this.selectValuesRequest(field,fmt);
        field=document.createElement('span');
        field.className='ricoEditLabel';
        field.id='labelnew__'+fmt.FieldName;
        field.innerHTML='&nbsp;&nbsp;&nbsp;'+RicoTranslate.getPhraseById('formNewValue').replace(' ','&nbsp;');
        entry.appendChild(field);
        name='textnew__'+fmt.FieldName;
        field=RicoUtil.createFormField(entry,'input','text',name,name);
        break;
      case 'S':
      case 'SL':
        field=RicoUtil.createFormField(entry,'select',null,name);
        if (fmt.isNullable) this.addSelectNone(field);
        this.selectValuesRequest(field,fmt);
        break;
      case 'MS':
        field=RicoUtil.createFormFieldMulti(entry,'select',null,name,name,'multiple',fmt.ColData, fmt.SelectField, false, column.format.showNumbers);
        break;
       // if (fmt.isNullable) this.addSelectNone(field);
        //this.selectValuesRequest(field,fmt);
      case 'MSE':
        field=RicoUtil.createFormFieldMulti(entry,'select',null,name,name,'multiple',fmt.ColData, fmt.SelectField, true, column.format.showNumbers);
        break;
      case 'DS':
        field=RicoUtil.createFormFieldDatixSelect(entry, name);
        this.selectValuesRequest(field,fmt);
        break;
      case 'CP':
        field=RicoUtil.createFormFieldCP(entry,'input','text',name,name);
        break;
      case 'D':
        if (!fmt.isNullable) fmt.required=true;
        if (typeof fmt.min=='string') fmt.min=fmt.min.toISO8601Date() || new Date(fmt.min);
        if (typeof fmt.max=='string') fmt.max=fmt.max.toISO8601Date() || new Date(fmt.max);
        if (this.hasWF2) {
          field=RicoUtil.createFormField(entry,'input','date',name,name);
          field.required=fmt.required;
          if (fmt.min) field.min=fmt.min.toISO8601String(3);
          if (fmt.max) field.max=fmt.max.toISO8601String(3);
          field.required=fmt.required;
          fmt.SelectCtl=null;  // use the WebForms calendar instead of the Rico calendar
        } else {
          field=RicoUtil.createFormField(entry,'input','text',name,name);
        }
        this.initField(field,fmt);
        break;
      case 'I':
        if (!fmt.isNullable) fmt.required=true;
        if (!fmt.pattern) fmt.pattern='int-signed';
        if (this.hasWF2) {
          field=RicoUtil.createFormField(entry,'input','number',name,name);
          field.required=fmt.required;
          field.min=fmt.min;
          field.max=fmt.max;
          field.step=1;
        } else {
          field=RicoUtil.createFormField(entry,'input','text',name,name);
        }
        if (typeof fmt.min=='string') fmt.min=parseInt(fmt.min,10);
        if (typeof fmt.max=='string') fmt.max=parseInt(fmt.max,10);
        this.initField(field,fmt);
        break;
      case 'F':
        if (!fmt.isNullable) fmt.required=true;
        if (!fmt.pattern) fmt.pattern='float-signed';
        field=RicoUtil.createFormField(entry,'input','text',name,name);
        this.initField(field,fmt);
        if (typeof fmt.min=='string') fmt.min=parseFloat(fmt.min);
        if (typeof fmt.max=='string') fmt.max=parseFloat(fmt.max);
        break;
      default:
        field=RicoUtil.createFormField(entry,'input','text',name,name);
        if (!fmt.isNullable && fmt.EntryType!='T') fmt.required=true;
        this.initField(field,fmt);
        break;
    }
    if (field) {
      if (fmt.SelectCtl)
        RicoEditControls.applyTo(column,field);
    }
  },

  addSelectNone: function(field) {
    this.addSelectOption(field,this.options.TableSelectNone,RicoTranslate.getPhraseById("selectNone"));
  },

  initField: function(field,fmt) {
    if (fmt.LengthOverride) {
        fmt.Length=fmt.LengthOverride;
    }
    if (fmt.Length) {
      field.maxLength=fmt.Length;
      field.size=Math.min(fmt.Length, this.options.maxDisplayLen);
    }
    if (fmt.uppercase)
    {
        field.style.textTransform='uppercase';
    }
    field.value=fmt.ColData;
  },

  checkSelectNew: function(e) {
    this.updateSelectNew(Event.element(e));
  },

  updateSelectNew: function(SelObj) {
    var vis=(SelObj.value==this.options.TableSelectNew) ? "" : "hidden";
    $("labelnew__" + SelObj.id).style.visibility=vis;
    $("textnew__" + SelObj.id).style.visibility=vis;
  },

  selectValuesRequest: function(elem,fldSpec) {
    if (fldSpec.SelectValues) {
      var valueList=fldSpec.SelectValues.split(',');
      for (var i=0; i<valueList.length; i++)
        if(fldSpec.SelectDescriptions)
        {
            var descriptionList =fldSpec.SelectDescriptions.split(',');
            this.addSelectOption(elem,valueList[i],descriptionList[i],i);
        }
        else
        {
            this.addSelectOption(elem,valueList[i],valueList[i],i);
        }
    } else {
      this.requestCount++;
      var options={};
      Object.extend(options, this.grid.buffer.ajaxOptions);
      options.parameters = 'id='+fldSpec.FieldName+'&offset=0&page_size=-1';
      options.onComplete = this.selectValuesUpdate.bind(this);
      new Ajax.Request(this.grid.buffer.dataSource, options);
      Rico.writeDebugMsg("selectValuesRequest: "+options.parameters);
    }
  },

  selectValuesUpdate: function(request) {
    var response = request.responseXML.getElementsByTagName("ajax-response");
    Rico.writeDebugMsg("selectValuesUpdate: "+request.status);
    if (response == null || response.length != 1) return;
    response=response[0];
    var error = response.getElementsByTagName('error');
    if (error.length > 0) {
      var errmsg=RicoUtil.getContentAsString(error[0],this.grid.buffer.isEncoded);
      Rico.writeDebugMsg("Data provider returned an error:\n"+errmsg);
      alert(RicoTranslate.getPhraseById("requestError",errmsg));
      return null;
    }
    response=response.getElementsByTagName('response')[0];
    var id = response.getAttribute("id").slice(0,-8);
    var rowsElement = response.getElementsByTagName('rows')[0];
    var rows = this.grid.buffer.dom2jstable(rowsElement);
    var elem=$(id);
    //alert('selectValuesUpdate:'+id+' '+elem.tagName);
    Rico.writeDebugMsg("selectValuesUpdate: id="+id+' rows='+rows.length);
    for (var i=0; i<rows.length; i++) {
      if (rows[i].length>0) {
        var c0=rows[i][0];
        var c1=(rows[i].length>1) ? rows[i][1] : c0;
        this.addSelectOption(elem,c0,c1,i);
      }
    }
    if ($('textnew__'+id))
      this.addSelectOption(elem,this.options.TableSelectNew,RicoTranslate.getPhraseById("selectNewVal"));
    if (this.panelGroup)
      setTimeout(this.initPanelGroup.bind(this),50);
  },

  addSelectOption: function(elem,value,text,idx) {
    switch (elem.tagName.toLowerCase()) {
      case 'div':
        var opt=RicoUtil.createFormField(elem,'input','radio',elem.id+'_'+idx,elem.id);
        opt.value=value;
        var lbl=document.createElement('label');
        lbl.innerHTML=text;
        lbl.htmlFor=opt.id;
        elem.appendChild(lbl);
        break;
      case 'select':
        RicoUtil.addSelectOption(elem,value,text);
        break;
      case 'input':
        if (this.dsvalues == undefined)
        {
            this.dsvalues = [];
        }
        if (this.dsvalues[elem.id] == undefined)
        {
            this.dsvalues[elem.id] = {};
        }
        this.dsvalues[elem.id][value] = text;
        break;
    }
  },

  clearSaveMsg: function() {
    if (this.saveMsg) this.saveMsg.innerHTML="";
  },

  addMenuItem: function(menuText,menuAction,enabled) {
    this.extraMenuItems.push({menuText:menuText,menuAction:menuAction,enabled:enabled});
  },

  editMenu: function(grid,r,c,onBlankRow) {
      var recordId,
          scripturl = window.location.href.split('?'),
          locked = 'N',
          xmlhttp,
          CCS2TableName = jQuery('.code_field_wrapper > select').attr('id'),
          showAddDeleteEntries = true;

      if (!CCS2TableName && typeof CCS2CodeTables !== 'undefined')
      {
          jQuery.each(CCS2CodeTables, function(index, value) {
              if (jQuery('#' + value + '_editDiv').length)
              {
                  CCS2TableName = jQuery('#' + value + '_editDiv').attr('id');
                  return false;
              }
          });
      }

      if(CCS2TableName)
      {
          aux = CCS2TableName.split('_');
          // Disable add and delete record entries from context menu for CCS2 fields
          CCS2TableName = aux[0] + '_' + aux[1] + '_' + aux[2] + '_' + aux[3];

          if (jQuery.inArray(CCS2TableName, CCS2CodeTables) > -1)
          {
              showAddDeleteEntries = false;
          }
      }
      else
      {
          CCS2TableName = '';
      }

      if (document.getElementById('combo_links_' + grid.menuIdx.row + '_0'))
      {
          recordId = document.getElementById('combo_links_' + grid.menuIdx.row + '_0').innerHTML;
      }

      if (!isNaN(parseInt(recordId, 10)))
      {
          if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp = new XMLHttpRequest();
          }
          else
          {// code for IE6, IE5
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }

          xmlhttp.onreadystatechange = function()
          {
              if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
              {
                  locked = xmlhttp.responseText;
              }
          }

          xmlhttp.open("GET", scripturl[0] + '?action=iscombolinklocked&recordid=' + recordId, false);
          xmlhttp.send();
      }

      this.clearSaveMsg();

      if (this.grid.buffer.sessionExpired == true || this.grid.buffer.startPos < 0)
      {
          return false;
      }

      if (locked != 'Y')
      {
          var elemTitle = $('pageTitle'),
              pageTitle = elemTitle ? elemTitle.innerHTML : document.title,
              menutxt;

          this.rowIdx = r;
          this.menu.addMenuHeading(pageTitle);

          for (var i = 0; i < this.extraMenuItems.length; i++)
          {
              this.menu.addMenuItem(this.extraMenuItems[i].menuText,this.extraMenuItems[i].menuAction.bindAsEventListener(this),this.extraMenuItems[i].enabled);
          }

          if (onBlankRow == false)
          {
              menutxt = RicoTranslate.getPhraseById("editRecord",this.options.RecordName);
              this.menu.addMenuItem(menutxt,this.editRecord.bindAsEventListener(this),this.options.canEdit);

              if (showAddDeleteEntries)
              {
                  menutxt = RicoTranslate.getPhraseById("deleteRecord",this.options.RecordName);
                  this.menu.addMenuItem(menutxt,this.deleteRecord.bindAsEventListener(this),this.options.canDelete);
              }

              if (this.options.canClone)
              {
                  menutxt = RicoTranslate.getPhraseById("cloneRecord",this.options.RecordName);
                  this.menu.addMenuItem(menutxt,this.cloneRecord.bindAsEventListener(this),this.options.canAdd && this.options.canEdit);
              }
          }

          if (showAddDeleteEntries)
          {
              menutxt = RicoTranslate.getPhraseById("addRecord",this.options.RecordName);
              this.menu.addMenuItem(menutxt,this.addRecord.bindAsEventListener(this),this.options.canAdd);
          }
      }

      return true;
  },

  cancelEdit: function(e) {
    Event.stop(e);
    for (var i=0; i<this.grid.columns.length; i++) {
      if (this.grid.columns[i].format && this.grid.columns[i].format.SelectCtl)
        RicoEditControls.close(this.grid.columns[i].format.SelectCtl);
    }
    this.makeFormInvisible();
    this.grid.highlightEnabled=true;
    this.menu.cancelmenu();
    return false;
  },

  setField: function(fldnum,fldvalue) {
    var fldSpec=this.grid.columns[fldnum].format;
    var e=$(fldSpec.FieldName);
    var a,i,elems,fldcode,opts,txt;
    if (!e) return;
    Rico.writeDebugMsg('setField: '+fldSpec.FieldName+'='+fldvalue);
    switch (e.tagName.toUpperCase()) {
      case 'DIV':
        elems=e.getElementsByTagName('INPUT');
        fldcode=this.getLookupValue(fldvalue)[0];
        for (i=0; i<elems.length; i++)
          elems[i].checked=(elems[i].value==fldcode);
        break;
      case 'INPUT':
        if (fldSpec.EntryType == 'DS')
        {
            var description = "";
            var code = "";

            if (fldvalue != "")
            {
                try
                {
                    code=this.getLookupValue(fldvalue)[0];
                    description=this.getLookupValue(fldvalue)[1];
                    //description = this.dsvalues[fldSpec.FieldName][fldvalue];
                }
                catch(e)
                {
                    description = fldvalue;
                }
            }

            //jQuery('#'+fldSpec.FieldName+'_title').data('datixselect').selectItem('<li id="'+fldvalue+'">'+description+'</li>');
            jQuery('#'+fldSpec.FieldName+'_title').selectItem('<li id="'+code+'">'+description+'</li>');
        }
        else
        {
            if (fldSpec.EntryType.charAt(1)=='L' || fldSpec.EntryType=='DS')
              fldvalue=this.getLookupValue(fldvalue)[0];
            if (fldSpec.EntryType=='D' && fldvalue != "") {
              // remove time data if it exists
              a=fldvalue.split(/\s|T/);
              a[0]=a[0].replace(/-/g, "/");
              temp_date = new Date(a[0]);
              fldvalue = temp_date.formatDate(RicoTranslate.dateFmt); 
              
            }
            if (fldSpec.EntryType=='CP') {
               if (fldvalue.length == 6 && fldvalue!='' && fldvalue>='000000' && fldvalue<='ffffff')
               {
                    e.style.backgroundColor='#'+fldvalue;
               }
               else
               {
                   e.style.backgroundColor='#ffffff';
               }
            }
            e.value=fldvalue;
            }
        break;
      case 'SELECT':
        if (fldSpec.EntryType=='MS' || fldSpec.EntryType=='MSE') {
           e.innerHTML='';
           if (fldvalue != ' ' && fldvalue != '')
           {
               var options_selected=fldvalue.split(' ');
               var innerHTML;
               
               for(i=0;i<options_selected.length;i++)
               {
                  option_value=document.createElement('option');
                  option_value.value=options_selected[i];
                  innerHTML = fldSpec.showNumbers ? (i+1)+": "+options_selected[i] : options_selected[i];
                  option_value.innerHTML=innerHTML;
                  e.appendChild(option_value);
               }
           }
        }
        else
        {
            opts=e.options;
            fldcode=this.getLookupValue(fldvalue)[0];

            //alert('setField SELECT: id='+e.id+'\nvalue='+fldcode+'\nopt cnt='+opts.length)
            for (i=0; i<opts.length; i++) {
              if (opts[i].value==fldcode) {
                e.selectedIndex=i;
                break;
              }
            }
        }
        if (fldSpec.EntryType=='N') {
          txt=$('textnew__'+e.id);
          if (!txt) alert('Warning: unable to find id "textnew__'+e.id+'"');
          txt.value=fldvalue;
          if (e.selectedIndex!=i) e.selectedIndex=opts.length-1;
          this.updateSelectNew(e);
        }
        return;
      case 'TEXTAREA':
        e.value=fldvalue;
        if (fldSpec.EntryType=='tinyMCE' && typeof(tinyMCE)!='undefined' && this.initialized)
          tinyMCE.updateContent(e.id);
        return;
    }
  },

  getLookupValue: function(value) {
    switch (typeof value) {
      case 'number': return [value.toString(),value.toString()];
      case 'string': return value.match(/<span\s+class=(['"]?)ricolookup\1>(.*)<\/span>/i) ? [RegExp.$2,RegExp.leftContext] : [value,value];
      default:       return ['',''];
    }
  },

  // use with care: Prototype 1.5 does not include disabled fields in the post-back
  setReadOnly: function(addFlag) {
    for (var i=0; i<this.grid.columns.length; i++) {
      var fldSpec=this.grid.columns[i].format;
      if (!fldSpec) continue;
      var e=$(fldSpec.FieldName);
      if (!e) continue;
      var ro=!fldSpec.Writeable || fldSpec.ReadOnly || (fldSpec.InsertOnly && !addFlag) || (fldSpec.UpdateOnly && addFlag);
      var color=ro ? this.options.readOnlyColor : '';
      switch (e.tagName.toUpperCase()) {
        case 'DIV':
          var elems=e.getElementsByTagName('INPUT');
          for (var j=0; j<elems.length; j++)
            elems[j].disabled=ro;
          break;
        case 'SELECT':
          if (fldSpec.EntryType=='N') {
            var txt=$('textnew__'+e.id);
            txt.disabled=ro;
          }
          e.disabled=ro;
          break;
        case 'TEXTAREA':
        case 'INPUT':
          e.readOnly=ro;
          e.style.color=color;
          if (fldSpec.selectIcon) fldSpec.selectIcon.style.display=ro ? 'none' : '';
          break;
      }
    }
  },

  hideResponse: function(msg) {
    this.responseDiv.innerHTML=msg;
    this.responseDialog.style.display='none';
  },

  showResponse: function() {
    var offset=Position.page(this.grid.outerDiv);
    offset[1]+=RicoUtil.docScrollTop();
    this.responseDialog.style.top=offset[1]+"px";
    this.responseDialog.style.left=offset[0]+"px";
    this.responseDialog.style.display='';
  },

  processResponse: function() {
    var responseText,success=true;
    var respNodes=Element.select(this.responseDiv,'.ricoFormResponse');
    if (respNodes) {
      // generate a translated response
      var phraseId=$w(respNodes[0].className)[1];
      responseText=RicoTranslate.getPhraseById(phraseId,this.options.RecordName);
    } else {
      // present the response as sent from the server (untranslated)
      var ch=this.responseDiv.childNodes;
      for (var i=ch.length-1; i>=0; i--) {
        if (ch[i].nodeType==1 && ch[i].nodeName!='P' && ch[i].nodeName!='DIV' && ch[i].nodeName!='BR')
          this.responseDiv.removeChild(ch[i]);
      }
      responseText=this.responseDiv.innerHTML.stripTags();
      success=(responseText.toLowerCase().indexOf('error')==-1);
    }
    if (success && this.options.showSaveMsg!='full') {
      this.hideResponse('');
      this.grid.resetContents();
      this.grid.buffer.foundRowCount = false;
      this.grid.buffer.fetch(this.grid.lastRowPos || 0);
      if (this.saveMsg) this.saveMsg.innerHTML='&nbsp;'+responseText+'&nbsp;';
    }
    this.processCallback(this.options.onSubmitResponse);
  },

  processCallback: function(callback) {
    switch (typeof callback) {
      case 'string': eval(callback); break;
      case 'function': callback(); break;
    }
  },

  // called when ok pressed on error response message
  ackResponse: function() {
    this.hideResponse('');
    this.grid.highlightEnabled=true;
  },

  cloneRecord: function() {
    this.displayEditForm("ins");
  },

  editRecord: function() {
    this.displayEditForm("upd");
  },

  displayEditForm: function(action) {
    this.grid.highlightEnabled=false;
    this.menu.cancelmenu();
    this.hideResponse(RicoTranslate.getPhraseById('saving'));
    this.grid.outerDiv.style.cursor = 'auto';
    this.action.value=action;
    for (var i=0; i<this.grid.columns.length; i++) {
      if (this.grid.columns[i].format) {
        var c=this.grid.columns[i];
        var v=c.getValue(this.rowIdx);
        if (c.format.type == 'date') 
        {
         //   v=v.replace(/-/g, "/");
        //    temp_date = new Date(v);
         //   v = temp_date.formatDate(RicoTranslate.dateFmt); 
            //c.dataCell.innerHTML=temp_date.formatDate(RicoTranslate.dateFmt);
            //c.format.selectDesc.innerHTML=temp_date.formatDate(RicoTranslate.dateFmt); 
        }
        this.setField(i,v);
        if (c.format.selectDesc)
          c.format.selectDesc.innerHTML=c._format(v);
        if (c.format.SelectCtl)
          RicoEditControls.displayClrImg(c, !c.format.InsertOnly);
        
      }
    }
    this.setReadOnly(false);
    this.key=this.getKey(this.rowIdx);
    this.makeFormVisible(this.rowIdx);
    this.originalKeyValues=this.getKeyValues(this.rowIdx);
  },

  addPrepare: function() {
    this.hideResponse(RicoTranslate.getPhraseById('saving'));
    this.setReadOnly(true);
    this.form.reset();
    this.action.value="ins";
    for (var i=0; i<this.grid.columns.length; i++) {
      if (this.grid.columns[i].format) {
        this.setField(i,this.grid.columns[i].format.ColData);
        if (this.grid.columns[i].format.SelectCtl)
          RicoEditControls.resetValue(this.grid.columns[i]);
      }
    }
    this.key='';
  },

  addRecord: function() {
    if (this.grid.filterCount() > 0)
    {
        this.grid.clearFilters();
    }
    this.menu.cancelmenu();
    this.addPrepare();
    this.makeFormVisible(-1);
    if (this.Accordion) this.Accordion.selectionSet.selectIndex(0);
  },

  drillDown: function(e,masterColNum,detailColNum) {
    var cell=Event.element(e || window.event);
    cell=RicoUtil.getParentByTagName(cell,'div','ricoLG_cell');
    if (!cell) return;
    this.grid.unhighlight();
    var idx=this.grid.winCellIndex(cell);
    this.grid.menuIdx=idx;  // ensures selection gets cleared when menu is displayed
    this.grid.highlight(idx);
    var drillValue=this.grid.columns[masterColNum].getValue(idx.row);
    for (var i=3; i<arguments.length; i++)
      arguments[i].setDetailFilter(detailColNum,drillValue);
    return idx.row;
  },

  // set filter on a detail grid that is in a master-detail relationship
  setDetailFilter: function(colNumber,filterValue) {
    this.grid.setDetailFilter(colNumber,filterValue);
  },

  makeFormVisible: function(row) {
    var margin=8;  // account for shadow
    this.editDiv.style.display='block';

    // set left position
    var editWi=this.editDiv.offsetWidth+margin;
    var odOffset=Position.page(this.grid.outerDiv);
    var winWi=RicoUtil.windowWidth();
    if (editWi+odOffset[0] > winWi)
      this.editDiv.style.left=(winWi-editWi)+'px';
    else
      this.editDiv.style.left=(odOffset[0]+1)+'px';

    // set top position
    var scrTop=RicoUtil.docScrollTop();
    var editHt=this.editDiv.offsetHeight+margin;
    var newTop=odOffset[1]+this.grid.hdrHt+scrTop;
    var bottom=RicoUtil.windowHeight()+scrTop;
    if (row >= 0) {
      newTop+=(row+1)*this.grid.rowHeight;
      if (newTop+editHt>bottom) newTop-=(editHt+this.grid.rowHeight);
    } else {
      if (newTop+editHt>bottom) newTop=bottom-editHt;
    }

      // Disable multiselect add and delete buttons for CCS2 fields
      var CCS2TableName = jQuery('.code_field_wrapper > select').attr('id');
      if(CCS2TableName)
      {
        var aux = CCS2TableName.split('_');
        CCS2TableName = aux[0] + '_' + aux[1] + '_' + aux[2] + '_' + aux[3];

          if (jQuery.inArray(CCS2TableName, CCS2CodeTables) > -1)
          {
              jQuery('.multilistbox_button_wrapper > div > a').click(function(e) {
                  e.preventDefault();
              });
          }
      }

    this.processCallback(this.options.formOpen);
    this.formPopup.openPopup(null,Math.max(newTop,scrTop));
    this.editDiv.style.visibility='visible';
    if (this.initialized) return;

    var i, spec;
    for (i = 0; i < this.grid.columns.length; i++) {
      spec=this.grid.columns[i].format;
      if (!spec || !spec.EntryType || !spec.FieldName) continue;
      switch (spec.EntryType) {
        case 'tinyMCE':
          if (typeof tinyMCE!='undefined') tinyMCE.execCommand('mceAddControl', true, spec.FieldName);
          break;
      }
    }

    if (!this.panelGroup) {
      this.editDiv.style.width=(this.editDiv.offsetWidth-this.grid.options.scrollBarWidth+2)+"px";
      this.editDiv.style.height=(this.editDiv.offsetHeight-this.grid.options.scrollBarWidth+2)+"px";
    }

    this.formPopup.openPopup();  // tinyMCE may have changed the dimensions of the form
    this.initialized=true;
  },

  makeFormInvisible: function() {
    this.editDiv.style.visibility='hidden';
    this.formPopup.closePopup();
    this.processCallback(this.options.formClose);
  },

  getConfirmDesc: function(rowIdx) {
    var desc=this.grid.columns[this.options.ConfirmDeleteCol].cell(rowIdx).innerHTML;
    desc=this.getLookupValue(desc)[1];
    return desc.stripTags().unescapeHTML();
  },

  deleteRecord: function() {
    this.menu.cancelmenu();
    var desc;
    switch(this.options.ConfirmDeleteCol){
			case -1 :
			  desc=RicoTranslate.getPhraseById("thisRecord",this.options.RecordName);
			  break;
			case -2 : // Use key/column header to identify the row
        for (var k=0; k<this.keys.length; k++) {
          var i=this.keys[k];
          var value=this.grid.columns[i].getValue(this.rowIdx);
          value=this.getLookupValue(value)[0];
  				if (desc) desc+=', ';
  				desc+=this.grid.columns[i].displayName+" "+value;
        }
				break;
			default   :
				desc='\"' + this.getConfirmDesc(this.rowIdx).truncate(50) + '\"';
        break;
    }
    if (!this.options.ConfirmDelete.valueOf || confirm(RicoTranslate.getPhraseById("confirmDelete",desc))) {
      this.hideResponse(RicoTranslate.getPhraseById('deleting'));
      this.showResponse();
      var parms=this.action.name+"=del"+this.getKey(this.rowIdx);
      new Ajax.Updater(this.responseDiv, this.options.updateURL, {parameters:parms,onComplete:this.processResponse.bind(this)});
    }
    this.menu.cancelmenu();
  },

  getKey: function(rowIdx) {
    var keyHash=$H();
    for (var k=0; k<this.keys.length; k++) {
      var i=this.keys[k];
      var value=this.grid.columns[i].getValue(rowIdx);
      value=this.getLookupValue(value)[0];
      keyHash.set('_k'+i,value);
    }
    return '&'+keyHash.toQueryString();
  },

  getKeyValues: function(rowIdx) {
    var keyHash=$H();
    var keyArray = new Array(2);
    for (var k=0; k<this.keys.length; k++) {
      var i=this.keys[k];
      var value=this.grid.columns[i].getValue(rowIdx);
      value=this.getLookupValue(value)[0];
      keyArray[i]=value;
      //keyHash.set('_k'+i,value);
    }
    //return '&'+keyHash.toQueryString();
    return keyArray;
  },

  validationMsg: function(elem,colnum,phraseId) {
    var col=this.grid.columns[colnum];
    if (this.Accordion) this.Accordion.openByIndex(col.format.panelIdx);
    var msg=RicoTranslate.getPhraseById(phraseId," \"" + col.formLabel.innerHTML + "\"");
    Rico.writeDebugMsg(' Validation error: '+msg);
    if (col.format.Help) msg+="\n\n"+col.format.Help;
    alert(msg);
    setTimeout(function() { try { elem.focus(); elem.select(); } catch(e) {}; }, 10);
    return false;
  },

  TESubmit: function(e) {
    var i,lbl,spec,elem,n;

    Event.stop(e || event);
    Rico.writeDebugMsg('Event: TESubmit called to validate input');

    for (i = 0; i < this.grid.columns.length; i++) {
      spec=this.grid.columns[i].format;
      if (!spec || !spec.EntryType || !spec.FieldName) continue;
      elem=$(spec.FieldName);
      if (!elem) continue;
      if (elem.tagName.toLowerCase()=='select') {
          if (elem.type=='select-multiple')
          {
            selectAllMultiple(elem);
            for (index=0;index < elem.options.length; index++)
            {
                elem.value[index]=elem.value +' '+elem.options[index].value;
            }
            continue;
          }
      }

      if (spec.EntryType=='ST')
      {
          if(!(!isNaN(parseFloat(elem.value)) && isFinite(elem.value)) || parseInt(elem.value.substr(0,2)) > 23 || parseInt(elem.value.substr(2,2)) > 59)
          {
            return this.validationMsg(elem,i,"timeInvalid");
          }
      }

      Rico.writeDebugMsg(' Validating field #'+i+' EntryType='+spec.EntryType+' ('+spec.FieldName+')');

      // check for blanks
      if (elem.value.length == 0 && spec.required)
        return this.validationMsg(elem,i,"formPleaseEnter");

      if (elem.tagName.toLowerCase()!='input') continue;
      if (elem.type.toLowerCase()!='text') continue;


      // check for duplicates
      if (spec.unique == true) //Only check when inserting
      {
          if (this.action.value == 'ins' || (this.action.value == 'upd' && elem.value.toUpperCase() != this.originalKeyValues[i].toUpperCase()))
          {
              var duplicate = false;

              if (this.grid.buffer.totalRows > this.grid.buffer.rows.length)
              {
                  // there are more records in the code table than stored in the buffer, so do the duplicate check via an Ajax call
                  jQuery.ajax(
                  {
                      url: scripturl + "?action=getcodes",
                      type: "POST",
                      dataType: "json",
                      data: {field:getQueryStringParm("codefieldname"), module:getQueryStringParm("module")},
                      global: false,
                      async: false,
                      success: function(data)
                      {
                          jQuery.each(data, function()
                          {
                              if (this["value"].toUpperCase() == elem.value.toUpperCase())
                              {
                                  duplicate = true;
                                  return false
                              }
                          });
                      },
                      error: function()
                      {
                          return this.validationMsg(elem,i,"duplicateCheckError");
                      }
                  });
              }
              else
              {
                  for (index=0;index < this.grid.buffer.totalRows; index++)
                  {
                      if (elem.value.toUpperCase() == this.grid.buffer.rows[index][i].toUpperCase())
                      {
                          duplicate = true;
                          break;
                      }
                  }
              }

              if (duplicate)
              {
                  return this.validationMsg(elem,i,"formDuplicate");
              }
          }
      }
      // check pattern
      if (elem.value.length > 0 && spec.regexp && !spec.regexp.test(elem.value))
        return this.validationMsg(elem,i,"formInvalidFmt");

      // check min/max
      switch (spec.EntryType.charAt(0)) {
        case 'I': n=parseInt(elem.value,10); break;
        case 'F': n=parseFloat(elem.value); break;
        case 'D': n=new Date(); n.setISO8601(elem.value); break;
        default:  n=NaN; break;
      }
      if (typeof spec.min!='undefined' && !isNaN(n) && n < spec.min)
        return this.validationMsg(elem,i,"formOutOfRange");
      if (typeof spec.max!='undefined' && !isNaN(n) && n > spec.max)
        return this.validationMsg(elem,i,"formOutOfRange");
    }

    // update drop-down for any columns with entry type of N

    for (i = 0; i < this.grid.columns.length; i++) {
      spec=this.grid.columns[i].format;
      if (!spec || !spec.EntryType || !spec.FieldName) continue;
      if (spec.EntryType.charAt(0) != 'N') continue;
      var SelObj=$(spec.FieldName);
      if (!SelObj || SelObj.value!=this.options.TableSelectNew) continue;
      var newtext=$("textnew__" + SelObj.id).value;
      this.addSelectOption(SelObj,newtext,newtext);
    }

    // handle any custom onSubmit callback function
    if (this.options.onSubmit)
    {
        var onSubmitError = eval(this.options.onSubmit);
        if (onSubmitError != "")
        {
            alert(onSubmitError);
            return false;
        }
    }

    if (typeof tinyMCE!='undefined') tinyMCE.triggerSave();
    this.makeFormInvisible();
    this.sendForm();
    this.menu.cancelmenu();
    return false;
  },

  sendForm: function() {
    this.showResponse();
    var parms=Form.serialize(this.form)+this.key;
    Rico.writeDebugMsg("sendForm: "+parms);
    new Ajax.Updater(this.responseDiv, this.options.updateURL, {parameters:parms,onComplete:this.responseHandler});
  }
});


/**
 * @namespace Registers custom popup widgets to fill in a text box (e.g. ricoCalendar and ricoTree)
 * <pre>
 * Custom widget must implement:
 *   open() method (make control visible)
 *   close() method (hide control)
 *   container property (div element that contains the control)
 *   id property (uniquely identifies the widget class)
 *
 * widget calls returnValue method to return a value to the caller
 *
 * this object handles clicks on the control's icon and positions the control appropriately.
 * </pre>
 */
var RicoEditControls = {
  widgetList : $H(),
  elemList   : $H(),
  clearImg   : Rico.imgDir+'delete.gif',

  register: function(widget, imgsrc) {
    this.widgetList.set(widget.id, {imgsrc:imgsrc, widget:widget, currentEl:''});
    widget.returnValue=this.setValue.bind(this,widget);
    Rico.writeDebugMsg("RicoEditControls.register:"+widget.id);
  },

  atLoad: function() {
    this.widgetList.each(function(pair) { if (pair.value.widget.atLoad) pair.value.widget.atLoad(); });
  },

  applyTo: function(column,inputCtl) {
    var wInfo=this.widgetList.get(column.format.SelectCtl);
    if (!wInfo) return;
    Rico.writeDebugMsg('RicoEditControls.applyTo: '+column.displayName+' : '+column.format.SelectCtl);
    var descSpan = document.createElement('span');
    var newimg = document.createElement('img');
    newimg.style.paddingLeft='4px';
    newimg.style.cursor='pointer';
    newimg.align='top';
    newimg.src=wInfo.imgsrc;
    newimg.id=this.imgId(column.format.FieldName);
    newimg.onclick=this.processClick.bindAsEventListener(this);
    inputCtl.parentNode.appendChild(descSpan);
    inputCtl.parentNode.appendChild(newimg);
    inputCtl.style.display='none';    // comment out this line for debugging
    if (column.format.isNullable) {
      var clrimg = document.createElement('img');
      clrimg.style.paddingLeft='4px';
      clrimg.style.cursor='pointer';
      clrimg.align='top';
      clrimg.src=this.clearImg;
      clrimg.id=newimg.id+'_clear';
      clrimg.alt=RicoTranslate.getPhraseById('clear');
      clrimg.onclick=this.processClear.bindAsEventListener(this);
      inputCtl.parentNode.appendChild(clrimg);
    }
    this.elemList.set(newimg.id, {descSpan:descSpan, inputCtl:inputCtl, widget:wInfo.widget, listObj:wInfo, column:column, clrimg:clrimg});
    column.format.selectIcon=newimg;
    column.format.selectDesc=descSpan;
  },

  displayClrImg: function(column,bool) {
    var el=this.elemList.get(this.imgId(column.format.FieldName));
    if (el && el.clrimg) el.clrimg.style.display=bool ? '' : 'none';
    if (column.format.colour==true)
    {
        el.descSpan.style.backgroundColor = el.inputCtl.value;
    }
  },

  processClear: function(e) {
    var elem=Event.element(e);
    var el=this.elemList.get(elem.id.slice(0,-6));
    if (!el) return;
    el.inputCtl.value='';
    el.descSpan.innerHTML=el.column._format('');
  },

  processClick: function(e) {
    var elem=Event.element(e);
    var el=this.elemList.get(elem.id);
    if (!el) return;
    if (el.listObj.currentEl==elem.id && el.widget.container.style.display!='none') {
      el.widget.close();
      el.listObj.currentEl='';
    } else {
      el.listObj.currentEl=elem.id;
      Rico.writeDebugMsg('RicoEditControls.processClick: '+el.widget.id+' : '+el.inputCtl.value);
      el.widget.open(el.inputCtl.value);     // this may change the size of the widget
      RicoUtil.positionCtlOverIcon(el.widget.container,elem);
      if (el.widget.move) el.widget.move();  // if widget is a Rico.Popup object, ensure shim and shadow follow
    }
  },

  imgId: function(fieldname) {
    return 'icon_'+fieldname;
  },

  resetValue: function(column) {
    var el=this.elemList.get(this.imgId(column.format.FieldName));
    if (!el) return;
    el.inputCtl.value=column.format.ColData;
    el.descSpan.innerHTML=column._format(column.format.ColData);
  },

  setValue: function(widget,newVal,newDesc) {
    var wInfo=this.widgetList.get(widget.id);
    if (!wInfo) return null;
    var id=wInfo.currentEl;
    if (!id) return null;
    var el=this.elemList.get(id);
    if (!el) return null;
    el.inputCtl.value=newVal;
    if (!newDesc) newDesc=el.column._format(newVal);
    el.descSpan.innerHTML=newDesc;
    if (el.column.format.colour==true)
    {
        el.descSpan.style.backgroundColor = newVal;
    }
    //alert(widget.id+':'+id+':'+el.inputCtl.id+':'+el.inputCtl.value+':'+newDesc);
  },

  close: function(id) {
    var wInfo=this.widgetList.get(id);
    if (!wInfo) return;
    if (wInfo.widget.container.style.display!='none')
      wInfo.widget.close();
  }
};

Rico.includeLoaded('ricoLiveGridForms.js');
