<?php
set_include_path('../');
if (!isset ($_SESSION)) session_start();
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Expires: ".gmdate("D, d M Y H:i:s",time()+(-1*60))." GMT");
header("Content-type: text/xml; charset=UTF-8");
echo "<?xml version='1.0' encoding='UTF-8'?".">\n";

// including this to prevent errors when including Logger class constants in DATIXConfig.php
spl_autoload_register('__autoload');

require_once "client/DATIXConfig.php";
require_once "rico/applib.php";
require_once "rico/plugins/php/ricoXmlResponse.php";
require_once "Source/classes/UnicodeStringClass.php";

$id=isset($_GET["id"]) ? $_GET["id"] : "";
$offset=isset($_GET["offset"]) ? $_GET["offset"] : "0";
$size=isset($_GET["page_size"]) ? $_GET["page_size"] : "";
$total=isset($_GET["get_total"]) ? strtolower($_GET["get_total"]) : "false";
$distinct=isset($_GET["distinct"]) ? $_GET["distinct"] : "";

echo "\n<ajax-response><response type='object' id='".$id."_updater'>";
if (empty($id)) {
  ErrorResponse("No ID provided!");
} elseif ($distinct=="" && !is_numeric($offset)) {
  ErrorResponse("Invalid offset!");
} elseif ($distinct=="" && !is_numeric($size)) {
  ErrorResponse("Invalid size!");
} elseif ($distinct!="" && !is_numeric($distinct)) {
  ErrorResponse("Invalid distinct parameter!");
} elseif (!isset($_SESSION[$id])) {
  ErrorResponse("Your connection with the server was idle for too long and timed out. Please refresh this page and try again.");
} elseif (!OpenDB()) {
  ErrorResponse(htmlspecialchars($oDB->LastErrorMsg));
} else {
  $filters=isset($_SESSION[$id . ".filters"]) ? $_SESSION[$id . ".filters"] : array();
  $oDB->DisplayErrors=false;
  $oDB->ErrMsgFmt="MULTILINE";
  $oXmlResp= new ricoXmlResponse();
  $oXmlResp->sendDebugMsgs=true;
  if ($distinct=="") {
    $oXmlResp->Query2xml($_SESSION[$id], intval($offset), intval($size), $total!="false", $filters);
  } else {
    $oXmlResp->Query2xmlDistinct($_SESSION[$id], intval($distinct), -1, $filters);
  }
  if (!empty($oDB->LastErrorMsg)) {
    echo "\n<error>";
    echo "\n".htmlspecialchars($oDB->LastErrorMsg);
    echo "\n</error>";
  }
  $oXmlResp=NULL;
  CloseApp();
}
echo "\n</response></ajax-response>";


function ErrorResponse($msg) {
  echo "\n<rows update_ui='false' /><error>" . $msg . "</error>";
}

function __autoload($class_name)
{
    if (substr($class_name, -9) == 'Exception' && file_exists('../Source/classes/Exceptions/'. $class_name . '.php'))
    {
        $fileToIncl = '../Source/classes/Exceptions/'. $class_name . '.php';
    }
    elseif (substr($class_name, 0, 5) == 'Zend_')
    {
        $fileToIncl = '../'.str_replace('_', '/', $class_name) . '.php';
    }
    elseif (substr($class_name, 0, 8) == 'Symfony\\' || substr($class_name, 0, 8) == 'Monolog\\' || substr($class_name, 0, 4) == 'Psr\\')
    {
        $fileToIncl = '../thirdpartylibs/' . str_replace('\\', '/', $class_name)  . '.php';
    }
    elseif (strpos($class_name, '\\') !== false)
    {
        $fileToIncl = '../'.str_replace('\\', '/', $class_name)  . '.php';
    }
    else
    {
        $fileToIncl = '../Source/classes/'. str_replace('_', '/', $class_name) . 'Class.php';
    }

    if (file_exists($fileToIncl))
    {
        require_once $fileToIncl;
    }
}