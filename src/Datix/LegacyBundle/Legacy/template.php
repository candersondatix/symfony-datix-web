<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <?php
    if(preg_match('/msie *(8)/i', $_SERVER['HTTP_USER_AGENT']) == 1 && $_GET['print'] == 1)
    {
        ?><meta http-equiv="X-UA-Compatible" content="IE=7" /><?php
    }
    else
    {
        ?><meta http-equiv="X-UA-Compatible" content="IE=edge" /><?php
    }
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <datix _system_css>

    <datix _system_js_top>

    <datix timeouthead>


    <title>Datix: <datix title></title>
</head>
<body <datix onload>>

    <datix header_js>

    <datix _system_click_block_div>

    <datix _system_banner_error_div>

    <datix side_panel>

    <div id="wrapper">
        <?php
        $registry = src\framework\registry\Registry::getInstance();

        if($registry->getParm('ENHANCED_ACCESSIBILITY', 'N', false, true))
        {
            ?><a class="skip-nav" href="#B" tabindex="1">Skip main navigation</a><?php
        }
        ?>
        <div id="H">
            <datix _system_accessibility_anchor><!--for screen-readers-->
            <div id="upper-nav">
                <div class="logo"><datix logo_topright></div><?php //client logo, or default datix logo ?>
                <div class="submenu" id="sub-menu">
                    <datix sub_menu>
                    <div style="clear: left;"></div>
                </div><?php //menu items at the top of the header bar ?>
                <div class="clearfix"></div>
            </div>
            <div id="lower-nav">
                <datix uname>
                <datix menu> <!--menu items at the bottom of the header bar-->
                <div class="clearfix"></div>
            </div>
        </div><!--H-->

        <datix wait>
        <div id="B" name="B" tabindex="-1">
            <div class="row">
                <div class="canvasA">
                    <div class="canvasB" id="datix-content">
                        <datix heading>
                        <div class="col menu-col" style="width:201px; z-index:2;">
                            <datix left_menu>
                        </div><!--menu-col-->
                        <div class="col form-wrapper">
                            <div class="content" id="main-content" tabindex="-1">
                                <div class="fieldset">
                                    <?php if($_GET['print'] == 1)
                                    {
                                        ?><table id="print-table" width="100%" cellpadding="0" cellspacing="0" border=0>
                                            <tbody><tr><td class="no-border">
                                                <datix main>
                                            </td></tr></tbody>
                                            <tfoot><tr><td class="no-border"><div id="print-footer">
                                                <datix print_footer>
                                            </div></td></tr></tfoot>
                                        </table><?php
                                    }
                                    else
                                    {
                                    /**
                                     * Newline after <datix main> is required as otherwise the closeing </div> for .fieldset gets removed as well and the template breaks
                                     */
                                        ?>
                                        <datix main>
                                        <?php
                                    }
                                    ?>
                                </div><!--fieldset-->
                            </div><!--content-->
                        </div><!--main-col-->
                    <div class="clearfix"></div>
                    </div><!--canvasB-->
                </div><!--canvasA-->
            </div><!--row-->
        </div><!--B-->

        <div style="text-align:right"><datix VBStyleLogin></div>

        <div id="F">
            <div class="content">
                <div class="copyright"><datix copyright></div>
                <div class="logo"><img class="colour-logo" src="images/datix-logo.gif" alt="Datix Logo" /><img class="grey-logo" src="images/datix_gray-logo.gif" alt="Datix Logo" style="display:none;" /></div>
                <?php if($_GET['print'] != 1)
                {
                    ?><div class="screen-footer"><datix screen_footer></div><?php
                }
                ?>
            </div>
        </div><!--F-->

    </div><!--wrapper-->

    <datix _system_js_bottom>

    <datix _system_click_block_js>

    <datix extra_javascript>

    <!-- Some icons used on this applications are from http://www.famfamfam.com/lab/icons/silk/ -->

</body>
</html>