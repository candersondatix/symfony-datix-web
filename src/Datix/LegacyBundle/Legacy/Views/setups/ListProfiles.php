<div class="new_titlebg">
    <div class="title_text_wrapper"><b>Import Profiles</b></div>
</div><!-- End of .new_titlebg div -->
<?php foreach ($this->Profiles as $Module => $Details): ?>
<div class="panel" style="display:<?php echo ($Module == $this->CurrentModule ? 'block' : 'none') ?>" id="panel-<?php echo $Module ?>">
    <div class="padded_div">
            <?php echo $Details['listing']->GetListingHTML(); ?>
    </div>
    <div><a href="<?php echo $this->scripturl ?>?action=editimportprofile&amp;module=<?php echo $Module ?>">Create a new import profile</a></div>
</div>
<?php endforeach ?>
