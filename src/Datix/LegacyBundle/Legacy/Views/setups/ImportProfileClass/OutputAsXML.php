    <TABLE name="excel_profile" recordid_req="1" module="ADM" recordid_col="exp_profile_id">
        <RECORD>
            <COLUMN name="exp_profile_name" value="<?= $this->ImportProfile->GetName() ?>"/>
            <COLUMN name="exp_profile_form" value="<?= $this->ImportProfile->GetForm() ?>"/>
            <COLUMN name="exp_start_row" value="<?= $this->ImportProfile->GetStartAt() ?>"/>
            <COLUMN name="exp_profile_type" value="<?= $this->ImportProfile->GetType() ?>"/>
            <COLUMN name="exp_profile_rowheader" value="<?= $this->ImportProfile->GetRowHeader() ?>"/>
<?php if($this->ImportProfile->getColumnConfig()): ?>
            <LINKTABLE name="excel_import" recordid_req="1" module="ADM" parent_link_field="exc_profile" parent_notkeycolumn="1" always_insert="1">
 <?php foreach($this->ImportProfile->getColumnConfig() as $ImportProfileColumn): ?>
                <RECORD>
                    <COLUMN name="exc_col_letter" value="<?= $ImportProfileColumn->Data['exc_col_letter'] ?>"/>
                    <COLUMN name="exc_dest_field" value="<?= $ImportProfileColumn->Data['exc_dest_field'] ?>"/>
                    <COLUMN name="exc_id_field" value="<?= $ImportProfileColumn->Data['exc_id_field'] ?>"/>
                    <COLUMN name="exc_import_order" value="<?= $ImportProfileColumn->Data['exc_import_order'] ?>"/>
                    <COLUMN name="exc_ignore_nulls" value="<?= $ImportProfileColumn->Data['exc_ignore_nulls'] ?>"/>
                    <COLUMN name="exc_form" value="<?= $this->ImportProfile->GetForm() ?>"/>
<?php if(is_array($ImportProfileColumn->MappingData)): ?>
                    <LINKTABLE name="excel_data_trans" recordid_req="1" module="ADM" parent_link_field="edt_import_field_id" always_insert="1">
<?php foreach($ImportProfileColumn->MappingData as $MappingData): ?>
                        <RECORD>
                            <COLUMN name="edt_old_string_val" value="<?= $MappingData['edt_old_string_val'] ?>"/>
                            <COLUMN name="edt_new_string_val" value="<?= $MappingData['edt_new_string_val'] ?>"/>
                            <COLUMN name="edt_old_number_val" value="<?= $MappingData['edt_old_number_val'] ?>"/>
                            <COLUMN name="edt_new_number_val" value="<?= $MappingData['edt_new_number_val'] ?>"/>
                            <COLUMN name="edt_old_date_val" value="<?= $MappingData['edt_old_date_val'] ?>"/>
                            <COLUMN name="edt_new_date_val" value="<?= $MappingData['edt_new_date_val'] ?>"/>
                            <COLUMN name="edt_form" value="<?= $this->ImportProfile->GetForm() ?>"/>
                            <COLUMN name="edt_field_name" value="<?= $ImportProfileColumn->Data['exc_dest_field'] ?>"/>
                            <COLUMN name="edt_profile_id" value="<?= $this->ImportProfile->getID() ?>"/>
                        </RECORD>
<?php endforeach; ?>
                    </LINKTABLE>
<?php endif; ?>
                </RECORD>
<?php endforeach; ?>
            </LINKTABLE>
<?php endif; ?>
        </RECORD>
    </TABLE>
