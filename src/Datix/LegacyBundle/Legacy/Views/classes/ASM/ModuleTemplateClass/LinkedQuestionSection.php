<li>
    <?php echo $this->ListingDisplay->GetListingHTML(); ?>
    <?php if (bYN(GetParm('ATM_MQ_TEMP', 'N')) && CanSeeModule('ATQ') && !$this->ReadOnly) : ?>
        <div class="new_link_row">
            <a href="javascript:if(CheckChange()) {SendTo('<?php echo $this->scripturl; ?>?action=addnew&module=ATQ&main_recordid=<?php echo $this->ID; ?>&level=2&link_module=ATM')}"><b>Link a new <?php echo $this->txt['ATQName']; ?></b></a>
        </div>
    <?php endif; ?>
</li>