<div class="panel">
    <div class="padded_div">
        <form id="asmtemplatetypeform" name="asmtemplatetypeform" action="<?php echo $this->scripturl ?>?action=asmselectlocations" method="post">
            <div style="clear:both; height:20px;">
                <div class="field_label" style="float:left; margin-right:10px;"><?php echo _tk('template_type') ?></div>
                <div style="float:left;"><?php echo $this->typeField->getField() ?></div>
            </div>    
        </form>
    </div>
    <div class="button_wrapper"><input type="button" value="<?php echo _tk('select_assessment_locations') ?>" onclick="jQuery('#asmtemplatetypeform').submit();"></div>
</div>