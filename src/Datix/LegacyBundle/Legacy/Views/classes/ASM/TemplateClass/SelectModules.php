<div class="panel">
    <div class="padded_div">
        <form id="modulesform" name="modulesform" action="<?php echo $this->scripturl ?>?action=asmcreateinstances" method="post">
            <input type="hidden" name="template_type" value="<?php echo $this->templateType ?>" />
            <input type="hidden" name="ati_cycle" value="<?php echo $this->cycleYear ?>" />
            <input type="hidden" name="due_date" value="<?php echo $this->dueDate ?>" />
            <?php echo _tk('selected_locations') ?>:
            <?php foreach ($this->Locations as $ID => $Name): ?>
            <div><?php echo $Name ?><input type="hidden" name="locations[]" value="<?php echo $ID ?>"></div>
            <?php endforeach ?>
            <br />
            <div id="selectallmodules" style="cursor:pointer"><a onclick="jQuery('.location_checkbox').attr('checked',true); jQuery('#selectallmodules').hide(); jQuery('#deselectallmodules').show();"><?php echo _tk('select_all') ?></a></div>
            <div id="deselectallmodules" style="display:none;cursor:pointer"><a onclick="jQuery('.location_checkbox').attr('checked',false); jQuery('#deselectallmodules').hide(); jQuery('#selectallmodules').show();"><?php echo _tk('deselect_all') ?></a></div>
            <?php if (empty($this->Modules)): ?>
            <div><?php echo _tk('no_modules_available') ?></div>
            <?php endif ?>
            <?php foreach ($this->Modules as $ID => $Name): ?>
            <div><input type="checkbox" class="location_checkbox" name="modules[]" value="<?php echo $ID ?>"> <?php echo $Name ?></div>
            <?php endforeach ?>
        </form>
    </div>
    <div class="button_wrapper">
        <?php if (!empty($this->Modules) && bYN(GetParm('ATM_CAI', 'N'))): ?>
        <input type="button" value="<?php echo _tk('generate_assessment_instances') ?>" onclick="jQuery('#modulesform').submit();">
        <?php endif ?>
        <input type="button" value="<?php echo _tk('btn_cancel') ?>" onclick="SendTo('<?php echo $this->scripturl ?>?action=asmselectlocations');">
    </div>
</div>
