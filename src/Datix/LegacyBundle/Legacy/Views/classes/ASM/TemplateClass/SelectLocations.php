<div class="panel">
    <div class="padded_div">
        <form id="asmlocationform" name="asmlocationform" action="<?php echo $this->scripturl ?>?action=asmselectmodules" method="post">
            <input type="hidden" name="template_type" value="<?php echo $this->templateType ?>" />
            <?php if (isset($this->cycleYearField)): ?>
            <div style="margin-bottom:10px;">
                <div class="field_label" style="float:left; margin-right: 5px;"><?php echo _tk('cycle_year') ?></div>
                <div><?php echo $this->cycleYearField->getField() ?></div>
            </div>
            <?php endif ?>
            <?php if (isset($this->dueDateField)): ?>
            <div>
                <div class="field_label" style="float: left; margin-right: 5px"><?php echo _tk('due_date') ?></div>
                <div><?php echo $this->dueDateField->Field ?></div>
            </div>
            <?php endif ?>
            <div id="selectalllocations" style="cursor:pointer"><a onclick="jQuery('.location_checkbox').attr('checked',true); jQuery('#selectalllocations').hide(); jQuery('#deselectalllocations').show();"><?php echo _tk('select_all') ?></a></div>
            <div id="deselectalllocations" style="display:none;cursor:pointer"><a onclick="jQuery('.location_checkbox').attr('checked',false); jQuery('#deselectalllocations').hide(); jQuery('#selectalllocations').show();"><?php echo _tk('deselect_all') ?></a></div>
            <?php foreach ($this->Locations as $loc_array): ?>
            <div><input type="checkbox" class="location_checkbox" name="locations[]" value="<?php echo $loc_array['recordid'] ?>" <?php if (is_array($_POST['locations']) && in_array($loc_array['recordid'], $_POST['locations'])): ?>checked<?php endif ?>> <?php echo $loc_array['loc_name']; ?></div>
            <?php endforeach ?>
        </form>
    </div>
    <div class="button_wrapper"><input type="button" value="<?php echo _tk('select_modules') ?>" onclick="jQuery('#asmlocationform').submit();"></div>
</div>