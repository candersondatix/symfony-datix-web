<div class="panel">
    <div class="padded_div">
        <?php if ($this->AllDuplicates): ?>
            All of the outcomes you have selected have been generated for the locations you have selected. Duplicates are not permitted, so no records will be generated.
        <?php else: ?>
            <form id="cqcoutcomeform" name="cqcoutcomeform" action="<?= $this->scripturl ?>?action=cqcgenerateoutcomes" method="post">
                <?php foreach ($this->Locations as $ID => $Name): ?>
                    <input type="hidden" name="locations[]" value="<?= $ID ?>">
                <?php endforeach ?>
                <?php foreach ($this->Outcomes as $ID => $Name): ?>
                    <input type="hidden" name="outcomes[]" value="<?= $ID ?>">
                <?php endforeach ?>
                <?php foreach ($this->Prompts as $PromptID): ?>
                    <input type="hidden" name="prompts[]" value="<?= $PromptID ?>">
                <?php endforeach ?>
                The following outcomes have already been created - duplicates of these will not be generated:
                <br>
                <?php foreach ($this->Duplicates as $ID => $Details): ?>
                <div><?= $this->Locations[$Details['cdo_location']] ?> - <?= $this->Outcomes[$Details['cqc_outcome_template_id']] ?><input type="hidden" name="duplicates[]" value="<?= $Details['cdo_location'] ?>|<?= $Details['cqc_outcome_template_id'] ?>"></div>
                <?php endforeach ?>
            </form>
        <?php endif ?>
    </div>
    <div class="button_wrapper">
        <?php if (!$this->AllDuplicates): ?>
            <input type="button" value="<?= _tk('btn_continue') ?>" onclick="jQuery('#cqcoutcomeform').submit();">
        <?php endif ?>
        <input type="button" value="<?= _tk('btn_cancel') ?>" onclick="SendTo('<?= $this->scripturl ?>?module=CQO');">
    </div>
</div>
