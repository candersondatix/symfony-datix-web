<form id="new-node">
    <ul>
        <li>
            <img class="mandatory_image" alt="Mandatory" src="images/Warning.gif">
	        <input type="hidden" id="level" value="">
        	<label class="field_label" for="name"><?php echo $this->txt['cqc_location_name'] ?></label><input type="text" id="name" maxlength="50">
        </li>
        <li>
        	<label class="field_label" for="parent"><?php echo $this->txt['cqc_location_parent'] ?></label>
        	<select id="parent"></select>
        </li>
	</ul>
</form>