<div id="location-hierarchy">
    <div id="location-tiers" class="tiers">
        <ul>
        <?php foreach($this->tiers as $tier): ?>
            <li>
                <span title="Double-click to edit"><?php echo $tier['lti_name'] ?></span>
                <a href="#" class="new-node">Add value</a>
                <?php if ($tier['deletable']): ?>
                    <a href="#" class="delete-tier">Delete tier</a>
                <?php endif ?>
            </li>
        <?php endforeach ?>
        </ul>
        <p>Outcomes can be applied only to locations in the lowest tier. Once outcomes have been applied to a location, no more tiers can be created.</p>
        <?php if ($this->outcomes_assigned == false): ?>
            <a href="#" class="new-tier">New tier</a>
        <?php endif ?> 
    </div>
    <div id="location-nodes" class="tree">
        <?php if (!empty($this->locations)): ?>
            <?php echo renderNestedList($this->locations) ?>
        <?php else: ?>
            <p>You currently have no CQC registered locations. Add a new tier to start adding locations.</p>
        <?php endif ?>
    </div>
    <div id="location-legend">
        <p>Locations with a <img class="attachment" src="Images/icons/icon_attachment_blk.gif" alt="paperclip" /> cannot be deleted because outcomes have been attached to them or a child location
    </div>
</div>
<script type="text/javascript" src="js_functions/jquery/jquery.editable-1.3.3<?php echo $this->addMinExtension; ?>.js"></script>
<script type="text/javascript" src="js_functions/LocationHierarchy<?php echo $this->addMinExtension; ?>.js"></script>
<script src="js_functions/jquery/jquery.qtip-1.0.0-rc3<?php echo $this->addMinExtension; ?>.js" type="text/javascript"></script>
<script src="js_functions/jquery/jquery.equalHeight<?php echo $this->addMinExtension; ?>.js" type="text/javascript"></script>
<script src="js_functions/jquery/jquery.datix.tree<?php echo $this->addMinExtension; ?>.js" type="text/javascript"></script>
<script type="text/javascript">
    LocationHierarchy.init(document.getElementById('location-hierarchy'), <?php echo $this->data ?>);
</script>
<?php

    function renderNestedList($nodes)
    {
        $html = '<ul>';
        
        foreach($nodes as $index => $node)
        {
            if (count($nodes) == 1)
            {
                $class = 'only-child';
            }
            else if (($index + 1) == count($nodes))
            {
                $class = 'last-child';
            }
            else if ($index == 0)
            {
                $class = 'first-child';
            }
            else
            {
                $class = '';
            }
            
            $html .= '<li id="location-' . $node['recordid'] . '" class="' . $class . '">
                        <div class="node">
                            <span class="name">' . htmlspecialchars($node['loc_name']) . '</span>
                            <div class="controls">
                                <a class="edit" href="#">Edit</a>' . ($node['deletable'] ? '<a class="delete" href="#">Delete</a>' : '') . '
                            </div>';
            
            if (!$node['deletable']) {
                $html .= '<img class="attachment" src="Images/icons/icon_attachment_blk.gif" alt="Location cannot be deleted because outcomes have been attached to it or one of its children" />';
            }                
            
            $html .= '</div>';
            
            if (count($node['children']) > 0) {
                $html .= renderNestedList($node['children']);
            }
            
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }

?>