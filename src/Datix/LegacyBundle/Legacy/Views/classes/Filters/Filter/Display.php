<script type="text/javascript">
/**
* Used to pass field values to functions which return values (codes) for those fields.
*
* @var Object
*/
var fieldValues = {};

/**
* Redirects to a blank filter (i.e. "list all").
*/
function resetFilter()
{
    jQuery.ajax({url:scripturl+'?action=httprequest&type=clearsessionrecordlist&module=<?php echo $this->module ?>'+getListingCheckboxStateString(), async:false});
    SendTo(scripturl + "?action=list&module=<?php echo $this->module ?>&listtype=all");
}

/**
* Removes "Add another" links from and adds "Delete" links to each previous layer when adding a new layer.
*/
function removeAddLinks()
{
    var last = jQuery("li[id^=filter_section_div_]").length;
    jQuery("li[id^=filter_section_div_]").each(function(i, layer)
    {
        if (i < last - 1)
        {
            jQuery(layer).find("a[href*=AddSectionToForm]").hide();
            jQuery(layer).find("a[href*=deleteLayer]").show();
        }
    });
}

/**
* Removes the selected layer form the filter.
*
* @param int id The layer ID.
*/
function deleteLayer(id)
{
    jQuery("#filter_section_div_"+id).remove();
    jQuery("#FilterButtonFilter").removeAttr('disabled');
}
</script>
<div class="listing_filter_container">
    <form name="filter_form" method="post" action="<?php echo $this->scripturl ?>?action=dofilter&<?php echo htmlspecialchars(getGetString(array('action'))) ?>">
        <input type="hidden" id="filter_max_suffix" name="filter_max_suffix" value="<?php echo $this->maxSuffix ?>" />
        <input type="hidden" id="module" name="module" value="<?php echo $this->module ?>" />
        <input type="hidden" id="side_menu_module" name="side_menu_module" value="<?php echo $this->sideMenuModule; ?>" />
        <ul style="_margin: 5px 0 10px 0">
    <?php foreach ($this->filterRows as $row_num => $row): ?>
            <li id="filter_section_div_<?php echo $row_num ?>">
                <?php echo $row ?>
            </li>
    <?php endforeach ?>
            <li style="margin-top:10px; _clear:both;">
                <div class="listing_filter_row"><input id="FilterButtonFilter" type="submit" value="<?php echo $this->filterButtonLabel; ?>" disabled="disabled"/></div>
                <div class="listing_filter_row" style="padding-top:4px;"><a href="javascript:resetFilter();"><?php echo $this->resetLinkLabel; ?></a></div>
                <?php if ($_GET['module'] != 'LOC'): ?>
                <div class="listing_filter_row" style="padding-top:4px;"><a id="save_link" href="<?php echo $this->scripturl ?>?action=savequery&module=<?php echo $this->module ?>&form_action=new"><?php echo $this->saveFilterLinkLabel; ?></a></div>
                <?php endif; ?>
            </li>
        </ul>
    </form>
</div>