<script type="text/javascript">
/**
* Removes the selected library link.
* 
* @param int linkId The id of the link.
* @param object button The button clicked.
*/
function CQCUnlink(linkId, button)
{
    if (confirm("<?php echo $this->unlinkConfirm ?>"))
    {
        displayLoadPopup();        
        jQuery.ajax(
        {
            url: scripturl+"?action=cqcunlink&linkid=" + linkId,
            type: "GET",
            global: false,
            success: function()
            {
                // remove table row
                jQuery(button).closest("tr").remove();
            },
            complete: function()
            {
                hideLoadPopup();    
            }
        });
    }    
}
</script>
<li><?php echo $this->listing ?></li>
<?php if ($this->canAdd): ?>
<li class="new_link_row">
    <a href="Javascript:if(CheckChange()){SendTo(scripturl+'?action=newcqclink&lib_id=<?php echo $this->libId ?>&callback=<?php echo $this->callback ?>');}"><b><?php echo $this->addLink ?></b></a>
</li>
<?php endif ?>