<form id="duplicate_search_form" method="post" action="<?= $this->scripturl ?>?service=duplicatesearch&event=performsearch">
    <div style="padding:5px;">Contact fields to include:</div>
    <div style="margin-left:20px">
        <?php foreach ($this->Fields as $Field): ?>
        <div><input type="checkbox" name="duplicate_fields[]" value="<?= $Field ?>" title="<?= Labels_FieldLabel::GetFieldLabel($Field) ?>" <?=in_array($Field, $this->SelectedFields) ? "checked" : ""?>/> <?= Labels_FieldLabel::GetFieldLabel($Field) ?> </div>
        <?php endforeach ?>
    </div>
    <div class="button_wrapper">
        <input type="button" value="Search" onclick="this.form.submit()" />
    </div>
</form>