<?php
namespace src\logger;

use Monolog\Logger;
use src\framework\registry\Registry;

class DatixLogger implements iLogger
{    
    /**
     * What type of log do we want to write to? (only FILE supported at the moment)
     * @var string
     */
    protected $method = 'FILE';
    
    /**
     * @var Logger
     */
    protected $logObject;

    /**
     * Log events associated with emails.
     */
    const EMAILS = 1;

    /**
     * Log events associated with reports.
     */
    const REPORTING = 2;

    /**
     * Log events associated with Word Merging.
     */
    const WORDMERGING = 3;

    /**
     * Log time taken for each request.
     */
    const REQUESTTIME = 4;

    /**
     * Log queries run for each request.
     */
    const QUERYSTRINGS = 5;
    
    /**
     * Log long-running queries.
     */
    const LONGQUERIES = 6;

    /**
     * Log CLI events.
     */
    const CLI = 7;

    /**
     * @var array
     * Provides categories to allow only certain types of action to be logged.
     */
    protected static $categories = array(
        DatixLogger::EMAILS => 'EMAILS',
        DatixLogger::REPORTING => 'REPORTING',
        DatixLogger::WORDMERGING => 'WORDMERGING',
        DatixLogger::REQUESTTIME => 'REQUESTTIME',
        DatixLogger::QUERYSTRINGS => 'QUERYSTRINGS',
        DatixLogger::LONGQUERIES => 'LONGQUERIES',
        DatixLogger::CLI => 'CLI'
    );

    /**
     * @var Set of categories to log for this request.
     */
    protected $categoryArray = array();

    /**
     * @var int The level set for logging on DatixConfig.php file.
     */
    protected $logLevel = Logger::EMERGENCY;

    public function __construct($name, $logLevel = null, $categoryArray = array(), $maxFiles = 10, Registry $registry = null)
    {
        $this->logLevel = $logLevel ?: Logger::EMERGENCY;
        $registry = $registry ?: Registry::getInstance();
        $maxFiles = (int) $maxFiles == 0 ? 10: $maxFiles;
        
        $this->logObject = new Logger($name);

        if (isset($category) && !isset(self::$categories[$category]))
        {
            throw new \InvalidParameterException('Invalid log category');
        }

        $this->categoryArray = $categoryArray;
        
        if ($this->method == 'FILE')
        {
            $this->logObject->pushHandler(new DatixHandler($registry->getClientFolder().'/datix.log', $maxFiles, $this->logLevel));
        }
        else
        {
            throw new \Exception('Storage method not defined');
        }
    }

    /*
     * Add a general log message.
     */
    public function log($level, $message, $context = array())
    {
        $this->logObject->log($level, $message, $context);
    }

    /*
     * Add a message with EMERGENCY priority level.
     * This will always be logged, even for clients not in debug mode.
     * Use to log information about crashes, exceptions etc.
     */
    public function logEmergency($message)
    {
        $this->logObject->addEmergency($message);
    }

    /*
     * Add a message with INFO priority level if in category EMAILSs.
     * Use to log information about emailing
     */
    public function logEmail($message)
    {
        if ($this->emailsToBeLogged())
        {
            return $this->logObject->addRecord(Logger::INFO, $message);
        }
    }

    /**
     * returns true if email logging is turned on. False otherwise.
     * @return bool
     */
    public function emailsToBeLogged()
    {
        return ($this->logLevel <= Logger::INFO && (empty($this->categoryArray) || in_array(self::EMAILS, $this->categoryArray)));
    }

    /*
     * Add a message with INFO priority level if in category REPORTING.
     * Use to log information about Reporting
     */
    public function logReporting($message)
    {
        if (empty($this->categoryArray) || in_array(self::REPORTING, $this->categoryArray))
        {
            return $this->logObject->addRecord(Logger::INFO, $message);
        }
    }

    /*
     * Add a message with INFO priority level if in category WORDMERGING.
     * Use to log information about Word Merging
     */
    public function logWordMerging($message)
    {
        if (empty($this->categoryArray) || in_array(self::WORDMERGING, $this->categoryArray))
        {
            return $this->logObject->addRecord(Logger::INFO, $message);
        }
    }

    /*
     * Add a message with INFO priority level if in category REQUESTTIME.
     * Use to log information about Word Merging
     */
    public function logRequestTime($message)
    {
        if (empty($this->categoryArray) || in_array(self::REQUESTTIME, $this->categoryArray))
        {
            return $this->logObject->addRecord(Logger::INFO, $message);
        }
    }

    /*
     * Add a message with DEBUG priority level.
     * Use to log information about completed processes
     */
    public function logDebug($message)
    {
        if ($GLOBALS['logLevel'] == 100)
        {
            $this->logObject->addDebug($message);
        }
    }

    /*
     * Add a message with INFO priority level.
     * Use to log information about completed processes
     */
    public function logInfo($message)
    {
        if ($GLOBALS['logLevel'] == 200)
        {
            $this->logObject->addInfo($message);
        }
    }

    /*
     * Add a message with INFO priority level if a particular category is being tracked.
     * Use to log information about completed processes
     */
    public function logInfoCategory($message, $category)
    {
        if (empty($this->categoryArray) || in_array($category, $this->categoryArray))
        {
            return $this->logObject->addRecord(Logger::INFO, $message);
        }
    }

    /**
     * Removes "xdebug_message" attributes, which are not appropriate for datix.log
     * @param array $trace
     * @return array
     */
    protected function removeXDebugMessages($trace)
    {
        foreach ($trace as $traceItem)
        {
            if ($traceItem['args'][0] instanceof \Exception && $traceItem['args'][0]->xdebug_message)
            {
                unset($traceItem['args'][0]->xdebug_message);
            }
        }

        return $trace;
    }

    /**
     * Keep track of the execution length of all scripts to assist with debugging performance problems
     */
    static function logScriptExecutionLength()
    {
        global $start_time, $query_counter;

        $finish_time = microtime(true);
        \src\framework\registry\Registry::getInstance()->getLogger()->logRequestTime('User='.$_SESSION['login'].',Request='.$_SERVER['REQUEST_URI'].',Start='.$start_time.',Finish='.microtime(true).',Length='.($finish_time-$start_time).',Queries='.$query_counter);
    }

    /**
     * returns true if email logging is turned on. False otherwise.
     * @return bool
     */
    public function cliToBeLogged()
    {
        return (empty($this->categoryArray) || in_array(self::CLI, $this->categoryArray));
    }
}
