<?php
namespace src\tasks\model;

use src\framework\model\RecordEntity;

/**
 * This entity models a task
 */
class Task extends RecordEntity
{
    protected $mod_id;

    protected $trg_message;
    
    protected $trg_expression;

    protected $lockedby;

    protected $trg_type;
    
    protected $trg_evt_code;
    
    protected $trg_query;
    
    protected $trg_link_mod_id;
    
    protected $trg_link_cas_id;
    
    protected $trg_tem_id;
    
    protected $trg_con_id;
    
    protected $trg_doc_edit;
    
    protected $trg_email_subject;
    
    protected $trg_email_mess;
    
    protected $trg_action_chain;

    protected $trg_app_show;
}