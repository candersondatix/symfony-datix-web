<?php
namespace src\tasks\model;

use src\framework\model\EntityCollection;

class TaskCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\tasks\\model\\Task';
    }
}