<?php

namespace src\tasks\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Response;
use src\framework\controller\Request;

class TasksTemplateController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }

    public function task()
    {
        global $JSFunctions;

        $ID = ($this->request->getParameter('ID') ? $this->request->getParameter('ID') : null);

        $Recordid = FirstNonNull(array($ID, $this->request->getParameter('recordid')));

        $action = (!$Recordid) ? _tk('new_task') : _tk('edit_task');
        $this->title = sprintf(_tk('edit_add_task'), $action);
        $this->hasPadding = false;

        $Trigger = new \System_Trigger($Recordid, $this->request->getParameter('module'));

        $sql = '
            SELECT
                recordid, sq_name
            FROM
                queries
            WHERE
                sq_module = :module
            ORDER BY
                sq_name
        ';

        $SavedQueries = \DatixDBQuery::PDO_fetch_all($sql, array('module' => $Trigger->getModule()), \PDO::FETCH_KEY_PAIR);

        $sql = '
            SELECT
                recordid, ach_title
            FROM
                action_chains
            WHERE
                ach_archive != 1
                AND
                (ach_module = :module OR ach_module = \'ALL\')
        ';

        $ActionChains = \DatixDBQuery::PDO_fetch_all($sql, array('module' => $Trigger->getModule()), \PDO::FETCH_KEY_PAIR);

        $FormArray = array(
            'trigger_details' => array(
                'Title' => 'Task Details',
                'Rows' => array(
                    array(
                        'Name' => 'recordid',
                        'Title' => 'ID',
                        'ReadOnly' => true,
                        'Type' => 'string'
                    ),
                    array(
                        'Name' => 'module',
                        'Title' => 'Module',
                        'Type' => 'ff_select',
                        'ReadOnly' => true,
                        'CustomCodes' => getModArray(array('MED')),
                        'SuppressCodeDisplay' => true
                    ),
                    array(
                        'Name' => 'trg_message',
                        'Title' => 'Name',
                        'Type' => 'string'
                    ),
                    array(
                        'Name' => 'trg_query',
                        'Title' => 'Query',
                        'Type' => 'ff_select',
                        'CustomCodes' => $SavedQueries,
                        'SuppressCodeDisplay' => true,
                        'OnChangeExtra' => 'getSavedQueryClauseForTrigger(jQuery(\'#trg_query\').val());'
                    ),
                    array(
                        'Name' => 'trg_expression',
                        'Title' => 'Where clause',
                        'Type' => 'textarea',
                        'ReadOnly' => true
                    ),
                )
            ),
            'action_chain' => array(
                'Title' => 'Action Chain',
                'Rows' => array(
                    array(
                        'Name' => 'trg_action_chain',
                        'Title' => 'Chain',
                        'Type' => 'ff_select',
                        'CustomCodes' => $ActionChains,
                        'SuppressCodeDisplay' => true
                    ),
                )
            )
        );

        $FormDesign = new \Forms_FormDesign();
        $FormDesign->MandatoryFields = array(
            'trg_action_chain' => 'action_chain',
            'trg_query' => 'trigger_details',
            'trg_message' => 'trigger_details'
        );

        if (in_array($Trigger->getQueryName(), $SavedQueries))
        {
            $SQReverse = array_flip($SavedQueries);

            $QueryID = $SQReverse[$Trigger->getQueryName()];
        }
        else
        {
            $QueryID = $Trigger->getQueryName();
        }

        $Data['recordid'] = $Trigger->getID();
        $Data['module'] = $Trigger->getModule();
        $Data['trg_message'] = $Trigger->getName();
        $Data['trg_query'] = $QueryID;
        $Data['trg_expression'] = $Trigger->getExpression();
        $Data['trg_action_chain'] = $Trigger->getActionChain();

        $Table = new \FormTable('', 'ADM', $FormDesign);
        $Table->MakeForm($FormArray, $Data, 'ADM');

        $Table->MakeTable();

        $JSFunctions[] = MakeJavaScriptValidation('ADM', $FormDesign, array(
            'trg_message' => 'Name',
            'trg_action_chain' =>
            'Chain', 'trg_query' => 'Query'
        ));

        $this->response->build('src/tasks/views/ShowTask.php', array(
            'Table' => $Table,
            'Data' => $Data
        ));
    }
}

?>
