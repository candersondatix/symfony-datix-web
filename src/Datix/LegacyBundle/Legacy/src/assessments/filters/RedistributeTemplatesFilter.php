<?php
namespace src\assessments\filters;

use src\framework\controller\ControllerFilter;

/**
* Secures access to the redistribution of assigned assessment templates.
*/
class RedistributeTemplatesFilter extends ControllerFilter
{
    /**
    * Access is controlled via the global ASM_MANAGE_CYCLES.
    * 
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException If access is not granted.
    */
    public function doAction($action)
    {
        if (!bYN(GetParm('ASM_MANAGE_CYCLES', 'N')))
        {
            throw new \URLNotFoundException();
        }
        return $this->proceed($action);
    }
}