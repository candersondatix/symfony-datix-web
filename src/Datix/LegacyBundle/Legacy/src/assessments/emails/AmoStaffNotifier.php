<?php
namespace src\assessments\emails;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\assessments\model\Assessment;
use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

// TODO - the duplicated function EmailStaff() (src/generic_modules/ATI/ModuleFunctions.php)
//        needs to be removed once the accreditation module has been converted to use the new model structure.
class AmoStaffNotifier implements Observer
{
    /**
     * {@inheritdoc}
     * 
     * Email staff that are added to the assessment record.
     * 
     * @throws InvalidArgumentException If the subject is not an Assessment.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof Assessment))
        {
            throw new \InvalidArgumentException('Object of type Assessment expected, '.get_class($subject).' given.');
        }

        if (!($event == Entity::EVENT_INSERT || $event == Entity::EVENT_UPDATE))
        {
            // this observer only listens to insert or update events
            return;
        }
        
        require_once 'Source/libs/Email.php';
        
        $data = $this->buildData($subject);
        $data['scripturl'] = $GLOBALS['scripturl'] . '?action=record&module=AMO&recordid=' . $subject->recordid;
        
        // sub-coordinator e-mail
        if (($event == Entity::EVENT_INSERT || $subject->getSub_coordinator_changed()) && $subject->adm_sub_coordinator!= '')
        {
            $Factory = new UserModelFactory();
            $User = $Factory->getMapper()->findByInitials($subject->adm_sub_coordinator);

            if ($User->con_email != '' && empty($User->con_dod) && bYN(GetUserParm($User->login, 'AMO_STAFF_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('Sub-coordinator added to Assessment ' . $data['recordid'] . '. E-mailing user ' . $User->login . '.');

                $emailSender = EmailSenderFactory::createEmailSender('AMO', 'NewStaff');
                $emailSender->addRecipient($User);
                $emailSender->sendEmails(array_merge($User->getVars(), $data));
            }
        }
        
        // end user e-mail
        if (($event == Entity::EVENT_INSERT || $subject->getEnd_user_changed()) && $subject->adm_end_user != '' && $subject->adm_end_user != $subject->adm_sub_coordinator)
        {
            $Factory = new UserModelFactory();
            $User = $Factory->getMapper()->findByInitials($subject->adm_end_user);
            if ($User->con_email != '' && empty($User->con_dod) && bYN(GetUserParm($User->login, 'AMO_STAFF_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('End user added to Assessment ' . $data['recordid'] . '. E-mailing user ' . $User->login . '.');

                $emailSender = EmailSenderFactory::createEmailSender('AMO', 'NewStaff');
                $emailSender->addRecipient($User);
                $emailSender->sendEmails(array_merge($User->getVars(), $data));
            }
        }
    }
    
    /**
     * Retrieves the data that is made available to merge into the e-mail.
     * 
     * TODO - the parent record information should be made available via the assessment object itself.
     * 
     * @param Assessment $assessment
     * 
     * @return array
     */
    public function buildData(Assessment $assessment)
    {
        return \DatixDBQuery::PDO_fetch('SELECT * FROM asm_modules WHERE recordid = :recordid ', array('recordid' => $assessment->recordid));
    }
}