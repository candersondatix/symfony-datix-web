<?php
namespace src\assessments\emails;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\assessments\model\AssignedAssessmentTemplate;
use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

// TODO - the duplicated function EmailAdditionalLocations() and associated functions (Source/generic_modules/ATI/ModuleFunctions.php)
//        need to be removed once the accreditation module has been converted to use the new model structure.
class AtiAddLocationNotifier implements Observer
{
    /**
     * {@inheritdoc}
     * 
     * E-mail users that are assigned to additional locations added to the assessment template.
     * 
     * @throws InvalidArgumentException If the subject is not an AssignedAssessmentTemplate.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof AssignedAssessmentTemplate))
        {
            throw new \InvalidArgumentException('Object of type AssignedAssessmentTemplate expected, '.get_class($subject).' given.');
        }

        if (!($event == Entity::EVENT_INSERT || $event == Entity::EVENT_UPDATE))
        {
            // this observer only listens to insert or update events
            return;
        }
        
        if ($event == Entity::EVENT_INSERT || $subject->ati_location_additional != $subject->getInitial_locations())
        {
            $data = $this->buildData($subject);
            $data['scripturl'] = $GLOBALS['scripturl'] . '?action=record&module=ATI&recordid=' . $subject->recordid;
            
            $newLocations = array();
            if ($event == Entity::EVENT_INSERT || $subject->getInitial_locations() == null)
            {
                // all locations are new
                $newLocations = explode(' ', $subject->ati_location_additional);
            }
            else
            {
                // determine which locations are new
                $initialLocations = explode(' ', $subject->getInitial_locations());
                $currentLocations = explode(' ', $subject->ati_location_additional);
                foreach ($currentLocations as $location)
                {
                    if (!in_array($location, $initialLocations))
                    {
                        $newLocations[] = $location;
                    }
                }
            }

            $userFactory = new UserModelFactory();
            
            foreach ($newLocations as $newLocation)
            {
                // e-mail users assigned to the new locations
                $usersDetails = $this->getUserPerLocation($newLocation);
                if (!empty($usersDetails))
                {
                    foreach ($usersDetails as $user)
                    {
                        if ($user['email'] != '' && empty($user['con_dod']) && bYN(GetUserParm($user['login'], 'ATI_LOC_EMAIL', 'N')))
                        {
                            require_once 'Source/libs/Email.php';
                            // TODO it would be great if we could pass the object into this function, rather than having to construct a separate data array to merge onto the e-mail
                            Registry::getInstance()->getLogger()->logEmail('E-mailing user ' . $user['fullname'] . '(' . $user['email'] . ') assigned to additional locations on Assessment template ' . $data['recordid']);

                            $recipient = $userFactory->getMapper()->findById($user['recordid']);

                            $emailSender = EmailSenderFactory::createEmailSender('ATI', 'NewLocation');
                            $emailSender->addRecipient($recipient);
                            $emailSender->sendEmails(array_merge($recipient->getVars(), $data));
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Retrieves the data that is made available to merge into the e-mail.
     * 
     * TODO - the parent template information should be made available via the assigned template object itself.
     * 
     * @param AssignedAssessmentTemplate $template
     * 
     * @return array
     */
    public function buildData(AssignedAssessmentTemplate $template)
    {
        return \DatixDBQuery::PDO_fetch('SELECT * FROM asm_templates WHERE recordid = :recordid ', array('recordid' => $template->recordid));
    }
    
    /**
     * Retrieves users assigned to a location.
     * 
     * @param int $location
     * 
     * @return array
     */
    public function getUserPerLocation($location)
    {
        return \DatixDBQuery::PDO_fetch_all('
            SELECT
                recordid, sta_title, sta_forenames, sta_surname, initials, email,
                fullname, con_dod, login, con_hier_location
            FROM
                staff
            WHERE
                con_hier_location LIKE \'' . (int) $location . ' %\' OR
                con_hier_location LIKE \'% ' . (int) $location . '\' OR
                con_hier_location LIKE \'% ' . (int) $location . ' %\' OR
                con_hier_location LIKE \'' . (int) $location . '\'
        ');
    }
}