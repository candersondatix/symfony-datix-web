<?php
namespace src\assessments\emails;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\assessments\model\AssignedAssessmentTemplate;
use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

// TODO - the duplicated function EmailStaff() (src/generic_modules/ATI/ModuleFunctions.php)
//        needs to be removed once the accreditation module has been converted to use the new model structure.
class AtiStaffNotifier implements Observer
{
    /**
     * {@inheritdoc}
     * 
     * Email staff that are added to the assessment template record.
     * 
     * @throws InvalidArgumentException If the subject is not an AssignedAssessmentTemplate.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof AssignedAssessmentTemplate))
        {
            throw new \InvalidArgumentException('Object of type AssignedAssessmentTemplate expected, '.get_class($subject).' given.');
        }

        if (!($event == Entity::EVENT_INSERT || $event == Entity::EVENT_UPDATE))
        {
            // this observer only listens to insert or update events
            return;
        }
        
        if ($event == Entity::EVENT_INSERT || $subject->ati_staff != $subject->getInitial_staff())
        {
            $data = $this->buildData($subject);
            $data['scripturl'] = $GLOBALS['scripturl'] . '?action=record&module=ATI&recordid=' . $subject->recordid;
            
            $newStaff = array();
            if ($event == Entity::EVENT_INSERT || $subject->getInitial_staff() == null)
            {
                // all staff are new
                $newStaff = explode(' ', $subject->ati_staff);
            }
            else
            {
                // determine which staff are new
                $initialStaff = explode(' ', $subject->getInitial_staff());
                $currentStaff = explode(' ', $subject->ati_staff);
                foreach ($currentStaff as $staff)
                {
                    if (!in_array($staff, $initialStaff))
                    {
                        $newStaff[] = $staff;
                    }
                }
            }
            
            foreach ($newStaff as $initials)
            {
                // e-mail new staff members
                require_once 'Source/libs/Email.php';

                $Factory = new UserModelFactory();
                $User = $Factory->getMapper()->findByInitials($initials);

                if ($User->con_email != '' && empty($User->con_dod) && bYN(GetUserParm($User->login, 'ATI_STAFF_EMAIL', 'N')))
                {
                    Registry::getInstance()->getLogger()->logEmail('E-mailing user ' . $User->fullname . '(' . $User->con_email . ') added to the Assessment template ' . $data['recordid'] . ' Staff.');

                    $emailSender = EmailSenderFactory::createEmailSender('ATI', 'NewStaff');
                    $emailSender->addRecipient($User);
                    $emailSender->sendEmails(array_merge($User->getVars(), $data));                }
            }
        }
    }
    
    /**
     * Retrieves the data that is made available to merge into the e-mail.
     * 
     * TODO - the parent template information should be made available via the assigned template object itself.
     * 
     * @param AssignedAssessmentTemplate $template
     * 
     * @return array
     */
    protected function buildData(AssignedAssessmentTemplate $template)
    {
        // delegate to the additional location notifier function, rather than duplicate here
        $obj = new AtiAddLocationNotifier();
        return $obj->buildData($template);
    }
}