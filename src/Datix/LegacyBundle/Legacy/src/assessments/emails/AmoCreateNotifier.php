<?php
namespace src\assessments\emails;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\assessments\model\Assessment;
use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

// TODO - the duplicated function ASM_Template::emailUsers() (Source/classes/ASM/TemplateClass.php)
//        needs to be removed once the accreditation module has been converted to use the new model structure.
class AmoCreateNotifier implements Observer
{
    /**
     * {@inheritdoc}
     * 
     * Email users that are assigned to the assessment's location.
     * 
     * @throws InvalidArgumentException If the subject is not an Assessment.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof Assessment))
        {
            throw new \InvalidArgumentException('Object of type Assessment expected, '.get_class($subject).' given.');
        }

        if ($event != Entity::EVENT_INSERT)
        {
            // this observer only listens to insert events
            return;
        }
        
        $data = $this->buildData($subject);
        $data['scripturl'] = $GLOBALS['scripturl'] . '?action=record&module=AMO&recordid=' . $subject->recordid;

        $usersDetails = $this->getUsers($subject->adm_location);

        $userFactory = new UserModelFactory();

        foreach ($usersDetails as $user)
        {
            if ($user['email'] != '' && empty($user['con_dod']) && bYN(GetUserParm($user['login'], 'ASM_CREATE_INSTANCES_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('Sending e-mail to '. $user['fullname'].' ('.$user['email'].')' . ' assigned to Assessment ' . $data['recordid']);

                $recipient = $userFactory->getMapper()->findById($user['recordid']);

                $emailSender = EmailSenderFactory::createEmailSender('AMO', 'NewInstances');
                $emailSender->addRecipient($recipient);
                $emailSender->sendEmails(array_merge($recipient->getVars(), $data));
            }
        }
    }
    
    /**
     * Retrieves the data that is made available to merge into the e-mail.
     * 
     * TODO - the parent record information should be made available via the assessment object itself.
     * 
     * @param Assessment $assessment
     * 
     * @return array
     */
    protected function buildData(Assessment $assessment)
    {
        // delegate to the staff notifier function, rather than duplicate here
        $obj = new AmoStaffNotifier();
        return $obj->buildData($assessment);
    }
    
    /**
     * Retrieves a set of users assigned to a location.
     * 
     * @param int $location
     * 
     * @return array
     */
    protected function getUsers($location)
    {
        // delegate to the additional location notifier function, rather than duplicate here
        $obj = new AtiAddLocationNotifier();
        return $obj->getUserPerLocation($location);
    }
}