<?php
namespace src\assessments\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\framework\model\ModelFactory;

/**
 * Manages the persistance of Assessment objects.
 */
class AssessmentMapper extends DatixEntityMapper
{
    /**
     * Used to insert criteria (if there are any, e.g. when being redistributed)
     * when the record is being created.
     */
    protected $criterionMapper;
    
    public function __construct(\DatixDBQuery $db, ModelFactory $factory, CriterionMapper $criterionMapper)
    {
        parent::__construct($db, $factory);
        $this->criterionMapper = $criterionMapper;
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\assessments\\model\\Assessment';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'asm_data_modules';
    }

    /**
     * {@inheritdoc}
     */
    public function getView()
    {
        return 'asm_modules';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doInsert(Entity $object)
    {
        $object->generatedby = $_SESSION['initials'];
        $id = parent::doInsert($object);
        
        foreach ($object->criteria as $criterion)
        {
            $criterion->asm_module_id = $id;
            $this->criterionMapper->insert($criterion);
        }
        
        return $id;
    }
}