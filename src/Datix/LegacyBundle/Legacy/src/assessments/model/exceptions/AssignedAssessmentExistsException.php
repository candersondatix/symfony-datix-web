<?php
namespace src\assessments\model\exceptions;

use src\framework\model\exceptions\ModelException;

class AssignedAssessmentExistsException extends ModelException{}