<?php
namespace src\assessments\model;

use src\framework\model\DatixWorkflowEntity;
use src\framework\query\Query;

/**
 * Represents an assessment template which has been made available to a particular location.
 * 
 * Used to generate concrete instances of assessments which are then carried out by end users.
 */
class AssignedAssessmentTemplate extends DatixWorkflowEntity
{
    /**
     * The id of the master assessment template.
     * 
     * @var int
     */
    protected $asm_module_template_id;
    
    /**
     * The id of the location this record has been assigned to.
     * 
     * @var int
     */
    protected $ati_location;
    
    /**
     * Additional locations assigned to this record.
     * 
     * Stored as a space-delimited string of integers.
     * 
     * @var string
     */
    protected $ati_location_additional;
    
    /**
     * Used to identify locations which are added to the record.
     * 
     * @var string
     */
    protected $initial_locations;
    
    /**
     * The Cycle Year for the assessment.
     * 
     * @var string
     */
    protected $ati_cycle;
    
    /**
     * The actual year the assessment takes place in.
     * 
     * @var string
     */
    protected $adm_year;
    
    /**
     * The due date.
     *
     * @var string
     */
    protected $ati_due_date;
    
    /**
     * Staff assigned to this record.
     * 
     * Stored as a space-delimited string of initials.
     * 
     * @var string
     */
    protected $ati_staff;
    
    /**
     * Used to identify staff that are added to the record.
     * 
     * @var string
     */
    protected $initial_staff;
    
    /**
     * Generic comments for this record.
     * 
     * @var string
     */
    protected $ati_comments;
    
    /**
     * The risk rank.
     * 
     * @var string
     */
    protected $ati_rank;
    
    /**
     * The subscriber risk rank.
     * 
     * @var string
     */
    protected $ati_sub_rank;
    
    /**
     * The assessment priority.
     * 
     * @var string
     */
    protected $ati_priority;

    /**
     * @var string
     */
    protected $adm_end_user;
    
    /**
     * The assessments generated from this template.
     * 
     * @var AssessmentCollection
     */
    protected $assessments;
    
    
    public function __construct($id = null)
    {
        $assessmentFactory = new AssessmentModelFactory();
        $this->assessments = $assessmentFactory->getCollection();
        parent::__construct($id);
    }
    
    /**
     * {@inheritdoc}
     */
    public function __clone()
    {
        parent::__clone();
        $this->rep_approved = 'OPEN';
        $this->ati_cycle    = null;
        $this->adm_year     = null;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setRecordid($id)
    {
        $this->recordid = (int) $id;
        
        // define how the assessments collection is linked to this entity
        $query = new Query();
        $this->assessments->setQuery($query->where(array('asm_modules.asm_template_instance_id' => $this->recordid)));
    }
    
    /**
     * Setter for ati_location_additional.
     *
     * @param string $ati_location_additional
     */
    public function setAti_location_additional($ati_location_additional) 
    {
        if ($this->ati_location_additional === null)
        {
            $this->initial_locations = $ati_location_additional;
        }
        $this->ati_location_additional = $ati_location_additional;
    }
    
    /**
     * Setter for ati_staff.
     *
     * @param $ati_staff
     */
    public function setAti_staff($ati_staff) 
    {
        if ($this->ati_staff === null)
        {
            $this->initial_staff = $ati_staff;
        }
        $this->ati_staff = $ati_staff;
    }
    
    /**
     * Getter for initial_staff.
     * 
     * @return string
     */
    public function getInitial_staff()
    {
        return $this->initial_staff;
    }
    
    /**
     * Getter for initial_locations.
     * 
     * @return string
     */
    public function getInitial_locations()
    {
        return $this->initial_locations;
    }
}