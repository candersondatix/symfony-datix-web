<?php
namespace src\assessments\model;

use src\framework\model\ModelFactory;

class AssessmentModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new AssessmentFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        $criterionModelFactory = new CriterionModelFactory();
        return new AssessmentMapper(new \DatixDBQuery(), $this, $criterionModelFactory->getMapper());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new AssessmentCollection($this->getMapper(), $this->getEntityFactory());
    }
}