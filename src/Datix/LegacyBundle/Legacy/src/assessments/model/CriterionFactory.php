<?php
namespace src\assessments\model;

use src\framework\model\EntityFactory;

class CriterionFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\assessments\\model\\Criterion';
    }
}