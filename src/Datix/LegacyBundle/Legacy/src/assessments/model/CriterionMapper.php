<?php
namespace src\assessments\model;

use src\framework\model\DatixEntityMapper;

/**
 * Manages the persistance of Criterion objects.
 */
class CriterionMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\assessments\\model\\Criterion';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'asm_data_questions';
    }

    /**
     * {@inheritdoc}
     */
    public function getView()
    {
        return 'asm_questions';
    }
}