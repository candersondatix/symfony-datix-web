<?php
namespace src\assessments\model;

use src\framework\model\EntityFactory;
use src\framework\model\Entity;
use src\assessments\emails\AtiCreateNotifier;
use src\assessments\emails\AtiAddLocationNotifier;
use src\assessments\emails\AtiStaffNotifier;

class AssignedAssessmentTemplateFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\assessments\\model\\AssignedAssessmentTemplate';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array())
    {
        $object = parent::doCreateObject($data);
        
        // attach observers
        $object->attach(new AtiCreateNotifier());
        $object->attach(new AtiAddLocationNotifier());
        $object->attach(new AtiStaffNotifier());
        
        return $object;
    }
}