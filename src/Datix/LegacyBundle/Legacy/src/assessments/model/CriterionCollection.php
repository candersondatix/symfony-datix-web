<?php
namespace src\assessments\model;

use src\framework\model\EntityCollection;

class CriterionCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\assessments\\model\\Criterion';
    }
}