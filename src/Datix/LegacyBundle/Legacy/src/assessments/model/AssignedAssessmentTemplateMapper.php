<?php
namespace src\assessments\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\framework\model\ModelFactory;
use src\framework\query\Query;
use src\framework\query\SqlWriter;
use src\assessments\model\exceptions\AssignedAssessmentExistsException;

/**
 * Manages the persistance of AssignedAssessmentTemplate objects.
 */
class AssignedAssessmentTemplateMapper extends DatixEntityMapper
{
    /**
     * Used to insert assessments (if there are any, e.g. when being redistributed)
     * when the record is being created.
     */
    protected $assessmentMapper;
    
    public function __construct(\DatixDBQuery $db, ModelFactory $factory, AssessmentMapper $assessmentMapper)
    {
        parent::__construct($db, $factory);
        $this->assessmentMapper = $assessmentMapper;
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\assessments\\model\\AssignedAssessmentTemplate';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'asm_template_instances';
    }

    /**
     * {@inheritdoc}
     */
    public function getView()
    {
        return 'asm_templates';
    }
    
    /**
     * {@inheritdoc}
     * 
     * @throws AssignedAssessmentExistsException If a record with the same criteria already exists.
     */
    protected function doInsert(Entity $object)
    {
        if ($this->recordExists($object))
        {
            throw new AssignedAssessmentExistsException('The assigned assessment template record already exists.');
        }
        
        $id = parent::doInsert($object);
        
        foreach ($object->assessments as $assessment)
        {
            $assessment->asm_template_instance_id = $id;
            $this->assessmentMapper->insert($assessment);
        }
        
        return $id;
    }
    
    /**
     * Checks whether an object already exists in the database.
     * 
     * @param AssignedAssessmentTemplate $object
     * 
     * @return boolean
     */
    protected function recordExists(AssignedAssessmentTemplate $object)
    {
        $query = new Query();
        
        $query->select(array('COUNT(*) AS num'))
              ->from('asm_templates')
              ->where(array(
                  'asm_module_template_id' => $object->asm_module_template_id,
                  'ati_cycle'              => $object->ati_cycle,
                  'adm_year'               => $object->adm_year,
                  'ati_location'           => $object->ati_location));
              
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return (bool) $this->db->fetch(\PDO::FETCH_COLUMN);
    }
}