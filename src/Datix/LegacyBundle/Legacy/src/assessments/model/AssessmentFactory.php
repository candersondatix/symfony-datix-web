<?php
namespace src\assessments\model;

use src\framework\model\EntityFactory;
use src\framework\model\Entity;
use src\assessments\emails\AmoStaffNotifier;
use src\assessments\emails\AmoCreateNotifier;

/**
 * Constructs Assessment objects.
 */
class AssessmentFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\assessments\\model\\Assessment';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array())
    {
        $object = parent::doCreateObject($data);
        
        // attach observers
        $object->attach(new AmoStaffNotifier());
        $object->attach(new AmoCreateNotifier());
        
        return $object;
    }
}