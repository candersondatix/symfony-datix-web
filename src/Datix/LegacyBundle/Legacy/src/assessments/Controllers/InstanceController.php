<?php
namespace src\assessments\controllers;

use src\framework\controller\TemplateController;
use Source\classes\Filters\Container;

/**
 * Controller class for hierarchical locations.
 */
class InstanceController extends TemplateController
{
    public function listcreatedassessmentinstances()
    {
        $AssessmentInstances = [];

        $Design = \Listings_ModuleListingDesignFactory::getListingDesign(array('module' => 'AMO'));
        $Design->LoadColumnsFromDB();

        $instancesIds = \Sanitize::SanitizeString($this->request->getParameter('instances'));
        $columns = $Design->getColumns();

        if (!in_array('recordid', $columns))
        {
            $columns[] = 'recordid';
        }

        if ($instancesIds)
        {
            // Get Assessment instances
            $sql = '
                SELECT
                    ' . implode(', ', $columns) . '
                FROM
                    asm_modules
                WHERE
                    recordid IN (' . $instancesIds . ')
            ';

            $AssessmentInstances = \DatixDBQuery::PDO_fetch_all($sql, array());
        }

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'AMO';
        $RecordList->AddRecordData($AssessmentInstances);

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design);

        $this->title = _tk('amo_listing');
        $this->module = 'AMO';

        $this->response->build('src/assessments/views/ListCreatedAssessmentInstances.php', array(
            'ListingDisplay' => $ListingDisplay
            ));
    }

    public function filterlocationscreateinstances()
    {
        $Records = $_SESSION['ATI']['RECORDLIST']->FlaggedRecords;

        if (empty($Records))
        {
            AddSessionMessage('ERROR', 'You must select at least one record');
            $this->redirect('app.php?action=list&module=ATI');
        }

        $LocationsFilter = Container::getFilter('LOC', true, 'ACR', true);
        $LocationsFilter->createWhereClause(array('records' => $Records));

        $this->redirect('app.php?action=list&module=LOC&listtype=search&sidemenu=ACR&btn=create');
        redirectexit();
    }

    /**
     * Function that creates Assessment instances based on the new Assigned Assessment Template listing with checkboxes
     *
     */
    public function newcreateinstances()
    {
        //\ASM_TemplateClass::CreateInstances uses $_POST, so need to keep it here for now.
        //TODO: refactor to use mappers + entities. This should eliminate the need for $_POST references.
        $_POST['modules'] = $_SESSION['ATI']['RECORDLIST']->FlaggedRecords;
        $_POST['locations'] = $_SESSION['LOC']['RECORDLIST']->FlaggedRecords;

        if (!empty($_POST['locations']))
        {
            if (\ASM_Template::validateAssignedAssessmentsPath($_POST['modules'], $_POST['locations']))
            {
                if (\ASM_Template::validateAssessmentPath($_POST['modules'], $_POST['locations']))
                {
                    if (validateMultipleTiers($_POST['locations']))
                    {
                        \ASM_Template::CreateInstances(true, 'app.php?action=listcreatedassessmentinstances');
                    }
                    else
                    {
                        AddSessionMessage('ERROR', _tk('multiple_tiers_error'));
                    }
                }
                else
                {
                    AddSessionMessage('ERROR', _tk('invalid_locations_error'));
                }
            }
            else
            {
                AddSessionMessage('ERROR', _tk('invalid_locations_error'));
            }
        }
        else
        {
            AddSessionMessage('ERROR', _tk('locations_error'));
        }

        $this->redirect('app.php?action=list&module=LOC&listtype=search&sidemenu=ACR&btn=create');
    }

}