<?php
namespace src\assessments\controllers;

use src\framework\controller\TemplateController;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

/**
* Controls the process of redistibuting assessments for subsequent cycle years.
*/
class AssessmentTemplatesController extends TemplateController
{
    /**
    * Takes a set of flagged records from an assessment template listing and attempts to combine
    * them with a set of flagged records from a locations listing to create a set of assigned
    * assessments records.
    *
    * If successful, takes the user to a list of newly created assigned assessments.
    * If unsucessful, returns user to their assessment template listing with error messages
    */
    public function asmCreateAssignments()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        //Action is controlled by a global. (This could easily be moved into a Filter)
        if (!bYN(GetParm('ATM_AAT', 'N')))
        {
            AddSessionMessage('ERROR', 'You do not have permission to perform this action');
            $this->redirect('?action=list&module=ATM');
        }

        if (empty($_SESSION['LOC']['RECORDLIST']->FlaggedRecords))
        {
            AddSessionMessage('ERROR', 'You must select at least one location');
            $this->redirect('?action=list&module=LOC&creatingassignments=1&sidemenu=ACR&adm_year='.$_GET['adm_year']);
        }

        if (!validateMultipleTiers($_SESSION['LOC']['RECORDLIST']->FlaggedRecords))
        {
            AddSessionMessage('ERROR', _tk('multiple_tiers_error'));
            $this->redirect('?action=list&module=LOC&creatingassignments=1&sidemenu=ACR&adm_year='.$_GET['adm_year']);
        }


        $Cycle = \DatixDBQuery::PDO_fetch('SELECT TOP 1 code FROM code_asm_cycle WHERE cod_priv_level IS NULL OR cod_priv_level = \'\' OR cod_priv_level = \'Y\' ORDER BY cod_listorder asc, code asc', array(), \PDO::FETCH_COLUMN);

        if(!$Cycle)
        {
            AddSessionMessage('ERROR', 'There is no valid cycle year available');
            $this->redirect('?action=list&module=LOC&creatingassignments=1&sidemenu=ACR&adm_year='.$_GET['adm_year']);
        }

        $sql_check = '
            SELECT
                recordid
            FROM
                asm_templates
            WHERE
                asm_module_template_id = :asm_module_template_id AND ati_location = :ati_location AND ati_cycle = :ati_cycle AND adm_year = :adm_year';


        $templateCount = 0;
        $instanceIds = array();

        if (is_array($_SESSION['ATM']['RECORDLIST']->FlaggedRecords) && !empty($_SESSION['ATM']['RECORDLIST']->FlaggedRecords))
        {
            foreach ($_SESSION['ATM']['RECORDLIST']->FlaggedRecords as $TemplateRecordid)
            {
                if (is_array($_SESSION['LOC']['RECORDLIST']->FlaggedRecords) && !empty($_SESSION['LOC']['RECORDLIST']->FlaggedRecords))
                {
                    foreach ($_SESSION['LOC']['RECORDLIST']->FlaggedRecords as $LocationRecordid)
                    {
                        $check_array = array(
                            'asm_module_template_id' => $TemplateRecordid,
                            'ati_cycle' => $Cycle,
                            'adm_year' => $_GET['adm_year'],
                            'ati_location' => $LocationRecordid
                        );

                        //check no matching assignments above or below location
                        $assignmentsBelow = \DatixDBQuery::PDO_fetch(
                            'SELECT count(*) as num FROM asm_templates
                            JOIN locations_main ON ati_location = locations_main.recordid
                            WHERE asm_module_template_id = :asm_module_template_id AND ati_cycle = :ati_cycle AND adm_year = :adm_year
                            AND
                            (
                                (
                                    locations_main.lft > (SELECT lft FROM locations_main WHERE recordid = :loc_recordid)
                                    AND locations_main.rght < (SELECT rght FROM locations_main WHERE recordid = :loc_recordid2)
                                )
                            OR
                            locations_main.recordid IN
                                (SELECT T2.recordid
                                FROM locations_main AS T1, locations_main AS T2
                                WHERE T1.lft BETWEEN T2.lft AND T2.rght
                                AND T1.recordid=:loc_recordid3)
                            )',
                            array('loc_recordid' => $LocationRecordid, 'loc_recordid2' => $LocationRecordid, 'loc_recordid3' => $LocationRecordid, 'asm_module_template_id' => $TemplateRecordid, 'ati_cycle' => $Cycle, 'adm_year' => $_GET['adm_year']),
                            \PDO::FETCH_COLUMN
                            );

                        if($assignmentsBelow != 0)
                        {
                            AddSessionMessage('ERROR', 'The selected assigned assessments have not been created due to assignments or instances already existing for them or for dependent locations');
                            $this->redirect('?action=list&module=LOC&creatingassignments=1&sidemenu=ACR&adm_year='.$_GET['adm_year']);
                        }

                        //check no matching instances above or below location
                        $instancesBelow = \DatixDBQuery::PDO_fetch(
                            'SELECT count(*) as num FROM asm_modules
                            JOIN locations_main ON adm_location = locations_main.recordid
                            WHERE asm_module_template_id = :asm_module_template_id AND ati_cycle = :ati_cycle AND adm_year = :adm_year
                            AND
                            (
                                (
                                    locations_main.lft > (SELECT lft FROM locations_main WHERE recordid = :loc_recordid)
                                    AND locations_main.rght < (SELECT rght FROM locations_main WHERE recordid = :loc_recordid2)
                                )
                            OR
                            locations_main.recordid IN
                                (SELECT T2.recordid
                                FROM locations_main AS T1, locations_main AS T2
                                WHERE T1.lft BETWEEN T2.lft AND T2.rght
                                AND T1.recordid=:loc_recordid3)
                            )',
                            array('loc_recordid' => $LocationRecordid, 'loc_recordid2' => $LocationRecordid, 'loc_recordid3' => $LocationRecordid, 'asm_module_template_id' => $TemplateRecordid, 'ati_cycle' => $Cycle, 'adm_year' => $_GET['adm_year']),
                            \PDO::FETCH_COLUMN
                            );

                        if($instancesBelow != 0)
                        {
                            AddSessionMessage('ERROR', 'The selected assigned assessments have not been created due to assignments or instances already existing for them or for dependent locations');
                            $this->redirect('?action=list&module=LOC&creatingassignments=1&sidemenu=ACR&adm_year='.$_GET['adm_year']);
                        }

                        //check no duplicates present
                        $loc_check = \DatixDBQuery::PDO_fetch($sql_check, $check_array, \PDO::FETCH_COLUMN);

                        if($loc_check)
                        {
                            AddSessionMessage('ERROR', 'The selected assigned assessments have not been created due to assignments or instances already existing for them or for dependent locations');
                            $this->redirect('?action=list&module=LOC&creatingassignments=1&sidemenu=ACR&adm_year='.$_GET['adm_year']);
                        }

                        //No validation problems, so we can store the data to be inserted here.
                        //We don't want to actually insert it until we've checked that there were no problems with the other locations selected
                        $DataToBeSaved[] = array(
                            'asm_module_template_id' => $TemplateRecordid,
                            'ati_location' => $LocationRecordid,
                            'ati_cycle' => $Cycle,
                            'adm_year' => $_GET['adm_year'],
                            'rep_approved' => 'OPEN',
                            'updatedby' => $_SESSION['initials'],
                            'updateddate' => GetTodaysDate(),
                        );

                    }
                }
            }
        }

        $query = new \DatixDBQuery('');
        $query->setSQL('
            INSERT INTO
                asm_template_instances (asm_module_template_id, ati_location, ati_cycle, rep_approved, updatedby, updateddate, adm_year)
            VALUES
                (:asm_module_template_id, :ati_location, :ati_cycle, :rep_approved, :updatedby, :updateddate, :adm_year)');

        //No duplicates found, so we can save data and send emails:
        foreach($DataToBeSaved as $data)
        {
            $recordid = $query->insert($data);
            $instanceIds[] = $recordid;
            $data['recordid'] = $recordid;
            $templateCount++;

            static::emailLocations($data);
        }

        //Since we have created new assigned assessments, we need to reset the Session RecordList to ensure that an old recordlist without these records available is not used.
        unset($_SESSION['ATI']['RECORDLIST']);

        //Need to construct a listing to display the records created (the ids of which are stored in $DatixInterface->CreatedRecordids
        $Design = new \Listings_ModuleListingDesign(array('module' => 'ATI'));
        $Design->LoadColumnsFromDB();

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'ATI';
        $RecordList->Columns = $Design->Columns;

        if(!empty($instanceIds))
        {
            $RecordList->WhereClause = MakeSecurityWhereClause('recordid IN ('.implode(',', $instanceIds).')', 'ATI');
            $RecordList->OrderBy = array(new \Listings_ListingColumn('recordid', 'asm_templates'));
            $RecordList->Paging = false;
            $RecordList->RetrieveRecords();
        }

        AddSessionMessage('INFO', _tk('number_of_assessments_distributed').$templateCount);

        $this->title = _tk('ati_listing');
        $this->module = 'ATI';

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design);

        $this->response->build('src/assessments/views/asmCreateAssignments.php', array(
            'ListingDisplay' => $ListingDisplay
        ));
    }

    /**
     * Fucntion to send emails to users that have the same locations as the Assessments that are being distributed
     *
     * @static
     * @param array $data Distributed Assessment data
     */
    public static function emailLocations($data)
    {
        global $scripturl;

        // Field needed for default e-mail template text
        $data['scripturl'] = $scripturl . '?action=record&module=ATI&recordid=' . $data['recordid'];

        // Get users that have the selected location in their location field
        $sql = '
            SELECT
                recordid,
                sta_title,
                sta_forenames,
                sta_surname,
                initials,
                email,
                fullname,
                con_dod,
                login,
                con_hier_location
            FROM
                staff
            WHERE
                con_hier_location LIKE \'' . $data['ati_location'] . ' %\'
                OR
                con_hier_location LIKE \'% ' . $data['ati_location'] . '\'
                OR
                con_hier_location LIKE \'% ' . $data['ati_location'] . ' %\'
                OR
                con_hier_location LIKE \'' . $data['ati_location'] . '\'
        ';

        $usersDetails = \DatixDBQuery::PDO_fetch_all($sql, array());

        $sql = '
            SELECT
                atm_title,
                loc_name
            FROM
                asm_templates
            WHERE
                recordid = :recordid
        ';

        $defEmailFields = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $data['recordid']));
        $data['atm_title'] = $defEmailFields['atm_title'];
        $data['loc_name'] = $defEmailFields['loc_name'];

        $userFactory = new UserModelFactory();

        foreach ($usersDetails as $user)
        {
            if ($user['email'] != '' && empty($user['con_dod']) && bYN(GetUserParm($user['login'], 'ATI_LOC_EMAIL', 'N')))
            {
                require_once 'source/libs/Email.php';
                Registry::getInstance()->getLogger()->logEmail('Sending new location e-mail to '. $user['fullname'].' ('.$user['email'].')' . ' when distributing Assessment ' . $data['recordid']);

                $recipient = $userFactory->getMapper()->findById($user['recordid']);

                $emailSender = EmailSenderFactory::createEmailSender('ATI', 'NewLocation');
                $emailSender->addRecipient($recipient);
                $emailSender->sendEmails(array_merge($recipient->getVars(), $data));
            }
        }
    }

}



