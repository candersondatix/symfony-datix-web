<?php

namespace src\assessments\controllers;

use src\framework\controller\Controller;

class AssessmentController extends Controller
{
    /**
     * Creates and displays a list of assessment modules assigned to this/descendants of this location,
     * with rolled-up compliance/completion percentages.
     */
    public function ListModules()
    {
        $Data = $this->request->getParameter('data');

        $Design = \Listings_ModuleListingDesignFactory::getListingDesign(array(
            'module' => 'AMO',
            'parent_module' => 'LOC',
            'link_type' => 'assessments'
        ));
        $Design->LoadColumnsFromDB();

        foreach ($Design->Columns as $column)
        {
            if(!($column instanceof \Listings_UDFListingColumn))
            {
                $columnsForSQL[] = $column->getTable().'.'.$column->getName();
            }
        }

        $sql = "
            SELECT
                asm_modules.recordid AS recordid,
                " . implode(', ', $columnsForSQL) . "
            FROM
                fn_asm_rollup(:location) ASM_rollup
            INNER JOIN
                asm_modules ON ASM_rollup.asm_module_template_id = asm_modules.asm_module_template_id
            INNER JOIN
                vw_locations_main ON ASM_rollup.adm_location = vw_locations_main.recordid
            LEFT JOIN
                code_asm_compliance ON ASM_rollup.score = code_asm_compliance.cod_score
            WHERE
                asm_modules.adm_location = :asm_location
            ORDER BY
                asm_modules.recordid
        ";

        $Assessments = \DatixDBQuery::PDO_fetch_all($sql, array(
            'location' => $Data['recordid'],
            'asm_location' => $Data['recordid']
        ));

        // Add parent_module and linktype to every record
        foreach ($Assessments as $id => $assessment)
        {
            $Assessments[$id]['parent_module'] = 'LOC';
            $Assessments[$id]['linktype'] = 'assessments';
        }

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'AMO';
        $RecordList->Paging = false;
        $RecordList->AddRecordData($Assessments);

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design);
        $ListingDisplay->URLParameterArray = array('parent_module', 'linktype');

        $this->response->build('src\assessments\Views\ListModules.php', array(
            'ListingDisplay' => $ListingDisplay
        ));
    }

    /**
     * Displays Assessment linked questions panel.
     */
    public function LinkedQuestionSection()
    {
        $id = $this->request->getParameter('recordid');

        $ReadOnly = ($this->request->getParameter('print') == 1);

        $Design = \Listings_ModuleListingDesignFactory::getListingDesign(array(
            'module' => 'AQU',
            'parent_module' => 'AMO',
            'link_type' => 'questions'
        ));
        $Design->LoadColumnsFromDB(null, true);

        foreach ($Design->Columns as $column)
        {
            if(!($column instanceof \Listings_UDFListingColumn))
            {
                $columnsForSQL[] = $column->getName();
            }
        }

        $Questions = \DatixDBQuery::PDO_fetch_all('SELECT recordid, '.implode(', ',$columnsForSQL).' FROM asm_questions WHERE asm_module_id = :asm_module_id ORDER BY atq_order, recordid', array('asm_module_id' => $id));

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'AQU';
        $RecordList->Paging = false;
        $RecordList->AddRecordData($Questions);

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design, '', $ReadOnly);
        $ListingDisplay->RecordURLSuffix = '&from_parent_record=1';

        $this->response->build('src\assessments\Views\LinkedQuestionSection.php', array(
            'ListingDisplay' => $ListingDisplay
        ));
    }

    /**
     * Listing of module instances that have generated using this template instance.
     */
    public function linkedModuleSection()
    {
        $data     = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('FormType');

        $ReadOnly = ($FormType == 'ReadOnly' || $FormType == 'Print');

        $Design = \Listings_ModuleListingDesignFactory::getListingDesign([
            'module' => 'AMO',
            'parent_module' => 'ATI',
            'link_type' => 'instances']
        );
        $Design->LoadColumnsFromDB(null, true);

        foreach ($Design->Columns as $column)
        {
            if(!($column instanceof \Listings_UDFListingColumn))
            {
                $columnsForSQL[] = $column->getName();
            }
        }

        $sql = '
            SELECT
                recordid, '.implode(', ', $columnsForSQL).'
            FROM
                asm_modules
            WHERE
                asm_template_instance_id = :id
            ORDER BY
                recordid
        ';

        $Modules = \DatixDBQuery::PDO_fetch_all($sql, array('id' => $data['recordid']));

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'AMO';
        $RecordList->Paging = false;
        $RecordList->AddRecordData($Modules);

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design, '', $ReadOnly);

        $this->response->build('src\assessments\Views\LinkedModuleSection.php', [
            'ListingDisplay' => $ListingDisplay
        ]);
    }
}