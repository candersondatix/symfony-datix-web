<?php

namespace src\ast\controllers;

use src\framework\controller\Controller;
use src\reports\exceptions\ReportException;

class SaveAssetController extends Controller
{
    /**
     * Saves an Equipment.
     */
    function assetlinkaction()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $form_action = $this->request->getParameter('rbWhat');
        $ast = $this->request->getParameters();
        $ast_id = $ast['ast_id'];
        $link_id = $ast['link_id'];
        $link_recordid = $ast['link_recordid'];
        $main_recordid = $ast['main_recordid'];
        $module = $ast['module'];
        $formlevel = $ast['holding_form'] ? 1 : 2;

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $ast_id)
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'ASSETS_MAIN', 'link_id' => $ast_id));
        }

        switch ($form_action)
        {
            case "SaveLink":
                // Specifically saving a link, rather than a main equipment record.
                $this->response = $this->call('src\ast\controllers\AssetsController', 'SaveLinkedEquipment', array(
                    'aParams' => array(
                        'main_recordid' => \Sanitize::SanitizeInt($main_recordid),
                        'module' => $module,
                        'formlevel' => $formlevel,
                        'ast_id' => $this->request->getParameter('ast_id'),
                    ),
                    //this is bad, but will have to do till we've sorted out the routing here, which is all over the place atm...
                    'link_exists' => $this->request->getParameter('link_exists'),
                    'ast' => $ast
                ));
                break;
            case 'Document':
            case 'Save':
                $this->SaveAsset();
                break;
            case 'Unlink':
                $this->UnlinkAsset($ast_id, $main_recordid, $module);
                break;
            case 'BackToGroup':
                    $_SESSION['security_group']['success'] = false;

                    $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                        'grp_id' => $_SESSION['security_group']['grp_id'],
                        'module' => $_SESSION['security_group']['module']
                    ));
                    return;
                break;
            case 'ShowAudit':
                $this->call('src\ast\controllers\ShowAssetController', 'asset', array(
                    'ast_id' => $this->request->getParameter('ast_id'),
                    'full_audit' => '1'
                ));
                return;
            case 'Search':
            case 'Link':
                $this->AssetsDoSelection(\UnicodeString::strtolower($form_action));
                obExit();
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');
                break;
        }
    }

    protected function UnlinkAsset($ast_id, $link_id, $module)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $column = $ModuleDefs[$module]['FK'];

        $sql = "DELETE FROM link_assets WHERE ast_id = :ast_id AND $column = :$column";
        \DatixDBQuery::PDO_query($sql, array('ast_id' => $ast_id, $column => $link_id));

        $this->call('src\\generic\\controllers\\RecordController', 'updateRecordLastUpdated', [
            'module'   => $module,
            'recordId' => $link_id
        ]);

        $this->redirect('app.php?action='.$ModuleDefs[$module]['ACTION'].'&recordid='.$link_id.'&panel=tprop');
    }

    protected function SaveAsset()
    {
        global $scripturl;

        session_start();

        $ModuleDefs = $this->registry->getModuleDefs();

        $form_action = \Sanitize::SanitizeString($this->request->getParameter('rbWhat'));
        $ast = \Sanitize::SanitizeRawArray($this->request->getParameters());
        $ast_id = \Sanitize::SanitizeInt($ast['ast_id']);
        $link_id = \Sanitize::SanitizeInt($ast['link_id']);
        $module = \Sanitize::getModule($ast['module']);
        $formlevel = $ast['holding_form'] ? 1 : 2;

        require_once GetAssetFormSettings($formlevel);

        if (!$ast_id)
        {
            $newRecord = true;
            $ast_id = GetNextRecordID("assets_main", true);
        }
        else
        {
            $newRecord = false;
            DoFullAudit('AST', 'assets_main', $ast_id);
        }

        if (bYN(GetParm('RECORD_LOCKING', 'N')))
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'assets_main', 'link_id' => $ast_id));
        }

        $ValidationErrors = AssetsController::ValidateEquipmentData();

        if ($ValidationErrors)
        {
            $error['Validation'] = $ValidationErrors;
        }

        if ($error)
        {
            $params = array();

            $params['error'] = $error;

            foreach ($this->request->getParameters() as $key => $value)
            {
                $params[$key] = \Sanitize::SanitizeString($value);
            }

            //Need to reformat dates to ensure they are in SQL format to be interpreted when reloading the form.
            $assetDateFields = array_merge(GetAllFieldsByType('AST', 'date'), getAllUdfFieldsByType('D', '', $params));
            foreach ($assetDateFields as $assetDate)
            {
                if (array_key_exists($assetDate, $params) && $params[$assetDate] != '')
                {
                    $params[$assetDate] = UserDateToSQLDate($params[$assetDate]);

                    // Some extra field dates look directly at the post value, so need to blank it out here.
                    $this->request->unSetParameter($assetDate);
                    unset($_POST[$assetDate]);
                }
            }

            $params['error'] = $error;

            $this->response = $this->call('src\ast\controllers\ShowAssetFormTemplateController', 'showassetform', array(
                'ast' => $params
            ));
            return;
        }

        $ast = ParseSaveData(array('module' => 'AST', 'data' => $ast));

        // Save the main asset record first, then create link to module if required
        if ($ast_id)
        {
            $sql = "UPDATE assets_main SET ";

            $sql .= GeneratePDOSQLFromArrays(array(
                    'FieldArray' => $ModuleDefs['AST']['FIELD_ARRAY'],
                    'DataArray' => $ast,
                    'end_comma' => true,
                    'Module' => 'AST'
                ),
                $PDOParamsArray
            );

            $sql .= " updateid = '" . GensUpdateID($ast['updateid']) . "',
                  updateddate = '" . date('d-M-Y H:i:s') . "',
                  updatedby = '". $_SESSION['initials']."'
                  WHERE recordid = :ast_id";

            if ($ast['updateid'])
            {
                $sql .= " and updateid = '$ast[updateid]'";
            }

            $PDOParamsArray['ast_id'] = $ast_id;
            $SaveAssetQuery = new \DatixDBQuery($sql);
            $result = $SaveAssetQuery->prepareAndExecute($PDOParamsArray);

            if (!$result)
            {
                $error = "An error has occurred when trying to save the equipment record. Please report the '.
                'following to the Datix administrator: $sql";
            }
            elseif ($SaveAssetQuery->PDOStatement->rowCount() == 0)
            {
                $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=asset&recordid='.
                    $ast_id.'">here</a> to return to the record';
            }

            //Save notepad
            require_once 'Source/libs/notepad.php';
            $error = SaveNotes(array(
                'id_field' => 'ast_id',
                'id' => $ast_id,
                'notes' => $ast['notes'],
                'new' => $newRecord
            ));

            //Save Progress Notes
            $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
                'module' => 'AST',
                'data'   => $ast
            ));

            //saving UDFs
            require_once 'Source/libs/UDF.php';
            SaveUDFs($ast_id, MOD_ASSETS);
        }

        if ($error != '')
        {
            SaveError($error);
        }

        // Create new link if required
        // We must first check whether the link already exists and the id is valid
        if ($ast['link_id'] && $ast_id)
        {
            $sql = "
                SELECT " .
                    $ModuleDefs[$module]['TABLE'] . ".recordid, link_assets."
                    . $ModuleDefs[$module]['FK'] . "
                FROM " .
                    $ModuleDefs[$module]['TABLE']. "
                LEFT JOIN
                    link_assets
                    ON " .
                    $ModuleDefs[$module]['TABLE'].".recordid = link_assets." . $ModuleDefs[$module]['FK'] . "
                    AND
                    link_assets.link_type = 'E'". "
                    AND
                    link_assets.ast_id = $ast_id". "
                WHERE " .
                    $ModuleDefs[$module]['TABLE'] . ".recordid = $link_id
            ";

            $request = db_query($sql);

            if ($row = db_fetch_array($request))
            {
                if (!$row[$ModuleDefs[$module]['FK']])
                {
                    \DatixDBQuery::PDO_build_and_insert('link_assets',
                        array(
                            'updateddate' => date('Y-m-d H:i:s'),
                            'updatedby' => $_SESSION['initials'],
                            'link_type' => 'E',
                            'ast_id' => $ast_id,
                            $ModuleDefs[$module]['FK'] => $ast['link_id']
                        )
                    );
                }
            }

            $LinkInfo = array(
                'module' => $module,
                'recordid' => $ast['link_id']
            );
        }

        // Need to update the url sent by documents form in case the incident has changed tables
        if ($form_action == 'Document')
        {
            $this->redirect($_POST['document_url']);
        }

        $ast['recordid'] = $ast_id;

        $_SESSION['ast_id'] = $ast['recordid'];

        // This was moved here because when we have unapproved contacts in a record the actions chains were not being attached
        if (bYN(GetParm('WEB_TRIGGERS', 'N')))
        {
            require_once 'Source/libs/Triggers.php';
            ExecuteTriggers($ast_id, $ast, $ModuleDefs['AST']['MOD_ID']);
        }

        $this->call('src\generic\controllers\SaveRecordTemplateController', 'ShowSaveRecord', array(
            'aParams' => array(
                'module' => 'AST',
                'data' => $ast
            )
        ));
    }

    /**
     * Displays a table to choose which equipment to link to a record.
     *
     * @param string $formAction The action of the form.
     */
    protected function AssetsDoSelection($formAction = 'search')
    {
        global $dbtype, $scripturl;

        $ModuleDefs = $this->registry->getModuleDefs();
        $FormAction = $this->request->getParameter('rbWhat');
        $FieldWhere = '';
        $Error = false;
        $search_where = array();
        ClearSearchSession('AST');

        if ($FormAction == 'Cancel')
        {
            if (isset($_SESSION['security_group']['grp_id']))
            {
                $_SESSION['security_group']['success'] = false;
                $this->response = $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                    'grp_id' => $_SESSION['security_group']['grp_id'],
                    'module' => $_SESSION['security_group']['module']
                ));
                return;
            }
            elseif ($_POST['qbe_recordid'] != '')
            {
                // query by example for generic modules
                $this->response = $this->call('src\generic\controllers\ShowRecordController', 'record', array(
                    'module' => $this->request->getParameter('qbe_return_module'),
                    'recordid' => $this->request->getParameter('qbe_recordid')
                ));
                return;
            }
            elseif (isset($_SESSION['packaged_report']))
            {
                $RedirectLocation = '?action=designapackagedreport';

                if ($_SESSION['packaged_report']['recordid'])
                {
                    $RedirectLocation .= '&recordid='.$_SESSION['packaged_report']['recordid'];
                }
                else
                {
                    $RedirectLocation .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
                }

                $_SESSION['packaged_report']['success'] = false;
                $this->redirect('app.php'.$RedirectLocation);
            }
            else
            {
                $this->redirect('app.php?module=AST');
            }
        }

        $LinkMode = ($FormAction == 'Link');

        // construct and cache a new Where object
        if (!$LinkMode)
        {
            try
            {
                $whereFactory = new \src\framework\query\WhereFactory();
                $where = $whereFactory->createFromRequest($this->request);

                if (empty($where->getCriteria()['parameters']))
                {
                    $where = null;
                }

                $_SESSION['AST']['NEW_WHERE'] = $where;
            }
            catch (\InvalidDataException $e)
            {
                $Error = true;
                $this->registry->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');
            }
            catch(ReportException $e)
            {
                $Error = true;
            }
        }

        $ast = QuotePostArray($this->request->getParameters());
        $_SESSION['AST']['LAST_SEARCH'] = \Sanitize::SanitizeRawArray($this->request->getParameters());

        $table = 'assets_main';

        while (list($name, $value) = each($ast))
        {
            if (!isset($value) || $value == '')
            {
                continue;
            }

            $explodeArray = explode('_', $name);

            if (in_array(str_replace('CURRENT_', '', $name), $ModuleDefs['AST']['LINK_FIELD_ARRAY']) || ($LinkMode && $explodeArray[0] == 'UDF'))
            {
                $_SESSION['AST']['LINK_DETAILS'][$name] = $value;
                continue;
            }

            // Soundex for matching equipment
            if ($LinkMode && $name == 'ast_name')
            {
                $ast_name = str_replace('*', '%', $value);
                $ast_name = str_replace('?', '_', $ast_name);

                if ($dbtype == "mssql" && !preg_match('/[%?_*]/u', $ast_name))
                {
                    $search_where[$name] = "soundex(ast_name) = soundex('".EscapeQuotes($ast_name)."')";
                }
                else
                {
                    $search_where[$name] = "ast_name LIKE '".EscapeQuotes($ast_name)."'";
                }
            }
            else
            {
                $explodeArray = explode('_', $name);

                if ($explodeArray[0] == 'UDF')
                {
                    if (MakeUDFFieldWhere($explodeArray, $value, MOD_ASSETS, $FieldWhere) === false)
                    {
                        $Error = true;
                        break;
                    }
                }
                elseif ($explodeArray[0] == 'searchtaggroup')
                {
                    // @prompt on tags is not yet supported
                    if (preg_match('/@prompt/iu',$value) == 1)
                    {
                        $search_where[] = '1=2';
                    }
                    elseif ($value != '')
                    {
                        $field = str_replace('searchtaggroup_'.$explodeArray[1].'_', '', $name);

                        // Need to find the codes associated with the selected tags.
                        $Codes = \DatixDBQuery::PDO_fetch_all('SELECT DISTINCT code FROM code_tag_links WHERE [group] = :group AND [table] = :table and field = :field and tag IN (\''.implode('\',\'', array_map('intval', explode('|', $value))).'\')', array('group' => intval($explodeArray[1]), 'field' => $field, 'table' => 'assets_main'), \PDO::FETCH_COLUMN);

                        if (!empty($Codes))
                        {
                            $search_where[] = $field.' IN (\''.implode('\',\'', $Codes).'\')';
                        }
                        else
                        {
                            // This tag hasn't been attached to any codes, so can't return any results.
                            $search_where[] = '1=2';
                        }
                    }
                }
                else
                {
                    if (MakeFieldWhere('AST', $name, $value, $FieldWhere, $table) === false)
                    {
                        $Error = true;
                        break;
                    }
                }

                if ($FieldWhere != '')
                {
                    $search_where[$name] = $FieldWhere;
                }
            }
        }

        if ($Error === true)
        {
            $this->response = $this->call('src\ast\controllers\AssetsSearchController', 'assetssearch', array(
                'validationerror' => '1'
            ));
            return;
        }

        $Where = '';

        if (empty($search_where) === false)
        {
            $Where = implode(' AND ', $search_where);
        }

        // For the moment, there is no analog to the CON_MATCH_CRITERIA_ functionality in equipment
        if ($LinkMode)
        {
            if ($search_where['ast_name'])
            {
                $linksearch[] = $search_where['ast_name'];
            }

            if ($search_where['ast_serial_no'])
            {
                $linksearch[] = $search_where['ast_serial_no'];
            }

            if (!empty($linksearch))
            {
                $Where = implode(' AND ', $linksearch);
            }
        }

        if ($LinkMode)
        {
            $Where = ($Where ? '('.$Where.') AND ' : '').'assets_main.rep_approved like \'FA\'';
        }

        if (!$Error && !ValidateWhereClause($Where, 'AST'))
        {
            $Error = true;
        }

        if ($Error === true)
        {
            AddSessionMessage('ERROR', _tk('search_errors'));

            if ($this->request->getParameter('udfRowArray'))
            {
                $_SESSION['AST']['UDFARRAY'] = $this->request->getParameter('udfRowArray');
            }

            $RedirectUrl = '?action=assetssearch&searchtype=lastsearch';
            $this->redirect('app.php' . $RedirectUrl);
        }

        $_SESSION['AST']['WHERE'] = $Where;
        $_SESSION['AST']['TABLES'] = 'assets_main';

        // We are doing a new search so unset LASTQUERYID
        unset($_SESSION['AST']['LASTQUERYID']);

        if (isset($_SESSION['security_group']['grp_id']))
        {
            $_SESSION['security_group']['success'] = true;
            $this->response = $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                'grp_id' => $_SESSION['security_group']['grp_id'],
                'module' => $_SESSION['security_group']['module']
            ));
            return;
        }
        elseif ($_POST['qbe_recordid'] != '')
        {
            // query by example for generic modules
            $this->response = $this->call('src\generic\controllers\ShowRecordController', 'record', array(
                'module' => $this->request->getParameter('qbe_return_module'),
                'recordid' => $this->request->getParameter('qbe_recordid'),
                'qbe_success' => true
            ));
            return;
        }
        elseif (isset($_SESSION['packaged_report']))
        {
            $RedirectLocation = '?action=designapackagedreport';

            if ($_SESSION['packaged_report']['recordid'])
            {
                $RedirectLocation .= '&recordid='.$_SESSION['packaged_report']['recordid'];
            }
            else
            {
                $RedirectLocation .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
            }

            $_SESSION['packaged_report']['success'] = true;
            $this->redirect('app.php'.$RedirectLocation);
        }
        elseif ($formAction == 'link')
        {
            // Specify the module linking from and the foreign keys
            $_SESSION['AST']['LINK']['MODULE'] = $this->request->getParameter('module');
            $_SESSION['AST']['LINK']['LINK_ID']['NAME'] = 'main_recordid';
            $_SESSION['AST']['LINK']['LINK_ID']['VALUE'] = $this->request->getParameter('main_recordid');
            $_SESSION['AST']['LINK']['MAIN_ID']['NAME'] = 'ast_recordid';
            $_SESSION['AST']['LINK']['LINK_RECORDID']['VALUE'] = $this->request->getParameter('link_recordid');

            $RedirectLocation = $scripturl . '?action=list&module=AST&listtype=search&link=1';
            $this->redirect($RedirectLocation);
        }
        else
        {
            $RedirectLocation = $scripturl . '?action=list&module=AST&listtype=search';
            $_SESSION['AST']['SEARCHLISTURL'] = $RedirectLocation;
            $this->redirect($RedirectLocation);
        }
    }
}