<?php

namespace src\ast\controllers;

use src\framework\controller\Controller;

class AssetsController extends Controller
{
    public function SaveLinkedEquipment()
    {
        global $formlevel;

        $aParams = ($this->request->getParameter('aParams') ? $this->request->getParameter('aParams') : array());

        $ModuleDefs = $this->registry->getModuleDefs();

        $Suffix = $aParams['suffixstring'];

        $formlevel = $formlevel ? $formlevel : $aParams['formlevel'];

        foreach ($this->request->getParameter('ast') as $key => $value)
        {
            $data[$key] = $value;
        }

        require_once GetAssetFormSettings($formlevel);

        $EquipmentErrors = $this->ValidateEquipmentData(array('data' => $data));

        if (!empty($EquipmentErrors))
        {
            $error['Validation'] = $EquipmentErrors;
        }

        if ($error)
        {
            $params = array();

            $params['error'] = $error;

            foreach ($this->request->getParameters() as $key => $value)
            {
                $params[$key] = \Sanitize::SanitizeString($value);
            }

            //Need to reformat dates to ensure they are in SQL format to be interpreted when reloading the form.
            $assetDateFields = array_merge(GetAllFieldsByType('AST', 'date'), getAllUdfFieldsByType('D', '', $params));
            foreach ($assetDateFields as $assetDate)
            {
                if (array_key_exists($assetDate, $params) && $params[$assetDate] != '')
                {
                    $params[$assetDate] = UserDateToSQLDate($params[$assetDate]);

                    // Some extra field dates look directly at the post value, so need to blank it out here.
                    $this->request->unSetParameter($assetDate);
                    unset($_POST[$assetDate]);
                }
            }

            $this->response = $this->call('src\ast\controllers\ShowAssetFormTemplateController', 'showassetform', array(
                'FormType' => $aParams['main_recordid'],
                'ast' => $params,
                'error' => $error,
                'main_recordid' => $this->request->getParameter('main_recordid'),
                'module' => $this->request->getParameter('module'),
                'LinkMode' => 'linkequipment'
            ));
            return;
        }

        // PostedEquipment is the array of equipment data to be saved, without suffix.
        // It is used for the pure equipment data (not link data) to simplify merging.
        $PostedEquipment = $this->GetEquipmentArray($data, $Suffix);

        // If there's no value to save just exit the function
        if ($PostedEquipment == null)
        {
            return;
        }

        $data = ParseSaveData(array('module' => 'AST', 'data' => $data, 'suffix' => $Suffix));

        //we also need to deal with linked data, which are listed under INC, rather than AST.
        $data = ParseSaveData(array('module' => 'INC', 'data' => $data, 'suffix' => $Suffix));

        if (!$data['ast_id'.$Suffix])
        {
            //this is a new equipment record.

            // NO MERGE FOR EQUIPMENT YET
            // the MergeContact function will alter the $PostedContact array to include database data where appropriate.
            $PostedEquipment = $this->GetEquipmentArray($data, $Suffix);
            $PostedEquipment['recordid'] = GetNextRecordid('assets_main', true);
        }
        else
        {
            $PostedEquipment = $this->GetEquipmentArray($data, $Suffix);
            $PostedEquipment['recordid'] = $data['ast_id'.$Suffix];
        }

        $aParams['recordid'] = $PostedEquipment['recordid'];
        $aParams['data'] = $data;

        $sql = "UPDATE assets_main SET ";

        //ensure we have a backup rep_approved value if this is a new record
        if (!$this->request->getParameter('recordid') && (!$PostedEquipment['rep_approved']
            || $PostedEquipment['rep_approved'] == 'NEW'))
        {
            $PostedEquipment['rep_approved'] = ($_SESSION['logged_in'] && !$data['holding_form'] ? 'FA' : 'UN');
        }

        $sql .= GeneratePDOSQLFromArrays(array(
                'FieldArray' => $ModuleDefs['AST']['FIELD_ARRAY'],
                'DataArray' => $PostedEquipment,
                'Module' => 'AST',
                'end_comma' => true
            ),
            $PDOParamsArray
        );

        $sql .= "
        updatedby = '$_SESSION[initials]',
        updateid = '" . GensUpdateID(\Sanitize::SanitizeInt($this->request->getParameter('updateid'.$Suffix))) . "',
        updateddate = '" . date('d-M-Y H:i:s') . "'";

        $sql .= " WHERE recordid = ".$PostedEquipment['recordid'];

        if ($this->request->getParameter('updateid'.$Suffix))
        {
            $sql .= " AND (updateid = :updateid)";
            $PDOParamsArray["updateid"] = $this->request->getParameter('updateid'.$Suffix);
        }
        else
        {
            $sql .= " AND (updateid IS NULL OR updateid = '')";
        }

        $result = \DatixDBQuery::PDO_query($sql, $PDOParamsArray);

        if (!$result)
        {
            fatal_error('Cannot insert equipment data');
        }

        //Save notepad
        require_once 'Source/libs/notepad.php';
        $error = SaveNotes(array(
            'id_field' => 'ast_id',
            'id' => $PostedEquipment['recordid'],
            'notes' => $data['notes']
        ));

        //saving UDFs
        require_once 'Source/libs/UDF.php';
        SaveUDFs($PostedEquipment['recordid'], MOD_ASSETS, $Suffix);

        $LinkArray = $this->getEquipmentLinkFields();

        if ($this->request->getParameter('link_exists'.$Suffix))
        {
            $link_recordid = \Sanitize::SanitizeInt($this->request->getParameter('link_recordid'.$Suffix));

            DoFullAudit('AST', 'link_assets', $PostedEquipment['recordid'], $LinkArray, "link_recordid = $link_recordid");

            $PDOParamsArray = array();

            $UpdateSetLink = GeneratePDOSQLFromArrays(array(
                    'FieldArray' => $LinkArray,
                    'DataArray' => $data,
                    'Module' => 'INC', //linked fields are stored under INC
                    'Suffix' => $aParams['suffix'],
                    'end_comma' => true
                ),
                $PDOParamsArray
            );

            $sql = "
                UPDATE
                    link_assets
                SET
                    $UpdateSetLink
                    updatedby = '$_SESSION[initials]',
                    updateddate = '" . date('Y-m-d H:i:s') . "'
                WHERE
                    link_recordid = :link_recordid
            ";

            $PDOParamsArray['link_recordid'] = $link_recordid;
            \DatixDBQuery::PDO_query($sql, $PDOParamsArray);
        }
        else
        {
            if ($this->request->getParameter('equipment_match_link_id'))
            {
                // Need to make sure that link data is not lost, so as long as it hasn't been altered on the form,
                // pick up all the link fields.
                $sql = '
                    SELECT
                        '.implode(', ',$LinkArray).'
                    FROM
                        link_assets
                    WHERE
                        link_recordid = :link_recordid';

                $row = \DatixDBQuery::PDO_fetch_all($sql, array('link_recordid' => $this->request->getParameter('equipment_match_link_id')));

                foreach ($LinkArray as $field)
                {
                    if (!$data[$field])
                    {
                        $linkdata[$field] = $row[$field];
                    }
                }

                $data = array_merge($data, QuotePostArray($linkdata,false));
            }

            $InsertIntoLink = GeneratePDOInsertSQLFromArrays(array(
                    'FieldArray' => $LinkArray,
                    'DataArray' => $data,
                    'Module' => 'INC', //linked fields are stored under INC
                    'Suffix' => $aParams['suffix'],
                    'Extras' => array(
                        'ast_id' => $PostedEquipment['recordid'],
                        $ModuleDefs[$aParams['module']]['FK'] => $aParams['main_recordid'],
                        'updatedby' => $_SESSION['initials'],
                        'updateddate' => date('Y-m-d H:i:s'),
                        'link_type' => 'E'
                    )
                ),
                $PDOInsertParams
            );

            $sql = "INSERT INTO link_assets $InsertIntoLink";

            if (!\DatixDBQuery::PDO_query($sql, $PDOInsertParams))
            {
                fatal_error('Cannot insert equipment link data');
            }

            $this->AuditEquipmentLink($aParams['module'], $aParams['main_recordid'], $PostedEquipment['recordid']);
        }

        require_once 'Source/libs/UDF.php';
        SaveUDFs($PostedEquipment['recordid'], MOD_ASSETS, $aParams['suffix']);

        //Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => 'AST',
            'data'   => $data
        ));

        //delete matched equipment if appropriate.
        if ($this->request->getParameter('from_equipment_match') && $this->request->getParameter('equipment_match_link_id'))
        {
            $sql = '
                SELECT
                    ast_id
                FROM
                    link_assets
                WHERE
                    link_recordid = :link_recordid';

            $row = \DatixDBQuery::PDO_fetch($sql, array('link_recordid' => $this->request->getParameter('equipment_match_link_id')));

            //delete old unapproved equipment record
            $sql = '
                DELETE FROM
                    assets_main
                WHERE
                    recordid = :recordid
                    AND
                    rep_approved = :rep_approved
            ';

            \DatixDBQuery::PDO_query($sql, array('recordid' =>$row['ast_id'], 'rep_approved'=>'UN'));

            //delete link to unapproved record
            $sql = '
                DELETE FROM
                    link_assets
                WHERE
                    link_recordid = :link_recordid';

            \DatixDBQuery::PDO_query($sql, array('link_recordid' => $this->request->getParameter('equipment_match_link_id')));
        }

        // Update record last updated
        $this->call('src\\generic\\controllers\\RecordController', 'updateRecordLastUpdated', [
            'module'   => $this->request->getParameter('module'),
            'recordId' => $this->request->getParameter('main_recordid')
        ]);

        if ($this->request->getParameter('no_redirect') !== true)
        {
            $RedirectLocation = '?action='.($ModuleDefs[$this->request->getParameter('module')]['GENERIC'] ? 'record&module='.$this->request->getParameter('module') : $ModuleDefs[$this->request->getParameter('module')]['ACTION']).'&recordid='.$this->request->getParameter('main_recordid').'&panel=tprop';
            $this->redirect('app.php' . $RedirectLocation);
        }
    }

    static public function EquipmentHidden($Data, $Suffix, $Module = 'INC')
    {
        global $SectionVisibility;

        if ($SectionVisibility['tprop'] === false)
        {
            return true; //Equipment sections not triggered.
        }

        if (!isset($Data['show_section_equipment_'.$Suffix]))
        {
            return true; // Equipment section doesn't exist
        }

        if ($Data['show_section_equipment_'.$Suffix] == '0')
        {
            return true; //Equipment section hidden.
        }

        return false;
    }

    static public function ValidateEquipmentData($aParams = array())
    {
        $error = ValidatePostedDates('AST', $aParams['suffix'], true);
        return $error;
    }

    /**
     * @param array $aData
     * @param string $SuffixString
     * @return mixed
     */
    private function GetEquipmentArray($aData, $SuffixString = '')
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        foreach ($ModuleDefs['AST']['FIELD_ARRAY'] as $field)
        {
            if (isset($aData[$field.$SuffixString]))
            {
                $EquipmentArray[$field] = $aData[$field.$SuffixString];
            }
        }

        return $EquipmentArray;
    }

    /**
     * Inserts an entry into the full_audit table for an Equipment contact link.
     *
     * @param string $Module Code of the current module
     * @param int $RecordID Record id of the main record
     * @param string $EquipmentID Record id of the equipment record
     */
    private function AuditEquipmentLink($Module, $RecordID, $EquipmentID)
    {
        InsertFullAuditRecord($Module, $RecordID, 'LINK', "Equipment ID: $EquipmentID");
    }

    /**
     * Returns a list of linked fields between the equipment module and the other modules.
     *
     * @return array
     */
    static public function getEquipmentLinkFields()
    {
        return array(
            'link_damage_type', 'link_other_damage_info', 'link_replaced', 'link_replace_cost', 'link_repaired',
            'link_repair_cost', 'link_written_off', 'link_disposal_cost','link_sold', 'link_sold_price',
            'link_decommissioned','link_decommission_cost','link_residual_value'
        );
    }
}