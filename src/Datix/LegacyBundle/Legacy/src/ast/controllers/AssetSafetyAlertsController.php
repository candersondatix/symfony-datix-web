<?php

namespace src\ast\controllers;

use src\framework\controller\Controller;

class AssetSafetyAlertsController extends Controller
{
    /**
     * Renders the Safety Alerts section on the Equipment form.
     */
    public function ListSafetyAlerts()
    {
        $SABPerms = $_SESSION['Globals']['SAB_PERMS'];
        $recordid = ($this->request->getParameter('recordid') ? $this->request->getParameter('recordid') : $this->request->getParameter('ast_recordid'));
        $resultArray = [];

        if (CanSeeModule('SAB'))
        {
            $sql = "
                SELECT
                    sabs_main.recordid as sab_id, sabs_main.sab_title, sabs_main.sab_reference
                FROM
                    sabs_main, link_assets
            ";

            $Where = "
                sabs_main.recordid = link_assets.sab_id
                AND
                link_assets.ast_id = " . $recordid;

            $Where = MakeSecurityWhereClause($Where, 'SAB');

            $sql .= " WHERE $Where";

            $resultArray = \DatixDBQuery::PDO_fetch_all($sql);
        }

        $this->response->build('src/ast/views/ListSafetyAlerts.php', array(
            'resultArray' => $resultArray,
            'FormType' => $this->request->getParameter('formType'),
            'SABPerms' => $SABPerms
        ));
    }
}