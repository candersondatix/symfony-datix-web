<?php

namespace src\ast\controllers;

use src\framework\controller\Controller;

class NewAssetController extends Controller
{
    /**
     * Shows new Equipment form.
     */
    function addnewasset()
    {
        $ASTPerms = GetParm('AST_PERMS');

        if ($ASTPerms && $ASTPerms != 'AST_READ_ONLY')
        {
            $this->call('src\ast\controllers\ShowAssetFormTemplateController', 'showassetform', array());
            return;
        }
        else
        {
            $msg = 'You do not have the necessary permissions to add new equipment.';
            fatal_error($msg, 'Information');
        }
    }
}