<?php
namespace src\codetaggroups\model;

use src\framework\model\EntityCollection;

class CodeTagGroupCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\codetaggroups\\model\\CodeTagGroup';
    }
}