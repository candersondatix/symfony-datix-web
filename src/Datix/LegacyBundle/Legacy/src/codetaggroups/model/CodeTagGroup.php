<?php
namespace src\codetaggroups\model;

use src\framework\model\DatixEntity;

/**
 * tag sets are entities collecting sets of tags that can be applied to coded field values.
 */
class CodeTagGroup extends DatixEntity
{
    /**
     * The title of the group - this will be used as the human-visible label in all situations
     * 
     * @var string
     */
    protected $name;
    
    /**
     * A description of the group - used to track its intended use.
     * 
     * @var string
     */
    protected $description;
}