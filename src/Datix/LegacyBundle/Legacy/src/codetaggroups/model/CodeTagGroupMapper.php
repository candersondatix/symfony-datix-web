<?php
namespace src\codetaggroups\model;

use src\framework\model\DatixEntityMapper;

/**
 * Manages the persistance of CodeTagGroup objects.
 */
class CodeTagGroupMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\codetaggroups\\model\\CodeTagGroup';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'code_tag_groups';
    }
}