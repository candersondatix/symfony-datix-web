<?php
/*
 * This is the 'Help Finder' that does the decision making on whether to get local help files or go to the online help
 */
namespace src\helpfinder\controllers;

use src\framework\controller\Controller;
use src\framework\events\EventQueue;
use src\framework\registry\Registry;
use src\framework\controller\Request;
use src\framework\controller\Response;

class HelpFinderController extends Controller
{
    /*
     * The main method accessible via action on the URL
     */
    public function helpfinder()
    {
        if ($this->shouldAccessRemote())
        {
            $this->redirect($this->getRemoteURL());
        }
        else
        {
            $this->redirect($this->getLocalURL());
        }
    }

    /*
     * Gets the page status for the online help files (hoping for 200 = OK)
     *
     * @return page status, could be 200, 404 etc or null if using local only.
     */
    protected function getRemoteHeaderStatusCode()
    {
        if($this->registry->getParm('HELP_LOCATION', 'LOCAL') == 'REMOTE' )
        {
            $requestVariables = array('http' => array('timeout' => 5));
            $http_response_header = null;

            $originalDefaults = stream_context_set_default($requestVariables);
            $meta = get_meta_tags($this->getRemoteURL());
            stream_context_set_default($originalDefaults);
            return explode(' ', $http_response_header[0])[1];
        }
        else
        {
            return null;
        }
    }

    /*
     * Gets a true or false value on whether the online help should be used. Caches value where appropriate
     *
     * @return true if the online help will be used, false for local help.
     */
    protected function shouldAccessRemote()
    {
        if($this->registry->getSessionCache()->exists('help.online-help-available'))
        {
            return $this->registry->getSessionCache()->get('help.online-help-available');
        }
        else
        {
            if($this->getRemoteHeaderStatusCode() == 200)
            {
                $this->registry->getSessionCache()->set('help.online-help-available', true);
                return true;
            }
            else
            {
                $this->registry->getSessionCache()->set('help.online-help-available', false);
                return false;
            }
        }
    }

    protected function getModule()
    {
        return $this->get('module');
    }

    protected function getLocalURL()
    {
        return $this->registry->getModuleDefs()[$this->getModule()]['LOCAL_HELP'];
    }

    protected function getRemoteURL()
    {
        return $this->registry->getModuleDefs()[$this->getModule()]['REMOTE_HELP'];
    }
}