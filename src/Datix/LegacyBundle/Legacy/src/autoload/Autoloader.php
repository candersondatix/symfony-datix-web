<?php

namespace src\autoload;

class Autoloader
{
    public static function register()
    {
        spl_autoload_register(['src\\autoload\\Autoloader', 'load']);
    }
    
    public static function load($class)
    {
        if (substr($class, -9) == 'Exception' && file_exists('Source/classes/Exceptions/'. $class . '.php'))
        {
            $fileToIncl = 'Source/classes/Exceptions/'. $class . '.php';
        }
        elseif (substr($class, 0, 5) == 'Zend_')
        {
            $fileToIncl = str_replace('_', '/', $class) . '.php';
        }
        elseif (substr($class, 0, 8) == 'Symfony\\' || substr($class, 0, 8) == 'Monolog\\' || substr($class, 0, 4) == 'Psr\\')
        {
            $fileToIncl = 'thirdpartylibs/' . str_replace('\\', '/', $class)  . '.php';
        }
        elseif (strpos($class, '\\') !== false)
        {
            $fileToIncl = str_replace('\\', '/', $class)  . '.php';
        }
        else
        {
            $fileToIncl = 'Source/classes/'. str_replace('_', '/', $class) . 'Class.php';
        }
    
        if (file_exists($fileToIncl))
        {
            require_once $fileToIncl;
        }
    }
}