<script language="javascript" type="text/javascript">
    AlertAfterChange = true;
    var submitClicked = false;
</script>
<form id="frmEditUser" name="frmEditUser" action="<?php echo $this->scripturl; ?>?action=saveuser" method="post" onsubmit="if (submitClicked) {return validateOnSubmit();}">
    <input type="hidden" name="updateid" value="<?php echo Sanitize::SanitizeString($this->staff['updateid']); ?>" />
    <input type="hidden" name="grp_id" value="<?php echo Sanitize::SanitizeInt($this->grp_id); ?>" />
    <input type="hidden" name="recordid" id="recordid" value="<?php echo Sanitize::SanitizeInt($this->user->recordid); ?>" />
    <input type="hidden" name="redirect_url" value="" />
    <?php if ($this->user->initials && $this->user->recordid) : ?>
    <input type="hidden" name="initials" value="<?php echo htmlspecialchars($this->user->initials); ?>" />
    <?php endif; ?>
    <input type="hidden" name="rbWhat" value="" />
    <input type="hidden" name="clearUserSettings" id="clearUserSettings" value="<?php echo ($this->staff['clearUserSettings'] ? Sanitize::SanitizeString($this->staff['clearUserSettings']) : ''); ?>" />
    <input type="hidden" name="sta_profile_old" id="sta_profile_old" value="<?php echo ($this->staff['sta_profile_old'] ? Sanitize::SanitizeInt($this->staff['sta_profile_old']) : Sanitize::SanitizeInt($this->user->sta_profile)); ?>" />
    <?php echo $this->UserTable->GetFormTable(); ?>
    <?php echo $this->buttonGroup->getHTML(); ?>
</form>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect(false, null, $this->UserTable); ?>
<?php endif; ?>