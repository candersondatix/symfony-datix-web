<?php
namespace src\users\controllers;

use src\framework\controller\TemplateController;
use src\users\model\UserModelFactory;

class UserTemplateController extends TemplateController
{
    function edituser()
    {
        global $JSFunctions;

        $this->module = 'ADM';

        // This reference to $ModuleDefs cannot be removed yet because it's needed by the BasicForm.php below.
        $ModuleDefs = $this->registry->getModuleDefs();

        require_once 'Source/security/SecurityUsers.php';

        $recordid = $this->request->getParameter('con_id') ?: $this->request->getParameter('recordid');

        $grp_id = \Sanitize::SanitizeInt($this->request->getParameter('grp_id'));

        $Factory = new UserModelFactory();

        $currentUser = $Factory->getMapper()->find($recordid, true);
        $params = $this->request->getParameters();
        $user = $Factory->getEntityFactory()->createObject($params, $currentUser);

        if (!$recordid)
        {
            $FormType = 'New';
            $this->title = _tk('add_new_user');
            $user = $Factory->getEntityFactory()->createFromRequest($this->request);
            $user->con_dopened = date('Y-m-d H:i:s.000');
        }
        else if (!$user->isEditable() && $recordid == $_SESSION['contact_login_id'])
        {
            $FormType = 'ReadOnly';
            $this->title = _tk('view_own_user');

            foreach (UserParamList() as $Param)
            {
                $userParameters[$Param] = FetchUserParm($user->login, $Param);

                if ($Param == 'DIF_TYPECAT_PERMS' && $userParameters[$Param])
                {
                    $userParameters[$Param] = explode(':', $userParameters[$Param]);
                }
            }
            $user->user_parameters = $userParameters;
            // Check if we need to show full audit trail
            // TODO: Retrieving this data should probably happen somewhere else.
            if ($this->request->getParameter('full_audit'))
            {
                $FullAudit = GetFullAudit(array('Module' => 'ADM', 'recordid' => $user->recordid));
            }
        }
        else
        {
            $FormType = 'Edit';

            // Check this user can be edited by the current user.
            if (!$user->isEditable())
            {
                fatal_error(_tk('user_perm_error'));
            }

            $this->title = _tk('edit_user_details').' - ' . $user->login;

            $userParameters = \DatixDBQuery::PDO_fetch_all('SELECT parameter, parmvalue FROM user_parms WHERE login = :login AND parameter IN (\''.implode('\', \'', UserParamList()).'\')', array('login' => $user->login), \PDO::FETCH_KEY_PAIR);

            foreach ($userParameters as $Param)
            {
                if ($Param == 'DIF_TYPECAT_PERMS' && $userParameters[$Param])
                {
                    $userParameters[$Param] = explode(':', $userParameters[$Param]);
                }
            }

            $user->user_parameters = $userParameters;

            // Check if we need to show full audit trail
            // TODO: Retrieving this data should probably happen somewhere else.
            if ($this->request->getParameter('full_audit'))
            {
                $FullAudit = GetFullAudit(array('Module' => 'ADM', 'recordid' => $user->recordid));
            }
        }

        //because the form construction uses a flat array with a load of extra data tagged in,
        //we need to stop using the object here for now.
        $staff = $user->getVars();

        // We need to map for Incidents Location/Type settings these fields
        // sta_orgcode as inc_organisation,
        // sta_clingroup as inc_clingroup,
        // sta_unit as inc_unit,
        // sta_directorate as inc_directorate,
        // sta_specialty as inc_specialty
        if (isset($staff['sta_orgcode']) && $staff['sta_orgcode'] != '')
        {
            $staff['inc_organisation'] = $staff['sta_orgcode'];
            unset($staff['sta_orgcode']);
        }

        if (isset($staff['sta_clingroup']) && $staff['sta_clingroup'] != '')
        {
            $staff['inc_clingroup'] = $staff['sta_clingroup'];
            unset($staff['sta_clingroup']);
        }

        if (isset($staff['sta_unit']) && $staff['sta_unit'] != '')
        {
            $staff['inc_unit'] = $staff['sta_unit'];
            unset($staff['sta_unit']);
        }

        if (isset($staff['sta_directorate']) && $staff['sta_directorate'] != '')
        {
            $staff['inc_directorate'] = $staff['sta_directorate'];
            unset($staff['sta_directorate']);
        }

        if (isset($staff['sta_specialty']) && $staff['sta_specialty'] != '')
        {
            $staff['inc_specialty'] = $staff['sta_specialty'];
            unset($staff['sta_specialty']);
        }
        if($FormType == 'ReadOnly')
        {
            $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'ADM', 'level' => 2, 'readonly' => true));
        }
        else
        {
            $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'ADM', 'level' => 2));
        }
        //Once all references to a user con_id are gone, we can remove this
        $staff['con_id'] = $staff->recordid;

        if ($FullAudit)
        {
            $staff['full_audit'] = $FullAudit;
        }

        foreach($user->user_parameters as $parameter => $value)
        {
            $staff[$parameter] =  $value;
        }

        require 'Source/security/BasicForm.php';

        $UserTable = new \FormTable($FormType,'ADM', $FormDesign);

        // Workaround to convert dates to SQL format
        // TODO: This needs to be removed when refactored
        $AdminDateFields = array_merge(GetAllFieldsByType('ADM', 'date'), getAllUdfFieldsByType('D', '', $staff));

        foreach ($AdminDateFields as $AdminDate)
        {
            if (array_key_exists($AdminDate, $staff) && $staff[$AdminDate] != '')
            {
                $staff[$AdminDate] = UserDateToSQLDate($staff[$AdminDate]);

                // Some extra field dates look directly at the post value, so need to blank it out here.
                unset($_POST[$AdminDate]);
            }
        }

        $UserTable->MakeForm($FormArray, $staff, 'ADM'); // $FormArray set in BasicForm.php (included above)

        $buttonGroup = new \ButtonGroup();

        //"Save" button
        if($user->isEditable())
        {
            $buttonGroup->AddButton(array(
                'id' => 'btnSubmit',
                'name' => 'btnSubmit',
                'label' => _tk('btn_save'),
                'onclick' => 'submitClicked=true;if(validateOnSubmit()){'.($user->recordid ? 'checkProfileSave()' : 'selectAllMultiCodes();jQuery(\'#frmEditUser\').submit()').';}',
                'action' => 'SAVE'
            ));

            if ($grp_id)
            {
                $buttonGroup->AddButton(array(
                'id' => 'btnCancel',
                'name' => 'btnCancel',
                'label' => 'Back to group',
                'onclick' => 'if(confirm(\'Cancel changes?\')){SendTo(\'app.php?action=editgroup&grp_id=' . $grp_id . '&panel=users\');}',
                'action' => 'BACK'
                ));
            }
            else
            {
                $buttonGroup->AddButton(array(
                    'id' => 'btnCancel',
                    'name' => 'btnCancel',
                    'label' => _tk('btn_cancel'),
                    'onclick' => 'if(confirm(\'Cancel changes?\')){SendTo(\'app.php?action=listusers\');}',
                    'action' => 'CANCEL'
                ));
            }
            if ($user->recordid)
            {
                $buttonGroup->AddButton(array(
                    'id' => 'btnDelete',
                    'name' => 'btnDelete',
                    'label' => _tk("btn_delete_user"),
                    'onclick' => 'if(confirm(\''._tk('confirm_delete_user').'\')){SendTo(\'app.php?action=deleteuser&recordid='.$user->recordid.'\');}',
                    'action' => 'DELETE'
                ));
            }
            if ($this->request->getParameter('fromprofile'))
            {
                $buttonGroup->AddButton(array(
                    'id' => 'btnBack',
                    'name' => 'btnBack',
                    'label' => 'Back to profile',
                    'onclick' => 'if(confirm(\''._tk('confirm_cancel_user').'\')){SendTo(\'app.php?action=editprofile&recordid='.\Sanitize::SanitizeInt($this->request->getParameter('fromprofile')).'\');};',
                    'action' => 'BACK'
                ));
            }
        }
        else
        {
            $buttonGroup->AddButton(array(
                'id' => 'btnBack',
                'name' => 'btnBack',
                'label' => 'Back to admin',
                'onclick' => 'SendTo(\'app.php?action=home&module=ADM\');',
                'action' => 'BACK'
            ));
        }

        $userParamsLocked = (IsCentrallyAdminSys() && !IsCentralAdmin() && bYN(GetParm('REMOVE_USER_PARAM_ACCESS', 'N')));

        $this->menuParameters = array(
            'table' => $UserTable,
            'buttons' => $buttonGroup,
            'no_print' => true,
            'recordid' => $user->recordid,
            'menu_array' => array(
                array(
                    'label' => _tk('userparms'),
                    'link' => 'action=listUserGlobals&user=' . $user->recordid,
                    'condition' => (IsFullAdmin() && $user->recordid != '' && !$userParamsLocked)
                ),
                array(
                    'label' => _tk('user_audit'),
                    'link' => 'action=full_audit&user=' . $user->recordid,
                    'condition' => (IsFullAdmin() && $user->recordid != '' && bYN(GetParm('ADM_SHOW_AUDIT', 'N')))
                ),
                array(
                    'label' => _tk('login_audit'),
                    'link' => 'action=show_login_audit&user=' . $user->recordid,
                    'condition' => (IsFullAdmin() && $user->recordid != '' && bYN(GetParm('ADM_SHOW_AUDIT', 'N')))
                )
            )
        );

        $JSFunctions[] = MakeJavaScriptValidation('CON', $FormDesign);

        $UserTable->MakeTable();

        $this->response->build('src/users/views/EditUser.php', array(
            'user' => $user,
            'staff' => $staff,
            'grp_id' => $grp_id,
            'UserTable' => $UserTable,
            'buttonGroup' => $buttonGroup
        ));
    }
}