<?php
namespace src\users\exceptions;

class CurrentUserNotFoundException extends \Exception
{
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        if ($message === null)
        {
            $message = 'Unable to locate the current logged-in user in the session';
        }
        parent::__construct($message, $code, $previous);
    }
}