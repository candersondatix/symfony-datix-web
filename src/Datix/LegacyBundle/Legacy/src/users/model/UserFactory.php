<?php
namespace src\users\model;

use src\framework\model\EntityFactory;

use src\users\observers\UserDeletionAuditor;

/**
 * Constructs CodeTagGroup objects.
 */
class UserFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\users\\model\\User';
    }

    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array())
    {
        $object = parent::doCreateObject($data);

        // attach observers
        $object->attach(new UserDeletionAuditor());

        return $object;
    }
}