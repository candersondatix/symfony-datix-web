<?php
namespace src\users\model\field;

use src\system\database\FieldInterface;

class MultiCodeStaffField extends StaffField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::MULTICODE;
    }
}