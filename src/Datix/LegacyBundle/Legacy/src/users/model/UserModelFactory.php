<?php
namespace src\users\model;

use src\framework\model\ModelFactory;

class UserModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new UserFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new UserMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new UserCollection($this->getMapper(), $this->getEntityFactory());
    }
}