<?php
/**
 * Created by PhpStorm.
 * User: canderson
 * Date: 20/01/2015
 * Time: 14:56
 */
namespace src\equipment\controllers;

use src\framework\controller\Controller;


class EquipmentController extends Controller
{

    /**
     * Page to list out equipment linked with a module item as user designed it
     * Requires the following to be passed in headers:
     * @header string module   the module that the item we're linking to belongs to
     * @header string recordid the id of the item we're linking to
     * @header string FormType 'ReadReadOnly', 'Print', ect.
     *
     * @return \Zend_View('src/equipment/views/ListLinkedEquipment.php')
     */
    function listLinkedEquipment()
    {
        global $ListingDesigns;

        $module       = $this->request->getParameter('module');
        $recordid     = $this->request->getParameter('recordid');
        $formType     = $this->request->getParameter('FormType');
        $formId       = $this->request->getParameter('form_id') ? $this->request->getParameter('form_id') : null;
        $listingId    = isset($ListingDesigns['tprop']['linked_assets']) ? $ListingDesigns['tprop']['linked_assets'] : null;
        $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        $readOnly     = ($formType == 'ReadOnly' || $formType == 'Print');


        //Get the design the user created for listing pages of the module in question
        $design = \Listings_ModuleListingDesignFactory::getListingDesign(array('module' => 'AST', 'parent_module' => 'INC'));
        $design->LoadColumnsFromDB($listingId , true);

        //Ensure the contents of a UDF table is selected
        foreach ($design->getColumns() as $key => $column)
        {
            if (preg_match('/^UDF.*_([0-9]+)$/ui', $column, $matches))
            {
                $udfFieldIDs[$column] = $matches[1];
                $listingColumns[] = 'null as '.$column;
            }
            else
            {
                $listingColumns[] = $column;
            }
        }

        // Select equipment linked to the parent module's item
        // TO - 11/03/2015 - The extra fields added to the query are needed to build the URLParameterArray that will
        // be used as the link for each listed linked equipment.
        $query = '
            SELECT
                ' . implode(', ', $listingColumns) . ', link_assets.link_recordid, \''.$module.'\' AS module, '.$recordid.' AS main_recordid, \'linkequipment\' AS LinkMode
            FROM
                link_assets LEFT JOIN assets_main ON assets_main.RECORDID = LINK_ASSETS.AST_ID
            WHERE
                link_assets.INC_ID = :inc_id
        ';

        $equipmentPieces = \DatixDBQuery::PDO_fetch_all($query,
                                                    array('inc_id' => $recordid));

        foreach ($equipmentPieces as $row)
        {
            $recordids[] = $row['recordid'];
        }

        // Select and append data from user defined forms
        $udfData = array();

        if (!empty($udfFieldIDs))
        {
            $udfData = getUDFData('AST', $recordids, $udfFieldIDs);
        }

        foreach ($equipmentPieces as $id => $question)
        {
            foreach ($udfFieldIDs as $udfField => $udfId)
            {
                $equipmentPieces[$id][$udfField] = $udfData[$question['recordid']][$udfId];
            }
        }

        //Logic for displaying record listings
        $recordList = new \RecordLists_ModuleRecordList();
        $recordList->Module = 'AST';
        $recordList->Paging = false;
        $recordList->AddRecordData($equipmentPieces);

        $listingDisplay = new \Listings_ListingDisplay($recordList, $design, '', $readOnly);
        $listingDisplay->RecordURLSuffix = '&from_parent_record=1';
        $listingDisplay->EmptyMessage = _tk('no_asset_records_found');
        $listingDisplay->URLParameterArray = ['module', 'link_recordid', 'main_recordid', 'LinkMode'];

        $this->response->build('src/equipment/views/ListLinkedEquipment.php', array(
            'ListingDisplay'      => $listingDisplay,
            'module'              => $module,
            'hasRecords'          => (count($recordList->Records) > 0),
            'recordid'            => $recordid,
            'form_id'             => $formId,
            'spellChecker'        => $spellChecker
        ));
    }
}