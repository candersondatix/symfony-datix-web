<?php
if ($this->FormType != 'Design' && $this->FormType != 'Print' && $this->FormType != 'Search' && $this->FormType != 'ReadOnly') : ?>
<script language="JavaScript" type="text/javascript">
    <?php echo MakeJavaScriptValidation("btnSave"); ?>
    var submitClicked = false;
    AlertAfterChange = true;
</script>
<?php endif; ?>
<form method="post" name="frmContact" action="<?php echo $this->scripturl; ?>?action=<?php echo $this->FormAction; ?>" <?php echo $this->FormOnSubmit; ?>>
    <input type="hidden" name="inc_id" value="<?php echo Sanitize::SanitizeInt($this->inc_id); ?>" />
    <input type="hidden" name="recordid" value="<?php echo Sanitize::SanitizeInt($this->con['recordid']); ?>" />
    <input type="hidden" id="form-panel" name="panel" value="<?php echo ($this->panel ?: ''); ?>" />
    <input type="hidden" name="updateid" value="<?php echo Escape::EscapeEntities($this->con['updateid']); ?>" />
    <input type="hidden" name="increp_recordid" value="<?php echo Sanitize::SanitizeInt($this->con['increp_recordid']); ?>" />
    <input type="hidden" name="main_recordid" value="<?php echo Sanitize::SanitizeInt($this->con['main_recordid']); ?>" />
    <input type="hidden" name="form_action" value="match" />
    <input type="hidden" name="rbWhat" value="" />
    <input type="hidden" name="form_id" value="<?php echo Sanitize::SanitizeInt($this->FormID); ?>" />
    <input type="hidden" name="rep_approved_old" value="<?php echo Escape::EscapeEntities($this->con['rep_approved_old'] ? $this->con['rep_approved_old'] : ($this->con['rep_approved'] ? $this->con['rep_approved'] : 'NEW')); ?>" />
    <input type="hidden" name="fromlisting" value="<?php echo Sanitize::SanitizeInt($this->fromlisting); ?>" />
    <input type="hidden" name="qbe_recordid" value="<?php echo Sanitize::SanitizeInt($this->qbe_recordid); ?>" />
    <input type="hidden" name="qbe_return_module" value="<?php echo Escape::EscapeEntities($this->qbe_return_module); ?>" />
    <?php if ($this->error['message']) : ?>
    <div class="form_error"><?php echo _tk('form_errors'); ?></div>
    <div class="form_error"><?php echo $this->error['message']; ?></div>
    <?php endif; ?>
    <?php if($this->sLockMessage) : ?>
    <div class="lock_message">
        <div id="LockMessage"><?php echo $this->sLockMessage; ?></div>
        <div id="userLockMessage"></div>
    </div>
    <?php endif; ?>
    <?php echo $this->ConTable->GetFormTable(); ?>
    <?php echo $this->ButtonGroup->getHTML(); ?>
</form>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect($this->Show_all_section, $this->panel, $this->ConTable); ?>
<?php endif; ?>