<form method="post" name="frmContact1" action="<?php echo $this->scripturl; ?>?action=saveactioncontacts">
    <input type="hidden" name="form_action" value="" />
    <input type="hidden" name="contact_id" value="<?php echo $this->contact_id; ?>" />
    <div class="windowbg2 padded_wrapper">
        <b>
            <?php echo "Contact '" . htmlspecialchars($this->contact_name) . "' has been saved. The ID of the contact is $this->contact_id."; ?>
        </b>
    </div>
    <div class="button_wrapper">
        <input type="submit" value="Go to Contacts module" name="btnCancel" onclick="document.frmContact1.form_action.value='Cancel'" />
        &nbsp;&nbsp;
        <?php if (CanListContacts()) : ?>
        <input type="submit" value="Go to Contacts listing" name="btnListContacts" onclick="document.frmContact1.form_action.value='ListContacts'" />
        &nbsp;&nbsp;
        <?php endif; ?>
        <input type="submit" value="Back to contact" name="btnBacktoContact" onclick="document.frmContact1.form_action.value='BacktoContact'" />
    </div>
</form>