<?php foreach ($this->contactsArray as $id => $row) : ?>
<tr >
    <td class="windowbg2">
        <input type="button" value="Choose" onclick="
        var field = null;
        if (document.getElementById('<?php echo $this->fieldName; ?>'))
        {
            <?php foreach ($row as $Field) : ?>
                <?php if (in_array($Field['fieldType'], array('ff_select', 'C', 'Y'))) : ?>
                field = jQuery('input[id=<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_title]');

                if (field.length)
                {
                    field.selectItem(jQuery('<li id=<?php echo $Field['fieldValue']; ?>><?php echo str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr('CON', $Field['field'], $Field['fieldValue'], '', false))); ?></li>'));
                    field.disable();
                }
                else if (jQuery('input[name=<?php echo $Field['field'] ?>_<?php echo $this->contactSuffix; ?>]:radio').length > 0)
                {
                    // represented by a radio button
                    jQuery('input[name=<?php echo $Field['field'] ?>_<?php echo $this->contactSuffix; ?>][value=<?php echo $Field['fieldValue'] ?>]:radio').prop('checked', true);
                    jQuery('input[name=<?php echo $Field['field'] ?>_<?php echo $this->contactSuffix; ?>]:radio').prop('disabled', true);
                }
                else if (jQuery('#<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').length)
                {
                    // e.g. the field is read-only
                    jQuery('#<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').val('<?php echo $Field['fieldValue']; ?>');

                    if (jQuery('div[id=<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_title]').length)
                    {
                        jQuery('div[id=<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_title]').html('<?php echo str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr('CON', $Field['field'], $Field['fieldValue'], '', false))); ?>');
                    }
                }
                <?php elseif (in_array($Field['fieldType'], array('multilistbox', 'T'))) : ?>
                field = jQuery('input[id=<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_title]');

                if (field.length)
                {
                    initDropdown(field);
                    field.data('datixselect').activeValues = [];
                    field.data('datixselect').valuesList.find('li').each(function(i, item) {
                        field.data('datixselect').activeValues.push(jQuery(item).attr('id'));
                    });
                    field.deleteSelectedItems();
                    field.data('datixselect').activeItem = null;
                    field.data('datixselect').activeItems = {};

                    <?php $valueArray = explode(' ', $Field['fieldValue']); ?>

                    <?php foreach ($valueArray as $value) : ?>
                    field.selectItem(jQuery('<li id=<?php echo $value; ?>><?php echo str_replace("&#39;", "\\'", str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr('CON', $Field['field'], $value, '', false)))); ?></li>'));
                    <?php endforeach; ?>

                    field.addSelectedItems();
                    field.disable();
                }
                else if (getCheckboxByName('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').length > 0)
                {
                    getCheckboxByName('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').prop('checked', false);

                    <?php foreach ($valueArray as $value) : ?>
                        getCheckboxByNameAndValue('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>', '<?php echo $value; ?>').prop('checked', true);
                    <?php endforeach; ?>

                    updateFieldWithCheckboxesValues('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>');
                    getCheckboxByName('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').prop('disabled', true);
                }
                else if (jQuery('#<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').length)
                {
                    // e.g. the field is read-only
                    var description = new Array();

                    <?php foreach ($valueArray as $value) : ?>
                    <?php $description = str_replace("&#39;", "\\'", str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr($this->module, $Field['field'], $value, '', false)))); ?>
                    addCodeDescr('<?php echo $value; ?>', '<?php echo $description; ?>', document.getElementById('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>'), false);
                    description.push('<?php echo $description; ?>');
                    <?php endforeach; ?>

                    if (jQuery('div[id=<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_title]').length && description.length > 0)
                    {
                        jQuery('div[id=<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_title]').html(description.join('<br />'));
                    }
                }
                <?php elseif ($Field['field'] == 'rep_approved') : ?>
                if (!jQuery('#rep_approved_<?php echo $this->contactSuffix; ?>').length)
                {
                    jQuery('<input>')
                        .attr('id', 'rep_approved_'<?php echo $this->contactSuffix; ?>')
                        .attr('name', 'rep_approved_<?php echo $this->contactSuffix; ?>')
                        .attr('type', 'hidden')
                        .appendTo(jQuery(form));
                }
                jQuery('rep_approved_<?php echo $this->contactSuffix; ?>').val('<?php echo $Field['fieldValue']; ?>');
                <?php else : ?>
                field = jQuery('#<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>');

                if (field.length)
                {
                    field.val('<?php echo $Field['fieldValue']; ?>');
                    field.attr('disabled', true);
                    AddHiddenField($('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>'));

                    if (field.attr('type') == 'hidden' && field.parent().attr('class') == 'field_input_div')
                    {
                        // add description to read-only field
                        field.parent().html('<?php echo $Field['fieldValue']; ?>');
                    }
                }

                if (jQuery('#img_cal_<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').length)
                {
                    jQuery('#img_cal_<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>').css('visibility', 'hidden');
                }
                <?php endif; ?>
                AddCustomHiddenField('<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_disabled', '<?php echo $Field['field']; ?>_<?php echo $this->contactSuffix; ?>_disabled', '1');
            <?php endforeach; ?>
            jQuery('.contact_check_btn_<?php echo $this->contactSuffix; ?>').attr('disabled', true);
            document.forms[0].check_btn_<?php echo $this->fieldName; ?>.disabled = 'true';
        }

        ContactSelectionCtrl.destroy();" />
    </td>
    <?php foreach ($this->listingColumnsArray[$id] as $value) : ?>
    <td class="windowbg2"><?php echo $value; ?></td>
    <?php endforeach; ?>
</tr>
<?php endforeach; ?>