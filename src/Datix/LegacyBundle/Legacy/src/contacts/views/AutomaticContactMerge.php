<div>Number of contacts before merge: <?php echo $this->preMergeCount; ?></div>
<div>Number of duplicates matched: <?php echo $this->totalDuplicates; ?></div>
<div>Limit imposed for this execution: <?php echo $this->maxDuplicatesToMerge; ?></div>
<div>Number of duplicates matches successfully merged: <?php echo $this->duplicatesMerged; ?></div>
<div>Number of contacts after merge: <?php echo $this->postMergeCount; ?></div>
<div>
    <?php foreach ($this->errors as $error): ?>
    <div>
        Failed to merge based on "<?php echo $error['details']['con_forenames'] ?>", "<?php echo $error['details']['con_surname'] ?>", "<?php echo $error['details']['con_nhsno'] ?>" and "<?php echo $error['details']['con_dob'] ?>"
        <?php switch ($error['type'])
        {
            case 'MULTIPLE_USERS':
                echo ' because more than one user matches this set of criteria.';
                break;
            case 'BLANK_LOGIN':
                echo ' because the master record does not have a login.';
                break;
            case 'NO_DEST_ID':
                echo ' because the system could not work out which record should be the master record.';
                break;
            default:
                echo ' because of an unknown error.';
        }
                ?>
    </div>
    <?php endforeach ?>
</div>