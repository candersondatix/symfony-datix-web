<?php

namespace src\contacts\controllers;

use src\framework\controller\TemplateController;

class ShowSaveContactTemplateController extends TemplateController
{
    function showsavecontact()
    {
        global $scripturl;

        $ModuleDefs = $this->registry->getModuleDefs();

        $contact_id = $_SESSION['contactid'];
        $contact_name = $_SESSION['contactname'];

        $this->title = 'Contact Number ' . $contact_id;
        $this->module = 'CON';
        $this->hasPadding = false;

        $recordurl = $scripturl . "?action=" . $ModuleDefs['CON']['ACTION'] . "&recordid=" . htmlspecialchars($contact_id);

        if ($this->request->getParameter('panel'))
        {
            $recordurl .= '&panel=' . $this->request->getParameter('panel');
        }

        $status_message = _tk('CON_save_message');

        AddSessionMessage('INFO', $status_message);

        $this->redirect($recordurl);

        $this->response->build('src/contacts/views/ShowSaveContact.php', array(
            'contact_id' => $contact_id,
            'contact_name' => $contact_name
        ));
    }
}