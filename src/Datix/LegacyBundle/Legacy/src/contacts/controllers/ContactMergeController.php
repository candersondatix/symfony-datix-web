<?php

namespace src\contacts\controllers;

use src\framework\controller\TemplateController;
use src\users\model\UserModelFactory;

class ContactMergeController extends TemplateController
{
    /**
     * Find all contacts with identical, non-null forename, surname, dob and nhsno fields and merge them into one.
     */
    function automaticcontactmerge()
    {
        $this->module = 'CON';
        $this->title = 'Automatic contact merge';

        $preMergeCount = \DatixDBQuery::PDO_fetch('SELECT count(*) from contacts_main', array(), \PDO::FETCH_COLUMN);

        $FieldsToSearchOn = array('con_forenames', 'con_surname', 'con_nhsno', 'con_dob');
        $FieldsToSearchOnWithNullHandling = array('con_forenames', 'con_surname', 'ISNULL(con_nhsno, \'\') as con_nhsno', 'con_dob');

        $SecurityWhere = MakeSecurityWhereClause('con_forenames != \'\' and con_forenames is not null and con_surname != \'\' and con_surname is not null and con_nhsno != \'\' and con_nhsno is not null and con_dob != \'\' and con_dob is not null', 'CON', $_SESSION['initials']);

        //We need to find contacts that match on forename, surname, nhs number and date of birth and merge them into the duplicate with the most
        //columns with data in. I think this is probably doable using CASE statements and extra joins, but I can't see how to do it quickly here.
        //Hence, for the 12.4.4 service pack, I'm doing it in another select, further down, with the understood performance hit that this will entail.
        $duplicates = \DatixDBQuery::PDO_fetch_all(
            'SELECT '.implode(', ', $FieldsToSearchOn).', num, login, numlogins, minrecordid, maxrecordid FROM
            (SELECT '.implode(', ', $FieldsToSearchOn).', count(*) as num, max(login) as login, count(login) as numlogins, MIN(recordid) as minrecordid, MAX(recordid) as maxrecordid FROM
            (SELECT '.implode(', ', $FieldsToSearchOnWithNullHandling).', login, recordid FROM contacts_main'.($SecurityWhere ? ' WHERE '.$SecurityWhere : '').') g
            GROUP BY '.implode(', ', $FieldsToSearchOn).') f
            WHERE f.num > 1 ORDER BY f.num DESC');

        $batchMerger = new \Service_BatchMerge();

        $duplicatesMerged = 0;

        //Set a limit for the duplicates to be merged
        $maxDuplicatesToMerge = GetParm('AUTOMATIC_CONTACT_MERGE_MAX', 1000);

        foreach ($duplicates as $duplicate)
        {
            if ($duplicatesMerged >= $maxDuplicatesToMerge)
            {
                break;
            }

            if ($duplicate['numlogins'] > 1)
            {
                //too many users to merge: log error.
                $errors[] = array(
                    'type' => 'MULTIPLE_USERS',
                    'details' => $duplicate
                );
                continue;
            }
            else if ($duplicate['numlogins'] == 1)
            {
                if ($duplicate['login'] != '')
                {
                    $destinationID = (new UserModelFactory)->getMapper()->findByLogin($duplicate['login'])->recordid;
                }
                else
                {
                    //no login to merge to: log error
                    $errors[] = array(
                        'type' => 'BLANK_LOGIN',
                        'details' => $duplicate
                    );
                    continue;
                }
            }
            else if ($duplicate['numlogins'] == 0)
            {
                //none of the contacts are users, so we need to work out which contact has the most data associated with it.
                $destinationID = \DatixDBQuery::PDO_fetch('SELECT TOP 1 recordid FROM contacts_main
                    WHERE
                      con_forenames = :con_forenames AND
                      con_surname = :con_surname AND
                      con_nhsno = :con_nhsno AND
                      con_dob = :con_dob
                    ORDER BY
                    ((CASE WHEN con_forenames IS NOT NULL AND con_forenames != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_surname IS NOT NULL AND con_surname != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_address IS NOT NULL AND con_address != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_dob IS NOT NULL AND con_dob != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_nhsno IS NOT NULL AND con_nhsno != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_tel1 IS NOT NULL AND con_tel1 != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_email IS NOT NULL AND con_email != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_gender IS NOT NULL AND con_gender != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_type IS NOT NULL AND con_type != \'\' THEN 1 ELSE 0 END)
                      + (CASE WHEN con_subtype IS NOT NULL AND con_subtype != \'\' THEN 1 ELSE 0 END)) DESC',
                    array(
                        'con_forenames' => $duplicate['con_forenames'],
                        'con_surname' => $duplicate['con_surname'],
                        'con_nhsno' => $duplicate['con_nhsno'],
                        'con_dob' => $duplicate['con_dob'],
                    ), \PDO::FETCH_COLUMN);
            }

            //can't use where object here because batch merge class doesn't support parameterised where clauses yet.
            $where = array();
            foreach ($FieldsToSearchOn as $field)
            {
                if (!empty($duplicate[$field]))
                {
                    $where[] = 'contacts_main.'.$field.' = \''.str_replace('\'', '\'\'', $duplicate[$field]).'\'';
                }
                else
                {
                    $where[] = '(contacts_main.'.$field.' = \'\' OR contacts_main.'.$field.' IS NULL)';
                }
            }

            if ($destinationID == '')
            {
                $errors[] = array(
                    'type' => 'NO_DEST_ID',
                    'details' => $duplicate
                );
            }
            else
            {
                //Ensure we don't also delete the record with the destination id.
                $where[] = 'contacts_main.recordid != '.$destinationID;

                $_SESSION['CON']['BATCH_MERGE_WHERE'] = implode(' AND ', $where);

                $results[] = $batchMerger->Execute(
                    array(
                        'module' => 'CON',
                        'merge_dest_id' => $destinationID,
                        'merge_documents' => true,
                        'merge_linked_records' => true,
                        'delete_after_merge' => true
                    )
                );

                $duplicatesMerged++;
            }
        }

        $postMergeCount = \DatixDBQuery::PDO_fetch('SELECT count(*) from contacts_main', array(), \PDO::FETCH_COLUMN);

        //TODO: Output error messages
        $this->response->build('src/contacts/views/AutomaticContactMerge.php', array(
            'preMergeCount' => $preMergeCount,
            'postMergeCount' => $postMergeCount,
            'totalDuplicates' => count($duplicates),
            'duplicatesMerged' => $duplicatesMerged,
            'maxDuplicatesToMerge' => $maxDuplicatesToMerge,
            'results' => $results,
            'errors' => $errors
        ));

    }
}
