<?php

namespace src\contacts\controllers;

use src\framework\controller\Controller;
use src\reports\exceptions\ReportException;

class ContactsDoSelectionController extends Controller
{
    function searchcontact()
    {
        global $dbtype, $FieldDefs, $ClientFolder;

        $ModuleDefs = $this->registry->getModuleDefs();

        $FormAction = $this->request->getParameter('form_action');
        $FieldWhere = '';
        $Error = false;
        $search_where = array();
        ClearSearchSession('CON');

        if ($FormAction == 'Cancel')
        {
            if (isset($_SESSION['security_group']['grp_id']))
            {
                $RedirectUrl = '?action=editgroup' .
                    '&grp_id=' . $_SESSION['security_group']['grp_id'] .
                    '&module=' . $_SESSION['security_group']['module'];
                $_SESSION['security_group']['success'] = false;
            }
            elseif (isset($_SESSION['packaged_report']))
            {
                $RedirectUrl = '?action=designapackagedreport';

                if ($_SESSION['packaged_report']['recordid'])
                {
                    $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
                }
                else
                {
                    $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
                }

                $_SESSION['packaged_report']['success'] = false;
            }
            elseif ($this->request->getParameter('qbe_recordid') != '')
            {
                // query by example for generic modules
                $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module') .
                    '&recordid=' . $this->request->getParameter('qbe_recordid');
            }
            else
            {
                $RedirectUrl = '?module=CON';
            }

            $this->redirect('app.php' . $RedirectUrl);
        }
        elseif ($FormAction == 'BackTo')
        {
            if ($_SESSION['FROMMODULE'])
            {
                $RedirectUrl = '?action='.$ModuleDefs[$_SESSION['FROMMODULE']]['ACTION']
                    . '&recordid=' . $_SESSION[$_SESSION['FROMMODULE']]['CURRENTID']
                    . '&panel=' . $_SESSION[$_SESSION['FROMMODULE']]['CURRENTPANEL'];
            }

            $this->redirect('app.php' . $RedirectUrl);
        }

        $LinkMode = ($FormAction == 'link');

        // construct and cache a new Where object
        if (!$LinkMode)
        {
            try
            {
                $whereFactory = new \src\framework\query\WhereFactory();
                $where = $whereFactory->createFromRequest($this->request);

                if (empty($where->getCriteria()['parameters'])) {
                    $where = null;
                }

                $_SESSION['CON']['NEW_WHERE'] = $where;
            }
            catch (\InvalidDataException $e)
            {
                $Error = true;
                $this->registry->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');
            }
            catch(ReportException $e)
            {
                $Error = true;
            }
        }

        $con = $this->request->getParameters();
        $_SESSION['CON']['LAST_SEARCH'] = $this->request->getParameters();

        if ($con['link_treatment_1'])
        {
            // Get rid of unnecessary contact suffix.
            $con['link_treatment'] = $con['link_treatment_1'];
        }

        while (list($name, $value) = each($con))
        {
            if (!isset($value) || $value == '')
            {
                continue;
            }

            $explodeArray = explode("_",$name);

            if ((!array_key_exists(str_replace('CURRENT_', '', $name), $FieldDefs['CON']) && $explodeArray[0] != 'UDF') ||
                $name == 'link_notes' || in_array($name, GetComplaintDateFields()))
            {
                $_SESSION['CON']['LINK_DETAILS'][$name] = $value;
                continue; // No point searching on these link fields
            }

            if ($LinkMode)
            {
                if($name == 'con_surname')
                {
                    $con_surname = str_replace('*', '%', $value);
                    $con_surname = str_replace('?', '_', $con_surname);

                    if ($dbtype == 'mssql' && !preg_match('/[%?_*]/u', $con_surname))
                    {
                        if(bYN(GetParm('CON_SEARCH_SOUNDEX', 'Y')))
                        {
                            $search_where[$name] = "soundex(con_surname) = soundex('".EscapeQuotes($con_surname)."')";
                        }
                        else
                        {
                            $search_where[$name] = "con_surname LIKE '".EscapeQuotes($con_surname)."'";
                        }
                    }
                    else
                    {
                        $search_where[$name] = "con_surname LIKE '".EscapeQuotes($con_surname)."'";
                    }
                }
                elseif ($name == 'con_forenames')
                {
                    $con_forenames = str_replace('*', '%', $value);
                    $con_forenames = str_replace('?', '_', $con_forenames);

                    if ($dbtype == 'mssql' && !preg_match('/[%_]/u', $con_forenames) && bYN(GetParm('CON_SEARCH_SOUNDEX', 'Y')))
                    {
                        if ($con['con_surname'] != '')
                        {
                            $search_where[$name] = "con_forenames LIKE '" . EscapeQuotes($con_forenames{0}) . "%'";
                        }
                        else
                        {
                            $search_where[$name] = "soundex(con_forenames) = soundex('$con_forenames')";
                        }
                    }
                    else
                    {
                        $search_where[$name] = "con_forenames LIKE '$con_forenames'";
                    }
                }

                if (bYN(GetParm('EXTERNAL_CONTACTS', 'N')) && !isset($GLOBALS['external_contact_address']))
                {
                    require_once "$ClientFolder/external_src_config.php";
                }
            }

            //In link mode, we always want to ignore recordid - it is never appropriate to match on.
            if (!$LinkMode || ($name != 'con_surname' && $name != 'con_forenames' && $name != 'recordid'))
            {
                if ($explodeArray[0] == 'UDF')
                {
                    if (MakeUDFFieldWhere($explodeArray, $value, MOD_CONTACTS, $FieldWhere) === false)
                    {
                        AddMangledSearchError($name, $value);
                        $Error = true;
                        break;
                    }
                }
                elseif ($explodeArray[0] == 'searchtaggroup')
                {
                    // @prompt on tags is not yet supported
                    if(preg_match('/@prompt/iu',$value) == 1)
                    {
                        $search_where[] = '1=2';
                    }
                    elseif ($value != '')
                    {
                        $field = str_replace('searchtaggroup_'.$explodeArray[1].'_', '', $name);

                        // Need to find the codes associated with the selected tags.
                        $sql = '
                            SELECT DISTINCT
                                code
                            FROM
                                code_tag_links
                            WHERE
                                [group] = :group
                                AND
                                [table] = :table
                                AND
                                field = :field
                                AND
                                tag IN (\''.implode('\',\'', array_map('intval', explode('|', $value))).'\')
                        ';

                        $Codes = \DatixDBQuery::PDO_fetch_all($sql, array(
                            'group' => intval($explodeArray[1]),
                            'field' => $field,
                            'table' => 'contacts_main'
                        ), \PDO::FETCH_COLUMN);

                        if (!empty($Codes))
                        {
                            $search_where[] = $field.' IN (\''.implode('\',\'', $Codes).'\')';
                        }
                        else
                        {
                            // This tag hasn't been attached to any codes, so can't return any results.
                            $search_where[] = '1=2';
                        }
                    }
                }
                else
                {
                    if (MakeFieldWhere('CON', $name, $value, $FieldWhere, 'contacts_main') === false)
                    {
                        AddMangledSearchError($name, $value);
                        $Error = true;
                        break;
                    }
                }

                if ($FieldWhere != '')
                {
                    if (CheckForCorrectAtCodes($FieldWhere))
                    {
                        $search_where[$name] = $FieldWhere;
                    }
                    else
                    {
                        AddMangledSearchError($name, $value);
                        $Error = true;
                    }
                }
            }
        }

        if ($LinkMode)
        {
            // We will set this later
            unset($search_where['rep_approved']);
            // In link mode, this will have been set automatically and so is not a valid search term.
            unset($search_where['con_dopened']);
        }

        if (!$Error && !ValidateWhereClause(implode(' AND ', $search_where), 'CON'))
        {
            $Error = true;
        }

        if ($Error === true)
        {
            AddSessionMessage('ERROR', _tk('search_errors'));
            if ($LinkMode)
            {
                fatal_error(GetSessionMessages());
            }
            else
            {
                $this->redirect('app.php?action=contactssearch');
            }
        }

        $Where = '';

        if (empty($search_where) === false)
        {
            $Where = implode(' AND ', $search_where);
        }

        if ($LinkMode)
        {
            $sql = "
                SELECT
                    count(*) as criteria_count
                FROM
                    globals
                WHERE
                    parameter LIKE 'CON_MATCH_CRITERIA_%'
            ";

            $criteria_check = \DatixDBQuery::PDO_fetch($sql, array());
            $current_criteria = 1;
            $fields_with_data = array_keys($search_where);

            if ($criteria_check['criteria_count'] == 0)
            {
                if ($search_where['con_surname'] || $search_where['con_forenames'] || $search_where['con_number'])
                {
                    // Default where should be as many of con_surname, con_forenames and con_number as have values.
                    if ($search_where['con_surname'])
                    {
                        $whereArray[] = $search_where[\UnicodeString::strtolower('con_surname')];
                    }
                    if ($search_where['con_forenames'])
                    {
                        $whereArray[] = $search_where[\UnicodeString::strtolower('con_forenames')];
                    }
                    if ($search_where['con_number'])
                    {
                        $whereArray[] = $search_where[\UnicodeString::strtolower('con_number')];
                    }

                    $Where = '('.implode(' AND ', $whereArray).')';
                }

                // If no appropriate values, then fallback is to search on all fields. There is a known issue here where the recordid will cause no records
                //to be displayed when searching based on an existing unapproved record.

            }
            else
            {
                $criteria_where = '';

                while ($current_criteria <= $criteria_check['criteria_count'] && ($concheck['con_count'] == 0))
                {
                    $criteria_where = GetParm('CON_MATCH_CRITERIA_' . $current_criteria, 'con_forenames&con_surname&con_number');

                    //Get rid of any spaces
                    $criteria_where = \UnicodeString::str_ireplace(' ', '', $criteria_where);

                    // Need to get list of fields from criteria
                    $tmp_field_list = $criteria_where;
                    $tmp_field_list = \UnicodeString::str_ireplace('&', ',', $tmp_field_list);
                    $tmp_field_list = \UnicodeString::str_ireplace('|', ',', $tmp_field_list);
                    $tmp_field_list = \UnicodeString::str_ireplace('(', '', $tmp_field_list);
                    $tmp_field_list = \UnicodeString::str_ireplace(')', '', $tmp_field_list);

                    $field_list = explode (',', $tmp_field_list);

                    foreach ($field_list as $fld_name)
                    {
                        if ($search_where[\UnicodeString::strtolower($fld_name)] != '')
                        {
                            $criteria_where = \UnicodeString::str_ireplace ($fld_name, $search_where[\UnicodeString::strtolower($fld_name)], $criteria_where);
                        }
                        else
                        {
                            $criteria_where = '';
                        }
                    }

                    $criteria_where = \UnicodeString::str_ireplace ( "()", "", $criteria_where);

                    //Clean up any unwanted symbols at beginning and end of WHERE clause after field value substitions made
                    while(\UnicodeString::substr($criteria_where,0,1) == '|' ||
                        \UnicodeString::substr($criteria_where,0,1) == '&' ||
                        \UnicodeString::substr($criteria_where,\UnicodeString::strlen($criteria_where)-1,1) == '|' ||
                        \UnicodeString::substr($criteria_where,\UnicodeString::strlen($criteria_where)-1,1) == '&')
                    {
                        $criteria_where = \UnicodeString::trim($criteria_where, '&');
                        $criteria_where = \UnicodeString::trim($criteria_where, '|');
                    }

                    // Replace symbols with relevant SQL operator
                    $criteria_where = \UnicodeString::str_ireplace('&', ' AND ', $criteria_where);
                    $criteria_where = \UnicodeString::str_ireplace('|', ' OR ', $criteria_where);

                    if ($criteria_where != '')
                    {
                        $sql = '
                            SELECT
                                count(*) as con_count
                            FROM
                                contacts_main
                            WHERE
                                ' . $criteria_where;

                        $concheck = \DatixDBQuery::PDO_fetch($sql, array());
                    }
                    else
                    {
                        $criteria_where = $Where;
                    }

                    $Where = $criteria_where;
                    $current_criteria++;
                }
            }
        }

        if ($LinkMode)
        {
            $Where = ($Where ? '('.$Where.') AND ' : '').'contacts_main.rep_approved like \'FA\'';
        }

        if ($FormAction == 'link')
        {
            $_SESSION['CON']['WHERELINK'] = $Where;
        }
        else
        {
            $_SESSION['CON']['WHERE'] = $Where;
        }

        if ($this->request->getParameter('from_duplicate_search'))
        {
            $_SESSION['CON']['DUPLICATE_SEARCH'] = true;
        }
        else
        {
            $_SESSION['CON']['DUPLICATE_SEARCH'] = false;
        }

        $RedirectUrl = '?action=listcontacts&fromsearch=1&module=CON';

        // Get rid of any stored recordlist
        unset($_SESSION['CON']['RECORDLIST']);

        // We are doing a new search so unset LASTQUERYID
        unset($_SESSION['CON']['LASTQUERYID']);

        if (isset($_SESSION['security_group']['grp_id']))
        {
            $RedirectUrl = '?action=editgroup' .
                '&grp_id=' . $_SESSION['security_group']['grp_id'] .
                '&module=' . $_SESSION['security_group']['module'];
            $_SESSION['security_group']['success'] = true;
        }
        elseif (isset($_SESSION['packaged_report']))
        {
            $RedirectUrl = '?action=designapackagedreport';

            if ($_SESSION['packaged_report']['recordid'])
            {
                $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
            }
            else
            {
                $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
            }

            $_SESSION['packaged_report']['success'] = true;
        }
        elseif ($this->request->getParameter('qbe_recordid') != '')
        {
            // query by example for generic modules
            $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module') .
                '&recordid=' . $this->request->getParameter('qbe_recordid') . '&qbe_success=true';
        }
        elseif ($FormAction == 'link')
        {
            // Specify the module linking from and the foreign keys
            $_SESSION['CON']['LINK']['MODULE'] = $this->request->getParameter('module');
            $_SESSION['CON']['LINK']['MAIN_ID']['NAME'] = 'con_id';
            $_SESSION['CON']['LINK']['LINK_TYPE']['VALUE'] = $this->request->getParameter('link_type');

            if ($this->request->getParameter('module') == 'SAB')
            {
                $_SESSION['CON']['LINK']['URL'] = "?action=sabslinkcontact&sab_id=" .
                    $this->request->getParameter('sab_id')
                    . "&formtype=" . $this->request->getParameter('formtype') .
                    "&linktype=" . $this->request->getParameter('link_type');
                $_SESSION['CON']['LINK']['LINK_ID']['NAME'] = 'sab_id';
                $_SESSION['CON']['LINK']['LINK_ID']['VALUE'] = $this->request->getParameter('sab_id');
            }
            else
            {
                $_SESSION['CON']['LINK']['URL'] = "?action=linkcontactgeneral&main_recordid=" .
                    $this->request->getParameter('main_recordid')
                    . "&formtype=" . $this->request->getParameter('formtype') .
                    "&link_type=" . $this->request->getParameter('link_type')
                    . "&module=" . $this->request->getParameter('module');
                $_SESSION['CON']['LINK']['LINK_ID']['NAME'] = 'main_recordid';
                $_SESSION['CON']['LINK']['LINK_ID']['VALUE'] = $this->request->getParameter('main_recordid');
                $_SESSION['CON']['LINK']['MAIN_ID']['NAME'] = 'con_recordid';
                $_SESSION['CON']['LINK']['LINK_RECORDID']['VALUE'] = $this->request->getParameter('link_recordid');
            }

            $RedirectUrl .= '&link=1';
        }
        else
        {
            $_SESSION['CON']['SEARCHLISTURL'] = $RedirectUrl;
        }

        $this->redirect('app.php' . $RedirectUrl);
    }
}