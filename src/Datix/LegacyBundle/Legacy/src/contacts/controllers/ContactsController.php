<?php

namespace src\contacts\controllers;

use src\framework\controller\Controller;

class ContactsController extends Controller
{
    public function ListLinkedContacts()
    {
        global $scripturl, $ListingDesigns;

        $ModuleDefs = $this->registry->getModuleDefs();
        $Data = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('FormType');
        $Module = $this->request->getParameter('module');
        $ExtraParameters = $this->request->getParameter('extraParameters');

        $recordid = $Data['recordid'];

        $LinkedContactFunction = ($ModuleDefs[$Module]['GET_LINKED_CONTACTS_FUNCTION'] ? $ModuleDefs[$Module]['GET_LINKED_CONTACTS_FUNCTION'] : 'GetLinkedContacts');
        $con = $LinkedContactFunction(array('recordid' => $recordid, 'module' => $Module, 'formlevel' => 2));

        $AdminUser = $_SESSION['AdminUser'];
        $Perms = GetParm($ModuleDefs[$Module]['PERM_GLOBAL']);

        $LinkPerms = (!$this->request->getParameter('print') && $FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked' && CanLinkNewContacts());

        $ContactGroup = $ModuleDefs[$Module]['CONTACTTYPES'][$ExtraParameters['link_type']];

        $total = count($con[$ExtraParameters['link_type']]);

        $SectionKey = 'contacts_type_'.$ExtraParameters['link_type'];

        require_once 'Source/libs/ContactListingClass.php';
        $Listing = new \ContactListing('CON', $ListingDesigns[$SectionKey][$SectionKey]);
        $Listing->LinkModule = $Module;
        $Listing->LoadColumnsFromDB();

        foreach ($con[$ExtraParameters['link_type']] as $contact)
        {
            $recordids[] = $contact['recordid'];
            $data[] = $contact;
        }

        //Hack to manually add extra field data to listings
        foreach ($Listing->Columns as $columnName => $details)
        {
            if (preg_match('/^UDF.*_([0-9]+)$/ui', $columnName, $matches))
            {
                $udf_field_ids[] = $matches[1];
            }
        }

        $udfData = array();

        if (!empty($udf_field_ids) && !empty($recordids))
        {
            $udfData = getUDFDisplayData('CON', $recordids, $udf_field_ids);

            foreach ($con[$ExtraParameters['link_type']] as $id => $contact)
            {
                if (!empty($udfData[$contact['recordid']]))
                {
                    foreach ($udfData[$contact['recordid']] as $field_id => $value)
                    {
                        $con[$ExtraParameters['link_type']][$id]['UDF_'.$field_id] = $value;
                    }
                }
            }
        }

        $Listing->LoadData($con[$ExtraParameters['link_type']]);
        $Listing->OverrideLinkType = $ExtraParameters['link_type'];
        $Listing->ExtraParameters = $ExtraParameters;
        $Listing->EmptyMessage = $ContactGroup["None"];
        $Listing->ShowFullContactDetails = ($this->request->getParameter('print') && bYN(GetParm('PRINT_CON_DETAILS', 'Y')) && CanSeeContacts($Module, $Perms, $Data['rep_approved']));
        $Listing->ReadOnly = ($this->request->getParameter('print') || $FormType == 'Print' || $FormType == 'ReadOnly' || $FormType == 'Locked');

        if ($LinkPerms && ($ContactGroup['Max'] == '' || $total < $ContactGroup['Max']))
        {
            $addNewContactUrl = $scripturl.'?action='.($ModuleDefs[$Module]['GENERIC'] ? 'linkcontactgeneral' : $ModuleDefs[$Module]['LINKCONTACTACTION']).'&module='.$Module.'&main_recordid='.\Sanitize::SanitizeInt($recordid).'&formtype='.$FormType.'&link_type='.$ExtraParameters['link_type'].($this->request->getParameter('fromsearch') ? '&fromsearch=1' : '');
        }
        else
        {
            $addNewContactUrl = null;
        }

        $this->response->build('src\contacts\views\ListLinkedContacts.php', array(
            'panel' => $this->request->getParameter('panel'),
            'contactwarning' => $this->request->getParameter('contactwarning'),
            'recordid' => $recordid,
            'Listing' => $Listing,
            'FormType' => $FormType,
            'ContactGroup' => $ContactGroup,
            'ExtraParameters' => $ExtraParameters,
            'addNewContactUrl' => $addNewContactUrl
        ));
    }

    /**
     * Displays the records (Incidents, Risks, PALS, Complaints and Claims) linked to a contact.
     */
    function listLinkedRecords()
    {
        global $ModuleDefs, $FieldDefsExtra, $scripturl;

        $Data                  = $this->request->getParameter('data');
        $FormType              = $this->request->getParameter('FormType');
        $aParams               = $this->request->getParameter('extraParameters');
        $PrefixedLinkedFields  = [];
        $PrefixedOrderByFields = [];

        $con_id = \Sanitize::SanitizeInt(FirstNonNull([
            $Data['recordid'],
            $_REQUEST['con_id'],
            $_REQUEST['con_recordid'],
            $_REQUEST['recordid']
        ]));

        $LinkedRecords = [];

        // If user doesn't have access to the module he should not see any linked data.
        if (CanSeeModule($aParams['link_module']))
        {
            $LinkedFields = $ModuleDefs[$aParams['link_module']]['LINKED_CONTACT_LISTING_COLS'];

            if (!in_array('recordid',$LinkedFields))
            {
                $LinkedFields[] = 'recordid';
            }

            foreach ($LinkedFields as $Field)
            {
                $PrefixedLinkedFields[] = $ModuleDefs[$aParams['link_module']]['TABLE'].'.'.$Field;
            }

            $sql = '
                SELECT
                    '.implode(', ',$PrefixedLinkedFields).'
                FROM
                    '.$ModuleDefs[$aParams['link_module']]['TABLE'].', contacts_main, link_contacts
                WHERE
                    contacts_main.recordid = '.$con_id.'
                    AND '.$ModuleDefs[$aParams['link_module']]['TABLE'].'.recordid = link_contacts.'.$ModuleDefs[$aParams['link_module']]['FK'].'
                    AND link_contacts.'.$ModuleDefs[$aParams['link_module']]['FK'].' > 0
                    AND link_contacts.con_id = '.$con_id;

            $Where = MakeSecurityWhereClause(null, $aParams['link_module'],  $_SESSION['initials']);

            if ($Where)
            {
                $sql .= " AND $Where";
            }

            $OrderByFields = $ModuleDefs[$aParams['link_module']]['LINKED_CONTACT_ORDERBY_COLS'];
            $OrderByFields[] = 'recordid';

            foreach ($OrderByFields as $Field)
            {
                $PrefixedOrderByFields[] = $ModuleDefs[$aParams['link_module']]['TABLE'].'.'.$Field;
            }

            $sql .= ' ORDER BY '.implode(', ',$PrefixedOrderByFields);

            $LinkedRecords = \DatixDBQuery::PDO_fetch_all($sql);
        }

        // We need to plug in link_respondents table here to show claims where the contact is attached as a respondent - NOT SURE IF THIS IS THE BEST WAY
        if (CanSeeModule('CLA') && 'CLA' == $aParams['link_module'])
        {
            $sql = '
                SELECT
                    ' . implode(', ', $PrefixedLinkedFields) . '
                FROM
                    ' . $ModuleDefs[$aParams['link_module']]['TABLE'] . ', contacts_main, link_respondents
                WHERE
                    contacts_main.recordid = ' . $con_id . '
                    AND ' . $ModuleDefs[$aParams['link_module']]['TABLE'] . '.recordid = link_respondents.main_recordid
                    AND link_respondents.main_recordid > 0
                    AND link_respondents.con_id = ' . $con_id;

            $Where = MakeSecurityWhereClause(null, $aParams['link_module'], $_SESSION['initials']);

            if ($Where)
            {
                $sql .= " AND $Where";
            }

            $sql .= ' ORDER BY ' . implode(', ', $PrefixedOrderByFields);

            $LinkedRespondents = \DatixDBQuery::PDO_fetch_all($sql);

            $LinkedRecords = array_merge($LinkedRecords, $LinkedRespondents);
        }

        $this->response->build('src\contacts\views\ListLinkedRecords.php', [
            'aParams'            => $aParams,
            'recNamePluralTitle' => $ModuleDefs[$aParams['link_module']]['REC_NAME_PLURAL_TITLE'],
            'con_id'             => $con_id,
            'LinkedRecords'      => $LinkedRecords,
            'recNamePlural'      => $ModuleDefs[$aParams['link_module']]['REC_NAME_PLURAL'],
            'LinkedFields'       => $LinkedFields,
            'FormType'           => $FormType,
            'print'              => $this->request->getParameter('print'),
            'scripturl'          => $scripturl,
            'action'             => $ModuleDefs[$aParams['link_module']]['ACTION'],
            'FieldDefsExtra'     => $FieldDefsExtra
        ]);
    }
}