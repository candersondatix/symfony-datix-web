<?php

namespace src\contacts\controllers;

use src\framework\controller\Controller;

class EditContactController extends Controller
{
    function editcontact()
    {
        $CONPerms = GetParm('CON_PERMS');

        if ($this->request->getParameter('link_action') == 'Cancel')
        {
            ReshowContactLink();
        }

        if (is_numeric($this->request->getParameter('recordid')))
        {
            $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        }

        // Check if this function has been called by a POST from the matching function
        // Checking actions here isn't great, and is a side effect of us not really having robust
        // validation routing: something to look at when we're able to refactor this.
        if (($this->request->getParameter('action') == 'editcontact' || $this->request->getParameter('action') == 'contactlinkmainaction') && $recordid)
        {
            $sql = '
                SELECT
                    recordid, rep_approved, con_title, con_forenames, con_surname,
                    con_type, con_gender,
                    con_subtype, con_address,
                    con_postcode, con_number, con_tel1, con_tel2,
                    con_fax, con_dob, con_email, updateid,
                    CAST (con_orgcode AS TEXT) AS con_orgcode,
                    CAST (con_unit AS TEXT) AS con_unit,
                    CAST (con_clingroup AS TEXT) AS con_clingroup,
                    CAST (con_directorate AS TEXT) AS con_directorate,
                    CAST (con_specialty AS TEXT) AS con_specialty,
                    CAST (con_loctype AS TEXT) AS con_loctype,
                    CAST (con_locactual AS TEXT) AS con_locactual,
                    con_empl_grade, con_payroll, con_notes, con_jobtitle,
                    con_nhsno, con_organisation, con_dod, con_language,
                    con_ethnicity, con_disability, con_sex_orientation,
                    con_religion, con_work_alone_assessed,
                    con_dopened, con_dclosed, initials, con_police_number,
                    show_document
                FROM
                    contacts_main
            ';

            $WhereClause = MakeSecurityWhereClause('recordid = :recordid', 'CON', $_SESSION['initials']);

            $sql .= ' WHERE ' . $WhereClause;

            $con = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

            if (!$con || $CONPerms == 'CON_INPUT_ONLY' || !$CONPerms)
            {
                CheckRecordNotFound(array('module' => 'CON', 'recordid' => $recordid));
            }

            //check whether data is being passed in manually (e.g. because a validation error has occurred).
            //this happens after the query because we still need to check the db to ensure we have permissions
            if ($this->request->getParameter('data') != '')
            {
                $con = $this->request->getParameter('data');
            }

            if ($this->request->getParameter('action') == 'editcontact' && $recordid)
            {
                // Find the number of other records linked to this contact
                $con = SafelyMergeArrays(array($con, GetContactLinkValues($recordid)));
            }
        }

        if ($CONPerms == 'CON_READ_ONLY')
        {
            $form_action = 'ReadOnly';
        }

        if ($this->request->getParameter('print') == 1)
        {
            $form_action = 'Print';
        }

        $Parameters = array(
            'inc_id' => $inc_id,
            'form_action' => $form_action,
            'data' => $con
        );
        $this->call('src\contacts\controllers\ShowContactFormTemplateController', 'newcontact', $Parameters);
    }
}