<?php

namespace src\contacts\controllers;

use src\framework\controller\Controller;

class SaveActionContactsController extends Controller
{
    function saveactioncontacts()
    {
        $FormAction = $this->request->getParameter('form_action');
        $contact_id = $this->request->getParameter('contact_id');

        switch ($FormAction)
        {
            case 'Cancel':
                $this->redirect('app.php?module=CON');
                break;
            case 'ListContacts':
                $this->redirect('app.php?action=listcontacts');
                break;
            case 'BacktoContact':
                $this->redirect('app.php?action=editcontact&recordid=' . $contact_id);
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');
                break;
            default:
                $this->redirect('app.php?module=CON');
        }
    }
}