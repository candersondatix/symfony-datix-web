<?php
namespace src\contacts\model;

use src\framework\model\EntityCollection;

class ContactCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\contacts\\model\\Contact';
    }
}