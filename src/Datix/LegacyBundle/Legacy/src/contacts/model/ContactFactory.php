<?php
namespace src\contacts\model;

use src\framework\model\EntityFactory;

/**
 * Constructs CodeTagGroup objects.
 */
class ContactFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\contacts\\model\\Contact';
    }
}