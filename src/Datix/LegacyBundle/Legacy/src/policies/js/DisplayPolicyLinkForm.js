function policyCreateNewLink() {

    var $form = jQuery('#policy_link_form'),
        policyIsValid = jQuery("#policyIsValid").val();

    if (policyIsValid == 0)
    {
        $form.submit();
    }
    else if (policyIsValid == 1)
    {
        if (confirm(confirmMsg1))
        {
            $form.submit();
        }
    }
    else if (policyIsValid == 2)
    {
        if (confirm(confirmMsg2))
        {
            $form.submit();
        }
    }
    else if (policyIsValid == 3)
    {
        if (confirm(confirmMsg3))
        {
            $form.submit();
        }
        else
        {
            jQuery("#cancel").trigger("click", [true]);
        }
    }
}

function checkForMatchingPolicies() {

    displayLoadPopup();

    jQuery.ajax({
        url:      "app.php?action=doselection&module=POL&response=ajax",
        type:     "POST",
        data:     jQuery("#policy_link_form").serialize(),
        dataType: "json",
        success: function(data)
        {
            var linkRecordid = jQuery("#link_recordid").val(),
                mainRecordid = jQuery("#main_recordid").val(),
                organisationRecordid = jQuery("#org_id").val(),
                type = jQuery("#type").val();

            jQuery(".error_div").remove();

            if (data.errors != undefined)
            {
                jQuery.each(data.errors.reverse(), function(i, error)
                {
                    jQuery(".fieldset").prepend(
                        jQuery("<div/>")
                            .addClass("error_div")
                            .text(error)
                    );
                });
            }
            else
            {
                var url = "app.php?action=listmatchingpolicies&module=CLA&link_recordid="+ linkRecordid +"&main_recordid="+ mainRecordid +
                    "&type="+ type +"&org_id="+ organisationRecordid;

                window.open(url, "wndMatchExisting", "dependent,menubar=false,screenX=200,screenY=330,titlebar,scrollbars,resizable");
            }

            hideLoadPopup();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert("An error occurred");
            hideLoadPopup();
        }
    });
}

function policyCancelLinking(skipConfirm) {

    skipConfirm = setDefaultParameter(skipConfirm, false);

    var url,
        type = jQuery("#type").val(),
        organisationRecordid = jQuery("#org_id").val(),
        linkRecordid = jQuery("#link_recordid").val(),
        mainRecordid = jQuery("#main_recordid").val();

    switch (type)
    {
        case "CON":
            url = "app.php?action=linkcontactgeneral&module=CLA&link_recordid="+ linkRecordid +"&link_type=O&main_recordid="+ mainRecordid +"&panel=policies";
            break;

        case "ORG":
            url = "app.php?action=editorganisationlink&module=CLA&recordid="+ organisationRecordid +"&main_recordid="+ mainRecordid +"&link_recordid="+ linkRecordid +"&panel=policies";
            break;

        default:
            Alert("Error: incorrect type value");
            return;
    }

    if (skipConfirm || confirm(txt["confirm_or_cancel"]))
    {
        SendTo(url);
    }
}

(function($)
{
    $(document).ready(function()
    {
        /**
         * Create new link button.
         */
        $("#create_new_link_button").click(function() {

            policyCreateNewLink();
        });

        /**
         * Check for matching policies button.
         */
        $("#check_for_matching_policies").click(function() {

            checkForMatchingPolicies();
        });
        
        /**
         * Cancel button.
         */
        $("#cancel").click(function(e, skipConfirm) {

            policyCancelLinking(skipConfirm);
        });
    });
})(jQuery);
