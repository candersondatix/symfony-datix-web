<?php
namespace src\policies\model;

use src\framework\model\DatixEntity;

use src\profiles\model\profileparameter\ProfileParameterModelFactory;
use src\security\groups\model\GroupModelFactory;
use src\framework\model\exceptions\UndefinedPropertyException;

/**
 * This entity models a user who can log in to the system
 */
class Policy extends DatixEntity
{
    /**
     * A human-readable name for the policy
     *
     * @var string
     */
    protected $title;

    /**
     * Reference
     *
     * @var string
     */
    protected $reference;
    
    /**
     * A description of the policy
     *
     * @var string
     */
    protected $description;

    /**
     * The date the policy becomes active
     *
     * @var int
     */
    protected $start_date;

    /**
     * The date the policy ceases to be active.
     *
     * @var int
     */
    protected $end_date;
    
    /**
     * Money value: the point from wich the insurance policy takes over on any one claim.
     *
     * @var float
     */
    protected $attachment_point;

    /**
     *  Money value: the limit this policy covers for a single incident
     *
     * @var float
     */
    protected $event_limit;

    /**
     *  Money value: the limit this policy covers over the course of its active life
     *
     * @var float
     */
    protected $aggregate_limit;

    /**
     * The insurer code.
     * 
     * @var string
     */
    protected $insurer;
    
    /**
     * The coverage type code.
     * 
     * @var string
     */
    protected $coverage_type;
    /**
     * The policy basis code.
     * 
     * @var string
     */
    protected $policy_basis;
    
    /**
     * The policy type code.
     * 
     * @var string
     */
    protected $policy_type;

    /**
     * The costs covered code.
     *
     * @var string
     */
    protected $costs_covered;

    /**
     * The title of the policy and the cover period joined as a string.
     *
     * @var string
     */
    protected $policy_title_cover_period;


    public function TestPolicyBasisDate($startDate, $endDate, $policyBasis, $claimID)
    {
        //return types: 0=valid, 1=date out of range, 2=incident date not set, 3=claim date not set
        if($policyBasis != null)
        {
            $sql = "SELECT CLA_DINCIDENT, CLA_DCLAIM
                  FROM CLAIMS_MAIN
                  WHERE RECORDID = :claimID";
            $claimDates = \DatixDBQuery::PDO_fetch_all($sql, array('claimID' => $claimID));
            if ($claimDates[0]['CLA_DINCIDENT'] != '')
            {
                $dateIncident = new \DateTime($claimDates[0]['CLA_DINCIDENT']);
                $dateIncident->setTime(0,0,0);
            }
            else
            {
                $claimDates[0]['CLA_DINCIDENT'] == null;
            }
            if ($claimDates[0]['CLA_DCLAIM'] != null)
            {
                $dateClaim = new \DateTime($claimDates[0]['CLA_DCLAIM']);
                $dateClaim->setTime(0,0,0);
            }
            else
            {
                $claimDates[0]['CLA_DCLAIM'] == null;
            }
            if($startDate != null)
            {
                $startDate = new \DateTime(UserDateToSQLDate($startDate));
                $startDate->setTime(0,0,0);
            }
            if ($endDate != null)
            {
                $endDate = new \DateTime(UserDateToSQLDate($endDate));
                $endDate->setTime(0,0,0);
            }
            if($startDate == null && $endDate == null)
            {
                return 0;
            }
            if($policyBasis == 'O')
            {
                if($dateIncident == null)
                {
                    return 2;
                }
                else if(($dateIncident < $startDate && $startDate != null) || ($dateIncident > $endDate && $endDate != null))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            elseif ($policyBasis == 'C')
            {
                if($dateClaim == null)
                {
                    return 3;
                }
                elseif(($dateClaim < $startDate && $startDate != null) || ($dateClaim > $endDate && $endDate != null))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
}