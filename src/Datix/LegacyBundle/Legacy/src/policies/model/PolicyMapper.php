<?php
namespace src\policies\model;

use src\framework\model\DatixEntityMapper;

/**
 * Manages the persistance of WordMergeTemplate objects.
 */
class PolicyMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\policies\\model\\Policy';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'policies';
    }

    /**
     * Creates a policy-respondent link.
     * 
     * @param int $policy_id
     * @param int $respondent_id
     */
    public function insertPolicyRespondentLink($policy_id, $respondent_id)
    {
        $this->db->setSql('INSERT INTO policy_defendant_links (policy, defendant_link_id) VALUES (?, ?)');
        return  $this->db->prepareAndExecute([$policy_id, $respondent_id]);
    }
    
    /**
     * Removes a policy-respondent link.
     * 
     * @param int $policy_id
     * @param int $respondent_id
     */
    public function deletePolicyRespondentLink($policy_id, $respondent_id)
    {
        $this->db->setSql('DELETE FROM policy_defendant_links WHERE policy = ? AND defendant_link_id = ?');
        return  $this->db->prepareAndExecute([$policy_id, $respondent_id]);
    }
    
    /**
     * Fetches the claims record linked to a policy (via respondents).
     * 
     * @param int $policy_id The policy recordid.
     * 
     * @return array The claims record data.
     */
    public function getLinkedClaims($policy_id)
    {
        $claims = [];

        if (CanSeeModule('CLA'))
        {
            $fields = $this->registry->getModuleDefs()['CLA']['FIELD_ARRAY'];

            $sql = '
                SELECT DISTINCT
                    claims_main.recordid, ' . implode(',', $fields) . '
                FROM
                    claims_main
                INNER JOIN
                    link_respondents l ON claims_main.recordid = l.main_recordid
                INNER JOIN
                    policy_defendant_links p ON l.recordid = p.defendant_link_id
                WHERE
                    p.policy = ?';

            $securityWhere = MakeSecurityWhereClause('', 'CLA');

            if ($securityWhere != '')
            {
                $sql .= ' AND '.$securityWhere;
            }

            $this->db->setSql($sql);
            $this->db->prepareAndExecute([$policy_id]);
            $claims = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
        }

        return $claims;
    }
}