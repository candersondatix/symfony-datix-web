<?php
namespace src\policies\model;

use src\framework\model\ModelFactory;

class PolicyModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new PolicyFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new PolicyMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new PolicyCollection($this->getMapper(), $this->getEntityFactory());
    }
}