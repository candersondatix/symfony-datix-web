<?php if ($this->hasInvalidPolicies): ?>
    <div class="info_div"><?=_tk('policies_inaccurate');?></div>
<?php endif; ?>
<?php echo $this->listing->GetListingHTML() ?>
<?php if ($this->userCanAddRecords) : ?>
<div class="new_link_row">
    <a href="Javascript:SendTo(scripturl+'?action=displaypolicylinkform&link_recordid=<?php echo $this->link_recordid ?><?php echo ($this->module) ? "&module=" . $this->module . "&" : ""; ?>&main_recordid=<?php echo $this->main_recordid ?>&type=<?php echo $this->type ?>&org_id=<?php echo $this->org_id ?>');">
        <b><?php echo _tk('create_a_new_policy_link') ?></b>
    </a>
</div>
<?php endif; ?>