<?php

namespace src\med\controllers;

use src\framework\controller\Controller;

class MedicationsSearchController extends Controller
{
    /**
     * Shows the Medication search form.
     */
    function medicationssearch()
    {
        if ($this->request->getParameter('searchtype') == 'lastsearch')
        {
            $med = $_SESSION['MED']['LAST_SEARCH'];
        }
        else
        {
            $med = '';
        }

        if ($this->request->getParameter('validationerror'))
        {
            $med['error']['message'] = '<br />There are invalid characters in one of the fields you have searched on.';
        }

        require_once 'Source/medications/MainMedications.php';
        ShowMedicationForm($med, 'Search');
    }
}