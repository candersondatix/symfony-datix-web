<?php

namespace src\med\controllers;

use src\framework\controller\Controller;

class NewMedicationController extends Controller
{
    /**
     * Shows the Medications new form.
     */
    function addnewmedication()
    {
        $MEDPerms = GetParm('MED_PERMS');

        if($MEDPerms && $MEDPerms != 'MED_READ_ONLY')
        {
            require_once 'Source/medications/MainMedications.php';
            ShowMedicationForm();
        }
        else
        {
            $msg = 'You do not have the necessary permissions to add new medications.';
            fatal_error($msg, 'Information');
        }
    }
}