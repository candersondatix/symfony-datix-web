<?php

namespace src\med\controllers;

use src\framework\controller\Controller;

class MedicationsCheckController extends Controller
{
    /**
     * Show the medications list when we are searching
     */
    function MedicationList()
    {
        global $dtxdebug, $ModuleDefs, $FieldDefs, $FieldDefsExtra;

        $module            = \Sanitize::getModule($this->request->getParameter('module'));
        $fieldname         = \Sanitize::SanitizeString($this->request->getParameter('fieldname'));
        $MedicationSuffix  = \Sanitize::SanitizeInt($this->request->getParameter('suffix'));
        $FieldList         = explode(",", \Sanitize::SanitizeString($this->request->getParameter('fieldlist')));
        $SearchMappingType = \Escape::EscapeEntities($this->request->getParameter('searchmappingtype'));
        $formLevel         = $_SESSION[$module]['FORMLEVEL'];
        $MedFields         = array();
        $sqlwhere          = array();

        $ParentFormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => $module,
            'level' => $formLevel,
            'id' => $this->request->getParameter('parentformid')
        ));

        $MedListingID = $ParentFormDesign->ListingDesigns['multi_medication_record']['MED'];

        $Design = new \Listings_ModuleListingDesign(array('module' => 'MED', 'listing_id' => $MedListingID));
        $Design->LoadColumnsFromDB();

        $MEDColumns = $Design->getColumns();

        foreach ($ModuleDefs['MED']['FIELD_ARRAY'] as $medfield)
        {
            $MedFields[] = $medfield . "_descr";
        }

        $MedFields = array_merge($MedFields, $ModuleDefs['MED']['FIELD_ARRAY']);

        $sql = '
            SELECT
                recordid as recordid_descr, '. implode(",", $MedFields) .'
            FROM
                vw_medications_main
            WHERE
        ';

        $FieldCriteria = str_replace("*", "%", $this->request->getParameter($SearchMappingType));
        $FieldCriteria = str_replace("?", "_", $FieldCriteria);
        $FieldCriteria = EscapeQuotes($FieldCriteria);

        foreach($FieldList as $field)
        {
            $FieldCriteria2 = str_replace("*", "%", $_POST[$field]);
            $FieldCriteria2 = str_replace("?", "_", $FieldCriteria2);
            $FieldCriteria2 = EscapeQuotes($FieldCriteria2);

            if ($FieldDefs[$module][$field]['FieldFormatsSrc'])
            {
                $FieldFormatsName = $FieldDefs[$module][$field]['FieldFormatsSrc'];
            }
            else
            {
                $FieldFormatsName = $field;
            }

            $FieldFormatsDescr = $FieldFormatsName . "_descr";

            if (!empty($FieldCriteria))
            {
                if (bYN(GetParm('MED_SEARCH_SOUNDEX', 'Y')))
                {
                    $sqlwhere[] = "soundex(" .$FieldFormatsDescr .") LIKE soundex('" . $FieldCriteria . "')";
                }

                $sqlwhere[] = "(" . $FieldFormatsDescr . " LIKE '%" . $FieldCriteria . "%' AND " . $FieldFormatsDescr . " IS NOT NULL)";
            }

            if (!empty($FieldCriteria2))
            {
                $sqlwhere2[] = $FieldFormatsName . " LIKE '" . $FieldCriteria2 . "'";

            }
        }

        if (!empty($sqlwhere2))
        {
            $sqlwhere[] .= "(" . implode(" AND ", $sqlwhere2) . ")";
        }

        if (!empty($sqlwhere))
        {
            $sql .= implode(" OR ", $sqlwhere);
        }

        $sql .= " ORDER BY med_name";

        $resultArray = \DatixDBQuery::PDO_fetch_all($sql);

        $this->response->build('src/med/views/MedicationsCheck.php', array(
            'MEDColumns'        => $MEDColumns,
            'resultArray'       => $resultArray,
            'fieldname'         => $fieldname,
            'MedicationSuffix'  => $MedicationSuffix,
            'FieldFormatsName'  => $FieldFormatsName,
            'dtxdebug'          => $dtxdebug,
            'sql'               => $sql,
            'module'            => $module,
            'ModuleDefs'        => $ModuleDefs,
            'SearchMappingType' => $SearchMappingType,
            'FieldDefsExtra'    => $FieldDefsExtra,
            'FieldDefs'        => $FieldDefs
        ));
    }
}