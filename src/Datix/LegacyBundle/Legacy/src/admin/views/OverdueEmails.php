<?php echo GetSessionMessages(); ?>
<?php foreach (array('INC', 'COM') as $module): ?>
<div id="panel-<?php echo $module; ?>" class="panel" style="display:<?php echo ($module == $this->overdueModule ? 'block' : 'none'); ?>">
    <?php echo $this->ALLTable[$module]->GetFormTable(); ?>
    <div class="padded_div">
        When clicking on the '<?php echo _tk('overdue_email_button'); ?>' button an email will be sent to all users that have been set to receive overdue notifications. The email will contain links to all the overdue incidents that the user is responsible for.
        <div style="padding:15px; text-align:center">
            <input type = "button" value="<?php echo _tk('overdue_email_button'); ?>" onClick="sendOverdueEmails('<?php echo $module ?>')" />
        </div>
        <br />
        <div class="padded_div" id="<?php echo $module ?>_overdue_progress_bar" name="<?php echo $module ?>_overdue_progress_bar" ></div>
        <div class="padded_div" id="<?php echo $module ?>_overdue_progress" name="<?php echo $module ?>_overdue_progress" ></div>
    </div>
</div>
<?php endforeach; ?>