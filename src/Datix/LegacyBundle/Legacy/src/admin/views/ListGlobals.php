<style type="text/css">
    div.ricoLG_cell {
        white-space:nowrap;
    }
</style>
<?php

require "rico/applib.php";
require "rico/plugins/php/ricoLiveGridForms.php";

if (OpenGridForm("", $this->table, $this->where))
{
    \src\admin\controllers\GlobalsTemplateController::DefineFields($this->table, $this->login);
}
else
{
    echo 'open failed';
}
CloseApp();
    
$lockBtnAction = $this->locked ? 'unlockglobals' : 'lockglobals';
$lockBtnLabel  = $this->locked ? 'Unlock globals' : 'Lock globals';
$lockBtnImg    = $this->locked ? 'unlock.png' : 'lock.png';
?>
<li>
    <div class="button_wrapper">
        <?php if ($this->table == 'globals'): ?>
            <button name="btnBackToAdmin" onClick="SendTo('<?php echo $this->scripturl ?>?action=home&module=ADM');">Back</button>
        <?php elseif ($this->table == 'user_parms'): ?>
            <button name="btnBacktoUser" onClick="SendTo('<?php echo $this->scripturl ?>?action=edituser&recordid=<?php echo \Escape::EscapeEntities($this->userId) ?>');">Back</button>
        <?php endif ?>
        <?php if ($this->centralAdmin): ?>
            <button name="btnLock" onClick="SendTo('<?php echo $this->scripturl ?>?action=<?php echo $lockBtnAction ?>');"><img src="Images/icons/<?php echo $lockBtnImg ?>" height="12" /> <?php echo $lockBtnLabel ?></button>
        <?php endif ?>
    </div>
</li>