<form name="frmPassword" action="<?php echo $this->scripturl; ?>?action=password2" method="post">
    <input type="hidden" name="pwd_action" value="<?php echo \src\security\Escaper::escapeForHTMLParameter($this->PwdAction); ?>" />
    <input type="hidden" name="fullname" value="<?php echo \src\security\Escaper::escapeForHTMLParameter($this->fullname); ?>" />
    <?php if ($this->error) : ?>
        <div class="form_error_wrapper">
            <div class="form_error"><?php echo $this->error; ?></div>
        </div>
    <?php endif; ?>
    <table class="titlebg" border="0" cellpadding="0" cellspacing="1" width="400" align="center">
        <tr>
            <td>
                <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
                    <tr>
                        <td align="left" class="rowtitlebg">
                            <font size="2"><b><?php echo _tk('user_name'); ?></b></font>
                        </td>
                        <td class="rowtitlebg">
                            <font size="2">
                                <input type="hidden" name="user" size="20" value="<?php echo $this->user; ?>" />
                                <?php echo \src\security\Escaper::escapeForHTML($this->user); ?>
                            </font>
                        </td>
                    </tr>
                    <?php if ($this->PwdAction == "Change" || $this->PwdAction == "Expired") :?>
                        <tr>
                            <td align="left" class="rowtitlebg">
                                <font size="2"><b><?php echo _tk('old_pwd'); ?></b></font>
                            </td>
                            <td class="rowtitlebg">
                                <font size="2"><input type="password" name="old_passwrd" size="20" /></font>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td align="left" class="rowtitlebg">
                            <font size="2"><b><?php echo _tk('new_pwd'); ?></b></font>
                        </td>
                        <td class="rowtitlebg">
                            <font size="2"><input type="password" name="passwrd" size="20" autocomplete="off" /></font>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="rowtitlebg">
                            <font size="2"><b><?php echo _tk('retype_pwd'); ?></b></font>
                        </td>
                        <td class="rowtitlebg">
                            <font size="2"><input type="password" name="retype_passwrd" size="20" /></font>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" class="titlebg">
                            <input type="submit" value="<?php echo _tk('btn_set_pwd'); ?>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
<script language="javascript" type="text/javascript">
    <?php if ($this->PwdAction == "Set") : ?>
        document.frmPassword.passwrd.focus();
    <?php elseif ($this->PwdAction == "Change" || $this->PwdAction == "Expired") : ?>
    document.frmPassword.old_passwrd.focus();
    <?php endif; ?>
</script>