<form enctype="multipart/form-data" method="post" name="frmDocument" action="<?php echo $this->scripturl; ?>?action=importcodesetup&module=<?php echo $this->Module; ?>&fieldname=<?php echo $this->FieldName; ?>&table=<?php echo $this->Table; ?>">
    <input type="hidden" name="rbWhat" value="import" />
    <?php if ($this->error) : ?>
    <div class="form_error">The form you submitted contained the following errors.  Please correct them and try again.</div>
    <div class="form_error"><?php echo $this->error['message']; ?></div>
    <?php endif; ?>
    <tr>
        <td><?php echo $this->CTable->GetFormTable(); ?></td>
    </tr>
    <div class="button_wrapper">
        <input type="submit" value="Import" onclick="document.frmDocument.rbWhat.value='save';" />
        <input type="submit" value="Back" onclick="document.frmDocument.rbWhat.value='cancel';" />
    </div>
</form>