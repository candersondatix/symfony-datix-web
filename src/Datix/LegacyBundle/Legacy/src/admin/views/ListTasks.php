<?php foreach ($this->Listings as $Module => $Listing): ?>
<div class="panel" style="display:<?php echo ($Module == $this->CurrentModule ? 'block' : 'none') ?>" id="panel-<?php echo $Module ?>">
    <div class="padded_div">
        <?php echo $Listing->GetListingHTML() ?>
    </div>
    <div style="background-color: #F0F0F0; padding:5px"><a href = "<?php echo $this->scripturl ?>?action=task&amp;module=<?php echo $Module ?>"><b><?php echo _tk('add_new_task') ?></b></a></div>
</div>
<?php endforeach; ?>