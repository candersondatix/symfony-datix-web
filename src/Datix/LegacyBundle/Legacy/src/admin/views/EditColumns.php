<script language="javascript" type="text/javascript">
    var NS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) < 5);

    function addToListing()
    {
        jQuery('#columns_list option:selected').remove().appendTo('#columns_list2').attr('selected', false);
        reDrawPreview();
    }

    function removeFromListing()
    {
        jQuery('#columns_list2 option:selected').remove().appendTo('#columns_list').attr('selected', false);
        sortListBox(document.getElementById('columns_list'));
        reDrawPreview();
    }
</script>
<form id="editcol" name="editcol" method="post" action="<?php echo $this->scripturl; ?>?action=editcoloptions">
    <input type="hidden" id="module" name="module" value="<?php echo Sanitize::SanitizeString($this->module); ?>" />
    <input type="hidden" id="form_id" name="form_id" value="<?php echo Sanitize::SanitizeInt($this->listing_id); ?>" />
    <input type="hidden" id="rbWhat" name="rbWhat" value="Save" />
    <ol>
        <li class="section_title_row"><b>Preview:</b></li>
        <li id="listing_preview_table_wrapper"><?php echo $this->Listing->GetListingPreviewHTML(); ?></li>
        <li class="section_title_row"><b>Edit Listing:</b></li>
        <li>
            <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
                <tr>
                    <td class="windowbg2" align="left">
                        Available columns:
                    </td>
                    <td class="windowbg2" align="left"></td>
                    <td class="windowbg2" align="left">
                        Current columns:
                    </td>
                </tr>
                <tr>
                    <td class="windowbg2" align="center">
                        <select id="columns_list" name="columns_list[]" multiple="multiple" size="12" id="columns_list" <?php echo ($this->Listing->ReadOnly ? 'disabled ' : ''); ?> style="width: 300px">
                            <?php foreach ($this->availCols as $Code => $Val) : ?>
                                <?php if(!array_key_exists($Code, $this->currentCols)) : ?>
                                    <?php if(is_array($list_columns_mandatory) && in_array($Code, $list_columns_mandatory)) : ?>
                                        <option value="<?php echo $Code; ?>" style="color: red;"><?php echo $Val; ?> *</option>
                                    <?php else : ?>
                                        <option value="<?php echo $Code; ?>"><?php echo $Val; ?></option>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td class="windowbg2" align="center">
                        <?php if (!$this->Listing->ReadOnly) : ?>
                        <input type="button" value="Add     >>>" onclick="addToListing();" /><br /><br />
                        <input type="button" value="<<<Remove" onclick="removeFromListing();" />
                        <?php endif; ?>
                    </td>
                    <td class="windowbg2" align="center">
                        <table>
                            <tr>
                                <td>
                                    <img src="Images/up_windowbg2.gif" border="0" onclick="moveUp('columns_list2');"/> <br />
                                    <img src="Images/collapse_windowbg2.gif" border="0" onclick="moveDown('columns_list2');"/>
                                </td>
                                <td>
                                    <select id="columns_list2" name="columns_list2[]" multiple="multiple" size="12" id="columns_list2" <?php echo ($this->Listing->ReadOnly ? 'disabled ' : ''); ?> style="width: 300px">
                                        <?php foreach($this->currentCols as $Code => $Val) : ?>
                                            <?php if(is_array($list_columns_mandatory) && in_array($Code, $list_columns_mandatory)) : ?>
                                                <option value="<?php echo $Code; ?>" style="color: red;"><?php echo $Val; ?> *</option>
                                            <?php else :?>
                                                <option value="<?php echo $Code; ?>"><?php echo $Val; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </li>
        <li id="default_listing_row" class="padded_div">
            <?php if($this->Listing->isDefault()) : ?>
            This is the default listing
            <?php else : ?>
            <a href="#" onclick="ForceSetGlobal('<?php echo $this->module; ?>_LISTING_ID', '<?php echo $this->listing_id; ?>'); jQuery('#default_listing_row').html('This is the default listing');">Make this the default listing</a>
            <?php endif; ?>
        </li>
        <li>
            <div class="button_wrapper">
                <?php if (!$this->Listing->ReadOnly) : ?>
                <input type="submit" value="Save settings" border="10" class="button" name="btnSaveDesign" onClick="selectAllMultiCodes(); this.form.rbWhat.value='Save';this.form.action='app.php?action=savecolumns';this.form.submit();">
                <input type="button" value="Reset to template" border="10" class="button" name="btnReset" onClick="if(confirm('Are you sure you want to reset this form?')){selectAllMultiCodes();this.form.rbWhat.value='Reset';this.form.action='app.php?action=resetcolumns';this.form.submit();}">
                <input type="button" value="Delete" border="10" class="button" name="btnReset" onClick="if(confirm('Are you sure you want to delete this form?')){selectAllMultiCodes();this.form.rbWhat.value='Delete';this.form.action='app.php?action=deletelistingdesign';this.form.submit();}">
                <?php endif; ?>
                <input type="button" value="Cancel" border="10" class="button" name="btnSaveDesign" onClick="if(confirm('Are you sure you want to cancel?')){SendTo('<?php echo $this->scripturl; ?>?action=listlistingdesigns&module=<?php echo $this->module; ?>');}">
            </div>
        </li>
    </ol>
</form>