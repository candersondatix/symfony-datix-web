<?php

    echo GetSessionMessages();

    $baseUrl = $this->scriptUrl . '?action=usersessions';
    $ApplicationArray = array("WEB" => "Web Application", "MAIN" => "Main Application");

?>
<?php if(count($this->resultArray) > 0): ?>
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
  <?php if(!empty($proc_result)): ?>
  <tr>
    <td class="windowbg2" colspan="4"><b>Session Ended</b></td>
  </tr>
  <?php endif ?>
  <tr class="tableHeader">
    <th><a class="<?php echo (($this->order=='recordid') ? 'sort ' . \UnicodeString::strtolower($this->dir) : '') ?>" href="<?php echo $baseUrl ?>&amp;order=recordid&dir=<?php echo (($this->dir=='ASC') ? 'DESC' : 'ASC') ?>">Session ID</a></td>
    <th><a class="<?php echo (($this->order=='fullname') ? 'sort ' . \UnicodeString::strtolower($this->dir) : '') ?>" href="<?php echo $baseUrl ?>&amp;order=fullname&dir=<?php echo (($this->dir=='ASC') ? 'DESC' : 'ASC') ?>">User</a></td>
    <th><a class="<?php echo (($this->order=='ses_application') ? 'sort ' . \UnicodeString::strtolower($this->dir) : '') ?>" href="<?php echo $baseUrl ?>&amp;order=ses_application&dir=<?php echo (($this->dir=='ASC') ? 'DESC' : 'ASC') ?>">Application</a></td>
    <th><a class="<?php echo (($this->order=='ses_start') ? 'sort ' . \UnicodeString::strtolower($this->dir) : '') ?>" href="<?php echo $baseUrl ?>&amp;orderby=ses_start&dir=<?php echo (($this->dir=='ASC') ? 'DESC' : 'ASC') ?>">Last Active</a></td>
    <th class="titlebg"></td>
  </tr>
  <?php foreach($this->resultArray as $row): ?>
  <tr class="listing-row">
    <td class="<?php echo (($this->order=='recordid') ? 'sort ' : '') ?>"><?php echo $row["recordid"] ?></td>
    <td class="<?php echo (($this->order=='fullname') ? 'sort ' : '') ?>"><?php echo $row["fullname"] ?></td>
    <td class="<?php echo (($this->order=='ses_application') ? 'sort ' : '') ?>"><?php echo $ApplicationArray[$row["ses_application"]] ?></td>
    <td class="<?php echo (($this->order=='ses_start') ? 'sort ' : '') ?>"><?php echo FormatDateVal($row["ses_start"]).' '.date("H:i:s",strtotime($row["real_time"])) ?></td>
    <td><a href="<?php echo $baseUrl ?>&delete_session=<?php echo $row["recordid"] ?>">[end session]</a></td>
  </tr>
  <?php endforeach ?>
</table>
<?php else: ?>
<div style="text-align:center;border:1px solid #6394bd;padding:10px"><b>There are currently no user sessions logged.</b></div>
<?php endif ?>