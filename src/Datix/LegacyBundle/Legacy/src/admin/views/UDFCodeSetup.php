<?php
require_once 'rico/applib.php';
require_once 'rico/plugins/php/ricoLiveGridForms.php';
?>

<?php
    if (OpenGridForm('', $this->table, $this->where, $this->mode))
    {
        \src\admin\controllers\UDFCodeSetupTemplateController::DefineFields($this->udf_id);
    }
    else
    {
        echo 'open failed';
    }

    CloseApp();
?>


    <li>
	    <div class="button_wrapper">
	        <input type="button" value="Back" border="10" name="btnSaveDesign" onClick="SendTo('<?php echo $this->scripturl; ?>?action=editextrafield&recordid=<?php echo $this->udf_id; ?>');">
	    </div>
    </li>
    </ol>

    </form>
 
