<?php echo GetSessionMessages(); ?>
<?php if (count($this->resultArray) > 0) : ?>
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <tr class="tableHeader">
        <th class="titlebg">Record ID</th>
        <th class="titlebg">Table</th>
        <th class="titlebg">User</th>
        <th class="titlebg">Last Active</th>
        <th class="titlebg"></th>
    </tr>

    <?php foreach ($this->resultArray as $row) : ?>
        <tr class="listing-row">
            <td><?php echo \src\security\Escaper::escapeForHTML($row['lck_linkid']); ?></td>
            <td><?php echo \src\security\Escaper::escapeForHTML($row['lck_table']); ?></td>
            <td><?php echo \src\security\Escaper::escapeForHTML($row['fullname']); ?> (session #<?php echo \src\security\Escaper::escapeForHTML($row['lck_sessionid']); ?>)</td>
            <td><?php echo \src\security\Escaper::escapeForHTML(FormatDateVal($row['lck_time'])); ?> <?php echo \src\security\Escaper::escapeForHTML(date('H:i:s', strtotime($row['real_time']))); ?></td>
            <td><a href="<?php echo $this->scripturl; ?>?action=lockedrecords&delete_lock=<?php echo \src\security\Escaper::escapeForHTML($row['recordid']); ?>">[unlock record]</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<?php else : ?>
    <?php if($_SESSION['AdminUser']) : ?>
        <div style="text-align:center;border:1px solid #6394bd;padding:10px"><b>There are currently no records locked.</b></div>
    <?php else : ?>
        <div style="text-align:center;border:1px solid #6394bd;padding:10px"><b>You currently have no records locked.</b></div>
    <?php endif; ?>
<?php endif; ?>