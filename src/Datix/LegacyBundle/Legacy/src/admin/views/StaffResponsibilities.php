<script language="javascript" type="text/javascript">
    function getRecords(initials, printmode)
    {
        new Ajax.Updater
            (
                initials,
                scripturl+'?action=httprequest&type=staffrespresponse',
                {
                    parameters:
                    {
                        initial : initials,
                        print : printmode
                    }
                }
            );
    }
</script>
<?php foreach($this->MatchingStaff as $initials => $name): ?>
    <div class="show-staff-title" onclick="jQuery('#<?php echo $initials ?>').toggle();getRecords('<?php echo $initials ?><?php echo ($this->PrintMode == true ? ", '1'" : "") ?>')">
        <b>
            <img hspace="2" id="twisty_<?php echo $initials ?>" src="Images/icons/icon_user16x16.png" alt="*" border="0" />
            <?php echo $name ?>
        </b>
    </div>
    <div id="<?php echo $initials ?>" style="display:<?php echo ($this->PrintMode ? 'block' : 'none') ?>">
        <!-- HTML output from AJAX response comes here -->
    </div>
<?php endforeach; ?>