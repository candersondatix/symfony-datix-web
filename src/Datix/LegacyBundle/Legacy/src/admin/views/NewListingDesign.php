<script language="Javascript" type="text/javascript">
    function validateOnSubmit()
    {
        if (document.forms[0].formaction.value == "cancel")
        {
            return true;
        }

        if (!validateEmpty(document.forms[0].form_name.value))
        {
            moveScreenToField('form_name', '');
            alert("You must enter the name of the listing");
            return false;
        }

        return true;
    }
</script>
<form method="post" action="<?php echo $this->scripturl; ?>?action=savenewlistingdesign" name="newform" onSubmit="return(validateOnSubmit());">
    <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
        <tr>
            <td class="windowbg2" width="30%"><b>Name of listing</b></td>
            <td class="windowbg2" width="70%"><input type="text" size="40" name="form_name"></td>
        </tr>
        <tr>
            <td class="windowbg2" width="30%"><b>Copy design from which listing?</b></td>
            <td class="windowbg2" width="70%">
                <select name="copy_from">
                    <option value="0">Datix Listing Design Template</option>
                    <?php foreach ($this->ListingDesigns as $Details) : ?>
                    <option value="<?php echo htmlspecialchars($Details['recordid']); ?>"><?php echo htmlspecialchars($Details['LST_TITLE']); ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="titlebg" align="center" colspan="2">
                <input type="hidden" name="formaction" value="cancel">
                <input type="hidden" name="module" value="<?php echo $this->module; ?>">
                <input type="submit" value="<?php echo _tk('btn_save_new_listing')?>" onclick="document.forms[0].formaction.value='save';">
                <input type="submit" value="<?php echo _tk('btn_cancel')?>" onclick="document.forms[0].formaction.value='cancel';">
            </td>
        </tr>
    </table>
</form>