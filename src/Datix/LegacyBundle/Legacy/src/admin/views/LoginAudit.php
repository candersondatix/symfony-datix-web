<?php
require_once 'rico/applib.php';
require_once 'rico/plugins/php/ricoLiveGridForms.php';
?>

<?php
    if (OpenGridForm('', $this->table, $this->where, 'r'))
    {
        \src\admin\controllers\LoginAuditTemplateController::DefineFields();
    }
    else
    {
        echo 'open failed';
    }

    CloseApp();
?>
        <li>
            <div class="button_wrapper">
                <?php if ($this->data['SetupType'] == 'Full') : ?>
                    <input type="button" value="Back" border="10" name="btnBack" onClick="SendTo('<?php echo $this->scripturl; ?>?action=home&module=ADM');">
                <?php elseif ($this->data['SetupType'] == 'User') : ?>
                    <input type="button" value="Back" border="10" class="button" name="btnBacktoUser" onClick="SendTo('<?php echo $this->scripturl; ?>?action=edituser&recordid=<?php echo Sanitize::SanitizeString($this->user); ?>');">
                <?php endif; ?>
            </div>
        </li>
    </ol>
</form>