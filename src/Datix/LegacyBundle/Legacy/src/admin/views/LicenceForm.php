<?php if ($this->invalid == '1' && empty($_SESSION['MESSAGES']['ERROR'])) : ?>
    <div class="error_div"><?php echo _tk('err_licence_invalid'); ?></div>
<?php endif; ?>
<form name="frmLicence" action="<?php echo $this->scripturl; ?>?action=licence" method="post">
    <input type="hidden" name="rbWhat" value="save" />
    <div class="new_windowbg2"><ol><?php echo $this->LicenceTable->GetFormTable(); ?></ol></div>
</form>