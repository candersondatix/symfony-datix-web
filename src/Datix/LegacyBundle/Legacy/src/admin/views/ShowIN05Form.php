<?php
require_once 'rico/applib.php';
require_once 'rico/plugins/php/ricoLiveGridForms.php';
?>
<script type="text/javascript">
    function generate()
    {
        displayLoadPopup();

        jQuery.get(scripturl+"?action=httprequest&type=countin05&responsetype=json", function(data) {
            if (confirm("There are " + data + " relevant Type, Category and Sub category combinations. Do you wish to continue?"))
            {
                displayLoadPopup();

                jQuery.get(scripturl+"?action=httprequest&type=generatein05&responsetype=json", function(data) {
                    hideLoadPopup();
                    alert("Completed: " + data + " new records generated.")
                });
            }
            else
            {
                hideLoadPopup();
            }
        });
    }

    function npsa_map_InitComplete()
    {
        jQuery("#<?php echo $this->carestage; ?>_title").data({field: "inc_carestage", fieldId: "<?php echo $this->carestage; ?>", module: "INC", table: "incidents_main", fieldmode: "edit", children: ["<?php echo $this->clindetail; ?>"], showCodes: true, "noresize": true});
        jQuery("#<?php echo $this->clindetail; ?>_title").data({field: "inc_clin_detail", fieldId: "<?php echo $this->clindetail; ?>", module: "INC", table: "incidents_main", fieldmode: "edit", parents: ["<?php echo $this->carestage; ?>"], children: ["<?php echo $this->clintype; ?>"], showCodes: true, "noresize": true});
        jQuery("#<?php echo $this->clintype; ?>_title").data({field: "inc_clintype", fieldId: "<?php echo $this->clintype; ?>", module: "INC", table: "incidents_main", fieldmode: "edit", parents: ["<?php echo $this->clindetail; ?>"], showCodes: true, "noresize": true});
    }

    function OnChange_<?php echo $this->carestage; ?>()
    {
        jQuery("#<?php echo $this->clindetail; ?>_title").checkClearChildField(false);
    }

    function OnChange_<?php echo $this->clindetail; ?>()
    {
        jQuery("#<?php echo $this->clintype; ?>_title").checkClearChildField(false);
    }
</script>

<?php
    if (OpenGridForm('', 'npsa_map', 'npsa_type = \'IN05\''))
    {
        \src\admin\controllers\IN05TemplateController::DefineFields($this->showDesc);
    }
    else
    {
        echo 'open failed';
    }

    CloseApp();
?>
        <li>
            <div class="button_wrapper">
                <input type="button" value="Generate" border="10" class="button" name="btnGenerate" onClick="generate()">
                <input type="button" value="<?php echo ($this->showDesc ? 'Hide Descr.' : 'Show Descr.'); ?>" border="10" class="button" name="btnDescr" onClick="SendTo('<?php echo $this->scripturl; ?>?action=in05<?php echo ($this->showDesc ? '' : '&showdesc=y'); ?>');">
                <input type="button" value="Back" border="10" class="button" name="btnBack" onClick="SendTo('<?php echo $this->scripturl; ?>?action=home&module=ADM');">
            </div>
        </li>
    </ol>
</form>