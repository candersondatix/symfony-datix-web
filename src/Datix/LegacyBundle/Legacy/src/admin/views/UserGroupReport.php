<table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
    <?php if (count($this->resultArray) == 0) : ?>
    <tr><td class="windowbg2" colspan="<?php echo $this->colspan; ?>">No users in this group.</td></tr>
    <?php else : ?>
    <tr>
        <td class="windowbg" colspan="<?php echo $this->colspan; ?>">
            <b><?php echo count($this->resultArray); ?> users.</b>
        </td>
    </tr>
    <tr class="tableHeader head2">
        <?php if(count($this->resultArray)) : ?>
        <th class="windowbg" width="15%"><b>Surname</b></th>
        <th class="windowbg" width="15%"><b>Forenames</b></th>
        <th class="windowbg" width="10%"><b>Login name</b></th>
        <th class="windowbg" width="10%"><b>User role</b></th>
        <th class="windowbg" width="5%"><b>Status</b></th>
        <?php endif; ?>
    </tr>
    <?php foreach($this->resultArray as $row) : ?>
    <tr>
        <?php if (!$this->PrintMode) : ?>
        <td class="windowbg2" valign="middle" align="left">
            <a href="<?php echo $row['con_url']; ?>"><?php echo $row['sta_surname']; ?></a>
        </td>
        <td class="windowbg2" valign="middle" align="left">
            <a href="<?php echo $row['con_url']; ?>"><?php echo $row['sta_forenames']; ?></a>
        </td>
        <td class="windowbg2" valign="middle" align="left">
            <a href="<?php echo $row['con_url']; ?>"><?php echo $row['login']; ?></a>
        </td>
        <td class="windowbg2" valign="middle" align="left">
            <a href="<?php echo $row['con_url']; ?>"><?php echo $row['perm_value']; ?></a>
        </td>
        <td class="windowbg2" valign="middle" align="left">
            <a href="<?php echo $row['con_url']; ?>"><?php echo ($row['status'] ? 'Active' : 'Inactive'); ?></a>
        </td>
        <?php else : ?>
        <td class="windowbg2" valign="middle" align="left"><?php echo $row["sta_surname"]; ?></td>
        <td class="windowbg2" valign="middle" align="left"><?php echo $row["sta_forenames"]; ?></td>
        <td class="windowbg2" valign="middle" align="left"><?php echo $row["login"]; ?></td>
        <td class="windowbg2" valign="middle" align="left"><?php echo $row["perm_value"]; ?></td>
        <td class="windowbg2" valign="middle" align="left"><?php echo ($row['status'] ? 'Active' : 'Inactive'); ?></td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>