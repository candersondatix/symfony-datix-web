<div style="float:left; margin: 5px 10px 0px 0px">Form:</div>
<?php echo $this->subformField->getField(); ?>
<?php
    require "rico/applib.php";
    require "rico/plugins/php/ricoLiveGridForms.php";
    require "Source/setups/ComboLinking.php";

    if (is_numeric($_GET['subform']))
    {
        if (OpenGridForm('', 'combo_links', 'cmb_form_id = '.$_GET['subform']))
        {
            DefineFields();
        }
        else
        {
            echo 'open failed';
        }
        CloseApp();
    }
    else
    {
        echo '<div style="height:5px;"></div>';
    }
?>
<div class="button_wrapper">
    <input type="button" value="Back" border="10" class="button" name="btnSaveDesign" onClick="SendTo('<?php echo $this->scripturl ?>?action=home&module=ADM');">
</div>
<script type="text/javascript">
    function setForm()
    {
        SendTo(scripturl+"?action=listcombolinks&subform="+jQuery("#subform").val());
    }

    function validate()
    {
        var error  = "",
            child  = jQuery("#combo_links_4").val(),
            parent = jQuery("#combo_links_6").val();

        if (child == "")
        {
            error = "<?php echo htmlspecialchars(_tk('combo_linking_child_error'), ENT_QUOTES) ?>";
        }
        else if (parent == "")
        {
            error = "<?php echo htmlspecialchars(_tk('combo_linking_parent_error'), ENT_QUOTES) ?>";
        }
        else if (child == parent)
        {
            error = "<?php echo htmlspecialchars(_tk('combo_linking_same_error'), ENT_QUOTES) ?>";
        }

        return error;
    }
</script>