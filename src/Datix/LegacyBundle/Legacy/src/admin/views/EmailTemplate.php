<script language='javascript' src='email<?php echo $this->addMinExtension; ?>.js'></script>
<form method="post" action="<?php echo $this->scripturl ?>?action=emailtemplate">
    <input type="hidden" name="rbWhat" />
    <input type="hidden" name="updateid" value="<?php echo $this->updateid ?>" />
<?php echo $this->form->GetFormTable() ?>
<?php if ($this->mode != 'ReadOnly'): ?>
    <div class="button_wrapper">
        <input type="button" value="Save" onclick="this.form.rbWhat.value='Save';if (emt_validateOnSubmit()){this.form.submit()}" />
    <?php if (isset($this->id)): ?>
        <input type="button" value="Delete" onclick="if (confirm('<?php echo _tk('delete_confirm') ?>')){this.form.rbWhat.value='Delete';this.form.submit()}" />
    <?php endif ?>
    </div>
<?php endif ?>
</form>