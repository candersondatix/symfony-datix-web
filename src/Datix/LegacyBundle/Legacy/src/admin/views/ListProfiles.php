<script language="JavaScript" type="text/javascript">
    function dnLDAPProfileSyncAllPopup()
    {
        mywindow = open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?php echo $this->scripturl; ?>?action=ldapprofilesyncall&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }

    function dnLDAPProfileSyncExistingPopup()
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?php echo $this->scripturl; ?>?action=ldapprofilesyncall&updateonly=1&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }

    function dnLDAPProfileSyncPopup(pfl_id)
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?php echo $this->scripturl; ?>?action=ldapprofilesync&profileid=' + pfl_id + '&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }

    function dnLDAPProfileUpdateOnly(pfl_id)
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?php echo $this->scripturl; ?>?action=ldapprofilesync&profileid=' + pfl_id +'&updateonly=1&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }
</script>

<form method="post" name="frmProfileOrder" action="<?php echo $this->scripturl; ?>?action=saveprofileorder">

    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
        <tr class="tableHeader">
            <th class="windowbg" width="10%" align="left"><a href="<?php echo $this->scripturl; ?>?action=listprofiles&sort=recordid&order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->sort == 'recordid' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">ID</a></th>
            <th class="windowbg" width="20%" align="left"><a href="<?php echo $this->scripturl; ?>?action=listprofiles&sort=pfl_name&order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->sort == 'pfl_name' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">Name</a></th>
            <th class="windowbg" width="45%" align="left"><a href="<?php echo $this->scripturl; ?>?action=listprofiles&sort=pfl_description&order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->sort == 'pfl_description' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">Description</a></th>

            <?php if ($this->ldap->LDAPEnabled() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS'): ?>
                <th class="windowbg" width="5%">Priority</th>
                <th class="windowbg" width="10%"></th>
                <th class="windowbg" width="10%"></th>
            <?php endif ?>
        </tr>

        <?php foreach ($this->data as $profile): ?>
        <?php $link = $this->scripturl.'?action=editprofile&recordid='.$profile->recordid ?>

            <tr class="listing-row">
                <td class="windowbg2<?php echo (($this->sort == 'recordid') ? ' sort' : '') ?>"><a href="<?php echo $link ?>" ><?php echo htmlspecialchars($profile->recordid) ?></a></td>
                <td class="windowbg2<?php echo (($this->sort == 'pfl_name') ? ' sort' : '') ?>"><a href="<?php echo $link ?>" ><?php echo htmlspecialchars($profile->pfl_name) ?></a></td>
                <td class="windowbg2<?php echo (($this->sort == 'pfl_description') ? ' sort' : '') ?>"><a href="<?php echo $link ?>" ><?php echo htmlspecialchars($profile->pfl_description) ?></a></td>

                <?php if ($this->ldap->LDAPEnabled() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS'): ?>
                    <?php if ($this->ldaporderedit): ?>
                        <td class="windowbg2" align="center" width="8%"><input type="text" maxlength="9" size="9" name="profile_order_<?php echo htmlspecialchars($profile->recordid) ?>" value="<?php echo htmlspecialchars($profile->pfl_ldap_order) ?>"></td>
                    <?php else: ?>
                        <td class="windowbg2" align="center" width="8%"><a href="<?php echo $link ?>" ><?php echo htmlspecialchars($profile->pfl_ldap_order) ?></td>
                    <?php endif ?>

                    <td class="windowbg2" align="center" width="10%"><a href="javascript:dnLDAPProfileSyncPopup(<?php echo htmlspecialchars($profile->recordid); ?>)"><?php echo _tk('ldap_sync_all_group') ?></a></td>
                    <td class="windowbg2" align="center" width="10%"><a href="javascript:dnLDAPProfileUpdateOnly(<?php echo htmlspecialchars($profile->recordid); ?>)"><?php echo _tk('ldap_sync_existing_group') ?></a></td>
                <?php endif ?>
            </tr>
        <?php endforeach ?>

        <?php if ($this->ldap->LDAPEnabled() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS'): ?>
            <?php if ($this->ldaporderedit): ?>
                <tr>
                    <td class="windowbg2"></td>
                    <td class="windowbg2"></td>
                    <td class="windowbg2"></td>
                    <td class="windowbg2" align="center" colspan="1"><a href="javascript:document.frmProfileOrder.submit();" ><?php echo _tk('save_ldap_order') ?></a></td>
                    <td class="windowbg2"></td>
                    <td class="windowbg2"></td>
                </tr>
            <?php else: ?>
                <tr>
                    <td class="windowbg2"></td>
                    <td class="windowbg2"></td>
                    <td class="windowbg2"></td>
                    <td class="windowbg2" align="center" colspan="1"><a href="<?php echo $this->scripturl ?>?action=listprofiles&edit=1" ><?php echo _tk('edit_ldap_order') ?></a></td>
                    <td class="windowbg2"></td>
                    <td class="windowbg2"></td>
                </tr>
            <?php endif ?>
            <tr>
                <td class="windowbg" align="left" colspan="6"><a href="javascript:dnLDAPProfileSyncAllPopup()" ><?php echo _tk('ldap_sync_all_profiles') ?></a></td>
            </tr>
            <tr>
                <td class="windowbg" align="left" colspan="6"><a href="javascript:dnLDAPProfileSyncExistingPopup()" ><?php echo _tk('ldap_sync_existing_profiles') ?></a></td>
            </tr>
        <?php endif ?>
    </table>
</form>