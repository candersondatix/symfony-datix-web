<script language="Javascript">
    var submitClicked = false;
    function copyProfile()
    {
        if(confirm('<?php echo _tk('copy_profile_confirm')?>'))
        {
            jQuery('#frmProfile').attr('action', '<?php echo $this->scripturl ?>?action=copyprofile');
            selectAllMultiCodes();
            $('frmProfile').submit();
        }
    }
    function deleteProfile()
    {
        if(confirm('<?php echo _tk('delete_profile_confirm')?>'))
        {
            jQuery('#frmProfile').attr('action', '<?php echo $this->scripturl ?>?action=deleteprofile');
            $('frmProfile').submit();
        }
    }

    <?php echo MakeJavaScriptValidation('ADM', $FormDesign, array('pfl_name' => 'Name', 'pfl_description' => 'Description')) ?>

</script>

<form method="post" name="frmProfile" id="frmProfile" enctype="multipart/form-data" action="<?php echo $this->scripturl ?>?action=saveprofile">

    <input type="hidden" id="updateid" name="updateid" value="<?php echo htmlspecialchars($this->data['updateid']) ?>" />
    <input type="hidden" id="recordid" name="recordid" value="<?php echo \Sanitize::SanitizeInt($this->data['recordid']) ?>" />
    <input type="hidden" name="redirect_url" id="redirect_url" value="" />

    <?php echo $this->Table->GetFormTable(); ?>

    <?php echo $this->ButtonGroup->getHTML(); ?>

</form>

<?php echo JavascriptPanelSelect(false,'details',$this->Table); ?>