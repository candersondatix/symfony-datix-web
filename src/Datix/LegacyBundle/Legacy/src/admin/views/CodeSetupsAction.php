<?php
require_once 'rico/applib.php';
require_once 'rico/plugins/php/ricoLiveGridForms.php';


echo '<script type="text/javascript">var codeSetups = ' . json_encode($this->jsSettings) . ', CCS2CodeTables = ' . json_encode($this->CCS2CodeTables) . ';</script>';
echo '<script type="text/javascript" src="js_functions/code-setups-edit-codes' . $this->addMinExtension . '.js"></script>';

// Create JS functions for each mapping columns of type multicode.
$FieldSetupMappings = $this->FieldSetupMappings;
$Parents = $this->Parents;
$code_table = $this->code;
$fmt_table = $this->table;
$module = $this->module;
$codefieldname = $this->codefieldname;
require_once 'Source/setups/views/MakeEditCodesJs.php';
require_once 'Source/classes/Setups/CodeSetupClass.php';

?>

<style type="text/css">
div.ricoLG_cell {
  white-space:nowrap;
}
</style>
<!--[if lte IE 7]>
<style type="text/css">
.button_wrapper
{
    padding-bottom: 20px;
}
</style>
<![endif]-->

<?php

// Locked fields will only be editable by central admin
$CodeSetup = new \Setups_CodeSetup($this->codefieldname, $this->table);
$CodeSetup->useSystemWideFieldNames();
$isCodeLocked = $CodeSetup->isCodeFieldLocked();

$mode = (IsCentrallyAdminSys() && $isCodeLocked && !IsCentralAdmin()) ? 'r' : 'rw';

if ($this->readOnly)
{
    $mode = 'r';
}

if (OpenGridForm("", $this->code, $this->code_where, $mode))
{
    \src\admin\controllers\CodeSetupsActionTemplateController::DefineFields($this->module, $this->codefieldname, $this->code, $this->code_type, $this->Parents, $this->table);
}
else
{
  echo 'open failed';
}

CloseApp();

?>

		<li>
			<div class="button_wrapper">
                <?php if ($this->mappedCode && $this->recordId !== false) : ?>
                <input type="button" value="Back" border="10" name="btnSaveDesign" onclick="SendTo('app.php?action=editextrafield&recordid=<?php echo $this->recordId; ?>');" />
                <?php else : ?>
				<?php if ($this->table): ?>
                    <?php if (IsCentrallyAdminSys() && IsCentralAdmin()) :?>
                      <?php if ($isCodeLocked) :?>
                    <button onclick="SendTo('app.php?action=unlockcodesetup&amp;module=<?php echo $this->module ?>&amp;fieldname=<?php echo $this->codefieldname ?>&amp;table=<?php echo $this->table ?>&amp;code=<?php echo $this->code; ?>');"><img height="12px" src="Images/icons/unlock.png"> <?php echo _tk('unlock_code_setups') ?></button>
                      <?php else: ?>
                    <button onclick="SendTo('app.php?action=lockcodesetup&amp;module=<?php echo $this->module ?>&amp;fieldname=<?php echo $this->codefieldname ?>&amp;table=<?php echo $this->table ?>&amp;code=<?php echo $this->code; ?>');"><img height="12px" src="Images/icons/lock.png"> <?php echo _tk('lock_code_setups') ?></button>
                      <?php endif ?>
                    <?php endif ?>
                    <?php if ($this->ShowExportImport) : ?>
                        <input type="button" value="<?php echo _tk('export_code_setups') ?>" onclick="SendTo('app.php?action=exportcodesetup&amp;module=<?php echo $this->module ?>&amp;fieldname=<?php echo $this->codefieldname?>&amp;table=<?php echo $this->table ?>');" />

                        <?php if (!$isCodeLocked || IsCentralAdmin()): ?>
                        <input type="button" value="<?php echo _tk('import_code_setups') ?>" onclick="SendTo('app.php?action=importcodesform&amp;module=<?php echo $this->module ?>&amp;fieldname=<?php echo $this->codefieldname ?>&amp;table=<?php echo $this->table ?>&amp;code=<?php echo $this->code; ?>');" />
                        <?php else: ?>
                        <input type="button" value="<?php echo _tk('import_code_setups') ?>" disabled />
                        <?php endif ?>
                    <?php endif; ?>
				<?php endif ?>
			    <?php if ($this->codefieldname == 'inc_clintype'): ?>
			    <input type="button" value="<?php echo _tk('identify_obsolete_nrls_mappings') ?>" onclick="SendTo('app.php?action=exportobsoletenpsamappings&amp;module=<?php echo $this->module ?>');" />
			    <?php endif; ?>
			    <input type="button" value="Back" border="10" name="btnSaveDesign" onclick="SendTo('app.php?action=codesetupslist&amp;module=<?php echo $this->module ?>');" />
            <?php endif; ?>
			</div>
		</li>
	</ol>
</form>