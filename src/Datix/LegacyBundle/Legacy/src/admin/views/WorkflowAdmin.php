<form method="post" name="editworkflow" id="editworkflow" action="<?php echo $this->scripturl; ?>?action=workflowadminsave">
    <div class="new_titlebg">
	    <div class="title_text_wrapper"><b><?php echo _t('Approval status colours'); ?></b></div>
    </div>
    <?php foreach ($this->ApprovalStatusCodes as $Module => $StatusCodes): ?>
        <div class="panel" style="display:<?= ($Module == $this->CurrentModule ? 'block' : 'none') ?>" id="panel-<?php echo $Module; ?>">
            <div class="padded_div">
                <?php foreach ($StatusCodes as $Code => $CodeDetails): ?>
                    <div>
                        <input type="text" class="cp" name="<?php echo $Module . '_' . $Code . '_display'; ?>" id="<?php echo $Module . '_' . $Code . '_display'; ?>" style="background-color:#<?= $CodeDetails['colour'] ?>;width:18px;cursor:pointer"> <?php echo _t($CodeDetails['label']); ?>
                        <input type="hidden" name="<?php echo $Module . '_' . $Code; ?>" id="<?php echo $Module . '_' . $Code; ?>" value="<?php echo $CodeDetails['colour']; ?>">
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    <?php endforeach ?>
    <div class="button_wrapper">
        <button type="button" value="save" onclick="jQuery('#editworkflow').submit();"><?php echo _tk('btn_save'); ?></button>
        <button type="button" value="cancel" onclick="SendTo('<?php echo $this->scripturl; ?>?action=home&module=ADM')"><?php echo _tk('btn_cancel'); ?></button>
    </div>
</form>