<?php echo $this->messages; ?>
<form id="login-audit" method="POST" action="<?php echo $this->scripturl; ?>?action=login_settings_update">
    <ul>
        <li class="section_title_row" id="details_title_row" name="details_title_row">
            <div class="section_title_group">
                <div class="section_title"><?php echo _tk('login_settings-title'); ?></div>
            </div>
        </li>
        <li class="field_div">
            <div class="field_label_div" style="width: 25%">
                <label for="login-attempts" class="field_label"><?php echo _tk('login_audit-max-logins-lbl'); ?></label>
                <p class="field_extra_text hint"><?php echo _tk('login_audit-max-logins-hint'); ?></p>
            </div>
            <div class="field_input_div">
                <input id="login-attempts" name="login-attempts" type="text" size="4" value="<?php echo $this->output['LOGIN_TRY']; ?>"/>
            </div>
        </li>
        <li class="field_div">
            <div class="field_label_div" style="width: 25%">
                <label for="login-inactivity" class="field_label"><?php echo _tk('login_audit-days-inactive-lbl'); ?></label>
                <p class="field_extra_text hint"><?php echo _tk('login_audit-days-inactive-hint'); ?></p>
            </div>
            <div class="field_input_div">
                <input id="login-inactivity" name="login-inactivity" type="text" size="4" value="<?php echo ($this->output['LOGIN_INACTIVITY_DAYS'] < 0) ? '' : $this->output['LOGIN_INACTIVITY_DAYS']; ?>" />
                <div class="field_related_input">
                    <input id="login-inactivity-disable" name="login-inactivity-disable" value="true" type="checkbox" <?php echo ($this->output['LOGIN_INACTIVITY_DAYS'] < 0 && !isset($this->login_inactivity)) ? 'checked' : ''; ?> />
                    <label for="login-inactivity-disable"><?php echo _tk('login_audit-disable'); ?></label>
                </div>
            </div>
        </li>
        <li>
            <div class="button_wrapper">
                <input id="btnSubmit" class="button" value="<?php echo _tk('login_audit-save'); ?>" name="submit" type="submit" />
                <input id="btnCancel" class="button" value="<?php echo _tk('btn_cancel'); ?>" name="cancel" type="submit" />
            </div>
        </li>
    </ul>
</form>
<script type="text/javascript" src="js_functions/login-audit<?php echo $this->addMinExtension; ?>.js"></script>