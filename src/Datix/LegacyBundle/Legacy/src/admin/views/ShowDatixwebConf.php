<script type="text/javascript">var dataComTimescaleMigrated = <?php echo bYN(GetParm('COM_COMPLAINT_CHAIN_TIMESCALES_MIGRATED', 'N', true)) ? 'true' : 'false'; ?> </script>
<script language="JavaScript" type="text/javascript">
    function validateOnSubmit()
    {
        if($('CHANGED-WORKFLOW').value == '1')
        {
            if(!confirm('<?php echo _tk('change_inc_workflow') ?>'))
            {
                return false;
            }
        }

        var errs = 0,
            errorFields = new Array(),
            error = '';

        if ($('LOGIN_USAGE_AGREEMENT').value == 'Y')
        {
            if ($('UsageAgreementMessage_0').value == '')
            {
                $('errUsageAgreementMessage').show();
                errorFields.push('Usage agreements to display');
                moveScreen(errs, 'UsageAgreementMessageDiv', 'panel-ALL', 'advanced_all');
                errs += 1;
            }

            if ($('DISAGREEMENT_MSG').value == '')
            {
                $('errDISAGREEMENT_MSG').show();
                errorFields.push('Disagreement message');
                moveScreen(errs, 'DISAGREEMENT_MSG', 'panel-ALL', 'advanced_all');
                errs += 1;
            }
        }

        var overdueDays = $('SettingsPage').select('input[name$=OVERDUE_DAYS]');
        var titleSet = false;

        for (var i = 0; i < overdueDays.length; i++)
        {
            if ((overdueDays[i].value != '' && isNaN(overdueDays[i].value)) || (!isNaN(overdueDays[i].value) && overdueDays[i].value < -1))
            {
                if (!titleSet)
                {
                    errorFields.push('Overdue days:\n\r');
                    titleSet = true;
                }

                if (overdueDays[i].name.indexOf('DIF_') || overdueDays[i].name.indexOf('INC_'))
                {
                    $('errDIF_OVERDUE_DAYS').show();
                    errorFields.push($('LABEL_' + overdueDays[i].name).innerHTML + '\n\r');
                    moveScreen(errs, 'DIF_OVERDUE_DAYS', 'panel-INC', '');
                    errs += 1;
                }
            }
        }

        if (errs > 1)
        {
            error = "The following fields need to be amended before you can submit the form:<br /><br />";
            error += errorFields.join("<br />");
            alert(error);
            return false;
        }
        else if (errs == 1)
        {
            error = "The following field needs to be amended before you can submit the form:<br /><br />";
            error += errorFields.join("<br />");
            alert(error);
            return false;
        }
        else
        {
            return true;
        }
    }

    function moveScreen(errs, fieldname, divName, advancedSection)
    {
        if (errs == 0)
        {
            hideAllDivs();
            $(divName).show();

            if (advancedSection != '')
            {
                $('show_' + advancedSection).checked = true;
                $(advancedSection).show();
            }

            moveScreenToField(fieldname, false);
        }
    }
</script>
<form method="post" name="SettingsPage" id="SettingsPage" action="<?php echo $this->scripturl; ?>?action=savedatixwebconf&amp;adminaction=save">
    <script language="JavaScript" type="text/javascript">
        function showDesignForms(selectedModule)
        {
            // Hide all divs first
            hideAllDivs();

            // Show div for selected module
            document.getElementById(selectedModule.options[selectedModule.selectedIndex].value).style.display = "";
            //document.getElementById("show_advanced").checked = false;
            document.forms[0].show_advanced_all.checked = false;
            <?php if ($_SESSION["licensedModules"][MOD_INCIDENTS]) : ?>
                document.forms[0].show_advanced.checked = false;
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_RISKREGISTER]) : ?>
                document.forms[0].show_advanced_risk.checked = false;
            <?php endif; ?>
        }
        function hideAllDivs()
        {
            document.getElementById("panel-ALL").style.display = "none";
            <?php if ($_SESSION["licensedModules"][MOD_INCIDENTS]) : ?>
                document.getElementById("panel-INC").style.display = "none";
                document.getElementById("advanced").style.display = "none";
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_CIV]) : ?>
                document.getElementById("panel-CIV").style.display = "none";
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_PALS]) : ?>
                document.getElementById("panel-PAL").style.display = "none";
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_RISKREGISTER]) : ?>
                document.getElementById("panel-RAM").style.display = "none";
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_SABS]) : ?>
                document.getElementById("panel-SAB").style.display = "none";
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_COMPLAINTS]) : ?>
                document.getElementById("panel-COM").style.display = "none";
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_STANDARDS]) : ?>
                document.getElementById("panel-STD").style.display = "none";
            <?php endif; ?>
            <?php if ($_SESSION["licensedModules"][MOD_CQC_OUTCOMES]) : ?>
                document.getElementById("panel-CQC").style.display = "none";
            <?php endif; ?>
            document.getElementById("panel-ACT").style.display = "none";
            document.getElementById("panel-CON").style.display = "none";
            document.getElementById("panel-AST").style.display = "none";
        }

        // Need this to make sure that the advanced section behaves properly based on the visibility of its parent section
        function CheckState_show_advanced()
        {
            expandIt('advanced',this);
        }
    </script>
    <div class="new_windowbg">
        <!-- All modules -->
        <div id="panel-ALL" class="panel" style="<?php echo ("ALL" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->ALLTable->GetFormTable(); ?>
            <?php echo $this->ALLAdvTable->GetFormTable(); ?>
        </div>
        <!-- Incident Module Settings -->
        <div id="panel-INC" class="panel" style="<?php echo ("INC" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->Table->GetFormTable(); ?>
            <?php echo $this->AdvTable->GetFormTable(); ?>
        </div>
        <!-- PALS Module Settings -->
        <div id="panel-PAL" class="panel" style="<?php echo ("PAL" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->PALTable->GetFormTable(); ?>
        </div>
        <!-- Claims Module Settings -->
        <div id="panel-CLA" class="panel" style="<?php echo ("CLA" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->CLATable->GetFormTable(); ?>
        </div>
        <!-- Complaints Module Settings -->
        <div id="panel-COM" class="panel" style="<?php echo ("COM" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->COMTable->GetFormTable(); ?>
        </div>
        <!-- Risk Module Settings -->
        <div id="panel-RAM" class="panel" style="<?php echo ("RAM" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->RAMTable->GetFormTable(); ?>
            <?php echo $this->RAMAdvTable->GetFormTable(); ?>
        </div>
        <!-- SABS Module Settings -->
        <div id="panel-SAB" class="panel" style="<?php echo ("SAB" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->SABTable->GetFormTable(); ?>
        </div>
        <!-- Actions module settings -->
        <div id="panel-ACT" class="panel" style="<?php echo ("ACT" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->ACTTable->GetFormTable(); ?>
        </div>
        <!-- Contacts Module Settings -->
        <div id="panel-CON" class="panel" style="<?php echo ("CON" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->CONTable->GetFormTable(); ?>
        </div>
        <!-- Equipment Module Settings -->
        <div id="panel-AST" class="panel" style="<?php echo ("AST" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->ASTTable->GetFormTable(); ?>
        </div>
        <!-- Standards Module Settings -->
        <div id="panel-STD" class="panel" style="<?php echo ("STD" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->STNTable->GetFormTable(); ?>
        </div>
        <!-- CQC Module Settings -->
        <div id="panel-CQC" class="panel" style="<?php echo ("CQC" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->CQCTable->GetFormTable(); ?>
        </div>
        <!-- Medication Module Settings -->
        <div id="panel-MED" class="panel" style="<?php echo ("MED" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->MEDTable->GetFormTable(); ?>
        </div>
        <!-- Payments Module Settings -->
        <div id="panel-PAY" class="panel" style="<?php echo ("PAY" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->PAYTable->GetFormTable(); ?>
        </div>
        <!-- To Do List Module Settings -->
        <div id="panel-TOD" class="panel" style="<?php echo ("TOD" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->TODTable->GetFormTable(); ?>
        </div>
        <!-- Accreditation Module Settings -->
        <div id="panel-ACR" class="panel" style="<?php echo ("ACR" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->AMOTable->GetFormTable(); ?>
        </div>
        <!-- Organisations Module Settings -->
        <div id="panel-ORG" class="panel" style="<?php echo ("CON" == $this->module ? '' : 'display:none'); ?>">
            <?php echo $this->ORGTable->GetFormTable(); ?>
        </div>
    </div>
    <input type="hidden" id="rbWhat" name="rbWhat" value="Save" />
    <?php echo $this->ButtonGroup->getHTML(); ?>
</form>