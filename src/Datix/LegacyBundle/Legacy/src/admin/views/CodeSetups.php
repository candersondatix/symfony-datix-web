<?php if ($this->message) : ?>
    <div class="form_error_wrapper">
        <div class="form_error"><?php echo $this->message; ?></div>
    </div>
<?php endif ?>
<div class="new_titlebg">
    <div class="title_text_wrapper"><b>Module:</b></div>
    <div class="title_select_wrapper" style="padding:2px">
        <form name="codesetupslist" method="post" action="<?php echo $this->scripturl; ?>?action=codesetupslist">
            <input type="hidden" id="orderby" name="orderby" value="<?php echo $this->orderby; ?>" />
            <input type="hidden" id="order" name="order" value="<?php echo $this->order; ?>" />
            <?php
                echo getModuleDropdown(array(
                'name' => 'module',
                'current' => $this->module,
                'not' => $this->NotArray,
                'includeModuleGroups' => true
                ));
            ?>
        </form>
    </div><!-- End of .title_select_wrapper div -->
</div><!-- End of .new_titlebg div -->
<table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
    <tr class="tableHeader">
        <th class="windowbg" width="68%" align="left"><a href="<?php echo $this->scripturl; ?>?action=codesetupslist&amp;module=<?php echo $this->module; ?>&amp;orderby=fdr_label&amp;order=<?php echo ($this->orderby == 'fdr_label' && $this->order == 'ASC') ? 'DESC' : 'ASC;' ?>" class="<?php echo ($this->orderby == 'fdr_label') ? 'sort ' . \UnicodeString::strtolower($this->order) : ''; ?>">Name</a></th>
    </tr>
    <?php if (count($this->resultArray) == 0) : ?>
        <tr>
            <td class="rowtitlebg" colspan="2">
                <strong><?php echo _tk('no-codes'); ?></strong>
            </td>
        </tr>
    <?php endif ?>
    <?php foreach ($this->resultArray as $row): ?>
        <tr class="listing-row">
            <td class="windowbg2">
                <a href="<?php echo $row['url'] . '&amp;form_action=edit'; ?>"><?php echo $row['fdr_label']; ?></a>
                <?php if (IsCentrallyAdminSys() && $row['locked']): ?><img height="12px" src="Images/icons/lock.png" alt="<?php echo _tk('locked'); ?>" title="<?php echo _tk('locked'); ?>"><?php endif ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>
<script type="text/javascript" src="js_functions/code-setups-list<?php echo $this->addMinExtension; ?>.js"></script>
