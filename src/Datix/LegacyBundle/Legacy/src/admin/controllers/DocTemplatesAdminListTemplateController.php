<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\wordmergetemplate\model\WordMergeTemplateModelFactory;

class DocTemplatesAdminListTemplateController extends TemplateController
{
    /**
     * Document template administration - Allows users to upload templates (form, invoice, letter, memorandum,
     * reply to query and secure doc).
     */
    function doctemplateadminlist()
    {
        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $message = \Sanitize::SanitizeString($this->request->getParameter('message'));

        if ($module == '')
        {
            $module = 'INC';
        }

        $TemplateType = array(
            'FORM' => 'Report form',
            'INV' => 'Invoice',
            'LETR' => 'Letter',
            'MEMO' => 'Memorandum',
            'REPL' => 'Reply to query',
            'SECUR' => 'Secure doc'
        );

        if ($this->request->getMethod() == 'GET' && $this->request->getParameter('orderby'))
        {
            $orderby = \Sanitize::SanitizeString($this->request->getParameter('orderby'));
        }
        else
        {
            $orderby = 'tem_notes';
        }

        if ($this->request->getMethod() == 'GET' && $this->request->getParameter('order'))
        {
            $order = \Sanitize::SanitizeString($this->request->getParameter('order'));

            if ($order != 'DESC')
            {
                $order = 'ASC';
            }
        }
        else
        {
            $order = 'ASC';
        }

        $this->title = 'Document template administration';
        $this->module = 'ADM';

        $this->menuParameters = array(
            'menu_array' => array(
                array(
                    'label' => 'New template', // TODO i18n
                    'link'  => 'action=doctemplateaction&form_action=new&module=' . $module
                )
            ),
            'module' => 'ADM'
        );

        $this->hasPadding = false;

        $ModuleDropDownHTML = getModuleDropdown(array(
            'name' => 'lbModule',
            'onchange' => 'showModule(this);',
            'current' => $module,
            'not' => array(
                'ELE', 'PRO', 'CQO', 'CQP', 'CQS', 'LOC', 'LIB', 'ACT', 'AST', 'DST', 'MED', 'PAY', 'HSA', 'HOT',
                'ADM', 'DAS', 'TOD', 'ATI', 'ATM', 'AMO', 'ATQ', 'AQU'
            )
        ));

        $Factory = new WordMergeTemplateModelFactory();

        $Query = $Factory->getQueryFactory()->getQuery();
        $DocTemplates = $Factory->getCollection();

        $FieldCollection = $Factory->getQueryFactory()->getFieldCollection();
        $FieldCollection2 = $Factory->getQueryFactory()->getFieldCollection();
        $FieldCollection3 = $Factory->getQueryFactory()->getFieldCollection();
        $FieldCollection->field('tem_module')->eq($module);
        $FieldCollection2->field('tem_module')->eq('');
        $FieldCollection3->field('tem_module')->isNull();
        $Where = $Factory->getQueryFactory()->getWhere();
        $Where->add('OR', $FieldCollection, $FieldCollection2, $FieldCollection3);

        $Query->where($Where);

        if ($orderby && $order && $orderby != 'tem_type')
        {
            $Query->orderBy(array(array($orderby, $order)));
        }

        $Query->overrideSecurity();

        $DocTemplates->setQuery($Query);

        $resultArray = array();

        foreach ($DocTemplates as $DocTemplate)
        {
            $resultArray[] = $DocTemplate;
        }

        if ($orderby && $orderby == 'tem_type')
        {
            usort($resultArray, function($a, $b) use ($TemplateType)
            {
                $val1 = $TemplateType[$a->tem_type];
                $val2 = $TemplateType[$b->tem_type];

                return \UnicodeString::strcasecmp($val1, $val2);
            });

            if ($order && $order == 'DESC')
            {
                $resultArray = array_reverse($resultArray);
            }
        }

        $LastGroup = '';

        $this->response->build('src/admin/views/DocTemplatesAdminList.php', array(
            'ModuleDropDownHTML' => $ModuleDropDownHTML,
            'resultArray' => $resultArray,
            'LastGroup' => $LastGroup,
            'message' => $message,
            'module' => $module,
            'order' => $order,
            'orderby' => $orderby,
            'TemplateType' => $TemplateType
        ));
    }
}