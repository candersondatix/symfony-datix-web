<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;

class AsmLocationHierarchyController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
    
    /**
     * Administration of location hieracry for Accreditation module.
     */
    public function asmLocations()
    {
        $this->title          = _tk('manage_locations');
        $this->hasPadding     = false;
        $this->menuParameters = array(
            'menu_array' => array(
                'new-tier' => array('label' => 'Add tier', 'link' => 'action=edituser')
        ));
        
        $locationMapper = \System_NestedSetMapperFactory::getNestedSetMapper(\System_NestedSetMapperFactory::LOCATIONS);
        $locations      = $this->_nest($locationMapper->findAll());
        $tiers          = \DatixDBQuery::PDO_fetch_all('SELECT * FROM location_tiers ORDER BY lti_depth ASC');
        $locTypes       = \DatixDBQuery::PDO_fetch_all("SELECT cod_code, cod_descr FROM code_types WHERE cod_type = :code", array('code' => 'LOCTYPE'));
 
        $this->response->build('src/admin/views/AsmLocations.php', array(
            'selectedLocation' => \Sanitize::SanitizeInt($this->request->getParameter('location_id')),
            'locations'        => $locations,
            'data'             => json_encode($locations),
            'tiers'            => $tiers,
            'locTypes'         => $locTypes
        ));
    }
    
    /**
     * Displays the location hierarchy search results.
     */
    public function asmShowSearchLocation()
    {
        $this->title          = _tk('search_locations');
        $this->menuParameters = array(
            'menu_array' => array(
                array(array('label' => _tk('back_loc_hier'), 'link' => 'action=asmlocations'))
        ));

        $this->response->build('src/admin/views/AsmShowSearchLocation.php', array(
            'locations' => $this->searchLocation($this->request->getParameter('location'))
        ));
    }
    
    /**
     * TODO This is cut n' paste from CQC location hierarchy
     */
    private function _nest(array $collection)
    {
        // Trees mapped
        $trees = array();
        $l = 0;

        if (count($collection) > 0) {
            // Node Stack. Used to help building the hierarchy
            $stack = array();

            foreach ($collection as $node) {
                $item = $node;
                $item['children'] = array();

                // Number of stack items
                $l = count($stack);

                // Check if we're dealing with different levels
                while($l > 0 && $stack[$l - 1]['depth'] >= $item['depth']) {
                    array_pop($stack);
                    $l--;
                }

                // Stack is empty (we are inspecting the root)
                if ($l == 0) {
                    // Assigning the root node
                    $i = count($trees);
                    $trees[$i] = $item;
                    $stack[] = & $trees[$i];
                } else {
                    // Add node to parent
                    $i = count($stack[$l - 1]['children']);
                    $stack[$l - 1]['children'][$i] = $item;
                    $stack[] = & $stack[$l - 1]['children'][$i];
                }
            }
        }

        return $trees;
    }
    
    /**
     * Returns the locations list for the search page.
     * 
     * @param string $searchTerm
     * 
     * @return array
     */
    private function searchLocation($searchTerm)
    {
        $locations  = array();
        $searchTerm = \Sanitize::SanitizeString($searchTerm);
        $locations  = addUpperTierInfo(\DatixDBQuery::PDO_fetch_all('SELECT * FROM vw_locations_main ORDER BY lft'));

        if ($searchTerm != '')
        {
            $locations = $this->highlightMatches($locations, $searchTerm);
        }

        return $locations;
    }
    
    /**
     * Filters a location list using the provided search term.
     * 
     * @param array  $locations
     * @param string $searchTerm
     * 
     * @return array
     */
    private function highlightMatches(array $locations, $searchTerm)
    {
        foreach ($locations as $id => $location)
        {
            if (preg_match("/$searchTerm/ui", $location['loc_name'], $matches))
            {
                if ($matches)
                {
                    $locations[$id]['loc_name'] = str_replace($matches[0], '<b>' . $matches[0] . '</b>', $locations[$id]['loc_name']);
                }
            }
            else
            {
                unset($locations[$id]);
            }
        }

        return $locations;
    }
}