<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class SaveColumnsController extends Controller
{
    function savecolumns()
    {
        $module = \Sanitize::getModule($this->request->getParameter('module'));

        if (!$module)
        {
            $module = 'INC';
        }

        $Data = $this->request->getParameters();

        $colarray = $this->request->getParameter('columns_list2');

        if (is_array($colarray))
        {
            foreach ($colarray as $colname)
            {
                if (array_key_exists('col_width_'.$colname, $Data))
                {
                    $newarray[$colname]['width'] = $this->request->getParameter('col_width_'.$colname);
                }
            }
        }

        require_once 'Source/libs/ListingClass.php';

        $Listing = new \Listing($module, \Sanitize::SanitizeInt($this->request->getParameter('form_id')));

        // Load current values
        $Listing->LoadColumnsFromDB();
        // Overwrite column array
        $Listing->LoadColumnsFromArray($newarray);

        $Listing->SaveToDB();

        AddSessionMessage('INFO', 'Listing design saved');

        $Parameters = array(
            'module' => $module,
            'listing_id' => $Listing->ListingId
        );

        $this->call('src\admin\controllers\EditColumnsTemplateController', 'editcolumns', $Parameters);
    }
}