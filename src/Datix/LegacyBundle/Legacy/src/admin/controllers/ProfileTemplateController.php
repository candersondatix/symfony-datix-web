<?php
namespace src\admin\controllers;

use src\profiles\model\profile\ProfileModelFactory;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;

/**
 * Controls the display of security-profile user interfaces
 */
class ProfileTemplateController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
	
    /**
     * Constructs a list of existing profiles.
     */
	public function listProfiles()
	{
        $this->hasPadding = false;
        $this->title = _tk('manage_profiles');
        $this->menuParameters = array(
            'menu_array' => array(array('label' => 'Create a new profile', 'link' => 'action=editprofile')),
            'floating_menu' => false);

        $LDAPOrderEdit = $this->request->getParameter('edit');

        require_once 'Source/libs/LDAP.php';
        $ldap = new \datixLDAP();

        $sort  = \Sanitize::SanitizeString((!is_null($this->request->getParameter('sort'))) ? $this->request->getParameter('sort') : 'pfl_name');
        $order = ($this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';

        $factory = new ProfileModelFactory();

        // attempt to fetch the entity from the database
        list($query, $where, $fields) = $factory->getQueryFactory()->getQueryObjects();

        if (GetParm('ADM_PROFILES') != '')
        {
            $where->add($fields->field('profiles.recordid')->in(explode(' ', GetParm('ADM_PROFILES'))));
            $query->where($where);
        }

        if ($sort && $sort != 'pfl_description')
        {
            $query->orderBy(array(array($sort, $order)));
        }

        $profiles = $factory->getCollection();
        $profiles->setQuery($query);

        foreach($profiles as $profile)
        {
            $data[] = $profile;
        }

        //If we've returned here after attempting to save values, we should retain them in case an error has occurred.
        foreach($data as $index => $profileDetails)
        {
            if ($LDAPOrderEdit)
            {
                if ($this->request->getParameter("profile_order_" . $profileDetails->recordid) != null)
                {
                    $data[$index]->pfl_ldap_order = $this->request->getParameter("profile_order_" . $profileDetails->recordid);
                }
            }
        }

        if ($sort && $sort == 'pfl_description')
        {
            usort($data, function($a, $b)
            {
                return \UnicodeString::strcasecmp($a->pfl_description, $b->pfl_description);
            });

            if ($order && $order == 'DESC')
            {
                $data = array_reverse($data);
            }
        }

        $this->response->build('src/admin/views/ListProfiles.php', array(
            'data' => $data,
            'ldap' => $ldap,
            'ldaporderedit' => $LDAPOrderEdit,
            'sort' => $sort,
            'order' => $order,
		));
	}

    /**
     * Constructs a form showing information about a particular profile
     */
	public function editProfile()
	{
        global $ModuleDefs;

        $NewProfile = ($this->request->getParameter('recordid') == '');
        $from_grp_id = $this->request->getParameter('from_grp_id');
        
        $this->title = ($NewProfile ? 'New Profile' : 'Edit Profile');

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => 'Save', 'onclick' => 'selectAllMultiCodes();submitClicked=true;if(validateOnSubmit()){document.frmProfile.submit()}', 'action' => 'SAVE'));
        if (!$NewProfile)
        {
            $ButtonGroup->AddButton(array('id' => 'btnCopy', 'name' => 'btnCopy', 'label' => 'Copy', 'onclick' => 'copyProfile()', 'action' => 'BACK', 'icon' => 'images/icons/toolbar-copy.png'));
        }
        
        if (empty($from_grp_id))
        {
            $cancel_buttton_url = 'SendTo(\'app.php?action=listprofiles\');';
            $cancel_button_label = _tk('btn_cancel');
        }
        else
        {
            $cancel_buttton_url = 'SendTo(\'app.php?action=editgroup&grp_id=' . $from_grp_id . '\');';
            $cancel_button_label = 'Back to security group';
        }
        $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => $cancel_button_label, 'onclick' => $cancel_buttton_url, 'action' => 'CANCEL'));

        if(!$NewProfile)
        {
            $ButtonGroup->AddButton(array('id' => 'btnDelete', 'name' => 'btnDelete', 'label' => 'Delete profile', 'onclick' => 'deleteProfile()', 'action' => 'DELETE'));
        }

        $factory = new ProfileModelFactory();

        if($this->request->getParameter('recordid'))
        {
            //get profile-specific data
            $profile = $factory->getMapper()->find($this->request->getParameter('recordid'));
            $data = $profile->getVars();

            //get parameter data
            foreach($profile->getLinkedConfigurationParameters() as $configParameter)
            {
                $data[$configParameter->lpp_parameter] = $configParameter->lpp_value;
            }

            //get security group data
            $security_groups = $profile->getLinkedGroups(); 
            if(!empty($security_groups))
            {
                foreach($security_groups as $group)
                {
                    $Groups[] = $group->recordid;
                }
            }

            if(is_array($Groups))
            {
                $data['sec_groups'] = join(' ', $Groups);
            }

            // Retrieve LDAP mappings
            $sql = 'SELECT ldap_dn FROM sec_ldap_profiles WHERE pfl_id = :recordid';
            $resultArray = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $_GET['recordid']));

            foreach ($resultArray as $row)
            {
                $data['ldap_dn'][] = $row["ldap_dn"];
            }
        }
        else
        {
            $data = array();
        }

        $FormMode = ($NewProfile ? 'New' : 'Edit');

        $FormDesign = new \Forms_FormDesign(array('module' => 'ADM', 'level' => 2));
        $FormDesign->MandatoryFields['pfl_name'] = 'details';
        $FormDesign->MandatoryFields['pfl_description'] = 'details';
        $FormDesign->HelpTexts = array('ATI_REVIEWED_EMAIL' => 'E-mails will be sent only for the main location for which the assigned assessment has been created. Users at other locations selected against the assigned assessment will not receive notification.');

        $Table = new \FormTable($FormMode, 'ADM', $FormDesign);

        include('Source/security/BasicFormProfile.php');
        $Table->MakeForm($FormArray, $data, 'ADM');
        $Table->MakeTable();

        $this->menuParameters = array(
            'table' => $Table,
            'buttons' => $ButtonGroup,
            'no_audit' => true,
            'no_print' => true
        );

        $this->response->build('src/admin/views/EditProfile.php', array(
            'data' => $data,
            'Table' => $Table,
            'ButtonGroup' => $ButtonGroup,
		));
	}
}