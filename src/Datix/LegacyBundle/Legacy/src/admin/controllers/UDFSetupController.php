<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;
use src\system\database\extrafield\CodeExtraField;
use src\system\database\extrafield\ExtraFieldModelFactory;

/**
 * Controls the process of creating/amending extra fields.
 */
class UDFSetupController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);    
    }
	
    /**
     * Landing page - constructs a list of existing extra fields by module and provides links to add/edit.
     */
	public function listExtraFields()
	{
		$columns = array(
            'columns' => array(
                new \Fields_DummyField(array(
                    'name' => 'recordid',
                    'label' => 'ID',
                    'type' => 'S'
                )),
			    new \Fields_DummyField(array(
                    'name' => 'fld_name',
                    'label' => 'Label',
                    'type' => 'S'
                )),
			    new \Fields_DummyField(array(
                    'name' => 'fld_type',
                    'label' => 'Type',
                    'type' => 'C',
                    'codes' => \Fields_DummyField::GetFieldTypesArray()
                )),
			    new \Fields_DummyField(array(
                    'name' => 'fld_format',
                    'label' => 'Format',
                    'type' => 'S'
                ))
            )
        );
		
		if (IsCentrallyAdminSys())
		{
			$columns['columns'][] = new \Fields_DummyField(array(
                'name' => 'central_id',
                'label' => 'Central?',
                'type' => 'Y'
            ));
			$columns['columns'][] = new \Fields_DummyField(array(
                'name' => 'central_locked',
                'label' => 'Locked?',
                'type' => 'Y'
            ));
		}
		
		$Design = new \Listings_ListingDesign($columns);
		
		if ($_GET['module'])
		{
			$CurrentModule = \Sanitize::SanitizeString($_GET['module']);
		}

        $availableModules = getModArray(array('MED', 'DAS', 'TOD'));

        $Factory = new ExtraFieldModelFactory();

        $orderby = \Sanitize::SanitizeString((!is_null($this->request->getParameter('orderby'))) ? $this->request->getParameter('orderby') : 'recordid');

        // Field type codes do not correspond to alphabetical order, so we need to add a custom ordering
        if ($orderby == 'fld_type')
        {
            $orderby = "CASE
				WHEN fld_type = 'C' THEN 1
				WHEN fld_type = 'D' THEN 2
				WHEN fld_type = 'M' THEN 3
				WHEN fld_type = 'T' THEN 4
				WHEN fld_type = 'N' THEN 5
				WHEN fld_type = 'S' THEN 6
				WHEN fld_type = 'L' THEN 7
				WHEN fld_type = 'Y' THEN 8
				ELSE 9 END";
        }

        $order   = ($this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';

        $extraFields = \DatixDBQuery::PDO_fetch_all(
            'SELECT recordid, fld_name, fld_type, fld_format, uml_module
            '.(IsCentrallyAdminSys() ? ', central_id, central_locked' : '').'
            FROM udf_fields
            LEFT JOIN udf_mod_link ON uml_id = recordid'.
            ($orderby ? ' ORDER BY '.$orderby.' '.$order : '')
        );

        foreach ($extraFields as $extraField)
        {
            if ($extraField['uml_module'] == '')
            {
                foreach ($availableModules as $availableModule => $moduleName)
                {
                    $extraFieldsByModule[$availableModule][] = $extraField;
                }
            }
            else
            {
                $extraFieldsByModule[$extraField['uml_module']][] = $extraField;
            }
        }

		foreach (getModArray(array('MED', 'DAS', 'TOD')) as $Module => $ModName)
		{
			if(!is_array($extraFieldsByModule[$Module]))
            {
                $extraFieldsByModule[$Module] = array();
            }

            $RecordList = new \RecordLists_RecordList();
			$RecordList->AddRecordData($extraFieldsByModule[$Module]);
		
			$Listings[$Module] = new \Listings_ListingDisplay($RecordList, $Design);
			$Listings[$Module]->setOrder( ($_GET['orderby']) ? $_GET['orderby'] : 'recordid' );
			$Listings[$Module]->setDirection( ($_GET['order']) ? $_GET['order'] : 'ASC' );
			$Listings[$Module]->setSortAction(\Sanitize::SanitizeURL($_GET['action']) . '&amp;module=' . $Module);
			$Listings[$Module]->Action = 'editextrafield';
		
			$PanelArray[$Module] = array('Label' => $GLOBALS['ModuleDefs'][$Module]['NAME']);
		
			if (!$CurrentModule)
			{
				$CurrentModule = $Module;
			}
		}
		
/*		foreach (getModArray(array('MED', 'DAS', 'TOD')) as $Module => $ModName)
		{
			$orderby = \Sanitize::SanitizeString((!is_null($this->request->getParameter('orderby'))) ? $this->request->getParameter('orderby') : 'recordid');

			// Field type codes do not correspond to alphabetical order, so we need to add a custom ordering
			if ($orderby == 'fld_type')
			{
				$orderby = "CASE
				WHEN fld_type = 'C' THEN 1
				WHEN fld_type = 'D' THEN 2
				WHEN fld_type = 'M' THEN 3
				WHEN fld_type = 'T' THEN 4
				WHEN fld_type = 'N' THEN 5
				WHEN fld_type = 'S' THEN 6
				WHEN fld_type = 'L' THEN 7
				WHEN fld_type = 'Y' THEN 8
				ELSE 9 END";
			}

			$order   = ($this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';

            $Query = $Factory->getQueryFactory()->getQuery();
            $ExtraFields = $Factory->getCollection();

            $FieldCollection = $Factory->getQueryFactory()->getFieldCollection();
            $FieldCollection2 = $Factory->getQueryFactory()->getFieldCollection();
            $FieldCollection->field('uml_module')->eq($Module);
            $FieldCollection2->field('uml_module')->isNull();
            $Where = $Factory->getQueryFactory()->getWhere();
            $Where->add('OR', $FieldCollection, $FieldCollection2);

            $Query->join('udf_mod_link', 'uml_id = recordid', 'LEFT')
                ->where($Where)
                ->orderBy(array(array($orderby, $order)));

            $ExtraFields->setQuery($Query);

			$RecordList = new \RecordLists_RecordList();
			$RecordList->AddRecordData($ExtraFields);

			$Listings[$Module] = new \Listings_ListingDisplay($RecordList, $Design);
			$Listings[$Module]->setOrder( ($_GET['orderby']) ? $_GET['orderby'] : 'recordid' );
			$Listings[$Module]->setDirection( ($_GET['order']) ? $_GET['order'] : 'ASC' );
			$Listings[$Module]->setSortAction(\Sanitize::SanitizeURL($_GET['action']) . '&amp;module=' . $Module);
			$Listings[$Module]->Action = 'editextrafield';

			$PanelArray[$Module] = array('Label' => $GLOBALS['ModuleDefs'][$Module]['NAME']);

			if (!$CurrentModule)
			{
				$CurrentModule = $Module;
			}
		}
		*/
		$this->title = 'Extra Fields';
		$this->menuParameters = array(
			'menu_array' => array(array('label' => _tk('create_extra_field'), 'link' => 'action=editextrafield')),
			'panels' => $PanelArray,
			'current_panel' => $CurrentModule,
			'floating_menu' => false
        );
		
		$this->response->build('src/admin/views/ListExtraFields.php', array(
			'Listings' => $Listings,	
			'CurrentModule' => $CurrentModule
		));
	}
	
	/**
	 * Constructs the form used to add/edit extra fields.
	 */
	public function editExtraField()
    {
        $id = $this->request->getParameter('recordid');
        
        $this->title = (!$id ? 'New' : 'Edit').' Extra Field';
        $this->hasPadding = false;
        
        $ExtraField = new \Fields_ExtraField($id);

        $IsFieldLocked = (bYN($ExtraField->getCentralLocked()) && IsCentrallyAdminSys() && !IsCentralAdmin());
        

        $Data['recordid'] = $ExtraField->GetID();
        $Data['fld_name'] = $ExtraField->getLabel();
        $Data['fld_type'] = $ExtraField->getFieldType();
        $Data['fld_format'] = $ExtraField->getFormat();
        $Data['fld_length'] = $ExtraField->getLength();
        $Data['fld_code_like'] = $ExtraField->getCodeLike();
        $Data['central_locked'] = $ExtraField->getCentralLocked();
        $Data['fld_code_like_module'] = $ExtraField->getMapFieldModule();

        if (is_array($ExtraField->getModules()))
        {
            $Data['fld_modules'] = implode(' ', $ExtraField->getModules());
        }

        if ($Data['fld_code_like_module'] != '')
        {
            $_POST['module']['module'] = $Data['fld_code_like_module'];
        }

        $this->addJs('src/admin/js/extraFields.js');

        $notArray = ['DAS','DST','HSA','ADM','HOT','TOD'];
        $modArray = getModArray($notArray, true);

        //Check user has setup permissions to available modules, and that requested module is valid.
        foreach ($modArray as $code => $name)
        {
            if (!HasSetupPermissions($code))
            {
                if (!in_array($code, $notArray))
                {
                    $notArray[] = $code;
                }
            }
        }

        $modArray = getModArray($notArray, true);

        $extraFieldModelFactory = new \src\system\database\extrafield\ExtraFieldModelFactory();
        $extraFieldMapper = $extraFieldModelFactory->getMapper();
        //This is logic for the profile select on a staff field limited by profile selections
        if($ExtraField->getCodeLike() != null && $extraFieldMapper->isStaffCode($ExtraField->getCodeLike()))
        {
            $CodeExtraField = $this->registry->getFieldDefs()['UDF_'.$id];
            $savedQueryModelFactory = new \src\savedqueries\model\SavedQueryModelFactory();
            $savedQueryMapper = $savedQueryModelFactory->getMapper();
            if(($profileSavedQuery = $savedQueryMapper->find($CodeExtraField->queryid)) != null)
            {
                $profileSavedQuery->getWhere();
                $profileQuery = $profileSavedQuery->createQuery();
                $profileQuery->select(array('recordid'));
                $profileQuery->from('profiles');
                $SqlWriter = new \src\framework\query\SqlWriter();
                list($sql, $params) = $SqlWriter->writeStatement($profileQuery);
                $profiles = \DatixDBQuery::PDO_fetch_all($sql, $params, \PDO::FETCH_COLUMN);
            }
            $sql = 'SELECT recordid as code, pfl_name as description FROM profiles';
            $codes = \DatixDBQuery::PDO_fetch_all($sql, array(), \PDO::FETCH_KEY_PAIR);
            if(count($codes) < 1)
            {
                $codes['!NOCODES!'] = 'No profiles available';
            }

            $ProfileMultiSelect = \Forms_SelectFieldFactory::createSelectField('profile_select', '', $profiles, '', true);
            $ProfileMultiSelect->setCustomCodes($codes);
            //$ProfileMultiSelect->setSuppressCodeDisplay();
            $ProfileMultiSelect->setIgnoreMaxLength();

            $ProfileSelectRowArray = (
                array(
                    'Name' => 'profile_select',
                    'Title' => 'Profile select',
                    'Type' => 'formfield',
                    'FormField' => $ProfileMultiSelect
                )
            );
        }

        if($Data['fld_code_like'])
        {
            $code_set_type = 'mapped';
        }
        else
        {
            $code_set_type = 'custom';
        }

        $FormArray = array(
            'extra_field' => array(
                'Title' => 'Extra Field Details',
                'Rows' => array(
                    array('Name' => 'recordid', 'Title' => 'ID', 'ReadOnly' => true, 'Type' => 'string'),
                    array('Name' => 'fld_name', 'Title' => 'Label', 'Type' => 'string', 'MaxLength' => 254),
                    array('Name' => 'fld_type', 'Title' => 'Type', 'Type' => 'ff_select', 'CustomCodes' => \Fields_DummyField::GetFieldTypesArray(), 'ReadOnly' => ($Data['recordid'] != '')),
                    array('Name' => 'fld_format', 'Title' => 'Format', 'Type' => 'string'),
                    array('Name' => 'fld_length', 'Title' => 'Length', 'Type' => 'number', 'MaxLength' => 3),
                    array('Name' => 'fld_modules', 'Title' => 'Modules', 'Type' => 'multilistbox', 'CustomCodes' => getModArray(array('MED','TOD','DAS')), 'IgnoreMaxLength' => true)
                )
            ),
            'code_setup' => [
                'Title' => 'Code set options',
                'Rows' => [
                    ['Title' => '<input id="custom-code-set" type="radio" name="code_set_type" value="custom" onclick="codeSetChanged()"' . ($code_set_type == 'custom' ? ' checked="checked"' : '') .' /><input type="hidden" id="CHANGED-code-set-type" value="" /><label for="custom-code-set">Custom code set</label>']
                ]
            ],
            'coding' =>[
                'Title' => '',
                'ControllerAction' => array(
                    'UDFCodeSetupOptions' => array(
                        'controller' => 'src\\admin\\controllers\\UDFCodeSetupController')),
                'Rows' => array()
            ],
            'mapped_field' => [
                'Title' => '',
                'Rows'  => [
                    [
                        'Title' => '<input id="mapped-code-set" type="radio" name="code_set_type" value="mapped" onclick="codeSetChanged()"' . ($code_set_type == 'mapped' ? ' checked="checked"' : '') .' /><label for="mapped-code-set">Mapped code set</label>'
                    ],
                    [
                        'Name' => 'fld_code_like_module',
                        'Title' => 'Module',
                        'Type' => 'ff_select',
                        'CustomCodes' => $modArray,
                        'OnChangeExtra' => 'setModuleValue();clearFieldValue();selectMappedRadio()'
                    ],
                    [
                        'Name' => 'fld_code_like',
                        'Title' => 'Field',
                        'Type' => 'ff_select',
                        'SelectFunction' => [
                            'getMapCodeFieldsList',
                            ['module' => 'module']
                        ],
                        'OnChangeExtra' => 'selectMappedRadio()'
                    ]
                ]
            ]
        );

        if(isset($ProfileSelectRowArray))
        {
            $FormArray['mapped_field']['Rows'][] = $ProfileSelectRowArray;
        }
        else if(getExtraFieldMapLink($Data['fld_code_like_module'], $Data['fld_code_like']))
        {
            $FormArray['mapped_field']['Rows'][] = array(
                'Name' => 'map_fld_link',
                'Type' => 'custom',
                'Title' => getExtraFieldMapLink($Data['fld_code_like_module'], $Data['fld_code_like'])
            );
        }
        if (IsCentrallyAdminSys() && IsCentralAdmin())
		{
			$FormArray['extra_field']['Rows'][] = array('Name' => 'central_locked', 'Title' => 'Locked?', 'Type' => 'yesno');
		}
		
		if ($IsFieldLocked)
		{
			$FormArray['extra_field']['Rows'][1]['ReadOnly'] = true;
			$FormArray['extra_field']['Rows'][2]['ReadOnly'] = true;
			$FormArray['extra_field']['Rows'][3]['ReadOnly'] = true;
			$FormArray['extra_field']['Rows'][4]['ReadOnly'] = true;
			$FormArray['extra_field']['Rows'][5]['ReadOnly'] = true;
		}

        $FormDesign = new \Forms_FormDesign();
        $FormDesign->ExpandSections = array (
        	'fld_type' =>
        	array (
        		0 =>
            	array (
              		'section' => 'coding',
              		'values' =>
              		array (
                		0 => 'C',
                		1 => 'T',
              		),
            	),
                array(
                    'section' => 'mapped_field',
                    'values' => array (
                        0 => 'C',
                        1 => 'T'
                    )
                ),
                array(
                    'section' => 'code_setup',
                    'values' => array (
                        0 => 'C',
                        1 => 'T'
                    )
                )
          	)
        );

        $FormDesign->ExpandFields = array (
        	'fld_type' =>
        	array (
        		0 =>
            	array (
              		'field' => 'fld_length',
              		'values' =>
              		array (
                		0 => 'S'
              		)
            	)
          	)
        );

        $FormDesign->MandatoryFields = array(
            'fld_name' => 'extra_field',
            'fld_type' => 'extra_field'
        );

        $Table = new \FormTable('', 'ADM', $FormDesign);
        $Table->MakeForm($FormArray, $Data, 'ADM');

        $Table->MakeTable();

        $GLOBALS['JSFunctions'][] = MakeJavaScriptValidation('ADM', $FormDesign, array('fld_name' => 'Label', 'fld_type' => 'Type'));

        $this->menuParameters = array(
            'menu_array' => array(array('label' => _tk('create_extra_field'), 'link' => 'action=editextrafield')),
            'floating_menu' => false
        );

        $this->response->build('src/admin/views/getEditFormHTML.php', array(
        	'Table' => $Table,
        	'udfLengthAlert' => str_replace('"', '\"', _tk('udf_length_alert')),
        	'IsFieldLocked'  => $IsFieldLocked
        ));
    }
    
    /**
     * Saves the extra field details and redirects to the edit form.
     */
    public function saveExtraField()
    {
        if($_POST['code_set_type'] == 'custom')
        {
            unset($_POST['fld_code_like']);
            unset($_POST['fld_code_like_module']);
        }

    	$ExtraField = new \Fields_ExtraField($this->request->getParameter('recordid'));
    	$ExtraField->UpdateFromPost();
    	
    	if ($this->request->getParameter('recordid'))
    	{
    	    $id = $this->request->getParameter('recordid');
    	}
    	else
    	{
    	    $id = $ExtraField->getID();
    	}

    	AddSessionMessage('INFO', 'Saved');
    	
    	$this->redirect('?action=editextrafield&recordid='.$id);
    }
}