<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ShowErrorLogTemplateController extends TemplateController
{
    function showerrorlog()
    {
        global $scripturl, $ClientFolder;

        ErrorLogRead();

        $this->title = 'Error Log';
        $this->module = 'ADM';
        $this->image = 'Images/icons/icon_cog_error_log.png';
        $this->subtitle = (file_exists($ClientFolder . "/SQLErrors.xml") ? '<a href="#" onclick="exportFile(\''.$scripturl.'?service=Export&event=exportConfigFile&file=SQLErrors.xml\')"> [download]</a>' : '');
        $this->hasMenu = false;
        $this->hasPadding = false;

        $ErrorLog = GetErrorLogContents(array(
            'applyxslt' => 'xsl_files/errorlog.xsl'
        ), $Errors);

        $this->response->build('src/admin/views/ShowErrorLog.php', array(
            'ErrorLog' => $ErrorLog,
            'Errors' => $Errors
        ));
    }
}