<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class EditColumnsTemplateController extends TemplateController
{
    /**
     * Provides the user with options for the creation of a new listing design.
     */
    function editcolumns()
    {
        global $FieldDefs, $FieldDefsExtra;

        $ModuleDefs = $this->registry->getModuleDefs();

        require_once 'Source/EditColumns.php';

        $module     = $this->request->getParameter('module');
        $listing_id = $this->request->getParameter('listing_id');

        // default module
        if (!$module) {
            $module = 'INC';
        }

        // default listing
        if (!$listing_id) {
            $listing_id = \Sanitize::SanitizeInt($this->request->getParameter('form_id'));
        }

        $this->title      = $this->getPageTitle($listing_id, $ModuleDefs[$module]['NAME']);
        $this->module     = 'ADM';
        $this->hasPadding = false;


        $Listing = \Listings_ListingFactory::getListing($module, $listing_id);
        $Listing->LoadColumnsFromDB();

        if ($listing_id == 0) {
            $Listing->ReadOnly = true;
        }

        $list_columns = $Listing->Columns;

        $sql = '
            SELECT
                fdr_name
            FROM
                field_directory
            WHERE
                fdr_name IN (\''.implode("','", array_keys($FieldDefs[$module])).'\')
            AND
                fdr_table = :table
        ';

        $FieldDirectoryFields = \DatixDBQuery::PDO_fetch_all($sql, array('table' => $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE']), \PDO::FETCH_COLUMN);

        $customFields = getCustomFields($module);

        // Disable UDFs for the admin and medications modules.
        // TODO: Once UDFs are working again in the admin module we can remove this.
        if ($module != 'ADM' && $module != 'MED')
        {
            $extraFields = \DatixDBQuery::PDO_fetch_all('SELECT \'UDF_\'+CAST(recordid AS varchar(20)) FROM udf_fields JOIN udf_mod_link ON uml_id = recordid AND uml_module = :module UNION ALL SELECT \'UDF_\'+CAST(recordid AS varchar(20)) FROM udf_fields WHERE recordid NOT IN (SELECT uml_id FROM udf_mod_link)', array('module' => $module), \PDO::FETCH_COLUMN);
        }
        else
        {
            $extraFields = [];
        }

        $PossibleFields = SafelyMergeArrays(array($FieldDirectoryFields, $ModuleDefs[$module]['LINKED_FIELD_ARRAY'], $extraFields, $customFields));

        if (is_array($PossibleFields))
        {
            foreach ($PossibleFields as $key => $FDfield)
            {
                if (in_array($FDfield, $customFields))
                {
                    // custom fields keys/values are the field names/descriptions
                    $availCols[$key] = $FDfield;
                }
                elseif (!$FieldDefsExtra[$module][$FDfield]['NoListCol_Override'] && (!$FieldDefs[$module][$FDfield] || !$FieldDefsExtra[$module][$FDfield]['NoListCol']))
                {
                    $availCols[$FDfield] = \Labels_FieldLabel::GetFieldLabel($FDfield,$FieldDefsExtra[$module][$FDfield]['Title'], $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE'], $module).($FieldDefsExtra[$module][$FDfield]['SubmoduleSuffix'] ? ' '.$FieldDefsExtra[$module][$FDfield]['SubmoduleSuffix'] : '');
                }
            }
        }

        $currentCols = array();

        if (is_array($list_columns))
        {
            foreach ($list_columns as $colname => $colval)
            {
                if (in_array($colname, array_keys($customFields)))
                {
                    $currentCols[$colname] = $customFields[$colname];
                }
                else
                {
                    $currentCols[$colname] = \Labels_FieldLabel::GetFieldLabel($colname,$FieldDefsExtra[$module][$colname]['Title'], $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE'], $module).($FieldDefsExtra[$module][$colname]['SubmoduleSuffix'] ? ' '.$FieldDefsExtra[$module][$colname]['SubmoduleSuffix'] : '');
                }
            }
        }

        asort($availCols);

        $this->response->build('src/admin/views/EditColumns.php', array(
            'module' => $module,
            'listing_id' => $listing_id,
            'Listing' => $Listing,
            'availCols' => $availCols,
            'currentCols' => $currentCols
        ));
    }

    /**
     * Get title for screen
     * @param $listing_id
     * @param $moduleName
     * @return string
     */
    public function getPageTitle($listing_id, $moduleName)
    {
        $sql = 'SELECT lst_title
                FROM web_listing_designs
                WHERE recordid = :recordid';
        $listingTitle = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $listing_id), \PDO::FETCH_COLUMN);

        if ($listing_id > 0 && $listingTitle != null) {
            return 'Listing design - ' . $moduleName . ' - ' . $listingTitle;
        } elseif ($listing_id == 0) {
            return 'Listing design - ' . $moduleName . ' - Datix Listing Design Template';
        } else {
            return 'Listing design - ' . $moduleName;
        }
    }
}