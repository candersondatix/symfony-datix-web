<?php
namespace src\admin\controllers;

use src\framework\controller\Controller;

/**
 * Controls the code setup section of the extra field edit form.
 */
class UDFCodeSetupController extends Controller
{
	/**
     * Displays link for Code setup when UDF field is a coded field.
     */
    public function UDFCodeSetupOptions()
    {
    	$this->response->build('src/admin/views/UDFCodeSetupOptions.php', array(
    		'id' => $this->request->getParameter('recordid'),
    		'codeSetup' => _tk('code_setup')
    	));
    }
}