<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ListListingDesignsTemplateController extends TemplateController
{
    /**
     * @desc Constructs HTML for the listing design admin page, showing links to edit
     * listing designs for all modules.
     */
    function listlistingdesigns()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $module = GetDefaultModule(array('default' => \Sanitize::getModule($this->request->getParameter('module'))));

        foreach (getModArray() as $mod => $name)
        {
            if (!in_array($mod, array('DST', 'DAS', 'TOD')))
            {
                $PanelArray[$mod] = array('Label' => $name);
            }
        }

        $SelectSql = '
            SELECT
                recordid,
                LST_TITLE
            FROM
                WEB_LISTING_DESIGNS
            WHERE
                lst_module = :module
        ';

        $this->title = 'Listing Design';
        $this->module = 'ADM';
        $this->menuParameters = array(
            'panels' => $PanelArray,
            'current_panel' => $module,
            'floating_menu' => false
        );

        $this->response->build('src/admin/views/ListListingDesigns.php', array(
            'ModuleDefs' => $ModuleDefs,
            'module' => $module,
            'SelectSql' => $SelectSql
        ));
    }
}