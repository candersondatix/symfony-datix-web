<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ListFormDesignsTemplateController extends TemplateController
{
    function listformdesigns()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));

        if (!$module)
        {
            $module = 'INC';
        }

        $CurrentModule = GetDefaultModule(array('default' => \Sanitize::getModule($this->request->getParameter('module'))));

        foreach (getModArray() as $mod => $name)
        {
            if (!in_array($mod, GetModulesWithNoFormDesign()))
            {
                $PanelArray[$mod] = array('Label' => $name);
            }
        }

        $this->title = 'Form Design';
        $this->module = 'ADM';
        $this->menuParameters = array(
            'panels' => $PanelArray,
            'current_panel' => $CurrentModule,
            'floating_menu' => false
        );

        $this->response->build('src/admin/views/ListFormDesigns.php', array(
            'ModuleDefs' => $ModuleDefs,
            'CurrentModule' => $CurrentModule
        ));
    }
}