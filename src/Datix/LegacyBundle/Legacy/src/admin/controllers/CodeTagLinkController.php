<?php
namespace src\admin\controllers;

use src\framework\controller\Controller;
use src\framework\controller\Request;
use src\framework\controller\Response;

/**
 * Controls the processes behind linking tags to codes
 */
class CodeTagLinkController extends Controller
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }

    /**
     * Saves a set of links between tags and codes
     */
	public function saveTagField()
	{
        //delete existing linked fields and then save posted ones.
        \DatixDBQuery::PDO_query('DELETE FROM code_tag_links WHERE field = :field AND [table] = :table', array('field' => $this->request->getParameter('field'), 'table' => $this->request->getParameter('table')));

        //need to look for parameters of the form codetags_<code>_<group>, which will contain arrays of tags to be saved
        foreach($this->request->getParameters() as $Parameter => $Values)
        {
            $ParameterParts = explode('_', $Parameter);

            if($ParameterParts[0] == 'codetags')
            {
                foreach($Values as $Value)
                {
                    \DatixDBQuery::PDO_query(
                        'INSERT INTO code_tag_links ([group], field, [table], code, tag) VALUES (:group, :field, :table, :code, :tag)',
                        array(
                            'group' => $ParameterParts[2],
                            'field' => $this->request->getParameter('field'),
                            'table' => $this->request->getParameter('table'),
                            'code' => $ParameterParts[1],
                            'tag' => $Value,
                        )
                    );
                }
            }
        }

        AddSessionMessage('INFO', _tk('tag_field_saved'));
        $this->redirect('app.php?action=linktagfield&field='. $this->request->getParameter('field') . '&table=' . $this->request->getParameter('table'));
    }

    /**
     * Retrieves the HTML for the multicode dropdown used to assign tags to codes.
     *
     * Requires the code and group id to be passed in the request
     */
    public function GetCodeTagLinkEditDropdown()
    {
        $Code = $this->request->getParameter('code');
        $Group = $this->request->getParameter('group');
        if(!$Code || !$Group)
        {
            throw new \InvalidParameterException();
        }

        //find existing tags attached to this code
        $ExistingTags = \DatixDBQuery::PDO_fetch_all('SELECT tag FROM code_tag_links WHERE [table] = :table AND field = :field AND code = :code AND [group] = :group',
            array(
                'field' => $this->request->getParameter('field'),
                'table' => $this->request->getParameter('table'),
                'code' => $this->request->getParameter('code'),
                'group' => $this->request->getParameter('group'),
            ), \PDO::FETCH_COLUMN);

        $AvailableTags = \DatixDBQuery::PDO_fetch_all('SELECT recordid, name FROM code_tags WHERE group_id = :group',
            array(
                'group' => $this->request->getParameter('group'),
            ), \PDO::FETCH_KEY_PAIR);

        $GroupMulticode = \Forms_SelectFieldFactory::createSelectField(
            'codetags_'.$Code.'_'.$Group,
            '',
            (!is_array($ExistingTags) ? '' : implode(' ',$ExistingTags)),
            '',
            true);
        $GroupMulticode->setIgnoreMaxLength();
        $GroupMulticode->setCustomCodes((!empty($AvailableTags) ? $AvailableTags : array()));

        $this->response->build('src/admin/views/GetCodeTagLinkEditDropdown.php', array(
            'multicode' => $GroupMulticode
        ));
    }



}