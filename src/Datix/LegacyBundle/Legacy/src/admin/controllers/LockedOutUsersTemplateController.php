<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\users\model\UserModelFactory;
use src\framework\query\FieldCollection;

class LockedOutUsersTemplateController extends TemplateController
{
	public function locked_users()
	{
        $this->title      = _tk('locked-users-title').' '._tk('active_only');
        $this->module     = 'ADM';
        $this->hasPadding = false;

        $orderby = $this->request->getParameter('orderby');
        $orderby = \Sanitize::SanitizeString((!is_null($orderby)) ? $orderby : 'login');

        $order   = ($this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';

        $Factory = new UserModelFactory();

        list($query, $where, $fields) = $Factory->getQueryFactory()->getQueryObjects();

        $fields->field('staff.login_tries')->notEq(-1)
            ->field('staff.lockout')->eq('Y')
            ->field('rep_approved')->eq('FA');
        $now = date('Y-m-d');
        $accessStart[] = (new FieldCollection)->field('sta_daccessstart')->isNull();
        $accessStart[] = (new FieldCollection)->field('sta_daccessstart')->ltEq($now);
        $newWhereParams[] = array('condition' => 'OR', 'parameters' => $accessStart);

        $accessEnd[] = (new FieldCollection)->field('sta_daccessend')->isNull();
        $accessEnd[] = (new FieldCollection)->field('sta_daccessend')->gt($now);
        $newWhereParams[] = array('condition' => 'OR', 'parameters' => $accessEnd);

        $dateClosed[] = (new FieldCollection)->field('sta_dclosed')->isNull();
        $dateClosed[] = (new FieldCollection)->field('sta_dclosed')->gt($now);
        $newWhereParams[] = array('condition' => 'OR', 'parameters' => $dateClosed);

        $where->add($fields);
        $where->addArray($newWhereParams);
        $where->addArray($newWhereParams);
        $where->addArray($newWhereParams);
        $query->where($where)
              ->orderBy(array(array($orderby, $order)));
        $userCollection = $Factory->getCollection();
        $userCollection->setQuery($query);

        if ($userCollection->isEmpty())
        {
            AddSessionMessage('INFO', _tk('no-locked-out-users'));
        }

        $this->response->build('src/admin/views/LockedOutUsers.php',
            array(
                'users' => $userCollection,
                'order' => $order,
                'orderby' => $orderby,
            ));
	}
}