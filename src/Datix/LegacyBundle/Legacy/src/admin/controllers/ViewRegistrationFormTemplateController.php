<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

class ViewRegistrationFormTemplateController extends TemplateController
{
    function viewregistrationform()
    {
        $factory = new UserModelFactory();
        $user = $factory->getMapper()->find(\Sanitize::SanitizeInt($this->request->getParameter('recordid')));

        require_once 'Source/contacts/BasicForm_Registration.php';

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'ADM',
            'level' => 2,
            'form_type' => 'ReadOnly',
            'id' => GetParm('REG_FORM_DESIGN', null)
        ));

        //Since this is not a standard read only form (it can be saved) we want to manually hide all the mandatory fields.
        $FormDesign->MandatoryFields = array();

        $FormDesign->ExpandFields['con_action'][] = array (
            'field' => 'con_reason',
            'alerttext' => '',
            'values' => array (
                0 => 'REJ',
            ),
        );

        StoreContactLabels();

        $ConTable = new \FormTable('ReadOnly', '', $FormDesign);
        $ConTable->MakeForm($FormArray, $user->getVars(), 'CON');

        $this->title = $FormDesign->FormTitle;
        $this->subtitle = $FormDesign->FormTitleDescr;
        $this->image = 'Images/login_sm.gif';
        $this->module = 'ADM';

        $this->response->build('src/admin/views/ViewRegistrationForm.php', array(
            'recordid' => $this->request->getParameter('recordid'),
            'ConTable' => $ConTable
        ));
    }

    function approveuserregistration()
    {
        require_once 'Source/libs/Email.php';

        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        $factory = new UserModelFactory();

        $user = $factory->getMapper()->find($recordid);

        if(empty($user))
        {
            throw new \Exception('Specified user does not exist. Unable to proceed with request.');
        }

        switch ($this->request->getParameter('con_action'))
        {
            case 'APP':
                $user->lockout = 'N';
                $user->rep_approved = 'FA';
                $user->login_tries = 0;

                // We need to pull this put of the if condition because it's causing errors getting the object properties
                $userUpdated = $factory->getMapper()->update($user);

                if ($userUpdated && $user->con_email != '' && bYN(GetParm('EMAIL_NEW_ACCOUNTS', 'N')))
                {
                    Registry::getInstance()->getLogger()->logEmail('Sending registration approved e-mail to '. $user->login.' ('.$user->con_email.').');

                    $emailSender = EmailSenderFactory::createEmailSender('INC', 'RegistrationApproved');
                    $emailSender->setModuleToStore('ADM');
                    $emailSender->addRecipient($user);
                    $emailSender->sendEmails($user->getVars());


                    if (count($emailSender->getFailedDomainValidation()) > 0)
                    {
                        AddSessionMessage('ERROR', 'An e-mail was not sent to the user as their domain is not permitted.');
                    }
                }

                $this->response = $this->call('src\\users\\controllers\\UserTemplateController', 'edituser', array('recordid' => $recordid, 'lockout' => 'N'));
                break;
            case 'REJ':
                if ($user->con_email != '' && bYN(GetParm('EMAIL_NEW_ACCOUNTS', 'N')))
                {
                    Registry::getInstance()->getLogger()->logEmail('Sending registration rejected e-mail to '. $user->login.' ('.$user->con_email.').');

                    $emailSender = EmailSenderFactory::createEmailSender('INC', 'RegistrationRejected');
                    $emailSender->addRecipient($user);
                    $emailSender->sendEmails(array_merge($user->getVars(), array('reason' => $this->request->getParameter('con_reason'))));

                    if (count($emailSender->getFailedDomainValidation()) > 0)
                    {
                        AddSessionMessage('ERROR', 'An e-mail was not sent to the user as their domain is not permitted.');
                    }
                }
                
                $factory->getMapper()->delete($user);
                
                $this->response = $this->call('src\\admin\\controllers\\RegistrationController', 'listawaitingapproval');
                break;
            default:
                $this->response = $this->call('src\\admin\\controllers\\RegistrationController', 'listawaitingapproval');
        }

        obExit();
    }
}