<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ImportTableDataTemplateController extends TemplateController
{
    /**
     * Show the form to import table data
     */
    function importtabledata()
    {
        $this->title = _tk('import_table_data');
        $this->module = 'ADM';

        $table = new \FormTable();
        $uploadField = new \FormField();
        $table->MakeTitleRow('<b>'. _tk('import_table_data') .'</b>');
        $table->MakeRow('<img src="images/Warning.gif" alt="Warning"> <b>Load file</b>', $uploadField->MakeCustomField('<input id="importfile" name="importfile" type="file" size="50"/>'));
        $table->MakeTable();

        $this->response->build('src/admin/views/ShowImportTableDataForm.php', array(
            'table' => $table
        ));
    }
}