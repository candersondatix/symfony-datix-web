<?php
namespace src\admin\controllers;

use src\admin\model\systemFlags\ExtraFieldsInGroupsFlag;
use src\admin\model\systemFlags\ExtraFieldsInMultipleGroupsFlag;
use src\admin\model\systemFlags\ExtraFieldsWithConflictingDataFlag;
use src\admin\model\systemFlags\InvestigatorsEmailSecGroupFlag;
use src\admin\model\systemFlags\LogLevelFlag;
use src\admin\model\systemFlags\RecordUpdateEmailDatetimeFlag;
use src\admin\model\systemFlags\StaffEmplFiltersFlag;
use src\admin\model\systemFlags\StaffFieldUsersFlag;
use src\admin\model\systemFlags\TextFieldGroupWhereClauseFlag;
use src\admin\model\systemFlags\UserDefinedActionsFlag;
use src\admin\model\systemFlags\UserDefinedFieldsFlag;
use src\admin\model\systemFlags\UserDefinedSectionsFlag;
use src\framework\controller\TemplateController;

/**
 * Controls actions around automated system checks.
 */
class SystemDiagnosisController extends TemplateController
{
    /**
     * Currently just checks for various performance related issues.
     *
     * Checks are constructed as "flags", which have a reportable GOOD/BAD state.
     */
    public function DiagnoseSystemProblems()
    {
        $this->module = 'ADM';
        $this->title = 'Diagnose system problems';

        $flags[] = new StaffEmplFiltersFlag();
        $flags[] = new InvestigatorsEmailSecGroupFlag();
        $flags[] = new UserDefinedFieldsFlag();
        $flags[] = new UserDefinedSectionsFlag();
        $flags[] = new UserDefinedActionsFlag();
        $flags[] = new LogLevelFlag();
        $flags[] = new StaffFieldUsersFlag();
        $flags[] = new TextFieldGroupWhereClauseFlag();
        $flags[] = new RecordUpdateEmailDatetimeFlag();
        $flags[] = new ExtraFieldsInGroupsFlag();
        $flags[] = new ExtraFieldsInMultipleGroupsFlag();
        $flags[] = new ExtraFieldsWithConflictingDataFlag();

        $this->response->build('src/admin/views/DiagnoseSystemProblems.php', array(
            'flags' => $flags
        ));
    }
}