<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

/**
 * Controls the LDAP configuration settings.
 */
class LDAPConfigController extends TemplateController
{
    /**
     * The key used to encrypt/decrypt the LDAP p_a_s_s_w_o_r_d.
     *
     * @param string
     */
    const LDAP_KEY = 'DATIXLDAP';

    /**
     * Index screen for LDAP configuration settings.
     */
    public function ldapconfig()
    {
        if ($this->request->getMethod() == 'POST')
        {
            $this->save();
        }

        $this->title = _tk('ldap_configuration');
        $this->module = 'ADM';

        $LDAPTypeFieldObj = \Forms_SelectFieldFactory::createSelectField('LDAP_TYPE', 'ADM', GetParm('LDAP_TYPE', 'GROUPS'), '');
        $LDAPTypeFieldObj->setCustomCodes(array('USERS' => 'Sync only user details', 'GROUPS' => 'Sync user details and security groups/profiles'));

        if(GetParm('LDAP_TYPE', 'GROUPS') == 'USERS')
        {
            $DN = \DatixDBQuery::PDO_fetch_all('SELECT dn from sec_ldap_all', array(), \PDO::FETCH_COLUMN);
            $ADFieldObj = new \FormField();
            $ADFieldObj->MakeInputToMultiListbox("ldap_dn", $DN);

            $ADFieldObj->Field .= '<br /><a href="javascript:dnChooserPopup(\'ldap_dnInput\',\'\');">Browse directory</a>';
        }

        $this->response->build('src/admin/views/LDAPConfig.php', array(
            'username' => GetParm('LDAP_USER'),
            'ADFieldObj' => $ADFieldObj,
            'LDAPTypeFieldObj' => $LDAPTypeFieldObj,
        ));
    }

    /**
     * Fetches and unencrypts the LDAP p_a_s_s_w_o_r_d.
     *
     * @return string
     */
    public static function getPassword()
    {
        //remove appended and prefixed !s
        $password = GetParm('LDAP_PASSWORD');

        $password = substr($password, 1, \UnicodeString::strlen($password) - 2);

        return self::convert($password, self::LDAP_KEY);
    }

    /**
     * Saves the LDAP config settings.
     */
    public function save()
    {
        SetGlobal('LDAP_USER', $this->request->getParameter('ldap_user'), true);
        SetGlobal('LDAP_TYPE', $this->request->getParameter('LDAP_TYPE'), true);

        if ($this->request->getParameter('ldap_password') != '')
        {
            //need to add !s before and after to avoid leading/following spaces which are stripped off when retrieving from the db
            SetGlobal('LDAP_PASSWORD', '!'.self::convert($this->request->getParameter('ldap_password'), self::LDAP_KEY).'!', true);
        }

        \DatixDBQuery::PDO_query('DELETE FROM sec_ldap_all');

        if (GetParm('LDAP_TYPE', 'GROUPS') == 'USERS')
        {
            if(is_array($this->request->getParameter('ldap_dn')))
            {
                foreach($this->request->getParameter('ldap_dn') as $DN)
                {
                    \DatixDBQuery::PDO_build_and_insert('sec_ldap_all', array('updatedby' => $_SESSION['login'], 'updateddate' => '', 'dn' => $DN));
                }
            }
        }

        AddSessionMessage('INFO', _tk('ldap_configuration_saved'));
    }

    /**
     * Used to encrypt/decrypt the LDAP p_a_s_s_w_o_r_d.
     *
     * http://www.phpbuilder.com/board/showthread.php?t=10326721
     *
     * @param  string  $str  The string to convert.
     * @param  string  $key  The key used for encryption, must be 8-32 characters without spaces.
     *
     * @return string  $str  The converted string.
     */
    protected static function convert($str, $key)
    {
        if ($key == '' || \UnicodeString::strlen($key) < 8)
        {
            return $str;
        }

        $key = str_replace(' ', '', $key);
        $kl = \UnicodeString::strlen($key) < 32 ? \UnicodeString::strlen($key) : 32;

        $k = array();

        for ($i = 0; $i < $kl; $i++)
        {
            $k[$i] = ord($key{$i})&0x1F;
        }

        $j = 0;

        for ($i = 0; $i < \UnicodeString::strlen($str); $i++)
        {
            $e = ord($str{$i});
            $str{$i} = $e&0xE0 ? chr($e^$k[$j]) : chr($e);
            $j++;
            $j = $j == $kl ? 0 : $j;
        }

        return $str;
    }
}