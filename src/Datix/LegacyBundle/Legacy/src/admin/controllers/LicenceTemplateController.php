<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class LicenceTemplateController extends TemplateController
{
    public function licence()
    {
        global $scripturl;

        // Set defaults for request parameters
        if (!$this->request->getParameter('saveok'))
        {
            $this->request->setParameter('saveok', 'false');
        }

        if (!$this->request->getParameter('licenceOnLogin'))
        {
            $this->request->setParameter('licenceOnLogin', 'false');
        }

        if ($this->request->getMethod() == 'POST' && ($this->request->getParameter('rbWhat') == 'save' || $this->request->getParameter('rbWhat') == 'savelogin'))
        {
            $rbWhat = ($this->request->getParameter('rbWhat') == 'savelogin' ? true : false);
            $this->saveLicence($rbWhat);
        }

        if ($this->request->getParameter('licenceOnLogin') == 'false' && !IsFullAdmin())
        {
            $this->redirect($scripturl);
        }

        // Retrieve licensing information
        require_once 'externals/keygen/DatixLicenceKeyFactory.php';
        require_once 'externals/keygen/DatixLicenceKey120.php';
        require_once 'externals/keygen/DatixLicenceKey121.php';
        require_once 'externals/keygen/DatixLicenceKey122.php';
        require_once 'externals/keygen/DatixLicenceKey.php';
        require_once 'externals/keygen/DatixLicenceKey114.php';

        $LicenceKey = \DatixLicenceKeyFactory::createLicenceKey(GetParm('LICENCE_KEY_WEB'));
        $LicenceKey->SetLicenceKey(GetParm('LICENCE_KEY_WEB'));
        $LicenceKey->SetClientName(GetParm('CLIENT'));

        if(!$LicenceKey->validateLicenceKey())
        {
            //this is an invalid key (possibly from a pre-11.4 version - we don't want to throw
            //an error, since that will prevent people from changing the key, so just blank it out
            //and add a message for the user.
            AddSessionMessage('ERROR', 'The current licence key is not valid for this version. A system administrator will need to insert an up-to-date licence key.');
            $LicenceKey = \DatixLicenceKeyFactory::createLicenceKey('');
            $LicenceKey->SetClientName(GetParm('CLIENT'));
            $LicenceKey->DecodeKey();
            $OldKey = true;
        }
        else
        {
            $LicenceKey->DecodeKey();
        }

        $this->title = 'Software Licensing';
        $this->module = 'ADM';
        $this->hasPadding = false;

        if (!$_SESSION['logged_in'])
        {
            $this->hasMenu = false;
        }

        $LicenceTable = new \FormTable();

        if (IsFullAdmin())   // Start of admin user section
        {
            $LicenceTable->MakeTitleRow((($this->request->getParameter('saveok') == 'true') ? "New" : "Current") . " licence details");

            $LicenceRow = new \FormField();

            $LicenceTable->MakeRow('<b>Client name:</b>', $LicenceRow->MakeCustomField($LicenceKey->GetClientName()));

            if(!$OldKey)
            {
                $LicenceTable->MakeRow('<b>Licence key:</b>', $LicenceRow->MakeCustomField($LicenceKey->GetLicenceKey()));

                if ($LicenceKey->GetExpiryDate())
                {
                    $LicenceTable->MakeRow('<b>Expiry date:</b>', $LicenceRow->MakeCustomField($LicenceKey->GetExpiryDate()));
                }
                else
                {
                    $LicenceTable->MakeRow('<b>Expiry date:</b>', $LicenceRow->MakeCustomField('Perpetual licence: no expiry date.'));
                }

                $modules = $LicenceKey->GetLicencedModules();
                $modulesStr = '';

                if ($modules)
                {
                    foreach($modules as $modId => $modLicenced)
                    {
                        if($modLicenced)
                        {
                            $modulesStr .= $LicenceKey->moduleArray[$modId].'<br>';
                        }
                    }

                    $LicenceTable->MakeRow('<b>Licensed modules:</b>', $LicenceRow->MakeCustomField($modulesStr));

                }
            }
            else
            {
                $LicenceTable->MakeRow('<b>Licence key:</b>', $LicenceRow->MakeCustomField(GetParm('LICENCE_KEY_WEB')));
            }

            if ($this->request->getParameter('saveok') == 'false')
            {
                $ClientName = $LicenceKey->GetClientName() . '<input type="hidden" name="newclientname" size="40" maxlength="64" value="' . $LicenceKey->GetClientName() .'" />';

                $licenceField = '
                <input type="text" name="newkey" size="80" value="" />
                <input type="submit" value="Activate" class="button" name="btnSave" onclick="newclientname.value=newclientname.value.toUpperCase();newkey.value=newkey.value.toUpperCase();document.frmLicence.rbWhat.value = \''.($this->request->getParameter('licenceOnLogin') == 'true' ? "savelogin" : "save").'\';" />';

                $LicenceTable->MakeTitleRow("New licence details");
                $LicenceTable->MakeRow('<b>Client name:</b>', $LicenceRow->MakeCustomField($ClientName));
                $LicenceTable->MakeRow('<b>Licence key:</b>', $LicenceRow->MakeCustomField($licenceField));
            }
        }

        $this->response->build('src/admin/views/LicenceForm.php', array(
            'LicenceTable' => $LicenceTable,
            'invalid' => $this->request->getParameter('invalid')
        ));
    }

    private function saveLicence($licenceOnLogin = false)
    {
        $AdminUser = $_SESSION['AdminUser'];

        if (!$licenceOnLogin && !$AdminUser)
        {
            $this->redirect('app.php');
        }

        if (!$this->request->getParameter('newclientname') || !$this->request->getParameter('newkey'))
        {
            if (!$this->request->getParameter('newclientname'))
            {
                AddSessionMessage('ERROR', 'Please enter a client name.');
            }

            if (!$this->request->getParameter('newkey'))
            {
                AddSessionMessage('ERROR', 'Please enter a valid licence key.');
            }

            $this->redirect('app.php?action=licence&saveok=false&licenceOnLogin='.$licenceOnLogin);
        }

        // Retrieve licensing information
        require_once 'externals/keygen/DatixLicenceKeyFactory.php';
        require_once 'externals/keygen/DatixLicenceKey120.php';
        require_once 'externals/keygen/DatixLicenceKey121.php';
        require_once 'externals/keygen/DatixLicenceKey122.php';
        require_once 'externals/keygen/DatixLicenceKey.php';
        require_once 'externals/keygen/DatixLicenceKey114.php';

        $LicenceKey = \DatixLicenceKeyFactory::createLicenceKey();
        $LicenceKey->SetLicenceKey($this->request->getParameter('newkey'));
        $LicenceKey->SetClientName($this->request->getParameter('newclientname'));

        if (!$LicenceKey->validateLicenceKey())
        {
            AddSessionMessage('ERROR', 'Please enter a valid licence key');
            $this->redirect('app.php?action=licence&saveok=false&licenceOnLogin='.$licenceOnLogin);
        }

        $LicenceKey->DecodeKey();

        if ($LicenceKey->validateVersion())
        {
            $LicenceKeyVersions = $LicenceKey->GetCONSTVersions();
            AddSessionMessage('ERROR', 'This licence key is only valid for version '.$LicenceKeyVersions[$LicenceKey->getVersion()]['label']);
            $this->redirect('app.php?action=licence&saveok=false&licenceOnLogin='.$licenceOnLogin);
        }

        //delete existing licence key
        $sql = "DELETE FROM globals WHERE parameter = 'LICENCE_KEY_WEB'";
        \DatixDBQuery::PDO_query($sql);

        //insert new licence key
        $sql = "INSERT INTO globals (parameter, parmvalue) VALUES (:parameter, :parmvalue)";
        \DatixDBQuery::PDO_query($sql, array(
            'parameter' => 'LICENCE_KEY_WEB',
            'parmvalue' => $LicenceKey->GetLicenceKey()
        ));

        //update the cached licence key value
        $_SESSION["Globals"]['LICENCE_KEY_WEB'] = $LicenceKey->GetLicenceKey();

        AddSessionMessage('INFO', 'Licence details updated successfully');

        $this->redirect('app.php?action=licence&saveok=true&licenceOnLogin='.$licenceOnLogin);
    }
}