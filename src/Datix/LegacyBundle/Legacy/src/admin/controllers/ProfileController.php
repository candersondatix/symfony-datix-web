<?php
namespace src\admin\controllers;

use src\framework\controller\Controller;
use src\framework\controller\Request;
use src\framework\controller\Response;

use src\profiles\model\profile\ProfileModelFactory;

/**
 * Controls the process of managing profiles
 */
class ProfileController extends Controller
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
	
    /**
     * Deletes a specified profile.
     * Linked data is removed via an observer.
     */
	public function deleteProfile()
	{
        $factory = new ProfileModelFactory();
        $profile = $factory->getMapper()->find($this->request->getParameter('recordid'));

        if(!empty($profile))
        {
            $factory->getMapper()->delete($profile);
        }

        $this->redirect('app.php?action=listprofiles');
    }

    /**
     * Copies a profile, including linked configuration parameters and groups, but without users.
     */
	public function copyProfile()
	{
        $RecordidToCopy = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        if($RecordidToCopy > 0)
        {
            $factory = new ProfileModelFactory();
            $profile = $factory->getMapper()->find($RecordidToCopy);

            //get profile-specific data
            $data = $profile->getVars();

            //get parameter data
            $params = \DatixDBQuery::PDO_fetch_all('SELECT lpp_parameter, lpp_value FROM link_profile_param WHERE lpp_profile = :recordid', array('recordid' => $RecordidToCopy));

            foreach ($params as $row)
            {
                $data[$row['lpp_parameter']] = $row['lpp_value'];
            }

            //get new id
            $data['recordid'] = GetNextRecordID('profiles', true);

            //ensure this is clearly marked as the copied profile.
            $data['pfl_name'] .= ' (Copy)';

            //call standard save procedure for profiles. In the future this should probably be
            //a method on a Profile object.
            $this->call('src\\admin\\controllers\\ProfileController', 'saveProfileData', $data);

            //get security group data
            $Groups = \DatixDBQuery::PDO_fetch_all('SELECT lpg_group FROM link_profile_group WHERE lpg_profile = :recordid', array('recordid' => $RecordidToCopy), \PDO::FETCH_COLUMN);

            //Save a link to each group against the profile
            if(is_array($Groups) && !empty($Groups))
            {
                foreach($Groups as $Group)
                {
                    \DatixDBQuery::PDO_build_and_insert('link_profile_group', array('lpg_profile' => $data['recordid'], 'lpg_group' => $Group ,'updatedby' => $_SESSION['initials'],'updateddate' => date('d-M-Y H:i:s')));
                }
            }

            $this->redirect('app.php?action=editprofile&recordid='.$data['recordid']);
        }
        else
        {
            $this->redirect('app.php?action=listprofiles');
        }
    }

    /**
     * Updates a specified profile with supplied values.
     */
    function saveProfile()
    {
        $data = $this->request->getParameters();

        if($data['recordid'] == '')
        {
            $data['recordid'] = GetNextRecordID('profiles', true);
            $data['updatedby'] = '';
        }

        $this->call('src\\admin\\controllers\\ProfileController', 'saveProfileData', $data);

        if($data['redirect_url'])
        {
            $this->redirect($data['redirect_url'] . '&pfl_id='.$data['recordid']);
        }

        AddSessionMessage('INFO', _tk('profile_saved'));
        $this->redirect('app.php?action=editprofile&recordid='.$data['recordid']);
    }

    /**
     * Given a set of data (passed in the request), updates an existing profile with that data
     */
    function saveProfileData()
    {
        $factory = new ProfileModelFactory();
        $currentProfile = $factory->getMapper()->find($this->request->getParameter('recordid'));
        $profile = $factory->getEntityFactory()->createObject($this->request->getParameters(), $currentProfile);

        $factory->getMapper()->save($profile);

        //TODO: be more selective about this data
        $data = $this->request->getParameters();

        //saving parameters.
        $sql = 'DELETE FROM link_profile_param WHERE lpp_profile = :recordid';
        $result = \DatixDBQuery::PDO_query($sql, array('recordid' => $profile->recordid));

        foreach(getConfigurationParameters() as $param)
        {
            if(is_array($data[$param]))
            {
                $data[$param] = implode(' ', $data[$param]);
            }

            if($data[$param] != '')
            {
                \DatixDBQuery::PDO_build_and_insert('link_profile_param', array('lpp_profile' => $profile->recordid, 'lpp_parameter' => $param, 'lpp_value' => $data[$param]));
            }
        }

        require_once 'Source/libs/LDAP.php';
        $ldap = new \datixLDAP();

        // Save LDAP mappings if enabled
        if ($ldap->LDAPEnabled())
        {
            \DatixDBQuery::PDO_query('DELETE from sec_ldap_profiles WHERE pfl_id = :pfl_id', array('pfl_id' => $profile->recordid));

            if($data['ldap_dn'])
            {
                foreach ($data['ldap_dn'] as $ldap_dn)
                {
                    \DatixDBQuery::PDO_build_and_insert('sec_ldap_profiles', array('pfl_id' => $profile->recordid, 'ldap_dn' => $ldap_dn));
                }
            }
        }
    }

    /**
     * @desc This function saves the order numbers for all profiles
     */
    function saveProfileOrder()
    {
        //get list of profiles to be ordered
        $Profiles = \DatixDBQuery::PDO_fetch_all('SELECT recordid FROM profiles', array(), \PDO::FETCH_COLUMN);

        $orderData = $this->request->getParameters();

        //TODO: find a more efficient way of validating this data
        $UniqueValidationFailed = false;
        $PositiveValidationFailed = false;
        $ValidateOk = true;
        foreach($orderData as $orderItem => $orderValue)
        {
            if(preg_match('/profile_order_[0-9]*/u',$orderItem) != 0 && $orderValue != '')
            {
                if (!$UniqueValidationFailed && count(array_keys($orderData, $orderValue)) > 1)
                {
                    AddSessionMessage('INFO', _tk('save_ldap_order_unique_validation'));
                    $ValidateOk = false;
                    $UniqueValidationFailed = true;
                }

                if (!$PositiveValidationFailed && $orderValue < 1 && $orderValue != null)
                {
                    AddSessionMessage('INFO', _tk('save_ldap_order_positive_validation'));
                    $ValidateOk = false;
                    $PositiveValidationFailed = true;
                }
            }
        }

        if($ValidateOk)
        {
            foreach ($Profiles as $profileId)
            {
                $orderno = $this->request->getParameter('profile_order_' . $profileId);
                if (!empty($orderno) || $orderno == 0)
                {
                    if ($orderno == "")
                    {
                        $orderno = null;
                    }
                    \DatixDBQuery::PDO_query('UPDATE profiles SET pfl_ldap_order = :order_no WHERE recordid = :recordid', array('order_no' => $orderno, 'recordid' => $profileId));
                }
            }

            AddSessionMessage('INFO', _tk('save_ldap_order_success'));
        }

        $data = $this->request->getParameters();

        if(!$ValidateOk)
        {
            $data['edit'] = 1;
        }

        $this->call('src\admin\controllers\ProfileTemplateController', 'ListProfiles', $data);
    }
}