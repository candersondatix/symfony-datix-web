<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ShowUserSessionsTemplateController extends TemplateController
{
    function usersessions()
    {
        if($this->request->getParameter("delete_session") != "" && bYN(GetParm("RECORD_LOCKING"), "N"))
        {
            $proc_result = \DatixDBQuery::CallStoredProcedure(array(
            "procedure" => "EndSession",
            "parameters" => array(
                array("@session_id", $this->request->getParameter("delete_session"), SQLINT4)
                )
            ));
        }

        $this->title  = 'Active Sessions';
        $this->module = 'ADM';
        $this->image  = 'Images/icons/icon_cog_active_session.png';
        $this->hasPadding = false;

        $order = ($this->request->getParameter('order')) ? \Sanitize::SanitizeString($this->request->getParameter('order')) : 'ses_start';
        $dir   = ($this->request->getParameter('dir') && in_array($this->request->getParameter('dir'), array('ASC', 'DESC'))) ? $this->request->getParameter('dir') : 'ASC';

        $ADMWhere = MakeSecurityWhereClause('','ADM');
        $sSQL = 'SELECT sessions.recordid, fullname, ses_application, ses_start, convert(varchar,ses_start,120) as real_time FROM sessions, staff WHERE ses_userid = staff.recordid'.($ADMWhere ? ' AND staff.recordid IN (SELECT recordid FROM staff WHERE '.$ADMWhere.')' : '').' ORDER BY ' . $order . ' ' . $dir;
        $resultArray = \DatixDBQuery::PDO_fetch_all($sSQL);

        $this->response->build('src/admin/views/ActiveSessions.php', array(
        	'resultArray' => $resultArray,
            'order'       => $order,
            'dir'         => $dir
        ));
    }
}

