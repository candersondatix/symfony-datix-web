<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class SaveDatixwebConfController extends Controller
{
    function savedatixwebconf()
    {
        if (empty($_POST))
        {
            // This can occur when using SSO and browsing the application with multiple tabs.
            // If the session ends in one tab (because of a timeout/manually logging out) then the form data is lost following a redirect
            // to the save page, so we redirect again here to ensure the configuration options aren't all blanked out.
            AddSessionMessage('ERROR', _tk('form_data_lost_error'));
            $this->redirect('app.php?action=setup');
        }
        
        if ($this->request->getParameter('adminaction') == 'save')
        {
            // Temporary fix. Ideally this would be generalised by identifying the variable type of globals.
            $this->request->setParameter('RECORDLOCK_TIMEOUT', intval($this->request->getParameter('RECORDLOCK_TIMEOUT')));
            $this->request->setParameter('DIF_TIMER_MINS', intval($this->request->getParameter('DIF_TIMER_MINS')));

            // Make sure a zero value is not saved, since this would couse continuous AJAX calls to check locking
            if ($this->request->getParameter('RECORDLOCK_TIMEOUT') == 0)
            {
                $this->request->setParameter('RECORDLOCK_TIMEOUT', 10);
            }

            if ($this->request->getParameter('DIF_TIMER_MINS') < -1
                || $this->request->getParameter('DIF_TIMER_MINS') == ''
                || !is_int($this->request->getParameter('DIF_TIMER_MINS')))
            {
                AddSessionMessage ('ERROR', sprintf(_tk('minutes_to_timeout_lbl') . ":"
                    . "{$this->request->getParameter('DIF_TIMER_MINS')} is not a valid value"));
                $this->call(
                    'src\admin\controllers\ShowDatixwebConfTemplateController',
                    'setup',
                    $this->request->getParameters()
                );
                $this->request->setParameter('DIF_TIMER_MINS', 10);
                return;
            }

            // DIF_OVERDUE_DAYS reverts to -1 if the form has been saved with a blank value
            if ($this->request->getParameter('DIF_OVERDUE_DAYS') == '')
            {
                $this->request->setParameter('DIF_OVERDUE_DAYS', -1);
            }

            // AJAX_WAIT_TIME cannot be < 0
            if ($this->request->getParameter('AJAX_WAIT_TIME') < 0)
            {
                $this->request->setParameter('AJAX_WAIT_TIME', '0');
            }

            // If error logging is set to no, ensure the log file is deleted
            if ($this->request->getParameter('SYSTEM_ERROR_LOGGING') && $this->request->getParameter('SYSTEM_ERROR_LOGGING') == 'N')
            {
                global $ClientFolder;

                if (file_exists($ClientFolder . '/SQLErrors.xml'))
                {
                    unlink($ClientFolder . '/SQLErrors.xml');
                }
            }

            // check permitted email domains
            $permittedEmailDomains = preg_split ("/[\r\n]+/u", $this->request->getParameter('PERMITTED_EMAIL_DOMAINS'));
            
            foreach ($permittedEmailDomains as &$emailDomain) {
                $emailDomain = \UnicodeString::trim ($emailDomain);
            }

            // save it
            require_once __DIR__ . '/../../../Source/libs/EmailDomainWhitelistClass.php';
            $whiteList = new \EmailDomainWhitelist ();
            $result    = $whiteList->setAllowedDomains($permittedEmailDomains);

            if ($result['success'] != true)
            {
                AddSessionMessage ('ERROR', sprintf(_tk('permitted_domain_invalid'), join ("\n", $result['invalid'])));
                $this->call('src\admin\controllers\ShowDatixwebConfTemplateController', 'setup', $this->request->getParameters());
                return;
            }
            else
            {
                $this->request->setParameter('PERMITTED_EMAIL_DOMAINS', join ("\n", $permittedEmailDomains));
            }
            
            // General settings
            $this->setGlobalFromRequest('SYSTEM_ERROR_LOGGING');
            $this->setGlobalFromRequest('SUPPORT_EMAIL_ADDR');
            $this->setGlobalFromRequest('WEB_MAINTMODE');
            $this->setGlobalFromRequest('WEB_MAINTMODE_MSG');
            $this->setGlobalFromRequest('CALENDAR_ONCLICK');
            $this->setGlobalFromRequest('CALENDAR_WEEKEND');
            $this->setGlobalFromRequest('VALID_FILE_TYPES');
            $this->setGlobalFromRequest('INVALID_FILE_TYPES');
            $this->setGlobalFromRequest('STAFF_NAME_DROPDOWN');
            $this->setGlobalFromRequest('WEB_TRIGGERS');
            $this->setGlobalFromRequest('REG_FORM_DESIGN');
            $this->setGlobalFromRequest('SEC_RECORD_PERMS');
            $this->setGlobalFromRequest('AJAX_WAIT_MSG');
            $this->setGlobalFromRequest('AJAX_WAIT_TIME');
            $this->setGlobalFromRequest('PROG_NOTES_EDIT');
            $this->setGlobalFromRequest('ADDITIONAL_REPORT_OPTIONS');
            $this->setGlobalFromRequest('LOGIN_USAGE_AGREEMENT');
            $this->setGlobalFromRequest('DISAGREEMENT_MSG');
            $this->setGlobalFromRequest('ENABLE_GENERATE');
            $this->setGlobalFromRequest('AUDIT_DISPLAY_DAYS');
            $this->setGlobalFromRequest('CURRENCY_CHAR');
            $this->setGlobalFromRequest('WORD_MERGE_ENGINE');
            $this->setGlobalFromRequest('LOGIN_DEFAULT_MODULE');
            $this->setGlobalFromRequest('HIDE_SENSITIVE_CONFIG_DATA');
            $this->setGlobalFromRequest('DISPLAY_LAST_LOGIN');
            $this->setGlobalFromRequest('EMAIL_NEW_ACCOUNTS');
            $this->setGlobalFromRequest('PERMITTED_EMAIL_DOMAINS');
            $this->setGlobalFromRequest('DEFAULT_PAPER_SIZE');
            $this->setGlobalFromRequest('DEFAULT_PAPER_ORIENTATION');
            $this->setGlobalFromRequest('SHOW_PANEL_INDICATORS');
            $this->setGlobalFromRequest('LISTING_REPORT_DISPLAY_NUM');

            if (IsCentrallyAdminSys() && IsCentralAdmin())
            {
                $this->setGlobalFromRequest('REMOVE_USER_PARAM_ACCESS');
            }

            // Incident module globals
            $this->setGlobalFromRequest('DIF_NO_OPEN');
            $this->setGlobalFromRequest('DIF_2_REGISTER');
            $this->setGlobalFromRequest('DIF_EMAIL_MGR');
            $this->setGlobalFromRequest('DIF_ADMIN_EMAIL');
            $this->setGlobalFromRequest('DIF_1_CUSTOM');
            $this->setGlobalFromRequest('DIF_2_CCS');
            $this->setGlobalFromRequest('DIF_1_CONTACT_REP');
            $this->setGlobalFromRequest('DIF_1_CCS');
            SetGlobalFromPost('DIF1_ONLY_FORM');
            $this->setGlobalFromRequest('DIF_1_REF_PREFIX');
            $this->setGlobalFromRequest('DIF_TIMER_MINS');
            $this->setGlobalFromRequest('RECORD_LOCKING');
            $this->setGlobalFromRequest('RECORDLOCK_TIMEOUT');
            $this->setGlobalFromRequest('DIF_1_NO_TIMEOUT');
            $this->setGlobalFromRequest('DIF_OWN_ONLY');
            $this->setGlobalFromRequest('DIF_1_ACKNOWLEDGE');
            $this->setGlobalFromRequest('DIF_OVERDUE_DAYS');
            $this->setGlobalFromRequest('DIF_OVERDUE_TYPE');
            $this->setGlobalFromRequest('DIF_2_REPORTEDBY');
            $this->setGlobalFromRequest('DIF_1_EMAIL_SELECT');
            $this->setGlobalFromRequest('DIF_1_NO_PRINT');
            $this->setGlobalFromRequest('DIF_WHERE_OVERRIDE');
            $this->setGlobalFromRequest('DIF2_DEFAULT_REPORTS');
            $this->setGlobalFromRequest('DIF_2_NORCGS');
            $this->setGlobalFromRequest('REASSIGN_HANDLER');
            $this->setGlobalFromRequest('GRANT_ACCESS_PERMISSIONS');
            $this->setGlobalFromRequest('DIF2_HIDE_CONTACTS');
            $this->setGlobalFromRequest('DIF2_REPORTER_POPULATE');
            $this->setGlobalFromRequest('INC_SHOW_AUDIT');
            $this->setGlobalFromRequest('DIF_LOGIN_MSG');
            $this->setGlobalFromRequest('DIF_SHOW_EMAIL');
            $this->setGlobalFromRequest('INC_L1_CON_MATCH_LISTING_ID');
            $this->setGlobalFromRequest('AUTO_POPULATE_REC_NAME_INC');
            $this->setGlobalFromRequest('NEW_RECORD_LVL1_INC');
            $this->setGlobalFromRequest('INDIVIDUAL_EMAILS');
            $this->setGlobalFromRequest('DIF_2_DESIGN');
            $this->setGlobalFromRequest('LISTING_DISPLAY_NUM');
            $this->setGlobalFromRequest('WEB_SHOW_CODE');
            $this->setGlobalFromRequest('DIF_SHOW_REJECT_BTN');
            $this->setGlobalFromRequest('FMT_DATE_WEB');
            $this->setGlobalFromRequest('DIF_NOMAINMENU');
            $this->setGlobalFromRequest('DIF_LOGOUTTOIN');
            $this->setGlobalFromRequest('DIF2_ADD_NEW');
            $this->setGlobalFromRequest('STAFF_CHANGE_EMAIL');
            $this->setGlobalFromRequest('STAFF_CHANGE_EMAIL_COM');
            $this->setGlobalFromRequest('WORKFLOW');
            $this->setGlobalFromRequest('DIF2_SEARCH_DEFAULT');
            $this->setGlobalFromRequest('CCS2_INC');
            $this->setGlobalFromRequest('EXPORT_CCS2');
            $this->setGlobalFromRequest('RECORD_UPDATE_EMAIL');

            if ($this->request->getParameter('EXPORT_CCS2') == 'Y')
            {
                SetGlobal('XMLDATASET', '2');
            }

            $recordUpdateEmail = $this->request->getParameter('RECORD_UPDATE_EMAIL');
            if ($recordUpdateEmail == 'Y')
            {
                if($this->registry->getParm('RECORD_UPDATE_EMAIL_DATETIME') == '')
                {
                    $datetime = new \DateTime();
                    SetGlobal('RECORD_UPDATE_EMAIL_DATETIME', $datetime->format('Y-m-d H:i:s.000'));
                }
            }
            else
            {
                SetGlobal('RECORD_UPDATE_EMAIL_DATETIME', '');
            }

            $this->setGlobalFromRequest('INC_SAVED_QUERIES_HOME_SCREEN');

            if (is_array($this->request->getParameter('INC_SAVED_QUERIES')))
            {
                $incSavedQueries = implode(' ', \Sanitize::SanitizeStringArray($this->request->getParameter('INC_SAVED_QUERIES')));
            }
            else
            {
                $incSavedQueries = '';
            }

            SetGlobal('INC_SAVED_QUERIES', $incSavedQueries, true);
            $this->setGlobalFromRequest('INC_SAVED_QUERIES_NUM_RECORDS');

            for($i = 0; $i <= $this->request->getParameter('maxLoginMessageId'); $i++)
            {
                if ($this->request->getParameter('LoginMessage_' . $i))
                {
                    $LoginMessages[] = EscapeQuotestoFile($this->request->getParameter('LoginMessage_' . $i));
                }
            }

            for($i = 0; $i <= $this->request->getParameter('maxUsageAgreementMessageId'); $i++)
            {
                if ($this->request->getParameter('UsageAgreementMessage_' . $i))
                {
                    $UsageAgreementMessages[] = EscapeQuotestoFile($this->request->getParameter('UsageAgreementMessage_' . $i));
                }
            }

            if ((is_array($LoginMessages) && !empty($LoginMessages)) || (is_array($UsageAgreementMessages) && !empty($UsageAgreementMessages)))
            {
                $LoginMessageFile = GetLoginMessageFile();
                $File = @fopen($LoginMessageFile, 'wb');

                @fwrite($File, '<?php');

                if (is_array($LoginMessages) && !empty($LoginMessages))
                {
                    @fwrite($File, '
$LoginMessages = array(
    ');
                    foreach ($LoginMessages as $LoginMessage)
                    {
                        @fwrite($File, '"' . $LoginMessage . '",
    ');
                    }

                    @fwrite($File, ');');
                }

                if (is_array($UsageAgreementMessages) && !empty($UsageAgreementMessages))
                {
                    @fwrite($File, '
$UsageAgreementMessages = array(
    ');
                    foreach ($UsageAgreementMessages as $UsageAgreementMessage)
                    {
                        @fwrite($File, '"' . $UsageAgreementMessage . '",
    ');
                    }

                    @fwrite($File, ');');
                }

                @fwrite($File, '
?>');
                fclose($File);
            }
            else
            {
                $LoginMessageFile = GetLoginMessageFile();
                unlink($LoginMessageFile);
            }

            // save 'Overdue days'
            foreach ($this->request->getParameters() as $var => $value)
            {
                if (\UnicodeString::substr($var, -12) == 'OVERDUE_DAYS')
                {
                    $overdueParts = explode('_', $var);

                    // we don't need to pay attention to 'overall' values since these are saved as globals
                    if (count($overdueParts) == 4)
                    {
                        if ($value == '')
                        {
                            $value = -1;
                        }

                        unset($overdueParts[3]);
                        $overdueType = implode('_', $overdueParts) . '_TYPE';
                        $sql = '
                            SELECT
                                recordid
                            FROM
                                timescales
                            WHERE
                                tim_module = :tim_module
                                AND
                                tim_approval_status = :tim_approval_status
                        ';
                        $result = PDO_fetch($sql, array(
                            'tim_module' => $overdueParts[0],
                            'tim_approval_status' => $overdueParts[1]
                        ));

                        if (!empty($result))
                        {
                            $sql = '
                                UPDATE
                                    timescales
                                SET
                                    tim_overdue_days = :tim_overdue_days,
                                    tim_type = :tim_type
                                WHERE
                                    tim_module = :tim_module
                                    AND
                                    tim_approval_status = :tim_approval_status
                            ';

                            $params = array(
                                'tim_overdue_days' => $value,
                                'tim_type' => $_POST[$overdueType],
                                'tim_module' => $overdueParts[0],
                                'tim_approval_status' => $overdueParts[1]
                            );
                            \DatixDBQuery::PDO_query($sql, $params);
                        }
                        else
                        {
                            $sql = '
                                INSERT INTO
                                    timescales
                                    (tim_module, tim_approval_status, tim_overdue_days, tim_type)
                                VALUES
                                    (:tim_module, :tim_approval_status, :tim_overdue_days, :tim_type)
                            ';

                            $params = array(
                                'tim_module' => $overdueParts[0],
                                'tim_approval_status' => $overdueParts[1],
                                'tim_overdue_days' => $value,
                                'tim_type' => $_POST[$overdueType]
                            );
                            \DatixDBQuery::PDO_query($sql, $params);
                        }
                    }
                }
            }

            // PALS module globals
            $this->setGlobalFromRequest('PAL_1_EMAIL_SELECT');
            $this->setGlobalFromRequest('PAL_WHERE_OVERRIDE');
            $this->setGlobalFromRequest('PAL_EMAIL_MGR');
            $this->setGlobalFromRequest('PAL_OWN_ONLY');
            $this->setGlobalFromRequest('PAL_1_REF_PREFIX');
            $this->setGlobalFromRequest('PAL_NO_OPEN');
            $this->setGlobalFromRequest('PAL_SHOW_EMAIL');
            $this->setGlobalFromRequest('PAL_L1_CON_MATCH_LISTING_ID');
            $this->setGlobalFromRequest('PAL2_DESIGN');
            $this->setGlobalFromRequest('PAL2_SEARCH_DEFAULT');
            $this->setGlobalFromRequest('PAL_SHOW_AUDIT');
            $this->setGlobalFromRequest('NEW_RECORD_LVL1_PAL');
            $this->setGlobalFromRequest('AUTO_POPULATE_REC_NAME_PAL');

            // Claims
            $this->setGlobalFromRequest('CLA_FINANCE_UDFS');
            $this->setGlobalFromRequest('NEW_RECORD_LVL1_CLA');
            $this->setGlobalFromRequest('STAFF_CHANGE_EMAIL_CLA');
            $this->setGlobalFromRequest('CCS2_CLA');
            $this->setGlobalFromRequest('CLA_SHOW_EMAIL');
            $this->setGlobalFromRequest('CLA_L1_ORG_MATCH_LISTING_ID');
            $this->setGlobalFromRequest('CLAIM2_SEARCH_DEFAULT');

            $this->setGlobalFromRequest('RESERVE_REASON_CLA');
            $this->setGlobalFromRequest('CLAIM_CLOSED_DATES');
            $this->setGlobalFromRequest('CLA_WORKFLOW');
            $this->setGlobalFromRequest('CLA_SHOW_DUPLICATES');


            // Payments
            $this->setGlobalFromRequest('PAY2_SEARCH_DEFAULT');
            $this->setGlobalFromRequest('PAY_ADD_NEW_RECIPIENT');

            // COM module globals
            $this->setGlobalFromRequest('COM_EMAIL_MGR');
            $this->setGlobalFromRequest('COM_OWN_ONLY');
            $this->setGlobalFromRequest('COM_1_REF_PREFIX');
            $this->setGlobalFromRequest('COM_NO_OPEN');
            $this->setGlobalFromRequest('COM_SHOW_EMAIL');
            $this->setGlobalFromRequest('COM_L1_CON_MATCH_LISTING_ID');
            $this->setGlobalFromRequest('COM2_SEARCH_DEFAULT');
            $this->setGlobalFromRequest('COM_SHOW_AUDIT');
            $this->setGlobalFromRequest('NEW_RECORD_LVL1_COM');
            $this->setGlobalFromRequest('SHOW_ACKNOWLEDGED');
            $this->setGlobalFromRequest('SHOW_ACTIONED');
            $this->setGlobalFromRequest('SHOW_RESPONSE');
            $this->setGlobalFromRequest('SHOW_HOLDING');
            $this->setGlobalFromRequest('SHOW_REPLIED');
            $this->setGlobalFromRequest('REV_DECISION_TIME');
            $this->setGlobalFromRequest('REV_TERMS_TIME');
            $this->setGlobalFromRequest('REV_COMMENTS_TIME');
            $this->setGlobalFromRequest('REV_INV_TIME');
            $this->setGlobalFromRequest('IR_ACKDAYS');
            $this->setGlobalFromRequest('IR_LAYCHAIRDAYS');
            $this->setGlobalFromRequest('IR_DECISIONDAYS');
            $this->setGlobalFromRequest('IR_PANELDAYS');
            $this->setGlobalFromRequest('IR_DRAFTDAYS');
            $this->setGlobalFromRequest('IR_FINALDAYS');
            $this->setGlobalFromRequest('IR_REPLYDAYS');
            $this->setGlobalFromRequest('ISD_A2');
            $this->setGlobalFromRequest('AUTO_POPULATE_REC_NAME_COM');
            $this->setGlobalFromRequest('COM_COMPLAINT_CHAIN_TIMESCALES');
            SetGlobalFromPost('CCS2_COM');

            // Risk register module globals
            $this->setGlobalFromRequest('RISK_1_EMAIL_SELECT');
            $this->setGlobalFromRequest('RAM_L1_CON_MATCH_LISTING_ID');
            $this->setGlobalFromRequest('RISK_WHERE_OVERRIDE');
            $this->setGlobalFromRequest('RISK_OWN_ONLY');
            $this->setGlobalFromRequest('RISK2_DESIGN');
            $this->setGlobalFromRequest('RISK_EMAIL_MGR');
            $this->setGlobalFromRequest('RISK_1_ACKNOWLEDGE');
            $this->setGlobalFromRequest('RAM_SHOW_EMAIL');
            $this->setGlobalFromRequest('RISK_NO_OPEN');
            $this->setGlobalFromRequest('RISK2_SEARCH_DEFAULT');
            $this->setGlobalFromRequest('RAM_SHOW_AUDIT');
            $this->setGlobalFromRequest('NEW_RECORD_LVL1_RAM');
            $this->setGlobalFromRequest('ASSURANCE_FRAMEWORK');

            // SABS
            $this->setGlobalFromRequest('SABS_E_MAIL_ADDRESS');
            $this->setGlobalFromRequest('SAB1_SEARCH_DEFAULT');

            // Actions module globals
            $this->setGlobalFromRequest('CUSTOM_REPORT_BUILDER');
            $this->setGlobalFromRequest('ACT_OWN_ONLY');
            $this->setGlobalFromRequest('ACT_EMAIL');
            $this->setGlobalFromRequest('ACT_COPY_LOCATIONS');
            $this->setGlobalFromRequest('ACT_SEARCH_DEFAULT');
            $this->setGlobalFromRequest('ACT_SHOW_AUDIT');
            $this->setGlobalFromRequest('REM_DAYS_ACT');

            // Contacts
            $this->setGlobalFromRequest('CON_SEARCH_DEFAULT');
            $this->setGlobalFromRequest('CON_SEARCH_SOUNDEX');
            $this->setGlobalFromRequest('CON_SHOW_REJECT_BTN');
            $this->setGlobalFromRequest('CON_PAS_CHECK');
            $this->setGlobalFromRequest('CON_MATCH_LISTING_ID');
            $this->setGlobalFromRequest('REPORTER_ROLE');

            if (count($this->request->getParameter('CON_PAS_CHK_FIELDS')) > 13)
            {
                AddSessionMessage('ERROR', _tk('contact_searching_error'));
                $this->call('src\admin\controllers\ShowDatixwebConfTemplateController', 'setup', array());
                return;
            }

            if (is_array($this->request->getParameter('CON_PAS_CHK_FIELDS')))
            {
                $conPasChkFields = implode(',', \Sanitize::SanitizeStringArray($this->request->getParameter('CON_PAS_CHK_FIELDS')));
            }
            else
            {
                $conPasChkFields = '';
            }

            SetGlobal('CON_PAS_CHK_FIELDS', $conPasChkFields, true);

            // To Do List
            if (is_array($this->request->getParameter('TOD_USERS_ACT')))
            {
                $TOD_USERS_ACT = implode(',', \Sanitize::SanitizeStringArray($this->request->getParameter('TOD_USERS_ACT')));
            }
            else
            {
                $TOD_USERS_ACT = '';
            }

            SetGlobal('TOD_USERS_ACT', $TOD_USERS_ACT, true);

            if (is_array($this->request->getParameter('TOD_USERS_INC')))
            {
                $TOD_USERS_INC = implode(',', \Sanitize::SanitizeStringArray($this->request->getParameter('TOD_USERS_INC')));
            }
            else
            {
                $TOD_USERS_INC = '';
            }

            SetGlobal('TOD_USERS_INC', $TOD_USERS_INC, true);

            if (is_array($this->request->getParameter('TOD_USERS_COM')))
            {
                $TOD_USERS_COM = implode(',', \Sanitize::SanitizeStringArray($this->request->getParameter('TOD_USERS_COM')));
            }
            else
            {
                $TOD_USERS_COM = '';
            }

            SetGlobal('TOD_USERS_COM', $TOD_USERS_COM, true);

            if (is_array($this->request->getParameter('TOD_USERS_RAM')))
            {
                $TOD_USERS_RAM = implode(',', \Sanitize::SanitizeStringArray($this->request->getParameter('TOD_USERS_RAM')));
            }
            else
            {
                $TOD_USERS_RAM = '';
            }

            SetGlobal('TOD_USERS_RAM', $TOD_USERS_RAM, true);

            if (is_array($this->request->getParameter('TOD_USERS_CQC')))
            {
                $todUsersCQC = implode(',', \Sanitize::SanitizeStringArray($this->request->getParameter('TOD_USERS_CQC')));
            }
            else
            {
                $todUsersCQC = '';
            }

            SetGlobal('TOD_USERS_CQC', $todUsersCQC, true);

            // Equipment
            $this->setGlobalFromRequest('AST_SEARCH_DEFAULT');

            // Standards
            $this->setGlobalFromRequest('STN_ADD_NEW');
            $this->setGlobalFromRequest('STAFF_CHANGE_EMAIL_ELE');

            // CQC
            $this->setGlobalFromRequest('CQC_ROLLUP_TYPE');

            // Dashboard
            $this->setGlobalFromRequest('DASH_GRAPHS_PER_ROW');

            // Medications
            $this->setGlobalFromRequest('MED_SEARCH_SOUNDEX');
            $this->setGlobalFromRequest('DMD_ENABLED');

            // Accrediation
            $this->setGlobalFromRequest('AMO_ROLLUP_TYPE');

            // Organisations
            if (count($this->request->getParameter('ORG_PAS_CHK_FIELDS')) > 13)
            {
                AddSessionMessage('ERROR', _tk('organisation_searching_error'));
                $this->call('src\admin\controllers\ShowDatixwebConfTemplateController', 'setup', array());
                return;
            }

            if (is_array($this->request->getParameter('ORG_PAS_CHK_FIELDS')))
            {
                $orgPasChkFields = implode(',', \Sanitize::SanitizeStringArray($this->request->getParameter('ORG_PAS_CHK_FIELDS')));
            }
            else
            {
                $orgPasChkFields = '';
            }

            SetGlobal('ORG_PAS_CHK_FIELDS', $orgPasChkFields, true);

            $this->setGlobalFromRequest('');

            GetParms($_SESSION['user']);

            $this->call('src\admin\controllers\ShowDatixwebConfTemplateController', 'setup', array());
        }
    }

    private function setGlobalFromRequest ($name)
    {
        $value = $this->request->getParameter ($name);

        // Special case for array - space separate it
        if (is_array($value))
        {
            $value = implode(" ", $value);
        }

        // This is a checkbox
        if ($value == "on")
        {
            $value = "Y";
        }

        SetGlobal (\Sanitize::SanitizeRaw($name), \Sanitize::SanitizeRaw($value));
    }
}
