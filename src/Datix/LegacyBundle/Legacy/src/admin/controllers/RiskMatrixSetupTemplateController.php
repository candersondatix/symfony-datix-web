<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;

/**
 * Controls the code setup section of the extra field edit form.
 */
class RiskMatrixSetupTemplateController extends TemplateController
{
    /**
    * @desc Constructs risk matrix setup list using Rico based LiveGrid
    */
    function riskmatrixsetup()
    {
        global $scripturl, $yySetLocation;
    
        if (!(IsFullAdmin() || bYN(GetParm('INC_SETUP', 'N'))))
        {
            $yySetLocation = $scripturl;
            redirectexit();
        }

        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));
    
        $this->title = _tk('code_setups') . ' - ' . _tk('risk_grading');
        $this->module = 'ADM';

        $FormArray = array (
            "Parameters" => array(
                "Panels" => "True",
                "Condition" => false
            ),
            "name" => array(
                "Title" => _tk('code_setups'),
                "MenuTitle" => _tk('code_setups'),
                "Rows" => array()
            ),
        );
        
        
        $SetupTable = new \FormTable();
        $SetupTable->MakeForm($FormArray, array(), $this->module);

        $SetupTable->MakeTable();

        $this->response->build('src/admin/views/RiskMatrixSetup.php', array(
            'table' => 'ratings_map',
        ));
    
    }    
    
    
    /**
    * Constructs the fields used in the Rico table.
    */
    static public function DefineFields() 
    {
        global $oForm;
    
        $oForm->AddSort('consequence', 'ASC');
        $oForm->AddSort('likelihood', 'ASC');
    
        $oForm->ConfirmDeleteColumn();
        $oForm->options['canAdd'] = false;
        $oForm->overrideKeys["consequence"] = true;
        $oForm->overrideKeys["likelihood"] = true;
        $oForm->options["FilterLocation"] = -1;
    
        $oForm->AddEntryFieldW('consequence', \Labels_FieldLabel::GetFieldLabel('inc_consequence'), 'S', '', 110);
        $oForm->CurrentField['FormView'] = 'hidden';
        $oForm->CurrentField["filterUI"] = 't';
        $oForm->CurrentField["width"] = 200;
    
        $oForm->AddEntryFieldW('likelihood', \Labels_FieldLabel::GetFieldLabel('inc_likelihood'), 'S', '', 110);
        $oForm->CurrentField['FormView'] = 'hidden';
        $oForm->CurrentField["filterUI"] = 't';
        $oForm->CurrentField["width"] = 200;
    
        $oForm->AddEntryFieldW('rating', \Labels_FieldLabel::GetFieldLabel('inc_grade'), 'S', '', 110);
        $oForm->CurrentField['SelectSql'] = 'SELECT code, description FROM code_inc_grades ORDER BY description';
        $oForm->CurrentField["filterUI"] = 't';
        $oForm->CurrentField["width"] = 200;
        
        $oForm->DisplayPage();
    }
    
    
}