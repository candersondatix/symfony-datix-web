<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ShowConfigTemplateController extends TemplateController
{
    /**
     * @desc Displays configuration information (environmental and internal).
     */
    function showconfig()
    {
        global $dbtype, $dtxversion, $ClientFolder;

        $ModuleDefs = $this->registry->getModuleDefs();

        require_once 'externals/keygen/DatixLicenceKeyFactory.php';
        require_once 'externals/keygen/DatixLicenceKey120.php';
        require_once 'externals/keygen/DatixLicenceKey121.php';
        require_once 'externals/keygen/DatixLicenceKey.php';
        require_once 'externals/keygen/DatixLicenceKey114.php';
        require_once 'Source/AdminSetup.php';

        $this->title = 'Configuration Settings';
        $this->module = 'ADM';
        $this->hasPadding = false;
        $this->hasMenu = false;

        $LicenceKey = \DatixLicenceKeyFactory::createLicenceKey();
        $LicenceKey->SetLicenceKey(GetParm('LICENCE_KEY_WEB'));
        $LicenceKey->SetClientName(GetParm('CLIENT'));
        $LicenceKey->DecodeKey();

        $modules = $LicenceKey->GetLicencedModules();

        if (!empty($modules))
        {
            foreach ($modules as $modId => $modLicenced)
            {
                if($modLicenced)
                {
                    $LicencedModules[] = $ModuleDefs[ModuleIDToCode($modId)]['NAME'];
                }
            }

            $lastModule = array_pop($LicencedModules);

            if(count($LicencedModules) > 0)
            {
                $modulesStr = implode(', ', $LicencedModules).' and '.$lastModule;
            }
            else
            {
                $modulesStr = $lastModule;
            }
        }

        $sql = '
            SELECT
                @@version as dbversion,
                @@SERVERNAME as dbservername,
                db_name() as dbname
        ';

        $row = \DatixDBQuery::PDO_fetch($sql);

        $dbversion = $row['dbversion'];
        $dbservername = $row['dbservername'];
        $dbname = $row['dbname'];

        ExportConfigtoXML();

        $filearray = array();

        $filearray[] = array('name' => 'index.php', 'source' => 'index.php');

        if ($handle = opendir($ClientFolder))
        {
            while (false !== ($file = readdir($handle)))
            {
                if ($file != "." && $file != ".." && (preg_match("/User/u", $file) || $file == "config.xml" || $file == "SQLErrors.xml"))
                {
                    $filearray[] = array("name" => $file, "source" => $ClientFolder . "/$file");
                }
            }

            closedir($handle);
        }

        //PHP settings - Only include essential support settings to minimise security implications/risks
        $phpini["PHP Version"] = phpversion();
        $phpini["max_execution_time"] = ini_get("max_execution_time");
        $phpini["max_input_time"] = ini_get("max_input_time");
        $phpini["memory_limit"] = ini_get("memory_limit");
        $phpini["error_reporting"] = ini_get("error_reporting");
        $phpini["display_errors"] = ini_get("display_errors");
        $phpini["log_errors"] = ini_get("log_errors");
        $phpini["error_log"] = ini_get("error_log");
        $phpini["register_globals"] = ini_get("register_globals");
        $phpini["post_max_size"] = ini_get('post_max_size');
        $phpini["magic_quotes_gpc"] = ini_get("magic_quotes_gpc");
        $phpini["magic_quotes_runtime"] = ini_get("magic_quotes_runtime");
        $phpini["magic_quotes_sybase"] = ini_get("magic_quotes_sybase");
        $phpini["always_populate_raw_post_data"] = ini_get("always_populate_raw_post_data");
        $phpini["include_path"] = ini_get("include_path");
        $phpini["extension_dir"] = ini_get("extension_dir");
        $phpini["Loaded extensions"] = implode("<br>", get_loaded_extensions());
        $phpini["fastcgi.impersonate"] = ini_get("fastcgi.impersonate");
        $phpini["file_uploads"] = ini_get("file_uploads");
        $phpini["upload_tmp_dir"] = ini_get("upload_tmp_dir");
        $phpini["upload_max_filesize"] = ini_get("upload_max_filesize");
        $phpini["allow_url_fopen"] = ini_get("allow_url_fopen");
        $phpini["allow_url_include"] = ini_get("allow_url_include");
        $phpini["date.timezone"] = ini_get("date.timezone");
        $phpini["SMTP"] = ini_get("SMTP");
        $phpini["smtp_port"] = ini_get("smtp_port");
        $phpini["sendmail_from"] = ini_get("sendmail_from");
        $phpini["mssql.compatability_mode"] = ini_get("mssql.compatability_mode");
        $phpini["mssql.connect_timeout"] = ini_get("mssql.connect_timeout");
        $phpini["mssql.timeout"] = ini_get("mssql.timeout");
        $phpini["mssql.textlimit"] = ini_get("mssql.textlimit");
        $phpini["mssql.textsize"] = ini_get("mssql.textsize");
        $phpini["mssql.datetimeconvert"] = ini_get("mssql.datetimeconvert");
        $phpini["mssql.secure_connection"] = ini_get("mssql.secure_connection");
        $phpini["soap.wsdl_cache_enabled"] = ini_get("soap.wsdl_cache_enabled");
        $phpini["soap.wsdl_cache_dir"] = ini_get("soap.wsdl_cache_dir");
        $phpini["soap.wsdl_cache_ttl"] = ini_get("soap.wsdl_cache_ttl");

        $sql = '
            SELECT
                parameter,
                parmvalue
            FROM
                globals
            ORDER BY
                parameter
        ';

        $Globals = \DatixDBQuery::PDO_fetch_all($sql);

        $sql = '
            SELECT
                login,
                parameter,
                parmvalue
            FROM
                user_parms
            ORDER BY
                login,
                parameter
        ';

        $UserParms = \DatixDBQuery::PDO_fetch_all($sql);

        $sql = '
            SELECT
                TOP(1) ccs2_version
            FROM
                code_ccs2_tier_one
        ';

        $ccs2Version = \DatixDBQuery::PDO_fetch($sql, array(), \PDO::FETCH_COLUMN);
        $ccs2Version = ($ccs2Version != '' ? $ccs2Version : 'Undefined');

        $showJS = false;
        $showSuccessJS = false;

        if($this->request->getMethod() == 'POST' && $this->request->getParameter('send') == 'Send')
        {
            $subject = 'Configuration files - ' . $LicenceKey->GetClientName();
            $body = 'Client name: ' . $LicenceKey->GetClientName();
            $supportEmail = GetParm('SUPPORT_EMAIL_ADDR');

            if (!empty($supportEmail))
            {
                SendEmail($supportEmail, $subject, $body, $filearray);
                $showJS = true;
                $showSuccessJS = true;
            }
            else
            {
                $showJS = true;
            }
        }

        $this->response->build('src/admin/views/ShowConfig.php', array(
            'ClientFolder' => $ClientFolder,
            'ClientName' => $LicenceKey->GetClientName(),
            'LicenceKey' => $LicenceKey->GetLicenceKey(),
            'ExpiryDate' => $LicenceKey->GetExpiryDate(),
            'modules' => $LicenceKey->GetLicencedModules(),
            'modulesStr' => $modulesStr,
            'dtxversion' => $dtxversion,
            'dbversion' => $dbversion,
            'dbservername' => $dbservername,
            'dbtype' => $dbtype,
            'dbname' => $dbname,
            'phpini' => $phpini,
            'Globals' => $Globals,
            'UserParms' => $UserParms,
            'ccs2Version' => $ccs2Version,
            'dmdVersion' => $this->registry->getParm('DMD_VERSION'),
            'showJS' => $showJS,
            'showSuccessJS' => $showSuccessJS
        ));
    }
}