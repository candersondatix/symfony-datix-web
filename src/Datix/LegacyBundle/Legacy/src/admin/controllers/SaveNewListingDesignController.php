<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class SaveNewListingDesignController extends Controller
{
    /**
     * Saves a new listing design, copying an existing design.
     * User is then redirected to the edit page for this design.
     */
    function savenewlistingdesign()
    {
        $module = \Sanitize::getModule($this->request->getParameter('module'));

        if (!$module)
        {
            $this->redirect('app.php');
        }

        if ($this->request->getParameter('formaction') == 'cancel')
        {
            $this->redirect('app.php?action=listlistingdesigns&module=' . $module);
        }

        require_once 'Source/libs/ListingClass.php';

        $Listing = new \Listing($module);

        // Load listing to be copied
        $Listing->LoadColumnsFromDB($this->request->getParameter('copy_from'));

        // This listing has no id yet - it will be created on save
        $Listing->ListingId = null;

        $Listing->Title = $this->request->getParameter('form_name');

        $Listing->SaveToDB();

        //Some information about listings is cached - we need to clear these caches now that the information in them is stale.
        $this->registry->getSessionCache()->delete('listing-design.modules-for-listings');

        $Parameters = array(
            'module' => $module,
            'listing_id' => $Listing->ListingId
        );

        $this->call('src\admin\controllers\EditColumnsTemplateController', 'editcolumns', $Parameters);
    }
}