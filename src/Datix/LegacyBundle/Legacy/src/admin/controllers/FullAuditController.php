<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class FullAuditController extends TemplateController
{
    function full_audit()
    {
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        if (!empty($_GET["user"]))
        {
            $data["SetupType"] = "User";

            $UserID = (int) $_GET["user"];

            $sql = "
                SELECT
                    login,
                    initials,
                    fullname
                FROM
                    contacts_main
                WHERE
                    recordid = :UserID
            ";

            $row = \DatixDBQuery::PDO_fetch($sql, array("UserID" => $UserID));

            $Title = _tk('user_audit') . " - " . $row["login"];
            $data['where'] = "aud_login in ('" .$row['login']."', '" . $row['initials'] . "')";
        }
        else
        {
            $data["SetupType"] = "Full";
            $Title = _tk('full_audit_title');
            $data['table'] = 'globals';
        }

        $this->title = $Title;
        $this->module = 'ADM';

        $table = "full_audit";
        $where = "aud_date >= (GETDATE()-" . GetParm('AUDIT_DISPLAY_DAYS', 180) .")";

        if (!empty($data["where"]))
        {
            $where.= " AND " . $data["where"];
        }

        $FormArray = array (
            "Parameters" => array(
                "Panels" => "True",
                "Condition" => false
            ),
            "name" => array(
                "Title" => $Title,
                "MenuTitle" => $Title,
                "NewPanel" => true,
                "Rows" => array()
            ),
        );

        $SetupTable = new \FormTable();
        $SetupTable->MakeForm($FormArray, $data, $this->module);

        $SetupTable->MakeTable();

        $this->response->build('src/admin/views/FullAudit.php', array(
            'data' => $data,
            'table' => $table,
            'where' => $where,
            'user' => $this->request->getParameter('user')
        ));
    }

    /**
     * @desc Defines the fields used in the Live grid for the Full Audit and displays the grid
     */
    static public function DefineFields()
    {
        global $oForm;

        $oForm->options["FilterLocation"]=-1;

        $oForm->AddEntryFieldW("aud_module", "Module", "B", "",125);
        $oForm->CurrentField["filterUI"]="t";
        $oForm->AddEntryFieldW("aud_record", "Record", "B", "",125);
        $oForm->CurrentField["filterUI"]="t";
        $oForm->AddEntryFieldW("aud_date", "Date/Time", "D", "", 125);
        $oForm->CurrentField["dateFmt"]="yyyy-mm-dd HH:nn:ss";
        $oForm->CurrentField["filterUI"]="t";
        $oForm->AddEntryFieldW("aud_login", "Login/Initials", "B", "",125);
        $oForm->CurrentField["filterUI"]="t";
        $oForm->AddEntryFieldW("aud_action", "Event", "B", "",125);
        $oForm->CurrentField["filterUI"]="t";
        $oForm->AddEntryFieldW("aud_detail", "Detail", "TA", "", 500);
        $oForm->CurrentField["filterUI"]="t";
        $oForm->CurrentField["canSort"]=false;
        $oForm->AddSort("aud_date", "DESC");

        $oForm->overrideKeys["aud_module"] = true;
        $oForm->overrideKeys["aud_record"] = true;
        $oForm->overrideKeys["aud_login"] = true;
        $oForm->overrideKeys["aud_date"] = true;

        $oForm->DisplayPage();
    }
}