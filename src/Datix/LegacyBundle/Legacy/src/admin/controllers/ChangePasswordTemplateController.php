<?php

namespace src\admin\controllers;

use SebastianBergmann\Exporter\Exception;
use src\framework\controller\TemplateController;
use src\passwords\model\PasswordModelFactory;
use src\users\model\UserModelFactory;

class ChangePasswordTemplateController extends TemplateController
{
    function password()
    {
        global $scripturl;

        // Parameter $user will be non-empty the first time the user
        // sets the p_a_s_s_w_o_r_d.
        
        $PwdAction = $this->request->getParameter('PwdAction');

        if ($this->request->getParameter('user') != '' &&
            $PwdAction != 'Change' &&
            $PwdAction != 'Expired' &&
            $PwdAction != 'Reset')
        {
            $PwdAction = "Set";

            $user = $this->request->getParameter('user');
        }
        else
        {
            if ($this->request->getParameter('resetcode'))
            {
                $sql = '
                    SELECT
                        recordid,
                        login
                    FROM
                        staff
                    WHERE
                        sta_pwd_reset_code = :sta_pwd_reset_code
                ';

                $row = \DatixDBQuery::PDO_fetch($sql, array('sta_pwd_reset_code' => $this->request->getParameter('resetcode')));

                if (empty($row))
                {
                    SimpleMessage(_tk('expired_pwd_reset_link'));
                    obExit();
                }

                $_SESSION['user'] = $row['login'];
                $PwdAction = 'Reset';
            }

            if ($_SESSION['user'] == '')
            {
                $this->redirect($scripturl);
            }

            if ($PwdAction != 'Expired' && $PwdAction != 'Reset')
            {
                $PwdAction = 'Change';
            }

            $user = $_SESSION['user'];
        }

        if($PwdAction == 'Set')
        {
            $passwordMapper = (new PasswordModelFactory())->getMapper();

            //"Set" should only work when the password has not been set yet otherwise this could be used to set
            // other people's passwords.
            if($passwordMapper->userHasPassword($user))
            {
                throw new \Exception('Invalid password action.');
            }
        }

        $AdminUser = $_SESSION['AdminUser'];

        switch($PwdAction)
        {
            case 'Change':
                $title = _tk('pwd_change_title');
                break;
            case 'Expired':
                $title = _tk('pwd_expired_title');
                break;
            case 'Set':
                $title = _tk('pwd_set_title');
                break;
            case 'Reset':
                $title = _tk('pwd_set_title');
                break;
        }

        $this->title = $title;
        $this->module = 'ADM';
        $this->image = 'images/icons/icon_cog_change_password.png';

        if ($_SESSION['logged_in'])
        {
           $this->hasPadding = false;
        }
        else
        {
            $this->hasPadding = false;
            $this->hasMenu = false;
        }

        $this->response->build('src/admin/views/ChangePassword.php', array(
            'PwdAction' => $PwdAction,
            'fullname' => $this->request->getParameter('fullname'),
            'error' => $this->request->getParameter('error'),
            'user' => $user
        ));
    }

    function password2()
    {
        require_once 'Source/security/SecurityBase.php';

        $User       = $this->request->getParameter('user');
        $PwdAction  = $this->request->getParameter('pwd_action');
        $Fullname   = \Sanitize::SanitizeString($this->request->getParameter('fullname'));
        $pwdExpired = (\Sanitize::SanitizeString($this->request->getParameter('pwd_expired'))=="Y");

        $CurrentUser  = $_SESSION["user"];
        $AdminUser    = $_SESSION["AdminUser"];
        $ContactLogin = $_SESSION["contact_login"];

        // you should not be able to set a password for an account with a sid: opens up potential account hijack vulnerabilities
        // need to override security as we are checking on behalf of the system, not a logged in user.
        $userObject = (new UserModelFactory)->getMapper()->findByLogin($User, true);
        if ($userObject->con_sid)
        {
            throw new \Exception('For security reasons, users imported from LDAP may not have a local Datix password set.');
        }

        // not matching passwords
        if ($this->request->getParameter('passwrd') != $this->request->getParameter('retype_passwrd'))
        {
            $this->redirect('app.php?action=password&user=' . urlencode($User) . '&fullname=' . $Fullname . '&PwdAction=' . $PwdAction . '&error=' . _tk('pwd_mismatch_err'));
        }

        if ($PwdAction == "Change")
        {
            // trying to change another user's password
            if ($CurrentUser && $User != $CurrentUser)
            {
                AuditLogin($User, "Password change failed: logged-in user trying to change another user's password");
                $this->redirect('app.php?action=password&user=' . urlencode($User) . '&fullname=' . $Fullname . '&PwdAction=' . $PwdAction . '&error=' . _tk('wrong_username_pwd_err'));
            }

            // wrong old password
            if (!ValidatePassword($User, $this->request->getParameter('old_passwrd')))
            {
                AuditLogin($User, "Password change failed: incorrect password");
                $this->redirect('app.php?action=password&user=' . urlencode($User) . '&fullname=' . $Fullname . '&PwdAction=' . $PwdAction . '&error=' . _tk('old_pwd_wrong_err'));
            }
        }
        else if ($PwdAction == "Reset")
        {
            // trying to change another user's password
            if ($CurrentUser && $User != $CurrentUser)
            {
                AuditLogin($User, "Password change failed: logged-in user trying to change another user's password");
                $this->redirect('app.php?action=password&user=' . urlencode($User) . '&fullname=' . $Fullname . '&PwdAction=' . $PwdAction . '&error=' . _tk('wrong_username_pwd_err'));
            }
        }
        else if ($PwdAction == "Expired")
        {
            // wrong old password
            if ($User == $CurrentUser && !ValidatePassword($User, $this->request->getParameter('old_passwrd')))
            {
                AuditLogin($User, "Password change failed: incorrect password");
                $this->redirect('app.php?action=password&user=' . urlencode($User) . '&fullname=' . $Fullname . '&PwdAction=' . $PwdAction . '&error=' . _tk('old_pwd_wrong_err'));
            }
        }

        if (!checkNewPassword($User, $this->request->getParameter('passwrd'), $pwdError))
        {
            AddSessionMessage('ERROR', $pwdError['message']);
            $this->redirect('app.php?action=password&user=' . urlencode($User) . '&fullname=' . $Fullname . '&PwdAction=' . $PwdAction);
        }

        SetUserPassword($User, $this->request->getParameter('passwrd'));

        // Clear staff p_a_s_s_w_o_r_d change flag at next login if set
        $sql = 'UPDATE contacts_main SET
            sta_pwd_change = \'\'
            WHERE login = :login';

        \DatixDBQuery::PDO_query($sql, array('login' => $User));

        if ($PwdAction == "Set" || $PwdAction == "Expired" || $PwdAction == "Reset" || !$_SESSION['logged_in'])
        {
            $sql = "
                    UPDATE
                        staff
                    SET
                        login_tries = 0,
                        lockout = 'N',
                        sta_pwd_reset_code = NULL
                    WHERE
                        login = :login
                ";

            $result = \DatixDBQuery::PDO_query($sql, array('login' => $User));
            //TODO: this needs to be rewritten once Login2 ia contained in a controller.
            require_once('Source/Login.php');
            Login2();
            obExit();
        }
        elseif ($PwdAction == 'Change')
        {
            AddSessionMessage('INFO', _tk('change_password_confirm'));
            $this->redirect('app.php?action=password');
        }

        $this->redirect('app.php');
    }
}