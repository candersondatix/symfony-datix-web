<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Response;
use src\framework\controller\Request;
use src\excelimport\model\ExcelProfileModelFactory;

class ListExcelImportProfilesTemplateController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }

    /**
     * Shows a list of Excel profiles for different modules
     */
    function listimportprofiles()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $ProfileListDesign = new \Listings_ListingDesign(
            array('columns' => array(
                new \Fields_DummyField(array('name' => 'exp_profile_id', 'label' => 'ID', 'type' => 'S')),
                new \Fields_DummyField(array('name' => 'exp_profile_name', 'label' => 'Name', 'type' => 'S'))
            ))
        );

        $orderby  = \Sanitize::SanitizeString($this->request->getParameter('orderby') ? $this->request->getParameter('orderby') : 'exp_profile_id');
        $order    = ($this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';

        $CurrentModule = \Sanitize::SanitizeString($this->request->getParameter('module'));

        $Factory = new ExcelProfileModelFactory();

        foreach (getModArray() as $Module => $Name)
        {
            if (!in_array($Module, array('DAS', 'ADM', 'TOD')))
            {
                $PanelArray[$Module] = array('Label' => $ModuleDefs[$Module]['NAME']);

                if (!$CurrentModule)
                {
                    $CurrentModule = $Module;
                }

                $Query = $Factory->getQueryFactory()->getQuery();
                $ExcelProfiles = $Factory->getCollection();

                $Query->where(array('exp_profile_form' => $ModuleDefs[$Module]['TABLE']))
                      ->orderBy(array(array($orderby, $order)));

                $ExcelProfiles->setQuery($Query);

                $RecordList = new \RecordLists_RecordList();
                $RecordList->AddRecordData($ExcelProfiles);

                $Profiles[$Module] = new \Listings_ListingDisplay($RecordList, $ProfileListDesign);
                $Profiles[$Module]->setOrder(($this->request->getParameter('orderby')) ? $this->request->getParameter('orderby') : 'exp_profile_id' );
                $Profiles[$Module]->setDirection(($this->request->getParameter('order')) ? $this->request->getParameter('order') : 'ASC' );
                $Profiles[$Module]->setSortAction(\Sanitize::SanitizeURL($this->request->getParameter('action')) . '&amp;module=' . $Module);
                $Profiles[$Module]->Action = 'editimportprofile';
                $Profiles[$Module]->URLParameterArray = array('exp_profile_id');
            }
        }

        $this->title = _tk('list-of-import-profiles');

        $this->menuParameters = array(
            'module' => 'ADM',
            'panels' => $PanelArray,
            'current_panel' => $CurrentModule,
            'floating_menu' => false,
            'extra_links' => array(array('label' => 'Import Profile', 'link' => 'action=importimportprofile'))
        );

        $this->response->build('src/admin/views/ListExcelImportProfiles.php', array(
            'profiles' => $Profiles,
            'currentModule' => $CurrentModule
        ));
    }
}