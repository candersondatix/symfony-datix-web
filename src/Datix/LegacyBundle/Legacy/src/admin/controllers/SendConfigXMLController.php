<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class SendConfigXMLController extends Controller
{
    function sendconfigxml()
    {
        global $ClientFolder;

        require_once('externals/keygen/DatixLicenceKeyFactory.php');
        require_once('externals/keygen/DatixLicenceKey120.php');
        require_once('externals/keygen/DatixLicenceKey121.php');
        require_once('externals/keygen/DatixLicenceKey.php');
        require_once('externals/keygen/DatixLicenceKey114.php');
        require_once 'Source/AdminSetup.php';

        $LicenceKey = \DatixLicenceKeyFactory::createLicenceKey();
        $LicenceKey->SetLicenceKey(GetParm('LICENCE_KEY_WEB'));
        $LicenceKey->SetClientName(GetParm('CLIENT'));
        $LicenceKey->DecodeKey();

        $subject = 'XML configuration settings file - ' . $LicenceKey->GetClientName();
        $body = 'Client name: ' . $LicenceKey->GetClientName();

        ExportConfigtoXML();

        if (file_exists($ClientFolder . '/config.xml'))
        {
            if ($handle = fopen($ClientFolder . '/config.xml', 'rb'))
            {
                $filelist[] = array(
                    'name' => 'config.xml',
                    'source' => $ClientFolder . '/config.xml'
                );

                fclose($handle);
            }
            else
            {
                $body.= '\r\n\r\nconfig.xml cannot be opened';
            }
        }
        else
        {
            $body.= '\r\n\r\nconfig.xml cannot be found';
        }

        if (file_exists($ClientFolder . '/SQLErrors.xml'))
        {
            $filelist[] = array(
                'name' => 'SQLErrors.xml',
                'source' => $ClientFolder . '/SQLErrors.xml'
            );
        }

        $supportEmail = GetParm('SUPPORT_EMAIL_ADDR');

        if (!empty($supportEmail))
        {
            SendEmail($supportEmail, $subject, $body, $filelist);
            $configSendStatus.= 'true';
        }
        else
        {
            $configSendStatus.= 'false';
        }

        // Delete the config file after the email has been pushed out
        if(file_exists($ClientFolder . '/config.xml'))
        {
            unlink($ClientFolder . '/config.xml');
        }

        $this->redirect('app.php?module=ADM&configSendStatus=' . $configSendStatus);
    }
}