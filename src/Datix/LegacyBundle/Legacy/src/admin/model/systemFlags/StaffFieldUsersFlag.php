<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks for a large number of users that might appear in staff dropdowns.
 */
class StaffFieldUsersFlag extends SystemFlag
{
    /**
     * Limit chosen arbitrarily during initial development: can be updated as appropriate.
     */
    const LIMIT = 500;

    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'Users available for staff dropdowns.';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'High numbers of users in staff dropdowns can contribute to low performance. (Limit set at '.self::LIMIT.')';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $performanceState = new SystemState();

        //This could be extended in the future to calculate the numbers of users for particular staff fields. This is a simple initial implmentation, though.
        $numberOfUsers = \DatixDBQuery::PDO_fetch('select count (*) from contacts_main where login is not null and (con_staff_include = \'Y\' OR con_staff_include IS NULL or con_staff_include =\'\')', [], \PDO::FETCH_COLUMN);

        $performanceState->setRating(($numberOfUsers > self::LIMIT ? SystemState::BAD : SystemState::GOOD));

        $performanceState->setMessage('Currently there are '.$numberOfUsers.' users available for display in staff fields.');

        return $performanceState;
    }

}