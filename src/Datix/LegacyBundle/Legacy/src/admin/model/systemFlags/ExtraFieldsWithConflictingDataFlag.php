<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks that there are no extra fields with conflicting data for the same record.
 */
class ExtraFieldsWithConflictingDataFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'Records with conflicting extra field data.';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'Extra fields placed in groups in the Rich Client can be assigned different values on the same record, conflicting with assumptions made by DatixWeb.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $state = new SystemState();

        $recordIds = $this->getRecordIds();

        if (count($recordIds) > 0)
        {
            $state->setRating(SystemState::BAD);

            $state->setMessage('There are '.count($recordIds).' records which contain conflicting data.');
        }
        else
        {
            $state->setRating(SystemState::GOOD);
        }

        return $state;
    }

    /**
     * @return array array of record ids with different values for the same extra field.
     */
    public function getRecordIds()
    {
        return \DatixDBQuery::PDO_fetch_all('
            SELECT DISTINCT
                udf_values.cas_id
                FROM
                    (
                        select cas_id, field_id, mod_id
                        from udf_values
                        group by field_id, cas_id, mod_id
                        having count (*) > 1
                    ) sub
                LEFT JOIN
                udf_values on udf_values.cas_id = sub.cas_id AND udf_values.field_id = sub.field_id AND udf_values.mod_id = sub.mod_id
                INNER JOIN
                (SELECT distinct cas_id, mod_id, field_id from
                    (
                    SELECT mod_id, cas_id, field_id, cast (cas_id as varchar(10)) + \' \' + cast (field_id as varchar(10)) + \' \' + cast (mod_id as varchar(10)) as combo
                    from udf_values
                    group by field_id, cas_id, mod_id, udv_string, udv_number, udv_date, udv_money, udv_text

                    ) subsub
                    group by combo, cas_id, mod_id, field_id
                    having count(combo) >1
                ) casidsub
                ON casidsub.cas_id = udf_values.cas_id AND casidsub.mod_id = udf_values.mod_id  AND casidsub.field_id = udf_values.field_id
                group by udf_values.cas_id
            ', [], \PDO::FETCH_COLUMN);
    }

}