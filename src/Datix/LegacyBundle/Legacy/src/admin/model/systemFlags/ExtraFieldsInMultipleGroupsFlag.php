<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks that there are no extra fields in more than one group in the same module.
 */
class ExtraFieldsInMultipleGroupsFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'Extra fields in more than one group within the same module.';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'Extra fields placed in groups in the Rich Client can be assigned to more than one module, conflicting with assumptions made by DatixWeb.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $state = new SystemState();

        $fieldIds = $this->getFieldIds();

        if (count($fieldIds) > 0)
        {
            $state->setRating(SystemState::BAD);

            $state->setMessage('There are '.count($fieldIds).' fields which appear in more than one group within the same module.');
        }
        else
        {
            $state->setRating(SystemState::GOOD);
        }

        return $state;
    }

    /**
     * @return array array of field ids which appear in more than one group in the same module
     */
    public function getFieldIds()
    {
        return \DatixDBQuery::PDO_fetch_all('
            SELECT field_id
            FROM udf_links
            INNER JOIN udf_groups on udf_links.group_id = udf_groups.recordid
            GROUP BY udf_links.field_id, udf_groups.mod_id
            HAVING count(group_id) > 1
            ORDER BY udf_groups.mod_id
            ', [], \PDO::FETCH_COLUMN);
    }
}