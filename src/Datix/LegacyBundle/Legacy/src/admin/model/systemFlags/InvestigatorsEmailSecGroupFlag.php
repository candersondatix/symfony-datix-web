<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks whether there are security groups set up to email based on an investigator field.
 */
class InvestigatorsEmailSecGroupFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'Emailing Investigators';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'Emailing investigators through security groups can be slow: consider using the built-in staff notification functionality.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $performanceState = new SystemState();

        //This can be refined in future, but for simplicity, this is a broad query at the moment.
        $groups = \DatixDBQuery::PDO_fetch_all('SELECT recordid FROM sec_groups WHERE permission LIKE \'%_investigator%\' AND grp_type IN (2,3)', [], \PDO::FETCH_COLUMN);

        if (count($groups) > 0)
        {
            //construct links to each group that was returned by the query.
            $groups = array_map(function($value) {
                return '<a href="app.php?action=editgroup&grp_id='.$value.'">'.$value.'</a>';
            }, $groups);


            $performanceState->setRating(SystemState::BAD);
            //More information could be provided here in the future, but just providing a link to the group for simplicity for now.
            $performanceState->setMessage('Group IDs to check: '.implode(', ',$groups));
        }
        else
        {
            $performanceState->setRating(SystemState::GOOD);
        }

        return $performanceState;
    }

}