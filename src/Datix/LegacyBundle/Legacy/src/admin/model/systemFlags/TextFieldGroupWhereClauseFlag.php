<?php
namespace src\admin\model\systemFlags;

use src\framework\registry\Registry;
use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks for the use of texts fields in security group where clauses.
 */
class TextFieldGroupWhereClauseFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'Group where clause containing text field.';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'Searching on text fields can be inefficient when processing emails.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $performanceState = new SystemState();

        $moduleDefs = Registry::getInstance()->getModuleDefs();

        $textFields = [];

        //Get an array of all text fields for each module
        foreach (array_keys($moduleDefs) as $module)
        {
            $textFields[] = array_merge($textFields, GetAllFieldsByType($module, 'text'));
        }

        //construct sql snippets to search for instances of each text field.
        $textFields = array_map(function($value){
            return 'permission LIKE \'%'.$value.'%\'';
        }, $textFields);

        $groups = \DatixDBQuery::PDO_fetch_all('SELECT recordid FROM sec_groups WHERE ('.implode(' OR ', $textFields).') AND grp_type IN (2,3)', [], \PDO::FETCH_COLUMN);

        if (count($groups) > 0)
        {
            //In the future we could expand this to give more information about module etc, but for now we just provide a link to the group.
            $groups = array_map(function($value) {
                return '<a href="app.php?action=editgroup&grp_id='.$value.'">'.$value.'</a>';
            }, $groups);

            $performanceState->setRating(SystemState::BAD);
            $performanceState->setMessage('Group IDs to check: '.implode(', ',$groups));
        }
        else
        {
            $performanceState->setRating(SystemState::GOOD);
        }

        return $performanceState;
    }

}