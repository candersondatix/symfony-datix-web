<?php
namespace src\admin\model\systemFlags;

use src\framework\registry\Registry;
use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks that if RECORD_UPDATE_EMAIL is set, that we also have a valid sql date in RECORD_UPDATE_EMAIL_DATETIME.
 */
class RecordUpdateEmailDatetimeFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'RECORD_UPDATE_EMAIL_DATETIME set incorrectly.';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'If RECORD_UPDATE_EMAIL is set to Y, RECORD_UPDATE_EMAIL_DATETIME must be set to a valid SQL datetime representing the moment when RECORD_UPDATE_EMAIL became Y.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $state = new SystemState();

        $recordUpdateEmailValue = bYN(Registry::getInstance()->getParm('RECORD_UPDATE_EMAIL', 'N', true));

        if ($recordUpdateEmailValue === false)
        {
            $state->setRating(SystemState::GOOD);
        }
        else
        {
            $recordUpdateEmailDatetimeValue = Registry::getInstance()->getParm('RECORD_UPDATE_EMAIL_DATETIME', '', true);

            if ($recordUpdateEmailDatetimeValue == '' || preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}(\.\d{3})?/', $recordUpdateEmailDatetimeValue) !== 1)
            {
                $state->setRating(SystemState::BAD);
            }
            else
            {
                $state->setRating(SystemState::GOOD);
            }
        }

        return $state;
    }

}