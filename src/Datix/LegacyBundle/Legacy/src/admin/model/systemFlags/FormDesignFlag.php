<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\framework\registry\Registry;

/**
 * An aspect of the system that can be checked for performance risk.
 */
abstract class FormDesignFlag extends SystemFlag
{
    /**
     * Returns array of form design objects from all modules
     */
    public function getAllFormDesigns()
    {
        $moduleDefs = Registry::getInstance()->getModuleDefs();
        $clientFolder = Registry::getInstance()->getClientFolder();

        $formDesigns = [];

        foreach (array_keys($moduleDefs) as $module)
        {
            $FormFiles = array();
            $FormFiles2 = array();

            if (isset($moduleDefs[$module]['FORM_DESIGN_ARRAY_FILE']) && file_exists($clientFolder . '/' . $moduleDefs[$module]['FORM_DESIGN_ARRAY_FILE']))
            {
                require $clientFolder . '/' . $moduleDefs[$module]['FORM_DESIGN_ARRAY_FILE'];
            }

            if (empty($FormFiles))
            {
                $formDesigns[] = new \Forms_FormDesign(['module' => $module, 'level' => 1, 'id' => 0]);
            }
            else
            {
                foreach ($FormFiles as $id => $FormFile)
                {
                    $formDesigns[] = new \Forms_FormDesign(['module' => $module, 'level' => 1, 'id' => $id]);
                }
            }

            if (empty($FormFiles2))
            {
                $formDesigns[] = new \Forms_FormDesign(['module' => $module, 'level' => 2, 'id' => 0]);
            }
            else
            {
                foreach ($FormFiles2 as $id => $FormFile)
                {
                    $formDesigns[] = new \Forms_FormDesign(['module' => $module, 'level' => 2, 'id' => $id]);
                }
            }
        }

        foreach ($formDesigns as $formDesign)
        {
            //We won't worry at this stage if the form design doesn't exist.
            //We can check the integrity of the form designs in a different flag if needed.
            try
            {
                $formDesign->PopulateFormDesign();
            }
            catch (\FileNotFoundException $e)
            {
            }
        }

        return $formDesigns;
    }
}