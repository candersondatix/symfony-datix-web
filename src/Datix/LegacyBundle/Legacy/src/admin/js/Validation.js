/*
 * Checks whether the whitelist contains only valid domains (something.sm.th) on the config page
 * and then throws an alert if not valid and returns focus to the whitelist. Also checks against
 * the admin email to make sure its valid with the whitelist.
 *
 * @param  whiteList   the whitelist element that needs to be verified
 * @return bool        true if the whitelist is valid, else false
 */
function isPermittedDomainWhitelist(whiteListName, adminEmailName)
{
    var whiteListElement = jQuery('#'+whiteListName);
    var whiteList = whiteListElement.val().replace(/ /g,"");
    var adminEmail = jQuery('#'+adminEmailName).val().replace(/ /g,"");

    if (whiteList.value != '')
    {
        var domainPattern = /^[0-9a-z]{1,1}[0-9a-z\.\-]*[0-9a-z]{1,1}(\.[0-9a-z]+)+$/ig;
        var domainArray = whiteList.split('\n');
        var adminEmailDomain = adminEmail.split('@')[1];
        var adminEmailValid = false;
        var whiteListIsBlank = true;
        var valid = true;

        for (var i = 0; i < domainArray.length; ++i)
        {
            var domainMatch = domainArray[i].match(domainPattern);

            if (domainMatch == null && domainArray[i] != '')
            {
                valid = false;
            }

            if (domainArray[i] == adminEmailDomain)
            {
                adminEmailValid = true;
            }

            if (domainArray[i].replace(/ /g,"") != '')
            {
                whiteListIsBlank = false
            }
        }

        if (!adminEmailValid && !whiteListIsBlank && adminEmail != '')
        {
            whiteListElement.focus();
            alertWithNoSubmit('The domains you have entered invalidate the admin email address. Either leave the whitelist blank and '+
            'change the admin email address, or add the admin domain to the whitelist. The domain to add is:\n' + adminEmailDomain);
            return false;
        }

        if (!valid)
        {
            whiteListElement.focus();
            alertWithNoSubmit('One of the email domains you entered was not of the correct form. Remove the value or enter a valid domain');
            return false;
        }
        return true;
    }
}

/*
 * Checks whether the admin email address is a valid email and also allowed according to
 * the domain whitelist.
 *
 * @param  adminEmail  the admin email element that needs to be verified
 * @return bool        true if the admin email is valid, else false
 */
function isPermittedAdminEmail(adminEmailName, domainWhiteListName)
{
    adminEmailElement = jQuery('#'+adminEmailName);
    adminEmail = adminEmailElement.val().replace(/ /g,""); //get rid of spaces.

    if (adminEmail != '')
    {
        var emailPattern = /^(([a-zA-Z0-9\'_-]+(\.))*[a-zA-Z0-9\'_-]+|[a-zA-Z0-9\'_-]+)@([a-zA-Z0-9_-]{1,63}\.([a-zA-Z0-9_-]+(\.))*[a-zA-Z0-9]+)$/;

        //tests for valid email pattern
        if (!adminEmail.match(emailPattern))
        {
            adminEmailElement.focus();
            return false
        }

        var whiteList = jQuery('#'+domainWhiteListName).val().replace(/ /g,"");
        var domainArray = whiteList.split('\n');
        var adminEmailDomain = adminEmail.split('@')[1];
        var adminEmailOnWhitelist = false;
        var whiteListIsBlank = true;

        for (var i = 0; i < domainArray.length; ++i)
        {
            if (domainArray[i] == adminEmailDomain)
            {
                adminEmailOnWhitelist = true;
            }

            if (domainArray[i].replace(/ /g,"") != '')
            {
                whiteListIsBlank = false
            }
        }

        if (!adminEmailOnWhitelist && !whiteListIsBlank && whiteList != '')
        {
            adminEmailElement.focus();
            alertWithNoSubmit('The admin email you have entered is not on the permitted domain white list. Either clear the admin ' +
            'email address and add the domain to the whitelist first, or add an admin email address with one of ' +
            'following domains\n' + whiteList);
            return false;
        }
        return true;
    }
    return true;
}

/*
 * Checks whether the timeout value on the config page is a valid value, can be either greater
 * than 0 or -1. If left blank defaults to 10.
 *
 * @param  timeOut    the timeout element that needs to be verified
 * @return bool       true if the timeout value is valid, else false
 */
function isPermittedTimeoutValue(timeOutName)
{
    timeOutElement = jQuery('#'+timeOutName);
    timeOut = timeOutElement.val().replace(/ /g,""); //get rid of spaces.

    if (timeOut == '')
    {
        timeOutElement.val(10);
    }
    else
    {
        var timeoutPattern = /^-1$|^[1-9][0-9]*$/g;

        //if the value entered is not a valid value
        if (!timeoutPattern.test(timeOut))
        {
            alertWithNoSubmit('Value must be -1, or greater than 0. If you leave it blank it will default to 10.');
            timeOutElement.focus();
            return false;
        }
        return true;
    }
}
