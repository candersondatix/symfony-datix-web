var module = {};

function setModuleValue() {

    module['module'] = jQuery('#fld_code_like_module').val();
}

function clearFieldValue() {

    jQuery('#fld_code_like_title').clearField();
}

function selectMappedRadio() {

    if(jQuery('#fld_code_like_module').val() || jQuery('#fld_code_like').val()) {

        jQuery('#mapped-code-set').trigger('click');
    }
}

function codeSetChanged() {

    if(jQuery('[name="code_set_type"]:checked').val() != jQuery('#CHANGED-code-set-type').data('originalValue')) {

        setChanged('code-set-type');
    }
}

jQuery(function() {

    if (jQuery('#fld_code_like_module_title').val()) {

        //setModuleValue();
    }

    jQuery('#CHANGED-code-set-type').data('originalValue', jQuery('[name="code_set_type"]:checked').val());

    setModuleValue();
});