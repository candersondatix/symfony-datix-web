<?php
namespace src\admin\actionchains\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Response;
use src\framework\controller\Request;
use src\actionchains\model\ActionChainModelFactory;

/**
 * Controls the pages concerned with the setup/administration of action chains.
 */
class ActionChainsAdminController extends TemplateController
{ 
    public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
    
    /**
     * The landing screen for action chains, displays a list of current chains which you can edit/delete.
     */
    function listActionChains()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $Design = new \Listings_ListingDesign(array('columns' => array(
            new \Fields_DummyField(array('name' => 'recordid', 'label' => 'ID', 'type' => 'S')),
            new \Fields_DummyField(array('name' => 'ach_title', 'label' => 'Title', 'type' => 'S')),
        )));

        $orderby       = \Sanitize::SanitizeString($this->request->getParameter('orderby') ? $this->request->getParameter('orderby') : 'recordid');
        $order         = ($this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';
        $CurrentModule = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $ExcludeArray  = getExcludeArray('action_chains');
        $factory       = new ActionChainModelFactory();
        
        foreach (array_merge(array('ALL' => _tk('action_chain_all_modules')), getModArray($ExcludeArray)) as $Module => $ModName)
        {
            $query        = $factory->getQueryFactory()->getQuery();
            $actionChains = $factory->getCollection();
            
            $query->where(array('action_chains.ach_module' => $Module))
                  ->orderBy(array(array($orderby, $order)));
            
            $actionChains->setQuery($query);
            
            $RecordList = new \RecordLists_RecordList();
            $RecordList->AddRecordData($actionChains);

            $Listings[$Module] = new \Listings_ListingDisplay($RecordList, $Design);
            $Listings[$Module]->Action = 'displayactionchain';
            $Listings[$Module]->setOrder(($this->request->getParameter('orderby')) ? $this->request->getParameter('orderby') : 'recordid');
            $Listings[$Module]->setDirection(($this->request->getParameter('order')) ? $this->request->getParameter('order') : 'ASC');
            $Listings[$Module]->setSortAction(\Sanitize::SanitizeURL($this->request->getParameter('action')) . '&amp;module=' . $Module);
            $Listings[$Module]->EmptyMessage = _tk('no_action_chains');

            $PanelArray[$Module] = array('Label' => $ModuleDefs[$Module]['NAME'] ?: $ModName);

            if (!$CurrentModule)
            {
                $CurrentModule = $Module;
            }
        }

        $this->title = _tk('action_chains');
        $this->menuParameters = array(
            'menu_array'    => array(array('label' => _tk('new_action_chain'), 'link' => 'action=displayactionchain')),
            'panels'        => $PanelArray,
            'current_panel' => $CurrentModule,
            'floating_menu' => false,
        );

        $this->response->build('src/admin/actionchains/views/ActionChains.php', array(
            'Listings'      => $Listings,
            'CurrentModule' => $CurrentModule
        ));
    }
    
    /**
     * The action chain form, allows you to add new/edit existing chains.
     */
    function displayActionChain()
    {
        $ModuleDefs  = $this->registry->getModuleDefs();
        $recordid    = ($this->request->getParameter('recordid') ? $this->request->getParameter('recordid') : null);
        $this->title = isset($recordid) ? _tk('edit_action_chain') : _tk('add_action_chain');
        $factory     = new ActionChainModelFactory();
        
        if (isset($recordid))
        {
            // fetch the action chain from the database
            $chain = $factory->getMapper()->find($recordid);
            if (!isset($chain))
            {
                throw new \URLNotFoundException();
            }
        }
        else
        {
            // new action chain form
            $chain = $factory->getEntityFactory()->createObject();
        }

        $ExcludeArray = getExcludeArray('action_chains');
        $moduleField = \Forms_SelectFieldFactory::createSelectField('ach_module', '', $chain->ach_module, '');
        $moduleField->setCustomCodes(array_merge(array('ALL' => _tk('action_chain_all_modules')), getModArray($ExcludeArray)));
        $moduleField->setSuppressCodeDisplay();

        $FormArray = array(
            'details' => array(
                'Title' => _tk('details_action_chain'),
                'Rows' => array(
                    array('Name' => 'recordid', 'Title' => 'ID', 'ReadOnly' => true, 'Type' => 'string', 'Condition' => isset($chain->recordid)),
                    array('Name' => 'ach_module', 'Title' => 'Module', 'Type' => 'formfield', 'FormField' => $moduleField),
                    array('Name' => 'ach_title', 'Title' => 'Title', 'Type' => 'string', 'Width' => 50, 'MaxLength' => 254),
                    array('Name' => 'ach_description', 'Title' => 'Description', 'Type' => 'textarea', 'Rows' => '10', 'Columns' => '70')
                )
            ),
            'steps' => array(
                'Title' => 'Steps',
                'ControllerAction' => array(
                	'stepsSection' => array(
    					'controller' => 'src\\admin\\actionchains\\controllers\\ActionChainStepsController')), 
                'Rows' => array(),
            )
        );

        $FormDesign = new \Forms_FormDesign();
        $FormDesign->MandatoryFields = array(
            'ach_title'  => 'details',
            'ach_module' => 'details'
        );

        $Table = new \FormTable('', 'ADM', $FormDesign);
        $Table->MakeForm($FormArray, $chain, 'ADM');
        $Table->MakeTable();

        $GLOBALS['JSFunctions'][] = MakeJavaScriptValidation('ADM', $FormDesign, array('ach_title' => 'Title', 'ach_module' => 'Module'));

        $this->response->build('src/admin/actionchains/views/DisplayActionChain.php', array(
            'Table'    => $Table,
            'recordid' => $chain->recordid
        ));
    }
    
    /**
     * Save the action chain record.
     */
    public function saveActionChain()
    {
        $factory = new ActionChainModelFactory();
        $mapper  = $factory->getMapper();
        $chain   = $factory->getEntityFactory()->createFromRequest($this->request);
        
        $mapper->save($chain);
        
        AddSessionMessage('INFO', _tk('action_chain_save_message'));
        $this->redirect('app.php?action=displayactionchain&recordid='.$chain->recordid);
    }
    
    /**
     * Delete the action chain record.
     */
    public function deleteActionChain()
    {
        $factory = new ActionChainModelFactory();
        $mapper  = $factory->getMapper();
        $chain   = $factory->getEntityFactory()->createObject(array('recordid' => $this->request->getParameter('recordid')));
        
        $mapper->delete($chain);
        
        AddSessionMessage('INFO', _tk('action_chain_delete_message'));
        $this->redirect('app.php?action=listactionchains');
    }
}