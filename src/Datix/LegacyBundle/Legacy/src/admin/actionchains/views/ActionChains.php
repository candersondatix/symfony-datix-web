<?php foreach ($this->Listings as $Module => $Listing) : ?>
<div class="panel" style="display:<?php echo ($Module == $this->CurrentModule ? 'block' : 'none'); ?>" id="panel-<?php echo $Module; ?>">
    <div class="padded_div">
        <?php echo $Listing->GetListingHTML(); ?>
    </div>
</div>
<?php endforeach; ?>