<?php

namespace src\admin\dmd\controllers;

use src\framework\controller\TemplateController;
use src\admin\dmd\model\DmdFile;
use src\admin\dmd\model\DmdImportFacade;

class DmdController extends TemplateController
{
    /**
     * Renders the DM+D data import form.
     */
    function dmdImportForm()
    {
        $this->title  = _tk('import_dmd_data');
        $this->module = 'ADM';
        
        $this->addJs('src/admin/dmd/js/importdmddata.js');

         $this->sendToJs([
            'please_select_a_file' => _tk('please_select_a_file'),
            'must_be_zip_file'     => _tk('must_be_zip_file'),
            'loading_data'         => _tk('loading_data'),
            'import_dmd_data'      => $this->title,
            'wrong_dmd_filename'   => _tk('wrong_dmd_filename'),
            'dmd_older_import'     => _tk('dmd_older_import'),
            'current_version'      => $this->registry->getParm('DMD_VERSION')
        ]);

        $table       = new \FormTable();
        $uploadField = new \FormField();

        $table->MakeTitleRow('<b>'.$this->title.'</b>');
        $table->MakeRow(
            '<img src="images/Warning.gif" alt="Warning"> <b>ZIP file</b>', 
            $uploadField->MakeCustomField('<input id="importfile" name="importfile" type="file" size="50"/>')
        );
        $table->MakeTable();

        // check max upload size
        $recommendedSize = 12582912; // 12M
        $error = '';
        if (
            $this->convertToBytes(ini_get('post_max_size')) < $recommendedSize
            || $this->convertToBytes(ini_get('upload_max_filesize')) < $recommendedSize
        )
        {
            $error = _tk('dmd_max_upload_error');
        }

        // output form
        $this->response->build('src/admin/dmd/views/ImportDmdData.php', array(
            'table' => $table,
            'error' => $error
        ));
    }

    /**
     * Imports medications data from provided DM+D files.
     */
    public function importDmd()
    {
        try
        {
            $file = new DmdFile($_FILES['importfile']);
            $tmpDir = $this->registry->getClientFolder().DIRECTORY_SEPARATOR.'dmd';

            $dmdImport = new DmdImportFacade (new \DatixDBQuery(), $tmpDir);
            $dmdImport->importFile ($file);

            $message = _tk('dmd_import_successful');
        }
        catch (\Exception $e)
        {
            $error = $e->getMessage();
        }

        $this->response->build('src/admin/dmd/views/ImportDmdMessage.php', array(
            'error'   => $error,
            'message' => $message
        ));
    }

    /**
     * This function converts storage (ie: 8M or 2k) into bytes
     * Copied from http://php.net/manual/en/function.ini-get.php
     * @param string $value
     * @return int bytes
     */
    public function convertToBytes ($value)
    {
        $value = trim ($value);
        $last = strtolower($value[strlen($value)-1]);
        switch($last) {
            case 'g':
                $value *= 1024;
            case 'm':
                $value *= 1024;
            case 'k':
                $value *= 1024;
        }

        return $value;
    }
}