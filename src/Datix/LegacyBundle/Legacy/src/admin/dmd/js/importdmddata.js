function importDmdData()
{
    var filename = jQuery("#importfile").val().split('\\').pop();

    if (validateDmdFilename(filename))
    {
        // Disable the Import and Back buttons and submit the form
        var button = jQuery('#btnImport');
        button.val(globals.loading_data);

        // disable buttons
        jQuery('#btnImport').attr('disabled','disabled');
        jQuery('#btnBack').attr('disabled','disabled');

        // submit file by posting to iframe
        jQuery('#dmdform').submit();
    }
}

function validateDmdFilename (filename)
{
    // check to see if upload file is selected
    if (filename == '')
    {
        alert(globals.please_select_a_file);
        return false;
    }

    // check to see that upload file has .zip extension
    if (filename.substr(filename.length - 4) != ".zip")
    {
        alert(globals.must_be_zip_file);
        return false;
    }

    // check filename
    if (!filename.match (/^nhsbsa_dmd_[\d\.]+_\d+\.zip/))
    {
        alert(globals.wrong_dmd_filename);
        return false;
    }

    // check version against current version
    if (globals.current_version)
    {
        // replace non-digits with period
        globals.current_version = globals.current_version.replace(/\D/, '.');

        var newFileVersion = filename.match (/^nhsbsa_dmd_([\d\.]+)_\d+\.zip/)[1];

        // compare the two
        var currentVersionTokens = globals.current_version.split(/\./);
        var newFileVersionTokens = newFileVersion.split(/\./);

        for (var i = 0, len = currentVersionTokens.size(); i < len; i++)
        {
            if (currentVersionTokens[i] > newFileVersionTokens[i])
            {
                if (!confirm(globals.dmd_older_import))
                {
                    return false;
                }
            }
            else if (currentVersionTokens[i] < newFileVersionTokens[i])
            {
                break;
            }
        }
    }

    return true;
}