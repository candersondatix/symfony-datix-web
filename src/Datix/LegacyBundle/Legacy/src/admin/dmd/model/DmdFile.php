<?php

namespace src\admin\dmd\model;

/**
 * This class is a value object for the DM+D uploaded file
 * @author Edson Medina <emedina@datix.co.uk>
 */

class DmdFile
{
    private $tempFilename;

    /**
     * @param array $file Should contain $_FILES['importfile']
     * @throws \InvalidArgumentException
     */
    public function __construct (array $file)
    {
        if ($this->isValid ($file))
        {
            $this->tempFilename = $file['tmp_name'];
            $this->filename     = $file['name'];
        }
    }

    /**
     * Checks if $file is a valid item of $_FILES
     * @param array $file
     * @return bool
     * @throws \InvalidArgumentException
     */
    protected function isValid ($file)
    {
        if (empty($file['tmp_name']) || empty($file['name']))
        {
            throw new \InvalidArgumentException ("Invalid argument");
        }

        if (!file_exists($file['tmp_name']))
        {
            throw new \InvalidArgumentException ('File not found');
        }

        if (!preg_match ('/^nhsbsa_dmd_[\d\.]+_\d+\.zip/', $file['name']))
        {
            throw new \InvalidArgumentException ("Wrong DM+D filename format");
        }

        return true;
    }

    /**
     * Returns the temporary upload filename
     * @return string
     */
    public function getTempFilename ()
    {
        return $this->tempFilename;
    }

    /**
     * Returns the original filename
     * @return string
     */
    public function getFilename ()
    {
        return $this->filename;
    }

    /**
     * Gets the version from the filename (ie: "11.2.0" from "nhsbsa_dmd_11.2.0_20141117000001.zip")
     * @return string
     */
    public function getVersion ()
    {
        preg_match ('/^nhsbsa_dmd_([\d\.]+)_\d+\.zip/', $this->getFilename(), $matches);
        return $matches[1];
    }
}
