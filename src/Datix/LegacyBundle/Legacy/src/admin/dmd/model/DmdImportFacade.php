<?php

/**
 * This file deals with the import of DM+D xml zip files (for medication)
 * @author Edson Medina <emedina@datix.co.uk>
 */

namespace src\admin\dmd\model;

use \DatixDBQuery;

class DmdImportFacade
{
    protected $db;

    protected $tmpDir;
    protected $extractedFiles;
    protected $zipFilename;
    protected $extractedDir;
    protected $dmdVersion;

    protected $actualMedProductPacks;
    protected $actualMedProducts;
    protected $virtualMedProductPacks;
    protected $virtualMedProductPacksDtInfo;
    protected $virtualMedProducts;
    protected $virtualMedProductIngredients;
    protected $virtualMedProductCatCd;
    protected $suppliers;
    protected $drugs;
    protected $brands;

    protected $finalStructure;
    protected $existingCodes;
    protected $maxReferenceCode;


    public function __construct (DatixDBQuery $db, $tmpDir)
    {
        $this->db     = $db;
        $this->tmpDir = $tmpDir;
    }

    /**
     * This will invoke all the tasks related to the import
     * @param DmdFile $zipFile
     * @throws \Exception
     */
    public function importFile (DmdFile $zipFile)
    {
        ini_set('memory_limit', '512M');

        $this->moveZipToTempDir($zipFile->getTempFilename(), $zipFile->getFilename());

        $this->dmdVersion = $zipFile->getVersion();

        $this->extractZipFile();

        $this->db->beginTransaction();

        try
        {
            $this->deleteNonDmdCodes();
            $this->prepareLookupData();
            $this->process();
            $this->insertData();
        }
        catch (\Exception $e)
        {
            $this->db->rollBack();
            $this->deleteFiles();
            throw $e;
        }

        $this->db->commit();

        $this->deleteFiles();
    }

    /**
     * Moves the uploaded
     * @param string $tmpFilename temp filename for uploaded file
     * @param string $zipFilename filename for uploaded file
     */
    protected function moveZipToTempDir ($tmpFilename, $zipFilename)
    {
        if (!is_dir($this->tmpDir))
        {
            mkdir ($this->tmpDir);
        }

        $this->zipFilename = $this->tmpDir.DIRECTORY_SEPARATOR.$zipFilename;

        move_uploaded_file ($tmpFilename, $this->zipFilename);
    }

    /**
     * Extracts the zip file containing the XML files to import
     * @throws \Exception
     */
    protected function extractZipFile ()
    {
        $zip = new \ZipArchive();

        $res = $zip->open($this->zipFilename);
        if (true !== $res)
        {
            throw new \Exception ('Unable to open DMD zip archive (code: '.$res.')');
        }

        // create new directory with the zip filename (minus extension)
        $dirName = preg_replace('/.zip$/iu', '', basename($this->zipFilename));
        $this->extractedDir = $this->tmpDir . DIRECTORY_SEPARATOR . $dirName;

        if (!is_dir($this->extractedDir))
        {
            mkdir ($this->extractedDir);
        }

        $zip->extractTo($this->extractedDir);
        $zip->close();
    }

    /**
     * Parses XML files necessary for lookups and inserts new codes
     * @throws \Exception
     */
    protected function prepareLookupData ()
    {
        // Actual medicinal product packs
        $this->actualMedProductPacks = $this->parseFile ($this->findFirstFile ('f_ampp2_*.xml'), 'AMPP', array('APID','VPPID'), 'APPID');

        // Actual medicinal products
        $this->actualMedProducts     = $this->parseFile ($this->findFirstFile ('f_amp2_*.xml'), 'AMP', array('NM','SUPPCD'), 'APID');

        // Virtual medicinal product packs
        $this->virtualMedProductPacks       = $this->parseFile ($this->findFirstFile ('f_vmpp2_*.xml'), 'VMPP', array('VPID'), 'VPPID');
        $this->virtualMedProductPacksDtInfo = $this->parseFile ($this->findFirstFile ('f_vmpp2_*.xml'), 'DTINFO', array('PRICE'), 'VPPID');

        // Virtual medicinal products
        $this->virtualMedProducts           = $this->parseFile ($this->findFirstFile ('f_vmp2_*.xml'), 'VMP', array('VTMID'), 'VPID');
        $this->virtualMedProductIngredients = $this->parseFile ($this->findFirstFile ('f_vmp2_*.xml'), 'VPI', array('VTMID','STRNT_NMRTR_VAL','STRNT_NMRTR_UOMCD'), 'VPID');
        $this->virtualMedProductCatCd       = $this->parseFile ($this->findFirstFile ('f_vmp2_*.xml'), 'CONTROL_INFO', array('CATCD'), 'VPID');

        // Lookup
        $this->importCodes();
    }

    /**
     * Parse XML file
     * @param string $filename filename of the XML file
     * @param string $nodeName name of the tag we're iterating
     * @param array $nodesToFetch list of sub-tags names to fetch
     * @param string $idNodeName name of the node that contains the primary key
     * @return array
     * @throws \Exception
     */
    protected function parseFile ($filename, $nodeName, array $nodesToFetch, $idNodeName)
    {
        // XMLReader is being used to avoid loading the file first into memory, and then
        // we use SimpleXMLElement to parse subnodes (for simpler access).
        // This is more memory efficient.

        $xml = new \XMLReader;
        $xml->open ($filename);

        $result = [];

        // move to first <$nodeName> node
        while ($xml->read() && $xml->name !== $nodeName);

        // iterate all <$nodeName> nodes
        while ($xml->name === $nodeName)
        {
            $node = new \SimpleXMLElement ($xml->readOuterXML());

            $obj = new \stdClass();
            foreach ($nodesToFetch as $item)
            {
                $obj->{$item} = (string) $node->{$item};
            }

            $result[(string) $node->{$idNodeName}] = $obj;

            $xml->next($nodeName);
        }

        if (count($result) == 0)
        {
            throw new \Exception ('Empty result '.$filename);
        }

        return $result;
    }

    /**
     * Returns the first filename in the extractedDir that matches $pattern
     * @param string $pattern
     * @return string filename
     * @throws \Exception
     */
    protected function findFirstFile ($pattern)
    {
        $list = glob ($this->extractedDir.DIRECTORY_SEPARATOR.$pattern);

        if (count($list) > 1)
        {
            throw new \Exception ('More than one file matches '.$pattern);
        }

        if (count($list) == 0)
        {
            throw new \Exception ('No file matches '.$pattern);
        }

        return $list[0];
    }

    /**
     * Compute final structure
     */
    protected function process ()
    {
        // iterate actual medicinal product packs
        foreach ($this->actualMedProductPacks as $amppId => $ampp)
        {
            $actualMedProduct            = $this->actualMedProducts[$ampp->APID];
            $virtualMedProductPack       = $this->virtualMedProductPacks[$ampp->VPPID];
            $virtualMedProductPackDtInfo = $this->virtualMedProductPacksDtInfo[$ampp->VPPID];
            $virtualMedProduct           = $this->virtualMedProducts[$virtualMedProductPack->VPID];
            $virtualMedProductCatCd      = $this->virtualMedProductCatCd[$virtualMedProductPack->VPID];

            $med = [
                'med_reference'    => $amppId,
                'med_brand'        => $this->brands[$ampp->APID],
                'med_name'         => $this->drugs[$virtualMedProduct->VTMID],
                'med_controlled'   => $virtualMedProductCatCd->CATCD == '0000' ? 'N' : 'Y',
                'med_price'        => $virtualMedProductPackDtInfo->PRICE,
                'med_manufacturer' => $this->suppliers[$actualMedProduct->SUPPCD]
            ];

            $this->finalStructure[] = $med;
        }
    }

    /**
     * Insert data into database
     */
    protected function insertData ()
    {
        $this->db->PDO_query("TRUNCATE TABLE medications_main");

        // The following was copy+paste from the original script.
        // There was no real need to reset the control ID, so feel free to
        // fetch the current ID and increment if you want
        $this->db->PDO_query("DELETE FROM controls WHERE tablename = 'medications_main'");
        $recordId = 0;

        foreach ($this->finalStructure as $med)
        {
            $med['recordid'] = ++$recordId;

            $sql = "INSERT INTO medications_main
                (recordid, med_name, med_brand, med_manufacturer, med_controlled, med_price, med_reference)
                VALUES (:recordid, :med_name, :med_brand, :med_manufacturer, :med_controlled, :med_price, :med_reference)";

            $this->db->PDO_query($sql, $med);
        }

        // update controls
        $this->db->PDO_query('INSERT INTO controls (tablename, currentid) VALUES (?,?)', ['medications_main', $recordId]);

        // update current DM+D version number
        $this->db->PDO_query("DELETE FROM globals WHERE parameter = 'DMD_VERSION'");
        $this->db->PDO_query("INSERT INTO globals VALUES ('DMD_VERSION','',?)", [$this->dmdVersion]);
    }

    /**
     * Imports codes from the lookup xml file
     * @return void
     */
    protected function importCodes ()
    {
        $this->maxReferenceCode = $this->getMaxExistingCodes();
        $this->existingCodes    = $this->getExistingReferences();


        // import suppliers (MEDMAN code - manufacturer)
        $xml = file_get_contents ($this->findFirstFile ('f_lookup2*.xml'));
        $doc  = new \SimpleXMLElement ($xml);
        $this->suppliers = [];
        foreach ($doc->SUPPLIER->INFO as $supplier)
        {
            $this->suppliers[(string) $supplier->CD] = $this->insertOrUpdateUnit('MEDMAN', (string) $supplier->DESC, (string) $supplier->CD);
        }


        // import drugs (DRUG code)
        $xml = file_get_contents ($this->findFirstFile ('f_vtm2_*.xml'));
        $doc  = new \SimpleXMLElement ($xml);

        $this->drugs = [];
        foreach ($doc->VTM as $drug)
        {
            // Virtual therapeutic moieties
            $this->drugs[(string) $drug->VTMID] = $this->insertOrUpdateUnit('DRUG', (string) $drug->NM, (string) $drug->VTMID);
        }


        // import brands (MEDBRA code)
        $xml = file_get_contents ($this->findFirstFile ('f_amp2_*.xml'));
        $doc  = new \SimpleXMLElement ($xml);

        $this->brands = [];
        foreach ($doc->AMPS->AMP as $brand)
        {
            // Virtual therapeutic moieties
            $this->brands[(string) $brand->APID] = $this->insertOrUpdateUnit('MEDBRA', (string) $brand->NM, (string) $brand->APID);
        }
    }

    /**
     * Updates an existing code description and reference, if it doesn't exist creates new record. Then returns the code.
     * @param string $type cod_type The code field type
     * @param string $description cod_desc The description for the code
     * @param string $reference cod_npsa Medical reference
     * @return int cod_cod Code
     */
    protected function insertOrUpdateUnit ($type, $description, $reference)
    {
        $code = null;

        if (!isset($this->existingCodes[$type][$reference]))
        {
            // insert new
            $code = max(++$this->maxReferenceCode[$type], 1);
            $this->db->PDO_query('INSERT INTO code_types (cod_code, cod_descr, cod_type, cod_npsa) VALUES (?,?,?,?)', [$code, $description, $type, $reference]);
        }
        else
        {
            $existingCode = $this->existingCodes[$type][$reference];
            $code = $existingCode->cod_code;

            if ($existingCode->cod_descr != $description)
            {
                // description changed, update
                $this->db->PDO_query('UPDATE code_types SET cod_descr = ? WHERE cod_type = ? AND cod_code = ? AND cod_npsa = ?', [$description, $type, $code, $reference]);
            }
        }

        return $code;
    }

    /**
     * Deletes all non-DM+D codes (just types: drugs, brands, manufacturers and units)
     * @return void
     */
    protected function deleteNonDmdCodes ()
    {
        $sql = "DELETE FROM code_types WHERE cod_type IN ('DRUG','MEDBRA','MEDMAN','MEDUNI') AND cod_npsa IS NULL";
        $this->db->PDO_query($sql);
    }

    /**
     * Lists the highest cod_code for all the relevant code types, so we can increment them when inserting new ones
     * @return array ('code_type' => max_cod_code)
     */
    protected function getMaxExistingCodes ()
    {
        $sql = "SELECT cod_type, MAX(cod_code) AS last_cod_code FROM code_types WHERE cod_type IN ('DRUG','MEDBRA','MEDMAN','MEDUNI') GROUP BY cod_type";
        $maxCodeValues = $this->db->PDO_fetch_all($sql);

        $result = [];
        foreach ($maxCodeValues as $row)
        {
            $result[$row['cod_type']] = $row['last_cod_code'];
        }

        return $result;
    }

    /**
     * Lists the existing medical references, grouped by cod_type
     * @return array (ie: ['DRUG']['ref1'] will return an stdclass with the cod_descr and cod_code properties)
     */
    protected function getExistingReferences ()
    {
        $sql = "SELECT cod_code, cod_type, cod_npsa, cod_descr FROM code_types WHERE cod_type IN ('DRUG','MEDBRA','MEDMAN','MEDUNI') AND cod_npsa IS NOT NULL";
        $list = $this->db->PDO_fetch_all($sql);

        // return an array indexed by the reference (ref => true)
        // because it's much faster to search than in_array()
        $existingRefs = [];
        foreach ($list as $row)
        {
            $obj = new \stdClass();
            $obj->cod_descr = $row['cod_descr'];
            $obj->cod_code  = $row['cod_code'];

            $existingRefs[$row['cod_type']][$row['cod_npsa']] = $obj;
        }

        return $existingRefs;
    }

    /**
     * Remove uploaded and extracted files
     * @return void
     */
    protected function deleteFiles()
    {
        $iterator = new \RecursiveIteratorIterator (new \RecursiveDirectoryIterator($this->extractedDir), \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($iterator as $path)
        {
            if ($path->isDir())
            {
                rmdir ($path);
            }
            else
            {
                unlink ($path);
            }
        }

        rmdir ($this->extractedDir);

        // delete zip file
        unlink ($this->zipFilename);
    }
}
