<?php if ($this->error) : ?>
    <div class="form_error_wrapper">
        <div class="form_error"><?=_tk('file_import_error')?> - <?php echo htmlspecialchars($this->error); ?></div>
    </div>
    <div class="button_wrapper">
        <input id="btnBack" type="button" value="<?=_tk('btn_back')?>" onclick="SendTo('app.php?action=dmdimportform');" />
    </div>
<?php elseif ($this->message) : ?>
    <div class="info_div"><?php echo htmlspecialchars($this->message); ?></div>
    <div class="button_wrapper">
        <input id="btnBack" type="button" value="<?=_tk('btn_back')?>" onclick="SendTo('app.php?action=home&module=MED');" />
    </div>
<?php endif; ?>
