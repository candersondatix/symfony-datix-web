/**
 * Show/hide each table section of the form.
 *
 * @param string     id     The section ID.
 * @param DOMElement twisty The expand/collapse image.
 */
function toggleSections(id, twisty)
{
    jQuery("#"+id).toggle();
    if (jQuery(twisty).prop("src").indexOf("expand.gif") != -1)
    {
        jQuery(twisty).prop("src", "Images/collapse.gif");
        jQuery("form").append('<input id="'+id+'_expanded" name="'+id+'_expanded" type="hidden" value="1" />');
    }
    else
    {
        jQuery(twisty).prop("src", "Images/expand.gif");
        jQuery("#"+id+"_expanded").remove();
    }
}
var optionsChanged = false;
/**
 * Moves options between field lists.
 *
 * @param string from The ID of the list the options are being moved from.
 * @param string to   The ID of the list the options are being moved to.
 */
function moveOptions(from, to)
{
    jQuery("#"+from).find("option:selected").each(function()
    {
        jQuery("#"+to).append(jQuery(this).prop("selected", false));
    });
    sortListBox(document.getElementById(to));
    optionsChanged = true;
}

function saveChanges()
{
    var pageUrl = jQuery('#lbModule').closest('form').attr('action');

    if(optionsChanged)
    {
        if(window.confirm(saveChangesMessage))
        {
            SendTo(pageUrl + '&lbModule='+jQuery('#lbModule').val());
            optionsChanged = false;
        }
        else
        {
            changeModuleValue(previousModule);
        }
    }
    else
    {
        SendTo(pageUrl + '&lbModule='+jQuery('#lbModule').val());
    }
}
function changeModuleValue(module)
{
    var description = '';
    jQuery.each(customCodes['lbModule'], function(key,value)
    {
        if (value['value'] == module)
        {
            description = value['description'];
        }
    });
    jQuery('#lbModule_title').selectItem('<li id="'+module+'">'+description+'</li>');
}

jQuery(function( $ ) {

    $('#lbModule_title').blur(function(){

        saveChanges();
        return false;
    });

    $('.toggle-section').on('click', function() {

        var $target = $(this),
            $parent = $target.closest('.section_title_row');

        if($target.is(':checked')) {

            $parent.removeClass('section-greyed');
            $target.attr('title', 'Hide section in reporting');
        }
        else{

            $parent.addClass('section-greyed');
            $target.attr('title', 'Show section in reporting');
        }
    });
});


