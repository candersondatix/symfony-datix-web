<script type="text/javascript">
    function sortList(orderBy)
    {
        if (orderBy != '')
        {
            var order = 'ASC';

            if (document.reportslistmodule.order.value == 'ASC')
            {
                order = 'DESC';
            }

            document.reportslistmodule.orderby.value = orderBy;
            document.reportslistmodule.order.value = order;
            document.reportslistmodule.submit();
        }
    }

    function showModule(selectedModule)
    {
        $("module").value = $("lbModule").value;
    }
</script>
<?php if ($this->message) : ?>
    <div class="form_error_wrapper">
        <div class="form_error"><?php echo htmlspecialchars($this->message); ?></div>
    </div>
<?php endif; ?>
<div class="new_titlebg">
    <div class="title_text_wrapper"><b>Module:</b></div>
    <div class="title_select_wrapper" style="padding:2px">
        <form name="reportslistmodule" method="post" action="<?php echo $this->scripturl; ?>?action=listlistingreports">
            <input type="hidden" id="module" name="module" value="<?php echo htmlspecialchars($this->module); ?>" />
            <input type="hidden" id="orderby" name="orderby" value="<?php echo htmlspecialchars($this->orderby); ?>" />
            <input type="hidden" id="order" name="order" value="<?php echo htmlspecialchars($this->order); ?>" />
            <?php echo $this->ModuleDropDown; ?>
        </form>
    </div>
</div>
<form method="post" name="editreport" action="<?php echo $this->scripturl; ?>?action=reportsadminaction">
    <input type="hidden" name="form_action" value="edit" />
    <input type="hidden" name="rep_type" value="" />
    <input type="hidden" name="module" value="<?php echo htmlspecialchars($this->module); ?>" />
    <input type="hidden" name="rep_id" value="" />
    <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
        <tr class="tableHeader">
            <th class="windowbg" width="68%" align="left"><a class="<?php echo (($this->orderby == 'title') ? ('sort ' . \UnicodeString::strtolower($this->order)) : ''); ?>" href="Javascript:sortList('title');">Name</a></th>
            <th class="windowbg" width="6%" align="left"><b></b></th>
            <th class="windowbg" width="6%" align="left"><b></b></th>
        </tr>
        <?php foreach($this->reportCollection as $report) : ?>
            <tr class="listing-row">
                <?php $repurl = '?action=reportsadminaction&rep_id=' . $report->base_listing_report . '&module='.urlencode($this->module).'&form_action=edit'; ?>
                <!-- // need to use full URL because of the permissions screen generic redirection -->
                <td class="windowbg2<?php echo (($this->orderby == 'title') ? ' sort ' : ''); ?>"><a href="<?php echo $repurl; ?>&amp;form_action=edit"><?php echo htmlspecialchars($report->title); ?></a></td>
                <td class="windowbg2" valign="right" border="0">
                    <a href="<?php echo $repurl; ?>&amp;form_action=edit"> [edit]</a>
                </td>
                <td class="windowbg2" valign="right" border="0">
                    <a href="javascript:if(confirm('<?php echo _tk("delete_report_q"); ?>')){document.forms[1].form_action.value='delete';document.forms[1].rep_id.value=<?php echo $report->base_listing_report; ?>;document.forms[1].submit();}"> [delete]</a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if (count($this->reportCollection) == 0) : ?>
            <tr>
                <td class="rowtitlebg" colspan="5">
                    <b>No reports.</b>
                </td>
            </tr>
        <?php endif; ?>
    </table>
</form>