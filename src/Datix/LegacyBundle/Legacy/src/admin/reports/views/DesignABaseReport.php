<form method="post" id="designReport" name="designBaseReport" action="app.php?action=saveabasereport">
    <input type="hidden" id="module" name="module" value="<?php echo $this->module ?>" />
    <input type="hidden" id="recordid" name="recordid" value="<?php echo $this->recordid ?>" />
    <?php echo $this->form ?>
    <?php if ($this->recordid): ?>
        <div class="section_title_row">
            <div class="section_title_group section_title"><?php echo _tk('packaged_reports_title') ?></div>
        </div>
        <?php echo $this->listingDisplay->getListingHTML() ?>
        <div class="new_link_row">
            <a href="app.php?action=designapackagedreport&web_report_id=<?php echo $this->recordid ?>"><b><?php echo _tk('new_packaged_report'); ?></b></a>
        </div>
    <?php endif; ?>
    <?php echo $this->buttonGroup->getHTML(); ?>
</form>