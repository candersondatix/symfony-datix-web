<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
 * Secures controllers on the basis of users having the global CLA_SETUP set.
 */
class ClaimsSetupCodesFilter extends ControllerFilter
{
    /**
     * Checks that the user have the global CLA_SETUP set to "Y"
     *
     * @param string $action The subsequent action to perform on success.
     *
     * @throws \URLNotFoundException
     */
    public function doAction($action)
    {
        if (bYN(GetParm('CLA_SETUP', 'N')))
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        }
    }
}