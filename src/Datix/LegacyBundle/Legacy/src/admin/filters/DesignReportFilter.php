<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
 * Secures controllers on the basis of users being a Full Admin.
 */
class DesignReportFilter extends ControllerFilter
{

    /**
     * Checks if the user is a Full Admin.
     *
     * @param string $action The subsequent action to perform on success.
     *
     * @throws URLNotFoundException
     */
    public function doAction($action)
    {
        if (IsFullAdmin() || bYn(GetParm('ADM_NO_ADMIN_REPORTS')))
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        }
    }
}