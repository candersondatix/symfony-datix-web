<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
* Secures controllers on the basis of users having access to given module 
*/
class ModuleSetupFilter extends ControllerFilter
{
    /**
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException
    */
    public function doAction($action)
    {
        if (IsFullAdmin() || bYN(GetParm($_GET["module"].'_SETUP', "N")))
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        } 
    }
}