<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
 * Secures controllers on the basis of system being administered centrally.
 */
class CentrallyAdminSysFilter extends ControllerFilter
{
    /**
     * Checks if the system is centrally administered.
     *
     * @param string $action The subsequent action to perform on success.
     *
     * @throws \URLNotFoundException
     */
    public function doAction($action)
    {
        if (IsCentrallyAdminSys())
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        }
    }
}