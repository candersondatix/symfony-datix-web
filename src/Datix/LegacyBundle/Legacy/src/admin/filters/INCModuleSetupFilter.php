<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
* Secures controllers on the basis of users having the global ADM_GROUP_SETUP set.
*/
class INCModuleSetupFilter extends ControllerFilter
{
    /**
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException
    */
    public function doAction($action)
    {
        if (IsFullAdmin() || bYN(GetParm('INC_SETUP', 'N')))
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        } 
    }
}