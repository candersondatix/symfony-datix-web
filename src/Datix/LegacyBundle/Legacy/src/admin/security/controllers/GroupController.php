<?php
namespace src\admin\security\controllers;

use src\security\groups\model\Group;

use src\framework\controller\Controller;
use src\framework\controller\Loader;
use src\security\groups\model\GroupModelFactory;

class GroupController extends Controller
{
    /**
     * Deletes a group
     */
    function deletegroup()
    {
    	$recordid = $this->request->getParameter('recordid');
    	if (empty($recordid))
    	{
    		$recordid = $this->request->getParameter('grp_id');
    	}
    	 
    	// delete
        $factory = new GroupModelFactory();
        $group = $factory->getMapper()->find($recordid);
        $factory->getMapper()->delete($group);
        
        // notification
        AddSessionMessage('INFO', 'Group ' . $group->grp_code . ' deleted');
        
        // redirect
        $this->redirect('app.php?action=listgroups');
    }

    /**
     * Saves a group
     */
    function savegroup()
    {
    	require_once 'Source/Security/SecurityGroups.php';
    	
    	$ModuleDefs = $this->registry->getModuleDefs();
    	
    	$grp = $this->request->getParameters();
    	$UserAction = $grp['user_action'];
    	$grp_id = \Sanitize::SanitizeInt($grp['grp_id']);
    	$con_id = \Sanitize::SanitizeInt($grp['con_id']);

		$isFullAdmin = bYN($this->registry->getParm('FULL_ADMIN'));

    	// We can do cancel actions before checking admin permissions, because non-admins might get to
    	// this step looking at read-only groups
    	if ($UserAction == 'cancel')
    	{
    		if ($con_id)
    		{
    			$this->redirect('app.php?action=edituser&recordid='.$con_id);
    		}
    		elseif ($grp['pfl_id'])
    		{
    			$this->redirect('app.php?action=editprofile&recordid='.$grp['pfl_id']);
    		}
    		else
    		{
    			$this->redirect('app.php?action=listgroups');
    		}
    	}
    	
    	$error_message = _tk('missing_data_error');
    	
    	if ($this->request->getParameter('grp_code') == '')
    	{
    		$error['grp_code'] = '<div class="field_error" style="font-weight:bold">' . _tk('mandatory_err') . '</div>';
    	}
    	
    	if ($this->request->getParameter('grp_description') == '')
    	{
    		$error['grp_description'] = '<div class="field_error" style="font-weight:bold">' . _tk('mandatory_err') . '</div>';
    	}

    	// Latch onto the WHERE clauses for each module which describe which records the security group can access
		// Evaluate them for errors and take approriate actions. Evaluations and their actions are as follows:
		// Evaluation 1 - Check that a module's permissionwhere value was set (as opposed to disablewhere)
		// Evaluation 2 - Check that @prompt is in the WHERE statement, and disalow if it is
		// Evaluation 3 - Check the WHERE statement is syntactically correct, and disalow it if it isnt
		// Evaluation 4 - Check that the current user has access permissions to edit access to this module
		//                If user is an admin, they're allowed to edit this, even if they don't have explicitly set permission
		// Evaluation 5 - If module's permissionwhere was not set, but it's disablewhere was set instead, then the
		//                SQL for the security group to access the module is explicitly set as being the equivalent of 'none can be seen'
		foreach (\Sanitize::SanitizeRawArray($this->request->getParameters()) as $FieldName => $FieldValue)
		{
			//$fieldName is the name of the element in the form that is submitted
			$Parts = explode('_', $FieldName);

			//latch onto the textbox used to hold the SQL which describes what records can be accessed by the security group
			// Evaluation 1 - see description above
			if ($Parts[0] == 'permissionwhere' && count($Parts) == 2)
			{
				if ($FieldValue)
				{
					// Evaluation 2 - see description above
					if (\UnicodeString::strpos(\UnicodeString::strtolower($grp['permission'][$Parts[1]]['where']), '@prompt') !== false)
					{
						AddValidationMessage($FieldName, '\'WHERE\' statement should not contain \'@prompt\'');
						$error[$FieldName] = '<div class="field_error" style="font-weight:bold">\'WHERE\' statement should not contain \'@prompt\'</div>';
						$grp['default_selected_module'] = $Parts[1];
					}
					// Evaluation 3 - see description above
					elseif (!ValidateWhereClause($grp['permission'][$Parts[1]]['where'], $Parts[1]))
					{
						AddValidationMessage($FieldName, '\'WHERE\' statement contains a syntax error');
						$error[$FieldName] = '<div class="field_error" style="font-weight:bold">\'WHERE\' statement contains a syntax error</div>';
						$grp['default_selected_module'] = $Parts[1];
					}
					// Evaluation 4 - see description above
					elseif ($grp[$ModuleDefs[$Parts[1]]['PERM_GLOBAL']] == '' &&
						($grp['grp_type'] != GRP_TYPE_EMAIL && $grp['grp_type'] != GRP_TYPE_DENY_ACCESS) &&
						$Parts[1] != 'ADM')
					{
						$PermFieldName = $ModuleDefs[$Parts[1]]['PERM_GLOBAL'];
						AddValidationMessage($FieldName, 'Access level required if criteria for this security group entered.');
						$error[$PermFieldName] = '<div class="field_error" style="font-weight:bold">Access level required if criteria for this security group entered.</div>';
						$grp['default_selected_module'] = $Parts[1];
					}
				}
			}
			// Evaluation 5 - see description above
			elseif ($Parts[0] == 'disablewhere' && count($Parts) == 2)
			{
				// WHERE clause is overridden and access denied by default
				$grp['permissionwhere_'.$Parts[1]] = '1=2';
			}
		}
    	
    	if ($error)
    	{
    		$grp['error'] = $error;
    		$grp['error_message'] = $error_message;
    	
    		$this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', $grp);
            return;
    	}
    	
    	// Set default permission string for no access
    	require_once 'Source/Security/SecurityBase.php';
    	$DefaultPermission = GetDefaultPermissionString();
    	$DefaultPermissionArray = ParsePermString($DefaultPermission);
    	
    	$Factory = new GroupModelFactory();
    	
    	// Add record if necessary
    	if ($UserAction == 'new' || !$grp_id)
    	{
    	    $group = $Factory->getEntityFactory()->createObject($grp);
    		$PermissionArray = ParsePermString();
    	}
    	else
    	{
    	    $group = $Factory->getMapper()->find($grp_id);
    	    $PermissionArray = $group->getPermissions();
    	}

    	$PermissionArray = array_merge($DefaultPermissionArray, $PermissionArray);
    	
    	// Set module WHERE clauses
    	foreach ($grp as $FieldName => $FieldValue)
    	{
    		$Parts = explode('_', $FieldName);
    	
    		if ($Parts[0] == 'permissionwhere' && count($Parts) == 2)
    		{
    			$PermissionArray[$Parts[1]]['where'] = $FieldValue;
    		}
    	}
    	
    	$Permission = CreatePermString($PermissionArray);
    	
    	if (!isset($grp['grp_type']) || $grp['grp_type'] == '')
    	{
    		$grp['grp_type'] = GRP_TYPE_ACCESS;
    	}
    	
    	$group->grp_code = $grp['grp_code'];
    	$group->grp_description = $grp['grp_description'];
    	$group->permission = $Permission;
    	$group->grp_type = $grp['grp_type'];

        
    	if (!$Factory->getMapper()->save($group))
    	{
    	    AddSessionMessage('ERROR', _tk('cannot_save_err'));
    	    $this->redirect('app.php?action=editgroup&grp_id='.$group->recordid);
    	}
    	
    	if (empty($grp_id))
    	{
    	    $grp_id = $group->recordid;
    	}
    	
    	require_once 'Source/libs/LDAP.php';
    	$ldap = new \datixLDAP();

    	// Save LDAP mappings if enabled
    	if ($ldap->LDAPEnabled())
    	{
    		$result = \DatixDBQuery::PDO_query('DELETE from sec_ldap_groups WHERE grp_id = :grp_id', array('grp_id' => $grp_id));
    	
    		if ($grp['ldap_dn'])
    		{
    			foreach ($grp['ldap_dn'] as $ldap_dn)
    			{
    				\DatixDBQuery::PDO_build_and_insert('sec_ldap_groups', array('grp_id' => $grp_id, 'ldap_dn' => $ldap_dn));
    			}
    		}
    	}
    	
    	// Access levels are only relevant for security groups
    	// of type "Record access" and "Record access and e-mail notification"
    	if ($grp['grp_type'] == ($grp['grp_type'] | GRP_TYPE_ACCESS))
    	{
    		saveGroupPermission($grp_id, 'DIF_PERMS', $grp["DIF_PERMS"]);
    		saveGroupPermission($grp_id, 'RISK_PERMS', $grp["RISK_PERMS"]);
    		saveGroupPermission($grp_id, 'PAL_PERMS', $grp["PAL_PERMS"]);
    		saveGroupPermission($grp_id, 'COM_PERMS', $grp["COM_PERMS"]);
    		saveGroupPermission($grp_id, 'SAB_PERMS', $grp["SAB_PERMS"]);
    		saveGroupPermission($grp_id, 'ACT_PERMS', $grp["ACT_PERMS"]);
    		saveGroupPermission($grp_id, 'CON_PERMS', $grp["CON_PERMS"]);
    		saveGroupPermission($grp_id, 'AST_PERMS', $grp["AST_PERMS"]);
    		saveGroupPermission($grp_id, 'MED_PERMS', $grp["MED_PERMS"]);
    		saveGroupPermission($grp_id, 'STN_PERMS', $grp["STN_PERMS"]);
    		saveGroupPermission($grp_id, 'LIB_PERMS', $grp["LIB_PERMS"]);
    		saveGroupPermission($grp_id, 'DST_PERMS', $grp["DST_PERMS"]);
    		saveGroupPermission($grp_id, 'MSG_PERMS', $grp["MSG_PERMS"]);
    		saveGroupPermission($grp_id, 'DAS_PERMS', $grp["DAS_PERMS"]);
    		saveGroupPermission($grp_id, 'HSA_PERMS', $grp["HSA_PERMS"]);
    		saveGroupPermission($grp_id, 'HOT_PERMS', $grp["HOT_PERMS"]);
    		saveGroupPermission($grp_id, 'ADM_PERMS', $grp["ADM_PERMS"]);
    		saveGroupPermission($grp_id, 'CLA_PERMS', $grp["CLA_PERMS"]);
    		saveGroupPermission($grp_id, 'PAY_PERMS', $grp["PAY_PERMS"]);
    		saveGroupPermission($grp_id, 'TOD_PERMS', $grp["TOD_PERMS"]);
    		saveGroupPermission($grp_id, 'CQO_PERMS', $grp["CQO_PERMS"]);
    		saveGroupPermission($grp_id, 'CQP_PERMS', $grp["CQP_PERMS"]);
    		saveGroupPermission($grp_id, 'CQS_PERMS', $grp["CQS_PERMS"]);
    		saveGroupPermission($grp_id, 'LOC_PERMS', $grp["LOC_PERMS"]);
    		saveGroupPermission($grp_id, 'ATM_PERMS', $grp["ATM_PERMS"]);
    		saveGroupPermission($grp_id, 'ATQ_PERMS', $grp["ATQ_PERMS"]);
    		saveGroupPermission($grp_id, 'AMO_PERMS', $grp["AMO_PERMS"]);
    		saveGroupPermission($grp_id, 'AQU_PERMS', $grp["AQU_PERMS"]);
    		saveGroupPermission($grp_id, 'ATI_PERMS', $grp["ATI_PERMS"]);
    		saveGroupPermission($grp_id, 'POL_PERMS', $grp["POL_PERMS"]);
            saveGroupPermission($grp_id, 'ORG_PERMS', $grp["ORG_PERMS"]);
    	}
    	else
    	{
    		\DatixDBQuery::PDO_query('DELETE from sec_group_permissions WHERE grp_id = :grp_id AND item_code LIKE :item_code', array(
    				'grp_id' => $grp_id,
    				'item_code' => '%[_]PERMS'
    		));
    	}
    	
    	if ($UserAction == 'search')
        {
            $_SESSION['security_group']['grp_id'] = $grp_id;
            $_SESSION['security_group']['module'] = $this->request->getParameter('search_module');

            if (isset($ModuleDefs[$this->request->getParameter('search_module')]['SEARCH_URL']))
            {
                $this->redirect('app.php?' . $ModuleDefs[$this->request->getParameter('search_module')]['SEARCH_URL'] . '&module=' . $this->request->getParameter('search_module'));
            }
            elseif ($ModuleDefs[$this->request->getParameter('search_module')]['GENERIC'])
            {
                $this->redirect('app.php?action=search&module=' . $this->request->getParameter('search_module'));
            }
        }
    	
    	if ($grp['redirect_url'])
    	{
    	    $this->redirect($grp['redirect_url'] . "&grp_id=".$grp['grp_id']);
    	}
    	
    	AddSessionMessage('INFO', "Details for group $grp_id updated successfully.");
    	$this->redirect('app.php?action=listgroups');
    }
}