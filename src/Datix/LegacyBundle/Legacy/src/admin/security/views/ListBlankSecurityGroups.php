<?php if (empty($this->groups)): ?>
    <div style="margin-bottom:20px;">The system has detected that there are no security groups with blank where clauses.</div>
<?php else: ?>
    <div style="margin-bottom:10px;">Please note: The following Security groups have been identified as having an 'Access level' set, yet they have no criteria entered against them in the 'Criteria for this security group (SQL WHERE clause)' field.</div>
    <div style="margin-bottom:10px;">Members of such groups or members of profiles which include such groups will have access to all records in a module where a blank criteria exists.</div>
    <div style="margin-bottom:10px;">If this is intentional then please add 1=1 to the 'Criteria for the security group (SQL WHERE clause)'</div>
    <div style="margin-bottom:20px;">If this was unintentional then please remove the 'Access Level' by deleting the text in this field. </div>
    <?php foreach ($this->groups as $id => $details): ?>
        <div style="margin-bottom:10px;">
            <div><?php echo $id ?>: <a href="app.php?action=editgroup&grp_id=<?php echo $id ?>"><?php echo htmlspecialchars($details['name']) ?></a></div>

        <?php foreach ($details['modules'] as $module): ?>
            <div style="margin-left:20px"><?php echo htmlspecialchars($this->ModuleDefs[$module]['NAME']) ?></div>
        <?php endforeach ?>

        </div>
    <?php endforeach ?>

<?php endif; ?>