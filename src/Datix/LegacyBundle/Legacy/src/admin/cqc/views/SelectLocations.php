<div class="panel">
    <div class="padded_div">
        <form id="cqclocationform" name="cqclocationform" action="<?= $this->scripturl ?>?action=cqcselectoutcomes" method="post">
            <div id="selectalllocations" style="cursor:pointer"><a onclick="jQuery('.location_checkbox').attr('checked',true); jQuery('#selectalllocations').hide(); jQuery('#deselectalllocations').show();">Select all</a></div>
            <div id="deselectalllocations" style="display:none;cursor:pointer"><a onclick="jQuery('.location_checkbox').attr('checked',false); jQuery('#deselectalllocations').hide(); jQuery('#selectalllocations').show();">Deselect all</a></div>
            <?php foreach ($this->Locations as $ID => $Name): ?>
            <div><input type="checkbox" class="location_checkbox" name="locations[]" value="<?= $ID ?>"> <?= $Name ?></div>
            <?php endforeach ?>
        </form>
    </div>
    <div class="button_wrapper"><input type="button" value="<?= _tk('select_outcomes') ?>" onclick="jQuery('#cqclocationform').submit();"></div>
</div>
