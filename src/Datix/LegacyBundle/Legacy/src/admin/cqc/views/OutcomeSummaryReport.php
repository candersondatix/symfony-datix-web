<?php echo $this->html; ?>
<script language="JavaScript" type="text/javascript">
    function ListingHTML2pdf()
    {
        document.listing.action = '<?php echo $this->scripturl; ?>?action=exportswitch';
        document.listing.target = '_self';
        document.listing.submit();
    }

    function backToReportMenu()
    {
        document.listing.action = '<?php echo $this->scripturl; ?>?action=reportdesigner&module=CLA';
        document.listing.target = '_self';
        document.listing.submit();
    }
</script>
<div class="button_wrapper">
    <form method="post" name="listing" action="<?php echo $this->scripturl; ?>?action=reportdesigner&module=CQO">
        <input type="hidden" value="CLA" name="module" />
        <input type="hidden" id="orientation_post" name="orientation_post" value="P" />
        <input type="hidden" id="papersize_post" name="papersize_post" value="a4" />
        <input type="hidden" id="exportmode" name="exportmode" value="listing" />
        <input type="hidden" id="reportoutputformat" name="reportoutputformat" value="pdf" />
        <input type="hidden" id="saved_query" name="saved_query" value="<?php echo Sanitize::SanitizeInt($this->saved_query); ?>" />
        <input type="hidden" id="crosstabtype" name="crosstabtype" value="listing" />
        <input type="hidden" id="report" name="report" value="report7" />
        <input type="button" onclick="SendTo('<?php echo $this->scripturl; ?>?action=record&module=CQO&recordid=<?php echo $this->recordid; ?>')" value="<?php echo _tk('btn_back_to_record'); ?>">
        <?php
        if(!\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet())
        {
        ?><input type="button" onclick="
            var buttons = new Array();
            buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns()){GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();ListingHTML2pdf();}'};
            buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};
            PopupDivFromURL('exporttopdfoptions', 'Export', '<?php echo $this->scripturl."?action=httprequest&amp;type=exporttopdfoptions&nocsv=1" ?>', '', buttons, '');" value="Export"/><?php
        }
        ?>
</div>