<?php
namespace src\admin\cqc\controllers;

use src\framework\controller\TemplateController;

class CqcPromptTemplateController extends TemplateController
{   
    /**
     * Displays the CQC Prompt Template form.
     */
    public function editCqcPromptTemplate()
    {
        $id = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $promptTemplate = new \CQC_PromptTemplate($id);
        
        if ($this->request->getMethod() == 'POST')
        {
            $promptTemplate->UpdateFromPost();
            $promptTemplate->SaveConfigurationToDatabase();
            AddSessionMessage('INFO', 'Saved');
            $this->redirect('app.php?action=editcqcprompttemplate&recordid='.$promptTemplate->GetID());
        }
        
        $this->title      = ($id ? _tk('edit_prompt_template') : _tk('new_prompt_template'));
        $this->hasPadding = false;
        $this->module = 'ADM';
        
        $trail = new \BreadCrumbs();
        $trail->add($promptTemplate->data["ctp_desc_short"], "$scripturl?action=editcqcprompttemplate&recordid=" . $id, 1);

        $FormArray = array(
            'prompt' => array(
                'Title' => 'Template details',
                'Rows' => array(
                    'recordid',
                    'cqc_outcome_template_id',
                    'ctp_ref',
                    'ctp_desc',
                    'ctp_desc_short',
                    'ctp_element',
                    'ctp_guidance',
                )
            ),
            'subprompts' => array(
                'Title' => 'Subprompt templates',
                "Class" => 'CQC_PromptTemplate',
                "Function" => "ListSubpromptTemplates",
                'Rows' => array()
            )
        );

        $FormDesign = new \Forms_FormDesign();

        $FormDesign->MandatoryFields = array(
            'ctp_desc' => 'prompt',
        );

        $Table = new \FormTable('', 'CQP', $FormDesign);
        $Table->MakeForm($FormArray, $promptTemplate->data, 'CQP');

        $Table->MakeTable();

        $GLOBALS['JSFunctions'][] = MakeJavaScriptValidation('CQP', $FormDesign);

        $this->response->build('src/admin/cqc/views/EditCqcPromptTemplate.php', array(
            'Table'      => $Table,
            'breadcrumb' => $trail->output()
        ));
    }
}