<?php
namespace src\admin\cqc\controllers;

use src\framework\controller\TemplateController;

class CqcSubpromptTemplateController extends TemplateController
{   
    /**
     * Displays the CQC Subprompt Template form.
     */
    public function editCqcSubpromptTemplate()
    {
        $id = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $subpromptTemplate = new \CQC_SubpromptTemplate($id);
        
        if ($this->request->getMethod() == 'POST')
        {
            $subpromptTemplate->UpdateFromPost();
            $subpromptTemplate->SaveConfigurationToDatabase();
            AddSessionMessage('INFO', 'Saved');
            $this->redirect('app.php?action=editcqcsubprompttemplate&recordid='.$subpromptTemplate->GetID());
        }
        
        $this->title      = ($id ? _tk('edit_subprompt_template') : _tk('new_subprompt_template'));
        $this->hasPadding = false;
        $this->module = 'ADM';
        
        $trail = new \BreadCrumbs();
        $trail->add($subpromptTemplate->data["cts_desc_short"], "$scripturl?action=editcqcsubprompttemplate&recordid=" . $id, 2);

        $FormArray = array(
            'subprompt' => array(
                'Title' => 'Template details',
                'Rows' => array(
                    'recordid',
                    'cqc_prompt_template_id',
                    'cts_ref',
                    'cts_desc',
                    'cts_desc_short',
                    'cts_guidance',
                )
            )
        );

        $FormDesign = new \Forms_FormDesign();
        $FormDesign->MandatoryFields = array(
            'cts_desc' => 'subprompt',
        );

        $Table = new \FormTable('', 'CQS', $FormDesign);
        $Table->MakeForm($FormArray, $subpromptTemplate->data, 'CQS');
        $Table->MakeTable();

        $GLOBALS['JSFunctions'][] = MakeJavaScriptValidation('CQS', $FormDesign);

        $this->response->build('src/admin/cqc/views/EditCqcSubpromptTemplate.php', array(
            'Table'      => $Table,
            'breadcrumb' => $trail->output()
        ));
    }
}