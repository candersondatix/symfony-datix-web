<?php if ($this->module == 'COM') : ?>
<input type="hidden" id="lcom_primary" name="lcom_primary" value="<?php echo $this->data['lcom_primary']; ?>" />
<input type="hidden" id="lcom_iscomplpat" name="lcom_iscomplpat" value="<?php echo $this->data['lcom_iscomplpat']; ?>" />
<?php endif; ?>
<?php echo GetDivFieldHTML(GetFieldLabel('lcom_dreceived', 'Date Received').GetValidationErrors($this->data, 'lcom_dreceived'), $this->lComDReveived->GetField()); ?>
<input type="hidden" value="1" id="show_field_lcom_dreceived" name="show_field_lcom_dreceived">
<li>
    <table class="datatable" cellspacing="0">
        <tr>
            <td></td>
            <td><b><?php echo _tk('com_chain_due'); ?></b></td>
            <td><b><?php echo _tk('com_chain_done'); ?></b></td>
        </tr>
        <?php if (bYN(GetParm("SHOW_ACKNOWLEDGED",'Y'))) : ?>
        <tr>
            <td><b><?php echo _tk('com_chain_acknowledged'); ?>:</b></td>
            <td>
                <?php echo $this->DateObjArray['lcom_ddueack']->GetField().' '.GetValidationErrors($this->data, 'lcom_ddueack', ['inline' => true]); ?>
                <input type="hidden" value="1" id="show_field_lcom_ddueack" name="show_field_lcom_ddueack">
            </td>
            <td>
                <?php echo $this->DateObjArray['lcom_dack']->GetField().' '.GetValidationErrors($this->data, 'lcom_dack', ['inline' => true]); ?>
                <input type="hidden" name="show_field_lcom_dack" id="show_field_lcom_dack" value="1" />
            </td>
        </tr>
        <?php endif; ?>
        <?php if (bYN(GetParm("SHOW_ACTIONED",'Y'))) : ?>
        <tr>
            <td><b><?php echo _tk('com_chain_actioned'); ?>:</b></td>
            <td>
                <?php echo $this->DateObjArray['lcom_ddueact']->GetField().' '.GetValidationErrors($this->data, 'lcom_ddueact', ['inline' => true]); ?>
                <input type="hidden" value="1" id="show_field_lcom_ddueact" name="show_field_lcom_ddueact">
            </td>
            <td>
                <?php echo $this->DateObjArray['lcom_dactioned']->GetField().' '.GetValidationErrors($this->data, 'lcom_dactioned', ['inline' => true]); ?>
                <input type="hidden" name="show_field_lcom_dactioned" id="show_field_lcom_dactioned" value="1" />
            </td>
        </tr>
        <?php endif; ?>
        <?php if (bYN(GetParm("SHOW_RESPONSE",'Y'))) : ?>
        <tr>
            <td><b><?php echo _tk('com_chain_response'); ?>:</b></td>
            <td>
                <?php echo $this->DateObjArray['lcom_ddueresp']->GetField().' '.GetValidationErrors($this->data, 'lcom_ddueresp', ['inline' => true]); ?>
                <input type="hidden" value="1" id="show_field_lcom_ddueresp" name="show_field_lcom_ddueresp">
            </td>
            <td>
                <?php echo $this->DateObjArray['lcom_dresponse']->GetField().' '.GetValidationErrors($this->data, 'lcom_dresponse', ['inline' => true]); ?>
                <input type="hidden" name="show_field_lcom_dresponse" id="show_field_lcom_dresponse" value="1" />
            </td>
        </tr>
        <?php endif; ?>
        <?php if (bYN(GetParm("SHOW_HOLDING",'Y'))) : ?>
        <tr>
            <td><b><?php echo _tk('com_chain_holding'); ?>:</b></td>
            <td>
                <?php echo $this->DateObjArray['lcom_dduehold']->GetField().' '.GetValidationErrors($this->data, 'lcom_dduehold', ['inline' => true]); ?>
                <input type="hidden" value="1" id="show_field_lcom_dduehold" name="show_field_lcom_dduehold">
            </td>
            <td>
                <?php echo $this->DateObjArray['lcom_dholding']->GetField().' '.GetValidationErrors($this->data, 'lcom_dholding', ['inline' => true]); ?>
                <input type="hidden" name="show_field_lcom_dholding" id="show_field_lcom_dholding" value="1" />
            </td>
        </tr>
        <?php endif; ?>
        <?php if (bYN(GetParm("SHOW_REPLIED",'Y'))) : ?>
        <tr>
            <td><b><?php echo _tk('com_chain_replied'); ?>:</b></td>
            <td>
                <?php echo $this->DateObjArray['lcom_dduerepl']->GetField().' '.GetValidationErrors($this->data, 'lcom_dduerepl', ['inline' => true]);?>
                <input type="hidden" value="1" id="show_field_lcom_dduerepl" name="show_field_lcom_dduerepl">
            </td>
            <td>
                <?php echo $this->DateObjArray['lcom_dreplied']->GetField().' '.GetValidationErrors($this->data, 'lcom_dreplied', ['inline' => true]); ?>
                <input type="hidden" name="show_field_lcom_dreplied" id="show_field_lcom_dreplied" value="1" />
            </td>
        </tr>
        <?php endif; ?>
    </table>
</li>
<?php echo GetDivFieldHTML(GetFieldLabel('', 'Re-opened').GetValidationErrors($this->data, 'lcom_dreopened'), $this->lComDReopened->GetField()); ?>
<input type="hidden" name="show_field_lcom_dreopened" id="show_field_lcom_dreopened" value="1" />