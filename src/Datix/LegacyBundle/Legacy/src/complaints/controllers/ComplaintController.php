<?php

namespace src\complaints\controllers;

use src\framework\controller\Controller;

class ComplaintController extends Controller
{
    /**
     * Displays the "Complainant Chain" section when you have a Complainant attached to a complaint.
     */
    public function complaintDates()
    {
        global $OnSubmitJS;

        $data     = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('FormType');
        $module   = $this->request->getParameter('module');

        $DateFieldArray = GetComplaintDateFields();

        $OnSubmitJS['btnSave'] = 'if (CheckComplaintDates()){OldAlert(\''._tk('no_due_date_calc').'\')};';

        if ($FormType == 'New')
        {
            $data['lcom_dreceived'] = date('Y-m-d H:i:s.000');
            $CalculatedDates = CalculateComplaintDates([
                'date_received' => (GetParm('FMT_DATE_WEB') == 'US' ? date('m/d/Y') : date('d/m/Y')),
                'complaint_recordid' => \Sanitize::SanitizeInt($_REQUEST['main_recordid'])
            ]);
            $data = array_merge($data, $CalculatedDates);
        }

        foreach ($DateFieldArray as $DateField)
        {
            $DateObjArray[$DateField] = new \FormField($FormType);
            $DateObjArray[$DateField]->MakeDateField($DateField, UserDateToSQLDate($data[$DateField]));
        }

        $lComDReveived = new \FormField($FormType);
        $lComDReveived->MakeDateField('lcom_dreceived', UserDateToSQLDate($data['lcom_dreceived']), array('NotFuture' => true));

        $ChangedField = new \FormField;
        $ChangedField->MakeChangedFlagField('lcom_dreceived');
        $lComDReveived->ConcatFields($lComDReveived, $ChangedField);

        $lComDReopened = new \FormField($FormType);
        $lComDReopened->MakeDateField('lcom_dreopened', $data['lcom_dreopened'], array('NotFuture' => true));

        $this->response->build('src\complaints\views\ComplaintChain.php', [
            'module'        => $module,
            'data'          => $data,
            'lComDReveived' => $lComDReveived,
            'DateObjArray'  => $DateObjArray,
            'lComDReopened' => $lComDReopened
        ]);

    }

    /**
     * Displays the "Complainant Chain History" section when you have a Complainant attached to a complaint
     * and re-opened complainants.
     */
    function complaintDatesHistory()
    {
        $data = $this->request->getParameter('data');

        if (!$data['con_id']) //use primary complainant
        {
            $sql = '
                SELECT
                    con_id
                FROM
                    link_compl
                WHERE
                    lcom_primary = :lcom_primary AND
                    com_id = :com_id
            ';

            $data['con_id'] = \DatixDBQuery::PDO_fetch($sql, [
                'lcom_primary' => 'Y',
                'com_id' => $data['com_id']
            ], \PDO::FETCH_COLUMN);
        }

        $sql = '
            SELECT
                lcom_dreceived, lcom_ddueack, lcom_dack, lcom_ddueact, lcom_dactioned, lcom_ddueresp, lcom_dresponse,
                lcom_dduehold, lcom_dholding, lcom_dduerepl, lcom_dreplied, lcom_dreopened
            FROM
                link_compl
            WHERE
                con_id = :con_id AND
                com_id = :com_id AND
                lcom_current = :lcom_current
            ORDER BY
                lcom_dreopened DESC
        ';

        $HistoryRecords = \DatixDBQuery::PDO_fetch_all($sql, [
            'con_id' => $data['con_id'],
            'com_id' => $data['com_id'],
            'lcom_current' => 'N']
        );

        $this->response->build('src\complaints\views\ComplaintChainHistory.php', [
            'HistoryRecords' => $HistoryRecords
        ]);
    }
}