<?php
namespace src\profiles\model\profileparameter;

use src\framework\model\DatixEntityMapper;

/**
 * Manages the persistance of WordMergeTemplate objects.
 */
class ProfileParameterMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\profiles\\model\\profileparameter\\ProfileParameter';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'link_profile_param';
    }

}