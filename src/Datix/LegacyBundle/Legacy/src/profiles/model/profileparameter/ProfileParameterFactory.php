<?php
namespace src\profiles\model\profileparameter;

use src\framework\model\EntityFactory;

/**
 * Constructs CodeTagGroup objects.
 */
class ProfileParameterFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\profiles\\model\\profileparameter\\ProfileParameter';
    }


}