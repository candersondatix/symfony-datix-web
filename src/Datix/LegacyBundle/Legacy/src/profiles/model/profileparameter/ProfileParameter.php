<?php
namespace src\profiles\model\profileparameter;

use src\framework\model\DatixEntity;

/**
 * This entity models a user who can log in to the system
 */
class ProfileParameter extends DatixEntity
{
    /**
     * The ID of the profile that this parameter is linked to
     *
     * @var int
     */
    protected $lpp_profile;

    /**
     * The unique name of the parameter
     *
     * @var string
     */
    protected $lpp_parameter;

    /**
     * The value of the parameter for this profile
     *
     * @var string
     */
    protected $lpp_value;

}