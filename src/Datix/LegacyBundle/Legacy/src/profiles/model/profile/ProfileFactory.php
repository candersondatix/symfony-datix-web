<?php
namespace src\profiles\model\profile;

use src\framework\model\EntityFactory;

use src\profiles\observers\ProfileDeletionAuditor;
use src\profiles\observers\ProfileDeletionCleaner;

/**
 * Constructs CodeTagGroup objects.
 */
class ProfileFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\profiles\\model\\profile\\Profile';
    }

    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array())
    {
        $object = parent::doCreateObject($data);

        // attach observers
        $object->attach(new ProfileDeletionAuditor());
        $object->attach(new ProfileDeletionCleaner());

        return $object;
    }

}