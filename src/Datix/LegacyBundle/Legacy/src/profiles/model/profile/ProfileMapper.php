<?php
namespace src\profiles\model\profile;

use src\framework\model\DatixEntityMapper;

/**
 * Manages the persistance of WordMergeTemplate objects.
 */
class ProfileMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\profiles\\model\\profile\\Profile';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'profiles';
    }

}