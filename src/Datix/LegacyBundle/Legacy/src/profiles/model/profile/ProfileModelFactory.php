<?php
namespace src\profiles\model\profile;

use src\framework\model\ModelFactory;

class ProfileModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ProfileFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ProfileMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ProfileCollection($this->getMapper(), $this->getEntityFactory());
    }
}