<?php

namespace src\cqc\model;

use src\framework\model\RecordEntity;
use src\framework\query\Query;

/**
 *
 */
class OutcomeTemplate extends RecordEntity
{
    /**
     * The id of the Outcome Template (the same as recordid)
     * @var int
     */
    protected $cto_ref;

    /**
     * Short description
     *
     * @var string
     */
    protected $cto_desc_short;

    /**
     * description
     *
     * @var string
     */
    protected $cto_desc;

    protected $cto_theme;

    protected $cto_guidance;

    /**
     * Published date
     *
     * @var string
     */
    protected $cto_dpub;
    
    
    /**
     * @var string
     */
    protected $cto_active;
    
}