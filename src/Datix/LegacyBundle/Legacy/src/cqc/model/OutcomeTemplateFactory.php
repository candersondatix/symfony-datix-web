<?php
namespace src\cqc\model;

use src\framework\model\EntityFactory;

class OutcomeTemplateFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\cqc\\model\\OutcomeTemplate';
    }
}