<?php
namespace src\locations\controllers;

use src\framework\controller\Controller;
use Source\classes\Filters\Container;

/**
 * Controller class for hierarchical locations.
 */
class LocationController extends Controller
{
    public function getlocation()
    {
        $id = \Sanitize::SanitizeInt($this->request->getParameter('id'));
        $location = \DatixDBQuery::PDO_fetch('SELECT * FROM locations_main WHERE recordid = :id', array(
            'id' => $id
        ));
        $locTypes = \DatixDBQuery::PDO_fetch_all("SELECT cod_code, cod_descr FROM code_types WHERE cod_type = :code", array(
            'code' => 'LOCTYPE'
        ));
        $location['loc_types'] = $locTypes;

        $this->response = $this->call('src\\json\\controllers\\jsoncontroller', 'outputjson', array('data' => $location));
    }

    public function saveasmlocation()
    {
        $locationMapper = \System_NestedSetMapperFactory::getNestedSetMapper(\System_NestedSetMapperFactory::LOCATIONS);

        $name    = \Sanitize::SanitizeRaw($this->request->getParameter('loc_name'));
        $id      = ($this->request->getParameter('recordid')) ? \Sanitize::SanitizeInt($this->request->getParameter('recordid')) : null;
        $locType = \Sanitize::SanitizeString($this->request->getParameter('loc_type'));
        $address = \Sanitize::SanitizeString($this->request->getParameter('loc_address'));
        $active  = \Sanitize::SanitizeString($this->request->getParameter('loc_active'));

        if (!is_null($_POST['parent_id']))
        {
            $parentId = \Sanitize::SanitizeInt($this->request->getParameter('parent_id'));
        }

        if (!is_null($id))
        {
            // update
            $result = \DatixDBQuery::PDO_query('UPDATE locations_main SET loc_name = :name, loc_type = :locType, loc_address = :address, loc_active = :active WHERE recordid = :recordid', array(
                'name'     => $name,
                'locType'  => $locType,
                'address'  => $address,
                'active'   => $active,
                'recordid' => $id
            ));

            $this->changeActiveForDescendents($id, $active);
        }
        else
        {
            // insert
            $id = $locationMapper->insert($name, $parentId);

            $result = \DatixDBQuery::PDO_query('UPDATE locations_main SET loc_name = :name, loc_type = :locType, loc_address = :address, loc_active = :active WHERE recordid = :recordid', array(
                'name'     => $name,
                'locType'  => $locType,
                'address'  => $address,
                'active'   => $active,
                'recordid' => $id
            ));
        }

        if ($result)
        {
            $data = array('success' => true, 'id' => $id);
        }
        else
        {
            $data = array('success' => false);
        }

        $this->response = $this->call('src\\json\\controllers\\jsoncontroller', 'outputjson', array('data' => $data));
    }

    /**
     * When a locations active status is changed, its descendents become affected
     *
     * @param int $nodeId The ID of the location being explicitly (de)activated, whose descendants will also be affected
     * @param string $loc_active Y/N, depending on the action
     */
    private function changeActiveForDescendents($nodeId, $loc_active)
    {

        $sql = '
	        SELECT
	            recordid,
	            loc_active
	        FROM
	            vw_locations_main
	        WHERE
	        	parent_id = :parent_id
	    ';

        $locations = \DatixDBQuery::PDO_fetch_all($sql, array('parent_id' => $nodeId));

        foreach ($locations as $location)
        {
            if ($loc_active == 'N')
            {
                $new_loc_active_inherit = "N";
            } else {
                $new_loc_active_inherit = "";
            }

            \DatixDBQuery::PDO_query('UPDATE locations_main SET loc_active_inherit = :loc_active_inherit WHERE recordid = :recordid', array(
                'loc_active_inherit'   => $new_loc_active_inherit,
                'recordid' => $location['recordid']
            ));

            if ($loc_active == 'N' || ($loc_active == 'Y' && $location['loc_active'] != "N"))
            {
                $this->changeActiveForDescendents($location['recordid'], $loc_active);
            }

        }
    }
}
