<?php if ($this->formType != 'ReadOnly' && $this->formType != 'Print' && $this->formType != 'Locked') : ?>
<li class="field_div" name="progress_notes_row" id="prog_notes_row">
    <div class="field_label_div" style="width:25%">
        <label class="field_label" for="prog_notes">New note</label>
    </div>
    <div class="field_input_div" style="width:70%">
        <?php echo $this->progNotesField->GetField(); ?>
    </div>
</li>
<?php endif; ?>
<?php if (empty($this->notes)) : ?>
<li class="field_div" name="progress_notes_row" id="prog_notes_row">
    <div class="field_label_div" style="width:25%">
        <div class="field_label"><?php echo _tk('no_progress_notes'); ?></div>
    </div>
</li>
<?php else : ?>
    <?php foreach ($this->notes as $note) : ?>
    <?php $this->progNotesFieldEdit->MakeTextAreaFieldWithEdit('prog_notes_'.$note['recordid'], 10, 70, false, $note['pno_progress_notes'], true, true, ($this->editPerms == 'ALL' || ($this->editPerms == 'OWN' && $note['pno_createdby'] == $_SESSION['initials']))); ?>
    <li class="field_div" name="progress_notes_row" id="prog_notes_<?php echo $note['recordid']; ?>_row">
        <div class="field_label_div" style="width:25%">
            <div class="field_label"><?php echo code_descr('INC', 'inc_mgr', $note['pno_createdby']); ?></div>
            <div class="field_label"><?php echo FormatDateVal($note['pno_createddate'], true); ?></div>
        </div>
        <div class="field_input_div" style="width:70%">
            <?php echo $this->progNotesFieldEdit->GetField(); ?>
            <input type="hidden" name="CHANGED-prog_notes_<?php echo $note['recordid']; ?>" id="CHANGED-prog_notes_<?php echo $note['recordid']; ?>" />
        </div>
    </li>
    <?php if ($this->fullAudit) : ?>
        <?php foreach ($this->audit as $auditEntry) : ?>
            <?php if (UnicodeString::substr($auditEntry['aud_action'], 4) == 'prog_notes_'.$note['recordid']) : ?>
            <li class="field_div" name="progress_notes_row" id="aud_prog_notes_<?php echo $note['recordid']; ?>_row">
                <div class="field_label_div" style="width:25%">
                    <?php echo code_descr('INC', 'inc_mgr', $auditEntry['aud_login']); ?> <?php echo FormatDateVal($auditEntry['aud_date'], true); ?>
                </div>
                <div class="field_input_div" style="width:70%">
                    <?php echo htmlspecialchars($auditEntry['aud_detail']); ?>
                </div>
            </li>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>