// The semicolon is to prevent any poorly closed plugin or class from causing problems
;
//global array to store dashboards in. Dashboards are keyed by their recordid in the reports_dashboards table.
var DashboardObjects = {};
//ID of the currently viewed dashboard.
var CurrentDashboard = null;
//ID of the default dashboard.
var DefaultDashboard = null;

(function( $ ) {

    $.fn.datixDashboard = function( options ) {

        var $obj = this,

        defaults = {
            id: null,
            numCols: 2,
            widgetOrder: [],
            widgetArray: [],
            editable: false,
            name: ''
        },

        settings = $.extend({}, defaults, options );

        DashboardObjects[settings.id] = $obj;

        this.initialise = function() {

            var $allDashboards = $('.dashboard'),
                $currentDashboard = $('#dashboard_' + settings.id);

            //hide all other dashboards and show the current one.
            $allDashboards.not($currentDashboard).hide()
                .removeClass('dashboard-selected');

            /**
             * Sets the widths of the columns based on the width of .fieldset - padding + border of .dashboard and
             * column padding * number of columns, all divided by the total number of columns
             * There could conceivable be issues if the padding values changed or rendering isn't exact
             */

            $currentDashboard.show().addClass('dashboard-selected');

            $currentDashboard.find('.column').width(Math.floor(($('.fieldset').width() - (12 + (10 * settings.numCols))) / settings.numCols));

            //add the dashboard_tab-selected class to the dashboard tab to make it obvious to the user that it is selected
            $('.dashboard_tab').removeClass('dashboard_tab-selected');
            $('.dashboard_tab img').attr({src: 'Images/tick0.png', alt: 'default dashboard'});
            $('#dashboard_tab_'+settings.id).addClass('dashboard_tab-selected');
            $('#dashboard_tab_'+settings.id+' img').attr({src: 'Images/tick1.png', alt: 'default dashboard'});

            var $allColumns = $('.column'),
                currentDashboardColumns = $currentDashboard.find('.column');

            if(settings.editable) {

                // remove sortable functionality from other dashboards to prevent interference with the current one
                if ($allColumns.hasClass("ui-sortable")) {

                    $allColumns.sortable().sortable("destroy");
                }

                //Makes widgets sortable within columns
                currentDashboardColumns.sortable({
                    connectWith: '.column',
                    handle: '.widget_header',
                    opacity: 0.5,
                    start: function() {

                        $obj.FixColumnHeight();
                    },
                    update: function(event, ui) {

                        //when a widget has been moved.
                        $obj.SaveWidgetOrder();
                        $obj.FixColumnHeight();
                    },
                    placeholder:'widget_placeholder' //css class used to show where the widget will be dropped
                }).disableSelection();
            }

            $currentDashboard.on('mouseenter', '.dropdown', function(){

                if(settings.editable) {

                    currentDashboardColumns.sortable("disable");
                }

                /**
                 * This makes sure that the widget header is ontop when required - only needed for IE7
                 */
                $(this).closest('.widget_header').css('z-index', 100);

            }).on('mouseleave', '.dropdown', function(){

                if(settings.editable) {

                    currentDashboardColumns.sortable("enable");
                }

                /**
                 * This makes sure that the widget header is reset when required - only needed for IE7
                 */
                $(this).closest('.widget_header').css('z-index', 0);
            });

            //Initialise all widgets on this page.
            for(var i=0; i < settings.widgetArray.length; i++) {

                if( ! settings.widgetArray[i].data('rendered')){

                    settings.widgetArray[i].Initialise();
                }
                else {

                    settings.widgetArray[i].redisplay();
                }
            }
        };

        /**
         * Sets all dashboard columns to the same height.
         */
        this.FixColumnHeight = function() {

            var minHeight = 0,
                $columns = $obj.find('.column');

            $columns.css("min-height", "");

            $columns.each(function() {

                if ($(this).outerHeight() > minHeight) {

                    minHeight = $(this).outerHeight();
                }
            });

            $columns.css("min-height", minHeight);
        };

        /**
         * Sets this dashboard object as the "current" one, meaning that any actions will be assumed to be performed on it
         */
        this.SetAsCurrent = function() {

            if(CurrentDashboard != settings.id) {

                //TODO - This feels a bit of a nasty way to kill any running processes
                if (DashboardObjects) {

                    jQuery.each(DashboardObjects, function () {

                        dashboardObj = this;
                        widgets = dashboardObj.getWidgetArray();

                        if (widgets) {

                            jQuery.each(widgets, function () {

                                this.AbortAjax();
                            });
                        }
                    });
                }

                /**
                 * Set global for current dashboard
                 * @type {null|id|_FCEO.defaultParameters.id|queryHash.id|child.id|root.id|*}
                 */
                CurrentDashboard = settings.id;

                if (!$('#dashboard_body_' + settings.id).children().length) {
                    /**
                     * Start the ajax call to get the contents of the new dashboard, if it hasn't already been loaded
                     */
                    var ajaxurl = scripturl + '?action=httprequest&type=getdashboardhtml&recordid=' + settings.id;

                    this.ajax = $.ajax({
                        url: ajaxurl,
                        cache: false,
                        async: true,
                        dataType: "json",
                        success: function (data) {

                            $('#dashboard_body_' + settings.id).html(data.html);

                            var NewjQueryDashboard = $('#dashboard_' + settings.id).datixDashboard({

                                id: settings.id,
                                numCols: data.numCols,
                                name: data.name,
                                editable: data.editable,
                                widgetArray: settings.widgetArray
                            });

                            NewjQueryDashboard.initialise();
                        }
                    });
                }
                else {
                    $obj.initialise();
                }
            }
        };

        /**
         * Polls the HTML on the page and works out which widgets are in which columns. Stored this information in an array attached to the object.
         */
        this.GetWidgetOrder = function() {

            $dashboardColumns = $obj.find('.column');

            $dashboardColumns.each(function() {

                $column = $(this),
                columnIdRegex = /[0-9]+$/i,
                columnId = columnIdRegex.exec($column.attr('id'));

                /**
                 * Setup array for the column so that it can have items pushed to it
                 * @type {Array}
                 */
                settings.widgetOrder[columnId] = [];

                $column.find('.widget').each(function() {

                    $widget = $(this),
                    widgetIdRegex = /[0-9]+$/i,
                    widgetId = widgetIdRegex.exec($widget.attr('id'));

                    settings.widgetOrder[columnId].push(widgetId);
                });
            });

            /*for(var i=0; i < $dashboardColumns.length; i++) {

                var columnid = jQuery('div.dashboard-selected > div.column')[i].id.split("_")[2];
                this.widgetOrder[columnid] = new Array();

                for(var j=0; j<jQuery('div.dashboard-selected > #column_'+this.id+'_'+columnid+' .widget').length; j++) {

                    this.widgetOrder[columnid].push(jQuery('div.dashboard-selected > #column_'+this.id+'_'+columnid+' .widget')[j].id.replace('widget_', ''));
                }
            }*/
        };

        /**
         * Takes the current widget order and sends it via ajax to a function on the server that will save the order in the database.
         */
        this.SaveWidgetOrder = function() {

            $obj.GetWidgetOrder();

            var WidgetOrderToSave = [];

            //TODO this may not work as columnId is used for the array key (which may not be correct anyway), so index may not start at 0
            for(var i=0; i < settings.widgetOrder.length; i++) {

                WidgetOrderToSave.push(settings.widgetOrder[i].join(','));
            }

            $.get(scripturl + '?action=httprequest&type=setwidgetorders&id=' + settings.id + '&order=' + WidgetOrderToSave.join('-'));
        };

        /**
         * Updates dashboard settings with those input by the user. Passes everything saved in dashboard_options_form
         */
        this.UpdateDashboardSettings = function() {

            selectAllMultiple($('dash_profiles_' + settings.id));
            selectAllMultiple($('dash_groups_' + settings.id));
            selectAllMultiple($('dash_users_' + settings.id));

            $.getJSON(scripturl + '?action=httprequest&type=updatedashboardsettings&responsetype=json&id=' + settings.id + '&'+jQuery('#dashboard_options_form_' + settings.id).serialize(),
                function(data) {
                    SendTo(scripturl + '?action=dashboard&dash_id=' + data.dash_id);
                }
            );
        };

        /**
         * Hides the dashboard options pane for this dashboard
         */
        this.HideDashboardOptions = function() {

            $('#dashboard_options_' + settings.id).hide(500);
        };

        /**
         * Hides the dashboard options pane for this dashboard
         */
        this.Edit = function() {

            var id = settings.id;

            $.ajax({url: scripturl+'?action=httprequest&type=editdashboard&id=' + settings.id,
                success: function(data)
                {
                    var sHTML = SplitJSAndHTML(data);

                    AlertWindow = AddNewFloatingDiv('DashboardEditPopup');

                    AlertWindow.setWidth(500+'px');
                    AlertWindow.setContents(sHTML);
                    AlertWindow.setTitle("Dashboard Options");

                    var buttonHTML = '';

                    if(settings.editable)
                    {
                        buttonHTML += '<input type="button" value="Save" onClick="DashboardObjects[' + settings.id + '].UpdateDashboardSettings()" />&nbsp;';
                    }
                    buttonHTML += '<input type="button" value="Cancel" onClick="var div = GetFloatingDiv(\'DashboardEditPopup\');div.CloseFloatingControl();" />';
                    AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);

                    AlertWindow.display();

                    RunGlobalJavascript();
                },
                error: function(request, status, error)
                {
                    OldAlert(request.responseText);
                }
            });
        };

        /**
         * Gets confirmation and then deletes the dashboard
         */
        this.SetAsDefault = function() {

            $.get(scripturl + '?action=httprequest&type=setdefaultdashboard&responsetype=json&id=' + settings.id);

            $('.default_dash_marker').hide();
            $('#default_dash_marker_' + settings.id).show();

            $('.default_dashboard_link').show();
            $('#default_dashboard_link_' + settings.id).hide();
        };

        /**
         * Gets confirmation and then deletes the dashboard
         */
        this.Delete = function() {

            if(confirm("Are you sure you want to delete this dashboard (" + settings.name + ")? You will lose all reports you have placed on it.")) {

                $.getJSON(scripturl + '?action=httprequest&type=deletedashboard&responsetype=json&id=' + settings.id, function(data) {
                    if(data.success) {

                        // TODO - make this work without having to reload page. Need to take into account widget move options
                        SendTo(scripturl+'?action=dashboard&dash_id='+data.id);
                    }
                    else {

                        OldAlert('Error: Could not delete dashboard');
                    }
                });
            }
        };

        /**
         * Adds a report from my reports to a dashboard
         */
        this.AddToDashboard = function(id) {

            $.getJSON(scripturl+'?action=httprequest&type=addtomydashboard&responsetype=json&dash_id=' + settings.id + '&report_id=' + id, function(data) {

                if (data.success) {

                    var sHTML = SplitJSAndHTML(data.html.toString());
                    $obj.find('.column').first().append(sHTML);
                    RunGlobalJavascript();
                    DashboardObjects[data.dash_id].initialiseNewWidget();
                    DashboardObjects[data.dash_id].CheckEmptyDashboardMessage();
                }
                else {

                    OldAlert('Error: ' + data.message);
                }
            });
        };

        /**
         * Displays or hides empty dashboard message as appropriate
         */
        this.CheckEmptyDashboardMessage = function() {

            if($obj.find('.widget').length == 0) {

                $('#empty_dashboard_message_' + settings.id).show();
            }
            else {

                $('#empty_dashboard_message_' + settings.id).hide();
            }
        };

        this.getId = function() {

            return settings.id;
        };

        this.addWidget = function(widget) {

            settings.widgetArray.push(widget);
        };

        this.initialiseNewWidget = function() {

            settings.widgetArray[settings.widgetArray.length -1].Initialise();
        };

        this.getWidgetArray = function() {

            return settings.widgetArray;
        };

        return $obj;
    };

}( jQuery ));