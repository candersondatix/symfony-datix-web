<?php

namespace src\combolinking\model;

use src\framework\model\KeyedEntityCollection;

class ComboLinkCollection extends KeyedEntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\combolinking\\model\\ComboLink';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function keyProperty()
    {
        return ['sfm_form'];
    }
}