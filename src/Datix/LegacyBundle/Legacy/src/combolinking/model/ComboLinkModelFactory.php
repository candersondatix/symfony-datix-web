<?php

namespace src\combolinking\model;

use src\framework\model\ModelFactory;

class ComboLinkModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ComboLinkFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ComboLinkMapper(new \DatixDBQuery(), $this);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ComboLinkCollection($this->getMapper(), $this->getEntityFactory());
    }
}