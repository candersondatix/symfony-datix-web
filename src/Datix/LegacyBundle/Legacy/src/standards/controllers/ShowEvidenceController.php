<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class ShowEvidenceController extends Controller
{
    /**
     * Retrieves data for this evidence record (depending on if/which IDs are passed) and displays the form with a call to {@link ShowEvidenceForm()}.
     *
     * @param string $_GET['recordid']       id of a main evidence form (unlinked) that needs to be loaded.
     * @param string $_GET['link_recordid']  id of the link record if this is a linked piece of evidence.
     * @param string $_GET['main_module']    the module this evidence is to be linked to (if any). (new links only)
     * @param string $_GET['main_recordid']  id of the record this evidence is to be linked to. (new links only)
     * @param string $_GET["full_audit"]     set to 1 if we're displaying audit values for this record.
     */
    function evidence()
    {
        global $FormArray;

        $RecordId = (int) $this->request->getParameter('recordid');
        
        if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')))
        {
            require_once 'Source/security/SecurityBase.php';
            $Perms = GetUserHighestAccessLvlForRecord('LIB_PERMS', 'LIB', $RecordId);
        }
        else
        {
            $Perms = GetParm('LIB_PERMS');
        }
        
        $lib = ($this->request->getParameter('lib') ? $this->request->getParameter('lib') : array());

        if ($Perms == 'LIB_INPUT' && $RecordId)
        {
            CheckRecordNotFound(array('module' => 'LIB', 'recordid' => $RecordId));
        }

        // Existing link
        if ($this->request->getParameter('link_recordid'))
        {
            $sql = '
                SELECT
                    lib_id,
                    link_library.link_notes as link_notes,
                    link_library.stn_id as standard_id,
                    link_library.ele_id as element_id,
                    link_library.pro_id as prompt_id,
                    link_library.cqo_id,
                    link_library.cqp_id,
                    link_library.cqs_id
                FROM
                    link_library
                WHERE
                    link_library.link_recordid = :link_recordid
            ';

            $link = \DatixDBQuery::PDO_fetch($sql, array('link_recordid' => $this->request->getParameter('link_recordid')));

            $sql = '
                SELECT
                    library_main.recordid,
                    library_main.updateid, library_main.updatedby,
                    library_main.lib_name, library_main.lib_ourref,
                    library_main.lib_type, library_main.lib_subtype,
                    library_main.lib_loctype, library_main.lib_locactual,
                    library_main.lib_manager, library_main.lib_handler,
                    library_main.lib_dopened, library_main.lib_dclosed,
                    library_main.lib_descr, library_main.lib_references, library_main.lib_web_link,
                    library_main.lib_organisation, library_main.lib_unit,
                    library_main.lib_clingroup, library_main.lib_directorate,
                    library_main.lib_specialty, library_main.lib_dreview
                FROM
                    library_main
            ';

            $WhereClause = MakeSecurityWhereClause('library_main.recordid = :recordid', 'LIB', $_SESSION['initials']);

            $sql .= ' WHERE ' .  $WhereClause;

            $lib = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $link['lib_id']));

            if ((!$lib || !$Perms) && $link['lib_id'] )
            {
                CheckRecordNotFound(array('module' => 'LIB', 'recordid' => $link['lib_id']));
            }

            if (is_array($link))
            {
                $lib = array_merge($lib, $link);
            }

            if (!empty($lib))
            {
                // Needed because this table contains duplicate information - links are stored even if the evidence is not directly linked to the record
                if ($lib['prompt_id'] != '')
                {
                    $lib['main_module'] = 'PRO';
                    $lib['main_recordid'] = $lib['prompt_id'];
                }
                elseif ($lib['element_id'] != '')
                {
                    $lib['main_module'] = 'ELE';
                    $lib['main_recordid'] = $lib['element_id'];
                }
                elseif ($lib['standard_id'] != '')
                {
                    $lib['main_module'] = 'STN';
                    $lib['main_recordid'] = $lib['standard_id'];
                }
                elseif ($lib['cqo_id'] != '')
                {
                    $_GET['module'] = 'CQO';
                }
                elseif ($lib['cqp_id'] != '')
                {
                    $_GET['module'] = 'CQP';
                }
                elseif ($lib['cqs_id'] != '')
                {
                    $_GET['module'] = 'CQS';
                }

                $lib['link_recordid'] = \Sanitize::SanitizeInt($this->request->getParameter('link_recordid'));
                $lib['link_exists'] = true;
            }
        }
        // Existing record
        elseif ($RecordId)
        {
            $sql = '
                SELECT
                    library_main.recordid,
                    library_main.updateid, library_main.updatedby,
                    library_main.lib_name, library_main.lib_ourref,
                    library_main.lib_type, library_main.lib_subtype,
                    library_main.lib_loctype, library_main.lib_locactual,
                    library_main.lib_manager, library_main.lib_handler,
                    library_main.lib_dopened, library_main.lib_dclosed,
                    library_main.lib_descr, library_main.lib_references, library_main.lib_web_link,
                    library_main.lib_organisation, library_main.lib_unit,
                    library_main.lib_clingroup, library_main.lib_directorate,
                    library_main.lib_specialty, library_main.lib_dreview
                FROM
                    library_main
            ';

            $WhereClause = MakeSecurityWhereClause('library_main.recordid = :recordid', 'LIB', $_SESSION['initials']);

            $sql .= ' WHERE ' .  $WhereClause;

            $lib = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $RecordId));

            if ((!$lib || !$Perms) && $RecordId)
            {
                CheckRecordNotFound(array('module' => 'LIB', 'recordid' => $RecordId));
            }
        }

        if ($this->request->getParameter('main_module') && $this->request->getParameter('main_recordid'))
        {
            $lib['main_module'] = \Sanitize::getModule($this->request->getParameter('main_module'));
            $lib['main_recordid'] = \Sanitize::SanitizeInt($this->request->getParameter('main_recordid'));
        }

        if ($this->request->getParameter('main_recordid') && !$this->request->getParameter('main_module'))
        {
            $lib['main_module'] = \Sanitize::getModule($this->request->getParameter('module'));
            $lib['main_recordid'] = \Sanitize::SanitizeInt($this->request->getParameter('main_recordid'));
        }

        if ($lib['recordid'])
        {
            // Documents
            $sql = "
                SELECT
                    recordid,
                    doc_notes,
                    doc_dcreated,
                    doc_type,
                    typ.description as type_descr
                FROM
                    documents_main
                    LEFT OUTER JOIN code_doc_type typ ON doc_type = typ.code
                WHERE
                    LIB_ID = :recordid
                ORDER BY
                    doc_dcreated DESC
            ";

            $lib['documents'] = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $lib['recordid']));
        }

        // Check if we need to show full audit trail
        if ($lib['recordid'] && $this->request->getParameter('full_audit'))
        {
            $FullAudit = GetFullAudit(array('Module' => 'LIB', 'recordid' => $lib['recordid']));

            if ($FullAudit)
            {
                $lib['full_audit'] = $FullAudit;
            }
        }

        require_once 'Source/standards/EvidenceForm.php';

        if ($lib['recordid'])
        {
            ShowEvidenceForm($lib, 'Edit', $doc);
        }
        elseif ($lib['main_recordid'])
        {
            ShowEvidenceForm($lib, 'Link');
        }
        else
        {
            ShowEvidenceForm($lib, 'New');
        }
    }
}