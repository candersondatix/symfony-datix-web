<?php

namespace src\standards\controllers;

use src\framework\controller\TemplateController;

class NewStandardTemplateController extends TemplateController
{
    function newstandard()
    {
        global $scripturl, $TitleWidth, $JSFunctions, $FormType;

        if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && $this->request->getParameter('action') == 'standard' && ($FormType != "Print" && $this->request->getParameter('print') != 1))
        {
            $this->addJs('src/generic/js/panelData.js');
        }

        if($FormType == "Print" || $this->request->getParameter('print') == 1)
        {
            $this->addJs('src/generic/js/print.js');
        }

        $stn = ($this->request->getParameter('stn') ? $this->request->getParameter('stn') : '');
        $FormAction = ($this->request->getParameter('FormAction') ? $this->request->getParameter('FormAction') : 'New');
        $ownonly = ($this->request->getParameter('ownonly') ? $this->request->getParameter('ownonly') : 0);
        $pending = ($this->request->getParameter('pending') ? $this->request->getParameter('pending') : 0);

        $Perms = GetParm('STN_PERMS');

        CheckForRecordLocks('STN', $stn, $FormAction, $sLockMessage);

        if ($Perms == 'STN_READ_ONLY' || $FormAction == 'Locked')
        {
            $ReadOnly = true;
            $FormAction = 'ReadOnly';
        }

        if ($this->request->getParameter('print') == 1)
        {
            $FormAction = 'Print';
        }

        if ($stn['recordid'])
        {
            $PrintView = "window.open('{$scripturl}?action=standard&recordid={$stn['recordid']}&print=1&token=".\CSRFGuard::getCurrentToken()."')";
        }

        // Load form settings
        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'STN',
            'level' => 2,
            'form_type' => $FormType
        ));

        $TitleWidth = 20;

        // Create status field - Retrieve colour
        $sql = '
            SELECT
                cod_web_colour,
                description
            FROM
                code_stn_status
            WHERE
                code = :code
        ';

        $row = \DatixDBQuery::PDO_fetch($sql, array('code' => $stn['stn_status']));

        if ($row)
        {
            $stn_status = '
                <table border="0" cellpadding="4" cellspacing="0">
                    <tr>
                        <td bgColor="' . $row['cod_web_colour'] . '">' . $row['description'] . '</td>
                    </tr>
                </table>
            ';
        }
        else
        {
            $stn_status = '';
        }

        $obj_stn_status = new \FormField($FormAction);
        $obj_stn_status->MakeCustomField($stn_status);

        include 'Source/generic_modules/STN/BasicForm2.php';

        $this->title = $FormDesign->FormTitle;
        $this->subtitle = $FormDesign->FormTitleDescr;
        $this->module = 'STN';

        $buttonGroup = new \ButtonGroup();

        if ($FormAction != 'Print')
        {
            if ($FormAction != 'Locked' && $FormAction != 'ReadOnly')
            {
                if ($FormAction == 'Search')
                {
                    $buttonGroup->AddButton(array(
                        'id' => 'btnSearch',
                        'name' => 'btnSearch',
                        'label' => _tk('btn_search'),
                        'onclick' => 'document.frmStandard.rbWhat.value = \'Search\';submitClicked = true;document.frmStandard.submit();',
                        'action' => 'SEARCH'
                    ));
                }

                if ($Perms == 'STN_FULL')
                {
                    $buttonGroup->AddButton(array(
                        'id' => 'btnSaveStn',
                        'name' => 'btnSaveStn',
                        'label' => _tk('btn_save'),
                        'onclick' => 'submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()){document.frmStandard.submit();}',
                        'action' => 'SAVE'
                    ));
                }
            }

            $buttonGroup->AddButton(array(
                'id' => 'btnCancel',
                'name' => 'btnCancel',
                'label' => _tk('btn_cancel'),
                'onclick' => getConfirmCancelJavascript('frmStandard'),
                'action' => 'CANCEL'
            ));

            if ($this->request->getParameter('fromlisting'))
            {
                $buttonGroup->AddButton(array(
                    'id' => 'btnBackToListing',
                    'name' => 'btnBackToListing',
                    'label' => _tk('btn_back_to_report'),
                    'onclick' => 'document.frmStandard.rbWhat.value = \'BackToListing\';submitClicked = true;document.frmStandard.submit();',
                    'action' => 'BACK'
                ));
            }
        }

        if ($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) &&!empty($_SESSION['STN']['RECORDLIST']) && (!isset($ModuleDefs['STN']['NO_NAV_ARROWS']) || $ModuleDefs['STN']['NO_NAV_ARROWS'] != true))
        {
            $CurrentIndex = $_SESSION['STN']['RECORDLIST']->getRecordIndex(array('recordid' => $stn['recordid']));

            if ($CurrentIndex !== false)
            {
                $buttonGroup->AddNavigationButtons($_SESSION['STN']['RECORDLIST'], $stn['recordid'], 'STN');
            }
        }

        $STNTable = new \FormTable($FormAction, 'STN', $FormDesign);
        $STNTable->MakeForm($FormArray, $stn, 'STN');

        $this->sendToJs(array(
                'FormType' => $FormType,
                'printSections' => $STNTable->printSections)
        );

        if ($FormAction != 'Print')
        {
            $this->menuParameters = array(
                'table' => $STNTable,
                'buttons' => $buttonGroup
            );
        }
        else
        {
            $this->hasMenu = false;
        }

        $JSFunctions[] = 'var submitClicked = false;';

        if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Search')
        {
            $JSFunctions[] = 'AlertAfterChange = true;';
        }

        $JSFunctions[] = MakeJavaScriptValidation('STN', $FormDesign);

        $STNTable->MakeTable();

        $this->response->build('src/standards/views/NewStandard.php', array(
            'fromlisting' => $this->request->getParameter('fromlisting'),
        	'fromsearch' => $this->request->getParameter('fromsearch'),
        	'from_report' => $this->request->getParameter('from_report'),
            'FormAction' => $FormAction,
            'stn' => $stn,
            'ownonly' => $ownonly,
            'pending' => $pending,
            'sLockMessage' => $sLockMessage,
            'STNTable' => $STNTable,
            'buttonGroup' => $buttonGroup
        ));
    }
}