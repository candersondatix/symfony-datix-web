<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class ShowStandardController extends Controller
{
    function standard()
    {
        global $stn, $dtxdebug;

        $stn_id = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        $STNPerms = GetParm('STN_PERMS');
        
        if ($STNPerms == '')
        {
            // no access to standards module
            $this->redirect('app.php');
        }

        $sql = "
            SELECT
                recordid, updateid, updatedby,
                stn_set, stn_domain, stn_name,
                stn_version, stn_ourref, stn_descr,
                stn_type, stn_manager, stn_handler, stn_listorder,
                standards_score.stn_score,
                stn_organisation, stn_unit, stn_clingroup, stn_directorate, stn_specialty, stn_loctype, stn_locactual,
                stn_assessment, stn_status, stn_ddue
            FROM
                standards_main
                LEFT JOIN standards_score ON standards_main.recordid = standards_score.stn_id
                LEFT JOIN standards_status ON standards_status.stn_id = standards_main.recordid
        ";

        $WhereClause = MakeSecurityWhereClause("recordid = $stn_id", 'STN',  $_SESSION['initials']);

        $sql .= " WHERE $WhereClause";

        if ($stn_id)
        {
            $stn = \DatixDBQuery::PDO_fetch($sql);

            if (empty($stn))
            {
                $msg = "You do not have the necessary permissions to view Standard ID $stn_id.";
                if ($GLOBALS['dtxdebug'])
                {
                    $msg .= '<br><br>'.$sql;
                }
                fatal_error($msg, "Information");
            }

            $stn['stn_score'] = round($stn['stn_score']);
        }

        // Check if we need to show full audit trail
        if ($stn_id && $this->request->getParameter('full_audit'))
        {
            $sql = "
                SELECT
                    aud_login, aud_date, aud_action, aud_detail
                FROM
                    full_audit
                WHERE
                    aud_module = 'STN'
                    AND
                    aud_record = :recordid
                    AND
                    aud_action LIKE 'WEB%'
                ORDER BY
                    aud_date ASC
            ";

            $AuditArray = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $stn_id));

            foreach ($AuditArray as $row)
            {
                $Action = explode(':', $row['aud_action']);
                $FieldName = $Action[1];
                $FullAudit[$FieldName][] = $row;
            }

            if ($FullAudit)
            {
                $stn['full_audit'] = $FullAudit;
            }
        }

        $ACTWhereClause = MakeSecurityWhereClause($ActionWhere, 'ACT', $_SESSION['initials']);

        // Get actions
        $sql = "
            SELECT
                recordid, act_module, act_cas_id, act_from_inits, act_to_inits, act_by_inits,
                act_ddue, act_ddone, act_descr, act_score, act_model, act_cost, act_type,
                act_cost_type, act_dstart, act_synopsis, act_progress, act_cost_min, act_cost_max,
                act_priority, act_rss_id, act_resources, act_monitoring, seclevel, secgroup,
                updateddate, updatedby, updateid
            FROM
                ca_actions
            WHERE
                act_module = 'STN'
                AND
                act_cas_id = :recordid
        ";

        if ($ACTWhereClause)
        {
            $sql .= " AND $ACTWhereClause";
        }

        $sql .= "
            ORDER BY
                act_ddue asc
        ";

        $ActionsArray = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $stn['recordid']));

        foreach ($ActionsArray as $action)
        {
            $stn['Actions'][] = $action;
        }

        $Parameters = array(
            'stn' => $stn,
            'FormAction' => (($stn_id) ? 'edit' : 'New'),
            'ownonly' => \Sanitize::SanitizeString($this->request->getParameter('ownonly')),
            'pending' => \Sanitize::SanitizeString($this->request->getParameter('pending'))
        );

        $this->call('src\standards\controllers\NewStandardTemplateController', 'newstandard', $Parameters);
    }
}