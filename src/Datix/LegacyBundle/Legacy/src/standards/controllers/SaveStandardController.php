<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class SaveStandardController extends Controller
{
    function savestandard()
    {
        global $scripturl;

        session_start();

        $ModuleDefs = $this->registry->getModuleDefs();

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'STN',
            'level' => 2,
            'id' => null,
            'form_type' => ''
        ));
        $FormDesign->LoadFormDesignIntoGlobals();

        $form_action = $this->request->getParameter('rbWhat');
        $stn = \Sanitize::SanitizeRawArray($this->request->getParameters());
        $stn = ParseSaveData(array('module' => 'STN', 'data' => $stn));
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $recordid)
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'STANDARDS_MAIN', 'link_id' => $recordid));
        }

        switch ($form_action)
        {
            case 'Cancel':
            	if ($this->request->getParameter('fromsearch'))
            	{
            		$this->redirect('app.php?action=list&module=STN&listtype=search');
            	}
            	elseif ($this->request->getParameter('from_report'))
            	{
            		$this->redirect('app.php?action=list&module=STN&listtype=search&from_report=1');
            	}
            	
            	
                if ($stn['ownonly'] == 1)
                {
                    if ($stn['pending'] == 1)
                    {
                        $Controller = 'src\standards\controllers\ListMyPendingElementsController';
                        $Action = 'listmypendingelements';
                    }
                    else
                    {
                        $Controller = 'src\standards\controllers\ListMyElementsController';
                        $Action = 'listmyelements';
                    }
                }
                else
                {
                    if ($stn['pending'] == 1)
                    {
                        $Controller = 'src\standards\controllers\ListPendingElementsController';
                        $Action = 'listpendingelements';
                    }
                    else
                    {
                        $Controller = 'src\standards\controllers\ListAllStandardsTemplateController';
                        $Action = 'liststandards';
                    }
                }

                $this->call($Controller, $Action, array());
                return;
                break;
            case 'ShowAudit':
                $this->call('src\standards\controllers\ShowStandardController', 'standard', array(
                    'recordid' => $this->request->getParameter('recordid'),
                    'full_audit' => 1
                ));
                return;
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');
                break;
        }

        if (!$recordid)
        {
            $recordid = GetNextRecordID('standards_main', true);
        }

        $FullAudit = true;

        if ($FullAudit)
        {
            DoFullAudit('STN', 'standards_main', $recordid);
        }

        if ($recordid > 0)
        {
            $sql = "
                UPDATE
                    standards_main
                    SET
			        updatedby = '" . $_SESSION['initials'] . "',
			        updateddate = '" . date('d-M-Y H:i:s') . "',
    		        updateid = '" . GensUpdateID($this->request->getParameter('updateid')) . "'
                ";

            $sql .= ', '.GeneratePDOSQLFromArrays(
                array(
                    'FieldArray' => $ModuleDefs['STN']['FIELD_ARRAY'],
                    'DataArray' => $stn,
                    'Module' => 'STN',
                    'end_comma' => false
                ),
                $bindParams
            );

            $sql .= " WHERE recordid = $recordid";

            if (array_key_exists('updateid', $stn) && $stn['updateid'])
            {
                $sql .= " and updateid = '" . $stn['updateid'] . "'";
            }

            $query = new \DatixDBQuery($sql);
            $result = $query->prepareAndExecute($bindParams);

            if (!$result)
            {
                $error = "An error has occurred when trying to save the standard.  Please report the following to the Datix administrator: $sql";
            }
            elseif ($query->getRowsAffected() == 0)
            {
                $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=standard&recordid='.$recordid.'">here</a> to return to the record';
            }
        }

        if ($error != '')
        {
            SaveError($error);
        }

        $stn['recordid'] = $recordid;

        $_SESSION['stn_number'] = $stn['recordid'];

        // Save UDFs
        require_once 'Source/libs/UDF.php';
        SaveUDFs($recordid, MOD_STANDARDS);

        // Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => 'STN',
            'data'   => $stn
        ));

        $_SESSION['STANDARDS']['stn'] = $stn;
        $this->call('src\standards\controllers\ShowSaveStandardTemplateController', 'showsavestandard', array());
    }
}