<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class ShowElementController extends Controller
{
    function element()
    {
        global $ele;
        
        if (GetParm("STN_PERMS") == '')
        {
            // no access to standards module
            $this->redirect('app.php');
        }

        $ele_id = $this->request->getParameter('recordid');

    	$sql =
            'SELECT
    			standard_elements.recordid, standard_elements.updateid, standard_elements.updatedby,
    			standard_elements.ele_code, standard_elements.ele_descr, standard_elements.ele_score,
    			standard_elements.ele_manager, standard_elements.ele_handler, standard_elements.ele_sources,
    			standard_elements.ele_stn_id,
    			standards_main.stn_set, standards_main.stn_domain,
    			standard_elements.ele_guidance, standard_elements.ele_examples,
                standard_elements.ele_comments, standard_elements.ele_listorder,
                standard_elements.ele_priority, standard_elements.ele_action_taken,
    			standard_elements.ele_dassigned, standard_elements.ele_ddue, standard_elements.ele_dreviewed,
    			standards_main.stn_handler
    		FROM
    			standard_elements
            INNER JOIN
    			standards_main ON standard_elements.ele_stn_id = standards_main.recordid
    		WHERE
                '.MakeSecurityWhereClause("standard_elements.recordid = :ele_id", "STN",  $_SESSION["initials"]);
    
    	if ($ele_id)
        {
    		$ele = \DatixDBQuery::PDO_fetch($sql, array('ele_id' => $ele_id));
    		
    		if (empty($ele))
    		{
    		    $msg = "You do not have the necessary permissions to view Element ID $ele_id.";
                if ($GLOBALS['dtxdebug'])
                {
                    $msg .= '<br><br>'.$sql;
                }
                fatal_error($msg, "Information");
    		}
        }
        else
        {
            $ele['ele_stn_id'] = $this->request->getParameter('stn_id');
        }

        // Check if we need to show full audit trail
        if ($ele_id && $this->request->getParameter('full_audit'))
        {
            $sql = "
                SELECT
                    aud_login, aud_date, aud_action, aud_detail
                FROM
                    full_audit
                WHERE
                    aud_module = 'ELE'
                    AND
                    aud_record = :ele_id
                    AND
                    aud_action LIKE 'WEB%'
                ORDER BY
                    aud_date ASC
            ";

            $resultArray = \DatixDBQuery::PDO_fetch_all($sql, array('ele_id' => $ele_id));

            foreach ($resultArray as $row)
            {
                $Action = explode(':', $row['aud_action']);
                $FieldName = $Action[1];
                $FullAudit[$FieldName][] = $row;
            }

            if ($FullAudit)
            {
                $ele['full_audit'] = $FullAudit;
            }
        }

        require_once 'Source/standards/ElementForm.php';

        ShowElementForm($ele, (($ele_id) ? 'edit' : 'New'));
    }
}