<?php

namespace src\standards\controllers;

use src\framework\controller\TemplateController;

class ShowSaveElementTemplateController extends TemplateController
{
    function showsaveelement()
    {
        $ele = ($_SESSION['STANDARDS']['ele'] ? $_SESSION['STANDARDS']['ele'] : '');

        $ele_number = $ele['recordid'];

        if ($ele_number == '')
        {
            $this->redirect('app.php');
        }

        $this->title = 'Standards';
        $this->module = 'STN';
        $this->hasPadding = false;

        $this->response->build('src/standards/views/ShowSaveElement.php', array(
            'ele_number' => $ele_number,
            'ele_stn_id' => $ele['ele_stn_id']
        ));
    }
}