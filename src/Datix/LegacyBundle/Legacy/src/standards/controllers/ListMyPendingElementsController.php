<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class ListMyPendingElementsController extends Controller
{
    /**
     * Lists pending elements in Standards module
     */
    function listmypendingelements()
    {
        $Parameters = array(
            'ownonly' => true,
            'pending' => true
        );

        $this->call('src\standards\controllers\ListAllStandardsTemplateController', 'liststandards', $Parameters);
    }
}