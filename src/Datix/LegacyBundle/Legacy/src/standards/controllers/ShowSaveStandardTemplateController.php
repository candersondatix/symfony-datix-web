<?php

namespace src\standards\controllers;

use src\framework\controller\TemplateController;

class ShowSaveStandardTemplateController extends TemplateController
{
    function showsavestandard()
    {
        $stn = ($_SESSION['STANDARDS']['stn'] ? $_SESSION['STANDARDS']['stn'] : '');
        $ModuleDefs = $this->registry->getModuleDefs();

        $stn_number = $stn['recordid'];

        if ($stn_number == '')
        {
            $this->redirect('app.php');
        }

        $this->title = 'Standards';
        $this->module = 'STN';
        $this->hasPadding = false;

        if (bYN(GetParm('WEB_TRIGGERS', 'N')))
        {
            require_once 'Source/libs/Triggers.php';
            ExecuteTriggers($stn_number, $stn, $ModuleDefs['STN']['MOD_ID']);
        }

        $recordurl = $scripturl . "?action=" . $ModuleDefs['STN']['ACTION'] . "&recordid=" . htmlspecialchars($stn_number);

        if ($this->request->getParameter('panel'))
        {
            $recordurl .= '&panel=' . $this->request->getParameter('panel');
        }

        $status_message = _tk('STN_save_message');

        AddSessionMessage('INFO', $status_message);

        $this->redirect($recordurl);

        $this->response->build('src/standards/views/ShowSaveStandard.php', array(
            'stn_number' => $stn_number
        ));
    }
}