<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class EvidenceMatchController extends Controller
{
    /**
     * Checks for matching evidence records based on the current values on the evidence form.
     *
     * @global string  $dtxtitle
     * @global array   $FieldDefs
     *
     * @param  array   $_GET       Contains the current form values.
     */
    function evidencematch()
    {
        global $dtxtitle, $FieldDefs;

        $dtxtitle = 'Check for matching evidence';

        // Get search criteria
        $Data = $this->request->getParameters();

        foreach ($Data as $name => $value)
        {
            if (isset($FieldDefs['LIB'][$name]) && $value != '' && \UnicodeString::strtolower($name) != 'link_notes')
            {
                $FieldDef = $FieldDefs['LIB'][$name];
                MakeFieldWhere('LIB', $name, $value, $search_where[$name]);
            }
        }

        $PDOParamsArray = array();

        switch ($Data['main_module'])
        {
            case 'STN':
                $select = ', sl.stn_id AS main_recordid';
                $leftJoin = 'LEFT JOIN
                link_library sl ON sl.lib_id = l.recordid AND sl.stn_id = :main_recordid AND sl.ele_id IS NULL AND sl.pro_id IS NULL AND sl.link_type = \'E\'';
                break;
            case 'ELE':
                $select = ', el.ele_id AS main_recordid';
                $leftJoin = 'LEFT JOIN
                link_library el ON el.lib_id = l.recordid AND el.stn_id IS NOT NULL AND el.ele_id = :main_recordid AND el.pro_id IS NULL AND el.link_type = \'E\'';
                break;
            case 'PRO':
                $select = ', pl.pro_id AS main_recordid';
                $leftJoin = 'LEFT JOIN
                link_library pl ON pl.lib_id = l.recordid AND pl.stn_id IS NOT NULL AND pl.ele_id IS NOT NULL AND pl.pro_id = :main_recordid AND pl.link_type = \'E\'';
                break;
        }

        if ($leftJoin)
        {
            $PDOParamsArray['main_recordid'] = $Data['main_recordid'];
        }

        if ($search_where)
        {
            $where = implode(" AND ", $search_where);
        }

        $sql = '
            SELECT
                l.recordid,
                l.lib_name,
                l.lib_ourref'.
                $select.'
            FROM
                library_main l '.
            $leftJoin;

        if ($where)
        {
            $sql .= '
            WHERE ('.$where.')';
        }
        else
        {
            // Must select at least one field to match on
            $sql .= '
            WHERE 1=2';
        }

        $result = PDO_fetch_all($sql, $PDOParamsArray);

        $this->response->build('src/standards/views/EvidenceMatch.php', array(
            'matches' => $result,
            'main_module' => $Data['main_module'],
            'main_recordid' => $Data['main_recordid']
        ));
    }
}