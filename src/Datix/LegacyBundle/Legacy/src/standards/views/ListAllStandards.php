<script language="JavaScript" type="text/javascript">
    function expandStandards(url_expand, expand_action)
    {
        document.frmStandards.url_expand.value = url_expand;
        document.frmStandards.expand_action.value = expand_action;
        document.frmStandards.submit();
    }
</script>
<table border="0" width="100%" cellspacing="1" cellpadding="2" class="gridlines" align="center">
    <?php
    $prev_set = '';
    $prev_domain = '';
    $prev_standard = '';
    ?>
    <?php if (count($this->resultset) == 0) : ?>
    <tr>
        <td class="windowbg2">
            <b>
                <?php if ($this->pending) : ?>
                No pending elements found.
                <?php elseif ($this->ownonly) : ?>
                No elements found.
                <?php else : ?>
                No standards found.
                <?php endif; ?>
            </b>
        </td>
    </tr>
    <?php endif; ?>
    <?php foreach($this->resultset as $row) : ?>
    <?php
    $url_set = 'stn_set=' . $row['stn_set'] . '&stn_version=' . $row['stn_version'] . '&stn_organisation='
        . $row['stn_organisation'] . '&stn_assessment=' . $row['stn_assessment'];
    $url_domain = $url_set . '&stn_domain=' . $row['stn_domain'];
    $url_standard = $url_domain . '&stn_id=' . $row['stn_id'];
    ?>
    <!-- Outpout sets/origin headers -->
    <?php if ($prev_set != $url_set) : ?>
    <tr>
        <td class="titlebg" width="1%">
            <?php if (is_array($this->url_expanded) && in_array($url_set, $this->url_expanded)) : ?>
            <a href="Javascript:expandStandards('<?php echo $url_set; ?>', 0);">
            <img src="Images/collapse_windowbg2.gif" alt="-" border="0" /></a>
            <?php $stn_set_isexpanded = true; ?>
            <?php else : ?>
            <a href="Javascript:expandStandards('<?php echo $url_set; ?>', 1);">
            <img src="Images/expand_windowbg.gif" alt="+" border="0" /></a>
            <?php $stn_set_isexpanded = false; ?>
            <?php endif; ?>
        </td>
        <td class="titlebg" colspan="5">
            <big><b><font>
                <?php
                echo ($row['set_descr'] ? $row['set_descr'] : $row['stn_set'])
                    . ($row['stn_version'] ? ' (' . GetFieldLabel('stn_version', 'Version') . ': ' . $row['stn_version'] . ')' : '')
                    . ($row['stn_organisation'] ? ' (' . $row['stn_organisation'] . ')' : '')
                    . ($row['stn_assessment'] ? ' (' . $row['stn_assessment'] . ')' : ''); ?>
            </font></b></big>
        </td>
    </tr>
    <?php endif; ?>
    <!-- Output domain headers -->
    <?php if ($stn_set_isexpanded && $prev_domain != $url_domain) : ?>
    <tr>
        <td class="windowbg" width="1%"></td>
        <td class="windowbg" width="1%">
            <?php if (is_array($this->url_expanded) && in_array($url_domain, $this->url_expanded)) : ?>
            <a href="Javascript:expandStandards('<?php echo $url_domain; ?>', 0);">
            <img src="Images/collapse_windowbg2.gif" alt="-" border="0" /></a>
            <?php $stn_domain_isexpanded = true; ?>
            <?php else : ?>
            <a href="Javascript:expandStandards('<?php echo $url_domain; ?>', 1);">
            <img src="Images/expand_windowbg.gif" alt="-" border="0" /></a>
            <?php $stn_domain_isexpanded = false; ?>
            <?php endif; ?>
        </td>
        <td class="windowbg" colspan="4">
            <b><?php echo ($row['domain_descr'] ? $row['domain_descr'] : $row['stn_domain']); ?></b>
        </td>
    </tr>
    <?php endif; ?>
    <!-- Ouput Standard -->
    <?php if ($stn_set_isexpanded && $stn_domain_isexpanded && $prev_standard != $url_standard) : ?>
    <tr>
        <td class="windowbg2" width="1%"></td>
        <td class="windowbg2" <?php echo ($row['colour_row'] ? 'style="background:#'.$row['colour_row']['cod_web_colour'].';" ' : '' ); ?> width="1%"></td>
        <td class="windowbg2" width="1%">
            <?php if (is_array($this->url_expanded) && in_array($url_standard, $this->url_expanded)) : ?>
            <a href="Javascript:expandStandards('<?php echo $url_standard; ?>', 0);">
            <img src="Images/collapse_windowbg2.gif" alt="-" border="0" /></a>
            <?php $stn_standard_isexpanded = true; ?>
            <?php else : ?>
            <a href="Javascript:expandStandards('<?php echo $url_standard; ?>', 1);">
            <img src="Images/expand_windowbg2.gif" alt="-" border="0" /></a>
            <?php $stn_standard_isexpanded = false; ?>
            <?php endif; ?>
        </td>
        <td class="windowbg2">
            <a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $row['stn_id']; ?><?php echo ($this->ownonly ? '&amp;ownonly=1'  : ''); ?><?php echo ($this->pending ? '&amp;pending=1'  : ''); ?>"><?php echo $row['stn_ourref']; ?></a>
        </td>
        <td class="windowbg2" colspan="2">
            <a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $row['stn_id']; ?><?php echo ($this->ownonly ? '&amp;ownonly=1'  : ''); ?><?php echo ($this->pending ? '&amp;pending=1'  : ''); ?>"><?php echo $row['stn_name']; ?></a>
        </td>
    </tr>
    <?php endif; ?>
    <!-- Output Element -->
    <?php if ($stn_set_isexpanded && $stn_domain_isexpanded && $stn_standard_isexpanded && $row['ele_id']) : ?>
    <tr>
        <td class="windowbg2_light" width="1%"></td>
        <td class="windowbg2_light" width="1%"></td>
        <td class="windowbg2_light" width="1%"></td>
        <td class="windowbg2_light" valign="top" align="right">
            <a href="<?php echo $this->scripturl; ?>?action=element&amp;recordid=<?php echo $row['ele_id']; ?>"><?php echo $row['ele_listorder']; ?></a>
        </td>
        <td class="windowbg2_light">
            <a href="<?php echo $this->scripturl; ?>?action=element&amp;recordid=<?php echo $row['ele_id']; ?>"><?php echo $row['ele_descr']; ?></a>
        </td>
        <td class="windowbg2_light" valign="top">
            <a href="<?php echo $this->scripturl; ?>?action=element&amp;recordid=<?php echo $row['ele_id']; ?>"><?php echo $row['ele_code']; ?></a>
        </td>
    </tr>
    <?php endif; ?>
    <?php
    $prev_set = $url_set;
    $prev_domain = $url_domain;
    $prev_standard = $url_standard;
    ?>
    <?php endforeach; ?>
</table>
<form method="POST" name="frmStandards" action="<?php echo $this->scripturl; ?>?action=<?php echo $this->url_action; ?>">
    <input type="hidden" name="expand_action" value="0">
    <input type="hidden" name="url_expand" value="">
    <?php if (is_array($this->url_expanded)) : ?>
        <?php foreach ($this->url_expanded as $expanded) : ?>
            <?php if ($expanded != '') : ?>
            <input type="hidden" name="url_expanded[]" value="<?php echo Escape::EscapeEntities($expanded); ?>">
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</form>