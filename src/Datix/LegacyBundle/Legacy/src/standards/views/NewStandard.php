<form method="post" id="frmStandard" name="frmStandard" action="<?php echo $this->scripturl; ?>?action=savestandard">
    <input type="hidden" name="fromlisting" value="<?php echo Escape::EscapeEntities($this->fromlisting); ?>" />
    <input type="hidden" name="fromsearch" value="<?php echo (is_numeric($this->fromsearch) ? (int) $this->fromsearch : ''); ?>" />
    <input type="hidden" name="from_report" value="<?php echo Escape::EscapeEntities($this->from_report); ?>" />
    <input type="hidden" name="form_type" value="<?php echo $this->FormAction; ?>">
    <input type="hidden" name="recordid" value="<?php echo $this->stn['recordid']; ?>">
    <input type="hidden" id="form-panel" name="panel" value="<?php echo ($this->panel ?: ''); ?>" />
    <input type="hidden" name="updateid" value="<?php echo $this->stn['updateid']; ?>">
    <input type="hidden" name="onwonly" value="<?php echo $this->ownonly; ?>">
    <input type="hidden" name="pending" value="<?php echo $this->pending; ?>">
    <input type="hidden" name="rbWhat" value="Save">
    <?php if ($this->sLockMessage) : ?>
    <div class="lock_message">
        <div id="LockMessage"><?php echo $this->sLockMessage; ?></div>
        <div id="userLockMessage"></div>
    </div>
    <?php endif; ?>
    <?php echo $this->STNTable->GetFormTable(); ?>
    <?php echo $this->buttonGroup->getHTML(); ?>
</form>
<?php if ($this->FormAction != 'Print') : ?>
<?php echo JavascriptPanelSelect(null, $_GET['panel'], $this->STNTable); ?>
<?php endif; ?>