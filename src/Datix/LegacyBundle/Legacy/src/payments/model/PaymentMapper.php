<?php
namespace src\payments\model;

use src\framework\model\DatixEntityMapper;

class PaymentMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\payments\\model\\Payment';
    }

    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    protected function getView()
    {
        return 'vw_payments';
    }

    /**
     * Returns an array of key/value pairs representing the entity's properties.
     *
     * Used when building insert/update statements.
     *
     * @param Entity $object
     *
     * @return array
     */
    protected function getEntityProperties(Entity $object)
    {
        $fields = parent::getEntityProperties($object);

        if($fields['resp_id'] === "") //stored as an INT in the database, but for DW purposes is a coded field. This writes it to the database in the correct format.
        {
            $fields['resp_id'] = NULL;
        }

        return $fields;
    }

}