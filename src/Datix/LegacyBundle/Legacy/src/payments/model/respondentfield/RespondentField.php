<?php
namespace src\payments\model\respondentfield;

use src\payments\model\respondentfield\RespondentFieldCodeModelFactory;
use src\system\database\field\CodeField;
use src\framework\query\Query;

/**
 * Displays a list of fields used when designing a report.
 */
class RespondentField extends CodeField
{
    /**
     * {@inherit}
     */
    protected function doGetCodes(Query $query = null)
    {
        // TODO this is a straightforward-enough case that we don't need a separate mapper - we just need to define a RespondentField::applyFormQuery() method
        //      to pay attention to the cla_id on the form and use it to filter respondent codes.  Can do this when refactoring away from SelectFunctions.php.
        $this->codes = (new RespondentFieldCodeModelFactory($this))->getCollection();
        $query = $query ?: new Query();
        $this->codes->setQuery($query);
    }
}