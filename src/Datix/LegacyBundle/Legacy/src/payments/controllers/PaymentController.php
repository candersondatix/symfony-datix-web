<?php
namespace src\payments\controllers;

use src\framework\controller\Controller;
use src\framework\query\Query;
use src\payments\model\PaymentModelFactory;
use src\payments\model\Payment;

class PaymentController extends Controller
{
    public function listPaymentsForClaim()
    {
        $link_recordid = (int) $this->request->getParameter('recordid');
        $query = new Query();

        $query->where(array('vw_payments.cla_id' => $link_recordid));

        if (CanSeeModule('PAY'))
        {
            $payments = (new PaymentModelFactory)->getCollection();
            $payments->setQuery($query);
        }
        else
        {
            $payments = [];
        }

        $recordList = new \RecordLists_ModuleRecordList();
        $recordList->Module = 'PAY';
        $recordList->AddRecordData($payments);

        $listingDesign = new \Listings_ModuleListingDesign(array('module' => 'PAY', 'parent_module' => 'CLA', "link_type" => "payments"));
        $listingDesign->LoadColumnsFromDB();

        $OrderCol = new \Listings_ListingColumn('pay_date', 'vw_payments');
        $OrderCol->setDescending();

        $recordList->OrderBy = array($OrderCol, new \Listings_ListingColumn('recordid', 'vw_payments'));

        // Remove extra field columns
        foreach ($listingDesign->Columns as $key => $Column)
        {
            if ($Column instanceof \Listings_UDFListingColumn)
            {
                preg_match('/^UDF.*_([0-9]+)$/ui', $Column->getName(), $matches);
                $extraFieldColumns[$matches[1]] = $Column;
                $extrafieldIds[] = $matches[1];
            }
        }

        if ($extraFieldColumns)
        {
            foreach ($recordList->Records as $payment)
            {
                $recordids[] = $payment->Data->recordid;
            }

            $recordList->udfData = getUDFData('PAY', $recordids, $extrafieldIds);
        }


        foreach ($recordList->Records as $record)
        {
            foreach (array('pay_calc_total','pay_calc_vat_amount','pay_amount') as $field)
            {
                if ($record->Data->pay_type == 'RECE')
                {
                    $record->Data->pay_calc_total = abs($record->Data->pay_calc_total);
                }
            }
        }

        $ReadOnly = false;
        $AccessLevel = GetAccessLevel('PAY');

        if ($AccessLevel != 'PAY_FULL')
        {
            $ReadOnly = true;
        }

        $Listing = new \Listings_ListingDisplay($recordList, $listingDesign);
        $Listing->EmptyMessage = 'No payments.';

        $this->response->build('src/payments/views/ListPaymentsForClaim.php',
            array('ReadOnly' => $ReadOnly, 'Listing' => $Listing, 'recordid' => $link_recordid));

    }

    /**
     * Displays the section "Payments" inside an incident or a complaint record.
     */
    public function listPayments()
    {
        global $ModuleDefs, $scripturl;

        $extraFieldColumns = [];
        $recordids         = [];
        $extrafieldIds     = [];
        $Data              = $this->request->getParameter('data');
        $FormType          = $this->request->getParameter('FormType');
        $Module            = $this->request->getParameter('module');

        $Design = new \Listings_ModuleListingDesign([
            'module' => 'PAY',
            'parent_module' => $Module,
            'link_type' => 'payments'
        ]);
        $Design->LoadColumnsFromDB();

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'PAY';
        $RecordList->Columns = $Design->Columns;

        $RecordList->Columns[] = new \Listings_ListingColumn($ModuleDefs['PAY']['LINKED_MODULE']['parent_ids'][$Module], 'vw_payments');

        // Remove extra field columns
        foreach ($RecordList->Columns as $key => $Column)
        {
            if ($Column instanceof \Listings_UDFListingColumn)
            {
                preg_match('/^UDF.*_([0-9]+)$/ui', $Column->getName(), $matches);
                $extraFieldColumns[$matches[1]] = $Column;
                $extrafieldIds[] = $matches[1];
                unset($RecordList->Columns[$key]);
            }
        }

        $RecordList->Paging = false;

        $whereClause = '1=2';

        // Make sure that only users with access to Payments module can see payments
        if (CanSeeModule('PAY'))
        {
            $whereClause = MakeSecurityWhereClause($ModuleDefs['PAY']['LINKED_MODULE']['parent_ids'][$Module].' = '.$Data['recordid'], 'PAY');
        }

        $RecordList->WhereClause = $whereClause;

        $OrderCol = new \Listings_ListingColumn('pay_date', 'vw_payments');
        $OrderCol->setDescending();

        $RecordList->OrderBy = array($OrderCol, new \Listings_ListingColumn('recordid', 'vw_payments'));
        $RecordList->RetrieveRecords();

        if ($extraFieldColumns)
        {
            foreach ($RecordList->Records as $record)
            {
                $recordids[] = $record->Data['recordid'];
            }

            $udfData = getUDFData('PAY', $recordids, $extrafieldIds);

            // Add extra field columns and data
            foreach ($extraFieldColumns as $udfId => $Column)
            {
                $RecordList->Columns[] = $Column;

                foreach ($RecordList->Records as $key => $record)
                {
                    $RecordList->Records[$key]->Data[$Column->getName()] = $udfData[$record->Data['recordid']][$udfId];
                }
            }
        }

        // transform negative values stored for receipts
        foreach ($RecordList->Records as $record)
        {
            foreach (array('pay_calc_total','pay_calc_vat_amount','pay_amount') as $field)
            {
                if (isset($record->Data[$field]) && $record->Data['pay_type'] == 'RECE')
                {
                    $record->Data[$field] = abs($record->Data[$field]);
                }
            }
        }

        $ReadOnly = false;
        $AccessLevel = GetAccessLevel('PAY');

        if ($AccessLevel != 'PAY_FULL' || $FormType == 'ReadOnly')
        {
            $ReadOnly = true;
        }

        $Listing = new \Listings_ListingDisplay($RecordList, $Design, $Module, $ReadOnly);
        $Listing->EmptyMessage = 'No payments.';

        $this->response->build('src\complaints\views\ListPayments.php', [
            'Listing'   => $Listing,
            'ReadOnly'  => $ReadOnly,
            'scripturl' => $scripturl,
            'Module'    => $Module,
            'recordId'  => $Data['recordid']
        ]);
    }
}