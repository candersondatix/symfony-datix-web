<?php

namespace src\riskregister\model\field;

use src\system\database\field\CodeField;

class RiskLevelField extends CodeField
{
    /**
     * {@inherit}
     */
    public function getCodeTable()
    {
        return $this->registry->getParm('RISK_MATRIX', 'N', false, true) ? 'code_inc_grades' : 'code_ra_levels';
    }
}