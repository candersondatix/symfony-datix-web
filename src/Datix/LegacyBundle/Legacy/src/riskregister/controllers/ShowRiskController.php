<?php

namespace src\riskregister\controllers;

use src\framework\controller\Controller;

class ShowRiskController extends Controller
{
    /**
     * @desc Shows a risk from the main table (ra_main) and displays it using the RISK2 form. Outputs HTML.
     *
     * @param int $recordid The id of the record to show in ra_main
     */
    function risk()
    {
        global $ram, $FormArray;

        $recordid = $this->request->getParameter('recordid');

        if (!($this->request->getParameter('submitandprint') && HashesMatch('RAM', $recordid)))
        {
            LoggedIn();
        }
        else
        {
            $Level1Review = true;
            $FormType = 'Print';
        }
        
        if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')))
        {
        	require_once 'Source/security/SecurityBase.php';
        	$recordid = (int) $this->request->getParameter('recordid');
        	$RISKPerms = GetUserHighestAccessLvlForRecord('RISK_PERMS', 'RAM', $recordid);
        }
        else
        {
        	$RISKPerms = GetParm('RISK_PERMS');
        }
        
        $RiskForm = GetParm('RISK_FORM');

        $ShowRISK1Values = $this->request->getParameter('show_risk1_values');

        $SelectString = '
            SELECT
                recordid, updateid, updateddate,
                ram_name, ram_risk_type, ram_risk_subtype,
                ram_organisation, ram_unit, ram_clingroup, ram_directorate,
                ram_specialty, ram_assurance, ram_location, ram_locactual,
                ram_objectives, ram_responsible, ram_description,
                ram_synopsis, ram_consequence, ram_likelihood,
                ram_rating, ram_cur_rating, ram_after_rating, ram_level, ram_after_conseq, ram_after_likeli,
                ram_after_level, ram_cur_conseq, ram_cur_likeli, ram_cur_level,
                ram_cur_cost, ram_dreview, ram_ourref, ram_dcreated, ram_dclosed,
                updatedby, ram_handler, rep_approved, ram_adeq_controls, show_document, ram_last_updated
        ';

        if (!$Level1Review)
        {
            $RiskWhere[] = "recordid = $recordid";

            $WhereClause = MakeSecurityWhereClause($RiskWhere, 'RAM', $_SESSION['initials']);
        }
        else
        {
            $WhereClause = "recordid = $recordid";
        }

        $sql = "$SelectString FROM ra_main WHERE $WhereClause";

        $ram = \DatixDBQuery::PDO_fetch($sql, array());

        // Access based on DB table link_access_approvalstatus
        $AccessFlag = '';
        $AccessFlag = GetAccessFlag('RAM', $RISKPerms, $ram['rep_approved']);

        if ($AccessFlag == 'R')
        {
            $form_action = "ReadOnly";
        }

        if (!$ram || ((!$RISKPerms || $AccessFlag == '') && !$Level1Review))
        {
            CheckRecordNotFound(array('module' => 'RAM', 'recordid' => $recordid));
        }

        $sql = '
            SELECT
                fullname
            FROM
                staff
            WHERE
                initials = :initials
        ';

        $row = \DatixDBQuery::PDO_fetch($sql, array('initials' => $ram['ram_handler']));

        $ram['ram_mgr_fullname'] = $row['fullname'];

        // Check if we need to show original values from RISK1
        if ($ram && $ShowRISK1Values)
        {
            $sql = "$SelectString FROM aud_ra_report WHERE main_recordid = $recordid and submit_stage = 2 and rep_approved = 'N'";
            $OldValues = \DatixDBQuery::PDO_fetch($sql, array());

            if ($OldValues)
            {
                $ram['old_values'] = $OldValues;
            }
        }

        if ($Level1Review)
        {
            $FormLevel = 1;
        }
        elseif ($ram['rep_approved'])
        {
            $FormLevel = GetFormLevel('RAM', $RISKPerms, $ram['rep_approved']);
        }

        $ram['con'] = GetLinkedContacts(array('recordid' => $recordid, 'module' => 'RAM', 'formlevel' => $FormLevel));

        // Check if we need to show full audit trail
        if ($ram && $this->request->getParameter('full_audit'))
        {
            $FullAudit = GetFullAudit(array('Module' => 'RAM', 'recordid' => $recordid));

            if ($FullAudit)
            {
                $ram['full_audit'] = $FullAudit;
            }
        }

        // Retrieve notepad
        $sql = '
            SELECT
                notes
            FROM
                notepad
            WHERE
                ram_id = :ram_id
        ';

        $row = \DatixDBQuery::PDO_fetch($sql, array('ram_id' => $recordid));

        if ($row)
        {
            $ram['notes'] = $row['notes'];
        }

        // Get Assurance stuff
        $sql = "
            SELECT
                recordid,
                ass_link_id,
                ass_text,
                ass_type,
                ass_listorder,
                ass_module,
                ass_source,
                updateid
            FROM
                assurance
            WHERE
                ass_module = 'RAM'
                AND
                ass_link_id = :link_id
            ORDER BY
                ass_listorder ASC
        ";

        $Assurances = \DatixDBQuery::PDO_fetch_all($sql, array('link_id' => $recordid));

        foreach ($Assurances as $ass1)
        {
            $ass_row[$ass1["ass_type"]][] = $ass1;
        }

        $ram['assurance'] = $ass_row;

        if ($FormLevel == 1)
        {
            // Need to populate show_xxx fields
            $ram = PopulateLevel1FormFields(array('module' => 'RAM', 'data' => $ram));
        }

        if ($Level1Review)
        {
            $ram['temp_record'] = true;
        }

        AuditOpenRecord('RAM', $recordid, '');

        $this->call('src\riskregister\controllers\ShowRiskFormTemplateController', 'showriskform', array(
            'form_action' => $form_action,
            'level' => $FormLevel,
            'data' => $ram
        ));
    }
}