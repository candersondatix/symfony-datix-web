<?php

namespace src\riskregister\controllers;

use src\framework\controller\Controller;
use src\reports\exceptions\ReportException;

class RisksDoSelectionController extends Controller
{
    function risksdoselection()
    {
        global $db_affected_rows, $scripturl, $report_saved;

        $FormAction = $this->request->getParameter('rbWhat');
        $FieldWhere = '';
        $Error = false;
        $search_where = array();
        ClearSearchSession('RAM');

        if ($FormAction == 'Cancel' || $FormAction == BTN_CANCEL)
        {
            if (isset($_SESSION['security_group']['grp_id']))
            {
                $_SESSION['security_group']['success'] = false;
                $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                    'recordid' => $_SESSION['security_group']['grp_id'],
                    'module' => $_SESSION['security_group']['module']
                ));
                return;
            }
            elseif (isset($_SESSION['packaged_report']))
            {
                $RedirectUrl = '?action=designapackagedreport';

                if ($_SESSION['packaged_report']['recordid'])
                {
                    $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
                }
                else
                {
                    $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
                }

                $_SESSION['packaged_report']['success'] = false;
            }
            elseif ($this->request->getParameter('qbe_recordid') != '')
            {
                // query by example for generic modules
                $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module') .
                    '&recordid=' . $this->request->getParameter('qbe_recordid');
            }
            else
            {
                $RedirectUrl = '?module=RAM';
            }

            $this->redirect('app.php' . $RedirectUrl);
        }

        // construct and cache a new Where object
        try
        {
            $whereFactory = new \src\framework\query\WhereFactory();
            $where = $whereFactory->createFromRequest($this->request);

            if (empty($where->getCriteria()['parameters'])) {
                $where = null;
            }

            $_SESSION['RAM']['NEW_WHERE'] = $where;
        }
        catch (\InvalidDataException $e)
        {
            $Error = true;
            $this->registry->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');
        }
        catch(ReportException $e)
        {
            $Error = true;
        }

        // Find all values which start with ram_ and add them to array
        // if they are not empty.
        // To do: parse names.
        $ram = \Sanitize::SanitizeRawArray($this->request->getParameters());
        $_SESSION['RAM']['LAST_SEARCH'] = $ram;

        $table = 'ra_main';

        while (list($name, $value) = each($ram))
        {
            $explodeArray = explode('_', $name);

            if ($explodeArray[0] == 'UDF')
            {
                if (MakeUDFFieldWhere($explodeArray, $value, MOD_RISKREGISTER, $FieldWhere) === false)
                {
                    $Error = true;
                    break;
                }
            }
            elseif ($explodeArray[0] == 'searchtaggroup')
            {
                // @prompt on tags is not yet supported
                if (preg_match('/@prompt/iu',$value) == 1)
                {
                    $search_where[] = '1=2';
                }
                elseif ($value != '')
                {
                    $field = str_replace('searchtaggroup_'.$explodeArray[1].'_', '', $name);

                    // Need to find the codes associated with the selected tags.
                    $sql = '
                        SELECT DISTINCT
                            code
                        FROM
                            code_tag_links
                        WHERE
                            [group] = :group
                            AND
                            [table] = :table
                            AND
                            field = :field
                            AND
                            tag IN (\''.implode('\',\'', array_map('intval', explode('|', $value))).'\')
                    ';

                    $Codes = \DatixDBQuery::PDO_fetch_all($sql, array('group' => intval($explodeArray[1]), 'field' => $field, 'table' => 'ra_main'), \PDO::FETCH_COLUMN);

                    if (!empty($Codes))
                    {
                        $search_where[] = $field.' IN (\''.implode('\',\'', $Codes).'\')';
                    }
                    else
                    {
                        // This tag hasn't been attached to any codes, so can't return any results.
                        $search_where[] = '1=2';
                    }
                }
            }
            else
            {
                if (MakeFieldWhere('RAM', $name, $value, $FieldWhere, $table) === false)
                {
                    $Error = true;
                    break;
                }
            }

            if ($FieldWhere != '')
            {
                if (CheckForCorrectAtCodes($FieldWhere))
                {
                    $search_where[$name] = $FieldWhere;
                }
                else
                {
                    AddMangledSearchError($name, $value);
                    $Error = true;
                }
            }
        }

        if (!$Error && !ValidateWhereClause(implode(' AND ', $search_where), 'RAM'))
        {
            $Error = true;
        }

        if ($Error === true)
        {
            AddSessionMessage('ERROR', _tk('search_errors'));
            if ($this->request->getParameter('udfRowArray'))
            {
                $_SESSION['RAM']['UDFARRAY'] = $this->request->getParameter('udfRowArray');
            }

            $this->call('src\riskregister\controllers\RiskSearchController', 'risksearch', array(
                'searchtype' => 'lastsearch'
            ));
            return;
        }

        $_SESSION['RAM']['UDFARRAY'] = NULL;

        $Where = '';

        if (empty($search_where) === false)
        {
            $Where = implode(' AND ', $search_where);
        }

        $_SESSION['RAM']['WHERE'] = $Where;
        $_SESSION['RAM']['TABLES'] = $table;

        // We are doing a new search so unset LASTQUERYID
        unset($_SESSION['RAM']['LASTQUERYID']);

        if (isset($_SESSION['security_group']['grp_id']))
        {
            $_SESSION['security_group']['success'] = true;
            $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                'grp_id' => $_SESSION['security_group']['grp_id'],
                'module' => $_SESSION['security_group']['module']
            ));
            return;
        }
        elseif (isset($_SESSION['packaged_report']))
        {
            $RedirectUrl = '?action=designapackagedreport';

            if ($_SESSION['packaged_report']['recordid'])
            {
                $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
            }
            else
            {
                $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
            }

            $_SESSION['packaged_report']['success'] = true;
        }
        elseif ($this->request->getParameter('qbe_recordid') != '')
        {
            // query by example for generic modules
            $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module') .
                '&recordid=' . $this->request->getParameter('qbe_recordid') . '&qbe_success=true';
        }
        else
        {
            $RedirectUrl = '?action=list&module=RAM&listtype=search';
            $_SESSION['RAM']['SEARCHLISTURL'] = $scripturl . $RedirectUrl;
        }

        $this->redirect('app.php' . $RedirectUrl);
    }
}