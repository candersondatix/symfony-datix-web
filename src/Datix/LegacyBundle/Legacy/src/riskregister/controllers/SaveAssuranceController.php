<?php

namespace src\riskregister\controllers;

use src\framework\controller\Controller;

class SaveAssuranceController extends Controller
{
    function saveassurance()
    {
        global $RISKPerms;

        session_start();

        $form_action = $this->request->getParameter('rbWhat');
        $ram_new = $this->request->getParameters();
        $RISKPerms = GetParm('RISK_PERMS');

        switch ($form_action)
        {
            case 'Cancel':
                $RedirectUrl = '?module=RAM';

                if ($this->request->getParameter('form_id'))
                {
                    $RedirectUrl .= '&form_id=' . $this->request->getParameter('form_id');
                }

                $this->redirect('app.php' . $RedirectUrl);
            case 'SaveChanges':
                require_once 'Source/risks/SaveAssurance.php';
                SavePrincipalObjectives($ram_new);
                break;
            case "ApplyChanges":
                require_once 'Source/risks/SaveAssurance.php';
                ApplyAssuranceItems($ram_new);
                break;

        }
    }
}