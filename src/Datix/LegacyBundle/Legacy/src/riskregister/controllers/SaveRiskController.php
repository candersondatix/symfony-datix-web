<?php

namespace src\riskregister\controllers;

use src\documents\controllers\DocumentController;
use src\framework\controller\Controller;

class SaveRiskController extends Controller
{
    function saverisk()
    {
        global $scripturl, $yySetLocation, $RISKPerms, $SectionVisibility, $ButtonDefs, $ModuleDefs,
               $AccessLvlDefs;

        session_start();

        $RequestParameters = $this->request->getParameters();

        if (!is_array($RequestParameters) || empty($RequestParameters))
        {
            $form_action = 'Cancel';
        }
        else
        {
            $form_action = $this->request->getParameter('rbWhat');
        }

        if (bYN(GetParm('RECORD_LOCKING', 'N')) &&
            ($this->request->getParameter('recordid') && $this->request->getParameter('assurance_framework_clicked') != '1'))
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array(
                'table' => 'ra_main',
                'link_id' => $this->request->getParameter('recordid')
            ));
        }

        switch ($form_action)
        {
            case 'Cancel':
            case BTN_CANCEL:
                if ($this->request->getParameter('fromsearch'))
                {
                    $RedirectUrl = '?action=list&module=RAM&listtype=search';
                }
                elseif ($this->request->getParameter('from_report'))
                {
                	$RedirectUrl = '?action=list&module=RAM&listtype=search&from_report=1';
                }
                else
                {
                    $RedirectUrl = '?module=RAM';

                    if ($this->request->getParameter('form_id'))
                    {
                        $RedirectUrl .= '&form_id=' . $this->request->getParameter('form_id');
                    }
                }

                $this->redirect('app.php' . $RedirectUrl);
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');

                break;
            case 'ShowRISK1Values':
                $Parameters = array(
                    'recordid' => $this->request->getParameter('recordid'),
                    'show_risk1_values' => 1
                );
                $this->call('src\riskregister\controllers\ShowRiskController', 'risk', $Parameters);
                return;
            case 'ShowAudit':
                $Parameters = array(
                    'recordid' => $this->request->getParameter('recordid'),
                    'full_audit' => 1
                );
                $this->call('src\riskregister\controllers\ShowRiskController', 'risk', $Parameters);
                return;
            case 'Search':
                $this->call('src\riskregister\controllers\RisksDoSelectionController', 'risksdoselection', array());
                return;
                break;
        }

        $RISKPerms = GetParm('RISK_PERMS');

        if (($this->request->getParameter('session_date') != $_SESSION['lastsession'][$this->request->getParameter('session_form')]) &&
            !empty($_SESSION['lastsession']))
        {
            if ($_SESSION['logged_in'])
            {
                if ($RISKPerms == 'RISK1')
                {
                    $this->call('src\riskregister\controllers\NewRisk1Controller', 'newrisk1', array());
                    return;
                }
                else
                {
                    $this->call('src\riskregister\controllers\NewRisk2Controller', 'newrisk2', array());
                    return;
                }
            }
            else
            {
                $RedirectUrl = '?module=RAM';

                if ($this->request->getParameter('form_id'))
                {
                    $RedirectUrl .= '&form_id=' . $this->request->getParameter('form_id');
                }

                $this->redirect('app.php' . $RedirectUrl);
            }
        }

        $HoldingForm = ($this->request->getParameter('holding_form') == 1);
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        $approved = GetValidApprovalStatusValueFromPOST(array('module' => 'RAM'));

        require_once 'Source/risks/SaveRisk.php';
        $error = ValidateRiskData();

        // Documents linked on RAM1
        if ($this->request->getParameter('max_doc_suffix'))
        {
            $DocError = DocumentController::ValidateLinkedDocumentData($RequestParameters);

            foreach($DocError as $DocValErrorField => $DocValErrorMessage)
            {
                AddValidationMessage($DocValErrorField, $DocValErrorMessage);
            }
        }

        if ($error || $DocError)
        {
            AddSessionMessage('ERROR', _tk('form_errors'));
            $ram = $this->request->getParameters();
            $ram['error'] = $error;
            $ram['rep_approved'] = $approved;
            $ram['submit_stage'] = $submit_stage;

            // Workaround to convert dates to SQL format
            // TODO: This needs to be removed when refactored
            $RiskDateFields = array_merge(GetAllFieldsByType('RAM', 'date'), getAllUdfFieldsByType('D', '', $ram));

            foreach ($RiskDateFields as $RiskDate)
            {
                if (array_key_exists($RiskDate, $ram) && $ram[$RiskDate] != '')
                {
                    $ram[$RiskDate] = UserDateToSQLDate($ram[$RiskDate]);

                    // Some extra field dates look directly at the post value, so need to blank it out here.
                    $this->request->unSetParameter($RiskDate);
                    unset($_POST[$RiskDate]);
                }
            }

            $this->call('src\riskregister\controllers\ShowRiskFormTemplateController', 'showriskform', array(
                'form_action' => $form_action,
                'level' => $this->request->getParameter('formlevel'),
                'data' => $ram
            ));
            return;
        }

        // Sections hidden on the submitted form should not store their values in the database. We need
        // to blank out any POST values from non-visible sections.
        GetSectionVisibility('RAM', ($HoldingForm? 1 : 2), $RequestParameters);
        BlankOutPostValues('RAM', ($HoldingForm? 1 : 2), null, null, $this->request);

        if (!$recordid)
        {
            $newRecord = true;
            $recordid = GetNextRecordID('ra_main', true, 'recordid', $_SESSION['initials']);
            ResetFormSession();
            $this->request->setParameter('ram_dreported', date('d-M-Y H:i:s'));

            if (!$_SESSION['logged_in'])
            {
                $_SESSION['RAM']['LOGGEDOUTRECORDHASH'] = getRecordHash($recordid);
            }
        }
        else
        {
            $newRecord = false;

            // If either consequece or likelihood have changed, we need to log both, so that the audit doesn't display <no value>
            if ($this->request->getParameter('CHANGED-ram_likelihood') || $this->request->getParameter('CHANGED-ram_consequence'))
            {
                $this->request->setParameter('CHANGED-ram_likelihood', 1);
                $this->request->setParameter('CHANGED-ram_consequence', 1);
            }

            if ($this->request->getParameter('CHANGED-ram_cur_likeli') || $this->request->getParameter('CHANGED-ram_cur_conseq'))
            {
                $this->request->setParameter('CHANGED-ram_cur_likeli', 1);
                $this->request->setParameter('CHANGED-ram_cur_conseq', 1);
            }

            if ($this->request->getParameter('CHANGED-ram_after_likeli') || $this->request->getParameter('CHANGED-ram_after_conseq'))
            {
                $this->request->setParameter('CHANGED-ram_after_likeli', 1);
                $this->request->setParameter('CHANGED-ram_after_conseq', 1);
            }

            DoFullAudit('RAM', 'ra_main', $recordid);
            AuditRating('I', $recordid);
            AuditRating('C', $recordid);
            AuditRating('T', $recordid);
        }

        if ($this->request->getParameter('ram_handler') == '' && $RISKPerms != 'RISK1')
        {
            $this->request->setParameter('ram_handler', $_SESSION['initials']);
        }

        $ram = \Sanitize::SanitizeRawArray($RequestParameters);

        // Cater for the 5 + 5 + 5 risk matrix if necessary
        if (bYN(GetParm('RAM_RATE_CONTROLS', 'N')))
        {
            $RateControlsArray = CalcRatingControlsValues($ram['ram_cur_conseq'], $ram['ram_cur_likeli'], $ram['ram_adeq_controls']);
            $ram['ram_cur_rating'] = $RateControlsArray['ram_cur_rating'];
            $ram['ram_cur_level'] = $RateControlsArray['ram_cur_level'];
        }

        $ram['rep_approved'] = $approved;

        $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'RAM', 'level' => ($HoldingForm ? 1 : 2)));
        $FormDesign->LoadFormDesignIntoGlobals();

        $ram = ParseSaveData(array('module' => 'RAM', 'data' => $ram));

        $sql = "UPDATE ra_main SET ";

        $sql .= GeneratePDOSQLFromArrays(array(
                'FieldArray' => getRiskFieldArray(),
                'DataArray' => $ram,
                'end_comma' => true,
                'Module' => 'RAM'
            ),
            $PDOParamsArray
        );

        if (!$this->request->getParameter('recordid'))
        {
            $sql .= " submit_login = '" . $_SESSION['login'] . "', ";
        }

        $sql .= " updatedby = '" . $_SESSION['initials'] . "',
        updateid = '" . GensUpdateID($this->request->getParameter('updateid')) . "',
        updateddate = '" . date('d-M-Y H:i:s') . "'";

        $sql .= " WHERE recordid = :recordid and (updateid = :updateid OR updateid IS NULL)";

        $PDOParamsArray['recordid'] = $recordid;
        $PDOParamsArray['updateid'] = $this->request->getParameter('updateid');
        $SaveRiskQuery = new \DatixDBQuery($sql);
        $result = $SaveRiskQuery->prepareAndExecute($PDOParamsArray);

        if (!$result)
        {
            $error = "An error has occurred when trying to save the risk.  Please report the following to the Datix administrator: $sql";
        }
        elseif ($SaveRiskQuery->PDOStatement->rowCount() == 0)
        {
            $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=risk&module=RAM&recordid='.$recordid.'">here</a> to return to the record';
        }

        if ($error != '')
        {
            SaveError($error);
        }

        $ram['recordid'] = $recordid;

        // Save UDFs
        require_once 'Source/libs/UDF.php';
        SaveUDFs($recordid, MOD_RISKREGISTER);

        $_SESSION['risk_number'] = "$recordid";

        // Save notepad
        require_once 'Source/libs/notepad.php';
        $error = SaveNotes(array('id_field' => 'ram_id', 'id' => $recordid, 'notes' => $ram['notes'], 'new' => $newRecord));

        // Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => 'RAM',
            'data'   => $ram
        ));

        // Save Rejection Notes
        if (bYN(GetParm('REJECT_REASON', 'Y')) && $ram['rep_approved'] == 'REJECT')
        {
            $this->call('src\reasons\controllers\ReasonsController', 'SaveReason', array(
                'module'  => 'RAM',
                'link_id' => $recordid,
                'data'    => $ram,
            ));
        }

        // Save contacts attached on RISK1.
        require_once 'Source/contacts/SaveContact.php';
        SaveContacts(array('main_recordid' => $recordid, 'module' => 'RAM', 'formlevel' => ($HoldingForm? 1 : 2)));

        if (ModIsLicensed('HOT'))
        {
            require_once 'Source/generic_modules/HOT/ModuleFunctions.php';
            InsertIntoHotspotQueue('RAM', $recordid);
        }

        // Check to see if we need to add any documents
        if ($SectionVisibility['extra_document'])
        {
            DocumentController::SaveDocsFromLevel1($recordid, 'RAM');
        }

        // e-mail the handler of the risk when:
        // - submitting a risk without logging in
        // - submitting a risk by a logged in RISK1 user
        // $approved is hardcoded, could be set in 'email sending' or 'module' settings or similar
        if (bYN(GetParm('RAM_SHOW_EMAIL','N')) && $HoldingForm)
        {
            require_once 'Source/libs/Email.php';

            $this->request->setParameter('recordid', $recordid);
            $Output = SendEmails(array(
                'module' => 'RAM',
                'data'=> $ram,
                'progressbar' => $Progress,
                'from' => $ram['rep_approved_old'],
                'to' => $ram['rep_approved'],
                'perms' => ($RISKPerms? $RISKPerms: 'NONE'),
                'level' => $this->request->getParameter('formlevel')
            ));
        }
        else
        {
            if ($Progress)
            {
                $Progress->IncrProgressBar($progress_increment);
                echo '<script>document.getElementById("waiting").style.display="none";</script>';
                ob_flush();
                flush();
            }
        }

        if ($form_action == 'Document') // redirect back to the documents page
        {
            $this->redirect($this->request->getParameter('document_url') . 'Main');
        }

        $UnapprovedContactArray = GetUnapprovedContacts(array('module' => 'RAM', 'recordid' => $recordid));

        if (is_array($UnapprovedContactArray))
        {
            IncludeCurrentFormDesign('RAM', 2);

            foreach ($UnapprovedContactArray as $key => $ContactDetails)
            {
                if ($GLOBALS['HideFields']['contacts_type_'.$ContactDetails['link_type']])
                {
                    unset($UnapprovedContactArray[$key]);
                }
            }
        }

        // This was moved here because when we have unapproved contacts in a record the actions chains were not being attached
        if (bYN(GetParm('WEB_TRIGGERS', 'N')))
        {
            require_once 'Source/libs/Triggers.php';
            ExecuteTriggers($recordid, $ram, $ModuleDefs['RAM']['MOD_ID']);
        }

        if ($form_action == 'RecordPermissions')
        {
            if ($this->request->getParameter('record_permissions_url'))
            {
                $yySetLocation = $this->request->getParameter('record_permissions_url');
            }
        }
        elseif (CheckUnapprovedContactRedirect(array('from' => $this->request->getParameter('rep_approved_old'), 'to' => $this->request->getParameter('rep_approved'), 'module' => 'RAM', 'access_level' => $RISKPerms))
            && count($UnapprovedContactArray) > 0) // Warn for unapproved contacts
        {
            DoUnapprovedContactRedirect(array('module' => 'RAM', 'recordid' => $recordid, 'contact_array' => $UnapprovedContactArray));
        }
        else
        {
            $this->call('src\generic\controllers\SaveRecordTemplateController', 'ShowSaveRecord', array(
                'aParams' => array(
                    'module' => 'RAM',
                    'data' => $ram,
                    'message' => $Output,
                    'form_id' => $_REQUEST['form_id']
                )
            ));
        }
    }
}