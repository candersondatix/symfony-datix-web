<?php

namespace src\riskregister\controllers;

use src\framework\controller\TemplateController;

class AssuranceListController extends TemplateController
{
    function assurancelist()
    {
        $this->module = 'RAM';
        $this->title = 'Risks - Assurance Framework listing';

        require_once 'Source/reporting/CustomListingReports.php';

        ob_start();
        call_user_func('report4', "rep_approved like 'FA'", true);
        $reportContent = ob_get_contents();
        ob_end_clean();

        $this->response->setBody($reportContent);
        
        return $this->response;

    }
}