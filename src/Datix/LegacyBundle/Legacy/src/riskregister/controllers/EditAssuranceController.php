<?php

namespace src\riskregister\controllers;

use src\framework\controller\Controller;

class EditAssuranceController extends Controller
{
    function editassurance()
    {
        $RISKPerms = GetParm('RISK_PERMS');
        $print = ($this->request->getParameter('print') == 1);

        if ($this->request->getParameter('ram_id'))
        {
            $sql = '
                SELECT
                    recordid,
                    ass_link_id,
                    ass_text,
                    ass_type,
                    ass_listorder,
                    ass_module,
                    ass_source,
                    updateid
                FROM
                    assurance
                WHERE
                    ass_module = \'RAM\'
                    AND
                    ass_link_id = :ass_link_id
                    AND
                    ass_type = :ass_type
                ORDER BY ass_listorder asc
            ';

            $ass_result = \DatixDBQuery::PDO_fetch_all($sql, array(
                'ass_link_id' => $this->request->getParameter('ram_id'),
                'ass_type' => $this->request->getParameter('type')
            ));

            foreach ($ass_result as $ass1)
            {
                $ass_row[$ass1['ass_type']][] = $ass1;
            }

            $sql_ram = '
                SELECT
                    ram_objectives
                FROM
                    ra_main
                WHERE
                    recordid = :ram_id
            ';

            $ass2 = \DatixDBQuery::PDO_fetch($sql_ram, array('ram_id' => $this->request->getParameter('ram_id')));
            $ass_row['ram_obj'] = $ass2['ram_objectives'];
        }

        require_once 'Source/risks/AssuranceForm.php';
        ShowAssuranceForm($ass_row);
    }
}