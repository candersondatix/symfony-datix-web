<?php 

namespace src\framework\validate;


class Date
{
    /* *
     * Date format string
     * 
     * @var string
     */
    protected $_format;

    /* *
     * Constructor
     * 
     * @param String|$format
     */
    public function __construct($format = null)
    {
        $this->_format = $format;
    }

    /* *
     * Set date format string
     * 
     * @param String|$format
     * @return Date
     */
    public function setFormat($format)
    {
        $this->_format = $format;
        return $this;
    }

    /* *
     * Get date format string
     * 
     * @return String
     */
    public function getFormat()
    {
        if(null === $this->_format)
            $this->_format = 'Y-m-d';

        return $this->_format;
    }

    /* *
     * Validate value
     * 
     * @param String|$value
     * @return bool
     */
    public function isValid($value)
    {
        if(is_array($value))
            return false;

        if(null === $this->_format) 
        {
            $this->_format = $this->getFormat();
        }

        $date = \DateTime::createFromFormat($this->_format, $value);
        return $date && $date->format($this->_format) == $value;
    }
}
