<?php
namespace src\framework\registry;

use src\memorycache\DatixSessionCache;
use src\framework\controller\helper\DeviceDetect;
use src\system\database\fielddef\FieldDefModelFactory;
use src\system\database\table\TableModelFactory;
use src\system\database\combolink\ComboLinkModelFactory;
use src\framework\query\QueryFactory;
use src\logger\DatixLogger;
use src\framework\query\Query;

/**
 * Allows the definition and retrieval of global application configuration information.
 */
class Registry
{
    /**
     * The Singleton instance.
     * 
     * @var Registry
     */
    private static $instance;
    
    /**
     * The values stored in this registry.
     * 
     * @var array
     */
    protected $values;
    
    private function __construct()
    {
        $this->values = array();
    }
    
    /**
     * Gets the Singleton instance.
     * 
     * @return Registry
     */
    public static function getInstance()
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }
        return static::$instance;
    }
    
    /**
     * Gets an item from the registry.
     * 
     * @param string $key
     * 
     * @return mixed
     */
    protected function get($key)
    {
        return $this->values[$key];
    }
    
    /**
     * Sets an item in the registry.
     * 
     * @param string $key
     * @param mixed  $val
     */
    protected function set($key, $val)
    {
        $this->values[$key] = $val;
    }
    
    /**
     * Getter for field definitions.
     * 
     * @return FieldDefCollection
     */
    public function getFieldDefs()
    {
        if ($this->get('FieldDefs') === null)
        {
            $factory = new FieldDefModelFactory();
            $this->set('FieldDefs', $factory->getCollection());
        }
        return $this->get('FieldDefs');
    }
    
    /**
     * Getter for table definitions.
     * 
     * @return TableDefCollection
     */
    public function getTableDefs()
    {
        if ($this->get('TableDefs') === null)
        {
            $factory = new TableModelFactory();
            $this->set('TableDefs', $factory->getCollection());
        }
        return $this->get('TableDefs');
    }
    
    /**
     * Getter for module definitions.
     * 
     * @return array
     */
    public function getModuleDefs()
    {
        // TODO this is more of a stub method at the moment
        //      we need to rethink how this information is defined and remove reliance on the global.
        return $GLOBALS['ModuleDefs'];
    }
    
    /**
     * Getter for access levels definition.
     *
     * @return array
     */
    public function getAccessLvlDefs()
    {
    	// TODO same as above :)
    	//      this is more of a stub method at the moment
    	//      we need to rethink how this information is defined and remove reliance on the global.
    	return $GLOBALS['AccessLvlDefs'];
    }
    
    /**
     * Getter for Logger object for the current script.
     *
     * @return DatixLogger Object to use for application action logging.
     */
    public function getLogger()
    {
        if ($this->get('logger') === null)
        {
            $this->set('logger', new DatixLogger('DatixWeb', $GLOBALS['logLevel'], $GLOBALS['logCategories'], $GLOBALS['maxLogFiles']));
        }
        return $this->get('logger');
    }

    /**
     * Getter for DeviceDetector object for the current script.
     *
     * @return DeviceDetect Object to use for application action logging.
     */
    public function getDeviceDetector()
    {
        if ($this->get('devicedetector') === null)
        {
            $this->set('devicedetector', new DeviceDetect());
        }
        return $this->get('devicedetector');
    }

    /**
     * Getter for DatixSessionCache object for the current script.
     *
     * @return DatixSessionCache Object to use for application action logging.
     */
    public function getSessionCache()
    {
        if ($this->get('sessioncache') === null)
        {
            $this->set('sessioncache', new DatixSessionCache());
        }
        return $this->get('sessioncache');
    }

    /**
     * Getter for application version number.
     * 
     * @param bool $major Whether or not to return just the major version number.
     * @param \src\framework\query\QueryFactory $qf
     * @param \DatixDBQuery $db
     * @return string
     */
    public function getVersion($major = false, QueryFactory $qf = null, \DatixDBQuery $db = null)
    {
        if ($this->get('version') === null)
        {
            $qf = $queryFactory ?: new QueryFactory();
            $db = $db ?: new \DatixDBQuery();
            
            $query = $qf->getQuery();
            $query->select(array('version','patchlevel'))
                  ->from('AAA_version');
            
            list($sql) = $qf->getSqlWriter()->writeStatement($query);
            
            $db->setSQL($sql);
            $db->prepareAndExecute();
            $row = $db->fetch();
            
            $this->set('version', array('major' => $row['version'], 'minor' => $row['patchlevel']));
        }
        
        $version = $this->get('version');
        
        if ($major)
        {
            return $version['major'];
        }
        else
        {
            return $version['major'] . '.' . $version['minor'];
        }
    }
    
    /**
     * Retrieve a system "global" parameter setting.
     * 
     * Currently just a wrapper for the existing procedural function.
     * 
     * @param string  $name    The parameter name.
     * @param string  $default The default value.
     * @param boolean $refresh If true then we lookup in the DB rather than relying on the cached version.
     * @param boolean $returnBoolean Returns true if result is "Y" or "y", false otherwise
     * 
     * @return mixed
     */
    public function getParm($name, $default = '', $refresh = false, $returnBoolean = false)
    {
        $result = GetParm($name, $default, $refresh);

        if ($returnBoolean) {
            return ($result == 'Y' || $result == 'y');
        } else {
            return $result;
        }
    }

    /**
     * Retrieve a user "global" parameter setting.
     *
     * Currently just a wrapper for the existing procedural function.
     *
     * @param string $login The login of the user
     * @param string $param The name of the global
     * @param string $default The value to return if no other is found in the session or db
     * @param boolean $returnBoolean Returns true if result is "Y" or "y", false otherwise
     *
     * @return mixed
     */
    public function getUserParm($login, $param, $default = '', $returnBoolean = false)
    {
        $result = GetUserParm($login, $param, $default);

        if ($returnBoolean) {
            return ($result == 'Y' || $result == 'y');
        } else {
            return $result;
        }
    }

    /**
     * Retrieves an array of dates that have been defined as public holidays.
     */
    public function getHolidays(QueryFactory $qf = null, \DatixDBQuery $db = null)
    {
        if ($this->get('holidays') === null)
        {
            $qf = $queryFactory ?: new QueryFactory();
            $db = $db ?: new \DatixDBQuery();
            
            $query = $qf->getQuery();
            $query->select(array('holidays.hol_date'));
            
            list($sql) = $qf->getSqlWriter()->writeStatement($query);
            
            $db->setSQL($sql);
            $db->prepareAndExecute();
            
            $this->set('holidays', $db->PDOStatement->fetchAll(\PDO::FETCH_COLUMN));
        }
        return $this->get('holidays');
    }
    
    /**
     * Returns the path to the client folder.
     * 
     * @return string
     */
    public function getClientFolder()
    {
        return $GLOBALS['ClientFolder'];
    }

    /**
     * Get encryption type to use with hashing
     * @return string (MD5, M, SHA1, BCRYPT)
     */
    public function getEncryptionType ()
    {
        $usingSharedPasswords = $this->getParm('PWD_SHARED', 'N', false, true);

        if ($usingSharedPasswords)
        {
            $dbEncryptionType = $this->getParm('PWD_ENCRYPT_TYPE', '', false);
            $encryption_type = $dbEncryptionType != '' && $dbEncryptionType != 'E'  ? $dbEncryptionType : 'SHA1';
        }
        else
        {
            $encryption_type = 'BCRYPT';
        }

        return $encryption_type;
    }

    /**
     * Getter for combo links.
     * 
     * @return ComboLinkCollection
     */
    public function getComboLinks()
    {
        if ($this->get('ComboLinks') === null)
        {
            $factory = new ComboLinkModelFactory();
            $this->set('ComboLinks', $factory->getCollection());
        }
        return $this->get('ComboLinks');
    }
    
    /**
     * Getter for FieldDefsExtra.
     * 
     * @return array
     */
    public function getFieldDefsExtra()
    {
        if ($this->get('FieldDefsExtra') === null)
        {
            $this->set('FieldDefsExtra', $GLOBALS['FieldDefsExtra']);
        }
        return $this->get('FieldDefsExtra');
    }
    
    /**
     * Getter for sub forms.
     * 
     * @return ComboLinkCollection
     */
    public function getSubForms()
    {
        if ($this->get('SubForms') === null)
        {
            $factory = new \src\combolinking\model\ComboLinkModelFactory();
            $collection = $factory->getCollection();
            $collection->setQuery(new Query());
            $this->set('SubForms', $collection);
        }
        return $this->get('SubForms');
    }
    
    /**
     * @return DeviceDetect
     */
    public function getDeviceDetectHelper()
    {
        if ($this->get('DeviceDetect') === null)
        {
            $this->set('DeviceDetect', new \src\framework\controller\helper\DeviceDetect());
        }
        return $this->get('DeviceDetect');
    }
}
