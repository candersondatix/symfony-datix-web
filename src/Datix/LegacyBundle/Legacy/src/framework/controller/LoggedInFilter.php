<?php
namespace src\framework\controller;

/**
* Secures controllers on the basis of users being a logged in.
*/
class LoggedInFilter extends ControllerFilter
{
    /**
    * Checks if the user is logged in.
    * 
    * @param string $action The subsequent action to perform on success.
    */
    public function doAction($action)
    {
        LoggedIn();
        return $this->proceed($action);
    }
}