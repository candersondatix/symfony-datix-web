<?php
namespace src\framework\controller;

use src\framework\controller\exceptions\ViewException;
use thirdpartylibs\OWASP\Reform;
use src\framework\controller\helper\FlashMessage as FlashMessage;

class View extends \Zend_View
{    
    /**
     * The full script path.
     * 
     * Shouldnt really be necessary to use since we can just use relative paths.
     * 
     * @var string
     */
    public $scripturl;
    
    /**
     * Set to '.min' if we're using the minifier.
     * 
     * @var string
     */
    public $addMinExtension;
    
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->setScriptPath('.');

        // this needs to be autoloaded sethelperpath however for now this is fine.
        // will come back after investigating why sethelperpath not working
        $this->flashmessage = new FlashMessage();
        $this->scripturl = $GLOBALS['scripturl'];
        $this->addMinExtension = ($GLOBALS['MinifierDisabled'] ? '' : '.min');
    }
    
    /**
     * Escape strings for use within views in a variety of contexts.
     * 
     * @param string $str    The string to be escaped.
     * @param int    $method The escape method to use.
     * 
     * @return string $str The escaped string.
     */
    public function escape($str, $method = 'html')
    {
        switch ($method)
        {
            case 'html':
                $str = Reform::HtmlEncode($str);
                break;
                
            case 'html_attr':
                $str = Reform::HtmlAttributeEncode($str);
                break;
                
            case 'xml':
                $str = Reform::XmlEncode($str);
                break;
                
            case 'xml_attr':
                $str = Reform::XmlAttributeEncode($str);
                break;
                
            case 'js':
                $str = Reform::JsString($str);
                break;
                
            case 'url':
                $str = urlencode($str);
                break;
                
            default:
                throw new ViewException('Invalid escape method');
        }
        return $str;
    }
}