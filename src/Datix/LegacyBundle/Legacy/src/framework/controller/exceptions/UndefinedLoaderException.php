<?php
namespace src\framework\controller\exceptions;

class UndefinedLoaderException extends ControllerException{}