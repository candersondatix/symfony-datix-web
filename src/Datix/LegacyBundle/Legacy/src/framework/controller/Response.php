<?php
namespace src\framework\controller;

/**
* Used by controllers to build a response to a request.
*/
class Response
{
    /**
    * The body of the response.
    * 
    * @var string
    */
    protected $body;
    
    /**
    * The view object responsible for rendering the display.
    * 
    * @var Zend_View
    */
    protected $view;
    
    /**
    * Constructor.
    * 
    * @param Zend_View $view
    */
    public function __construct(\Zend_View $view)
    {
        $this->view = $view;
        $this->body = '';    
    }
    
    /**
    * Builds the response body.
    * 
    * @param string $file
    * @param array  $params
    */
    public function build($file, array $params = array())
    {
        $this->view->assign($params);
        $this->body = $this->view->render($file);
    }
    
    /**
     * Setter for body.
     * 
     * Convenient to use when the response body is simple enough that 
     * defining it within a separate view file is overkill.
     * 
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }
    
    /**
    * Outputs the response body.
    */
    public function __toString()
    {
        return $this->body ?: '';
    }
}