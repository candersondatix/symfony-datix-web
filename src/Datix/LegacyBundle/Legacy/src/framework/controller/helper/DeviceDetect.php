<?php

namespace src\framework\controller\helper;

//use vendor\mobiledetect\mobiledetectlib\Mobile_Detect;

class DeviceDetect {

    private $MobileDetect;

    private $DeviceDetect = array();

    public function __construct()
    {
        require_once "vendor\mobiledetect\mobiledetectlib\Mobile_Detect.php";
        $this->MobileDetect = new \Mobile_Detect;
    }

    /**
     * Return true if device is a tablet or false otherwise
     *
     * @return bool
     */
    public function isTablet()
    {
        if( ! isset($_SESSION['DeviceDetect']['isTablet']))
        {
            /**
             * Runs the MobileDetect isTablet check and then adds a check for Windows 8 with touch, this could include non
             * touch devices unfortunately. The Windows NT version number looks for 6.2 and up as 6.2 is Win 8.0, 6.3 = 8.1
             */
            if($this->MobileDetect->isTablet() || preg_match('/MSIE [0-9]{1,2}\.[0-9]; Windows NT ([6]\.[2-9]|[7-9]\.[0-9]);.*?; Touch.*?/i', $this->MobileDetect->getUserAgent()) === 1)
            {
                $this->setDetectValue('isTablet', true);
            }
            else
            {
                $this->setDetectValue('isTablet', false);
            }

            $this->setSession();
        }
        else
        {
            $this->setDetectValue('isTablet', $_SESSION['DeviceDetect']['isTablet']);
        }

        return $this->getDetectValue('isTablet');
    }

    /**
     * Return true if device is running iOS or false otherwise
     *
     * @return bool
     */
    public function isIOS()
    {
        if( ! isset($_SESSION['DeviceDetect']['isIOS']))
        {
            /**
             * Runs the MobileDetect isTablet check and then adds a check for Windows 8 with touch, this could include non
             * touch devices unfortunately. The Windows NT version number looks for 6.2 and up as 6.2 is Win 8.0, 6.3 = 8.1
             */
            if($this->MobileDetect->isIOS())
            {
                $this->setDetectValue('isIOS', true);
            }
            else
            {
                $this->setDetectValue('isIOS', false);
            }

            $this->setSession();
        }
        else
        {
            $this->setDetectValue('isIOS', $_SESSION['DeviceDetect']['isIOS']);
        }

        return $this->getDetectValue('isIOS');
    }

    private function setSession()
    {
        $_SESSION['DeviceDetect'] = $this->DeviceDetect;
    }

    private function getSessionValue($key)
    {
        return $_SESSION['DeviceDetect'][$key];
    }

    private function setDetectValue($key, $value)
    {
        $this->DeviceDetect[$key] = $value;
    }

    private function getDetectValue($key)
    {
        return $this->DeviceDetect[$key];
    }

    public function getDetectValues()
    {
        $this->isTablet();
        $this->isIOS();

        return $this->DeviceDetect;
    }
} 