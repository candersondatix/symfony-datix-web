<?php

namespace src\framework\controller\helper;

class FlashMessage
{
    const ERROR = 'danger';
    const WARNING = 'warning';
    const SUCCESS = 'success';
    const NOTICE = 'info';

    protected $_namespace = 'flashmessage';
    protected $type = '';
    protected $title = '';

    public function __construct($type = null, $title = null)
    {
        $this->type = $type;
        $this->title = $title;
    }

    public function setType($type)
    {
        if(!is_string($type) || 0 == strcmp($type, ''))
        {
            $this->type = NOTICE;
        }
        else
        {
            $this->type = $type;
        }
        return $this;
    }

    public function getType()
    {
        if(0 == strcmp($this->type, ''))
        {
            $this->setType(NOTICE);
            return $this->type;
        }
        return $this->type;
    }

    public function addMessage($message, $namespace = null)
    {
        if(!is_string($namespace) || 0 == strcmp($namespace, ''))
        {
            $namespace = $this->getNamespace();
        }

        $_SESSION[$namespace][] = $message;

        return $this;
    }

    public function setNamespace($namespace = "flashmessage")
    {
        $this->_namespace = $namespace;
        return $this;
    }

    public function getNamespace()
    {
        return $this->_namespace;
    }

    public function resetNamespace()
    {
        $this->setNamespace();
        return $this;
    }

    public function hasMessage($namespace = null)
    {
        if(!is_string($namespace) || 0 == strcmp($namespace, ''))
        {
            $namespace = $this->getNamespace();
        }

        if(isset($_SESSION[$namespace]))
        {
            return true;
        }

        return false;
    }

    public function getMessage($namespace = null)
    {
        if(!is_string($namespace) || 0 == strcmp($namespace, ''))
        {
            $namespace = $this->getNamespace();
        }

        $message = $_SESSION[$namespace];
        $this->clearMessage($namespace);

        return $message;
    }

    public function clearMessage($namespace = null)
    {
        if(!is_string($namespace) || 0 == strcmp($namespace, ''))
        {
            $namespace = $this->getNamespace();
        }

        unset($_SESSION[$namespace]);
    }
}