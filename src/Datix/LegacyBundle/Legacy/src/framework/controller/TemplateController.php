<?php
namespace src\framework\controller;
use src\framework\registry\Registry;

/**
* Handles the construction of the system template.
*/
class TemplateController extends Controller
{
    /**
    * The page title.
    * 
    * @var string
    */
    protected $title;
    
    /**
    * The module code.
    * 
    * @var string
    */
    protected $module;
    
    /**
    * True if the left hand menu is displayed.
    * 
    * @var boolean
    */
    protected $hasMenu;
    
    /**
    * True if the main container has padding.
    * 
    * @var boolean
    */
    protected $hasPadding;
    
    /**
     * An array of additional parameters used when constructing the side menu.
     * 
     * @var array
     */
    protected $menuParameters;

    /**
     * The image to display on page title
     *
     * @var string
     */
    protected $image;

    /**
     * Information about the current record showing
     *
     * @var string
     */
    protected $recordInfo;

    /**
     * An additional link to display alongside the title image and title text
     *
     * @var string
     */
    protected $subtitle;

    /**
     * An array of additional parameters used when constructing the title.
     *
     * @var array
     */
    protected $titleParameters;
    
    /**
     * Whether or not the page footer is displayed.
     * 
     * @var boolean
     */
    protected $hasFooter;
    
    /**
     * Whether or not the page header (nav links etc) is displayed.
     * 
     * @var boolean
     */
    protected $hasHeader;

    protected $deviceDetect;
    
    /**
    * Constructor - left hand menu and main container padding are used by default (this is the most common case).
    * 
    * @param Request  $request
    * @param Response $response
    */
    public function __construct(Request $request, Response $response)
    {
        $this->hasMenu    = true;
        $this->hasPadding = true;
        $this->hasFooter  = true;
        $this->hasHeader  = true;

        parent::__construct($request, $response);

        $this->deviceDetect = $this->registry->getDeviceDetector();

        /**
        * Add required CSS files
        */
        $this->addCss(array(
                'css/global.css',
                'css/nav.css',
                'css/dropdown_popups.css',
                'css/crosstab.css',
                'css/reporting.css',
                'css/IE6.css',
                'css/jquery-ui-1.10.3.custom.css',
                'css/jquery.datixweb.css',
                'js_functions/qTip/jquery.qtip.min.css'
            )
        );

        if($this->registry->getParm('ENHANCED_ACCESSIBILITY', 'N', false, true)) { $this->addCss('css/accessibility.css'); }

        $this->addJs(array(
            'js_functions/jquery/jquery-1.7.1.min.js',
            'js_functions/jquery/jquery-ui-1.10.3.custom.js',
            'js_functions/lib/jquery.navigation.js',
            'js_functions/jquery/jquery.cookie.js',
            '<script>jQuery.noConflict();</script>',
            'src/generic/js/modernizr.datix.min.js'
            ),
        array('sub_array' => 'first'));

        if($GLOBALS['devMockjax']){ $this->addJs('helper_scripts/jquery-mockjax-master/jquery.mockjax.js', array('sub_array' => 'first')); }

        // Script to bind the calendar to date fields
        if (GetParm('FMT_DATE_WEB') == 'US')
        {
            $date_fmt= 'mm/dd/yy';
        }
        else
        {
            $date_fmt = 'dd/mm/yy';
        }

        /**
         * Add required variables
         */
        $this->sendToJs('scripturl', $GLOBALS['ClientSpecificPrefix'].$GLOBALS['scripturl'], false);
        $this->sendToJs(array
            (

                'scripturl' => $GLOBALS['ClientSpecificPrefix'].$GLOBALS['scripturl'],
                'calendarOnclick' => GetParm('CALENDAR_ONCLICK', 'N'),
                'dateFormatCode' => GetParm('FMT_DATE_WEB'),
                'dateFormat' => $date_fmt,
                'weekStartDay' => GetParm('WEEK_START_DAY', 2),
                'calendarWeekend' => GetParm('CALENDAR_WEEKEND', 1),
                'loggedIn' => $GLOBALS['logged_in'],
                'deviceDetect' => $this->deviceDetect->getDetectValues(),
                'currencyChar' => GetParm('CURRENCY_CHAR', '£')
            )
        );

        /**
         * Add CSRF assets if required
         */
        if (bYN(GetParm('CSRF_PREVENTION','N')))
        {
            $this->sendToJs(array(
                'token' => \CSRFGuard::getCurrentToken(),
                'suppressClientSideCSRF' => false
            ), false);
            $this->addJs('js_functions/tokenizer.js');
        }
        else
        {
            $this->sendToJs('token', '', false);
        }
    }
    
    /**
    * Builds the template in addition to the controller action.
    * 
    * @param string $action The requested action.
    */
    public function callAction($action)
    {
        parent::callAction($action);

        /**
         * Add template required JS files
         * Ordering needs to keep framework.js and outputToVars() at the top with framwork.js first
         */
        $this->addJs(array(
            'src/framework/js/framework.js',
            $this->outputVarsToJs(),
            'js_functions/common.js',
            array('file' => 'js_functions/Listing.js', 'conditions' => $this->request->getParameter('print') != 1),
            array('file' => 'js_functions/listbox.js', 'conditions' => $this->request->getParameter('print') != 1),
            'js_functions/timerClass.js',
            array('file' => 'js_functions/DynamicList.js', 'conditions' => $this->request->getParameter('print') != 1),
            array('file' => 'js_functions/ie6compat.js', 'conditions' => $this->request->getParameter('print') != 1),
            array('file' => 'js_functions/email.js', 'conditions' => $this->request->getParameter('print') != 1),
            'js_functions/jquery/jquery.ready.js',
            array('file' => 'js_functions/prototype.js'),
            array('file' => 'js_functions/scriptaculous/scriptaculous.js'),
            array('file' => 'js_functions/CodeSelectionCtrl.js', 'conditions' => $this->request->getParameter('print') != 1),
            array('file' => 'src/generic/js/limitTextareaChars.js', 'conditions' => $this->request->getParameter('print') != 1),
            array('file' => 'js_functions/qTip/jquery.qtip.min.js', 'conditions' => $this->request->getParameter('print') != 1),
        ),
        array('sub_array' => 'first'));

        if (bYN(GetParm('WEB_SPELLCHECKER', 'N')) && $this->request->getParameter('print') != 1)
        {
            $this->addJs('thirdpartylibs/phpspellcheck/include.js');
        }

        if ( ! isset($_SESSION['FlashAvailable']))
        {
            $this->addJs('js_functions/AdobeFlashDetection.js');
        }

        if (bYN(GetParm('DETECT_TIMEZONES', 'N')) && ! isset($_SESSION['Timezone']) && $_GET['action'] != 'reset_password')
        {
            $this->addJs('<script language="javascript" type="text/javascript">SetTimezoneValue(\''.\Sanitize::SanitizeString($_GET['action']).'\')</script>');
        }

        if($this->request->getParameter('print') == 1)
        {
            $this->addCss('css/print.css', array('attrs' => 'media="print"'));
        }

        if( ! $this->hasHeader())
        {
            $this->addCss('css/template_css/no-header.css');
        }

        if( ! $this->hasMenu())
        {
            $this->addCss('css/template_css/no-menu.css');
        }

        if( ! $this->hasPadding())
        {
            $this->addCss('css/template_css/no-padding.css');
        }

        if (file_exists($GLOBALS['dtxuser_css_file']))
        {
            $this->addCss($GLOBALS['dtxuser_css_file']);
        }

        $this->addJs(array(
            'js_functions/xml.js',
            array('file' => 'js_functions/FloatingWindowClass.js', 'conditions' => true),
            'js_functions/jquery/jquery.class.js',
            'js_functions/encoder.js',
            'js_functions/dropdowns.js',
            array('file' => 'js_functions/ContactMatchCtrl.js', 'conditions' => true),
            array('file' => 'js_functions/OrganisationMatchCtrl.js', 'conditions' => true),
            array('file' => 'js_functions/MedicationMatchCtrl.js', 'conditions' => true),
            array('file' => 'js_functions/EquipmentMatchCtrl.js', 'conditions' => true),
            'js_functions/export.js',
            'js_functions/contactSearch.js',
            'js_functions/organisationSearch.js'
            ),
        array('conditions' => $this->request->getParameter('print') != 1, 'sub_array' => 'bottom'));

        $this->getTitle();
        $this->getMenu();
        $this->getTemplateHeader();
        
        // controllers should never echo the response themselves, only return it.
        // this is a dirty hack to fit in with our current templating logic for now!
        echo $this->response;
        $this->response = null;
        
        $this->getFooter();
    }
    
    /**
    * Constructs the page title.
    */
    protected function getTitle()
    {
        if ($this->title)
        {
            $params = array(
                'title' => $this->title,
                'module' => $this->module,
                'image' => $this->image,
                'record_info' => $this->recordInfo,
                'subtitle' => $this->subtitle
            );

            if (!empty($this->titleParameters))
            {
                $params = array_merge($params, $this->titleParameters);
            }

            GetPageTitleHTML($params);
        }
    }

    /**
     * Function to call to add a suffix to form titles based on global values for the module
     */
    public function getTitleSuffix($module, $data, $add_separator = true)
    {
        $title_suffix = null;

        /**
         * Array of valid fields, this is done here manually as this is easiest for the moment
         */
        $allowed_fields = array_map('UnicodeString::strtoupper', array(
            'RECORDID',
            'CLA_OURREF', 'CLA_NAME', 'cla_type', 'cla_subtype', 'cla_curstage',
            'COM_OURREF', 'COM_NAME', 'com_type', 'com_subtype',
            'INC_OURREF', 'INC_NAME', 'INC_TYPE', 'INC_CARESTAGE', 'INC_CLIN_DETAIL', 'INC_CLINTYPE',
            'PAL_OURREF', 'PAL_NAME',
            'RAM_OURREF', 'RAM_NAME', 'ram_risk_type', 'ram_risk_subtype', 'ram_cur_rating', 'ram_cur_level'
        ));

        /**
         * Array of coded fields that will need their description retrieved in order to be show properly
         */
        //TODO make this use fielddefs instead of a manual list
        $coded_fields = array_map('UnicodeString::strtoupper', array(
            'cla_type', 'cla_subtype', 'cla_curstage',
            'com_type', 'com_subtype',
            'INC_TYPE', 'INC_CARESTAGE', 'INC_CLIN_DETAIL', 'INC_CLINTYPE',
            'ram_risk_type', 'ram_risk_subtype', 'ram_cur_level'
        ));

        $suffix_fields = GetParm($module . '_FORM_TITLE_FIELDS');

        if( ! empty($suffix_fields))
        {
            $field_array = explode(' ', $suffix_fields);

            /**
             * Filter array fields to restrict output to only certain fields
             */
            foreach($field_array as $key => $field)
            {
                if( ! in_array(\UnicodeString::strtoupper($field), $allowed_fields))
                {
                    unset($field_array[$key]);
                }
            }

            $showing = 1;

            foreach($field_array as $field)
            {
                $field_value = $data[\UnicodeString::strtolower($field)];

                if($field_value != false || $field_value != null || $field_value != '')
                {
                    if($showing > 1)
                    {
                        $title_suffix .= ' | ';
                    }

                    if(empty($title_suffix) && $add_separator)
                    {
                        $title_suffix = '<h1 id="title-additional">';
                    }

                    if(in_array(\UnicodeString::strtoupper($field), $coded_fields))
                    {
                        $field_value = code_descr($module, \UnicodeString::strtolower($field), $field_value);
                    }

                    $title_suffix .= ' ' . $field_value;

                    $showing++;
                }
            }

            $title_suffix .= '</h1>';
        }

        return $title_suffix;
    }
    
    /**
    * Constructs the left hand menu.
    */
    protected function getMenu()
    {
        if ($this->hasMenu)
        {
        	$params = array('module' => $this->module);

        	if (!empty($this->menuParameters))
        	{
        		$params = array_merge($params, $this->menuParameters);
        	}

            GetSideMenuHTML($params);
        }    
    }
    
    /**
    * Constructs the top portion of the template.
    */
    protected function getTemplateHeader()
    {
        $GLOBALS['dtxtitle'] = $this->title; // used in template_header() to populate the page's title tag

        if ($this->hasPadding)
        {
            if ($this->hasMenu)
            {
                template_header('','',false,"UTF-8", null, $this);
            }
            else
            {
                template_header_nomenu($this);
            }
        }
        else if ($this->hasMenu)
        {
            template_header_nopadding($this);
        }
        else
        {
            template_header_nomenu_nopadding($this);
        }    
    }
    
    /**
    * Constructs the bottom portion of the template.
    */
    protected function getFooter()
    {
        if ($this->hasFooter)
        {
            footer($this);
        }
    }
    
    /**
     * Getter for hasHeader
     *
     * @return boolean
     */
    public function hasHeader()
    {
        return $this->hasHeader;
    }

    /**
     * Getter for hasMenu
     *
     * @return boolean
     */
    public function hasMenu()
    {
        return $this->hasMenu;
    }

    /**
     * Getter for hasPadding
     *
     * @return boolean
     */
    public function hasPadding()
    {
        return $this->hasPadding;
    }
}