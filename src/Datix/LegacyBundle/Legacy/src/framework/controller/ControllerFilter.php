<?php
namespace src\framework\controller;

/**
* Base class for controller filters.
* 
* Controller filters provide a mechanism for providing access to controllers.
* They are implemented using the Decorator pattern.
*/
abstract class ControllerFilter implements ControllerInterface
{
    /**
    * The object responsible for the next action, 
    * either the controller itself or another filter.
    * 
    * @var ControllerInterface
    */
    protected $controller;
    
    /**
    * Constructor.
    * 
    * @param ControllerInterface $controller
    */
    public function __construct(ControllerInterface $controller)
    {
        $this->controller = $controller;   
    }
    
    /**
    * Used on success to call the next action.
    * 
    * @param string $action The name of the next action.
    */
    protected function proceed($action)
    {
        return $this->controller->doAction($action);
    }
}