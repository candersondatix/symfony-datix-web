<?php
namespace src\framework\events;

/**
 * Used alongside Subject to implement the Observer Design Pattern.
 */
interface Observer
{
    /**
     * Receive an update from the subject.
     * 
     * @param Subject $subject The object that's being observed.
     * @param int     $event   The event type.
     */
    public function update(Subject $subject, $event);
}