<?php
namespace src\framework\events;

/**
 * Used alongside Observer to implement the Observer Design Pattern.
 */
interface Subject
{
    /**
     * Attach an observer.
     * 
     * @param Observer $observer
     */
    public function attach(Observer $observer);
    
    /**
     * Detach an observer.
     * 
     * @param Observer $observer
     */
    public function detach(Observer $observer);
    
    /**
     * Notify an observer.
     * 
     * @param int $event The event type.
     */
    public function notify($event);
}