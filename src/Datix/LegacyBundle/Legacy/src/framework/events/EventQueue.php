<?php
namespace src\framework\events;

/**
 * Enables the storing of observers in order to defer their execution.
 */
class EventQueue
{
    /**
     * The Singleton instance.
     * 
     * @var EventQueue
     */
    private static $instance;
    
    /**
     * The deferred events.
     * 
     * @var array
     */
    private $events;
    
    private function __construct()
    {
        $this->events = array();
    }
    
    /**
     * Get the Singleton instance.
     * 
     * @return EventQueue
     */
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            self::$instance = new EventQueue();
        }
        return self::$instance;
    }
    
    /**
     * Add an event to the queue.
     * 
     * @param Observer $observer
     * @param Subject  $subject
     * @param int      $event
     */
    public function add(Observer $observer, Subject $subject, $event)
    {
        $this->events[] = array('observer' => $observer, 'subject' => $subject, 'event' => $event);
    }
    
    /**
     * Execute all the events in the queue.
     */
    public function execute()
    {
        foreach ($this->events as $key => $event)
        {
            $event['observer']->update($event['subject'], $event['event']);
            unset($this->events[$key]);
        }
    }
}