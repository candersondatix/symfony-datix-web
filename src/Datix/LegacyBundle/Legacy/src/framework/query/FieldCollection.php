<?php
namespace src\framework\query;

use src\framework\query\exceptions\IncompleteFieldException;
use src\framework\query\exceptions\UndefinedFieldException;
use src\framework\query\exceptions\InvalidFieldException;
use src\framework\query\exceptions\InvalidComparisonException;
use src\framework\registry\Registry;
use src\reports\exceptions\ReportException;
use src\system\database\FieldInterface;

/**
 * A collection of fields and asscoiated comparisons.
 * 
 * Provides a fluid interface for building a set of parameters used in a query.
 */
class FieldCollection
{
    // operator constants
    const EQ        = 1;
    const NOT_EQ    = 2;
    const GT        = 3;
    const GT_EQ     = 4;
    const LT        = 5;
    const LT_EQ     = 6;
    const IN        = 7;
    const NOT_IN    = 8;
    const IS_NULL   = 9;
    const NOT_NULL  = 10;
    const IS_EMPTY  = 11;
    const NOT_EMPTY = 12;
    const LIKE      = 13;
    const NOT_LIKE  = 14;
    const BETWEEN   = 15;
    const ALL       = 16;
    const NONE      = 17;

    /**
     * The fields and their comparisons.
     * 
     * @var array
     */
    protected $fields;
    
    /**
     * The field that's currently being defined.
     *
     * @var Field
     */
    protected $currentField;
    
    /**
     * A map of original field names to aliases.
     * 
     * @var array
     */
    protected $originalFields;
    
    /**
     * Whether or not any of the comparison values are @codes.
     * 
     * @var boolean
     */
    protected $atCode;
    
    /**
     * Whether or not any of the comparison values are @prompts.
     * 
     * @var boolean
     */
    protected $atPrompt;
    
    /**
     * The fields within this collection that use @prompt as their comparison.
     * 
     * @var array
     */
    protected $atPromptFields;
    
    public function __construct()
    {
        $this->fields = array();
        $this->originalNames = array();
        $this->hasAtCodes = false;
        $this->hasAtPrompt = false;
        $this->atPromptFields = array();
    }

    public function __clone()
    {
        foreach ($this->fields as $name => $field)
        {
            $this->fields[$name] = clone $field;
        }
    }
    
    /**
     * Getter for fields.
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }
    
    /**
     * Sets up the field for comparision.
     *
     * @param string|array $name         Either a single field name, or an array where the first element is a "wrapper"
     *                                   (alias, SQL function etc) and the second element is the field name(s).  The wrapper contains
     *                                   one or more placeholders (? characters) which are replaced in turn by the field names.
     *                                   When passing an array, the second element can either be a string if only a single field name is being wrapped,
     *                                   or an array if multiple field names are being wrapped.
     *
     * @throws IncompleteFieldException If the comparison for the current field hasn't been defined.
     *
     * @return FieldCollection
     */
    public function field($name)
    {
        if ($this->currentField != null && !$this->currentField->isComplete())
        {
            throw new IncompleteFieldException('The current field is incomplete.');
        }
        
        if (is_array($name))
        {
            $this->currentField = new Field($name);
            $this->fields[] = $this->currentField;
        }
        else
        {
            if (isset($this->fields[$name]))
            {
                $this->currentField = $this->fields[$name];
            }
            else
            {
                $this->currentField = new Field($name);
                $this->fields[$name] = $this->currentField;
            }
        }
        
        return $this;
    }
    
    /**
     * Used to convert the name of one of the fields in the collection.
     * 
     * Useful when mapping entity properties or dealing with aliases.
     * 
     * @param string $current The name of an existing field in this collection.
     * @param string $new     The name of the field we wish to convert to.
     */
    public function convertFieldName($current, $new)
    {
        foreach ($this->fields as $field)
        {
            $field->convertFieldName($current, $new);
        }
    }
    
    /**
     * Adds an equality operation to current field.
     *
     * @param string  $value   The value we're comparing against.
     * @param boolean $isField Whether or not the value is another field.
     *
     * @return FieldCollection
     */
    public function eq($value, $isField = false)
    {
        return $this->operation(self::EQ, $value, $isField);
    }
    
    /**
     * Adds an inequality operation to current field.
     *
     * @param string  $value   The value we're comparing against.
     * @param boolean $isField Whether or not the value is another field.
     *
     * @return FieldCollection
     */
    public function notEq($value, $isField = false)
    {
        return $this->operation(self::NOT_EQ, $value, $isField);
    }
    
    /**
     * Adds a greater than operation to current field.
     *
     * @param string  $value   The value we're comparing against.
     * @param boolean $isField Whether or not the value is another field.
     *
     * @return FieldCollection
     */
    public function gt($value, $isField = false)
    {
        return $this->operation(self::GT, $value, $isField);
    }
    
    /**
     * Adds a greater than or equal to operation to current field.
     *
     * @param string  $value   The value we're comparing against.
     * @param boolean $isField Whether or not the value is another field.
     *
     * @return FieldCollection
     */
    public function gtEq($value, $isField = false)
    {
        return $this->operation(self::GT_EQ, $value, $isField);
    }
    
    /**
     * Adds a less than operation to current field.
     *
     * @param string  $value   The value we're comparing against.
     * @param boolean $isField Whether or not the value is another field.
     *
     * @return FieldCollection
     */
    public function lt($value, $isField = false)
    {
        return $this->operation(self::LT, $value, $isField);
    }
    
    /**
     * Adds a less than or equal to operation to current field.
     *
     * @param string  $value   The value we're comparing against.
     * @param boolean $isField Whether or not the value is another field.
     *
     * @return FieldCollection
     */
    public function ltEq($value, $isField = false)
    {
        return $this->operation(self::LT_EQ, $value, $isField);
    }
    
    /**
     * Adds an in operation to current field.
     *
     * @param string|array|Query $values The values we're comparing against, either an array of values or a Query object representing a sub-query.
     *
     * @return FieldCollection
     */
    public function in($values)
    {
        if (is_array($values) && empty($values))
        {
            //This may break some logic elsewhere, but it makes more sense this way round. Anything this breaks is likely wrong. Ahem.
            Registry::getInstance()->getLogger()->logDebug('Constructing in() with empty array.');
            return $this->operation(self::NONE);
        }
        
        if (!is_array($values))
        {
            if ($values instanceof Query && $values->getType() !== Query::SELECT)
            {
                throw new \InvalidArgumentException('Query objects passed to FieldCollection::in() must of type Query::SELECT');
            }
            else if (!($values instanceof Query))
            {
                $values = [$values];
            }
        }
        
        return $this->operation(self::IN, $values);
    }
    
    /**
     * Adds a not in operation to current field.
     *
     * @param array|Query $values The values we're comparing against, either an array of values or a Query object representing a sub-query.
     *
     * @return FieldCollection
     */
    public function notIn($values)
    {
        if (is_array($values) && empty($values))
        {
            return $this;
        }
        
        if (!is_array($values) && (!($values instanceof Query) || $values->getType() !== Query::SELECT))
        {
            throw new \InvalidArgumentException('FieldCollection::notIn() argument must be an array or Query of type Query::SELECT');
        }
        
        return $this->operation(self::NOT_IN, $values);
    }
    
    /**
     * Adds a not null operation to current field.
     *
     * @return FieldCollection
     */
    public function notNull()
    {
        return $this->operation(self::NOT_NULL);
    }

    /**
     * Adds a is null operation to current field.
     *
     * @return FieldCollection
     */
    public function isNull()
    {
        return $this->operation(self::IS_NULL);
    }

    /**
     * Adds an like operation to current field.
     *
     * @param string $value The value we're comparing against.
     *
     * @return FieldCollection
     */
    public function like($value)
    {
        return $this->operation(self::LIKE, $value);
    }
    
    /**
     * Adds a not like operation to current field.
     *
     * @param string $value The value we're comparing against.
     *
     * @return FieldCollection
     */
    public function notLike($value)
    {
        return $this->operation(self::NOT_LIKE, $value);
    }

    /**
     * Adds a not empty operation to current field.
     *
     * @return FieldCollection
     */
    public function notEmpty()
    {
        return $this->operation(self::NOT_EMPTY);
    }
    
    /**
     * Adds an is empty operation to current field.
     *
     * @return FieldCollection
     */
    public function isEmpty()
    {
        return $this->operation(self::IS_EMPTY);
    }

    /**
     * Adds a between operation to current field.
     * 
     * @param array $values A two element array containing the between limits.
     *
     * @return FieldCollection
     */
    public function between(array $values)
    {
        //pre-12.2, you could use value: or :value as valid inputs for money and number fields, but JC thinks
        //no one uses these, so I have left them out for now, pending further investigation by him.
        if (count($values) != 2)
        {
            throw new ReportException('Incorrect number of values specified in "BETWEEN" clause. Two values are permitted, '.count($values).' supplied.');
        }

        return $this->operation(self::BETWEEN, $values);
    }

    /**
     * Adds a dummy operation that returns all records
     *
     * @return FieldCollection
     */
    public function all()
    {
        return $this->operation(self::ALL);
    }

    /**
     * Adds a dummy operation that returns no records
     *
     * @return FieldCollection
     */
    public function none()
    {
        return $this->operation(self::NONE);
    }

    /**
     * Enables the addition of a field comparison by referencing the operator constant.
     * 
     * Delegates to the equivalent operator methods.
     * 
     * @param string        $operator The comparison operator.
     * @param string|array  $value    The value(s) we're comparing against.
     * @param boolean       $isField  Whether or not the value is another field.
     * 
     * @return FieldCollection
     */
    public function addComparison($operator, $value, $isField)
    {
        switch ($operator)
        {
            case self::EQ:
                $this->eq($value, $isField);
                break;
            case self::NOT_EQ:
                $this->notEq($value, $isField);
                break;
            case self::GT:
                $this->gt($value, $isField);
                break;
            case self::GT_EQ:
                $this->gtEq($value, $isField);
                break;  
            case self::LT:
                $this->lt($value, $isField);
                break;
            case self::LT_EQ:
                $this->ltEq($value, $isField);
                break;
            case self::IN:
                $this->in($value);
                break;
            case self::NOT_IN:
                $this->notIn($value);
                break;
            case self::IS_NULL:
                $this->isNull();
                break;
            case self::NOT_NULL:
                $this->notNull();
                break;
            case self::IS_EMPTY:
                $this->isEmpty();
                break;
            case self::NOT_EMPTY:
                $this->notEmpty();
                break;
            case self::LIKE:
                $this->like($value);
                break;
            case self::NOT_LIKE:
                $this->notLike($value);
                break;
            case self::BETWEEN:
                $this->between($value);
                break;
            case self::ALL:
                $this->all();
                break;
            case self::NONE:
                $this->none();
                break;
            default:
                throw new InvalidComparisonException('The operator '.$operator.' is invalid');
        }
        return $this;
    }
    
    /**
     * Getter for atCode.
     * 
     * @return boolean
     */
    public function hasAtCode()
    {
        return $this->atCode;
    }
    
    /**
     * Getter for atPrompt.
     * 
     * @return boolean
     */
    public function hasAtPrompt()
    {
        return $this->atPrompt;
    }
    
    /**
     * Replace @prompt codes in each field with their literal values.
     */
    public function replaceAtPrompts()
    {
        foreach ($this->fields as $field)
        {
            $field->replaceAtPrompts();
        }
    }
    
    /**
     * Retrieves a list of field names from the query criteria that use @prompt as their comparison.
     * 
     * @return array
     */
    public function getAtPromptFields()
    {
        return $this->atPromptFields;
    }

    /**
     * Does the work for the operator methods by adding the comparison to the current field.
     *
     * @param string             $operator The comparison operator.
     * @param string|array|Query $value    The value(s) we're comparing against.
     * @param boolean            $isField  Whether or not the value is another field.
     *
     * @throws UndefinedFieldException If the current field is not yet defined.
     *
     * @return FieldCollection
     */
    protected function operation($operator, $value = null, $isField = false)
    {
        if ($this->currentField == null)
        {
            throw new UndefinedFieldException('Field is undefined.');
        }
        
        $this->identifyAtCodes($value);
        
        $this->currentField->addComp($operator, $value, $isField);
        return $this;
    }
    
    /**
     * Determines whether the value in the comparison is an @code.
     * 
     * @param string|array|Query $value The value(s) in the comparison.
     */
    protected function identifyAtCodes($value)
    {
        if (($this->atCode && $this->atPrompt) || $value === null || $value instanceof Query)
        {
            return;
        }
        
        if (!is_array($value))
        {
            $value = [$value];
        }
        
        foreach ($value as $v)
        {
            if (!$this->atPrompt)
            {
                if (\UnicodeString::strtoupper($v) == '@PROMPT' && in_array( $this->currentField->getFieldDef()->getType(), [FieldInterface::DATE, FieldInterface::CODE, FieldInterface::MULTICODE, FieldInterface::YESNO]) )
                {
                    $this->atPrompt = true;
                    $this->atCode = true;
                    
                    $this->atPromptFields[] = $this->currentField->getName();
                }
            }
            
            if (!$this->atCode)
            {
                if (\UnicodeString::strpos($v, '@') !== false)
                {
                    $this->atCode = true;
                }
            }
        }
    }
}