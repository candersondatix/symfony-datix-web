<?php
namespace src\framework\query;

use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\framework\query\exceptions\QueryException;
use src\system\database\FieldInterface;
use src\system\database\extrafield\ExtraField;
use src\savedqueries\model\SavedQuery;

use src\reports\model\report\Report;
use src\framework\query\Field;

/**
 * Represents a database query.
 */
class Query
{
    // Type constants
    const SELECT = 1;
    const INSERT = 2;
    const UPDATE = 3;
    const DELETE = 4;
    
    // where method constants
    const WHERE_DEFAULT   = 1;    // carry out all query build steps when adding a new where clause
    const WHERE_NO_TABLES = 2;    // suppress automatic addition of table names
    
    /**
     * The type of query.
     * 
     * @var int
     */
    protected $type;
    
    /**
     * The TOP clause.
     * 
     * @var int
     */
    protected $top;
    
    /**
     * The fields used in SELECT/INSERT statements.
     * 
     * @var array
     */
    protected $fields;
    
    /**
     * The table name used in SELECT/INSERT statements.
     * 
     * @var string
     */
    protected $table;
    
    /**
     * All of the tables involved in this query.
     * 
     * @var array
     */
    protected $tables;
    
    /**
     * The JOIN clauses.
     * 
     * @var array
     */
    protected $joins;
    
    /**
     * The CROSS APPLY clauses (SQL Server-specific!).
     * 
     * @var array
     */
    protected $crossApply;
    
    /**
     * The object representing the WHERE clause.
     *
     * @var Where
     */
    protected $where;
    
    /**
     * Since the vast majority of pre-defined where clauses are plain strings, rather than Where objects,
     * we need a way of adding these to the query, so they're stored in this array.
     * 
     * This is obvioulsy not ideal, and it would be wonderful if things were more consistent, but there you go.
     * 
     * @var array
     */
    protected $whereStrings;
    
    /**
     * The ORDER BY clause.
     * 
     * @var array
     */
    protected $orderBy;
    
    /**
     * The GROUP BY clause.
     * 
     * @var array
     */
    protected $groupBy;
    
    /**
     * The modules involved in this query.
     * 
     * This property determines which security where clauses to append to queries in order to maintain access control.
     * 
     * @var array
     */
    protected $modules;
    
    /**
     * Security where clauses are not automatically appended to the query of this flag is set.
     * 
     * @var boolean
     */
    protected $overrideSecurity;
    
    /**
     * Defines the limits of the query, for paging.
     * 
     * @var array
     */
    protected $limit;
    
    /**
     * String holding a complete query, used when the query is too complex to be constructed by the writer
     * 
     * @var string
     */
    protected $queryString;

    /**
     * Used to construct associated query objects.
     *
     * @var QueryFactory
     */
    protected $factory;
    
    /**
     * @var Registry
     */
    protected $registry;
    
    /**
     * Used to keep track of extra fields and their generated aliases.
     * 
     * @var array
     */
    protected $extraFields;
    
    /**
     * Use this value to set datefirst param in MSSQL
     * 
     * @var integer
     */
    protected $datefirst;
    
    /**
     * Flags whether or not the Query object attempts to automatically create exra field joins.
     * 
     * @var boolean
     */
    protected $autoCreateExtraFieldJoins;

    public function __construct(QueryFactory $factory = null, Registry $registry = null)
    {
        $this->fields       = [];
        $this->extraFields  = [];
        $this->tables       = [];
        $this->joins        = [];
        $this->crossApply   = [];
        $this->orderBy      = [];
        $this->groupBy      = [];
        $this->whereStrings = [];
        
        $this->factory  = $factory ?: new QueryFactory();
        $this->registry = $registry ?: Registry::getInstance();
        
        $this->overrideSecurity = false;
        $this->autoCreateExtraFieldJoins = true;
    }

    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
    }
    
    public function getQueryString()
    {
        return $this->queryString;
    }
    
    /**
     * Set value of datefirst (first day of the week)
     * 
     * @author gszucs
     * @param integer $value: 1 is Monday, 2 Tuesday...7 is Sunday
     */
    public function setDateFirst( $value )
    {
    	if( in_array( (int) $value, range(1, 7)) )
    	{
    		$this->datefirst = (int) $value;
    	}
    }
    
    /**
     * Get value of datefirst
     * 
     * @author gszucs
     * @return integer
     */
    public function getDateFirst()
    {
    	return $this->datefirst;
    }
    
    /**
     * Setter for top.
     * 
     * @param int $top
     * 
     * @return Query
     */
    public function top($top)
    {
        $this->top = (int) $top;
        return $this;
    }
    
    /**
     * Initiates a SELECT statement and assigns the fields to be selected.
     * 
     * Fields can be appended to an existing select clause via subsequent calls to this method.
     * 
     * @param array   $fields    Each element can either be a string representing the field name, or a two-element array where the first element is
     *                           a statement that is wrapped around the field (e.g. an alias, SQL function etc) which uses '?' as a placeholder for the field name,
     *                           which is defined in the second element, e.g. array('table.field1', array('? AS the_second_field', 'table.field2'), ...)
     *                      
     * @param boolean $overwrite Whether to overwrite the existing clause.
     * 
     * @return Query
     */
    public function select(array $fields, $overwrite = false)
    {
        $this->type = self::SELECT;
        $this->extractTables($fields);
        
        if ($overwrite)
        {
            $this->fields = $fields;
        }
        else
        {
            $this->fields = array_merge($this->fields, $fields);
            $this->fields = array_unique($this->fields, SORT_REGULAR);
        }
        
        if (!isset($this->table) && count($this->tables) == 1)
        {
            // automatically set table name if all fields are from one table
            $this->table = $this->tables[0];
        }
        
        $this->fields = $this->createExtraFieldJoins($this->fields);
        
        return $this;
    }
    
    /**
     * Assign the table name to be used in the SELECT statement.
     * 
     * @param string $table
     * 
     * @return Query
     */
    public function from($table)
    {
        $this->table = $table;
        return $this;
    }
    
    /**
     * Used to manually set the join condition.
     * 
     * @param string        $table The name of the table we're joining.
     * @param Where|string  $where The condition of the join.
     * @param string        $type  The type of join (INNER/LEFT).
     * @param string        $whereString A string representing an additional join clause. Used for the clauses defined in table_links.
     * 
     * @throws \InvalidArgumentException If the type is not either 'INNER' or 'LEFT'
     * 
     * @return Query
     */
    public function join($table, $where, $type = 'INNER', $whereString = '', $alias = '')
    {
        if (!in_array($type, array('INNER','LEFT','RIGHT')))
        {
            throw new \InvalidArgumentException('Joins must be of type "INNER", "RIGHT" or "LEFT"');
        }

        $this->joins[$table] = array('where' => $where, 'wherestring' => $whereString, 'type' => $type, 'alias' => $alias);
        return $this;
    }
    
    /**
     * Set a cross apply condition.
     * 
     * @param string $alias The alias for the sub query.
     * @param Query  $query The sub query.
     * 
     * @return Query
     */
    public function crossApply($alias, Query $query)
    {
        $this->crossApply[$alias] = $query;
        return $this;
    }
    
    /**
     * Setter for where.
     * 
     * Can either accept an array of key/values pairs in order to build up a simple WHERE statement (i.e. 'WHERE key = value AND key = value' etc)
     * or can accept a Where object that has been manually constructed in situations where a more complex query is needed.
     * When passing an array, each element value can either be a string to compare to a single value, or an array when comparing the field to multiple values.
     * There where clause is appended to if this method is called several times on the same object.
     * 
     * @param array|Where $where
     * @param int         $options
     * 
     * @throws \InvalidArgumentException If the argument is neither an array nor a Where object.
     * 
     * @return Query
     */
    public function where($where, $options = self::WHERE_DEFAULT)
    {
        if (!is_array($where) && !($where instanceof Where))
        {
            throw new \InvalidArgumentException('Argument must be an array or an instance of Where');
        }
        
        if (is_array($where))
        {
            $where = $this->buildWhere($where);
        }
        
        $fields = $where->getFieldList();

        //add extra field joins if required.
        $this->createExtraFieldJoins($fields);
        
        //add tables from where fields
        if (self::WHERE_NO_TABLES != $options && is_array($fields))
        {
        	$this->addTableFromFieldList($fields);
        }
        
        unset( $fields );

        if (!empty($this->extraFields))
        {
            // convert extra field references to pre-generated aliases
            foreach ($this->extraFields as $current => $new)
            {
                $where->convertFieldName($current, $new);
            }
        }

        if ($this->where === null)
        {
            $this->where = $where;
        }
        else
        {
            // append the new query conditions to the existing where object
            $this->where->add($where->getCriteria());
        }
        
        return $this;
    }
    
    /**
     * Add tables from where fields
     * 
     * @author gszucs
     * @param array $fields
     */
    protected function addTableFromFieldList( array $fields )
    {
    	foreach ($fields as $field)
    	{
    		if( ( $details = explode('.', $field, 2) ) && count( $details ) == 2 )
    		{
    			$this->addTable($details[0]);
    		}
    	}
    }
    
    /**
     * Constructs a Where object from an array of paramaters.
     * 
     * @param array $where
     * 
     * @return Where
     */
    protected function buildWhere(array $where)
    {
        $fields = $this->factory->getFieldCollection();
        foreach ($where as $field => $value)
        {
            $fields->field($field);
            if (is_array($value))
            {
                $fields->in($value);
            }
            else
            {
                $fields->eq($value);
            }
        }
        
        $where = $this->factory->getWhere();
        $where->add($fields);
        
        return $where;
    }
    
    /**
     * Setter for orderBy.
     * 
     * @param array   $orderBy   The fields/associated order directions that make up the clause.  Can be passed in using the following interchangable formats:
     *                               array('table.field1', 'table.field2', ...) - order by each field in turn in ascending order;
     *                               array(array('table.field1', 'ASC'), array('table.field2', 'DESC'), ...) - order by each field in turn using the defined direction for each field;
     *                               array(array(array('YEAR(?)', 'incidents_main.inc_dincident'), 'DESC'), ...) - a means for ordering by more complicated statements, using the same format when
     *                                   using functions/aliases etc in a SELECT clause (see {@link Query::select()}).
     *                               
     * @param boolean $overwrite Whether to overwrite the existing order by conditions.
     * 
     * @throws \InvalidArgumentException If the array argument is of an invalid format.
     * 
     * @return Query
     */
    public function orderBy(array $orderBy, $overwrite = false)
    {
        if ($overwrite)
        {
            $this->orderBy = array();
        }
        
        foreach ($orderBy as $field)
        {
            if (is_array($field))
            {
                if (!in_array($field[1], array('ASC','DESC')))
                {
                    throw new \InvalidArgumentException('Argument must be in the format array(\'field\', \'ASC|DESC\')');
                }
                $this->extractTables(array($field[0]));
                
                if (array_key_exists($field[0], $this->extraFields))
                {
                    // convert extra field references to pre-generated aliases
                    $field[0] = $this->extraFields[$field[0]];
                }
                
                $this->orderBy[] = $field;
            }
            else
            {
                $this->extractTables(array($field));
                
                if (array_key_exists($field, $this->extraFields))
                {
                    // convert extra field references to pre-generated aliases
                    $field = $this->extraFields[$field];
                }
                
                $this->orderBy[] = array($field, 'ASC');
            }
        }
        
        return $this;
    }
    
    /**
     * Setter for limit.
     * 
     * The offset of the initial row is 0 (not 1), so limit(5,10) returns rows 6 - 15.
     * 
     * @param int $start  The initial row.
     * @param int $number The number of records.
     * 
     * @return Query
     */
    public function limit($start, $number)
    {
        $this->limit = array((int) $start, (int) $number);
        return $this;
    }
    
    /**
     * Unsets the limit.
     * 
     * @return Query
     */
    public function unsetLimit()
    {
        $this->limit = [];
        return $this;
    }
    
    /**
     * Setter for groupBy.
     * 
     * @param array $groupBy
     * 
     * @return Query
     */
    public function groupBy(array $groupBy, $overwrite = false)
    {
        $this->extractTables($groupBy);
        
        if (!empty($this->extraFields))
        {
            // convert extra field references to pre-generated aliases
            foreach ($groupBy as $key => $field)
            {
                if (is_array($field))
                {
                    // when the field is part of a more complex contruct (e.g. uses an alias, wrapped in a function etc)
                    if (is_array($field[1]))
                    {
                        foreach ($field[1] as $nameKey => $name)
                        {
                            if (array_key_exists($name, $this->extraFields))
                            {
                                $groupBy[$key][1][$nameKey] = $this->extraFields[$name];
                            }
                        }
                    }
                    else
                    {
                        if (array_key_exists($field[1], $this->extraFields))
                        {
                            $groupBy[$key][1] = $this->extraFields[$field[1]];
                        }
                    }
                }
                else
                {
                    if (array_key_exists($field, $this->extraFields))
                    {
                        $groupBy[$key] = $this->extraFields[$field];
                    }
                }
            }
        }

        if ($overwrite)
        {
            $this->groupBy = $groupBy;
        }
        else
        {
            $this->groupBy = array_merge($this->groupBy, $groupBy);
        }
        
        return $this;
    }
    
    /**
     * Initiates an INSERT statement.
     * 
     * Assigns the fields/values to be used as part of the statement.
     * 
     * @param array $fields
     * 
     * @return Query
     */
    public function insert(array $fields)
    {
        $this->type   = self::INSERT;
        $this->fields = array_merge($this->fields, $fields);
        return $this;
    }
    
    /**
     * Assign the table name to be used in the INSERT statement.
     * 
     * @param string $table
     * 
     * @return Query
     */
    public function into($table)
    {
        $this->table = $table;
        return $this;
    }
    
    /**
     * Initiates an UPDATE statement.
     * 
     * Identifies the table to update.
     * 
     * @param string $table
     * 
     * @return Query
     */
    public function update($table)
    {
        $this->type  = self::UPDATE;
        $this->table = $table;
        return $this;
    }
    
    /**
     * Defines the fields/values to be used in the UPDATE statement.
     * 
     * @param array $fields
     * 
     * @return Query
     */
    public function set(array $fields)
    {
        $this->fields = array_merge($this->fields, $fields);
        return $this;
    }
    
    /**
     * Initiates a DELETE statement.
     * 
     * @return Query
     */
    public function delete()
    {
        $this->type = self::DELETE;
        return $this;
    }
    
    /**
     * Getter for type.
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Getter for top.
     * 
     * @return int
     */
    public function getTop()
    {
        return $this->top;
    }
    
    /**
     * Getter for fields.
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }
    
    /**
     * Getter for from.
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }
    
    /**
     * Add a table
     * 
     * @author gszucs
     * @param string $table
     * @return Query
     */
    public function addTable( $table )
    {
    	if (!in_array($table, $this->tables) && $table != $this->table)
    	{
    		$this->tables[] = $table;
    	}
    	return $this;
    }
    
    /**
     * Getter for tables.
     *
     * @return array
     */
    public function getTables()
    {
        return $this->tables;
    }
    
    /**
     * Getter for joins.
     * 
     * Attempts to automatically create join information if not already manually set.
     *
     * @return array
     */
    public function getJoins()
    {
        foreach ($this->tables as $table)
        {
            if ($table != $this->table && !in_array($table, array_keys($this->joins)))
            {
                if (!isset($tableDefs))
                {
                    $tableDefs = $this->registry->getTableDefs();
                }
                
                if (isset($tableDefs[$this->table]->link_tables[$table]))
                {
                    foreach ($tableDefs[$this->table]->link_tables[$table]->route as $link)
                    {
                        // HACK need to fix metadata so table_routes.trt_default is applied consistently so we don't have to hardcode contacts_main here 
                        if ('contacts_main' == $table && !$link->isDefault())
                        {
                            continue;
                        }
                        
                        $params = $this->factory->getFieldCollection();
                        $where  = $this->factory->getWhere();
                        
                        // the join is defined once for both directions, so need to determine which table we're joining to
                        $joinTable = $link->tlk_table1 != $this->table && !in_array($link->tlk_table1, array_keys($this->joins)) ? $link->tlk_table1 : $link->tlk_table2;
                        
                        foreach ($link->conditions as $condition)
                        {
                            $params->field($condition[0])->eq($condition[1], true);
                        }
        
                        $where->add($params);
                        $this->join($joinTable, $where, 'INNER', $link->tlk_where);
                    }
                }
            }
        }
        
        // if the udf_values table is the first in the array of joins, we need to put it to the bottom of the array otherwise it will cause SQL error
        if( ( $orig_key = array_keys( $this->joins )[0] ) && ( strpos( $orig_key, 'udf_values' ) !== false ) )
        {
        	$tmp = $this->joins[ $orig_key ];
        	unset( $this->joins[ $orig_key ] );
        	$this->joins[ $orig_key ] = $tmp;
        	unset( $orig_key, $tmp );
        }
        
        return $this->joins;
    }
    
    /**
     * Getter for crossApply.
     * 
     * @return array
     */
    public function getCrossApply()
    {
        return $this->crossApply;
    }
    
    /**
     * Getter for where.
     *
     * @return Where
     */
    public function getWhere()
    {
        return $this->where;
    }
    
    /**
     * Getter for orderBy.
     *
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }
    
    /**
     * Getter for groupBy.
     *
     * @return array
     */
    public function getGroupBy()
    {
        return $this->groupBy;
    }
    
    /**
     * Getter for modules.
     * 
     * Automatically determines the modules involved in this query using the tables property.
     *
     * @return array
     */
    public function getModules()
    {
        if ($this->modules === null)
        {
            $this->modules = [];
            foreach (array_merge($this->tables, [$this->table], array_keys($this->joins)) as $table)
            {
                foreach ($this->registry->getModuleDefs() as $module => $config)
                {
                    if (in_array($table, [$config['TABLE'], $config['VIEW']]) && !in_array($module, $this->modules))
                    {
                        $this->modules[] = $module;
                        break;
                    }
                }
            }
        }
        return $this->modules;
    }
    
    /**
     * Setter for modules.
     * 
     * @param array
     */
    public function setModules(array $modules)
    {
        if ($this->modules === null)
        {
            $this->modules = $modules;
        }
        else
        {
            $this->modules = array_merge($this->modules, $modules);
        }
    }
    
    /**
     * Getter for whereStrings.
     * @return array
     */
    public function getwhereStrings()
    {
        return $this->whereStrings;
    }
    
    /**
     * Setter for whereStrings.
     * @param array $whereStrings
     */
    public function setWhereStrings(array $where)
    {
        $this->whereStrings = $where;
    }
    
    /**
     * Getter for limit.
     * 
     * @return array
     */
    public function getLimit()
    {
        return $this->limit;
    }
    
    /**
     * Determines whether or not this query is complete (i.e. ready to be executed).
     * 
     * @return boolean
     */
    public function isComplete()
    {
        if (!isset($this->type))
        {
            return false;
        }
        
        // select/insert/update query require fields and table
        if (in_array($this->type, array(self::SELECT, self::INSERT, self::UPDATE)) && (empty($this->fields) || !isset($this->table)))
        {
            return false;
        }
        
        // delete only requires a table (although a where clause is advisable!)
        if ($this->type == self::DELETE && !isset($this->table))
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * Replace the occurance of a field name anywhere within the query.
     * 
     * @param string $current The name of an existing field within the query.
     * @param mixed  $new     The name of the field we wish to convert to.
     */
    public function convertFieldName($current, $new)
    {
        // resolve new field name to string in cases when it's an array (in the form ['wrapper', 'field name'])
        if (is_array($new))
        {
            if (!is_array($new[1]))
            {
                $new[1] = [$new[1]];
            }
            
            foreach ($new[1] as $field)
            {
                $new[0] = implode($field, explode('?', $new[0], 2));
            }
            $new = $new[0];
        }
        
        // convert fields in select clause
        foreach ($this->fields as $key => $field)
        {
            if (is_array($field) && $field[1] == $current)
            {
                $this->fields[$key][1] = $new;
            }
            elseif ($field == $current)
            {
                $this->fields[$key] = $new;
            }
        }
        
        // convert fields in join clauses
        foreach ($this->joins as $join)
        {
            if ($join['where'] instanceof Where)
            {
                $join['where']->convertFieldName($current, $new);
            }
            // TODO string replace if not a Where object?
        }
        
        // convert fields in where clause
        if (null !== $this->where)
        {
            $this->where->convertFieldName($current, $new);
        }
        
        // convert fields in group by clause
        foreach ($this->groupBy as $key => $groupBy)
        {
            if (is_array($groupBy) && $groupBy[1] == $current)
            {
                $this->groupBy[$key][1] = $new;
            }
            elseif ($groupBy == $current)
            {
                $this->groupBy[$key] = $new;
            }
        }
        
        // convert fields in order by clause
        foreach ($this->orderBy as $key => $orderBy)
        {
            if (is_array($orderBy[0]) && $orderBy[0][1] == $current)
            {
                $this->orderBy[$key][0][1] = $new;
            }
            elseif ($orderBy[0] == $current)
            {
                $this->orderBy[$key][0] = $new;
            }
        }
    }
    
    /**
     * Removes table names from fully-qualified field names so we can keep
     * track of the tables involved in this query.
     * 
     * @param array $fields
     */
    protected function extractTables(array $fields)
    {
        foreach ($fields as $field)
        {
            if (is_array($field))
            {
                // when the field is part of a more complex contruct (e.g. uses an alias, wrapped in a function etc)
                $fieldNames = is_array($field[1]) ? $field[1] : [$field[1]];
            }
            else
            {
                $fieldNames = [$field];
            }
            
            foreach ($fieldNames as $fieldName)
            {
                if (\UnicodeString::strpos($fieldName, '.') !== false)
                {
                    // capture the table names so we can handle joins
                    $parts = explode('.', $fieldName);
                    if (!in_array($parts[0], $this->tables))
                    {
                        $this->tables[] = $parts[0];
                    }
                }
            }
        }
    }
    
    /**
     * Setter for overrideSecurity.
     */
    public function overrideSecurity()
    {
        $this->overrideSecurity = true;
    }
    
    /**
     * Getter for overrideSecurity.
     * @return boolean.
     */
    public function isSecurityOverriden()
    {
        return $this->overrideSecurity;
    }
    
    /**
     * Automatically constructs the clauses required to query on extra fields.
     * 
     * @param array $fields A list of field names.
     * 
     * @return array $fields The field names with the replaced UDF aliases.
     */
    protected function createExtraFieldJoins($fields = array())
    {
        if (!$this->autoCreateExtraFieldJoins)
        {
            return $fields;
        }
        
        $fieldDefs = $this->registry->getFieldDefs();
        foreach ($fields as $key => $field)
        {
            $fieldName = is_array($field) ? ( is_array( $field[1] ) ? ( is_array( $field[1][1] ) ? $field[1][1][0] : $field[1][0] ) : $field[1] ) : $field;
            if (null !== ($fieldDef = $fieldDefs[$fieldName]) && $fieldDef instanceof ExtraField)
            {
                //need to make sure we don't create duplicate links for udfs referenced more than once.
                if($this->extraFields[$fieldName] != '')
                {
                    $newFieldName = $this->extraFields[$fieldName];
                    $udfValuesAlias = explode('.', $newFieldName)[0];
                }
                else
                {
                    $udfValuesAlias = 'udf_'.(count($this->extraFields) + 1);
                    $newFieldName = $udfValuesAlias.'.'.$fieldDef->getValueField();
                    $this->extraFields[$fieldName] = $newFieldName;
                }

                if (is_array($field))
                {
                    if (is_array($fields[$key][1]))
                    {
                        // replace all matching occurances in the field array with the new alias
                        $fields[$key][1] = array_replace($fields[$key][1],
                            array_fill_keys(
                                array_keys($fields[$key][1], $fieldName),
                                $newFieldName
                            )
                        );
                    }
                    else
                    {
                        $fields[$key][1] = $newFieldName;
                    }
                }
                else
                {
                    $fields[$key] = $newFieldName;
                }
                
                $table = explode('.', $fieldName)[0];
                
                list($subQuery, $where, $conditions) = $this->factory->getQueryObjects();
                
                $conditions->field($table.'.recordid')->eq($udfValuesAlias.'.cas_id', true)
                           ->field($udfValuesAlias.'.mod_id')->eq(GetModIDFromShortName(getModuleFromTable($table)))
                           ->field($udfValuesAlias.'.field_id')->eq($fieldDef->recordid);
                
                if ($fieldDef->group_id !== null)
                {
                    $conditions->field($udfValuesAlias.'.group_id')->eq($fieldDef->group_id);
                }
                
                $where->add($conditions);
                
                $this->join('udf_values AS '.$udfValuesAlias, $where, 'LEFT');
            }
        }
        return $fields;
    }

    /**
     * Builds the criteria used when querying for records based on a predefined listing.
     * 
     * @param string  $module   The module code.
     * @param string  $listType The overdue listing type.
     * @param boolean $overdue  Whether or not the listing is an overdue records listing.
     */
    public function setPredefinedListingCriteria($module, $listType, $overdue)
    {
        $moduleDefs = $this->registry->getModuleDefs();
        
        if (isset($moduleDefs[$module]['HARD_CODED_LISTINGS']))
        {
            $this->setHardcodedListingCriteria($module, $listType, $overdue);
        }
        else
        {
            $this->setGenericListingCriteria($module, $listType, $overdue);
        }
    }
    
    /**
     * Builds the criteria used when querying for records based on a 'hard-coded' predefined listing.
     * 
     * @param string  $module   The module code.
     * @param string  $listType The overdue listing type.
     * @param boolean $overdue  Whether or not the listing is an overdue records listing.
     */
    protected function setHardcodedListingCriteria($module, $listType, $overdue)
    {
        $moduleDefs = $this->registry->getModuleDefs();
        
        $where = $overdue ? 'OverdueWhere' : 'Where';
        
        if ($moduleDefs[$module]['HARD_CODED_LISTINGS'][$listType][$where])
        {
            $this->setwhereStrings([ $moduleDefs[$module]['HARD_CODED_LISTINGS'][$listType][$where] ]);
        }
        else
        {
            throw new QueryException('Unable to determine predefined listing criteria');
        }
    }
    
    /**
     * Builds the criteria used when querying for records based on a generic (i.e. approval status-based) predefined listing.
     * 
     * @param string  $module         The module code.
     * @param string  $approvalStatus The approval status code.
     * @param boolean $overdue        Whether or not the listing is an overdue records listing.
     */
    protected function setGenericListingCriteria($module, $approvalStatus, $overdue)
    {
        if ($overdue)
        {
            $this->setGenericOverdueListingCriteria($module, $approvalStatus);
        }
        else
        {
            // we can just build a standard query object for normal approval status listings
            $moduleDefs = $this->registry->getModuleDefs();
            $table = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];
        
            $this->where([$table.'.rep_approved' => $approvalStatus]);
        }
    }
    
    /**
     * Builds the criteria used when querying for generic overdue listings based on approval status codes.
     * 
     * @param string $module         The module code.
     * @param string $approvalStatus The approval status code.
     */
    protected function setGenericOverdueListingCriteria($module, $approvalStatus)
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $where      = new Where();
        $comparison = '<='; // changes to '<' when we're using working days to handle records that have been modified on weekends
        $Statuses   = getOverdueStatuses(array('module' => $module));
        $criteria   = [];

        $where->add($this->factory->getFieldCollection()->field($ModuleDefs[$module]['TABLE'].'.rep_approved')->like($approvalStatus));
        
        if (!empty($approvalStatus) && empty($Statuses[$approvalStatus])) //cannot be overdue.
        {
            $this->where(array('1' => '2'));
            return;
        }
        
        $OverdueDays = GetOverdueDays($module, $approvalStatus);
        $onDate = new \DateTime();

        if (!empty($approvalStatus) && $OverdueDays[$approvalStatus] >= 0)
        {
            if ($OverdueDays[$approvalStatus.'_type'] == 'W')
            {
                // we're calculating the overdue date based on working days
                $OverdueDays[$approvalStatus] = CalculateWorkingDays($OverdueDays[$approvalStatus], $onDate, true);
                $comparison = '<';
            }

            $MyDate = $onDate->modify('-'.((int) $OverdueDays[$approvalStatus]).' day')->format('Y-m-d');

            if (empty($OverdueDays['overdue_date_field']))
            {
                // we could eventually allow users to decide whether the overdue date is calculated from
                // the first time a record reaches an approval status, or the most recent time.
                // e.g. $minMax = $OverdueDays['overdue_calculated_from'] == 'earliest' ? 'MIN' : 'MAX';
                // for now we always look for the most recent date a record reached its current approval status,
                // effectively resetting the clock if a record is moved back to a previous status
                $minMax = 'MAX';

                $subQuery = new Query();
                $fieldsArray = array(
                    'aud_record',
                    'DATEADD(day, DATEDIFF(day, \'20000101\', ' . $minMax . '(aud_date)), \'20000101\') AS auditdate'
                );
                $whereArray = array(
                    'aud_module' => $module,
                    'aud_action' => 'WEB:rep_approved',
                    'CAST(aud_detail AS varchar(6))' => $approvalStatus
                );
                $subQuery->select($fieldsArray)->from('full_audit')->where($whereArray)->groupBy(array('aud_record'));
                $whereString = 'fa.aud_record = '.$ModuleDefs[$module]['TABLE'].'.recordid';
                $this->joinSubSelect($subQuery, 'fa', $whereString, 'LEFT');

                // in addition to checking the audit date, we need to check
                // against the OVERDUE_CHECK_FIELD in cases where this record has
                // no full_audit entry (i.e. this record has never been updated)
                if ($comparison == '<=')
                {
                    $criteria[] = $this->factory->getFieldCollection()->field('fa.auditdate')->ltEq($MyDate);
                    $criteria[] = $this->factory->getFieldCollection()
                                       ->field('fa.auditdate')->isNull()
                                       ->field($ModuleDefs[$module]['OVERDUE_CHECK_FIELD'])->ltEq($MyDate);
                }
                elseif ($comparison = '<')
                {
                    $criteria[] = $this->factory->getFieldCollection()->field('fa.auditdate')->lt($MyDate);
                    $criteria[] = $this->factory->getFieldCollection()
                                       ->field('fa.auditdate')->isNull()
                                       ->field($ModuleDefs[$module]['OVERDUE_CHECK_FIELD'])->lt($MyDate);
                }
            }
            else
            {
                // we've specified a field to calculate the overdue date from for this approval status
                if ($comparison == '<=')
                {
                    $criteria[] = $this->factory->getFieldCollection()->field($OverdueDays['overdue_date_field'])->ltEq($MyDate);
                }
                elseif ($comparison = '<')
                {
                    $criteria[] = $this->factory->getFieldCollection()->field($OverdueDays['overdue_date_field'])->lt($MyDate);
                }
            }
        }

        if ($OverdueDays['overall'] >= 0)
        {
            // we need to take the overall timescale into consideration
            if ($OverdueDays['overall_type'] == 'W')
            {
                // we're calculating the overdue date based on working days
                $OverdueDays['overall'] = CalculateWorkingDays($OverdueDays['overall'], $onDate, true);
                $comparison = '<';
            }

            $MyDate = $onDate->modify('-'.((int) $OverdueDays['overall']).' day')->format('Y-m-d');

            if ($comparison == '<=')
            {
                $criteria[] = $this->factory->getFieldCollection()->field($ModuleDefs[$module]['OVERDUE_CHECK_FIELD'])->ltEq($MyDate);
            }
            elseif ($comparison = '<')
            {
                $criteria[] = $this->factory->getFieldCollection()->field($ModuleDefs[$module]['OVERDUE_CHECK_FIELD'])->lt($MyDate);
            }
        }
        
        if (!empty($criteria))
        {
            array_unshift($criteria, 'OR');
            $where->addArray($criteria);
            $this->where($where);
        }
    }

    /**
     * Sets the join that uses a subquery
     *
     * @param Query  $subQuery The subquery used in the join.
     * @param string $alias    The alias for the subquery.
     * @param string $where    The join where.
     * @param string $type     The type of join.
     *
     * @throws \InvalidArgumentException If the type is not either 'INNER' or 'LEFT'
     */
    public function joinSubSelect(Query $subQuery, $alias, $where, $type = 'INNER')
    {
        if (!in_array($type, array('INNER','LEFT')))
        {
            throw new \InvalidArgumentException('Joins must be of type "INNER" or "LEFT"');
        }

        $this->joins[$alias] = array('subQuery' => $subQuery, 'where' => $where, 'type' => $type);
    }

    /**
     * Creates a SavedQuery object from the query parameters.
     * 
     * @throws QueryException If this object doesn't have the necessary properties to create a SavedQuery.
     *
     * @return SavedQuery
     */
    public function createSavedQuery(Registry $registry = null)
    {
        if ($this->table === null)
        {
            throw new QueryException('Unable to create SavedQuery: table property not set.');
        }

        if ($this->where === null)
        {
            throw new QueryException('Unable to create SavedQuery: where property not set.');
        }

        $registry = $registry ?: Registry::getInstance();
        foreach ($registry->getModuleDefs() as $mod => $params)
        {
            if (in_array($this->table, [$params['VIEW'], $params['TABLE']]))
            {
                $module = $mod;
                break;
            }
        }

        if ($module == '')
        {
            throw new QueryException('Unable to create SavedQuery: cannot determine module.');
        }

        $savedQuery = new SavedQuery();
        $savedQuery->module = $module;
        $savedQuery->where = clone $this->where;

        return $savedQuery;
    }
    
    /**
     * Whether or not the query conditions contain any @prompt codes.
     * 
     * @return boolean
     */
    public function hasAtPrompt()
    {
        $atPrompt = false;
        if ($this->where !== null)
        {
            $atPrompt = $this->where->hasAtPrompt();
        }
        
        if (!$atPrompt && !empty($this->whereStrings))
        {
            foreach ($this->whereStrings as $whereString)
            {
                if (\UnicodeString::stripos($whereString, '@PROMPT') !== false)
                {
                    $atPrompt = true;
                    break;
                }
            }
        }
        
        return $atPrompt;
    }
    
    /**
     * Whether or not the query conditions contain any @code values.
     * 
     * @return boolean
     */
    public function hasAtCode()
    {
        $atCode = false;
        if ($this->where !== null)
        {
            $atCode = $this->where->hasAtCode();
        }
        
        if (!$atCode && !empty($this->whereStrings))
        {
            foreach ($this->whereStrings as $whereString)
            {
                if (\UnicodeString::strpos($whereString, '@') !== false)
                {
                    $atCode = true;
                    break;
                }
            }
        }
        
        return $atCode;
    }
    
    /**
     * Replaces instances of @codes within the query conditions.
     */
    public function replaceAtCodes()
    {
        // TODO The idea here is that this method delegates to a set of individual methods that are each responsible for handling
        //      an @code type.  At the moment the only type that's catered for here is @prompt, but eventually all of the @code
        //      replacement logic should be moved out of SqlWriter::replaceAtCodes() and catered for in the same way.
        $this->replaceAtPrompts();
        
        if (!empty($this->whereStrings))
        {
            foreach ($this->whereStrings as $key => $whereString)
            {
                $this->whereStrings[$key] = TranslateWhereCom($whereString);
            }
        }
    }
    
    /**
     * Replace @prompt codes in the query with their literal values.
     * 
     * @param string $module The module code is required in order to replace @prompt codes in where strings.
     * 
     * @throws QueryException If there are @prompts that need replacing within where strings and we don't know the module context.
     */
    public function replaceAtPrompts($module = null)
    {
        if ($this->where !== null)
        {
            $this->where->replaceAtPrompts();
        }
        
        if (!empty($this->whereStrings))
        {
            require_once 'Source/libs/SavedQueries.php';
            
            foreach ($this->whereStrings as $key => $whereString)
            {
                if (\UnicodeString::stripos($whereString, '@PROMPT') !== false)
                {
                    if ($module == '')
                    {
                        throw new QueryException('Unable to replace @prompts within where string: the module context is unknown');
                    }
                    $this->whereStrings[$key] = replaceAtPrompts($whereString, $module);
                }
            }
        }
    }

    /**
     * Retrieves a list of field names from the query criteria that use @prompt as their comparison.
     * 
     * @param string $module The module code is required in order to retrieve @prompt fields from where strings.
     * 
     * @return array
     */
    public function getAtPromptFields($module = null)
    {
        if (isset($this->where))
        {
            return $this->where->getAtPromptFields();
        }
        
        if (!empty($this->whereStrings))
        {
            // old method of pattern matching in where strings
            require_once 'Source/libs/SavedQueries.php';
            
            $atPromptFields = [];
            
            foreach ($this->whereStrings as $whereString)
            {
                if (\UnicodeString::stripos($whereString, '@PROMPT') !== false)
                {
                    if ($module == '')
                    {
                        throw new QueryException('Unable to retrieve @prompt fields from where string: the module context is unknown');
                    }
                    
                    $matches = CheckForPrompt($whereString, $module);
                    
                    foreach ($matches as $match)
                    {
                        $field = $this->registry->getFieldDefs()[$match['field']];

                        if ($field == '')
                        {
                            throw new QueryException('Unable to retrieve @prompt fields from where string: fields must use fully qualified name.');
                        }

                        if( in_array( $field->getType(), [FieldInterface::DATE, FieldInterface::CODE, FieldInterface::YESNO, FieldInterface::MULTICODE] ) )
                        {
                        	//Odd capitalisation can cause problems when these fields are compared to field metadata collections - hence using lowercase only.
                        	$atPromptFields[] = \UnicodeString::strtolower($match['field']);
                        }
                    }
                }
            }
            
            return array_unique($atPromptFields);
        }
        
        return [];
    }
    
    /**
     * Whether or not this query object has no parameters.
     * 
     * @return boolean
     */
    public function isEmpty()
    {
        return $this == new self();
    }
    
    /**
     * Setter for autoCreateExtraFieldJoins.
     * 
     * @param boolean $bool
     */
    public function setAutoCreateExtraFieldjoins($bool)
    {
        $this->autoCreateExtraFieldJoins = (bool) $bool;
    }
}
