<?php
namespace src\framework\query\exceptions;

class UndefinedFieldException extends QueryException{}