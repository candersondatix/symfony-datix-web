<?php
namespace src\framework\query\exceptions;

class IncompleteFieldException extends QueryException{}