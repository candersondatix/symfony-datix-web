<?php
namespace src\framework\query;

/**
 * Encapsulates the criteria used to execute a query.
 */
class Where
{
    /**
     * One or more field collection combined to describe this WHERE condition.
     * 
     * @var array
     */
    protected $criteria;
    
    /**
     * Whether the criteria contains an @ code.
     * 
     * @var boolean
     */
    protected $hasAtCode;
    
    /**
     * Whether the criteria contains an @prompt code.
     * 
     * @var boolean
     */
    protected $hasAtPrompt;
    
    /**
     * The @prompt fields defined within the criteria (if any).
     * 
     * @var array
     */
    protected $atPromptFields;
    
    public function __construct()
    {
        $this->criteria = [
            'parameters' => [],
            'condition'  => 'AND'
        ];
    }
    
    public function __clone()
    {
        array_walk_recursive($this->criteria, [ $this, 'doClone' ]);
    }
    
    /**
     * The callable passed to array_walk_recursive by Where::__clone().
     * 
     * We avoid using an anonymous function here because for some reason it disagrees with ionCube.
     */
    protected function doClone(&$value)
    {
        if ($value instanceof FieldCollection)
        {
            $value = clone $value;
        }
    }
    
    /**
     * Getter for criteria.
     */
    public function getCriteria()
    {
        return $this->criteria;
    }
    
    /**
     * Setter for criteria.
     * 
     * @param array $criteria
     */
    public function setCriteria(array $criteria)
    {
        $this->checkCriteria($criteria);
        $this->criteria = $criteria;
    }
    
    /**
     * Used to add sets of query parameters and the conditions which join them to the set of criteria that make up this object.
     * 
     * Uses a variable-length argument list.
     * 
     * @param string                The first argument can optionally be the condition used to add the FieldCollection(s) (AND|OR).
     * @param array|FieldCollection Each subsequent argument should be a FieldCollection, or a set of FieldCollections grouped into an array.
     */
    public function add()
    {
        $this->criteria['parameters'][] = $this->buildCriterion(func_get_args());
    }

    /**
     * Convenience method which allows passing in of an array, rather than a variable-length argument list.
     *
     * @param array $args An array containing the arguments that would be passed to {@link Where::add()} as individual arguments.
     */
    public function addArray(array $args)
    {
        $this->criteria['parameters'][] = $this->buildCriterion($args);
    }
    
    /**
     * Used to nest sets of query parameters within the object conditions to enable construction of more complex criteria sets.
     * 
     * Uses the same method signature as {@link Where::add()}.
     * 
     * @param string                The first argument can optionally be the condition used to add the FieldCollection(s) (AND|OR).
     * @param array|FieldCollection Each subsequent argument should be a FieldCollection, or a set of FieldCollections grouped into an array.
     * 
     * @return array
     */
    public function nest()
    {
        return $this->buildCriterion(func_get_args());
    }

    /**
     * Convenience method which allows passing in of an array, rather than a variable-length argument list.
     *
     * @param array $args   An array containing the arguments that would be passed to {@link Where::nest()} as individual arguments.
     * @return array
     */
    public function nestArray(array $args)
    {
        return $this->buildCriterion($args);
    }
    
    /**
     * Used to convert the name of one of the fields anywhere within the criteria.
     * 
     * Useful when mapping entity properties or dealing with aliases.
     * 
     * @param string $current The name of an existing field within the criteria.
     * @param string $new     The name of the field we wish to convert to.
     */
    public function convertFieldName($current, $new)
    {
        array_walk_recursive($this->criteria, [ $this, 'doConvertFieldName' ], [ $current, $new ]);
    }
    
    /**
     * The callable passed to array_walk_recursive by Where::convertFieldName().
     * 
     * We avoid using an anonymous function here because for some reason it disagrees with ionCube.
     */
    protected function doConvertFieldName($value, $key, $conversion)
    {
        if ($value instanceof FieldCollection)
        {
            $value->convertFieldName($conversion[0], $conversion[1]);
        }
    }
    
    /**
     * Whether or not the criteria contain any @ codes.
     * 
     * @return boolean
     */
    public function hasAtCode()
    {
        $this->hasAtCode = false;
        array_walk_recursive($this->criteria, [ $this, 'doHasAtCode' ]);
        return $this->hasAtCode;
    }
    
    /**
     * The callable passed to array_walk_recursive by Where::hasAtCode().
     * 
     * We avoid using an anonymous function here because for some reason it disagrees with ionCube.
     */
    protected function doHasAtCode($value)
    {
        if (!$this->hasAtCode && $value instanceof FieldCollection)
        {   
            $this->hasAtCode = $value->hasAtCode();
        }
    }
    
    /**
     * Whether or not the criteria contain any @ codes.
     * 
     * @return boolean
     */
    public function hasAtPrompt()
    {
        $this->hasAtPrompt = false;
        array_walk_recursive($this->criteria, [ $this, 'doHasAtPrompt' ]);
        return $this->hasAtPrompt;
    }
    
    /**
     * The callable passed to array_walk_recursive by Where::hasAtPrompt().
     * 
     * We avoid using an anonymous function here because for some reason it disagrees with ionCube.
     */
    protected function doHasAtPrompt($value)
    {
        if (!$this->hasAtPrompt && $value instanceof FieldCollection)
        {   
            $this->hasAtPrompt = $value->hasAtPrompt();
        }
    }
    
    /**
     * Replace @prompt codes in criteria with their literal values.
     */
    public function replaceAtPrompts()
    {
        array_walk_recursive($this->criteria, [ $this, 'doReplaceAtPrompts' ]);
    }
    
    /**
     * The callable passed to array_walk_recursive by Where::replaceAtPrompts().
     * 
     * We avoid using an anonymous function here because for some reason it disagrees with ionCube.
     */
    protected function doReplaceAtPrompts($value)
    {
        if ($value instanceof FieldCollection)
        {
            $value->replaceAtPrompts();
        }
    }
    
    /**
     * Retrieves a list of field names from the query criteria that use @prompt as their comparison.
     * 
     * @return array
     */
    public function getAtPromptFields()
    {
        $this->atPromptFields = [];
        array_walk_recursive($this->criteria, [ $this, 'doGetAtPromptFields' ]);
        return $this->atPromptFields;
    }
    
    /**
     * The callable passed to array_walk_recursive by Where::getAtPromptFields().
     * 
     * We avoid using an anonymous function here because for some reason it disagrees with ionCube.
     */
    protected function doGetAtPromptFields($value)
    {
        if ($value instanceof FieldCollection)
        {
            $this->atPromptFields = array_merge($this->atPromptFields, $value->getAtPromptFields());
        }
    }
    
    /**
     * Used to validate the arguments passed to {@link Where::add()} and {@link Where::nest()} and return a set of criteria.
     * 
     * @param array $args The arguments passed from {@link Where::add()} or {@link Where::nest()}.
     * 
     * @return array
     */
    protected function buildCriterion(array $args)
    {
        if (!in_array($args[0], array('AND','OR')))
        {
            // default to an AND condition if not specified in the first argument.
            $condition = 'AND';
        }
        else
        {
            $condition = array_shift($args);
        }

        $parameters = array();

        foreach ($args as $arg)
        {
            if ($arg instanceof Where)
            {
                $parameters[] = $arg->getCriteria();
            }
            else
            {
                $parameters[] = $arg;
            }
        }

        foreach ($parameters as $parameter)
        {
            $this->checkArg($parameter);
        }

        return array(
            'parameters' => $parameters,
            'condition'  => $condition
        );
    }
    
    /**
     * Checks that the argument passed to {@link Where::add()} is of a correct type.
     * 
     * @param array|FieldCollection $arg
     * 
     * @throws \InvalidArgumentException If the argument is of an incorrect type.
     */
    protected function checkArg($arg)
    {
        // check that the remaining arguments are of an expected type
        if (!($arg instanceof FieldCollection) && !is_array($arg))
        {
            throw new \InvalidArgumentException('Argument must be a a FieldCollection or an array of FieldCollections.');
        }
        
        // if an array, check it is in the correct format
        if (is_array($arg))
        {
            if (!is_array($arg['parameters']) || !isset($arg['condition']))
            {
                throw new \InvalidArgumentException('Array arguments must be in the correct format.');
            }
            
            // check for a valid condition
            if (!in_array($arg['condition'], array('AND','OR')))
            {
                throw new \InvalidArgumentException('Condition must be either AND or OR');
            }
            
            // check that each parameter is either a FieldCollection, or itself an array
            foreach ($arg['parameters'] as $parameter)
            {
                if (!($parameter instanceof FieldCollection) && !is_array($parameter))
                {
                    throw new \InvalidArgumentException('Parameters must be a FieldCollection or an array of FieldCollections.');
                }
            }
        }
    }
    
    /**
     * Checks the structure of criteria that's being manually set.
     * 
     * @param array|FieldCollection $criteria
     */
    protected function checkCriteria($criteria)
    {
        $this->checkArg($criteria);
        if (is_array($criteria))
        {
            foreach ($criteria['parameters'] as $parameters)
            {
                $this->checkCriteria($parameters);
            }
        }
    }

    public function getFieldList(array $fields = array())
    {
        $fields = $this->getFieldListFromCollection($this->criteria, $fields);
        return $fields;
    }

    protected function getFieldListFromCollection($parentCollection, $fields)
    {
        foreach ($parentCollection['parameters'] as $collection)
        {
            if($collection instanceof FieldCollection)
            {
                foreach ($collection->getFields() as $field)
                {
                    $fields = array_merge( $fields, $field->getRawNames() );
                }
            }
            else
            {
                $fields = $this->getFieldListFromCollection($collection, $fields);
            }
        }
        return array_unique($fields);
    }
}