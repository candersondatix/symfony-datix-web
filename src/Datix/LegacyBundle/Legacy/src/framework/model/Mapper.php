<?php
namespace src\framework\model;

use src\framework\query\Query;
use src\framework\registry\Registry;
use src\system\database\FieldInterface;

/**
 * Database-facing class, responsible for persisting/extracting domain objects.
 */
abstract class Mapper implements TargetClass
{
    /**
     * The db wrapper object.
     * 
     * @var \DatixDBQuery
     */
    protected $db;
    
    /**
     * Used to fetch persistence tools.
     * 
     * @var ModelFactory
     */
    protected $factory;
    
    /**
     * The application registry.
     * 
     * @var Registry
     */
    protected $registry;
    
    /**
     * The entity cache.
     * 
     * @var EntityCache
     */
    protected $cache;
    
    public function __construct(\DatixDBQuery $db, ModelFactory $factory, Registry $registry = null, EntityCache $cache = null)
    {
        $this->db       = $db;
        $this->factory  = $factory;
        $this->registry = $registry ?: Registry::getInstance();
        $this->cache    = $cache ?: EntityCache::getInstance();
    }

    /**
     * Finds the entity by ID.
     *
     * @param mixed $id The value for the property that identifies the entity.
     * @param boolean $overrideSecurity Ignore security where clauses when hydrating the entity.
     *
     * @throws \MapperException
     * @return Entity|NULL
     */
    public function find($id, $overrideSecurity = false)
    {
        $class = $this->targetClass();
        $identity = $class::getIdentity();
        
        if (count($identity) > 1)
        {
            // if the identity is defined by one property (e.g. record id) then $id should be a single value, if not it should be an array
            throw new \MapperException('Mapper::find() is only compatible with entities which are identified by a single property');
        }
        
        // return the cached entity, if it exists
        if ($entity = $this->cache->get($class, $id))
        {
            return $entity;
        }
        
        // attempt to fetch the entity from the database
        list($query, $where, $fields) = $this->factory->getQueryFactory()->getQueryObjects();
        
        $where->add($fields->field($this->getDBObjectForSelect().'.'.$this->mapField($identity[0]))->eq((int) $id));
        $query->where($where);
        
        if ($overrideSecurity === true)
        {
            $query->overrideSecurity();
        }
        
        $result = $this->select($query);
        
        if (!empty($result))
        {
            $object = $this->factory->getEntityFactory()->createObject($result[0]);
            $this->cache->set($object);
        }
        
        return $object;
    }

    /**
     * Executes a select statement and returns the resulting record set array.
     * @param Query $query
     * @throws \MapperException
     * @return array
     */
    public function select(Query $query)
    {
        if (!$query->getQueryString())
        {
            $class     = $this->targetClass();
            $fieldDefs = $this->registry->getFieldDefs();
            $fields    = array();
            $table     = $this->getDBObjectForSelect();
            
            foreach ($class::getProperties() as $property)
            {
                $map = $this->mapField($property);
                $fullyQualifiedName = $table.'.'.$map;
                
                if (isset($fieldDefs[$fullyQualifiedName]))
                {
                    $fields[] = $fullyQualifiedName.' AS ['.$property.']';
                }
            }

            if (empty($fields))
            {
                throw new \MapperException('Unable to determine which fields to select - have you got the latest load files?');
            }
            
            foreach ($this->getFieldMappings() as $property => $dbField)
            {
                $query->convertFieldName($property, $table.'.'.$dbField);
            }
            
            $query->select($fields)
                  ->from($this->getDBObjectForSelect());
        }

        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    /**
     * Inserts the object into the database.
     * 
     * @param Entity $object
     * 
     * @return boolean Whether or not the operation was successful.
     * 
     * @throws \InvalidArgumentException When the object is of an incorrect type for this mapper.
     * @throws \MapperException          When the concrete implementation of doInsert() fails to return an id.
     */
    public function insert(Entity $object)
    {
        if (!$this->isObjectValid($object))
        {
            throw $this->invalidObjectException($object);
        }
        
        $id = $this->doInsert($object);
        
        $identity = $object::getIdentity();
        
        if ($object instanceof RecordEntity || count($identity) == 1) // assume that the identity is an integer if only one value is defined
        {
            if (!isset($id))
            {
                throw new \MapperException('The ID was not returned when inserting the object into the database');
            }
            else
            {
                $idProp = $identity[0];
                $object->$idProp = $id;
            }
        }
        
        $object->notify(Entity::EVENT_INSERT);
        $this->cache->set($object);
        
        return true;
    }
    
    /**
     * Updates the object in the database.
     * 
     * @param Entity $object
     * 
     * @return boolean Whether or not the operation was successful.
     * 
     * @throws \InvalidArgumentException When the object is of an incorrect type for this mapper.
     */
    public function update(Entity $object)
    {
        if (!$this->isObjectValid($object))
        {
            throw $this->invalidObjectException($object);
        }
        
        $result = $this->doUpdate($object);
        
        if ($result)
        {
            $object->notify(Entity::EVENT_UPDATE);
            $this->cache->set($object);
        }
        
        return $result;
    }
    
    /**
     * Deletes the object from storage.
     * 
     * NB - the object is passed in by reference explicitly here so that it can be destroyed once deleted 
     * (this appears to be the only way to achieve this).
     * 
     * @param Entity $object
     * 
     * @return boolean Whether or not the operation was successful.
     * 
     * @throws \InvalidArgumentException When the object is of an incorrect type for this mapper.
     */
    public function delete(Entity &$object)
    {
        if (!$this->isObjectValid($object))
        {
            throw $this->invalidObjectException($object);
        }

        $result = $this->doDelete($object);
        
        if ($result)
        {
            $object->notify(Entity::EVENT_DELETE);
            $object = null;
        }
            
        return $result;
    }
    
    /**
     * Convenience function which inserts/updates the entity, depending on 
     * whether or not it already exists in the database.
     * 
     * @param Entity $object
     * 
     * @return boolean Whether or not the operation was successful.
     * 
     * @throws \InvalidArgumentException When the object is of an incorrect type for this mapper.
     */
    public function save(Entity $object)
    {
        if (!$this->isObjectValid($object))
        {
            throw $this->invalidObjectException($object);
        }
        
        if ($object instanceof RecordEntity)
        {
            $entityExists = (int) $object->recordid > 0;
        }
        
        if (!isset($entityExists))
        {
            $identity = $object::getIdentity();
            if (count($identity) == 1)
            {
                // Assume that the identity is an integer (and has therefore been set in the database) if only one value is defined
                $idProp       = $identity[0];
                $entityExists = $object->$idProp != '';
            }
        }
        
        if (!isset($entityExists))
        {
            // We need an additional trip to the DB to check whether this entity exists
            // so it's more efficient to call insert/update directly in this context (if possible)
            $where = array();
            foreach ($identity as $id)
            {
                $where[$this->mapField($id)] = $object->$id;
            }
            
            $query = $this->factory->getQueryFactory()->getQuery();
            $query->select(array('COUNT(*)'))
                  ->from($this->getTable())
                  ->where($where);
            
            list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
            $this->db->setSQL($sql);
            $this->db->prepareAndExecute($parameters);
            $entityExists = (bool) $this->db->PDOStatement->fetch(\PDO::FETCH_COLUMN);
        }
        
        if ($entityExists)
        {
            return $this->update($object);
        }
        else
        {
            return $this->insert($object);
        }
    }
    
    /**
     * Checks that the provided object type is correct for this mapper.
     * 
     * @param Entity $object
     * 
     * @return boolean
     */
    protected function isObjectValid(Entity $object)
    {
        $targetClass = $this->targetClass();
        return $object instanceof $targetClass;
    }
    
    /**
     * Instantiates and returns an InvalidArgumentException.
     * 
     * Used when the object provided to the concrete mapper class is of an incorrect type.
     * 
     * @param Entity $object
     * 
     * @return \InvalidArgumentException
     */
    protected function invalidObjectException(Entity $object)
    {
        return new \InvalidArgumentException('Object of type '.$this->targetClass().' expected, '.get_class($object).' given.');
    }
    
    /**
     * Inserts an entity into the database by delegating to the appropriate insert function, depending on table type.
     * 
     * @param Entity $object
     * 
     * @return int The newly inserted record id.
     */
    protected function doInsert(Entity $object)
    {
        $tableDefs = $this->registry->getTableDefs();
        
        if ($tableDefs[$this->getTable()]->is_identity || count($object::getIdentity()) > 1) // assume that the identity is an integer if only one value is defined
        {
            return $this->doInsertWithIdentity($object);
        }
        else
        {
            return $this->doInsertWithoutIdentity($object);
        }
    }
    
    /**
     * Executes the statements required to insert a record into a table which uses an Identity (auto-increment) column.
     * 
     * @param Entity $object
     * 
     * @return int The newly inserted record id.
     */
    protected function doInsertWithIdentity(Entity $object)
    {
        $query = $this->factory->getQueryFactory()->getQuery();
        
        $query->insert($this->getEntityProperties($object))
              ->into($this->getTable());
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        $this->db->PDOStatement->nextRowset();
        
        $id = $this->db->fetch(\PDO::FETCH_COLUMN);

        // remove unwanted characters from returned integer (",00", ".00" etc)
        $id = preg_replace('/[,.]0+$/u', '', $id);
        $id = preg_replace('/[,.]/u', '', $id);

        return $id;
    }
    
    /**
     * Executes the statements required to insert a record into a table which does not use an Identity (auto-increment) column.
     * 
     * @param Entity $object
     * 
     * @return int The newly inserted record id.
     */
    protected function doInsertWithoutIdentity(Entity $object)
    {
        // TODO remove reliance on GetNextRecordID() and handle all logic explicitly here
        $identity = $object::getIdentity();
        $idProp = $identity[0];
        $object->$idProp = GetNextRecordID($this->getTable(), true);
        $this->update($object);
        return $object->$idProp;
    }
    
    /**
     * Executes an update statement to update the passed object.
     * 
     * @param Entity $object
     * 
     * @return boolean Whether or not the operation was successful.
     */
    protected function doUpdate(Entity $object)
    {
        $identity = $object::getIdentity();
        $where    = array();
        foreach ($identity as $id)
        {
            $where[$this->mapField($id)] = $object->$id;
        }
        
        $query = $this->factory->getQueryFactory()->getQuery();
        $query->update($this->getTable())
              ->set($this->getEntityProperties($object))
              ->where($where);
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        return $this->db->prepareAndExecute($parameters);
    }
    
    /**
     * Returns an array of key/value pairs representing the entity's properties.
     * 
     * Used when building insert/update statements.
     * 
     * @param Entity $object
     * 
     * @return array
     */
    protected function getEntityProperties(Entity $object)
    {
        $class     = $this->targetClass();
        $fieldDefs = $this->registry->getFieldDefs();
        $fields    = array();
        
        foreach ($class::getProperties() as $property)
        {
            $field = $fieldDefs[$this->getDBObjectForSelect().'.'.$this->mapField($property)];
            if (isset($field) && $this->mapField($property) != 'recordid' && !$field->isCalculated())
            {
                if ($object->$property === '' && ($field->getType() == FieldInterface::DATE || $field->getType() == FieldInterface::NUMBER))
                {
                    // empty date and integer values need to be converted to null
                    $fields[$this->mapField($property)] = null;
                }
                else
                {
                    $fields[$this->mapField($property)] = $object->$property;
                }
            }
        }
        
        return $fields;
    }
    
    /**
     * Executes a delete statement to remove the passed object.
     * 
     * @param Entity $object
     * 
     * @return boolean Whether or not the operation was successful.
     */
    protected function doDelete(Entity $object)
    {
        $identity = $object::getIdentity();
        $where    = array();
        foreach ($identity as $id)
        {
            $where[$this->mapField($id)] = $object->$id;
        }
        
        $query = $this->factory->getQueryFactory()->getQuery();
        $query->delete()
              ->from($this->getTable())
              ->where($where);
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        return $this->db->prepareAndExecute($parameters);
    }
    
    /**
     * Returns the name of the database table the entity is stored in.
     * 
     * @return string
     */
    protected abstract function getTable();

    /**
     * Returns the name of a view used for data retrieval, if required.
     *
     * @return string
     */
    protected function getView()
    {
        return;
    }

    /**
     * Returns the name of the db object to use for data retrieval, with a view getting priority if defined.
     *
     * @return string
     */
    protected function getDBObjectForSelect()
    {
        $view = $this->getView();

        if (isset($view))
        {
            return $view;
        }
        else
        {
            return $this->getTable();
        }
    }
    
    /**
     * Provides a means to automatically map fields which relate to entity properties contained in queries 
     * to their database counterparts in situations where the property name is different from the database field name.
     * 
     * The array keys represent the entity properties and the values the equivalent database field names.
     * 
     * @return array
     */
    protected function getFieldMappings()
    {
        return array();
    }

    /**
     * Returns the mapped database field name for a given entity property, based on the mappings defined in Mapper::getFieldMappings().
     * @param $property
     * @return string
     */
    protected function mapField($property)
    {
        return $this->getFieldMappings()[$property] ?: $property;
    }
}
