<?php
namespace src\framework\model;

/**
 * An entity stored with a recordid.
 */
class RecordEntity extends Entity
{
    /**
     * The object's ID (as defined in the database).
     * 
     * @var int
     */
    protected $recordid;
    
    public function __construct($id = null)
    {
        $this->recordid = $id;
        parent::__construct();
    }
    
    /**
     * {@inherit}
     */
    public function __clone()
    {
        $this->recordid = null;
        parent::__clone();
    }
    
    /**
     * {@inherit}
     */
    protected static function setIdentity()
    {
        return array('recordid');
    }
}