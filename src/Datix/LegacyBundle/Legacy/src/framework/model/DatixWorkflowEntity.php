<?php
namespace src\framework\model;

/**
 * Describes entities that use the Datix workflow system.
 */
abstract class DatixWorkflowEntity extends DatixEntity
{
    /**
     * The workflow status.
     * 
     * @var string
     */
    protected $rep_approved;
}