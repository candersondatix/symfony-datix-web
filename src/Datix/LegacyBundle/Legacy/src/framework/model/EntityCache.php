<?php
namespace src\framework\model;

/**
 * Stores system entities to remove the need to fetch from the database more than once per request.
 */
class EntityCache
{
    /**
     * The Singleton instance.
     * 
     * @var Registry
     */
    private static $instance;
    
    /**
     * The cached entities.
     * 
     * @var array
     */
    private $entities;
    
    private function __construct()
    {
        $this->entities = array();
    }
    
    /**
     * Gets the Singleton instance.
     * 
     * @return EntityCache
     */
    public static function getInstance()
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }
        return static::$instance;
    }
    
    /**
     * Retireves the entity from the cache.
     * 
     * @param string       $classname The type of the entity we're retrieving.
     * @param string|array $id        The identity of the entity we're retrieving.
     * 
     * @return RecordEntity
     */
    public function get($classname, $id)
    {
        if (is_array($id))
        {
            $id = implode('.', $id);
        }
        return isset($this->entities[$classname.'.'.$id]) ? $this->entities[$classname.'.'.$id] : null;
    }
    
    /**
     * Caches the entity.
     * 
     * @param Entity $entity
     */
    public function set(Entity $entity)
    {
        if (!$entity->isCacheable() || null === ($key = $entity->getIdentityKey()))
        {
            return;
        }
        $this->entities[$key] = $entity;
    }
}