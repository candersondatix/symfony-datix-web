<?php
namespace src\framework\model;

use src\framework\model\exceptions\ModelException;

/**
 * Implements SPL ArrayAccess interface to allow objects in the collection to be accessed via a key.
 */
abstract class KeyedEntityCollection extends EntityCollection implements \ArrayAccess
{
    /**
     * Used to reference the collection element number to a key.
     */
    protected $keyReference;
    
    public function __construct(Mapper $mapper, EntityFactory $factory)
    {
        parent::__construct($mapper, $factory);
        $this->keyReference = array();
    }
    
    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        throw new ModelException('It\'s not currently possible to add elements to keyed collections.');
    }
    
    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        $offset = strtolower($offset);

        $this->initCollection();
        return $this->getElement($this->keyReference[$offset]) !== null;
    }
    
    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        throw new ModelException('It\'s not currently possible to remove elements from keyed collections.');
    }
    
    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        $offset = strtolower($offset);

        //We need to initialise the collection here so that key references can be set
        $this->initCollection();
        return $this->getElement($this->keyReference[$offset]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        if (null === $this->nativeArray)
        {
            $this->initCollection();
            
            $this->nativeArray = [];
            $keys = array_flip($this->keyReference);
            $count = count($this->data);
            
            for ($i = 0; $i < $count; $i++)
            {
                $this->nativeArray[$keys[$i]] = $this->getElement($i);
            }
        }
        
        return $this->nativeArray;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getElement($key)
    {
        $this->initCollection();
        return parent::getElement($key);
    }
    
    /**
     * Sets up the collection so it can be accessed by key, by populating the keyReference array.
     * 
     * @throws ModelException If the concrete implementation of KeyedEntityCollection::keyProperty() doesn't return an array.
     */
    protected function initCollection()
    {
        if (!is_array($this->keyProperty()))
        {
            throw new ModelException('KeyedEntityCollection::keyProperty() must return an array.');
        }
        
        if (empty($this->keyReference))
        {
            if (!isset($this->data))
            {
                $this->data = $this->mapper->select($this->query);
            }
            
            $keyProp = implode('.', $this->keyProperty());
            
            foreach ($this->data as $key => $record)
            {
                $keyProp = '';
                foreach ($this->keyProperty() as $prop)
                {
                    $keyProp .= $record[$prop] . '.';
                }
                //Don't need to use UnicodeString, since these keys will always be simple strings.
                $this->keyReference[strtolower(substr($keyProp, 0, -1))] = $key;
            }
        }
    }
    
    /**
     * Used to define the data field(s) which is used as the collection key.
     * 
     * @return array
     */
    abstract protected function keyProperty();
}