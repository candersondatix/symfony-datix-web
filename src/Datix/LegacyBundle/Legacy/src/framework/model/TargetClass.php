<?php
namespace src\framework\model;

interface TargetClass
{
    /**
     * Returns the fully-qualified class name of the entity being handled by the concrete class.
     * 
     * @return string
     */
    public function targetClass();
}