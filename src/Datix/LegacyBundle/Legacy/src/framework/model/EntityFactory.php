<?php
namespace src\framework\model;

use src\framework\controller\Request;
use src\framework\registry\Registry;
use \src\reports\exceptions\InvalidOptionException;

/**
 * Factory for creating Entity objects.
 */
abstract class EntityFactory implements TargetClass
{
    /**
     * Returns the newly-constructed object.
     *
     * @param array $data The data used to construct the object.
     * @param Entity $baseEntity
     * @throws \InvalidArgumentException If the passed entity is of an incorrect type.
     * @return Entity
     *
     */
    public function createObject(array $data = array(), Entity $baseEntity = null)
    {
        if ($baseEntity !== null && !is_a($baseEntity, $this->targetClass()))
        {
            throw new \InvalidArgumentException('Object of type '.$this->targetClass().' expected, '.get_class($baseEntity).' given.');
        }
        return $this->doCreateObject($data, $baseEntity);
    }
    
    /**
     * Creates a new object using the controller request to automatically bind property values.
     * 
     * @param Request $request The controller request.
     * 
     * @return Entity
     */
    public function createFromRequest(Request $request, Entity $baseEntity = null)
    {
        return $this->createObject($request->getParameters(), $baseEntity);
    }

    /**
     * Constructs and returns the object using the provided data.
     *
     * Handles the most common case where an entity consists of basic single-value properties.
     *
     * @param array $data The data used to construct the object.
     * @param Entity $baseEntity
     * @param EntityCache $cache
     * @param \src\framework\registry\Registry $registry
     * @return Entity
     */
    protected function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null, Registry $registry = null)
    {
        $class    = $this->targetClass();
        $cache    = $cache ?: EntityCache::getInstance();
        $identity = $class::getIdentity();
        $registry = $registry ?: Registry::getInstance();
        $logger   = $registry->getLogger();
        
        $useCache = true;

        //if an existing entity has been passed, we want to use that as the base object, not the cached version
        if ($baseEntity instanceof $class)
        {
            $useCache = false;
            $object = $baseEntity;
        }
        else
        {
            $idData   = array();
            foreach ($identity as $id)
            {
                if (empty($data[$id]))
                {
                    // we don't have all the identity information we need to cache this object
                    $useCache = false;
                    break;
                }
                $idData[] = $data[$id];
            }
        }
        
        if ($useCache)
        {
            $object = $cache->get($class, $idData);
        }
        
        if (!isset($object))
        {
            $object = new $class();
        }
        
        foreach ($class::getProperties() as $property)
        {
            if (array_key_exists($property, $data))
            {
                //Don't want to override a base entity property with a null value.
                if (!($baseEntity && $baseEntity->$property !== null && $data[$property] === null))
                {
                    $object->$property = isset($data[$property]) ? $data[$property] : null;
                    if ($data[$property] === null)
                    {
                        // log this event so we can verify that we're purposefully setting this property to null, rather than accidentally removing it
                        $logger->logDebug('Setting entity '.$class.' property '.$property.' to NULL');
                    }
                }
            }
        }

        return $object;
    }
}