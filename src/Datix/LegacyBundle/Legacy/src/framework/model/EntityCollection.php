<?php
namespace src\framework\model;

use src\framework\query\Query;

/**
 * A type-safe iterable collection of Entity objects.
 */
abstract class EntityCollection implements \Iterator, \Countable, TargetClass
{
    /**
     * The object store.
     * 
     * @var array
     */
    private $objects;
    
    /**
     * The current position of the array cursor.
     * 
     * @var int
     */
    private $cursor;
    
    /**
     * The mapper used to retrieve data for this collection.
     * 
     * @var Mapper
     */
    protected $mapper;
    
    /**
     * The entity factory that's used to create objects from the raw data set.
     * 
     * @var EntityFactory
     */
    protected $factory;
    
    /**
     * An the raw data set retrieved by the mapper.
     * 
     * @var array
     */
    protected $data;
    
    /**
     * Whether or not to defer the execution of observers attached to the objects in the collection.
     * 
     * @var Boolean
     */
    protected $queueEvents;
    
    /**
     * The parameters used to retrieve the collection.
     * 
     * @var Query
     */
    protected $query;
    
    /**
     * A native PHP array version of this collection.
     * 
     * @var array
     */
    protected $nativeArray;
    
    public function __construct(Mapper $mapper, EntityFactory $factory)
    {
        if ($mapper->targetClass() != $this->targetClass())
        {
            throw new \InvalidArgumentException('Invalid mapper type for this collection');
        }
        
        if ($factory->targetClass() != $this->targetClass())
        {
            throw new \InvalidArgumentException('Invalid entity factory type for this collection');
        }
        
        $this->mapper      = $mapper;
        $this->factory     = $factory;
        $this->objects     = array();
        $this->cursor      = 0;
        $this->queueEvents = false;
    }
    
    /**
     * {@inherit}
     */
    public function __clone()
    {
        // set data property in cases where the collection hasn't yet been accessed
        $this->fetchData();
        
        // ensure the objects array is fully constructed
        foreach ($this->data as $key => $data)
        {
            if (!isset($this->objects[$key]))
            {
                $this->objects[$key] = $this->factory->createObject($data);
            }
        }
        
        // clone each of the objects in the collection
        foreach ($this->objects as $key => $object)
        {
            $this->objects[$key] = clone $object;
        }
        
        $this->query = null;
        $this->data  = array();
    }
    
    /**
     * Setter for query.
     * 
     * @param Query
     */
    public function setQuery(Query $query)
    {
        $this->query = $query;
    }
    
    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
       $this->cursor = 0;
    }
    
    /**
     * {@inheritdoc}
     */
    public function current()
    {
       return $this->getElement($this->cursor);
    }
    
    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->cursor;
    }
    
    /**
     * {@inheritdoc}
     */
    public function next()
    {
        $object = $this->getElement($this->cursor);
        if ($object !== null)
        {
            $this->cursor++;
        }
        return $object;
    }
    
    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->current() !== null;
    }
    
    /**
     * {@inheritdoc}
     */
    public function count()
    {
        $this->fetchData();
        return count($this->data);
    }
    
    /**
     * Setter for queueEvents.
     * 
     * @param Boolean $bool
     */
    public function setQueueEvents($bool)
    {
        $this->queueEvents = (bool) $bool;
        foreach ($this->objects as $object)
        {
            $object->setQueueEvents($bool);
        }
    }
    
    /**
     * Setter for data.
     * 
     * @param array $data
     */
    public function setData(array $data = array())
    {
        if (!isset($this->data) && !empty($data))
        {
            $this->data = $data;
        }
    }
    
    /**
     * Getter for data array.
     *
     * @param array $data
     */
    public function getData()
    {
        $this->fetchData();
        return $this->data;
    }

    /**
     * Retrieves an object from the collection.
     * 
     * @param int $key The position of the object in the collection.
     * 
     * @return Entity|NULL
     */
    protected function getElement($key)
    {
        // lazy-load data on access
        $this->fetchData();
        
        if (isset($this->objects[$key]))
        {
            // the object has already been created
            return $this->objects[$key];
        }
        
        if (isset($this->data[$key]))
        {
            // create the object from the raw data
            $this->objects[$key] = $this->factory->createObject($this->data[$key]);
            
            if ($this->queueEvents)
            {
                $this->objects[$key]->setQueueEvents(true);
            }
            
            return $this->objects[$key];
        }
        
        return null;
    }
    
    /**
     * Uses the mapper to fetch the raw data for this collection, if not already set.
     */
    protected function fetchData()
    {
        if (!isset($this->data) && isset($this->query))
        {
            $this->data = $this->mapper->select($this->query);
        }
    }

    /**
     * Checks whether this collection is empty.
     */
    public function isEmpty()
    {
        // lazy-load data on access
        $this->fetchData();

        if ((!isset($this->data) || empty($this->data))
            && (!isset($this->objects) || empty($this->objects)))
        {
            return true;
        }

        return false;
    }

    /**
     * Returns a native PHP array containing the objects in the collection.
     * 
     * Useful when iterating over collections of objects many (i.e. 1000s) of times
     * since iterating over native arrays is many orders of magnitude faster.
     */
    public function toArray()
    {
        if (null === $this->nativeArray)
        {
            $this->fetchData();
            
            $count = count($this->data);
            
            for ($i = 0; $i < $count; $i++)
            {
                $this->getElement($i);
            }
            
            $this->nativeArray = $this->objects;
        }
        
        return $this->nativeArray;
    }
}
