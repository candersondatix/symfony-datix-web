<?php
namespace src\framework\model;

/**
 * Adds DatixEntity-specific functionality to Mapper.
 */
abstract class DatixEntityMapper extends Mapper
{
    /**
     * {@inheritdoc}
     * 
     * Sets updatedby and updateddate before insert.
     */
    protected function doInsert(Entity $object)
    {
        $this->updateEntity($object);
        return parent::doInsert($object);
    }
    
    /**
     * {@inheritdoc}
     * 
     * Sets updatedby, updateddate, and updateid properties before update.
     */
    protected function doUpdate(Entity $object)
    {
        $this->updateEntity($object);
        $object->incrementUpdateId();
        return parent::doUpdate($object);
    }
    
    /**
     * Sets the updatedby and updateddate properties.
     * 
     * @param Entity $object
     */
    protected function updateEntity(Entity $object)
    {
        $object->updatedby   = $_SESSION['initials'];
        $object->updateddate = date('d-M-Y H:i:s');
    }
}