<?php
namespace src\hotspots\filters;

use src\framework\controller\ControllerFilter;

/**
 * Restricts access to command line interface only.
 */
class CliOnlyFilter extends ControllerFilter
{
    public function doAction($action)
    {
        if ('cli' != php_sapi_name())
        {
            throw new \URLNotFoundException();
        }
        return $this->proceed($action);
    }
}