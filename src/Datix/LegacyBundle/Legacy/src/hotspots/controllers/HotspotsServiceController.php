<?php
namespace src\hotspots\controllers;

use src\framework\controller\Controller;
use src\framework\controller\Request;
use src\framework\controller\Response;
use src\framework\query\SqlWriter;
use src\contacts\model\ContactModelFactory;
use src\email\EmailSenderFactory;
use src\logger\DatixLogger;
use Monolog\Logger;

class HotspotsServiceController extends Controller
{
    /**
     * This controller uses its own logger instance since the log level 
     * is defined in the .NET service config rather than in DATIXConfig.php.
     * 
     * @var DatixLogger
     */
    protected $logger;
    
    /**
     * The location field names for each module.
     * 
     * @var array
     */
    protected $locationFields;
    
    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
        
        $logLevel = (bool) $this->request->getParameter('debug') ? Logger::DEBUG : Logger::EMERGENCY;
        $this->logger = new DatixLogger('HotspotsService', $logLevel);
    }
    
    /**
     * Removes all records from the Hotspots process queue.
     * 
     * Invoked by the .NET service on startup.
     */
    public function clearHotspotQueue()
    {
        \DatixDBQuery::PDO_query('DELETE FROM hotspots_queue');
        $this->logger->logDebug('Cleared Hotspot queue');
    }
    
    /**
     * Processes the hotspots queue and creates hotspot records/e-mail when an agent's conditions are met.
     * 
     * Invoked by the .NET service at regular intervals (as defined in the service config).
     */
    public function processHotspotsQueue()
    {
        // since this is a CLI request, we can't build the scripturl (used for e-mail links) from the $_SERVER global
        // so the url is defined in the DatixService config file and submitted with the request
        $GLOBALS['scripturl'] = $this->request->getParameter('url');
        
        $moduleDefs = $this->registry->getModuleDefs();
        $sw = new SqlWriter();
        
        $this->logger->logDebug('Processing hotspots queue');
        
        // fetch all records in hotspots queue
        $queue = \DatixDBQuery::PDO_fetch_all('SELECT recordid, hsq_module, cas_id FROM hotspots_queue');
        
        // fetch all active agents
        $agents = \DatixDBQuery::PDO_fetch_all('
            SELECT 
                recordid, hsa_module, hsa_name, hsa_description, hsa_where_clause, 
                hsa_count_threshold, hsa_within, hsa_date_field, hsa_effective_from, 
                hsa_effective_to, hsa_organisation, hsa_unit, hsa_clingroup, hsa_directorate, 
                hsa_specialty, hsa_loctype, hsa_locactual, hsa_use_loc_for_crit, hsa_emt_recordid
            FROM 
                hotspots_agent
            WHERE
                (hsa_retrigger = \'NEVER\' AND recordid NOT IN (SELECT hsa_id FROM hotspots_main WHERE hot_closed_date IS NULL))
            OR 
                hsa_retrigger = \'NOSAME\'');
        
        foreach ($queue as $record)
        {
            $this->logger->logDebug('Processing '.$record['hsq_module'].' record '.$record['cas_id']);
            
            // process each agent designed for this record's module
            foreach ($agents as $agent)
            {
                if ($agent['hsa_module'] != $record['hsq_module'])
                {
                    continue;
                }
                
                $this->logger->logDebug('Processing agent '.$agent['recordid']);
                
                $table  = $moduleDefs[$agent['hsa_module']]['VIEW'] ?: $moduleDefs[$agent['hsa_module']]['TABLE'];
                $where  = [];
                $params = [];
                
                // create query criteria if location codes on agent are being used for hotspot criteria (as well as for agent access control)
                if ($agent['hsa_use_loc_for_crit'] == 'Y')
                {
                    if ($agent['hsa_organisation'] != '')
                    {
                        $where[]  = $this->getLocationField($agent['hsa_module'], '!ORG').' = ?';
                        $params[] = $agent['hsa_organisation'];
                    }
                    
                    if ($agent['hsa_unit'] != '')
                    {
                        $where[]  = $this->getLocationField($agent['hsa_module'], 'code_unit').' = ?';
                        $params[] = $agent['hsa_unit'];
                    }
                    
                    if ($agent['hsa_clingroup'] != '')
                    {
                        $where[]  = $this->getLocationField($agent['hsa_module'], '!CLINGROUP').' = ?';
                        $params[] = $agent['hsa_clingroup'];
                    }
                    
                    if ($agent['hsa_directorate'] != '')
                    {
                        $where[]  = $this->getLocationField($agent['hsa_module'], 'code_directorate').' = ?';
                        $params[] = $agent['hsa_directorate'];
                    }
                    
                    if ($agent['hsa_specialty'] != '')
                    {
                        $where[]  = $this->getLocationField($agent['hsa_module'], 'code_specialty').' = ?';
                        $params[] = $agent['hsa_specialty'];
                    }
                    
                    if ($agent['hsa_loctype'] != '')
                    {
                        $where[]  = $this->getLocationField($agent['hsa_module'], 'code_location').' = ?';
                        $params[] = $agent['hsa_loctype'];
                    }
                    
                    if ($agent['hsa_locactual'] != '')
                    {
                        $where[]  = $this->getLocationField($agent['hsa_module'], 'code_locactual').' = ?';
                        $params[] = $agent['hsa_locactual'];
                    }
                }
                
                // create query criteria based on the agent's effective dates
                if ((int) $agent['hsa_within'] > 0 && $agent['hsa_date_field'] != '')
                {
                    if ($agent['hsa_effective_from'] != '')
                    {
                        $where[]  = $agent['hsa_date_field'].' >= ?';
                        $params[] = $agent['hsa_effective_from'];
                    }

                    if ($agent['hsa_effective_to'] != '')
                    {
                        $where[]  = $agent['hsa_date_field'].' <= ?';
                        $params[] = $agent['hsa_effective_to'];
                    }
                }
                
                // append manually created string based where clause if defined for this agent
                if ($agent['hsa_where_clause'] != '')
                {
                    $where[] = $agent['hsa_where_clause'];
                }
                
                // create the individual where clauses that are defined by the agent criteria
                $conditionSets = [];
                $hSConditions = \DatixDBQuery::PDO_fetch_all('
                    SELECT 
                        hsc_field_name, hsc_values, hsc_count_type
                    FROM 
                        hotspots_condition
                    WHERE 
                        hsa_id = ?', [$agent['recordid']]);
                
                foreach ($hSConditions as $hSCondition)
                {
                    if ($hSCondition['hsc_values'] != '')
                    {
                        $conditions = [];
                        $values = explode(' ', $hSCondition['hsc_values']);
                        
                        if ($hSCondition['hsc_count_type'] == 'ALL')
                        {
                            // count records which match any of the values entered
                            $conditions[] = [
                                'sql'    => $hSCondition['hsc_field_name'].' IN ('.implode(',', array_map(function(){return '?';}, $values)).')',
                                'params' => $values
                            ];
                        }
                        elseif ($hSCondition['hsc_count_type'] == 'EACH')
                        {
                            // count records which match each of the values individually
                            foreach ($values as $value)
                            {
                                $conditions[] = [
                                    'sql'    => $hSCondition['hsc_field_name'].' = ?',
                                    'params' => [$value]
                                ];
                            }
                        }
                        
                        $conditionSets[] = $conditions;
                    }
                }
                
                $this->logger->logDebug('Begin conditions debug');
                
                $conditionClauses = $this->generateConditionClauses($conditionSets, 0);
                foreach ($conditionClauses as $conditionClause)
                {
                    $hit = false;
                    
                    if (!empty($where))
                    {
                        // append additional conditions defined by location codes, effective dates, and manual where clauses
                        $conditionClause = [
                            'sql'    => $conditionClause['sql'].' AND '.implode(' AND ', $where),
                            'params' => array_merge($conditionClause['params'], $params)
                        ];
                    }
                    
                    // the queue record needs to match the agent criteria itself in order to
                    // generate a hotspot, so this is the first thing we check
                    $match = (bool) \DatixDBQuery::PDO_fetch('
                        SELECT
                            COUNT(*)
                        FROM
                            '.$table.'
                        WHERE
                            '.$conditionClause['sql'].'
                        AND
                           recordid = ?', array_merge($conditionClause['params'], [$record['cas_id']]), \PDO::FETCH_COLUMN);
                    
                    if (!$match)
                    {
                        $this->logger->logDebug($record['hsq_module'].' record '.$record['cas_id'].' does not satisfy the following criteria: '.
                            $this->tidyString($sw->combineSqlAndParameters($conditionClause['sql'], $conditionClause['params'])));
                        continue;
                    }
                    
                    if ($agent['hsa_within'] > 0 && $agent['hsa_date_field'] != '')
                    {
                        $date = \DatixDBQuery::PDO_fetch('
                            SELECT
                                '.$agent['hsa_date_field'].'
                            FROM
                                '.$table.'
                            WHERE
                                recordid = ?', [$record['cas_id']], \PDO::FETCH_COLUMN);
                        
                        if ($date != '')
                        {
                            $date = new \DateTime($date);
                            for ($i = 0; $i <= $agent['hsa_within']; $i++)
                            {
                                $periodStart = abs($i - $agent['hsa_within']);
                                
                                $startDate = clone $date;
                                $endDate   = clone $date;
                                
                                $startDate->sub(new \DateInterval('P'.$periodStart.'D'));
                                $endDate->add(new \DateInterval('P'.$i.'D'));
                                
                                $hsSql = 
                                   'SELECT
                                        recordid
                                    FROM
                                        '.$table.'
                                    WHERE
                                        '.$conditionClause['sql'].'
                                    AND
                                        '.$agent['hsa_date_field'].' > ?
                                    AND
                                        '.$agent['hsa_date_field'].' <= ?';
                                
                                $hsParams = array_merge($conditionClause['params'], [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')]);
                                
                                $this->logger->logDebug('Executing hotspot SQL: '.$this->tidyString($sw->combineSqlAndParameters($hsSql, $hsParams)));
                                
                                $records = \DatixDBQuery::PDO_fetch_all($hsSql, $hsParams, \PDO::FETCH_COLUMN);
                                $count = count($records);
                                
                                if ($count >= $agent['hsa_count_threshold'])
                                {
                                    $hit = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            $this->logger->logDebug('Cannot process '.$record['hsq_module'].' record '.$record['cas_id'].' with agent '.$agent['recordid'].
                                ' because the based on date field ('.$agent['hsa_date_field'].') is NULL');
                        }
                    }
                    else
                    {
                        $hsSql = 
                           'SELECT
                                recordid
                            FROM
                                '.$table.'
                            WHERE
                                '.$conditionClause['sql'];
                        
                        $hsParams = $conditionClause['params'];
                                        
                        $this->logger->logDebug('Executing hotspot SQL: '.$this->tidyString($sw->combineSqlAndParameters($hsSql, $hsParams)));
                        
                        $records = \DatixDBQuery::PDO_fetch_all($hsSql, $conditionClause['params'], \PDO::FETCH_COLUMN);
                        $count = count($records);
                                
                        if ($count >= $agent['hsa_count_threshold'])
                        {
                            $hit = true;
                        }
                    }
                    
                    if ($hit)
                    {
                        $this->logger->logDebug('Hotspot hit');
                        
                        $criteria = $this->tidyString($sw->combineSqlAndParameters($hsSql, $hsParams));
                        $hotHitCriteria = $agent['hsa_count_threshold'].'|'.$criteria;
                        
                        $existingHotspot = \DatixDBQuery::PDO_fetch('
                            SELECT
                                recordid
                            FROM
                                hotspots_main
                            WHERE
                                hsa_id = ?
                            AND
                                hot_closed_date IS NULL
                            AND
                                hot_hit_criteria = ?', [$agent['recordid'], $hotHitCriteria], \PDO::FETCH_COLUMN);
                        
                        if ($existingHotspot == '')
                        {
                            // create a new hotspot record
                            $hotspotId = \DatixDBQuery::PDO_build_and_insert('hotspots_main', [
                                'hsa_id'           => $agent['recordid'],
                                'hot_name'         => $agent['hsa_name'],
                                'hot_descr'        => $agent['hsa_description']."\n\rCriteria: \n\r".$criteria,
                                'hot_opened_date'  => (new \DateTime)->format('Y-m-d'),
                                'rep_approved'     => 'FA',
                                'hot_hit_criteria' => $hotHitCriteria,
                                'hot_hit_occurred' => $count
                            ]);
                            
                            $this->logger->logDebug('Hotspot hit');
                            
                            // create links to records which triggered hotspot
                            foreach ($records as $recordid)
                            {
                                if ($record['hsq_module'] == 'HOT')
                                {
                                    // links to records of the same module are stored in a different table
                                    \DatixDBQuery::PDO_query('
                                        INSERT INTO links
                                            (lnk_mod1, lnk_id1, lnk_mod2, lnk_id2, link_notes)
                                        VALUES
                                            (?,?,?,?,?)', ['HOT', $hotspotId, 'HOT', $recordid, 'Link generated by Hotspots Hit']);
                                }
                                else
                                {
                                    \DatixDBQuery::PDO_query('
                                        INSERT INTO link_modules
                                            (hot_id, '.$record['hsq_module'].'_ID, link_notes)
                                        VALUES
                                            (?,?,?)', [$hotspotId, $recordid, 'Link generated by Hotspots Hit']);
                                }
                                
                                $this->logger->logDebug('Inserted new hotspot link: '.$record['hsq_module'].' record '.$recordid.' for hotspot '.$hotspotId);
                            }
                            
                            // process recipient e-mails
                            $recipientIDs = [];
                            
                            $recipientCriteria = \DatixDBQuery::PDO_fetch_all('
                                SELECT
                                	hrcon.hrc_id, hrcon.hrc_field_name, hrcon.hrc_values
                                FROM
                                	hotspots_recip_conditions hrcon
                                INNER JOIN
                                	hotspots_recipients_criteria hrcri ON hrcon.hrc_id = hrcri.recordid
                                WHERE
                                	hrcri.hsa_id = ?
                            ', [$agent['recordid']]);
                            
                            foreach ($recipientCriteria as $recipientCriterion)
                            {
                                $recipientCriteriaClauses[$recipientCriterion['hrc_id']][$recipientCriterion['hrc_field_name']] = explode(' ', $recipientCriterion['hrc_values']);
                            }
                            
                            foreach ($recipientCriteriaClauses as $hrc_id => $recipCriteria)
                            {
                                $recipWhere  = [];
                                $recipParams = [];
                                
                                foreach ($recipCriteria as $recipField => $recipValues)
                                {
                                    if (!empty($recipValues))
                                    {
                                        $recipWhere[]  = $recipField.' IN ('.implode(',', array_map(function(){return '?';}, $recipValues)).')';
                                        $recipParams = array_merge($recipParams, $recipValues);
                                    }
                                }
                                
                                if (!empty($recipWhere))
                                {
                                    // test that the hotspot criteria matches this set of recipient criteria
                                    $recipSql   = $hsSql.' AND '.implode(' AND ', $recipWhere);
                                    $recipMatch = \DatixDBQuery::PDO_fetch_all($recipSql, array_merge($hsParams, $recipParams));
                                    
                                    if (count($recipMatch) > 0)
                                    {
                                        // retrieve a list of individual recipients for this recipient criteria set
                                        $recipients = \DatixDBQuery::PDO_fetch_all('
                                            SELECT
                                            	hsr_type, hsr_value
                                            FROM
                                            	hotspots_recipients
                                            WHERE
                                            	hrc_id = ?
                                        ', [$hrc_id]);
                                        
                                        foreach ($recipients as $recipient)
                                        {
                                            if ($recipient['hsr_type'] == 'sec_group')
                                            {
                                                // fetch individual users from the security group
                                                $recipientIDs = array_merge(
                                                    $recipientIDs,
                                                    \DatixDBQuery::PDO_fetch_all('SELECT con_id FROM sec_staff_group WHERE grp_id = ?', [$recipient['hsr_value']], \PDO::FETCH_COLUMN)        
                                                );
                                            }
                                            else
                                            {
                                                $recipientIDs[] = $recipient['hsr_value'];
                                            }
                                        }                                        
                                    }
                                }
                            }
                            
                            if (!empty($recipientIDs))
                            {
                                $recipientIDs = array_unique($recipientIDs);
                                
                                $this->logger->logDebug('Recipients List: '.implode(',', $recipientIDs));
                                
                                $hotspotData = [
                                    'recordid'         => $hotspotId,
                                    'hsa_id'           => $agent['recordid'],
                                    'hsa_name'         => $agent['hsa_name'],
                                    'hsa_description'  => $agent['hsa_description'],
                                    'hsa_emt_recordid' => $agent['hsa_emt_recordid']
                                ];
                                
                                $this->sendEmails($hotspotData, $recipientIDs);
                                $this->setRecordPermissions($hotspotId, $recipientIDs);
                            }
                        }
                        else
                        {
                            $this->logger->logDebug('An open hotspot record with the same criteria already exists (recordid: '.$existingHotspot.')');
                        }
                    }
                }
            }
            
            // remove hotspot queue record
            \DatixDBQuery::PDO_query('DELETE FROM hotspots_queue WHERE recordid = ?', [$record['recordid']]);
            $this->logger->logDebug('Removed Hotspot queue record: '.$record['recordid']);
        }
        $this->logger->logDebug('Finished processing hotspots');
    }
    
    /**
     * Returns the relevant database column name for system-wide (location) field for a given module.
     * 
     * @param string $module     The module code.
     * @param string $code_table The code table that's used to uniquely identify the locaton field across modules.
     * 
     * @return string
     */
    protected function getLocationField($module, $code_table)
    {
        if ($this->locationFields === null)
        {
            require_once 'Source/generic_modules/HSA/FormFunctions.php';
        
            $this->locationfields = [];
            
            $fields = \DatixDBQuery::PDO_fetch_all("
                SELECT 
                    fmt_field, fmt_code_table, fmt_module
                FROM
                    field_formats
                WHERE
                    fmt_module IN ('".implode("','", array_keys(getHsaModules()))."')
                AND 
                    fmt_table IN (SELECT sfm_tables FROM subforms WHERE sfm_form = sfm_tables AND sfm_module = fmt_module)
                AND 
                    fmt_code_table IN ('!ORG','code_unit','!CLINGROUP','code_directorate','code_specialty','code_location','code_locactual')
                AND 
                    fmt_list_type NOT LIKE '%E%'");
            
            foreach ($fields as $field)
            {
                $this->locationfields[$field['fmt_module']][$field['fmt_code_table']] = $field['fmt_field'];
            }
        }
        return $this->locationfields[$module][$code_table];
    }
    
    /**
     * Recursively builds up a set of where clauses based on all the possible combinations defined by the agent criteria.
     * 
     * @param array  $conditionSets       The collection of agent conditions.
     * @param int    $setNum              The current condition set we're iterating over.
     * @param string $where               The sql string.
     * @param array  $params              The sql parameters.
     * @param array  $allConditionClauses An array of all possible condition combinations.
     * 
     * @return array $allConditionClauses
     */
    protected function generateConditionClauses(array $conditionSets, $setNum, $where = '', array $params = [], array $allConditionClauses = [])
    {
        $conditionClauses = $conditionSets[$setNum];
        $sw = new SqlWriter();
        
        foreach ($conditionClauses as $clause)
        {
            if ($where != '')
            {
                $tmpWhere = $where.' AND '.$clause['sql'];
            }
            else
            {
                $tmpWhere = $clause['sql'];
            }
            
            $tmpParams = array_merge($params, $clause['params']);
            
            if ($setNum + 1 >= count($conditionSets))
            {
                $allConditionClauses[] = [
                    'sql'    => $tmpWhere,
                    'params' => $tmpParams
                ];
            }
            else
            {
                $allConditionClauses = $this->generateConditionClauses($conditionSets, $setNum + 1, $tmpWhere, $tmpParams, $allConditionClauses);
            }
        }
        
        return $allConditionClauses;
    }
    
    /**
     * Sends hotspot notification e-mails.
     * 
     * @param array $hotspotData
     * @param array $recipients
     */
    protected function sendEmails(array $hotspotData, array $recipients)
    {
        $this->logger->logEmail('Sending notification e-mails when saving HOT with ID '.$hotspotData['recordid']);

        if ($hotspotData['hsa_emt_recordid'] == '')
        {
            $this->logger->logEmergency('Unable to e-mail Hotspot recipients (Hotspot ID .'.$hotspotData['recordid'].') - no e-mail template ID provided');
            return;
        }
        
        $contactFactory = new ContactModelFactory();
        $emailSender    = EmailSenderFactory::createEmailSender('HOT', 'Notify', $hotspotData['hsa_emt_recordid']);
    
        foreach ($recipients as $id)
        {
            if (null !== ($recipient = $contactFactory->getMapper()->find($id)))
            {
                $emailSender->addRecipient($recipient);
            }
            else
            {
                $this->logger->logEmergency('Unable to e-mail Hotspot recipient ID '.$id.' (Hotspot ID .'.$hotspotData['recordid'].')');
            }
        }
    
        $emailSender->sendEmails($hotspotData);
    }
    
    /**
     * Sets record-level permissions for the hotspot record for each notification recipient.
     * 
     * @param int   $recordid
     * @param array $recipients
     */
    protected function setRecordPermissions($recordid, array $recipients)
    {
        $this->logger->logEmail('Sending notification e-mails when saving HOT with ID '.$recordid);
        
        foreach ($recipients as $con_id)
        {
            $existingLinks = \DatixDBQuery::PDO_fetch('
                SELECT
                    COUNT(*)
                FROM
                    sec_record_permissions
                WHERE
                    module = :module
                AND 
                    tablename = :tablename
                AND 
                    link_id = :link_id
                AND 
                    con_id = :con_id
            ', ['HOT', 'hotspots_main', $recordid, $con_id], \PDO::FETCH_COLUMN);
            
            if ($existingLinks == 0)
            {
                \DatixDBQuery::PDO_query('
                    INSERT INTO sec_record_permissions
                        (module, tablename, link_id, con_id, updatedby, updateddate)
                    VALUES
                        (?,?,?,?,?,?)
                ', ['HOT', 'hotspots_main', $recordid, $con_id, 'ADM', date('d-M-Y H:i:s')]);
            }
        }
    }
    
    /**
     * Removes superfluous whitespace from a string.
     * 
     * Used to tidy SQL statements for use in descriptions/logs etc.
     * 
     * @param $string
     * 
     * @return $string
     */
    protected function tidyString($string)
    {
        // remove carriage returns
        $string = preg_replace('/\r|\n/', '', $string);
        
        // remove repeated whitespace
        $string = preg_replace('/\s+/', ' ', $string);
        
        return $string;
    }
}