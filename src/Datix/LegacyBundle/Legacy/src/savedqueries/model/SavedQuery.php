<?php
namespace src\savedqueries\model;

use src\framework\model\DatixEntity;
use src\framework\model\exceptions\ModelException;
use src\framework\query\exceptions\QueryException;
use src\framework\registry\Registry;
use src\framework\query\Query;
use src\framework\query\Where;

class SavedQuery extends DatixEntity
{
    /**
     * The initials of the user that created the report.
     * 
     * @var string
     */
    protected $createdby;
    
    /**
     * The date the report was created.
     * 
     * @var string
     */
    protected $createddate;
    
    /**
     * @var string
     */
    protected $module;
    
    /**
     * If we're using a "new" saved query, we'll have a proper Where object.
     * 
     * @var Where
     */
    protected $where;
    
    /**
     * If we're using an "old" saved query, we have to rely on a string.
     * 
     * @var string
     */
    protected $where_string;
    
    /**
     * The query name (not relevant when query is part of a packaged report).
     * 
     * @var string
     */
    protected $name;
    
    /**
     * The ID of the user that has exclusive access to this query.
     * 
     * If NULL then everyone has access.
     * 
     * @var int
     */
    protected $user_id;
    
    /**
     * The ID of the equivalent "old" query record.
     * 
     * @var int
     */
    protected $old_query_id;
    
    /**
     * The list type, if this query is based upon a predefined listing.
     * 
     * @var string
     */
    protected $list_type;
    
    /**
     * Whether or not this query is based on an overdue records listing.
     * 
     * @var boolean
     */
    protected $is_overdue;
    
    /**
     * {@inheritdoc}
     */
    public function __clone()
    {
        if ($this->where !== null)
        {
            $this->where = clone $this->where;
        }
        
        $this->name = null;
        $this->old_query_id = null;
        
        parent::__clone();
    }
    
    /**
     * Setter for Where.
     * 
     * @param Where $where
     */
    public function setWhere(Where $where)
    {
        $this->where = $where;
    }
    
    /**
     * Setter for where_string.
     * 
     * We remove the existing where property if this is set, the assumption being that
     * the where_string will contain the criteria in current Where object criteria.
     * 
     * @param string $where
     */
    public function setWhere_string($where_string)
    {
        $this->where_string = $where_string;
        $this->where = null;
    }
    
    /**
     * Getter for where.
     * 
     * @return Where
     */
    public function getWhere(SavedQueryModelFactory $factory = null)
    {
        if ($this->where === null)
        {
            // we attempt to fetch this data on access for the purposes of lazy-loading
            $factory = $factory ?: new SavedQueryModelFactory();
            $this->where = $factory->getMapper()->getSavedQueryWhere($this);
        }
        return $this->where;
    }
    
    /**
     * Setter for is_overdue.
     * 
     * @param boolean
     */
    public function setIs_overdue($is_overdue)
    {
        $this->is_overdue = (bool) $is_overdue;
    }
    
    /**
     * Creates a Query object from the saved query parameters.
     * 
     * @return Query
     */
    public function createQuery(Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();
        $moduleDefs = $registry->getModuleDefs();
        
        $from = $moduleDefs[$this->module]['VIEW'] ?: $moduleDefs[$this->module]['TABLE'];

        $query = new Query();
        $query->setModules = [$this->module];
        $query->from($from);
        
        if ($this->where_string != '')
        {
            $query->setWhereStrings([$this->where_string]);
        }
        
        if (null !== ($where = $this->getWhere()))
        {
            $query->where(clone $where);
        }
        
        // if $this->old_query_id has a value it means the value of the query field has been changed from [Current criteria] to a saved query so we shouldn't use
        // any predefined listing criteria
        if (isset($this->list_type) && is_null( $this->old_query_id ) )
        {
            $query->setPredefinedListingCriteria($this->module, $this->list_type, $this->is_overdue);   
        }

        return $query;
    }
    
    /**
     * Whether or not the query conditions contain any @prompt codes.
     * 
     * @return boolean
     */
    public function hasAtPrompt()
    {
        return $this->createQuery()->hasAtPrompt();
    }
    
    /**
     * Replace any @prompt codes within the query criteria with literal values.
     * 
     * @param string $module The module code is required in order to replace @prompt codes in where strings.
     */
    public function replaceAtPrompts($module = null)
    {
        $query = $this->createQuery();
        $query->replaceAtPrompts($module);
        
        $this->where = $query->getWhere();
        $this->where_string = $query->getwhereStrings()[0];
    }
    
    /**
     * Retrieves a list of field names from the query criteria that use @prompt as their comparison.
     * 
     * @param string $module The module code is required in order to retrieve @prompt fields from where strings.
     * 
     * @return array
     */
    public function getAtPromptFields($module = null)
    {
        try {
            return $this->createQuery()->getAtPromptFields($module);
        } catch (QueryException $e) {
        }
    }
}