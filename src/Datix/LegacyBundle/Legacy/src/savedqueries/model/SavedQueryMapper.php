<?php
namespace src\savedqueries\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\framework\query\Query;
use src\framework\query\FieldCollection;
use src\framework\query\WhereFactory;
use src\framework\query\SqlWriter;
use src\users\model\User;
use src\savedqueries\exceptions\SavedQueryDeleteException;
use src\system\database\CodeFieldInterface;

class SavedQueryMapper extends DatixEntityMapper
{
    /**
     * Keeps track of the top level criteria set that's attached to the query.
     * 
     * @var int
     */
    protected $criteria_id;
    
    /**
     * Keeps track of the criterion order.
     * 
     * @var int
     */
    protected $criterion_order;
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\savedqueries\\model\\SavedQuery';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'web_queries';
    }
    
    /**
     * Finds a saved query using the ID of the "old" query it's linked to.
     * 
     * @param int $id The ID of the "old" query.
     * 
     * @return SavedQuery|null
     */
    public function findByOldQueryID($id)
    {
        $query = new Query();
        $query->where(['old_query_id' => $id]);
        
        $data = $this->select($query);
        
        if (empty($data))
        {
            // we don't have a "new" saved query, so use the "old" one
            $this->db->setSQL('
                SELECT
                    q.sq_name AS name,
                    c.recordid AS user_id,
                    q.sq_module AS module,
                    q.sq_where AS where_string
                FROM
                    queries q
                LEFT JOIN
                    contacts_main c ON q.sq_user = c.login
                WHERE
                    q.recordid = ?');
            
            $this->db->prepareAndExecute([$id]);
            $data = $this->db->PDOStatement->fetch();

            //Todo: remove this
            $data['user_id'] = '';

            if (empty($data))
            {
                return null;
            }
        }
        else
        {
            $data = $data[0];
        }
        
        $data['old_query_id'] = $id;
        
        return $this->factory->getEntityFactory()->createObject($data);
    }
    
    /**
     * Fetches the WHERE clause attached to the saved query.
     * 
     * @param SavedQuery $savedQuery
     * 
     * @return Where|null
     */
    public function getSavedQueryWhere(SavedQuery $savedQuery, WhereFactory $whereFactory = null)
    {
        if (!( (int) $savedQuery->recordid > 0 ))
        {
            return null;
        }
        
        $this->db->setSQL('SELECT criteria_id FROM '.$this->getTable().' WHERE recordid = ?');
        $this->db->prepareAndExecute([$savedQuery->recordid]);
        $mainCriteriaID = $this->db->PDOStatement->fetch(\PDO::FETCH_COLUMN);

        $whereFactory = $whereFactory ?: new WhereFactory();
        return $whereFactory->createFromMapper($this->getCriteria($mainCriteriaID));
    }
    
    /**
     * Fecthes a saved query associated with a code field, used as a means for users to restrict code sets.
     * 
     * @param CodeFieldInterface $field The code field that this query is attached to.
     */
    public function getCodeFieldQuery(CodeFieldInterface $field)
    {
        // stub method, will be implemented as part of the work for DW-10208
        return;
    }
    
    /**
     * Fetches WHERE clause criteria based on its ID.  Called recursively to handle nested sets of criteria.
     * 
     * There's a fair amount of duplication in the dataset that's created by the SQL query - this is in order to 
     * try and keep the number of trips to the database to a minimum.
     * 
     * @param int $id The criteria ID.
     * 
     * @return array
     */
    protected function getCriteria($id)
    {
        $criteria = [];
        
        $sql = '
            SELECT
            	web_query_criteria.condition, web_query_criterion.nested_criteria_id, web_query_criterion.recordid as criterion_id,
                web_query_fields.recordid as field_id, web_query_fields.wrapper, web_query_field_names.recordid as name_id,
                web_query_field_names.name, web_query_field_comps.recordid as comp_id, web_query_field_comps.operator,
                web_query_field_values.recordid as value_id, web_query_field_values.value, web_query_field_values.is_field,
                web_query_field_subqueries.query_id as subquery_id, web_query_field_subqueries.field as subquery_field
            FROM
            	web_query_criteria
            INNER JOIN
            	web_query_criterion ON web_query_criteria.recordid = web_query_criterion.criteria_id
            LEFT JOIN
            	web_query_fields ON web_query_criterion.recordid = web_query_fields.criterion_id
            LEFT JOIN
            	web_query_field_names ON web_query_fields.recordid = web_query_field_names.field_id
            LEFT JOIN
            	web_query_field_comps ON web_query_fields.recordid = web_query_field_comps.field_id
            LEFT JOIN
            	web_query_field_values ON web_query_field_comps.recordid = web_query_field_values.comparison_id
            LEFT JOIN
            	web_query_field_subqueries ON web_query_field_comps.recordid = web_query_field_subqueries.comparison_id
            WHERE
            	web_query_criteria.recordid = ?
            ORDER BY
            	web_query_criterion.listorder, web_query_fields.recordid, web_query_field_names.listorder';
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute([$id]);
        $rawData = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
        
        foreach ($rawData as $data)
        {
            if ($data['nested_criteria_id'] != '')
            {
                $criteria[] = $this->getCriteria($data['nested_criteria_id']);
            }
            else
            {
                $criteria[] = $data;
            }
        }
        
        return $criteria;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doInsert(Entity $object)
    {
        $this->db->beginTransaction();
        
        try
        {
            $this->saveCriteria($object);
            $id = parent::doInsert($object);
            
            $this->db->commit();
            
            return $id;
        }
        catch (\Exception $e)
        {
            $this->db->rollBack();
            $this->registry->getLogger()->logEmergency(get_class($e).': '.$e->getMessage());
            
            throw new \MapperException('Unable to insert SavedQuery');
        }
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate(Entity $object)
    {
        $this->db->beginTransaction();
        
        try
        {
            $this->saveCriteria($object);
            parent::doUpdate($object);
            
            $this->db->commit();
            
            return true;
        }
        catch (\Exception $e)
        {
            $this->db->rollBack();
            $this->registry->getLogger()->logEmergency(get_class($e).': '.$e->getMessage());
            
            throw new \MapperException('Unable to update SavedQuery');
        }
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doDelete(Entity $object)
    {
        $this->deleteCriteria($object);
        return parent::doDelete($object);
    }
    
    /**
     * Deletes the criteria records for a given SavedQuery.
     * 
     * @param Entity $object The SavedQuery object.
     */
    protected function deleteCriteria(Entity $object)
    {
        $this->db->setSQL('SELECT criteria_id FROM '.$this->getTable().' WHERE recordid = ?');
        $this->db->prepareAndExecute([$object->recordid]);
        $mainCriteriaID = $this->db->PDOStatement->fetch(\PDO::FETCH_COLUMN);
        
        if ($mainCriteriaID != '')
        {
            list($criteriaIDs, $fieldIDs, $compIDs) = $this->getIDsForDelete($mainCriteriaID);
            
            if (!empty($compIDs))
            {
                $sql = 'DELETE FROM web_query_field_values WHERE comparison_id IN ('.implode(',', array_fill(0, count($compIDs), '?')).')';
                $this->db->setSQL($sql);
                $this->db->prepareAndExecute($compIDs);

                $sql = 'DELETE FROM web_query_field_subqueries WHERE comparison_id IN ('.implode(',', array_fill(0, count($compIDs), '?')).')';
                $this->db->setSQL($sql);
                $this->db->prepareAndExecute($compIDs);
            }
            
            if (!empty($fieldIDs))
            {
                $sql = 'DELETE FROM web_query_field_comps WHERE field_id IN ('.implode(',', array_fill(0, count($fieldIDs), '?')).')';
                $this->db->setSQL($sql);
                $this->db->prepareAndExecute($fieldIDs);
                
                $sql = 'DELETE FROM web_query_field_names WHERE field_id IN ('.implode(',', array_fill(0, count($fieldIDs), '?')).')';
                $this->db->setSQL($sql);
                $this->db->prepareAndExecute($fieldIDs);
                
                $sql = 'DELETE FROM web_query_fields WHERE recordid IN ('.implode(',', array_fill(0, count($fieldIDs), '?')).')';
                $this->db->setSQL($sql);
                $this->db->prepareAndExecute($fieldIDs);
            }
            
            if (!empty($criteriaIDs))
            {
                $sql = 'DELETE FROM web_query_criterion WHERE criteria_id IN ('.implode(',', array_fill(0, count($criteriaIDs), '?')).')';
                $this->db->setSQL($sql);
                $this->db->prepareAndExecute($criteriaIDs);
                
                $sql = 'DELETE FROM web_query_criteria WHERE recordid IN ('.implode(',', array_fill(0, count($criteriaIDs), '?')).')';
                $this->db->setSQL($sql);
                $this->db->prepareAndExecute($criteriaIDs);
            }
        }
    }
    
    /**
     * Fetches the linked data IDs required to completely remove a SavedQuery object from the database.
     * Called recursively to handle nested sets of criteria.
     * 
     * @param int $id The criteria ID.
     * 
     * @return array
     */
    protected function getIDsForDelete($id)
    {
        $criteriaIDs = [];
        $fieldIDs    = [];
        $compIDs     = [];
        
        $sql = '
            SELECT
            	web_query_criterion.nested_criteria_id, web_query_fields.recordid as field_id, web_query_field_comps.recordid as comp_id
            FROM
            	web_query_criteria
            INNER JOIN
            	web_query_criterion ON web_query_criteria.recordid = web_query_criterion.criteria_id
            LEFT JOIN
            	web_query_fields ON web_query_criterion.recordid = web_query_fields.criterion_id
            LEFT JOIN
            	web_query_field_comps ON web_query_fields.recordid = web_query_field_comps.field_id
            WHERE
            	web_query_criteria.recordid = ?';
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute([$id]);
        $result = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
        
        foreach ($result as $row)
        {
            if ($row['nested_criteria_id'] != '')
            {
                $nestedIDs   = $this->getIDsForDelete($row['nested_criteria_id']);
                $criteriaIDs = array_merge($criteriaIDs, $nestedIDs[0]);
                $fieldIDs    = array_merge($fieldIDs, $nestedIDs[1]);
                $compIDs     = array_merge($compIDs, $nestedIDs[2]);
            }
            else
            {
                $fieldIDs[] = $row['field_id'];
                $compIDs[]  = $row['comp_id'];
            }
        }
        
        $criteriaIDs[] = $id;
        
        return [array_unique($criteriaIDs), array_unique($fieldIDs), array_unique($compIDs)];
    }
    
    /**
     * Handles the peristence of where clause criteria.
     * 
     * @param Entity $object The SavedQuery object.
     */
    protected function saveCriteria(Entity $object)
    {
        $where = $object->where;
        
        if ($where !== null && $object->where_string != '')
        {
            // in this situation, we deconstruct the where object to a string and append to
            // the existing where string so the criteria is persisted in a consistent format
            $object->where_string = (new SqlWriter)->writeLiteralWhereClause($object->createQuery());
            unset($where);
        }
        
        if ((int) $object->recordid > 0)
        {
            $this->deleteCriteria($object);
        }
            
        if ($where !== null)
        {
            $this->criterion_order = 1;
            $this->doSaveCriteria($object->where->getCriteria(), $object->module);
        }
    }
    
    /**
     * Saves the query criteria the proper way (i.e. not as a string).
     * 
     * @param array   $criteria The where clause criteria, as extracted from the Where object.
     * @param boolean $topLevel A flag for the top level set of criteria (rather than a nested set).
     */
    protected function doSaveCriteria(array $criteria, $module, $parentCriteriaID = null)
    {
        $this->db->setSQL('INSERT INTO web_query_criteria (condition) VALUES (?)');
        $criteria_id = $this->db->insert([ $criteria['condition'] ]);
        
        if ($parentCriteriaID === null)
        {
            $this->criteria_id = $criteria_id;
        }
        else
        {
            $this->db->setSQL('INSERT INTO web_query_criterion (listorder, criteria_id, nested_criteria_id) VALUES (?, ?, ?)');
            $this->db->insert([ $this->criterion_order, $parentCriteriaID, $criteria_id ]);
            $this->criterion_order++;
        }
        
        foreach ($criteria['parameters'] as $parameter)
        {
            if ($parameter instanceof FieldCollection)
            {
                $this->db->setSQL('INSERT INTO web_query_criterion (listorder, criteria_id) VALUES (?, ?)');
                $criterion_id = $this->db->insert([ $this->criterion_order, $criteria_id ]);
                $this->criterion_order++;
                
                foreach ($parameter->getFields() as $field)
                {
                    $this->db->setSQL('INSERT INTO web_query_fields (criterion_id, wrapper) VALUES (?, ?)');
                    $field_id = $this->db->insert([ $criterion_id, $field->getWrapper() ]);
                    
                    foreach ($field->getOriginalNames() as $key => $name)
                    {
                        $this->db->setSQL('INSERT INTO web_query_field_names (field_id, name, listorder) VALUES (?, ?, ?)');
                        $this->db->insert([ $field_id, $name, $key+1 ]);
                    }
                    
                    foreach ($field->getComps() as $comparision)
                    {
                        $this->db->setSQL('INSERT INTO web_query_field_comps (field_id, operator) VALUES (?, ?)');
                        $comparison_id = $this->db->insert([ $field_id, $comparision['operator'] ]);

                        if ($comparision['value'] instanceof Query && $comparision['operator'] == FieldCollection::IN)
                        {
                            $subQuery = (new SavedQueryFactory)->createObject(array('where' => $comparision['value']->getWhere(), 'module' => $module));
                            (new SavedQueryModelFactory)->getMapper()->save($subQuery);

                            //we are looking to compare to a subquery, so we need to save the sub where as a new record.
                            $this->db->setSQL('INSERT INTO web_query_field_subqueries (comparison_id, query_id, field) VALUES (?, ?, ?)');
                            $this->db->insert([ $comparison_id, $subQuery->recordid, $comparision['value']->getFields()[0]]);
                        }
                        else
                        {
                            foreach ($comparision['value'] as $value)
                            {
                                if ($value !== null)
                                {
                                    // comparisons don't always have a value (e.g. is null etc)
                                    $this->db->setSQL('INSERT INTO web_query_field_values (comparison_id, value, is_field) VALUES (?, ?, ?)');
                                    $this->db->insert([ $comparison_id, $value, ($comparision['isField'] ? 1 : 0) ]);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // this is a nested set of criteria, so call this method recursively
                $this->doSaveCriteria($parameter, $module, $criteria_id);
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getEntityProperties(Entity $object)
    {
        $properties = parent::getEntityProperties($object) + ['criteria_id' => $this->criteria_id];
        $properties['is_overdue'] = $properties['is_overdue'] ? 1 : 0;
        
        return $properties;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function updateEntity(Entity $object)
    {
        parent::updateEntity($object);
        
        if ( !((int) $object->recordid > 0) )
        {
            // TODO this should be factored out for other entities that use these properties, maybe into a Trait?
            if ($_SESSION['CurrentUser'] instanceof User)
            {
                $createdby = $_SESSION['CurrentUser']->initials;
            }
            else
            {
                $createdby = '';
                $this->registry->getLogger()->logEmergency('Unable to determine logged-in user.');
            }
            
            $object->createdby   = $createdby;
            $object->createddate = date('d-M-Y H:i:s');
        }
    }
}