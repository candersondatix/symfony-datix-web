<?php
namespace src\savedqueries\model;

use src\framework\model\EntityCollection;

class SavedQueryCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\savedqueries\\model\\SavedQuery';
    }
}