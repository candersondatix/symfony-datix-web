<?php
namespace src\savedqueries\model\field;

use src\system\database\field\CodeField;
use src\system\database\code\CodeModelFactory;
use src\framework\query\Query;
use src\savedqueries\exceptions\SavedQueryException;
use src\users\model\User;

class SavedQueryField extends CodeField
{
    /**
     * {@inherit}
     */
    protected function doGetCodes(Query $query = null)
    {
        if (!($_SESSION['CurrentUser'] instanceof User))
        {
            throw new SavedQueryException('Cannot retrieve saved queries: unable to determine current user.');
        }
        
        $factory     = new CodeModelFactory($this);
        $this->codes = $factory->getCollection();
        
        // TODO - this logic belongs in SavedQueryField::applyFormQuery() instead.  Can be factored out when moving away from SelectFunctions.php.
        $qf    = $factory->getQueryFactory();
        $query = $query ?: $qf->getQuery();
        $where = $qf->getWhere();
        
        $where->add(
            $where->nest('OR',
                $qf->getFieldCollection()
                    ->field('sq_user_id')->eq($_SESSION['CurrentUser']->recordid),
                $qf->getFieldCollection()
                    ->field('sq_user_id')->isNull()
            ),
             $where->nest('OR',
                $qf->getFieldCollection()
                    ->field('sq_type')->notEq('UI'),
                $qf->getFieldCollection()
                    ->field('sq_type')->isNull()
            )
        );
        
        $query->where($where);
        
        $this->codes->setQuery($query);
    }
    
    /**
     * {@inherit}
     */
    public function getCodeTable()
    {
        return 'queries';
    }
    
    /**
     * {@inherit}
     */
    public function getCodeFields()
    {
        if (!isset($this->codeFields))
        {
            $this->codeFields = ['recordid', 'sq_name'];
        }
        return $this->codeFields;
    }
    
    /**
     * {@inherit}
     */
    public function getCodeOrderColumn()
    {
        return 'sq_name';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCodeColumn()
    {
        return 'recordid';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptionColumn()
    {
        return 'sq_name';
    }
}