<?php
namespace src\savedqueries\controllers;

use src\framework\controller\TemplateController;
use src\users\model\UserModelFactory;

class ShowSavedQueriesTemplateController extends TemplateController
{
    function savedqueries()
    {
        require_once 'Source/libs/SavedQueries.php';

        $error = ($this->request->getParameter('error') ? $this->request->getParameter('error') : '');
        $module = \Sanitize::getModule($this->request->getParameter('module'));
        $qry_id = ($this->request->getParameter('qry_id') ? $this->request->getParameter('qry_id') : '');
        $qry_name = $this->request->getParameter('qry_name');
        $qry_recordid = ($this->request->getParameter('qry_recordid') ? $this->request->getParameter('qry_recordid') : '');
        $hasPermissions = ($_SESSION['CurrentUser']->canSeeModule($module));

        if ($hasPermissions === false)
        {
            AddSessionMessage('ERROR', _tk('no_permission_to_access_module'));
            $this->hasMenu = false;
        }

        $qry['linktypes'] = $LinkTypes;
        $qry['error'] = $error;

        unset($_SESSION[$module]['PROMPT']['PROMPTSUBMITTED']);

        $this->title = _tk('saved_queries');
        $this->image = "Images/icons/icon_{$module}_save.png";
        $this->module = $module;
        $this->hasPadding = false;

        $this->response->build('src/savedqueries/views/ShowSavedQueries.php', array(
            'module' => $module,
            'qry_id' => $qry_id,
            'qry_name' => $qry_name,
            'qry_recordid' => $qry_recordid,
            'qry' => $qry,
            'FormAction' => $FormAction,
            'hasPermissions' => $hasPermissions
        ));
    }

    public function showSavedQueryForm()
    {
        global $scripturl;

        $AdminUser   = $_SESSION['AdminUser'];
        $qryRecordId = $this->request->getParameter('qry_recordid');
        $formAction  = ($this->request->getParameter('formAction') ?: '');
        $error       = ($this->request->getParameter('error') ?: '');
        $module      = $this->request->getParameter('module');

        if ($qryRecordId)
        {
            $sql = "
                SELECT
                    recordid as qry_recordid, sq_name, sq_where, sq_tables, sq_module, sq_user_id
                FROM
                    queries
                WHERE
                    recordid = :recordid
            ";

            $qry = \DatixDBQuery::PDO_fetch($sql, ['recordid' => $qryRecordId]);

            $this->title = 'Edit saved query';
        }
        else
        {
            if ($formAction == 'new')
            {
                if (!empty($_SESSION[$module]['current_drillwhere']))
                {
                    $qry['sq_where'] = $_SESSION[$module]['current_drillwhere'];
                    $_SESSION[$module]['WHERE'] = $_SESSION[$module]['current_drillwhere'];
                    $_SESSION[$module]['current_drillwhere'] = '';
                }
                else
                {
                    $qry['sq_where'] = $_SESSION[$module]['WHERE'];
                }

                $qry['sq_tables'] = $_SESSION[$module]['TABLES'];
            }
            else
            {
                $_POST['sq_where'] = StripPostQuotes($_POST['sq_where']);
                $qry['sq_tables'] = \Sanitize::SanitizeString($_POST['sq_tables']);
            }

            $this->title = 'New saved query';
        }

        $qry['error'] = $error;

        if ($error)
        {
            $module = filter_var($module, FILTER_SANITIZE_STRING);
            $qry = StripPostQuotesArray($this->request->getParameters());
        }
        else
        {
            $module = \Sanitize::getModule($module);
        }

        if (!empty($qry['sq_user_id']) || $formAction == 'new')
        {
            $qry['qry_type'] = 'USER';
        }

        $LinkTypes['ALL'] = 'Accessible to everyone';
        $LinkTypes['USER'] = 'Accessible to you only';
        $qry['linktypes'] = $LinkTypes;

        $this->module = $module;
        $this->hasPadding = false;
        $this->addJs('src/savedqueries/js/savedqueries.js');

        $Factory = new UserModelFactory();
        $User = $Factory->getMapper()->findByInitials($_SESSION["initials"]);

        require_once 'Source/security/SecurityBase.php';
        $Perms = ParsePermString($User->permission);

        $allowSetup = $Perms[$module]["disallow_setup"] === false;

        // Is this user the owner of the query?
        $sql = 'SELECT 1 FROM QUERIES WHERE recordid = :recordid AND sq_user_id = (SELECT recordid FROM staff WHERE initials = :initials)';

        $isOwner = \DatixDBQuery::PDO_fetch($sql, [
            'recordid' => $qryRecordId,
            'initials' => $User->initials
        ], \PDO::FETCH_COLUMN);

        $this->response->build('src\savedqueries\views\SavedQueriesForm.php', [
            'scripturl'  => $scripturl,
            'module'     => $module,
            'formAction' => $formAction,
            'qryName'    => $this->request->getParameter('qry_name'),
            'qry'        => $qry,
            'table'      => $this->request->getParameter('table'),
            'AdminUser'  => $AdminUser,
            'allowSetup' => $allowSetup,
            'isOwner'    => $isOwner
        ]);
    }
}