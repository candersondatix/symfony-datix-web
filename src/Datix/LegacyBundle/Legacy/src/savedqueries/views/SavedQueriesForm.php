<form enctype="multipart/form-data" method="post" name="frmSavedQuery" action="<?php echo $this->scripturl.'?action=savequeryaction'; ?>">
    <input type="hidden" name="module" value="<?php echo Sanitize::getModule($this->module); ?>" />
    <input type="hidden" name="form_action" value="<?php echo Escape::EscapeEntities($this->formAction); ?>" />
    <input type="hidden" name="qry_name" value="<?php echo \src\security\Escaper::escapeForHTMLParameter($this->qryName); ?>" />
    <input type="hidden" name="qry_recordid" value="<?php echo Sanitize::SanitizeInt($this->qry["qry_recordid"]); ?>" />
    <input type="hidden" name="sq_tables" value="<?php echo \src\security\Escaper::escapeForHTMLParameter($this->qry["sq_tables"]); ?>" />
    <input type="hidden" name="table" value="<?php echo \src\security\Escaper::escapeForHTMLParameter($this->table); ?>" />
    <input type="hidden" name="rbWhat" value="save" />
    <div class="new_windowbg">
        <?php require_once 'Source/libs/SavedQueries.php'; ?>
        <?php echo SavedQueryDetailsSection($this->qry, $this->formAction, $this->module); ?>
    </div>
    <div class="button_wrapper">
        <?php if((!$this->AdminUser && ($this->qry['qry_type'] == 'USER' || $this->formAction == 'new')) || $this->AdminUser || $this->allowSetup) : ?>
        <input type="submit" value="Save" onClick="document.forms[0].rbWhat.value='save'" />
        <?php endif; ?>
        <input type="button" value="Cancel" onClick="document.forms[0].rbWhat.value='cancel'; if (confirm('<?php echo Escape::EscapeEntities(_tk('cancel_confirm')); ?>')){this.form.submit();}" />
        <?php if ($this->formAction != 'new' && $this->qry['qry_recordid'] != '' && ($this->AdminUser || $this->allowSetup || ($this->isOwner == '1'))) : ?>
        <input type="button" value="Delete" onClick="confirmDelete();" />
        <?php endif; ?>
    </div>
</form>