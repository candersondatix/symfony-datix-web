<div class="section_title_row"><b>Copy records</b></div>
<div class="windowbg2 padded_wrapper">
    <div><b>The selected <?php echo $this->RecName; ?> record(s) have been copied successfully.</b></div>
</div>
<div class="button_wrapper">
    <input type="button" value="<?php echo _tk('list_search'); ?>" name="btnSearchListings" onclick="SendTo('<?php echo $this->scripturl; ?>?action=list&module=<?php echo htmlspecialchars($this->module); ?>&listtype=search');" />
</div>