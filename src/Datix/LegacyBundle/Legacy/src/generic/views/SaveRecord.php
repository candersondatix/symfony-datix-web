<?php if ($this->ajaxEmails) : ?>
<!-- Send Email alerts via Ajax command -->
<script language="javascript" type="text/javascript" src="js_functions/EmailAlerts<?php echo $this->addMinExtension; ?>.js"></script>
<script language="javascript" type="text/javascript">
    SendEmailAlertsMessage('<?php echo $this->module; ?>', <?php echo $this->recordid; ?>, '<?php echo htmlspecialchars($this->data['rep_approved']); ?>', '<?php echo htmlspecialchars($this->data['rep_approved_old']); ?>', <?php echo $this->FormLevel; ?>);
</script>
<?php endif; ?>
<form method="post" name="frm<?php echo $this->module; ?>" action="<?php echo $this->scripturl; ?>">
    <input type="hidden" name="form_action" value="" />
    <input type="hidden" name="form_id" value="<?php echo $this->formID; ?>" />
    <input type="hidden" name="recordid" value="<?php echo $this->recordid; ?>" />
    <input type="hidden" id="form-panel" name="panel" value="<?php echo ($this->panel ?: ''); ?>" />
    <input type="hidden" name="module" value="<?php echo $this->module; ?>" />
    <!-- Because incidents always has to work differently. -->
    <?php if ($this->module == 'INC' && $this->FormLevel == 1) : ?>
    <div class="section_title_row">
        <b><?php echo Sanitize::SanitizeRaw(_tk('INC_1_saved_title').$this->data['inc_ourref']); ?></b>
    </div>
    <?php elseif (in_array($this->module, array('CQO', 'CQP', 'CQS'))) : ?>
    <div class="section_title_row">
        <b><?php echo Sanitize::SanitizeRaw(_tk($this->module.'_saved_title')); ?></b>
    </div>
    <?php else : ?>
    <div class="section_title_row">
        <b><?php echo Sanitize::SanitizeRaw(_tk($this->module.'_saved_title').$this->recordid); ?></b>
    </div>
    <?php endif; ?>
    <div class="windowbg2 padded_wrapper">
        <?php if ($this->FormLevel == 1) : ?>
            <?php if ($this->module == 'INC') : ?>
            <div>
                <b><?php echo Sanitize::SanitizeRaw(_tk('INC_report_saved').$this->data['inc_ourref']); ?></b>
            </div>
            <?php else : ?>
            <div>
                <b><?php echo Sanitize::SanitizeRaw(_tk($this->module.'_report_saved').$this->recordid); ?></b>
            </div>
            <?php endif; ?>
        <?php else : ?>
        <div>
            <b>
                <?php echo _tk($this->module . '_2_record_saved'); ?>
                <?php if ($this->USES_APPROVAL_STATUSES !== false) : ?>
                    "<?php echo (_tk('approval_status_'.$this->module.'_'.$this->data['rep_approved']) ?: code_descr($this->module, 'rep_approved', $this->data['rep_approved'])); ?>"
                <?php endif; ?>
                <?php if (!in_array($this->module, array('CQO', 'CQP', 'CQS'))) : ?>
                    .&nbsp;<?php echo _tk('level_2_record_ref') . $this->recordid; ?>
                <?php endif; ?>
            </b>
        </div>
        <?php endif; ?>
        <?php if ($this->message) : ?>
        <?php echo $this->message; ?>
        <?php endif; ?>
    </div>
    <div class="button_wrapper">
        <?php if ($_SESSION['logged_in'] && (!$this->INPUT_ONLY_LEVELS || !in_array(GetAccessLevel($this->module), $this->INPUT_ONLY_LEVELS))) : ?>
            <?php if (bYN(GetParm('PRINT_FINISH_BTN_'.$this->module, 'N'))) : ?>
            <input type="button" value="<?php echo _tk('btn_print_and_finish'); ?>" name="btnFinishPrint" onclick="SendTo('<?php echo $this->scripturl; ?>');SendTo('<?php echo $this->scripturl; ?>?action=<?php echo $this->EditAction; ?>&recordid=<?php echo $this->recordid; ?>&print=1','_blank');" />
            <?php endif; ?>
            <input type="button" value="<?php echo _tk('btn_back_to_record'); ?>" name="btnEditRisk" onclick="SendTo('<?php echo $this->recordurl; ?>');" />
            <?php if ($_SESSION[$this->module]['WHERE'] && in_array($this->module, array('CQO', 'CQP', 'CQS'))) : ?>
            <input type="button" value="Back to listing" name="btnEditRisk" onclick="SendTo('<?php echo $this->scripturl; ?>?action=list&module=<?php echo $this->module; ?>&listtype=search');" />
            <?php endif; ?>
        <?php else : ?>
            <?php if ($_SESSION["logged_in"]) : ?>
            <input type="button" value="<?php echo _tk('btn_add_another_record'); ?>" name="btnEditRisk" onclick="SendTo('<?php echo $this->scripturl; ?>?action=addnew&module=<?php echo $this->module; ?>&level=<?php echo $this->FormLevel; ?>');" />
            <?php else : ?>
            <input type="button" value="<?php echo _tk('btn_add_another_record'); ?>" name="btnEditRisk" onclick="SendTo('<?php echo $this->scripturl; ?>?module=<?php echo $this->module; ?>&form_id=<?php echo $this->data['form_id']; ?>');" />
            <?php endif; ?>
        <?php endif; ?>
    </div>
</form>
<?php if ($this->printAfterSubmit == 1) : ?>
<script language="Javascript" type="text/javascript">
    SendTo('<?php echo getRecordURL(array('module' => $this->module, 'recordid' => $this->recordid)); ?>&print=1&submitandprint=1&form_id=<?php echo ($this->formID != null ? $this->formID : $this->data['form_id']); ?>&token=<?php echo CSRFGuard::getCurrentToken() ?>', '_blank');
</script>
<?php endif; ?>