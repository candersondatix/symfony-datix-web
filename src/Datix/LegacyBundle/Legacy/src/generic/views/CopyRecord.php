<form method="post" name="frmCopyOptions" action="<?php echo $this->scripturl; ?>?action=docopyrecord&amp;module=<?php echo htmlspecialchars($this->module); ?>">
    <input type="hidden" name="module" value="<?php echo htmlspecialchars($this->module); ?>" />
    <?php if ($this->recordid) : ?>
    <input type="hidden" name="recordid" value="<?php echo $this->recordid; ?>" />
    <?php endif; ?>
    <div class="new_windowbg">
        <?php echo $this->CopyOptionsSection->GetFormTable(); ?>
    </div>
    <div class="button_wrapper">
        <input type="hidden" name="rbWhat" value="copy" />
        <input type="button" value="<?php echo _tk('btn_copy'); ?>"  onClick="copyBtnOnClick('<?php echo $this->numSelectedRecords; ?>');" />
        <input type="submit" value="<?php echo _tk('btn_cancel'); ?>" onClick="document.forms[0].rbWhat.value='cancel';" />
    </div>
</form>