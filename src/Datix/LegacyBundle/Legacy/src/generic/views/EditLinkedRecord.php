<script language="Javascript" type="text/javascript">
    <?php echo $this->MakeJavaScriptValidation; ?>
    submitClicked = false;
</script>
<form enctype="multipart/form-data" method="post" name="frmDocument" onsubmit="return(submitClicked)" action="<?php echo $this->scripturl; ?>?action=linkrecords&recordid=<?php echo Sanitize::SanitizeInt($this->current_id); ?>">
    <input type="hidden" id="currentMod" name="currentMod" value="<?php echo htmlspecialchars($this->current_mod); ?>">
    <input type="hidden" id="currentID" name="currentID" value="<?php echo Sanitize::SanitizeInt($this->current_id); ?>">
    <input type="hidden" id="postedaction" name="postedaction" value="action=<?php echo htmlspecialchars($this->action); ?>">
    <input type="hidden" name="editMod" id="editMod" value="<?php echo htmlspecialchars($this->edit_mod); ?>">
    <input type="hidden" name="editID" id="editID" value="<?php echo Sanitize::SanitizeInt($this->edit_id); ?>">
    <input type="hidden" name="rbWhat" value="Save" />
    <div class="new_windowbg">
        <?php echo $this->Table->GetFormTable(); ?>
    </div>
    <div class="button_wrapper">
        <input type="button" name="saveRecordButton" value="Save" onClick="submitClicked=true;if(validateOnSubmit()){document.forms[0].rbWhat.value='<?php echo ($this->editMode ? 'Save' : 'Link'); ?>';document.forms[0].submit();}"/>
        <input type="button" name="cancelRecordButton" value="Cancel" onClick="document.forms[0].rbWhat.value='Cancel';document.forms[0].submit();" />
        <?php if ($this->editMode) : ?>
        <input type="button" name="unlinkRecordButton" value="Unlink" onClick="if(confirm('Are you sure you want to remove this link')){ document.forms[0].rbWhat.value='Unlink';document.forms[0].submit(); }" />
        <?php endif; ?>
    </div>
</form>