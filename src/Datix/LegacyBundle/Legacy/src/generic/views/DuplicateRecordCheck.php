        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <thead>
                <tr class="tableHeader head2">
                    <th width="6%">Choose</th>
                    <?php foreach($this->fields as $field_name => $label): ?>
                    <th><?= $label ?></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php if ($this->hidden_matches): ?>
                    <tr data-unauthorised="true"><td colspan="<?=count($this->fields)+1?>"><div class="warning-msg"><span class="ui-icon ui-icon-alert"></span><?= _tk('no_perms_view_num') . ' ' . $this->hidden_matches . ' ' . ($this->hidden_matches == 1 ? _tk('record') : _tk('records')); ?></div></td></tr>
                <?php endif; ?>
                <?php /*foreach($this->rows as $row): ?>
                <tr>
                    <td><input type="button" value="Choose" data-record-id="<?=$row['recordid']?>" onclick="if(confirm('<?= _tk('confirm_unsaved_changes'); ?>')){SendTo('index.php?module=<?=$this->module?>&action=record&recordid=<?=$row['recordid']?>');}"/></td>
                    <?php foreach($this->fields as $field_name => $label): ?>
                        <td><?= $row[$field_name] ?></td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach;*/ ?>
            </tbody>
        </table>