<div style="padding:5px;">
    <b>The <?php echo $this->REC_NAME; ?> record (<?php echo $this->GeneratedRecordID; ?>) has been generated successfully.</b>
</div>
<div class="button_wrapper">
    <input type="button" value="<?php echo _tk('btn_back_to_main_menu'); ?>" name="btnFinish" onclick="SendTo('app.php?action=home&module=<?php echo htmlspecialchars($this->module); ?>');" />
    <input type="button" value="<?php echo _tk('btn_back_to_source_record'); ?>" name="btnGotoSource" onclick="SendTo('app.php?action=<?php echo $this->action_src_record; ?>&fromsearch=1&recordid=<?php echo $this->recordid; ?>');" />
    <input type="button" value="<?php echo _tk('btn_goto_generated_record'); ?>" name="btnGotoGenerated" onclick="SendTo('app.php?action=<?php echo $this->action_generated_record; ?>&fromsearch=1&recordid=<?php echo $this->GeneratedRecordID; ?>');" />
</div>