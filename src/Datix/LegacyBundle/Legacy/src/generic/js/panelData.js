/**
 * Checks if a panel contains data and add an indicator to the tab if it does
 *
 * @param $elems list of panel elements to check
 * @returns {boolean}
 */
function hasData($elems) {

    var excludedFields = ['fbk_subject', 'fbk_body', 'fin_calc_total', 'fin_calc_ourshare', 'cla_insurer_excess', 'fin_calc_liability', 'fin_calc_reimbursement', 'fin_calc_reserve_1', 'fin_calc_reserve_2', 'pay_type_DAMA', 'pay_type_DEFE', 'pay_type_PLAI', 'pay_type_RECE', 'pay_type_PAYTOTAL'];

    $elems.each(function() {

        var $elem = jQuery(this),
            $target = jQuery('#' + $elem.attr('id').replace(/^panel/i, 'menu')),
            hasData = false;

        /**
         * Get all non hidden or button input fields and add textareas, multiselect lists and tables to the collection
         */
        var $matched = $elem.find('input').not('[type="hidden"],[type="button"]').add($elem.find('textarea, .multiSelectList, table.gridlines'));

        /**
         * Filter excluded fields from collection
         */
        $matched = $matched.filter(function() {

            return (jQuery.inArray(jQuery(this).attr('id'), excludedFields) == -1);

        });

        $matched.each(function() {

            var $item = jQuery(this);

            var noDataMessages = ['No messages', 'No notification e-mails sent', 'No hotspot records.', 'No linked users.', 'No linked groups.'];

            if(
                ($item.attr('type') == 'text' && $item.val().length > 0) ||
                (jQuery.inArray($item.attr('type'), ['checkbox', 'radio']) > -1 && $item.prop("checked")) ||
                ($item.is("textarea") && $item.text().length > 0) ||
                ($item.is("textarea") && $elem.find('input#CURRENT_' + $item.attr('id')).length > 0 && $elem.find('input#CURRENT_' + $item.attr('id')).val().length > 0) ||
                ($item.hasClass('multiSelectList') && $item.find('ul').children('li').length > 0) ||
                ($item.hasClass('gridlines') && $item.find('tr').not('.tableHeader').length > 0 && jQuery.inArray(jQuery.trim($item.find('tr').last().find('td').text()), noDataMessages) == -1 && jQuery.inArray(jQuery.trim($item.find('tr').first().find('td').text()), noDataMessages) == -1)
                ) {

                hasData = true;
            }
        });

        if(hasData) {

            $target.addClass('has-data');
        }
        else
        {
            $target.removeClass('has-data');
        }
    });

    return false;
}

jQuery(function(){

    hasData(jQuery('.panel'));
});