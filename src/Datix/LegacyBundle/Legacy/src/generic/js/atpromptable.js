(function( $ ) {

    $.fn.atpromptable = function() {

        return this.each(function() {

            var $obj = $(this);

            var initialValue = $obj.val(),
                $parent = $obj.closest('.field_input_div'),
                $label = $obj.closest('.field_div').find('.field_label'),
                $textInput = $(),
                $button = $();

            setupInput();

            function setupInput() {

                $obj.detach();

                var $output = $('<div/>');

                $textInput = $obj.clone();

                $textInput.attr({
                    'type': 'text',
                    'pattern': '[0-9]*',
                    'class': 'codefield'
                }).css({minWidth: '100px'});

                $button = $('<a/>', {
                    'class':'dropdown_button_image_mobile'
                });

                $obj.attr({
                    'id': $obj.attr('id') + '-' + $obj.attr('type'),
                    'name': $obj.attr('name') + '_' + $obj.attr('type')
                }).hide();

                $output.append($textInput, $button, $obj);

                $parent.append($output);

                $label.find('.field_date_format').show().text(' (yyyy-mm-dd)');

                $obj.on('blur', function() {

                    $textInput.val($obj.val());
                    closeDate();
                    $obj.val('');
                });

                $button.on('click', function(){

                    toggleDate();
                });
            }

            function toggleDate() {

                if($obj.is(':visible')) {

                    closeDate()
                }
                else {

                    showDate();
                }
            }

            function showDate() {

                $obj.show();
                $button.addClass('dropdown_button_image_mobile_close').removeClass('dropdown_button_image_mobile');
            }

            function closeDate() {

                $obj.hide();
                $button.addClass('dropdown_button_image_mobile').removeClass('dropdown_button_image_mobile_close');
            }

            $obj.data('atpromptable', $obj);

            return $obj;
        });
    };
}(jQuery));