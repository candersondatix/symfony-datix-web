<?php

namespace src\generic\controllers;

use src\framework\controller\Controller;

class DuplicateRecordCheckController extends Controller
{
    function DuplicateRecordCheck()
    {
        $value    = $this->request->getParameter('value');
        $module   = $this->request->getParameter('module');
        $recordid = $this->request->getParameter('recordid');

        // get listing
        $FormDesign = \Forms_FormDesign::GetFormDesign(['module' => $module, 'level' => 2]);
        $listing_id = $FormDesign->ListingDesigns['DUPLICATES']['cla_name'];

        // get listing columns
        require_once 'Source/libs/ListingClass.php';
        $Listing = new \Listing($module, $listing_id);
        $Listing->LoadColumnsFromDB();

        // column labels
        $fields = [];
        foreach (array_keys($Listing->Columns) as $column) {
            $fields[$column] = \Labels_FieldLabel::GetFieldLabel($column);
        }

        // TODO: Replace this with a claim mapper in version 140
         $whereClause = \MakeSecurityWhereClause('', $module, $_SESSION['initials']);

        // select accessible matching records
        $sql = "SELECT recordid, ".join(', ', array_keys($Listing->Columns))." FROM claims_main WHERE cla_name = :cla_name";
        $sql .= " AND recordid != :recordid" . ($whereClause ? " AND ({$whereClause})" : "");
        $matched_rows = \DatixDBQuery::PDO_fetch_all($sql, ['cla_name' => $value, 'recordid' => $recordid]);

        // get total matching record count
        $sql = "SELECT count(recordid) AS number FROM claims_main WHERE cla_name = :cla_name AND recordid != :recordid";
        $row = \DatixDBQuery::PDO_fetch($sql, ['cla_name' => $value, 'recordid' => $recordid]);
        $hiddenMatches = $row['number'] - count($matched_rows);

        // REFACTOR: this should just return the results in json,
        // and the html should be built by the javascript
        $this->response->build('src/generic/views/DuplicateRecordCheck.php', array(
            'fields' => $fields,
            //'rows'   => $matched_rows,
            'hidden_matches' => $hiddenMatches,
            'module' => $module
        ));

        /**
         * Remove whitespace between tags to fix a bug in IE9 (yay - browser specific features)
         */
        $body = preg_replace(array('/td>\s*<\/td/', '/([a-z]+)>\s+<(\/?[a-z]+)/i'), array('td>&nbsp;</td', '$1><$2'), (string) $this->response);

        $this->response->setBody (
            json_encode([
                'status'     => 'success',
                'numResults' => count($matched_rows) + $hiddenMatches,
                'fields'     => $fields,
                'rows'       => $matched_rows,
                'text'       => array('confirmMsg' => _tk('confirm_unsaved_changes')),
                'html'       => $body // HACK: returning html in a json sucks - reduced amount of HTMl being sent back
            ])
        );
    }
}
