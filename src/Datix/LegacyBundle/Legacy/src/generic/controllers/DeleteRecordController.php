<?php

namespace src\generic\controllers;

use src\framework\controller\Controller;

class DeleteRecordController extends Controller
{
    /**
     * Deletes a generic record
     *
     * Note: Used only in Payments module
     */
    function deleterecord()
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $Module = \Sanitize::getModule($this->request->getParameter('module'));
        $Recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $LinkModule = \Sanitize::getModule($this->request->getParameter('link_module'));
        $MainRecordid = \Sanitize::SanitizeInt($this->request->getParameter('main_recordid'));

        // Check that we can perform this deletion.
        if (CanDeleteRecord($Module))
        {
            $SQLWhere = MakeSecurityWhereClause('recordid = '. $Recordid, $Module);

            $sql = '
                SELECT
                    count(*) as num
                FROM
                    ' . $ModuleDefs[$Module]['TABLE'] . '
                WHERE
                    ' . $SQLWhere;

            $AccessPermissions = \DatixDBQuery::PDO_fetch($sql, array(), \PDO::FETCH_COLUMN);

            if ($AccessPermissions == '1')
            {
                $sql = '
                    DELETE FROM
                        ' . $ModuleDefs[$Module]['TABLE'] . '
                    WHERE
                        recordid = :recordid
                ';

                $DeletionComplete = \DatixDBQuery::PDO_query($sql, array('recordid' => $Recordid));
            }
        }
        else
        {
            AddSessionMessage('ERROR', 'You do not have permission to delete this record.');
            $this->call('src\generic\controllers\ShowRecordController', 'record');
            return;
        }

        if ($DeletionComplete)
        {
            if($Module == 'PAY')
            {
                require_once 'source/generic_modules/CLA/ModuleFunctions.php';
                $claimData = zeroReservesOnDate(array('main_recordid' => $MainRecordid, 'module' => 'PAY', 'pay_module' => 'CLA'));
            }

            AddSessionMessage('INFO', 'The ' . $ModuleDefs[$Module]['REC_NAME'] . ' has been deleted');

            if ($ModuleDefs[$Module]['LINKED_MODULE'] && $LinkModule && $MainRecordid)
            {
                $RedirectLocation = getRecordURL(array('module' => $LinkModule, 'recordid' => $MainRecordid));
            }
            else
            {
                $RedirectLocation = getRecordURL(array('module' => $Module, 'recordid' => $Recordid));
            }

            $this->redirect($RedirectLocation);
        }
        else
        {
            AddSessionMessage('ERROR', 'A problem was encountered when deleting this record.');
            $this->call('src\generic\controllers\ShowRecordController', 'record');
        }
    }
}