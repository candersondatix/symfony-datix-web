<?php

namespace src\generic\controllers;

use src\framework\controller\Controller;
use src\system\database\field\StringField;

class RecordController extends Controller
{
    /**
     * Handles redirection if user confirms generation or cancels
     */
    public function DoGenerateRecord()
    {
        if (!is_array($this->request->getParameters()))
        {
            $form_action = 'Cancel';
        }
        else
        {
            $form_action = $this->request->getParameter('rbWhat');
        }

        $module = $this->request->getParameter('module');
        $recordid = $this->request->getParameter('recordid');

        switch ($form_action)
        {
            case 'cancel':
            case BTN_CANCEL:
                $this->response = $this->call('src\generic\controllers\ShowRecordController', 'record', array(
                    'module' => $module,
                    'fromsearch' => '1',
                    'recordid' => $recordid
                ));
                return;
        }
        //check to see a module has been provided for the new record
        if (!$this->request->getParameter('generate_module'))
        {
            AddSessionMessage('ERROR', 'Module to generate record in needs to be filled in before you can submit the form');
            $this->response = $this->call('src\generic\controllers\RecordTemplateController', 'generaterecord');
            return;
        }
        //check to see an approval status has been provided for new record
        if (!$this->request->getParameter('relevant_approval_status'))
        {
            AddSessionMessage('ERROR', 'Approval status needs to be filled in before you can submit the form');
            $this->response = $this->call('src\generic\controllers\RecordTemplateController', 'generaterecord');
            return;
        }

        $aCopyParams['module'] = \Sanitize::getModule($this->request->getParameter('module'));
        $aCopyParams['recordid'] = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $aCopyParams['generate_module'] = \Sanitize::getModule($this->request->getParameter('generate_module'));
        $aCopyParams['where'] = \Sanitize::SanitizeString($_SESSION[$module]['WHERE']);
        $aCopyParams['copies'] = 1; //$_POST["copies"];
        $aCopyParams['copy_linked_contacts'] = \Sanitize::SanitizeString($this->request->getParameter('copy_linked_contacts'));
        $aCopyParams['generate_link_to_master'] = \Sanitize::SanitizeString($this->request->getParameter('generate_link_to_master'));
        if ($this->request->getParameter('relevant_approval_status') != null)
        {
            $aCopyParams['approval_status'] = \Sanitize::SanitizeString($this->request->getParameter('relevant_approval_status'));
        }
        $GeneratedRecordID = $this->GenerateMainTable($aCopyParams);

        $this->response = $this->call('src\generic\controllers\RecordTemplateController', 'ShowGenerateFinished', array(
            'aParams' => $aCopyParams,
            'generatedRecordID' => $GeneratedRecordID
        ));
    }

    /**
     * Selects main module record data for copying and generates new record for destination module based on
     * options chosen. Confirms to the user if successful.
     *
     * @param string $Module Module to insert into.
     * @param string $where Module to insert into.
     * @param int $copies Number of copies required
     * @param $copy_linked_contacts Y/N whether to copy linked contacts
     * @param $generate_link_to_master Y/N whether to create a link to the master record being copied.
     *
     */
    private function GenerateMainTable($aParams)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $Module = $aParams['module'];
        $recordid = $aParams['recordid'];
        $GenerateModule = $aParams['generate_module'];
        $MappedData = array();
        $where = $aParams['where'];
        $copies = $aParams['copies'];
        $copy_linked_contacts = $aParams['copy_linked_contacts'];
        $generate_link_to_master = $aParams['generate_link_to_master'];
        $DestWorkflow = GetWorkflowID($GenerateModule);

        $ModuleMappings = $ModuleDefs[$Module]['GENERATE_MAPPINGS'][$GenerateModule];

        if (!empty($ModuleDefs[$Module]['ADDITIONAL_GENERATE_LIB']))
        {
            require_once $ModuleDefs[$Module]['ADDITIONAL_GENERATE_LIB'];
        }

        $ModID = $ModuleDefs[$Module]['MOD_ID'];

        $sql = "SELECT recordid, ".implode(', ',$ModuleDefs[$Module]['FIELD_ARRAY'])." FROM ".$ModuleDefs[$Module]['TABLE'];

        if (!empty($recordid))
        {
            $sql .= " WHERE recordid = " . $recordid;
        }
        elseif (!empty($where))
        {
            $sql .= " WHERE " . $where;
        }

        $data = \DatixDBQuery::PDO_fetch($sql);

        if (!empty($data))
        {
            $fieldDefs = $this->registry->getFieldDefs();
            $truncatedFields = array();

            if (is_array($ModuleMappings))
            {
                foreach ($ModuleMappings as $from_field => $to_field)
                {
                    // attempt to handle data truncations by checking field defs
                    $toFieldDef = $fieldDefs[$ModuleDefs[$GenerateModule]['TABLE'].'.'.$to_field];

                    if ($toFieldDef instanceof StringField && \UnicodeString::strlen($data[$from_field]) > (int) $toFieldDef->fdr_data_length)
                    {
                        $data[$from_field] = \UnicodeString::substr($data[$from_field], 0, (int) $toFieldDef->fdr_data_length, 'UTF-8');
                        $truncatedFields[] = \Labels_FormLabel::GetFormFieldLabel($to_field);
                    }

                    $MappedData[$to_field] = $data[$from_field];
                }
            }

            // Check for approval status
            if (!empty($data['rep_approved']) || $ModuleDefs[$GenerateModule]['USES_APPROVAL_STATUSES'])
            {
                // Set the approval status to the first status in the workflow for the destination module.
                $sql =  "
                    SELECT
                        TOP 1 code
                    FROM
                        code_approval_status
                    WHERE
                        module LIKE '$GenerateModule'
                        AND
                            workflow = $DestWorkflow
                        AND
                            cod_priv_level != 'X'
                        AND
                            code NOT IN ('NEW', 'STCL')
                        AND
                            (code IN (SELECT apac_from FROM approval_action WHERE apac_workflow = $DestWorkflow) OR
                            code IN (SELECT apac_to FROM approval_action WHERE apac_workflow = $DestWorkflow))
                    ORDER BY
                        cod_listorder
                ";

                $row = \DatixDBQuery::PDO_fetch($sql);
                $MappedData['rep_approved'] = $row['code'];
            }
            if ((isset($aParams['approval_status']) && $aParams['approval_status'] != _tk('generate_default_approval')) && isset($MappedData['rep_approved']))
            {
                $MappedData['rep_approved'] = $aParams['approval_status'];
            }

            for ($i = 0;$i < $copies; $i++)
            {
                if (\CopyRecords::InsertRecords($GenerateModule, $NewRecordID, $MappedData))
                {
                    if ($copy_linked_contacts == 'Y')
                    {
                        \CopyRecords::CopyLinkedContacts($Module, $data['recordid'], $NewRecordID, $GenerateModule);
                    }

                    if ($generate_link_to_master == 'Y')
                    {
                        \CopyRecords::LinkToMasterCrossModule($Module, $data['recordid'], $GenerateModule, $NewRecordID, 'Linked via generate option');
                    }
                }
            }

            if (!empty($truncatedFields))
            {
                AddSessionMessage('INFO', sprintf(_tk('generate_truncate_message'), implode(", ", $truncatedFields)));
            }
        }

        return $NewRecordID;
    }

    public function updateRecordLastUpdated()
    {
        $moduleDefs = $this->registry->getModuleDefs();
        $module     = $this->request->getParameter('module');
        $recordId   = $this->request->getParameter('recordId');
        $updatedBy  = $_SESSION['initials'];
        $auditDate  = (new \DateTime())->format('Y-m-d H:i:s');

        if ($module && $recordId && in_array($module, ['INC', 'COM', 'CLA', 'PAL', 'RAM']))
        {
            $table = $moduleDefs[$module]['TABLE'];

            $sql = 'UPDATE '.$table.' SET updatedby = :updatedby, updateddate = :updateddate WHERE recordid = :recordid';
            \DatixDBQuery::PDO_query($sql, [
                'updatedby'   => $updatedBy,
                'updateddate' => (new \DateTime())->format('Y-m-d H:i:s'),
                'recordid'    => $recordId
            ]);

            // Do full audit for this field
            $sql = 'SELECT '.strtolower($module).'_last_updated FROM '.$table.' WHERE recordid = :recordid';
            $audDetail = \DatixDBQuery::PDO_fetch($sql, ['recordid' => $recordId], \PDO::FETCH_COLUMN);

            $sql = '
                INSERT INTO full_audit (
                    aud_module, aud_record, aud_login, aud_date, aud_action, aud_detail
                )
                VALUES (
                    :aud_module, :aud_record, :aud_login, :aud_date, :aud_action, :aud_detail
                )
            ';
            \DatixDBQuery::PDO_query($sql, [
                'aud_module' => $module,
                'aud_record' => $recordId,
                'aud_login'  => $updatedBy,
                'aud_date'   => $auditDate,
                'aud_action' => 'WEB:'.strtolower($module).'_last_updated',
                'aud_detail' => $audDetail
            ]);
        }
    }
}