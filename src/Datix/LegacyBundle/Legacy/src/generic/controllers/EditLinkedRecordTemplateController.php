<?php
namespace src\generic\controllers;

use src\framework\controller\TemplateController;

class EditLinkedRecordTemplateController extends TemplateController
{
    /**
     * Shows linked record form
     */
    function editlinkedrecord()
    {
        require_once 'Source/libs/ModuleLinks.php';

        $ModuleDefs = $this->registry->getModuleDefs();

        $current_id = \Sanitize::SanitizeInt($this->request->getParameter('currentID'));
        $current_mod = \Sanitize::SanitizeString($this->request->getParameter('currentMod'));
        $edit_mod = \Sanitize::SanitizeString($this->request->getParameter('editMod'));
        $edit_id = \Sanitize::SanitizeInt($this->request->getParameter('editID'));

        $editMode = ($edit_mod && $edit_id);

        $modArray = getModLinksArray($current_mod);

        if ($editMode)
        {
            $this->title = 'Editing ' . $ModuleDefs[$current_mod]['REC_NAME'] . ' link';
        }
        else
        {
            $this->title = 'Adding link to ' . $ModuleDefs[$current_mod]['REC_NAME'] . ' with ID '.$current_id;
        }

        $this->module = $current_mod;

        $DummyFormDesign = new \Forms_FormDesign();
        $DummyFormDesign->MandatoryFields['link_id'] = 'link';
        $DummyFormDesign->MandatoryFields['link_module'] = 'link';

        $MakeJavaScriptValidation = MakeJavaScriptValidation(
            'ADM',
            $DummyFormDesign,
            array('link_id' => 'ID', 'link_module' => 'Module')
        );

        $FormArray = array(
            'link' => array(
                'Title' => 'Link Details',
                'Rows' => array(
                    array(
                        'Title' => 'Module',
                        'Name' => 'link_module',
                        'Type' => 'ff_select',
                        'ReadOnly' => $editMode,
                        'CustomCodes' => getModLinksArray($current_mod)
                    ),
                    array(
                        'Title' => 'ID',
                        'Name' => 'link_id',
                        'Type' => 'number',
                        'ReadOnly' => $editMode
                    ),
                    array(
                        'Title' => 'Link Notes',
                        'Name' => 'link_notes',
                        'Type' => 'textarea',
                        'Columns' => 50,
                        'Rows' => 4
                    )
                )
            )
        );

        if ($editMode)
        {
            if ($current_mod == $edit_mod)
            {
                $sql = "
                    SELECT
                        link_notes
                    FROM
                        links
                    WHERE
                        (LNK_MOD1='$current_mod' AND LNK_MOD2='$edit_mod' AND LNK_ID1='$current_id' AND LNK_ID2='$edit_id')
                        OR
                        (LNK_MOD2='$current_mod' AND LNK_MOD1='$edit_mod' AND LNK_ID2='$current_id' AND LNK_ID1='$edit_id')
                ";
            }
            else
            {
                $sql = "
                    SELECT
                        link_notes
                    FROM
                        link_modules
                    WHERE
                        ".$ModuleDefs[$current_mod]['FK'] . " = '$current_id'
                        AND
                        ".$ModuleDefs[$edit_mod]["FK"] . " = '$edit_id'
                ";
            }

            $data = \DatixDBQuery::PDO_fetch($sql, array());
        }

        $data['link_module'] = $edit_mod;
        $data['link_id'] = $edit_id;

        $Table = new \FormTable($FormType);
        $Table->MakeForm($FormArray, $data, '', '');

        $this->response->build('src/generic/views/EditLinkedRecord.php', array(
            'DummyFormDesign' => $DummyFormDesign,
            'current_id' => $current_id,
            'current_mod' => $current_mod,
            'action' => $ModuleDefs[$current_mod]['ACTION'],
            'edit_mod' => $edit_mod,
            'edit_id' => $edit_id,
            'Table' => $Table,
            'editMode' => $editMode,
            'MakeJavaScriptValidation' => $MakeJavaScriptValidation
        ));
    }
}