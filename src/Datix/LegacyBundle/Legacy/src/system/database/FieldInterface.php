<?php
namespace src\system\database;

interface FieldInterface
{
    // field type constants
    const CODE         = 1;
    const MULTICODE    = 2;
    const DATE         = 3;
    const TEXT         = 4;
    const MONEY        = 5;
    const NUMBER       = 6;
    const STRING       = 7;
    const YESNO        = 8;
    const TIME         = 9;
    const COMPUTEDDATE = 10;

    /**
     * Getter for the field name.
     * 
     * @return string
     */
    public function getName();
    
    /**
     * Getter for the table name.
     * 
     * @return string
     */
    public function getTable();
    
    /**
     * Getter for the field type.
     * 
     * @var int
     */
    public function getType();
    
    /**
     * Getter for the data format.
     * 
     * @return string
     */
    public function getFormat();
    
    /**
     * Whether or not the field contains numeric (e.g. integer/decimal) data.
     * 
     * @return boolean
     */
    public function isNumeric();
    
    /**
     * Whether or not the field value is calculated based on other field values.
     * 
     * @return boolean
     */
    public function isCalculated();

}