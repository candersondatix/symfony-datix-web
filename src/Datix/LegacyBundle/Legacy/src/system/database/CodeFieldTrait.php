<?php

namespace src\system\database;

use src\framework\query\Query;
use src\framework\query\QueryFactory;
use src\savedqueries\model\SavedQueryModelFactory;
use src\savedqueries\model\SavedQuery;

/**
 * Used to control the setting of form context-specific queries for returning codes. 
 */
trait CodeFieldTrait
{
    /**
     * Whether this field should use a form query (if defined) when returning codes.
     * 
     * @var boolean
     */
    protected $isInFormContext = false;
    
    /**
     * Form data used when building the form context-specific query.
     * 
     * @var array
     */
    protected $formData = [];
    
    /**
     * A user-defined query for filtering codes.
     * 
     * @var SavedQuery
     */
    protected $userQuery;
    
    /**
     * The codes for this code field.
     * 
     * @var CodeCollection
     */
    protected $codes;
    
    /**
     * The database columns for code table this field uses.
     * 
     * @var array
     */
    protected $codeFields;
    
    /**
     * The parent (up to two) fields used for combo linking (cascading options).
     *
     * @var array
     */
    protected $parents;
    
    /**
     * The child fields that this field is a parent of.
     * 
     * @var array
     */
    protected $children;
    
    /**
     * {@inherit}
     */
    public function useInFormContext($val = true)
    {
        $this->isInFormContext = $val;
    }
    
    /**
     * {@inherit}
     */
    public function setFormData(array $data)
    {
        $this->formData = $data;
    }
    
    /**
     * {@inherit}
     */
    public function getCodes(Query $query = null, SavedQueryModelFactory $factory = null)
    {
        if (!isset($this->codes))
        {
            $query = $query ?: new Query();
            
            if ($this->isInFormContext)
            {
                $this->applyFormQuery($query, $factory);
            }
            
            $this->doGetCodes($query);
        }
        return $this->codes;
    }
    
    /**
     * {@inherit}
     */
    public function getParents()
    {
        if (!isset($this->parents))
        {
            $this->parents = $this->registry->getComboLinks()->getParents($this);
        }
        return $this->parents;
    }
    
    /**
     * {@inherit}
     */
    public function getChildren()
    {
        if (!isset($this->children))
        {
            $this->children = $this->registry->getComboLinks()->getChildren($this);
        }
        return $this->children;
    }
    
    /**
     * Applies additional query parameters which are used when returning a set of codes for the field in a form context.
     * 
     * @param Query                  $query   The original query object.
     * @param SavedQueryModelFactory $factory Used to retrieve user-defined queries for this code field.
     */
    protected function applyFormQuery(Query $query, SavedQueryModelFactory $factory = null)
    {
        $factory = $factory ?: new SavedQueryModelFactory();
        $qf      = $factory->getQueryFactory();
        
        $this->applySearchTermQuery($query, $qf);
        
        $this->applyComboLinksQuery($query, $qf);
        
        $this->applyCodeStatusQuery($query, $qf);
        
        $this->applyCurrentValueQuery($query, $qf);
        
        $this->applyUserDefinedQuery($query, $factory);
        
        // TODO handle free text fields (presumably within FreeTextField::applyFormQuery())
    }
    
    /**
     * Appends the "type-ahead" search term used to filter codes.
     * 
     * @param Query        $query   The query object we're appending conditions to.
     * @param QueryFactory $factory Used to access the Query objects.
     */
    protected function applySearchTermQuery(Query $query, QueryFactory $factory)
    {
        if ('' != ($term = $this->formData['term']))
        {
            $termConditions = ['OR', $factory->getFieldCollection()->field('description')->like('%'.$term.'%')];
            
            if ($this->registry->getParm('WEB_SHOW_CODE', 'N', false, true) && $this->formData['showCodes'] == 'true')
            {
                // we use the search term against the codes too if we're displaying them alongside the descriptions
                $termConditions[] = $factory->getFieldCollection()->field('code')->like('%'.$term.'%');
            }
            
            $where = $factory->getWhere();
            $where->addArray($termConditions);
            
            $query->where($where);
        }
    }
    
    /**
     * Appends the "combo-link" conditions used to filter codes based on the value(s) of the current parent field(s).
     * 
     * @param Query        $query   The query object we're appending conditions to.
     * @param QueryFactory $factory Used to access the Query objects.
     */
    protected function applyComboLinksQuery(Query $query, QueryFactory $factory)
    {
        if ($this->registry->getParm('COMBO_LINK_IN_SEARCH', 'Y', false, true) || 'search' != strtolower($this->formData['fieldmode']))
        {
            $parentConditions = [];
            
            foreach ($this->getParents() as $parent)
            {
                $parentNum = 0;
                foreach ([1,2] as $key)
                {
                    if ($this->formData['parent'.$key] == $parent->getName())
                    {
                        $parentNum = $key-1;
                        break;
                    }
                }

                //if a parent value has been defined, we need to match all records with that parent or where the
                //parent column is empty.
                if ('' != $this->formData['parent_value'][$parentNum])
                {
                    $parentConditions = [
                        'OR',
                        $factory->getFieldCollection()
                              ->field($this->getCodeParentColumn($parentNum+1))
                              ->in(explode('|', $this->formData['parent_value'][$parentNum])),
                        $factory->getFieldCollection()
                            ->field($this->getCodeParentColumn($parentNum+1))->isNull(),
                        $factory->getFieldCollection()
                            ->field($this->getCodeParentColumn($parentNum+1))->isEmpty()
                        ];
                }
            }
            
            if (!empty($parentConditions))
            {
                $where = $factory->getWhere();
                $where->addArray($parentConditions);
                
                $query->where($where);
            }
        }
    }
    
    /**
     * Appends the conditions used to filter out inactive codes.
     * 
     * @param Query        $query   The query object we're appending conditions to.
     * @param QueryFactory $factory Used to access the Query objects.
     */
    protected function applyCodeStatusQuery(Query $query, QueryFactory $factory)
    {
        if (!$this->usesCustomCodes() && 1 != $this->formData['childcheck'])
        {
            $statusConditions = $factory->getFieldCollection()->field('cod_priv_level')->notEq('X');
            
            if ('search' != strtolower($this->formData['fieldmode']))
            {
                $statusConditions->notEq('N');
            }
            
            $where = $factory->getWhere();
            $where->add($statusConditions);
            
            $query->where($where);
        }
    }
    
    /**
     * Appends the conditions used to filter out currently-selected codes.
     * 
     * @param Query        $query   The query object we're appending conditions to.
     * @param QueryFactory $factory Used to access the Query objects.
     */
    protected function applyCurrentValueQuery(Query $query, QueryFactory $factory)
    {
        if (!in_array($this->formData['value'], ['','null']))
        {
            if (is_array($this->formData['value']))
            {
                $currentValues = $this->formData['value'];
            }
            elseif (preg_match('/[<>=:!]/u', $this->formData['value']) == 0)
            {
                $currentValues = explode(('search' == strtolower($this->formData['fieldmode']) ? '|' : ' '), $this->formData['value']);
            }
            
            if (!empty($currentValues))
            {
                $where = $factory->getWhere();
                $where->add($factory->getFieldCollection()->field('code')->notIn($currentValues));
                
                $query->where($where);
            }
        }
    }
    
    /**
     * Appends a user-defined query used for filtering codes.
     * 
     * @param Query                  $query   The query object we're appending conditions to.
     * @param SavedQueryModelFactory $factory Used to access the SavedQuery model.
     */
    protected function applyUserDefinedQuery(Query $query, SavedQueryModelFactory $factory)
    {
        if (!isset($this->userQuery))
        {
            $this->userQuery = $factory->getMapper()->getCodeFieldQuery($this) ?: new SavedQuery();
        }
        
        if (null !== ($where = $this->userQuery->createQuery()->getWhere()))
        {
            $query->where($where);
        }
    }
    
    /**
     * Returns the name of the code table database column which holds the parent codes.
     * 
     * @param int $num The parent we're interested in (1 or 2).
     * 
     * @return string
     */
    protected function getCodeParentColumn($num)
    {
        $table = $this->getCodeTable();
        return 2 == $num ? $table.'.cod_parent2' : $table.'cod_parent.';
    }
    
    /**
     * Carries out the CodeCollection setup for this field.
     * 
     * @param Query $query The query used to fetch the codes for this field.
     */
    abstract protected function doGetCodes(Query $query);
}