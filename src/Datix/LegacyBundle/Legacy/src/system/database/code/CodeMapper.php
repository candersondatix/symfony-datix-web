<?php
namespace src\system\database\code;

use src\framework\model\Mapper;
use src\framework\model\ModelFactory;
use src\framework\query\Query;
use src\system\database\CodeFieldInterface;
use src\system\database\extrafield\ExtraField;

class CodeMapper extends Mapper
{
    /**
     * Table is set dynamically, since codes exist in multiple tables.
     * 
     * @var string
     */
    protected $table;
    
    /**
     * The field we're retrieving codes for.
     * 
     * @var CodeFieldInterface
     */
    protected $field;
    
    /**
     * The db column mappings for the codes associated with the field we're handling.
     * 
     * @var array
     */
    protected $fieldMappings;
    
    public function __construct(\DatixDBQuery $db, ModelFactory $factory)
    {
        if (!($factory instanceof CodeModelFactory))
        {
            throw new MapperException('$factory must be an instance of src\\system\\database\\code\\CodeModelFactory, '.get_class($factory).' given');
        }
        
        $this->field = $factory->getField();
        $this->table = $this->field->getCodeTable();
        
        $this->fieldMappings = [
            'code'        => $this->field->getCodeColumn(),
            'description' => $this->field->getDescriptionColumn()
        ];
        
        if ($this->field instanceof ExtraField)
        {
            $this->fieldMappings['cod_priv_level'] = 'active';
        }
        
        parent::__construct($db, $factory);
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\code\\Code';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return $this->table;
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        foreach ($this->fieldMappings as $property => $dbField)
        {
            if ($property != $dbField)
            {
                $query->convertFieldName($property, $dbField);
            }
        }
        
        $fields = array_map(
            [$this, 'mapFields'], // not using a closure here since ionCube doesn't like it...
            $this->field->getCodeFields()
        );
        
        $query->select($fields)
              ->from($this->getTable());
        
        if (count($query->getOrderBy()) == 0)
        {
        	$query->orderBy([$this->field->getCodeOrderColumn()]);
        	
            if ($this->field->getCodeOrderColumn() != $this->field->getDescriptionColumn() && !is_array($this->field->getDescriptionColumn()))
            {
                $query->orderBy([$this->field->getDescriptionColumn()]);
            }
        }

        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getFieldMappings()
    {
        return $this->fieldMappings;
    }
    
    public function find($id)
    {
        throw new MapperException(get_class($this).'::find() not yet implemented.');
    }
    
    public function insert(Entity $object)
    {
        throw new MapperException(get_class($this).'::insert() not yet implemented.');
    }
    
    public function update(Entity $object)
    {
        throw new MapperException(get_class($this).'::update() not yet implemented.');
    }
    
    public function delete(Entity &$object)
    {
        throw new MapperException(get_class($this).'::delete() not yet implemented.');
    }
    
    public function save(Entity $object)
    {
        throw new MapperException(get_class($this).'::save() not yet implemented.');
    }
    
    /**
     * Handles the mapping of entity propery names.
     * 
     * @param string $value
     * 
     * @return string|array
     */
    private function mapFields($value)
    {
        $alias = array_search($value, $this->getFieldMappings());
        if (false !== $alias)
        {
            if (is_array($value))
            {
                $value[0] .= ' AS '.$alias;
            }
            else
            {
                $value = ['? AS '.$alias, $value];
            }
        }
        return $value;
    }
}