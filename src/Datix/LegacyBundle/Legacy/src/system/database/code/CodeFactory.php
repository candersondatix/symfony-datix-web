<?php
namespace src\system\database\code;

use src\framework\model\EntityFactory;

class CodeFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\code\\Code';
    }
}