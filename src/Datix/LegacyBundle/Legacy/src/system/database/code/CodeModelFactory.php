<?php
namespace src\system\database\code;

use src\framework\model\ModelFactory;
use src\system\database\CodeFieldInterface;

class CodeModelFactory extends ModelFactory
{
    /**
     * The field whose codes we're working with.
     * 
     * @var CodeFieldInterface
     */
    protected $field;
    
    public function __construct(CodeFieldInterface $field)
    {
        $this->field = $field;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new CodeFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new CodeMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new CodeCollection($this->getMapper(), $this->getEntityFactory());
    }
    
    /**
     * Getter for field.
     * 
     * @return CodeFieldInterface
     */
    public function getField()
    {
        return $this->field;
    }
}