<?php
namespace src\system\database\extrafield;

use src\framework\model\EntityCollection;

class ExtraFieldCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\extrafield\\ExtraField';
    }
}