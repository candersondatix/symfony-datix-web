<?php
namespace src\system\database\extrafield;

use src\system\database\FieldInterface;

class NumberExtraField extends ExtraField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::NUMBER;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isNumeric()
    {
        return true;
    }
}