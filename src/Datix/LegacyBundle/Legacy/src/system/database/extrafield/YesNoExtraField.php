<?php
namespace src\system\database\extrafield;

use src\system\database\FieldInterface;
use src\system\database\field\YesNoField;
use src\framework\query\Query;
use src\savedqueries\model\SavedQueryModelFactory;

class YesNoExtraField extends CodeExtraField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::YESNO;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCodes(Query $query = null, SavedQueryModelFactory $factory = null)
    {
        return (new YesNoField)->getCodes($query, $factory);
    }
}