<?php
namespace src\system\database\extrafield;

use src\savedqueries\model\SavedQuery;
use src\savedqueries\model\SavedQueryModelFactory;
use src\system\database\CodeFieldInterface;
use src\system\database\FieldInterface;
use src\system\database\code\CodeModelFactory;
use src\framework\model\exceptions\ModelException;
use src\framework\query\Query;
use src\system\database\CodeFieldTrait;

class CodeExtraField extends ExtraField implements CodeFieldInterface
{
    use CodeFieldTrait
    {
        getCodes as traitGetCodes;
    }
    
    /**
     * The name of the standard field whose codes this field uses.
     *
     * @var string
     */
    protected $fld_code_like;

    /**
     * The name of the standard field/subform table whose codes this field uses.
     *
     * @var string
     */
    protected $fld_code_like_table;
    
    /**
     * @var CodeCollection
     */
    protected $codes;
    
    /**
     * This should be removed as part of DW-10208 as it will be no longer required.
     * 
     * @var int
     */
    protected $queryid;

    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::CODE;
    }
    
    /**
     * {@inherit}
     */
    public function getCodeTable()
    {
        return 'udf_codes';
    }
    
    /**
     * {@inherit}
     */
    public function getCodeFields()
    {
        if (!isset($this->codeFields))
        {
            $this->codeFields = [$this->getCodeColumn(), $this->getDescriptionColumn(), 'active'];
        }
        return $this->codeFields;
    }
    
    /**
     * {@inherit}
     */
    public function getCodeOrderColumn()
    {
        return 'listorder';
    }

    /**
     * {@inherit}
     */
    public function getCodeColumn()
    {
        return 'udc_code';
    }

    /**
     * {@inherit}
     */
    public function getDescriptionColumn()
    {
        return 'udc_description';
    }
    
    /**
     * {@inherit}
     * 
     * Can't currently define combo links for extra fields, so we return an empty array.
     */
    public function getParents()
    {
        return [];
    }
    
    /**
     * {@inherit}
     * 
     * Can't currently define combo links for extra fields, so we return an empty array.
     */
    public function getChildren()
    {
        return [];
    }
    
    /**
     * {@inherit}
     */
    public function usesCustomCodes()
    {
        return false;
    }
    
    /**
     * {@inherit}
     */
    public function getCodes(Query $query = null, SavedQueryModelFactory $factory = null)
    {
        if (!isset($this->codes))
        {
            if ($this->fld_code_like != '')
            {
                // delegate to the standard field definition to retrieve the codes
                $fieldDefs = $this->registry->getFieldDefs();
                
                if (null === ($field = $fieldDefs[$this->fld_code_like_table.'.'.$this->fld_code_like]))
                {
                    // cannot determine the standard field simply via the field/table names.
                    // this may be because the table name is missing, or the table is actually a "subform".
                    if (false !== ($pos = \UnicodeString::strpos($this->fld_code_like, '.')))
                    {
                        // the field name is fully-qualified, so check if this is sufficient to retireve the field def
                        $field = $fieldDefs[$this->fld_code_like];
                    }
                    
                    if ($field === null)
                    {
                        // otherwise, we have to scan all field defs for the field name.
                        // this is likely to be slow and possibly error-prone (since field object will be the first matched)
                        $fieldName = $pos === false ? $this->fld_code_like : substr($this->fld_code_like, $pos);
                        foreach ($fieldDefs as $fieldDef)
                        {
                            if ($fieldName == $fieldDef->getName())
                            {
                                $field = $fieldDef;
                                break;
                            }
                        }
                    }
                }
                
                if ($field instanceof CodeFieldInterface)
                {
                    $field->useInFormContext($this->isInFormContext);
                    $field->setFormData($this->formData);
                    
                    $this->codes = $field->getCodes($query);
                }
                else
                {
                    $this->registry->getLogger()->logEmergency('Unable to retrieve standard field codes for this UDF (recordid: '.
                            $this->recordid.'; fld_code_like: '.$this->fld_code_like.'; fld_code_like_table: '.$this->fld_code_like_table.')');
                    throw new ModelException('Error retrieving codes');
                }
            }
            else
            {
                $this->codes = $this->traitGetCodes($query, $factory);
            }
        }
        return $this->codes;
    }
    
    /**
     * {@inherit}
     */
    protected function doGetCodes(Query $query)
    {
        $query->where(array('field_id' => $this->recordid));
            
        $this->codes = (new CodeModelFactory($this))->getCollection();
        $this->codes->setQuery($query);
    }
}