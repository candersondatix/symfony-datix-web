<?php

namespace src\system\database\extrafield;

use src\framework\model\RecordEntity;
use src\system\database\FieldInterface;
use src\framework\registry\Registry;

abstract class ExtraField extends RecordEntity implements FieldInterface
{
    /**
     * The name of the Extra Field.
     *
     * @var string
     */
    protected $fld_name;

    /**
     * The type of the Extra Field.
     *
     * @var string
     */
    protected $fld_type;

    /**
     * The module id.
     *
     * @var int
     */
    protected $mod_id;

    /**
     * The format of the Extra Field.
     *
     * @var string
     */
    protected $fld_format;

    /**
     * The identity of the field as defined by a 'central' organisation (for distributed configurations).
     *
     * @var int
     */
    protected $central_id;

    /**
     * The central organisation that defined the field.
     *
     * @var string
     */
    protected $central_source;

    /**
     * The length of the Extra Field.
     *
     * @var int
     */
    protected $fld_length;

    /**
     * If the Extra Field is locked by a central administrator.
     *
     * @var string
     */
    protected $central_locked;
    
    /**
     * The group this extra field is associated with.
     * 
     * This value is only assigned when accessing the object from a FieldDefCollection
     * using the key format UDF_<type>_<group_id>_<field_id>.
     * 
     * @var int
     */
    protected $group_id;
    
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry = null)
    {
        parent::__construct();
        $this->addToCache = false;
        $this->registry = $registry ?: Registry::getInstance();
    }
    
    /**
     * Override parent so recordid is not lost when cloning (which happens when assigning a group_id in the FieldDefCollection).
     */
    public function __clone(){}
    
    /**
     * Custom return value for field central_id.
     *
     * @return string
     */
    public function getcentral_id()
    {
        if ($this->central_id != NULL)
        {
            return 'Y';
        }
        else
        {
            return '';
        }
    }
    
    /**
     * {@inherit}
     * 
     * Returns the field name in the format UDF_<field_id>, or UDF_<type>_<group_id>_<field_id> if a group_id is set.
     */
    public function getName()
    {
        return isset($this->group_id) ? $this->getFullName() : 'UDF_'.$this->recordid;
    }
    
    /**
     * {@inherit}
     */
    public function getTable()
    {
        $moduleDefs = $this->registry->getModuleDefs();
        foreach ($moduleDefs as $module)
        {
            if ($this->mod_id == $module['MOD_ID'])
            {
                return $module['TABLE'];
            }
        }
        return;
    }

    /**
     * {@inherit}
     */
    public function getFormat()
    {
        return $this->fld_format;
    }
    
    /**
     * {@inherit}
     */
    public function isNumeric()
    {
        return false;
    }
    
    /**
     * {@inherit}
     */
    public function isCalculated()
    {
        return false;
    }
    
    /**
     * Returns the name of the DB field that the extra field value is stored in.
     */
    public function getValueField()
    {
        switch ($this->fld_type)
        {
            case 'C':
            case 'T':
            case 'Y':
            case 'S':
                $valueField = 'udv_string';
                break;
                
            case 'D':
                $valueField = 'udv_date';
                break;
                
            case 'N':
                $valueField = 'udv_number';
                break;
                
            case 'M':
                $valueField = 'udv_money';
                break;
                
            case 'L':
                $valueField = 'udv_text';
                break;
        }
        return $valueField;
    }
    
    /**
     * Returns the field name in the format UDF_<type>_<group_id>_<field_id>.
     * 
     * @return string
     */
    public function getFullName()
    {
        return 'UDF_'.$this->fld_type.'_'.$this->group_id.'_'.$this->recordid;
    }
}