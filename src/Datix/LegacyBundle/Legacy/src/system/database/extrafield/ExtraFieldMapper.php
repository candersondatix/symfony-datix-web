<?php

namespace src\system\database\extrafield;

use src\framework\model\Mapper;
use src\framework\query\Query;

class ExtraFieldMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\extrafield\\ExtraField';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'udf_fields';
    }
    
    /**
     * {@inherit}
     * 
     * This data needs to be pulled manually because it forms part of the FieldDefs collection which the generic Mapper functionality relies on.
     */
    public function select(Query $query)
    {
        $sql = 'SELECT
                    recordid,central_id,central_source,fld_name,fld_type,
                    mod_id,fld_format,fld_code_like,fld_code_like_table,fld_length,central_locked,queryid
                FROM
                    udf_fields';
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute();
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function isStaffCode($ExtraFieldCodeLike)
    {
            $sql = "SELECT fdr_name FROM field_directory WHERE fdr_code_table = 'vw_staff_combos'";
            $staffFields = \DatixDBQuery::PDO_fetch_all($sql);
            foreach ($staffFields as $field)
            {
                if ($ExtraFieldCodeLike == $field['fdr_name'])
                {
                    return true;
                }
            }
        return false;
    }
}