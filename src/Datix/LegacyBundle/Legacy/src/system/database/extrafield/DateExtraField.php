<?php
namespace src\system\database\extrafield;

use src\system\database\FieldInterface;

class DateExtraField extends ExtraField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::DATE;
    }
}