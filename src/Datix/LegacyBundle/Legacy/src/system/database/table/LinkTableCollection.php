<?php
namespace src\system\database\table;

use src\framework\model\KeyedEntityCollection;
use src\framework\model\Mapper;
use src\framework\model\EntityFactory;

class LinkTableCollection extends KeyedEntityCollection
{
    /**
     * A reference to the originating table that this table links to.
     * 
     * @var TableDef
     */
    protected $origin;
    
    /**
     * Getter for origin.
     * 
     * @return TableDef
     */
    public function getOrigin()
    {
        return $this->origin;
    }
    
    /**
     * Setter for origin.
     * 
     * @param TableDef $origin
     */
    public function setOrigin(Table $origin)
    {
        $this->origin = $origin;
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\LinkTable';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function keyProperty()
    {
        return array('name');
    }
    
    /**
     * {@inheritdoc}
     * 
     * Makes the originating table definition available to the link table object.
     */
    protected function getElement($key)
    {
        $object = parent::getElement($key);
        if ($object !== null && $object->origin === null)
        {
            $object->origin = $this->origin;
        }
        return $object;
    }
}