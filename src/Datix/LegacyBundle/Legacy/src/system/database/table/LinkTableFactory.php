<?php
namespace src\system\database\table;

use src\framework\model\EntityFactory;

class LinkTableFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\LinkTable';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array(), EntityCache $cache = null)
    {
        $class  = $this->targetClass();
        $object = new $class();
        
        foreach ($class::getProperties() as $property)
        {
            if ($data[$property] !== null)
            {
                $object->$property = $data[$property];
            }
        }
        
        return $object;
    }
}