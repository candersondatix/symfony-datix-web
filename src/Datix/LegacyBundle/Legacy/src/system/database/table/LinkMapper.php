<?php
namespace src\system\database\table;

use src\framework\model\Mapper;
use src\framework\query\Query;
use src\framework\query\Where;
use src\framework\query\FieldCollection;

class LinkMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\Link';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'table_links';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        // select all of the joins that make up this link
        $condition = new FieldCollection();
        $condition->field('table_links.tlk_link_id')->eq('table_routes.trt_link', true);
        
        $where = new Where();
        $where->add($condition);
        
        $query->select(array('table_links.tlk_link_id AS recordid', 'table_links.tlk_table1', 'table_links.tlk_table2', 'table_links.tlk_where', 'table_routes.trt_default AS [default]'))
              ->from($this->getTable())
              ->join('table_routes', $where)
              ->orderBy(array('table_routes.trt_order'));
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        $joins = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
        
        // select the conditions for each of the joins
        foreach ($joins as $key => $join)
        {
            $joins[$key]['conditions'] = array();
            
            $query = new Query();
            $query->select(array('tky_table1', 'tky_field1', 'tky_table2', 'tky_field2'))
                  ->from('table_link_keys')
                  ->where(array('tky_link_id' => $join['recordid']));
            
            list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
            
            $this->db->setSQL($sql);
            $this->db->prepareAndExecute($parameters);
            $conditions = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
            
            foreach ($conditions as $condition)
            {
                $joins[$key]['conditions'][] = array($condition['tky_table1'].'.'.$condition['tky_field1'], $condition['tky_table2'].'.'.$condition['tky_field2']);
            }
        }
        
        return $joins;
    }
    
    /**
     * Fetches the links used for a given fieldset.
     * 
     * @param int $fieldset
     * 
     * @returns LinkCollection
     */
    public function findLinksByFieldset($fieldset)
    {
        $links = $this->factory->getCollection();
        
        $links->setQuery($this->factory
                              ->getQueryFactory()
                                  ->getQuery()
                                      ->where(['table_routes.fieldset' => $fieldset]));
        
        return $links;
    }
}