<?php
namespace src\system\database\table;

use src\framework\model\EntityFactory;

class LinkFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\Link';
    }
}