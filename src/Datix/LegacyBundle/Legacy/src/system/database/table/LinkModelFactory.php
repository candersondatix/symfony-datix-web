<?php
namespace src\system\database\table;

use src\framework\model\ModelFactory;
use src\framework\query\Query;

class LinkModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new LinkFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new LinkMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new LinkCollection($this->getMapper(), $this->getEntityFactory());
    }
}