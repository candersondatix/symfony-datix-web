<?php
namespace src\system\database\table;

use src\framework\model\RecordEntity;

class Link extends RecordEntity
{
    /**
     * The first table in the link.
     * 
     * @var string
     */
    protected $tlk_table1;
    
    /**
     * The second table in the link.
     * 
     * @var string
     */
    protected $tlk_table2;
    
    /**
     * Additional specific link conditions.
     * 
     * @var string
     */
    protected $tlk_where;
    
    /**
     * Whether or not this is the default link in cases where there are multiple links to the same table.
     * 
     * @var int
     */
    protected $default;
    
    /**
     * The conditions that form this link.
     * 
     * @var array
     */
    protected $conditions;
    
    public function __construct()
    {
        parent::__construct();
        $this->addToCache = false;
    }
    
    /**
     * Whether or not this is the default route through to the link table.
     * 
     * @return boolean
     */
    public function isDefault()
    {
        return (bool) $this->default;
    }
}