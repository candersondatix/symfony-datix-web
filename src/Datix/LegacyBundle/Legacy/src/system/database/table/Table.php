<?php
namespace src\system\database\table;

use src\framework\model\Entity;
use src\framework\query\Query;

class Table extends Entity
{
    /**
     * The table name.
     * 
     * @var string
     */
    protected $tdr_name;
    
    /**
     * Whether or not this is a link table.
     * 
     * @var string
     */
    protected $tdr_link;
    
    /**
     * The table's unique index (e.g. recordid).
     * 
     * @var string
     */
    protected $tdr_index;
    
    /**
     * english.php key to the description of the record type the table holds.
     * 
     * @var string
     */
    protected $tdr_desc;
    
    /**
     * Flag used to determine if this table uses an Identity (auto-increment) id column.
     * 
     * @var int
     */
    protected $is_identity;
    
    /**
     * Flag used to determine if the columns for this table can be extended using extra fields.
     *
     * @var int
     */
    protected $udf_link;

    /**
     * A collection of tables that this table is linked to.
     * 
     * @var LinkTableCollection
     */
    protected $link_tables;
    
    public function __construct()
    {
        parent::__construct();
        $factory           = new LinkTableModelFactory();
        $this->link_tables = $factory->getCollection();
        $this->addToCache  = false;
        
        $this->link_tables->setOrigin($this);
    }
    
    /**
     * Once set, use the table name to set the query condition for the link tables collection.
     * 
     * @param string $value
     */
    public function setTdr_name($value)
    {
        $this->tdr_name = $value;
        
        $query = new Query();
        $this->link_tables->setQuery($query->where(array('trt_table1' => $this->tdr_name)));
    }
    
    /**
     * {@identity}
     */
    protected static function setIdentity()
    {
        return array('tdr_name');
    }
}