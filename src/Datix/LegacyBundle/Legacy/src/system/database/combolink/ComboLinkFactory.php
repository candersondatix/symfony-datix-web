<?php

namespace src\system\database\combolink;

use src\framework\model\EntityFactory;

class ComboLinkFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\combolink\\ComboLink';
    }
}