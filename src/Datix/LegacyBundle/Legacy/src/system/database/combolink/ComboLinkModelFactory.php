<?php

namespace src\system\database\combolink;

use src\framework\model\ModelFactory;
use src\framework\query\Query;

class ComboLinkModelFactory extends ModelFactory
{
    /**
     * {@inherit}
     */
    public function getEntityFactory()
    {
        return new ComboLinkFactory();
    }
    
    /**
     * {@inherit}
     */
    public function getMapper()
    {
        return new ComboLinkMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inherit}
     */
    public function getCollection()
    {
        $collection = new ComboLinkCollection($this->getMapper(), $this->getEntityFactory());
        $collection->setQuery(new Query());
        
        return $collection;
    }
}