<?php

namespace src\system\database\combolink;

use src\framework\model\EntityCollection;
use src\system\database\CodeFieldInterface;
use src\framework\registry\Registry;

class ComboLinkCollection extends EntityCollection
{
    /**
     * Stores parent-child relationships in an accessible format to prevent having to scan
     * the entire data array each time we want to fetch a field's children (which would be non-performant).
     * 
     * @var array
     */
    protected $parentChild;
    
    /**
     * Stores child-parent relationships in an accessible format to prevent having to scan
     * the entire data array each time we want to fetch a field's parents (which would be non-performant).
     * 
     * @var array
     */
    protected $childParent;
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\combolink\\ComboLink';
    }
    
    /**
     * Retrieves the parents of a given field.
     * 
     * @param CodeFieldInterface $field
     * @param Registry           $registry
     * 
     * @return array $parentObjects
     */
    public function getParents(CodeFieldInterface $field, Registry $registry = null)
    {
        $registry  = $registry ?: Registry::getInstance();
        $fieldDefs = $registry->getFieldDefs();
        
        $this->fetchData();
        
        $parents = $this->childParent[$field->getTable()][$field->getName()] ?: [];
        $parentObjects = [];
        
        foreach ($parents as $parent)
        {
            if (null !== ($parentObj = $fieldDefs[$field->getTable().'.'.$parent]))
            {
                $parentObjects[] = $parentObj;
            }
        }
        
        return $parentObjects;
    }
    
    /**
     * Retrieves the children of a given field.
     * 
     * @param CodeFieldInterface $field
     * @param Registry           $registry
     * 
     * @return array $childObjects
     */
    public function getChildren(CodeFieldInterface $field, Registry $registry = null)
    {
        $registry  = $registry ?: Registry::getInstance();
        $fieldDefs = $registry->getFieldDefs();
        
        $this->fetchData();
        
        $children = $this->parentChild[$field->getTable()][$field->getName()] ?: [];
        $childObjects = [];
        
        foreach ($children as $child)
        {
            if (null !== ($childObj = $fieldDefs[$field->getTable().'.'.$child]))
            {
                $childObjects[] = $childObj;
            }
        }
        
        return $childObjects;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function fetchData()
    {
        parent::fetchData();
        
        if (!isset($this->parentChild) || !isset($this->childParent))
        {
            foreach ($this->data as $comboLink)
            {
                $this->parentChild[$comboLink['table']][$comboLink['parent']][] = $comboLink['child'];
                $this->childParent[$comboLink['table']][$comboLink['child']][] = $comboLink['parent'];
            }
        }
    }
}