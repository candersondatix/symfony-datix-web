<?php
namespace src\system\database\fielddef;

use src\framework\model\KeyedEntityCollection;
use src\system\database\field\Field;

/**
 * A combined collection of both standard and extra field definitions.
 */
class FieldDefCollection extends KeyedEntityCollection
{
    /**
     * Keeps track of the extra field group ID if present in the key.
     * 
     * @var int
     */
    protected $groupID;
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\FieldInterface';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function keyProperty()
    {
        // not used in this implementation because of custom initCollection() implementation
    }
    
    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return parent::offsetExists($this->transformKey($offset));
    }
    
    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        $object = parent::offsetGet($this->transformKey($offset));
        if (null !== $object && null !== $this->groupID)
        {
            // assign the extra field group ID.
            $object = clone $object; // so the group ID isn't set on the object in the collection, just the returned instance
            $object->group_id = $this->groupID;
            $this->groupID = null;
        }
        return $object;
    }
    
    /**
     * Carries out the necessary key transformation when accessing objects in the collection.
     * 
     * ExtraField objects are accessed using the key format UDF_<field_id>, UDF_<type>_<group_id>_<field_id>
     * or UDF_<type>__<field_id> (sometimes the group is missing...). The key may also contain a suffix for fields that
     * appear in repeating form sections, so this is also taken into account. Additionally, the table name may be 
     * prefixed in order to indicate the module the extra field is linked to.  Standard field objects are
     * always accessed via their fully-qualified (i.e. including the table prefix) database field name.
     * 
     * @param string $key The key used to access an object in the collection.
     * 
     * @return string $key
     */
    protected function transformKey($key)
    {
        $key = strtolower($key);
        
        if (1 === preg_match('/(^|\.)udf_(\d+|\w_(\d+|)_(\d+))($|_\d+)/u', $key, $matches))
        {
            if (false !== strpos($key, '.'))
            {
                // strip the table prefix (if any)
                $key = explode('.', $key)[1];
            }
            
            if ('' != $matches[3])
            {
                $this->groupID = $matches[3];
            }
            
            if ('' != $matches[4])
            {
                $key = 'udf_'.$matches[4];
            }
        }

        return $key;
    }
    
    /**
     * {@inherit}
     * 
     * Handles this specific case where different object types are contained within the same collection.
     */
    protected function initCollection()
    {
        if (!isset($this->data))
        {
            $this->data = $this->mapper->select($this->query);
            foreach ($this->data as $key => $record)
            {
                if (isset($record['fdr_name']))
                {
                    // standard field
                    $keyProp = $record['fdr_table'].'.'.$record['fdr_name'];
                }
                else
                {
                    // extra field
                    $keyProp = 'udf_'.$record['recordid'];
                }
                $this->keyReference[strtolower($keyProp)] = $key;
            }
        }
    }
}