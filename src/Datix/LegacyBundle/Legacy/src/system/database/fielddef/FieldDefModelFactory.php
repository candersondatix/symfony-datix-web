<?php
namespace src\system\database\fielddef;

use src\framework\model\ModelFactory;
use src\framework\query\Query;

class FieldDefModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new FieldDefFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new FieldDefMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        $collection = new FieldDefCollection($this->getMapper(), $this->getEntityFactory());
        $collection->setQuery(new Query());
        
        return $collection;
    }
}