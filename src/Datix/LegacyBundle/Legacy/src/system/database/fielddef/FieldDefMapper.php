<?php
namespace src\system\database\fielddef;

use src\framework\model\Mapper;
use src\framework\query\Query;
use src\system\database\field\FieldModelFactory;
use src\system\database\extrafield\ExtraFieldModelFactory;

/**
 * Delegates to the Field/ExtraField Mappers to fetch data for combined FieldDef collection.
 */
class FieldDefMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\FieldInterface';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return;
    }
    
    /**
     * {@inherit}
     */
    public function select(Query $query)
    {
        return array_merge((new FieldModelFactory)->getMapper()->select(clone $query), (new ExtraFieldModelFactory)->getMapper()->select(clone $query));
    }
}