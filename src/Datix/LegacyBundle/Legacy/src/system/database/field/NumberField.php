<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;

class NumberField extends Field
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::NUMBER;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isNumeric()
    {
        return true;
    }
}