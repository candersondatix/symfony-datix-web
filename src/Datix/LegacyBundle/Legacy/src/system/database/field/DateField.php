<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;

class DateField extends Field
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::DATE;
    }
}