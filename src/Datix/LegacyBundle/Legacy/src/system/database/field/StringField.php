<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;

class StringField extends Field
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::STRING;
    }
    
    /**
     * Getter for fdr_data_length.
     * 
     * @return int
     */
    public function getLength()
    {
        return (int) $this->fdr_data_length;
    }
}