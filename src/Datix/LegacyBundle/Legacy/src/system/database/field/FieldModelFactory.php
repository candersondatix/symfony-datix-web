<?php
namespace src\system\database\field;

use src\framework\model\ModelFactory;
use src\framework\query\Query;

class FieldModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new FieldFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new FieldMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        $collection = new FieldCollection($this->getMapper(), $this->getEntityFactory());
        $collection->setQuery(new Query());
        
        return $collection;
    }
}