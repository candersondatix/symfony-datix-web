<?php

namespace src\system\database\field;

use src\framework\query\Query;
use src\system\database\code\CodeModelFactory;

class InitialCurrentField extends CodeField
{
    /**
     * {@inheritdoc}
     */
    protected function doGetCodes(Query $query)
    {
        $initialCurrent = array(
            0 => array(
                'code' => 'I',
                'description' => _tk('initial_value'),
                'cod_priv_level' => null,
                'cod_listorder' => null,
                'cod_web_colour' => ''
            ),
            1 => array(
                'code' => 'C',
                'description' => _tk('current_value'),
                'cod_priv_level' => null,
                'cod_listorder' => null,
                'cod_web_colour' => ''
            )
        );
    
        $this->codes = (new CodeModelFactory($this))->getCollection();
        $this->codes->setData($initialCurrent);
    }
}