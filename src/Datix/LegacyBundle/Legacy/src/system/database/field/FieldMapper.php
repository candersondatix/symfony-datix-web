<?php
namespace src\system\database\field;

use src\framework\model\Mapper;
use src\framework\query\Query;

class FieldMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\field\\Field';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'field_directory';
    }
    
    /**
     * {@inherit}
     * 
     * The generic select relies on this data to extract entity properties, so we need to override it here.
     */
    public function select(Query $query)
    {
        $fields = ['fdr_name','fdr_label','fdr_table','fdr_code_table','fdr_code_descr','fdr_code_field','fdr_data_type','fdr_data_length',
                   'fdr_format','fdr_code_where','fdr_code_order','fdr_setup','fdr_setup_condition','fdr_custom_code','fdr_calculated_field'];
        
        $table = 'field_directory';
        
        if ($query->isEmpty())
        {
            // the SqlWriter class relies on this data too, so the first time we fetch it we 
            // have to do it by manually constructing the SQL to prevent an infinite loop
            $sql = 'SELECT '.implode(',', $fields).' FROM '.$table;
            $parameters = [];
        }
        else
        {
            // force the previous condition first in case we haven't accessed the FieldDefCollection yet in this request
            $fieldDefs = $this->registry->getFieldDefs();
            
            $query->select($fields)
                  ->from($table);
            
            list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        }
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }
}