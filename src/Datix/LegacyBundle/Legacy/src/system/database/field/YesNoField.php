<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;
use src\framework\query\Query;
use src\savedqueries\model\SavedQueryModelFactory;

class YesNoField extends CodeField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::YESNO;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCodes(Query $query = null, SavedQueryModelFactory $factory = null)
    {
        $this->fdr_code_table = '!YESNO';
        return parent::getCodes($query, $factory);
    }
}