<?php
namespace src\system\database\field;

use src\framework\model\KeyedEntityCollection;

class FieldCollection extends KeyedEntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\field\\Field';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function keyProperty()
    {
        return array('fdr_table', 'fdr_name');
    }
}