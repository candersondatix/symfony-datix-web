<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;

class TextField extends Field
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::TEXT;
    }
}