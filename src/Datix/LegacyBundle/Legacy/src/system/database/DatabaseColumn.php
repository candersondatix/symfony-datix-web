<?php

namespace src\system\database;

use src\system\database\exceptions\InvalidDatabaseColumnNameException;

/**
 * Value object representing a fully-qualified database table column name.
 */
class DatabaseColumn
{
    /**
     * The database table name.
     * 
     * @var string
     */
    protected $table;
    
    /**
     * The database column name.
     * 
     * @var string
     */
    protected $column;
    
    /**
     * Constructor.
     * 
     * @param string $name The fully qualified database column name in the format "table_name.column_name".
     * 
     * @throws InvalidDatabaseColumnNameException If the name format is incorrect.
     */
    public function __construct($name)
    {
        $parts = explode('.', $name);
        
        if (2 != count($parts))
        {
            throw new InvalidDatabaseColumnNameException('The database column name "'.$name.'" is not in the format "table_name.column_name"');
        }
        
        $this->validateObjectNameLength('table', $parts[0]);
        $this->validateObjectNameLength('column', $parts[1]);
        
        $this->table  = $parts[0];
        $this->column = $parts[1];
    }
    
    /**
     * Getter for table.
     * 
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }
    
    /**
     * Getter for column.
     * 
     * @return string
     */
    public function getColumn()
    {
        return $this->column;
    }
    
    /**
     * Equality test against another database column.
     * 
     * @param DatabaseColumn $column
     * 
     * @return boolean
     */
    public function equals(DatabaseColumn $column)
    {
        return $column->getTable() == $this->table && $column->getColumn() == $this->column;
    }
    
    /**
     * Validates that the database object (table/column) name does not exceed 128 characters, as per the SQL Server specification.
     * 
     * @param string $type The object type (table/column).
     * @param string $name The object name we're validating.
     * 
     * @throws InvalidDatabaseColumnNameException If the name length exceeds 128 characters.
     */
    protected function validateObjectNameLength($type, $name)
    {
        if (128 < strlen($name))
        {
            throw new InvalidDatabaseColumnNameException('The '.$type.' name "'.$name.'" exceeds 128 characters.');
        }
    }
}