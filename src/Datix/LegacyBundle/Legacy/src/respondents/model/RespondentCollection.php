<?php
namespace src\respondents\model;

use src\framework\model\EntityCollection;

class RespondentCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function targetClass()
    {
        return 'src\\respondents\\model\\Respondent';
    }
}