<?php
namespace src\respondents\model;

use src\framework\model\Mapper;
use src\framework\model\Entity;
use src\framework\query\Query;

class RespondentMapper extends Mapper
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function targetClass()
    {
        return 'src\\respondents\\model\\Respondent';
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getTable()
    {
        return 'link_respondents';
    }

    /**
     * Returns list of respondents attached to a claim based on user permissions.
     *
     * @param string $main_recordid The claim record ID.
     *
     * @return array $respondents
     */
    public function findRespondentsByMainID($main_recordid)
    {
        $respondents = [];

        // Access to CLA but not CON and ORG
        if (!CanSeeModule('CON') && !CanSeeModule('ORG'))
        {
            return $respondents;
        }
        else
        {
            $sqlSelect = "
                SELECT
                    l.recordid AS recordid,
                    l.link_type,
                    l.link_resp,
                    l.indemnity_reserve_assigned,
                    l.expenses_reserve_assigned,
                    l.remaining_indemnity_reserve_assigned,
                    l.remaining_expenses_reserve_assigned,
                    l.resp_total_paid,
            ";
            $sqlFrom = " FROM link_respondents AS l ";
            $sqlWhere = " WHERE l.main_recordid = :main_recordid ";

            // Access to CLA and CON but not ORG
            if (CanSeeModule('CON') && !CanSeeModule('ORG'))
            {
                $sqlSelect .= " contacts_main.fullname AS name ";
                $sqlLeftJoin = " LEFT JOIN contacts_main ON l.link_type = 'CON' AND l.con_id = contacts_main.recordid ";

                // Apply security where clause so that users see the appropriate linked data
                $securityWhereCON = MakeSecurityWhereClause('', 'CON');

                $sqlWhere .= " AND l.link_type = 'CON' ".($securityWhereCON != '' ? "AND ".$securityWhereCON : "");

                $respondents = $this->db->PDO_fetch_all($sqlSelect.$sqlFrom.$sqlLeftJoin.$sqlWhere, ['main_recordid' => $main_recordid]);
            }
            // Access to CLA and ORG but not CON
            elseif (!CanSeeModule('CON') && CanSeeModule('ORG'))
            {
                $sqlSelect .= " organisations_main.org_name AS name ";
                $sqlLeftJoin = " LEFT JOIN organisations_main ON l.link_type = 'ORG' AND l.org_id = organisations_main.recordid ";

                // Apply security where clause so that users see the appropriate linked data
                $securityWhereORG = MakeSecurityWhereClause('', 'ORG');

                $sqlWhere .= " AND l.link_type = 'ORG' ".($securityWhereORG != '' ? "AND ".$securityWhereORG : "");

                $respondents = $this->db->PDO_fetch_all($sqlSelect.$sqlFrom.$sqlLeftJoin.$sqlWhere, ['main_recordid' => $main_recordid]);
            }
            // Access to CLA and CON and ORG
            elseif (CanSeeModule('CON') && CanSeeModule('ORG'))
            {
                $sqlSelect .= " (CASE l.link_type WHEN 'CON' THEN contacts_main.fullname ELSE organisations_main.org_name END) AS name ";
                $sqlLeftJoin = "
                    LEFT JOIN contacts_main ON l.link_type = 'CON' AND l.con_id = contacts_main.recordid
                    LEFT JOIN organisations_main ON l.link_type = 'ORG' AND l.org_id = organisations_main.recordid
                ";

                // Apply security where clause so that users see the appropriate linked data
                $securityWhereCON = MakeSecurityWhereClause('', 'CON');
                $securityWhereORG = MakeSecurityWhereClause('', 'ORG');

                if ($securityWhereCON != '' && $securityWhereORG == '')
                {
                    $sqlWhere .= " AND ((l.link_type = 'CON' AND ".$securityWhereCON. ")  OR (l.link_type = 'ORG' AND (1=1)))";
                }
                elseif ($securityWhereORG != '' && $securityWhereCON == '')
                {
                    $sqlWhere .= " AND ((l.link_type = 'ORG' AND ".$securityWhereORG. ")  OR (l.link_type = 'CON' AND (1=1)))";
                }
                elseif ($securityWhereCON != '' && $securityWhereORG != '')
                {
                    $sqlWhere .= " AND ((l.link_type = 'CON' AND ".$securityWhereCON. ") OR (l.link_type = 'ORG' AND ".$securityWhereORG. "))";
                }

                $respondents = $this->db->PDO_fetch_all($sqlSelect.$sqlFrom.$sqlLeftJoin.$sqlWhere, ['main_recordid' => $main_recordid]);
            }

            return $respondents;
        }
    }

    public function update(Entity $object)
    {
        // check for changes in responsibility
        $old = $this->factory->getMapper()->find($object->recordid)->link_resp;

        if ($old_responsibility != $object->link_resp)
        {
            // save audit trail
            $sql = "
                INSERT INTO respondents_resp_audit
                (field_value, changed_by, datetime_from, link_respondent_id)
                VALUES (:new_value, :changed_by, :when, :link_id)
            ";

            $params = array (
                'new_value'  => $object->link_resp,
                'changed_by' => $_SESSION['initials'],
                'when'       => (new \DateTime())->format('Y-m-d H:i:s'),
                'link_id'    => $object->recordid
            );

            $this->db->PDO_query ($sql, $params);
        }

        return parent::update ($object);
    }

    public function saveLinkedOrganisations(array $data)
    {
        if (!is_array($data) || count($data) == 0)
        {
            throw new \Exception;
        }

        $sql = '
            INSERT INTO link_respondents
                (main_recordid, link_type, org_id, link_notes, link_resp, link_role)
            VALUES
                (:main_recordid, :link_type, :org_id, :link_notes, :link_resp, :link_role)
        ';

        $this->db->PDO_query($sql, array(
            'main_recordid' => $data['main_recordid'],
            'link_type' => 'ORG',
            'org_id' => $data['org_id'],
            'link_notes' => ($data['link_notes'] ?: null),
            'link_resp' => ($data['link_resp'] ?: null),
            'link_role' => ($data['link_role'] ?: null)
        ));
    }
    
    /**
     * {@inheritdoc}
     * 
     * Additionally handles deletion of linked policies.
     */
    public function delete(Entity &$object)
    {
        $this->db->setSQL('DELETE FROM policy_defendant_links WHERE defendant_link_id = ?');
        $this->db->prepareAndExecute([$object->recordid]);
        
        parent::delete($object);
    }
    
    /**
     * Fetches data for respondents linked to a given policy.
     * 
     * @param int $policy_id The policy recordid.
     * 
     * @return array
     */
    public function findRespondentsByPolicyId($policy_id)
    {
        $respondents = [];
        $individuals = [];
        $organisations = [];

        $individualsSql = '
            SELECT
                link_respondents.recordid, CONTACTS_MAIN.fullname AS name, link_respondents.link_type, link_respondents.link_resp
            FROM
                link_respondents, CONTACTS_MAIN, policy_defendant_links
            WHERE
                CONTACTS_MAIN.RECORDID = link_respondents.con_id AND
                link_respondents.recordid = policy_defendant_links.defendant_link_id AND
                policy_defendant_links.policy = ?
        ';

        $organisationsSql = '
            SELECT
                link_respondents.recordid, organisations_main.ORG_NAME AS name, link_respondents.link_type, link_respondents.link_resp
            FROM
                link_respondents, organisations_main, policy_defendant_links
            WHERE
                organisations_main.RECORDID = link_respondents.org_id AND
                link_respondents.recordid = policy_defendant_links.defendant_link_id AND
                policy_defendant_links.policy = ?
        ';

        if (!CanSeeModule('CON') && !CanSeeModule('ORG'))
        {
            return $respondents;
        }
        else
        {
            if (CanSeeModule('CON') && !CanSeeModule('ORG'))
            {
                $securityWhere = MakeSecurityWhereClause('', 'CON');

                if ($securityWhere != '')
                {
                    $individualsSql .= ' AND '.$securityWhere;
                }

                $this->db->setSQL($individualsSql);
                $this->db->prepareAndExecute([$policy_id]);

                $individuals = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
            }
            elseif (!CanSeeModule('CON') && CanSeeModule('ORG'))
            {
                $securityWhere = MakeSecurityWhereClause('', 'ORG');

                if ($securityWhere != '')
                {
                    $organisationsSql .= ' AND '.$securityWhere;
                }

                $this->db->setSQL($organisationsSql);
                $this->db->prepareAndExecute([$policy_id]);

                $organisations = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
            }
            elseif (CanSeeModule('CON') && CanSeeModule('ORG'))
            {
                $securityWhere = MakeSecurityWhereClause('', 'CON');

                if ($securityWhere != '')
                {
                    $individualsSql .= ' AND '.$securityWhere;
                }

                $this->db->setSQL($individualsSql);
                $this->db->prepareAndExecute([$policy_id]);

                $individuals = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);

                $securityWhere = MakeSecurityWhereClause('', 'ORG');

                if ($securityWhere != '')
                {
                    $organisationsSql .= ' AND '.$securityWhere;
                }

                $this->db->setSQL($organisationsSql);
                $this->db->prepareAndExecute([$policy_id]);

                $organisations = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
            }
        }

        $respondents = array_merge($individuals, $organisations);

        return $respondents;
    }

    /**
     * Gets payments assigned to a respondent.
     *
     * @param string $claimId      The id of the claims record.
     * @param string $respondentId The id of the linked respondent.
     *
     * @return string $assignedPayments The number of assigned payments to a respondent or 0 if there's any.
     */
    public function getAssignedPayments($claimId, $respondentId)
    {
        $sql = 'SELECT COUNT(*) FROM PAYMENTS WHERE cla_id = :cla_id AND resp_id = :resp_id';

        $assignedPayments = $this->db->PDO_fetch_all($sql, ['cla_id' => $claimId, 'resp_id' => $respondentId], \PDO::FETCH_COLUMN);

        if (null === $assignedPayments)
        {
            $assignedPayments = '0';
        }
        else
        {
            $assignedPayments = $assignedPayments[0];
        }

        return $assignedPayments;
    }
}
