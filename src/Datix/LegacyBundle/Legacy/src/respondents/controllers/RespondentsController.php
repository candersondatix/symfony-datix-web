<?php

namespace src\respondents\controllers;

use src\respondents\model\RespondentModelFactory;
use src\organisations\model\OrganisationModelFactory;
use src\framework\controller\Controller;
use src\policies\model\PolicyModelFactory;

class RespondentsController extends Controller
{
    public function ListLinkedRespondents()
    {
        $module   = $this->request->getParameter('module');
        $recordid = $this->request->getParameter('recordid');
        $FormType = $this->request->getParameter('FormType');

        // build listing
        $RecordList = new \RecordLists_RecordList();

        $respondentsList = (new RespondentModelFactory)->getMapper()->findRespondentsByMainID($recordid);

        $RecordList->AddRecordData($respondentsList);

        //If there are records, check for total responsibility and convert 'type' from code to description
        if (count($RecordList->Records))
        {
            $total_resp = 0;

            foreach ($RecordList->Records as $record)
            {
                $total_resp += $record->Data['link_resp'];

                if ($record->Data['link_type'] == 'CON')
                {
                    $record->Data['link_type'] = 'Contact';
                }
                elseif ($record->Data['link_type'] == 'ORG')
                {
                    $record->Data['link_type'] = 'Organisation';
                }
            }
        }

        $columns = array('columns' => array(
            new \Fields_DummyField(array('name' => 'name', 'label' => _tk('name'), 'type' => 'S', 'width' => 5)),
            new \Fields_DummyField(array('name' => 'link_type', 'label' => 'Type', 'type' => 'S', 'width' => 3)),
            new \Fields_DummyField(array('name' => 'link_resp', 'label' => 'Responsibility', 'type' => 'PCT', 'width' => 3)),
            new \Fields_DummyField(array('name' => 'indemnity_reserve_assigned', 'label' => 'Indemnity incurred', 'type' => 'M', 'width' => 7)),
            new \Fields_DummyField(array('name' => 'expenses_reserve_assigned', 'label' => 'Expenses incurred', 'type' => 'M', 'width' => 7)),
            new \Fields_DummyField(array('name' => 'remaining_indemnity_reserve_assigned', 'label' => 'Indemnity Reserve Assigned', 'type' => 'M', 'width' => 7)),
            new \Fields_DummyField(array('name' => 'remaining_expenses_reserve_assigned', 'label' => 'Expenses Reserve Assigned', 'type' => 'M', 'width' => 7)),
            new \Fields_DummyField(array('name' => 'resp_total_paid', 'label' => 'Total Paid', 'type' => 'M', 'width' => 10)),
        ));

        $Design = new \Listings_ListingDesign($columns);

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design, '', ($FormType == 'Print' || $FormType == 'ReadOnly'));
        $ListingDisplay->Action = 'redirecttorespondent';
        $ListingDisplay->EmptyMessage = 'No respondents.';

        if ($FormType == 'ReadOnly')
        {
            $ListingDisplay->ReadOnly = true;
        }

        //Because we're looking at linked respondents, we need to check for CASCADE_PERMISSIONS, which overrides the normal con/org perms.
        if (bYN($this->registry->getParm('CASCADE_PERMISSIONS', 'N')))
        {
            $canAddIndividuals = true;
            $canAddOrganisations = true;
        }
        else
        {
            $canAddIndividuals = $_SESSION['CurrentUser']->canAddRecords('CON');
            $canAddOrganisations = $_SESSION['CurrentUser']->canAddRecords('ORG');
        }

        $this->response->build('src/respondents/views/ListLinkedRespondents.php', array(
            'ListingDisplay'      => $ListingDisplay,
            'module'              => $module,
            'hasRecords'          => (count($RecordList->Records) > 0),
            'totalResponsibility' => number_format($total_resp,2),
            'recordid'            => $recordid,
            'canAddIndividuals'   => $canAddIndividuals,
            'canAddOrganisations' => $canAddOrganisations
        ));
    }

    /**
     * Renders the linked respondent listing on a policy form.
     */
    public function listRespondentsForPolicy()
    {
        $policy_id = (int) $this->request->getParameter('recordid');
        $formType  = $this->request->getParameter('FormType');

        $recordList = new \RecordLists_RecordList();

        // TODO would be better to use a respondent collection here, but currently the respondent model only encapsulates link meta data
        //      so rather than spend ages rewriting all of that on a tocket that's already taken me far longer than it should have, we'll just
        //      fetch the raw data for the listing via the mapper for now.
        $respondents = (new RespondentModelFactory)->getMapper()->findRespondentsByPolicyId($policy_id);

        foreach ($respondents as $key => $respondent)
        {
            $respondents[$key]['link_type'] = (strcasecmp($respondent['link_type'], 'con') == 0 ? 'Contact' : 'Organisation');
        }

        $recordList->AddRecordData($respondents);

        $columns = array('columns' => array(
            new \Fields_DummyField(array('name' => 'name', 'label' => _tk('name'), 'type' => 'S')),
            new \Fields_DummyField(array('name' => 'link_type', 'label' => 'Type', 'type' => 'S')),
            new \Fields_DummyField(array('name' => 'link_resp', 'label' => 'Responsibility', 'type' => 'PCT'))
        ));

        $design = new \Listings_ListingDesign($columns);

        $listingDisplay = new \Listings_ListingDisplay($recordList, $design, '', in_array($formType, ['Print','ReadOnly']));
        $listingDisplay->Action = 'redirecttorespondent';
        $listingDisplay->URLParameterArray = [
            'module'        => 'POL',
            'main_recordid' => $policy_id,
            'recordid'
        ];

        $this->response->setBody($listingDisplay->GetListingHTML());
    }

    public function redirectToRespondent ()
    {
        // The listing table only allows one action for all links, but
        // organisations and individual contacts are two completely different
        // entities, so we need to redirect them to their respective edit forms

        $link_recordid = $this->request->getParameter('recordid');
        $respondent    = (new RespondentModelFactory)->getMapper()->find ($link_recordid);
        $main_recordid = (int) $this->request->getParameter('main_recordid') ?: $respondent->main_recordid;
        $module        = $this->request->getParameter('module') ?: 'CLA';

        switch ($respondent->link_type)
        {
            case 'CON':
                $url = 'app.php?action=linkcontactgeneral&module='.$module.'&link_recordid='.$link_recordid.'&link_type=O&main_recordid='.$main_recordid.'&from_parent_record=1';
                break;
            case 'ORG':
                $url = 'app.php?action=editorganisationlink&recordid='.$respondent->org_id.'&main_recordid='.$main_recordid.'&link_recordid='.$link_recordid.'&module='.$module.'&from_parent_record=1';
                break;
        }

        $this->redirect($url);
    }

    public function UnlinkRespondent ()
    {
        $data = $this->request->getParameters();

        $main_recordid = $data['main_recordid'];
        $link_recordid = $data['link_recordid'];
        $module        = \Sanitize::getModule($this->request->getParameter('module'));

        $linkMapper         = (new RespondentModelFactory())->getMapper();

        $link = $linkMapper->find($link_recordid);
        $linkMapper->delete($link);

        AddSessionMessage('INFO', _tk('unlink_organisation_successfully'));

        // redirect back to claims
        $this->redirect('app.php?action=record&module='.$module.'&fromsearch=1&panel=respondents&recordid='.$main_recordid);
    }

    public function MakeDynamicOrganisationSection()
    {
        global $MaxSuffixField, $ModuleDefs;

        $organisationsToShow  = array();
        $organisationsSection = array();

        $aParams = $this->request->getParameters();

        $module = $aParams['module'];

        // Define hard-coded values
        $aParams['organisationType'] = 'G';
        $aParams['suffix'] = 100;
        $aParams['level'] = 1;
        $organisationsMaxSuffix = $aParams['suffix'];

        $Data = $aParams['data']; //need to check whether there are multiple sections to put into the form.

        if (!empty($Data['org']) && !$Data['temp_record'])
        {
            $NumToShow = count($Data['org'][$aParams['organisationType']]);

            if (!$MaxSuffixField)
            {
                $MaxSuffixField = true;
                $totalNumContacts = '<input type="hidden" name="max_suffix" id="max_suffix" value="'.(GetTotalNumContacts($Data)+9).'" />';
            }

            if (!$NumToShow)
            {
                if ($aParams['FormType'] != 'Print' && $aParams['FormType'] != 'ReadOnly')
                {
                    $NumToShow = 1;
                }
            }
        }
        else
        {
            if ($Data['organisation_max_suffix'])
            {
                $organisationsToShow = (new OrganisationModelFactory())->getMapper()->getPostedOrganisations($Data, $aParams['organisationType']);
                $aParams['suffixpresent'] = true; //prevent additional suffix being added.
                $NumToShow = count($organisationsToShow);
            }
            elseif ($Data['temp_record'])
            {
                $organisationsToShow = $Data['con'][$aParams['organisationType']];
                $Data['org'][$aParams['organisationType']] = $Data['con'][$aParams['organisationType']];
                $NumToShow = count($organisationsToShow);
            }

            if (!$NumToShow)
            {
                $organisationsToShow[] = $aParams['suffix'];

                if ($aParams['FormType'] != 'Print' && $aParams['FormType'] != 'ReadOnly')
                {
                    $NumToShow = 1;
                }
            }

            if (!$MaxSuffixField)
            {
                $MaxSuffixField = true;
                $maxSuffix = '<input type="hidden" name="max_suffix" id="max_suffix" value="'.($Data['max_suffix'] ? $Data['max_suffix'] : '9').'" />';
            }
        }

        if ($organisationsMaxSuffix)
        {
            $organisationsMaxSuffix += $NumToShow;
        }
        else
        {
            $organisationsMaxSuffix = 6;
        }

        for ($i = 0; $i < $NumToShow; $i++)
        {
            if (($_SESSION['logged_in'] && !isset($Data['error'])) || $Data['temp_record'])
            {
                $aParams['data'] = $Data['org'][$aParams['organisationType']][$i];

                if ($i != 0 || !$aParams['suffix']) //first contact should take the default suffix
                {
                    $aParams['suffix'] = $i + $organisationsMaxSuffix;
                }
            }
            else
            {
                if (!empty($organisationsToShow[$i]))
                {
                    $aParams['suffix'] = $organisationsToShow[$i];
                }
            }

            if ($i == 0)
            {
                $aParams['clear'] = true;
            }

            $organisationsSection[$aParams['suffix']] = $this->call('src\respondents\controllers\RespondentsController', 'getOrganisationSection', array('aParams' => $aParams));
        }

        $this->response->build('src/respondents/views/OrganisationSection.php', array(
            'MaxSuffixField' => $MaxSuffixField,
            'Data' => $Data,
            'aParams' => $aParams,
            'NumToShow' => $NumToShow,
            'level1OrgOptions' => $ModuleDefs[$module]['LEVEL1_ORG_OPTIONS'][$aParams['organisationType']]['Max'],
            'webSpellChecker' => $_SESSION['Globals']['WEB_SPELLCHECKER'],
            'module' => $module,
            'MaxSuffix' => $organisationsMaxSuffix,
            'ContactsToShow' => $organisationsToShow,
            'totalNumContacts' => $totalNumContacts,
            'maxSuffix' => $maxSuffix,
            'organisationsSection' => $organisationsSection
        ));
    }

    public function getOrganisationSection()
    {
        global $ModuleDefs, $FieldDefs;

        // Set this so that the link details section is displayed
        $ShowLinkDetails = true;

        require_once 'Source/generic_modules/ORG/AppVars.php';

        $aParams      = $this->request->getParameter('aParams');
        $data         = $aParams['data'];
        $AJAX         = ($aParams === null);
        $formId       = $aParams['form_id'];
        $parentFormId = $aParams['parent_form_id'];
        $Module       = \Sanitize::SanitizeString($aParams['module']);
        $Type         = \Sanitize::SanitizeString($aParams['organisationType']);
        $Suffix       = \Sanitize::SanitizeString($aParams['suffix']);
        $clear        = \Sanitize::SanitizeString($aParams['clear']);

        if ($AJAX)
        {
            $aParams = \Sanitize::SanitizeRawArray($this->request->getParameters());
        }

        if ($Suffix && !$aParams['suffixpresent'] && !empty($data))
        {
            $Rows = getContactLinkFields($Type);

            $Rows = array_merge($Rows, $ModuleDefs['ORG']['FIELD_ARRAY']);

            $data = AddSuffixToData($data, $Suffix, array('Rows' => $Rows));
        }

        $organisationFormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module'        => 'ORG',
            'level'         => $aParams['level'],
            'parent_level'  => $aParams['level'],
            'parent_module' => $Module,
            'link_type'     => $Type
        ));
        $organisationFormDesign->AddSuffixToFormDesign($Suffix, _tk('ORGNameTitle'));

        $organisationTable = new \FormTable($aParams['FormType'], 'ORG', $organisationFormDesign);

        if ($aParams['FormType'] != 'ReadOnly' && $aParams['FormType'] != 'Print')
        {
            if (!$clear)
            {
                $RightHandLink['onclick'] = 'var DivToDelete=$(\'organisation_section_div_'.$Suffix.'\');DivToDelete.parentNode.removeChild(DivToDelete)';
                $RightHandLink['text'] = _tk('delete_section');
            }
            else
            {
                if (is_numeric($formId))
                {
                    $parentFormId = \Sanitize::SanitizeInt($formId);
                }
                elseif (is_numeric($parentFormId))
                {
                    $parentFormId = \Sanitize::SanitizeInt($parentFormId);
                }
                else
                {
                    $parentFormId = '';
                }

                $spellChecker = $_SESSION['Globals']['WEB_SPELLCHECKER'] == 'Y' ? 'true' : 'false';
                $RightHandLink['onclick'] = 'ReplaceSection(\''.$Module.'\','.$Suffix.', \'organisation\', \''.$Type.'\', '.$spellChecker.', \''.$parentFormId.'\')';
                $RightHandLink['text'] = _tk('clear_section');
            }
        }

        if (is_array($GLOBALS['OrganisationMatch']))
        {
            // Add external contact search button to fields.
            $OrgMatchFields = array_keys($organisationFormDesign->OrganisationMatch);

            foreach ($OrgMatchFields as $FieldName)
            {
                if ($organisationFormDesign->OrganisationMatch[$FieldName])
                {
                    $FieldName = \UnicodeString::substr($FieldName, 0, (\UnicodeString::strlen($FieldName) - \UnicodeString::strlen("_" . $Suffix)));
                    $organisationTable->FieldDefs[$FieldName]['OriginalType'] = $FieldDefs['ORG']["$FieldName"]['Type'];
                    $organisationTable->FieldDefs["$FieldName"]['Type'] = 'string_org_search';
                }
            }
        }

        // We need to use require instead of require_once because this function can be loaded multiple times within the same request.
        require 'Source/generic_modules/ORG/BasicForm1.php';

        $organisationTable->MakeForm($FormArray, $data, $Module, array('dynamic_section' => true));

        $this->response->build('src/respondents/views/ShowOrganisationSection.php', array(
            'organisationTable' => $organisationTable,
            'Suffix'            => $Suffix,
            'data'              => $data
        ));
    }

    /**
     * Gets payments assigned to a respondent.
     *
     * Called via AJAX that's why we are echoing instead of returning.
     */
    public function getRespondentPayments()
    {
        $respondentMapper = (new RespondentModelFactory())->getMapper();

        $numPayments = $respondentMapper->getAssignedPayments($this->request->getParameter('mainRecordId'), $this->request->getParameter('linkRecordId'));

        echo json_encode([
            'numPayments' => $numPayments,
            'message' => _tk('payments_assigned_to_respondent')
        ]);
    }
}