<?php
/**
 * Created by PhpStorm.
 * User: toliveira
 * Date: 27/05/14
 * Time: 14:52
 */

namespace src\memorycache;

interface CacheEngineInterface
{
    /**
     * Adds a variable to the cache.
     *
     * @param string|array $key   Key of a variable to store.
     * @param mixed        $value Value of a variable to store.
     * @param int          $ttl   Time for the variable to live in the cache in seconds.
     *
     * @return bool
     */
    public function add($key, $value, $ttl = 0);

    /**
     * Adds a variable to the cache and overwrites a variable if it already exists.
     *
     * @param string|array $key   Key of a variable to store.
     * @param mixed        $value Value of a variable to store.
     * @param int          $ttl   Time for the variable to live in the cache in seconds.
     *
     * @return bool
     */
    public function set($key, $value, $ttl = 0);

    /**
     * Deletes variables from the cache.
     *
     * @param mixed $key The key that was used to store the variable in the cache.
     *
     * @return bool
     */
    public function delete($key);

    /**
     * Checks if a variable exists in the cache.
     *
     * @param string $key The key that was used to store the variable in the cache.
     *
     * @return bool
     */
    public function exists($key);

    /**
     * Gets a variable stored in the cache.
     *
     * @param mixed $key The key that was used to store the variable in the cache.
     *
     * @return bool
     */
    public function get($key);

    /**
     * Clears/deletes all the values stored in the cache.
     */
    public function flush();
}