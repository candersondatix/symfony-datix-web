<?php

require_once 'src/autoload/autoload.php';
require_once 'client/DATIXConfig.php';

use src\messagequeue\MessageQueueFactory;
use src\messagequeue\MessageQueue;

(new MessageQueueFactory)->getWorker(MessageQueue::EMAIL, 'sendemails', ['recordid', 'perms', 'formlevel', 'emailtype']);