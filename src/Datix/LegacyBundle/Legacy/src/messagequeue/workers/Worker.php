<?php

namespace src\messagequeue\workers;

require_once __DIR__ . '/../../../vendor/autoload.php';

use PhpAmqpLib\Message\AMQPMessage;
use src\messagequeue\MessageQueueFactory;
use src\logger\iLogger;

class Worker
{
    /**
     * The command that this worker executes.
     * 
     * @var string
     */
    protected $command;
    
    /**
     * The arguments we're sending with the command.
     * 
     * @var array
     */
    protected $parameters;
    
    /**
     * @var iLogger
     */
    protected $logger;
    
    /**
     * Constructor.
     * 
     * Initiates the worker process.
     * 
     * @param int                 $queue               The queue that this worker consumes.
     * @param string              $action              The controller action responsible for processing messages from this queue.
     * @param array               $parameters          The message content we're forwarding to the controller.
     * @param MessageQueueFactory $messageQueueFactory The factory used to retrieve the message queue instance.
     * @param iLogger             $logger              The logger service.
     */
    public function __construct($queue, $action, array $parameters, MessageQueueFactory $messageQueueFactory, iLogger $logger)
    {
        $this->command    = 'php.exe -f index.php '.escapeshellarg($action);
        $this->parameters = $parameters;
        $this->logger     = $logger;
        
        $messageQueueFactory = $messageQueueFactory ?: new MessageQueueFactory();
        
        try 
        {
            $messageQueueFactory->getQueue()->consume($queue, [$this, 'callback']);
        } 
        catch (\Exception $e)
        {
            $this->handleError('MQ Error ('.get_class($e).'): '.$e->getMessage());
        }
    }
    
    /**
     * The function used to process each queue message.
     * 
     * @param AMQPMessage $message The queue message.
     */
    public function callback(AMQPMessage $message)
    {
        $content = json_decode($message->body);
        
        foreach ($this->parameters as $parameter)
        {
            $args .= ' '.escapeshellarg($parameter).'='.escapeshellarg($content->$parameter);
        }
    
        exec($this->command.$args, $output, $return_var);
        
        if ($return_var > 0)
        {
            $this->handleError('MQ Message Processing Error: '.$return_var.'; Message: '.$message->body.'; Details: '.json_encode($output));
        }
        
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    }
    
    /**
     * Handles error reporting.
     * 
     * @param string $error The error that's being reported.
     */
    protected function handleError($error)
    {
        $this->logger->logEmergency($error);
        echo $error;
    }
}