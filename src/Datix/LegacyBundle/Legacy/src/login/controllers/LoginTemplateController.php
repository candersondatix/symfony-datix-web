<?php

namespace src\login\controllers;

use src\framework\controller\TemplateController;

class LoginTemplateController extends TemplateController
{
    function login()
    {
        global $scripturl, $UserSSOSettingsFile, $dtxlogin_image;

        $this->addCss('src/login/css/login.css');

        require_once 'Source/Login.php';

        if ($this->request->getParameter('user'))
        {
            $user = \Sanitize::SanitizeString($this->request->getParameter('user'));
        }

        if ($this->request->getParameter('form_id') && is_numeric($this->request->getParameter('form_id')))
        {
            $formID = \Sanitize::SanitizeInt($this->request->getParameter('form_id'));
            $form_idtag = '&form_id=' . $formID;
        }

        if ($this->request->getParameter('url'))
        {
            $url = \Sanitize::SanitizeURL($this->request->getParameter('url'));
        }
        else
        {
            $url = \Sanitize::SanitizeURL($_SERVER['QUERY_STRING']);
        }

        if (\UnicodeString::strpos($url, 'error') || \UnicodeString::strpos($url, 'login_failed'))
        {
            $url = $scripturl . '?action=login';
        }

        $this->title = _tk('login_title');

        // Are we using SSO?
        $NetworkLogin = bYN(GetParm('WEB_NETWORK_LOGIN', 'N'));

        // If $NetworkLogin is set and we have got here because Login2() generated
        // an error, redirect to the Login page.
        if ($NetworkLogin && ($this->request->getParameter('error') || $this->request->getParameter('login_failed')))
        {
            session_unset();
            @session_destroy();
            $this->response->build('src/login/views/LoginError.php', array(
                'error' =>
                    $this->request->getParameter('error') ?
                        \Sanitize::SanitizeString($this->request->getParameter('error')) :
                        _tk('login_failed_err')
            ));
            return;
        }

        // Need to include LDAP so we can display the Domain dropdown, or use SSO.
        require_once 'Source/libs/LDAP.php';
        $ldap = new \datixLDAP();

        $LDAPAuthentication = $ldap->LDAPEnabled();
        $ldap_domains = $ldap->LDAPDomains();

        if ($NetworkLogin)
        {
            if ($UserSSOSettingsFile && is_file($UserSSOSettingsFile))
            {
                include($UserSSOSettingsFile);
                $SSOAccountStatus = CheckSSOAccountStatus($LDAPAuthentication);
            }
            else
            {
                fatal_error('No authenticated user has been provided. Please contact your Datix administrator');
            }

            //If there is no UserSSOSettingsFile set, or the file does not exist, then we
            //should return a message, since the system has been set up incorrectly.
            if (!$LoginAndDomain || !$SSOAccountStatus['account_active'])
            {
                $NetworkLogin = false;
            }
            else
            {
                // Separate out the login name and domain
                $LoginArray = GetUserAndDomain($LoginAndDomain);

                // Go straight to do the login processing without displaying the
                // login page.  This call to Login2() is the reason why we don't
                // want to destroy the session.
                Login2(true, $LoginArray['user'], $LoginArray['domain'], $url);
                obExit();
            }
        }

        // Only display the header if we aren't using SSO.
        // We only want to destroy the session if we are NOT using SSO.
        if (!$NetworkLogin)
        {
            $this->hasPadding = false;
            $this->hasMenu = false;

            if (!$SSOAccountStatus['account_active'] && $SSOAccountStatus['message'])
            {
                $this->response->build('src/login/views/LoginError.php', array(
                    'error' => \Sanitize::SanitizeString($SSOAccountStatus['message'])
                ));
                return;
            }
        }

        $url = htmlspecialchars($url);

        $userImage = ($dtxlogin_image ? '<img src="'.$dtxlogin_image.'">' : '');

        $usernameLabel = _tk('user_name');
        $passwordLabel = _tk('password');

        if ($LDAPAuthentication)
        {
            if($ldap_domains)
            {
                $NumDomains = count($ldap_domains);
            }
        }

        $this->response->build('src/login/views/Login.php', array(
            'form_idtag' => $form_idtag,
            'url' => $url,
            'formID' => $formID,
            'error' => $this->request->getParameter('error'),
            'login_failed' => $this->request->getParameter('login_failed'),
            'usernameLabel' => $usernameLabel,
            'user' => $user,
            'passwordLabel' => $passwordLabel,
            'LDAPAuthentication' => $LDAPAuthentication,
            'ldap_domains' => $ldap_domains,
            'NumDomains' => $NumDomains,
            'userImage' => $userImage,
            'attempts' => $this->request->getParameter('attempts')
        ));
    }
}