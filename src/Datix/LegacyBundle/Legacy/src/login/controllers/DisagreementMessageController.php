<?php

namespace src\login\controllers;

use src\framework\controller\Controller;

class DisagreementMessageController extends Controller
{
    /**
     * Displays disagreement message
     */
    function disagreementmessage()
    {
        $this->response->build('src/login/views/DisagreementMessage.php', array());
    }
}