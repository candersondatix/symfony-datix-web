<?php if ($this->SSOAccountStatus['no_reg_form']) : ?>
<div class="info_div"><?php echo $this->SSOAccountStatus['message']; ?></div>
<?php else : ?>
<form method="post" name="frmContact" action="<?php echo $this->scripturl; ?>?action=contactformregister2">
    <?php if ($this->con['login']) : ?>
    <input type="hidden" id="login" name="login" value="<?php echo htmlspecialchars($this->con['login']); ?>">
    <?php endif; ?>
    <?php if ($this->con['error']) : ?>
    <div class="form_error">The form you submitted contained the following errors.  Please correct them and try again.</div>
    <div class="form_error"><?php echo $this->con['error']['message']; ?></div>
    <?php endif; ?>
    <?php echo $this->ConTable->GetFormTable(); ?>
    <div class="button_wrapper">
        <input type="hidden" id="rbWhat" name="rbWhat" value="add">
        <input type="button" id="btnSave" name="btnSave" value="<?php echo _tk('btn_submit_reg'); ?>" onclick="submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()) { document.forms[0].rbWhat.value='add'; document.forms[0].submit(); }" />
        <input type="button" value="<?php echo _tk('btn_cancel'); ?>" onclick="if(confirm('Press \'OK\' to confirm or \'Cancel\' to return to the form')){submitClicked = true; document.forms[0].rbWhat.value='cancel'; this.form.submit();}" />
    </div>
</form>
<script type="text/javascript" language="javascript">
    <?php echo MakeJavaScriptValidation(); ?>
    var submitClicked = false;
</script>
<?php endif; ?>