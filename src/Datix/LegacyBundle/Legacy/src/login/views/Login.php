<div id="login" style="float: left;">
    <div class="login-wrap">
        <form name="frmLogin" action="<?php echo $this->scripturl; ?>?action=login2<?php echo $this->form_idtag; ?>" method="post">
            <input type="hidden" name="url" value="<?php echo $this->url; ?>" />
            <input type="hidden" name="form_id" value="<?php echo Sanitize::SanitizeInt($this->formID); ?>" />
            <table border="0" width="400" cellspacing="1" cellpadding="0" class="bordercolor" align="center">
                <tr>
                    <td class="windowbg" width="100%">
                        <table width="100%" cellspacing="0" cellpadding="3">
                            <tr>
                                <td class="titlebg" colspan="2">
                                    <img src="images/group_key.png" alt="" style="float: left; margin-right: 5px;">
                                    <h1><?php echo htmlspecialchars(_tk('login_title')); ?></h1>
                                </td>
                            </tr>
                            <?php if ($this->login_failed != '') : ?>
                                <tr>
                                    <td class="windowbg2" colspan="2">
                                        <div style="color: red"><b><?php echo _tk('login_failed_err'); ?></b> <img src="Images/Help.gif" onclick="jQuery('#login_failed_desc').toggle()" /></div>
                                        <div style="color: red;display:none" id="login_failed_desc"><?php echo _tk('login_failed_err_desc'); ?></div>
                                    </td>
                                </tr>
                            <?php elseif ($this->error != '') : ?>
                            <tr>
                                <td class="windowbg2" colspan="2">
                                    <font color="red">
                                        <b><?php echo src\security\Escaper::escapeForHTML($this->error); ?></b>
                                        <?php if ($this->attempts && $this->attempts > 0) : ?>
                                        <br />
                                        <b><?php echo src\security\Escaper::escapeForHTML(_tk('login_attempts_left').': '.$this->attempts); ?></b>
                                        <?php endif; ?>
                                    </font>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <td align="right" class="windowbg2">
                                    <label for="name"><?php echo $this->usernameLabel; ?></label>
                                </td>
                                <td class="windowbg2">
                                    <font size="2">
                                        <input type="text" id="name" name="user" size="20" maxlength="254" value="<?php echo $this->user; ?>" />
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="windowbg2">
                                    <label for="password"><?php echo $this->passwordLabel; ?></label>
                                </td>
                                <td class="windowbg2">
                                    <font size="2">
                                        <input type="password" id="password" name="passwrd" size="20" autocomplete="off" />
                                    </font>
                                </td>
                            </tr>
                            <?php if ($this->LDAPAuthentication) : ?>
                                <?php if($this->ldap_domains) : ?>
                                <tr>
                                    <td align="right" class="windowbg2">
                                        <label for="domain"><?php echo _tk("domain"); ?></label>
                                    </td>
                                    <td class="windowbg2">
                                        <font size="2">
                                            <select name="domain" id="domain">
                                                <?php
                                                $i = 0;
                                                ?>
                                                <option value="" style="font-style: italic;">&lt;None&gt;</option>
                                                <option value="<?php echo $this->ldap_domains[$i]; ?>" selected><?php echo $this->ldap_domains[$i]; ?></option>
                                                <?php
                                                $i++;
                                                while ($i < $this->NumDomains)
                                                {
                                                ?>
                                                <option value="<?php echo $this->ldap_domains[$i]; ?>"><?php echo $this->ldap_domains[$i]; ?></option>
                                                <?php
                                                $i++;
                                                }
                                                ?>
                                            </select>
                                    </td>
                                </tr>
                                <?php endif; ?>
                            <?php endif; ?>
                            <tr>
                                <td colspan="2" class="windowbg2" style="text-align: center; padding-top: 15px;">
                                    <input type="submit" value="<?php echo _tk('btn_log_in'); ?>" />
                                </td>
                            </tr>
                            <?php if (!$this->LDAPAuthentication) : ?>
                            <tr>
                                <td align="center" colspan="2" class="windowbg2">
                                    <a href="<?php echo $this->scripturl; ?>?action=reset_password&amp;user=<?php echo $this->user; ?>"><small><?php echo _tk('forgotten_password'); ?></small></a>
                                    <br /><br />
                                </td>
                            </tr>
                            <?php endif; ?>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div style="float: left; padding: 15px;"><?php echo $this->userImage; ?></div>
<script language="javascript" type="text/javascript">
    document.frmLogin.user.focus();
</script>