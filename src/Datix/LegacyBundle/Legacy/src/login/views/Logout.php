<?php if ($this->TimedOut) : ?>
<table border="0" width="80%" cellspacing="1" align="center" cellpadding="4">
    <tr>
        <td class="titlebg">
            <font class="text1"><b><?php echo _tk('timed_out_title'); ?></b></font>
        </td>
    </tr>
    <tr>
        <td class="windowbg">
            <br /><?php echo _tk('timed_out_msg'); ?><br /><br />
        </td>
    </tr>
</table>
<?php elseif ($this->no_session) : ?>
<table border="0" width="80%" cellspacing="1" align="center" cellpadding="4">
    <tr>
        <td class="titlebg">
            <font class="text1"><b>Session not found</b></font>
        </td>
    </tr>
    <tr>
        <td class="windowbg">
            <br />A reference to your session was not found. This may be because it timed out or because it was manually ended by an administrator.<br /><br />
        </td>
    </tr>
</table>
<?php endif; ?>