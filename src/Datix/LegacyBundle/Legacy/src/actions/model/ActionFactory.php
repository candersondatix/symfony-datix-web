<?php

namespace src\actions\model;

use src\framework\model\EntityFactory;
use src\framework\model\Entity;
use src\framework\model\EntityCache;
use src\framework\registry\Registry;
use src\actions\emails\NewActionEmails;
use src\email\EmailSenderFactory;
use src\users\model\UserModelFactory;

class ActionFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actions\\model\\Action';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null, Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();
        
        $object = parent::doCreateObject($data, $baseEntity, $cache, $registry);
        
        if ($registry->getParm('ACT_EMAIL', 'N', false, true))
        {
            $object->attach(new NewActionEmails(EmailSenderFactory::createEmailSender('ACT', 'New'), $registry->getLogger(), new UserModelFactory()));
        }
        
        return $object;
    }
}