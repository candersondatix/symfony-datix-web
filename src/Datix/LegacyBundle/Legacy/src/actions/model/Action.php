<?php
namespace src\actions\model;

use src\framework\model\DatixEntity;

class Action extends DatixEntity
{
    /**
     * The module this action is linked to.
     *
     * @var string
     */
    protected $act_module;

    /**
     * The id of the main record this action is linked to.
     *
     * @var int
     */
    protected $act_cas_id;

    /**
     * The Trust assigned to this action.
     *
     * @var string
     */
    protected $act_organisation;

    /**
     * The Unit assigned to this action.
     *
     * @var string
     */
    protected $act_unit;

    /**
     * The Clinical Group assigned to this action.
     *
     * @var string
     */
    protected $act_clingroup;

    /**
     * The Directorate assigned to this action.
     *
     * @var string
     */
    protected $act_directorate;

    /**
     * The Specialty assigned to this action.
     *
     * @var string
     */
    protected $act_specialty;

    /**
     * The Location (type) assigned to this action.
     *
     * @var string
     */
    protected $act_loctype;

    /**
     * The Location (actual) assigned to this action.
     *
     * @var string
     */
    protected $act_locactual;

    /**
     * The user (initials) that assigned the action.
     *
     * @var string
     */
    protected $act_from_inits;

    /**
     * The user (initials) responsible for the action.
     *
     * @var string
     */
    protected $act_to_inits;

    /**
     * The user (initials) that completed the action.
     *
     * @var string
     */
    protected $act_by_inits;

    /**
     * The due date.
     *
     * @var string
     */
    protected $act_ddue;

    /**
     * The done date.
     *
     * @var string
     */
    protected $act_ddone;

    /**
     * The description.
     *
     * @var string
     */
    protected $act_descr;

    /**
     * The score.
     *
     * @var int
     */
    protected $act_score;

    /**
     * The cost.
     *
     * @var string
     */
    protected $act_cost;

    /**
     * The type.
     *
     * @var string
     */
    protected $act_type;

    /**
     * The cost type.
     *
     * @var string
     */
    protected $act_cost_type;

    /**
     * The start date.
     *
     * @var string
     */
    protected $act_dstart;

    /**
     * The priority.
     *
     * @var string
     */
    protected $act_priority;

    /**
     * The id of the action chain used to create this action (if applicable).
     *
     * @var int
     */
    protected $act_chain_id;

    /**
     * The identifier for this action chain instance (since the same chain can be added to a record multiple times).
     *
     * @var int
     */
    protected $act_chain_instance;

    /**
     * The step number this action represents in the chain.
     * 
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var int
     */
    protected $act_step_no;

    /**
     * The event which triggers the start of the action (chain commencement or completion of a previous action).
     * 
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var string
     */
    protected $act_start_after_type;

    /**
     * The ID of the previous step when acs_start_after_type is completion of a previous action.
     * 
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var int
     */
    protected $act_start_after_step;

    /**
     * The number of days after the event specified by acs_start_after_type that the action starts (act_dstart).
     * 
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var int
     */
    protected $act_start_after_days_no;

    /**
     * How act_start_after_days_no is calculated (calendar days or working days).
     * 
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var string
     */
    protected $act_start_after_days_type;

    /**
     * The number of days after the start date that the action is due (act_ddue).
     * 
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var string
     */
    protected $act_due_days_no;

    /**
     * How act_due_days_no is calculated (calendar days or working days).
     * 
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var string
     */
    protected $act_due_days_type;

    /**
     * The number of days before the due date a reminder is sent out for the action.
     *
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var string
     */
    protected $act_reminder_days_no;

    /**
     * How act_reminder_days_no is calculated (calendar days or working days).
     *
     * Copied from the Action Chain template in order to create a snapshot when the action record is created
     * (i.e. so that subsequent updates to the Action Chain don't affect concrete chains already assigned to records).
     *
     * @var string
     */
    protected $act_reminder_days_type;

    /**
     * The synopsis.
     *
     * @var string
     */
    protected $act_synopsis;

    /**
     * The progress.
     *
     * @var string
     */
    protected $act_progress;

    /**
     * Resource requirements.
     *
     * @var string
     */
    protected $act_resources;

    /**
     * Reporting/Monitoring requirements.
     *
     * @var string
     */
    protected $act_monitoring;
}