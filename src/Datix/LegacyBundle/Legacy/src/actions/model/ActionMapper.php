<?php
namespace src\actions\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;

class ActionMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actions\\model\\Action'; 
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'ca_actions';
    }
    
    /**
     * {@inheritdoc}
     * 
     * Not-null constraints on ca_actions.act_module and ca_actions.act_cas_id means we can't insert a blank record when
     * the recordid is generated, so we have to handle differently here.
     */
    protected function doInsertWithoutIdentity(Entity $object)
    {
        // TODO remove reliance on GetNextRecordID() and handle all logic explicitly here
        $object->recordid = GetNextRecordID($this->getTable());
        
        $props = $this->getEntityProperties($object);
        $props = $props + array('recordid' => $object->recordid);
        
        $query = $this->factory->getQueryFactory()->getQuery();
        $query->insert($props)
              ->into($this->getTable());
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        
        return $object->recordid;
    }
}