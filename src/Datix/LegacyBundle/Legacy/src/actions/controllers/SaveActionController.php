<?php

namespace src\actions\controllers;

use src\framework\controller\Controller;
use src\actionchains\ActionChainManager;

class SaveActionController extends Controller
{
    function saveaction()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $form_action = \Escape::EscapeEntities($this->request->getParameter('rbWhat'));
        $module = \Sanitize::getModule($this->request->getParameter('act_module'));
        $act_cas_id = \Sanitize::SanitizeInt($this->request->getParameter('act_cas_id'));

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $this->request->getParameter('recordid'))
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'CA_ACTIONS', 'link_id' => $this->request->getParameter('recordid')));
        }

        if ($form_action == 'Cancel')
        {
            $frommainrecord = \Sanitize::SanitizeInt($this->request->getParameter('frommainrecord'));
            $fromsearch = \Sanitize::SanitizeInt($this->request->getParameter('fromsearch'));

            if ($frommainrecord != '')
            {
                $RedirectUrl = getRecordURL(array('module' => $module, 'recordid' => $act_cas_id)).'&panel='.($this->request->getParameter('actionchain') == 1 ? 'action_chains' : 'linked_actions');
            }
            elseif ($this->request->getParameter('from_report'))
            {
                $RedirectUrl = '?action=list&module=ACT&listtype=search&from_report=1';
            }
            elseif ($fromsearch != '')
            {
                $RedirectUrl = '?action=list&module=ACT&listtype=search';
            }
            else
            {
                $RedirectUrl = '?module=ACT';
            }

            if ($frommainrecord != '')
            {
                $this->redirect($RedirectUrl);
            }
            else
            {
                $this->redirect('app.php' . $RedirectUrl);
            }
        }

        if ($form_action == 'BackToListing')
        {
            $this->redirect('app.php?action=reportdesigner&from_report=1');
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'ACT',
            'link_type' => 'linked_actions',
            'level' => 2,
            'parent_module' => $module,
            'parent_level' => 2
        ));
        $FormDesign->LoadFormDesignIntoGlobals();

        $this->request->setParameter('act_cost', str_replace(',', '', $this->request->getParameter('act_cost')));
        $this->request->setParameter('act_cost', str_replace('£', '', $this->request->getParameter('act_cost')));
        $this->request->setParameter('act_cost', str_replace('$', '', $this->request->getParameter('act_cost')));

        $DateErrors = ValidatePostedDates('ACT');
        $MoneyErrors = ValidatePostedMoney('ACT');

        $ValidationErrors = array_merge($DateErrors, $MoneyErrors);

        if ($ValidationErrors)
        {
            $error['Validation'] = $ValidationErrors;
        }

        // act_score - MS SQL int32 is between -2147483648 and 2147483647.
        $score = $this->request->getParameter('act_score');

        if ($score && ($score < -2147483648 || $score > 2147483647))
        {
            $error['Validation']['act_score'] = GetFormFieldLabel('act_score','','ACT').' must be between -2147483648 and 2147483647.';
        }

        $ErrorMark = '<font size="3" color="red"><b>*</b></font>';

        if ($error)
        {
            $params = array();

            foreach ($this->request->getParameters() as $key => $value)
            {
                $params[$key] = \Sanitize::SanitizeString($value);
            }

            //Need to reformat dates to ensure they are in SQL format to be interpreted when reloading the form.
            $actionDateFields = array_merge(GetAllFieldsByType('ACT', 'date'), getAllUdfFieldsByType('D', '', $params));
            foreach ($actionDateFields as $actionDate)
            {
                if (array_key_exists($actionDate, $params) && $params[$actionDate] != '')
                {
                    $params[$actionDate] = UserDateToSQLDate($params[$actionDate]);

                    // Some extra field dates look directly at the post value, so need to blank it out here.
                    $this->request->unSetParameter($actionDate);
                    unset($_POST[$actionDate]);
                }
            }

            $_SESSION['ACTIONS']['act'] = $params;
            $Parameters = array(
                'FormType' => $this->request->getParameter('formaction'),
                'module' => $module,
                'act_cas_id' => $act_cas_id,
                'error' => $error,
            );
            $this->call('src\actions\controllers\ShowActionFormTemplateController', 'showactionform', $Parameters);
            return;
        }

        $AdminPerms = $_SESSION['Perms']['ADM']['form'][0];
        $AdminUser = ($AdminPerms == '');

        $Act = $this->request->getParameters();

        if ($this->request->getParameter('actionchain') == 1)
        {
            $sql = '
                SELECT
                    act_ddone
                FROM
                    ca_actions
                WHERE
                    recordid = :recordid
            ';

            $currentDoneDate = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $Act['recordid']), \PDO::FETCH_COLUMN);

            if (strtotime(UserDateToSQLDate($Act['act_ddone'])) != strtotime($currentDoneDate))
            {
                $updateChainSteps = true;
            }
        }

        if ($this->request->getParameter('recordid') == '')
        {
            $Act['recordid'] = GetNextRecordID('ca_actions');

            $sql = '
                INSERT INTO
                    ca_actions
                    (recordid, act_module, act_cas_id)
                VALUES
                    (:recordid, :act_module, :act_cas_id)
            ';

            \DatixDBQuery::PDO_query($sql, array(
                'recordid' => $Act['recordid'],
                'act_module' => $Act['act_module'],
                'act_cas_id' => $Act['act_cas_id']
            ));
            $NewRecord = true;
            $Act['act_from_inits'] = $_SESSION['initials'];
        }
        else
        {
            DoFullAudit('ACT', 'ca_actions', $Act['recordid']);
        }

        $Act = ParseSaveData(array('module' => 'ACT', 'data' => $Act));

        $sql = 'UPDATE ca_actions SET ';

        $sql .= GeneratePDOSQLFromArrays(array(
                'FieldArray' => $ModuleDefs['ACT']['FIELD_ARRAY'],
                'DataArray' => $Act,
                'end_comma' => true,
                'Module' => 'ACT'
            ),
            $PDOParamsArray
        );

        $sql .= " updateid = '" . GensUpdateID($this->request->getParameter('updateid')) . "',
              updateddate = '" . date('d-M-Y H:i:s') . "',
              updatedby = '". $_SESSION['initials']."'
        WHERE recordid = :recordid";

        $PDOParamsArray['recordid'] = $Act['recordid'];

        // If linking to a risk, we need to update the "open actions" field for use in the main app.
        if ($Act['act_module'] == 'RAM')
        {
            require_once 'Source/actions/LinkedActions.php';
            UpdateRAMActionsOpen(array('risk_id' => $Act['act_cas_id']));
        }

        if (bYN(GetParm('ACT_EMAIL', 'N')))
        {
            require_once 'Source/actions/ActionEmail.php';

            if ($NewRecord)
            {
                $response = EmailAction($Act['recordid'], $Act, 'New');
            }
            else
            {
                $response = EmailAction($Act['recordid'], $Act, 'Update');
            }

            if (!$response['result']) 
            {
                AddSessionMessage('POPUP', sprintf(_tk('emails_not_set_domain_not_permitted'), $response['blocked']));
            }
        }

        $result = \DatixDBQuery::PDO_query($sql, $PDOParamsArray);

        if ($result)
        {
            $frommainrecord = is_numeric($this->request->getParameter('frommainrecord')) ? (int) $this->request->getParameter('frommainrecord') : '';

            if ($frommainrecord != '')
            {
                // Update record last updated
                $this->call('src\\generic\\controllers\\RecordController', 'updateRecordLastUpdated', [
                    'module'   => $module,
                    'recordId' => $act_cas_id
                ]);
            }

            if (bYN(GetParm('WEB_TRIGGERS', 'N')))
            {
                require_once 'Source/libs/Triggers.php';
                ExecuteTriggers($Act['recordid'], $Act, $ModuleDefs['ACT']['MOD_ID']);
            }

            if ($frommainrecord != '')
            {
                AddSessionMessage('INFO', _tk('action_saved'));
                $Url = getRecordURL(array('module' => $Act['act_module'], 'recordid' => $Act['act_cas_id'])).'&panel='.($this->request->getParameter('actionchain') == 1 ? 'action_chains' : 'linked_actions');
                $Url = explode('?', $Url);
                $RedirectUrl = '?' . $Url[1];
            }
            else
            {
                $_SESSION['action_number'] = $Act['recordid'];
                $_SESSION['action_whichmodule'] = $Act['act_module'];
                $RedirectUrl = '?action=showsaveaction&panel=' . $this->request->getParameter('panel');
            }

            require_once 'Source/libs/UDF.php';
            SaveUDFs($Act['recordid'], MOD_ACTIONS);

            // Save Progress Notes
            $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
                'module' => 'ACT',
                'data'   => $Act
            ));

            if (ModIsLicensed('HOT'))
            {
                require_once 'Source/generic_modules/HOT/ModuleFunctions.php';
                InsertIntoHotspotQueue('ACT', $Act['recordid']);
            }

            if ($updateChainSteps)
            {
                // Update dates on subsequent chain steps
                $acManager = new ActionChainManager();
                $acManager->updateStepDates($Act['recordid']);
            }

            $this->redirect('app.php' . $RedirectUrl);
        }

        obExit();
    }
}