<?php

namespace src\actions\controllers;

use src\framework\controller\Controller;

class SaveActionActionsController extends Controller
{
    function saveactionactions()
    {
        $FormAction = $this->request->getParameter('form_action');
        $recordid = $this->request->getParameter('recordid');

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $recordid)
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'CA_ACTIONS', 'link_id' => $recordid));
        }

        switch ($FormAction)
        {
            case 'Cancel':
                $this->redirect('app.php?module=ACT');
                break;
            case 'ListActions':
                $this->redirect('app.php?action=list&module=ACT');
                break;
            case 'BacktoAction':
                $this->call('src\actions\controllers\OpenActionController', 'action', array(
                    'recordid' => $recordid
                ));
                return;
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');
            default:
                $this->redirect('app.php?module=ACT');
        }
    }
}