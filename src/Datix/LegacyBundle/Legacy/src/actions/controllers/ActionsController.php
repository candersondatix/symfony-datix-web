<?php

namespace src\actions\controllers;

use src\framework\controller\Controller;
use src\actionchains\ActionChainManager;

class ActionsController extends Controller
{
    /**
     * Renders "links" panel in Actions module
     */
    function ListModuleLinks()
    {
        require_once 'Source/actions/ActionForm.php';

        $ModuleDefs = $this->registry->getModuleDefs();
        $Data = $this->request->getParameter('data');
        $FormMode = $this->request->getParameter('formType');
        $Module = $this->request->getParameter('module');

        switch ($Data['act_module'])
        {
            case 'INC':
                $this->ListIncidents($Data, $FormMode);
                break;
            case 'RAM':
                $this->ListRisks($Data, $FormMode);
                break;
            case 'SAB':
                $this->ListSABS($Data, $FormMode);
                break;
            case 'AST':
                $this->ListAssets($Data, $FormMode);
                break;
            case 'STN':
                $this->ListStandards($Data, $FormMode);
                break;
            default:
                if (is_array($ModuleDefs[$Data['act_module']]['ACTION_LINK_LIST_FIELDS']))
                {
                    $this->ListRecords($Data, $FormMode);
                }

                break;
        }
    }

    /**
     * Lists incidents actions.
     *
     * @param array $act Incident record information.
     * @param string $FormAction Form mode.
     */
    private function ListIncidents($act, $FormAction)
    {
        $IncidentsActions = array();

        if (CanSeeModule('INC'))
        {
            $sql = '
                SELECT
                    incidents_main.recordid, incidents_main.inc_name, incidents_main.inc_dincident, incidents_main.inc_type,
                    incidents_main.inc_locactual, incidents_main.inc_notes
                FROM
                    incidents_main, ca_actions
                WHERE
                    incidents_main.recordid = ca_actions.act_cas_id
                    AND
                    ca_actions.recordid = :recordid
            ';

            $Where = MakeSecurityWhereClause($Where, 'INC',  $_SESSION['initials']);

            if ($Where)
            {
                $sql .= " AND $Where";
            }

            $sql .= " ORDER BY incidents_main.inc_name";

            $IncidentsActions = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $act['recordid']));
        }

        $this->response->build('src/actions/views/ListIncidents.php', array(
            'IncidentsActions' => $IncidentsActions,
            'print' => $this->request->getParameter('print'),
            'FormAction' => $FormAction
        ));
    }

    /**
     * Lists risks actions.
     *
     * @param array $act Risk record information.
     * @param string $FormAction Form mode.
     */
    private function ListRisks($act, $FormAction)
    {
        $sql = '
            SELECT
                ra_main.recordid, ra_main.ram_name, ra_main.ram_responsible, ra_main.ram_risk_type,
                ra_main.ram_cur_rating, ra_main.ram_after_rating
            FROM
                ra_main, ca_actions
            WHERE
                ra_main.recordid = ca_actions.act_cas_id
                AND
                ca_actions.recordid = :recordid
        ';

        $Where = MakeSecurityWhereClause($Where, 'RAM',  $_SESSION['initials']);

        if ($Where)
        {
            $sql .= " AND $Where";
        }

        $RisksActions = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $act['recordid']));

        foreach ($RisksActions as $ID => $RisksAction)
        {
            $sql = "SELECT cod_descr FROM code_types WHERE cod_code = :cod_code AND cod_type = 'RAMTYPE'";
            $result = \DatixDBQuery::PDO_fetch($sql, array('cod_code' => $RisksAction['ram_risk_type']));
            $RisksActions[$ID]['cod_descr'] = $result['cod_descr'];

            $sql = "SELECT cod_web_colour, description FROM code_inc_grades WHERE code = :code";
            $result = \DatixDBQuery::PDO_fetch($sql, array('code' => $RisksAction['ram_level']));
            $RisksActions[$ID]['colour'] = $result['cod_web_colour'];

            $sql = "SELECT cod_web_colour, description FROM code_inc_grades WHERE code = :code";
            $result = \DatixDBQuery::PDO_fetch($sql, array('code' => $RisksAction['ram_after_level']));
            $RisksActions[$ID]['after_colour'] = $result['cod_web_colour'];
        }

        $this->response->build('src/actions/views/ListRisks.php', array(
            'RisksActions' => $RisksActions,
            'print' => $this->request->getParameter('print'),
            'FormAction' => $FormAction
        ));
    }

    /**
     * Lists safety alerts actions.
     *
     * @param array $act Safety Alert record information.
     * @param string $FormAction Form mode.
     */
    private function ListSABS($act, $FormAction)
    {
        $sql = '
            SELECT
                sabs_main.recordid, sabs_main.sab_handler, sabs_main.updateid, sabs_main.sab_title,
                sabs_main.sab_reference, sabs_main.sab_dopened, sabs_main.sab_action_type, sabs_main.sab_descr
            FROM
                sabs_main, ca_actions
            WHERE
                sabs_main.recordid = ca_actions.act_cas_id
                AND
                ca_actions.recordid = :recordid
        ';

        $Where = MakeSecurityWhereClause($Where, 'SAB',  $_SESSION['initials']);

        if ($Where)
        {
            $sql .= " AND $Where";
        }

        $sql .= " ORDER BY sab_title";

        $SABSActions = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $act['recordid']));

        $this->response->build('src/actions/views/ListSABS.php', array(
            'SABSActions' => $SABSActions,
            'print' => $this->request->getParameter('print'),
            'FormAction' => $FormAction
        ));
    }

    /**
     * Lists equipments actions.
     *
     * @param array $act Equipment record information.
     * @param string $FormAction Form mode.
     */
    private function ListAssets($act, $FormAction)
    {
        $AssetsActions = array();

        if (CanSeeModule('AST'))
        {
            $sql = '
                SELECT
                    assets_main.recordid, ast_name, ast_ourref, ast_dlastservice, ast_dnextservice
                FROM
                    assets_main, ca_actions
                WHERE
                    assets_main.recordid = ca_actions.act_cas_id
                    AND
                    ca_actions.recordid = :recordid
            ';

            $Where = MakeSecurityWhereClause($Where, 'AST',  $_SESSION['initials']);

            if ($Where)
            {
                $sql .= " AND $Where";
            }

            $sql .= " ORDER BY ast_name";

            $AssetsActions = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $act['recordid']));
        }

        $this->response->build('src/actions/views/ListAssets.php', array(
            'AssetsActions' => $AssetsActions,
            'print' => $this->request->getParameter('print'),
            'FormAction' => $FormAction
        ));
    }

    /**
     * Lists standards actions.
     *
     * @param array $act Standard record information.
     * @param string $FormAction Form mode.
     */
    private function ListStandards($act, $FormAction)
    {
        $sql = '
            SELECT
                standards_main.recordid, standards_main.stn_name, standards_main.stn_set, standards_main.stn_domain,
                standards_main.stn_ourref, standards_main.stn_type, standards_main.stn_organisation,
                standards_main.stn_descr
            FROM
                standards_main, ca_actions
            WHERE
                standards_main.recordid = ca_actions.act_cas_id
                AND
                ca_actions.recordid = :recordid
        ';

        $Where = MakeSecurityWhereClause($Where, 'STN',  $_SESSION['initials']);

        if ($Where)
        {
            $sql .= " AND $Where";
        }

        $sql .= " ORDER BY standards_main.stn_name";

        $StandardsActions = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $act['recordid']));

        $this->response->build('src/actions/views/ListStandards.php', array(
            'StandardsActions' => $StandardsActions,
            'print' => $this->request->getParameter('print'),
            'FormAction' => $FormAction
        ));
    }

    /**
     * Lists linked actions.
     *
     * @param array $data Record information.
     * @param string $FormAction Form mode.
     */
    private function ListRecords($data, $FormAction)
    {
        global $scripturl;

        $ModuleDefs = $this->registry->getModuleDefs();

        $readonlyModule = (GetParm($ModuleDefs[$data['act_module']]['PERM_GLOBAL']) == '');

        foreach ($ModuleDefs[$data['act_module']]['ACTION_LINK_LIST_FIELDS'] as $details)
        {
            $FieldList[] = (!empty($ModuleDefs[$data['act_module']]['VIEW']) ? $ModuleDefs[$data['act_module']]['VIEW'] : $ModuleDefs[$data['act_module']]['TABLE']) . '.' . $details['field'];
        }

        $records = array();

        if (CanSeeModule($data['act_module']))
        {
            $sql = '
                SELECT
                    '.implode(', ',$FieldList).'
                FROM
                    ' . (!empty($ModuleDefs[$data['act_module']]['VIEW']) ? $ModuleDefs[$data['act_module']]['VIEW'] : $ModuleDefs[$data['act_module']]['TABLE'])  . ', ca_actions
                WHERE
                    ' . (!empty($ModuleDefs[$data['act_module']]['VIEW']) ? $ModuleDefs[$data['act_module']]['VIEW'] : $ModuleDefs[$data['act_module']]['TABLE']) . '.recordid = ca_actions.act_cas_id
                    AND
                    ca_actions.recordid = :recordid
            ';

            $Where = MakeSecurityWhereClause('', $data['act_module'],  $_SESSION['initials']);

            if ($Where)
            {
                $sql .= " AND $Where";
            }

            $records = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $data['recordid']));
        }

        foreach ($records as $ID => $Record)
        {
            if ($this->request->getParameter('print') != 1 && $FormAction != 'ReadOnly' && !$readonlyModule)
            {
                $records[$ID]['url'] = $scripturl.'?action='.$ModuleDefs[$data['act_module']]['ACTION'].'&recordid='.$Record['recordid'];
            }
        }

        $this->response->build('src/actions/views/ListRecords.php', array(
            'records' => $records,
            'ACT_MODULE_NAME' => $ModuleDefs[$data['act_module']]['NAME'],
            'ACTION_LINK_LIST_FIELDS' => $ModuleDefs[$data['act_module']]['ACTION_LINK_LIST_FIELDS'],
            'data' => $data,
            'ACT_MODULE_TABLE' => $ModuleDefs[$data['act_module']]['TABLE']
        ));
    }

    /**
     * Function lists linked actions for an Actions panel.
     */
    public function ListLinkedActions()
    {
        global $scripturl, $FieldDefs, $ListingDesigns;

        $module = $this->request->getParameter('module');
        $recordid = $this->request->getParameter('recordid');
        $FormType = $this->request->getParameter('FormType');


        $Design = \Listings_ModuleListingDesignFactory::getListingDesign(array(
            'module' => 'ACT',
            'parent_module' => $module,
            'link_type' => 'linked_actions'
        ));
        $Design->LoadColumnsFromDB();

        $ActPerms = GetParm('ACT_PERMS');

        $ACTWhereClause = MakeSecurityWhereClause($ActionWhere, 'ACT', $_SESSION['initials']);

        $listingColumns = $Design->getColumns();

        foreach ($listingColumns as $col_field)
        {
            if (preg_match('/^UDF.*_([0-9]+)$/ui', $col_field, $matches))
            {
                $udf_field_ids[$col_field] = $matches[1];
            }
        }

        foreach ($listingColumns as $key => $column)
        {
            if (!isset($FieldDefs['ACT'][$column]))
            {
                $listingColumns[$key] = 'null as '.$column;
            }
        }

        if (!\UnicodeString::strpos($listingColumns, 'act_ddone'))
        {
            $listingColumns[] = 'act_ddone';
        }
        if (!\UnicodeString::strpos($listingColumns, 'recordid'))
        {
            $listingColumns[] = 'recordid';
        }

        // This is needed for the callback to be included when the action is part of an action chain. This will trigger
        // the complete button to reload the record page when we click on the action complete button making the
        // action chain complete button clicked also.
        if (!\UnicodeString::strpos($listingColumns, 'act_chain_id'))
        {
            $listingColumns[] = 'act_chain_id';
        }

        $Columns = implode(', ',$listingColumns);

        $Actions = array();

        if (CanSeeModule('ACT'))
        {
            // Get actions
            $sql = '
                SELECT
                    ' . $Columns . '
                FROM
                  ca_actions
                WHERE
                  act_module = :module
                  AND
                  act_cas_id = :recordid
                  AND
                  (act_chain_id IS NULL OR (act_dstart <= GETDATE() OR act_ddone IS NOT NULL OR (act_dstart IS NULL AND act_start_after_step IS NULL)))
            ';

            if ($ACTWhereClause)
            {
                $sql.= " AND $ACTWhereClause";
            }

            $sql.= 'ORDER BY act_ddue asc';

            $PDOParams = array(
                'module' => $module,
                'recordid' => $recordid
            );

            $Actions = \DatixDBQuery::PDO_fetch_all($sql, $PDOParams);
        }

        foreach ($Actions as $row)
        {
            $recordids[] = $row['recordid'];
        }

        $udfData = array();

        if (!empty($udf_field_ids))
        {
            $udfData = getUDFData('ACT', $recordids, $udf_field_ids);
        }

        $ShowChangeButtonLinkColumn = false;

        foreach($Actions as $k => $action)
        {
            if ($action['act_ddone'] == '' && $FormType != 'ReadOnly' && $FormType != 'Print')
            {
                $ShowChangeButtonLinkColumn = true;
                $ChangeButtonLink = "$scripturl?action=httprequest&amp;type=changeactionprogress&amp;id=" . $action['recordid'] . "&amp;targettype=field&amp;module=INC&amp;alerttext=" . urlencode($ExpandAlertText);

                // callback URL when completing action chain actions
                $callback = $_SERVER['QUERY_STRING'];

                if (\UnicodeString::strpos($callback, 'panel='))
                {
                    $callback = preg_replace('/panel=[a-z_]+/u', 'panel=linked_actions', $callback);
                }
                else
                {
                    $callback .= '&panel=linked_actions';
                }

                $callback = addslashes($callback);

                $Actions[$k]['change_btn_link'] = '
                <span id="completed_'.$action['recordid'].'" width="100%"><a class="javascript_link" ></a></span>
                <input id="btn_complete_'.$action['recordid'].'" type="button" onclick="
                    var buttons = new Array();
                    buttons[0]={\'value\':\''._tk('btn_apply').'\',\'onclick\':\'completeActionPost('.$action["recordid"].', $(\\\'completed_'.$action["recordid"].'\\\'),  getElementById(\\\'act_progress\\\').value, \\\''.$module.'\\\''.($action['act_chain_id'] != '' ? ', scripturl+\\\'?'.$callback.'\\\'' : '').');document.forms[0].btn_complete_'.$action['recordid'].'.style.visibility=\\\'hidden\\\';GetFloatingDiv(\\\'progress_popup\\\').CloseFloatingControl()\'};
                    buttons[1]={\'value\':\''._tk('btn_cancel').'\',\'onclick\':\'GetFloatingDiv(\\\'progress_popup\\\').CloseFloatingControl();\'};

                    PopupDivFromURL(\'progress_popup\', \'Complete action\', \''.$ChangeButtonLink.'\', \'\', buttons, \'\');"'
                    . ' value="' . _tk('action_complete') . '" />
            ';
            }

            foreach ($udf_field_ids as $udf_field => $udf_id)
            {
                $Actions[$k][$udf_field] = $udfData[$action['recordid']][$udf_id];
            }
        }

        if ($ShowChangeButtonLinkColumn)
        {
            $AdditionalColumns[] = new \Fields_DummyField(array(
                'name' => 'change_btn_link',
                'type' => 'X',
                'label' => _tk('done_date'),
                'width' => '1'
            ));
            $Design->AddAdditionalColumns($AdditionalColumns);
        }

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'ACT';
        $RecordList->AddRecordData($Actions);

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design, '', ($FormType == 'Print' || $FormType == 'ReadOnly'));
        $ListingDisplay->URLParameterArray = array('frommainrecord');
        $ListingDisplay->EmptyMessage = _tk('act_no_actions');

        $this->response->build('src/actions/views/ListLinkedActions.php', array(
            'ListingDisplay' => $ListingDisplay,
            'FormType' => $FormType,
            'module' => $module,
            'recordid' => $recordid
        ));
    }

    public function actioncomplete()
    {
        if ($this->request->getParameter('id'))
        {
            // need to include the form design to check whether or not progress has been set as a timstamp field
            $FormDesign = \Forms_FormDesign::GetFormDesign(array(
                'module' => 'ACT',
                'link_type' => 'linked_actions',
                'level' => 2,
                'parent_module' => $this->request->getParameter('module'),
                'parent_level' => 2
            ));

            if ($FormDesign->TimestampFields['act_progress'])
            {
                $user = $_SESSION['logged_in'] ? \DatixDBQuery::PDO_fetch('SELECT (con_forenames + \' \' + con_surname) AS name FROM contacts_main WHERE initials = \''.$_SESSION['initials'].'\'', array(), \PDO::FETCH_COLUMN) : 'the reporter';
                $date = GetParm('FMT_DATE_WEB') == 'US' ? date('m/d/Y H:i:s') : date('d/m/Y H:i:s');
                $current = \DatixDBQuery::PDO_fetch('SELECT act_progress FROM ca_actions WHERE recordid = :recordid', array('recordid' => $this->request->getParameter('id')), \PDO::FETCH_COLUMN);
                $this->request->setParameter('act_progress', '['.$date.' '.$user.'] ' . $this->request->getParameter('act_progress') . "\r\n" . $current);
            }

            $ACTWhereClause = MakeSecurityWhereClause($ActionWhere, 'ACT', $_SESSION['initials']);

            $sql = '
                UPDATE
                    ca_actions
                SET
                    act_ddone = :act_ddone,
                    act_by_inits = :act_by_inits,
                    act_progress = :act_progress
                WHERE
                    recordid = :recordid
            ';

            if ($ACTWhereClause)
            {
                $sql .= ' AND '.$ACTWhereClause;
            }

            $query = new \DatixDBQuery($sql);
            $query->prepareAndExecute(array(
                'act_ddone' => date('Y-m-d H:i:s.000'),
                'act_by_inits' => $_SESSION['initials'],
                'act_progress' => $this->request->getParameter('act_progress'),
                'recordid' => $this->request->getParameter('id')));
            $rows = $query->getRowsAffected();
        }

        if ($rows == 1)
        {
            if (bYN(GetParm('ACT_EMAIL', 'N')))
            {
                // Get record data to be used on email if designed by the client
                $data = [];

                if ($this->request->getParameter('id'))
                {
                    $sql = 'SELECT * from ca_actions WHERE recordid = :recordid';
                    $data = \DatixDBQuery::PDO_fetch($sql, ['recordid' => $this->request->getParameter('id')]);
                }

                $data = array_change_key_case($data, CASE_LOWER);

                require_once 'Source/actions/ActionEmail.php';
                $response = EmailAction($this->request->getParameter('id'), $data, 'Complete');
            }

            require_once 'Source/actions/LinkedActions.php';
            UpdateRAMActionsOpen(array('action_id' => $this->request->getParameter('id')));

            if (\DatixDBQuery::PDO_fetch('SELECT act_chain_id FROM ca_actions WHERE recordid = :recordid', array('recordid' => $this->request->getParameter('id')), \PDO::FETCH_COLUMN) > 0)
            {
                $acManager = new ActionChainManager();
                $acManager->updateStepDates($this->request->getParameter('id'));
            }

            $this->response->build('src/actions/views/MarkActionAsComplete.php', array(
                'response' => $response
            ));
        }
    }

    public function ChangeActionProgress()
    {
        require_once 'Source/libs/FormClasses.php';

        $parentModule = filter_var($this->request->getParameter('module'), FILTER_SANITIZE_STRING);

        $formDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'ACT',
            'level' => 2,
            'parent_module' => $parentModule,
            'parent_level' => 2
        ));
        $formDesign->LoadFormDesignIntoGlobals();

        $sql = '
            SELECT
                act_progress
            FROM
                ca_actions
            WHERE
                recordid = :id
        ';

        $actProgress = \DatixDBQuery::PDO_fetch($sql, array('id' => $this->request->getParameter('id')), \PDO::FETCH_COLUMN);

        $ValuesSelect = new \FormField();
        $ValuesSelect->MakeTextAreaField('act_progress', 3, 70, '', $actProgress, false, false);

        $this->response->build('src/actions/views/ChangeActionProgress.php', array(
            'ValuesSelect' => $ValuesSelect
        ));
    }
}