<?php

namespace src\actions\controllers;

use src\framework\controller\Controller;

class ActionsSearchController extends Controller
{
    function actionssearch()
    {
        $_SESSION['ACT']['PROMPT']['VALUES'] = array();

        if ($this->request->getParameter('validationerror'))
        {
            $error['message'] = 'There are invalid characters in one of the fields you have searched on.';
        }

        if ($this->request->getParameter('searchtype') == 'lastsearch')
        {
            $data = $_SESSION['ACT']['LAST_SEARCH'];
        }
        else
        {
            $data = '';
        }

        $Parameters = array(
            'FormType' => 'Search',
            'error' => $error
        );

        $_SESSION['ACTIONS']['act'] = $data;

        $this->call('src\actions\controllers\ShowActionFormTemplateController', 'showactionform', $Parameters);
    }
}