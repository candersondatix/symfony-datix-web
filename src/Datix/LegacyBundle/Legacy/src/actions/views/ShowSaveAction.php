<form method="post" name="frmAction1" action="<?php echo $this->scripturl; ?>?action=saveactionactions">
    <input type="hidden" name="form_action" value="" />
    <input type="hidden" name="recordid" value="<?php echo $this->recordid; ?>" />
    <input type="hidden" name="action_whichmodule" value="<?php echo $this->action_whichmodule; ?>" />
    <div class="windowbg2 padded_wrapper"><b>
        <?php echo _tk('action_saved'); ?>. The ID of the action is <?php echo $this->recordid; ?>.
    </b>
    </div>
    <div class="button_wrapper">
        <input type="Submit" value="Go to Actions module" name="btnCancel" onclick="document.frmAction1.form_action.value='<?php echo _tk('btn_cancel')?>'" />
        &nbsp;&nbsp;
        <input type="Submit" value="Go to Actions listing" name="btnListActions" onclick="document.frmAction1.form_action.value='ListActions'" />
        &nbsp;&nbsp;
        <input type="button" value="Back to action" name="btnBacktoAction" onclick="SendTo('app.php?action=action&recordid=<?php echo $this->recordid; ?>');" />
    </div>
</form>