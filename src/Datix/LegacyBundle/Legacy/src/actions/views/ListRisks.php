<li>
    <table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines">
        <tr>
            <td class="titlebg" colspan="6">
                <table width="100%">
                    <tr>
                        <td class="titlebg">
                            <b>Risks</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if (count($this->RisksActions) == 0) : ?>
            <div class="padded_div windowbg2"><b><?php echo _tk('no_records_found'); ?></b></div>
        <?php else: ?>
        <tr>
            <td class="windowbg" width="5%"  align="center"><b>ID</b></td>
            <td class="windowbg" width="5%"  align="center"><b>Lead</b></td>
            <td class="windowbg" width="33%" align="center"><b>Title</b></td>
            <td class="windowbg" width="8%"  align="center"><b>Type</b></td>
            <td class="windowbg" width="8%"  align="center"><b>Current</b></td>
            <td class="windowbg" width="8%"  align="center"><b>Target</b></td>
        </tr>
        <?php foreach ($this->RisksActions as $RisksAction) : ?>
            <?php if($this->print == 1 || $this->FormAction == 'ReadOnly') : ?>
            <tr>
                <td class="windowbg2" valign="middle" align="center"><?php echo $RisksAction['recordid']; ?></td>
                <td class="windowbg2" valign="middle" align="left"><?php echo htmlspecialchars($RisksAction['ram_responsible']); ?></td>
                <td class="windowbg2" valign="middle" align="left"><?php echo htmlspecialchars($RisksAction['ram_name']); ?></td>
                <td class="windowbg2" valign="middle" align="left"><?php echo htmlspecialchars($RisksAction['cod_descr']); ?></td>
                <td valign="middle" align="left"<?php echo (($RisksAction['colour'] == 0) ? "class=\"windowbg2\"" : "bgcolor=\"#".$RisksAction['colour']."\""); ?>><?php echo htmlspecialchars($RisksAction['ram_cur_rating']); ?></td>
                <td valign="middle" align="left"<?php echo (($RisksAction['after_colour'] == 0) ? "class=\"windowbg2\"" : "bgcolor=\"#".$RisksAction['after_colour']."\""); ?>><?php echo htmlspecialchars($RisksAction['ram_after_rating']); ?></td>
            </tr>
            <?php else : ?>
            <tr>
                <td class="windowbg2" valign="middle" align="center"><a href="<?php echo $this->scripturl; ?>?action=risk&amp;table=main&amp;recordid=<?php echo $RisksAction['recordid']; ?>"><?php echo $RisksAction['recordid']; ?></a></td>
                <td class="windowbg2" valign="middle" align="left"><a href="<?php echo $this->scripturl; ?>?action=risk&amp;table=main&amp;recordid=<?php echo $RisksAction['recordid']; ?>"><?php echo htmlspecialchars($RisksAction['ram_responsible']); ?></a></td>
                <td class="windowbg2" valign="middle" align="left"><a href="<?php echo $this->scripturl; ?>?action=risk&amp;table=main&amp;recordid=<?php echo $RisksAction['recordid']; ?>"><?php echo htmlspecialchars($RisksAction['ram_name']); ?></a></td>
                <td class="windowbg2" valign="middle" align="left"><a href="<?php echo $this->scripturl; ?>?action=risk&amp;table=main&amp;recordid=<?php echo $RisksAction['recordid']; ?>"><?php echo htmlspecialchars($RisksAction['cod_descr']); ?></a></td>
                <td valign="middle" align="left" <?php echo (empty($RisksActions['colour'])) ? 'class=\"windowbg2\"' : 'bgcolor=\"#' . $RisksActions['colour'] . '"'; ?>><a href="<?php echo $this->scripturl; ?>?action=risk&amp;table=main&amp;recordid=<?php echo $RisksAction['recordid']; ?>"><?php echo htmlspecialchars($RisksAction['ram_cur_rating']); ?></a></td>
                <td valign="middle" align="left" <?php echo (empty($RisksActions['after_colour'])) ? "class=\"windowbg2\"" : "bgcolor=\"#".$RisksActions['after_colour']."\""; ?>><a href="<?php echo $this->scripturl; ?>?action=risk&amp;table=main&amp;recordid=<?php echo $RisksAction['recordid']; ?>"><?php echo htmlspecialchars($RisksAction['ram_after_rating']); ?></a></td>
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
    </table>
</li>