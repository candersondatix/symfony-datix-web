<li>
    <div class="new_titlebg">
        <b><?php echo _tk('INCNamesTitle'); ?></b>
    </div>
    <?php if (count($this->IncidentsActions) == 0) : ?>
        <div class="padded_div windowbg2"><b><?php echo _tk('no_records_found'); ?></b></div>
    <?php else: ?>
    <div>
        <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
            <tr>
                <td class="windowbg" align="center" width="6%"><b>ID</b></td>
                <td class="windowbg" align="center" width="30%"><b>Name</b></td>
                <td class="windowbg" align="center" width="10%"><b>Date</b></td>
                <td class="windowbg" align="center" width="10%"><b>Location</b></td>
                <td class="windowbg" align="center" width="10%"><b>Type</b></td>
                <td class="windowbg" align="center" width="34%"><b>Description</b></td>
            </tr>
            <?php foreach ($this->IncidentsActions as $IncidentsAction) : ?>
                <?php if ($this->print == 1 || $this->FormAction == 'ReadOnly') : ?>
                <tr>
                    <td class="windowbg2" align="center"><?php echo $IncidentsAction['recordid']; ?></td>
                    <td class="windowbg2"><?php echo htmlspecialchars($IncidentsAction['inc_name']); ?></td>
                    <td class="windowbg2"><?php echo FormatDateVal($IncidentsAction['inc_dincident']); ?></td>
                    <td class="windowbg2"><?php echo Sanitize::htmlentities_once(code_descr('INC', 'inc_locactual', $IncidentsAction['inc_locactual'])); ?></td>
                    <td class="windowbg2"><?php echo Sanitize::htmlentities_once(code_descr('INC', 'inc_type', $IncidentsAction['inc_type'])); ?></td>
                    <td class="windowbg2"><?php echo Sanitize::htmlentities_once($IncidentsAction['inc_notes']); ?></td>
                </tr>
                <?php else : ?>
                <tr>
                    <td class="windowbg2" align="center"><a href="<?php echo $this->scripturl; ?>?action=incident&amp;recordid=<?php echo $IncidentsAction['recordid']; ?>"><?php echo $IncidentsAction['recordid']; ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=incident&amp;recordid=<?php echo $IncidentsAction['recordid']; ?>"><?php echo htmlspecialchars($IncidentsAction['inc_name']); ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=incident&amp;recordid=<?php echo $IncidentsAction['recordid']; ?>"><?php echo FormatDateVal($IncidentsAction['inc_dincident']); ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=incident&amp;recordid=<?php echo $IncidentsAction['recordid']; ?>"><?php echo Sanitize::htmlentities_once(code_descr('INC', 'inc_locactual', $IncidentsAction['inc_locactual'])); ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=incident&amp;recordid=<?php echo $IncidentsAction['recordid']; ?>"><?php echo Sanitize::htmlentities_once(code_descr('INC', 'inc_type', $IncidentsAction['inc_type'])); ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=incident&amp;recordid=<?php echo $IncidentsAction['recordid']; ?>"><?php echo htmlspecialchars($IncidentsAction['inc_notes']); ?></a></td>
                </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
    </div>
    <?php endif; ?>
</li>