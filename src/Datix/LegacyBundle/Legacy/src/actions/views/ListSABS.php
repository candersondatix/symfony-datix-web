<li>
    <table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
        <tr>
            <td class="titlebg" colspan="6">
                <table width="100%">
                    <tr>
                        <td class="titlebg">
                            <b>Safety Alerts</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if (count($this->SABSActions) == 0) : ?>
            <div class="padded_div windowbg2"><b><?php echo _tk('no_records_found'); ?></b></div>
        <?php else: ?>
        <tr>
            <td class="windowbg" align="center" width="6%"><b>ID</b></td>
            <td class="windowbg" align="center" width="10%"><b>Handler</b></td>
            <td class="windowbg" align="center" width="50%"><b>Title</b></td>
            <td class="windowbg" align="center" width="10%"><b>Date</b></td>
            <td class="windowbg" align="center" width="10%"><b>Action type</b></td>
        </tr>
        <?php foreach ($this->SABSActions as $SABSAction) : ?>
            <?php if($this->print == 1 || $this->FormAction == 'ReadOnly') : ?>
            <tr>
                <td class="windowbg2" align="center"><?php echo $SABSAction['recordid']; ?></td>
                <td class="windowbg2"><?php echo Sanitize::htmlentities_once(code_descr('SAB', 'sab_handler', $SABSAction['sab_handler'])); ?></td>
                <td class="windowbg2"><?php echo htmlspecialchars($SABSAction['sab_title']); ?></td>
                <td class="windowbg2"><?php echo FormatDateVal($SABSAction['sab_dopened']); ?></td>
                <td class="windowbg2"><?php echo Sanitize::htmlentities_once(code_descr('SAB', 'sab_action_type', $SABSAction['sab_action_type'])); ?></td>
            </tr>
            <?php else : ?>
                <tr>
                    <td class="windowbg2" align="center"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $SABSAction['recordid']; ?>"><?php echo $SABSAction['recordid']; ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $SABSAction['recordid']; ?>"><?php echo Sanitize::htmlentities_once(code_descr('SAB', 'sab_handler', $SABSAction['sab_handler'])); ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $SABSAction['recordid']; ?>"><?php echo htmlspecialchars($SABSAction['sab_title']); ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $SABSAction['recordid']; ?>"><?php echo FormatDateVal($SABSAction['sab_dopened']); ?></a></td>
                    <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $SABSAction['recordid']; ?>"><?php echo Sanitize::htmlentities_once(code_descr('SAB', 'sab_action_type', $SABSAction['sab_action_type'])); ?></a></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
    </table>
</li>