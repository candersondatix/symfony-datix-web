<li>
    <table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
        <tr>
            <td class="titlebg" colspan="6">
                <table width="100%">
                    <tr>
                        <td class="titlebg">
                            <b>Standards</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if (count($this->StandardsActions) == 0) : ?>
            <div class="padded_div windowbg2"><b><?php echo _tk('no_records_found'); ?></b></div>
        <?php else: ?>
        <tr>
            <td class="windowbg" align="center" width="6%"><b>ID</b></td>
            <td class="windowbg" align="center" width="30%"><b>Title</b></td>
            <td class="windowbg" align="center" width="10%"><b>Origin</b></td>
            <td class="windowbg" align="center" width="10%"><b>Domain</b></td>
            <td class="windowbg" align="center" width="10%"><b>Ref</b></td>
            <td class="windowbg" align="center" width="34%"><b>Description</b></td>
        </tr>
        <?php foreach ($this->StandardsActions as $StandardsAction) : ?>
            <?php if ($this->print == 1 || $this->FormAction == 'ReadOnly') : ?>
            <tr>
                <td class="windowbg2" align="center"><?php echo $StandardsAction['recordid']; ?></td>
                <td class="windowbg2"><?php echo htmlspecialchars($StandardsAction['stn_name']); ?></td>
                <td class="windowbg2"><?php echo htmlentities_once(code_descr('STN', 'stn_set', $StandardsAction['stn_set'])); ?></td>
                <td class="windowbg2"><?php echo htmlentities_once(code_descr('STN', 'stn_domain', $StandardsAction['stn_domain'])); ?></td>
                <td class="windowbg2"><?php echo htmlspecialchars($StandardsAction['stn_ourref']); ?></td>
                <td class="windowbg2"><?php echo htmlspecialchars($StandardsAction['stn_descr']); ?></td>
            </tr>
            <?php else : ?>
            <tr>
                <td class="windowbg2" align="center"><a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $StandardsAction['recordid']; ?>"><?php echo $StandardsAction['recordid']; ?></a></td>
                <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $StandardsAction['recordid']; ?>"><?php echo htmlspecialchars($StandardsAction['stn_name']); ?></a></td>
                <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $StandardsAction['recordid']; ?>"><?php echo Sanitize::htmlentities_once(code_descr('STN', 'stn_set', $StandardsAction['stn_set'])); ?></a></td>
                <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $StandardsAction['recordid']; ?>"><?php echo Sanitize::htmlentities_once(code_descr('STN', 'stn_domain', $StandardsAction['stn_domain'])); ?></a></td>
                <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $StandardsAction['recordid']; ?>"><?php echo htmlspecialchars($StandardsAction['stn_ourref']); ?></a></td>
                <td class="windowbg2"><a href="<?php echo $this->scripturl; ?>?action=standard&amp;recordid=<?php echo $StandardsAction['recordid']; ?>"><?php echo htmlspecialchars($StandardsAction['stn_descr']); ?></a></td>
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
    </table>
</li>