<?php
namespace src\email;

use src\email\EmailTemplateInterface;
use src\framework\model\EntityCollection;
use src\users\model\User;
use src\contacts\model\Contact;
use src\email\EmailFileTemplate;
use src\email\EmailLogger;

//Symfony adaption - CA
if( !class_exists('Swift', false) ) {
    require_once 'thirdpartylibs/SwiftMailer/swift_required.php';
}

/**
 * Class EmailSender
 *
 * Used as a mechanism for constructing Datix email templates and recipient lists and providing them to Swiftmailer.
 */
abstract class EmailSender
{
    /**
     * @var string The module this email is associated with.
     */
    protected $module;

    /**
     * @var string Module to store when saving audit record. e.g. for user registration the email is edited in the INC file, but should be
     * marked as ADM in this history
     */
    protected $moduleToStore;

    /**
     * @var object swiftMailer object that will do the actual sending.
     */
    protected $swiftMailer;

    /**
     * @var string code representing the type of email being sent - used to identify the template to use.
     */
    protected $type;

    /**
     * @var int db recordid of the email template to use.
     */
    protected $templateID;

    /**
     * @var array of contact and user objects representing people to be emailed
     */
    protected $recipients = array();

    /**
     * @var object Emailable object representing source of the email.
     */
    protected $fromUser;

    /**
     * @var object Logger object used to catch SMTP errors
     */
    protected $logger;

    /**
     * @var EmailTemplateInterface template object identifying the content of the email to be sent.
     */
    protected $template;

    /**
     * @var array of information about emails that have successfully been sent.
     */
    protected $successful = array();

    /**
     * @var array of information about emails that have not been sent because their address structure is invalid.
     */
    protected $failedEmailStructureValidation = array();

    /**
     * @var array of information about emails that have not been sent because their domain is invalid.
     */
    protected $failedDomainValidation = array();

    /**
     * @var array of information about emails that have not been sent due to an unknown reason.
     */
    protected $failedOther = array();

    /**
     * Stores data to be constructed into a sql insert
     */
    protected $auditData = array();

    /**
     * @param $module
     * @param string $type
     * @param string $templateID
     *
     * Builds the basic object - requires a module and either a type of email or a specific template id to enable the
     * template contents to be found.
     */
    public function __construct($module, $type = '', $templateID = '')
    {
        $this->templateID = $templateID;
        $this->type = $type;
        $this->module = $module;

        // Create the Transport
        $transport = \Swift_SmtpTransport::newInstance(ini_get('SMTP'), ini_get('smtp_port'));

        // Create the Mailer object using the Transport
        $this->swiftMailer = \Swift_Mailer::newInstance($transport);

        //Register a logger to enable us to track response messages coming from the SMTP server.
        $this->logger = new EmailLogger();
        $this->swiftMailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($this->logger));
    }

    public function getFailedDomainValidation()
    {
        return $this->failedDomainValidation;
    }

    public function getFailedOther()
    {
        return $this->failedOther;
    }

    public function getFailedEmailStructureValidation()
    {
        return $this->failedEmailStructureValidation;
    }

    public function getSuccessful()
    {
        return $this->successful;
    }

    public function setModuleToStore($module)
    {
        $this->moduleToStore = $module;
    }

    protected function getModuleToStore()
    {
        return $this->moduleToStore ?: $this->module;
    }

    /**
     * Sometimes an email will be sent to someone without an associated db record. In this case, a dummy object
     * is used to represent the recipient.
     *
     * @param string $email The emailaddress to be added to the recipient list
     */
    public function addRecipientEmail($email)
    {
        if($email != '')
        {
            $this->recipients[] = (object) array('recordid' => null, 'con_email' => $email);
        }
    }

    /**
     * Adds a user or contact to the list of people to be emailed.
     *
     * @param object $recipient A user or contact object
     * @throws \InvalidParameterException
     */
    public function addRecipient($recipient)
    {
        //TODO: These could implement an "emailable" interface instead
        if(!($recipient instanceof User) && !($recipient instanceof Contact))
        {
            throw new \InvalidParameterException('Object provided is not emailable');
        }

        $this->recipients[] = $recipient;
    }

    /**
     * Adds a group of users or contacts to the list of people to be emailed.
     * The process of creating a lot of objects is so intensive, that we just grab the data.
     *
     * @param object $recipient A user or contact object
     * @throws \InvalidParameterException
     */
    public function addRecipients(EntityCollection $recipientCollection)
    {
        //TODO: These could implement an "emailable" interface instead
        if(!is_a((new User), $recipientCollection->targetClass()) && !is_a((new Contact),$recipientCollection->targetClass()))
        {
            throw new \InvalidParameterException('Object provided is not emailable');
        }

        $this->recipients = array_merge($this->recipients, array_map(function($d){$ao = new \ArrayObject($d); $ao->setFlags(\ArrayObject::ARRAY_AS_PROPS); return $ao;}, $recipientCollection->getData()));
    }

    /**
     * Sets an object to act as the "from" address on an email.
     *
     * @param object $fromUser An object with an email address.
     * @throws \InvalidParameterException
     */
    public function setFromUser($fromUser)
    {
        //TODO: These could implement an "emailable" interface instead
        if ($fromUser !== null && $fromUser->con_email == '')
        {
            throw new \InvalidParameterException('Object provided is not emailable');
        }

        $this->fromUser = $fromUser;
    }

    /**
     * Used if we want to manually pass a template object to the emailSender, rather than using the calculation.
     *
     * @param EmailTemplateInterface $template
     */
    public function setTemplate(EmailTemplateInterface $template)
    {
        $this->template = $template;
    }

    /**
     * Generates a template object based off either the type or ID passed to the constructor.
     * @return EmailTemplateInterface
     * @throws \MissingParameterException
     */
    protected function getTemplate()
    {
        if (isset($this->template))
        {
            return $this->template;
        }

        if (empty($this->templateID))
        {
            switch ($this->type)
            {
                case "Acknowledge":
                    $sShortWhat = "ACK";
                    break;
                case "Notify":
                    $sShortWhat = "NOT";
                    break;
                case "Reminder":
                    $sShortWhat = "REM";
                    break;
                case "Overdue":
                    $sShortWhat = "OVD";
                    break;
                case "UpdatedRecord":
                    $sShortWhat = "UPD";
            }

            $emailID = GetParm("EMT_".$this->module."_".$sShortWhat);
        }
        else
        {
            $emailID = $this->templateID;
        }

        // check identified email actually exists
        $sql = 'SELECT recordid, emt_type FROM email_templates WHERE recordid = :recordid';
        $emailTemplate = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $emailID));

        if($emailID && $emailTemplate!='')
        {
            require_once 'Source/libs/EmailTemplateClass.php';
            require_once 'Source/libs/EmailTemplates.php';
            require_once 'Source/TableDefs.php';

            $template = new \NewEmailTemplate($emailID);

            if ($this->type == '')
            {
                // attempt to automatically determine the type based on the template
                // TODO this logic should be built into the model.  And not the NewEmailTemplate model but the new email template model (which is EmailTemplate).
                $config = include_config_array();
                $this->type = $config[$this->module][$template->TemplateData['emt_type']]['history_map'];
            }
        }
        else
        {
            $template = new EmailFileTemplate($this->module, $this->type);
        }

        $this->template = $template;

        if (!isset($this->template))
        {
            throw new \MissingParameterException('You must provide either a type or a template ID to identify the email text to be sent');
        }

        return $this->template;
    }

    /**
     * Checks all recipient email addresses for validity, logs errors and returns all valid addresses.
     * @return array
     */
    public function getRecipientAddresses()
    {
        require_once 'Source/libs/EmailDomainWhitelistClass.php';
        $whitelist = new \EmailDomainWhitelist();

        $emailList = array();

        foreach ($this->recipients as $recipient)
        {
            $contactEmailAddresses = explode(',', $recipient->con_email);

            foreach ($contactEmailAddresses as $contactEmailAddress)
            {
                $contactEmailAddress = \UnicodeString::trim($contactEmailAddress);

                if(!self::isEmailValid($contactEmailAddress))
                {
                    $this->failedEmailStructureValidation[$contactEmailAddress] = $recipient;
                }
                else if (!$whitelist->isEmailAllowed($contactEmailAddress))
                {
                    $this->failedDomainValidation[$contactEmailAddress] = $recipient;
                }
                else
                {
                    $emailList[] = $contactEmailAddress;
                }
            }
        }

        $emailList = array_unique($emailList);

        return $emailList;
    }

    /**
     * Takes an email address as a string and validates whether it is structured correctly. If not, we don't need to try
     * and send it to know that it will fail.
     *
     * @param string $email
     * @return bool
     */
    public static function isEmailValid($email)
    {
        $isValid = true;
        $atIndex = \UnicodeString::strrpos($email, "@");

        if (is_bool($atIndex) && !$atIndex)
        {
            $isValid = false;
        }
        else
        {
            $domain    = \UnicodeString::substr($email, $atIndex + 1);
            $local     = \UnicodeString::substr($email, 0, $atIndex);
            $localLen  = \UnicodeString::strlen($local);
            $domainLen = \UnicodeString::strlen($domain);

            if ($localLen < 1 || $localLen > 64)
            {
                // local part length exceeded
                $isValid = false;
            }
            elseif ($domainLen < 1 || $domainLen > 255)
            {
                // domain part length exceeded
                $isValid = false;
            }
            else if ($local[0] == '.' || $local[$localLen-1] == '.')
            {
                // local part starts or ends with '.'
                $isValid = false;
            }
            else if (preg_match('/\\.\\./u', $local))
            {
                // local part has two consecutive dots
                $isValid = false;
            }
            else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/u', $domain))
            {
                // character not valid in domain part
                $isValid = false;
            }
            else if (preg_match('/\\.\\./u', $domain))
            {
                // domain part has two consecutive dots
                $isValid = false;
            }
            else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/u', str_replace("\\\\","",$local)))
            {
                // character not valid in local part unless
                // local part is quoted
                if (!preg_match('/^"(\\\\"|[^"])+"$/u', str_replace("\\\\","",$local)))
                {
                    $isValid = false;
                }
            }
        }

        return $isValid;
    }

    /**
     * Gets the emailable object set as the "from" user if set, or defualts to the admin address if not.
     * @return object
     */
    protected function getFromUser()
    {
        if (!isset($this->fromUser))
        {
            $adminEmail = (!empty($_SESSION['Globals']['DIF_ADMIN_EMAIL']) ?
                $_SESSION['Globals']['DIF_ADMIN_EMAIL'] : GetParm('DIF_ADMIN_EMAIL', '', true));

            $this->fromUser = new User();
            $this->fromUser->con_email = $adminEmail;
        }

        return $this->fromUser;
    }

    /**
     * Returns a content-type string for use in an email header.
     * @param EmailTemplateInterface $template
     * @return string
     */
    protected function getContentType(EmailTemplateInterface $template)
    {
        //set the Content-Type header
        if ($template->isHTMLEmail())
        {
            $contentType = 'text/html; charset=utf-8';
        }
        else
        {
            $contentType = 'text/plain; charset=utf-8';
        }

        return $contentType;
    }

    /**
     * Defined in child classes - calculates which audit records to save for any emails sent.
     * @param $data
     * @param EmailTemplateInterface $template
     * @param $failedRecipients
     * @param bool $groupFailed
     * @param string $groupFailedMessage
     * @return mixed
     */
    abstract protected function saveToEmailHistory($data, EmailTemplateInterface $template, $failedRecipients, $groupFailed = false, $groupFailedMessage = '');

    /**
     * Saves an audit record to record a successful or failed email.
     *
     * @param $recipientEmails
     * @param $data
     * @param string $recipientId
     * @param bool $successful
     * @param string $errorMessage
     */
    protected function prepareAuditRecord($recipientEmails, $data, $recipientId = '', $successful = true, $errorMessage = '')
    {
        global $ModuleDefs;

        $this->auditData[] = array(
            'emh_body' => $this->getTemplate()->getBody($data),
            'link_id' => $data['recordid'],
            'mod_id' => $ModuleDefs[$this->getModuleToStore()]['MOD_ID'],
            'emh_email' => implode(', ', $recipientEmails),
            'emh_dsent' => date('Y-m-d H:i:s'),
            'emh_type' => strtoupper($this->type), //Doesn't need to be Unicode safe, since it's just a code.
            'emh_subject' => \UnicodeString::substr($this->getTemplate()->getSubject($data), 0, 254, 'UTF-8'),
            'con_id' => $recipientId,
            'emh_from_id' => $this->getFromUser()->recordid,
            'emh_not_sent' => $successful ? 0 : 1,
            'emh_error' => $errorMessage
        );
    }

    protected function saveAuditRecords()
    {
        $auditQuery = new \DatixDBQuery('INSERT INTO email_history (emh_body, link_id, mod_id, emh_email, emh_dsent, emh_type, emh_subject, con_id, emh_from_id, emh_not_sent, emh_error) VALUES (:emh_body, :link_id, :mod_id, :emh_email, :emh_dsent, :emh_type, :emh_subject, :con_id, :emh_from_id, :emh_not_sent, :emh_error)');
        $auditQuery->prepare();

        foreach ($this->auditData as $auditRecord)
        {
            $auditQuery->execute($auditRecord);
        }
    }
    /**
     * Only ever called in extended classes, but there is some common functionality held here.
     * @param array $data
     * @throws \Exception
     */
    public function sendEmails($data = array())
    {
        // sendmail_from setting in php.ini must be initialized
        // (even with empty string) otherwise mail() fails.
        if (ini_get("sendmail_from") == "")
        {
            ini_set("sendmail_from", $this->getFromUser()->getFirstValidEmail());
        }

        if (!($this->getTemplate() instanceof EmailTemplateInterface))
        {
            throw new \Exception('Error initialising template object.');
        }
    }

}
