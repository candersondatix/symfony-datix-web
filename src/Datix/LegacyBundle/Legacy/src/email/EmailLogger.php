<?php
namespace src\email;

use src\framework\registry\Registry;

//Symfony adaption - CA
if( !class_exists('Swift', false) ) {
    require_once 'thirdpartylibs/SwiftMailer/swift_required.php';
}

/**
 * Class EmailLogger
 *
 * Listens for errors being returned from the Swiftmailer SMTP transport and saves them for future analysis.
 */
class EmailLogger implements \Swift_Plugins_Logger
{
    public $errors;

    /**
     * Called when a message is returned.
     * @param string $entry
     */
    public function add($entry)
    {
        // Split entry into messages
        $messages = explode(PHP_EOL, rtrim($entry));

        foreach ($messages as $message)
        {
            //check to see whether the log is an error
            if (preg_match('/\<\< ([\d]{3}) \<([^\>]*)\>: (.*)/', $message, $matches))
            {
                $this->errors[$matches[2]] = $matches[1].': '.$matches[3];
                // Add a log message
                Registry::getInstance()->getLogger()->logEmergency('[SMTP] '.rtrim($message));
            }
            else
            {
                // Add a log message
                Registry::getInstance()->getLogger()->logEmail('[SMTP] '.rtrim($message));
            }
        }
    }

    public function clear()
    {
        // Not implemented
    }

    public function dump()
    {
        // Not implemented
    }
}