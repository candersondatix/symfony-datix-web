<?php
namespace src\email;

/**
 * Implemented by objects that can be used to construct the contents of emails.
 */
interface EmailTemplateInterface
{
    public function getSubject($data);

    public function getBody($data);

    public function isHTMLEmail();
}