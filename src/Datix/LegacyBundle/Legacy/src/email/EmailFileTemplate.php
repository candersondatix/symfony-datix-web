<?php

namespace src\email;

use src\email\EmailTemplateInterface;

require_once 'Source/libs/Email.php';

/**
 * Class EmailFileTemplate
 *
 * Used to model an email from the old-style email templates contained in php files.
 */
class EmailFileTemplate implements EmailTemplateInterface
{
    /**
     * @var string The three-letter code for the module this template belongs to
     */
    protected $module;

    /**
     * @var string The type of email being sent - acts as a key in the user-defined template file
     */
    protected $type;

    /**
     * @var array Used to cache the email template file by module and type.
     */
    protected $templateCache = [];


    public function __construct($module, $type)
    {
        $this->module = $module;
        $this->type = $type;
    }

    public function getSubject($data)
    {
        // This global is used inside the email template file to create links to the record.
        global $scripturl;
        
        //TODO: This is really inefficient (adds >1 second for 1000+ emails)
        //need to format date values for display
        foreach (GetAllFieldsByType($this->module, 'date') as $field)
        {
            if ($data[$field] != '')
            {
                // Workaround to convert dates to SQL format
                // TODO: This needs to be removed when refactored
                $data[$field] = UserDateToSQLDate($data[$field]);

                $data[$field] = FormatDateVal($data[$field]);
            }
        }

        //$inc is used instead of $data in some older files.
        $inc = $data;

        if (!$this->templateCache['EmailText_'.$this->module.'_'.$this->type])
        {
            require 'Source/EmailText_' . \Sanitize::getModule($this->module) . '.php';

            if ($EmailTextFile = getUserEmailTextFile($this->module))
            {
                require($EmailTextFile);
            }

            $this->templateCache['EmailText_'.$this->module.'_'.$this->type] = $EmailText;
        }
        else
        {
            $EmailText = $this->templateCache['EmailText_'.$this->module.'_'.$this->type];
        }

        return $EmailText[$this->type]["Subject"];
    }

    public function getBody($data)
    {
        // This global is used inside the email template file to create links to the record.
        global $scripturl;

        //TODO: This is really inefficient (adds >1 second for 1000+ emails)
        //need to format date values for display
        foreach (GetAllFieldsByType($this->module, 'date') as $field)
        {
            if ($data[$field] != '')
            {
                // Workaround to convert dates to SQL format
                // TODO: This needs to be removed when refactored
                $data[$field] = UserDateToSQLDate($data[$field]);

                $data[$field] = FormatDateVal($data[$field]);
            }
        }

        //$inc is used instead of $data in some older files.
        $inc = $data;

        if (!$this->templateCache['EmailText_'.$this->module.'_'.$this->type])
        {
            require 'Source/EmailText_' . \Sanitize::getModule($this->module) . '.php';

            if ($EmailTextFile = getUserEmailTextFile($this->module))
            {
                require($EmailTextFile);
            }

            $this->templateCache['EmailText_'.$this->module.'_'.$this->type] = $EmailText;
        }
        else
        {
            $EmailText = $this->templateCache['EmailText_'.$this->module.'_'.$this->type];
        }

        return $EmailText[$this->type]["Body"];
    }

    public function isHTMLEmail()
    {
        require 'Source/EmailText_' . \Sanitize::getModule($this->module) . '.php';

        if ($EmailTextFile = getUserEmailTextFile($this->module))
        {
            require($EmailTextFile);
        }

        return ($EmailText[$this->type]['HTML'] || $EmailText[$this->type]['html']);
    }

}