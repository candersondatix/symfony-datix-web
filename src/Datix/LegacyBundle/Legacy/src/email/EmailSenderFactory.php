<?php
namespace src\email;
use src\email\EmailSender;

class EmailSenderFactory
{
    /**
     * Constructs an EmailSender-derived object based on the INDIVIDUAL_EMAILS global
     * @param $module
     * @param string $type
     * @param string $templateID
     * @return EmailSender
     */
    public static function createEmailSender($module, $type = '', $templateID = '')
    {
        switch(GetParm('INDIVIDUAL_EMAILS', 'Y'))
        {
            case 'Y':
                return new EmailSenderIndividual($module, $type, $templateID);
                break;
            case 'N':
                return new EmailSenderGroupCC($module, $type, $templateID);
                break;
            case 'B':
                return new EmailSenderGroupBCC($module, $type, $templateID);
                break;
        }
    }
}