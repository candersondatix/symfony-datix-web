<?php

namespace src\email\controllers;

use src\framework\controller\Controller;
use src\incidents\model\IncidentModelFactory;
use src\users\model\UserModelFactory;

class NotificationEmailController extends Controller
{
    /**
     * Processes and sends notification e-mails for a single record.
     */
    public function sendEmails()
    {
        require_once 'Source/libs/Email.php';
        
        $data = (new IncidentModelFactory)->getMapper()->find($this->get('recordid'));
        
        if (null === $data)
        {
            $this->registry->getLogger()->logEmergency('Unable to process notifcation e-mails for recordid '.
                $this->get('recordid').': record not found.');
            
            return;
        }
        
        $usersAlreadyEmailed = (new UserModelFactory)->getMapper()->getNotificationEmailRecipients($this->get('recordid'));
        
        SendEmails([
            'module'  => 'INC',
            'data'    => $data,
            'from'    => $data['rep_approved_old'],
            'to'      => $data['rep_approved'],
            'perms'   => $this->get('perms') ?: 'NONE',
            'level'   => $this->get('formlevel'),
            'notList' => $usersAlreadyEmailed,
            'emailType' => $this->get('emailtype')
        ]);
    }
}