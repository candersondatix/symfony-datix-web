<?php

namespace src\email\controllers;

use src\framework\controller\Controller;

class EmailHistoryController extends Controller
{
    /**
     * Displays the Notifications panel.
     */
    function MakeEmailHistoryPanel()
    {
        $module = $this->request->getParameter('module');
        $linkId = $this->request->getParameter('recordid');
        $data   = array();

        // Check only notification emails for now
        $sql = '
            SELECT
                emh_body, emh_email, emh_dsent, emh_type, emh_from_id, con_id, con_email, con_surname, con_forenames, con_title, con_jobtitle, con_tel1
            FROM
                email_history
            LEFT JOIN
                contacts_main ON email_history.con_id = contacts_main.recordid
            WHERE
                mod_id = :mod_id AND
                emh_type = :emh_type AND
                link_id = :link_id
            ORDER BY
                emh_dsent DESC
        ';

        $emh = \DatixDBQuery::PDO_fetch_all($sql, [
            'mod_id'   => GetModIDFromShortName($module),
            'emh_type' => 'NOTIFY',
            'link_id'  => $linkId
        ]);

        if ($emh)
        {
            foreach ($emh as $history)
            {
                if ($history['con_id'] != '')
                {
                    if ($history['con_surname'] || $history['con_forenames'] || $history['con_title'])
                    {
                        $recipient_name = $history['con_surname'].", ".$history['con_forenames']." ".$history['con_title'];
                    }
                    else
                    {
                        $recipient_name = 'No details found for the contact with ID '.$history['con_id'].'.';
                    }
                }
                else
                {
                    $recipient_name = 'No contact recorded';
                }

                $data[] = [
                    'recipientName' => $recipient_name,
                    'emailAddress'  => $history['emh_email'],
                    'emailDateTime' => FormatDateVal($history['emh_dsent'], true),
                    'contactId'     => $history['con_id'],
                    'telNumber'     => $history['con_tel1'],
                    'jobTitle'      => $history['con_jobtitle'],
                ];
            }
        }

        $this->response->build('src/email/views/EmailHistoryPanel.php', array(
            'data' => $data
        ));
    }
}