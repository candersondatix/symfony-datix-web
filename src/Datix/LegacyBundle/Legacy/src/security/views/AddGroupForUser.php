<form enctype="multipart/form-data" method="post" name="frmUserGroup" action="<?php echo $this->scripturl; ?>?action=save_user_group_link">
    <?php if ($this->Action == 'add_group_for_user') : ?>
    <input type="hidden" name="con_id" value="<?php echo $this->Link['con_id']; ?>" />
    <?php elseif ($this->Action == 'add_group_for_profile') : ?>
    <input type="hidden" name="pfl_id" value="<?php echo $this->Link['pfl_id']; ?>" />
    <?php else : ?>
    <input type="hidden" name="grp_id" value="<?php echo $this->Link['grp_id']; ?>" />
    <?php endif; ?>
    <input type="hidden" name="action" value="<?php echo $this->Action; ?>" />
    <input type="hidden" name="rbWhat" value="save" />
    <?php if ($this->Action == 'add_group_for_user' || $this->Action == 'add_group_for_profile') : ?>
    <?php SectionAddGroup($this->Link); ?>
    <?php else : ?>
    <?php SectionAddUser($this->Link); ?>
    <?php endif; ?>
    <div class="button_wrapper">
        <input type="submit" value="<?php echo _tk('btn_add')?>" onclick="document.frmUserGroup.rbWhat.value='save';selectAllMultiCodes();" />
        <input type="submit" value="<?php echo _tk('btn_cancel')?>" onclick="document.frmUserGroup.rbWhat.value='cancel';" />
    </div>
</form>