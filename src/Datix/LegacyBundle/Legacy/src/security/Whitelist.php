<?php

namespace src\security;

/**
 * Collects together methods of filtering user input.
 */

class Whitelist
{
    /**
     * Should return $string if it is a valid module code. For now, this will just sanitize it.
     *
     * @param string $string
     */
    public static function module($string)
    {
        return \Sanitize::SanitizeString($string);
    }
}