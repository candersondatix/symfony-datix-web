<?php
namespace src\security\groups\model;

use src\framework\model\EntityCollection;

class GroupCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\security\\groups\\model\\Group';
    }
}