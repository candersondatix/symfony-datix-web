<?php
namespace src\security\groups\model;

use src\framework\model\ModelFactory;

class GroupModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new GroupFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new GroupMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new GroupCollection($this->getMapper(), $this->getEntityFactory());
    }
}