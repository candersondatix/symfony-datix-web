<?php

namespace src\security\controllers;

use src\framework\controller\TemplateController;

class AddGroupTemplateController extends TemplateController
{
    function add_group_for_user()
    {
        require_once 'Source/security/SecurityUserGroupLinks.php';

        $Link['grp_id'] = \Sanitize::SanitizeInt($this->request->getParameter('grp_id'));
        $Link['con_id'] = \Sanitize::SanitizeInt($this->request->getParameter('con_id'));
        $Link['pfl_id'] = \Sanitize::SanitizeInt($this->request->getParameter('pfl_id'));
        $Action = \Sanitize::SanitizeString($this->request->getParameter('action'));

        if (empty($Link['con_id']))
        {
            $Link['con_id'] = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        }
        
        
        $this->title = 'Add security group';
        $this->module = 'ADM';

        $this->response->build('src/security/views/AddGroupForUser.php', array(
            'Action' => $Action,
            'Link' => $Link
        ));
    }

    function add_group_for_profile()
    {
        require_once 'Source/security/SecurityUserGroupLinks.php';

        $Link['grp_id'] = \Sanitize::SanitizeInt($this->request->getParameter('grp_id'));
        $Link['con_id'] = \Sanitize::SanitizeInt($this->request->getParameter('con_id'));
        $Link['pfl_id'] = \Sanitize::SanitizeInt($this->request->getParameter('pfl_id'));
        $Action = \Sanitize::SanitizeString($this->request->getParameter('action'));

        $this->title = 'Add security group';
        $this->module = 'ADM';

        $this->response->build('src/security/views/AddGroupForUser.php', array(
            'Action' => $Action,
            'Link' => $Link
        ));
    }

    function add_user_to_group()
    {
        require_once 'Source/security/SecurityUserGroupLinks.php';

        $Link['grp_id'] = \Sanitize::SanitizeInt($this->request->getParameter('grp_id'));
        $Link['con_id'] = \Sanitize::SanitizeInt($this->request->getParameter('con_id'));
        $Link['pfl_id'] = \Sanitize::SanitizeInt($this->request->getParameter('pfl_id'));
        $Action = \Sanitize::SanitizeString($this->request->getParameter('action'));

        $this->title = 'Add user';
        $this->module = 'ADM';

        $this->response->build('src/security/views/AddGroupForUser.php', array(
            'Action' => $Action,
            'Link' => $Link
        ));
    }
}