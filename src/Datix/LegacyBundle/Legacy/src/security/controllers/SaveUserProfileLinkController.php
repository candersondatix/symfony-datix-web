<?php
namespace src\security\controllers;

use src\framework\controller\Controller;

class SaveUserProfileLinkController extends Controller
{
    /**
     * Attaches users to a profile
     */
    function save_user_profile_link()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $Link = \Sanitize::SanitizeRawArray($this->request->getParameters());

        if ($Link['rbWhat'] == 'cancel')
        {
            $this->redirect('app.php?action=editprofile&recordid='.$Link['pfl_id']);
        }
        else
        {
            if (!$Link['con_id'])
            {
                AddSessionMessage('ERROR', 'Please select the users you wish to add to this profile.');
                if($Link['pfl_id']) {
                    //Return user to linked profile's record via id
                    $this->redirect('app.php?action=add_user_to_profile&pfl_id=' . $Link['pfl_id']);
                } else{
                    //Return user to new profile reocrd
                    $this->redirect('app.php?action=add_user_to_profile');
                }
            }

            require_once 'Source/libs/Licence.php';
            $licence = New \licence();

            require_once 'Source/security/Profiles.php';
            $ProfileGroups = GetProfileGroups($Link['pfl_id']);

            if (!empty($ProfileGroups))
            {
                require_once 'Source/security/SecurityGroups.php';

                foreach ($ProfileGroups as $GroupID)
                {
                    $GroupModules = GetGroupModules($GroupID);

                    if (!empty($GroupModules))
                    {
                        // Foreach Module Perm
                        foreach ($GroupModules as $Mod)
                        {
                            // Check if limit reached already
                            if ($licence->checkUserLimitExceeded($Mod, $Link['con_id']))
                            {
                                AddSessionMessage('ERROR', $ModuleDefs[$Mod]['NAME']. ': ' . _tk('user_limit_exceeded_err'));
                                $Error = true;
                            }
                        }

                        if ($Error)
                        {
                            $this->redirect('app.php?action=add_user_to_profile');
                        }
                    }
                }
            }

            foreach ($Link['con_id'] as $ConID)
            {
                $sql = '
                    UPDATE
                        contacts_main
                    SET
                        sta_profile = :sta_profile
                    WHERE
                        recordid = :recordid
                ';

                \DatixDBQuery::PDO_query($sql, array('sta_profile' => $Link['pfl_id'], 'recordid' => $ConID));

                if ($this->request->getParameter('clearUserSettings'))
                {
                    require_once 'Source/security/SecurityUsers.php';

                    $sta = array();
                    $sta['recordid'] = $ConID;
                    $sta['login'] = \DatixDBQuery::PDO_fetch('SELECT login FROM contacts_main WHERE recordid = :recordid',
                        $sta,
                        \PDO::FETCH_COLUMN
                    );

                    // Delete configuration parameters from the main page
                    deleteConfigurationParameters($sta);
                    deleteSecurityGroups($sta);
                    deleteLocTypeSettings($sta);
                }
            }
        }

        $this->redirect('app.php?action=editprofile&recordid='.$Link['pfl_id']);
    }
}