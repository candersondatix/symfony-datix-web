<?php
namespace src\organisations\model;

use src\framework\model\Mapper;
use src\framework\model\Entity;
use src\framework\query\Query;

class OrganisationMapper extends Mapper
{
    /**
     * @codeCoverageIgnore
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\organisations\\model\\Organisation';
    }

    /**
     * @codeCoverageIgnore
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'organisations_main';
    }

    /**
     * Returns organisation data based on the record id.
     *
     * @param string $recordId The id of the organisation.
     *
     * @return array
     */
    public function findOrganisationByID($recordId)
    {
        $sql = '
            SELECT
                *
            FROM
              organisations_main
            WHERE
              recordid = :recordid
        ';

        return $this->db->PDO_fetch($sql, ['recordid' => $recordId]);
    }

    /**
     * Function that gets the suffixes for linked organisations in level 1 claims forms.
     *
     * @param array  $data The data entered on the form.
     * @param string $type The type of organisation.
     *
     * @return array
     */
    public function getPostedOrganisations($data, $type = '')
    {
        $totalOrganisations = $data['organisation_max_suffix'] + 1;
        $type = 'organisations_type_'.$type;

        for ($i = 100; $i < $totalOrganisations; $i++ )
        {
            if ($type == $data['organisation_div_name_'.$i] || $type == '')
            {
                $organisationSuffixes[] = $i;
            }
        }

        return $organisationSuffixes;
    }

    /**
     * Returns organisations that were already linked as respondents.
     *
     * @param string $mainRecordId The id of the claim.
     *
     * @return array The ids of the organisations that were already linked.
     */
    public function getDisallowedOrganisations($mainRecordId)
    {
        $sql = 'SELECT org_id FROM link_respondents WHERE main_recordid = :main_recordid AND link_type = \'ORG\'';

        return $this->db->PDO_fetch_all($sql, ['main_recordid' => $mainRecordId], \PDO::FETCH_COLUMN);
    }
}
