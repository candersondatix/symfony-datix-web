<?php
namespace src\organisations\model;

use src\framework\model\RecordEntity;

/**
 * @codeCoverageIgnore
 */
class Organisation extends RecordEntity
{
    protected $rep_approved;

    protected $org_name;

    protected $org_address;

    protected $org_postcode;

    protected $org_tel1;

    protected $org_tel2;

    protected $org_email;

    protected $org_organisation;

    protected $org_unit;

    protected $org_clingroup;

    protected $org_directorate;

    protected $org_specialty;

    protected $org_loctype;

    protected $org_locactual;

    protected $org_notes;

    protected $org_reference;
}