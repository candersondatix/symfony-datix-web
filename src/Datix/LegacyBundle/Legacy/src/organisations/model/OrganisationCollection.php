<?php
namespace src\organisations\model;

use src\framework\model\EntityCollection;

class OrganisationCollection extends EntityCollection
{
    /**
     * @codeCoverageIgnore
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\organisations\\model\\Organisation';
    }
}