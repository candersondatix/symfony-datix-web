<?php
namespace src\organisations\model;

use src\framework\model\EntityFactory;

/**
 * @codeCoverageIgnore
 */
class OrganisationFactory extends EntityFactory
{
    public function targetClass()
    {
        return 'src\\organisations\\model\\Organisation';
    }
}