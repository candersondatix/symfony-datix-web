<?php

namespace src\organisations\controllers;

use src\respondents\model\RespondentModelFactory;
use src\organisations\model\OrganisationModelFactory;
use src\framework\controller\TemplateController;

class OrganisationsTemplateController extends TemplateController
{
    public function EditOrganisationLink ()
    {
        $data = $this->request->getParameters();

        // get recordid from matching organisation
        if (!empty($data['from_match'])) {
            $data['recordid'] = \Sanitize::SanitizeInt($data['match_link_id']);
            $this->request->setParameter('recordid', $data['match_link_id']);
        }

        $recordid      = \Sanitize::SanitizeInt(!empty($data['from_match']) ? $data['match_link_id'] : $data['recordid']);
        $main_recordid = \Sanitize::SanitizeInt($data['main_recordid']);
        $link_recordid = \Sanitize::SanitizeInt($data['link_recordid']);
        $level         = $data['level'] ?: 2;
        $module        = \Sanitize::getModule($data['module']);
        $ModuleDefs    = $this->registry->getModuleDefs();

        $this->module = 'ORG';
        $this->title  = _tk('cla_organisation_respondent_title');
      
        // check user access level
        if (!empty($ModuleDefs[$this->module]['CUSTOM_ACCESS_LEVEL_FUNCTION']))
        {
            $accessFlag = $ModuleDefs[$this->module]['CUSTOM_ACCESS_LEVEL_FUNCTION']($this->module, $recordid);
        }
        else
        {
            if (bYN($this->registry->getParm('CASCADE_PERMISSIONS', 'N')))
            {
                // Give full permissions for the purposes of linking.
                $Perms = 'ORG_FULL';
            }
            else if (bYN($this->registry->getParm('USE_ADVANCED_ACCESS_LEVELS','N')))
            {
                require_once 'Source/security/SecurityBase.php';
                $Perms = GetUserHighestAccessLvlForRecord($ModuleDefs[$this->module]['PERM_GLOBAL'], $this->module, $recordid);
            }
            else
            {
                $Perms = GetParm($ModuleDefs[$this->module]['PERM_GLOBAL']);
            }

            // URL for linking callback
            $_SESSION['ORG']['LINK']["URL"] = "?action=editorganisationlink&recordid={$recordid}".
                "&main_recordid={$main_recordid}&link_recordid={$link_recordid}".
                "&module=CLA&from_parent_record=1";

            // Access based on DB table link_access_approvalstatus
            $accessFlag = GetAccessFlag($this->module, $Perms, $data['rep_approved']);
        }


        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $accessFlag != 'R')
        {
            $Error = $this->validateInput ($data);

            if ($Error || $data['validationerror'])
            {
                // there's validation errors, show form again
                $this->showEditForm('Edit', $data);
            }
            else
            {
                // save respondent
                $organisationFactory = new OrganisationModelFactory();
                $linkFactory         = new RespondentModelFactory();

                $organisation = $organisationFactory->getEntityFactory()->createFromRequest($this->request);
                $link         = $linkFactory->getEntityFactory()->createFromRequest($this->request);

                $organisationFactory->getMapper()->save($organisation);

                $link->recordid       = $link_recordid;
                $link->main_recordid  = $main_recordid;
                $link->link_type      = 'ORG';
                $link->org_id         = $organisation->recordid;

                $linkFactory->getMapper()->save($link);

                require_once "Source/libs/UDF.php";
                SaveUDFs ($organisation->recordid, MOD_ORGANISATIONS);

                AddSessionMessage('INFO', _tk('linked_ORG_saved'));

                // redirect back to claims
                $this->redirect('app.php?action=record&module='.$module.'&fromsearch=1&panel=respondents&recordid='.$main_recordid);
            }
        }
        else
        {
            if (empty($recordid))
            {
                // new
                $defaults    = array();
                $this->title = _tk('new_organisation_defaults');
                $FormType    = 'New';
            }
            else
            {
                // edit
                $defaults    = (new OrganisationModelFactory)->getMapper()->find($recordid)->getVars();
                $this->title = _tk('edit_organisation_defaults');
                $FormType    = ($accessFlag == 'R') ? 'ReadOnly' : 'Edit';

                $link = (new RespondentModelFactory)->getMapper()->find($link_recordid);
                $defaults['link_notes'] = $link->link_notes;
                $defaults['link_resp']  = $link->link_resp;
                $defaults['link_role']  = $link->link_role;

                if ($data['from_match'])
                {
                    // get previous link data
                    foreach ($_SESSION['ORG']['LAST_SEARCH'] as $field => $value)
                    {
                        // name starts with link_?
                        if (strpos($field, 'link_') === 0 && empty($data[$field]))
                        {
                            $defaults[$field] = $value;
                        }
                    }

                    unset ($_SESSION['ORG']['LAST_SEARCH']);
                }

                $defaults['indemnity_reserve_assigned'] = $link->indemnity_reserve_assigned;
                $defaults['expenses_reserve_assigned']  = $link->expenses_reserve_assigned;
                $defaults['remaining_indemnity_reserve_assigned'] = $link->remaining_indemnity_reserve_assigned;
                $defaults['remaining_expenses_reserve_assigned']  = $link->remaining_expenses_reserve_assigned;
                $defaults['resp_total_paid']  = $link->resp_total_paid;
            }

            $this->showEditForm($FormType, $defaults);
        }
    }

    protected function createEditFormTable($FormType, $level, $defaults)
    {
        global $JSFunctions;
       
        $design = array('module' => 'ORG', 'form_type' => $FormType, 'level' => $level, 
            'parent_module' => 'CLA', 'link_type' => 'G');

        $FormDesign = \Forms_FormDesign::GetFormDesign($design);
        $FormDesign->LoadFormDesignIntoGlobals();
        
        // the following values are referenced within BasicForm2.php
        $link_recordid   = $this->request->getParameter('link_recordid');
        $ShowLinkDetails = true;
        
        require 'Source/generic_modules/ORG/BasicForm2.php';

        $JSFunctions[] = MakeJavaScriptValidation('ORG', $FormDesign);

        $formTable = new \FormTable($FormType, 'ORG', $FormDesign);
        $formTable->MakeForm($FormArray, $defaults, "ORG");
        $formTable->MakeTable();

        return $formTable;
    }

    public function validateInput ($params)
    {
        // save
        $organisationFactory = new OrganisationModelFactory();
        $linkFactory = new RespondentModelFactory();

        $Error = false;

        if (!empty($params['recordid'])) {
            $record = $organisationFactory->getMapper()->find($params['recordid']);
            if (empty($record)) {
                AddSessionMessage('ERROR', _tk('invalid_organisation'));
                $Error = true;
            }
        }

        // check if the link exists
        if (!empty($params['link_recordid'])) {
            $record = $linkFactory->getMapper()->find($params['link_recordid']);
            if (empty($record)) {
                AddSessionMessage('ERROR', 'Invalid link');
                $Error = true;
            }
        }

        return $Error;
    }

    public function showEditForm($FormType, $defaults)
    {
        $this->addJs('src\respondents\js\respondents.js');

        $params = $this->request->getParameters();
        $level  = $params['level'] ?: 2;
        $module = \Sanitize::getModule($this->request->getParameter('module'));

        $formTable = $this->createEditFormTable($FormType, $level, $defaults);

        $buttonGroup = new \ButtonGroup();

        if ($FormType == 'New')
        {
            $buttonGroup->AddButton(array(
                'label' => _tk('check_matching_organisations'),
                'id' => 'btnCheck',
                'name' => 'btnCheck',
                'onclick' => 'MatchExistingOrganisations();',
                'action' => 'SEARCH'
            ));
        }

        if ($FormType != 'ReadOnly' && ($params['from_match'] || $FormType == 'Edit'))
        {
            $buttonGroup->AddButton(array(
                'id' => 'btnSaveDesign',
                'name' => 'btnSaveDesign',
                'label' => ($FormType == 'Edit') ? 'Save' : 'Create new link',
                'onclick' => "if(validateOnSubmit()){ jQuery('#orglinkform').submit(); }",
                'action' => 'SAVE'
            ));
        }

        if ($FormType == 'Edit')
        {
            $buttonGroup->AddButton(array(
                'id' => 'btnUnlink',
                'name' => 'btnUnlink',
                'label' => _tk('unlink_organisation'),
                'onclick' => "unlinkRespondent('".$params['main_recordid']."', '".$params['link_recordid']."', '".$module."', 'organisation');",
                'action' => 'DELETE'
            ));
        }

        $buttonGroup->AddButton(array(
            'label'   => ($FormType == 'Edit') ? _tk('btn_back_to_record') :_tk('btn_export'),
            'id'      => ($FormType == 'Edit') ? 'btnBack' : 'btnCancel',
            'name'    => ($FormType == 'Edit') ? 'btnBack' : 'btnCancel',
            'onclick' => "if(confirm('Press \\'OK\\' to confirm or \\'Cancel\\' to return to the form')){ SendTo(scripturl+'?action=record&module=".$module."&recordid={$params['main_recordid']}&panel=respondents'); }",
            'action'  => ($FormType == 'Edit') ? 'BACK' : 'CANCEL'
        ));

        // This is used to create the floating bar on the organisations edit form
        $this->menuParameters = array(
            'table'    => $formTable,
            'buttons'  => $buttonGroup,
            'recordid' => $params['recordid'],
            'no_audit' => true
        );

        //Only admin users can unlink contacts, so the same rule is put in place here.
        $canUnlinkRespondent = IsFullAdmin();

        $this->response->build('src/organisations/views/EditRespondent.php', array(
            'recordid'              => $params['recordid'],
            'main_recordid'         => $params['main_recordid'],
            'link_recordid'         => $params['link_recordid'],
            'formTable'             => $formTable,
            'FormType'              => $FormType,
            'cancel_url'            => '?action=record'.(strpos($_SERVER['QUERY_STRING'], 'from_parent_record=1') ? '&fromsearch=1' : '').'&module='.$module.'&recordid='.$params['main_recordid'].'&panel=respondents',
            'module'                => $module,
            'canUnlinkRespondent'   => $canUnlinkRespondent
        ));
    }
}
