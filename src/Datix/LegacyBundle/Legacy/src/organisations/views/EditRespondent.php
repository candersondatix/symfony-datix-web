<script type="text/javascript" language="javascript">
    <?php echo MakeJavaScriptValidation(); ?>
    var submitClicked = true;

    function MatchExistingOrganisations()
    {
        var f = document.orglinkform;

        // keep old value
        var old_url = f.action;

        // change form and submit
        f.rbWhat.value = 'Link';
        f.module.value = 'ORG';
        f.action = '<?=$this->scripturl?>?action=doselection&link=1&token=<?=CSRFGuard::getCurrentToken()?>';
        f.target = "wndMatchExisting";
        f.onsubmit = "var child = window.open('', 'wndMatchExisting', 'dependent,menubar=false,screenX=200,screenY=330,titlebar,scrollbars,resizable');";
        f.submit();

        // restore
        f.target = "";
        f.action = old_url;
        f.rbWhat.value = 'Save';
        f.module.value = 'CLA';
    }

</script>
<form method="POST" id="orglinkform" name="orglinkform" action="<?php echo $this->scripturl ?>?action=editorganisationlink">
    <input type="hidden" name="main_recordid" value="<?=$this->main_recordid?>" />
    <input type="hidden" name="link_recordid" value="<?=$this->link_recordid?>" />
    <input type="hidden" name="module" value="<?=$this->module?>" />
    <input type="hidden" name="rbWhat" value="Save" />
    <?php echo $this->formTable->GetFormTable(); ?>
</form>
<div class="button_wrapper">
    <?php if ($this->FormType != 'ReadOnly' && $this->FormType != 'Print'): ?>
        <?php if (!$this->link_recordid): ?>
        <input class="button" type="button" id="btnMatch" value="<?=_tk('check_matching_organisations')?>" onclick="MatchExistingOrganisations();">
        <?php endif; ?>
        <?php if ($this->link_recordid || $this->FormType == 'Edit'): ?>
        <input class="button" type="button" id="btnSave" value="<?=$this->recordid ? _tk('btn_save') : _tk('create_new_link') ?>" onclick="if(validateOnSubmit()){ jQuery('#orglinkform').submit(); }">
        <?php endif; ?>
        <?php if ($this->link_recordid && $this->canUnlinkRespondent): ?>
        <input class="button" type="button" value="<?=_tk('unlink_organisation')?>" onclick="unlinkRespondent('<?php echo $this->main_recordid; ?>', '<?php echo $this->link_recordid; ?>', '<?php echo $this->module; ?>', 'organisation');">
        <?php endif; ?>
    <?php endif; ?>
    <input class="button" type="button" value="<?=$this->main_recordid ? _tk('btn_back_to_record') : _tk('btn_cancel') ?>" onclick="if(confirm('<?=addslashes(_tk('confirm_or_cancel'))?>')){ SendTo(scripturl+'<?= addslashes($this->cancel_url) ?>'); }">
    <?php echo JavascriptPanelSelect(count($this->formTable->PanelArray) < 2 ? 'Y' : 'N', null, $this->formTable); ?>
</div>