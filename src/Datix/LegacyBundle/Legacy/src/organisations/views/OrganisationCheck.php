<div id="organisationlist_title">
    <span style="cursor: default;">
        <font size="3"><b>&nbsp;<?php echo _tk('matching_organisations'); ?></b></font>
        <span style="cursor: pointer;" class="edit_options" onclick="OrganisationSelectionCtrl.destroy();">[x]</span>
    </span>
</div>
<div id="organisationlist_container">
    <table id="organisationlist_table" class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
        <tr class="tableHeader">
            <th class="titlebg" width="6%" align="center"><b><?php echo _tk('choose'); ?></b></th>
            <?php foreach ($this->listingColumns as $column) : ?>
                <?php $defaultLabel = $this->FieldDefs['ORG'][$column->getName()]['Title']; ?>
                <th class="titlebg" align="left"><b><?php echo GetColumnLabel($column->getName(), $defaultLabel); ?></b></th>
            <?php endforeach; ?>
        </tr>
        <?php if (count($this->resultArray) > 0) : ?>
            <?php echo $this->organisationRows; ?>
        <?php else : ?>
            <tr>
                <td class="windowbg2" colspan="4" align="left"><b><?php echo _tk('no_matching_organisations'); ?></b></td>
            </tr>
        <?php endif; ?>
    </table>
</div>