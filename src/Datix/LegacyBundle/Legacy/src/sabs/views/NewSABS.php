<script language="javascript" type="text/javascript">
    var submitClicked = false;
    <?php if($this->FormType != 'Search') : ?>
    AlertAfterChange = true;
    <?php endif; ?>
    <?php echo MakeJavaScriptValidation('SAB', $this->FormDesign); ?>
    function CheckContactSelected()
    {
        for(var i = 0, l = document.forms[0].length; i < l; i++)
        {
            if (document.forms[0].elements[i].checked)
            {
                return true;
            }
        }

        return false;
    }
</script>
<form enctype="multipart/form-data" method="post" id="frmSabsSAB1" name="frmSabsSAB1" action="<?php echo $this->scripturl; ?>?action=<?php echo $this->FormAction; ?>" onsubmit="<?php echo $this->onsubmit; ?>">
    <input type="hidden" name="recordid" value="<?php echo $this->sab['recordid']; ?>" />
    <input type="hidden" id="form-panel" name="panel" value="<?php echo ($this->panel ?: ''); ?>" />
    <input type="hidden" name="fromsearch" value="<?php echo (is_numeric($this->fromsearch) ? (int) $this->fromsearch : ''); ?>" />
    <input type="hidden" name="fromlisting" value="<?php echo Escape::EscapeEntities($this->fromlisting); ?>" />
    <input type="hidden" name="from_report" value="<?php echo Escape::EscapeEntities($this->from_report); ?>" />
    <input type="hidden" name="sabrep_recordid" value="<?php echo $this->sab['sabrep_recordid']; ?>" />
    <input type="hidden" name="updateid" value="<?php echo $this->sab['updateid']; ?>" />
    <input type="hidden" name="rsp_id" value="<?php echo $this->sab['rsp_id']; ?>" />
    <input type="hidden" name="currentpanel" value="" />
    <input type="hidden" name="session_form" value="<?php echo $this->session_form; ?>" />
    <input type="hidden" name="session_date" value="<?php echo $this->lastsession; ?>" />
    <input type="hidden" name="rbWhat" value="Save"/>
    <?php if ($this->sab['error']['message']) : ?>
    <div class="form_error"><?php echo $this->sab['error']['message']; ?></div>
    <?php endif; ?>
    <?php if($this->sLockMessage) : ?>
    <div class="lock_message">
        <div id="LockMessage"><?php echo $this->sLockMessage; ?></div>
        <div id="userLockMessage"></div>
    </div>
    <?php endif; ?>
    <?php echo $this->SABTable->GetFormTable(); ?>
    <?php echo $this->buttonGroup->getHTML(); ?>
</form>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect($GLOBALS['Show_all_section'], null, $this->SABTable); ?>
<?php endif; ?>
<?php
if ($_SESSION['SAB']['WARNING_MESSAGE']  != '')
{
    CreateWarningDlg($_SESSION['SAB']['WARNING_MESSAGE'], 'left');
    $_SESSION['SAB']['WARNING_MESSAGE'] = '';
}
?>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect($this->FormDesign->Show_all_section, $this->panel, $this->SABTable); ?>
<?php endif; ?>