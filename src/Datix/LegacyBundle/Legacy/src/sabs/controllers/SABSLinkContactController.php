<?php

namespace src\sabs\controllers;

use src\framework\controller\Controller;

class SABSLinkContactController extends Controller
{
    function sabslinkcontact()
    {
        require_once 'Source/contacts/SABSContactForm.php';

        $ModuleDefs = $this->registry->getModuleDefs();

        $contact_fields = implode(',', $ModuleDefs['CON']['FIELD_ARRAY_SELECT']);

        if ($this->request->getParameter('link_action') == 'Cancel')
        {
            ReshowContactLink();
        }

        // Check if this function has been called by a POST from the matching function
        if ($this->request->getMethod() == 'POST' && $this->request->getParameter('con_id') != '')
        {
            $sab_id = $this->request->getParameter('sab_id');

            $sql = '
                SELECT
                    recordid AS con_id,
                    rep_approved,
                    updateid,
                    ' . $contact_fields . '
                FROM
                    contacts_main
                WHERE
                    recordid = :con_id
            ';

            $con = \DatixDBQuery::PDO_fetch($sql, array('con_id' => $this->request->getParameter('con_id')));

            $con = array_merge(\Sanitize::SanitizeRawArray($this->request->getParameters()), $con);
        }
        // A pre-existing linked contact
        elseif ($this->request->getMethod() == 'GET' && $this->request->getParameter('con_id') != '')
        {
            $sab_id = $this->request->getParameter('sab_id');

            $sql = '
                SELECT
                    recordid,
                    recordid AS con_id,
                    rep_approved,
                    updateid,
                    ' . $contact_fields . '
                FROM
                    contacts_main
                WHERE
                    recordid = :con_id
            ';

            $con = \DatixDBQuery::PDO_fetch($sql, array('con_id' => $this->request->getParameter('con_id')));

            // Check for existing link
            $LinkOwnerId = $_SESSION['contact_login_id'];

            $sql = '
                SELECT
                    link_role, link_type, link_status, link_notes, link_comments,
                    link_rsp_type, link_rsp_date, link_read_ackn, link_read_date, link_datetime
			    FROM
			        sab_link_contacts
			    WHERE
			        con_id = :con_id
			        AND
			        sab_id = :sab_id
            ';

            $PDOParams = array(
                'con_id' => $this->request->getParameter('con_id'),
                'sab_id' => $this->request->getParameter('sab_id')
            );

            if ($_SESSION['Globals']['SAB_PERMS'] != 'SAB2')
            {
                $sql .= ' AND link_owner_id = :link_owner_id';
                $PDOParams['link_owner_id'] = $LinkOwnerId;
            }
            else
            {
                $sql .= ' AND link_owner_id IS NULL';
            }

            $row = \DatixDBQuery::PDO_fetch($sql, $PDOParams);

            if (!empty($row))
            {
                $con = array_merge($con, $row);
                $con['link_exists'] = true;
            }

        }
        elseif ($this->request->getParameter('sabrep_recordid') != '')
        {
            $sab_id = \Sanitize::SanitizeInt($this->request->getParameter('sab_id'));

            $sql = '
                SELECT
                    con_title, con_forenames, con_surname,
                    con_type, con_gender, con_ethnicity, con_language,
                    con_address,
                    con_postcode, con_number, con_nhsno, con_tel1, con_tel2,
                    con_fax, con_dob, con_dod, con_jobtitle, link_role, link_type, link_notes,
                    link_treatment, link_injury1, link_bodypart1
                FROM
                    rep_contacts
                WHERE
                    recordid = :sabrep_recordid
            ';

            $con = \DatixDBQuery::PDO_fetch($sql, array('sabrep_recordid' => $this->request->getParameter('sabrep_recordid')));
            $con['sabrep_recordid'] = \Sanitize::SanitizeInt($this->request->getParameter('sabrep_recordid'));
        }
        else
        {
            $sab_id = \Sanitize::SanitizeInt($this->request->getParameter('sab_id'));
        }

        ShowContactForm($con, $sab_id);
    }
}