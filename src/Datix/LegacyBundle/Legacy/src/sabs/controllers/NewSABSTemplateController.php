<?php

namespace src\sabs\controllers;

use src\framework\controller\TemplateController;

class NewSABSTemplateController extends TemplateController
{
    /**
     * Shows the SAB1 or SAB2 form.
     */
    function addnewsabs()
    {
        global $scripturl, $FormType;

        if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && $this->request->getParameter('action') == 'sabs' && ($FormType != "Print" && $this->request->getParameter('print') != 1))
        {
            $this->addJs('src/generic/js/panelData.js');
        }

        if($FormType == "Print" || $this->request->getParameter('print') == 1)
        {
            $this->addJs('src/generic/js/print.js');
        }

        $FormType = $this->request->getParameter('FormType');
        $sab = $this->request->getParameter('PrePopulatedData') ?: array();

        $SABPerms = $_SESSION['Globals']['SAB_PERMS'];

        if (!$SABPerms)
        {
            throw new \PermissionDeniedException('You do not have permissions to access this module.');
        }

        if ($FormType != 'ReadOnly' && $FormType != 'Print' && $FormType != 'Search' && $FormType != 'Design')
        {
            if ($SABPerms == 'SAB2')
            {
                if ($sab['recordid'] && bYN(GetParm('RECORD_LOCKING', 'N')))
                {
                    require_once 'Source/libs/RecordLocks.php';

                    $aReturn = LockRecord(array(
                        'link_id' => $sab['recordid'],
                        'table' => 'SABS_MAIN'
                    ));

                    $sLockMessage = $aReturn["lock_message"];

                    if ($aReturn['formtype'])
                    {
                        $FormType = $aReturn['formtype'];
                    }
                }
            }
            else
            {
                // SAB1 forms are always "Locked"
                $FormType = 'Locked';
            }
        }

        if (($SABPerms == 'SAB1' || $SABPerms == '') && $FormType != 'Search')
        {
            if (!$sab['link_read_date'] && $sab['rsp_id'] != '')
            {
                if (GetParm('FMT_DATE_WEB') == 'US')
                {
                    $sab['link_read_date'] = date('m/d/Y');
                }
                else
                {
                    $sab['link_read_date'] = date('d/m/Y');
                }

                $sql = '
                    UPDATE
                        sab_link_contacts
                    SET
                        link_read_date = :link_read_date
                    WHERE
                        recordid = :recordid';

                \DatixDBQuery::PDO_query($sql, array('link_read_date' => date('Y-m-d 00:00:00.000'), 'recordid' => $sab['rsp_id']));
            }
        }

        if ($this->request->getParameter('print') == 1)
        {
            $FormType = 'Print';
        }
        //this logic isn't ideal, but can't think of a better way to identify whether the incident in question has been saved yet.
        elseif (!$FormType && ($this->request->getParameter('action') == 'addnewsabs' || (!$sab['recordid'] && !empty($sab['error']))))
        {
            $FormType = 'New';
        }

        if ($sab['sabrep_recordid'] || $sab['recordid'] )
        {
            $LinkCondition = true;
            $PrintView = "window.open('{$scripturl}?action=sabs&recordid={$sab["recordid"]}&print=1&token=". \CSRFGuard::getCurrentToken() ."')";
        }
        else
        {
            $LinkCondition = false;
        }

        $Level = ($SABPerms == 'SAB2' ? 2 : 1);

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'SAB',
            'level' => $this->request->getParameter('action') == 'sabssearch' ? 1 : $Level,
            'form_type' => $FormType
        ));

        // If the opened date is empty and this is a new record,
        // set it to today's date
        if ($sab['sab_dopened'] == '' && !$sab['recordid'] && $FormType != 'Search')
        {
            $sab['sab_dopened'] = date('Y-m-d H:i:s.000');
        }

        if ($sab['recordid'] && $_SESSION['contact_login_id'] && $SABPerms != 'SAB2' && $sab['rsp_id'])
        {
            $bShowSaveButton = true;
        }

        if ($FormDesign->FormTitle == '')
        {
            $title = 'Datix ' . _tk('SABSNameTitle');
        }
        else
        {
            $title = $FormDesign->FormTitle;
        }

        if ($sab['recordid'] != '')
        {
            $title .= ' - Reference ' . $sab['sab_reference'];
        }

        $this->title = $title . (($FormType == 'Search') ? _tk('search_for_records') : '');
        $this->subtitle = $FormDesign->FormTitleDescr;
        $this->module = 'SAB';

        include 'Source/sabs/BasicFormSAB.php';

        $SABTable = new \FormTable($FormType, 'SAB', $FormDesign);
        $SABTable->MakeForm($FormArray, $sab, 'SAB');

        $this->sendToJs(array(
                'FormType' => $FormType,
                'printSections' => $SABTable->printSections)
        );

        $buttonGroup = new \ButtonGroup();

        if ($FormType != 'Print')
        {
            // If this is a completely new record being added
            if ($sab['recordid'] == '' && $sab['sabrep_recordid'] == '' && $FormType != 'Search')
            {
                $buttonGroup->AddButton(array(
                    'id' => 'btnSubmitSABS',
                    'name' => 'btnSubmitSABS',
                    'label' => _tk('btn_save'),
                    'onclick' => 'submitClicked = true; selectAllMultiCodes(); if (validateOnSubmit()){document.frmSabsSAB1.submit();}',
                    'action' => 'SAVE'
                ));

                $buttonGroup->AddButton(array(
                    'id' => 'btnCancel',
                    'name' => 'btnCancel',
                    'label' => _tk('btn_cancel'),
                    'onclick' => getConfirmCancelJavascript('frmSabsSAB1'),
                    'action' => 'CANCEL'
                ));
            }
            else // Not a new record
            {
                if ($FormType == 'Search')
                {
                    $buttonGroup->AddButton(array(
                        'id' => 'btnSearch',
                        'name' => 'btnSearch',
                        'label' => ((isset($_SESSION['security_group']['grp_id']) || isset($_SESSION['packaged_report']['rep_id'])) ? 'Continue' : _tk('btn_search')),
                        'onclick' => 'document.frmSabsSAB1.rbWhat.value = \'Search\';submitClicked = true;document.frmSabsSAB1.submit();',
                        'action' => 'SEARCH'
                    ));
                }
                else // Editing a record
                {
                    if ($SABPerms == 'SAB2' && $FormType != 'ReadOnly' && $FormType != 'Locked' || $bShowSaveButton)
                    {
                        $buttonGroup->AddButton(array(
                            'id' => 'btnSaveSABS',
                            'name' => 'btnSaveSABS',
                            'label' => _tk('btn_save'),
                            'onclick' => 'submitClicked = true; document.frmSabsSAB1.rbWhat.value=\'Save\';selectAllMultiCodes(); if (validateOnSubmit()){document.frmSabsSAB1.submit();}',
                            'action' => 'SAVE'
                        ));
                    }
                }

                if (!in_array($FormType, array('ReadOnly', 'Locked', 'Search')))
                {
                    $buttonGroup->AddButton(array(
                        'id' => 'btnCancel',
                        'name' => 'btnCancel',
                        'label' => _tk('btn_cancel'),
                        'onclick' => getConfirmCancelJavascript('frmSabsSAB1'),
                        'action' => 'CANCEL'
                    ));
                }
                else
                {
                    $buttonGroup->AddButton(array(
                        'id' => 'btnCancel',
                        'name' => 'btnCancel',
                        'label' => _tk('btn_cancel'),
                        'onclick' => 'document.frmSabsSAB1.rbWhat.value = \'Cancel\';submitClicked = true;document.frmSabsSAB1.submit();',
                        'action' => 'CANCEL'
                    ));
                }

                if ($this->request->getParameter('fromlisting'))
                {
                    $buttonGroup->AddButton(array(
                        'id' => 'btnBack',
                        'name' => 'btnBack',
                        'label' => _tk('btn_back_to_report'),
                        'onclick' => 'submitClicked=true;selectAllMultiCodes();document.frmSabsSAB1.rbWhat.value=\'BackToListing\';document.frmSabsSAB1.submit();',
                        'action' => 'BACK'
                    ));
                }
            }
        }

        if ($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) &&!empty($_SESSION['SAB']['RECORDLIST']) && (!isset($ModuleDefs['SAB']['NO_NAV_ARROWS']) || $ModuleDefs['SAB']['NO_NAV_ARROWS'] != true))
        {
            $CurrentIndex = $_SESSION['SAB']['RECORDLIST']->getRecordIndex(array('recordid' => $sab['recordid']));

            if ($CurrentIndex !== false)
            {
                $buttonGroup->AddNavigationButtons($_SESSION['SAB']['RECORDLIST'], $sab['recordid'], 'SAB');
            }
        }

        if ($FormType != 'Print')
        {
            $this->menuParameters = array(
                'table' => $SABTable,
                'buttons' => $buttonGroup,
                'no_audit' => true
            );
        }
        else
        {
            $this->hasMenu = false;
        }

        if ($FormType == 'Search')
        {
            $FormAction = 'sabsdoselection';
        }
        else
        {
            $FormAction = 'savesabs';
        }

        $onsubmit = 'return(submitClicked';

        if ($FormType != 'Print' && $FormType != 'Search' && $FormType != 'ReadOnly')
        {
            $onsubmit.= ' &amp;&amp; validateOnSubmit()';
            SetFormSession($session_form, $lastsession);
        }

        $onsubmit.= ')';

        $SABTable->MakeTable();

        $this->response->build('src/sabs/views/NewSABS.php', array(
            'FormType' => $FormType,
            'FormDesign' => $FormDesign,
            'FormAction' => $FormAction,
            'onsubmit' => $onsubmit,
            'sab' => $sab,
            'fromlisting' => $this->request->getParameter('fromlisting'),
        	'from_report' => $this->request->getParameter('from_report'),
        	'fromsearch' => $this->request->getParameter('fromsearch'),
            'session_form' => $session_form,
            'lastsession' => $lastsession,
            'sLockMessage' => $sLockMessage,
            'SABTable' => $SABTable,
            'buttonGroup' => $buttonGroup,
            'panel' => $this->request->getParameter('panel')
        ));
    }
}