<?php

namespace src\sabs\controllers;

use src\framework\controller\Controller;

class SaveSABSController extends Controller
{
    function savesabs()
    {
        global $scripturl;

        $ModuleDefs = $this->registry->getModuleDefs();

        $Data = $this->request->getParameters();

        $SABPerms = $_SESSION['Globals']['SAB_PERMS'];

        if (!is_array($Data) || empty($Data))
        {
            $form_action = 'Cancel';
        }
        else
        {
            $form_action = $Data['rbWhat'];
            $sab = \Sanitize::SanitizeRawArray($Data);
            $recordid = \Sanitize::SanitizeInt($Data['recordid']);
        }

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $recordid)
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'SABS_MAIN', 'link_id' => $recordid));
        }

        switch ($form_action)
        {
            case 'Cancel':
            	if ($Data['fromsearch'])
            	{
            		$this->redirect('app.php?action=list&module=SAB&table=main&listtype=search');
            	}
            	elseif ($Data['from_report'])
                {
                    $this->redirect('app.php?action=list&module=SAB&table=main&listtype=search&from_report=1');
                }
                else 
                {
                	$this->redirect('app.php?action=list&module=SAB&table=main');
                }
                break;
            case 'Email':
                require_once 'Source/sabs/SABSEmail.php';
                $EmailsSent = EmailSABSAlert(\Sanitize::SanitizeRawArray($Data));
                ShowEmailedSABS($sab, $Warning, $EmailsSent);
                break;
            case 'BatchUnlink':
                require_once 'Source/sabs/SaveSABS.php';
                ContactBatchUnlink($sab);
                $this->redirect('app.php?action=sabs&recordid='.$recordid.'&panel='.$Data['currentpanel']);
                return;
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');
                break;
        }

        if (($Data['session_date'] != $_SESSION['lastsession'][$Data['session_form']]) && !empty($_SESSION['lastsession']))
        {
            $this->call('src\sabs\controllers\NewSABSTemplateController', 'addnewsabs', array());
            return;
        }

        $sab_issued_date = '\''.UserDateToSQLDate($Data['sab_issued_date']).'\'';
        $sab_dstart_due = '\''.UserDateToSQLDate($Data['sab_dstart_due']).'\'';
        $sab_dend_due = '\''.UserDateToSQLDate($Data['sab_dend_due']).'\'';
        $sab_dstart = '\''.UserDateToSQLDate($Data['sab_dstart']).'\'';
        $sab_dend = '\''.UserDateToSQLDate($Data['sab_dend']).'\'';
        $sab_read_by_due = '\''.UserDateToSQLDate($Data['sab_read_by_due']).'\'';
        $sab_dopened = '\''.UserDateToSQLDate($Data['sab_dopened']).'\'';
        $sab_dclosed = '\''.UserDateToSQLDate($Data['sab_dclosed']).'\'';

        $sab['sab_cost'] = str_replace(',', '', $sab['sab_cost']);
        $sab['sab_cost'] = str_replace('£', '', $sab['sab_cost']);
        $sab['sab_cost'] = str_replace('$', '', $sab['sab_cost']);

        if ($sab['sab_cost'] == '')
        {
            $sab['sab_cost'] = 'NULL';
        }

        GetSectionVisibility('SAB', 2, $_POST);
        BlankOutPostValues('SAB', 2, null, null, $this->request);

        //--------Validation----------------//
        $ValidationErrors = $this->ValidateSABSData();

        if ($ValidationErrors)
        {
            AddSessionMessage('ERROR', _tk('form_errors'));
            $error['Validation'] = $ValidationErrors;
        }
        //-----------------------------------//

        if ($error)
        {
            $sab = array();

            foreach ($Data as $key => $value)
            {
                $sab[$key] = \Sanitize::SanitizeRaw($value);
            }

            $sab['error'] = $error;

            if ($HoldingForm)
            {
                require_once 'Source/sabs/SABSForm.php';
                ShowHoldingForm($sab);
            }
            else
            {
                $this->call('src\sabs\controllers\NewSABSTemplateController', 'addnewsabs', array(
                    'FormType' => 'Main',
                    'PrePopulatedData' => $sab,
                ));
                return;
            }

            obExit();
        }

        if (!$recordid)
        {
            $newRecord = true;
            $recordid = GetNextRecordID('sabs_main', true);
            ResetFormSession();
        }
        else
        {
            $newRecord = false;
            DoFullAudit('SAB', 'sabs_main', $recordid);
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'SAB',
            'level' => ($_SESSION['Globals']['SAB_PERMS'] == 'SAB2' ? 2 : 1)
        ));
        $FormDesign->LoadFormDesignIntoGlobals();

        $sab = ParseSaveData(array('module' => 'SAB', 'data' => $sab));

        $sql = "UPDATE sabs_main SET ";

        $sql .= GeneratePDOSQLFromArrays(array(
                'FieldArray' => $ModuleDefs['SAB']['FIELD_ARRAY'],
                'DataArray' => $sab,
                'end_comma' => true,
                'Module' => 'SAB'
            ),
            $PDOParamsArray);

        // If SAB1 user, we don't want to set a new updateid or check for an updated updateid,
        // since this might cause problems if users are using the record at the same time.
        if ($SABPerms == 'SAB2')
        {
            $sql .= " updateid = '" . GensUpdateID($Data['updateid']) . "',";
        }

        $sql .= " updateddate = '" . date('d-M-Y H:i:s') . "',
              updatedby = '". $_SESSION['initials']."'
        WHERE recordid = :recordid";

        $PDOParamsArray['recordid'] = $recordid;

        if ($SABPerms == 'SAB2')
        {
            $sql .= " AND (updateid = :updateid OR updateid IS NULL)";
            $PDOParamsArray['updateid'] = $Data['updateid'];
        }

        $SaveSABQuery = new \DatixDBQuery($sql);
        $result = $SaveSABQuery->prepareAndExecute($PDOParamsArray);

        if (!$result)
        {
            $error = "An error has occurred when trying to save the record.  Please report the following to the Datix administrator: $sql";
        }
        elseif ($SaveSABQuery->PDOStatement->rowCount() == 0)
        {
            $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=sabs&recordid='.$recordid.'">here</a> to return to the record';
        }

        if (GetParm('SAB_PERMS') == 'SAB1')
        {
            // Save response
            $rsp_recordid = \Sanitize::SanitizeInt($Data['rsp_id']);

            if (!$rsp_recordid)
            {
                $rsp_recordid = GetNextRecordID('sab_link_contacts', true);
            }

            if ($rsp_recordid > 0)
            {
                $sql = '
                    UPDATE
                        sab_link_contacts
                    SET
                        sab_id = :sab_id,
                        con_id = :con_id,
                        link_read_ackn = :link_read_ackn,
                        link_rsp_type = :link_rsp_type,
                        link_rsp_date = '.($sab['link_rsp_date'] ? '\''.$sab['link_rsp_date'].'\'' : 'NULL').',
                        link_comments = :link_comments,
                        link_datetime = \''.date('d-M-Y H:i:s').'\',
                        updatedby = :updatedby,
                        updateddate = \''.date('d-M-Y H:i:s').'\',
                        updateid = \''.GensUpdateID($Data['updateid']).'\'
                    WHERE
                        recordid = :recordid
                ';

                $PDOParamsArray = array(
                    'sab_id' => $Data['recordid'],
                    'con_id' => $_SESSION['contact_login_id'],
                    'link_read_ackn' => $sab['link_read_ackn'],
                    'link_rsp_type' => $sab['link_rsp_type'],
                    'link_comments' => $sab['link_comments'],
                    'updatedby' => $_SESSION['initials'],
                    'recordid' => $rsp_recordid,
                );

                $SaveSABQuery = new \DatixDBQuery($sql);
                $result = $SaveSABQuery->prepareAndExecute($PDOParamsArray);

                if (!$result)
                {
                    $error = "An error has occurred when trying to save the record.  Please report the following to the Datix administrator: $sql";
                }
                elseif ($SaveSABQuery->PDOStatement->rowCount() == 0)
                {
                    $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=sabs&recordid='.$rsp_recordid.'">here</a> to return to the record';
                }
            }
        }

        if ($error != '')
        {
            SaveError($error);
        }

        $sab['recordid'] = $recordid;

        if ($recordid)
        {
            require_once 'Source/libs/UDF.php';
            SaveUDFs($recordid, MOD_SABS);

            require_once 'Source/libs/notepad.php';
            $error = SaveNotes(array(
                'id_field' => 'sab_id',
                'id' => $recordid,
                'notes' => $sab['notes'],
                'new' => $newRecord
            ));

            // Save Progress Notes
            $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
                'module' => 'SAB',
                'data'   => $sab
            ));
        }

        $_SESSION['sab_number'] = $sab['recordid'];

        $recordurl = $scripturl . "?action=" . $ModuleDefs['SAB']['ACTION'] . "&recordid=" . htmlspecialchars($recordid);

        if ($this->request->getParameter('panel'))
        {
            $recordurl .= '&panel=' . $this->request->getParameter('panel');
        }

        $status_message = _tk('SABS_2_record_saved');

        AddSessionMessage('INFO', $status_message);

        if (bYN(GetParm("WEB_TRIGGERS","N")))
        {
            require_once 'Source/libs/Triggers.php';
            ExecuteTriggers($sab['recordid'], $sab, $ModuleDefs['SAB']["MOD_ID"]);
        }

        $this->redirect($recordurl);
    }

    function ValidateSABSData()
    {
        $errorDates = ValidatePostedDates('SAB');
        $errorMoney = ValidatePostedMoney('SAB');

        $error = array_merge($errorDates, $errorMoney);

        $cost = $_POST['sab_cost'];
        $cost = str_replace(',', '', $cost);
        $cost = str_replace('£', '', $cost);
        $cost = str_replace('$', '', $cost);
        $cost = str_replace('.', '', $cost);
        $cost = str_replace(',', '', $cost);

        if ($cost == '')
        {
            $cost = 'NULL';
        }

        // act_cost - MS SQL decimal(13,2);
        if (\UnicodeString::strlen($cost) > 13)
        {
            $error['message'] = '<br/>' . GetFormFieldLabel('sab_cost','','SAB').' cannot be longer than 13 digits.';
            $error['Validation']['sab_cost'] = GetFormFieldLabel('sab_cost','','SAB').' cannot be longer than 13 digits.';
        }

        return $error;
    }
}