<?php

namespace src\sabs\controllers;

use src\framework\controller\Controller;

class ShowMainSABSController extends Controller
{
    /**
     * Shows a Safety Alert record.
     */
    function sabs()
    {
        global $sab, $link_contacts, $investigation, $FormArray, $FormTitleDescr, $FormTitle, $dtxdebug;


        $sab_num = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $ShowSAB1Values = $this->request->getParameter('show_sab1_values');

        $SabsForm = $_SESSION['Globals']['SABS_FORM'];
        $SABPerms = $_SESSION['Globals']['SAB_PERMS'];

        $sql = "
            SELECT
                recordid, updateid, sab_title, sab_handler, sab_reference,
                sab_source, sab_type, sab_subtype,sab_issued_date,sab_descr,
                sab_action_type,sab_action_descr,sab_dstart_due,
                sab_dend_due,sab_dstart,sab_dend,
                sab_read_by_due,sab_dopened,sab_dclosed, sab_cost, sab_cost_notes,
                sab_organisation, sab_unit, sab_clingroup, sab_directorate,
                sab_specialty, sab_loctype, sab_locactual, updatedby
		    FROM
		        sabs_main
        ";

        $WhereClause = MakeSecurityWhereClause("recordid = :recordid", 'SAB', $_SESSION['initials']);
        $sql .= " WHERE $WhereClause";

        $sab = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $sab_num));

        if (!$sab || !$SABPerms)
        {
            $msg = 'You do not have the necessary permissions to view Safety Alert ID ' . $sab_num . '.';

            if ($dtxdebug)
            {
                $msg .= "\n$sql";
            }

            fatal_error($msg, 'Information');
        }

        $sqlnotepad = "
            SELECT
                notes
            FROM
                notepad
            WHERE
                sab_id = :sab_id
        ";

        $sabnotes = \DatixDBQuery::PDO_fetch($sqlnotepad, array('sab_id' => $sab_num));

        $sab['notes'] = $sabnotes['notes'];

        $sql = "
            SELECT
                fullname
            FROM
                staff
		    WHERE
		        initials = :initials
        ";

        $row = \DatixDBQuery::PDO_fetch($sql, array('initials' => $sab['sab_handler']));

        if ($row)
        {
            $sab['sab_mgr_fullname'] = $row['fullname'];
        }

        // Documents
        $sql = "
            SELECT
                recordid,
                doc_notes,
                doc_dcreated,
                doc_type
            FROM
                documents_main
            WHERE
                documents_main.sab_id = :sab_id
            ORDER BY
                doc_dcreated DESC
        ";

        $request = \DatixDBQuery::PDO_fetch_all($sql, array('sab_id' => $sab_num));

        foreach ($request as $document)
        {
            $doc[''][] = $document;
        }

        // Web links
        $sql = "
            SELECT
                recordid AS doc_id,
                wlk_descr ,
                UPDATEDDATE as doc_dcreated,
                wlk_web_link
            FROM
                web_links
            WHERE
                web_links.link_id = :link_id
                AND
                web_links.link_mod = 'SAB'
            ORDER BY
                UPDATEDDATE DESC
        ";

        $request = \DatixDBQuery::PDO_fetch_all($sql, array('link_id' => $sab_num));

        foreach ($request as $document)
        {
            $doc[''][] = $document;
        }

        // SAB_LINK_CONTACTS
        $sql = "
            SELECT
                sab_link_contacts.recordid AS rsp_id,
                sab_link_contacts.sab_id AS sab_id,
                sab_link_contacts.con_id as con_id,
                link_comments,
                link_rsp_type,
                link_rsp_date,
                link_read_ackn,
                link_read_date,
                link_datetime,
                link_type
            FROM
                sab_link_contacts
            WHERE
                sab_link_contacts.sab_id = $sab_num
        ";

        if ($_SESSION['contact_login_id'])
        {
            $sql .= ' AND con_id = ' . $_SESSION['contact_login_id'];
        }

        $sql .= ' ORDER BY link_datetime DESC';

        $slc = \DatixDBQuery::PDO_fetch($sql, array());

        $sab['rsp_id'] = $slc['rsp_id'];
        $sab['sab_id'] = $slc['sab_id'];
        $sab['con_id'] = $slc['con_id'];
        $sab['link_comments'] = $slc['link_comments'];
        $sab['link_rsp_type'] = $slc['link_rsp_type'];
        $sab['link_rsp_date'] = $slc['link_rsp_date'];
        $sab['link_read_ackn'] = $slc['link_read_ackn'];
        $sab['link_read_date'] = $slc['link_read_date'];
        $sab['link_datetime'] = $slc['link_datetime'];
        $sab['link_type'] = $slc['link_type'];

        $ACTWhereClause = MakeSecurityWhereClause($ActionWhere, 'ACT', $_SESSION['initials']);

        // Get actions
        $sql = "
            SELECT
                recordid, act_module, act_cas_id, act_from_inits, act_to_inits, act_by_inits,
                act_ddue, act_ddone, act_descr, act_score, act_model, act_cost, act_type,
                act_cost_type, act_dstart, act_synopsis, act_progress, act_cost_min, act_cost_max,
                act_priority, act_rss_id, act_resources, act_monitoring, seclevel, secgroup,
                updateddate, updatedby, updateid
            FROM
                ca_actions
            WHERE
                act_module = 'SAB'
                AND
                act_cas_id = :act_cas_id
        ";

        if ($ACTWhereClause)
        {
            $sql .= " and $ACTWhereClause ";
        }

        $sql .= '
            ORDER BY
                act_ddue asc
        ';

        $request = \DatixDBQuery::PDO_fetch_all($sql, array('act_cas_id' => $sab_num));
        $sab['Actions'] = $request;

        // Put the documents in $sab
        $sab['doc'] = $doc;

        $Parameters = array(
            'FormType' => 'Edit',
            'PrePopulatedData' => $sab
        );

        $this->call('src\sabs\controllers\NewSABSTemplateController', 'addnewsabs', $Parameters);
    }
}