<?php
namespace src\passwords\model;

use src\framework\model\EntityFactory;

/**
 * Constructs CodeTagGroup objects.
 */
class PasswordFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\passwords\\model\\Password';
    }
}