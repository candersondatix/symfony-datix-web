<?php
namespace src\passwords\model;

use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\passwords\exceptions\InvalidPasswordEncryptionTypeException;

/**
 * This entity models a password 
 */
class Password extends Entity
{
    /**
     * The user's login: used as a unique id
     * @var string
     */
    protected $login;

    /**
     * The user's encrypted password 
     * @var string hash
     */
    protected $password;

    /**
     * Date of expiration
     * @var string SQL datetime format
     */
    protected $expire_date;

    /**
     * Wether this is the user's current password
     * @var char Y/N
     */
    protected $current;

    /**
     * Encryption type
     * @var string MD5, E, SHA1, M, BCRYPT
     */
    protected $encryption_type;

    /**
     * Setter for encryption_type, checks for valid value
     * @param $value
     * @throws InvalidPasswordEncryptionTypeException
     */
    public function setencryption_type ($value)
    {
        $value = $value ?: 'BCRYPT'; // default value

        if (!in_array ($value, ['MD5','E','SHA1','M','BCRYPT']))
        {
            throw new InvalidPasswordEncryptionTypeException ();
        }

        $this->encryption_type = $value;
    }

    /**
     * Password table doesn't have a primary key
     */
    protected static function setIdentity()
    {
        return array();
    }
}