<?php

namespace src\documents\exceptions;

class DocumentException extends \Exception
{
    public $userVisible = true;
}