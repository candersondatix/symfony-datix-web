<form enctype="multipart/form-data" method="post" name="frmDocument" action="<?php echo $this->scripturl; ?>?action=savedocument">
    <input type="hidden" name="recordid" value="<?php echo $this->recordid; ?>" />
    <input type="hidden" name="module" value="<?php echo $this->module; ?>" />
    <input type="hidden" name="linkedrecordid" value="<?php echo $this->linkedrecordid; ?>" />
    <input type="hidden" name="returnURL" value="<?php echo $this->cancelLink; ?>" />
    <?php echo $this->TableObj->GetFormTable(); ?>
    <div class="button_wrapper">
        <input type="button" value="Save" onClick="this.disabled=true;this.form.submit();" />
        <input type="button" value="Cancel" onClick="if(confirm('Press \'OK\' to confirm or \'Cancel\' to return to the form')){SendTo('<?php echo $this->cancelLink; ?>');}" />
        <?php if ($this->recordid && $this->DeleteDocuments == 'Y') : ?>
            <script>

                function confirmAndDelete ()
                {
                    if (confirm("<?php echo _tk('delete_document'); ?>"))
                    {
                        SendTo('app.php?action=deletedocument&recordid=<?php echo $this->recordid; ?>&linkedrecordid=<?php echo $this->linkedrecordid; ?>&module=<?php echo $this->module; ?>');
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            </script>
            <input type="button" value="Delete" onClick="return confirmAndDelete();" />
        <?php endif; ?>
    </div>
</form>