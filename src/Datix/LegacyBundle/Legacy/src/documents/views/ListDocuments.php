<?php if (count($this->DocumentObjArray) == 0): ?>
    <li class="new_windowbg padded_div">
        <b><?php echo _tk('no_documents')?></b>
    </li>
<?php else: ?>
    <li name="documents_row" id="documents_row">
        <input type="hidden" name="document_url" value="" />
        <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
            <tr>
                <td class="windowbg" width="10%" align="left"><b><?php echo _tk('doc_created')?></b></td>
                <td class="windowbg" width="15%" align="left"><b><?php echo _tk('doc_type')?></b></td>
                <td class="windowbg" width="70%" align="left"><b><?php echo _tk('doc_description')?></b></td>
                <td class="windowbg" width="5%" align="left"><b><?php echo _tk('doc_id')?></b></td>

                <?php if ($this->formType != 'Print' && $this->formType != "ReadOnly"): ?>
                    <td class="windowbg" width="5%" align="left"></td>
                <?php endif; ?>
            </tr>
            <?php foreach($this->DocumentObjArray as $Document): ?>
                <tr>
                    <td class="windowbg2" align="left">
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            <a href="<?php echo $this->scripturl ?>?action=downloaddocument&amp;recordid=<?php echo $Document->recordid ?>&amp;main_table=1"<?php if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet() && \src\framework\registry\Registry::getInstance()->getDeviceDetector()->isIOS()){ echo ' target="_blank"'; } ?>>
                        <?php endif ?>
                        <?php echo FormatDateVal($Document->doc_dcreated) ?>
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            </a>
                        <?php endif ?>
                    </td>
                    <td class="windowbg2">
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            <a href="<?php echo $this->scripturl ?>?action=downloaddocument&amp;recordid=<?php echo $Document->recordid ?>&amp;main_table=1"<?php if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet() && \src\framework\registry\Registry::getInstance()->getDeviceDetector()->isIOS()){ echo ' target="_blank"'; } ?>>
                        <?php endif ?>
                        <?php echo $this->LinkTypes[$Document->doc_type] ?>
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            </a>
                        <?php endif ?>
                    </td>
                    <td class="windowbg2">
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            <a href="<?php echo $this->scripturl ?>?action=downloaddocument&amp;recordid=<?php echo $Document->recordid ?>&amp;main_table=1"<?php if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet() && \src\framework\registry\Registry::getInstance()->getDeviceDetector()->isIOS()){ echo ' target="_blank"'; } ?>>
                        <?php endif ?>
                        <?php echo $Document->doc_notes ?>
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            </a>
                        <?php endif ?>
                    </td>
                    <td class="windowbg2" align="left">
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            <a href="<?php echo $this->scripturl ?>?action=downloaddocument&amp;recordid=<?php echo $Document->recordid ?>&amp;main_table=1"<?php if(\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet() && \src\framework\registry\Registry::getInstance()->getDeviceDetector()->isIOS()){ echo ' target="_blank"'; } ?>>
                        <?php endif ?>
                        <?php echo $Document->recordid ?>
                        <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && is_file($Document->GetPath()) && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                            </a>
                        <?php endif ?>
                    </td>
                    <?php if($this->formType != 'Print' && $this->formType != 'ReadOnly' && (IsFullAdmin() || $Document->doc_createdby == $_SESSION['initials'] || bYN(GetParm('VIEW_DTYPE_' . $Document->doc_type, 'Y')))): ?>
                        <td class="windowbg2" valign="right" border="0">
                            <?php if(is_file($Document->GetPath())): ?>
                                <a href="javascript:if(CheckChange()){SendTo('<?php echo $this->scripturl ?>?action=editdocument&amp;module=<?php echo $this->module ?>&amp;link_id=<?php echo $this->linkId ?>&amp;recordid=<?php echo $Document->recordid ?>&amp;main_table=1');}">[edit]</a>
                            <?php else: ?>
                                <?php $MissingDoc = true; ?>
                                <img src="Images/no_doc.png" alt="<?php echo _tk('missing_document_alt_text') ?>" title="<?php echo _tk('missing_document_alt_text') ?>" />
                            <?php endif ?>
                        </td>
                    <?php endif ?>
                </tr>
            <?php endforeach ?>
        </table>
    </li>
<?php endif ?>
<?php if ($this->formType != 'Print' && $this->formType != "ReadOnly" && $MissingDoc): ?>
    <li class="row_below_table">
        <table>
            <tr>
                <td><img src="Images/no_doc.png" alt="<?php echo _tk('missing_document_explanation') ?>" title="<?php echo _tk('missing_document_explanation') ?>>"></td>
                <td><i><?php echo _tk('missing_document_explanation') ?></i></td>
            </tr>
        </table>
    </li>
<?php endif ?>
<?php if (!($this->formType == "Print" || $this->formType == "ReadOnly")): ?>
    <li class="new_link_row">
            <a href="Javascript:if(CheckChange()){SendTo(<?php echo $this->escape( $this->scripturl.'?action=editdocument&module='.$this->module.'&link_id='.$this->linkId.'&main_table=1&returnURL='.$this->escape($this->returnURL, 'url') , 'js') ?>);}" >
            <b><?php echo _tk('attach_a_new_document')?></b>
        </a>
    </li>
<?php endif ?>