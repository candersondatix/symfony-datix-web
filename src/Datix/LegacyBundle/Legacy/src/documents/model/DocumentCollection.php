<?php
namespace src\documents\model;

use src\framework\model\EntityCollection;

class DocumentCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\documents\\model\\Document';
    }
}