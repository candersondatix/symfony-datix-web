<?php
namespace src\documents\model;

use src\framework\model\Mapper;
use src\framework\model\Entity;
use src\framework\query\Query;

/**
 * Manages the persistance of CodeTagGroup objects.
 */
class DocumentMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\documents\\model\\Document';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'documents_main';
    }

    /**
     * Finds the entity by ID.
     *
     * @param mixed   $id               The value for the property that identifies the entity.
     * @param boolean $overrideSecurity Ignore security where clauses when hydrating the entity.
     *
     * @return Entity|NULL
     */
    public function find($id, $overrideSecurity = false)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $object = parent::find($id, $overrideSecurity);

        if ($object instanceof Document)
        {
            //check user has access to the document type and the module it is linked to.
            if ($overrideSecurity || (CanSeeModule($object->getModule()) && (IsFullAdmin() || $object->doc_createdby == $_SESSION['CurrentUser']->initials || bYN(GetParm('VIEW_DTYPE_'.$object->doc_type, 'Y')))))
            {
                $securityWhereClause = MakeSecurityWhereClause('', $object->getModule());
                //Check whether the user has access to the linked record.
                //TODO: this should be moved into either the MakeSecurityWhereClause function for a DOC module, or into the select() method in this class.
                $recordid = \DatixDBQuery::PDO_fetch('SELECT TOP 1 recordid
                FROM '.($ModuleDefs[$object->getModule()]['VIEW'] ?: $ModuleDefs[$object->getModule()]['TABLE']).'
                WHERE recordid = :recordid' . ($securityWhereClause ? ' AND ('.$securityWhereClause.')' : ''),
                    array('recordid' => $object->getMainRecordId()), \PDO::FETCH_COLUMN);

                if ($recordid)
                {
                    //This removed the . before the extension name, as it is assumed that there is no . before it later on
                    $doc_extension_pattern = '/^\./';

                    if(preg_match($doc_extension_pattern, $object->doc_extension))
                    {
                        $object->doc_extension = preg_replace($doc_extension_pattern, '', $object->doc_extension);
                    }

                    return $object;
                }
            }
        }
    }

    /**
     * Convenience function which inserts/updates the entity, depending on
     * whether or not it already exists in the database.
     *
     * Overriden here becuase you can't insert a linked document without a
     * physical document to link to, so this must be provided on call
     *
     * @param Entity $object
     *
     * @return boolean Whether or not the operation was successful.
     *
     * @throws \InvalidArgumentException When the object is of an incorrect type for this mapper.
     */
    public function save(Entity $object, $fileLocation, $fileExtension)
    {
        if (!$this->isObjectValid($object))
        {
            throw $this->invalidObjectException($object);
        }

        if ($object->recordid)
        {
            return $this->update($object);
        }
        else
        {
            return $this->insert($object, $fileLocation, $fileExtension);
        }
    }

    /**
     * @param Entity $object
     * @param $document
     *
     * Can't insert a linked document without a physical document to link to, so
     * this must be provided on call
     */
    public function insert(Entity $object, $fileLocation, $fileExtension)
    {
        $object->savePhysicalDocument($fileLocation, $fileExtension);

        parent::insert($object);
    }

    /**
     * {@inheritdoc}
     *
     * @param $document File location of the uploaded document being referenced by this entity.
     *
     * Sets various audit values before insert.
     */
    protected function doInsert(Entity $object)
    {
        $object->doc_dcreated = date('Y-m-d H:i:s.000');
        $object->doc_createdby  = $_SESSION['initials'];
        $this->updateEntity($object);
        return parent::doInsert($object);
    }

    /**
     * {@inheritdoc}
     *
     * Sets doc_updatedby, doc_dupdated, and updateid properties before update.
     */
    protected function doUpdate(Entity $object)
    {
        $this->updateEntity($object);
        $object->updateid = $this->generateUpdateId();
        return parent::doUpdate($object);
    }

    /**
     * When deleting a document, we need to also delete the physical document and doc_documents record.
     *
     * @param Entity $object
     *
     * @return boolean Whether or not the operation was successful.
     */
    protected function doDelete(Entity $object)
    {
        $fileName = $object->GetPath();

        if(file_exists($fileName) && !unlink($fileName))
        {
            throw new \Exception('Could not delete physical document.');
        }

        \DatixDBQuery::PDO_query('DELETE FROM doc_documents WHERE recordid = :doc_id', array('doc_id' => $object->doc_id));

        return parent::doDelete($object);
    }


    /**
     * Sets the doc_updatedby and doc_dupdated properties.
     *
     * @param Entity $object
     */
    protected function updateEntity(Entity $object)
    {
        $object->doc_updatedby = $_SESSION['initials'];
        $object->doc_dupdated  = date('Y-m-d H:i:s.000');
    }

    /**
     * Generates a new two-character updateid based on the existing one (if any) for this record.
     * Document entity needs its own version of this because it is not a DatixEntity descendant.
     *
     * NB - copy of GensUpdateID() in Subs.php
     *
     * @return string The new updateid.
     */
    protected function generateUpdateId()
    {
        if ($this->updateid == '')
        {
            $char1 = 0;
            $char2 = 0;
        }
        else
        {
            $char1 = \UnicodeString::ord($this->updateid{0});
            $char2 = \UnicodeString::ord($this->updateid{1});
        }

        $char2 = $this->incrementChar($char2);

        if ($char2 == 48)
        {
            $char1 = $this->incrementChar($char1);
        }

        if ($char1 == 0)
        {
            $this->updateid = \UnicodeString::chr($char2);
        }
        else
        {
            $this->updateid = \UnicodeString::chr($char1) . \UnicodeString::chr($char2);
        }

        return $this->updateid;
    }

    /**
     * Increments an ASCII value within a defined range, used when generating updateids.
     * Document entity needs its own version of this because it is not a DatixEntity descendant.
     *
     * NB - copy of IncUpdateIDChar() in Subs.php
     *
     * @param int $val The ASCII value of the character we're incrementing.
     *
     * @return int $val
     */
    protected function incrementChar($val)
    {
        $val++;
        if ($val < 48)
        {
            $val = 49;
        }
        elseif ($val > 57 && $val < 65)
        {
            $val = 65;
        }
        elseif ($val > 90 && $val < 97)
        {
            $val = 97;
        }
        elseif ($val > 122)
        {
            $val = 48;
        }
        return $val;
    }

    /**
     * {@inheritdoc}
     * 
     * Select the path property in addition to the main table fields.
     */
    public function select(Query $query)
    {
        $where = $this->factory->getQueryFactory()->getWhere();
        $fc    = $this->factory->getQueryFactory()->getFieldCollection();
        
        $where->add($fc->field('documents_main.doc_id')->eq('doc_documents.recordid', true));
        
        $query->select(['doc_documents.path'])
              ->join('doc_documents', $where);
        
        return parent::select($query);
    }
}