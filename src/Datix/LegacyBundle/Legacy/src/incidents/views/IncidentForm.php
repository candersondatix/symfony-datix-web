<script language="JavaScript" type="text/javascript">
    var submitClicked = false;
    <?php if ($this->FormType != 'Search') : ?>
    AlertAfterChange = true;
    <?php endif; ?>
</script>
<form method="post" id="INCform" name="INCform" enctype="multipart/form-data" action="<?php echo $this->scripturl; ?>?action=<?php echo ($this->FormType == 'Search' ? 'incidentsdoselection' : 'saveIncident'); ?>" onsubmit="<?php echo $this->onSubmit; ?>" >
    <input type="hidden" name="recordid" value="<?php echo (is_numeric($this->inc['recordid']) ? (int) $this->inc['recordid'] : ''); ?>" />
    <input type="hidden" id="form-panel" name="panel" value="<?php echo ($this->panel ?: ''); ?>" />
    <input type="hidden" name="fromsearch" value="<?php echo (is_numeric($this->fromsearch) ? (int) $this->fromsearch : ''); ?>" />
    <input type="hidden" name="fromlisting" value="<?php echo Escape::EscapeEntities($this->fromlisting); ?>" />
    <input type="hidden" name="from_report" value="<?php echo Escape::EscapeEntities($this->from_report); ?>" />
    <input type="hidden" name="overdue" value="<?php echo Escape::EscapeEntities($this->overdue); ?>" />
    <input type="hidden" name="updateid" value="<?php echo Escape::EscapeEntities($this->inc['updateid']); ?>" />
    <input type="hidden" name="formname" value="dif2" />
    <input type="hidden" name="increp_recordid" value="<?php echo (is_numeric($this->inc['increp_recordid']) ? (int) $this->inc['increp_recordid'] : ''); ?>" />
    <input type="hidden" name="session_form" value="<?php echo $this->session_form; ?>" />
    <input type="hidden" name="session_date" value="<?php echo $this->lastsession; ?>" />
    <input type="hidden" name="form_id" value="" />
    <input type="hidden" name="rep_approved_old" value="<?php echo Escape::EscapeEntities($this->inc['rep_approved_old'] ? $this->inc['rep_approved_old'] : ($this->inc['rep_approved'] ? $this->inc['rep_approved'] : 'NEW')); ?>" />
    <input type="hidden" name="inc_ourref" value="<?php echo Escape::EscapeEntities($this->inc['inc_ourref']); ?>" />
    <input type="hidden" name="qbe_recordid" value="<?php echo (is_numeric($this->qbe_recordid) ? (int) $this->qbe_recordid : ''); ?>" />
    <input type="hidden" name="qbe_return_module" value="<?php echo Escape::EscapeEntities($this->qbe_return_module); ?>" />
    <input type="hidden" name="predefined" value="<?php echo (is_numeric($this->predefined) ? '1' : ''); ?>" />
    <input type="hidden" name="formlevel" id="formlevel" value="<?php echo $this->formlevel; ?>" />
    <?php if ($this->formlevel == 1) : ?>
    <input type="hidden" name="holding_form" value="1" />
    <?php endif; ?>
    <?php if (!$this->LoggedIn) : ?>
    <input type="hidden" name="rep_approved" value="NEW" />
    <?php endif; ?>
    <?php if ($this->sLockMessage) : ?>
    <div class="lock_message">
        <div id="LockMessage"><?php echo $this->sLockMessage; ?></div>
        <div id="userLockMessage"></div>
    </div>
    <?php endif; ?>
    <?php echo $this->IncTable->GetFormTable(); ?>
    <input type="hidden" id="rbWhat" name="rbWhat" value="Save">
    <?php echo $this->ButtonGroup->getHTML(); ?>
</form>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect($this->FormDesign->Show_all_section, $this->panel, $this->IncTable); ?>
<?php endif; ?>