<form method="post" name="frmIncidentDIF1" id="frmIncidentDIF1" enctype="multipart/form-data" action="<?php echo $this->scripturl.$this->FormAction; ?>" <?php echo $this->FormOnSubmit; ?>>
    <input type="hidden" name="holding_form" value="1" />
    <input type="hidden" name="recordid" value="<?php echo (is_numeric($this->inc['recordid']) ? (int) $this->inc['recordid'] : ''); ?>" />
    <input type="hidden" name="updateid" value="<?php echo Escape::EscapeEntities($this->inc['updateid']); ?>" />
    <input type="hidden" name="module" value="DIF1" />
    <input type="hidden" name="session_form" value="<?php echo Escape::EscapeEntities($this->session_form); ?>" />
    <input type="hidden" name="session_date" value="<?php echo $this->lastsession; ?>" />
    <input type="hidden" name="formlevel" id="formlevel" value="1" />
    <input type="hidden" name="form_id" value="<?php echo (is_numeric($this->FormDesign->ID) ? (int) $this->FormDesign->ID : ''); ?>" />
    <input type="hidden" name="rep_approved_old" value="<?php echo Escape::EscapeEntities($this->inc['rep_approved_old'] ? $this->inc['rep_approved_old'] : ($this->inc['rep_approved'] ? $this->inc['rep_approved'] : 'NEW')); ?>" />
    <?php if ($this->FormMode != 'Print' && $this->FormMode != 'Search' && $this->FormMode != 'ReadOnly') : ?>
    <script language="JavaScript" type="text/javascript">
        var submitClicked = false;
    </script>
    <?php endif; ?>
    <?php echo $this->IncTable->GetFormTable(); ?>
    <div class="button_wrapper">
        <input type="hidden" id="rbWhat" name="rbWhat" value="Save" />
        <input type="hidden" id="printAfterSubmit" name="printAfterSubmit" value="0" />
        <?php if($_SESSION['logged_in']) : ?>
        <?php echo GetFormButtonsHTML(array('module' => 'INC', 'formtype' => $this->FormType, 'data' => $this->inc)); ?>
        <?php else : ?>
            <?php if ($this->FormMode != 'Print' && $this->FormMode != 'ReadOnly') : ?>
            <input type="button" value="<?php echo FirstNonNull(array(_tk('btn_submit_INC'), _tk('btn_submit'))); ?>" class="button" name="btnSubmit" id="btnSubmit" onclick="document.frmIncidentDIF1.rbWhat.value = 'Save';selectAllMultiCodes();submitClicked = true; writeSuffixLimits();if(validateOnSubmit())this.form.submit();" />&nbsp;&nbsp;
                <?php if(!bYN(GetParm('DIF_1_NO_PRINT')) && !\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()) : ?>
                <input type="button" value="<?php echo _tk('btn_submit_and_print_incident'); ?>" class="button" onclick="$('printAfterSubmit').value = 1;$('rbWhat').value = 'Save';selectAllMultiCodes();submitClicked=true; writeSuffixLimits();if(validateOnSubmit())this.form.submit();" name="btnSubmitPrint" />&nbsp;&nbsp;
                <?php endif; ?>
            <input type="button" value="<?php echo _tk('btn_cancel'); ?>" class="button" onclick="<?php echo getConfirmCancelJavascript(); ?>" name="btnCancel" />
            <br />
            <?php elseif ($this->FormMode == 'ReadOnly') : ?>
            <div id="btns">
                <input type="button" value="<?php echo _tk('btn_close'); ?>" name="btnCancel" onclick="document.forms[0].rbWhat.value='Cancel'; submitClicked = true; this.form.submit();" class="button" />
                <br />
            </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</form>