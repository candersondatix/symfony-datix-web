<?php

namespace src\incidents\controllers;

use src\framework\controller\Controller;

class ShowIncidentController extends Controller
{
    function incident()
    {
        global $inc, $link_contacts, $investigation, $FormArray;

        $ModuleDefs = $this->registry->getModuleDefs();

        $recordid = ($this->request->getParameter('recordid') ? $this->request->getParameter('recordid') : '');
        $message = ($this->request->getParameter('message') ? $this->request->getParameter('message') : '');

        if (!(!$_SESSION['logged_in'] && $this->request->getParameter('submitandprint') &&
            HashesMatch('INC', $this->request->getParameter('recordid'))))
        {
            LoggedIn();
        }
        else
        {
            $Level1Review = true;
            $FormType = 'Print';
            $form_action = 'Print';
        }

        if ($recordid)
        {
            $inc_num = $recordid;
        }
        else
        {
            $inc_num = \Sanitize::SanitizeInt($recordid);
        }

        $ShowDIF1Values = $this->request->getParameter('show_dif1_values');

        if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')))
        {
            require_once 'Source/security/SecurityBase.php';
            $DIFPerms = GetUserHighestAccessLvlForRecord('DIF_PERMS', 'INC', $inc_num);
        }
        else
        {
            $DIFPerms = GetParm('DIF_PERMS');
        }

        $SelectString = "
            SELECT
                recordid,
                rep_approved,
                updateid,
                updatedby, ".
                implode(', ', $ModuleDefs['INC']['FIELD_ARRAY']);

        $sql = $SelectString . ' FROM incidents_main';

        if (!$Level1Review)
        {
            $WhereClause = MakeSecurityWhereClause('recordid = :inc_num', 'INC', $_SESSION['initials'], "", "", false, true, "", array(), $inc_num);
        }
        else
        {
            $WhereClause = 'recordid = :inc_num';
        }

        $sql .= " WHERE $WhereClause";

        $inc = PDO_fetch($sql, array('inc_num' => $inc_num));

        ////////////////////////////////////////////////////////////////////////
        // Access based on DB table link_access_approvalstatus
        $AccessFlag = GetAccessFlag('INC', $DIFPerms, $inc['rep_approved']);

        if ($AccessFlag == 'R')
        {
            $form_action = 'ReadOnly';
        }

        if (!$inc || ((!$DIFPerms || $AccessFlag == '') && !$Level1Review))
        {
            CheckRecordNotFound(array('module' => 'INC', 'recordid' => $inc_num));
        }

        $extra_data = GetExtraData('INC', $inc_num);

        $inc = array_merge($inc, $extra_data);

        $sqlnotepad = '
            SELECT
                notes
            FROM
                notepad
            WHERE
                inc_id = :inc_num
        ';

        $incnotes = PDO_fetch($sqlnotepad, array('inc_num' => $inc_num));
        $inc['notes'] = $incnotes['notes'];

        $sql = '
            SELECT
                fullname
            FROM
                staff
            WHERE
                initials = :initials
        ';

        $row = PDO_fetch($sql, array('initials' => $inc['inc_mgr']));

        if ($row['fullname'])
        {
            $inc['inc_mgr_fullname'] = $row['fullname'];
        }

        if ($Level1Review)
        {
            $FormLevel = 1;
        }
        elseif ($inc['rep_approved'])
        {
            $FormLevel = GetFormLevel('INC', $DIFPerms, $inc['rep_approved']);
        }

        $_SESSION['INC']['FORMLEVEL'] = $FormLevel;

        // Need to override security for level 1 forms or contacts attached will not be populated.
        $inc['con'] = GetLinkedContacts(array('recordid' => $inc_num, 'module' => 'INC', 'formlevel' => $FormLevel), ($FormLevel == 1));
        $inc['ast'] = GetLinkedEquipment(array('recordid' => $inc_num, 'module' => 'INC', 'formlevel' => $FormLevel));

        if ($FormLevel == 1)
        {
            // Need to populate show_xxx fields
            $inc = PopulateLevel1FormFields(array('module' => 'INC', 'data' => $inc));
        }

        // Check if we need to show original values from DIF1
        if ($inc && $ShowDIF1Values)
        {
            $sql = $SelectString . '
                FROM
                    incidents_report
			    WHERE
			        main_recordid = :inc_num
            ';

            $OldValues = PDO_fetch($sql, array('inc_num' => $inc_num));

            require_once 'Source/incidents/MainIncident.php';
            $OldValuesUDF = getDIF1OldValuesUDF($inc_num);

            if (count($OldValuesUDF))
            {
                $OldValues = array_merge($OldValues, $OldValuesUDF);
            }

            if ($OldValues)
            {
                $inc['old_values'] = $OldValues;
            }
        }

        // Check if we need to show full audit trail
        if ($inc && $this->request->getParameter('full_audit'))
        {
            $FullAudit = GetFullAudit(array('Module' => 'INC', 'recordid' => $inc_num));

            if ($FullAudit)
            {
                $inc['full_audit'] = $FullAudit;
            }
        }

        // Check if there are RIDDOR fields with data
        if ($inc['inc_is_riddor'] == '')
        {
            if ($inc['inc_dnotified'] != '' || $inc['inc_ridloc'] != '' || $inc['inc_riddor_ref'] != '' ||
                $inc['inc_address'] != '' || $inc['inc_localauth'] != '' || $inc['inc_acctype'] != '' ||
                $inc['inc_riddorno'] != '')
            {
                $inc['inc_is_riddor'] = 'Y';
            }
        }

        if ($Level1Review)
        {
            $inc['temp_record'] = true;
        }

        AuditOpenRecord('INC', $inc_num, '');

        $this->response = $this->call('src\incidents\controllers\IncidentFormTemplateController', 'ShowIncidentForm',
            array(
                'form_action' => $form_action,
                'level' => $FormLevel,
                'data' => $inc,
                'fromsearch' => $this->request->getParameter('fromsearch'),
                'fromlisting' => $this->request->getParameter('fromlisting'),
                'qbe_return_module' => $this->request->getParameter('qbe_return_module'),
                'qbe_recordid' => $this->request->getParameter('qbe_recordid'),
                'predefined' => $this->request->getParameter('predefined'),
                'panel' => $this->request->getParameter('panel')
            )
        );
    }
}