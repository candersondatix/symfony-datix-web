<?php

namespace src\incidents\controllers;

use src\framework\controller\Controller;

class IncidentSnapshotController extends Controller
{
    function showSnapshot()
    {
        $recordid = $this->request->getParameter('recordid');

        if(!$recordid)
        {
            throw new \RequiredParameterMissingException('No recordid supplied');
        }

        //Should only be able to see the snapshot if you can see the incident that it is a snapshot of.
        $WhereClause = MakeSecurityWhereClause('recordid = :recordid', 'INC', ($_SESSION['CurrentUser'] instanceof User ? $_SESSION['CurrentUser']->initials : ''));
        if (\DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM incidents_main'. ($WhereClause ? ' WHERE '.$WhereClause : ''), array('recordid' => $recordid), \PDO::FETCH_COLUMN) > 0)
        {
            $data = \DatixDBQuery::PDO_fetch('SELECT recorddata FROM inc_submission_snapshot WHERE recordid = :recordid', array('recordid' => $recordid), \PDO::FETCH_COLUMN);
            $this->response->setBody($data);
        }
    }
}