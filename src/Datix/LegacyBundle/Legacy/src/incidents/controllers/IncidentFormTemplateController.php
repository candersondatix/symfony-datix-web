<?php

namespace src\incidents\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Response;
use src\framework\controller\Request;
use src\reasons\controllers\ReasonsController;
use src\users\model\UserMapper;
use src\users\model\UserModelFactory;

class IncidentFormTemplateController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        $this->module = 'INC';
        parent::__construct($request, $response);
    }

    /**
     * Shows an incident form (level 1 or level 2 or search)
     */
    public function ShowIncidentForm()
    {
        global $FormType, $formlevel, $JSFunctions;
        $ModuleDefs = $this->registry->getModuleDefs();

        $this->addJs('src/incidents/js/CheckCauses.js');

        $inc = $this->request->getParameter('data') ?: null;
        $form_action = $this->request->getParameter('form_action') ?: 'edit';
        $level = $this->request->getParameter('level') ?: '2';

        if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && $form_action != 'search')
        {
        	require_once 'Source/security/SecurityBase.php';
        	$DIFPerms = GetUserHighestAccessLvlForRecord('DIF_PERMS', 'INC', $inc['recordid']);
        }
        else
        {
        	$DIFPerms = GetParm('DIF_PERMS');        	
        }

        $LoggedIn = isset($_SESSION['logged_in']);

        $formlevel = $level;

        // Default, overwrite as required
        $FormType = 'Edit';

        SetUpFormTypeAndApproval('INC', $inc, $FormType, $form_action, $DIFPerms);

        $this->addJs(array(
            array('file' => 'src/generic/js/print.js', 'conditions' => ($FormType == "Print" || $this->request->getParameter('print') == 1)),
            array('file' => 'src/generic/js/panelData.js', 'conditions' => ($this->registry->getParm('SHOW_PANEL_INDICATORS', 'N', false, true) && ($FormType != "Print" && $this->request->getParameter('print') != 1))),
            array('file' => 'src/generic/js/atpromptable.js', 'conditions' => ($this->deviceDetect->isTablet() && $FormType == 'Search'))
            ));

        CheckForRecordLocks('INC', $inc, $FormType, $sLockMessage);

        if ($inc['rep_approved'] == 'REJECT' && bYN(GetParm('REJECT_REASON', 'Y')) && $form_action != 'search')
        {
            $inc = ReasonsController::GetReasonsData('INC', $inc['recordid'], $inc, $FormType);
        }

        if (($inc['rep_approved'] == '' || $inc['rep_approved'] == 'AWAFA' || $inc['rep_approved'] == 'AWAREV'
            || $inc['rep_approved'] == 'FA' || $inc['rep_approved'] == 'INFA' || $inc['rep_approved'] == 'INREV'
            || $inc['rep_approved'] == 'REJECT') && $FormType != 'Search')
        {
            if ($inc['inc_dopened'] == '')
            {
                $inc['inc_dopened'] = date('Y-m-d H:i:s.000');
            }
        }

        // Load form settings
        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'INC',
            'level' => $level,
            'form_type' => $FormType
        ));

        $_SESSION['LASTUSEDFORMDESIGN'] = $FormDesign->GetFilename();
        $_SESSION['LASTUSEDFORMDESIGNFORMLEVEL'] = $level;

        if ($LoggedIn && $this->request->getParameter('action') != 'addnewincident')
        {
            $FormDesign->UnsetDefaultValues();
        }

        SetUpApprovalArrays('INC', $inc, $CurrentApproveObj, $ApproveObj, $FormType, $FormDesign, '', $DIFPerms);

        $notsaved = ($this->request->getParameter('action') == 'addnewincident'
            || $this->request->getParameter('action') == 'incidentsearch');

        if (empty($GLOBALS['HideFields']['inc_mgr']) && bYN(GetParm('DIF_EMAIL_MGR', 'N')) && $_GET['print'] != 1)
        {
            $ManagerDropdownField = MakeManagerDropdownGeneric('INC', $inc, $FormType, array('DIF2', 'RM'));
        }

        include($ModuleDefs['INC']['BASIC_FORM_FILES'][$level]);

        $IncTable = new \FormTable($FormType, 'INC', $FormDesign);
        $IncTable->MakeForm($FormArray, $inc, 'INC');

        $this->sendToJs(
            array(
                'FormType' => $FormType,
                'printSections' => $IncTable->printSections
            )
        );

        $migrateDateString = $this->registry->getParm('RECORD_UPDATE_EMAIL_DATETIME');
        $migrateDateIsValid = $migrateDateString != '' && preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}(\.\d{3})?/', $migrateDateString) === 1;
        $recordUpdateEmail = bYN($this->registry->getParm('RECORD_UPDATE_EMAIL', 'N')) && $migrateDateIsValid;

        if($recordUpdateEmail)
        {
            try
            {
                $updatedDateSQL = 'SELECT updateddate FROM incidents_main WHERE recordid = :recordid';
                $updatedDate = new \DateTime(\DatixDBQuery::PDO_fetch($updatedDateSQL, ['recordid' => $inc['recordid']])['updateddate']);
                $migrateDate = new \DateTime($migrateDateString);

                if ($migrateDate > $updatedDate)
                {
                    $this->sendToJs('txt.email_merge_wait_msg', _tk('email_merge_wait_msg'), false);
                }

            }
            catch (\Exception $e)
            {
                $logger = $this->registry->getLogger();

                $logger->logEmergency('INC: invalid SQL date stored in database - updateddate or RECORD_UPDATE_EMAIL_DATETIME');
            }
        }

        if ($this->request->getParameter('submitandprint'))
        {
            $FormDesign->FormTitle .= '<br>Reference: '.$inc['inc_ourref'];
        }

        $FormDesign->FormTitle .= GetRejectedSuffix($inc, 'INC', $ModuleDefs['INC']['FIELD_NAMES']['HANDLER'],
            $inc['rea_con_name']);

        $title_suffix = $this->getTitleSuffix('INC', $inc, !empty($FormDesign->FormTitle));

        $this->title = $FormDesign->FormTitle;
        $this->recordInfo = $title_suffix;
        $this->subtitle = $FormDesign->FormTitleDescr;
        $this->image = ($FormType == 'Search' ? 'images/icons/icon_INC_search.png' : 'images/icons/icon_INC_new.png');

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->PopulateWithStandardFormButtons(array(
            'module' => 'INC',
            'formtype' => $FormType,
            'data' => $inc,
            'form_id' => 'INCform',
            'level' => $level
        ));

        if ($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) &&!empty($_SESSION['INC']['RECORDLIST']) && (!isset($ModuleDefs['INC']['NO_NAV_ARROWS']) || $ModuleDefs['INC']['NO_NAV_ARROWS'] != true))
        {
            $CurrentIndex = $_SESSION['INC']['RECORDLIST']->getRecordIndex(array('recordid' => $inc['recordid']));

            if ($CurrentIndex !== false)
            {
                $ButtonGroup->AddNavigationButtons($_SESSION['INC']['RECORDLIST'], $inc['recordid'], 'INC');
            }
        }

        // If we are in "Print" mode, we don't want to display the menu
        if ($FormType != 'Print' && $LoggedIn)
        {
            $this->menuParameters = array(
                'module' => 'INC',
                'table' => $IncTable,
                'buttons' => $ButtonGroup,
                'recordid' => $inc['recordid']
            );
        }
        else
        {
            $this->hasMenu = false;
        }

        if ($FormType != 'Print' && $FormType != 'Search')
        {
            $multiPerms = $_SESSION['CurrentUser']->getAccessLevels('INC');
            $StatusesNoMandatory = GetStatusesNoMandatory(
                array(
                    'module' => 'INC',
                    'level' => $multiPerms,
                    'from' => $inc['rep_approved']
                ),
                true
            );

            if (is_array($StatusesNoMandatory) && !empty($StatusesNoMandatory))
            {
                $JSFunctions[] = 'StatusesNoMandatory = [\''.implode("','",$StatusesNoMandatory).'\'];';
            }

            $JSFunctions[] = MakeJavaScriptValidation('INC', $FormDesign);
        }

        $onSubmit = 'return(submitClicked';

        if ($FormType != 'Print' && $FormType != 'Search' && $FormType != 'ReadOnly')
        {
            $onSubmit .= ' && validateOnSubmit()';
            SetFormSession($session_form, $lastsession);
        }

        $qbe_return_module = filter_var($this->request->getParameter('qbe_return_module'), FILTER_SANITIZE_STRING);

        $onSubmit .= ');';

        $IncTable->MakeTable();

        $this->response->build('src/incidents/views/IncidentForm.php', array(
            'FormType'          => $FormType,
            'onSubmit'          => $onSubmit,
            'inc'               => $inc,
            'fromsearch'        => $this->request->getParameter('fromsearch'),
            'fromlisting'       => $this->request->getParameter('fromlisting'),
        	'from_report'       => $this->request->getParameter('from_report'),
        	'overdue'           => $this->request->getParameter('overdue'),
            'session_form'      => $session_form,
            'lastsession'       => $lastsession,
            'qbe_recordid'      => $this->request->getParameter('qbe_recordid'),
            'qbe_return_module' => $qbe_return_module,
            'predefined'        => $this->request->getParameter('predefined'),
            'formlevel'         => $formlevel,
            'LoggedIn'          => $LoggedIn,
            'sLockMessage'      => $sLockMessage,
            'IncTable'          => $IncTable,
            'ButtonGroup'       => $ButtonGroup,
            'FormDesign'        => $FormDesign,
            'panel'             => $this->request->getParameter('panel')
        ));
    }

    /**
     * Function to show level 1 incidents form
     */
    public function ShowHoldingForm()
    {
        global $HideFields, $MandatoryFields, $OrderFields, $DIF1UDFGroups, $UserLabels, $UserExtraText, $FormTitle,
               $Show_all_section, $FormTitleDescr, $JSFunctions, $UserSSOSettingsFile, $formlevel;

        $ModuleDefs = $this->registry->getModuleDefs();
        $inc = $this->request->getParameter('data') ?: null;
        $FormMode = ($this->request->getParameter('FormMode') ? $this->request->getParameter('FormMode') : '');

        //Form level is always 1 for DIF1
        $formlevel = 1;

        $_SESSION['INC']['FORMLEVEL'] = $formlevel;

        $this->request->setParameter('module', 'INC');

        $DIFPerms = $_SESSION['Globals']['DIF_PERMS'];

        if ($FormMode == 'ReadOnly')
        {
            $ReadOnly = true;
        }

        if (!isset($inc['rep_approved']))
        {
            $inc['rep_approved'] = 'NEW';
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'INC',
            'level' => 1,
            'form_type' => $FormMode
        ));

        // The title of the form should be defined in the settings file.
        // If not, set a default.
        if (!$FormDesign->FormTitle)
        {
            $FormDesign->FormTitle = _tk('dif1_title');
        }

        if ($inc['rep_approved'] == 'REJECT')
        {
            $FormDesign->FormTitle .= " (" . _tk('rejected_by') . " " . code_descr('INC', 'inc_mgr', $inc['updatedby']) . ")";
        }

        if ($_SESSION['contact_login_id'] && bYN(GetParm('DIF_1_USER_DETAILS', 'Y')))
        {
            $DIF1UserArray = $this->MakeDIF1UserDetailsSection($_SESSION['contact_login_id'], $FormMode, $inc);
        }

        // Fill in reporter details automatically if WEB_NETWORK_LOGIN is Y
        if (bYN(GetParm('WEB_NETWORK_LOGIN', 'N')) && !$inc['error'])
        {
            if ($UserSSOSettingsFile)
            {
                include($UserSSOSettingsFile);

                $inc['con_forenames_3'] = $UserDetails['FirstNames'];
                $inc['con_surname_3'] = $UserDetails['Surname'];
                $inc['con_email_3'] = $UserDetails['Email'];
            }
            else
            {
                require_once 'Source/libs/LDAP.php';

                $LDAP = new \DatixLDAP;

                if ($LDAP->AuthenticateUser($_SERVER['LOGON_USER'], ''))
                {
                    $UserDetails = $LDAP->GetUser();
                    $inc['inc_repname'] = $UserDetails['forenames']
                        . ($UserDetails['forenames'] ? ' ' : '') . $UserDetails['surname'];
                    $inc['inc_rep_tel'] = $UserDetails['telephone'];
                    $inc['inc_rep_email'] = $UserDetails['email'];
                    $inc['con_forenames_3'] = $UserDetails['forenames'];
                    $inc['con_surname_3'] = $UserDetails['surname'];
                    $inc['con_email_3'] = $UserDetails['email'];
                    $inc['con_tel1_3'] = $UserDetails['telephone'];
                }
            }
        }

        // Set default locations
        if ($inc['inc_organisation'] == '')
        {
            if ($_SESSION['sta_orgcode'] != '')
            {
                $inc['inc_organisation'] = $_SESSION['sta_orgcode'];
            }
            else
            {
                $inc['inc_organisation'] = $this->request->getParameter('inc_organisation');
            }
        }

        if ($inc['inc_unit'] == '')
        {
            if ($_SESSION['sta_unit'] != '')
            {
                $inc['inc_unit'] = $_SESSION['sta_unit'];
            }
            else
            {
                $inc['inc_unit'] = $this->request->getParameter('inc_unit');
            }
        }

        if ($inc['inc_clingroup'] == '')
        {
            if ($_SESSION['sta_clingroup'] != '')
            {
                $inc['inc_clingroup'] = $_SESSION['sta_clingroup'];
            }
            else
            {
                $inc['inc_clingroup'] = $this->request->getParameter('inc_clingroup');
            }
        }

        if (!$inc['inc_directorate'])
        {
            if ($_SESSION['sta_directorate'] != '')
            {
                $inc['inc_directorate'] = $_SESSION['sta_directorate'];
            }
            else
            {
                $inc['inc_directorate'] = $this->request->getParameter('inc_directorate');
            }
        }

        if ($inc['inc_specialty'] == '')
        {
            if ($_SESSION['sta_specialty'] != '')
            {
                $inc['inc_specialty'] = $_SESSION['sta_specialty'];
            }
            else
            {
                $inc['inc_specialty'] = $this->request->getParameter('inc_specialty');
            }
        }

        if ($inc['inc_locactual'] == '')
        {
            $inc['inc_locactual'] = $this->request->getParameter('inc_locactual');
        }

        if (file_exists('ClientLogo.jpg'))
        {
            $ClientLogo = 'ClientLogo.jpg';
        }
        else
        {
            $ClientLogo = $GLOBALS['ClientLogo'];
        }

        if ($inc['recordid'] != '')
        {
            $FormDesign->FormTitle .= '. ' . _tk('number') . ': ' . $_SESSION['Globals']['DIF_1_REF_PREFIX'] . $inc['recordid'] .
                ($ClientLogo ? '<img src="' . $ClientLogo . '" align="right" />' : '');
        }

        // End of DIF1 table header
        SetUpFormTypeAndApproval('INC', $inc, $FormType, $form_action, $DIFPerms);

        $this->sendToJs('FormType', $FormType);

        SetUpApprovalArrays('INC', $inc, $CurrentApproveObj, $ApproveObj, $FormType, $FormDesign, '', $DIFPerms);

        // rest of Dif1 table
        include($ModuleDefs['INC']['BASIC_FORM_FILES'][1]);

        // Form is now generated and output here, rather than in the form
        // definition file (BasicForm.php or BasicForm2.php)
        $IncTable = new \FormTable($FormMode, 'INC', $FormDesign);
        $IncTable->MakeForm($FormArray, $inc, 'INC');

        $this->title = $FormDesign->FormTitle;
        $this->subtitle = $FormDesign->FormTitleDescr;
        $this->image = 'Images/'.$ModuleDefs['INC']['ICON'];

        if ($_SESSION['logged_in'] && $FormMode != 'Print')
        {
            $ButtonGroup = new \ButtonGroup();
            $ButtonGroup->PopulateWithStandardFormButtons(array(
                'module' => 'INC',
                'formtype' => $FormType,
                'data' => $inc,
                'form_id' => 'frmIncidentDIF1',
                'level' => 1
            ));

            $this->menuParameters = array('module' => 'INC', 'table' => $IncTable, 'buttons' => $ButtonGroup);
        }
        else
        {
            $this->hasMenu = false;
        }

        $FormAction = '?action=saveIncident';

        if ($FormID)
        {
            $FormAction .= '&form_id='.$FormID;
        }

        if ($FormMode != 'Print' && $FormMode != 'Search' && $FormMode != 'ReadOnly')
        {
            $FormOnSubmit = ' onsubmit="return (submitClicked &amp;&amp; validateOnSubmit())"';
            SetFormSession($session_form, $lastsession);
        }
        else
        {
            $FormOnSubmit = '';
        }

        if ($FormType != 'Print' && $FormType != 'Search')
        {
            $StatusesNoMandatory = GetStatusesNoMandatory(
                array(
                    'module' => 'INC',
                    'level' => $DIFPerms,
                    'from' => $inc['rep_approved']
                )
            );

            if (is_array($StatusesNoMandatory) && !empty($StatusesNoMandatory))
            {
                $JSFunctions[] = 'StatusesNoMandatory = [\''.implode("','",$StatusesNoMandatory).'\'];';
            }

            $JSFunctions[] = MakeJavaScriptValidation('INC', $FormDesign);
        }

        $IncTable->MakeTable();

        $this->response->build('src/incidents/views/IncidentHoldingForm.php', array(
            'FormAction' => $FormAction,
            'FormOnSubmit' => $FormOnSubmit,
            'inc' => $inc,
            'session_form' => $session_form,
            'lastsession' => $lastsession,
            'FormDesign' => $FormDesign,
            'FormMode' => $FormMode,
            'IncTable' => $IncTable,
            'FormType' => $FormType
        ));
    }

    /**
     * Creates a section on the DIF1 form when a user is logged in to hold that user's information and to give
     * them the 'Your Manager' option.
     *
     * @param string $recordid The id of the incident.
     * @param string $FormType The type fo the form.
     * @param string $inc The data array
     *
     * @return array Containing the fields for this section.
     */
    protected function MakeDIF1UserDetailsSection($recordid, $FormType, $inc="")
    {
        $sql = '
            SELECT
                con_type, con_subtype, con_title, con_forenames,
                con_surname, con_email, con_number, con_nhsno, con_tel1
            FROM
                contacts_main
            WHERE
                recordid = :recordid
        ';

        $row = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

        $TypeObj = new \FormField('ReadOnly');
        $TypeObj->MakeCustomField(code_descr('INC','con_type', $row['con_type']));
        $DIF1UserArray[] = array(
            'Name' => 'con_type',
            'Title' => _tk('con_type'),
            'Type' => 'formfield',
            'FormField' => $TypeObj
        );

        $SubTypeObj = new \FormField('ReadOnly');
        $SubTypeObj->MakeCustomField(code_descr('INC','con_subtype', $row['con_subtype']));
        $DIF1UserArray[] = array(
            'Name' => 'con_subtype',
            'Title' => _tk('con_subtype'),
            'Type' =>
            'formfield',
            'FormField' => $SubTypeObj
        );

        $TitleObj = new \FormField('ReadOnly');
        $TitleObj->MakeCustomField($row['con_title']);
        $DIF1UserArray[] = array(
            'Name' => 'con_title',
            'Title' => _tk('con_title'),
            'Type' => 'formfield',
            'FormField' => $TitleObj
        );

        $ForenameObj = new \FormField('ReadOnly');
        $ForenameObj->MakeCustomField($row['con_forenames']);
        $DIF1UserArray[] = array(
            'Name' => 'con_forenames',
            'Title' => _tk('con_forenames'),
            'Type' => 'formfield',
            'FormField' => $ForenameObj
        );

        $SurnameObj = new \FormField('ReadOnly');
        $SurnameObj->MakeCustomField($row['con_surname']);
        $DIF1UserArray[] = array(
            'Name' => 'con_surname',
            'Title' => _tk('con_surname'),
            'Type' => 'formfield',
            'FormField' => $SurnameObj
        );

        $EmailObj = new \FormField('ReadOnly');
        $EmailObj->MakeCustomField($row['con_email']);
        $DIF1UserArray[] = array(
            'Name' => 'con_email',
            'Title' => _tk('con_email'),
            'Type' => 'formfield',
            'FormField' => $EmailObj
        );

        if ($row['con_number'])
        {
            $NumberObj = new \FormField('ReadOnly');
            $NumberObj->MakeCustomField($row['con_number']);
            $DIF1UserArray[] = array(
                'Name' => 'con_number',
                'Title' => _tk('con_number'),
                'Type' =>
                'formfield',
                'FormField' => $NumberObj
            );
        }

        $TelNumObj = new \FormField('ReadOnly');
        $TelNumObj->MakeCustomField($row['con_tel1']);
        $DIF1UserArray[] = array(
            'Name' => 'con_tel1',
            'Title' => _tk('con_tel1'),
            'Type' => 'formfield',
            'FormField' => $TelNumObj
        );

        $DIF1UserArray[] = array(
            'Type' => 'formfield',
            'Name' => 'inc_mgr',
            'Condition' => bYN(GetParm('DIF_EMAIL_MGR', 'N')),
            'Title' => _tk('your_manager'),
            'FormField' => self::MakeManagerDropdown($inc, ($FormType == 'ReadOnly' ? 'Print' : $FormType))
        );

        return $DIF1UserArray;
    }

    /**
     * Returns a FormField object containing a dropdown list of all the
     * available managers to approve incidents.
     * Finds all of those whose DIF_PERMS are equal to "DIF2" or "RM".
     *
     * @param array $inc Submitted form data.
     * @param string $FieldMode e.g. read-only etc.
     *
     * @return object(FormField) The field.
     */
    static function MakeManagerDropdown($inc, $FieldMode)
    {
        global $DefaultValues, $HideFields, $ModuleDefs, $txt;

        $field = \Forms_SelectFieldFactory::createSelectField('inc_mgr', 'INC', $inc['inc_mgr'], $FieldMode,
            false, _tk('your_manager'), 'your_manager', $inc['CHANGED-inc_mgr']);

        if (!bYN(GetParm('STAFF_EMPL_FILTERS', 'N')))
        {
            // CB - this condition may now be unnecessary, need to double check differences between two methods
            $field->setSelectFunction('getDIF1Managers');
        }

        return $field;
    }
}