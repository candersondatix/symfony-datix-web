<?php

namespace src\incidents\controllers;

use src\framework\controller\Controller;

class ShowUAIncidentController extends Controller
{
    /**
     * @desc This function is needed because some clients may follow links sent pre-v10. These links will
     * reference holding area records rather than main table ones, and so we need to translate the recordid
     * and present the correct record.
     *
     * TODO: Remove this when there's no clients on pre-v10 releases.
     */
    function ShowUAIncident()
    {
        $sql = 'SELECT main_recordid FROM incidents_report WHERE recordid = :recordid';
        $row = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $this->request->getParameter('recordid')));

        $this->response = $this->call('src\incidents\controllers\ShowIncidentController', 'incident', array(
            'recordid' => $row['main_recordid']
        ));

        return;
    }
}