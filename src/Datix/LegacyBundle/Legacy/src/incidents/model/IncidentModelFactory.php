<?php

namespace src\incidents\model;

use src\framework\model\ModelFactory;
use src\framework\model\exceptions\ModelException;

class IncidentModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        throw new \ModelException('Incident model not yet implemented');
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new IncidentMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        throw new \ModelException('Incident model not yet implemented');
    }
}