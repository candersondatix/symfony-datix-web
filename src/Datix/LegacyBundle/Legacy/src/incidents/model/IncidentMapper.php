<?php

namespace src\incidents\model;

use src\framework\model\Mapper;
use src\framework\query\Query;
use src\framework\model\Entity;
use src\framework\model\exceptions\MapperException;

class IncidentMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\incidents\\model\\Incident';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'incidents_main';
    }
    
    /**
     * {@inheritdoc}
     * 
     * Temporary implementation pending proper modeling of incidents.
     * Returns an array of raw data, rather than an entity.
     */
    public function find($id, $overrideSecurity = false)
    {
        $moduleDefs = $this->registry->getModuleDefs();
        
        $sql = 'SELECT recordid, '.implode(', ', $moduleDefs['INC']['FIELD_ARRAY']).' FROM '.$this->getTable().' WHERE recordid = ?';
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute([$id]);
        $result = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
        
        return $result[0];
    }
    
    /**
     * {@inheritdoc}
     * 
     * Temporary implementation pending proper modeling of incidents.
     */
    public function select(Query $query)
    {
        throw new MapperException('Incident model not yet implemented');
    }
    
    /**
     * {@inheritdoc}
     * 
     * Temporary implementation pending proper modeling of incidents.
     */
    public function insert(Entity $object)
    {
        throw new MapperException('Incident model not yet implemented');
    }
    
    /**
     * {@inheritdoc}
     * 
     * Temporary implementation pending proper modeling of incidents.
     */
    public function update(Entity $object)
    {
        throw new MapperException('Incident model not yet implemented');
    }
    
    /**
     * {@inheritdoc}
     * 
     * Temporary implementation pending proper modeling of incidents.
     */
    public function delete(Entity &$object)
    {
        throw new MapperException('Incident model not yet implemented');
    }
    
    /**
     * {@inheritdoc}
     * 
     * Temporary implementation pending proper modeling of incidents.
     */
    public function save(Entity $object)
    {
        throw new MapperException('Incident model not yet implemented');
    }
}