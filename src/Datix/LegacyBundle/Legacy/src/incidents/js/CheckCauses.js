/*
 * @desc    This function checks the 'root causes' section of an incident, and makes
 *          sure that no more than 40 causes are checked. This is because of the maximum
 *          number of characters supported on the database.
 */
function CheckCauses()
{
    var checkedCount = 0;
    var causes = jQuery('input[name^="incrootcause_"]').filter(':checked');
    checkedCount = causes.length;

    if (checkedCount >= 40)
    {
        DeactivateCauseCheckboxes();
    }
    else
    {
        ActivateCauseCheckboxes();
    }
}

/*
 * @desc    This function deactivates all of the unchecked root cause checkboxes so
 *          that no additional checkboxes can be checked
 */
function DeactivateCauseCheckboxes()
{
    jQuery('input[name^="incrootcause_"]').not(':checked').prop('disabled', true);
}

/*
 * @desc    This function activates all of the root cause checkboxes so that additional
 *          checkboxes can be checked
 */
function ActivateCauseCheckboxes()
{
    jQuery('input[name^="incrootcause_"]').prop('disabled', false);
}

//This is the on ready section that performs the check on load and then attaches the js to the checkboxes
jQuery(function()
{
    CheckCauses();
    jQuery('input[name^="incrootcause_"]').on('change', CheckCauses);
}
);