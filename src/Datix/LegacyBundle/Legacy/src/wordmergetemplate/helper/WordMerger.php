<?php
namespace src\wordmergetemplate\helper;

use src\wordmergetemplate\model\exceptions\InvalidMergeCodeException;
use \src\wordmergetemplate\model\WordMergeTemplate;
use \src\wordmergetemplate\model\WordMergeTemplateModelFactory;
use \src\documents\model\DocumentModelFactory;
use \src\framework\registry\Registry;
/**
 * Used to co-ordinate the merging of a document template and a module record.
 */
class WordMerger
{
    /**
     * @var Object representing the template for data to be merged into
     */
    protected $WordMergeTemplate;

    /**
     * @var Integer representing the id of the record to merge into the template
     */
    protected $recordid;

    /**
     * @var Array holding role-keyed list of contact ids selected by the user for merging.
     */
    protected $selectedContacts = array();

    /**
     * @param \src\wordmergetemplate\model\WordMergeTemplate $WordMergeTemplate the template object to use for merging
     * @param $recordid
     * @param $selectedContacts
     */
    public function __construct(WordMergeTemplate $WordMergeTemplate, $recordid, $selectedContacts)
    {
        $this->WordMergeTemplate = $WordMergeTemplate;
        $this->recordid = $recordid;

        if(!empty($selectedContacts))
        {
            $this->selectedContacts = $selectedContacts;
        }
    }

    /**
     * @return string returns the code for the module associated with the document template linked to this object.
     */
    protected function getModule()
    {
        return $this->WordMergeTemplate->getModule();
    }

    /**
     * @return \src\documents\model\Document A document entity representing the merged document.
     */
    public function merge()
    {
        global $ModuleDefs;

        //create a temporary physical file which will hold the merged document
        $temporaryFile = tempnam(GetParm("PATH_TEMPLATES"), 'zip');

        Registry::getInstance()->getLogger()->logWordMerging('Copying template from '.$this->WordMergeTemplate->getTemporaryFile().' to '. $temporaryFile);

        //Copy the document template into the newly created temporary file: this means that the structure of the
        //docx file will be in place, and we only need to change the contents.
        if (file_exists($temporaryFile) === false)
        {
            throw new \FileNotFoundException('Could not create temporary file.');
        }
        if (!copy(\Sanitize::SanitizeFilePath($this->WordMergeTemplate->getTemporaryFile()), \Sanitize::SanitizeFilePath($temporaryFile)))
        {
            throw new \CopyFileException('File copy failed.');
        }

        $MergeCodes = $this->WordMergeTemplate->getMergeCodes();
        Registry::getInstance()->getLogger()->logWordMerging('Retrieved merge codes: '.var_export($MergeCodes, true));
        $Data = $this->getMergeValues($MergeCodes);
        Registry::getInstance()->getLogger()->logWordMerging('Retrieved merge data: '.var_export($Data, true));

        //Get the xml string representing the merged data.
        $NewXML = $this->getMergedXMLDocument($Data)->saveXML();

        //Push the new xml string into the temporary file.
        $zip = new \ZipArchive();
        if ($zip->open($temporaryFile) !== true)
        {
            throw new \Exception('Zip file failed to open.');
        }
        $zip->addFromString("word/document.xml", $NewXML);
        $zip->close();

        //we need to create a new document record to link the merged template to the main record.
        $Factory = new DocumentModelFactory();

        //This LinkedDocument object will hold all of the meta-data to be saved in documents_main
        //and will be passed back to the calling function for saving.
        $LinkedDocument = $Factory->getEntityFactory()->createObject();

        //TODO: what should this be linked as?
        $LinkedDocument->doc_type = $this->WordMergeTemplate->tem_type;

        //Add a description of the new file based on the name of the template
        $LinkedDocument->doc_notes = \DatixDBQuery::PDO_fetch(
            'SELECT tem_notes
            FROM templates_main
            WHERE tem_id = :tem_id',
            array('tem_id' => $this->WordMergeTemplate->tem_id),
            \PDO::FETCH_COLUMN).' (merged on '.FormatDateVal(date('Y-m-d h:i:s.000')).')';

        //Identify linked record id
        $LinkedDocument->$ModuleDefs[$this->getModule()]['FK'] = $this->recordid;

        Registry::getInstance()->getLogger()->logWordMerging('Saving to documents_main with id '.$LinkedDocument->doc_id);

        $Factory->getMapper()->insert($LinkedDocument, $temporaryFile, 'docx');
    }

    /**
     * Takes the xml part of the docx file associated with the WordMergeTemplate object and replaces all MERGEFIELD
     * instances with data from the provided data array.
     *
     * @param $Data
     * @return \DOMDocument
     */
    protected function getMergedXMLDocument($Data)
    {
        $Document = $this->WordMergeTemplate->getXMLDocument();

        //Loop recursively through the xml looking for MERGEFIELD tags and replacing them with text tags containing
        //the data from the db record.
        $this->RecursiveMergecodeReplace($Document->documentElement, $Data, $Document, $deleteRows, $deleteNextRow);

        return $Document;
    }

    /**
     * Loops recursively through an xml (sub-)tree looking for MERGEFIELD tags and replacing them with text tags containing
     * the data from the provided array. Then calls itself on all further sub-trees.
     *
     * @param $domNode \DomNode The subtree to act on.
     * @param $data array The data to merge in.
     * @param $domDocument \DomDocument The main DomDocument object (needed to create new nodes).
     * @param $deleteRows bool An instruction to delete w:r nodes when encountered.
     */
    function RecursiveMergecodeReplace(&$domNode, $data, $domDocument, &$deleteRows, &$deleteNextRow)
    {
        $blockNextDeletion = false;

        //need to loop backwards here because we're altering the DOM tree as we go.
        if ($domNode->childNodes->length > 0)
        {
            for ($i = $domNode->childNodes->length; --$i >= 0; )
            {
                $childNode = $domNode->childNodes->item($i);

                // If we're in "delete rows mode", we need to be clearing out w:r nodes after we've processed them
                if (($deleteNextRow || $deleteRows) && !$blockNextDeletion)
                {
                    $nodeToDelete = $childNode->nextSibling;

                    if ($nodeToDelete && $nodeToDelete->nodeName=='w:r')
                    {
                        $deleteRow = true;

                        foreach($nodeToDelete->attributes as $attribute)
                        {
                            //This attribute is set on the new node created to hold the substituted text.
                            if ($attribute->name == 'datixNoDelete')
                            {
                                $deleteRow = false;
                                $nodeToDelete->removeAttribute('datixNoDelete');
                            }
                        }

                        if ($deleteRow)
                        {
                            $nodeToDelete->parentNode->removeChild($nodeToDelete);
                        }
                    }
                }

                $blockNextDeletion = false;
                $deleteNextRow = false;

                $content = null;

                /*
                 * There are two ways that a merge code can appear: as an fldSimple tag, or between two fldChar nodes.
                 * In the latter case, we need to clean up a few of the surrounding nodes once the replacement
                 * has been made.
                 */
                if($childNode->nodeName=='w:fldSimple')
                {
                    foreach($childNode->attributes as $attribute)
                    {
                        if($attribute->name == 'instr' && preg_match('/MERGEFIELD([!A-Za-z0-9_:\s\"]*)/iu', \UnicodeString::trim(strval($attribute->value)), $matches))
                        {
                            $content = $data[\UnicodeString::strtoupper(\UnicodeString::trim(str_replace('"', '', $matches[1])))];
                        }
                    }

                    // wrap existing merge field in w:r (run) tags
                    $r = $domDocument->createElement('w:r');
                    $mergeField = $r->appendChild($childNode->cloneNode());
                    $domNode->insertBefore($r, $childNode);
                    $domNode->removeChild($childNode);
                    
                    $this->replaceMergeField($domDocument, $mergeField, $content);
                }
                else if ($childNode->nodeName=='w:instrText' && preg_match('/MERGEFIELD([!A-Za-z0-9_:\s\"]*)/iu', \UnicodeString::trim(strval($childNode->textContent)), $matches))
                {
                    $content = $data[\UnicodeString::strtoupper(\UnicodeString::trim(str_replace('"', '', $matches[1])))];
                    
                    $this->replaceMergeField($domDocument, $childNode, $content);

                    $domNode->setAttribute('datixNoDelete', '1');

                    $blockNextDeletion = true;
                }
                else if ($childNode->nodeName=='w:fldChar')
                {
                    /**
                     * 'end' and 'begin' are the opposite way round because we are moving backwards through the DOM tree.
                     */
                    foreach($childNode->attributes as $attribute)
                    {
                        if ($attribute->name == 'fldCharType' && $attribute->value == 'end')
                        {
                            $deleteRows = true;
                        }
                        else if ($attribute->name == 'fldCharType' && $attribute->value == 'begin')
                        {
                            $deleteRows = false;
                            $deleteNextRow = true;
                        }
                    }
                }
                else
                {
                    $this->RecursiveMergecodeReplace($childNode, $data, $domDocument, $deleteRows, $deleteNextRow);
                }
            }
        }
    }
    
    /**
     * Processes an individual merge field by replacing it with record data.
     * 
     * Handles the creation of line break markup.
     * 
     * @param \DOMDocument $document   The object used to create the DOMNodes.
     * @param \DOMNode     $mergeField The merge field we're replacing.
     * @param string       $content    The content being merged onto the document.
     */
    protected function replaceMergeField(\DOMDocument $document, \DOMNode $mergeField, $content)
    {
        $parent = $mergeField->parentNode;
        
        if (!($parent instanceof \DOMNode))
        {
            return; // merge field will be left on the document in this situation
        }
        
        $lines = explode("\r\n", $content);
        $numLines = count($lines);
        
        foreach ($lines as $key => $line)
        {
            $textNode = $document->createTextNode($line);
            $t = $document->createElement('w:t');
            $t->appendChild($textNode);
            $parent->insertBefore($t, $mergeField);
            
            if (($key + 1) < $numLines)
            {
                $parent->insertBefore($document->createElement('w:br'), $mergeField);
            }
        }
        
        $parent->removeChild($mergeField);
    }

    /**
     * Interprets an array of mergecodes, calling the functions needed to populate them with data and returning
     * an array that can be used to merge the data back into the template file.
     *
     * @param $MergeFields array Array of the raw mergecode strings from a template file
     * @return array Array of data mapping mergecode strings to values.
     */
    function getMergeValues($MergeFields)
    {
        $ModuleDefs = Registry::getInstance()->getModuleDefs();

        if($MergeFields != "")
        {
            foreach($MergeFields as $FullMergeCode)
            {
                if (\UnicodeString::strpos($FullMergeCode, 'EXTRA') !== false)
                {
                    // extra fields merge code
                    $FieldsToMerge['UDF'][$FullMergeCode] = $FullMergeCode;
                }
                elseif (\UnicodeString::strpos($FullMergeCode, '!TODAY') !== false)
                {
                    // todays date merge code
                    $FieldsToMerge['TodaysDate'][$FullMergeCode] = $FullMergeCode;
                }
                else
                {
                    //Remove formatting before using db field name.
                    $SplitFieldName = explode(' ', $FullMergeCode);


                    //TODO: This is all a bit of a mess now - should be refactored using new FieldDef object.

                    $DBFieldName = array_shift($SplitFieldName);

                    $Table = '';
                    $noTableProvided = true;
                    $FieldObject = null;

                    foreach($SplitFieldName as $AttributeValue)
                    {
                        list($attribute, $value) = explode(':',$AttributeValue);
                        if(\UnicodeString::strtoupper($attribute) == 'TABLE')
                        {
                            $Table = $value;
                        }
                    }

                    // If no table manually provided, assume the main module table.
                    if(!$Table)
                    {
                        $Table = $ModuleDefs[$this->getModule()]['TABLE'];
                    }
                    else
                    {
                        $noTableProvided = false;
                    }

                    try
                    {
                        $FieldObject = new \Fields_Field($DBFieldName, $Table);
                    }
                    catch(\Exception $e)
                    {
                        try
                        {
                            if ($noTableProvided)
                            {
                                //Couldn't find table-specific field, so look for fields from other tables.
                                $FieldObject = new \Fields_Field($DBFieldName);
                            }
                            else
                            {
                                //field not recognised. For now, just ignore it.
                                $Data[$FullMergeCode] = '';
                            }
                        }
                        catch(\Exception $e)
                        {
                            //field not recognised. For now, just ignore it.
                            $Data[$FullMergeCode] = '';
                        }
                    }

                    if ($FieldObject)
                    {
                        if($FieldObject->getTable() == $ModuleDefs[$this->getModule()]['TABLE'])
                        {
                            $FieldsToMerge['Main'][] = $FullMergeCode;
                        }
                        elseif($FieldObject->getTable() == 'link_compl')
                        {
                            if ($this->getModule() != 'COM')
                            {
                                throw new InvalidMergeCodeException('The merge code "'.$FullMergeCode.'" cannot be used in this module');
                            }

                            $FieldsToMerge['Link'][] = $FullMergeCode;
                        }
                        elseif(($FieldObject->getTable() == 'link_contacts' || $FieldObject->getTable() == 'contacts_main' || $FieldObject->getTable() == 'staff') && $this->getModule() != 'CON')
                        {
                            $FieldsToMerge['Link'][] = $FullMergeCode;
                        }
                    }
                } // end of if check for extra fields else branch
            } // end of foreach
        } // end of if

        ///////////////////////////////////////////////////////////////////////////////////

        if(!empty($FieldsToMerge['Main']))
        {
            $Data = $this->getMainTableColVal($FieldsToMerge['Main'], $Data);
        }

        if(!empty($FieldsToMerge['Link']))
        {
            $Data = $this->getLinkedTableColVal($FieldsToMerge['Link'], $Data);
        }

        if(isset($FieldsToMerge['UDF']))
        {
            $Data = $this->getExtraFieldsVal($FieldsToMerge['UDF'], $Data);
        }

        if(isset($FieldsToMerge['TodaysDate']))
        {
            $Data = $this->getTodaysDate($FieldsToMerge['TodaysDate'], $Data);
        }

        return $Data;
    }

    /**
     * Collects data for merge codes related to today's date.
     *
     * @param $FieldsToMerge array Array of the raw mergecode strings from a template file
     * @param $Data array Array of current data values for the discovered values here to be appended to.
     * @return array updated data array
     */
    protected function getTodaysDate($FieldsToMerge, $Data)
    {
        foreach($FieldsToMerge as $FieldToMerge)
        {
            list($date, $format) = explode('_', $FieldToMerge);

            switch($format)
            {
                case 'SHORT':
                    $Data[$FieldToMerge] = FormatDateVal(date('Y-m-d H:i:s.000'));
                    break;
                case 'LONG':
                default:
                    if(GetParm("FMT_DATE_WEB") == "US")
                    {
                        $Data[$FieldToMerge] = date("F j Y");
                    }
                    else
                    {
                        $Data[$FieldToMerge] = date("j F Y");
                    }
                    break;
            }
        }

        return $Data;
    }

    /**
     * Collects data for merge codes related to the main record table.
     *
     * This funtion queries the database to collect data and organises the merge codes and the returned values
     * to be later processed from an array.
     *
     * @param $FieldsToMerge array Array of the raw mergecode strings from a template file
     * @param $Data array Array of current data values for the discovered values here to be appended to.
     * @return array updated data array
     */
    function getMainTableColVal($FieldsToMerge, $Data)
    {
        $ModuleDefs = Registry::getInstance()->getModuleDefs();

        $selecttables[0] = $ModuleDefs[$this->getModule()]['TABLE'];
        $selectfields = array();

        foreach($FieldsToMerge as $FieldToMerge)
        {
            $FieldRef[$FieldToMerge]['FULL'] = $FieldToMerge;

            //This merge code will be of the form FIELDNAME [ATTRIBUTE1:VALUE1] [ATTRIBUTE2:VALUE2]...
            $SplitField = explode(' ', $FieldToMerge);

            $FieldRef[$FieldToMerge]['DB'] = \UnicodeString::strtolower(\UnicodeString::trim(array_shift($SplitField)));

            foreach($SplitField as $attributevalue)
            {
                list($attribute, $value) = explode(':', $attributevalue);
                $FieldRef[$FieldToMerge]['DETAILS'][$attribute] = $value;
            }

            $FieldRef[$FieldToMerge]['OBJ'] = new \Fields_Field($FieldRef[$FieldToMerge]['DB'], $ModuleDefs[$this->getModule()]['TABLE']);
            $dbFields[] = $FieldRef[$FieldToMerge]['DB'];

        }

        foreach($FieldsToMerge as $FieldToMerge)
        {
            $selectfields[$FieldRef[$FieldToMerge]['OBJ']->getName()] = $FieldRef[$FieldToMerge]['OBJ']->getTable() . '.' . $FieldRef[$FieldToMerge]['OBJ']->getName();
            if(!in_array($FieldRef[$FieldToMerge]['OBJ']->getTable(), $selecttables))
            {
                if ($row['mer_table'] = 'rev_main')
                {
                    $selecttables[] = $FieldRef[$FieldToMerge]['OBJ']->getTable() . " ON " . $FieldRef[$FieldToMerge]['OBJ']->getTable() . '.link_id = ' . $ModuleDefs[$this->getModule()]['TABLE'] . '.recordid' .
                        " AND link_module = :link_module";

                    $selectParams['link_module'] = $this->getModule();
                }
                else
                {
                    $selecttables[] = $FieldRef[$FieldToMerge]['OBJ']->getTable() . " ON " . $FieldRef[$FieldToMerge]['OBJ']->getTable() . "." . $ModuleDefs[$this->getModule()]['FK'] . ' = ' .
                        $ModuleDefs[$this->getModule()]['TABLE'] . '.recordid';
                }
            }
        }

        if(!empty($selectfields) && !empty($selecttables))
        {
            $sql = "SELECT ";
            $sql .= implode_select_as_key(' ,', $selectfields);
            $sql .= " FROM ";
            $sql .= implode(' LEFT JOIN ', array_unique($selecttables));
            $sql .= " WHERE ".$ModuleDefs[$this->getModule()]['TABLE'].".recordid = :recordid";

            $selectParams['recordid'] = $this->recordid;

            $row = \DatixDBQuery::PDO_fetch($sql, $selectParams);
        }

        if(is_array($FieldsToMerge))
        {
            foreach($FieldsToMerge as $FieldToMerge)
            {
                $Data[$FieldToMerge] = $this->GetMergeCodeDescr($row[$FieldRef[$FieldToMerge]['DB']],$FieldRef[$FieldToMerge]);
            }
        }

        return $Data;
    }

    /**
     * Collects data for merge codes related to linked contact tables.
     *
     * This function queries the database to collect data and organises the merge codes and the returned values
     * to be later processed from an array.
     *
     * @param $FieldsToMerge array Array of the raw mergecode strings from a template file
     * @param $Data array Array of current data values for the discovered values here to be appended to.
     * @return array updated data array
     */
    function getLinkedTableColVal($FieldsToMerge, $Data)
    {
        $ModuleDefs = Registry::getInstance()->getModuleDefs();

        $selecttables_linkcon = array();
        $selectfields_linkcon = array();

        foreach($FieldsToMerge as $FieldToMerge)
        {
            $FieldRef[$FieldToMerge]['FULL'] = $FieldToMerge;

            //This merge code will be of the form FIELDNAME [ATTRIBUTE1:VALUE1] [ATTRIBUTE2:VALUE2]...
            $SplitField = explode(' ', $FieldToMerge);

            $FieldRef[$FieldToMerge]['DB'] = \UnicodeString::trim(array_shift($SplitField));

            foreach($SplitField as $attributevalue)
            {
                list($attribute, $value) = explode(':', $attributevalue);
                $FieldRef[$FieldToMerge]['DETAILS'][$attribute] = $value;
            }

            $FieldRef[$FieldToMerge]['OBJ'] = new \Fields_Field($FieldRef[$FieldToMerge]['DB'], $FieldRef[$FieldToMerge]['DETAILS']['TABLE']);
            $dbFields[] = $FieldRef[$FieldToMerge]['DB'];
        }

        foreach($FieldsToMerge as $FieldToMerge)
        {
            if($FieldRef[$FieldToMerge]['OBJ']->getTable() == 'link_compl')
            {
                $selectfields_linkcon[$FieldToMerge] = $FieldRef[$FieldToMerge]['OBJ']->getTable() . '.' . $FieldRef[$FieldToMerge]['OBJ']->getName();

                if($FieldRef[$FieldToMerge]['OBJ']->getTable() == 'link_compl')
                {
                    if(!in_array('compl_main', $selecttables_linkcon))
                    {
                        $selecttables_linkcon[] = 'compl_main';
                    }
                    if(!in_array('link_compl', $selecttables_linkcon))
                    {
                        $selecttables_linkcon[] = 'link_compl';
                    }
                }
            }
            elseif(($FieldRef[$FieldToMerge]['OBJ']->getTable() == 'link_contacts' || $FieldRef[$FieldToMerge]['OBJ']->getTable() == 'staff' || $FieldRef[$FieldToMerge]['OBJ']->getTable() == 'contacts_main') && $this->getModule() != 'CON')
            {
                if($FieldRef[$FieldToMerge]['OBJ']->getTable() == 'staff')
                {
                    //nothing from "staff" is needed - we can treat is as contacts_main for simplicity.
                    $selectfields_linkcon[$FieldToMerge] = 'contacts_main.' . $FieldRef[$FieldToMerge]['OBJ']->getName();
                }
                else
                {
                    $selectfields_linkcon[$FieldToMerge] = $FieldRef[$FieldToMerge]['OBJ']->getTable() . '.' . $FieldRef[$FieldToMerge]['OBJ']->getName();
                }

                if($FieldRef[$FieldToMerge]['OBJ']->getTable() == 'contacts_main' || $FieldRef[$FieldToMerge]['OBJ']->getTable() == 'staff')
                {
                    if(!in_array('contacts_main', $selecttables_linkcon))
                    {
                        $selecttables_linkcon[] = 'contacts_main';
                    }
                    if(!in_array('link_contacts', $selecttables_linkcon))
                    {
                        $selecttables_linkcon[] = 'link_contacts';
                    }
                }
                elseif($FieldRef[$FieldToMerge]['OBJ']->getTable() == 'link_contacts')
                {
                    if(!in_array('link_contacts', $selecttables_linkcon))
                    {
                        $selecttables_linkcon[] = 'link_contacts';
                    }
                }
            }
        }

        $MainTable = $ModuleDefs[$this->getModule()]['TABLE'];

        if(is_array($selectfields_linkcon))
        {
            $sql = "SELECT ";

            if(is_array($selectfields_linkcon))
            {
                $sql .= implode_select_as_key(' ,', $selectfields_linkcon);
            }

            $sql .= ", link_contacts.link_role as LINK_ROLE_SWITCH";
            $sql .= ", contacts_main.recordid as CONTACT_RECORDID";

            $sql .= " FROM ";

            if(!in_array($MainTable, $selecttables_linkcon))
            {
                $selecttables_linkcon[] = $MainTable;
            }

            $sql .= 'contacts_main';

            if(in_array('link_contacts', $selecttables_linkcon) || true)
            {
                $sql .= " LEFT JOIN link_contacts on contacts_main.recordid = link_contacts.con_id";
                $sql .= " LEFT JOIN ".$MainTable." on ".$MainTable.".recordid = link_contacts.".$ModuleDefs[$this->getModule()]['FK'];
            }
            if(in_array('link_compl', $selecttables_linkcon))
            {
                $sql .= " LEFT JOIN link_compl on compl_main.recordid = link_compl.com_id AND link_compl.con_id = link_contacts.con_id AND lcom_current = 'Y'";
            }

            $sql .= " WHERE ".$MainTable.".recordid = :recordid";

            if(in_array('contacts_main', $selecttables_linkcon))
            {
                $sql .= " ORDER BY contacts_main.con_surname ASC, contacts_main.con_forenames ASC, contacts_main.recordid ASC";
            }

            $LinkedContacts = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $this->recordid));

            foreach($LinkedContacts as $LinkedContact)
            {
                $LinkedContactsByRole[$LinkedContact['LINK_ROLE_SWITCH']][] = $LinkedContact;

                //selected contacts are passed by id, but need to be referenced by position in the next loop. Hence this slightly clumsy conversion:
                if($this->selectedContacts[$LinkedContact['LINK_ROLE_SWITCH']] == $LinkedContact['CONTACT_RECORDID'])
                {
                    $OrderedSelectedContacts[$LinkedContact['LINK_ROLE_SWITCH']] = count($LinkedContactsByRole[$LinkedContact['LINK_ROLE_SWITCH']])-1;
                }
            }

            //As above, need to convert selected contact with no role to be referenced by position, rather than id.
            if($this->selectedContacts['NO_LINK_ROLE'])
            {
                foreach($LinkedContacts as $index => $LinkedContact)
                {
                    if($this->selectedContacts['NO_LINK_ROLE'] == $LinkedContact['CONTACT_RECORDID'])
                    {
                        $OrderedSelectedContacts['NO_LINK_ROLE'] = $index;
                    }
                }
            }

            if(count($LinkedContacts) > 0)
            {
                foreach($FieldsToMerge as $FullMergeCode)
                {
                    if($FieldRef[$FullMergeCode]['DETAILS']['ROLE'])
                    {
                        if ($FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'])
                        {
                            //role and contact number both specified, so use that contact if it exists
                            if (isset($LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']][intval($FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'])-1]))
                            {
                                Registry::getInstance()->getLogger()->logWordMerging('Role ('.$FieldRef[$FullMergeCode]['DETAILS']['ROLE'].') and contact number ('.$FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'].') both specified, so using value '.$LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']][intval($FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'])-1][$FullMergeCode]);
                                $Data[$FullMergeCode] = $LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']][intval($FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'])-1][$FullMergeCode];
                            }
                        }
                        else if (count($LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']]) > 1)
                        {
                            //A role is specified, but not a contact number, and there is more than one contact for this role.

                            if(!isset($OrderedSelectedContacts[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']]))
                            {
                                throw new \Exception('Contact not correctly identified for merge.');
                            }

                            Registry::getInstance()->getLogger()->logWordMerging('Role ('.$FieldRef[$FullMergeCode]['DETAILS']['ROLE'].') specified, but no contact number. User chose option '.$OrderedSelectedContacts[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']].' so using value '.$LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']][intval($FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'])-1][$FullMergeCode]);
                            $Data[$FullMergeCode] = $LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']][$OrderedSelectedContacts[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']]][$FullMergeCode];
                        }
                        else if (count($LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']]) > 0)
                        {
                            //Role specified and only one of that role exists, so just use that contact.
                            Registry::getInstance()->getLogger()->logWordMerging('Role ('.$FieldRef[$FullMergeCode]['DETAILS']['ROLE'].') specified and only one contact, so using value '.$LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']][intval($FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'])-1][$FullMergeCode]);
                            $Data[$FullMergeCode] = $LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']][0][$FullMergeCode];
                        }
                    }
                    else if(count($LinkedContacts) > 1)
                    {
                        if(!isset($OrderedSelectedContacts['NO_LINK_ROLE']))
                        {
                            throw new \Exception('Contact not correctly identified for merge.');
                        }

                        //No role specified, and more than one possible contact
                        $Data[$FullMergeCode] = $LinkedContacts[$OrderedSelectedContacts['NO_LINK_ROLE']][$FullMergeCode];
                        Registry::getInstance()->getLogger()->logWordMerging('Role not specified, and no contact number. User chose option '.$OrderedSelectedContacts['NO_LINK_ROLE'].' so using value '.$Data[$FullMergeCode]);
                    }
                    else
                    {
                        //Only one contact linked
                        $Data[$FullMergeCode] = $LinkedContacts[0][$FullMergeCode];
                        Registry::getInstance()->getLogger()->logWordMerging('Role not specified, and no contact number. Only one possible contact, so using value '.$Data[$FullMergeCode]);
                    }

                    //replace code with description if needed.
                    if($Data[$FullMergeCode] != '')
                    {
                        $Data[$FullMergeCode] = $this->GetMergeCodeDescr($Data[$FullMergeCode], $FieldRef[$FullMergeCode]);
                    }
                }
            }
        } // end if is_array

        return $Data;
    }

    /**
     * TODO: refactor this - it is copied from a legacy file.
     *
     * Formats values for dates and coded fields. Still not ideally structured, since it is based off the previous bloated implmentation.
     *
     * @param string $FieldValue The current column value from the DB.
     * @param array $MergeDetails An array containing meta information about this merge code.
     *
     * @return string the new value, formatted to be inserted into the document
     */
    function GetMergeCodeDescr($FieldValue, $MergeDetails)
    {
        if($MergeDetails['OBJ']->getFieldType() == 'D')
        {
            switch($MergeDetails['DETAILS']['FORMAT'])
            {
                case 'SHORT':
                    $FieldValue = FormatDateVal(($FieldValue));
                    break;
                case 'LONG':
                default:
                    if($FieldValue != '')
                    {
                        $FieldValue = date("j F Y", strtotime($FieldValue));
                    }
                    break;
            }
        }
        else if($MergeDetails['OBJ']->getFieldType() == 'C')
        {
            if($MergeDetails['DETAILS']['FORMAT'] != 'CODE')
            {
                $MergeDetails['OBJ']->setValue($FieldValue);
                $MergeDetails['OBJ']->getDisplayInfo();
                $FieldValue = $MergeDetails['OBJ']->getDescription();
            }
        }

        return $FieldValue;
    }

    /**
     * Collects data for extra field merge codes.
     *
     * This funtion queries the database to collect data and organises the merge codes and the returned values
     * to be later processed as an array.
     *
     * @param array $ExtraFieldMergeCodes The extra fields to be selected in the SQL query.
     * @param string $Data Array of data for the extra field values to be appended to.
     *
     * @return array An array of data including extra field values.
     */
    function getExtraFieldsVal($ExtraFieldMergeCodes, $Data)
    {
        if(is_array($ExtraFieldMergeCodes))
        {
            $aExtraFields = $this->GetAllMergeExtraFields();

            foreach($ExtraFieldMergeCodes as $key => $val)
            {
                if(\UnicodeString::strpos($key, 'EXTRA') !== false)
                {
                    //This merge code will be of the form FIELDNAME [ATTRIBUTE1:VALUE1] [ATTRIBUTE2:VALUE2]...
                    $SplitField = explode(' ', $key);

                    $UDF_reference = array_shift($SplitField);

                    foreach($SplitField as $attributevalue)
                    {
                        list($attribute, $value) = explode(':', $attributevalue);
                        $FieldRef[$key]['DETAILS'][\UnicodeString::strtoupper($attribute)] = $value;
                    }

                    if($FieldRef[$key]['DETAILS']['GROUP'] == '')
                    {
                        $FieldRef[$key]['DETAILS']['GROUP'] = '0';
                    }

                    if($UDF_reference != '')  //EXTRAX
                    {
                        $UDF_ID = \UnicodeString::substr($UDF_reference, 5);

                        if($FieldRef[$key]['DETAILS']['FORMAT'] == 'CODE' || $FieldRef[$key]['DETAILS']['FORMAT'] == 'SHORT') // get code (or short date... don't ask!)
                        {
                            $Data[$key] = $aExtraFields[$UDF_ID][$FieldRef[$key]['DETAILS']['GROUP']]['VALUE_C'];
                        }
                        else // get description
                        {
                            $Data[$key] = $aExtraFields[$UDF_ID][$FieldRef[$key]['DETAILS']['GROUP']]['VALUE'];
                        }
                    }
                }
            }
        }
        return $Data;
    }

    /**
     * @desc Retrieves all the extra fields that have data associated with this record and keys them on field_id.
     *
     * @param string $Mod Module code.
     * @param int $RecordID The recordid of the main record.
     *
     * @return array $udfArray Returns an array of extra fields with data, description and extra field name.
     */
    function GetAllMergeExtraFields()
    {
        global $ModuleDefs;

        // Get the UDF value
        $sql = "
            SELECT
                udf_fields.fld_type,
                udf_fields.fld_name,
                udf_fields.fld_format,
                udf_fields.recordid as fld_id,

                udf_values.group_id,

                udf_values.udv_string,
                udf_values.udv_number,
                udf_values.udv_date,
                udf_values.udv_money,
                udf_values.udv_text
            FROM
                udf_values
            LEFT JOIN
                udf_fields ON udf_fields.recordid = udf_values.field_id
            WHERE
                udf_values.mod_id = :ModID AND
                udf_values.cas_id = :CasID";

        $PDOParams = array(
            'ModID' => $ModuleDefs[$this->getModule()]['MOD_ID'],
            'CasID' => $this->recordid,
        );

        $UDFValues = \DatixDBQuery::PDO_fetch_all($sql, $PDOParams);

        $udfArray = array();
        foreach($UDFValues as $UDFValue)
        {
            if ($UDFValue['group_id'] == '')
            {
                $UDFValue['group_id'] = '0';
            }

            if(!isset($udfArray[$UDFValue['fld_id']][$UDFValue['group_id']]))
            {
                $UDFDisplayValue = $this->GetUDFFieldValue($UDFValue);

                $udfArray[$UDFValue['fld_id']][$UDFValue['group_id']] = $UDFDisplayValue;

                //add a value to the "0" group in case no group is provided on the template
                if(!isset($udfArray[$UDFValue['fld_id']]['0']))
                {
                    $udfArray[$UDFValue['fld_id']]['0'] = $UDFDisplayValue;
                }
            }
        }

        return $udfArray;
    }


    /**
     * TODO: Refactor the process of getting the udf data - it is hopeless at the moment.
     *
     * @desc Gets the value, the description and the field name of an extra field.
     * A rough copy of MakeUDFField() method of FormTable class.
     *
     * @param array $UDFProperties Properties of the extra field e.g. name, id, type and format.
     * @param array $UDFValue Array of udf value columns from udf_values table
     *
     * @return array Value, description and the name of the extra field in an array format.
     */
    function GetUDFFieldValue($UDFValue)
    {
        $HasValue = false;
        if (!empty($UDFValue) && is_array($UDFValue))
        {
            if(
                $UDFValue['udv_string'] != '' ||
                $UDFValue['udv_number'] != '' ||
                $UDFValue['udv_date'] != '' ||
                $UDFValue['udv_text'] != '' ||
                $UDFValue['udv_money'] != ''
            )
            {
                $HasValue = true;
            }
        }

        $FieldValue = '';
        $FieldDescr = '';

        switch ($UDFValue['fld_type'])
        {
            case "S":
            case "E":
                if ($HasValue === true)
                {
                    $FieldValue = $UDFValue["udv_string"];
                    $FieldDescr = $FieldValue;
                }
                break;
            case "D":
                if ($HasValue === true)
                {
                    $FieldValue = FormatDateVal($UDFValue["udv_date"]);

                    if(GetParm("FMT_DATE_WEB") == "US")
                    {
                        $FieldDescr = date("F j Y", strtotime($UDFValue["udv_date"]));

                    }
                    else
                    {
                        $FieldDescr = date("j F Y", strtotime($UDFValue["udv_date"]));
                    }
                }
                break;
            case "C":
                if ($HasValue === true)
                {
                    $FieldValue = $UDFValue["udv_string"];
                    $FieldDescr = code_descr_udf($UDFValue["fld_id"], $FieldValue, $this->getModule());
                }
                break;
            case "N":
                if ($HasValue === true)
                {
                    //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                    if ($UDFValue['fld_format'] != "")
                    {
                        $FormattedNumber = GuptaFormatEmulate($UDFValue["udv_number"], $UDFValue['fld_format']);
                    }
                    else
                    {
                        $FormattedNumber = number_format($UDFValue["udv_number"], 2, '.', '');
                    }
                    $FieldValue = $FormattedNumber;
                    $FieldDescr = $FieldValue;
                }
                break;
            case "M":
                if ($HasValue === true)
                {
                    //If format specified in UDF setup, then try and emulate. Otherwise use default format.
                    if ($UDFValue['fld_format'] != "")
                    {
                        $FormattedNumber = GuptaFormatEmulate($UDFValue["udv_money"], $UDFValue['fld_format']);
                    }
                    else
                    {
                        $FormattedNumber = number_format($UDFValue["udv_money"], 2, '.', '');
                    }
                    $FieldValue = $FormattedNumber;
                    $FieldDescr = $FieldValue;
                }
                break;
            case "T":
                if ($HasValue === true)
                {
                    $ValueArray = explode(' ', $UDFValue["udv_string"]);
                    foreach($ValueArray as $Value)
                    {
                        $DescriptionArray[] = code_descr_udf($UDFValue["fld_id"], $Value, $this->getModule());
                    }
                    $FieldDescr = implode(', ', $DescriptionArray);
                    $FieldValue = $UDFValue["udv_string"];
                }
                break;
            case "Y":
                if ($HasValue === true)
                {
                    $FieldValue = $UDFValue["udv_string"];
                    $FieldDescr = code_descr_udf($UDFValue["fld_id"], $FieldValue, $this->getModule());
                }
                break;
            case "L":
                if ($HasValue === true)
                {
                    $FieldValue = $UDFValue["udv_text"];
                    $FieldDescr = $FieldValue;
                }
                break;
        }

        return array(
            'VALUE_C' => $FieldValue,
            'VALUE' => $FieldDescr,
        );
    }
}
