<?php
namespace src\wordmergetemplate\controllers;

use src\framework\controller\Controller;
use src\wordmergetemplate\model\WordMergeTemplateModelFactory;

class DocumentTemplatesController extends Controller
{
    public function doctemplateaction()
    {
        $form_action = \Sanitize::SanitizeString($this->request->getParameter('form_action'));
        $tem_doc_id = \Sanitize::SanitizeInt($this->request->getParameter('tem_doc_id'));
        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));

        switch($form_action)
        {
            case 'edit':
                $this->DocTemplateAdminEdit($tem_doc_id, $module);
                break;
            case 'delete':
                $this->DocTemplateAdminDelete($tem_doc_id, $module);
                break;
            case 'new':
                $this->DocTemplateAdminNew($module);
                break;
            case 'cancel':
                $this->redirect('app.php?module=ADM');
                break;
            default:
                //error
                $this->redirect('app.php?module=ADM');
                break;
        }
    }

    /**
     * Shows the new form for Document Templates
     *
     * @param string $module The module acronym.
     */
    private function DocTemplateAdminNew($module)
    {
        $this->response = $this->call('src\wordmergetemplate\controllers\DocumentTemplatesTemplateController', 'DocumentTemplateForm', array(
            'tem_doc_id' => '',
            'module' => $module,
            'form_action' => 'new'
        ));
    }

    /**
     * Shows the Document Template form with the template that we are editing.
     *
     * @param int $tem_doc_id The Document Template id.
     * @param string $module The module acronym.
     */
    private function DocTemplateAdminEdit($tem_doc_id, $module)
    {
        $Factory = new WordMergeTemplateModelFactory();

        if ($tem_doc_id != '')
        {
            $DocumentTemplate = $Factory->getMapper()->find($tem_doc_id);
            $DocumentTemplate->getPath();
        }

        $this->response = $this->call('src\wordmergetemplate\controllers\DocumentTemplatesTemplateController', 'DocumentTemplateForm', array(
            'tem_doc_id' => $tem_doc_id,
            'module' => $module,
            'form_action' => 'edit',
            'rep' => $DocumentTemplate
        ));
    }

    /**
     * Deletes a Document Template for a specified module.
     */
    private function DocTemplateAdminDelete()
    {
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('tem_doc_id'));
        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));

        $sql = 'DELETE FROM templates_main WHERE recordid = :recordid';

        if (!\DatixDBQuery::PDO_query($sql, array('recordid' => $recordid)))
        {
            fatal_error('Could not delete template');
        }
        else
        {
            AddSessionMessage('INFO', 'Template has been deleted.');
        }

        $this->redirect('app.php?action=doctemplateadminlist&module='. $module);
    }

    /**
     * Outputs Document template settings form panel
     */
    public function MakeDocumentTemplateAdminEditPanel()
    {
        $rep = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('form_action');
        $module = $this->request->getParameter('module');

        $TemplateType = array(
            'FORM' => 'Report form',
            'INV' => 'Invoice',
            'LETR' => 'Letter',
            'MEMO' => 'Memorandum',
            'REPL' => 'Reply to query',
            'SECUR' => 'Secure doc'
        );

        $FormFieldTemNotes = new \FormField();
        $FormFieldTemNotes->MakeInputField('tem_notes', 70, 256, $rep['tem_notes'], '');

        $FieldTemType = \Forms_SelectFieldFactory::createSelectField('tem_type', $module, $rep['tem_type'], $FormType);
        $FieldTemType->setCustomCodes($TemplateType);

        if ($rep['path'] != '')
        {
            $FormFieldPath = new \FormField();
            $FormFieldPath->MakeCustomField($rep['path']);

            $FormFieldUserFile = new \FormField();
            $FormFieldUserFile->MakeCustomField('<input name="userfile" type="file"  size="50"/>');
        }
        else
        {
            $FormFieldUserFile = new \FormField();
            $FormFieldUserFile->MakeCustomField('<input name="userfile" type="file"  size="50"/>');
        }

        $this->response->build('src/wordmergetemplate/views/DocumentTemplateAdminEditPanel.php', array(
            'FormFieldTemNotes' => $FormFieldTemNotes,
            'FieldTemType' => $FieldTemType,
            'rep' => $rep,
            'FormFieldPath' => $FormFieldPath,
            'FormFieldUserFile' => $FormFieldUserFile
        ));
    }

    /**
     * Outputs Permissions form panel
     */
    public function RecordPermissionsDocTemplate()
    {
        require_once 'Source/security/SecurityRecordPerms.php';

        $rep = $this->request->getParameter('data');
        $FormAction = $this->request->getParameter('form_action');

        $this->response->build('src/wordmergetemplate/views/PermissionsPanel.php', array(
            'rep' => $rep,
            'FormAction' => $FormAction
        ));
    }

    public function doctemplateadmineditsave()
    {
        if ($this->request->getParameter('rbWhat') == 'cancel')
        {
            $this->response = $this->call('src\admin\controllers\DocTemplatesAdminListTemplateController', 'doctemplateadminlist');
            return;
        }
        elseif ($this->request->getParameter('rbWhat') == 'delete')
        {
            $this->response = $this->call('src\wordmergetemplate\controllers\DocumentTemplatesController', 'doctemplateaction', array(
                'form_action' => 'delete',
                'tem_doc_id' => $this->request->getParameter('tem_doc_id'),
                'module' => $this->request->getParameter('module')
            ));
            return;
        }
        elseif ($this->request->getParameter('rbWhat') == 'save')
        {
            $this->response = $this->call('src\wordmergetemplate\controllers\DocumentTemplatesController', 'DocumentTemplateAdminSaveTemplate', array());
            return;
        }

        $this->response = $this->call('src\admin\controllers\DocTemplatesAdminListTemplateController', 'doctemplateadminlist');
        return;
    }

    public function DocumentTemplateAdminSaveTemplate()
    {
        $tem_doc_id = \Sanitize::SanitizeInt($this->request->getParameter('tem_doc_id'));
        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $tem_type = \Sanitize::SanitizeString($this->request->getParameter('tem_type'));
        $form_action = \Sanitize::SanitizeString($this->request->getParameter('form_action'));
        $path = \Sanitize::SanitizeString($this->request->getParameter('path'));
        $tem_documents_id = '';
        $rep = QuotePostArray($this->request->getParameters());

        $ErrorMark = '<font size="3" color="red"><b>*</b></font>';

        if ($this->request->getParameter('tem_notes') == '')
        {
            $error['message'] = 'You must enter a template name.';
            $error['tem_notes'] = $ErrorMark;
        }

        if ($this->request->getParameter('tem_type') == '')
        {
            $error['message'] = 'You must select the template type.';
            $error['tem_type'] = $ErrorMark;
        }

        if ($_FILES['userfile']['name'] == '' && !$tem_doc_id)
        {
            $error['message'] .= ($error['message'] ? '<br />' : '') . 'You must select a file to upload.';
            $error['userfile'] = $ErrorMark;
        }

        if ($error)
        {
            $this->response = $this->call('src\wordmergetemplate\controllers\DocumentTemplatesTemplateController', 'DocumentTemplateForm', array(
                'tem_doc_id' => $tem_doc_id,
                'module' => $module,
                'form_action' => $form_action,
                'error' => $error,
                'rep' => $rep
            ));
            return;
        }

        if ($tem_doc_id == '')
        {
            $tem_doc_id = GetNextRecordID('templates_main', true);
            $NewRecord = true;
            //When creating a new document, the permission where clause will be out of date (as it is simply a list of ids). We need to force this to refresh.
            $_SESSION["permission_where"]['TEM'] = null;
        }

        if ($_FILES['userfile']['name'] != '')
        {
            $sqldoc = "UPDATE tem_documents SET ";

            if ($this->request->getParameter('tem_id') == '')
            {
                $tem_documents_id = GetNextRecordID('tem_documents', true);
                $NewRecordTemDoc = true;
                $sqldoc .= " updateid = 1, compressed = 'N', ";
            }
            else
            {
                $tem_documents_id = \Sanitize::SanitizeInt($this->request->getParameter('tem_id'));
            }

            $TemplateUploadFileName = 'TEM' . $tem_documents_id . '.doc';

            GetParms($_SESSION['user']);
            $ToFile = \Sanitize::SanitizeFilePath(GetParm('PATH_TEMPLATES', '', true) . "\\" . $TemplateUploadFileName);

            if ($_FILES['userfile']['size'] == '0' || !@move_uploaded_file(\Sanitize::SanitizeFilePath($_FILES['userfile']['tmp_name']), $ToFile))
            {
                fatal_error('Cannot copy file to server. Please contact your IT department.');
            }

            $sqldoc .= " path = :path WHERE recordid = :recordid";

            if (!\DatixDBQuery::PDO_query($sqldoc, array('path' => $TemplateUploadFileName, 'recordid' => $tem_documents_id)))
            {
                fatal_error('Could not save template document' . $sqldoc);
            }
        }

        $pdoParams = array();

        $sql = "
            UPDATE
                templates_main
            SET
                tem_notes = :tem_notes,
                tem_type = :tem_type,
                tem_module = :tem_module, ";

        $pdoParams['tem_notes'] = $this->request->getParameter('tem_notes');
        $pdoParams['tem_type'] = $this->request->getParameter('tem_type');
        $pdoParams['tem_module'] = $this->request->getParameter('module');

        if ($tem_documents_id != '')
        {
            $sql .= "tem_id = :tem_id, ";
            $pdoParams['tem_id'] = $tem_documents_id;
        }

        if ($NewRecord)
        {
            $sql .= "tem_createdby = :tem_createdby,
                 tem_dcreated = '" . date('Y-m-d H:i:s') . "', ";
            $pdoParams['tem_createdby'] = $_SESSION['initials'];
        }

        $sql .= "tem_dupdated = '" . date('Y-m-d H:i:s') . "',
        tem_updatedby = :tem_updatedby,
        updateid = :updateid
        WHERE recordid = :recordid
        AND (updateid = :updateid_old OR updateid IS NULL)";

        $pdoParams['tem_updatedby'] = $_SESSION['initials'];
        $pdoParams['updateid'] = GensUpdateID($this->request->getParameter('updateid'));
        $pdoParams['recordid'] = $tem_doc_id;
        $pdoParams['updateid_old'] = $this->request->getParameter('updateid');

        $result = \DatixDBQuery::PDO_query($sql, $pdoParams);

        if ($result == false)
        {
            fatal_error('Could not save template' . $sql);
        }
        else
        {
            AddSessionMessage('INFO', _tk('document_template_saved'));

            require_once 'Source/security/SecurityRecordPerms.php';

            $aParam = array(
                'module' =>'TEM',
                'table' => 'templates_main',
                'recordid' => $tem_doc_id
            );

            $UserAccessList = GetRecordAccessUserList($aParam);
            $GroupAccessList = GetRecordAccessGroupList($aParam);
            $SomeoneLinked = false;

            if (is_array($UserAccessList))
            {
                if (count($UserAccessList) > 0)
                {
                    $SomeoneLinked = true;
                }
            }

            if (is_array($GroupAccessList))
            {
                if (count($GroupAccessList) > 0)
                {
                    $SomeoneLinked = true;
                }
            }

            if ($SomeoneLinked === false)
            {
                AddSessionMessage('INFO', _tk('document_template_saved_permission_warning'));
            }
        }

        $Params = array(
            'tem_doc_id' => $tem_doc_id,
            'module' => $module,
            'form_action' => 'edit'
        );

        if ($NewRecord)
        {
            $Params['panel'] = 'permissions';
        }

        $this->response = $this->call('src\wordmergetemplate\controllers\DocumentTemplatesController', 'doctemplateaction', $Params);
        return;
    }
}