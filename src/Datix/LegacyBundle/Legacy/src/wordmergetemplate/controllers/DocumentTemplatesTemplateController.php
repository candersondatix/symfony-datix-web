<?php

namespace src\wordmergetemplate\controllers;

use src\framework\controller\TemplateController;

class DocumentTemplatesTemplateController extends TemplateController
{
    public function DocumentTemplateForm()
    {
        $tem_doc_id = $this->request->getParameter('tem_doc_id');
        $module = $this->request->getParameter('module');
        $form_action = $this->request->getParameter('form_action');
        $error = ($this->request->getParameter('error') ? $this->request->getParameter('error') : '');
        $rep = ($this->request->getParameter('rep') ? $this->request->getParameter('rep') : array());

        $FormArray = array (
            'Parameters' => array(
                'Panels' => 'True',
                'Condition' => false
            ),
            'name' => array(
                'Title' => 'Document template settings',
                'MenuTitle' => 'Document template settings',
                'NewPanel' => true,
                'ControllerAction' => array(
                    'MakeDocumentTemplateAdminEditPanel' => array(
                        'controller' => 'src\\wordmergetemplate\\controllers\\DocumentTemplatesController'
                    )
                ),
                'Rows' => array()
            ),
            'permissions' => array(
                'Title' => 'Permissions',
                'NewPanel' => true,
                'NoFieldAdditions' => true,
                'ControllerAction' => array(
                    'RecordPermissionsDocTemplate' => array(
                        'controller' => 'src\\wordmergetemplate\\controllers\\DocumentTemplatesController'
                    )
                ),
                'Condition' => $tem_doc_id,
                'NotModes' => array('new'),
                'Rows' => array()
            )
        );

        $RepTable = new \FormTable($form_action);
        $RepTable->MakeForm($FormArray, $rep, $module);

        $this->title = 'Document template administration';
        $this->module = 'ADM';

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array(
            'id' => 'btnSave',
            'name' => 'btnSave',
            'label' => _tk('btn_save'),
            'onclick' => 'document.frmTemplateDocument.rbWhat.value=\'save\';document.frmTemplateDocument.submit()',
            'action' => 'SAVE'
        ));
        $ButtonGroup->AddButton(array(
            'id' => 'btnCancel',
            'name' => 'btnCancel',
            'label' => _tk('btn_cancel'),
            'onclick' => 'document.frmTemplateDocument.rbWhat.value=\'cancel\';document.frmTemplateDocument.submit()',
            'action' => 'CANCEL'
        ));

        if ($tem_doc_id)
        {
            $ButtonGroup->AddButton(array(
                'id' => 'btnDelete',
                'name' => 'btnDelete',
                'label' => _tk('delete_template'),
                'onclick' => 'if(confirm(\''._tk('delete_template_q').'\')){document.frmTemplateDocument.rbWhat.value=\'delete\';document.frmTemplateDocument.submit();}',
                'action' => 'DELETE'
            ));
        }

        $this->menuParameters = array(
            'buttons' => $ButtonGroup,
            'table' => $RepTable,
            'no_print' => true,
            'no_audit' => true
        );

        $RepTable->MakeTable();

        $this->response->build('src/wordmergetemplate/views/DocumentTemplateForm.php', array(
            'form_action' => $form_action,
            'tem_doc_id' => $tem_doc_id,
            'rep' => $rep,
            'module' => $module,
            'error' => $error,
            'RepTable' => $RepTable,
            'ButtonGroup' => $ButtonGroup,
            'print' => $this->request->getParameter('print'),
            'panel' => $this->request->getParameter('panel')
        ));
    }
}