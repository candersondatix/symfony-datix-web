<form enctype="multipart/form-data" method="post" id="frmTemplateDocument" name="frmTemplateDocument" action="<?php echo $this->scripturl; ?>?action=doctemplateadmineditsave">
    <input type="hidden" id="form_action" name="form_action" value="<?php echo $this->form_action; ?>" />
    <input type="hidden" id="tem_doc_id" name="tem_doc_id" value="<?php echo $this->tem_doc_id; ?>" />
    <input type="hidden" id="recordid" name="recordid" value="<?php echo $this->tem_doc_id; ?>" />
    <input type="hidden" id="rbWhat" name="rbWhat" value="" />
    <input type="hidden" id="path" name="path" value="<?php echo $this->rep->path; ?>" />
    <input type="hidden" id="tem_id" name="tem_id" value="<?php echo $this->rep->tem_id; ?>" />
    <input type="hidden" id="module" name="module" value="<?php echo $this->module; ?>" />
    <input type="hidden" id="updateid" name="updateid" value="<?php echo $this->rep->updateid; ?>" />
    <?php if ($this->error) : ?>
    <div class="form_error_wrapper">
        <?php if ($this->error) : ?>
        <div class="form_error_title"><?php echo _tk('form_errors'); ?></div>
        <div class="form_error"><?php echo $this->error['message']; ?></div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php echo $this->RepTable->GetFormTable(); ?>
    <?php echo $this->ButtonGroup->getHTML(); ?>
</form>
<?php if (!$this->print) : ?>
<?php echo JavascriptPanelSelect(false, $this->panel, $this->RepTable); ?>
<?php endif; ?>