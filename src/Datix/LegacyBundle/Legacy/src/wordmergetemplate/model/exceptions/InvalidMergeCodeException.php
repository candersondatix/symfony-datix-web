<?php
namespace src\wordmergetemplate\model\exceptions;

use src\framework\model\exceptions\ModelException;

class InvalidMergeCodeException extends ModelException{}