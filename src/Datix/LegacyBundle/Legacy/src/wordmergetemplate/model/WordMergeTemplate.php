<?php
namespace src\wordmergetemplate\model;

use src\framework\model\RecordEntity;

/**
 * This entity models a record in templates_main.
 */
class WordMergeTemplate extends RecordEntity
{
    /**
     * The id of this document in the tem_documents table.
     */
    protected $tem_id;

    /**
     * The description of this template.
     */
    protected $tem_notes;

    /**
     * The type of template this is (Letter, Invoice...)
     */
    protected $tem_type;

    /**
     * The module that this document template is intended for.
     */
    protected $tem_module;

    /**
     * Acts as a marker to ensure that other people's changes are not overwritten.
     *
     * @var string
     */
    protected $updateid;

    /**
     * The initials of the user who created the document.
     *
     * @var string
     */
    protected $tem_createdby;

    /**
     * The initials of the last user to update the document.
     *
     * @var string
     */
    protected $tem_updatedby;

    /**
     * The date the document was created.
     *
     * @var string
     */
    protected $tem_dcreated;

    /**
     * The date the document was last updated.
     *
     * @var string
     */
    protected $tem_dupdated;

    /**
     * @var string filename for location of temporary file on server. If this is set, it will be used instead of re-copying the source file on subsequent uses of the same object.
     */
    protected $temporaryFile;

    /**
     * @var \DomDocument contents of the xml portion of the template.
     */
    protected $xmlDocument;

    /**
     * @var array set of merge codes present in this template.
     */
    protected $mergeCodes;

    /**
     * @var string Path for location of template file.
     */
    protected $path;

    /**
     * @return string Returns the code for the module associated with this template.
     */
    public function getModule()
    {
        return $this->tem_module;
    }

    /**
     * @return string Returns the server location of the temporary file containing a copy of this template file.
     */
    public function getTemporaryFile()
    {
        if(!$this->temporaryFile)
        {
            $this->populateTemporaryFile();
        }

        return $this->temporaryFile;
    }

    /**
     * @return \DomDocument returns the xml portion of this template file.
     */
    public function getXMLDocument()
    {
        if(!$this->xmlDocument)
        {
            $this->populateXMLDocument();
        }

        return $this->xmlDocument;
    }

    /**
     * @return array An array of all the merge codes in the docx file associated with this object.
     */
    public function getMergeCodes()
    {
        if(!isset($this->mergeCodes))
        {
            $this->populateMergeCodes();
        }

        return $this->mergeCodes;
    }

    /**
     * Populates the temporaryFile property of this object.
     *
     * @throws \CopyFileException
     * @throws \FileNotFoundException
     */
    protected function populateTemporaryFile()
    {
        //Get location of physical document to process
        $docFile = \DatixDBQuery::PDO_fetch('SELECT path FROM tem_documents WHERE recordid = :recordid', array('recordid' => $this->tem_id), \PDO::FETCH_COLUMN);
        $docTemplatePath = GetParm("PATH_TEMPLATES") . '\\' . $docFile;

        //Create temporary file to use when manipulating this template.
        $this->temporaryFile = tempnam(GetParm("PATH_TEMPLATES"), 'zip');

        //Copy the document template into the temporary file.
        if (file_exists($docTemplatePath) === false || file_exists($this->temporaryFile) === false)
        {
            throw new \FileNotFoundException('File not found.');
        }
        if (!copy(\Sanitize::SanitizeFilePath($docTemplatePath), \Sanitize::SanitizeFilePath($this->temporaryFile)))
        {
            throw new \CopyFileException('File copy failed.');
        }

        //the mergeCodes and xmlDocument properties needs to be kept in sync with the temporaryFile property, so we
        //blank them out here, forcing them to be recalculated when next accessed.
        $this->xmlDocument = null;
        $this->mergeCodes = null;
    }

    /**
     * Populates the xmlDocument property of this object
     *
     * @throws \Exception
     */
    protected function populateXMLDocument()
    {
        if(!$this->temporaryFile)
        {
            $this->populateTemporaryFile();
        }

        //Open the docx file to reveal the various xml files that make it up.
        $zip = new \ZipArchive();
        if ($zip->open($this->temporaryFile) !== true)
        {
            throw new \Exception('Zip file failed to open.');
        }

        //Pull out the xml file representing the contents of the document
        if (($index = $zip->locateName("word/document.xml")) !== false)
        {
            $XMLData = strval($zip->getFromIndex($index));
        }
        else
        {
            throw new \Exception('File has no xml document component.');
        }
        $zip->close();

        //simplexml doesn't seem to handle the complex docx namespaces very well, so
        //it was actually easier to use DomDocument
        $this->xmlDocument = new \DOMDocument();
        $this->xmlDocument->loadXML($XMLData);

        //the mergeCodes property needs to be kept in sync with the xmlDocument property, so we
        //blank it out here, forcing it to be recalculated when it is next accessed.
        $this->mergeCodes = null;
    }

    /**
     * Populates the mergeCodes property of this object by interrogating the xmlDocument property
     */
    protected function populateMergeCodes()
    {
        $this->RecursiveMergecodeSearch($this->getXMLDocument()->documentElement, $this->mergeCodes);
    }

    /**
     * Loops recursively through a passed xml (sub-)tree looking for nodes matching "MERGEFIELD" and collecting the
     * text of the mergecode in an array. Calls itself on all sub-trees of the passed tree.
     * @param $domNode \DomDocument the tree to search
     * @param $results array an array to append any results to.
     */
    protected function RecursiveMergecodeSearch($domNode, &$results)
    {
        foreach($domNode->childNodes as $childNode)
        {
            /*
             * There are two ways that a merge code can appear: as an fldSimple tag, or between two fldChar nodes.
             */
            if($childNode->nodeName=='w:fldSimple')
            {
                foreach($childNode->attributes as $attribute)
                {
                    if($attribute->name == 'instr' && preg_match('/MERGEFIELD([!A-Za-z0-9_:\s\"]*)/ui', \UnicodeString::trim(strval($attribute->value)), $matches))
                    {
                        $results[] = \UnicodeString::strtoupper(\UnicodeString::trim(str_replace('"', '', $matches[1])));
                    }
                }
            }
            else if ($childNode->nodeName=='w:instrText' && preg_match('/MERGEFIELD([!A-Za-z0-9_:\s\"]*)/iu', \UnicodeString::trim(strval($childNode->textContent)), $matches))
            {
                $results[] = \UnicodeString::strtoupper(\UnicodeString::trim(str_replace('"', '', $matches[1])));
            }
            else
            {
                $this->RecursiveMergecodeSearch($childNode, $results);
            }
        }
    }

/**
 * @return string Returns the location of the template file.
 */
    public function getPath()
    {
        if (!isset($this->path))
        {
            $this->populatePath();
        }

        return $this->path;
    }

    /**
     * Populates the path property of this object.
     */
    protected function populatePath()
    {
        //Get location of physical document to process
        $docFile = \DatixDBQuery::PDO_fetch('SELECT path FROM tem_documents WHERE recordid = :recordid', array('recordid' => $this->tem_id), \PDO::FETCH_COLUMN);
        $this->path = $docFile;
    }
}