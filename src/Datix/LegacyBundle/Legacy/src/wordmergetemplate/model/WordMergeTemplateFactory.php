<?php
namespace src\wordmergetemplate\model;

use src\framework\model\EntityFactory;

/**
 * Constructs CodeTagGroup objects.
 */
class WordMergeTemplateFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\wordmergetemplate\\model\\WordMergeTemplate';
    }
}