<?php
namespace src\actionchains;

use src\actionchains\model\ActionChainModelFactory;
use src\actions\model\ActionModelFactory;
use src\framework\registry\Registry;

/**
 * Helper class responsible for operations involving Action Chains.
 */
class ActionChainManager
{
    /**
     * @var ActionChainModelFactory
     */
    protected $actionChainFactory;
    
    /**
     * @var ActionModelFactory
     */
    protected $actionFactory;
    
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(ActionChainModelFactory $actionChainFactory = null, ActionModelFactory $actionFactory = null, Registry $registry = null)
    {
        $this->actionChainFactory = $actionChainFactory ?: new ActionChainModelFactory();
        $this->actionFactory      = $actionFactory ?: new ActionModelFactory();
        $this->registry           = $registry ?: Registry::getInstance();
    }

    /**
     * Creates a set of actions against a main module record using an Action Chain as a template.
     *
     * @param int              $chainID          The ID of the action chain being used to create the action records.
     * @param int              $mainRecordID     The ID of the record we're creating the chain against.
     * @param string           $mainRecordModule The module of the record we're creating the chain against.
     * @param \DateTime|string $commencementDate The chain commencement date.
     *
     * @return int             $instanceID       The newly-created instance ID for this action chain.
     */
    public function createConcreteActionChain($chainID, $mainRecordID, $mainRecordModule, \DateTime $commencementDate = null)
    {
        $acMapper       = $this->actionChainFactory->getMapper();
        $actionFactory  = $this->actionFactory->getEntityFactory();
        $instance       = $acMapper->getNextInstanceId($chainID, $mainRecordID, $mainRecordModule);
        $chain          = $acMapper->find($chainID);
        $moduleDefs     = $this->registry->getModuleDefs();
        $locationFields = array();

        foreach ($chain->steps as $step)
        {
            // We need to keep track of the original Commencement date because we are modifying it below
            if ($commencementDate != null)
            {
                $acStartDate = clone $commencementDate;   
            }

            if (is_array($moduleDefs[$mainRecordModule]['STAFF_EMPL_FILTER_MAPPINGS']) && sizeof($moduleDefs[$mainRecordModule]['STAFF_EMPL_FILTER_MAPPINGS']) > 0)
            {
                $locationFields = $acMapper->getLocationFields($mainRecordModule, $mainRecordID);

                // Replace location by loctype key because some modules are not coherent (WHY???) with the rest!!!
                if (array_key_exists(\UnicodeString::strtolower($mainRecordModule) . '_location', $locationFields)) {
                    $locationFields[\UnicodeString::strtolower($mainRecordModule) . '_loctype'] = $locationFields[\UnicodeString::strtolower($mainRecordModule) . '_location'];
                    unset($locationFields[\UnicodeString::strtolower($mainRecordModule) . '_location']);
                }
            }

            $action = $actionFactory->createObject(array(
                'act_cas_id'                => $mainRecordID,
                'act_module'                => $mainRecordModule,
                'act_type'                  => $step->acs_act_type,
                'act_descr'                 => $step->acs_act_desc,
                'act_chain_id'              => $chain->recordid,
                'act_chain_instance'        => $instance,
                'act_step_no'               => $step->acs_order,
                'act_start_after_type'      => $step->acs_start_after_type,
                'act_start_after_step'      => $step->acs_start_after_step,
                'act_start_after_days_no'   => $step->acs_start_no,
                'act_start_after_days_type' => $step->acs_start_days_type,
                'act_due_days_no'           => $step->acs_due_no,
                'act_due_days_type'         => $step->acs_due_days_type,
                'act_reminder_days_no'      => $step->acs_reminder_no,
                'act_reminder_days_type'    => $step->acs_reminder_days_type,
                'act_to_inits'              => $acMapper->getHandler($mainRecordModule, $mainRecordID),
                'act_organisation'          => $locationFields[\UnicodeString::strtolower($mainRecordModule).'_organisation'],
                'act_unit'                  => $locationFields[\UnicodeString::strtolower($mainRecordModule).'_unit'],
                'act_clingroup'             => $locationFields[\UnicodeString::strtolower($mainRecordModule).'_clingroup'],
                'act_directorate'           => $locationFields[\UnicodeString::strtolower($mainRecordModule).'_directorate'],
                'act_specialty'             => $locationFields[\UnicodeString::strtolower($mainRecordModule).'_specialty'],
                'act_loctype'               => $locationFields[\UnicodeString::strtolower($mainRecordModule).'_loctype'],
                'act_locactual'             => $locationFields[\UnicodeString::strtolower($mainRecordModule).'_locactual'],
            ));

            // create the action start/due dates if based on the commencement date.
            if ($step->acs_start_after_type == 'START' && $step->acs_start_no != '' && $acStartDate !== null)
            {
                // days after type defaults to 'Calendar' if none explicitly stated.
                $daysAfter = $step->acs_start_no;

                if ($step->acs_start_days_type == 'WOR')
                {
                    $daysAfter = CalculateWorkingDays($daysAfter, $acStartDate);
                }

                $action->act_dstart = $acStartDate->modify('+'.$daysAfter.' day')->format('Y-m-d H:i:s');

                if ($step->acs_due_no != '')
                {
                    $daysAfter = $step->acs_due_no;

                    if ($step->acs_due_days_type == 'WOR')
                    {
                        $daysAfter = CalculateWorkingDays($daysAfter, $acStartDate);
                    }

                    $action->act_ddue = $acStartDate->modify('+'.$daysAfter.' day')->format('Y-m-d H:i:s');
                }
            }
            
            $this->actionFactory->getMapper()->save($action);
        }
        
        return $instance;
    }
    
    /**
    * Updates start/due date values for subsequent steps on completion/uncompletion of a step.
    *
    * @param int $id The recordid of the action being completed.
    */
    public function updateStepDates($id)
    {
        // retrieve the action that represents this step in the chain
        $mapper   = $this->actionFactory->getMapper();
        $thisStep = $mapper->find($id);
        
        // retrieve the subsequent steps (i.e. those whose start date depend on the completion of this step) as a collection of actions
        $query = $this->actionFactory->getQueryFactory()->getQuery();
        $query->where(array(
            'act_module'           => $thisStep->act_module,
            'act_cas_id'           => $thisStep->act_cas_id,
            'act_chain_id'         => $thisStep->act_chain_id,
            'act_chain_instance'   => $thisStep->act_chain_instance,
            'act_start_after_step' => $thisStep->act_step_no
        ));
        
        $subSteps = $this->actionFactory->getCollection();
        $subSteps->setQuery($query);
        
        // update the start/due dates for each subsequent step
        foreach ($subSteps as $subStep)
        {
            if ($thisStep->act_ddone == '')
            {
                // this step is not complete, so ensure subsequent steps have no start/due date
                $subStep->act_dstart = null;
                $subStep->act_ddue   = null;
            }
            else
            {
                // populate start/due dates for subsequent steps
                if ($subStep->act_start_after_days_no != '')
                {
                    // calculate start date
                    $daysAfter = $subStep->act_start_after_days_no;
                    if ($subStep->act_start_after_days_type == 'WOR')
                    {
                        $daysAfter = CalculateWorkingDays($daysAfter, new \DateTime($thisStep->act_ddone));
                    }
                    $subStep->act_dstart = date('Y-m-d H:i:s', strtotime($thisStep->act_ddone.' +'.$daysAfter.' day'));
                }

                if ($subStep->act_due_days_no != '')
                {
                    // calculate due date
                    $daysAfter = $subStep->act_due_days_no;
                    if ($subStep->act_due_days_type == 'WOR')
                    {
                        $daysAfter = CalculateWorkingDays($daysAfter, new \DateTime($subStep->act_dstart));
                    }
                    $subStep->act_ddue = date('Y-m-d H:i:s', strtotime($subStep->act_dstart.' +'.$daysAfter.' day'));
                }
            }
            
            $mapper->save($subStep);
        }
    }
}