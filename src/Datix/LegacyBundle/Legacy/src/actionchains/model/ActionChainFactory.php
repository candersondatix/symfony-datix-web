<?php
namespace src\actionchains\model;

use src\framework\model\EntityFactory;

class ActionChainFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actionchains\\model\\ActionChain';
    }
    
    /**
     * {@inheritdoc}
     */
    public function doCreateObject(array $data = array())
    {
        $chain = parent::doCreateObject($data);
        $chain->ach_archive = 0;
        
        // retrieve step collection info
        $properties = Step::getProperties();
        
        $steps = array();
        for ($i = 1; $i < $data['step_max_suffix']; $i++)
        {
            if (!isset($data['step_'.$i.'_recordid']))
            {
                continue;
            }
            
            $step = array();
            $step['recordid'] = $data['step_'.$i.'_recordid'];
            $step['acs_chain_id'] = $data['recordid'];
            
            foreach ($properties as $property)
            {
                if (isset($data[$property.'_'.$i]))
                {
                    $step[$property] = $data[$property.'_'.$i];
                }
            }
            $steps[] = $step;
        }
        
        $chain->steps->setData($steps);
        return $chain;
    }
}