<?php
namespace src\actionchains\model;

use src\framework\model\Mapper;

class StepMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actionchains\\model\\Step';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
       return 'action_chain_steps'; 
    }
}