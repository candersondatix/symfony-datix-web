<?php
namespace src\actionchains\model;

use src\framework\model\RecordEntity;
use src\framework\query\Query;

/**
 * A user-defined collection of actions.  Used as a means of applying a pre-defined list of actions to a main module record.
 */
class ActionChain extends RecordEntity
{
    /**
     * The module this chain is available for use in (can be 'ALL').
     * 
     * @var string
     */
    protected $ach_module;
    
    /**
     * Tha action chain title.
     * 
     * @var string
     */
    protected $ach_title;
    
    /**
     * Set to 1 when record is unavailable ("deleted").
     * 
     * @var int
     */
    protected $ach_archive;
    
    /**
     * The action chain description.
     * 
     * @var string
     */
    protected $ach_description;
    
    /**
     * The steps that make up this chain.
     * 
     * @var StepCollection
     */
    protected $steps;
    
    public function __construct($id = null)
    {
        $factory     = new StepModelFactory();
        $this->steps = $factory->getCollection();
        parent::__construct($id);
    }
    
    /**
     * Defines the relationship to this object's step collection when the ID is set.
     * 
     * @param int $id
     */
    public function setRecordid($id)
    {
        $this->recordid = (int) $id;
        
        $query = new Query();
        $query->where(array('action_chain_steps.acs_chain_id' => $id))
              ->orderBy(array('action_chain_steps.acs_order'));
        
        $this->steps->setQuery($query);
    }

    /**
     * Function to make sure that ach_archive is always an integer
     *
     * @return int
     */
    public function getAch_archive()
    {
        return ($this->ach_archive == 1 ? 1 : 0);
    }
}