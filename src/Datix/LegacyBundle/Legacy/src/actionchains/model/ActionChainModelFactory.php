<?php
namespace src\actionchains\model;

use src\framework\model\ModelFactory;

class ActionChainModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ActionChainFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        $stepFactory = new StepModelFactory();
        return new ActionChainMapper(new \DatixDBQuery(), $this, $stepFactory->getMapper());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ActionChainCollection($this->getMapper(), $this->getEntityFactory());
    }
}