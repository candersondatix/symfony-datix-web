<?php
namespace src\actionchains\model;

use src\framework\model\EntityFactory;

class StepFactory extends EntityFactory
{
    public function targetClass()
    {
        return 'src\\actionchains\\model\\Step';
    }
}