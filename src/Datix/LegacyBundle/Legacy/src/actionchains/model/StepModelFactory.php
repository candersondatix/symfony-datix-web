<?php
namespace src\actionchains\model;

use src\framework\model\ModelFactory;

class StepModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new StepFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new StepMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new StepCollection($this->getMapper(), $this->getEntityFactory());
    }
}