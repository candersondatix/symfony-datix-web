<?php
namespace src\actionchains\model;

use src\framework\model\EntityCollection;

class StepCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actionchains\\model\\Step';
    }
}