<li>
    <div id="no_action_chains" style="font-weight:bold; padding:5px; color:#083c5c;<?php if (!empty($this->chains)): ?> display:none;<?php endif ?>">No action chains</div>
    <?php foreach ($this->chains as $chain) : ?>
        <div id="chain_<?php echo $chain['act_chain_id'] ?>_<?php echo $chain['act_chain_instance'] ?>" style="padding:10px;">
            <div class="new_titlebg">
                <div style="float:left;font-weight:bold;"><?php echo $chain['ach_title'] ?></div>
                <?php if ($this->canDelete): ?><div class="section_title_right_hand_link" style="float:right;" onclick="deleteActionChain(<?php echo $chain['act_chain_id'] ?>, <?php echo $chain['act_chain_instance'] ?>)">Delete</div><?php endif ?>
            </div>
            <?php echo $chain['listing']->GetListingHTML(); ?>
        </div>
    <?php endforeach ?>
</li>
<?php if ($this->canEdit) : ?>
    <li class="new_link_row">
        <a href="Javascript:if(CheckChange()){newActionChain();}"><b><?php echo $this->addNew; ?></b></a>
    </li>
    <script type="text/javascript">
        /**
         * Builds and displays the dialogue box used to create action chains.
         */
        function newActionChain()
        {
            var buttons = [];
            buttons[0] = {"value":"<?php echo _tk('btn_add'); ?>", "onclick":"jQuery(\'#actionChainForm\').submit();"};
            buttons[1] = {"value":"<?php echo _tk('btn_cancel'); ?>", "onclick":"GetFloatingDiv(\'newActionChain\').CloseFloatingControl();"};
            PopupDivFromURL("newActionChain", "New action chain", scripturl+"?action=httprequest&type=newactionchain&module=<?php echo $this->module; ?>&act_cas_id=<?php echo $this->recordid; ?>&callback=<?php echo $this->callback; ?>", "", buttons, "400px");
            createCalendar(jQuery('.date'), "<?php echo $this->calendarOnClick; ?>", "<?php echo $this->date_fmt; ?>", <?php echo $this->weekStartDay; ?>, <?php echo $this->calendarWeekend; ?>);
        }
    </script>
<?php endif ?>
<?php if ($this->canDelete): ?>
    <script type="text/javascript">
        /**
         * On confirmation, removes action chain from record.
         *
         * @param {int} id       The action chain id.
         * @param {int} instance The action chain instance.
         */
        function deleteActionChain(id, instance)
        {
            if (confirm("<?php echo $this->deleteConfirm; ?>"))
            {
                jQuery.get(scripturl+"?action=deletechainfromrecord", "act_cas_id=<?php echo $this->recordid; ?>&act_module=<?php echo $this->module; ?>&act_chain_id="+id+"&act_chain_instance="+instance);

                if (jQuery("#chain_"+id+"_"+instance).length)
                {
                    // remove action chain table
                    jQuery("#chain_"+id+"_"+instance).remove();
                }

                if (jQuery(".chain_"+id+"_"+instance).length)
                {
                    // remove actions from this chain from the actions listing
                    jQuery(".chain_"+id+"_"+instance).remove();
                }

                if (!jQuery("div[id^=chain_").length)
                {
                    jQuery("#no_action_chains").show();
                }
            }
        }
    </script>
<?php endif ?>