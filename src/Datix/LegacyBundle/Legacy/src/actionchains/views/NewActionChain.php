<form id="actionChainForm" method="POST" action="<?php echo $this->scripturl ?>?action=createactionsfromchain" onsubmit="if (jQuery('#chain_id').val() == ''){alert('<span style=\'color:red\'>You must select an action chain</span>');return false;}else{return true;}">
    <input name="module" type="hidden" value="<?php echo $this->module  ?>" />
    <input name="act_cas_id" type="hidden" value="<?php echo $this->act_cas_id  ?>" />
    <input name="callback" type="hidden" value="<?php echo $this->callback  ?>" />
    <input name="token" type="hidden" value="<?php echo $this->token;  ?>" />
    <div style="clear:both; padding:10px; margin-bottom:10px;">
        <div class="field_label" style="float:left; height:100%; margin-right:10px; width:130px;">Action chain:</div>
        <div style="float:left;"><?php echo $this->chains->getField() ?></div>
    </div>
    <div style="clear:both; padding:10px; margin-bottom:15px;">
        <div class="field_label" style="float:left; height:100%; margin-right:10px; width:130px;">Commencement date:</div>
        <div style="float:left;"><?php echo $this->date->getField() ?></div>
    </div>
</form>