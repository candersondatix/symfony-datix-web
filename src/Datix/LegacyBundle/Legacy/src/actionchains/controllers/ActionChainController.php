<?php
namespace src\actionchains\controllers;

use src\framework\controller\Controller;
use src\actionchains\ActionChainManager;
use src\actionchains\model\ActionChainModelFactory;

class ActionChainController extends Controller
{
    /**
     * Controls the process of creating concrete action chains.
     */
    public function createActionsFromChain()
    {
        if ($this->request->getParameter('start_date') != '')
        {
            try
            {
                $startDate = new \DateTime(UserDateToSQLDate($this->request->getParameter('start_date')));
            }
            catch (\Exception $e)
            {
                $this->registry->getLogger()->logEmergency('Unable to parse start date ('.$this->request->getParameter('start_date').') when creating action chain: ' . $e->getMessage());
                throw $e;
            }
        }
        
        $manager = new ActionChainManager();
        $manager->createConcreteActionChain(
            $this->request->getParameter('chain_id'),
            $this->request->getParameter('act_cas_id'),
            $this->request->getParameter('module'),
            $startDate
        );

        // Update record last updated
        $this->call('src\\generic\\controllers\\RecordController', 'updateRecordLastUpdated', [
            'module'   => $this->request->getParameter('module'),
            'recordId' => $this->request->getParameter('act_cas_id')
        ]);
        
        $this->redirect('app.php?'. $this->request->getParameter('callback'));
    }
    
    /**
     * Controls the process of removing a concrete action chain from a record.
     */
    public function deleteChainFromRecord()
    {
        $factory = new ActionChainModelFactory();
        $factory->getMapper()->deleteConcreteActionChain(
            $this->request->getParameter('act_cas_id'),
            $this->request->getParameter('act_module'),
            $this->request->getParameter('act_chain_id'),
            $this->request->getParameter('act_chain_instance')
        );
    }
    
    /**
     * Renders the "new action chain" popup dialogue.
     */
    public function newActionChain()
    {
        require_once 'Source/libs/FormClasses.php';
        
        $module = \Sanitize::getModule($this->request->getParameter('module'));
        
        $chains = \Forms_SelectFieldFactory::createSelectField('chain_id', '', '', '');
        $chains->setSelectFunction('getActionChains', array('module' => '"'.$module.'"'));
        $chains->setSuppressCodeDisplay();
        
        $date = new \FormField();
        $date->MakeDateField('start_date');
        
        $this->response->build('src/actionchains/views/NewActionChain.php', array(
            'chains'     => $chains,
            'date'       => $date,
            'module'     => $module,
            'act_cas_id' => (int) $this->request->getParameter('act_cas_id'),
            'callback'   => \Sanitize::SanitizeURL($this->request->getParameter('callback')),
            'token'      => \CSRFGuard::getCurrentToken()
        ));
    }

    /**
     * Constructs the action chain table(s).
     */
    public function getActionChains()
    {
        global $scripturl, $FieldDefs;

        $data     = $this->request->getParameter('data');
        $formType = $this->request->getParameter('FormType');
        $module   = $this->request->getParameter('module');

        $sql = '
            SELECT
                a.act_chain_id, a.act_chain_instance, c.ach_title
            FROM
                ca_actions a INNER JOIN action_chains c ON a.act_chain_id = c.recordid
            WHERE
                a.act_module = :module AND a.act_cas_id = :act_cas_id AND a.act_chain_id IS NOT NULL
            GROUP BY
                a.act_chain_id, a.act_chain_instance, c.ach_title
            ORDER BY
                a.act_chain_id, a.act_chain_instance
        ';

        $chains = \DatixDBQuery::PDO_fetch_all($sql, ['module' => $module, 'act_cas_id' => $data['recordid']]);

        //I've re-written some of this as part of a bugfix, so don't be mad that its some crazy half-implementation
        $design = \Listings_ModuleListingDesignFactory::getListingDesign(array(
            'module' => 'ACT',
            'parent_module' => $module,
            'link_type' => 'linked_actions'
        ));
        /*Get the columns from the DB for a regular actions listing
          (because an action chain doesn't have its own design)*/
        $design->LoadColumnsFromDB();

        //This is one of the dummy fields we have to add to the listing because it doesn't exist for a regular actions listing
        $customColumns[] = new \Fields_DummyField(array('name' => 'act_step_no', 'label' => 'Step', 'type' => 'S', 'width' => 3));

        foreach ($design->Columns as $key => $column)
        {
            //Now we transfer the columns into the 'proper' listing columns, which includes some dummy fields
            if ($column->GetName() == 'act_ddone')
            {
                //In this particular case, we need to replace the real field with this one to handle the special 'button' functionality
                $act_ddone_available = true;
                $customColumns[] = new \Fields_DummyField(array('name' => 'act_ddone', 'label' => \Labels_FieldLabel::GetFieldLabel('act_ddone'), 'type' => 'X', 'width' => 4));
            }
            else
            {
                $customColumns[] = $column;
            }
        }

        //This is one of the dummy fields we have to add to the listing because it doesn't exist for a regular actions listing
        $customColumns[] = new \Fields_DummyField(array('name' => 'active', 'label' => 'Active', 'type' => 'S', 'width' => 3));
        //Now we re-load the custom columns that we've put together
        $design->SetColumnArray($customColumns);

        $securityWhereClause = MakeSecurityWhereClause('', 'ACT', $_SESSION['initials']);

        $module = \Sanitize::getModule($module);
        $callback = $_SERVER['QUERY_STRING'];
        $canEdit = ($_GET['print'] != 1 && $formType != 'Print' && $formType != 'ReadOnly' && $formType != 'Locked' && GetParm('ACT_PERMS') != 'ACT_READ_ONLY');

        if (\UnicodeString::strpos($callback, 'panel='))
        {
            $callback = preg_replace('/panel=[a-z_]+/u', 'panel=action_chains', $callback);
        }
        else
        {
            $callback .= '&panel=action_chains';
        }

        $callback = addslashes($callback);

        //This bit goes into some ridiculous SQL further down, do some formatting.
        $listingColumns = $design->getColumns();

        //catch UDFs
        foreach ($listingColumns as $col_field)
        {
            if (preg_match('/^UDF.*_([0-9]+)$/ui', $col_field, $matches))
            {
                $udf_field_ids[$col_field] = $matches[1];
            }
        }

        //Make sure we are only using allowed fields from ACT (custom fields not working atm)
        foreach ($listingColumns as $key => $column)
        {
            if (!isset($FieldDefs['ACT'][$column]) && $column != 'act_step_no')
            {
                $listingColumns[$key] = 'null as '.$column;
            }
        }

        //Some certain fields we need to make sure we get.
        if (!in_array('act_ddone', $listingColumns))
        {
            $listingColumns[] = 'act_ddone';
        }
        if (!in_array('recordid', $listingColumns))
        {
            $listingColumns[] = 'recordid';
        }
        if (!in_array('act_chain_id', $listingColumns))
        {
            $listingColumns[] = 'act_chain_id';
        }

        //For each column make sure we prefix it correctly for the mess of SQL that comes after this
        foreach($listingColumns as $key => $column)
        {
            if($column != 'act_type' && strpos($column, 'null as') === null)
            {
                $listingColumns[$key] = 'a.'.$column;
            }
            elseif(strpos($column, 'null as') !== null){}
            else
            {
                $listingColumns[$key] = 'COALESCE(c.description, a.act_type) as act_type';
            }
        }

        $columns = implode(', ',$listingColumns);

        //Everything that follows is a problem that existed before I put my dirty paws on it.
        foreach ($chains as $key => $chain)
        {
            // An active step is defined as (see sub query for active field below):
            // - an action the user has access to (via their security where clause)
            // - whose start date has passed OR whose start date is blank and isn't based on the completion of a previous step
            $sql = '
                SELECT
                    '.$columns.',
                    \''.\Sanitize::getModule($module).'\' as module, \'1\' as frommainrecord,
                    (CASE WHEN
                        (SELECT COUNT(recordid)
                         FROM ca_actions
                         WHERE ca_actions.recordid = a.recordid
                         AND (CAST( FLOOR( CAST(ca_actions.ACT_DSTART AS FLOAT)) AS DATETIME) <= CAST( FLOOR( CAST(GETDATE() AS FLOAT)) AS DATETIME)
                         OR (ca_actions.ACT_DSTART IS NULL AND ca_actions.act_start_after_step IS NULL))
                         '.($securityWhereClause != '' ? 'AND ('.$securityWhereClause.')' : '').'
                         )
                      > 0 THEN \'Y\' ELSE \'N\' END) as active
                FROM
                    ca_actions a
                LEFT JOIN
                    code_ra_acttype c ON a.act_type = c.code
                WHERE
                    a.act_module = :module AND a.act_cas_id = :act_cas_id AND a.act_chain_id = :act_chain_id AND a.act_chain_instance = :act_chain_instance
                ORDER BY
                    a.act_step_no
            ';

            $steps = \DatixDBQuery::PDO_fetch_all($sql, [
                'module' => $module,
                'act_cas_id' => $data['recordid'],
                'act_chain_id' => $chain['act_chain_id'],
                'act_chain_instance' => $chain['act_chain_instance']
            ]);

            // add done date column
            foreach ($steps as $no => $step)
            {
                $actId = $step['recordid'];

                if ($canEdit && GetParm('ACT_PERMS') == 'ACT_FULL' && $step['act_ddone'] == '' && $step['active'] == 'Y' && $act_ddone_available)
                {
                    $steps[$no]['act_ddone'] = $this->call('src\\actionchains\\controllers\\ActionChainController', 'showCompleteButton', [
                        'actId'     => $actId,
                        'module'    => $module,
                        'callback'  => $callback,
                        'scripturl' => $scripturl
                    ]);
                }
                else if ($act_ddone_available)
                {
                    $doneDate = FormatDateVal($step['act_ddone']);
                    $steps[$no]['act_ddone'] = $this->call('src\\actionchains\\controllers\\ActionChainController', 'showDoneDate', [
                        'canEdit'   => $canEdit,
                        'scripturl' => $scripturl,
                        'module'    => $module,
                        'actId'     => $actId,
                        'doneDate'  => $doneDate
                    ]);
                }
            }

            $recordList = new \RecordLists_RecordList();
            $recordList->AddRecordData($steps);

            $chains[$key]['listing'] = new \Listings_ActionChainListingDisplay($recordList, $design);
            $chains[$key]['listing']->Action = 'action';
            $chains[$key]['listing']->URLParameterArray = array('module','recordid','frommainrecord');

            if ($_GET['print'] == 1 || $formType == 'Print' || $formType == 'ReadOnly' || $formType == 'Locked')
            {
                $chains[$key]['listing']->ReadOnly = true;
            }
        }

        $recordid = \Sanitize::SanitizeInt($data['recordid']);
        $callback = urlencode($callback);
        $canDelete = ($canEdit && bYN(GetParm('DELETE_ACT_CHAIN', 'N')));
        $deleteConfirm = addslashes(_tk('action_chain_delete_confirm'));
        $addNew = _tk('add_new_action_chain');
        $calendarOnClick = GetParm('CALENDAR_ONCLICK', 'N');

        if (GetParm('FMT_DATE_WEB') == 'US')
        {
            $dateFmt= 'mm/dd/yy';
        }
        else
        {
            $dateFmt = 'dd/mm/yy';
        }

        $calendarWeekend = GetParm('CALENDAR_WEEKEND', 1);
        $weekStartDay = GetParm('WEEK_START_DAY', 2);

        $this->response->build('src/actionchains/views/GetActionChains.php', [
            'chains'          => $chains,
            'canDelete'       => $canDelete,
            'canEdit'         => $canEdit,
            'addNew'          => $addNew,
            'module'          => $module,
            'recordid'        => $recordid,
            'callback'        => $callback,
            'calendarOnClick' => $calendarOnClick,
            'date_fmt'         => $dateFmt,
            'calendarWeekend' => $calendarWeekend,
            'weekStartDay' => $weekStartDay,
            'deleteConfirm'   => $deleteConfirm
        ]);
    }

    /**
     * Displays the complete button used on action chain table.
     */
    public function showCompleteButton()
    {
        $this->response->build('src\actionchains\views\CompleteButton.php', [
            'actId'     => $this->request->getParameter('actId'),
            'module'    => $this->request->getParameter('module'),
            'callback'  => $this->request->getParameter('callback'),
            'scripturl' => $this->request->getParameter('scripturl')
        ]);
    }

    /**
     * Displays the done date used on action chain table.
     */
    public function showDoneDate()
    {
        $this->response->build('src\actionchains\views\DoneDate.php', [
            'canEdit'   => $this->request->getParameter('canEdit'),
            'scripturl' => $this->request->getParameter('scripturl'),
            'module'    => $this->request->getParameter('module'),
            'actId'     => $this->request->getParameter('actId'),
            'doneDate'  => $this->request->getParameter('doneDate')
        ]);
    }
}