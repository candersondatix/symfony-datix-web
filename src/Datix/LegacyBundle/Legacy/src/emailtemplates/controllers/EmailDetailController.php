<?php
namespace src\emailtemplates\controllers;

use src\framework\controller\Controller;

class EmailDetailController extends Controller
{
/*
 * getEmailDetails is designed to be called from ajax, with parameters passed via $_POST.
 *
 * getEmailDetails creates a temporary instance of an EmailTemplate object using the email
 * at id $_POST['emailid'] merged with the record number $_POST['recordid'] in module $_POST['module'].
 * Other parameters for EmailTemplate object creation are also passed.
 *
 * The function then returns HTML code (by echoing it for the ajax to pick up) which will replace
 * some item on the page (body, subject or duplicate options).
 *
 * Three seperate calls need to be made to get all three bits of HTML due to problems passing
 * complex HTML/Javascript within an XML response.
 */
    public function getEmailDetails()
    {
        require_once 'src/libs/EmailTemplateClass.php';
        $EmailTemplateObj = new NewEmailTemplate(Sanitize::SanitizeInt($_POST['emailid']));

        $EmailTemplateObj->ConstructEmailToSend(Sanitize::SanitizeInt($_POST['recordid']));

        switch ($_POST['returntype'])
        {
            case "body":
                echo $EmailTemplateObj->EmailBody;
                break;
            case "subject":
                echo $EmailTemplateObj->EmailSubject;
                break;
        }
    }
}