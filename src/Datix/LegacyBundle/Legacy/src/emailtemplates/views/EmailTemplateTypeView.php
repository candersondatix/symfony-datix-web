<?php if (!empty($this->TypeArray)) : ?>
    <?php echo ArrayToSelect(array('id' => 'emt_type', 'options' => $this->TypeArray)); ?>
<?php else : ?>
    <div id="emt_type_div"><select style="width:200px" disabled="true" /></div>
<?php endif ?>
