<?php
namespace src\emailtemplates\model;

use src\framework\model\EntityCollection;

class EmailTemplateCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\emailtemplates\\model\\EmailTemplate';
    }
}