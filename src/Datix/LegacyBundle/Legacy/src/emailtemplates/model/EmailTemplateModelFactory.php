<?php
namespace src\emailtemplates\model;

use src\framework\model\ModelFactory;

class EmailTemplateModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new EmailTemplateFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new EmailTemplateMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new EmailTemplateCollection($this->getMapper(), $this->getEntityFactory());
    }
}