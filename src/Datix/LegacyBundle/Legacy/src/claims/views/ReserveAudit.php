<style>
    #reserve-audit .tableHeader
    {
        background-color: #666;
    }

    #reserve-audit .listing-row, #reserve-audit .listing-row td
    {
       color: #666;
    }

    #reserve-audit .listing-row:hover, #reserve-audit .listing-row:hover td
    {
        background-color: #eee;
    }
</style>
<?php if (count($this->reserveAudit) > 0) : ?>
<table width="100%" id="reserve-audit">
    <tr class="tableHeader head2">
        <th width="8%"><?php echo $this->finIndemnityReserveLabel.' '._tk('adjustment'); ?></th>
        <th width="8%"><?php echo $this->finIndemnityReserveLabel.' '._tk('value'); ?></th>
        <th><?php echo _tk('reason_for_indem_change'); ?></th>
        <th width="8%"><?php echo $this->finExpensesReserveLabel.' '._tk('adjustment'); ?></th>
        <th width="8%"><?php echo $this->finExpensesReserveLabel.' '._tk('value'); ?></th>
        <th><?php echo _tk('reason_for_expen_change'); ?></th>
        <th width="15%"><?php echo _tk('changed_by'); ?></th>
        <th width="8%"><?php echo _tk('changed_on'); ?></th>
    </tr>
    <?php foreach ($this->reserveAudit as $dateTimeFrom => $row) : ?>
    <tr class="listing-row">
        <?php $ind = $row['fin_indemnity_reserve']['field_value'] == _tk('no_change') || $row['fin_indemnity_reserve']['field_value'] == '' || $row['fin_indemnity_reserve']['field_value'] == null; ?>
        <td <?php echo ($ind ? 'style="text-align: center;"' : ''); ?>>
            <?php echo ($row['fin_indemnity_reserve']['change_value'] > 0 ? '+' : '').($ind ? htmlspecialchars(_tk('no_change')) : FormatMoneyVal($row['fin_indemnity_reserve']['change_value'])); ?></td>
        <td <?php echo ($ind ? 'style="text-align: center;"' : ''); ?>>
            <?php echo ($ind ? htmlspecialchars(_tk('no_change')) : FormatMoneyVal($row['fin_indemnity_reserve']['field_value'])); ?></td>
        <td><?php echo $row['fin_indemnity_reserve']['reason_for_change']; ?></td>
        <?php $exp = $row['fin_expenses_reserve']['field_value'] == _tk('no_change') || $row['fin_expenses_reserve']['field_value'] == '' || $row['fin_expenses_reserve']['field_value'] == null; ?>
        <td <?php echo ($exp ? 'style="text-align: center;"' : ''); ?>>
            <?php echo ($row['fin_expenses_reserve']['change_value'] > 0 ? '+' : '').($exp ? htmlspecialchars(_tk('no_change')) : FormatMoneyVal($row['fin_expenses_reserve']['change_value'])); ?></td>
        <td <?php echo ($exp ? 'style="text-align: center;"' : ''); ?>>
            <?php echo ($exp ? htmlspecialchars(_tk('no_change')) : FormatMoneyVal($row['fin_expenses_reserve']['field_value'])); ?></td>
        <td><?php echo $row['fin_expenses_reserve']['reason_for_change']; ?></td>
        <td><?php echo ($row['fin_indemnity_reserve']['fullname']) ?: $row['fin_expenses_reserve']['fullname']; ?></td>
        <td><?php echo FormatDateVal($dateTimeFrom, true); ?></td>

    </tr>
    <?php endforeach; ?>
</table>
<?php else : ?>
<?php echo _tk('no_records_found'); ?>
<?php endif; ?>