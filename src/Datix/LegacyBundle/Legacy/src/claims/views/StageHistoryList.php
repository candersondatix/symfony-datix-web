<li>
    <?php echo $this->listing->GetListingHTML(); ?>
</li>
<?php if (!$this->readonly) : ?>
    <li class="new_link_row">
        <a href="Javascript:if(CheckChange()){editStageHistory();}"><b><?php echo _tk('stage_history_add_stage'); ?></b></a>
    </li>
    <script type="text/javascript">
        /**
         * Builds and displays the popup dialogue used to add/edit stages.
         */
        function editStageHistory(id)
        {
            var buttons = [];
            buttons[0] = {"value":"<?php echo _tk('btn_submit'); ?>","onclick":"jQuery(\'#stageHistoryForm\').submit();"};
            buttons[1] = {"value":"<?php echo _tk('btn_cancel'); ?>","onclick":"GetFloatingDiv(\'newActionChain\').CloseFloatingControl();"};
            var title = id == undefined ? "<?php echo _tk('stage_history_add_stage'); ?>" : "<?php echo _tk('stage_history_edit_stage'); ?>";
            PopupDivFromURL("newActionChain", title, scripturl+"?action=httprequest&type=editstagehistory&id="+id+"&cas_id=<?php echo $this->cas_id; ?>&callback=<?php echo $this->callback; ?>", "", buttons, "370px");
            createCalendar(jQuery('.date'), "<?php echo $this->calendarOnClick; ?>", "<?php echo $this->date_fmt; ?>", <?php echo $this->weekStartDay; ?>, <?php echo $this->calendarWeekend; ?>);
        }

        /**
         * Handles the deletion of stages.
         *
         * @param {int} id           The recordid of the stage we're deleting.
         * @param {int} mainRecordId The id of the main record where the stage is attached.
         */
        function deleteStageHistory(id, mainRecordId)
        {
            if (confirm("<?php echo _tk('stage_history_delete_confirm'); ?>"))
            {
                jQuery.get(scripturl+"?action=httprequest&type=deletestagehistory&id="+id+"&mainrecordid="+mainRecordId);
                jQuery("#stage_history_"+id).closest(".listing-row").remove();
            }
        }
    </script>
<?php endif ?>