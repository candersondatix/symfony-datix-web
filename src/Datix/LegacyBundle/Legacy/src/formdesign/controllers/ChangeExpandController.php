<?php

namespace src\formdesign\controllers;

use src\framework\controller\Controller;

class ChangeExpandController extends Controller
{
    function changeexpand()
    {
        global $FieldDefs;

        $TargetType = $this->request->getParameter('targettype');
        $SectionArray = array();
        $FieldList = array();
        $Module = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $FormLevel = \Sanitize::SanitizeString($this->request->getParameter('formlevel'));
        $FormId = \Sanitize::SanitizeString($this->request->getParameter('formid'));

        require_once 'Source/FieldDefs.php';
        require_once 'Source/libs/FormClasses.php';

        if ((isset($Module) && $Module != '') && (isset($FormLevel) && $FormLevel != '') &&
            (isset($FormId) && $FormId != ''))
        {
            $FormDesign = new \Forms_FormDesign(array(
                'module' => $this->request->getParameter('module'),
                'level' => $this->request->getParameter('formlevel'),
                'id' => $this->request->getParameter('formid')
            ));
            $FormDesign->PopulateFormDesign(array('NonExistentFile' => 'DEFAULT'));
        }

        switch($TargetType)
        {
            case 'section':
                $RecursionLimitations = $_SESSION['RecursionLimitations']['actions'][$this->request->getParameter('originalfield')];

                if (is_array($_SESSION['SectionsSetup']))
                {
                    foreach ($_SESSION["SectionsSetup"] as $Section => $SectionName)
                    {
                        if (!$RecursionLimitations || ($RecursionLimitations &&
                            !in_array($Section, $_SESSION['RecursionLimitations']['actions'][$this->request->getParameter('originalfield')])))
                        {
                            if (IsCentrallyAdminSys() && !IsCentralAdmin())
                            {
                                if (isset($FormDesign) && isset($FormDesign->MoveFieldsToSections))
                                {
                                    if (!empty($FormDesign->LockFieldAtribs))
                                    {
                                        foreach ($FormDesign->LockFieldAtribs as $Field => $Attributes)
                                        {
                                            if (is_array($FormDesign->MoveFieldsToSections[$Field]) &&
                                                $FormDesign->MoveFieldsToSections[$Field]['New'] != $Section)
                                            {
                                                $SectionArray[$Section] = $SectionName;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $SectionArray[$Section] = $SectionName;
                                    }
                                }
                                else
                                {
                                    $SectionArray[$Section] = $SectionName;
                                }
                            }
                            else
                            {
                                $SectionArray[$Section] = $SectionName;
                            }
                        }
                    }
                }

                $targetDropdown = \Forms_SelectFieldFactory::createSelectField('sections', '', $this->request->getParameter('section'), '');
                $targetDropdown->setCustomCodes($SectionArray);
                break;
            case 'field':
                if (is_array($_SESSION['FieldsSetup'][$this->request->getParameter('currentsection')]))
                {
                    $FieldList = $_SESSION['FieldsSetup'][$this->request->getParameter('currentsection')];
                }

                // Check for fields which need to be excluded from the list because they would cause a recursive loop.
                $ExcludedFields = $_SESSION['RecursionLimitations']['field_actions'][$this->request->getParameter('originalfield')];

                if (is_array($ExcludedFields))
                {
                    $FieldList = array_diff_key($FieldList, array_flip($ExcludedFields));
                }

                if (is_array($_SESSION['LockedFieldsHIDE']))
                {
                    $FieldList = array_diff_key($FieldList, array_flip($_SESSION['LockedFieldsHIDE']));
                }

                unset($FieldList[$this->request->getParameter('originalfield')]);

                $targetDropdown = \Forms_SelectFieldFactory::createSelectField('fields', '', $this->request->getParameter('targetfield'), '');
                $targetDropdown->setCustomCodes($FieldList);
                break;
        }

        $valuesSelect = \Forms_SelectFieldFactory::createSelectField(
            $this->request->getParameter('originalfield'),
            $this->request->getParameter('module'),
            urldecode($this->request->getParameter('fieldvalues')),
            '',
            true
        );
        $valuesSelect->setAltFieldName('valuesselect');
        $valuesSelect->setIgnoreMaxLength();

        if (isset($FieldDefs[$this->request->getParameter('module')][$this->request->getParameter('originalfield')]['FieldFormatsName']))
        {
            $valuesSelect->setFieldFormatsName($FieldDefs[$this->request->getParameter('module')][$this->request->getParameter('originalfield')]['FieldFormatsName']);
        }

        if (!$this->request->getParameter('udf'))
        {
            if (!isset($FieldDefs[$this->request->getParameter('module')][$this->request->getParameter('originalfield')]))
            {
                $this->request->setParameter('module', getModuleFromField($this->request->getParameter('originalfield')));
            }

            if (in_array($FieldDefs[$this->request->getParameter('module')][$this->request->getParameter('originalfield')]['Type'], array('yesno','checkbox')))
            {
                $valuesSelect->setCustomCodes(array('Y' => 'Yes', 'N' => 'No'));
            }
        }

        $this->response->build('src/formdesign/views/ChangeExpand.php', array(
            'TargetType' => $TargetType,
            'sectionindex' => $this->request->getParameter('sectionindex'),
            'inputfield' => $this->request->getParameter('inputfield'),
            'fieldindex' => $this->request->getParameter('fieldindex'),
            'targetDropdown' => $targetDropdown,
            'valuesSelect' => $valuesSelect,
            'alerttext' => $this->request->getParameter('alerttext')
        ));
    }
}