<script language="JavaScript" type="text/javascript">
    function validateOnSubmit()
    {
        if (!submitClicked)
        {
            return false;
        }

        if (document.forms[0].formaction.value == 'cancel')
        {
            return true;
        }

        if (!validateEmpty(document.forms[0].form_name.value))
        {
            moveScreenToField('form_name', '');
            alert("You must enter the name of the form");
            return false;
        }

        return true;
    }

    var submitClicked = false;
</script>
<form method="post" action="<?php echo $this->scripturl; ?>?action=savenewformdesign" name="newform" onSubmit="return(validateOnSubmit())">
    <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
        <tr>
            <td class="windowbg2" width="30%"><b>Name of form</b></td>
            <td class="windowbg2" width="70%"><input type="text" size="40" name="form_name"></td>
        </tr>
        <?php /*<tr>
            <td class="windowbg2" width="30%"><b>Copy design from which form?</b></td>
            <td class="windowbg2" width="70%">
                <select name="copy_from">
                    <?php if($this->formlevel == 2) : ?>
                        <?php foreach ($this->FormFiles2 as $Number => $Name) : ?>
                            <option value="<?php echo $Number; ?>"><?php echo $Name; ?></option>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <?php foreach ($this->FormFiles as $Number => $Name) : ?>
                            <option value="<?php echo $Number; ?>"><?php echo $Name; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>

            </td>
        </tr>*/ ?>
        <tr>
            <td class="windowbg2" width="30%"><b>Copy design from which form?</b></td>
            <td class="windowbg2" width="70%"><?php echo $this->fieldContents['copy_from']->GetField(); ?></td>
        </tr>
        <tr>
            <td class="titlebg" align="center" colspan="2">
                <input type="hidden" name="formaction" value="save">
                <input type="hidden" name="module" value="<?php echo Escape::EscapeEntities($this->module); ?>">
                <input type="hidden" name="formlevel" value="<?php echo Escape::EscapeEntities($this->formlevel); ?>">
                <input type="button" value="Save new form" onclick="submitClicked=true; document.forms[0].formaction.value='save'; if(validateOnSubmit()){document.forms[0].submit();}">
                <input type="submit" value="Cancel" onclick="submitClicked=true; document.forms[0].formaction.value='cancel';">
            </td>
        </tr>
    </table>
</form>