<form method="POST" enctype="multipart/form-data" id="importprofile" name="importprofile" action="<?php echo $this->scripturl; ?>?action=doimportimportprofile">
    <div class="new_titlebg">
        <div class="title_text_wrapper"><b><?php echo _t('Choose file to import'); ?></b></div>
    </div>
    <ol>
        <li class="field_div" name="_row" id="_row" >
            <div class="field_label_div" style="width: 25%;">
                <b>Import from this file:</b>
            </div><div class="field_input_div" style="width: 70%;"><input name="userfile" type="file"  size="50"/></div>
        </li>
    </ol>
    <div class="button_wrapper">
        <button type="button" value="import" onclick="submitClicked=true;jQuery('#importprofile').submit();"><?php echo _t('Import'); ?></button>
        <button type="button" value="cancel" onclick="SendTo('<?php echo $this->scripturl; ?>?action=listimportprofiles&module=INC');"><?php echo _tk('btn_cancel'); ?></button>
    </div>
</form>