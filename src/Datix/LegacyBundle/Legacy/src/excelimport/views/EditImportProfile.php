<form method="POST" id="editimportprofle" name="editimportprofle" action="<?php echo $this->scripturl; ?>?action=saveimportprofile">
    <div class="new_titlebg">
        <div class="title_text_wrapper"><b><?php echo _tk('import_profiles'); ?></b></div>
    </div>
    <?php echo $this->Table->GetFormTable(); ?>
    <div class="button_wrapper">
        <input type="hidden" name="exp_profile_form" value="<?php echo $this->Data['exp_profile_form']; ?>" />
        <button type="button" value="save" onclick="submitClicked=true;if(validateOnSubmit()){jQuery('#editimportprofle').submit();}"><?php echo _tk('btn_save'); ?></button>
        <button type="button" value="cancel" onclick="SendTo('<?php echo $this->scripturl; ?>?action=home&module=ADM');"><?php echo _tk('btn_cancel'); ?></button>
        <?php if($this->Data['exp_profile_id']) : ?>
        <button type="button" value="delete" onclick="if(confirm('Are you sure you want to delete this profile?')){SendTo('<?php echo $this->scripturl; ?>?action=deleteimportprofile&recordid=<?php echo $this->Data['exp_profile_id']; ?>');}"><?php echo _tk('btn_delete'); ?></button>
        <button type="button" value="export" onclick="SendTo('<?php echo $this->scripturl; ?>?action=exportimportprofile&recordid=<?php echo $this->Data['exp_profile_id']; ?>');"><?php echo _t('Export Profile'); ?></button>
        <?php endif; ?>
        <button type="button" value="back" onclick="SendTo('<?php echo $this->scripturl; ?>?action=listimportprofiles&module=<?php echo $this->Data['exp_profile_module']; ?>');"><?php echo _tk('btn_back'); ?></button>
    </div>
</form>
<script language="Javascript">
    submitClicked = false;

    <?php echo $this->JSValidation; ?>

    function ShowTransform()
    {
        jQuery('.mapping-div').hide();
        jQuery('#mapping-' + this.grid.columns[0].getValue(this.rowIdx)).show();

        //we need to resize the rico divs, since they behave strangely if they have been created while hidden.
        <?php if (is_array($this->mappingGridIDs)) : ?>
            <?php foreach($this->mappingGridIDs as $recordid => $id) : ?>
                <?php echo $id; ?>['grid'].sizeDivs();
            <?php endforeach; ?>
        <?php endif; ?>
    }

    jQuery('.mapping-div').hide();
</script>