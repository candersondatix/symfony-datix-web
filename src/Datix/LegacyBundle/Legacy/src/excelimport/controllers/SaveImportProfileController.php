<?php

namespace src\excelimport\controllers;

use src\framework\controller\Controller;

class SaveImportProfileController extends Controller
{
    function saveimportprofile()
    {
        $Profile = new \ImportProfile(new \DatixDBQuery(''), $this->request->getParameter('exp_profile_id'), $this->request->getParameter('exp_profile_module'));

        $Profile->setName($this->request->getParameter('exp_profile_name'));
        $Profile->setForm($this->request->getParameter('exp_profile_form'));
        $Profile->setStartAt($this->request->getParameter('exp_start_row'));
        $Profile->setType($this->request->getParameter('exp_profile_type'));
        $Profile->setRowHeader($this->request->getParameter('exp_profile_rowheader'));

        $Profile->SaveToDB();

        AddSessionMessage('INFO', 'Import Profile Saved');

        // Need to redirect because rico tables reload the page they are on and this causes problems with ID
        // generation if we are on a save page.
        $this->redirect('app.php?action=editimportprofile&exp_profile_id='.$Profile->getID());
    }
}