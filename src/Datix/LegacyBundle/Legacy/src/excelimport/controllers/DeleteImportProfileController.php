<?php

namespace src\excelimport\controllers;

use src\framework\controller\Controller;

class DeleteImportProfileController extends Controller
{
    function deleteimportprofile()
    {
        $Profile = new \ImportProfile(new \DatixDBQuery(''), $this->request->getParameter('recordid'));
        $Profile->DeleteFromDB();

        AddSessionMessage('INFO', 'Import Profile Deleted');

        $this->call('src\admin\controllers\ListExcelImportProfilesTemplateController', 'listimportprofiles', array());
    }
}