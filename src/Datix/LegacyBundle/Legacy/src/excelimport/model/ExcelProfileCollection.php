<?php
namespace src\excelimport\model;

use src\framework\model\EntityCollection;

class ExcelProfileCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\excelimport\\model\\ExcelProfile';
    }
}