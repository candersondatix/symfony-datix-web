<?php
namespace src\excelimport\model;

use src\framework\model\ModelFactory;

class ExcelProfileModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ExcelProfileFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ExcelProfileMapper(new \DatixDBQuery(), $this);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ExcelProfileCollection($this->getMapper(), $this->getEntityFactory());
    }
}