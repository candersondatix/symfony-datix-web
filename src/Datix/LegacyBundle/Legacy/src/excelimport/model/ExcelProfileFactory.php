<?php
namespace src\excelimport\model;

use src\framework\model\EntityFactory;

class ExcelProfileFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\excelimport\\model\\ExcelProfile';
    }
}