<?php

namespace src\excelimport\model;

use src\framework\model\RecordEntity;
use src\framework\query\Query;

/**
 *
 */
class ExcelProfile extends RecordEntity
{
    /**
     * The id of the Excel Profile
     * @var int
     */
    protected $exp_profile_id;

    /**
     * The Excel Profile name.
     *
     * @var string
     */
    protected $exp_profile_name;

    /**
     * The module table name that this Excel Profile is assigned.
     *
     * @var string
     */
    protected $exp_profile_form;

    /**
     * The index to start reading profiles.
     *
     * @var int
     */
    protected $exp_start_row;

    /**
     * The Excel Profile type. (Can be Excel or CSV)
     *
     * @var string
     */
    protected $exp_profile_type;

    /**
     * The value of the header row.
     *
     * @var string
     */
    protected $exp_profile_rowheader;

    /**
     * {@inherit}
     */
    protected static function setIdentity()
    {
        return array('exp_profile_id');
    }
}