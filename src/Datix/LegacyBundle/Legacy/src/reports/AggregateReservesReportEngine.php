<?php

namespace src\reports;

use src\reports\model\report\Report;

/**
 * Class AggregateReservesReportEngine
 * @package src\reports
 *
 * Used for relabeling the tooltips to show the user selected date.
 */
class AggregateReservesReportEngine extends GraphicalReportEngine
{
    /**
     * {@inheritdoc}
     */
    protected function generateToolText(Report $report, $value, $headerText, $percentage = Report::TOOLTIP_VALUE_ONLY)
    {
        $toolText = parent::generateToolText($report, $value, $headerText, $percentage);

        switch ($report->count_style_field_value_at)
        {
            case Report::TODAY:
                $dateFormat = (GetParm('FMT_DATE_WEB', 'GB') == 'US' ? 'm/d/Y' : 'd/m/Y');
                $dateLabel = ' ('._tk('field_value_at').' '.(new \DateTime())->format($dateFormat).')';
                break;
            case Report::SELECT_DATE:
                $dateLabel = ' ('._tk('field_value_at').' '.FormatDateVal($report->count_style_field_calendar).')';
                break;
        }

        return $toolText.$dateLabel;
    }
}