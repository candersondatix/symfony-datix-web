/**
 * Similar to the PromptUserAddToMyReports() function, this function works on session-stored reports.
 *
 * Presents the user with a popup to name the current report, and then saves it to packaged reports. Can also add to the dashboard
 * if desired.
 */
function PromptForReportTitle(rep_name, module, dashboard, rep_id, as_new)
{
    // no name? assume generated default
    if (rep_name == '')
    {
        rep_name = jQuery ('#current_rep_name').val()/* || jQuery ('#rep_name').val()*/; // if we've got a listing report
        jQuery ('#title').val(rep_name);
    }

    jQuery.ajax({
        url: scripturl+'?action=httprequest&type=addreportprompt&dashboard='+dashboard+'&module='+module+'&rep_id='+rep_id+'&rep_name='+encodeURIComponent(rep_name),
        success: function(data)
        {
            var sHTML = SplitJSAndHTML(data);
            AlertWindow = AddNewFloatingDiv('PromptUserAddToMyReports');

            // no extra options to display on modal?
            if (jQuery.trim(sHTML) == '')
            {
                if (rep_name == '')
                {
                    // this should never happen
                    alert ('Report name is missing');
                    return false;
                }

                SaveReportPrompt(dashboard, module, as_new, rep_id);
                return true;
            }

            AlertWindow.setContents(sHTML);

            //TODO - figure out a better way to get this to work so that the title doesn't have to be hard encoded
            if (dashboard)
            {
                AlertWindow.setTitle("Set Report Title");
            }
            else
            {
                AlertWindow.setTitle("Select @prompt setting");
            }

            var buttonHTML = '';

            buttonHTML += '<input type="button" value="Save" onClick="SaveReportPrompt('+dashboard+', \''+module+'\', '+as_new+', '+rep_id+');" />';
            buttonHTML += '<input type="button" value="Cancel" onClick="var div = GetFloatingDiv(\'PromptUserAddToMyReports\');div.CloseFloatingControl();" />';
            AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);
            AlertWindow.display();

            jQuery('.ff_select').each(function()
            {
                resizeInput(jQuery(this));
            });

            RunGlobalJavascript();
        },
        error: function(request, status, error)
        {
            OldAlert(request.responseText);
        }
    });
}

/**
 * Saves a packaged report to My Reports/Dasboard.
 * 
 * @param dashboard The Dashboard ID (if any).
 * @param module    The report module.
 */
function SaveReportPrompt(dashboard, module, as_new, rep_id)
{
    var ReportTitle = dashboard ? encodeURIComponent(jQuery('#title').val()) : encodeURIComponent(jQuery('#current_rep_name').val());
    var UseValues = jQuery('#use_values').val();
    var TargetDashboard = jQuery('#target_dashboard').val();

    var div = GetFloatingDiv('PromptUserAddToMyReports');
    div.CloseFloatingControl();

    if (dashboard)
    {
        jQuery.getJSON(scripturl+'?action=addcurrenttomydashboard&responsetype=json&title='+ReportTitle+'&use_values='+UseValues+'&dash_id='+TargetDashboard, function(data) {
            if(data.success)
            {
                SendTo(scripturl+'?action=dashboard&dash_id='+data.dash_id);
            }
            else
            {
                OldAlert('An error occurred');
            }
        });
    }
    else
    {
        jQuery.ajax({
            url: scripturl+'?action=httprequest&type=addtomyreports&title='+ReportTitle+'&use_values='+UseValues+'&as_new='+as_new+'&recordid='+rep_id,
            type: "GET",
            success: function()
            {
                SendTo(scripturl+'?action=listmyreports&module='+module);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert(XMLHttpRequest.responseText);
            }
        });
    }
}

function chart_rendered() {

    /**
     * Currently unused but required to stop console error when showing reports outside of dashboard
     * Could be used to display charts in a more pleasing way
     */
}

/**
 * Highlights the CSV export option as a suggested method to export large listing reports.
 */
function suggestCsvExport() {

    jQuery("#csv_label").qtip({
        show: true,
        hide: false,
        overwrite: false,
        style: {
            classes: 'qtip-red'
        },
        position: {
            my: 'left center',
            at: 'right center',
            adjust: {
                x: 5,
                scroll: false,
                resize: false
            }
        }
    });
}

function removeSuggestCsvExport() {

    var $csvElem = jQuery("#csv_label");

    if('object' === typeof $csvElem.data('qtip')) {

        $csvElem.qtip('destroy', true);
    }
}