/**
 * Controls the paging of listing report output.
 */
(function($) {

    $.fn.listingPager = function(options) {

        /**
         * Used to track whether or not we're currently fetching the next page of the report.
         *
         * @var boolean
         */
        var processing = false;

        /**
         * Whether or not we've loaded the report data in its entirety.
         *
         * @var boolean
         */

        var $obj = this,
            defaults = {
                viewport: $obj.find(".report-viewport"),
                recordid: '',
                context: 1,
                startPosition: 0,
                endOfReport: false,
                previousRow: {},
                columnTotals: {}
            },
            settings = $.extend({}, defaults, options),
            $viewport = settings.viewport,
            $listingTable = $viewport.find('#listingtable'),
            $loading = $('<div class="info-bar"><img src="Images/loading.gif"></div>'),
            $error = $('<div class="error">' + globals.error_loading_report_data + ' - <a class="load-retry">' + globals.retry_text + '</a></div>'),
            $endOfReport = $('<div class="info-bar">' + globals.end_of_report + '</div>');

        /**
         * Whether or not the report viewport currently has a vertical scroll bar.
         *
         * @return boolean
         */
        function windowHasVScroll() {

            return $listingTable.height() > $viewport.height();
        }

        function showLoadError() {

            $loading.remove();
            $error.remove();
            $viewport.append($error);
            processing = false;
            settings.endOfReport = true;
        }

        /**
         * Decide if more pages need loading or if we can stop
         * @returns {boolean}
         */
        function loadPages() {

            if(settings.endOfReport && ! $viewport.find($endOfReport).length) {

                $viewport.append($endOfReport);
            }

            if( ! windowHasVScroll() && ! settings.endOfReport) {

                loadNextPage();

                return false;
            }
            else if( ! windowHasVScroll() && settings.endOfReport) {

                $viewport.height('auto');

                return true;
            }
        }

        /**
         * Fetches the next page of data and appends it to the report output.
         */
        function loadNextPage() {

            if (processing || settings.endOfReport) {

                return;
            }

            processing = true;

            var url = settings.context == 2 ? 'app.php?action=loaddashboardlistingreportpage' : 'app.php?action=loadlistingreportpage';

            $error.remove();
            $loading.remove();
            $viewport.append($loading);

            $.ajax(
                {
                    url: url,
                    data: {
                        'recordid':      settings.recordid,
                        'context':       settings.context,
                        'startPosition': settings.startPosition,
                        'previousRow':   settings.previousRow,
                        'columnTotals':  settings.columnTotals
                    },
                    type: 'POST',
                    dataType: 'text',
                    success: function (response, status, jqXHR) {

                        try {

                            eval("var response = " + response + ";");

                            if(response.status == 'error') {

                                showLoadError();

                                return;
                            }

                            settings.endOfReport = response.endOfReport;
                            settings.startPosition = response.startPosition;
                            settings.previousRow = response.previousRow;
                            settings.columnTotals = response.columnTotals;
                            $listingTable.append(response.output);
                            $loading.remove();
                            processing = false;

                            if(settings.endOfReport && ! $endOfReport.length) {

                                $viewport.append($endOfReport);
                            }

                            loadPages();

                        } catch (e) {

                            showLoadError();
                        }
                    },
                    error: function () {

                        showLoadError();
                    }
                });
        }

        $obj.init = function () {

            if(loadPages()) {

                return;
            }

            // register onscroll event
            $viewport.on('scroll', function() {

                // load next page if viewport bottom edge is 1000 pixels from end of current records
                if (($viewport.scrollTop() + $viewport.height()) >= ($listingTable.height() - 1000)) {

                    loadNextPage();
                }
            });

            // Check if the report is processing and block scrolling of the main window to stop unneccessary scrolling
            $viewport.on('mousewheel', function(e) {

                if(processing) {

                    // Prevent scrolling and then mock scrolling so that you can still scroll the viewports.  Scroll.
                    e.preventDefault();

                    var viewScroll = $viewport.scrollTop(),
                        scrollAmount = -(e.originalEvent.wheelDelta);

                    $viewport.scrollTop(viewScroll + scrollAmount);

                    return false;
                }
            });

            $viewport.on('click', '.load-retry', function() {

                settings.endOfReport = false;
                loadNextPage();
            });
        };

        $obj.destroy = function() {

            $viewport.off('mousewheel scroll click');
        };

        return $obj;
    };
    
})(jQuery);