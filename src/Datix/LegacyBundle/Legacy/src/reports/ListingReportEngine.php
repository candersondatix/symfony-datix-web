<?php

namespace src\reports;

use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\listingreport\Listing;
use src\reports\model\listingreport\column\Column;
use src\reports\model\listingreport\column\ColumnCollection;
use src\reports\model\listingreport\groupby\GroupByCollection;
use src\reports\controllers\ReportWriter;
use src\reports\exceptions\ReportPagingException;
use src\reports\views\helpers\ListingReportCell;
use src\reports\views\helpers\ListingReportGroupHeader;
use src\reports\querybuilder\IReportQueryBuilder;
use src\framework\query\QueryFactory;
use src\framework\registry\Registry;
use src\system\database\FieldInterface;

class ListingReportEngine extends ReportEngine
{
    /**
     * {@inheritdoc}
     */
    protected function formatData(PackagedReport $packagedReport, array $data, $context = ReportWriter::FULL_SCREEN)
    {
        if (empty($data))
        {
            // no records in the report
            $packagedReport->report->endOfReport = true;
            return $data;
        }
        
        if (ReportWriter::FULL_SCREEN == $context || ReportWriter::DASHBOARD == $context)
        {
            $packagedReport->report->incrementStartPosition(count($data));
        }
        
        switch ($context)
        {
            case ReportWriter::EXPORT_CSV;
                $formattedData = $this->formatDataForCsv($packagedReport, $data);
                break;
                
            default:
                $formattedData = $this->formatDataDefault($packagedReport, $data, $context);
        }
        
        return $formattedData;
    }
    
    /**
     * The default case for formatting data.  Handles the majority of cases for formatting listing report data.
     * 
     * @param PackagedReport $packagedReport
     * @param array          $data
     * @param int            $context
     * 
     * @return array
     */
    protected function formatDataDefault(PackagedReport $packagedReport, array $data, $context)
    {
        if ($packagedReport->report->merge_repeated_values)
        {
            return $this->formatDataMergeRepeated($packagedReport, $data, $context);
        }
        else
        {
            return $this->formatDataStandard($packagedReport, $data, $context);
        }
    }
    
    /**
     * Standard method for formatting data for output. 
     * 
     * @param PackagedReport $packagedReport
     * @param array          $data
     * @param int            $context
     * 
     * @return array
     */
    protected function formatDataStandard(PackagedReport $packagedReport, array $data, $context)
    {
        $formattedData = [];
        $report        = $packagedReport->report;
        $columns       = $report->columns->toArray();
        $previousRow   = $report->previousRow;
        $module        = $report->module;
        $includeLinks  = ReportWriter::FULL_SCREEN == $context && !$packagedReport->override_security;
        $moduleDefs    = $this->registry->getModuleDefs();
        $table         = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];
        
        if ($includeLinks)
        {
            $url = $this->getLinkUrl($module);
        }

        foreach ($data as $row)
        {
            if ($includeLinks)
            {
                $link = $url.'&recordid='.$row[$table.'_id'];
            }
            
            $formattedData = $this->formatGroupHeaders($report, $formattedData, $row, $previousRow);
            $formattedRow  = [];
            
            foreach ($columns as $column)
            {
                $rawData = $row[$column->getAlias()];
                $align   = 'left';
                
                if (FieldInterface::MONEY == $column->getType())
                {
                    $align = 'right';
                    $column->incrementTotals($row[$column->getIdDataKey()], $rawData);
                }
                
                $output = $column->formatData($rawData, $context);
                $formattedRow[] = new ListingReportCell($output, $link, $column->getColour($rawData), $align);
            }
            
            $formattedData[] = $formattedRow;
            $previousRow = $row;
        }
        
        $report->previousRow = $previousRow;
        
        if ($report->endOfReport)
        {
            $formattedData = $this->formatGroupTotals($formattedData, $report->columns);
            $formattedData = $this->formatTotals($formattedData, $report->columns);
        }
        
        return $formattedData;
    }
    
    /**
     * Formats data for output when merging repeated values. 
     * 
     * @param PackagedReport $packagedReport
     * @param array          $data
     * @param int            $context
     * 
     * @return array
     */
    protected function formatDataMergeRepeated(PackagedReport $packagedReport, array $data, $context)
    {
        $formattedData = [];
        $report        = $packagedReport->report;
        $columns       = $report->columns->toArray();
        $columnNo      = count($columns);
        $previousRow   = $report->previousRow;
        $module        = $report->module;
        $includeLinks  = ReportWriter::FULL_SCREEN == $context && !$packagedReport->override_security;
        $moduleDefs    = $this->registry->getModuleDefs();
        $table         = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];
        $usingGrouping = $report->groupBys->count() > 0;
        $cellMap       = [];
        $cells         = array_fill(0, $columnNo, []);
        $recordid      = 0;
        $colNum        = 0;
        
        if ($includeLinks)
        {
            $url = $this->getLinkUrl($module);
        }

        foreach ($data as $row)
        {
            $newRecord = $row[$table.'_id'] != $recordid;
            $recordid  = $row[$table.'_id'];
            
            if ($includeLinks)
            {
                $link = $url.'&recordid='.$recordid;
            }
            
            if ($usingGrouping)
            {
                $groupHeaders = $this->formatGroupHeaders($report, [], $row, $previousRow);
                
                if (!empty($groupHeaders))
                {
                    $formattedData = array_merge($formattedData, $groupHeaders);
                }
            }
            
            if ($newRecord || ($usingGrouping && !empty($groupHeaders)))
            {
                // reset the cell map to prevent the rowspan for cells in previous groups/records being incorrectly incremented
                $cellMap = [];
            }
            
            $formattedData[] = array_fill(0, $columnNo, null);
            
            foreach ($columns as $column)
            {
                $rawData = $row[$column->getAlias()];
                $output  = $column->formatData($rawData, $context);
                $mapId   = $column->getAlias().'.'.$row[$column->getIdDataKey()];
                $align   = 'left';
                
                if (FieldInterface::MONEY == $column->getType())
                {
                    $align = 'right';
                    $column->incrementTotals($row[$column->getIdDataKey()], $rawData);
                }
                    
                if (!$newRecord && null !== ($existingCell = $cellMap[$mapId]))
                {
                    $existingCell->incrementRowSpan();
                }
                else
                {
                    $cell = new ListingReportCell($output, $link, $column->getColour($rawData), $align);
                    $cells[$colNum][] = $cell;
                    $cellMap[$mapId]  = $cell;
                }
                ++$colNum;
            }
            $previousRow = $row;
            $colNum = 0;
        }
        
        $report->previousRow = $previousRow;
        
        if ($report->endOfReport)
        {
            $formattedData = $this->formatGroupTotals($formattedData, $report->columns);
            $formattedData = $this->formatTotals($formattedData, $report->columns);
        }
        
        foreach ($cells as $colNum => $column)
        {
            $row = 0;
            
            foreach ($column as $cell)
            {
                if ($usingGrouping)
                {
                    while ($formattedData[$row] instanceof ListingReportGroupHeader || $formattedData[$row][$colNum] instanceof ListingReportCell)
                    {
                        ++$row;
                    }
                }
                $formattedData[$row][$colNum] = $cell;
                $row += $cell->getRowSpan();
            }
        }
        
        return $formattedData;
    }
    
    /**
     * Determines whether or not we need to output any group headers 
     * at this point in the report and, if so, adds them to the output.
     * 
     * @param Listing $listingReport The listing report design.
     * @param array   $formattedData The formatted-for-output data array.
     * @param array   $row           The current raw data row being processed.
     * @param array   $previousRow   The previous raw data row.
     * 
     * @return array $formattedData The (potentially modified) formatted-for-output data array.
     */
    protected function formatGroupHeaders(Listing $listingReport, array $formattedData, array $row, array $previousRow = null)
    {
        $groupBys = $listingReport->getGroupBys()->toArray();
        
        if (0 == count($groupBys))
        {
            return $formattedData;
        }
        
        $colCount = $listingReport->getColumns()->count();
        $hasMoneyFields = $listingReport->getColumns()->hasMoneyFields();
        
        foreach ($groupBys as $indent => $groupBy)
        {
            $dataKey = $groupBy->getAlias();
            
            $previousValue = null !== $previousRow ? $groupBy->formatData($previousRow[$dataKey]) : null;
            $currentValue  = $groupBy->formatData($row[$dataKey]);
            
            if (null === $previousRow || $newGroup || $currentValue != $previousValue)
            {
                if (null !== $previousRow && !$newGroup && $hasMoneyFields)
                {
                    $formattedData = $this->formatGroupTotals($formattedData, $listingReport->getColumns());
                }
                
                $formattedData[] = new ListingReportGroupHeader($groupBy->getTitle(), $currentValue, $indent, $colCount);
                ++$indent;
                $newGroup = true;
            }
        }
        
        return $formattedData;
    }
    
    /**
     * Adds a group totals row to the report output to display totals for money fields.
     * 
     * @param array            $formattedData
     * @param ColumnCollection $columns
     * 
     * @return array $formattedData
     */
    protected function formatGroupTotals(array $formattedData, ColumnCollection $columns)
    {
        if (!$columns->hasMoneyFields())
        {
            return $formattedData;
        }
        
        $groupTotals = [];
        
        foreach ($columns->toArray() as $column)
        {
            $data = '';
            if (FieldInterface::MONEY == $column->getType())
            {
                $data = $column->formatData($column->getGroupTotal());
                $column->resetGroupTotals();
            }
            $groupTotals[] = new ListingReportCell($data, null, 'C5DBFB', 'right');
        }
        
        $formattedData[] = $groupTotals;
        return $formattedData;
    }
    
    /**
     * Adds the totals row to the report output to display totals for money fields.
     * 
     * @param array            $formattedData
     * @param ColumnCollection $columns
     * 
     * @return array $formattedData
     */
    protected function formatTotals(array $formattedData, ColumnCollection $columns)
    {
        if (!$columns->hasMoneyFields())
        {
            return $formattedData;
        }
        
        $totals = [];
        
        foreach ($columns->toArray() as $column)
        {
            $data = '';
            if (FieldInterface::MONEY == $column->getType())
            {
                $data = $column->formatData($column->getTotal());
                $column->resetTotals();
            }
            $totals[] = new ListingReportCell($data, null, 'ABC9F7', 'right');
        }
        
        $formattedData[] = $totals;
        return $formattedData;
    }
    
    /**
     * Provides a formatted data set for output to csv.
     * 
     * @param array $data
     * @param array $columns
     * 
     * @return array
     */
    protected function formatDataForCsv(PackagedReport $packagedReport, array $data)
    {
        $formattedData = [];
        $columns = $packagedReport->report->columns->toArray();
        $groupBys = $packagedReport->report->groupBys->toArray();
        $usingGrouping = count($groupBys) > 0;
        
        foreach ($data as $rowNum => $row)
        {
            if ($usingGrouping)
            {
                $newGroup = false;
                
                foreach ($groupBys as $groupBy)
                {
                    $dataKey = $groupBy->getAlias();
                    
                    if (null === $previousRow || $newGroup || $row[$dataKey] != $previousRow[$dataKey])
                    {
                        $value = $groupBy->formatData($row[$dataKey]);
                        
                        $formattedData[] = [$groupBy->getTitle().': '.$value];
                        $newGroup = true;
                    }
                }
                
                $previousRow = $row;
            }
            
            $formattedRow = [];
            
            foreach ($columns as $column)
            {
                $formattedRow[] = $column->formatData($row[$column->getAlias()], ReportWriter::EXPORT_CSV);
            }
            
            $formattedData[] = $formattedRow;
        }
                
        return $formattedData;
    }
    
    /**
     * Constructs and returns the base URL for record links.
     * 
     * @param string $module
     * 
     * @return string
     */
    protected function getLinkUrl($module)
    {
        $moduleDefs = $this->registry->getModuleDefs();
        
        $action = $moduleDefs[$module]['GENERIC'] && !$moduleDefs[$module]['MAIN_URL_OVERRIDE'] ? 'record' : $moduleDefs[$module]['ACTION'];
        
        $url = 'app.php?action='.$action.'&module='.$module.'&fromlisting=1' . (($_POST['predefined'] == 1 || $_SESSION['listing']['predefined']) ? '&predefined=1' : '' );
        return \CSRFGuard::addTokenToURL($url, \CSRFGuard::URL_WITH_QUERY);
    }
    
    /**
     * Fetches the total number of main records in a report.
     * 
     * @return int
     */
    public function getMainRecordCount(PackagedReport $packagedReport)
    {
        $writer = $this->queryFactory->getSqlWriter();
        $query  = $packagedReport->getQuery()->createQuery();
        
        $query->select(['COUNT(recordid)']);
        
        $query->replaceAtPrompts($packagedReport->report->module);
        
        list($sql, $parameters) = $writer->writeStatement($query);
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->fetch(\PDO::FETCH_COLUMN);
    }
    
    /**
     * Returns the total number of rows (of raw data) in a report.
     * 
     * @return int
     */
    public function getRowCount(PackagedReport $packagedReport)
    {
        $query  = $this->queryBuilder->build($packagedReport);
        $writer = $this->queryFactory->getSqlWriter();
        
        $query->select(['COUNT(*)'], true)
              ->orderBy([], true);
        
        $query->replaceAtPrompts($packagedReport->report->module);
        
        list($sql, $parameters) = $writer->writeStatement($query);
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->fetch(\PDO::FETCH_COLUMN);
    }
}