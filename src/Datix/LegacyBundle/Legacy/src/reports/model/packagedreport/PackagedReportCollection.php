<?php
namespace src\reports\model\packagedreport;

use src\framework\model\EntityCollection;

class PackagedReportCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\packagedreport\\PackagedReport';
    }
}