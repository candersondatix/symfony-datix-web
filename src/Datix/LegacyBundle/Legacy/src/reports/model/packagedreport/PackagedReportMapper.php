<?php
namespace src\reports\model\packagedreport;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\framework\query\Query;
use src\reports\model\report\Report;
use src\savedqueries\model\SavedQuery;
use src\users\model\User;
use src\reports\model\report\ReportModelFactory;
use src\savedqueries\model\SavedQueryModelFactory;

class PackagedReportMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\packagedreport\\PackagedReport';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return 'web_packaged_reports';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->select([$this->getTable().'.web_report_id']);
        return parent::select($query);
    }
    
    /**
     * Fetches the Report object attached to a PackagedReport.
     * 
     * @param PackagedReport     $packagedReport
     * @param ReportModelFactory $factory
     * 
     * @return Report
     */
    public function getPackagedReportReport(PackagedReport $packagedReport, ReportModelFactory $factory = null)
    {
        $factory = $factory ?: new ReportModelFactory();
        list($query, $where, $fields) = $factory->getQueryFactory()->getQueryObjects();
        
        $fields->field('web_packaged_reports.web_report_id')->eq('web_reports.recordid', true);
        
        $where->add($fields);
        
        $query->join('web_packaged_reports', $where)
              ->where(['web_packaged_reports.recordid' => $packagedReport->recordid]);
        
        $data = $factory->getMapper()->select($query);
        
        if (!empty($data))
        {
            $report = $factory->getEntityFactory()->createObject($data[0]);
        }
        
        return $report;
    }
    
    /**
     * Fetches the SavedQuery object attached to a PackagedReport.
     * 
     * @param PackagedReport         $packagedReport
     * @param SavedQueryModelFactory $factory
     * 
     * @return SavedQuery
     */
    public function getPackagedReportQuery(PackagedReport $packagedReport, SavedQueryModelFactory $factory = null)
    {
        $factory = $factory ?: new SavedQueryModelFactory();
        list($query, $where, $fields) = $factory->getQueryFactory()->getQueryObjects();
        
        $fields->field('web_packaged_reports.query_id')->eq('web_queries.recordid', true);
        
        $where->add($fields);
        
        $query->join('web_packaged_reports', $where)
              ->where(['web_packaged_reports.recordid' => $packagedReport->recordid]);
        
        $data = $factory->getMapper()->select($query);
        
        if (!empty($data))
        {
            $savedQuery = $factory->getEntityFactory()->createObject($data[0]);
        }
        
        return $savedQuery;
    }
    
    /**
     * Saves the IDs of the users who can access this packaged report.
     * 
     * @param int   $packagedReportID
     * @param array $userIDs
     */
    public function saveUsers($packagedReportID, array $userIDs)
    {
        $this->db->setSQL('DELETE FROM web_packaged_report_users WHERE web_packaged_report_id = ?');
        $this->db->prepareAndExecute([$packagedReportID]);
        
        $this->db->setSQL('INSERT INTO web_packaged_report_users VALUES (?, ?)');
        foreach ($userIDs as $id)
        {
            $this->db->prepareAndExecute([$packagedReportID, $id]);
        }
    }
    
    /**
     * Saves the IDs of the groups who can access this packaged report.
     * 
     * @param int   $packagedReportID
     * @param array $groupIDs
     */
    public function saveGroups($packagedReportID, array $groupIDs)
    {
        $this->db->setSQL('DELETE FROM web_packaged_report_groups WHERE web_packaged_report_id = ?');
        $this->db->prepareAndExecute([$packagedReportID]);
        
        $this->db->setSQL('INSERT INTO web_packaged_report_groups VALUES (?, ?)');
        foreach ($groupIDs as $id)
        {
            $this->db->prepareAndExecute([$packagedReportID, $id]);
        }
    }
    
    /**
     * Saves the IDs of the profiles who can access this packaged report.
     * 
     * @param int   $packagedReportID
     * @param array $profileIDs
     */
    public function saveProfiles($packagedReportID, array $profileIDs)
    {
        $this->db->setSQL('DELETE FROM web_packaged_report_profiles WHERE web_packaged_report_id = ?');
        $this->db->prepareAndExecute([$packagedReportID]);
        
        $this->db->setSQL('INSERT INTO web_packaged_report_profiles VALUES (?, ?)');
        foreach ($profileIDs as $id)
        {
            $this->db->prepareAndExecute([$packagedReportID, $id]);
        }
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doInsert(Entity $object)
    {
        if ($object->report === null)
        {
            throw new MapperException('Unable to insert PackagedReport - report property is not set');
        }
        
        if ($object->query === null)
        {
            throw new MapperException('Unable to insert PackagedReport - query property is not set');
        }
        
        $this->db->beginTransaction();
        
        try
        {
            if ($object->report->recordid == null && $object->report->getReportType() == Report::LISTING)
            {
                /*
                The web_reports record does not exist yet
                For statistical reports, we can just create a new one.
                For listing reports we need to find the web_report record with the correct base_listing_id
                */

                $existingReport = $this->factory->getReportModelFactory()->getMapper()->findByBaseListingReportID($object->report->base_listing_report);

                if ($existingReport)
                {
                    $this->setReport($existingReport);
                }
            }

            $this->factory->getReportModelFactory()->getMapper()->save($object->report);

            // ensure new packaged report has its own query instance, so it can be updated independently
            $object->query = clone $object->query;
            $this->factory->getSavedQueryModelFactory()->getMapper()->save($object->query);
            
            $id = parent::doInsert($object);
            
            $this->db->commit();
            
            return $id;
        }
        catch (\Exception $e)
        {
            $this->db->rollBack();
            throw $e;
        }
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate(Entity $object)
    {
        //persist report
        if ($object->report instanceof Report && $object->report->getReportType() != Report::LISTING)
        {
            // currently, it's not possible to update a base listing report when saving a packaged report
            (new ReportModelFactory)->getMapper()->save($object->report);
        }

        //persist saved query
        if ($object->query instanceof SavedQuery)
        {
            (new SavedQueryModelFactory)->getMapper()->save($object->query);
        }

        return parent::doUpdate($object);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function updateEntity(Entity $object)
    {
        parent::updateEntity($object);
        
        if ( !((int) $object->recordid > 0) )
        {
            // TODO this should be factored out for other entities that use these properties, maybe into a Trait?
            if ($_SESSION['CurrentUser'] instanceof User)
            {
                $createdby = $_SESSION['CurrentUser']->initials;
            }
            else
            {
                $createdby = '';
                $this->registry->getLogger()->logEmergency('Unable to determine logged-in user.');
            }
            
            $object->createdby   = $createdby;
            $object->createddate = date('d-M-Y H:i:s');
        }
    }
    
    /**
     * {@inheritdoc}
     * 
     * Additionally sets the web_report_id and query_id values.
     */
    protected function getEntityProperties(Entity $object)
    {
        $fields = parent::getEntityProperties($object);
        $fields['web_report_id'] = $object->report->recordid;
        $fields['query_id'] = $object->query->recordid;
        
        return $fields;
    }
    
    /**
     * If the package report is already on the dashboard?
     * 
     * @author gszucs
     * @param PackagedReport $packagedReport
     * @return boolean
     */
    public function isOnDashboard(PackagedReport $packagedReport)
    {
    	$this->db->setSQL('SELECT COUNT(*) as num
  						   FROM reports_dashboards as rd left join reports_dash_links as rdl on rd.recordid = rdl.dlink_dash_id
  						   WHERE rd.dash_owner_id = ? and rdl.dlink_package_id = ?');
    	$this->db->prepareAndExecute([$_SESSION["contact_login_id"], $packagedReport->recordid]);
    	return (bool) $this->db->PDOStatement->fetchColumn();
    }
    
    /**
     * Gets an array of the IDs of dashboards that a given report can be placed on. 
     * The dashboards have to be owned by the logged in user and must not have the report already on them.
     * 
     * @param int $packagedReportID
     * 
     * @return array
     */
    public function getTargetDashboards($packagedReportID)
    {
        // TODO: this would probably sit better as part of the dashboard model than here
        
        $this->db->setSQL('
            SELECT
                reports_dashboards.recordid
            FROM
                reports_dashboards
            WHERE
                reports_dashboards.recordid NOT IN (
                    SELECT
                        reports_dashboards.recordid
                    FROM
                        reports_dashboards,
                        reports_dash_links
                    WHERE
                        reports_dashboards.recordid = reports_dash_links.dlink_dash_id
                        AND
                        dlink_package_id = ?
                )
            AND
                dash_owner_id = ?');
        
        $this->db->prepareAndExecute([ $packagedReportID, $_SESSION["contact_login_id"] ]);
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_COLUMN);
    }
}