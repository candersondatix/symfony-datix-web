<?php

namespace src\reports\model\packagedreport;

use src\framework\controller\Request;
use src\framework\model\Entity;
use src\framework\model\EntityFactory;
use src\framework\query\FieldCollection;
use src\framework\query\Where;
use src\framework\query\SqlWriter;
use src\framework\query\Query;
use src\framework\registry\Registry;
use src\reports\exceptions\ReportException;
use src\reports\model\report\Report;
use src\reports\model\report\ReportFactory;
use src\reports\model\report\ReportModelFactory;
use src\savedqueries\model\SavedQuery;
use src\savedqueries\model\SavedQueryModelFactory;
use src\system\database\FieldInterface;
use src\system\database\table\LinkModelFactory;
use src\system\database\table\LinkTableModelFactory;

class PackagedReportFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\packagedreport\\PackagedReport';
    }

    /**
     * The first thing we need to do in may places in the system is to build a report object that will be used throughout
     * a request.
     *
     * The information to build the report might come from one of these places, in the order they are checked for:
     * 1) A packaged report object passed in the request
     * 2) A set of values stored in the database, identified by a "recordid" parameter
     * 3) A set of values passed from a design form on the previous page
     * 4) A report object already present in the session (that has been previously run by this user)
     *
     * If none of these are present, a new report object should be built, containing default settings (mostly empty).
     *
     * @return PackagedReport
     */
    public function createCurrentPackagedReportFromRequest(Request $request)
    {
        if ($request->getParameter('blank'))
        {
            //override parameter passed: ignore any other instructions and generate a blank report
            if ($request->getParameter('module'))
            {
                $packagedReport = (new PackagedReportModelFactory)->getEntityFactory()->createObject(array('type' => Report::BAR, 'module' => $request->getParameter('module'), 'query' => $request->getParameter('query')));
            }
            else
            {
                throw new ReportException('Not enough information to build the Packaged Report object.');
            }
        }
        else if ($request->getParameter('packagedReport') instanceof PackagedReport)
        {
            if($request->getParameter('isCrosstab'))
            {
                $packagedReport = $this->buildCrosstabPackagedReport($request);
            }
            else
            {
                $packagedReport = $request->getParameter('packagedReport');
            }
        }
        else if ($request->getParameter('recordid') != '')
        {
            $factory = new PackagedReportModelFactory();
            $packagedReport  = $factory->getMapper()->find($request->getParameter('recordid'));

            if ($packagedReport === null)
            {
                throw new \URLNotFoundException();
            }
        }
        else if (is_numeric($request->getParameter('type')))
        {
            if($request->getParameter('type') == Report::LISTING)
            {
                if (is_numeric($request->getParameter('base_listing_report')))
                {
                    $request->setParameter('report', (new ReportModelFactory)->getMapper()->findByBaseListingReportID($request->getParameter('base_listing_report')));
                }
            }
            else
            {
                $request->setParameter('report', (new ReportFactory)->createFromFormRequest($request));
            }

            $request->setParameter('name', $request->getParameter('title') ?: '');

            $packagedReport = (new PackagedReportModelFactory)->getEntityFactory()->createFromRequest($request);
        }
        else if ($_SESSION['CurrentReport'] instanceof PackagedReport)
        {
            $packagedReport = $_SESSION['CurrentReport'];
        }
        else if ($request->getParameter('module') != '')
        {
            $packagedReport = (new PackagedReportModelFactory)->getEntityFactory()->createObject(array('type' => Report::BAR, 'module' => $request->getParameter('module')));
        }
        else
        {
            throw new ReportException('Not enough information to build the Packaged Report object.');
        }

        return $packagedReport;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array(), Entity $baseEntity = null)
    {
        if ($data['query'] != '')
        {
            if ($data['query'] == '0')
            {
                // use the current search criteria saved in the session
                $props = ['module' => $data['module']];
                if ($_SESSION[$data['module']]['NEW_WHERE'] instanceof Where)
                {
                    $props['where'] = clone $_SESSION[$data['module']]['NEW_WHERE'];
                }
                
                $data['query'] = (new SavedQueryModelFactory)->getEntityFactory()->createObject($props);
            }
            else if ($data['query'] == '-1' && $data['base_report'])
            {
                $data['query'] = (new PackagedReportModelFactory)->getMapper()->find($data['base_report'])->query;
            }
            else
            {
               $data['query'] = (new SavedQueryModelFactory)->getMapper()->findByOldQueryID($data['query']);
            }
        }
        else if ($data['new_query_id'] != '')
        {
            $data['query'] = (new SavedQueryModelFactory)->getMapper()->find($data['new_query_id']);
        }
        else
        {
            unset($data['query']);
        }
        
        if (isset($data['web_report_id']))
        {
            // need to check report exists before assigning it, since otherwise it will be set to null, which
            // then causes an exception when the null value is passed to packagedReport->setReport()
            $report = (new ReportModelFactory)->getMapper()->find($data['web_report_id']);

            if ($report)
            {
                $data['report'] = $report;
            }
        }
        else
        {
            // we don't want to overwrite the base entity report if one has been set, or an existing object if one has been passed
            if (!($baseEntity->report instanceof Report) && !($data['report'] instanceof Report))
            {
                $report = (new ReportModelFactory)->getEntityFactory()->createObject($data);

                if ($report)
                {
                    $data['report'] = $report;
                }
            }
        }

        if(isset($data['report']->rows_date_option) && $data['report']->rows_date_option == 0)
        {
            $data['report']->rows_date_option = null;
        }

        if(isset($data['report']->columns_date_option) && $data['report']->columns_date_option == 0)
        {
            $data['report']->columns_date_option = null;
        }

        if(isset($data['report']->limit_to_top) && $data['report']->limit_to_top == 0)
        {
            $data['report']->limit_to_top = null;
        }

        $packagedReport = parent::doCreateObject($data, $baseEntity);

        return $packagedReport;
    }

    /**
     * Used in the situation where we've got a report in the session that we want to save over an existing report, identified by id.
     *
     * If anyone wants to suggest a different name for this function, be my guest.  Be my guest.  Put my service to the test.
     */
    public function createPackagedReportFromSessionBasedOnReportId($recordid)
    {
        $packagedReport = $_SESSION['CurrentReport'];

        $packagedReport->recordid = $recordid;

        //bit of a hack, but these fields are blank otherwise, and not populated during "update"
        $baseReport = (new PackagedReportModelFactory)->getMapper()->find($packagedReport->recordid);
        $packagedReport->createdby = $baseReport->createdby;
        $packagedReport->createddate = $baseReport->createddate;

        $packagedReport->report->recordid = $baseReport->report->recordid;
        $packagedReport->report->createdby = $baseReport->report->createdby;
        $packagedReport->report->createddate = $baseReport->report->createddate;
        $packagedReport->report->title = $baseReport->report->title;

        return $packagedReport;
    }

    /**
     * Builds a crosstab report.
     *
     * @param PackagedReportModelFactory $factory
     *
     * @return \src\framework\model\Entity
     */
    protected function buildCrosstabPackagedReport(Request $request)
    {
        $factory    = new PackagedReportModelFactory();
        $registry = Registry::getInstance();

        $drillDown = $request->getParameter('drilldown');
        $drillLevel = $request->getParameter('drilllevel');
        $FieldDefs = $registry->getFieldDefs();

        if (($drillDown === null || $drillDown === '') && ($drillLevel === null || $drillLevel === ''))
        {
            // Clear the session if it's the first time we are designing a crosstab
            unset($_SESSION['crosstab_drill_levels']);
            unset($_SESSION['crosstab_current_drill_level']);
            unset($_SESSION['crosstab_rows_field']);
            unset($_SESSION['crosstab_columns_field']);
            unset($_SESSION['crosstab_title']);

            $packagedReport = $request->getParameter('packagedReport');

            // Set the name (title) in the packaged report
            if ($request->getParameter('title') != '')
            {
                $packagedReport->name = $request->getParameter('title');
            }

            $_SESSION['crosstab_title'] = $packagedReport->name;
        }
        elseif (isset($drillLevel) && $_SESSION['crosstab_drill_levels'][$drillLevel])
        {
            // Return the report from the session if we are drilling back
            $packagedReport = $_SESSION['crosstab_drill_levels'][intval($drillLevel)];

            $_SESSION['crosstab_title'] = $packagedReport->name;
        }
        else
        {
            $recordid = $request->getParameter('packagedReport')->recordid ?: $request->getParameter('base_report') ?: $_SESSION['CurrentReport']->recordid;

            // We need to clone the packaged report object otherwise we will be messing with the previous created one
            $packagedReport = clone $_SESSION['crosstab_drill_levels'][$_SESSION['crosstab_current_drill_level']-1];

            $packagedReport->recordid = $recordid;

            // Extract the report and where objects from the cloned object and amend them
            $report = $packagedReport->report;
            $where  = $packagedReport->query->where ?: new Where();

            $moduleDefs = $registry->getModuleDefs();
            
            /*
             HACK
             
             If the report axis we're replacing via the action of drilling down is from a fieldset describing a contact type (i.e. Other Contacts)
             then we have a problem.  The problem is that the filter that restricts the query to contacts of that type is defined as part of the join condition.
             Join conditions are rebuilt at runtime on the basis of the report options/query conditions each time a report is run, and are therefore not retained 
             between requests.  Which means that we lose the condition which filters the data on the basis of contact type.  So, we need a way of retaining these query 
             conditions before swapping out the original axis value with the drill-down field.
             
             For now (read: mere days before the release of v14.0) we'll have to deal with this in a non-ideal way (see below) but beyond that we should revisit 
             this issue and try to engineer a solution which doesn't involve littering our code with horrible, hard-coded exceptions.  This may involve a rethink of our 
             table relationship metadata so that we're only using it to describe links between tables, and not filters for subsets of data based on the different entity
             types persisted in those tables.
             */
            $fieldset = ('row' == $request->getParameter('rowcol')) ? $report->rows_fieldset : $report->columns_fieldset;
            
            if ($fieldset > 0)
            {
                $contactsMainFieldsets = (new LinkTableModelFactory)->getMapper()->getContactsTypeFieldsets();
                
                if (in_array($fieldset, $contactsMainFieldsets))
                {
                    /* this fieldset describes a link contact type, so we need to extract the join conditions
                       for that contact type and ensure they're retained in the report query object */
                    $links = (new LinkModelFactory)->getMapper()->findLinksByFieldset($fieldset);
                    
                    /* SavedQuery objects cannot be persisted with both string- and object-based where clauses,
                       so we convert the existing Where object to a string before appending the addtional condition */
                    if (null !== $packagedReport->query->where)
                    {
                        $packagedReport->query->where_string = (new SqlWriter)->writeWhereClause((new Query)->where($packagedReport->query->where));
                    }
                    
                    foreach ($links as $link)
                    {
                        if ('' != $link->tlk_where)
                        {
                            $prefix = ('' == $packagedReport->query->where_string) ? '' : ' AND ';
                            $packagedReport->query->where_string .= $prefix.$link->tlk_where;
                        }
                    }
                }
            }

            switch ($request->getParameter('rowcol'))
            {
                case 'row':
                    list($table, $field) = explode('.', $report->rows);
                    $report->rows = $request->getParameter('dropdownval');
                    $report->rows_fieldset = 0;
                    $dateOption = $report->rows_date_option;
                    break;
                case 'col':
                    list($table, $field) = explode('.', $report->columns);
                    $report->columns = $request->getParameter('dropdownval');
                    $report->columns_fieldset = 0;
                    $dateOption = $report->columns_date_option;
                    break;
            }

            // If field is of type date the where needs a different approach
            if ($FieldDefs[$table.'.'.$field]->getType() == FieldInterface::DATE)
            {
                (new PackagedReportModelFactory)->getDatesEngine()->setDateOptionWhere($table.'.'.$field, $dateOption, $request->getParameter('fieldval'), $where);
            }
            else
            {
                $fc = new FieldCollection();
                $fc2 = new FieldCollection();
                $fieldVal = $request->getParameter('fieldval');
                //if we are drilling down into 'No Value' we need a special case
                if($fieldVal === '')
                {
                    $fc->field($table.'.'.$field)->eq($fieldVal);
                    $fc2->field($table.'.'.$field)->isNull();
                    $where->add('OR', $fc, $fc2);
                }
                else
                {
                    $fc->field($table.'.'.$field)->eq($fieldVal);
                    $where->add($fc);
                }
            }

            $packagedReport->query->where = $where;

            $_SESSION['crosstab_title'] = $packagedReport->name;
        }

        return $packagedReport;
    }
}