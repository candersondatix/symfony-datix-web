<?php

namespace src\reports\model\listingreport\column;

use src\framework\model\KeyedEntityCollection;
use src\system\database\FieldInterface;

class ColumnCollection extends KeyedEntityCollection
{
    /**
     * @var boolean
     */
    protected $moneyFields;
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\column\\Column';
    }
    
    /**
     * Get the combined width of all the columns in the collection.
     * 
     * @return float
     */
    public function getTotalWidth()
    {
        $this->fetchData();
        
        $width = 0;
        
        foreach ($this->data as $column)
        {
            $width += $column['width'];
        }
        
        return $width;
    }
    
    /**
     * Whether or not there are any money fields in this collection.
     * 
     * @return boolean
     */
    public function hasMoneyFields()
    {
        if (null === $this->moneyFields)
        {
            $this->initCollection();
            
            $this->moneyFields = false;
            
            foreach ($this->data as $column)
            {
                if ('M' == $column['type'])
                {
                    $this->moneyFields = true;
                    break;
                }
            }
        }
        return $this->moneyFields;
    }
    
    /**
     * Set the current running totals for the relevant columns in this collection.
     * 
     * @param array $totals
     */
    public function setTotals(array $totals)
    {
        if (!$this->hasMoneyFields() || empty($totals))
        {
            return;
        }
        
        foreach ($this as $column)
        {
            if (FieldInterface::MONEY == $column->getType() && isset($totals[$column->getAlias()]))
            {
                $column->totals = $totals[$column->getAlias()]['totals'];
                $column->groupTotals = $totals[$column->getAlias()]['groupTotals'];
            }
        }
    }
    
    /**
     * Set the current running totals for the relevant columns in this collection.
     * 
     * @return array $totals
     */
    public function getTotals()
    {
        $totals = [];
        
        if (!$this->hasMoneyFields())
        {
            return $totals;
        }
        
        foreach ($this as $column)
        {
            if (FieldInterface::MONEY == $column->getType())
            {
                $totals[$column->getAlias()] = [
                    'totals' => $column->totals,
                    'groupTotals' => $column->groupTotals
                ];
            }
        }
        
        return $totals;
    }
    
    /**
     * Resets the totals value for each object in the collection.
     */
    public function resetTotals()
    {
        foreach ($this->objects as $column)
        {
            $column->totals = [];
            $column->groupTotals = [];
        }
    }
    
    /**
     * {@inheritdoc}
     */
    protected function keyProperty()
    {
        return ['form','field'];
    }
}