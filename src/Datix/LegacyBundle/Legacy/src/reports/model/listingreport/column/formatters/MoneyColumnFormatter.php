<?php

namespace src\reports\model\listingreport\column\formatters;

require_once 'Source/libs/Subs.php';

class MoneyColumnFormatter extends CachedColumnFormatter
{
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        return FormatMoneyVal($data, false);
    }
}