<?php

namespace src\reports\model\listingreport\orderby;

use src\reports\model\listingreport\OptionMapper;
use src\framework\query\Query;
use src\reports\model\listingreport\Listing;

class OrderByMapper extends OptionMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\orderby\\OrderBy';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        // ignore fields that we're grouping by, since they already appear in the ORDER BY clause
        list($subQuery, $where, $fc) = $this->factory->getQueryFactory()->getQueryObjects();
        
        $subQuery->select([['rpf_form + rpf_field']])
                 ->from($this->getTable())
                 ->where(clone $query->getWhere())
                 ->where(['rpf_section' => 'GROUP']);
        
        $where->add(
            $fc->field('rpf_section')->eq('SORT')
               ->field(['rpf_form + rpf_field'])->notIn($subQuery)
        );
        
        $query->where($where);
              
        return parent::select($query);
    }
    
    /**
     * Retrieves the OrderBys for a given listing report design.
     * 
     * @param Listing $report
     */
    public function getOrderBysForListingReport(Listing $report)
    {
        $query = $this->factory->getQueryFactory()->getQuery();
        $query->where(['rpf_rep_id' => $report->base_listing_report])
              ->orderBy(['rpf_order']);
        
        $OrderBys = $this->factory->getCollection();
        $OrderBys->setQuery($query);
        
        return $OrderBys;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getFieldMappings()
    {
        return [
            'direction' => 'rpf_format'
        ];
    }
}