<?php

namespace src\reports\model\listingreport\column;

use src\reports\model\listingreport\Option;
use src\framework\registry\Registry;
use src\system\database\FieldInterface;
use src\reports\model\listingreport\column\formatters\IColourFormatter;

class Column extends Option
{
    /**
     * @var float
     */
    protected $width;
    
    /**
     * Custom format for numbers/dates.
     * 
     * @var mixed
     */
    protected $format;
    
    /**
     * Option to display codes or description for coded field columns.
     * 
     * @var boolean
     */
    protected $showDescription;
    
    /**
     * The element key used in the data to uniquely identify a value set for this column.
     * 
     * @var string
     */
    protected $idDataKey;
    
    /**
     * The total value for this column.
     * 
     * Used to display totals for money fields.
     * 
     * @var array
     */
    protected $totals;
    
    /**
     * The total value for this column in this group.
     * 
     * Used to display totals for money fields.
     * 
     * @var array
     */
    protected $groupTotals;
    
    public function __construct()
    {
        $this->totals = [];
        $this->groupTotals = [];
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        if (null === $this->alias)
        {
            $this->alias = 'column'.$this->order;
        }
        return $this->alias;
    }
    
    /**
     * Getter for format.
     * 
     * @return mixed
     */
    public function getFormat()
    {
        if ('' != $this->format)
        {
            return $this->format;
        }
        
        if ($this->fieldDef instanceof \src\system\database\extrafield\ExtraField)
        {
            return $this->fieldDef->getFormat();
        }
        
        return;
    }
    
    /**
     * Getter for width.
     * 
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }
    
    /**
     * Returns the column width as a percentage of a given total line width.
     * 
     * @param float $lineWidth
     * 
     * @return float
     */
    public function getWidthAsPercentage($lineWidth)
    {
        return round($this->width / $lineWidth * 100);
    }
    
    /**
     * Returns the column title with a module-specific suffix, if appropriate.
     * 
     * @param string   $module
     * @param Registry $registry
     * 
     * @return string
     */
    public function getTitleWithSuffix($module, Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();
        $fieldDefsExtra = $registry->getFieldDefsExtra();
        
        if ('' != $fieldDefsExtra[$module][$this->field]['SubmoduleSuffix'])
        {
             return $this->title.' '.$fieldDefsExtra[$module][$this->field]['SubmoduleSuffix'];
        }
        
        return $this->title;
    }
    
    /**
     * Getter for id data key.
     */
    public function getIdDataKey()
    {
        if (null === $this->idDataKey)
        {
            $this->idDataKey = $this->form.'_id';
        }
        return $this->idDataKey;
    }
    
    /**
     * Getter for the total value.
     * 
     * @return float
     */
    public function getTotal()
    {
        if (FieldInterface::MONEY == $this->getType())
        {
            return array_sum($this->totals);
        }
        return '';
    }
    
    /**
     * Reset the totals to an empty array.
     */
    public function resetTotals()
    {
        $this->totals = [];
    }
    
    /**
     * Getter for group total value.
     * 
     * @return float
     */
    public function getGroupTotal()
    {
        if (FieldInterface::MONEY == $this->getType())
        {
            return array_sum($this->groupTotals);
        }
        return '';
    }
    
    /**
     * Reset the group totals to an empty array.
     */
    public function resetGroupTotals()
    {
        $this->groupTotals = [];
    }
    
    /**
     * Increments group/overall totals by adding the value to the totals arrays.
     * 
     * @param mixed $id    The identifier for this value, so we don't add repeated values.
     * @param float $value The value we're incrementing by.
     */
    public function incrementTotals($id, $value)
    {
        $this->totals[$id] = $value;
        $this->groupTotals[$id] = $value;
    }
    
    /**
     * Returns the colour associated with a particular cell value, if applicable.
     * 
     * @param mixed $rawData The raw (i.e. unformatted) cell value.
     * 
     * @return string|null
     */
    public function getColour($rawData)
    {
        if (null === $this->formatter)
        {
            $this->formatter = $this->formatterFactory->getFormatter($this);
        }
        
        if (!($this->formatter instanceof IColourFormatter))
        {
            return;
        }
        
        return $this->formatter->getColour($rawData);
    }
}