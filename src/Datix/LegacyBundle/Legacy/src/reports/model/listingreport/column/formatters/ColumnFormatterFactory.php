<?php

namespace src\reports\model\listingreport\column\formatters;

use src\reports\model\listingreport\Option;
use src\reports\model\listingreport\column\Column;
use src\system\database\FieldInterface;
use src\system\database\extrafield\ExtraField;
use src\framework\registry\Registry;
use src\reports\controllers\ReportWriter;

class ColumnFormatterFactory
{
    public function getFormatter(Option $option)
    {
        if ('' != $option->getDateOption())
        {
            return new DateOptionFormatter($option, Registry::getInstance());
        }
        
        $optionIsColumn           = $option instanceof Column;
        $optionIsColumnWithFormat = $optionIsColumn && '' != $option->getFormat();
        $optionIsNumber           = FieldInterface::NUMBER == $option->getType();
        $optionIsExtraField       = null !== ($fieldDef = $option->getFieldDef()) && $fieldDef instanceof ExtraField;
        
        $useGuptaformatting = $optionIsColumnWithFormat && $optionIsNumber && $optionIsExtraField;
        
        if ($useGuptaformatting)
        {
            return new GuptaColumnFormatter($option);
        }
        
        if ('inc_time' == $option->getField())
        {
            return new TimeColumnFormatter();
        }
        
        $showDescription = $optionIsColumn ? $option->showDescription : 'Y';
        
        switch ($option->getType())
        {
            case FieldInterface::DATE:
                return new DateColumnFormatter();

            case FieldInterface::CODE:
                return new CodeColumnFormatter($option->getFieldDef(), $showDescription);
                
            case FieldInterface::MULTICODE:
                return new MultiCodeColumnFormatter(new CodeColumnFormatter($option->getFieldDef(), $showDescription));
                
            case FieldInterface::MONEY:
                return new MoneyColumnFormatter();
                
            case FieldInterface::COMPUTEDDATE:
                return new ComputedDateColumnFormatter();
                
            case FieldInterface::YESNO:
                return new YesNoColumnFormatter();

            case FieldInterface::STRING:
            case FieldInterface::TEXT:
                return new TextColumnFormatter();

            default:
                return new DummyColumnFormatter();
        }
    }
}