<?php

namespace src\reports\model\listingreport\column\formatters;

class MultiCodeColumnFormatter extends ColumnFormatter
{
    /**
     * @var CodeColumnFormatter
     */
    protected $formatter;
    
    public function __construct(CodeColumnFormatter $formatter)
    {
        $this->formatter = $formatter;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function escapeOutput($data, $context)
    {
        return $data;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        $data   = explode(' ', $data);
        $output = [];
        
        foreach ($data as $value)
        {
            $output[] = $this->formatter->format($value, $context);
        }
        
        return implode(', ', $output);
    }
}