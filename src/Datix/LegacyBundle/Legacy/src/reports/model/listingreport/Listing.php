<?php
namespace src\reports\model\listingreport;

use src\reports\model\report\Report;
use src\reports\model\listingreport\column\ColumnModelFactory;
use src\reports\model\listingreport\column\ColumnCollection;
use src\reports\model\listingreport\groupby\GroupByModelFactory;
use src\reports\model\listingreport\groupby\GroupByCollection;
use src\reports\model\listingreport\orderby\OrderByModelFactory;
use src\reports\model\listingreport\orderby\OrderByCollection;
use src\framework\registry\Registry;

class Listing extends Report
{
    /**
     * The id of the base listing report design to use.
     *
     * @var string
     */
    protected $base_listing_report;

    /**
     * Boolean values used for custom listing report #5.
     *
     * @var string
     */
    protected $include_elements;
    protected $include_compliance;
    protected $include_evidence;
    protected $include_documents;
    
    /**
     * @var ColumnCollection
     */
    protected $columns;
    
    /**
     * @var GroupByCollection
     */
    protected $groupBys;
    
    /**
     * @var OrderByCollection
     */
    protected $orderBys;
    
    /**
     * @var int
     */
    protected $merge_repeated_values;
    
    /**
     * Flag to disable paging and return all records.
     * 
     * @var boolean
     */
    protected $returnAllRecords;
    
    /**
     * The starting (zero-based) record number for the current report page.
     * 
     * Used instead of a simple page number since each page can display
     * a different number of records when merging repeated values.
     * 
     * @var int
     */
    protected $startPosition;
    
    /**
     * Whether or not we've reached the end of the report.
     * 
     * @var boolean
     */
    protected $endOfReport;
    
    /**
     * The final row of data from the previous page.
     * 
     * Used to facilitate grouping results by value when paging.
     * 
     * @var array|null
     */
    protected $previousRow;
    
    public function __construct()
    {
        $this->reset();
        $this->returnAllRecords = false;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('listing_report');
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::LISTING;
    }
    
    /**
     * Getter for columns.
     * 
     * @param ColumnModelFactory $factory
     * 
     * @return ColumnCollection
     */
    public function getColumns(ColumnModelFactory $factory = null)
    {
        if (null === $this->columns)
        {
            $factory = $factory ?: new ColumnModelFactory();
            $this->columns = $factory->getMapper()->getColumnsForListingReport($this);
        }
        return $this->columns;
    }
    
    /**
     * Setter for columns.
     * 
     * @param ColumnCollection $columns
     */
    public function setColumns(ColumnCollection $columns)
    {
        $this->columns = $columns;
    }
    
    /**
     * Getter for groupBys.
     * 
     * @param GroupByModelFactory $factory
     * 
     * @return GroupByCollection
     */
    public function getGroupBys(GroupByModelFactory $factory = null)
    {
        if (null === $this->groupBys)
        {
            $factory = $factory ?: new GroupByModelFactory();
            $this->groupBys = $factory->getMapper()->getGroupBysForListingReport($this);
        }
        return $this->groupBys;
    }
    
    /**
     * Setter for groupBys.
     * 
     * @param GroupByCollection $groupBys
     */
    public function setGroupBys(GroupByCollection $groupBys)
    {
        $this->groupBys = $groupBys;
    }
    
    /**
     * Getter for orderBys.
     * 
     * @param OrderByModelFactory $factory
     * 
     * @return OrderByCollection
     */
    public function getOrderBys(OrderByModelFactory $factory = null)
    {
        if (null === $this->orderBys)
        {
            $factory = $factory ?: new OrderByModelFactory();
            $this->orderBys = $factory->getMapper()->getOrderBysForListingReport($this);
        }
        return $this->orderBys;
    }
    
    /**
     * Setter for orderBys.
     * 
     * @param OrderByCollection $orderBys
     */
    public function setOrderBys(OrderByCollection $orderBys)
    {
        $this->orderBys = $orderBys;
    }
    
    /**
     * Resets the page-tracking properties.
     */
    public function reset()
    {
        $this->startPosition = 0;
        $this->endOfReport = false;
        $this->previousRow = null;
        
        if (null !== $this->columns)
        {
            $this->columns->resetTotals();
        }
    }
    
    /**
     * Increments the start position by the number of records on the last page.
     * 
     * Also sets the endOfReport property.
     * 
     * @param int $recordCount
     */
    public function incrementStartPosition($recordCount)
    {
        $this->startPosition += (int) $recordCount;
        $this->endOfReport = $recordCount < $this->getPageLimit();
    }

    /**
     * Setter for start position
     *
     * @param int $startPosition
     */
    public function setStartPosition($startPosition)
    {
        $this->startPosition = (int) $startPosition;
    }
    
    /**
     * Define whether or not all records should be returned.
     * 
     * @param boolean $bool
     */
    public function setReturnAllRecords($bool)
    {
        $this->returnAllRecords = (bool) $bool;
        $this->endOfReport = (bool) $bool;
    }
    
    /**
     * Setter for end of report.
     *
     * @param boolean $bool
     */
    public function setEndOfReport($bool)
    {
        $this->endOfReport = (bool) $bool;
    }
    
    /**
     * Setter for previousRow.
     * 
     * @param array $previousRow
     */
    public function setPreviousRow(array $previousRow)
    {
        $this->previousRow = $previousRow;
    }
    
    /**
     * Getter for previousRow.
     * 
     * @return array|null
     */
    public function getPreviousRow()
    {
        if (is_array($this->previousRow))
        {
            // replace nulls with empty strings, to avoid problem with the values being interpreted as
            // the literal string "null" when returned via ajax to load the next page of results
            foreach ($this->previousRow as $key => $value)
            {
                if (null === $value)
                {
                    $this->previousRow[$key] = '';
                }
            }
        }
        return $this->previousRow;
    }
    
    /**
     * @return boolean
     */
    public function isHardCoded()
    {
        return in_array($this->base_listing_report, ['report1', 'report2', 'report3', 'report4', 'report5', 'report6', 'report7', 'report8']);
    }
    
    /**
     * Provides the global setting for the row limit on a single page.
     * 
     * @param Registry $registry
     * 
     * @return int
     */
    public function getPageLimit(Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();
        return $registry->getParm('LISTING_REPORT_DISPLAY_NUM', 100);
    }

    public function ParseHTMLTableForExport($html, $output_format, $pdf)
    {
        libxml_use_internal_errors(true); // required to prevent IIS throwing a wobbly on some installations when loading invalid HTML markup

        $html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>'.$html.'</html>';

        $doc = new \DOMDocument('1.0', 'utf-8');
        @$doc->loadHTML($html);
        $telements = $doc->getElementsByTagName('table');

        // remove attributes from tables
        foreach ($telements as $telement)
        {
            $telement->removeAttribute('class');
            $telement->removeAttribute('border');
            $telement->removeAttribute('width');
            $telement->removeAttribute('bgcolor');
            $telement->setAttribute('border', '1');
    
            if ($output_format == 'pdf')
            {
                $telement->removeAttribute('width');
                $telement->setAttribute('width', $pdf->width);
            }
        }

        // deal with cell widths
        $thelements = $doc->getElementsByTagName('th');
        $lineWidth = 0;

        foreach ($thelements as $thelement)
        {
            if ($thelement->getAttribute('excelwidth') != '')
            {
                $lineWidth += $thelement->getAttribute('excelwidth');
            }
        }

        foreach ($thelements as $thelement)
        {
            if ($_REQUEST['report'] == 'report6' || $_REQUEST['report'] == 'report5')
            {
                $thelement->setAttribute('bgcolor', '#d2e4fc');
            }
            else
            {
                $thelement->setAttribute('bgcolor', '#162D54');

                if ($output_format == 'pdf')
                {
                    $thelement->setAttribute('color', '#FFFFFF');
                }
                else
                {
                    $thelement->setAttribute('style', 'color: #FFFFFF;');
                }
            }

            $excelwidth = $thelement->getAttribute('excelwidth');

            if ($output_format == 'excel' && $excelwidth != '' && $excelwidth > 0)
            {
                $thelement->setAttribute('width', round($excelwidth * 10 / $lineWidth * 100 + 20));
                $thelement->removeAttribute('excelwidth');
            }
        }

        $tdelements = $doc->getElementsByTagName('td');

        foreach ($tdelements as $tdelement)
        {
            // some colouring
            if ($tdelement->getAttribute('class')=='titlebg2')
            {
                $tdelement->setAttribute('bgcolor', '#6E94B7');

                if ($output_format == 'pdf')
                {
                    $tdelement->setAttribute('color', '#FFFFFF');
                }
                else
                {
                    $tdelement->setAttribute('style', 'color: #FFFFFF;');
                }
            }

            $tdelement->removeAttribute('onmouseover');
            $tdelement->removeAttribute('onmouseout');
            $tdelement->removeAttribute('onclick');
            $tdelement->removeAttribute('onClick');

            if ($tdelement->hasChildNodes())
            {
                $aelement = $tdelement->firstChild;

                if ($aelement->nodeName == 'a')
                {
                    // This deals with every type of content that we have in cells specially progress notes
                    $children = $aelement->childNodes;

                    $newelement = $doc->createElement('span');

                    foreach ($children as $child)
                    {
                        if ($child->nodeName == 'br')
                        {
                            $newelement->appendChild($doc->createElement('br'));
                        }
                        else
                        {
                            $newelement->appendChild($doc->createTextNode($child->nodeValue));
                        }
                    }

                    $tdelement->replaceChild($newelement, $aelement);
                }
            }

            // truncate text to 1024 chars. The text nodes may be stored in different paragraphs, so we need to track
            // the total remaining length over multiple iterations.
            $remainingLength = 1024;
            foreach ($tdelement->childNodes as $node)
            {
                if ($node->nodeName == '#text' && $remainingLength > 0)
                {
                    if (\UnicodeString::strlen($node->nodeValue) > $remainingLength)
                    {
                        $node->nodeValue = \UnicodeString::substr($node->nodeValue, 0, $remainingLength) . '...';
                        $remainingLength = 0;
                    }
                    else
                    {
                        $remainingLength -= \UnicodeString::strlen($node->nodeValue);
                    }
                }
                else if ($node->nodeName == '#text' && $remainingLength <= 0)
                {
                    $node->nodeValue = '';
                }
            }
        }

        return $doc->saveHTML();
    }
}