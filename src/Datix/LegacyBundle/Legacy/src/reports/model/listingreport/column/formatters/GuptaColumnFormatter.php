<?php

namespace src\reports\model\listingreport\column\formatters;

use src\reports\model\listingreport\column\Column;

require_once 'Source/libs/FormClasses.php';

class GuptaColumnFormatter extends CachedColumnFormatter
{
    /**
     * @var Column
     */
    protected $column;
    
    public function __construct(Column $column)
    {
        $this->column = $column;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        return GuptaFormatEmulate($data, $this->column->getFormat());
    }
}