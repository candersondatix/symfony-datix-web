<?php

namespace src\reports\model\listingreport\groupby;

use src\reports\model\listingreport\Option;

class GroupBy extends Option
{    
    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        if (null === $this->alias)
        {
            $this->alias = 'groupby'.$this->order;
        }
        return $this->alias;
    }
}