<?php

namespace src\reports\model\listingreport\column\formatters;

require_once 'Source/libs/Subs.php';

class ComputedDateColumnFormatter extends CachedColumnFormatter
{
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        if ('' == $data)
        {
            return 0;
        }
        
        $dates = explode('|', $data);
        
        // get number of days between these two dates
        $days = floor((strtotime($dates[0]) - strtotime($dates[1])) / 86400);

        // work out how many of these are working days
        $currentDate = new \DateTime($dates[1]);
        $workingDays = 0;

        while ($days > 0)
        {
            $currentDate->modify('+1 day');

            if (!isHoliday($currentDate))
            {
                $workingDays++;
            }

            $days--;
        }

        return $workingDays;
    }
}