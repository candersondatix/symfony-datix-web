<?php

namespace src\reports\model\listingreport;

use src\framework\model\Entity;
use src\system\database\FieldInterface;
use src\reports\model\listingreport\column\formatters\ColumnFormatterFactory;

abstract class Option extends Entity
{
    /**
     * The value that's used to define the data for this option, as defined in field_formats.
     * 
     * @var string
     */
    protected $field;
    
    /**
     * The alias used for this option within the report query.
     * 
     * @var string
     */
    protected $alias;
    
    /**
     * The subform identifier.
     * 
     * @var string
     */
    protected $form;
    
    /**
     * The field type, as defined by the FieldInterface.
     * 
     * @var int
     */
    protected $type;
    
    /**
     * The option's title, typically the field name.
     * 
     * @var string
     */
    protected $title;
    
    /**
     * The order number for this option.
     * 
     * @var int
     */
    protected $order;
    
    /**
     * The date option used for this option, as defined by src\reports\model\report\Report.
     * 
     * @var int
     */
    protected $dateOption;
    
    /**
     * The equivalent FieldDef object for this option (if any).
     * 
     * @var FieldDefInterface
     */
    protected $fieldDef;
    
    /**
     * Formats the data for this option for output on the report.
     * 
     * @var ColumnFormatter
     */
    protected $formatter;
    
    /**
     * Used to fetch the appropriate formatter for this option.
     * 
     * @var ColumnFormatterFactory
     */
    protected $formatterFactory;
    
    /**
     * Getter for alias.
     * 
     * @return string
     */
    abstract public function getAlias();
    
    /**
     * Getter for field.
     * 
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }
    
    /**
     * Getter for form.
     * 
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }
    
    /**
     * Getter for type.
     * 
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Getter for title.
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Getter for order.
     * 
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Getter for dateOption.
     * 
     * @return int
     */
    public function getDateOption()
    {
        return $this->dateOption;
    }
    
    /**
     * Getter for fieldDef.
     * 
     * @return FieldInterface $fieldDef
     */
    public function getFieldDef()
    {
        return $this->fieldDef;
    }
        
    /**
     * Setter for fieldDef.
     * 
     * @param FieldInterface $fieldDef
     */
    public function setFieldDef(FieldInterface $fieldDef)
    {
        $this->fieldDef = $fieldDef;
    }
    
    /**
     * Setter for formatterFactory.
     * 
     * @param ColumnFormatterFactory $formatterFactory
     */
    public function setFormatterFactory(ColumnFormatterFactory $formatterFactory)
    {
        $this->formatterFactory = $formatterFactory;
    }
    
    /**
     * Formats data appropriately for this option.
     * 
     * @param string $data
     * 
     * @return string
     */
    public function formatData($data, $context = null)
    {
        if (null === $this->formatter)
        {
            $this->formatter = $this->formatterFactory->getFormatter($this);
        }
        return $this->formatter->format($data, $context);
    }
    
    /**
     * @inheritdoc
     */
    protected static function setIdentity()
    {
        return ['form','field'];
    }
}