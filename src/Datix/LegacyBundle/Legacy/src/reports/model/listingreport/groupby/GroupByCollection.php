<?php

namespace src\reports\model\listingreport\groupby;

use src\framework\model\EntityCollection;

class GroupByCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\groupby\\GroupBy';
    }
}