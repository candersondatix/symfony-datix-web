<?php

namespace src\reports\model\listingreport\column\formatters;

use src\reports\controllers\ReportWriter;

abstract class Columnformatter
{
    /**
     * Formats listing report cell data for output.
     * 
     * @param string $data    The formatted data.
     * @param int    $context The output context.
     * 
     * @return string
     */
    public function format($data, $context)
    {
        return $this->escapeOutput($this->doFormatting($data, $context), $context);
    }
    
    /**
     * Escapes the output data appropriately for the given context.
     * 
     * @param string $data    The formatted data.
     * @param int    $context The output context.
     * 
     * @return string
     */
    protected function escapeOutput($data, $context)
    {
        if (in_array($context, [ReportWriter::FULL_SCREEN, ReportWriter::DASHBOARD, ReportWriter::EXPORT_PDF]))
        {
            $data = htmlspecialchars($data);
        }
        return $data;
    }
    
    /**
     * Executes the formatting.
     * 
     * @param string $data    The formatted data.
     * @param int    $context The output context.
     * 
     * @return string
     */
    abstract protected function doFormatting($data, $context);
}