<?php

namespace src\reports\model\listingreport\orderby;

use src\reports\model\listingreport\OptionFactory;

class OrderByFactory extends OptionFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\orderby\\OrderBy';
    }
}