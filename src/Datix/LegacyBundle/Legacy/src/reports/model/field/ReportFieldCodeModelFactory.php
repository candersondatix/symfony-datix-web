<?php
namespace src\reports\model\field;

use src\system\database\code\CodeModelFactory;
use src\system\database\CodeFieldInterface;
use src\reports\exceptions\ReportException;

class ReportFieldCodeModelFactory extends CodeModelFactory
{
    public function __construct(CodeFieldInterface $field)
    {
        if (!($field instanceof ReportField))
        {
            throw new ReportException('field must be an instance of src\\reports\\model\\field\\ReportField, '.get_class($field).' given');
        }
        parent::__construct($field);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ReportFieldCodeMapper(new \DatixDBQuery(), $this);
    }
}