<?php
namespace src\reports\model\field;

use src\system\database\code\CodeMapper;
use src\system\database\field\Field;
use src\system\database\CodeFieldInterface;
use src\framework\query\Query;
use src\framework\query\FieldCollection;
use src\framework\model\ModelFactory;
use src\system\database\table\TableModelFactory;

class ReportFieldCodeMapper extends CodeMapper
{
    public function __construct(\DatixDBQuery $db, ModelFactory $factory)
    {
        parent::__construct($db, $factory);
        if (!($this->field instanceof ReportField))
        {
            throw new MapperException('field must be an instance of src\\reports\\model\\field\\ReportField, '.get_class($this->field).' given');
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
    	global $FieldDefsExtra;
    	
        // attempt to determine the module for the table that's being reported on
        $module = $this->field->reportModule;
        $moduleDefs = $this->registry->getModuleDefs();
        $TableDefs = $this->registry->getTableDefs();
        $fieldDefs = $this->registry->getFieldDefs();
        $showOnlyExcludedFields = $this->field->showOnlyExcludedFields();
        $showExcludedFields     = $this->field->showExcludedFields();

        /*
         * Check if this fieldset is excluded and if it is return an empty array otherwise carry on.
         * TODO: This can be refactored once Where class accepts AND NOT EXISTS plus a sub-select.
         */
        $sql = 'SELECT COUNT(*) FROM report_fieldset_exclusions WHERE rex_module = :rex_module AND rex_fieldset = :rex_fieldset';
        $result = \DatixDBQuery::PDO_fetch($sql, ['rex_module' => $module, 'rex_fieldset' => $this->field->fieldset], \PDO::FETCH_COLUMN);

        if ($result != '0' && false === $showExcludedFields && false === $showOnlyExcludedFields)
        {
            return [];
        }
        
        // build the standard field query
        $qf       = $this->factory->getQueryFactory();
        $stdQuery = $qf->getQuery();
        $where    = $qf->getWhere();

        // "0" designates the main module table
        if ($this->field->fieldset != 0)
        {
            //TODO - convert this into an entity collection
            $fdrTableArray = \DatixDBQuery::PDO_fetch_all('SELECT data_table FROM reportable_fieldsets WHERE module = :module AND fieldset = :fieldset', array('module' => $this->field->reportModule, 'fieldset' => $this->field->fieldset), \PDO::FETCH_COLUMN);

            foreach ($fdrTableArray as $tableName)
            {
                $table = $TableDefs[$tableName];
                if ($table->udf_link == '1')
                {
                    $includeUDFs = true;
                    //This module get will cause problems if there are 2 tables in the $fdrTableArray which both use extra fields.
                    //At the moment this shouldn't be a problem, but could be if we add linked tables with extra fields, in which
                    //case this whole method will need a look at.
                    $module = getModuleFromTable($tableName);
                }
            }
        }
        else
        {
            $fdrTableArray[] = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];
            $includeUDFs = true;
        }
        
        $fieldLabelTables = $fdrTableArray;
        foreach (['INCMED' => 'inc_medications','PALSUB' => 'pals_subjects','COMSUB' => 'compl_subjects'] as $subform => $subformTable)
        {
            if (in_array($subformTable, $fieldLabelTables))
            {
                $fieldLabelTables[] = $subform;
            }
        }

        $where->add(
            $where->nest('OR',
                $qf->getFieldCollection()
                    ->field('fdr_table')->in($fdrTableArray)
                    ->field('fdr_data_type')->in(array('C','D','Y')),
                $qf->getFieldCollection()
                    ->field('fdr_name')->in($this->field->fieldset == 0 ? GetAllFieldsByType($module, 'string_search', false) : [])
            ),
            $qf->getFieldCollection()->field('fdr_name')->notIn(GetHardCodedReportExcludedFields())
        );

        if (true === $showOnlyExcludedFields)
        {
            $where->add(
                $qf->getFieldCollection()
                    ->field('fdr_table + \'.\' + fdr_name')->in(
                        $qf->getQuery()
                            ->select(array('rex_field'))
                            ->from('report_exclusions')
                            ->where(array('rex_module' => $this->field->getReportModule(), 'rex_fieldset' => $this->field->fieldset)))
            );
        }
        elseif (false === $showExcludedFields && false === $showOnlyExcludedFields)
        {
            $where->add(
                $qf->getFieldCollection()
                    ->field('fdr_table + \'.\' + fdr_name')->notIn(
                        $qf->getQuery()
                            ->select(array('rex_field'))
                            ->from('report_exclusions')
                            ->where(array('rex_module' => $this->field->getReportModule(), 'rex_fieldset' => $this->field->fieldset)))
            );
        }

        //Hack to allow "Policy title and cover period" field to appear with relabelling. Fails without this because it doesn't appear on the policies table.
        //Can be removed once relabelling is controlled only via field_directory meta data.
        if ($this->field->reportModule == 'CLA')
        {
            $fieldLabelTables[] = 'CLAPOL';
        }
        
        $stdQuery->select(array('fdr_table + \'.\' + fdr_name AS code', 'COALESCE((SELECT TOP 1 fmt_new_label FROM vw_field_formats WHERE fdr_name = fmt_field AND fmt_table IN (\''.implode("','", $fieldLabelTables).'\')), fdr_label) AS description'))
                 ->from('field_directory')
                 ->where($where);
        
        // convert and append passed query object conditions to standard field query
        $stdPassedParams = array();
        if (null !== ($passedWhere = $query->getWhere()))
        {
            $stdPassedWhere = clone $passedWhere;
            $stdPassedWhere->convertFieldName('code', 'fdr_name');
            // the table name has to be manually stitched into the query below for now - we may need to make this more flexible so we can pass a subquery in as part of a field name
            $stdPassedWhere->convertFieldName('description', 'COALESCE((SELECT TOP 1 fmt_new_label FROM vw_field_formats WHERE fdr_name = fmt_field AND fmt_table IN (\''.implode("','", $fieldLabelTables).'\')), fdr_label)');
            $stdPassedWhere->convertFieldName('data_type', 'fdr_data_type');
            $stdQuery->where($stdPassedWhere);
        }
       
        // build the extra field query
        if ($module !== null && $includeUDFs)
        {
            //Slight hack to map reportable_fieldsets (groups of joined tables) to modules: there should be at most one
            //main module table per reportable fieldset.
            foreach ($fdrTableArray as $fdrTable)
            {
                if (!$module)
                {
                    $module = getModuleFromTable($fdrTable);
                }
            }
            $udfQuery  = $qf->getQuery();
            $udfWhere  = $qf->getWhere();
            $joinWhere = $qf->getWhere();
            
            $udfWhere->add(
                $udfWhere->nest('OR',
                    $qf->getFieldCollection()->field('uml_module')->eq($module),
                    $qf->getFieldCollection()->field('uml_module')->isNull(),
                    $qf->getFieldCollection()->field('uml_module')->isEmpty()
                ),
                $qf->getFieldCollection()->field('fld_type')->in(array('C','D','T','Y'))
            );

            if (true === $showOnlyExcludedFields)
            {
                $udfWhere->add(
                    $qf->getFieldCollection()
                        ->field(['\''.($moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE']).'.UDF_\' + CAST(? AS VARCHAR(32))','udf_fields.recordid'])->in(
                            $qf->getQuery()
                                ->select(array('rex_field'))
                                ->from('report_exclusions')
                                ->where(array('rex_module' => $this->field->getReportModule(), 'rex_fieldset' => $this->field->fieldset)))
                );
            }
            elseif (false === $showExcludedFields && false === $showOnlyExcludedFields)
            {
                $udfWhere->add(
                    $qf->getFieldCollection()
                        ->field(['\''.($moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE']).'.UDF_\' + CAST(? AS VARCHAR(32))','udf_fields.recordid'])->notIn(
                            $qf->getQuery()
                                ->select(array('rex_field'))
                                ->from('report_exclusions')
                                ->where(array('rex_module' => $this->field->getReportModule(), 'rex_fieldset' => $this->field->fieldset)))
                );
            }
                        
            $joinWhere->add($qf->getFieldCollection()->field('udf_fields.recordid')->eq('udf_mod_link.uml_id', true));
            
            $udfQuery->select(array('\''.($moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE']).'.UDF_\' + CAST(udf_fields.recordid AS VARCHAR(32)) AS code', 'fld_name AS description'))
                     ->from('udf_fields')
                     ->join('udf_mod_link', $joinWhere, 'LEFT')
                     ->where($udfWhere);
            
            // convert and append passed query object conditions to extra field query
            if ($passedWhere !== null)
            {
                $udfPassedWhere = clone $passedWhere;
                $udfPassedWhere->convertFieldName('code', '\'UDF_\' + CAST(udf_fields.recordid AS VARCHAR(32))');
                $udfPassedWhere->convertFieldName('description', 'fld_name');
                $udfPassedWhere->convertFieldName('data_type', 'fld_type');
                $udfQuery->where($udfPassedWhere);
            }
        }
        
        $orderBy = count($query->getOrderBy()) == 0 ? array('description') : $query->getOrderBy();
        
        if (isset($udfQuery))
        {
            $udfQuery->orderBy($orderBy);
            list($sql, $parameters) = $qf->getSqlWriter()->writeUnionStatement($stdQuery, $udfQuery);
        }
        else
        {
            $stdQuery->orderBy($orderBy);
            list($sql, $parameters) = $qf->getSqlWriter()->writeStatement($stdQuery);
        }

        $parameters = array_merge($parameters);

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        $rs = $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
        
        foreach ($rs as $key => $data)
        {
            $fieldDef = $fieldDefs[$data['code']];

            if ($fieldDef !== null)
            {
                if( isset( $FieldDefsExtra[ $module ][ $fieldDef->getName() ]['BlockFromReports'] ) && $FieldDefsExtra[ $module ][ $fieldDef->getName() ]['BlockFromReports'] === true )
                {
                    unset( $rs[ $key ] );
                    $renumber = true;
                }
                //special cases for fields that don't relabel well from the RC - can remove once relabelling works sensibly.
                else if ($fieldDef->getName() == 'link_treatment' && _tk('treatment_received') != '')
                {
                    $rs[$key]['description'] = _tk('treatment_received');
                }
                else if ($fieldDef->getName() == 'inc_injury' && _tk('injury') != '' && $data['code'] != 'incidents_main.inc_injury')
                {
                    $rs[$key]['description'] = _tk('injury');
                }
                else if ($fieldDef->getName() == 'inc_bodypart' && _tk('body_part') != '' && $data['code'] != 'incidents_main.inc_bodypart')
                {
                    $rs[$key]['description'] = _tk('body_part');
                }
                else
                {
                    //need to add suffix for accreditation
                    if (isset($FieldDefsExtra[$module][$fieldDef->getName()]['SubmoduleSuffix']))
                    {
                        $rs[$key]['description'] .= ' '.$FieldDefsExtra[$module][$fieldDef->getName()]['SubmoduleSuffix'];
                    }
                }
            }
        }

        return isset( $renumber ) ? array_values( $rs ) : $rs; // if a numeric key has been removed, the keys need to be renumbered otherwise the getNewReportFields() will give wrong result
    }
}
