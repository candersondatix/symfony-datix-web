<?php
namespace src\reports\model\report;

class RiskTrackerReport extends LineChart
{
    /**
     * The recordid of the risk record to base this report on.
     * @var int
     */
    protected $recordid;

    /**
     * Date to start the data set from.
     * @var date
     */
    protected $startDate;

    /**
     * Date to end the dataset at
     * @var date
     */
    protected $endDate;

    /**
     * Target field (grade or level) to report on.
     * @var string
     */
    protected $field;


    protected $yAxisName;

    /**
     * Fake Y axis values to replace "counts".
     * Needed because this is a line chart that doesn't report on numerical data.
     * @var array
     */
    protected $yAxisValueArray = array();

    /**
     * {@inheritdoc}
     */
    public function __construct($recordid, $field, $startDate = '', $endDate = '')
    {
        parent::__construct();
        $this->count_style = self::COUNT_NUM;

        $this->recordid = $recordid;
        $this->field = $field;
        $this->startDate = $startDate;
        $this->endDate = $endDate;

        if($field == 'level')
        {
            $this->yAxisName = _tk('level');
        }
        else
        {
            $this->yAxisName = _tk('rating');
        }

        $this->title = _tk('risk_grade_tracker_report_title').$recordid;

        $this->rows = _tk('date');
        $this->columns = _tk('level');
    }

    public function setyAxisValueArray(array $array)
    {
        $this->yAxisValueArray = $array;
    }

    public function getyAxisValueArray()
    {
        return $this->yAxisValueArray;
    }

    public function getXAxisLabel()
    {
        return _tk('date_update');
    }

}