<?php
namespace src\reports\model\report;

use src\framework\registry\Registry;
use src\reports\exceptions\InvalidOptionException;
use src\reports\exceptions\ReportException;
use src\reports\ReportEngine;

class SpcChart extends Report
{
    // type constants
    const C_CHART      = 1;
    const I_CHART      = 2;
    const MOVING_RANGE = 3;
    const RUN_CHART    = 4;
    
    /**
     * The SPC chart type.
     * 
     * @var int
     */
    protected $type;

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        switch ($this->type)
        {
            case self::C_CHART: return _tk('spc_chart_cchart'); break;
            case self::I_CHART: return _tk('spc_chart_ichart'); break;
            case self::MOVING_RANGE: return _tk('spc_chart_movingrange'); break;
            case self::RUN_CHART: return _tk('spc_chart_runchart'); break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        switch ($this->type)
        {
            case self::C_CHART: return Report::SPC_C_CHART; break;
            case self::I_CHART: return Report::SPC_I_CHART; break;
            case self::MOVING_RANGE: return Report::SPC_MOVING_RANGE; break;
            case self::RUN_CHART: return Report::SPC_RUN_CHART; break;
        }
    }

    /**
     * Prevent columns property from being set.
     * 
     * @param $value
     * 
     * @throws InvalidOptionException
     */
    public function setColumns($value)
    {
        $this->columns = null;
        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns" property on SPC Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }
    
    /**
     * Prevent columns_date_option property from being set.
     * 
     * @param $value
     * 
     * @throws InvalidOptionException
     */
    public function setColumns_date_option($value)
    {
        $this->columns_date_option = null;
        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns_date_option" property on SPC Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }
    
    /**
     * Check for valid type values.
     * 
     * @param int $value
     * 
     * @throws ReportException
     */
    public function setType($value)
    {
        if (!in_array($value, array(self::C_CHART, self::I_CHART, self::MOVING_RANGE, self::RUN_CHART)))
        {
            throw new ReportException('Invalid SPC chart type value.');
        }
        $this->type = $value;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    protected function getLegendState($data)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function buildDataTab(\PHPExcel $objPHPExcel, array $data)
    {
        $objPHPExcel = parent::buildDataTab($objPHPExcel, $data);

        switch ($this->type)
        {
            case self::RUN_CHART:
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                break;

            case self::MOVING_RANGE:
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Moving range');
                $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'RUCL');
                $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                break;

            case self::C_CHART:
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'UCL');
                $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'LCL');
                $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'UWL');
                $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'LWL');
                $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                break;

            case self::I_CHART:
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Mean');
                $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'UCL');
                $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'LCL');
                $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                break;
        }

        $excelRowNumber = 2;

        $data = $this->addCalculatedDataToArray($data);

        foreach ($data['rowHeaders'] as $code => $description)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('0', $excelRowNumber, str_replace('&#034', '"', $description));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('1', $excelRowNumber, $data['data'][$code] ?: 0);

            switch ($this->type)
            {
                case self::RUN_CHART:
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $excelRowNumber, $data['calculated']['average']);
                    break;

                case self::MOVING_RANGE:
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $excelRowNumber, $data['calculated']['average']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('3', $excelRowNumber, round($data['calculated']['upper_ctrl_limit'], 2));
                    break;

                case self::C_CHART:
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $excelRowNumber, round($data['calculated']['average'], 2));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('3', $excelRowNumber, round($data['calculated']['upper_ctrl_limit'], 2));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('4', $excelRowNumber, round($data['calculated']['lower_ctrl_limit'], 2));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('5', $excelRowNumber, round($data['calculated']['upper_warning_limit'], 2));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('6', $excelRowNumber, round($data['calculated']['lower_warning_limit'], 2));
                    break;

                case self::I_CHART:
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $excelRowNumber, round($data['calculated']['average'], 2));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('3', $excelRowNumber, round($data['calculated']['upper_ctrl_limit'], 2));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('4', $excelRowNumber, round($data['calculated']['lower_ctrl_limit'], 2));
                    break;
            }

            $excelRowNumber++;
        }

        return $objPHPExcel;
    }

     /**
     * {@inheritdoc}
     */
    protected function getDataSeriesValues($data)
    {
        switch ($this->type)
        {
            case self::RUN_CHART: $numberOfColumns = 2; break;
            case self::MOVING_RANGE: $numberOfColumns = 3; break;
            case self::C_CHART: $numberOfColumns = 6; break;
            case self::I_CHART: $numberOfColumns = 4; break;
        }

        //	Set the Labels for each data series we want to plot
        for ($colNumber = 1; $colNumber <= $numberOfColumns; $colNumber++)
        {
            $dataseriesLabels[] = new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$'.self::ExcelConvertToLetter($colNumber).'$1');
        }

        //	Set the X-Axis Labels
        $xAxisTickValues = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$2:$A$'.(count($data['rowHeaders']) + 1)),
        );

        //	Set the Data values for each data series we want to plot
        $colNumber = 1;
        for ($colNumber = 1; $colNumber <= $numberOfColumns; $colNumber++)
        {
            $colLetter = self::ExcelConvertToLetter($colNumber);
            if( $colLetter == 'B' )
            {
            	$dataSeriesValues[] = new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$'.$colLetter.'$2:$'.$colLetter.'$'.(count($data['rowHeaders']) + 1));
            }
            else
            {
            	$dataSeriesValues[] = (new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$'.$colLetter.'$2:$'.$colLetter.'$'.(count($data['rowHeaders']) + 1)))->setPointMarker('none');
            }
        }

        return array(
            'dataseriesLabels' => $dataseriesLabels,
            'xAxisTickValues' => $xAxisTickValues,
            'dataSeriesValues' => $dataSeriesValues
        );    
    }

     /**
     * {@inheritdoc}
     */
    protected function getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues)
    {
        $series = parent::getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues);
        
        $series->setPlotType(\PHPExcel_Chart_DataSeries::TYPE_LINECHART);

        return $series;
    }

    public function addCalculatedDataToArray($data)
    {
        switch ($this->getType())
        {
            case SpcChart::C_CHART:
                // For c-Charts we need:
                // To plot: The data itself, the average, the control limits
                // To calculate: The average, the standard deviation, the control limits (calculated from the mean range)
                // Average
                if (count($data['data']) > 0)
                {
                    $data['calculated']['average'] = array_sum($data['data'])/count($data['data']);
                }

                // Standard deviation
                $s = sqrt($data['calculated']['average']);
                // Warning limit
                $data['calculated']['upper_warning_limit'] = $data['calculated']['average']+2*$s;
                $data['calculated']['lower_warning_limit'] = $data['calculated']['average']-2*$s;
                // Control limits
                $data['calculated']['upper_ctrl_limit'] = $data['calculated']['average']+3*$s;
                $data['calculated']['lower_ctrl_limit'] = $data['calculated']['average']-3*$s;
                break;
            case SpcChart::I_CHART:
                // For I-Charts we need:
                // To plot: The data itself, the average, the control limits
                // To calculate: The average, the sum of the moving ranges, the mean range, the control limits (calculated from the mean range)
                // Average
                if (count($data['data']) > 0)
                {
                    $data['calculated']['average'] = array_sum($data['data'])/count($data['data']);
                }

                // Sum of moving ranges
                $moving_ranges = $this->getMovingRanges($data);
                $sum_moving_ranges = array_sum($moving_ranges);
                // Mean range
                $mean_range = ((count($data['data'])-1) == 0) ? 0 : ($sum_moving_ranges/(count($data['data'])-1));

                // Upper control limit
                $data['calculated']['upper_ctrl_limit'] = $data['calculated']['average']+(2.66*$mean_range);
                // Lower control limit
                $data['calculated']['lower_ctrl_limit'] = $data['calculated']['average']-(2.66*$mean_range);
                break;
            case SpcChart::MOVING_RANGE:
                // For a moving range chart we need:
                // To plot: The moving ranges, the median line, the upper control limit
                // To calculate: The moving ranges, the median value of the median ranges,
                // the sum of the moving ranges, the mean range, the upper control limit (calculated from the mean range)
                // Sum of moving ranges
                $moving_ranges = $this->getMovingRanges($data);
                $sum_moving_ranges = array_sum($moving_ranges);
                // Mean range
                $mean_range = $sum_moving_ranges/(count($moving_ranges)+1); // +1 to take into account the first piece of data

                if (count($data['data']) > 0)
                {
                    $data['calculated']['average'] = array_sum($data['data'])/count($data['data']);
                }

                // Range Upper control limit
                $data['calculated']['upper_ctrl_limit'] = (3.863*$data['calculated']['average']); // 3.863 because there are no subgroup, we are dealing with only one set of observations
                // There is no lower control limit

                // Set data structure for plotting the above
                // We need to plot the moving ranges not the data itself
                $firstKey = array_keys($data['data'])[0];
                $data['data'][$firstKey] = null;
                // so there is no value at index 0
                $ubound = count($data['data'])-1;

                for ($i = 1; $i <= $ubound; $i++)
                {
                    $moving_range = $moving_ranges[$i-1];
                    $key = array_keys($data['data'])[$i];
                    $data['data'][$key] = "$moving_range";
                }
                break;
            case SpcChart::RUN_CHART:
                // For Run charts we need:
                // To plot: The data itself, the median for the center line
                // To calculate: The median
                // Median
                if (count($data['data']) > 0)
                {
                    $data['calculated']['average'] = array_sum($data['data'])/count($data['data']);
                }
                break;
        }

        return $data;
    }

    /**
     * Returns an array of moving ranges, from an array of data.
     *
     * @param array $data The report data.
     *
     * @return array
     */
    protected function getMovingRanges(array $data)
    {
        $moving_ranges = array();
        $auxData = array_values($data['data']);

        for ($i = 0; $i < (count($auxData) - 1); $i++)
        {
            $moving_ranges[] = abs($auxData[$i] - $auxData[$i+1]);
        }

        return $moving_ranges;
    }


}