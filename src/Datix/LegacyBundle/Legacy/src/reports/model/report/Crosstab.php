<?php
namespace src\reports\model\report;

use src\framework\model\RecordEntity;
use src\reports\ReportEngine;

class Crosstab extends Report
{
    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('crosstab_report');
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::CROSSTAB;
    }

    /**
     * {@inheritdoc}
     */
    protected function buildDataTab(\PHPExcel $objPHPExcel, array $data)
    {
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Data');

        // Set graph title
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', $this->getTitle());

        //Column headers
        $currentColumn = 1;
        foreach($data['columnHeaders'] as $colTitle)
        {
            $colLetter = self::ExcelConvertToLetter($currentColumn);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, 3, $colTitle);
            $objPHPExcel->getActiveSheet()->getColumnDimension($colLetter)->setAutoSize(true);
            $currentColumn++;
        }

        //Add Total column
        $colLetter = self::ExcelConvertToLetter($currentColumn);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, 3, 'Total');
        $objPHPExcel->getActiveSheet()->getColumnDimension($colLetter)->setAutoSize(true);

        //Row Headers
        $currentRow = 4;
        foreach ($data['rowHeaders'] as $rowTitle)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $currentRow, $rowTitle);
            $currentRow++;
        }

        //Add Total row
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $currentRow, 'Total');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        // Footer
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $currentRow+5, (isset($GLOBALS['dtxexcel_footer']) ? strip_tags($GLOBALS['dtxexcel_footer']) : strip_tags($GLOBALS['dtxprint_footer'])));
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        //Report data
        $currentRow = 4;
        foreach ($data['rowHeaders'] as $rowKey => $rowTitle)
        {
            $currentColumn = 1;
            foreach($data['columnHeaders'] as $colKey => $colTitle)
            {
                if($data['data'][$rowKey][$colKey] == '')
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['toolText']['null']);
                }
                else
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['toolText'][$rowKey][$colKey]);
                }
                $currentColumn++;
            }

            //Add total column value for this row
            if($data['totals']['rows'][$rowKey] == '')
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['toolText']['null']);
            }
            else
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['totals']['rows'][$rowKey]);
            }

            $currentRow++;
        }

        //Add total row values for each column
        $currentColumn = 1;
        foreach($data['columnHeaders'] as $colKey => $colTitle)
        {
            if($data['totals']['columns'][$colKey] == '')
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['toolText']['null']);
            }
            else
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['totals']['columns'][$colKey]);
            }
            $currentColumn++;
        }

        //Add overall total
        if($data['totals']['total'] == '')
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['toolText']['null']);
        }
        else
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, $data['totals']['total']);
        }

        $totalRow = $currentRow;
        $totalColumn = self::ExcelConvertToLetter($currentColumn);

        // Set styles for spreadsheet cells:

        //Top left cell
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('AFC6DB');

        //Row and column totals
        $objPHPExcel->getActiveSheet()->getStyle('B' . $totalRow . ':' . $totalColumn . $totalRow)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('AFC6DB');
        $objPHPExcel->getActiveSheet()->getStyle($colLetter . '4:' . $totalColumn . $totalRow)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('AFC6DB');

        //Row headers
        $objPHPExcel->getActiveSheet()->getStyle('A4:A' . $totalRow)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('162D54');
        $objPHPExcel->getActiveSheet()->getStyle('A4:A' . $totalRow)->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);

        //Column Headers
        $objPHPExcel->getActiveSheet()->getStyle('B3:' . $totalColumn . '3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('162D54');
        $objPHPExcel->getActiveSheet()->getStyle('B3:' . $totalColumn . '3')->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_WHITE);

        //Borders
        $objPHPExcel->getActiveSheet()->getStyle('A3:' . $totalColumn . $totalRow)->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);

        return $objPHPExcel;
    }

    protected function buildChartTab(\PHPExcel $objPHPExcel, array $data)
    {
        return $objPHPExcel;
    }

    public function ParseHTMLTableForExport($html, $output_format, $pdf)
    {
        $html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>'.$html.'</html>';

        // Replace the whitespace with nothing
        // This is needed because PHP DOMDocument interprets the whitespace has child nodes of td elements
        $html = preg_replace("/>[\s\r\n\t]+</", "><", $html);

        $doc = new \DOMDocument('1.0', 'utf-8');
        @$doc->loadHTML($html);
        $telements = $doc->getElementsByTagName('table');

        // remove attributes from tables
        foreach ($telements as $telement)
        {
            $telement->removeAttribute('class');
            $telement->removeAttribute('border');
            $telement->removeAttribute('width');
            $telement->removeAttribute('bgcolor');
            $telement->setAttribute('border', '1');
    
            if ($output_format == 'pdf')
            {
                $telement->removeAttribute('width');
                $telement->setAttribute('width', $pdf->width);
            }
        }

        $tdelements = $doc->getElementsByTagName('td');

        foreach ($tdelements as $tdelement)
        {
            if ($tdelement->getAttribute('class') == 'crosstab header' ||
                $tdelement->getAttribute('class') == 'crosstab header top-header')
            {
                $tdelement->setAttribute('bgcolor', '#162D54');

                if ($output_format == 'pdf')
                {
                    $tdelement->setAttribute('color', '#FFFFFF');
                }
                else
                {
                    $felement = $tdelement->firstChild;

                    if ($felement != null)
                    {
                        $fvalue = $felement->textContent;
                        $fontelement = $doc->createElement("font");
                        $fontelement->setAttribute('color', '#FFFFFF');
                        $newelement = $doc->createTextNode($fvalue);
                        $fontelement->appendChild($newelement);
                        $tdelement->replaceChild($fontelement, $felement);
                    }
                }
            }

            if ($tdelement->getAttribute('class') == 'crosstab total data')
            {
                $tdelement->setAttribute('bgcolor', '#afc6db');
            }

            $tdelement->removeAttribute('onmouseover');
            $tdelement->removeAttribute('onmouseout');
            $tdelement->removeAttribute('onclick');
            $tdelement->removeAttribute('onClick');

            if ($tdelement->hasChildNodes())
            {
                $aelement = $tdelement->firstChild;

                if ($aelement->nodeName == 'a')
                {
                    $avalue = $aelement->textContent;

                    $newelement = $doc->createTextNode($avalue);
                    $tdelement->replaceChild($newelement, $aelement);
                }
            }

            if ($output_format == 'pdf')
            {
                // truncate text to 1024 chars
                foreach ($tdelement->childNodes as $node)
                {
                    if ($node->nodeName == '#text' && \UnicodeString::strlen($node->nodeValue) > 1024)
                    {
                        $node->nodeValue = \UnicodeString::substr($node->nodeValue, 0, 1024) . '...';
                    }
                }
            }
        }

        return $doc->saveHTML();
    }

}