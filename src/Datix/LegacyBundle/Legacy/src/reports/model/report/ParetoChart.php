<?php
namespace src\reports\model\report;

use src\framework\registry\Registry;
use src\reports\exceptions\InvalidOptionException;
use src\reports\ReportEngine;

class ParetoChart extends Report
{

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('pareto_graph');
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::PARETO;
    }

    /**
     * Prevent columns property from being set.
     * 
     * @param $value
     * 
     * @throws InvalidOptionException
     */
    public function setColumns($value)
    {
        $this->columns = null;
        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns" property on Pareto Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }
    
    /**
     * Prevent columns_date_option property from being set.
     * 
     * @param $value
     * 
     * @throws InvalidOptionException
     */
    public function setColumns_date_option($value)
    {
        $this->columns_date_option = null;
        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns_date_option" property on Pareto Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }

    /**
     * {@inheritdoc}
     */
   protected function buildDataTab(\PHPExcel $objPHPExcel, array $data)
   {
       $objPHPExcel = parent::buildDataTab($objPHPExcel, $data);

       $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Cumulative data');
       $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
       $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
       $pareto_datasum = 0;

       $rowNumber = 2;
       arsort($data['data']);
       foreach ($data['data'] as $value)
       {
           $pareto_datasum += $value;
           $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('2', $rowNumber, $pareto_datasum);
           $rowNumber++;
       }

        return $objPHPExcel;
    }

    /**
     * {@inheritdoc}
     */
    protected function getDataSeriesValues($data)
    {
        //	Set the Labels for each data series we want to plot
        $dataseriesLabels[] = new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$1', NULL, 1);
        $dataseriesLabels[] = new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$B$1', NULL, 1);

        //	Set the X-Axis Labels
        $xAxisTickValues = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$2:$A$'.(count($data['rowHeaders']) + 1), NULL, 4),	//	Q1 to Q4
        );

        //	Set the Data values for each data series we want to plot
        $dataSeriesValues[] = new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$B$2:$B$'.(count($data['rowHeaders']) + 1), NULL, 1);
        $dataSeriesValues[] = (new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$C$2:$C$'.(count($data['rowHeaders']) + 1), NULL, 1))->setPointMarker('none');

        return array(
            'dataseriesLabels' => $dataseriesLabels,
            'xAxisTickValues' => $xAxisTickValues,
            'dataSeriesValues' => $dataSeriesValues);
    }


    /**
     * {@inheritdoc}
     */
    protected function getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues)
    {
    	$series1 = new \PHPExcel_Chart_DataSeries(
    		\PHPExcel_Chart_DataSeries::TYPE_BARCHART,		
    		\PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,	
    		range(0, count($dataSeriesValues[0])-1),			
    		array( $dataseriesLabels[0] ),						
    		$xAxisTickValues,								
    		array( $dataSeriesValues[0]	)		
    	);

    	// Make it a vertical column rather than a horizontal bar graph
    	$series1->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);
    	
    	$series2 = new \PHPExcel_Chart_DataSeries(
    		\PHPExcel_Chart_DataSeries::TYPE_SCATTERCHART,		
    		null,	
    		range(0, count($dataSeriesValues[1])-1),			
    		array($dataseriesLabels[1]),					
    		null,											
    		array($dataSeriesValues[1]),
    		null,
    		\PHPExcel_Chart_DataSeries::STYLE_LINEMARKER
    	);
    	
        return array( $series1, $series2 );
    }

}