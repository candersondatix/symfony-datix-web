<?php
namespace src\reports\model\report;

use src\reports\exceptions\ReportException;

class LineChart extends Report
{

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('line_graph');
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::LINE;
    }

    /**
     * @see \src\reports\model\report\Report::getDataSeriesValues()
     */
    protected function getDataSeriesValues($data)
    {
    	$dsv = parent::getDataSeriesValues($data);
    	
    	// because of the appropriate tooltips in Excel
    	while( count( $dsv['xAxisTickValues'] ) != count( $dsv['dataseriesLabels'] ) )
    	{
    		$dsv['xAxisTickValues'][] = $dsv['xAxisTickValues'][0];
    	}
    	
    	return $dsv;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues)
    {
        $series = parent::getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues);

        $series->setPlotType(\PHPExcel_Chart_DataSeries::TYPE_LINECHART);

        return $series;
    }
}