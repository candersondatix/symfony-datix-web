<?php
namespace src\reports\model\report;

class GaugeChart extends Report
{
    /**
     * Max value for the first section of the report
     * @var string
     */
    protected $gauge_section1_max = '';

    /**
     * Colour for the first section of the report
     * @var string
     */
    protected $gauge_section1_colour = '';

    /**
     * Max value for the second section of the report
     * @var string
     */
    protected $gauge_section2_max = '';

    /**
     * Colour for the second section of the report
     * @var string
     */
    protected $gauge_section2_colour = '';

    /**
     * Max value for the third section of the report
     * @var string
     */
    protected $gauge_section3_max = '';

    /**
     * Colour for the third section of the report
     * @var string
     */
    protected $gauge_section3_colour = '';

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('gauge_chart');
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::GAUGE;
    }

    public function getTitle()
    {
        global $ModuleDefs;

        if ($this->title == '')
        {
            return _tk('gauge_default_title') . ' ' . $ModuleDefs[$this->module]['REC_NAME_PLURAL'];
        }

        return $this->title;
    }


}