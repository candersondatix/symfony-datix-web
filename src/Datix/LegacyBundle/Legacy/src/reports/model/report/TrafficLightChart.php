<?php
namespace src\reports\model\report;

use src\framework\registry\Registry;
use src\reports\exceptions\InvalidOptionException;
use src\reports\model\report\ReportModelFactory;

class TrafficLightChart extends Report
{
    /**
     * @var array
     */
    protected $trafficCodeColours;

    /**
     * @var array
     */
    protected $trafficLightSegments = array();

    /**
     * @var array
     */
    protected $defaultColours = array(
        'AFD8F8',
        'F6BD0F',
        '8BBA00',
        'FF8E46',
        '008E8E',
        'D64646',
        '8E468E',
        '588526',
        'B3AA00',
        '008ED6',
        '9D080D',
        'A186BE',
        'CC6600',
        'FDC689',
        'ABA000',
        'F26D7D',
        'FFF200',
        '0054A6',
        'F7941C',
        'CC3300',
        '006600',
        '663300',
        '6DCFF6'
    );

    /**
     * @var null
     */
    protected $trafficLightImageMap = null;

    /**
     * @var null
     */
    protected $trafficLightLegendImage = null;

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('traffic_light_chart');
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::TRAFFIC;
    }

    /**
     * Prevent columns property from being set.
     *
     * @param $value
     *
     * @throws InvalidOptionException
     */
    public function setColumns($value)
    {
        $this->columns = null;

        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns" property on Traffic Light Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }

    /**
     * Prevent columns_date_option property from being set.
     *
     * @param $value
     *
     * @throws InvalidOptionException
     */
    public function setColumns_date_option($value)
    {
        $this->columns_date_option = null;

        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns_date_option" property on Traffic Light Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }

    /**
     * Provides a list of default colour to use in the case that colours aren't explicitly defined.
     *
     * @return array
     */
    public function getDefaultColours()
    {
        return $this->defaultColours;
    }

    /**
     * @param $segments
     */
    public function setTrafficLightSegments($segments)
    {
        $this->trafficLightSegments = $segments;
    }

    /**
     * Builds the legend for traffic lights.
     */
    public function getLegendForTrafficLights()
    {
        $lineHeight = 20;
        $margin = 30;
        $widthFull = 800;
        $legendUnits = array();
        $linePointer = $margin;
        $lines = 1;
        $defaultColours = $this->getDefaultColours();

        if (is_array($this->trafficLightSegments))
        {
            foreach ($this->trafficLightSegments as $trafficLightSegment)
            {
                if ($trafficLightSegment['colour'] == '')
                {
                    if (empty($defaultColours))
                    {
                        $defaultColours = $this->getDefaultColours();
                    }

                    $trafficLightSegment['colour'] = array_shift($defaultColours);
                }

                $segmentLength = $this->getLegendLabelLength($trafficLightSegment['description']);

                if (($linePointer + $segmentLength) > $widthFull)
                {
                    $lines++;
                    $linePointer = $margin;
                }

                $description = $this->decodeQuotes($trafficLightSegment['description']);

                $legendUnits[] = array(
                    'colour' => $trafficLightSegment['colour'],
                    'description' => $description,
                    'link' => $trafficLightSegment['link'],
                    'line' => $lines,
                    'linePointer' => $linePointer,
                    'segmentLength' => $segmentLength
                );

                $linePointer += $segmentLength;
            }
        }

        $height = $lines * 20 + 30;

        if (!$img=imagecreatetruecolor($widthFull,$height))
        {
            fatal_error("Error creating Traffic lights legend as GD image");
        }

        // allocate a background colour
        if (!$bgColor=imagecolorallocate($img,0xF1,0xF7,0xFF))  //#F1F7FF
        {
            fatal_error("Error allocating GD colour");
        }

        imagefilledrectangle($img, 0, 0, $widthFull, $height, $bgColor);
        $blackFontColor = imagecolorallocate($img,0x00,0x00,0x00);

        $imgMap = '<map name="traffic">';

        foreach ($legendUnits as $legendUnit)
        {
            $rCol = hexdec(substr($legendUnit['colour'], 0, 2));
            $gCol = hexdec(substr($legendUnit['colour'], 2, 2));
            $bCol = hexdec(substr($legendUnit['colour'], 4, 2));

            $segmentColour = imagecolorallocate($img,$rCol,$gCol,$bCol);

            imagefilledrectangle($img, $legendUnit['linePointer'], $legendUnit['line'] * $lineHeight, $legendUnit['linePointer'] + 8, $legendUnit['line']*$lineHeight+8, $segmentColour);
            imagettftext ($img, 9, 0, $legendUnit['linePointer']+15, $legendUnit['line'] * $lineHeight+9, $blackFontColor, 'Arial.ttf' ,$legendUnit['description']);
            $imgMap .= '<area shape="rect" coords="'.$legendUnit['linePointer'].','.($legendUnit['line'] * $lineHeight).','.($legendUnit['linePointer'] + $legendUnit['segmentLength']).','.($legendUnit['line'] * $lineHeight + 8).'" href="'.$legendUnit['link'].'" alt="'.$legendUnit['description'].'">';
        }

        $imgMap .= '</map>';
        $this->trafficLightImageMap = $imgMap;

        ob_start();
        imagepng($img);
        $this->trafficLightLegendImage =  ob_get_contents();
        ob_end_clean();

        // free memory
        imagedestroy($img);
    }

    /**
     * @param $string
     * @return int
     */
    protected function getLegendLabelLength($string)
    {
        return \UnicodeString::strlen($string) * 6.2 + 30;
    }

    /**
     * Manually decodes encoded double quotes.
     *
     * @param string $string
     *
     * @return string
     */
    protected function decodeQuotes($string)
    {
        return str_replace('&#034', '"', $string);
    }

    /**
     * @return null
     */
    public function getTrafficLightLegendImage()
    {
        if ($this->trafficLightLegendImage === null)
        {
            $this->getLegendForTrafficLights();
        }

        return $this->trafficLightLegendImage;
    }

    /**
     * @return null
     */
    public function getTrafficLightImageMap()
    {
        if ($this->trafficLightImageMap === null)
        {
            $this->getLegendForTrafficLights();
        }

        return $this->trafficLightImageMap;
    }
    
    /**
     * Getter for trafficCodeColours.
     * 
     * @return array
     */
    public function getTrafficCodeColours()
    {
        if ($this->trafficCodeColours === null)
        {
            $this->trafficCodeColours = (new ReportModelFactory)->getMapper()->getTrafficCodeColours($this);
        }
        return $this->trafficCodeColours;
    }
    
    /**
     * Setter for trafficCodeColours.
     */
    public function setTrafficCodeColours(array $trafficCodeColours)
    {
        $this->trafficCodeColours = $trafficCodeColours;
    }
}