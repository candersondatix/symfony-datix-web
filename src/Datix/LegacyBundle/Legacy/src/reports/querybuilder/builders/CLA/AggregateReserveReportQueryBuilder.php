<?php

namespace src\reports\querybuilder\builders\CLA;

use src\reports\querybuilder\builders\AbstractReportQueryBuilder;
use src\reports\model\report\Report;
use src\reports\exceptions\ReportException;
use src\reports\specifications\ReportSpecificationFactory;
use src\framework\query\QueryFactory;
use src\system\database\DatabaseColumn;

/**
 * Builds the report query properties required for running aggregate reports across reserve values in Claims.
 */
class AggregateReserveReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var ReportSpecificationFactory
     */
    protected $rsf;
    
    /**
     * @var QueryFactory
     */
    protected $queryFactory;
    
    /**
     * @var array
     */
    protected $moduleDefs;
    
    public function __construct(ReportSpecificationFactory $rsf, QueryFactory $queryFactory, array $moduleDefs)
    {
        $this->rsf = $rsf;
        $this->queryFactory = $queryFactory;
        $this->moduleDefs = $moduleDefs;
    }
    
    /**
     * {@inheritdoc}
     * 
     */
    protected function doUpdate()
    {
        $notAggReserveOnSelDateSpec = $this->rsf->getAggregateReserveOnSelectedDateSpecification()->not();
         
        if ($notAggReserveOnSelDateSpec->isSatisfiedBy($this->packagedReport))
        {
            throw new ReportException('Invalid report specification for this query build step');
        }
        
        $table = $this->moduleDefs[$this->report->module]['VIEW'] ?: $this->moduleDefs[$this->report->module]['TABLE'];
        
        if ('' == $table)
        {
            throw new ReportException('Unable to determine module view/table');
        }
        
        $field = Report::COUNT_SUM == $this->report->count_style ? $this->report->count_style_field_sum : $this->report->count_style_field_average;
        $field = (new DatabaseColumn($field))->getColumn();
        
        if ('fin_indemnity_reserve' == $field)
        {
            $alias = 'fira';
            $crossApplyTable = 'fin_indemnity_reserve_audit';
        }
        else
        {
            $alias = 'fera';
            $crossApplyTable = 'fin_expenses_reserve_audit';
        }
        
        if (Report::COUNT_SUM == $this->report->count_style)
        {
            $this->query->select([['SUM(?) AS num', $alias.'.field_value']]);
        }
        else
        {
            $this->query->select([['COALESCE(CONVERT(DECIMAL(13,2), AVG(CONVERT(DECIMAL(13,2), ?))), 0) AS num', $alias.'.field_value']]);
        }
        
        list($crossApplyQuery, $crossApplyWhere, $crossApplyConditions) = $this->queryFactory->getQueryObjects();
        
        $crossApplyConditions->field($alias.'.cla_id')
                             ->eq($table.'.recordid', true);
        
        $orderDirection = 'ASC';
        
        if ('Y' != $this->report->count_style_field_initial_levels)
        {
            $dateTimeFrom = UserDateToSQLDate($this->report->count_style_field_calendar); // TODO remove reliance on this procedural function
            $dateTimeFrom = substr($dateTimeFrom, 0, -13);
            
            $crossApplyConditions->field('CAST(FLOOR(CAST('.$alias.'.datetime_from AS FLOAT)) AS DATETIME)')
                                 ->ltEq($dateTimeFrom.' 00:00:00.000');
            
            $orderDirection = 'DESC';
        }
        
        $crossApplyWhere->add($crossApplyConditions);

        $crossApplyQuery->select(['TOP(1) field_value'])
                        ->from($crossApplyTable.' '.$alias)
                        ->where($crossApplyWhere)
                        ->orderBy([[$alias.'.datetime_from', $orderDirection]]);

        $this->query->crossApply($alias, $crossApplyQuery);
    }
}