<?php

namespace src\reports\querybuilder\builders;

use src\reports\exceptions\ReportException;
use src\reports\model\listingreport\Listing;
use src\framework\registry\Registry;
use src\framework\query\QueryFactory;

/**
 * Modifies the query to support merging of repeated values when paging.
 * 
 * Involves changing the query to return the full results set for each distinct main record (or each distinct group, if grouping) displayed on a given page.
 */
class ListingMergeRepeatedPagingReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var Registry
     */
    protected $registry;
    
    /**
     * @var QueryFactory
     */
    protected $queryFactory;
    
    /**
     * @var \DatixDBQuery
     */
    protected $db;
    
    public function __construct(Registry $registry, QueryFactory $queryFactory, \DatixDBQuery $db)
    {
        $this->registry = $registry;
        $this->queryFactory = $queryFactory;
        $this->db = $db;
    }
    
    /**
     * {@inheritdoc}
     * 
     * @throws ReportException
     */
    protected function doUpdate()
    {
        if (!($this->report instanceof Listing))
        {
            throw new ReportException('Report type of Listing expected, '.get_class($packagedReport->report).' given.');
        }
        
        if ($this->report->returnAllRecords)
        {
            throw new ReportException('Incorrect listing report query build step loaded - paging not required.');
        }
        
        if (!$this->report->merge_repeated_values)
        {
            throw new ReportException('Incorrect listing report query build step loaded - repeated value merging not required.');
        }
        
        $moduleDefs = $this->registry->getModuleDefs();
        $table = $moduleDefs[$this->report->module]['VIEW'] ?: $moduleDefs[$this->report->module]['TABLE'];
        $usingGrouping = $this->report->getGroupBys()->count() > 0;

        // clone the report query and override the SELECT clause to retrieve just the recordids
        $pagedQuery = clone $this->query;
        $pagedQuery->select([['? AS '.$table.'_id', $table.'.recordid']], true);
        
        // select the group by field values so we know which groups are represented on this page
        if ($usingGrouping)
        {
            $selectFields = [];
            foreach ($this->report->getGroupBys() as $groupBy)
            {
                $field = $groupBy->getForm() == $table ? $groupBy->getField() : $groupBy->getForm().'.'.$groupBy->getAlias();
                
                $selectFields[] = ['? AS '.$groupBy->getAlias(), $field];
            }
            $pagedQuery->select($selectFields);
        }
        
        // execute the query to get the recordids and group/order field values
        list($sql, $params) = $this->queryFactory->getSqlWriter()->writeStatement($pagedQuery);
        $this->db->setSql($sql);
        $this->db->prepareAndExecute($params);
        $result = $this->db->fetchAll();
        
        // extract the recordids and determine which groups are represented on this page (if we're grouping)
        $recordids = [];
        $groups = [];
        
        foreach ($result as $row)
        {
            $recordids[] = $row[$table.'_id'];
            if ($usingGrouping)
            {
                $group = [];
                foreach ($this->report->groupBys as $groupBy)
                {
                    $field = $groupBy->getForm() == $table ? $groupBy->getField() : $groupBy->getForm().'.'.$groupBy->getAlias();
                    
                    $group[$field] = $row[$groupBy->getAlias()];
                }
                $groups[] = $group;
            }
        }
        
        $recordids = array_unique($recordids);
        $groups = array_unique($groups, SORT_REGULAR);
        
        // use the recordid list as a filter for the report query
        $where = $this->queryFactory->getWhere();
        $where->add(
            $this->queryFactory->getFieldCollection()->field($table.'.recordid')->in($recordids)
        );
        
        // add criteria for each group to the main query
        if ($usingGrouping)
        {
            $groupConditions = ['OR'];
            foreach ($groups as $group)
            {
                $groupWhere = $this->queryFactory->getWhere();
                foreach ($group as $field => $value)
                {
                    if ('' == $value)
                    {
                        $groupWhere->add(
                            'OR',
                            $this->queryFactory->getFieldCollection()->field($field)->isEmpty(),
                            $this->queryFactory->getFieldCollection()->field($field)->isNull()
                        );
                    }
                    else 
                    {
                        $groupWhere->add($this->queryFactory->getFieldCollection()->field($field)->eq($value));
                    }
                }
                $groupConditions[] = $groupWhere;
            }
            $where->addArray($groupConditions);
        }
        
        // we remove the page limit, since we're relying on our new conditions to restrict the result set for this page
        $this->query->where($where)
                    ->unsetLimit();
    }
}