<?php

namespace src\reports\querybuilder\builders;

use src\reports\exceptions\ReportException;
use src\reports\model\listingreport\Listing;
use src\framework\registry\Registry;

/**
 * Defines the SELECT clause for the listing report query.
 */
class ListingColumnReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
    
    /**
     * {@inheritdoc}
     * 
     * @throws ReportException
     */
    protected function doUpdate()
    {
        if (!($this->report instanceof Listing))
        {
            throw new ReportException('Report type of Listing expected, '.get_class($this->report).' given.');
        }
        
        $moduleDefs = $this->registry->getModuleDefs();
        $table = $moduleDefs[$this->report->module]['VIEW'] ?: $moduleDefs[$this->report->module]['TABLE'];
        
        foreach ($this->report->columns as $column)
        {
            // add field to select clause
            $field = $column->getForm() == $table ? $column->getField() : $column->getForm().'.'.$column->getAlias();
            
            $this->query->select([['? AS '.$column->getAlias(), $field]]);
        }
        
        // ensure the main record ID is always included in the output
        $this->query->select([['? AS '.$table.'_id', $table.'.recordid']]);
    }
}