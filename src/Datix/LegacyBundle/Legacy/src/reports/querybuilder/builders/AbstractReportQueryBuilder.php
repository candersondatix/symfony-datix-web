<?php

namespace src\reports\querybuilder\builders;

use src\reports\querybuilder\ReportQueryBuilder;
use src\reports\exceptions\ReportException;

abstract class AbstractReportQueryBuilder implements \SplObserver
{
    /**
     * The packaged report object.
     * 
     * @var PackagedReport
     */
    protected $packagedReport;
    
    /**
     * The report design object.
     * 
     * @var Report
     */
    protected $report;
    
    /**
     * The query object which is being built for the report.
     * 
     * @var ReportQuery
     */
    protected $query;

    /**
     * {@inheritdoc}
     * 
     * @throws ReportException If the subject type is incorrect.
     */
    public function update(\SplSubject $subject)
    {
        if (!($subject instanceof ReportQueryBuilder))
        {
            throw new ReportException('Subject of type src\\reports\\querybuilder\\ReportQueryBuilder expected, '.get_class($subject).' given.');
        }
        
        $this->packagedReport = $subject->getPackagedReport();
        $this->report = $this->packagedReport->report;
        $this->query = $subject->getQuery();
        
        $this->doUpdate();
    }
    
    /**
     * Concrete implementation of self::update().
     */
    abstract protected function doUpdate();
}