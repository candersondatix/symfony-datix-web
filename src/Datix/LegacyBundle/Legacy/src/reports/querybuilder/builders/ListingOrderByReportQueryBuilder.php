<?php

namespace src\reports\querybuilder\builders;

use src\reports\model\listingreport\Listing;
use src\reports\exceptions\ReportException;
use src\framework\registry\Registry;

/**
 * Defines the ORDER BY clause used for the listing report query.
 */
class ListingOrderByReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {
        if (!($this->report instanceof Listing))
        {
            throw new ReportException('Report type of Listing expected, '.get_class($packagedReport->report).' given.');
        }
        
        foreach ($this->report->orderBys as $orderBy)
        {
            $this->query->orderBy([[$orderBy->getField(), $orderBy->getDirection()]]);
        }
        
        // assign default ordering if no explicit order by fields set
        if (0 == $this->report->orderBys->count())
        {
            $moduleDefs = $this->registry->getModuleDefs();
            $module     = $this->report->module;
            $table      = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];
            
            $defaultOrderBy = $_SESSION['LIST'][$module]['ORDERBY'] ?:
                              $moduleDefs[$module]['DEFAULT_SORT_FIELD'] ?:
                              $moduleDefs[$module]['FIELD_NAMES']['NAME'] ?:
                              'recordid';
            
            $defaultOrderBy = $table.'.'.$defaultOrderBy;
            
            $this->query->orderBy([$defaultOrderBy]);
        }
    }
}