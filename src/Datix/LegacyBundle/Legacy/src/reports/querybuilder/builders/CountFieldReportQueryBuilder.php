<?php

namespace src\reports\querybuilder\builders;

use src\reports\exceptions\ReportException;
use src\reports\model\report\Report;

/**
 * Defines the SQL method (COUNT/SUM/AVG) to use for calculating report figures.
 */
class CountFieldReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * {@inheritdoc}
     * 
     * @throws ReportException
     */
    protected function doUpdate()
    {        
        switch ($this->report->count_style)
        {
            case Report::COUNT_NUM:
                $this->query->select(['COUNT(*) AS num']);
                break;
                
            case Report::COUNT_SUM:
                $this->query->select([['SUM(?) AS num', $this->report->count_style_field_sum]]);
                break;
                
            case Report::COUNT_AVE:
                $this->query->select([['COALESCE(CONVERT(DECIMAL(13,2), AVG(CONVERT(DECIMAL(13,2), ?))), 0) AS num', $this->report->count_style_field_average]]);
                break;
                
            default:
                throw new ReportException('Unable to determine report count type.');
        }
    }
}