<?php

namespace src\reports\querybuilder\builders;

/**
 * Replaces @prompt codes within the report query conditions with literal values.
 */
class ReplaceAtPromptsReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {
        $this->query->replaceAtPrompts($this->report->module);
    }
}