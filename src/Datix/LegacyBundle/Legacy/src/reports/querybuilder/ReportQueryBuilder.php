<?php

namespace src\reports\querybuilder;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Iterates through an attached collection of builders (observers) in order to build a Query object used to fetch report data.
 */
class ReportQueryBuilder implements IReportQueryBuilder, \SplSubject
{
    /**
     * @var \SplObjectStorage
     */
    private $builders;
    
    /**
     * @var ReportQuery
     */
    protected $query;
    
    /**
     * @var PackagedReport
     */
    protected $packagedReport;
    
    public function __construct(\SplObjectStorage $builders)
    {
        $this->builders = $builders;
    }
    
    /**
     * Builds and returns the report query for the given packaged report.
     * 
     * @param PackagedReport $packagedReport
     * 
     * @return ReportQuery
     */
    public function build(PackagedReport $packagedReport)
    {
        $this->packagedReport = clone $packagedReport;
        $this->query = $this->packagedReport->query->createQuery();
        $this->notify();
        
        return $this->query;
    }
    
    /**
     * Getter for query.
     * 
     * @return ReportQuery
     */
    public function getQuery()
    {
        return $this->query;
    }
    
    /**
     * Getter for packagedReport.
     */
    public function getPackagedReport()
    {
        return $this->packagedReport;
    }
    
    /**
     * {@inheritdoc}
     */
    public function attach(\SplObserver $observer)
    {
        $this->builders->attach($observer);
    }

    /**
     * {@inheritdoc}
     */
    public function detach(\SplObserver $observer)
    {
        $this->builders->detach($observer);
    }

    /**
     * {@inheritdoc}
     */
    public function notify()
    {
        foreach ($this->builders as $builder)
        {
            $builder->update($this);
        }
    }
}