<?php
namespace src\reports\observers;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\framework\model\Entity;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\model\report\Report;

class ReportDeletionCleaner implements Observer
{
    /**
     * {@inheritdoc}
     *
     * Removes orphan data when deleting a base report
     *
     * @throws InvalidArgumentException If the subject is not a Profile.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof Report))
        {
            throw new \InvalidArgumentException('Object of type Report expected, '.get_class($subject).' given.');
        }

        if ($event != Entity::EVENT_DELETE)
        {
            // this observer only listens to delete events
            return;
        }

        //delete all linked packaged reports
        foreach ($subject->packagedReports as $packagedReport)
        {
            (new PackagedReportModelFactory)->getMapper()->delete($packagedReport);
        }
    }
}