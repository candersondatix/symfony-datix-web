<?php
namespace src\reports\controllers;

use src\reports\model\report\Report;
use src\reports\ReportEngine;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\FusionChartFactory;

class GraphicalReportWriter extends ReportWriter
{
    /**
     * {@inheritdoc}
     */
    public function writeReport()
    {
        global $ClientFolder;

        $packagedReport = $this->request->getParameter('packagedReport');
        $engine = $this->request->getParameter('engine');
        $context =  $this->request->getParameter('context') ?: self::FULL_SCREEN;

        $this->addJs('thirdpartylibs/fusioncharts/FusionCharts.js');

        // We need to pass the recordid here to be able to display multiple reports on the dashboard
        $factory = new FusionChartFactory($packagedReport->recordid);
        $chart      = $factory->getChart($packagedReport, $engine, $context);
        $report     = $packagedReport->report;
        $reportType = get_class($report);

        $this->sendToJs('fusionChartId', $factory->getChartId(), false);

        if ($reportType == 'src\\reports\\model\\report\\TrafficLightChart' && $context == self::FULL_SCREEN)
        {
            $height = '300px';

            $tempPath = GetParm('TEMP_DIR', $ClientFolder);

            $tmpFile = uniqid('pngfile');

            file_put_contents($tempPath.'/'.$tmpFile.'.png', $report->getTrafficLightLegendImage());
            $trafficLightImageMap = $report->getTrafficLightImageMap();
        }
        else
        {
            if ($context == ReportWriter::DASHBOARD)
            {
                if($report->getReportType() == Report::TRAFFIC)
                {
                    $height = '130px';
                }
                else
                {
                    $height = '300px';
                }
            }
            else
            {
                $height = '600px';
            }
        }

        // Remove the columns added for this particular case otherwise te columns field on the report designer will display it
        if ($report->count_style_field_initial_levels == 'Y')
        {
            $report->columns = '';
        }

        $this->response->build('src/reports/views/WriteGraphicalReport.php', array(
            'chart'                => $chart,
            'height'               => $height,
        	'report'               => $report,
            'reportType'           => $reportType,
            'tmpFile'              => $tmpFile,
            'ClientFolder'         => $ClientFolder,
            'trafficLightImageMap' => $trafficLightImageMap,
            'context'              => $context
        ));
        
        return $this->response;
    }
}
