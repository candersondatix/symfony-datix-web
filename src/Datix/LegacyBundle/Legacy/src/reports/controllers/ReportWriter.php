<?php
namespace src\reports\controllers;

use src\framework\controller\Controller;
use src\reports\ReportEngine;
use src\reports\model\packagedreport\PackagedReport;

abstract class ReportWriter extends Controller
{
    // context constants
    const FULL_SCREEN  = 1;
    const DASHBOARD    = 2;
    const PRINTED      = 4;
    const EXPORT_EXCEL = 5;
    const EXPORT_PDF   = 6;
    const EXPORT_CSV   = 7;

    /**
     * Constructs the report output.
     * 
     * @param PackagedReport $packagedreport The report object.
     * @param ReportEngine   $engine         The object responsible for extracting the report data.
     * @param int            $context        The context in which the report is being rendered.
     * 
     * @return Response
     */
    abstract public function writeReport();
}