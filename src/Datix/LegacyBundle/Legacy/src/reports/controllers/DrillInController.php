<?php

namespace src\reports\controllers;

use src\framework\controller\Controller;
use src\framework\query\FieldCollection;
use src\framework\query\Query;
use src\framework\query\Where;
use src\reports\exceptions\ReportException;
use src\reports\specifications\ReportSpecificationFactory;
use src\system\database\FieldInterface;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\model\report\Crosstab;
use src\reports\model\report\Report;

/**
 * Class DrillInController
 *
 * Responsible for redirecting the drill in feature to the list
 *
 * @package src\reports\controllers
 */
class DrillInController extends Controller
{
    /**
     * Controls the action of drilling into a report.
     * 
     * @throws ReportException If the current report object isn't set in the session.
     */
    public function drillIn()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        if (!($_SESSION['CurrentReport'] instanceof PackagedReport))
        {
            throw new ReportException('Unable to access current report instance.');
        }

        if ($this->request->getParameter('recordid') != '')
        {
            $packagedReport = (new PackagedReportModelFactory())->getMapper()->find($this->request->getParameter('recordid'));
        }
        else
        {
            $packagedReport = $_SESSION['CurrentReport'];
        }

        // Remove columns from initial levels report type because it's a dummy field and will cause errors building the query
        if ($packagedReport->report->count_style_field_initial_levels == 'Y')
        {
            $packagedReport->report->columns = '';
        }
        
        $Module            = $packagedReport->report->module;
        $Rows              = $packagedReport->report->rows;
        $RowsValue         = urldecode($this->request->getParameter('rowsvalue'));
        $RowsDateOption    = $packagedReport->report->rows_date_option;
        $Columns           = $packagedReport->report->columns;
        $ColumnsValue      = urldecode($this->request->getParameter('columnsvalue'));
        $ColumnsDateOption = $packagedReport->report->columns_date_option;
        $From              = ($packagedReport->report instanceof Crosstab ? '&fromcrosstab=1' : '&from_report=1');
        $IsOverdue         = $packagedReport->query->is_overdue;
        $rsf               = new ReportSpecificationFactory();

        $queryBuilder = (new \src\reports\model\packagedreport\PackagedReportModelFactory)->getReportQueryBuilder($_SESSION['CurrentReport'], $rsf, $this->registry);//Even if I'm the last change on this line, talk to Chris it's his fault.
        $query = $queryBuilder->build($_SESSION['CurrentReport']);
        
        $query->setAutoCreateExtraFieldJoins(false); // supress the creation of additional joins when appending additional conditions (below), since the query builder should have already done this

        $this->addConditions('rows', $RowsValue, $RowsDateOption, $query, $packagedReport->report);

        if ($Columns != '')
        {
            $this->addConditions('columns', $ColumnsValue, $ColumnsDateOption, $query, $packagedReport->report);
        }
        
        // Remove other fields from query->fields
        $table = ($ModuleDefs[$Module]['VIEW'] ?: $ModuleDefs[$Module]['TABLE']);
        $query->select(array($table.'.recordid'), true);

        // Remove group by statement - not needed when listing
        $query->groupBy(array(), true);

        $_SESSION[$Module]['DRILL_QUERY'] = $query;


        //In case we want to save the listing after drilling in, we need to have a saved query object available in the session too.
        //TODO: this might be unecessary if we can use the query object to generate a saved query, but this seemed safer at the time.

        $query = clone $packagedReport->query;

        if (null === ($Where = $query->where))
        {
            $query->where = new Where();
        }

        $this->addConditionsToSavedQuery($Rows, $RowsValue, $RowsDateOption, $query->where, $packagedReport->report);

        if ($Columns != '')
        {
            $this->addConditionsToSavedQuery($Columns, $ColumnsValue, $ColumnsDateOption, $query->where, $packagedReport->report);
        }

        $_SESSION[$Module]['DRILL_SAVED_QUERY'] = $query;

        $RedirectUrl = 'app.php?action=list&module='.$Module.$From.'&rows='.$Rows.'&columns='.$Columns.
            '&listtype=search'.( $IsOverdue ? '&overdue=1' : '' );
        
        $this->redirect($RedirectUrl);
    }
    
    /**
     * Adds the drill-in WHERE clause conditions.
     * 
     * @param string       $axis       rows/columns
     * @param string       $value      The field value.
     * @param string       $dateOption The date option (if any).
     * @param Query        $query      The report query.
     * @param Report       $report     The report design.
     */
    protected function addConditions($axis, $value, $dateOption, Query $query, Report $report)
    {
        $fieldDefs = $this->registry->getFieldDefs();
        $field = $report->$axis;

        $where = new Where();

        if ($value == 'DTX_TOTAL')
        {
            if ($report->null_values !== true)
            {
                $fc = (new FieldCollection)->field($field)->notNull();
                
                if (!$fieldDefs[$field]->isNumeric())
                {
                    $fc->notEmpty();
                }

                $where->add($fc);
            }
        }
        else
        {
            if ($fieldDefs[$field]->getType() == FieldInterface::DATE)
            {
                (new PackagedReportModelFactory)->getDatesEngine()->setDateOptionWhere($field, $dateOption, $value, $where);
            }
            else
            {
                $this->getFieldCollection($field, $value, $where);
            }
        }
        
        // we suppress the automatic addition of query table definitions to prevent unwanted joins being created when the query is written -
        // in theory the joins required should already have been created by the report query builder
        $query->where($where, Query::WHERE_NO_TABLES);
        
        // we need the field name as it appears in the select clause (minus the alias), since it may have changed versus the axis value on the report design
        foreach ($query->getFields() as $selectField)
        {
            if (false !== strpos($selectField[0], ' AS '.$axis))
            {
                $axisSelectField = is_array($selectField[1]) ? $selectField[1][0] : $selectField[1];
                
                if ($axisSelectField != $field)
                {
                    $where->convertFieldName($field, $axisSelectField);
                }
                
                break;
            }
        }
    }

    /**
     * Adds the drill-in WHERE clause conditions.
     *
     * @param string       $field      The field name.
     * @param string       $value      The field value.
     * @param string       $dateOption The date option (if any).
     * @param string|Where $where      The current WHERE clause.
     *
     * @return string|Where
     */
    protected function addConditionsToSavedQuery($field, $value, $dateOption, $where, Report $report)
    {
        $fieldDefs = $this->registry->getFieldDefs();

        if ($value == 'DTX_TOTAL')
        {
            if ($report->null_values !== true)
            {
                $fc = (new FieldCollection)->field($field)->notNull();

                if (!$fieldDefs[$field]->isNumeric())
                {
                    $fc->notEmpty();
                }

                $where->add($fc);
            }
        }
        else
        {
            if ($fieldDefs[$field]->getType() == FieldInterface::DATE)
            {
                (new PackagedReportModelFactory)->getDatesEngine()->setDateOptionWhere($field, $dateOption, $value, $where);
            }
            else
            {
                $this->getFieldCollection($field, $value, $where);
            }
        }
    }

    
    /**
     * Get FieldCollection based on field type and date option if it is neccessary.
     * 
     * @author gszucs
     * @param string $field
     * @param string $fieldValue
     * @param Where $where
     * @return FieldCollection
     */
    protected function getFieldCollection($field, $fieldValue, Where $where)
    {
    	$fieldDefs = $this->registry->getFieldDefs();
    	
    	if ($fieldValue === '')
    	{
    		$params = array('OR', (new FieldCollection())->field($field)->isNull());
    		if (!$fieldDefs[$field]->isNumeric())
    		{
    			$params[] = (new FieldCollection())->field($field)->isEmpty();
    		}
    		$where->addArray($params);
    	}
    	else
    	{
    	    $where->add(
    	        (new FieldCollection)->field($field)->eq($fieldValue)
    	    );
    	}
    }
}