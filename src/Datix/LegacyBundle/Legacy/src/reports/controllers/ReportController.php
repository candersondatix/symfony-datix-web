<?php
namespace src\reports\controllers;

use src\framework\controller\Controller;
use src\framework\query\FieldCollection;
use src\framework\query\QueryFactory;
use src\framework\query\SqlWriter;
use src\framework\query\Where;
use src\framework\registry\Registry;
use src\reports\exceptions\ReportException;
use src\reports\model\packagedreport\PackagedReportFactory;
use src\reports\model\report\Report;
use src\reports\model\report\Crosstab;
use src\reports\model\report\GaugeChart;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\SqlServerDatesEngine;
use src\system\database\FieldInterface;
use src\users\model\User;

class ReportController extends Controller
{
    /**
     * Renders a report.
     */
    public function displayReport()
    {
        $factory    = new PackagedReportModelFactory();
        $isCrosstab = $this->request->getParameter('isCrosstab');

        $packagedReport = (new PackagedReportFactory)->createCurrentPackagedReportFromRequest($this->request);

        $context = ($this->request->getParameter('context') != '' ? $this->request->getParameter('context') : ReportWriter::FULL_SCREEN);

        // TODO: review positioning of this as I'm not sure it's the best place for it - AW
        $_SESSION['CurrentReport'] = $packagedReport;

        if ($isCrosstab)
        {
            $_SESSION['crosstab_rows_field'] = $packagedReport->report->rows;
            $_SESSION['crosstab_columns_field'] = $packagedReport->report->columns;
        }
        
        if (null !== ($currentUser = $_SESSION['CurrentUser']))
        {
            $login = $currentUser->login;
        }
        
        $this->registry->getLogger()->logReporting($login.' has started running packaged report ID '.$packagedReport->recordid.' (name: "'.$packagedReport->name.'"; type: '.get_class($packagedReport->report).'; context: '.$context.')');

        $this->response = $this->call($factory->getReportWriter($packagedReport->report), 'writeReport', array('packagedReport' => $packagedReport, 'engine' => $factory->getReportEngine($packagedReport), 'context' => $context));
        
        $this->registry->getLogger()->logReporting('Packaged report ID '.$packagedReport->recordid.' ("'.$packagedReport->name.'") for '.$login.' is complete.');
    }

    /**
     * Takes a report (calculated based on the request) and exports it to either pdf or excel.
     * @throws \src\reports\exceptions\ReportException
     */
    public function exportReport()
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $ClientFolder = $this->registry->getClientFolder();

        $report = (new PackagedReportFactory)->createCurrentPackagedReportFromRequest($this->request);

        if (!($report instanceof PackagedReport))
        {
            throw new ReportException('Cannot work out which report to export.');
        }
        
        if (null !== ($currentUser = $_SESSION['CurrentUser']))
        {
            $login = $currentUser->login;
        }
        
        $this->registry->getLogger()->logReporting($login.' has started running packaged report ID '.$report->recordid.' (name: "'.$report->name.'"; type: '.get_class($report->report).'; context: '.$this->request->getParameter('format').')');

        switch ($this->request->getParameter('format'))
        {
            case 'pdf':
                //pdf exports for fusion charts are done using js, so we should only be dealing with crosstabs and listings here.
                if($report->report->getReportType() == Report::LISTING)
                {
                    $factory = new PackagedReportModelFactory();

                    $this->response = $this->call($factory->getReportWriter($report->report), 'writeReport', array('packagedReport' => $report, 'engine' => $factory->getReportEngine($report), 'context' => ReportWriter::EXPORT_PDF));
                }
                elseif ($report->report instanceof Crosstab)
                {
                    $factory = new PackagedReportModelFactory();
                    $engine = $factory->getReportEngine($report);

                    $this->response->build('src/reports/views/WriteCrosstabReport.php', array(
                        'data' => $engine->getData($report)
                    ));
                }
                else
                {
                    throw new ReportException('Incorrect report type. Crosstab or Listing expected, '.get_class($report->report).' provided.');
                }
                
                switch ($this->request->getParameter('orientation'))
                {
                	case 'landscape':
                		$orientation = 'L';
                		break;
                	case 'portrait':
                	default:
                		$orientation = 'P';
                		break;
                }
                
                $paperSize = $this->request->getParameter('papersize') ?: 'a4';

                $mode = ($report->report->getReportType() == Report::LISTING ? 'listing' : 'crosstab');

                define('FPDF_FONTPATH','font/');
                require_once __DIR__.'/../../../export/PDFTableWrapper.php';
                
                $pdf = new \PDFTableWrapper($orientation,'mm',$paperSize);
                $pdf->SetHeaderTitle($report->name);
                $pdf->AliasNbPages();
                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetMargins(20,20,20);
                $html = $report->report->ParseHTMLTableForExport($this->response, 'pdf', $pdf);
                $pdf = OutputPDFTable($html, $mode, $orientation, $paperSize, $pdf);

                if (\UnicodeString::strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false)
                {
                    // fix "file not found" issue when opening file directly from IE6
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                }

                $pdf->Output('DatixWebReport.pdf', 'D');
                
                $this->registry->getLogger()->logReporting('Packaged report ID '.$report->recordid.' ("'.$report->name.'") for '.$login.' is complete.');
                
                obExit();
                break;

            case 'excel':
                if($report->report->getReportType() == Report::LISTING)
                {
                    $factory = new PackagedReportModelFactory();

                    $this->response = $this->call($factory->getReportWriter($report->report), 'writeReport', array('packagedReport' => $report, 'engine' => $factory->getReportEngine($report), 'context' => ReportWriter::EXPORT_EXCEL));
                }
                else
                {
                    /** PHPExcel */
                    require_once('thirdpartylibs/PHPExcel/PHPExcel.php');

                    $factory = new PackagedReportModelFactory();

                    $engine = $factory->getReportEngine($report);

                    header("Pragma: public");
                    header("Cache-Control: max-age=0");
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment; filename="DatixWebReport.xlsx"');
                    header("Content-Encoding: none");

                    $excelReport = $report->getExcelObject($engine);

                    $objWriter = \PHPExcel_IOFactory::createWriter($excelReport, 'Excel2007');
                    $objWriter->setIncludeCharts(TRUE);
                    $objWriter->save('php://output');
                    
                    $this->registry->getLogger()->logReporting('Packaged report ID '.$report->recordid.' ("'.$report->name.'") for '.$login.' is complete.');
                    
                    exit;
                }
                break;
            case 'csv':
                
                $this->response = $this->call('src\\reports\\controllers\\ListingReportWriter', 'writeReport', [
                    'packagedReport' => $report,
                    'engine'         => (new PackagedReportModelFactory)->getReportEngine($report),
                    'context'        => ReportWriter::EXPORT_CSV
                ]);
                
                $this->registry->getLogger()->logReporting('Packaged report ID '.$report->recordid.' ("'.$report->name.'") for '.$login.' is complete.');
        }
    }

    public function gaugedesignsection()
    {
        $this->addCss('js_functions/colorpicker/css/colorpicker.css');
        $this->addJs(array(
            'js_functions/colorpicker/js/colorpicker.js',
            "<script type=\"text/javascript\">
                jQuery(document).ready(function() {

                    if(globals.deviceDetect.isTablet) {

                        jQuery('.colour-picker').prop('disabled', true);
                    }
                    else {

                        jQuery('.colour-picker').ColorPicker({
                            flat: false,
                            onSubmit: function(hsb, hex, rgb, el)
                            {
                                jQuery(el).val(hex);
                                jQuery(el).ColorPickerHide();
                                jQuery(el).css({backgroundColor : '#'+hex});
                            },
                            onBeforeShow: function ()
                            {
                                jQuery(this).ColorPickerSetColor(this.value);
                            }
                        })
                        .bind('keyup', function()
                        {
                            jQuery(this).ColorPickerSetColor(this.value);
                        });
                    }
                });
            </script>"
        ));

        $this->response->build('src/reports/views/GaugeDesignSection.php', array('data' => $this->request->getParameter('data')));
    }
    
    /**
     * Constructs the popup used when adding to my reports/dashboard.
     */
    public function addReportPrompt()
    {
        $packagedReport = $_SESSION['CurrentReport'];
        
        if (!($packagedReport instanceof PackagedReport))
        {
            header('HTTP/1.1 500 Internal Server Error');
            die(_tk('add_report_prompt_error'));
        }
        
        $name = $packagedReport->report instanceof GaugeChart ? $packagedReport->report->getTitle() : $this->request->getParameter('rep_name');
        
        require_once 'Source/libs/FormClasses.php';

        if ($this->request->getParameter('dashboard') == 'true')
        {
            require_once 'Source/dashboards/dashboard.php';
    
            $dashboardOptions = array('NEW' => 'Create new dashboard');
            $defaultDashboard = GetDefaultDashboard();
    
            if ($this->request->getParameter('rep_id'))
            {
                $ValidDashboards = (new PackagedReportModelFactory)->getMapper()->getTargetDashboards($this->request->getParameter('rep_id'));
            }
    
            foreach (GetUserDashboards(array('own_only' => true)) as $DashboardDetails)
            {
                if (!is_array($ValidDashboards) || in_array($DashboardDetails['recordid'], $ValidDashboards))
                {
                    $dashboardOptions[$DashboardDetails['recordid']] = $DashboardDetails['dash_name'];
                }
            }
    
            $dashboardField = \Forms_SelectFieldFactory::createSelectField('target_dashboard', '', ($defaultDashboard == 0 || !isset($dashboardOptions[$defaultDashboard])? 'NEW' : $defaultDashboard), '');
            $dashboardField->setCustomCodes($dashboardOptions);
            $dashboardField->setSuppressCodeDisplay();
        }
        else if ($packagedReport->query->hasAtPrompt())
        {
            // display option to choose between saving the report query with @prompt codes or literal values
            $promptField = \Forms_SelectFieldFactory::createSelectField('use_values', '', 'SAVE', '');
            $promptField->setCustomCodes(array('SAVE' => _tk('atprompt_save_literal'), 'PROMPT' => _tk('atprompt_save_codes')));
            $promptField->setSuppressCodeDisplay();
        }
        else
        {
            // no dashboard and no @prompt?
            // we don't need a modal then
            return '';
        }

        $this->response->build('src/reports/views/AddReportPrompt.php', array(
            'name'        => $name,
            'promptField'    => $promptField,
            'dashboardField' => $dashboardField
        ));
    }
    
    /**
     * Saves the current report to My Reports.
     */
    public function addToMyReports()
    {
        $existingReport = false;
        
        if ($this->request->getParameter('as_new') == '1')
        {
            //save as new clicked, means we want to use the session report, but want to force it through insert() rather than update()
            $packagedReport = clone $_SESSION['CurrentReport'];
            $packagedReport->recordid = null;
            $packagedReport->name = $this->request->getParameter('title');

            // The title of a base listing report should not be altered when saving a new packaged report.
            if ($packagedReport->report->getReportType() != Report::LISTING)
            {
                $packagedReport->report->title = $this->request->getParameter('title');
            }
        }
        else if ($this->request->getParameter('recordid'))
        {
            // if we're passing a recordid, then the user has clicked "save changes" and so we want to update an existing record.
            $packagedReport = (new PackagedReportFactory)->createPackagedReportFromSessionBasedOnReportId($this->request->getParameter('recordid'));
            $existingReport = true;
        }
        else
        {
            $packagedReport = $_SESSION['CurrentReport'];
            $packagedReport->name = $this->request->getParameter('title');
            $packagedReport->report->title = $this->request->getParameter('title');
        }

        $currentUser = $_SESSION['CurrentUser'];
        
        if (!($packagedReport instanceof PackagedReport))
        {
            header('HTTP/1.1 500 Internal Server Error');
            die(_tk('add_report_prompt_error'));
        }
        
        if (!($currentUser instanceof User))
        {
            header('HTTP/1.1 500 Internal Server Error');
            die(_tk('current_user_error'));
        }

        if ($this->request->getParameter('use_values') == 'SAVE')
        {
            // save literal values, rather than @prompt codes, in the packaged report query
            $packagedReport->query->replaceAtPrompts($packagedReport->report->module);
        }
        
        if (!((int) $packagedReport->report->recordid > 0))
        {
            // set the current user as the "owner" so only they can access the base report
            $packagedReport->report->owner = $currentUser->initials;
        }
        
        $mapper = (new PackagedReportModelFactory)->getMapper();
        
        try 
        {
            $mapper->save($packagedReport);
            
            if (!$existingReport)
            {
                // restrict access to this user only when creating a new packaged report
                $mapper->saveUsers($packagedReport->recordid, [$currentUser->recordid]);
            }
        }
        catch (\Exception $e)
        {
            $this->registry->getLogger()->logEmergency($e->getMessage());
            
            header('HTTP/1.1 500 Internal Server Error');
            
            $message = $GLOBALS['dtxdebug'] ? $e->getMessage() : _tk('my_reports_save_error');
            die($message);
        }
    }
    
    /**
     * Serves png files that have been generated on-the-fly.
     * 
     * Currently only used for traffic light report legend.
     * 
     * @throws Exception If filename contains non-alphanumeric characters.
     */
    function getAndDeletePNGFile()
    {
        global $ClientFolder;
        
        if (preg_match_all('/[^a-zA-Z0-9]/', $_GET['pngfile']))
        {
            throw new \Exception('Invalid filename');
        }
    
        $tempPath = trim(GetParm('TEMP_DIR', $ClientFolder));
        $fullPath = $tempPath.'/'.\Sanitize::SanitizeString($_GET['pngfile']).'.png';
    
        // serve file directly
        if (file_exists($fullPath))
        {
            header('Content-Type: image/png');
            readfile($fullPath);
            unlink($fullPath);
            exit();
        }
        else
        {
            header('HTTP/1.0 404 Not Found');
            $this->response->setBody('<h1>404 PNG File not found</h1>');
        }
    }
    
    /**
     * Handles the "open in excel" function (from both the report design form and my reports).
     */
    public function openInExcel()
    {
        $packagedReport = (new PackagedReportModelFactory)->getEntityFactory()->createCurrentPackagedReportFromRequest($this->request);
        
        $this->call('src\\reports\\controllers\\ReportDesignFormController', 'setAtPromptValues', [
            'packagedReport' => $packagedReport
        ]);
        
        $this->response = $this->call('src\\reports\\controllers\\ReportController', 'exportReport', [
            'format'      => $this->get('reportoutputformat'),
            'orientation' => $this->get('page_orientation') ?: $this->get('orientation'),
            'papersize'   => $this->get('papersize'),
            'module'      => $packagedReport->report->module
        ]);
    }
}
