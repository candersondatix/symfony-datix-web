<?php
namespace src\reports\controllers;

use SplObjectStorage;
use src\reports\model\packagedreport\PackagedReport;

use src\framework\controller\TemplateController;
use src\framework\query\QueryFactory;
use src\framework\registry\Registry;
use src\reports\querybuilder\ReportQueryBuilder;
use src\reports\RiskTrackerReportEngine;
use src\reports\model\report\RiskTrackerReport;
use src\reports\FusionChartFactory;
use src\reports\SqlServerDatesEngine;

class RiskTrackerTemplateController extends TemplateController
{
    /**
     * On-the-fly report designer.
     */
    public function riskTrackerReport()
    {
        $this->hasMenu = false;

        $this->addJs(array(
                'thirdpartylibs/fusioncharts/FusionCharts.js',
                'src/reports/js/reports.js',
            )
        );

        $report = new RiskTrackerReport(
            $this->request->getParameter('recordid'),
            $this->request->getParameter('reporton'),
            $this->request->getParameter('startdate'),
            $this->request->getParameter('enddate')
        );
        
        $packagedReport = new PackagedReport();
        $packagedReport->report = $report;
        
        $_SESSION['CurrentReport'] = $packagedReport;

		$query = new \DatixDBQuery();
		$factory = new QueryFactory();
		$registry = Registry::getInstance();
		$queryBuilder = new ReportQueryBuilder(new SplObjectStorage());
		$datesEngine = new SqlServerDatesEngine(new \DatixDBQuery(), new QueryFactory(), Registry::getInstance());

		$engine = new RiskTrackerReportEngine($query, $factory, $registry, $queryBuilder, $datesEngine);

        if ($this->request->getParameter('format') == 'excel')
        {
			//pdf exports for fusion charts are done using js, so we need to deal only with excel
			/** PHPExcel */
			require_once('thirdpartylibs/PHPExcel/PHPExcel.php');

			header("Pragma: public");
			header("Cache-Control: max-age=0");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="DatixWebReport.xlsx"');
			header("Content-Encoding: none");

			$excelReport = $report->getExcelObject($engine->getData($packagedReport));

			$objWriter = \PHPExcel_IOFactory::createWriter($excelReport, 'Excel2007');
			$objWriter->setIncludeCharts(TRUE);
			$objWriter->save('php://output');
			exit;
        }
        else
        {
			// This is needed to be able to export to PDF
			$fcFactory = new FusionChartFactory($packagedReport->recordid);
			$this->sendToJs('fusionChartId', $fcFactory->getChartId(), false);

	        $chart = $fcFactory->getChart($packagedReport, $engine, ReportWriter::FULL_SCREEN);
	
	        $this->response->build('src/reports/views/WriteRiskTrackerReport.php', array(
	            'chart' => $chart,
	        	'type'  => $packagedReport->report->getReportType(),
	        	'format'  => $this->request->getParameter('format'),
	        	'export'  => $this->request->getParameter('export'),
	            'recordid' => $this->request->getParameter('recordid'),
	        	'reporton' => $this->request->getParameter('reporton'),
	        	'startdate' => $this->request->getParameter('startdate'),
	        	'enddate' => $this->request->getParameter('enddate')
	        ));
        }
    }
}