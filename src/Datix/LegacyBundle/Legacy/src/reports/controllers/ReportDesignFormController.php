<?php
namespace src\reports\controllers;

use src\framework\controller\Controller;
use src\framework\query\Field;
use src\framework\query\Query;
use src\framework\model\exceptions\UndefinedPropertyException;
use src\reports\exceptions\ReportException;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\packagedreport\PackagedReportFactory;
use src\reports\model\report\BarChart;
use src\reports\model\report\Crosstab;
use src\reports\model\report\Report;
use src\reports\model\report\ReportModelFactory;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\system\database\extrafield\ExtraField;
use src\system\database\field\FieldModelFactory;
use src\system\database\FieldInterface;
use src\system\database\field\CodeField;
use src\system\database\code\Code;
use src\reports\model\field\ReportField;
use src\reports\model\report\PieChart;
use src\savedqueries\model\field\SavedQueryField;
use src\savedqueries\model\SavedQueryModelFactory;
use src\framework\query\Where;

class ReportDesignFormController extends Controller
{
    /**
     * Constructs and displays the form used to design a report.
     */
    public function reportDesignerForm()
    {
        // CSS
        $this->addCss(array(
                'src/reports/css/reportDesigner.css',
                'src/reports/css/reports.css',
                array('file' => 'src/reports/css/report.print.css', 'attrs' => 'media="print"'),
                'js_functions/colorpicker/css/colorpicker.css',
                'js_functions/qTip/jquery.qtip.min.css'
            )
        );

        //JS
        $this->addJs(array(
                'js_functions/jquery/jquery.ba-resize.js',
                'js_functions/colorpicker/js/colorpicker.js',
                'thirdpartylibs/fusioncharts/FusionCharts.js',
                'js_functions/qTip/jquery.qtip.min.js',
                'src/reports/js/ReportDesigner.js',
                'src/reports/js/reports.js',
                'js_functions/ie7.js'
            )
        );

        $moduleDefs = $this->registry->getModuleDefs();

        // need to know whether we are calling this form from the "report designer" or "reports administration".
        $context = $this->request->getParameter('context');

        // Need to know if we have to show excluded fields/forms or to hide them.
        if ($this->request->getParameter('show_excluded'))
        {
            $showExcluded = $this->request->getParameter('show_excluded');
        }
        elseif ($_SESSION['reports_administration:show_excluded'])
        {
            $showExcluded = $_SESSION['reports_administration:show_excluded'];
        }
        else
        {
            $showExcluded = '0';
        }

        $this->module = $this->request->getParameter('module');

        if ($this->request->getParameter('blank') && $this->displayCurrentCriteriaOption($this->request->getParameter('module')))
        {
            // ensure the current criteria is used to build the packaged report's saved query object when on a new report designer form
            $this->request->setParameter('query', '0');
            $query = '0';
        }
        
        if( isset( $_SESSION[$this->request->getParameter('module')]['LASTQUERYID'] ) )
        {
        	$this->request->setParameter('query',  $_SESSION[$this->request->getParameter('module')]['LASTQUERYID'] );
        	$query =  $_SESSION[$this->request->getParameter('module')]['LASTQUERYID'];
        }

        $packagedReport = (new PackagedReportFactory)->createCurrentPackagedReportFromRequest($this->request);

        $module = $packagedReport->report->module;
        
        if ($packagedReport->query !== null)
        {
            if ($packagedReport->query->recordid == '' && $packagedReport->query->old_query_id == '' )
            {
                // this is required so that the queries dropdown still displays [Current criteria] once the report is run
                $query = '0';
            }
            
            if ($query != '0')
            {
                // the queries dropdown uses "old" saved query IDs
                $query = $packagedReport->query->old_query_id;
            }
            
            if ($query === '0' && $this->request->getParameter('old_query_id') )
            {
            	$query = $this->request->getParameter('old_query_id');
            }
            else if( ! isset($query) && $this->request->getParameter('old_query_id') === '')
            {
                $query = '-1';
            }
            
            if (!isset($query))
            {
                // refer to the "new" query id, since there's no "old" equivalent
                $newQueryID = $packagedReport->query->recordid;
                $query = '-1';
            }
            
            $hasAtPrompt = $packagedReport->query->hasAtPrompt();
        }

        $report = $packagedReport->report;

        $type = $report->getReportType();

        // Check if the selected fields are date fields and use this to set state below
        $fieldDefs = $this->registry->getFieldDefs();

        if ($report->rows && $fieldDefs[$report->rows]->getType() == FieldInterface::DATE)
        {
            $rowsIsDate = true;
        }

        if ($fieldDefs[$report->columns] !== null && $fieldDefs[$report->columns]->getType() == FieldInterface::DATE)
        {
            $columnsIsDate = true;
        }
        
        $showPrompt = false;
        if ($this->request->getParameter('from_my_reports') == '1' && $hasAtPrompt)
        {
            $designerState = 'open';
            $showPrompt = true;
        }
        else
        {
            $designerState = $this->request->getParameter('designer_state') ? $this->request->getParameter('designer_state') : $_SESSION['designer_state']?: 'open';
        }

        $reserveFields = array(
            'claims_main.fin_indemnity_reserve',
            'claims_main.fin_expenses_reserve',
            'claims_main.fin_calc_reserve_1',
            'claims_main.fin_calc_reserve_2'
        );

        if(in_array($report->count_style_field_sum, $reserveFields) || in_array($report->count_style_field_average, $reserveFields))
        {
            $isReserve = true;
        }
        else
        {
            $isReserve = false;
        }

        // Array of sections states to persist when page reloads
        $states = array(
            'designer_state'                         => $designerState,
            'scroll_pos'                             => $this->request->getParameter('scroll_pos') ? $this->request->getParameter('scroll_pos') : 0,
            'title_toggle_state'                     => $this->request->getParameter('title_toggle_state') ? $this->request->getParameter('title_toggle_state') : 'open',
            'type_toggle_state'                      => $this->request->getParameter('type_toggle_state') ? $this->request->getParameter('type_toggle_state') : 'open',
            'atprompt_toggle_state'                  => $this->request->getParameter('atprompt_toggle_state') ? $this->request->getParameter('atprompt_toggle_state') : 'open',
            'settings_toggle_state'                  => $this->request->getParameter('settings_toggle_state') ? $this->request->getParameter('settings_toggle_state') : 'open',
            'gauge_toggle_state'                     => $this->request->getParameter('gauge_toggle_state') ? $this->request->getParameter('gauge_toggle_state') : 'open',
            'rows_toggle_state'                      => $this->request->getParameter('rows_toggle_state') ? $this->request->getParameter('rows_toggle_state') : 'open',
            'traffic_data_toggle_state'              => $this->request->getParameter('traffic_data_toggle_state') ? $this->request->getParameter('traffic_data_toggle_state') : 'open',
            'traffic_toggle_state'                   => $this->request->getParameter('traffic_toggle_state') ? $this->request->getParameter('traffic_toggle_state') : 'open',
            'columns_toggle_state'                   => $this->request->getParameter('columns_toggle_state') ? $this->request->getParameter('columns_toggle_state') : 'open',
            'options_toggle_state'                   => $this->request->getParameter('options_toggle_state') ? $this->request->getParameter('options_toggle_state') : 'open',
            'rows_state'                             => in_array($type, array(Report::LISTING, Report::GAUGE, Report::TRAFFIC)) ? 'hide' : 'show',
            'columns_state'                          => in_array($type, array(Report::PIE, Report::PARETO, Report::SPC_C_CHART, Report::SPC_I_CHART, Report::SPC_MOVING_RANGE, Report::SPC_RUN_CHART, Report::LISTING, Report::GAUGE, Report::TRAFFIC)) ? 'hide' : 'show',
            'traffic_state'                          => in_array($type, array(Report::TRAFFIC)) ? 'show' : 'hide',
            'additional_options_state'               => in_array($type, array(Report::LISTING)) ? 'hide' : 'show',
            'rows_date_option_state'                 => $rowsIsDate ? 'show' : 'hide',
            'columns_date_option_state'              => $columnsIsDate ? 'show' : 'hide',
            'count_style_field_sum_state'            => $report->count_style == 2 ? 'show' : 'hide',
            'count_style_field_average_state'        => $report->count_style == 3 ? 'show' : 'hide',
            'count_style_field_value_at_state'       => ($module == 'CLA' && ($report->count_style == 2 || $report->count_style == 3) && $isReserve) ? 'show' : 'hide',
            'count_style_field_calendar_state'       => ($module == 'CLA' && $report->count_style_field_value_at == 2 && $isReserve) ? 'show' : 'hide',
            'count_style_field_initial_levels_state' => ($module == 'CLA' && ($report->count_style == 2 || $report->count_style == 3) && in_array($type, array(Report::BAR, Report::CROSSTAB, Report::LISTING)) && $isReserve) ? 'show' : 'hide',
            'at_prompt_state'                        => $this->request->getParameter('at_prompt_state') ? $this->request->getParameter('at_prompt_state') : 'open'
        );
        
        // Create $data array from object

        $data = $report->getReportData();

        if($context != 'administration')
        {
            $data['title'] = $data['title'] == '' ? '' : $packagedReport->name;
        }

        if(in_array($type, array(Report::SPC_C_CHART, Report::SPC_I_CHART, Report::SPC_MOVING_RANGE, Report::SPC_RUN_CHART)))
        {
            $data['type'] = 99;
            $data['spc_type'] = $type;
        }
        else
        {
            $data['type'] = $type;
        }
        $data['query'] = $query;

        if($data['rows'])
        {
            $data['rows_table'] = $data['rows_fieldset'];
            $data['rows_field'] = $data['rows'];
        }

        if($data['columns'])
        {
            $data['columns_table'] = $data['columns_fieldset'];
            $data['columns_field'] = $data['columns'];
        }

        $data['orientation'] = $data['orientation'] ?: 1;
        $data['style'] = $data['style'] ?: 1;

        if ($type == Report::TRAFFIC)
        {
            //list($data['traffic_table'], $data['traffic_field']) = preg_split('/\./', $data['rows']);
            $data['traffic_field'] = $data['rows'];
            $data['rows_traffic_lights'] = $report->rows;
            // these colour values will be posted to the getTrafficLightOptions() function so the colours will retained after updating the report
            $data['rep_colours'] = str_replace('=', ':', http_build_query( $report->getTrafficCodeColours(), '', ',' ) );
        }

        if ($type == Report::GAUGE)
        {
            $data['gauge'] = array(
                'section1_max'                  => ! is_null($report->gauge_section1_max) ? $report->gauge_section1_max : '',
                'section2_max'                  => ! is_null($report->gauge_section2_max) ? $report->gauge_section2_max : '',
                'section3_max'                  => ! is_null($report->gauge_section3_max) ? $report->gauge_section3_max : '',
                'section1_colour'               => ! is_null($report->gauge_section1_colour) ? $report->gauge_section1_colour : '',
                'section2_colour'               => ! is_null($report->gauge_section2_colour) ? $report->gauge_section2_colour : '',
                'section3_colour'               => ! is_null($report->gauge_section3_colour) ? $report->gauge_section3_colour : ''
            );
        }
        else
        {
            $data['gauge'] = array(
                'section1_max'    => '',
                'section2_max'    => '',
                'section3_max'    => '',
                'section1_colour' => '1df055',
                'section2_colour' => 'ffdf0f',
                'section3_colour' => 'ff0000'
            );
        }

        if ($type == Report::LISTING)
        {
            $data['base_listing_report'] = $report->base_listing_report ?: '';
        }

        $types = array(
            Report::BAR              => _tk('bar_chart'),
            Report::PIE              => _tk('pie_chart'),
            Report::LINE             => _tk('line_graph'),
            Report::PARETO           => _tk('pareto_graph'),
            Report::SPC              => _tk('spc_chart'),
            Report::CROSSTAB         => _tk('crosstab_report'),
            Report::LISTING          => _tk('listing_report'),
            Report::GAUGE            => _tk('gauge_chart'),
            Report::TRAFFIC          => _tk('traffic_light_chart')
        );

        //There are no fields available to report on, so traffic lights are not a valid chart type for these modules
        if ( ! isset($moduleDefs[$module]['TRAFFICLIGHTS_FIELDS']))
        {
            unset($types[Report::TRAFFIC]);
        }

        if($data['type'] == Report::BAR)
        {
            if($data['orientation'] == BarChart::HORIZONTAL && $data['style'] == BarChart::GROUPED)
            {
                $bar_default = 'bar-horiz';
            }
            elseif($data['orientation'] == BarChart::HORIZONTAL && $data['style'] == BarChart::STACKED)
            {
                $bar_default = 'bar-stacked-horiz';
            }
            elseif($data['orientation'] == BarChart::VERTICAL && $data['style'] == BarChart::STACKED)
            {
                $bar_default = 'bar-stacked';
            }
            else
            {
                $bar_default = 'bar';
            }

            $pie_default = 'pie-standard';
        }
        elseif($data['type'] == Report::PIE)
        {
            if($data['style'] == PieChart::EXPLODED)
            {
                $pie_default = 'pie-exploded';
            }
            else
            {
                $pie_default = 'pie-standard';
            }

            $bar_default = 'bar';
        }
        else
        {
            $bar_default = 'bar';
            $pie_default = 'pie-standard';
        }

        $types_combined = array(
            Report::BAR              => array(
                'title'              => _tk('bar_chart'),
                'image'              => $bar_default,
                'options'            => array(
                    'bar'               => array(
                        'title'             => _tk('bar_chart_vert'),
                        'image'             => 'bar',
                        'default'           => true
                    ),
                    'bar-stacked'       => array(
                        'title'             => _tk('bar_chart_stacked_vert'),
                        'image'             => 'bar-stacked'
                    ),
                    'bar-horiz'         => array(
                        'title'             => _tk('bar_chart_horiz'),
                        'image'             => 'bar-horiz'
                    ),
                    'bar-stacked-horiz' => array(
                        'title'             => _tk('bar_chart_stacked_horiz'),
                        'image'             => 'bar-stacked-horiz'
                    )
                )
            ),
            Report::PIE             => array(
                'title'             => _tk('pie_chart'),
                'image'             => $pie_default,
                'options'           => array(
                    'pie-standard'      => array(
                        'title'             => _tk('pie_chart_standard'),
                        'image'             => 'pie-standard'
                    ),
                    'pie-exploded'      => array(
                        'title'             => _tk('pie_chart_exploded'),
                        'image'             => 'pie-exploded'
                    )
                )
            ),
            Report::LINE             => array(
                'title'              => _tk('line_graph'),
                'image'              => 'line',
            ),
            Report::PARETO           => array(
                'title'              => _tk('pareto_graph'),
                'image'              => 'pareto',
            ),

            Report::SPC              => array(
                'title'              => _tk('spc_chart'),
                'image'              => 'spc'
            ),
            Report::CROSSTAB         => array(
                'title'              => _tk('crosstab_report'),
                'image'              => 'crosstab'
            ),
            Report::LISTING          => array(
                'title'              => _tk('listing_report'),
                'image'              => 'listing'
            ),
            Report::GAUGE            => array(
                'title'              => _tk('gauge_chart'),
                'image'              => 'gauge'
            ),
            Report::TRAFFIC          => array(
                'title'              => _tk('traffic_light_chart'),
                'image'              => 'traffic'
            )
        );

        //There are no fields available to report on, so traffic lights are not a valid chart type for these modules
        if( ! isset($moduleDefs[$module]['TRAFFICLIGHTS_FIELDS']))
        {
            unset($types_combined[Report::TRAFFIC]);
        }

        $spcTypes = array(
            Report::SPC_C_CHART      => _tk('spc_chart_cchart'),
            Report::SPC_I_CHART      => _tk('spc_chart_ichart'),
            Report::SPC_MOVING_RANGE => _tk('spc_chart_movingrange'),
            Report::SPC_RUN_CHART    => _tk('spc_chart_runchart')
        );
        
        $reportNames = array(
            Report::BAR              => 'bar',
            Report::LINE             => 'line',
            Report::CROSSTAB         => 'crosstab',
            Report::LISTING          => 'listing',
            Report::PARETO           => 'pareto',
            Report::PIE              => 'pie',
            Report::GAUGE            => 'gauge',
            Report::TRAFFIC          => 'traffic',
            Report::SPC              => 'spc'
        );

        $spcReportNames = array(
            Report::SPC_C_CHART      => 'spc',
            Report::SPC_I_CHART      => 'spc',
            Report::SPC_MOVING_RANGE => 'spc',
            Report::SPC_RUN_CHART    => 'spc',
        );

        if ($context == 'administration')
        {
            // listing reports have their own admin design area.
            unset($types[Report::LISTING]);
            unset($types_combined[Report::LISTING]);
        }

        $queries = $this->getQueries($module, $packagedReport);
        $tableCodes = $this->getReportableFieldSets($module);

        // Hide excluded forms
        if ($showExcluded == '0' || $context != 'administration')
        {
            $excludedTables = \DatixDBQuery::PDO_fetch_all('SELECT rex_fieldset FROM report_fieldset_exclusions WHERE rex_module = \''.$module.'\'', array(), \PDO::FETCH_COLUMN);

            foreach ($tableCodes as $int => $desc)
            {
                if(in_array($int, $excludedTables))
                {
                    unset($tableCodes[$int]);
                }
            }
        }

        $dateOptions = array(
            Report::DAY_OF_WEEK       => _tk('day_of_week'),
            Report::WEEK              => _tk('week'),
            Report::WEEK_DATE         => _tk('week_date'),
            Report::MONTH             => _tk('month'),
            Report::MONTH_AND_YEAR    => _tk('month_and_year'),
            Report::QUARTER           => _tk('quarter'),
            Report::YEAR              => _tk('year'),
            Report::FINANCIAL_MONTH   => _tk('financial_month'),
            Report::FINANCIAL_QUARTER => _tk('financial_quarter'),
            Report::FINANCIAL_YEAR    => _tk('financial_year')
        );

        // TYPES
        $fields = array(
            array(
                'Name'                => 'title',
                'Title'               => _tk('report_name'),
                'Type'                => 'string'
            ),
            array(
                'Name'                => 'type',
                'Title'               => _tk('report_type'),
                'Type'                => 'ff_select',
                'CustomCodes'         => $types,
                'SuppressCodeDisplay' => true,
                'OnChangeExtra'       => 'toggleRowsColumns(jQuery("#type").val());setReportType();'
            ),
            array(
                'Name'                => 'base_listing_report',
                'Title'               => _tk('select_base_listing_report'),
                'Type'                => 'ff_select',
                'CustomCodes'         => $this->getArrayOfBaseListingReports($module),
                'SuppressCodeDisplay' => true
            ),
            array(
                'Name'                => 'spc_type',
                'Title'               => _tk('select_spc_type'),
                'Type'                => 'ff_select',
                'CustomCodes'         => $spcTypes,
                'SuppressCodeDisplay' => true
            ),
            array(
                'Name'                => 'query',
                'Title'               => _tk('query'),
                'Type'                => 'ff_select',
                'CustomCodes'         => $queries,
                'SuppressCodeDisplay' => true,
                'OnChangeExtra'       => ($this->context != 'administration' && $this->context != 'dashboard' ? 'checkQueryForAtPrompt(jQuery("#query").val());' : '')
            ),
            array(
                'Name'                => 'rows_table',
                'Title'               => _tk('report_table'),
                'Type'                => 'ff_select',
                'CustomCodes'         => $tableCodes,
                'SuppressCodeDisplay' => true,
                'OnChangeExtra'       => 'jQuery("#rows_field_title").clearField();setTableValues();getDatefields(jQuery("#rows_table").val(), "'.$module.'");'
            ),
            array(
                'Name'                => 'rows_field',
                'Title'               => _tk('report_field'),
                'Type'                => 'ff_select',
                'SelectFunction'      => array(
                    'getNewReportFields',
                    array(
                        'table'        => '"rows"',
                        'tableValues'  => 'tables',
                        'reportType'   => 'reportType',
                        'module'       => '"'.$module.'"',
                        'showExcluded' => '"'.$showExcluded.'"',
                        'context'      => '"'.$context.'"'
                    )
                ),
                'SuppressCodeDisplay' => true,
                'OnChangeExtra'       => 'toggleDateOptions(jQuery("#rows_field").val(), "rows");'
            ),
            array(
                'Name' => 'rows_date_option',
                'Title' => _tk('report_date_option'),
                'Type' => 'ff_select',
                'CustomCodes' => $dateOptions,
                'SuppressCodeDisplay' => true
            ),
            array(
                'Name' => 'columns_table',
                'Title' => _tk('report_table'),
                'Type' => 'ff_select',
                'CustomCodes' => $tableCodes,
                'SuppressCodeDisplay' => true,
                'OnChangeExtra' => 'jQuery("#columns_field_title").clearField();setTableValues();getDatefields(jQuery("#columns_table").val(), "'.$module.'");'
            ),
            array(
                'Name' => 'columns_field',
                'Title' => _tk('report_field'),
                'Type' => 'ff_select',
                'SelectFunction' => array(
                    'getNewReportFields', array(
                        'table'        => '"columns"',
                        'tableValues'  => 'tables',
                        'module'       => '"'.$module.'"',
                        'showExcluded' => '"'.$showExcluded.'"',
                        'context'      => '"'.$context.'"'
                    )
                ),
                'SuppressCodeDisplay' => true,
                'OnChangeExtra' => 'toggleDateOptions(jQuery("#columns_field").val(), "columns");'
            ),
            array(
                'Name' => 'columns_date_option',
                'Title' => _tk('report_date_option'),
                'Type' => 'ff_select',
                'CustomCodes' => $dateOptions,
                'SuppressCodeDisplay' => true
            ),
            array(
                'Name'                => 'traffic_field',
                'Title'               => _tk('report_field'),
                'Type'                => 'ff_select',
                'SelectFunction'      => array(
                    'getNewReportFields',
                    array(
                        'table'        => '"traffic"',
                        'tableValues'  => 'tables',
                        'reportType'   => 'reportType',
                        'module'       => '"'.$module.'"',
                        'showExcluded' => '"'.$showExcluded.'"',
                        'context'      => '"'.$context.'"'
                    )
                ),
                'SuppressCodeDisplay' => true,
                'OnChangeExtra'       => 'toggleDateOptions(jQuery("#traffic_field").val(), "traffic");getTrafficLightCodedFieldsList();'
            ),
            array(
                'Name' => 'traffic_date_option',
                'Title' => _tk('report_date_option'),
                'Type' => 'ff_select',
                'CustomCodes' => $dateOptions,
                'SuppressCodeDisplay' => true
            ),
            array(
                'Title' => _tk('show_as_percentages'),
                'Type' => 'ff_select',
                'CustomCodes' => array( Report::TOOLTIP_VALUE_ONLY => _tk('value_only'), Report::TOOLTIP_PERCENTAGE_ONLY => _tk('percentage_only'), Report::TOOLTIP_PERCENTAGE_AND_VALUE => _tk('percentage_and_value') ),
                'Name' => 'percentages',
                'SuppressCodeDisplay' => true
            ),
            array(
                'Title' => _tk('show_total_nulls'),
                'Type' => 'checkbox',
                'Name' => 'null_values'
            ),
            array(
                'Title' => _tk('count_style'),
                'Type' => 'ff_select',
                'CustomCodes' => array(Report::COUNT_NUM => _tk('number_of_records'), Report::COUNT_SUM => _tk('sum_of_values'), Report::COUNT_AVE => _tk('ave_of_values')),
                'Name' => 'count_style',
                'SuppressCodeDisplay' => true/*,
                'OnChangeExtra'       => 'toggleSumAverage(jQuery("#count_style").val());'*/
            ),
            array(
                'Title' => _tk('field_to_sum'),
                'Type' => 'ff_select',
                'CustomCodes' => $this->getArrayOfAggregatableFields($module),
                'Name' => 'count_style_field_sum',
                'SuppressCodeDisplay' => true/*,
                'OnChangeExtra' => 'toggleValueAt(jQuery("#count_style_field_sum").val(), jQuery("#type").val());'*/
            ),
            array(
                'Title' => _tk('field_to_ave'),
                'Type' => 'ff_select',
                'CustomCodes' => $this->getArrayOfAggregatableFields($module),
                'Name' => 'count_style_field_average',
                'SuppressCodeDisplay' => true/*,
                'OnChangeExtra' => 'toggleValueAt(jQuery("#count_style_field_average").val(), jQuery("#type").val());'*/
            ),
            array(
                'Title' => _tk('field_value_at'),
                'Type' => 'ff_select',
                'CustomCodes' => array(Report::TODAY => _tk('today'), Report::SELECT_DATE => _tk('select_date')),
                'Name' => 'count_style_field_value_at',
                'SuppressCodeDisplay' => true/*,
                'OnChangeExtra' => 'toggleCalendar(jQuery("#count_style_field_value_at").val());'*/
            ),
            array(
                'Type' => 'date',
                'Name' => 'count_style_field_calendar',
                'NotFuture' => true,
                'NoValidation' => true
            ),
            array(
                'Title' => _tk('initial_levels'),
                'Type' => 'checkbox',
                'Name' => 'count_style_field_initial_levels'
            ),
        );

        $gauge_options = array(
            'gaugeoptions' => array(
                'ControllerAction' => array(
                    'gaugedesignsection' => array(
                        'controller' => 'src\\reports\\controllers\\ReportController')),
            )
        );

        $fieldContent['gauge'] = new \FormTable('', $module);
        $fieldContent['gauge']->MakeForm($gauge_options, $data, $module);

        foreach($fields as $field)
        {
            $fieldObj = new \FormField();
            $make_field = new \FormTable('', $module);
            $fieldContent[strtolower($field['Name'])] = $make_field->call_makeField($field['Type'], $field, $field['Name'], $data, 'new', '', $field['Name'], '', $fieldObj, '', '', $module, '');

            if (in_array($field['Name'], ['rows_field', 'columns_field', 'traffic_field' ]))
            {
                $fieldContent[strtolower($field['Name'])]->setDescription($this->getReportFieldDescription($field['Name'], $data, $module));
            }

            if($field['Name'] == 'rows_table')
            {
                $rowsTable = $this->request->getParameter("rows_table");
                $fieldContent[strtolower($field['Name'])]->setDescription($this->getReportFormFieldDescription($data['rows'], $rowsTable, $module));
            }
            elseif($field['Name'] == 'columns_table')
            {
                $fieldContent[strtolower($field['Name'])]->setDescription($this->getReportFormDescription($data['columns'], $module));
            }
        }

        $text = array(
            'run' => _tk('run_report'),
            'update' => _tk('update_report'),
            'clear' => _tk('clear_settings'),
            'form_label' => _tk('form_label'),
            'field_label' => _tk('field_label'),
        );
        //call_makeField($Type, $FieldDef, $OriginalFieldName, $Data, $mode, $ContactSuffix, $InputFieldName, $AltFieldName, $FieldObj, $AuditValue, $AuditObj, $Module, $Label = "")

        //$make_type_field = new \FormTable('', $module);
        //$type_field = $make_type_field->call_makeField($type['Type'], $type, $type['Name'], $type['CustomCodes'], 'new', '', $type['Name'], '', '', '', '', $module, '');

        $moduleDefs = $this->registry->getModuleDefs();

        /**
         * Send data to JS so it can be used in the functions that require this data
         */
        $this->sendToJs(array
            (
                'text' => array(
                    'opt1' => array(
                        'rows' => _tk('rows'),
                        'columns' => _tk('columns')
                    ),
                    'opt2' => array(
                        'rows' => _tk('field_1'),
                        'columns' => _tk('field_2')
                ),
                    'opt3' => array(
                        'rows' => _tk('field')
                    ),
                    'report_button' => array(
                        'run' => _tk('run_report'),
                        'update' => _tk('update_report')
                    ),
                    'form' => _tk('form_label'),
                    'prompt' => _tk('select_required_options'),
                    'clear_confirm' => _tk('clear_confirm'),
                    'save_confirm' => _tk('save_confirm'),
                    'changes_confirm' => _tk('changes_confirm')
                ),
                'module' => $module,
                'showPrompt' => ($showPrompt ? true : false),
                'isListing' => ($data['type'] == \src\reports\model\report\Report::LISTING ? true : false),
                'isCrosstab' => ($data['type'] == \src\reports\model\report\Report::CROSSTAB ? true : false),
                'reserveFields' => $reserveFields
            )
        , null, false);

        foreach($reportNames as $report_val => $report_name)
        {
            $this->sendToJs('REPORT_' . strtoupper($report_name), $report_val, false);
        }

        $type_names = array();

        foreach($reportNames as $report_val => $report_name)
        {
            $type_names[$report_val] = $report_name;
        }

        $this->sendToJs('type_names', $type_names, false);

        $this->response->build('src/reports/views/ReportDesignerForm.php', array(
            'states'                    => $states,
            'base_report'               => $this->request->getParameter('base_report') ? $this->request->getParameter('base_report') : $packagedReport->recordid,
            'report'                    => $report,
            'data'                      => $data,
            'report_run'                => $this->request->getParameter('form_action') == 'show_report' || $this->request->getParameter('recordid') ? true : false,
            'text'                      => $text,
            'module'                    => $module,
            'heading'                   => getPageTitleHTML(array
                                            (
                                                'title' => _tk('report_designer').' - ' . $moduleDefs[$module]['NAME'],
                                                'module' => $this->module
                                            ), false),
            'types'                     => $types,
            'types_combined'            => $types_combined,
            'fieldContent'              => $fieldContent,
            'hideRowsDateOption'        => $data['rows_date_option'] == '',
            'hideColumnsDateOption'     => $data['columns_date_option'] == '',
            'hideRowsTrafficOption'     => $data['rows_traffic_lights'] == '',
            'context'                   => $context,
            'reportNames'               => $reportNames,
            'hasAtPrompt'               => $hasAtPrompt,
            'atPromptSection'           => $hasAtPrompt ? $this->call(get_class($this), 'getatpromptsection', ['id' => $query, 'rep_id' => $this->request->getParameter('recordid'), 'new_query_id' => $newQueryID]) : '',
            'newQueryID'                => $newQueryID,
            'showPrompt'                => $showPrompt,
            'atPromptOnly'              => !bYN($this->registry->getParm('CUSTOM_REPORT_BUILDER', 'Y')) && $hasAtPrompt
        ));
    }

    /**
     * Returns a list of date fields for a given table.
     * 
     * Called via ajax and used to toggle the date options field on the report design form.
     */
    public function getDateFields()
    {
        $fieldset   = $this->request->getParameter('table');
        $module     = $this->request->getParameter('module');
        $dateFields = array();
        $query      = new Query();

        // We just want the fields of type date so let's filter the query
        $query->where(array('data_type' => 'D'));

        foreach ((new ReportField($fieldset, $module))->getCodes($query) as $code)
        {
            $dateFields[] = $code->code;
        }
        
        $this->response->build('src/reports/views/GetDateFields.php', array(
            'dateFields' => $dateFields
        ));
    }

    /**
     * Constructs a list of all tables available for reporting on for this module.
     * 
     * Used to populate the form dropdowns.
     * 
     * @param string $module
     * 
     * @return array
     */
    public function getTables($module)
    {
        $tables = array();
        
        $moduleDefs = $this->registry->getModuleDefs();
        $tableDefs  = $this->registry->getTableDefs();
        
        // the view should be used preferentially if one is defined for this module
        $tableName = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];
        
        $table = $tableDefs[$tableName];
        
        $tables[$table->tdr_name] = _tk($table->tdr_desc);

        $excludedTables = \DatixDBQuery::PDO_fetch_all(
            'SELECT rex_fieldset FROM report_fieldset_exclusions WHERE rex_module = :module',
            array('module' => $module), \PDO::FETCH_COLUMN);
        
        foreach ($table->link_tables as $linkTable)
        {
            //Excluding inc_injuries here is a hack to allow injuries to appear under "People Affected" without
            //appearing on the appropriate view (due to duplications)
            if ($linkTable->name != 'inc_injuries' && ($this->request->getParameter('action') == 'excludefields' || !in_array($linkTable->name, $excludedTables)))
            {
                $tables[$tableDefs[$linkTable->name]->tdr_name] = _tk($tableDefs[$linkTable->name]->tdr_desc);
            }
        }

        $tables = $this->sortTableArray($tables, $module);

        return $tables;
    }

    /**
     * Constructs a list of all tables available for reporting on for this module.
     *
     * Used to populate the form dropdowns.
     *
     * @param string $module
     *
     * @return array
     */
    public function getReportableFieldSets($module)
    {
        $moduleDefs = $this->registry->getModuleDefs();
        $tableDefs  = $this->registry->getTableDefs();

        // the view should be used preferentially if one is defined for this module
        $tableName = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];

        $table = $tableDefs[$tableName];

        $fieldsets[0] = _tk($table->tdr_desc);

        $fieldsetData = \DatixDBQuery::PDO_fetch_all('SELECT DISTINCT fieldset, label_key FROM reportable_fieldsets WHERE module = :module', array('module' => $module), \PDO::FETCH_KEY_PAIR);

        foreach ($fieldsetData as $fieldset => $label)
        {
            $fieldsets[$fieldset] = _tk('table_'.$label);
        }

        return $fieldsets;
    }


    protected function sortTableArray($tables, $module)
    {
        $tableOrders = array(
            'INC' => array(
                'incidents_main',
                'assets_main',
                'ca_actions',
                'causal_factors',
                'inc_medications',
                'vw_payments',
                'aud_reasons',
                'link_contacts_INC',
                'link_contacts_INC_A',
                'link_contacts_INC_E',
                'link_contacts_INC_N',
            ),
            'RAM' => array(
                'ra_main',
                'ca_actions',
                'aud_reasons',
                'link_contacts_RAM_N',
            ),
            'PAL' => array(
                'pals_main',
                'pals_subjects',
                'ca_actions',
                'aud_reasons',
                'link_contacts_PAL',
                'link_contacts_PAL_P',
                'link_contacts_PAL_N',
            ),
            'COM' => array(
                'compl_main',
                'compl_subjects',
                'compl_isd_issues',
                'ca_actions',
                'vw_payments',
                'rev_main',
                'aud_reasons',
                'link_contacts_COM',
                'link_contacts_COM_C',
                'link_contacts_COM_A',
                'link_contacts_COM_E',
                'link_contacts_COM_N',
            ),
            'CLA' => array(
                'claims_main',
                'ca_actions',
                'events_history',
                'vw_payments',
                'aud_reasons',
                'link_contacts_CLA',
                'link_contacts_CLA_M',
                'link_contacts_CLA_A',
                'link_contacts_CLA_E',
                'link_contacts_CLA_N',
            ),
            'SAB' => array(
                'sabs_main',
                'assets_main',
                'ca_actions',
                'link_contacts_SAB',
                'link_contacts_SAB_S',
                'link_contacts_SAB_I',
                'link_contacts_SAB_N',
            ),
            'STN' => array(
                'standards_main',
                'assets_main',
                'ca_actions',
            ),

        );

        $newTables = array();

        foreach ($tableOrders[$module] as $table)
        {
            if(isset($tables[$table]))
            {
                $newTables[$table] = $tables[$table];
            }
        }

        //Add onto the end all unordered tables.
        foreach ($tables as $table => $label)
        {
            if (!isset($newTables[$table]))
            {
                $newTables[$table] = $tables[$table];
            }
        }

        return $newTables;
    }

    /**
     * Provides the codes for the saved queries dropdown.
     * 
     * @return array
     */
    protected function getQueries($module, PackagedReport $packagedReport)
    {
        $queries = (new SavedQueryField)->getCodes(
            (new Query)->where(['sq_module' => $module])
        );
        
        $queryCodes = [];
        if ($this->displayReportCriteriaOption($packagedReport))
        {
            // the most recent search can be used rather than a saved query
            $queryCodes[-1] = '[' . _tk('report_criteria') . ']';
        }
        if ($this->displayCurrentCriteriaOption($module))
        {
            // the most recent search can be used rather than a saved query
            $queryCodes[0] = '[' . _tk('current_criteria') . ']';
        }

        foreach ($queries as $query)
        {
            $queryCodes[$query->code] = $query->description;
        }
        
        return $queryCodes;
    }

    /**
     * Returns an array of fields which should be available to sum/average data over.
     * TODO: MOVE THIS TO USE A CodeField OBJECT
     */
    function getArrayOfAggregatableFields($module)
    {
        global $ModuleDefs, $FieldDefs;

        $numericExclusions = array(
            'inc_dreported-inc_dincident',
            'inc_dsched-inc_dincident',
            'inc_dsched-inc_dopened',
            'seclevel',
            'ram_parent',
            'com_dclosed-com_dreceived',
            'pal_dclosed-pal_dreceived',
            'asm_module_template_id',
            'asm_template_instance_id',
            'asm_question_template_id',
            'asm_module_id',
        );

        $table = $ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE'];

        if ($module == 'PAY')
        {
            $fformats_table = $ModuleDefs[$module]['TABLE'];
        }
        else
        {
            $fformats_table = $table;
        }

        // Get reportable numeric fields (money and number)
        $sql = "SELECT '{$table}.'+fmt_field, fmt_new_label
        FROM vw_field_formats
        WHERE fmt_module = :module1
        AND fmt_table = :ff_table1
        AND
            (fmt_data_type in ('M','N'))
        AND fmt_field NOT IN
        (SELECT rex_field FROM report_exclusions
            WHERE rex_module = :module2
        AND rex_fieldset = 0)
        AND fmt_field NOT IN ('".implode("','",GetHardCodedReportExcludedFields())."')
        AND fmt_field NOT IN ('".implode("','",$numericExclusions)."')
        AND fmt_field NOT LIKE '%.recordid%'
        AND fmt_field NOT LIKE 'recordid'
        UNION
        SELECT '{$table}.'+('UDF_' + CAST(udf_fields.recordid AS VARCHAR(32))) as fmt_field, fld_name as fmt_new_label
        FROM udf_fields
        LEFT JOIN udf_mod_link ON udf_mod_link.uml_id = udf_fields.recordid
        WHERE
        fld_type in ('M','N')
        AND (udf_mod_link.uml_module = :module3 OR udf_mod_link.uml_module IS NULL OR udf_mod_link.uml_module = '')
        AND ('UDF_' + CAST(udf_fields.recordid AS VARCHAR(32))) NOT IN
        (SELECT rex_field FROM report_exclusions
            WHERE rex_module = :module4
        AND rex_fieldset = 0)
        ORDER BY 2 ASC";

        $numericFields = PDO_fetch_all($sql, array(
            'module1' => $module,
            'module2' => $module,
            'module3' => $module,
            'module4' => $module,
            'ff_table1' => $fformats_table
            ),
         \PDO::FETCH_KEY_PAIR);

        //If we're in claims, get payment summary fields
        if ($module == 'CLA')
        {
            $PaymentTypes = \DatixDBQuery::PDO_fetch_all('SELECT code, description from code_fin_type');

            foreach($PaymentTypes as $PaymentType)
            {
                $numericFields[$table.'.pay_type_'.$PaymentType['code']] = $PaymentType['description'].' (Payment Summary)';
            }

            $numericFields[$table.'.pay_type_PAYTOTAL'] = 'Total (Payment Summary)';
        }

        // removed fields flagged with "BlockFromReports"
        foreach ($numericFields as $name => $label)
        {
            list (,$field) = explode ('.', $name);
            if ($FieldDefs[$module][$field]['BlockFromReports'])
            {
                unset ($numericFields[$name]);
            }
        }

        return $numericFields;
    }

    /**
     * Constructs the options form used when exporting reports.
     */
    public function getExportFormHTML()
    {
        $packagedReport = (new PackagedReportFactory)->createCurrentPackagedReportFromRequest($this->request);

        $extraOptions = in_array ($packagedReport->report->getReportType(), array(Report::CROSSTAB, Report::LISTING));

        $this->response->build('src/reports/views/ExportForm.php', array(
            'extraOptions' => $extraOptions,
            'noExcel' => in_array($packagedReport->report->getReportType(), array(Report::TRAFFIC, Report::GAUGE)) || $this->request->getParameter('noExcel')
        ));
    }

    protected function getArrayOfBaseListingReports($module)
    {
        $sql = "
            SELECT
                recordid,
                rep_name
            FROM
                reports_extra
            WHERE
                rep_module = :module
                AND
                rep_type = 'R'
                AND
                ((rep_private != 'Y' OR rep_private IS NULL) OR (rep_private = 'Y' AND rep_createdby = '" . $_SESSION["initials"] . "'))
                AND
                (rep_app_show = 'WEB' OR rep_app_show = 'BOTH')
        ";

        $result = PDO_fetch_all($sql, array('module' => $module));
        $ReportArray = array();

        foreach ($result as $row)
        {
            $ReportArray[$row["recordid"]] = $row["rep_name"];
        }

        switch ($module)
        {
            case 'INC':
                if (bYN(GetParm('DIF2_DEFAULT_REPORTS','N')))
                {
                    $ReportArray['report1'] = 'Listing with description and correctable cause analysis';
                    $ReportArray['report2'] = (_tk('INCNamesTitle').' grouped by ' . GetFieldLabel('inc_directorate', 'Directorate') . ' and correctable cause');
                    $ReportArray['report3'] = _tk('INCNamesTitle').' grouped by correctable cause and severity';
                }
                break;
            case 'CLA':
                $ReportArray['report7'] = 'Analysis of claims links report';
                break;
            case 'RAM':
                $ReportArray['report4'] = 'Assurance Framework report';
                break;
            case 'STN':
                $ReportArray['report5'] = 'Full Listing 3 (colour)';
                $ReportArray['report6'] = 'CQC Outcomes';
                break;
        }

        if (!empty($ReportArray))
        {
            asort($ReportArray);
        }

        return $ReportArray;
    }
    
    /**
     * Fetches the description for the currently-selected report field option.
     * 
     * Used on page load to ensure field description rather than the actual field name is displayed.
     * 
     * @param string $fieldName "rows_field"/"columns_field".
     * @param array  $data      The request data relevant to the design form.
     * 
     * @return string
     */
    protected function getReportFieldDescription($fieldName, array $data, $module)
    {

        if (!isset($data[$fieldName]))
        {
            return;
        }
        
        $field = explode('_', $fieldName)[0];
        $fieldset = $data[$field.'_table'];
        
        $codes = (new ReportField($fieldset, $module, false, true))->getCodes((new Query)->where(['code' => explode('.', $data[$fieldName])[1]]));

        if (null !== ($code = $codes[$data[$fieldName]]))
        {
            return $code->description;
        }
    }
    
    /**
     * Whether or not to display the [Current criteria] query option.
     * 
     * @return boolean
     */
    protected function displayCurrentCriteriaOption($module)
    {
        return isset($_SESSION[$module]['NEW_WHERE']) || isset($_SESSION[$module]['OVERDUE']) || isset($_SESSION[$module]['list_type']);
    }
    
    /**
     * Whether or not to display the [Criteria saved against report] query option.
     *
     * @return boolean
     */
    protected function displayReportCriteriaOption(PackagedReport $packagedReport)
    {
        if ($this->request->getParameter('base_report'))
        {
            $packagedReport = (new PackagedReportModelFactory)->getMapper()->find($this->request->getParameter('base_report'));
        }

        if ((!$packagedReport->query->old_query_id && $packagedReport->query->recordid) || ( $this->request->getParameter('isCrosstab') == true && $this->request->getParameter('drilllevel') !== '0' && $this->request->getParameter('old_query_id') == '' ))
        {
            return true;
        }

        return false;
    }

    /**
     * Checks to see whether or not a given saved query uses @prompt codes.
     */
    public function queryHasAtPrompt()
    {
        $id = $this->request->getParameter('id');
        $module = $this->request->getParameter('module');
        
        if ($id == '0' && $_SESSION[$module]['NEW_WHERE'] instanceof Where)
        {
            $savedQuery = (new SavedQueryModelFactory)->getEntityFactory()->createObject(['where' => $_SESSION[$module]['NEW_WHERE']]);
        }
        else
        {
            $savedQuery = (new SavedQueryModelFactory)->getMapper()->findByOldQueryID($id);
        }
        
        if ($savedQuery !== null && $savedQuery->hasAtPrompt())
        {
            $this->response->setBody('true');
        }
        else
        {
            $this->response->setBody('false');
        }
    }
    
    /**
     * Retrieves the at prompt values section of the report design form.
     */
    public function getAtPromptSection()
    {
        $id = $this->request->getParameter('id');
        $module = $this->request->getParameter('module');
        $context = $this->request->getParameter('context');
        $packagedReportID = $this->request->getParameter('rep_id');
        $newQueryID = $this->request->getParameter('new_query_id');
        
        $displayCurrentValues = $context != 'ajax' || $id == '0';
        
        if ($packagedReportID != '')
        {
            // loading a saved packaged report
            $packagedReport = (new PackagedReportModelFactory)->getMapper()->find($packagedReportID);
            if ($packagedReport != null)
            {
                $savedQuery = $packagedReport->query;
            }
        }
        else if ($newQueryID != '')
        {
            $savedQuery = (new SavedQueryModelFactory)->getMapper()->find($newQueryID);
        }
        else if ($id == '0' && $_SESSION[$module]['NEW_WHERE'] instanceof Where)
        {
            // using the [Current criteria]
            $savedQuery = (new SavedQueryModelFactory)->getEntityFactory()->createObject(['where' => $_SESSION[$module]['NEW_WHERE']]);
        }
        else
        {
            // using an existing saved query
            $savedQuery = (new SavedQueryModelFactory)->getMapper()->findByOldQueryID($id);
        }
        
        if (isset($savedQuery))
        {
            if ($module == '')
            {
                // which will be the case if we're loading a saved packaged report
                $module = $savedQuery->module;
            }
            
            $fieldDefs = $this->registry->getFieldDefs();
            $form = new \FormTable('Search', $module);
            $fields = [];
            
            foreach ($savedQuery->getAtPromptFields($module) as $field)
            {
                if (null !== ($fieldDef = $fieldDefs[$field]))
                {
                    $fields[$fieldDef->getName()]['label'] = \Labels_FieldLabel::getLabelFromFieldDef($fieldDef);
                    $fields[$fieldDef->getName()]['id'] = $fieldDef->getName();

                    if ($fieldDef->getType() == FieldInterface::DATE)
                    {
                        if ($displayCurrentValues)
                        {
                            $start = $_SESSION['NEW_PROMPT_VALUES'][$field]['start'];
                            $end = $_SESSION['NEW_PROMPT_VALUES'][$field]['end'];
                        }
                        
                        $name = $fieldDef instanceof ExtraField ? $fieldDef->getFullName() : $fieldDef->getName();
                        
                        $fields[$fieldDef->getName()]['start'] = (new \FormField('Search'))->MakeDateField($name.'_start', $start);
                        $fields[$fieldDef->getName()]['end'] = (new \FormField('Search'))->MakeDateField($name.'_end', $end);
                    }
                    else if ($fieldDef instanceof ExtraField)
                    {
                        if ($displayCurrentValues)
                        {
                            $data = [$fieldDef->getValueField() => $_SESSION['NEW_PROMPT_VALUES'][$field]];
                        }
                        else
                        {
                            $data = [];
                        }
                        
                        $fields[$fieldDef->getName()]['field'] = $form->MakeUDFField(
                            $fieldDef->getFullName(),
                            $fieldDef->recordid,
                            $fieldDef->getType(),
                            $data
                        );
                    }
                    else if ($fieldDef->getName() == 'rep_approved')
                    {
                        if ($displayCurrentValues)
                        {
                            $data = ['rep_approved' => $_SESSION['NEW_PROMPT_VALUES'][$field]];
                        }
                        else
                        {
                            $data = [];
                        }
                        
                        SetUpApprovalArrays($module, $data, $dummyFieldObj, $fieldObj, 'Search');
                        $fields[$fieldDef->getName()]['field'] = $fieldObj;
                    }
                    else
                    {
                        if ($displayCurrentValues)
                        {
                            $data = [$fieldDef->getName() => $_SESSION['NEW_PROMPT_VALUES'][$field]];
                        }
                        else
                        {
                            $data = [];
                        }
                        
                        $fields[$fieldDef->getName()]['field'] = $form->call_makeField(
                            $fieldDef->getType(),
                            ['Module' => $module],
                            $fieldDef->getName(),
                            $data,
                            'Search',
                            '',
                            $fieldDef->getName()
                        );
                    }
                }
            }
        }
            
        $this->response->build('src/reports/controllers/AtPromptSection.php', [
            'fields'  => $fields,
            'context' => $context
        ]);
    }
    
    /**
     * Renders the traffic light report options section.
     */
    public function getTrafficLightOptions()
    {
        $fieldDefs  = $this->registry->getFieldDefs();

        $field  = $fieldDefs[$this->request->getParameter('field')];
        
        if (!($field instanceof CodeField))
        {
            throw new ReportException('Unable to fetch codes for field '.$this->request->getParameter('field'));
        }
        
        $codes          = $field->getCodes();
        $defaultPalette = ['C2300A', 'DEA312', 'EDE400', '8BD239', '079217'];
        
        $colours = explode(',', $this->request->getParameter('colours'));   
        if (!empty($colours))
        {
            foreach ($colours as $colourPair)
            {
                $pairSplit = explode(':', $colourPair);
                $codeColours[$pairSplit[0]] = $pairSplit[1];
            }
        }
    
        $options = [];
        foreach ($codes as $code)
        {
            $options[$code->code] = ['description' => $code->description];
            
            if (isset($codeColours[$code->code]))
            {
                // use the colour sent with the request
                $options[$code->code]['colour'] = $codeColours[$code->code];
            }
            else if ($code->cod_web_colour != '')
            {
                // use the predefined colour for this code
                $options[$code->code]['colour'] = $code->cod_web_colour;
            }
            else if (!empty($defaultPalette))
            {
                // use one of the colours defined in the default palette
                $options[$code->code]['colour'] = array_pop($defaultPalette);
            }
            else
            {
                // generate a random colour
                $c = '';
                for ($i = 0; $i < 6; $i++)
                {
                    $c .=  dechex(\Random::unsecure_rand(0, 15));
                }
                $options[$code->code]['colour'] = $c;
            }
        }
        
        $this->addCss('js_functions/colorpicker/css/colorpicker.css');
        $this->addJs('js_functions/colorpicker/js/colorpicker.js');
        
        $this->response->build('src/reports/views/TrafficLightOptions.php', array(
            'options' => $options
        ));
    }
    
    /**
     * Sets @prompt values in the session to those chosen on the report designer form.
     */
    public function setAtPromptValues()
    {
        $packagedReport = $this->request->getParameter('packagedReport');
        
        if ($packagedReport instanceof PackagedReport && null !== ($savedQuery = $packagedReport->query) && $savedQuery->hasAtPrompt())
        {
            $module = $savedQuery->module;
            
            foreach ($savedQuery->getAtPromptFields($module) as $atPromptField)
            {
                // remove table prefix
                if (false !== ($pos = strpos($atPromptField, '.')))
                {
                    $fieldName = substr($atPromptField, $pos+1);
                }
                else
                {
                    $fieldName = $atPromptField;
                }
                
                if ($this->request->getParameter('from_my_reports') == '1')
                {
                    // ensure @prompt values stored in session are not used when loading a saved packaged report
                    $_SESSION['NEW_PROMPT_VALUES'][$atPromptField] = null;
                    $_SESSION[$module]['PROMPT']['VALUES'][$fieldName] = null;
                }
                
                if ($this->request->getParameter($fieldName.'_start') !== null)
                {
                    $_SESSION['NEW_PROMPT_VALUES'][$atPromptField]['start'] = $this->request->getParameter($fieldName.'_start');
                    $_SESSION['NEW_PROMPT_VALUES'][$atPromptField]['end'] = $this->request->getParameter($fieldName.'_end');
                    
                    // set "old" @prompt session values too, in case we're using an old query
                    $_SESSION[$module]['PROMPT']['VALUES'][$fieldName]['start'] = $this->request->getParameter($fieldName.'_start');
                    $_SESSION[$module]['PROMPT']['VALUES'][$fieldName]['end'] = $this->request->getParameter($fieldName.'_end');
                }
                else if ($this->request->getParameter($fieldName) !== null)
                {
                    $_SESSION['NEW_PROMPT_VALUES'][$atPromptField] = $this->request->getParameter($fieldName);
                    
                    // set "old" @prompt session value too, in case we're using an old query
                    $_SESSION[$module]['PROMPT']['VALUES'][$fieldName] = $this->request->getParameter($fieldName);
                }
            }
        }
    }

    /**
     * Function to get description for rows and columns table names.
     *
     * @param string $rowsOrColumns Table plus field names.
     * @param string $module        The 3 letter module code.
     *
     * @return string The description of the table.
     */
    private function getReportFormDescription($rowsOrColumns, $module)
    {
        $tableName = explode('.', $rowsOrColumns);

        $sql = 'SELECT label_key FROM reportable_fieldsets WHERE module = :module AND data_table = :data_table';

        $result = \DatixDBQuery::PDO_fetch($sql, ['module' => $module, 'data_table' => $tableName[0]]);

        return _tk('table_'.$result['label_key']);
    }

    private function getReportFormFieldDescription($rowsOrColumns, $fieldset, $module)
    {
        $tableName = explode('.', $rowsOrColumns);

        $sql = 'SELECT label_key FROM reportable_fieldsets WHERE module = :module AND data_table = :data_table AND fieldset = :fieldset';

        $result = \DatixDBQuery::PDO_fetch($sql, ['module' => $module, 'data_table' => $tableName[0], 'fieldset' => $fieldset]);

        return _tk('table_'.$result['label_key']);
    }
}
