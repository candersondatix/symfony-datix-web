<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report counts values at a specified point.
 */
class CountValueAtReportSpecification extends AbstractReportSpecification
{
    /**
     * @var string
     */
    protected $countValueAt;
    
    public function __construct($countValueAt)
    {
        $this->countValueAt = $countValueAt;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return $this->countValueAt == $packagedReport->report->count_style_field_value_at;
    }
}