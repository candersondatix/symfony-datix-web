<?php

namespace src\reports\specifications\CLA;

use src\reports\specifications\AbstractReportSpecification;
use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report is doing a sum on a Claims Payment Type code.
 */
class SumOnPayTypeReportSpecification extends AbstractReportSpecification
{
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return 'claims_main.pay_type' == substr($packagedReport->report->count_style_field_sum, 0, 20);
    }
}