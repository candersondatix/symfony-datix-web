<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;
use src\system\database\fielddef\FieldDefCollection;
use src\system\database\table\Link;

/**
 * Determines whether we're reporting on linked contact main table fields without linked contact link table fields.
 */
class LinkedContactMainTableReportSpecification extends AbstractReportSpecification
{
    /**
     * @var FieldDefs
     */
    protected $fieldDefs;

    /*
     * The link table for linking to contacts main
     *
     * @var String
     */
    protected $contactLinkTable;

    public function __construct(FieldDefCollection $fieldDefs, $contactLinkTable)
    {
        $this->fieldDefs = $fieldDefs;
        $this->contactLinkTable = $contactLinkTable;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        $rows = $this->fieldDefs[$packagedReport->report->rows];
        $columns = $this->fieldDefs[$packagedReport->report->columns];

        if (null !== $rows && 'contacts_main' == $rows->getTable())
        {
            return (null === $columns || $this->contactLinkTable != $columns->getTable());
        }
        else
        {
            if (null !== $columns && 'contacts_main' == $columns->getTable())
            {
                return (null === $rows || $this->contactLinkTable != $rows->getTable());
            }
        }

        return false;
    }
}