<?php
namespace src\reports;

use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\report\GaugeChart;
use src\reports\model\report\ParetoChart;
use src\reports\model\report\Report;
use src\reports\model\report\BarChart;
use src\reports\model\report\LineChart;
use src\reports\model\report\PieChart;
use src\reports\model\report\RiskTrackerReport;
use src\reports\model\report\TrafficLightChart;
use src\reports\controllers\ReportWriter;
use src\reports\exceptions\ReportException;
use src\reports\model\report\SpcChart;
use src\system\database\FieldInterface;
use src\framework\registry\Registry;
use src\framework\controller\Request;

require_once 'thirdpartylibs/fusioncharts/FusionCharts_DATIX_Widgets.php';

class FusionChartFactory
{
    /**
     * @var string
     */
    protected $chartID;
    
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Context for the class - eg. dashboard, fullscreen
     */
    //TODO This should probably be updated to be used throughout the class and not just in newer functions
    protected $context;
    protected $reportType;
    protected $report;

    public function __construct($chartID = '', Registry $registry = null)
    {
        $request = new Request();
        $this->chartID  = 'myFusionChartReport'.(($request->getParameter('dbid')) ? '_' . $request->getParameter('dbid') . '_' : '').$chartID;
        $this->registry = $registry ?: Registry::getInstance();
    }
    
    /**
     * Constructs and returns a FusionChart object.
     * 
     * @param PackagedReport        $packagedReport  The report object.
     * @param GraphicalReportEngine $engine  Then engine responsible for fetching the report data.
     * @param int                   $context The context in which the report is being rendered.
     * 
     * @throws exceptions\ReportException
     * @return \FusionChartsWidgets
     */
    public function getChart(PackagedReport $packagedReport, GraphicalReportEngine $engine, $context)
    {
        $report = $packagedReport->report;
        $data   = $this->cleanData($engine->getData($packagedReport, $context));

        $reportType = get_class($report);

        // Set the class parameters
        $this->context = $context;
        $this->reportType = $reportType;
        $this->report = $report;

        switch ($reportType)
        {
            case 'src\\reports\\model\\report\\BarChart':
                $chart = $this->getBarChart($report, $context);
                break;

            case 'src\\reports\\model\\report\\ParetoChart':
                $chart = $this->getParetoChart($report, $context);
                break;
                
            case 'src\\reports\\model\\report\\PieChart':
                $chart = $this->getPieChart($report, $context);
                break;
                
            case 'src\\reports\\model\\report\\LineChart':
                $chart = $this->getLineChart($report, $context);
				break;

            case 'src\\reports\\model\\report\\RiskTrackerReport':
                $chart = $this->getRiskTrackerChart($report, $context);
				break;

            case 'src\\reports\\model\\report\\SpcChart':
                $chart = $this->getSpcChart($report, $context);
                break;

            case 'src\\reports\\model\\report\\TrafficLightChart':
                $chart = $this->getTrafficLightChart($report, $context, $data);
                break;

            case 'src\\reports\\model\\report\\GaugeChart':
                $chart = $this->getGaugeChart($report, $context, $data);
                break;

            default:
                throw new ReportException('Invalid chart type');
        }
        
        $this->addGenericProperties($chart, $report);
        $this->addContextSpecificProperties($chart, $report, $context, $packagedReport->name);
        $this->addExportSpecificProperties($chart);

        // To keep consistency with the FusionCharts code flow we need to add chart data after setting the chart properties
        switch ($reportType)
        {
            case 'src\\reports\\model\\report\\BarChart':
            	$this->addBarData($chart, $data, $report);
                break;

            case 'src\\reports\\model\\report\\ParetoChart':
                $this->addParetoData($chart, $data);
                break;

            case 'src\\reports\\model\\report\\PieChart':
                $this->addPieData($chart, $data, $report);
                break;

            case 'src\\reports\\model\\report\\RiskTrackerReport':
            case 'src\\reports\\model\\report\\LineChart':
                $this->addLineData($chart, $data, $report);
				break;

            case 'src\\reports\\model\\report\\SpcChart':
                $this->addSpcData($chart, $data, $report);
                break;

            case 'src\\reports\\model\\report\\TrafficLightChart':
                $this->addTrafficLightData($chart, $data, $report);
                break;

            case 'src\\reports\\model\\report\\GaugeChart':
                $this->addGaugeData($chart, $data, $report);
                break;

            default:
                throw new ReportException('Invalid chart type');
        }
        
        return $chart;
    }
    
    /**
     * IE hack: if Flash isn't installed and there are some special characters in the header string, fusionchart will not work in IE, it will give back "Invalid data" error message.
     * By the way, fusioncahrt encodes these characters but IE don't like these encodes strings. That's why we remove these characters.
     * 
     * @author gszucs
     * @param array $data
     * @return array:
     */
    protected function cleanData(array $data)
    {
    	if( false === $_SESSION['FlashAvailable'] )
    	{
	    	$data['rowHeaders'] = array_map( array($this,'removeSpecialChars'), $data['rowHeaders']);
	    	
	    	$data['columnHeaders'] = array_map( array($this,'removeSpecialChars'), $data['columnHeaders']);
	    		
	    	$data['toolText'] = array_map( array($this,'removeSpecialChars'), $data['toolText']);	
    	}
    	
    	return $data;
    }
    
    /**
     * Remove some speacil chars from a string
     * 
     * @author gszucs
     * @param string $string
     * @return mixed
     */
    protected function removeSpecialChars($string)
    {
    	return str_replace( array('"', "'", '>', '<'), '', $string );
    }
    
    /**
     * Adds properties common to all chart types.
     * 
     * @param FusionChartsWidgets $chart  The chart object.
     * @param Report              $report The report object.
     * @param array               $data   The report data.
     */
    protected function addGenericProperties(\FusionChartsWidgets $chart, Report $report)
    {
        $chart->setSWFPath('thirdpartylibs/fusioncharts/Charts/');
        $chart->encodeXMLChars(false);

        // x-axis label
        $chart->setChartParam('xAxisName', $report->getXAxisLabel());
        // general chart styles/settings
        $chart->setChartParam('bgSWFAlpha', '50');
        $chart->setChartParam('showValues', '0');
        $chart->setChartParam('bgColor', 'e3efff');
        $chart->setChartParam('exportEnabled', '1');
        $chart->setChartParam('exportAtClient', '0');
        
        if (get_class($report) != 'src\\reports\\model\\report\\TrafficLightChart')
        {
            $chart->setChartParam('exportAction', 'save');
            $chart->setChartParam('labelDisplay', 'Rotate');
        }

        $chart->setChartParam('exportHandler', 'thirdpartylibs/fusioncharts/ExportHandlers/FCExporter.php');
        $chart->setChartParam('html5ExportHandler', 'thirdpartylibs/fusioncharts/ExportHandlers/index.php');
        $chart->setChartParam('registerWithJS', '1');
        $chart->setChartParam('exportFileName', 'DATIXWebReport');
        $chart->setChartParam('exportShowMenuItem', '0');
        $chart->setChartParam('ShowPrintMenuItem', '0');
        $chart->setChartParam('XTLabelmanagement', '0');
        $chart->setChartParam('labelDisplay', 'rotate');
        $chart->setChartParam('slantLabels', '1');
        $chart->setChartParam('manageLabelOverflow', '1');

        // disable animation non-Flash charts because of display issues when exporting to PDF
        if ($_SESSION['FlashAvailable'] === false || $this->context == ReportWriter::DASHBOARD)
        {
            $chart->setChartParam('animation', '0');
        }
        
        // create a lighter font for the the subcaption
        $chart->defineStyle('SubCap', 'font', 'color=b3b3b3');
        $chart->applyStyle('SUBCAPTION', 'SubCap');

        $chart->defineStyle('dataLabels', 'font', 'size=9');
        $chart->applyStyle('DATALABELS', 'dataLabels');

        // handle currency prefix for when doing a count/sum of a money field
        $FieldDefs = $this->registry->getFieldDefs();

        $countField = '';
        $countFieldName = '';

        switch ($report->count_style)
        {
            case Report::COUNT_AVE:
                $countFieldName = $report->count_style_field_average;
                break;
            case Report::COUNT_SUM:
                $countFieldName = $report->count_style_field_sum;
                break;
        }

        $countField = $FieldDefs[$countFieldName];

        if (
            (!is_null($countField) && $countField->getType() == FieldInterface::MONEY)
            || substr($countFieldName, 0, 20) == 'claims_main.pay_type')
        {
            $chart->setChartParam('numberPrefix', $this->registry->getParm('CURRENCY_CHAR', '£'));
            $chart->setChartParam('decimals', '2');
            $chart->setChartParam('forceDecimals', '1');
        }
    }

    /**
     * Adds common chart properties which are specific to the context in which the chart is being rendered.
     *
     * @param \FusionChartsWidgets $chart   The chart object.
     * @param Report               $report  The report object.
     * @param int                  $context The context in which the report is being rendered.
     * @param null                 $title
     */
    protected function addContextSpecificProperties(\FusionChartsWidgets $chart, Report $report, $context, $title = null)
    {
        global $scripturl;

        switch ($context)
        {
            case ReportWriter::FULL_SCREEN:
                $chart->setChartParam('caption', htmlspecialchars($title ?: $report->title, ENT_QUOTES));
                $chart->setChartParam('showLegend', '1');
                $chart->defineStyle('myCaptionFont', 'font', 'size=14');
                $chart->applyStyle('Caption', 'myCaptionFont');
                break;
                
            case ReportWriter::DASHBOARD:
                $chart->setChartParam('showLegend', '0');
                $chart->setChartParam('clickURL', $scripturl.'?action=reportdesigner&designer_state=closed&recordid='.$_SESSION['CurrentReport']->recordid.(isset($_REQUEST['dbid']) && $_REQUEST['dbid'] != '' ? '&dash_id='.\Sanitize::SanitizeString($_REQUEST['dbid']).'&token='.\CSRFGuard::getCurrentToken() : ''));
                break;
        }
    }

    /**
     * Adds the footer as a subtitle to the FusionChart.
     *
     * @param \FusionChartsWidgets $chart The chart object.
     */
    protected function addExportSpecificProperties(\FusionChartsWidgets $chart)
    {
        if (isset($_SESSION['reportExport']) && $_SESSION['reportExport'] == '1')
        {
            $subCaption = (isset($GLOBALS['dtxpdf_footer']) ? strip_tags($GLOBALS['dtxpdf_footer']) : strip_tags($GLOBALS['dtxprint_footer']));

            if ($subCaption != '')
            {
                $chart->setChartParam('subCaption', $subCaption);
            }
        }

        unset($_SESSION['reportExport']);
    }

    /**
     * Add data to chart.
     * 
     * Methods for adding rows only and rows & columns data to a FusionChart object are (annoyingly) significantly different
     * that the two are completely separated here.
     *
     * @param \FusionChartsWidgets $chart
     * @param array                $data
     * @param string               $additionalParameters A string of parameters to pass with each data group.
     * @param mixed				   $nullValue
     */
    protected function addData(\FusionChartsWidgets $chart, array $data, $additionalParameters = '', $nullValue = 0, Report $report = null)
    {
        if (empty($data['columnHeaders']))
        {
            // rows only
            foreach ($data['rowHeaders'] as $code => $header)
            {
                $chart->addCategory($this->getLabel($header), "hoverText=$header");
                $parameters = $additionalParameters
                    .'label='.$this->getLabel($header).';'
					.'toolText='. $data['toolText'][$code] .';'
                    .'link='.$data['links']['values'][$code];    
                $chart->addChartData($data[ $report instanceof Report && $report->percentages == Report::TOOLTIP_PERCENTAGE_ONLY ? 'percentages' : 'data' ][$code] ?: $nullValue, $parameters);
            }
        }
        else
        {
            // rows and columns
            foreach ($data['rowHeaders'] as $rowHeader)
            {
                $chart->addCategory($this->getLabel($rowHeader), "hoverText=$rowHeader");
            }
            
            foreach ($data['columnHeaders'] as $columnCode => $columnHeader)
            {
                $chart->addDataset($columnHeader);
                foreach (array_keys($data['rowHeaders']) as $rowCode)
                {
                    $parameters = $additionalParameters
                        .'label='.$this->getLabel($columnHeader).';'
                        .'toolText='. $data['toolText'][$rowCode][$columnCode] .';'
                        .'link='.$data['links']['values'][$rowCode][$columnCode];
                    $chart->addChartData($data[ $report instanceof Report && $report->percentages == Report::TOOLTIP_PERCENTAGE_ONLY ? 'percentages' : 'data' ][$rowCode][$columnCode] ?: $nullValue, $parameters);
                }
            }
        }
    }
    
    /**
     * Add data to a stacked bar chart
     *
     * @author gszucs
     * @param \FusionChartsWidgets $chart
     * @param array $data
     * @param Report $report
     */
    protected function addBarData(\FusionChartsWidgets $chart, array $data, Report $report)
    {
    	if ($report->columns == '' && $report->style == BarChart::STACKED)
    	{
    		$chart->addCategory('');
    		foreach ($data['rowHeaders'] as $code => $header)
    		{
    			$chart->addDataset($header);
    			$parameters = $additionalParameters
    				.'label='.$this->getLabel($header).';'
    				.'toolText='. $data['toolText'][$code] .';'
    				.'link='.$data['links']['values'][$code];
    			$chart->addChartData($data[ $report->percentages == Report::TOOLTIP_PERCENTAGE_ONLY ? 'percentages' : 'data' ][$code] ?: null, $parameters);
    		}
    	}
    	else
    	{
    		$this->addData($chart, $data, '', null, $report);
    	}
    }

    /**
     * Adds data to a Pareto chart.
     *
     * @param \FusionChartsWidgets $chart
     * @param array $data
     */
    protected function addParetoData(\FusionChartsWidgets $chart, array $data)
    {
        $rowHeaders = [];
        $rowData    = $data['data'];
        
        arsort($rowData);
        
        foreach (array_keys($rowData) as $rowCode)
        {
            $rowHeaders[$rowCode] = $data['rowHeaders'][$rowCode];
        }
        
        $data['rowHeaders'] = $rowHeaders;
        
        $this->addData($chart, $data);

        $chart->addDataset(_tk('cumulative_data'), 'parentYAxis=S;color=ff0000;renderAs=Line;');
        $paretoDatasum = 0;

        foreach ($data['rowHeaders'] as $code => $header)
        {
            $paretoDatasum += $data['data'][$code];
            $chart->addChartData($paretoDatasum);
        }
    }

    /**
     * Adds data to a Gauge chart.
     *
     * @param \FusionChartsWidgets $chart
     * @param array $data
     */
    protected function addGaugeData(\FusionChartsWidgets $chart, array $data, GaugeChart $report)
    {
        $rearExtension = 50;

        if($data['total'] < 0)
        {
            $lowerLimit = $data['total'];
        }
        else
        {
            $lowerLimit = 0;
        }

        if ($data['total'] > $report->gauge_section3_max)
        {
            $upperLimit = $data['total'];
        }
        else
        {
            $upperLimit = $report->gauge_section3_max;
        }

        $parameter = 'minValue=' . $lowerLimit . ';';
        $parameter .= 'maxValue='.$report->gauge_section1_max.';';
        $parameter .= 'code='.$report->gauge_section1_colour.';';
        $chart->addChartData('', $parameter, '');

        $parameter = 'minValue='.$report->gauge_section1_max.';';
        $parameter .= 'maxValue='.$report->gauge_section2_max.';';
        $parameter .= 'code='.$report->gauge_section2_colour.';';
        $chart->addChartData('', $parameter, '');

        $parameter = 'minValue='.$report->gauge_section2_max.';';
        $parameter .= 'maxValue=' . $upperLimit . ';';
        $parameter .= 'code='.$report->gauge_section3_colour.';';
        $chart->addChartData('', $parameter, '');

        $chart->setChartParam('lowerLimit', $lowerLimit);
        $chart->setChartParam('lowerLimitDisplay', $lowerLimit);
        $chart->setChartParam('upperLimit', $upperLimit);
        $chart->setChartParam('upperLimitDisplay', $upperLimit);

        $parameter = 'value='.$data['total'].';';
        $parameter .= 'rearExtension='.$rearExtension.';';
        $chart->addGaugeDialData($parameter);
    }

    /**
     * Adds data to a Pie chart.
     *
     * @param \FusionChartsWidgets $chart
     * @param array $data
     */
    protected function addPieData(\FusionChartsWidgets $chart, array $data, Report $report)
    {
        $data['data'] = array_reverse($data['data'], true);
        $data['rowHeaders'] = array_reverse($data['rowHeaders'], true);
  
        if ($report->style == PieChart::EXPLODED)
        {
            $sliceString = 'isSliced=1;';
        }

        $this->addData($chart, $data, $sliceString);
    }

    /**
     * Adds data to a Line chart.
     *
     * @param \FusionChartsWidgets $chart
     * @param array $data
     */
    protected function addLineData(\FusionChartsWidgets $chart, array $data,Report $report)
    {
    	// if rows and columns are populated, we need to use multi value tooltip
    	if (!empty($data['columnHeaders']))
    	{
    		$chart->setChartParam('unescapeLinks', '0');
    		
    		foreach( $data[ $report instanceof Report && $report->percentages == Report::TOOLTIP_PERCENTAGE_ONLY ? 'percentages' : 'data' ] as $rowCode => $columnData)
    		{
    			// try to figure out there are multiple values
    			$temp = array();
    			foreach( $columnData as $columnCode => $value )
    			{
    				$temp[ (string) $value ][] = $columnCode;
    			}
    			
    			foreach ($temp as $columCodes)
    			{
    				// if there are multiple values we change the values of the link and toolText keys
    				// http://docs.fusioncharts.com/charts/contents/DrillDown/JavaScript.html
    				// e.g.: link='j-customReportTooltip-<a href="">value</a><br><a href="">value</a>'
    				if( count( $columCodes ) > 1)
    				{
    					$newLinkValue = 'j-customReportTooltip-';
    					foreach ($columCodes as $colCode)
    					{
    						// IE HACK: IE doesn't like these characters: '<', '>'. It will throw an error with 'Invalid data' message. We use '{' and '}'. On the client side they will be replaced with '<' and '>'.
                            if ($report instanceof RiskTrackerReport)
                            {
                                $newLinkValue .= $data['toolText'][$rowCode][$colCode].'{br}';
                            }
                            else
                            {
	    					    $newLinkValue .= '{a href='. $data['links']['values'][$rowCode][$colCode] .'}'. $data['toolText'][$rowCode][$colCode] .'{/a}{br}';
                            }
	    					$data['toolText'][$rowCode][$colCode] = _tk('linegraph_tooltip_multiple');    					
    					}
    					
    					// apply the new link text to all column codes
    					foreach ($columCodes as $colCode)
    					{
    						$data['links']['values'][$rowCode][$colCode] = $newLinkValue;
    					}
    					
    				}
    			}
    		}
    	}
    	
        $this->addData($chart, $data, '', 0, $report);
    }

    protected function addSpcData(\FusionChartsWidgets $chart, array $data, SpcChart $report)
    {
        $upperControlLimit = false;
        $lowerControlLimit = false;

        $data = $this->reorderDataArray($data);

        $data = $report->addCalculatedDataToArray($data);

        $this->addData($chart, $data, '', 0, $report);

        switch ($report->getType())
        {
            case SpcChart::C_CHART:
                $startValue = $data['calculated']['average'];
                $chart->addTrendLine("startValue=$startValue;displayvalue=Mean;color=008800;");
                $upperControlLimit = $data['calculated']['upper_ctrl_limit'];
                $chart->addTrendLine("startValue=$upperControlLimit;displayvalue=UCL;color=ff0000;");
                $lowerControlLimit = $data['calculated']['lower_ctrl_limit'];
                $chart->addTrendLine("startValue=$lowerControlLimit;displayvalue=LCL;color=ff0000;");
                $startValue = round($data['calculated']['upper_warning_limit']);
                $chart->addTrendLine("startValue=$startValue;displayvalue=UWL;color=FF7A00");
                $startValue = $data['calculated']['lower_warning_limit'];
                $chart->addTrendLine("startValue=$startValue;displayvalue=LWL;color=FF7A00");
                break;

            case SpcChart::I_CHART:
                $startValue = $data['calculated']['average'];
                $chart->addTrendLine("startValue=$startValue;displayvalue=Mean;color=008800;");
                $upperControlLimit = $data['calculated']['upper_ctrl_limit'];
                $chart->addTrendLine("startValue=$upperControlLimit;displayvalue=UCL;color=ff0000;");
                $lowerControlLimit = $data['calculated']['lower_ctrl_limit'];
                $chart->addTrendLine("startValue=$lowerControlLimit;displayvalue=LCL;color=ff0000;");
                break;

            case SpcChart::MOVING_RANGE:
                $startValue = $data['calculated']['average'];
                $chart->addTrendLine("startValue=$startValue;displayvalue=Mean;color=008800;");
                $upperControlLimit = $data['calculated']['upper_ctrl_limit'];
                $chart->addTrendLine("startValue=$upperControlLimit;displayvalue=RUCL");
                break;

            case SpcChart::RUN_CHART:
                $startValue = $data['calculated']['average'];
                $chart->addTrendLine("startValue=$startValue;displayvalue=Mean;color=008800;");
                break;
        }

        // In case of SPC c-Chart types we need to explicitly set the chart limits because of the trend lines
        // Setting to the nearest multiple of 10
        if ($lowerControlLimit !== false)
        {
            $chart->setChartParam('yAxisMinValue', round(($lowerControlLimit - 10 / 2) / 10) * 10);
        }

        if ($upperControlLimit !== false)
        {
            $chart->setChartParam('yAxisMaxValue', round(($upperControlLimit + 10 / 2) / 10) * 10);
        }
    }

    /**
     * Adds data to a Traffic Light chart.
     *
     * @param \FusionChartsWidgets $chart
     * @param array $data
     * @param TrafficLightChart $report
     */
    protected function addTrafficLightData(\FusionChartsWidgets $chart, array $data, TrafficLightChart $report)
    {
        $lowerLimit = 0;
        $upperLimit = ceil($data['totals']['total']);
        $prevValue = $lowerLimit;
        $sumValue = 0;
        $defaultColours = $report->getDefaultColours();
        $aTrafficColour = $report->trafficCodeColours;

        foreach ($data['rowHeaders'] as $code => $value)
        {
            $sumValue += $data['data'][$code];

            $parameter = 'minValue='.$prevValue.';';
            $parameter .= 'maxValue='.$sumValue.';';

            if (isset($aTrafficColour[$code]))
            {
                $parameter .= 'code='.$aTrafficColour[$code].';';
            }
            else
            {
                // assign default colour to this code
                if (empty($defaultColours))
                {
                    // if all default colours already assigned
                    $defaultColours = $report->getDefaultColours();
                }

                $parameter .= 'code='.array_shift($defaultColours).';';
            }

            $trafficLightSegments[] = array(
                'description' => $data['rowHeaders'][$code],
                'colour' => $aTrafficColour[$code],
                'link' => $data['links']['values'][$code]
            );

            $chart->addChartData('', $parameter, '');
            $prevValue = $sumValue;
        }

        $chart->setChartParam('lowerLimit', $lowerLimit);
        $chart->setChartParam('upperLimit', $upperLimit);

        $report->setTrafficLightSegments($trafficLightSegments);
    }

    /**
     * Instantiates and returns a basic FusionChart object.
     * 
     * @param string $type
     * 
     * @return FusionChartsWidgets
     */
    protected function getChartObject($type)
    {
        return new \FusionChartsWidgets($type, '100%', '100%', $this->chartID, true);
    }
    
    /**
     * Constructs and returns a BarChart object.
     * 
     * @param BarChart $report  The report object.
     * @param int      $context The context in which the report is being rendered.
     * 
     * @return FusionChartsWidgets
     */
    protected function getBarChart(BarChart $report, $context)
    {
        if ($report->style == BarChart::STACKED)
        {
            if ($report->orientation == BarChart::HORIZONTAL)
            {
                $type = 'stackedbar2d';
            }
            else
            {
                $type = 'stackedcolumn2d';
            }
        }
        else
        {
            if ($report->orientation == BarChart::HORIZONTAL)
            {
                $type = $report->columns != '' ? 'msbar2d' : 'bar2d';
            }
            else
            {
                $type = $report->columns != '' ? 'mscolumn2d' : 'column2d';
            }
        }

        $chart = $this->getChartObject($type);

        //$chart->setChartParam('maxLabelWidthPercent', '20');
        
        switch ($context)
        {
            case ReportWriter::FULL_SCREEN:
                $chart->setChartParam('chartRightMargin', '60');
                $chart->setChartParam('chartLeftMargin', '60');
                break;
                
            case ReportWriter::DASHBOARD:
                $chart->setChartParam('chartRightMargin', '22');
                $chart->setChartParam('chartLeftMargin', '22');
                break;
        }

        return $chart;
    }

    /**
     * Constructs and returns a ParetoChart object.
     *
     * @param ParetoChart $report  The report object.
     * @param int         $context The context in which the report is being rendered.
     *
     * @return \FusionChartsWidgets
     */
    protected function getParetoChart(ParetoChart $report, $context)
    {
        $type = 'mscombi2d';
        $chart = $this->getChartObject($type);

        if ($context == ReportWriter::FULL_SCREEN)
		{
            $chart->setChartParam('chartRightMargin', '50');
        }
		
        return $chart;
    }

    /**
     * Constructs and returns a ParetoChart object.
     *
     * @param ParetoChart $report  The report object.
     * @param int         $context The context in which the report is being rendered.
     *
     * @return \FusionChartsWidgets
     */
    protected function getGaugeChart(GaugeChart $report, $context)
    {
        $fieldDef = $this->registry->getFieldDefs();

        $type = 'angulargauge';
        $chart = $this->getChartObject($type);

        if ($context == ReportWriter::FULL_SCREEN)
		{
            $chart->setChartParam('chartRightMargin', '50');
            $chart->setChartParam('caption', $report->title);
            $chart->setChartParam('showLegend', '1');
        }

        if ($context == ReportWriter::DASHBOARD)
        {
            $chart->setChartParam('showLegend', '0');
        }

        if ($report->count_style_field_sum && $fieldDef[$report->count_style_field_sum]->getType() == FieldInterface::MONEY)
        {
            $chart->setChartParam('numberPrefix', GetParm('CURRENCY_CHAR', '£'));
        }

        $upperLimit = '100';

        $chart->setChartParam('exportAction', 'download');
        $chart->setChartParam('gaugeStartAngle', '180');
        $chart->setChartParam('gaugeEndAngle', '0');
        $chart->setChartParam('showValue', '0');
        $chart->setChartParam('chartBottomMargin', '70');
        $chart->setChartParam('exportShowMenuItem', '0');

        return $chart;
    }

    /**
     * Constructs and returns a SpcChart object.
     *
     * @param SpcChart $report The report object.
     * @param int      $context The context in which the report is being rendered.
     *
     * @return \FusionChartsWidgets
     */
    protected function getSpcChart(SpcChart $report, $context)
    {
        $type = 'line';
        $chart = $this->getChartObject($type);

        return $chart;
    }
    
    /**
     * Constructs and returns a LineChart object.
     *
     * @param LineChart $report  The report object.
     * @param int       $context The context in which the report is being rendered.
     *
     * @return FusionChartsWidgets
     */
    protected function getLineChart(LineChart $report, $context)
    {
        $type = $report->columns != '' ? 'msline' : 'line';
        $chart = $this->getChartObject($type);

        if ($context == ReportWriter::FULL_SCREEN) {
            $chart->setChartParam('chartRightMargin', '50');
        }

        return $chart;
    }
    
     /**
     * Constructs and returns a LineChart object representing the custom Risk Tracker chart.
     *
     * @param LineChart $report  The report object.
     * @param int       $context The context in which the report is being rendered.
     *
     * @return FusionChartsWidgets
     */
    protected function getRiskTrackerChart(RiskTrackerReport $report, $context)
    {
        $type = $report->columns != '' ? 'msline' : 'line';
        $chart = $this->getChartObject($type);

        if ($context == ReportWriter::FULL_SCREEN) {
            $chart->setChartParam('chartRightMargin', '50');
        }

        $yAxisValueArray = $report->getyAxisValueArray();
        if (count($yAxisValueArray) > 0)
        {
            // use trend lines to display custom values on the Y axis
            $chart->setChartParam('showYAxisValues','0');

            // use trend lines to display custom values on the Y axis
            $chart->setChartParam('numDivLines', count($report->getyAxisValueArray()));

            foreach ($report->getyAxisValueArray() as $value => $description)
            {
                $chart->addTrendLine('startValue='.$value.';displayvalue='.$description.';alpha=0');
            }
        }

        $chart->setChartParam('yAxisName', $report->yAxisName);
        return $chart;
    }

     /**
     * Constructs and returns a PieChart object.
     *
     * @param PieChart $report  The report object.
     * @param int      $context The context in which the report is being rendered.
     *
     * @return \FusionChartsWidgets
     */
    protected function getPieChart(PieChart $report, $context)
    {
        $chart = $this->getChartObject('pie2d');

        $chart->setChartParam('startingAngle', '90');
        $chart->setChartParam('startingAngle', '90');
        $chart->setChartParam('enableRotation', '0');

        switch ($context)
        {
            case ReportWriter::FULL_SCREEN:
                $chart->setChartParam('chartRightMargin', '60');
                $chart->setChartParam('chartLeftMargin', '60');
                break;

            case ReportWriter::DASHBOARD:
                $chart->setChartParam('chartRightMargin', '22');
                $chart->setChartParam('chartLeftMargin', '22');
                $chart->setChartParam('showLabels', '0');
                break;
        }

        return $chart;
    }

    /**
     * Constructs and returns a TrafficLightChart object.
     *
     * @param TrafficLightChart $report  The report object.
     * @param int               $context The context in which the report is being rendered.
     * @param array             $data    The report data.
     *
     * @return FusionChartsWidgets
     */
    protected function getTrafficLightChart(TrafficLightChart $report, $context, array $data)
    {
        $chart = $this->getChartObject('hlineargauge');

        $chart->setChartParam('tickValueDistance', '20');
        $chart->setChartParam('chartRightMargin', '50');
        $chart->setChartParam('chartLeftMargin', '50');
        $chart->setChartParam('chartTopMargin', '20');
        $chart->setChartParam('exportAction', 'download');
        $chart->setChartParam('labelDisplay', 'WRAP');
        $chart->defineStyle('labelFont', 'font', 'bgColor=F1F7FF;borderColor=000000;size=10;');
        $chart->applyStyle('GAUGELABELS', 'labelFont');

        return $chart;
    }
	
	/**
     * This function reorders the data array based on the rowHeaders one because it's the one used in the
     * addData function to add the data to the chart.
     * This is needed because SPC charts needs to manipulate the data array.
     *
     * @param array $data The data of the chart.
     *
     * @return array
     */
    protected function reorderDataArray(array $data)
    {
        $auxData = array();

        foreach ($data['rowHeaders'] as $code => $value)
        {
            $auxData[$code] = $data['data'][$code];
        }

        $data['data'] = $auxData;

        return $data;
    }

    protected function getLabel($rowLabel)
    {
        if ($this->context == ReportWriter::DASHBOARD)
        {
            $BaseRowTitleLenLimit = 10;
            $MaxRowTitleLimit = 20;
        }
        else
        {
            $BaseRowTitleLenLimit = 140;
            $MaxRowTitleLimit = 40;
        }

        if ($this->reportType == 'src\\reports\\model\\report\\BarChart' && $this->report->orientation == BarChart::HORIZONTAL)
        {
            if (\UnicodeString::strlen($rowLabel) > $MaxRowTitleLimit)
            {
                //$rowLabel = \UnicodeString::substr($rowLabel, 0, ($MaxRowTitleLimit - 3) ) . "...";
                $rowLabel = \UnicodeString::substr($rowLabel, 0, ($MaxRowTitleLimit - 3) ) . "...";
            }
        }
        elseif ($this->reportType == 'src\\reports\\model\\report\\BarChart' ||  $this->reportType == 'src\\reports\\model\\report\\SpcChart' || $this->reportType == 'src\\reports\\model\\report\\ParetoChart' || $this->reportType == 'src\\reports\\model\\report\\LineChart')
        {
            if (\UnicodeString::strlen($rowLabel) > $MaxRowTitleLimit)
            {
                $rowLabel = \UnicodeString::substr($rowLabel, 0, ($MaxRowTitleLimit - 3) ) . "...";
            }
        }
        elseif ($this->reportType == 'src\\reports\\model\\report\\PieChart')
        {
            $RowTitleLenLimit = 28;

            if (\UnicodeString::strlen($rowLabel) > $RowTitleLenLimit)
            {
                $rowLabel = \UnicodeString::substr($rowLabel, 0, ($RowTitleLenLimit - 3) ) . "...";
            }
        }

        return $rowLabel;
    }

    public function getChartId()
    {
        return $this->chartID;
    }
}
