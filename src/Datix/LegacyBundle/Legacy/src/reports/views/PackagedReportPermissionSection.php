<script language="JavaScript">
    function UnlinkSelectedUsers()
    {
        var urlGroups = '';
        var groups = document.getElementsByName('cbSelectUser');

        jQuery.each(jQuery('input[type=checkbox][name=cbSelectUser]:checked'), function(i, checkbox) {urlGroups += '&link_id[]=' + checkbox.value;});

        if (urlGroups != '')
        {
            urlGroups = urlGroups.substr(1);
            if (confirm('Remove selected user(s) from record?') == false)
            {
                return;
            }

            var oAjaxCall = new Ajax.Request(
                scripturl + '?action=editrecordperms&type=user&module=REP&delete=1&token='+token,
                {
                    method: 'post',
                    postBody: urlGroups,
                    onSuccess: function(r)
                    {
                        location.reload();
                    },

                    onFailure: function()
                    {
                        alert('Error occurred trying to remove selected users from record.');
                    }
                }
            );

        }
        else
        {
            alert('No user selected.');
        }

    }
    function UnlinkSelectedGroups()
    {
        var urlGroups = '';
        var groups = document.getElementsByName('cbSelectGroup');

        jQuery.each(jQuery('input[type=checkbox][name=cbSelectGroup]:checked'), function(i, checkbox) {urlGroups += '&link_id[]=' + checkbox.value;});

        if (urlGroups != '')
        {
            urlGroups = urlGroups.substr(1);
            if (confirm('Remove selected group(s) from record?') == false)
            {
                return;
            }

            var oAjaxCall = new Ajax.Request(
                scripturl + '?action=editrecordperms&type=group&module=REP&delete=1&token='+token,
                {
                    method: 'post',
                    postBody: urlGroups,
                    onSuccess: function(r)
                    {
                        location.reload();
                    },

                    onFailure: function()
                    {
                        alert('Error occurred trying to remove selected groups from record.');
                    }
                }
            );
        }
        else
        {
            alert('No group selected.');
        }

    }
    function UnlinkSelectedProfiles()
    {
        var urlProfiles = '';
        var profiles = document.getElementsByName('cbSelectProfile');

        jQuery.each(jQuery('input[type=checkbox][name=cbSelectProfile]:checked'), function(i, checkbox) {urlProfiles += '&link_id[]=' + checkbox.value;});

        if (urlProfiles != '')
        {
            urlProfiles = urlProfiles.substr(1);
            if (confirm('Remove selected profile(s) from record?') == false)
            {
                return;
            }

            var oAjaxCall = new Ajax.Request(
                scripturl + '?action=editrecordperms&type=profile&module=REP&delete=1&token='+token,
                {
                    method: 'post',
                    postBody: urlProfiles,
                    onSuccess: function(r)
                    {
                        location.reload();
                    },

                    onFailure: function()
                    {
                        alert('Error occurred trying to remove selected profiles from record.');
                    }
                }
            );
        }
        else
        {
            alert('No profile selected.');
        }

    }
</script>

<!--<input type="hidden" name="record_permissions_url" value="" />
<input type="hidden" name="url_from" value="' . filter_var($url_from, FILTER_SANITIZE_URL) . '" />-->
<li>
    <table id="tblUsers" class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <?php if (count($this->userCollection) == 0): ?>
        <tr>
            <td class="rowtitlebg" colspan="8">No linked users.</td>
        </tr>
    <?php else: ?>
        <tr>
            <td class="windowbg" width="1%">
                <input type="checkbox" id="cbSelectAllUsers" name="cbSelectAllUsers" onclick="jQuery('input[type=checkbox][name=cbSelectUser]').attr('checked', this.checked);" />
            </td>
            <td>
                <b>Users</b>
            </td>
        </tr>

        <?php foreach($this->userCollection as $user): ?>

        <tr>
            <td width="12px">
                <input type="checkbox" name="cbSelectUser" value="<?php echo $user->recordid; ?>"
                       onclick="if(jQuery('input[type=checkbox][name=cbSelectUser]').not(':checked').length == 0){jQuery('#cbSelectAllUsers').attr('checked', true)}else{jQuery('#cbSelectAllUsers').attr('checked', false)}" />
            </td>
            <td class="rowtitlebg">
                <a href="javascript:if(CheckChange()){SendTo('app.php?action=edituser&recordid=<?php echo $user->recordid ?>')}">
                    <?php echo htmlspecialchars(\UnicodeString::trim($user->con_surname.', '.$user->con_forenames.' '.$user->con_title)) ?>
                    </a>
            </td>
        </tr>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if (count($this->userCollection) > 0): ?>
        <tr id="row_remove_selected_users">
            <td class="windowbg2" colspan="5">
                <a href="javascript:if(CheckChange()){UnlinkSelectedUsers()}">
                    <b>Remove selected user(s) from record</b>
                </a>
            </td>
        </tr>
    <?php endif; ?>

        <tr>
            <td class="new_link_row" colspan="8">
                <a href="JavaScript:javascript:if(CheckChange()){SendTo('app.php?action=editrecordperms&type=user&module=REP&table_name=web_packaged_reports&link_id=<?php echo $this->packagedReport->recordid; ?>')}">
                    <b>Add new user</b></a>
            </td>
        </tr>


        <?php if (count($this->groupCollection) == 0): ?>
        <tr>
            <td class="rowtitlebg" colspan="8">No linked groups.</td>
        </tr>
    <?php else: ?>
        <tr>
            <td class="windowbg" width="1%">
                <input type="checkbox" id="cbSelectAllGroups" name="cbSelectAllGroups" onclick="jQuery('input[type=checkbox][name=cbSelectGroup]').attr('checked', this.checked);" />
            </td>
            <td>
                <b>Security Groups</b>
            </td>
        </tr>

        <?php foreach($this->groupCollection as $group): ?>

            <tr>
                <td width="12px">
                    <input type="checkbox" name="cbSelectGroup" value="<?php echo $group->recordid; ?>"
                           onclick="if(jQuery('input[type=checkbox][name=cbSelectGroup]').not(':checked').length == 0){jQuery('#cbSelectAllGroups').attr('checked', true)}else{jQuery('#cbSelectAllGroups').attr('checked', false)};" />
                </td>
                <td class="rowtitlebg">
                    <a href="javascript:if(CheckChange()){SendTo('app.php?action=editgroup&recordid=<?php echo $group->recordid ?>')}">
                        <?php echo htmlspecialchars($group->grp_code); ?>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if (count($this->groupCollection) > 0): ?>
        <tr id="row_remove_selected_groups">
            <td class="windowbg2" colspan="5">
                <a href="javascript:if(CheckChange()){UnlinkSelectedGroups()}" >
                    <b>Remove selected group(s) from record</b>
                </a>
            </td>
        </tr>
    <?php endif; ?>

        <tr>
            <td class="new_link_row" colspan="8">
                <a href="JavaScript:javascript:if(CheckChange()){SendTo('app.php?action=editrecordperms&type=group&module=REP&table_name=web_packaged_reports&link_id=<?php echo $this->packagedReport->recordid; ?>')}">
                    <b>Add new group</b></a>
            </td>
        </tr>

    <?php if (count($this->profileCollection) == 0): ?>
        <tr>
            <td class="rowtitlebg" colspan="8">No linked profiles.</td>
        </tr>
    <?php else: ?>
        <tr>
            <td class="windowbg" width="1%">
                <input type="checkbox" id="cbSelectAllProfiles" name="cbSelectAllProfiles" onclick="jQuery('input[type=checkbox][name=cbSelectProfile]').attr('checked', this.checked);" />
            </td>
            <td>
                <b>Profiles</b>
            </td>
        </tr>

        <?php foreach($this->profileCollection as $profile): ?>

            <tr>
                <td width="12px">
                    <input type="checkbox" name="cbSelectProfile" value="<?php echo $profile->recordid; ?>"
                           onclick="if(jQuery('input[type=checkbox][name=cbSelectProfile]').not(':checked').length == 0){jQuery('#cbSelectAllProfiles').attr('checked', true)}else{jQuery('#cbSelectAllProfiles').attr('checked', false)};" />
                </td>
                <td class="rowtitlebg">
                    <a href="javascript:if(CheckChange()){SendTo('app.php?action=editprofile&recordid=<?php echo $profile->recordid ?>')}">
                        <?php echo htmlspecialchars($profile->pfl_name); ?>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>

        <?php if (count($this->profileCollection) > 0): ?>
            <tr id="row_remove_selected_profiles">
                <td class="windowbg2" colspan="5">
                    <a href="javascript:if(CheckChange()){UnlinkSelectedProfiles()}" >
                        <b>Remove selected profile(s) from record</b>
                    </a>
                </td>
            </tr>
        <?php endif; ?>

        <tr>
            <td class="new_link_row" colspan="8">
                <a href="JavaScript:javascript:if(CheckChange()){SendTo('app.php?action=editrecordperms&type=profile&module=REP&table_name=web_packaged_reports&link_id=<?php echo $this->packagedReport->recordid; ?>')}">
                    <b>Add new profile</b></a>
            </td>
        </tr>


    </table>
</li>