Options: <br/><br/>
<table style='border-width: 1px;border-spacing: 0px;border-style: none none none none;border-color: white white white white;border-collapse: collapse;' >
    <tr>
        <td style='border-width: 1px;padding: 3px;border-style: inset inset inset inset;border-color: white white white white;width: 350px;'>
            <div id="selectPDF">
            <input type='radio' name='reportoutputformat' value='pdf' checked
                <?php if ($this->extraOptions): ?>
                   onclick="jQuery('#orientation_row').show();jQuery('#papersize_row').show()";
            <?php endif; ?>
            > PDF
            </div>

            <?php if (!$this->noExcel): ?>
                <div id="selectExcel">
                    <input type='radio' name='reportoutputformat' value='excel'
                    <?php if ($this->extraOptions): ?>
                        onclick="jQuery('#orientation_row').hide();jQuery('#papersize_row').hide()"
                    <?php endif; ?>
                    > Excel
                </div>
            <?php endif ?>

        </td>
    </tr>

    <?php if ($this->extraOptions): ?>
        <tr id="orientation_row">
            <td style='border-width: 1px;padding: 3px;border-style: inset inset inset inset;border-color: white white white white;width: 200px;'>
                <input type='radio' id='orientation1' name='orientation' value='portrait' <?= (GetParm('DEFAULT_PAPER_ORIENTATION', 'P') == 'P' ? ' checked' : '')?> > Portrait<br>
                <input type='radio' id='orientation2' name='orientation' value='landscape' <?= (GetParm('DEFAULT_PAPER_ORIENTATION', 'P') == 'L' ? ' checked' : '')?> > Landscape<br>
            </td>
        </tr>

        <tr id="papersize_row">
            <td style='border-width: 1px;padding: 3px;border-style: inset inset inset inset;border-color: white white white white;width: 200px;'>
                &nbsp; Paper size: &nbsp;<select id='papersize' name='papersize'>
                    <option value='a4'<?= (GetParm('DEFAULT_PAPER_SIZE', 'A4') == 'A4' ? ' selected' : '')?> >A4</option>
                    <option value='a3'<?= (GetParm('DEFAULT_PAPER_SIZE', 'A4') == 'A3' ? ' selected' : '')?> >A3</option>
                    <option value='letter'<?= (GetParm('DEFAULT_PAPER_SIZE', 'A4') == 'LETTER' ? ' selected' : '')?> >Letter</option>
                </select>
            </td>
        </tr>
    <?php endif; ?>
</table>

<script type="text/javascript" src="js_functions/AdobeFlashDetection.js"></script>
<script type="text/javascript">
    //IE 11 does not work with pdf report generation when flash is disabled -CA 05/02/2015
    if(browser.isIE && browser.version >= 11 && GetSwfVer() < 0)
    {
        jQuery('#selectPDF').html('Please install flash to enable the export of reports in PDF format.');
        jQuery('input[name=\'reportoutputformat\']').prop('checked', true);
    }

</script>
