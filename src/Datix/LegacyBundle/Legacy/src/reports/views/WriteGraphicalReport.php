<?php if( $this->reportType == 'src\reports\model\report\LineChart'): ?>
	<script type="text/javascript">
		function customReportTooltip(content)
		{
			content = content.replace( /{/g, '<').replace( /}/g, '>'); // IE hack
		    jQuery("<div></div>")
		        .addClass("customReportTooltipFusion")
		        .css({"left" : mouseX, "top" : mouseY})
		        .html(content)
		        .appendTo(document.body)
		        .bind("mouseleave", function(){
		            jQuery(this).hide();
		        })
		}
		
		var mouseX = 0;
		var mouseY = 0;
		jQuery(document).ready(function(){
		    jQuery(document).mousemove(function(e){
		        mouseX = e.pageX - 10;
		        mouseY = e.pageY - 10;
		    });
		});
	</script>
<?php endif; ?>
<div id="chartContainer" style="height:<?php echo $this->height ?>;">
<?php $this->chart->renderChart(); ?>
</div>
<?php if ($this->reportType == 'src\\reports\\model\\report\\TrafficLightChart' && $this->context != \src\reports\controllers\ReportWriter::DASHBOARD) : ?>
<br/><br/>
<div style="text-align: center;background-color: #F1F7FF;border-top: 1px solid #A9A9A9;border-bottom: 1px solid #A9A9A9;border-left: 1px solid #A9A9A9;border-right: 1px solid #A9A9A9;margin:0 auto;width:99%;padding: 10px 0px;font-size:16px;">
    <img src="<?php echo $this->scripturl; ?>?action=httprequest&type=getanddeletepngfile&pngfile=<?php echo $this->tmpFile; ?>" usemap='#traffic'/>
    <?php if ($this->trafficLightImageMap != '') : ?>
    <?php echo $this->trafficLightImageMap; ?>
    <?php endif; ?>
</div>
<?php endif; ?>