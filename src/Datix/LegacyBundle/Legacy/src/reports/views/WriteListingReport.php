<?php if ($this->isDashboard): ?>
    <div style="cursor:pointer" onclick="SendTo('<?= $this->scripturl . '?action=reportdesigner&designer_state=closed&recordid='.$this->recordid.'&from_dashboard='.$this->dashboardId; ?>');">
<?php elseif ($this->isFullscreen): ?>
    <table id="listing-title" border="0" align="center" cellspacing="1" cellpadding="0" class="bordercolor" width="100%">
        <tr>
            <td style="text-align:center;">
                <span style="font-size:20px;font-weight:bold;margin-left:200px;"><?= $this->escape($this->packagedReport->name); ?></span>
                <span style="float:right; width:200px;"><?= _tk('record_count') ?>: <span id="record_count"></span><span style="display:none; color:red;"><?= _tk('error_retrieving_count')?></span></span>
            </td>
        </tr>
    </table>
<div class="report-viewport">
<?php endif ?>
    <table id="tabletoremove" border="0" align="center" cellspacing="1" cellpadding="0" class="bordercolor" width="100%">
        <tr><td>
            <table border="0" width="100%" cellspacing="1" cellpadding="4" class="bordercolor" id="listingtable">
                <thead>
                <tr valign="top" repeat="1">
                    <?php foreach ($this->packagedReport->report->columns as $column): ?>
                        <th class="tableHeader" width="<?= $column->getWidthAsPercentage($this->lineWidth) ?>%">
                            <?= $column->getTitleWithSuffix($this->packagedReport->report->module) ?>
                        </th>
                    <?php endforeach ?>
                </tr>
                </thead>
                <?= $this->rows ?>
            </table>
            </td></tr>
    </table>
<?php if ($this->isFullscreen || $this->isDashboard): ?>
    </div>
<?php endif ?>