<table class="crosstab" id="crosstabtable">
    <tr>
        <td class="crosstab header"></td>
        <?php foreach ($this->data['columnHeaders'] as $column => $value):?>
        <td class="crosstab header top-header" title="<?php echo htmlspecialchars($value) ?>" onclick="drillDown('col', '<?php echo $column; ?>');"><?php echo htmlspecialchars($value) ?></td>
        <?php endforeach ?>
        <td title="<?php echo _tk('total') ?>" class="crosstab header top-header"><?php echo _tk('total') ?></td>
    </tr>
    <?php foreach ($this->data['rowHeaders'] as $row => $value):?>
	    <tr>
	        <td class="crosstab header" title="<?php echo htmlspecialchars($value) ?>" onclick="drillDown('row', '<?php echo $row; ?>');"><?php echo htmlspecialchars($value) ?></td>
	        <?php foreach ($this->data['columnHeaders'] as $column => $value):?>
		        <td class="crosstab data">
		            <?php if ($this->data['data'][$row][$column] != ''): ?>
		            	<?php if ($this->context != \src\reports\controllers\ReportWriter::DASHBOARD && $this->data['links']): ?>
                            <a href="<?php echo $this->data['links']['values'][$row][$column]; ?>">
                                <?php echo $this->data['toolText'][$row][$column]; ?>
                            </a>
		                <?php else: ?>
                            <?php echo $this->data['toolText'][$row][$column]; ?>
                        <?php endif ?>
		            <?php else: ?>
		            	<?php echo $this->data['toolText']['null']; ?>
		            <?php endif ?>
		        </td>
	        <?php endforeach ?>
	        <td class="crosstab total data">
	            <?php if ($this->data['totals']['rows'][$row] != ''): ?>
                    <?php if ($this->context != \src\reports\controllers\ReportWriter::DASHBOARD && $this->data['links']): ?>
                        <a href="<?php echo $this->data['links']['row_totals'][$row] ?: $this->data['links']['values'][$row]; ?>">
                            <?php echo $this->data['totals']['rows'][$row]; ?>
                        </a>
                    <?php else: ?>
                        <?php echo $this->data['totals']['rows'][$row]; ?>
                    <?php endif ?>
	            <?php else: ?>
	            	<?php echo $this->data['toolText']['null']; ?>
	            <?php endif ?>
	        </td>
	    </tr>
    <?php endforeach ?>
    <tr>
        <td title="<?php echo _tk('total') ?>" class="crosstab header"><?php echo _tk('total') ?></td>
        <?php foreach ($this->data['columnHeaders'] as $column => $value):?>
		    <td class="crosstab total data">
	            <?php if ($this->data['totals']['columns'][$column] != ''): ?>
                    <?php if ($this->context != \src\reports\controllers\ReportWriter::DASHBOARD && $this->data['links']): ?>
                        <a href="<?php echo $this->data['links']['column_totals'][$column]; ?>">
                            <?php echo $this->data['totals']['columns'][$column]; ?>
                        </a>
                    <?php else: ?>
                        <?php echo $this->data['totals']['columns'][$column]; ?>
                    <?php endif ?>
	            <?php else: ?>
	            	<?php echo $this->data['toolText']['null']; ?>
	            <?php endif ?>
	        </td>
        <?php endforeach ?>
        <td class="crosstab total data">
            <?php if ($this->data['totals']['total'] != ''): ?>
                <?php if ($this->context != \src\reports\controllers\ReportWriter::DASHBOARD && $this->data['links']): ?>
                    <a href="<?php echo $this->data['links']['total']; ?>">
                        <?php echo $this->data['totals']['total']; ?>
                    </a>
                <?php else: ?>
                    <?php echo $this->data['totals']['total']; ?>
                <?php endif ?>
            <?php else: ?>
            	<?php echo $this->data['toolText']['null']; ?>
            <?php endif ?>
        </td>
    </tr>
</table>
<script type="text/javascript">
    function drillDown(roworcol, value)
    {
        if (jQuery('#drill').val())
        {
            SendTo('app.php?action=reportdesigner&old_query_id=<?php echo $this->old_query_id; ?>&base_report=<?php echo $this->base_report; ?>&drilldown=<?php echo $_SESSION['crosstab_current_drill_level'] + 1; ?>&rowcol='+roworcol+'&fieldval='+value+'&dropdownval='+jQuery('#drill').val()+'&module=<?php echo $this->module; ?>');
        }
    }
</script>
