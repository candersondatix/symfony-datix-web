<?php 
/**
 * If you're wondering why there's no left indent in this view, it's because IE9 will randomly make certain table cells span two columns
 * when the rows are returned by Ajax and appended to the table.  Removing all whitespace within the td tags seems to prevent this (see 
 * https://social.msdn.microsoft.com/Forums/ie/en-US/28d78780-c95c-4c35-9695-237ebb912d90/ie9-table-layout-apparently-affected-by-whitespace-between-html-elements?forum=iewebdevelopment).
 * 
 * It could be that this is an issue with our version of jQuery (currently 1.71) and that 
 * an upgrade will resolve it.  In the mean time, we'll have to live with a view script that's slightly harder to read.
 */
?>
<?php foreach ($this->data as $row): ?>
<?php if ($row instanceof src\reports\views\helpers\ListingReportGroupHeader):?>
<tr>
<td class="windowbg" colspan="<?php echo $row->getColspan() ?>">
<b><?php if ($this->context != src\reports\controllers\ReportWriter::EXPORT_PDF):?><?php echo str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $row->getIndent()) ?><?php endif ?><?php echo $row->getTitle().': '.$row->getValue() ?></b>
</td>
</tr>
<?php continue ?>
<?php endif ?>
<tr valign="top" style="height:25px">
<?php foreach ($row as $cell): ?>
<?php if (null !== $cell): ?>
<td style="text-align:<?php echo $cell->getAlign() ?>" <?php echo ($cell->hasColour() ? 'bgcolor="#'.htmlspecialchars($cell->getColour()).'"' : 'class="windowbg2"') ?> rowspan="<?php echo $cell->getRowSpan() ?>">
<?php if ($cell->hasLink()): ?>
<a href="<?php echo $cell->getLink() ?>">
<?php endif ?>
<?php echo $cell->getData() ?>
<?php if ($cell->hasLink()): ?>
</a>
<?php endif ?>
</td>
<?php endif ?>
<?php endforeach ?>
</tr>
<?php endforeach ?>