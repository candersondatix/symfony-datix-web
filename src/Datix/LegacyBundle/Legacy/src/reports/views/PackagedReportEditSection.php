<script language="javascript" type="text/javascript">
    function getQueryString(query_id, module)
    {
        new Ajax.Request
        (
            scripturl + '?action=httprequest&type=getquerywhere&responsetype=json',
            {
                method: 'post',
                postBody:
                    '&recordid=' + query_id,
                onSuccess: function(r)
                {
                    var response = r.responseText.evalJSON();
                    $('where').value = response.where;
                },
                onFailure: function()
                {
                    alert('Error retrieving query...');
                }
            }

        );
    }

    AlertAfterChange = true;
</script>
<?php echo GetDivFieldHTML('Name:', $this->titleField->GetField()); ?>
<?php echo GetDivFieldHTML(_tk('query_colon'), $this->queryField->getField()); ?>
<?php echo GetDivFieldHTML('SQL where clause:', $this->whereField->GetField()); ?>

<?php echo GetDivFieldHTML('', $this->searchField->GetField()); ?>
<li class="new_windowbg field_div">
    <div style="float:left;padding:6px;width:25%;">
        <b>Override user security?</b>
        <br />Set this option if you want the report viewed by users to include records to which they do not have access. Note: Setting this option will disable record links in the report
    </div>
    <div style="width:70%;float:left;padding:4px">
        <?php echo $this->securityField->GetField(); ?>
    </div>
</li>