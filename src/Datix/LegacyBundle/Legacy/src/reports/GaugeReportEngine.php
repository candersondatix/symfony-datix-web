<?php
namespace src\reports;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Class GaugeReportEngine
 *
 * Gathers data for the Gauge report
 *
 * @package src\reports
 */
class GaugeReportEngine extends GraphicalReportEngine
{
    /**
     * {@inheritdoc}
     * 
     * Formats data for Gauge charts.
     */
    protected function formatData(PackagedReport $packagedReport, array $data, $context = ReportWriter::FULL_SCREEN)
    {
        return ['total' => $data[0]['num']];
    }
}