<?php
namespace src\reports;

use src\framework\query\FieldCollection;
use src\framework\query\Where;
use src\reports\exceptions\ReportException;
use src\reports\model\report\Report;

/**
 * SQL Server-specific dates functionality for reporting engine.
 */
class SqlServerDatesEngine extends DatesEngine
{
    /**
     * {@inherit}
     */
    function getDateSelectField($field, $dateOption)
    {
        switch ($dateOption)
        {
            case Report::DAY_OF_WEEK:
                $selectField = ['DATEPART(weekday, ?)', $field];
                break;
                
            case Report::WEEK:
                $selectField = ['DATEPART(week, ?)', $field];
                break;
                
            case Report::WEEK_DATE:
            	 $selectField = ['CONVERT(VARCHAR(10), DATEADD(dd, -(DATEPART(dw, ?)-1), ?), 120)', [$field, $field]];
            	break;
                
            case Report::MONTH:
            case Report::FINANCIAL_MONTH:
                $selectField = ['DATEPART(month, ?)', $field];
                break;
                
            case Report::FINANCIAL_QUARTER:
            	$finMonths = array_keys($this->getFinancialMonthHeaders());
            	$selectField = ['
                    CONVERT(VARCHAR(4), 
                	    CASE 
                	        WHEN MONTH(?) >= '.$this->registry->getParm('FINYEAR_START_MONTH', 4).' THEN YEAR(?)
    		                WHEN MONTH(?) < '.$this->registry->getParm('FINYEAR_START_MONTH', 4).' THEN YEAR(?) - 1
    		            END) + 
		            CONVERT(VARCHAR(1), (
        				CASE 
        					WHEN MONTH(?) IN ('.$finMonths[0].','.$finMonths[1].','.$finMonths[2].') THEN 1
                    		WHEN MONTH(?) IN ('.$finMonths[3].','.$finMonths[4].','.$finMonths[5].') THEN 2
                    		WHEN MONTH(?) IN ('.$finMonths[6].','.$finMonths[7].','.$finMonths[8].') THEN 3
                    		WHEN MONTH(?) IN ('.$finMonths[9].','.$finMonths[10].','.$finMonths[11].') THEN 4
        				END))', array_fill(0, 8, $field)];
                break;
                
            case Report::QUARTER:
                $selectField = ['CONVERT(VARCHAR(4), YEAR(?)) + CONVERT(VARCHAR(1), DATEPART(quarter, ?))', [$field, $field]];
                break;
                
            case Report::YEAR:
                $selectField = ['DATEPART(year, ?)', $field];
                break;
                
            case Report::FINANCIAL_YEAR:
                $selectField = ['DATEPART(year, DATEADD(month, -'.($this->registry->getParm('FINYEAR_START_MONTH', 4) - 1).', ?) )', $field];
                break;
                
            case Report::MONTH_AND_YEAR:
                $selectField = ['CONVERT(VARCHAR(4), YEAR(?)) + CONVERT(VARCHAR(2), RIGHT(\'0\' + RTRIM(MONTH(?)), 2))', [$field, $field]];
                break;
                
            default:
                throw new ReportException('Error selecting date field - unable to determine date option');
        }
    
        return $selectField;
    }
    
    /**
     * {@inherit}
     */
    public function getWeekDayHeaders()
    {
        $this->db->setSQL('SELECT @@DATEFIRST');
        $this->db->prepareAndExecute();
        $dateFirst = (int) $this->db->PDOStatement->fetch(\PDO::FETCH_COLUMN);
    
        $dayRef = array(
            1 => _tk('monday'), 2 => _tk('tuesday'), 3 => _tk('wednesday'), 4 => _tk('thursday'),
            5 => _tk('friday'), 6 => _tk('saturday'), 7 => _tk('sunday'));
    
        $i = intval($this->registry->getParm('WEEK_START_DAY', '2', true)) - 2;
        $j = 1 - $dateFirst + (intval($this->registry->getParm('WEEK_START_DAY', '2', true)) - 2);
    
        $days = array();
        while (count($days) < 7)
        {
            $days[mod_pos($j, 7) + 1] = $dayRef[(mod_pos($i, 7)) + 1];
            $i++;
            $j++;
        }
    
        return $days;
    }
}