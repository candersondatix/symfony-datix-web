<?php
include_once(dirname(__FILE__).'/FusionCharts_Gen.php');
  
/***********************************************************************************
 *  This is a class extension written for FUSIONCHARTS v3 API PHP CLASS defined 
 *  in FusionCharts_Gen.php
 *  Author  :  Zsolt Szalai
 *  Company :  DATIX Ltd. 
 *  Version :  FusionCharts V3
 *     
 *  FusionCharts PHP Class handles all FusionCharts XML elements like
 *  chart, categories, dataset, set, Trendlines, vTrendlines, vline, styles etc.
 *  It binds data into FusionCharts XML Structures.
 * 
 *  This Extension is provided to support the new chart types such as 
 *  Trafficlights (HLinearGauge) and Gauges (AngularGauge) that was not implemented
 *  by FusionCharts' PHP API. 
 * 
 ***********************************************************************************/
  
class FusionChartsWidgets extends FusionCharts
{

    var $dialCounter = 0;
    var $dials = array();
    
    /**
    * @desc Extends setChartArrays function with the new chart types.
    */
    function setChartArrays()
    {
        parent::setChartArrays();
        
        // Series Type //5
        $this->chartSWF['hlineargauge'][0]="HLinearGauge";
        $this->chartSWF['hlineargauge'][1]=5;
        
        // Series Type //6
        $this->chartSWF['angulargauge'][0]="AngularGauge";
        $this->chartSWF['angulargauge'][1]=6;
    }
                                           
    /**
    * @desc Extends addChartData() function, adds chart data elements and
    * generates $this->dataset
    * 
    * @param string $value The value of a chart data
    * @param string $params String of parameters with default delimiter (;) 
    *      e.g. colour code, min or max value
    * @param string $vlineParams vLine parameters (not in use at this point)
    */
    function addChartData($value="",$params="",$vlineParams = "" )
    {
         parent::addChartData($value,$params,$vlineParams);
         $strSetXML = "";
         
         // Choose dataset depending on seriesType and get chart's XML
         switch ($this->seriesType)
         {
            case 5:
            case 6:
                $strSetXML = $this->genHLinGaugeChartDataXML($params);
                $this->dataset[$this->setCounter] = $strSetXML;
                $this->setCounter++;
                break;
         }
    }
    
    /**
    * @desc The addGaugeDialData() function adds a dial to gauge type charts
    * 
    * @param string $params String of parameters with default delimiter (;) 
    */                                                                      
    function addGaugeDialData($params)
    {
        switch ($this->seriesType)
        {
            case 6:
                $strSetXML = $this->genGaugeDialChartDataXML($params);
                $this->dials[$this->dialCounter] = $strSetXML;
                $this->dialCounter++;
                break;
        }
    }
    
    /**
    * @desc overwrites method getDatasetXML() which gets dataset and process it with 
    * the one of these functions. Added case 5,6 for traffic lights and gauges.
    */
    function getDatasetXML()
    {
        // Calling dataset function depending on seriesType
        switch ($this->seriesType)
        {
        case 1 :
            return $this->getSSDatasetXML();
            break;
        case 2 :
            return $this->getMSDatasetXML();
            break;
        case 3 :
            return $this->getMSDatasetXML();
            break;
        case 4 :
            return $this->getMSStackedDatasetXML();
            break;
        case 5 :  // traffic lights
            return $this->getHLinearGaugeDatasetXML();
            break;
        case 6 :  // angular gauge
            return $this->getAngularGaugeDatasetXML();
            break;
        }
     }     
     
     /**
     * @desc Add elements to the XML string for seriesType 5,6
     */
     function genHLinGaugeChartDataXML($param="")
     {
         //$this->arr_FCColors[0];
         if($this->seriesType==5 || $this->seriesType==6)
         {
             $strSetXML = "";
             $strParam="";
             $color=0;
             //only one dataset
             //foreach($this->dataset as  $x)
             if($param!="")
             {
                $strParam = $this->ConvertParamToXMLAttribute($param);
             }
             // User defined color 
             //$colorSet = '';
             //$colorSet = " code='" . $this->arr_FCColors[$this->setCounter] . "' ";
             // Setting color parameter 
             $strSetXML .= "<color " . $strParam . $colorSet . " />";
             return $strSetXML;
         }
     }
     
     /**
     * @desc Add elements to the XML string for seriesType 6
     */
     function genGaugeDialChartDataXML($param="")
     {
         if($this->seriesType==6)
         {
             $strSetXML = "";
             $strParam="";
             //only one dataset
             //foreach($this->dataset as  $x)
             if($param!="")
             {
                $strParam = $this->ConvertParamToXMLAttribute($param);
             }
             // Setting color parameter 
             $strSetXML .= "<dial ".$strParam." />";
             return $strSetXML;
         }
     }
       
     /**
     * @desc Add elements to the XML string for seriesType 6
     */
     function getAngularGaugeDatasetXML()
     {
         //$this->arr_FCColors[0];
         
        if($this->seriesType==6)
        {
            $strSetXML = $this->getHLinearGaugeDatasetXML(); 
            $partXML="";
            $partXML="<dials>";
            foreach($this->dials as $part_type => $part_name)
            {
               if($part_name!=""){
                
                 // Add elements 
                 if($part_name!="Array")
                 {
                   $partXML .= $part_name;
                 }
               } 
            }
            $partXML.="</dials>";
            $strSetXML .= $partXML;
            return $strSetXML;
        }
     }
     
     /**
     * @desc Add elements to the XML string for seriesType 5,6
     */
     function getHLinearGaugeDatasetXML()
     {
        if($this->seriesType==5 || $this->seriesType==6)
        {
           $partXML="";
           $partXML="<colorRange>";
           foreach($this->dataset as $part_type => $part_name)
           {
               if($part_name!=""){
                
                 // Add elements 
                 if($part_name!="Array")
                 {
                   $partXML .= $part_name;
                 }
               } 
           }
           $partXML.="</colorRange>";
           return $partXML;
        }
     }
}

?>
