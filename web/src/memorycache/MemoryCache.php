<?php
/**
 * Created by PhpStorm.
 * User: toliveira
 * Date: 21/05/14
 * Time: 12:26
 */

namespace src\memorycache;

// Include engines
require_once 'DatixWinCache.php';
require_once 'DatixSessionCache.php';

/**
 * Class MemoryCache
 *
 * @package src\memorycache
 */
class MemoryCache implements CacheEngineInterface
{
    /*
     * Variable that holds the caching object.
     */
    protected $engine;

    /**
     *
     */
    public function __construct(CacheEngineInterface $engine)
    {
        $this->engine = $engine;
    }

    /**
     * {@inheritdoc}
     */
    public function add($name, $value, $ttl = 0)
    {
        $this->engine->add($name, $value, $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function set($name, $value, $ttl = 0)
    {
        $this->engine->set($name, $value, $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function delete($name)
    {
        $this->engine->delete($name);
    }

    /**
     * {@inheritdoc}
     */
    public function exists($name)
    {
        return $this->engine->exists($name);
    }

    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
        return $this->engine->get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        $this->engine->flush();
    }
}