<?php
/**
 * Created by PhpStorm.
 * User: toliveira
 * Date: 21/05/14
 * Time: 12:31
 */

namespace src\memorycache;

/**
 * Class DatixWinCache - WinCache cache engine.
 *
 * @package src\memorycache
 */
class DatixWinCache implements CacheEngineInterface
{
    /**
     * {@inheritdoc}
     */
    public function add($key, $value, $ttl = 0)
    {
        return wincache_ucache_add($key, $value, $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value, $ttl = 0)
    {
        return wincache_ucache_set($key, $value, $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        return wincache_ucache_delete($key);
    }

    /**
     * {@inheritdoc}
     */
    public function exists($key)
    {
        return wincache_ucache_exists($key);
    }

    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        return wincache_ucache_get($key);
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        wincache_ucache_clear();
    }
}