<?php
/**
 * Created by PhpStorm.
 * User: toliveira
 * Date: 21/05/14
 * Time: 12:31
 */

namespace src\memorycache;

/**
 * Class DatixSessionCache - Session cache engine
 *
 * @package src\memorycache
 */
class DatixSessionCache implements CacheEngineInterface
{
    /*
     * Variable used to define the cache namespace used inside the PHP $_SESSION array.
     */
    private $cacheNamespace = 'DatixSessionCache';

    /**
     * {@inheritdoc}
     */
    public function add($key, $value, $ttl = 0)
    {
        if (isset($_SESSION[$this->cacheNamespace][$key]) && $_SESSION[$this->cacheNamespace][$key] != '')
        {
            return false;
        }

        $_SESSION[$this->cacheNamespace][$key] = $value;

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value, $ttl = 0)
    {
        $_SESSION[$this->cacheNamespace][$key] = $value;

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        unset($_SESSION[$this->cacheNamespace][$key]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function exists($key)
    {
        if (isset($_SESSION[$this->cacheNamespace][$key]) && $_SESSION[$this->cacheNamespace][$key] != '')
        {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        if (isset($_SESSION[$this->cacheNamespace][$key]) && $_SESSION[$this->cacheNamespace][$key] != '')
        {
            return $_SESSION[$this->cacheNamespace][$key];
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        session_unset();
    }
}