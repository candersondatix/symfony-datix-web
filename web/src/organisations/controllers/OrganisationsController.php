<?php

namespace src\organisations\controllers;

use src\organisations\model\OrganisationModelFactory;
use src\respondents\model\RespondentModelFactory;
use src\framework\controller\Controller;

class OrganisationsController extends Controller
{
    //Manually building this list so that we can include the linked data. Not Customisable
    public function ListLinkedClaims()
    {
        $data = $this->request->getParameters();

        //Build the SQL
        $sql = '
            SELECT
                c.recordid,
                c.cla_name,
                c.cla_ourref,
                c.cla_dopened,
                c.cla_type,
                l.indemnity_reserve_assigned,
                l.expenses_reserve_assigned
            FROM link_respondents AS l
            LEFT JOIN claims_main AS c ON l.main_recordid = c.recordid
            WHERE
                l.link_type = \'ORG\'
                AND l.org_id = :org_id
        ';

        //Get the data
        $RecordData = \DatixDBQuery::PDO_fetch_all($sql, array('org_id' => $data['recordid']));
        //Make it into a RecordList so as to interface with the listing
        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'CLA';
        $RecordList->AddRecordData($RecordData);

        //Make the listing
        $ListingDesign = new \Listings_ListingDesign(array('module' => 'CLA'));
        //Build the columns with widths
        $ColumnsToAdd;
        foreach(array('recordid' => '2', 'cla_name' => '8', 'cla_ourref' => '2', 'cla_dopened' => '3', 'cla_type' => '4') as $field => $width)
        {
            $NewColumn = \Listings_ListingColumnFactory::getColumn('CLA', $field, 'CLAIMS_MAIN');
            $NewColumn->setWidth($width);
            $ColumnsToAdd[] = $NewColumn;
        }
        foreach(array('indemnity_reserve_assigned' => '5', 'expenses_reserve_assigned' => '5') as $field => $width)
        {
            $NewColumn = \Listings_ListingColumnFactory::getColumn('CLA', $field, 'link_respondents');
            $NewColumn->setWidth($width);
            $ColumnsToAdd[] = $NewColumn;
        }
        //Put the columns in the listing
        $ListingDesign->LoadColumnsFromArray($ColumnsToAdd);

        //make something to display the records/listing
        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $ListingDesign);

        //show on page
        $this->response->build('src/organisations/views/ListLinkedClaims.php', array(
            'listing'     => $ListingDisplay,
        ));
    }

    /**
     * Gets a list of matched organisations.
     */
    public function organisationList()
    {
        global $NoMainMenu, $FieldDefs;

        $ModuleDefs  = $this->registry->getModuleDefs();
        $contactRows = null;

        $NoMainMenu = true;

        $module = \Sanitize::getModule($this->request->getParameter('module'));

        if ($module == '')
        {
            $module = 'ORG';
        }

        $modId = ModuleCodeToID($module);
        $fieldname = \Escape::EscapeEntities($this->request->getParameter('fieldname'));
        $ContactSuffix = \Escape::EscapeEntities($this->request->getParameter('suffix'));
        $FieldList = explode(',', \Sanitize::SanitizeString($this->request->getParameter('fieldlist')));
        $listingColumns = \Listings_LevelOneOrgMatchListing::GetListingColumns($this->request->getParameter('parentmodule'));
        $extraFieldList = array();

        foreach ($FieldList as $key => $field)
        {
            if (\UnicodeString::substr($field, 0, 3) == 'UDF')
            {
                $extraFieldList[] = $field;
                unset($FieldList[$key]);
            }
        }

        if (is_array($_POST['extraFieldsOnForm']))
        {
            $extraFieldsOnForm = \Sanitize::SanitizeStringArray($this->request->getParameter('extraFieldsOnForm'));
        }

        $pdoParams = array();

        $sql = '
            SELECT
            '.implode(', ', $ModuleDefs['ORG']['FIELD_ARRAY']) . ',
            organisations_main.recordid AS [org_id]
        ';

        if (!empty($extraFieldsOnForm))
        {
            $join = '';

            foreach ($extraFieldsOnForm as $key => $extraField)
            {
                $udfParts = explode('_', $extraField);
                $udfColumn = $this->getUDFValueColumn($udfParts[1]);

                // remove suffix
                array_pop($udfParts);
                $extraField = implode('_', $udfParts);

                $sql .= ', udf'.$key.'.'.$udfColumn.' AS '.$extraField;
                $join .= ' LEFT JOIN udf_values udf'.$key.' ON udf'.$key.'.mod_id = :mod_id'.$key.'
                AND udf'.$key.'.cas_id = recordid
                AND udf'.$key.'.group_id = :group_id'.$key.'
                AND udf'.$key.'.field_id = :field_id'.$key.'';

                $pdoParams['mod_id'.$key] = $modId;
                $pdoParams['group_id'.$key] = $udfParts[2];
                $pdoParams['field_id'.$key] = $udfParts[3];

                if (in_array($extraField, $extraFieldList))
                {
                    // this UDF is being used for search criteria, so also construct WHERE clause portion
                    $FieldCriteria = $_POST[$extraField];

                    if (is_array($FieldCriteria))
                    {
                        $FieldCriteria = implode(' ', $FieldCriteria);
                    }

                    $FieldCriteria = str_replace('*', '%', $FieldCriteria);
                    $FieldCriteria = str_replace('?', '_', $FieldCriteria);

                    if ($udfParts[1] == 'D')
                    {
                        $FieldCriteria = UserDateToSQLDate($FieldCriteria);
                    }

                    $sqlwhere[] = 'udf'.$key.'.'.$udfColumn.' = :' . $extraField;
                    $pdoParams[$extraField] = $FieldCriteria;
                }
            }
        }

        foreach ($FieldList as $field)
        {
            $FieldCriteria = $_POST[$field];

            if (is_array($_POST[$field]))
            {
                $FieldCriteria = implode(' ', $FieldCriteria);
            }

            $FieldCriteria = str_replace(array('*','?'), array('%','_'), $FieldCriteria);

            $sqlwhere[] = $field . ' = :' . $field;

            $pdoParams[$field] = $FieldCriteria;
        }

        $sql .= ' FROM organisations_main'.$join;

        if (!empty($sqlwhere))
        {
            $sql .= ' WHERE ' . implode(' AND ', $sqlwhere);
        }

        $sql .= ' ORDER BY org_name';

        $resultArray = \DatixDBQuery::PDO_fetch_all($sql, $pdoParams);

        if (count($resultArray) > 0)
        {
            $organisationRows = $this->outputOrganisationRows($resultArray, $module, $fieldname, $ContactSuffix, $listingColumns);
        }

        $this->response->build('src/organisations/views/OrganisationCheck.php', array(
            'listingColumns' => $listingColumns,
            'FieldDefs' => $FieldDefs,
            'resultArray' => $resultArray,
            'module' => $module,
            'fieldname' => $fieldname,
            'ContactSuffix' => $ContactSuffix,
            'organisationRows' => $organisationRows
        ));
    }

    /**
     * Returns the udf_values table column used to store data for this extra field, based on the field type.
     *
     * @param string $type The extra field type.
     * @return string $column The corresponding database column name
     */
    private function getUDFValueColumn($type)
    {
        switch ($type)
        {
            case 'C':
            case 'T':
            case 'S':
            case 'Y':
                $column = 'udv_string';
                break;
            case 'N':
                $column = 'udv_number';
                break;
            case 'D':
                $column = 'udv_date';
                break;
            case 'M':
                $column = 'udv_money';
                break;
            case 'L':
                $column = 'udv_text';
                break;
        }

        return $column;
    }

    /**
     * Outputs organisations that were matched.
     *
     * @param array $organisations
     * @param string $module
     * @param string $fieldName
     * @param string $contactSuffix
     * @param $listingColumns
     *
     * @return \src\framework\controller\Response
     */
    private function outputOrganisationRows($organisations, $module, $fieldName, $contactSuffix, $listingColumns)
    {
        global $FieldDefs;

        $organisationsArray  = array();
        $listingColumnsArray = array();

        foreach ($organisations as $id => $row)
        {
            $row = ProcessHTMLEntities($row);

            foreach ($row as $Field => $FieldValue)
            {
                if ($FieldValue == '' || (is_array($FieldValue) && empty($FieldValue)))
                {
                    continue;
                }

                if (ini_get('magic_quotes_sybase'))
                {
                    $FieldValue = str_replace("'", "\\'", $row[$Field]);
                }
                elseif (ini_get('magic_quotes_gpc'))
                {
                    $FieldValue = addslashes($row[$Field]);
                }
                else
                {
                    $FieldValue = str_replace("'", "\\'", $row[$Field]);
                }

                $FieldValue = str_replace("\r\n", "\\n", $FieldValue);

                $FieldValue = str_replace("\"", "&quot;", $FieldValue);

                $FieldValue = str_replace('&#13;&#10;', '\n', $FieldValue);

                $FieldValue = str_replace("&#39;", "\\'", $FieldValue);

                if (\UnicodeString::substr($Field, 0, 3) == 'UDF')
                {
                    $udfParts = explode('_', $Field);
                    $fieldType = $udfParts[1];

                    if ($fieldType == 'D')
                    {
                        $FieldValue = FormatDateVal($FieldValue);
                    }
                }
                else
                {
                    $fieldType = $FieldDefs[$module][$Field]['Type'];
                }

                $organisationsArray[$id][$Field] = array(
                    'field'      => $Field,
                    'fieldValue' => $FieldValue,
                    'fieldType'  => $fieldType
                );
            }

            foreach ($listingColumns as $column)
            {
                $field = $column->getName() == 'recordid' ? 'org_id' : $column->getName();

                switch ($FieldDefs['ORG'][$field]['Type'])
                {
                    case 'ff_select':
                        $value = code_descr('ORG', $field, $row[$field], '', false);
                        break;
                    case 'multilistbox':
                        $values = array();
                        $codes = explode(' ', $row[$field]);

                        foreach ($codes as $code)
                        {
                            $values[] = code_descr('ORG', $field, $code, '', false);
                        }

                        $value = implode('<br />', $values);
                        break;
                    case 'time':
                        $value = $row[$field]{0} . $row[$field]{1} . ":" . $row[$field]{2} . $row[$field]{3};
                        break;
                    default:
                        $value = $row[$field];
                        break;
                }

                $listingColumnsArray[$id][] = $value;
            }
        }

        $this->response->build('src/organisations/views/OrganisationRows.php', array(
            'organisationsArray' => $organisationsArray,
            'fieldName' => $fieldName,
            'contactSuffix' => $contactSuffix,
            'module' => $module,
            'listingColumnsArray' => $listingColumnsArray
        ));

        return $this->response;
    }

    /**
     * Saves organisations linked to claims level 1 form.
     */
    public function saveLinkedOrganisations()
    {
        $maxLinkedOrganisations = $this->request->getParameter('organisation_max_suffix');
        $organisationNumber     = 1;

        while ($organisationNumber <= $maxLinkedOrganisations)
        {
            if ($this->request->getParameter('main_recordid') && $this->request->getParameter('org_id_'.$organisationNumber))
            {
                (new RespondentModelFactory())->getMapper()->saveLinkedOrganisations(array(
                    'main_recordid' => $this->request->getParameter('main_recordid'),
                    'link_type' => 'ORG',
                    'org_id' => $this->request->getParameter('org_id_'.$organisationNumber),
                    'link_notes' => $this->request->getParameter('link_notes_'.$organisationNumber),
                    'link_resp' => $this->request->getParameter('link_resp_'.$organisationNumber),
                    'link_role' => $this->request->getParameter('link_role_'.$organisationNumber)
                ));
            }

            $organisationNumber++;
        }
    }
}