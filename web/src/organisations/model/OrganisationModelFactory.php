<?php
namespace src\organisations\model;

use src\framework\model\ModelFactory;

/**
 * @codeCoverageIgnore
 */
class OrganisationModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new OrganisationFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new OrganisationMapper(new \DatixDBQuery(), $this);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new OrganisationCollection($this->getMapper(), $this->getEntityFactory());
    }
}