<?php
namespace src\email;

use src\email\EmailTemplateInterface;

require_once 'thirdpartylibs\SwiftMailer\swift_required.php';

/**
 * Class EmailSenderIndividual
 *
 * Used as a mechanism for constructing Datix email templates and recipient lists and sending them with a single email
 * going to each recipient.
 */
class EmailSenderIndividual extends EmailSender
{
    /**
     * Loops through each recipients and constructs a personal email for them.
     * @param array $data
     * @return int|void
     */
    public function sendEmails($data = array())
    {
        parent::sendEmails($data);

        //work out where the email text will come from.
        $template = $this->getTemplate();

        $numSuccessfulEmails = 0;
        $failedRecipients = array();

        foreach (array_unique($this->getRecipientAddresses()) as $recipientEmail)
        {
            $failedRecipient = array();
            $failedReason = '';
            $successfulEmail = 0;

            try
            {
                $message = \Swift_Message::newInstance($template->getSubject($data))
                    ->setFrom(array($this->getFromUser()->getFirstValidEmail()))
                    ->setTo(array($recipientEmail))
                    ->setBody($template->getBody($data))
                    ->setContentType($this->getContentType($template))
                ;

                // Send the message
                $successfulEmail = $this->swiftMailer->send($message, $failedRecipient);
            }
            catch (\Exception $exception)
            {
                //If swiftmailer catches an error, it will be output as an exception message and caught here.
                $failedRecipient = $recipientEmail;
                $failedReason = $exception->getMessage();
            }

            if (count($failedRecipient) > 0)
            {
                if(!$failedReason)
                {
                    if($this->logger->errors[$recipientEmail])
                    {
                        //If the logger catches an SMTP error, it will be stored here
                        $failedReason = $this->logger->errors[$recipientEmail];
                    }
                    else
                    {
                        //If we've got here, an error has occured, but we don't have a contextual error.
                        $failedReason = _tk('unknown_email_error');
                    }
                }

                $failedRecipients[$recipientEmail] = $failedReason;
            }

            if ($successfulEmail == 1)
            {
                $numSuccessfulEmails++;
            }
        }

        //record successful and failed sendings
        $this->saveToEmailHistory($data, $template, $failedRecipients);

        if ($numSuccessfulEmails > 0)
        {
            return 1;
        }

        return 0;
    }

    /**
     * Determines whether we know enough about the success or failure of a message to save an audit record.
     * @param $data
     * @param EmailTemplateInterface $template
     * @param $failedRecipients
     * @param bool $groupFailed
     * @param string $groupFailedMessage
     * @return mixed|void
     */
    protected function saveToEmailHistory($data, EmailTemplateInterface $template, $failedRecipients, $groupFailed = false, $groupFailedMessage = '')
    {
        foreach ($this->recipients as $recipient)
        {
            foreach (explode(',', $recipient->con_email) as $recipientEmailAddress)
            {
                $errorMessage = '';

                if (isset($this->failedDomainValidation[$recipientEmailAddress]))
                {
                    $failedEmail = true;
                    $errorMessage = sprintf(_tk('emails_not_set_domain_not_permitted'), $recipientEmailAddress);
                }
                else if (isset($this->failedEmailStructureValidation[$recipientEmailAddress]))
                {
                    $failedEmail = true;
                    $errorMessage = sprintf(_tk('invalid_email_structure_error'), $recipientEmailAddress);
                }
                else
                {
                    $failedEmail = isset($failedRecipients[$recipientEmailAddress]);

                    if($failedEmail)
                    {
                        $errorMessage = $failedRecipients[$recipientEmailAddress];
                        $this->failedOther[$recipientEmailAddress] = $recipient;
                    }
                    else
                    {
                        $this->successful[$recipientEmailAddress] = $recipient;
                    }
                }

                $this->prepareAuditRecord(array($recipientEmailAddress), $data, $recipient->recordid, !$failedEmail, $errorMessage);
            }
        }

        $this->saveAuditRecords();
    }
}

