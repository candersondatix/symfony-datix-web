<?php

namespace src\email;

use src\email\EmailTemplateInterface;

/**
 * Class EmailFileTemplate
 *
 * Used to model the contents of an email that is created on the fly.
 */
class EmailCustomTemplate implements EmailTemplateInterface
{
    /**
     * @var string The subject of the email
     */
    protected $subject;

    /**
     * @var string The body of the email
     */
    protected $body;

    /**
     * @var bool Whether the email should be rendered as HTML.
     */
    protected $isHTML;


    public function __construct($subject, $body, $isHTML = false)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->isHTML = $isHTML;
    }

    public function getSubject($data)
    {
        return $this->subject;
    }

    public function getBody($data)
    {
        return $this->body;
    }

    public function isHTMLEmail()
    {
        return $this->isHTML;
    }

}