<?php
namespace src\email;

use src\email\EmailTemplateInterface;
use src\framework\registry\Registry;
use src\users\model\User;
use src\contacts\model\Contact;
use src\email\EmailFileTemplate;
use src\email\EmailLogger;
use src\email\EmailSenderFactory;

require_once 'thirdpartylibs\SwiftMailer\swift_required.php';

/**
 * Class EmailSenderGroup
 *
 * Abstact class used as a parent for emailSender classes controlling emails sent as groups (rather than multiple
 * individual emails).
 */
abstract class EmailSenderGroup extends EmailSender
{
    protected abstract function createMessage($data, EmailTemplateInterface $template, $recipientAddresses);

    /**
     * Pulls together all the component parts of the message and calls swiftMailer->send().
     * @param array $data
     * @return int|void
     */
    public function sendEmails($data = array())
    {
        parent::sendEmails($data);

        //work out where the email text will come from.
        $template = $this->getTemplate();

        $failedRecipients = array();
        $groupFailed = false;

        try
        {
            // Create a message
            $message = $this->createMessage($data, $template, $this->getRecipientAddresses());

            // Send the message
            $numSuccessfulEmails = $this->swiftMailer->send($message, $unknownFailedRecipients);

            foreach ($unknownFailedRecipients as $unknownFailedRecipient)
            {
                if($this->logger->errors[$unknownFailedRecipient])
                {
                    //If the logger catches an SMTP error, it will be stored here
                    $failedReason = $this->logger->errors[$unknownFailedRecipient];
                }
                else
                {
                    //If we've got here, an error has occured, but we don't have a contextual error.
                    $failedReason = _tk('unknown_email_error');
                }

                $failedRecipients[$unknownFailedRecipient] = $failedReason;
            }
        }
        catch (\Exception $exception)
        {
            //If swiftmailer catches an error, it will be output as an exception message and caught here.
            //We don't know which email failed, so we need to log it as a group.
            $groupFailed = true;
            $groupFailedMessage = $exception->getMessage();
        }

        //record successful and failed sendings
        $this->saveToEmailHistory($data, $template, $failedRecipients, $groupFailed, $groupFailedMessage);

        if ($numSuccessfulEmails > 0)
        {
            return 1;
        }

        return 0;
    }

    /**
     * Calculates which audit records to save for any sttempted emails sent
     *
     * @param $data
     * @param EmailTemplateInterface $template
     * @param $failedRecipients
     * @param bool $groupFailed
     * @param string $groupFailedMessage
     * @return mixed|void
     */
    protected function saveToEmailHistory($data, EmailTemplateInterface $template, $failedRecipients, $groupFailed = false, $groupFailedMessage = '')
    {
        $groupFailRecipients = array();

        //Run through all potential recipients. If we have an error message associated with a particular address,
        //we can record it individually. Otherwise, all remaining addresses are grouped with a common success or
        //failure message.
        foreach ($this->recipients as $recipient)
        {
            foreach (explode(',', $recipient->con_email) as $recipientEmailAddress)
            {
                $errorMessage = '';

                if (isset($this->failedDomainValidation[$recipientEmailAddress]))
                {
                    $failedEmail = true;
                    $errorMessage = sprintf(_tk('emails_not_set_domain_not_permitted'), $recipientEmailAddress);
                }
                else if (isset($this->failedEmailStructureValidation[$recipientEmailAddress]))
                {
                    $failedEmail = true;
                    $errorMessage = sprintf(_tk('invalid_email_structure_error'), $recipientEmailAddress);
                }
                else
                {
                    $failedEmail = isset($failedRecipients[$recipientEmailAddress]);

                    if($failedEmail)
                    {
                        $errorMessage = $failedRecipients[$recipientEmailAddress];
                        $this->failedOther[$recipientEmailAddress] = $recipient;
                    }
                    else if ($groupFailed)
                    {
                        $groupFailRecipients[$recipientEmailAddress] = $recipient;
                    }
                    else
                    {
                        $this->successful[$recipientEmailAddress] = $recipient;
                    }
                }

                if($failedEmail)
                {
                    $this->prepareAuditRecord(array($recipientEmailAddress), $data, $recipient->recordid, false, $errorMessage);
                }
            }

        }

        if (!empty($this->successful))
        {
            foreach ($this->successful as $successfulEmail => $successfulUser)
            {
                $this->prepareAuditRecord(array($successfulEmail), $data, $successfulUser->recordid);
            }
        }

        if($groupFailed)
        {
            foreach ($groupFailRecipients as $failedEmail => $failedUser)
            {
                $this->prepareAuditRecord(array($failedEmail), $data, $failedUser->recordid, false, $groupFailedMessage);
            }
        }

         $this->saveAuditRecords();
    }
}

