<li>
    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
        <tr>
            <td class="windowbg"><b><?php echo _tk('recipient_name'); ?></b></td>
            <td class="windowbg"><b><?php echo _tk('recipient_email'); ?></b></td>
            <td class="windowbg"><b><?php echo _tk('date_time'); ?></b></td>
            <td class="windowbg"><b><?php echo _tk('contact_id'); ?></b></td>
            <td class="windowbg"><b><?php echo _tk('tel_number'); ?></b></td>
            <td class="windowbg"><b><?php echo _tk('job_title'); ?></b></td>
        </tr>
        <?php if (count($this->data) == 0) : ?>
        <tr>
            <td class="windowbg2" colspan="4"><b><?php echo _tk('no_notification_emails_sent')?></b></td>
        </tr>
        <?php else : ?>
            <?php foreach ($this->data as $emailData) : ?>
            <tr>
                <td class='windowbg2'><?php echo $emailData['recipientName']; ?></td>
                <td class='windowbg2'><?php echo $emailData['emailAddress']; ?></td>
                <td class='windowbg2' nowrap='nowrap' align='left'  width='10%'><?php echo $emailData['emailDateTime']; ?></td>
                <td class='windowbg2' align='right' width='5%'><?php echo $emailData['contactId']; ?></td>
                <td class='windowbg2' align='right' width='15%'><?php echo $emailData['telNumber']; ?></td>
                <td class='windowbg2' width='15%'><?php echo $emailData['jobTitle']; ?></td>
            </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
</li>