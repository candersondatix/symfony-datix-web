<?php
namespace src\email;

use src\email\EmailTemplateInterface;
use src\email\EmailSenderGroup;

require_once 'thirdpartylibs\SwiftMailer\swift_required.php';

/**
 * Class EmailSenderGroupBCC
 *
 * Used as a mechanism for constructing Datix email templates and recipient lists and sending them with recipients
 * grouped in the BCC header.
 */
class EmailSenderGroupBCC extends EmailSenderGroup
{
    /**
     * Creates message object, using setBcc() to add recipients.
     * @param $data
     * @param EmailTemplateInterface $template
     * @param $recipientAddresses
     * @return \Swift_Mime_SimpleMimeEntity
     */
    protected function createMessage($data, EmailTemplateInterface $template, $recipientAddresses)
    {
        $message = \Swift_Message::newInstance($template->getSubject($data))
            ->setFrom(array($this->getFromUser()->getFirstValidEmail()))
            ->setBcc($recipientAddresses)
            ->setBody($template->getBody($data))
            ->setContentType($this->getContentType($template))
        ;

        return $message;
    }
}

