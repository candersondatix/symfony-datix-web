<?php
namespace src\email;

use src\email\EmailTemplateInterface;
use src\email\EmailSenderGroup;

require_once 'thirdpartylibs\SwiftMailer\swift_required.php';

/**
 * Class EmailSenderGroupCC
 *
 * Used as a mechanism for constructing Datix email templates and recipient lists and sending them with recipients
 * grouped in the CC header.
 */
class EmailSenderGroupCC extends EmailSenderGroup
{
    /**
     * Creates message object, using setCc() to add recipients.
     * @param $data
     * @param EmailTemplateInterface $template
     * @param $recipientAddresses
     * @return \Swift_Mime_SimpleMimeEntity
     */
    protected function createMessage($data, EmailTemplateInterface $template, $recipientAddresses)
    {
        $message = \Swift_Message::newInstance($template->getSubject($data))
            ->setFrom(array($this->getFromUser()->getFirstValidEmail()))
            ->setCc($recipientAddresses)
            ->setBody($template->getBody($data))
            ->setContentType($this->getContentType($template))
        ;

        return $message;
    }
}

