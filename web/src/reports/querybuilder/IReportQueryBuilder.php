<?php

namespace src\reports\querybuilder;

use src\reports\model\packagedreport\PackagedReport;

interface IReportQueryBuilder
{
    /**
     * Builds and returns the report query for the given packaged report.
     * 
     * @param PackagedReport $packagedReport
     * 
     * @return src\framework\query\Query
     */
    public function build(PackagedReport $packagedReport);
}