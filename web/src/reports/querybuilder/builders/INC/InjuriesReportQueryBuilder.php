<?php

namespace src\reports\querybuilder\builders\INC;

use src\reports\querybuilder\builders\AbstractReportQueryBuilder;
use src\system\database\fielddef\FieldDefCollection;

/**
 * Manually defines the route to use when reporting on injuries to prevent duplicates in the record count (i.e. when there are multiple injuries).
 * May need to be made more generic if/when similarly related data is made reportable.
 * 
 * The base report object is amended rather than the query, so this step needs to be loaded and executed first
 * so that the changes to the report properties are available to subsequent build steps.
 */
class InjuriesReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var FieldDefCollection
     */
    protected $fieldDefs;
    
    public function __construct(FieldDefCollection $fieldDefs)
    {
        $this->fieldDefs = $fieldDefs;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {
        foreach (['rows','columns'] as $axis)
        {
            if (null !== ($$axis = $this->fieldDefs[$this->report->$axis]) && 'inc_injuries' == $$axis->getTable())
            {
                $fieldset = $axis.'_fieldset';
                $this->report->$fieldset = 62;
            }
        }
    }
}