<?php

namespace src\reports\querybuilder\builders;

use src\framework\registry\Registry;
use src\reports\model\report\Report;

/**
 * Sets the "date first" query property for reports which use "week date" as a date option, so we "know" how to number the weeks.
 */
class WeekDateReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
    
    /**
     * {@inheritdoc}
     */
    public function doUpdate()
    {
        if (in_array(Report::WEEK_DATE, [$this->report->rows_date_option, $this->report->columns_date_option]))
        {
            // transform value of WEEK_START_DAY global (0=Sat->6=Fri) to SQL Server DATEFIRST values (1=Mon->7=Sun)
            $weekStartDay = $this->registry->getParm('WEEK_START_DAY', '2');
        	switch ($weekStartDay)
        	{
        		// Saturday
        		case 0:
        			$dateFirst = 6;
        			break;
        	
        		// Sunday
        		case 1:
        			$dateFirst = 7;
        			break;
        	
        		default:
        			$dateFirst = $weekStartDay - 1;
        	}
            
            $this->query->setDateFirst($dateFirst);
        }
    }
}