<?php

namespace src\reports\querybuilder\builders\CLA;

use src\reports\querybuilder\builders\AbstractReportQueryBuilder;
use src\reports\specifications\ReportSpecificationFactory;
use src\reports\model\report\Report;
use src\reports\exceptions\ReportException;

/**
 * Builds the report query properties required for running aggregate reports across remaining reserve values in Claims.
 */
class AggregateRemainingReserveReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var ReportSpecificationFactory
     */
    protected $rsf;
    
    public function __construct(ReportSpecificationFactory $rsf)
    {
        $this->rsf = $rsf;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {        
        $notAggRemainingReserveSpec = $this->rsf->getAggregateRemainingReserveSpecification()->not();
         
        if ($notAggRemainingReserveSpec->isSatisfiedBy($this->packagedReport))
        {
            throw new ReportException('Invalid report specification for this query build step');
        }
        
        if (Report::COUNT_SUM == $this->report->count_style)
        {
            $fieldSplit = explode('.', $this->report->count_style_field_sum);

            if ($fieldSplit[1] == 'fin_calc_reserve_1')
            {
                $reserveType = '1';
            }
            elseif ($fieldSplit[1] == 'fin_calc_reserve_2')
            {
                $reserveType = '2';
            }

            $this->query->select([['SUM(?) AS num', 'arr.remaining_reserve_value']]);
        }

        if (Report::COUNT_AVE == $this->report->count_style)
        {
            $fieldSplit = explode('.', $this->report->count_style_field_average);

            if ($fieldSplit[1] == 'fin_calc_reserve_1')
            {
                $reserveType = '1';
            }
            elseif ($fieldSplit[1] == 'fin_calc_reserve_2')
            {
                $reserveType = '2';
            }

            $this->query->select([['AVG(?) AS num', 'arr.remaining_reserve_value']]);
        }
        
        if ($this->report->count_style_field_value_at == Report::TODAY)
        {
            $dateTimeFrom = (new \DateTime())->format('Y-m-d');
        }
        elseif ($this->report->count_style_field_value_at == Report::SELECT_DATE)
        {
            $dateTimeFrom = UserDateToSQLDate($this->report->count_style_field_calendar); // TODO - remove reliance on this procedural function
            $dateTimeFrom = substr($dateTimeFrom, 0, -13);
        }

        $this->query->join('fn_agg_rem_res(\''.$dateTimeFrom.'\', \''.$reserveType.'\', \''.($this->report->count_style_field_initial_levels == 'Y' ? 'ASC' : 'DESC').'\')', 'claims_main.recordid = arr.cla_id', 'INNER', '', 'arr', 'num');
    }
}