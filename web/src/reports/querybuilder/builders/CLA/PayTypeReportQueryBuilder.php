<?php

namespace src\reports\querybuilder\builders\CLA;

use src\reports\querybuilder\builders\AbstractReportQueryBuilder;
use src\reports\specifications\ReportSpecificationFactory;
use src\reports\model\report\Report;
use src\reports\exceptions\ReportException;
use src\framework\query\QueryFactory;

/**
 * Handles report figure calculations when reporting on Claims and doing a sum/average on Payment Type codes.
 */
class PayTypeReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var ReportSpecificationFactory
     */
    protected $rsf;
    
    /**
     * @var QueryFactory
     */
    protected $queryFactory;
    
    public function __construct(ReportSpecificationFactory $rsf, QueryFactory $queryFactory)
    {
        $this->rsf = $rsf;
        $this->queryFactory = $queryFactory;
    }
    
    /**
     * {@inheritdoc}
     * 
     * @throws ReportException If the report is of an incorrect type for this build step.
     */
    protected function doUpdate()
    {
        $notPayTypeSpec = $this->rsf->getPayTypeSpecification()->not();
        $sumPayTypeSpec = $this->rsf->getSumOnPayTypeSpecification();
        $avePayTypeSpec = $this->rsf->getAverageOnPayTypeSpecification();
        
        if ($notPayTypeSpec->isSatisfiedBy($this->packagedReport))
        {
            throw new ReportException('Incorrect report type for this query build step');
        }
        
        if ($sumPayTypeSpec->isSatisfiedBy($this->packagedReport))
        {
            $count = 'SUM';
            $payType = explode('_', $this->report->count_style_field_sum)[3];
        }
        
        if ($avePayTypeSpec->isSatisfiedBy($this->packagedReport))
        {
            $count = 'AVG';
            $payType = explode('_', $this->report->count_style_field_average)[3];
        }
        
        if ('' == $payType)
        {
            throw new ReportException('Unable to identify Payment Type for this report.');
        }
        
        $this->query->select([$count.'(pay_totals.total) AS num']);
        
        $paymentsSubQuery = $this->queryFactory->getQuery();
        $paymentsSubQuery->select(['SUM(pay_calc_total) AS total', 'cla_id'])
                         ->from('vw_payments')
                         ->groupBy(['cla_id']);

        if ('PAYTOTAL' != $payType)
        {
            $paymentsSubQuery->where(['pay_type' => $payType]);
        }
        
        $this->query->joinSubSelect($paymentsSubQuery, 'pay_totals', 'claims_main.recordid = pay_totals.cla_id', 'LEFT');
    }
}