<?php

namespace src\reports\querybuilder\builders;

/**
 * Suppresses addition of security where clauses to report queries where we're overriding record security.
 */
class OverrideSecurityReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {
        if ($this->packagedReport->override_security)
        {
            $this->query->overrideSecurity();
        }
    }
}