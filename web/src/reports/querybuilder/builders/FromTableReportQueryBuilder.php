<?php

namespace src\reports\querybuilder\builders;

use src\reports\exceptions\ReportException;

/**
 * Defines the FROM clause used for the report query.
 */
class FromTableReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var array
     */
    protected $moduleDefs;
    
    public function __construct(array $moduleDefs)
    {
        $this->moduleDefs = $moduleDefs;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {
        $table = $this->moduleDefs[$this->report->module]['VIEW'] ?: $this->moduleDefs[$this->report->module]['TABLE'];
        
        if ('' == $table)
        {
            throw new ReportException('Unable to determine module view/table');
        }
        
        $this->query->from($table);
    }
}