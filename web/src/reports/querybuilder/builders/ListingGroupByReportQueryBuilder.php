<?php

namespace src\reports\querybuilder\builders;

use src\reports\model\listingreport\Listing;
use src\reports\exceptions\ReportException;
use src\framework\registry\Registry;

/**
 * Defines the ORDER BY clause used for the listing report query when grouping records.
 */
class ListingGroupByReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {
        if (!($this->report instanceof Listing))
        {
            throw new ReportException('Report type of Listing expected, '.get_class($packagedReport->report).' given.');
        }
        
        $moduleDefs = $this->registry->getModuleDefs();
        $table = $moduleDefs[$this->report->module]['VIEW'] ?: $moduleDefs[$this->report->module]['TABLE'];
        
        foreach ($this->report->groupBys as $groupBy)
        {
            $field = $groupBy->getForm() == $table ? $groupBy->getField() : $groupBy->getForm().'.'.$groupBy->getAlias();
            
            $this->query->select([['? AS '.$groupBy->getAlias(), $field]])
                        ->orderBy([$field]);
        }
    }
}