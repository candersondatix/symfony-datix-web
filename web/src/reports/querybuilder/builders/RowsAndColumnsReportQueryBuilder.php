<?php

namespace src\reports\querybuilder\builders;

use src\framework\query\Query;
use src\framework\query\QueryFactory;
use src\reports\model\report\Report;
use src\reports\DatesEngine;
use src\reports\exceptions\ReportException;
use src\reports\specifications\ReportSpecificationFactory;
use src\system\database\fielddef\FieldDefCollection;
use src\system\database\table\LinkTableModelFactory;
use src\system\database\FieldInterface;
use src\system\database\DatabaseColumn;
use src\system\database\extrafield\ExtraField;

/**
 * Constructs the query properties required based on the rows/columns report design settings.
 */
class RowsAndColumnsReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var FieldDefCollection
     */
    protected $fieldDefs;
    
    /**
     * @var DatesEngine
     */
    protected $datesEngine;
    
    /**
     * @var QueryFactory
     */
    protected $queryFactory;
    
    /**
     * @var LinkTableModelFactory
     */
    protected $linkTableModelFactory;
    
    /**
     * @var ReportSpecificationFactory
     */
    protected $rsf;
    
    /**
     * @var array
     */
    protected $moduleDefs;

    public function __construct(DatesEngine $datesEngine, FieldDefCollection $fieldDefs, QueryFactory $queryFactory, LinkTableModelFactory $linkTableModelFactory, ReportSpecificationFactory $rsf, array $moduleDefs)
    {
        $this->datesEngine = $datesEngine;
        $this->fieldDefs = $fieldDefs;
        $this->queryFactory = $queryFactory;
        $this->linkTableModelFactory = $linkTableModelFactory;
        $this->rsf = $rsf;
        $this->moduleDefs = $moduleDefs;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate()
    {
        $this->buildQuery($this->query, $this->report);

        if ($this->report->columns != '')
        {
            $this->buildQuery($this->query, $this->report, 'columns');
        }
    }
    
    /**
     * Adds the appropriate parameters to the query to report on the provided axis value (field).
     * 
     * @param Query  $query  The query object.
     * @param Report $report The report object.
     * @param string $axis   The report axis (rows|columns).
     * 
     * @throws ReportException If the field definition for the axis value cannot be found. 
     */
    protected function buildQuery(Query $query, Report $report, $axis = 'rows')
    {
        $fieldDefs = $this->fieldDefs;
        $query->setAutoCreateExtraFieldjoins(true); // in case it's been disabled when creating the rows query conditions

        if (null === ($axisFieldDef = $fieldDefs[$report->$axis]))
        {
            throw new ReportException('Unable to load definition for field: '.$report->$axis);
        }
        
        if ($axisFieldDef->getType() == FieldInterface::DATE)
        {
            $dateOption = $axis.'_date_option';
        
            // date fields need to be wrapped in a date function depending on the date option chosen
            $field = $this->datesEngine->getDateSelectField($report->$axis, $report->$dateOption);
        }
        else
        {
            // keep the field format consistent
            $field = ['?', $report->$axis];
        }
        
        $axis_fieldset = $axis.'_fieldset';
        
        if ($report->$axis_fieldset != 0)
        {
            // The field we're reporting on is on a linked table, so we need to define the table relationships required.
            // For ease of construction and consistency, we do this via a sub query aliased as row/column.
            $subQuery = $this->queryFactory->getQuery()->select([$report->$axis]);
            
            $linkTable = $this->linkTableModelFactory->getMapper()->find($report->$axis_fieldset);
            $linkTable->setFieldset($report->$axis_fieldset);
            
            $subSelectAlias = '['.substr($axis, 0, -1).']';
            $currentTable   = $query->getTable();
            $firstJoin      = true;
            $contactsLinkTable = '';
            
            // iterate over each join condition 
            foreach ($linkTable->route as $link)
            {
                if ($firstJoin)
                {
                    // the first join condition becomes the main table (i.e. the basis of the FROM clause) in the sub query used for the join
                    $subQueryJoin = $this->queryFactory->getWhere();
                    $subQueryJoinConditions = $this->queryFactory->getFieldCollection();
                    
                    $joinColumn = new DatabaseColumn($link->conditions[0][0]);
                    
                    // determine which of the two fields in the join condition(s) belong to the other table in the relationship,
                    // add them to the sub query select statement (so they're available for the main query join to the sub query)
                    // and construct the join statement within the sub query itself
                    if ($joinColumn->getTable() != $currentTable)
                    {
                        foreach ($link->conditions as $condition)
                        {
                            $subQuery->select([$condition[0]]);
                            
                            $subQueryJoinColumn = $subSelectAlias.'.'.(new DatabaseColumn($condition[0]))->getColumn();
                            $subQueryJoinConditions->field($subQueryJoinColumn)->eq($condition[1], true);
                        }
                        
                        $currentTable = $link->tlk_table1;
                    }
                    else
                    {
                        foreach ($link->conditions as $condition)
                        {
                            $subQuery->select([$condition[1]]);
                            
                            $subQueryJoinColumn = $subSelectAlias.'.'.(new DatabaseColumn($condition[1]))->getColumn();
                            $subQueryJoinConditions->field($condition[0])->eq($subQueryJoinColumn, true);
                        }
                        
                        $joinColumn = new DatabaseColumn($link->conditions[0][1]);
                        $currentTable = $link->tlk_table2;
                    }
                    
                    $subQuery->from($joinColumn->getTable());
                    $subQueryJoin->add($subQueryJoinConditions);
                    
                    if ('' != $link->tlk_where)
                    {
                        // include the additional join condition for the "from" table in the main WHERE clause of the sub select
                        $subQuery->setWhereStrings([$link->tlk_where]);
                    }
                    
                    $firstJoin = false;
                }
                else
                {
                    // construct each join clause for the sub query in turn for each subsequent iteration through the route
                    $params = $this->queryFactory->getFieldCollection();
                    $where  = $this->queryFactory->getWhere();
                    
                    if ($link->tlk_table1 != $currentTable && !in_array($link->tlk_table1, array_keys($subQuery->getJoins())) && $link->tlk_table1 != $subQuery->getTable())
                    {
                        $joinTable = $link->tlk_table1;
                    }
                    else
                    {
                        $joinTable = $link->tlk_table2;
                    }
                    
                    foreach ($link->conditions as $condition)
                    {
                        $params->field($condition[0])->eq($condition[1], true);
                    }
    
                    $where->add($params);
                    $subQuery->join($joinTable, $where, 'INNER', $link->tlk_where);
                    
                    $currentTable = $joinTable;
                }
                //If one of the link_tables is contacts main, grab the contacts link table so we can figure out what ID to use.
                if($link->tlk_table1 == 'contacts_main')
                {
                    $contactsLinkTable = $link->tlk_table2;
                }
                else if($link->tlk_table2 == 'contacts_main')
                {
                    $contactsLinkTable = $link->tlk_table1;
                }
            }

            // handle suppression of duplicate contacts if reporting on a contacts_main field
            $contactsMainSpec = $this->rsf->getLinkedContactMainTableReportSpecification($contactsLinkTable);

            $contactsLinkTable = $contactsLinkTable ?: 'link_contacts';

            if ($contactsMainSpec->isSatisfiedBy($this->packagedReport))
            {
                if (!$this->moduleDefs[$report->module]['FK'])
                {
                    throw new \MissingMetaDataException('Missing foreign key for module "'.$report->module.'"');
                }

                if(isset($this->moduleDefs[$report->module]['CONTACT_LINK_TABLE_ID']))
                {
                    $linkID = $this->moduleDefs[$report->module]['CONTACT_LINK_TABLE_ID'][$contactsLinkTable];
                }
                else
                {
                    $linkID = $this->moduleDefs[$report->module]['FK'];
                }

                $subQuery->groupBy(array_unique(array_merge([$contactsLinkTable.'.'.$linkID, $contactsLinkTable.'.con_id'], $subQuery->getFields())));
            }
            
            // add the sub query and the assosiated join clause to the main report query
            $query->joinSubSelect($subQuery, $subSelectAlias, $subQueryJoin);
            
            // swap $field table name with sub query alias
            if ($axisFieldDef instanceof ExtraField)
            {
                $subQueryAlias = $subSelectAlias.'.'.$axisFieldDef->getValueField();
                
                // we need to prevent the main Query object creating the extra field joins in this scenario,
                // since they're already catered for in the sub query
                $query->setAutoCreateExtraFieldjoins(false);
            }
            else
            {
                $subQueryAlias = $subSelectAlias.'.'.$axisFieldDef->getName();
            }
            
            if (is_array($field[1]))
            {
                $field[1] = array_replace($field[1],
                    array_fill_keys(
                        array_keys($field[1], $report->$axis),
                        $subQueryAlias
                    )
                );
            }
            else
            {
                $field[1] = $subQueryAlias;
            }
        }
        
        $fieldWithoutAlias = $field;
        
        // append alias
        $field[0] .= ' AS '.$axis;

        // add field to select statement
        $query->select([$field]);
        
        // We have to group by after the call to select() so the Query object can swap in extra field subquery aliases (which isn't ideal...)
        $query->groupBy([$fieldWithoutAlias]);
            
        // the report data should only contain records where the selected field has a value if we don't select null values
        if ($report->null_values !== true)
        {
            // ensure we use the sub query alias for the field name if required when filtering nulls
            $notNullField = $report->$axis_fieldset != 0 ? $subQueryAlias : $report->$axis;
            
            $where      = $this->queryFactory->getWhere();
            $conditions = $this->queryFactory->getFieldCollection();
            $conditions->field($notNullField)->notNull();

            if (!$axisFieldDef->isNumeric())
            {
                $conditions->field($notNullField)->notEmpty();
            }

            $where->add($conditions);
            $query->where($where);
        }
    }
}