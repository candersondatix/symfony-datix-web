<?php

namespace src\reports\querybuilder\builders;

use src\reports\exceptions\ReportException;
use src\reports\model\listingreport\Listing;
use src\framework\registry\Registry;

/**
 * Creates the join conditions required when including linked table fields in the report design.
 * 
 * Currently based on subforms metadata, which is hideous to deal with.
 * At some point it would be great to alter this to use reportable fieldsets instead, however
 * this will need to happen in the context of severing the ties with the RC for listing reports,
 * changing the report designer interface, and migrating existing report design data.  Hence, for
 * the time being, we're sticking with the existing report design/table relationship data.
 */
class ListingSubformJoinsReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
    
    /**
     * {@inheritdoc}
     * 
     * @throws ReportException
     */
    protected function doUpdate()
    {
        if (!($this->report instanceof Listing))
        {
            throw new ReportException('Report type of Listing expected, '.get_class($packagedReport->report).' given.');
        }
        
        $moduleDefs     = $this->registry->getModuleDefs();
        $subForms       = $this->registry->getSubForms();
        $table          = $moduleDefs[$this->report->module]['VIEW'] ?: $moduleDefs[$this->report->module]['TABLE'];
        $subQueryFields = [];
        
        // we need to query for linked record ids in order to process output when merging repeated values, or when providing totals for money fields
        $includeLinkIds = $this->report->merge_repeated_values || $this->report->columns->hasMoneyFields();
        
        // determine which columns are linked table fields
        foreach ($this->report->getColumns()->toArray() as $column)
        {
            if ($column->getForm() != $table)
            {
                $subQueryFields[$column->getForm()][] = ['? AS '.$column->getAlias(), $column->getField()];
            }
        }
        
        // determine which group bys are linked table fields
        foreach ($this->report->getGroupBys()->toArray() as $groupBy)
        {
            if ($groupBy->getForm() != $table)
            {
                $subQueryFields[$groupBy->getForm()][] = ['? AS '.$groupBy->getAlias(), $groupBy->getField()];
            }
        }
        
        // create a subquery for each subform designed into the listing report
        foreach ($subQueryFields as $subFormId => $subFormFields)
        {
            $subForm = $subForms[$subFormId];
            
            list($subQuery, $joinCondition) = $subForm->getSubQuery();
            
            $subQuery->select($subFormFields);
            
            if ($includeLinkIds && '' != $subForm->id_column)
            {
                // include the unique identifier for each linked record in the query
                $this->query->select([['? AS '.$subFormId.'_id', $subFormId.'.id']]);
                $subQuery->select([['? AS id', $subForm->id_column]]);
            }
            
            $this->query->joinSubSelect($subQuery, $subFormId, $joinCondition, 'LEFT');
        }
    }
}