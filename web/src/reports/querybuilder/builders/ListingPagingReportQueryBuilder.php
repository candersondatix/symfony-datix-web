<?php

namespace src\reports\querybuilder\builders;

use src\framework\query\SqlWriter;

use src\reports\exceptions\ReportException;
use src\reports\model\listingreport\Listing;
use src\framework\registry\Registry;

/**
 * Builds paging into the listing report query.
 */
class ListingPagingReportQueryBuilder extends AbstractReportQueryBuilder
{
    /**
     * @var Registry
     */
    protected $registry;
    
    /**
     * @var SqlWriter
     */
    protected $writer;
    
    /**
     * @var \DatixDBQuery
     */
    protected $db;
    
    public function __construct(Registry $registry, SqlWriter $writer, \DatixDBQuery $db)
    {
        $this->registry = $registry;
        $this->writer   = $writer;
        $this->db       = $db;
    }
    
    /**
     * {@inheritdoc}
     * 
     * @throws ReportException
     */
    protected function doUpdate()
    {
        if (!($this->report instanceof Listing))
        {
            throw new ReportException('Report type of Listing expected, '.get_class($packagedReport->report).' given.');
        }
        
        if ($this->report->returnAllRecords)
        {
            throw new ReportException('Incorrect listing report query build step loaded - paging not required.');
        }
        
        $this->query->limit($this->report->startPosition, $this->report->getPageLimit());
    }
}