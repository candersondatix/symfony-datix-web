<?php

namespace src\reports;

use src\framework\registry\Registry;
use src\framework\query\QueryFactory;
use src\reports\controllers\ReportWriter;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\model\report\Report;
use src\reports\querybuilder\IReportQueryBuilder;
use src\reports\specifications\ReportSpecificationFactory;
use src\reports\exceptions\ReportException;

/**
 * Class InitialLevelsReportEngine
 *
 * Gathers data for the include initial levels report.
 *
 * @package src\reports
 */
class InitialLevelsReportEngine extends GraphicalReportEngine
{
    /**
     * @var ReportSpecificationFactory
     */
    protected $rsf;
    
    public function __construct(\DatixDBQuery $db, QueryFactory $factory, Registry $registry, IReportQueryBuilder $queryBuilder, DatesEngine $datesEngine, ReportSpecificationFactory $rsf)
    {
        $this->rsf = $rsf;
        parent::__construct($db, $factory, $registry, $queryBuilder, $datesEngine);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getData(PackagedReport $packagedReport, $context = ReportWriter::FULL_SCREEN)
    {
        $notInitialLevelsSpec = $this->rsf->getInitialLevelsSpecification()->not();
        
        if ($notInitialLevelsSpec->isSatisfiedBy($packagedReport))
        {
            throw new ReportException('Incorrect report engine for this packaged report.');
        }
        
        // We need to clear the saved dummy column
        if ($packagedReport->report->columns == 'claims_main.initial_current')
        {
            $packagedReport->report->columns = '';
        }

        $report = $packagedReport->report;
        $packagedReportInitialLevels = clone $packagedReport;
        
        $reservesInitialLevelsSpec = $this->rsf->getAggregateReserveWithInitialLevelsSpecification();
        $remainingReservesInitialLevelsSpec = $this->rsf->getAggregateRemainingReserveWithInitialLevelsSpecification();
        
        $isReservesInitialLevels = $reservesInitialLevelsSpec->isSatisfiedBy($packagedReport);
        $isRemainingReservesInitialLevels = $remainingReservesInitialLevelsSpec->isSatisfiedBy($packagedReport);

        $report->count_style_field_initial_levels = '';

        $data = parent::getData($packagedReport, $context);

        if ($packagedReportInitialLevels)
        {
            if ($isReservesInitialLevels)
            {
                if ($packagedReportInitialLevels->report->count_style_field_value_at == Report::TODAY)
                {
                    $dateFormat = (GetParm('FMT_DATE_WEB', 'GB') == 'US' ? 'm/d/Y' : 'd/m/Y');

                    $packagedReportInitialLevels->report->count_style_field_calendar = (new \DateTime())->format($dateFormat);
                    $packagedReportInitialLevels->report->count_style_field_value_at = Report::SELECT_DATE;
                }
            }
            elseif ($isRemainingReservesInitialLevels)
            {
                // Get initial date from the reserve audit
                if ($packagedReportInitialLevels->report->count_style == Report::COUNT_SUM)
                {
                    $FieldSplit = explode('.', $report->count_style_field_sum);
                }
                elseif ($packagedReportInitialLevels->report->count_style == Report::COUNT_AVE)
                {
                    $FieldSplit = explode('.', $report->count_style_field_average);
                }

                if ($FieldSplit[1] == 'fin_calc_reserve_1')
                {
                    $sql = 'SELECT TOP(1) datetime_from FROM fin_indemnity_reserve_audit ORDER BY datetime_from ASC';
                }
                elseif ($FieldSplit[1] == 'fin_calc_reserve_2')
                {
                    $sql = 'SELECT TOP(1) datetime_from FROM fin_expenses_reserve_audit ORDER BY datetime_from ASC';
                }

                $packagedReportInitialLevels->report->count_style_field_calendar = \DatixDBQuery::PDO_fetch($sql, [], \PDO::FETCH_COLUMN);
                $packagedReportInitialLevels->report->count_style_field_value_at = Report::SELECT_DATE;
            }

            // switch query builder to initial levels query and fetch additional data set
            $this->queryBuilder = (new PackagedReportModelFactory)->getReportQueryBuilder($packagedReportInitialLevels, $this->rsf, $this->registry);
            $dataInitialLevels = parent::getData($packagedReportInitialLevels, $context);

            // We need to set the columns property of the report otherwise the engine doesn't know how to display the data
            $report->columns = 'claims_main.initial_current';

            // Restore the original initial levels value
            $packagedReport->report->count_style_field_initial_levels = 'Y';
        }

        $data = $this->mergeInitialLevels($data, $dataInitialLevels);

        return $this->parentFormatData($packagedReport, $data, $context);
    }

    /**
     * {@inheritdoc}
     */
    protected function parentFormatData(PackagedReport $packagedReport, array $data, $context = ReportWriter::FULL_SCREEN)
    {
        return parent::formatData($packagedReport, $data, $context);
    }

    /**
     * {@inheritdoc}
     */
    protected function formatData(PackagedReport $packagedReport, array $data, $context = ReportWriter::FULL_SCREEN)
    {
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    protected function getHeaders(Report $report, array $axisValues, $axis)
    {
        if ($axis == 'rows')
        {
            return parent::getHeaders($report, $axisValues, $axis);
        }
        elseif ($axis == 'columns')
        {
            switch ($report->count_style)
            {
                case Report::COUNT_AVE:
                    $fieldName = $report->count_style_field_average;
                    break;
                case Report::COUNT_SUM:
                    $fieldName = $report->count_style_field_sum;
                    break;
            }

            $fieldLabel = \Labels_FieldLabel::getFieldDefLabel($fieldName);

            switch ($report->count_style_field_value_at)
            {
                case Report::TODAY:
                    $dateFormat = (GetParm('FMT_DATE_WEB', 'GB') == 'US' ? 'm/d/Y' : 'd/m/Y');
                    $dateLabel = ' ('._tk('field_value_at').' '.(new \DateTime())->format($dateFormat).')';
                    break;
                case Report::SELECT_DATE:
                    $dateLabel = ' ('._tk('field_value_at').' '.FormatDateVal($report->count_style_field_calendar).')';
                    break;
            }

            return (array(
                'I' => $fieldLabel.' ('._tk('initial_value').')',
                'C' => $fieldLabel.$dateLabel
            ));
        }
    }

    /**
     * Function that merges the data array when we include the initial level checkbox (Claims module only).
     *
     * @param array $data Data array of the current or user defined date reserve values.
     * @param array $dataInitialLevels Data array that includes the initial reserve values.
     *
     * @return array Merged data array.
     */
    protected function mergeInitialLevels(array $data, array $dataInitialLevels)
    {
        $mergedData = array();

        foreach ($data as $point)
        {
            foreach ($dataInitialLevels as $initialKey => $initialPoint)
            {
                if ($point['rows'] == $initialPoint['rows'])
                {
                    $mergedData[] = array(
                        'num' => $dataInitialLevels[$initialKey]['num'],
                        'rows' => $point['rows'],
                        'columns' => 'I'
                    );

                    break;
                }
            }

            $mergedData[] = array(
                'num' => $point['num'],
                'rows' => $point['rows'],
                'columns' => 'C'
            );
        }

        if (sizeof($mergedData) == 0)
        {
            $mergedData[] = [
                'num' => 0,
                'rows' => 0,
                'columns' => 'I'
            ];

            $mergedData[] = [
                'num' => 0,
                'rows' => 0,
                'columns' => 'C'
            ];
        }

        return $mergedData;
    }

    /**
     * {@inheritdoc}
     */
    protected function formatAdditionalOptions(Report $report, array $data)
    {
        return parent::formatAdditionalOptions($report, $data);
    }
}