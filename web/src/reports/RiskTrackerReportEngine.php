<?php
namespace src\reports;

use src\reports\exceptions\ReportException;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\report\RiskTrackerReport;

/**
 * Class RiskTrackerReportEngine
 *
 * Gathers data for the Risk Tracker report, a custom report on audit values relating to a single risk record.
 *
 * @package src\reports
 */
class RiskTrackerReportEngine extends GraphicalReportEngine
{
    /**
     * {@inheritdoc}
     */
    public function getData(PackagedReport $packagedReport)
    {
        global $txt;

        $report = $packagedReport->report;
        
        if (!($report instanceof RiskTrackerReport))
        {
            throw new ReportException('Incorrect report type passed.');
        }

        require_once 'Source/libs/RiskRatingBase.php';

        $startdate = strtotime(UserDateToSQLDate($report->startDate));
        $enddate = strtotime(UserDateToSQLDate($report->endDate));

        $grading = array(
            'initial' => array(
                GetFieldLabel('ram_consequence', $txt["consequence"])  => 'ram_consequence',
                GetFieldLabel('ram_likelihood', $txt["likelihood"])    => 'ram_likelihood',
                GetFieldLabel('ram_rating', $txt["rating"])            => 'ram_rating',
                GetFieldLabel('ram_level', $txt["level"])              => 'ram_level'
            ),
            'current' => array(
                GetFieldLabel('ram_cur_conseq', $txt["consequence"])   => 'ram_cur_conseq',
                GetFieldLabel('ram_cur_likeli', $txt["likelihood"])    => 'ram_cur_likeli',
                GetFieldLabel('ram_cur_rating', $txt["rating"])        => 'ram_cur_rating',
                GetFieldLabel('ram_cur_level', $txt["level"])          => 'ram_cur_level'
            ),
            'target' => array(
                GetFieldLabel('ram_after_conseq', $txt["consequence"]) => 'ram_after_conseq',
                GetFieldLabel('ram_after_likeli', $txt["likelihood"])  => 'ram_after_likeli',
                GetFieldLabel('ram_after_rating', $txt["rating"])      => 'ram_after_rating',
                GetFieldLabel('ram_after_level', $txt["level"])        => 'ram_after_level'
            ),
        );

        $dateHeaders = array();
        foreach ($grading as $stage => $fields)
        {
            // get historic risk grading values
            $audit = GetRiskRatingAudit(array(
                'module'   => 'RAM',
                'recordid' => $report->recordid,
                'fields'   => $fields,
                'include_rich_client_data'   => true
            ));

            // get current risk grading values, as well as creation date
            $sql = 'SELECT ' . (implode(', ', $fields)) . ', COALESCE(ram_dcreated, ram_dreported) AS aud_date
                FROM ra_main
                WHERE recordid = :recordid';
            $audit[] = PDO_fetch($sql, array('recordid' => $report->recordid));

            // we need to bump the dates along in our audit data, since we record the date of change against the previous values (!)
            $dates = array();
            foreach ($audit as $update)
            {
                $dates[] = $update['aud_date'];
            }
            array_unshift($dates, array_pop($dates));
            foreach ($audit as $key => $update)
            {
                $audit[$key]['aud_date'] = $dates[$key];
            }

            switch ($stage)
            {
                case 'initial':
                    $field = 'ram_';
                    break;
                case 'current':
                    $field = 'ram_cur_';
                    break;
                case 'target':
                    $field = 'ram_after_';
                    break;
            }

            // now we need to lose any audit values which fall outside of our defined date range
            foreach ($audit as $key => $update)
            {
                $date = strtotime(explode(' ', $audit[$key]['aud_date'])[0]);
                if ($startdate && $date < $startdate)
                {
                    // record the final date value before the cutoff in cases where this stage doesn't have an audit value for the initial dates
                    $previousAuditValue[$stage] = $audit[$key][$field.$report->field];
                    unset($audit[$key]);
                }
                elseif ($enddate && $date > $enddate)
                {
                    unset($audit[$key]);
                }
            }

            foreach ($audit as $update)
            {
                $update['aud_date'] = FormatDateVal($update['aud_date']);

                // if there are multiple updates in the same day then the last one is used in the report
                $data[$stage][$update['aud_date']] = $update[$field.$report->field];
                if (!in_array($update['aud_date'], $dateHeaders))
                {
                    $dateHeaders[] = $update['aud_date'];
                }
            }
        }

        // ensure date headers are ordered sequentially
        foreach ($dateHeaders as $key => $value)
        {
            $dateHeaders[$key] = strtotime(UserDateToSQLDate($value));
        }
        sort($dateHeaders);
        foreach ($dateHeaders as $key => $value)
        {
            $dateHeaders[$key] = FormatDateVal(date('Y-m-d H:i:s.000', $value));
        }

        if ($report->field == 'level')
        {
            // we need to map level codes to numeric values for display in the chart
            if (bYN(GetParm('RISK_MATRIX', 'N')))
            {
                // use incidents risk grading
                $sql = 'SELECT DISTINCT
                        r.rating AS code, c.description, c.cod_listorder, c.lower_rating, c.upper_rating
                    FROM
                        RATINGS_MAP r
                    INNER JOIN
                        CODE_INC_GRADES c ON c.CODE = r.RATING
                    ORDER BY
                        c.cod_listorder, c.lower_rating, c.upper_rating';
            }
            else
            {
                // use standard risk grading
                $sql = 'SELECT
                        code, description
                    FROM
                        code_ra_levels
                    WHERE
                        lower_rating != \'\' AND lower_rating IS NOT NULL AND
                        upper_rating != \'\' AND upper_rating IS NOT NULL
                    ORDER BY
                        lower_rating, upper_rating';
            }
            $levels = PDO_fetch_all($sql);

            // add an empty element to beginning of array so mapped numeric values start at 1 (improves chart display)
            array_unshift($levels, null);
        }

        // construct column headers
        $columnHeaders = array(
            'initial' => 'Initial',
            'current' => 'Current',
            'target' => 'Target'
        );

        // construct main data array and row headers
        $rowHeaders = array();
        $dataSet = array('data' => array(), 'alt_text' => array(), 'text' => array(), 'target' => array());
        $ydata_group = array('initial' => $dataSet, 'current' => $dataSet, 'target' =>$dataSet);
        foreach ($dateHeaders as $date)
        {
            $rowHeaders[$date] = $date;
            foreach ($ydata_group as $stage => $ydata)
            {
                if (!$data[$stage][$date])
                {
                    // the value hasn't changed, so use the previous one
                    if (!$data[$stage][$previousDate[$stage]])
                    {
                        $value = $previousAuditValue[$stage];
                    }
                    else
                    {
                        $value = $data[$stage][$previousDate[$stage]];
                    }
                }
                else
                {
                    $value = $data[$stage][$date];
                    $previousDate[$stage] = $date;
                }

                if ($report->field == 'level')
                {
                    // need to use numeric data mapped to level codes
                    foreach ($levels as $key => $level)
                    {
                        if ($level['code'] == $value)
                        {
                            $value = $key;
                            $description = $level['description'];
                            break;
                        }
                    }

                    if (!is_numeric($value))
                    {
                        // we have a code which no longer exists in the code tables, so all we can do is add it to the levels list w/o any ordering info
                        $levels[] = array('code' => $value, 'description' => $value);
                        $value = count($levels) - 1;
                    }
                }
                else
                {
                    $description = $value;
                }

                $reportData[$date][$stage] = $value;
                if ($report->field == 'level')
                {
                    $reportLabels[$date][$stage] = $levels[$value]['description'].' ('.$columnHeaders[$stage].')';
                }
                else
                {
                    $reportLabels[$date][$stage] = $description.' ('.$columnHeaders[$stage].')';
                }
            }
        }

        if ($report->field == 'level')
        {
            $yAxisValueArray = array();
            // create y axis values for chart
            foreach ($levels as $key => $level)
            {
                if ($level['description'])
                {
                    $yAxisValueArray[$key] = $level['description'];
                }
            }
            if ($yAxisValueArray)
            {
                $report->setyAxisValueArray($yAxisValueArray);
            }
        }

        return array(
            'data'          => $reportData,
            'toolText'      => $reportLabels,
            'rowHeaders'    => $rowHeaders,
            'columnHeaders' => $columnHeaders
        );
    }
}