<?php
namespace src\reports\controllers;

use src\framework\controller\TemplateController;
use src\reports\exceptions\ReportException;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\packagedreport\PackagedReportFactory;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\framework\query\Query;
use src\reports\model\report\Crosstab;
use src\reports\model\report\Report;
use src\reports\model\report\ReportModelFactory;
use src\savedqueries\model\SavedQuery;

class ReportDesignerTemplateController extends TemplateController
{
    /**
    * Full-screen report display.
    * 
    * @throws \URLNotFoundException If the report can't be found by recordid.
    */
    public function reportdesigner()
    {
        $this->hasMenu    = false;
        $this->hasPadding = false;

        // Crosstab reports have additional page elements (title, drilldown select box etc) so we need to determine type here.
        $isCrosstab = false;
        $drillDown = $this->request->getParameter('drilldown') ?: '';
        $drillLevel = $this->request->getParameter('drilllevel') ?: '';

        // TODO - this seems silly, to build a package report from the request so that you can find out what kind of report it is for when creating the report for use later on
        $packagedReport = (new PackagedReportFactory)->createCurrentPackagedReportFromRequest($this->request);

        $isCrosstab = ($packagedReport->report->getReportType() == Report::CROSSTAB);
        $isListing = ($packagedReport->report->getReportType() == Report::LISTING);

        // Save the $isCrosstab on the request object
        $this->request->setParameter('isCrosstab', $isCrosstab);

        // Save the current drill level for future drill down use
        if ($isCrosstab)
        {
            if ($drillDown == '' && $drillLevel == '')
            {
                $drillLevel = 0;
            }
            elseif ($drillDown)
            {
                $drillLevel = intval($drillDown);
            }

            $_SESSION['crosstab_current_drill_level'] = intval($drillLevel);
        }

        $this->call('src\\reports\\controllers\\ReportDesignFormController', 'setAtPromptValues', [
            'packagedReport' => $packagedReport
        ]);

        // Save the export parameter in the Session to be used to export the footer to PDF
        $_SESSION['reportExport'] = ($this->request->getParameter('export') ?: null);

        //If we have created a report from a package that includes the override security setting and changed nothing except the @prompt values,
        //the override security parameter should still be taken into account
        if ($this->request->getParameter('base_report'))
        {
            $baseReport = (new PackagedReportModelFactory)->getMapper()->find($this->request->getParameter('base_report'));

            //The most important thing here is that the report type is the same so the user can't change to a listing and get access to data.
            if ($baseReport->override_security && $baseReport->canPassOverrideSecurityTo($packagedReport))
            {
                $packagedReport->override_security = true;
            }
        }

        // Create the actual report to display
        if($packagedReport->query instanceof SavedQuery && ! ($this->request->getParameter('from_my_reports') == '1' && $packagedReport->query->hasAtPrompt()))
        {
            $reportImage = $this->call('src\\reports\\controllers\\ReportController', 'displayReport', $this->request->getParameters() + array('title' => $this->request->getParameter('context') == 'administration' ? $packagedReport->report->title : $packagedReport->name, 'packagedReport' => $packagedReport));
        }
        elseif($this->request->getParameter('from_my_reports') == '1' && $packagedReport->query->hasAtPrompt())
        {
            $_SESSION['CurrentReport'] = $packagedReport;
        }
        else
        {
            $reportImage = null;
            // reset as no current report
            $_SESSION['CurrentReport'] = null;

        }

        // Create the drill down select
        $drillDownField = $this->buildDrillDownField($isCrosstab, $packagedReport->report->module);

        // Dashboard permissions
        $dashPerms = $_SESSION['Globals']['DAS_PERMS'];

        //if this report is based on a previously-saved design
        if ($this->request->getParameter('base_report'))
        {
            $baseReport = (new PackagedReportModelFactory)->getMapper()->find($this->request->getParameter('base_report'));
        }

        $reportWatermark = _tk('no_report_to_display');
        $hasAtPrompt = $packagedReport->query instanceof SavedQuery && $packagedReport->query->hasAtPrompt();
        
        if($this->request->getParameter('dash_id') || $this->request->getParameter('from_dashboard'))
        {
            $_SESSION['from_dashboard'] = $this->request->getParameter('dash_id') ?: $this->request->getParameter('from_dashboard');
            unset($_SESSION['from_my_reports']);
        }
        elseif($this->request->getParameter('from_my_reports'))
        {
            $_SESSION['from_my_reports'] = $this->request->getParameter('from_my_reports');
            unset($_SESSION['from_dashboard']);
            
            if ($hasAtPrompt)
            {
                $reportWatermark = _tk('please_select_filter_options');
            }
        }

        if( ! $_SESSION['CurrentReport'] || ($packagedReport !== $_SESSION['CurrentReport'] && ($drillLevel === null || $drillLevel === false)))
        {
            unset($_SESSION['from_dashboard']);
            unset($_SESSION['from_my_reports']);
        }

        if($this->request->getParameter('designer_state'))
        {
            $_SESSION['designer_state'] = $this->request->getParameter('designer_state');
        }
        elseif($this->request->getParameter('blank') == 1)
        {
            unset($_SESSION['designer_state']);
        }
        
        $canSaveReport = bYN(GetParm('SAVE_MY_REPORTS', 'N')) &&
                         $baseReport instanceof PackagedReport &&
                         $baseReport->report->getReportType() != Report::LISTING &&
                         $baseReport->createdby == $_SESSION['CurrentUser']->initials;

        $reportNames = array(
            Report::BAR              => 'bar',
            Report::LINE             => 'line',
            Report::CROSSTAB         => 'crosstab',
            Report::LISTING          => 'listing',
            Report::PARETO           => 'pareto',
            Report::PIE              => 'pie',
            Report::GAUGE            => 'gauge',
            Report::TRAFFIC          => 'traffic',
            Report::SPC              => 'spc'
        );

        if ($isCrosstab && $this->request->getParameter('context') != 'administrator')
        {
            $reportTitle = $_SESSION['crosstab_title'];
        }
        else
        {
            if ($this->request->getParameter('context') == 'administrator' || $packagedReport->recordid === null)
            {
                $reportTitle = $packagedReport->report->title;
            }
            else
            {
                $reportTitle = $packagedReport->name;
            }
        }

        $this->response->build('src/reports/views/Report.php', array(
            'designer'       => $this->call('src\\reports\\controllers\\ReportDesignFormController', 'reportDesignerForm', $this->request->getParameters() + array('packagedReport' => $packagedReport)),
            'base_report'    => ($baseReport instanceof PackagedReport ? $baseReport->recordid : ''),
            'overrideSecurity' => $packagedReport->override_security,
            'isCrosstab'     => $isCrosstab,
            'isListing'      => $isListing,
            'isHardCodedListing' => ($isListing && !(is_numeric($packagedReport->report->base_listing_report))),
            'type'           => $packagedReport->report->getReportType(),
            'module'         => $packagedReport->report->module,
            'title'          => $reportTitle,
            'report'         => $reportImage,
            'format'         => $this->request->getParameter('format'),
            'export'         => $this->request->getParameter('export'),
            'drillDownField' => $drillDownField,
            'dashPerms'      => $dashPerms,
            'recordid'       => $packagedReport->recordid,
        	'isOnDashboard'  => (new PackagedReportModelFactory)->getMapper()->isOnDashboard( $baseReport instanceof PackagedReport ? $baseReport : $packagedReport),
            'saveMyReports'  => bYN(GetParm('SAVE_MY_REPORTS', 'N')),
            'customReportBuilder'  => bYN(GetParm('CUSTOM_REPORT_BUILDER', 'Y')),
            'dashboardId'    => $_SESSION['from_dashboard'],
            'fromMyReports'  => $_SESSION['from_my_reports'],
            'reportWatermark' => $reportWatermark,
            'hasAtPrompt'    => $hasAtPrompt,
            'canSaveReport'  => $canSaveReport,
            'reportNames'    => $reportNames
        ));

        // Store the report in the session for future drill down use
        $_SESSION['crosstab_drill_levels'][$drillLevel] = $_SESSION['CurrentReport'];

        return $reportImage;
    }

    /**
     * Builds the drill down select field if we are building a crosstab report.
     *
     * @param bool $isCrosstab True if the report is a crosstab, false otherwise.
     *
     * @return \Forms_SelectField|null
     */
    protected function buildDrillDownField($isCrosstab, $module = '')
    {
        if ($isCrosstab)
        {
            $module = $module ?: $this->request->getParameter('module');
            $moduleDefs = $this->registry->getModuleDefs();
            $fmtTable = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];

            // Get fields already used in the crosstab
            if ($_SESSION['crosstab_rows_field'])
            {
                list($table, $rowsField) = explode('.', $_SESSION['crosstab_rows_field']);
            }

            if ($_SESSION['crosstab_columns_field'])
            {
                list($table, $columnsField) = explode('.', $_SESSION['crosstab_columns_field']);
            }

            $drillDownField = \Forms_SelectFieldFactory::createSelectField('drill', $module, '', '');
            $drillDownField->setSuppressCodeDisplay();

            $params = array(
                'module' => '"'.$module.'"',
                'rowsField' => '"'.$rowsField.'"',
                'columnsField' => '"'.$columnsField.'"'
            );

            $drillDownField->setSelectFunction('getDrilldownFields', $params);

            return $drillDownField;
        }

        return null;
    }
}
