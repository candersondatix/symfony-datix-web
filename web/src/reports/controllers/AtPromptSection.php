<?php foreach ($this->fields as $field): ?>
    <div class="ui-widget form-item">
        <label class="codefield-label" for="<?php echo $field['id'] . (isset($field['start']) ? '_start' : '_title'); ?>"><?php echo $field['label']; ?></label>
        <?php if (isset($field['start'])): ?>
        <div class="codefield-container"><?php echo _tk('at_prompt_start') ?><br/><?php echo $field['start']->getField() ?><br/><?php echo _tk('at_prompt_end') ?><br/><?php echo $field['end']->getField() ?></div>
        <?php else: ?>
        <div class="codefield-container"><?php echo $field['field']->getField() ?></div>
        <?php endif ?>
        <div class="clearer"></div>
    </div>
<?php endforeach ?>
<?php if (count($this->fields) == 0) : ?>
    <div class="ui-widget form-item qtip-red"><?php echo 'Unable to retrieve @prompt fields from where string.' ; ?></div>
<?php endif; ?>
<?php
if ($this->context == 'ajax')
{
    echoJSFunctions();
}    
?>