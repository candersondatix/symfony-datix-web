<?php

namespace src\reports\controllers;

use src\framework\query\QueryFactory;
use src\framework\registry\Registry;
use src\framework\query\SqlWriter;
use src\framework\profiler\Profiler;
use src\reports\ReportEngine;
use src\reports\ListingReportEngine;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\packagedreport\PackagedReportFactory;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\model\report\Report;
use src\reports\model\listingreport\Listing;
use src\reports\exceptions\ReportException;
use src\reports\views\helpers\ListingReportGroupHeader;
use src\system\database\FieldInterface;

class ListingReportWriter extends ReportWriter
{
    /**
     * {@inheritdoc}
     */
    public function writeReport()
    {
        $packagedReport = $this->get('packagedReport');
        $context        = $this->get('context') ?: self::FULL_SCREEN;

        if (!($packagedReport instanceof PackagedReport))
        {
            throw new ReportException('Unable to execute listing report - packaged report instance not provided.');
        }
        
        if (!($packagedReport->report instanceof Listing))
        {
            throw new ReportException('Unable to execute listing report - 
                report type of "src\reports\model\listingreport\Listing" expected, "'.get_class($packagedReport->report).'" given.');
        }
        
        if ($packagedReport->report->isHardCoded())
        {
            $this->writeHardCodedReport($packagedReport, $context);
            return;
        }
        
        switch ($context)
        {
            case self::EXPORT_CSV:
                $this->outputToCSV($packagedReport);
                break;
                
            case self::EXPORT_EXCEL:
                $this->outputToExcel($packagedReport);
                break;
                
            case self::EXPORT_PDF:
                $this->outputToPdf($packagedReport);
                break;

            case self::DASHBOARD:

                $page = json_decode($this->call(get_class($this), 'loadListingReportPage', ['context' => $context]));

                $this->response->build('src/reports/views/WriteListingReport.php', [
                    'packagedReport' => $packagedReport,
                    'rows'           => $page->output,
                    'context'        => $context,
                    'lineWidth'      => $packagedReport->report->columns->getTotalWidth(),
                    'dashboardId'    => $this->get('dbid'),
                    'recordid'       => $this->get('packagedReport')->recordid,
                    'isDashboard'    => true,
                    'isFullscreen'   => false
                ]);

                $page->output = (string) $this->response;
                $this->response->setBody(json_encode($page));
                break;
                
            default:
                
                $page = json_decode($this->call(get_class($this), 'loadListingReportPage', ['context' => $context]));

                // load paging assets
                $this->addJs('src/reports/js/ListingReportPager.js', ['sub_array' => 'bottom']);
                $this->addJs('src/reports/js/listing_report.ready.js', ['sub_array' => 'bottom']);

                $this->sendToJs([
                    'error_loading_report_data' => _tk('error_loading_report_data'),
                    'end_of_report'             => _tk('end_of_report'),
                    'retry_text'                => _tk('retry_text'),
                    'error_retrieving_count'    => _tk('error_retrieving_count'),
                    'suppressClientSideCSRF'    => true
                ]);

                $this->sendToJs([
                    'startPosition' => $page->startPosition,
                    'endOfReport'   => $page->endOfReport,
                    'previousRow'   => $page->previousRow,
                    'columnTotals'  => $page->columnTotals
                ], false);

                $this->response->build('src/reports/views/WriteListingReport.php', [
                    'packagedReport' => $packagedReport,
                    'rows'           => $page->output,
                    'context'        => $context,
                    'lineWidth'      => $packagedReport->report->columns->getTotalWidth(),
                    'isDashboard'    => false,
                    'isFullscreen'   => $context === self::FULL_SCREEN
                ]);
        }
    }
    
    /**
     * Constructs the markup for a "page" of listing report output.
     * 
     * Returns a JSON-formatted reponse which contains additional metadata.
     * Invoked on initial page load when outputting to the screen, and also
     * subsequently via Ajax when paging.
     */
    public function loadListingReportPage()
    {
        $packagedReport = $this->get('packagedReport') ?: $_SESSION['CurrentReport'];
        $context = $this->get('context') ?: self::FULL_SCREEN;

        if (!($packagedReport instanceof PackagedReport))
        {
            throw new ReportException('Unable to execute listing report - cannot find packaged report object in the session.');
        }
        
        $report = $packagedReport->getReport();
        
        if (!($report instanceof Listing))
        {
            throw new ReportException('Unable to execute listing report - 
                report type of "src\reports\model\listingreport\Listing" expected, "'.get_class($report).'" given.');
        }

        /* We don't want to retain any transient properties (like startPosition) from the session, since we
           wouldn't want to start from the third page when returning to a report from a listing, for example.
           So we reset them here and use values from the request, if available. */
        $report->reset();

        if (null !== $this->get('startPosition'))
        {
            $report->setStartPosition($this->get('startPosition'));
        }
        
        if (null !== $this->get('previousRow'))
        {
            $report->setPreviousRow($this->get('previousRow'));
        }
        
        if ($report->getColumns()->hasMoneyFields() && null !== $this->get('columnTotals'))
        {
            $report->getColumns()->setTotals($this->get('columnTotals'));
        }

        $report->setReturnAllRecords(false);
        
        $engine = (new PackagedReportModelFactory)->getReportEngine($packagedReport);
        $data   = $engine->getData($packagedReport, $context);
        
        $this->response->build('src/reports/views/WriteListingReportRows.php', [
            'data' => $data
        ]);

        $json = [
            'output'        => (string) $this->response,
            'startPosition' => $report->startPosition,
            'endOfReport'   => $report->endOfReport,
            'previousRow'   => $report->previousRow,
            'columnTotals'  => $report->getColumns()->getTotals()
        ];

        $this->response->setBody(json_encode($json));
    }
    
    /**
     * Constructs the markup for the report in its entirety to be used subsequently when exporting to PDF.
     * 
     * @param PackagedReport $packagedReport
     */
    protected function outputToPdf(PackagedReport $packagedReport)
    {
        $packagedReport->report->setReturnAllRecords(true);
        
        $engine = (new PackagedReportModelFactory)->getReportEngine($packagedReport);
        $data   = $engine->getData($packagedReport, self::EXPORT_PDF);
        
        $this->response->build('src/reports/views/WriteListingReportRows.php', [
            'data'    => $data,
            'context' => self::EXPORT_PDF
        ]);
        
        $rows = (string) $this->response;
        
        $this->response->build('src/reports/views/WriteListingReport.php', [
            'packagedReport' => $packagedReport,
            'rows'           => $rows,
            'context'        => self::EXPORT_PDF,
            'lineWidth'      => $packagedReport->report->columns->getTotalWidth(),
            'isDashboard'    => false,
            'isFullscreen'   => false
        ]);
    }
    
    /**
     * Writes the listing report output to csv.
     * 
     * @param PackagedReport $packagedReport
     */
    protected function outputToCSV(PackagedReport $packagedReport)
    {
        $packagedReport->report->setReturnAllRecords(true);
        
        $data = (new PackagedReportModelFactory)->getReportEngine($packagedReport)->getData($packagedReport, self::EXPORT_CSV);
        $out  = fopen('php://output', 'w');
        
        $headers = [];
        foreach ($packagedReport->report->columns as $column)
        {
            $title = $column->getTitleWithSuffix($this->packagedReport->report->module);

            //Excel has some custom functionality that triggers if the first column of a CSV file is "ID"
            // (See http://support.microsoft.com/kb/215591). To suppress it, we add a space in that case.
            if (empty($headers) && \UnicodeString::strtoupper($title) == 'ID')
            {
                $title = ' '.$title;
            }

            $headers[] = $title;
        }

        fputcsv($out, $headers);
        
        foreach ($data as $row)
        {
            fputcsv($out, $row);
        }
        
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=DatixWebReport.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        
        fclose($out);
    }
    
    /**
     * Writes the listing report output to Excel.
     * 
     * @param PackagedReport $packagedReport
     */
    protected function outputToExcel(PackagedReport $packagedReport)
    {
        $profiler = new Profiler();
        $profiler->start();
        
        $report = $packagedReport->getReport();
        $columns = $report->getColumns()->toArray();
        $dateFormat = $this->registry->getParm('FMT_DATE_WEB') == 'US' ? 'mm/dd/yyyy' : 'dd/mm/yyyy';
        
        $report->setReturnAllRecords(true);
        
        $data = (new PackagedReportModelFactory)->getReportEngine($packagedReport)->getData($packagedReport, self::EXPORT_EXCEL);
        
        require_once 'Source/classes/Export/DatixExcelSharedDate.php';
        require_once 'export/PHPExcel.php';
        require_once 'export/PHPExcel/Writer/Excel2007.php';
        require_once 'export/PHPExcel/IOFactory.php';
        require_once 'export/PHPExcel/Cell/AdvancedValueBinder.php';
        
        $lastColLetter = \PHPExcel_Cell::stringFromColumnIndex($report->getColumns()->count()-1);
        
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setTitle(_tk('datixweb_excel_export'));
        $objPHPExcel->getProperties()->setSubject(_tk('datixweb_excel_export'));
        $objPHPExcel->getProperties()->setDescription(_tk('datixweb_excel_export'));
        
        $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet = $objPHPExcel->getActiveSheet();
        
        $activeSheet->setTitle(_tk('datix_listing_report'));
        
        $activeSheet->SetCellValue('A2', $packagedReport->name);
        
        // output column headers
        foreach ($report->getColumns() as $colNum => $column)
        {
            $colLetter = \PHPExcel_Cell::stringFromColumnIndex($colNum);
            $activeSheet->SetCellValue($colLetter . '6', $column->getTitle());
        
            if ($column->getWidth() > 0)
            {
                $activeSheet->getColumnDimension($colLetter)->setWidth(round($column->getWidth() / $report->getColumns()->getTotalWidth() * 100 + 20));
            }
        }
        
        // output data
        foreach ($data as $rowNum => $row)
        {
            $currentRow = $rowNum + 7;
            
            if ($row instanceof ListingReportGroupHeader)
            {
                $colLetter = \PHPExcel_Cell::stringFromColumnIndex($row->getIndent());
                $cellId    = $colLetter.($currentRow);
                
                $activeSheet->SetCellValue($cellId, $row->getTitle().': '.$row->getValue());
                $activeSheet->getStyle('A'.($currentRow).':'.$lastColLetter.($currentRow))->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('AFC6DB');
                
                continue;
            }
            
            foreach ($row as $colNum => $cell)
            {
                $colLetter = \PHPExcel_Cell::stringFromColumnIndex($colNum);
                $cellId    = $colLetter.$currentRow;
                
                if (null !== $cell)
                {
                    $activeSheet->SetCellValue($cellId, $cell->getData());
                    
                    if (isset($columns[$colNum]) && $columns[$colNum]->getType() == FieldInterface::DATE)
                    {
                        $activeSheet->getStyle($cellId)->getNumberFormat()->setFormatCode($dateFormat);
                    }
                    
                    if ($cell->getRowSpan() > 1)
                    {
                        $activeSheet->mergeCells($cellId.':'.$colLetter.($currentRow - 1 + $cell->getRowSpan()));
                    }
                    
                    if ($cell->hasColour())
                    {
                        $activeSheet->getStyle($cellId)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($cell->getColour());
                    }
                }
            }
        }
        
        // add footer
        $footer = isset($GLOBALS['dtxexcel_footer']) ? strip_tags($GLOBALS['dtxexcel_footer']) : strip_tags($GLOBALS['dtxprint_footer']);
        if ('' != $footer)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $currentRow+4, $footer);
            $activeSheet->getColumnDimension('A')->setAutoSize(true);
        }
        
        // add header style
        $activeSheet->getStyle('A6:'.$lastColLetter.'6')->applyFromArray([
            'fill' => [
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => [
                    'argb' => '162D54'
                ]
            ],
            'font' => [
                'color' => [
                    'argb' => \PHPExcel_Style_Color::COLOR_WHITE
                ]
            ]
        ]);
        
        // add table style
        $activeSheet->getStyle('A6:'.$lastColLetter.($currentRow))->applyFromArray([
            'borders' => [
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                ]
            ],
            'alignment' => [
                'wrap' => true,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ]);
        
        header('Pragma: public');
        header("Cache-Control: max-age=0");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="DatixWebReport.xlsx"');
        header('Content-Encoding: none');
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        
        $profiler->stopAndPublish();
        
        if (null !== ($currentUser = $_SESSION['CurrentUser']))
        {
            $login = $currentUser->login;
        }
        
        $this->registry->getLogger()->logReporting('Packaged report ID '.$packagedReport->recordid.' ("'.$packagedReport->name.'") for '.$login.' is complete.');
        
        exit;
    }
    
    /**
     * Handles execution and output of hard-coded listing reports.
     * 
     * Currently just delegates to the old procedural functions.
     * 
     * @param PackagedReport $packagedReport
     * @param int            $context
     */
    protected function writeHardCodedReport(PackagedReport $packagedReport, $context)
    {
        require_once 'Source/reporting/CustomListingReports.php';
        
        $moduleDefs = $this->registry->getModuleDefs();
        $module     = $packagedReport->report->module;
        $table      = $moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'];
        $writer     = new SqlWriter();

        $query = $packagedReport->query->createQuery();

        $query->select([$table.'.recordid'], true);

        if ($packagedReport->override_security)
        {
            $query->overrideSecurity();
        }

        $query->replaceAtPrompts($module);

        list($sql, $parameters) = $writer->writeStatement($query);

        $where = $table.'.recordid IN ('.$writer->combineSqlAndParameters($sql, $parameters).')';
        
        ob_start();
        call_user_func($packagedReport->report->base_listing_report, $where);

        if ($context == self::EXPORT_EXCEL)
        {
            ExportReport($_SESSION['listingreportstream'], 'listing', 'excel');
        }
        else
        {
            $this->response->setBody(ob_get_contents());
            ob_end_clean();
        }
    }
  
    /**
     * Fetches the total number of main module records within a listing report.
     * 
     * Used to output the record count on full screen view, via ajax.
     */
    public function getRecordCount()
    {
        $packagedReport = (new PackagedReportFactory)->createCurrentPackagedReportFromRequest($this->request);
        $packagedReport->report->setReturnAllRecords(true);
        
        $engine = (new PackagedReportModelFactory)->getReportEngine($packagedReport);
        
        $this->response->setBody(number_format($engine->getMainRecordCount($packagedReport), 0));
    }
}