<?php
namespace src\reports\controllers;

use src\reports\FusionChartFactory;
use src\reports\ReportEngine;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\GraphicalReportEngine;
use src\reports\exceptions\ReportException;
use src\reports\model\report\Report;

class CrosstabReportWriter extends ReportWriter
{
    /**
     * {@inheritdoc}
     */
    public function writeReport()
    {
        $packagedReport = $this->request->getParameter('packagedReport');
        $engine = $this->request->getParameter('engine');
        $context =  $this->request->getParameter('context') ?: self::FULL_SCREEN;

        if (!($engine instanceof GraphicalReportEngine))
        {
            throw new ReportException('Instance of GraphicalReportEngine expected, '.get_class($engine).' given.');
        }

        $this->response->build('src/reports/views/WriteCrosstabReport.php', array(
            'data'           => $engine->getData($packagedReport, $context),
            'module'         => $this->request->getParameter('module'),
            'context'        => $context,
            'base_report'    => $this->request->getParameter('recordid') ?: $this->request->getParameter('base_report'),
        	'old_query_id'   => $packagedReport->query->old_query_id ?: ( $this->request->getParameter('old_query_id') ?: null)
        ));
        
        return $this->response;
    }
}