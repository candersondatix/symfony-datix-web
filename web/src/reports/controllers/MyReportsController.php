<?php
namespace src\reports\controllers;

use src\framework\controller\TemplateController;
use src\framework\query\Query;
use src\framework\query\QueryFactory;
use src\framework\query\Where;
use src\framework\registry\Registry;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\savedqueries\model\SavedQueryModelFactory;
use src\system\database\field\FieldCollection;
use src\system\database\field\FieldModelFactory;
use src\users\model\UserModelFactory;

class MyReportsController extends TemplateController
{
    /**
     * Produces the My Reports listing.
     */
    public function listMyReports()
    {
        $this->image = "Images/icons/icon_{$this->module}_report.png";

        $this->addCss('js_functions/qTip/jquery.qtip.min.css');

        $this->addJs('js_functions/export.js');
        $this->addJs('src/reports/js/reports.js');
        $this->addJs('js_functions/qTip/jquery.qtip.min.js');

        // get module name
        $this->module = \Sanitize::getModule($this->request->getParameter('module'));
        $ModuleDefs = $this->registry->getModuleDefs();
        $moduleName = $ModuleDefs[$this->module]['NAME'];
        $this->title = _tk('my_reports').' - '.$moduleName;
        
        // TODO: reinstate this when the model has been improved to allow for nested object data to be returned when hydrating Entities via Mappers
        // $myReports = (new PackagedReportModelFactory)->getMyReportsCollection((new Query)->where( ['web_reports.module' => $this->module] ));
        
        list($select, $where, $orderBy, $params) = (new PackagedReportModelFactory)->getMyReportsSQL();
    
        $orderby = \Sanitize::SanitizeString($this->request->getParameter('orderby') ? $this->request->getParameter('orderby') : 'name');
        $order   = ($this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';
        
        $myReportsSql = $select . $where . ' AND web_reports.module = :module ORDER BY '.$orderby.' '.$order;
        $myReports = \DatixDBQuery::PDO_fetch_all($myReportsSql, array_merge($params, ['module' => $this->module]));

        // build list
        $listing = new \RecordLists_RecordList();
        $listing->AddRecordData($myReports);

        // define design
        $listingDesign = new \Listings_ListingDesign(
            array('columns' => array(
                new \Fields_DummyField(array('name' => 'name', 'label' => 'Name', 'type' => 'S')),
                new \Fields_DummyField(array('name' => 'fullname', 'label' => 'Created By', 'type' => 'S')),
                new \Fields_DummyField(array('name' => 'createddate', 'label' => 'Created Date', 'type' => 'DT'))
            )));

        // build options
        foreach ($myReports as $pkgReport)
        {
            $options = array();

            // open in excel (for listing reports)
            if ($pkgReport['type'] == \src\reports\model\report\Report::LISTING && !$this->registry->getDeviceDetector()->isTablet())
            {
                $savedQuery = (new SavedQueryModelFactory)->getMapper()->find($pkgReport['query_id']);
                if (isset($savedQuery))
                {
                    // Displays a popup if the query has @prompt values
                    if ($savedQuery->hasAtPrompt())
                    {
                        $options[3] = '<a href="javascript:displayAtPrompt('.$pkgReport['recordid'].', \''.$savedQuery->module.'\');"> ['._tk('btn_export').']</a>';
                    }
                    else
                    {
                        $options[3] = "
                            <a id=\"export_no_render\" href=\"javascript: void(0);\" onclick=\"
                                var buttons = [];
                                buttons[0]={'value':'"._tk('btn_export')."','onclick':'if(setReturns(1)){var exportFormat = jQuery(\'input[name=reportoutputformat]:checked\').val();var exportOrientation = jQuery(\'input[name=orientation]:checked\').val();var exportPaperSize = jQuery(\'#papersize\').val();GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();exportNoRenderMyReports(exportFormat, exportOrientation, exportPaperSize, ".$pkgReport['recordid'].", \'".$savedQuery->module."\');}'};
                                buttons[1]={'value':'"._tk('btn_cancel')."','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                                PopupDivFromURL('exporttopdfoptions', 'Export', 'app.php?action=httprequest&type=exporttopdfoptions&report_type=listing&recordid=".$pkgReport['recordid']."', '', buttons, '', function(){ removeSuggestCsvExport(); }); return false;\" value=\"Export\">["._tk('btn_export')."]
                            </a>
                        ";
                    }
                }
            }

            // reports created by the current
            // user can be edited or deleted
            if ($pkgReport['createdby'] == $_SESSION["initials"])
            {
                $options[2] = '<a href="?action=reportdesigner&designer_state=open&from_report=1&form_action=show_report&recordid='.$pkgReport['recordid'].'&from_my_reports=1">['._tk('edit_report').']</a>';
                $options[1] = '<a onClick="return confirm(\''._tk('delete_packaged_report_q').'\');" href="?action=deletemyreport&packaged_report='.$pkgReport['recordid'].'&module='.$this->module.'">['._tk('delete_report').']</a>';
            }

            $listingDesign->setOptions($pkgReport['recordid'], $options);
        }

        // display
        $listingDesignDisplay = new \Listings_ListingDisplay($listing, $listingDesign);
        $listingDesignDisplay->Action = 'reportdesigner&designer_state=closed&from_my_reports=1';
        $listingDesignDisplay->setOrder($this->request->getParameter('orderby') ?: 'name');
        $listingDesignDisplay->setDirection($this->request->getParameter('order') ?: 'ASC');
        $listingDesignDisplay->setSortAction(\Sanitize::SanitizeURL($this->request->getParameter('action')) . '&amp;module=' . $this->module);
        
        $this->addCss('src/reports/css/reportDesigner.css');
        
        $this->response->setBody($this->buildCss().$listingDesignDisplay->GetListingHTML());
    }


    public function deleteMyReport ()
    {
        $this->module = \Sanitize::getModule($this->request->getParameter('module'));
        $rep_pack     = \Sanitize::SanitizeInt($this->request->getParameter('packaged_report'));

        $report = new PackagedReportModelFactory();
        $mapper = $report->getMapper();

        $packageReport = $mapper->find($rep_pack);

        $mapper->delete ($packageReport);

        // Delete report from any dashboards it is linked to
        $sql = 'DELETE FROM reports_dash_links WHERE dlink_package_id = :dlink_package_id';
        \DatixDBQuery::PDO_query($sql, array('dlink_package_id' => $rep_pack));

        AddSessionMessage('INFO',_tk('my_report_deleted'));

        $this->redirect ('?action=listmyreports&module='.$this->module);
    }
}
