<?php
namespace src\reports;

use src\reports\controllers\ReportWriter;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\report\Report;
use src\reports\model\report\ParetoChart;
use src\reports\model\report\Crosstab;
use src\reports\querybuilder\IReportQueryBuilder;
use src\reports\exceptions\ReportException;
use src\framework\query\QueryFactory;
use src\framework\registry\Registry;
use src\system\database\FieldInterface;
use src\system\database\CodeFieldInterface;
use src\system\database\code\Code;

/**
 * Responsible for fetching the data for crosstab/graphical reports.
 */
class GraphicalReportEngine extends ReportEngine
{
    /**
     * Responsible for dates-specific reporting functionality.
     * 
     * @var DatesEngine
     */
    protected $datesEngine;
    
    public function __construct(\DatixDBQuery $db, QueryFactory $factory, Registry $registry, IReportQueryBuilder $queryBuilder, DatesEngine $datesEngine)
    {
        parent::__construct($db, $factory, $registry, $queryBuilder);
        $this->datesEngine  = $datesEngine;
    }

    /**
     * {@inheritdoc}
     */
    protected function formatData(PackagedReport $packagedReport, array $data, $context = ReportWriter::FULL_SCREEN)
    {
        $report = $packagedReport->report;

        if (empty($data))
        {
            // no records in the report
            return $data;
        }
        
        // Get report type
        $reportType = get_class($report);
        
        // initialise array variables TODO Consider the sanity of this assignment.
        $formattedData = $rows = $columns = $totals = $links = array();
        
        // reformat multicode data so we can process in a consistent way
        $data = $this->formatMultiCodeData($report, $data);
        
        // retrieve information about code statuses so they can be excluded from the data set
        // NB - unfortunately, codes can't be excluded by joining on code tables because of the way multicode data is stored
        list($rowCodes, $columnCodes) = $this->getReportCodes($report);
        
        // loop through the raw data set and group data points by row
        foreach ($data as $point)
        {
            if ((isset($rowCodes[$point['rows']]) && $rowCodes[$point['rows']]->getStatus() == Code::DISABLED)
                    || (isset($columnCodes[$point['columns']]) && $columnCodes[$point['columns']]->getStatus() == Code::DISABLED))
            {
                // ignore disabled codes
                continue;
            }
            
            if ($report->columns != '') // we're reporting across two axes (rows and columns)
            {
                // add data point to the 2D grouped data array and calculate row/column totals
                $formattedData[$point['rows']][$point['columns']] += $point['num'];
                $totals['rows'][$point['rows']] += $point['num'];
                $totals['columns'][$point['columns']] += $point['num'];

                if ($context != ReportWriter::DASHBOARD && !$packagedReport->override_security)
                {
                    // build drill-in link for this data point
                    if ($reportType == 'src\reports\model\report\Crosstab')
                    {
                        // Need to encode rowsvalue and columnsvalue because we can have special characters in the value.
                        $links['values'][$point['rows']][$point['columns']] = 'app.php?action=drillin&rowsvalue='.urlencode($point['rows']).'&columnsvalue='.urlencode($point['columns']);
                    }
                    else
                    {
                        $links['values'][$point['rows']][$point['columns']] = 'app.php?action=drillin&rowsvalue=' . $point['rows'] . '&columnsvalue=' . $point['columns'];
                    }
                }
                elseif($context == ReportWriter::DASHBOARD)
                {
                    $links['values'][$point['rows']][$point['columns']] = $GLOBALS['scripturl'].'?action=reportdesigner&designer_state=closed&recordid='.$_SESSION['CurrentReport']->recordid.(isset($_REQUEST['dbid']) && $_REQUEST['dbid'] != '' ? '&dash_id='.\Sanitize::SanitizeString($_REQUEST['dbid']).'&token='.\CSRFGuard::getCurrentToken() : '');
                }
                
                if (!in_array($point['columns'], $columns))
                {
                    // build a list of distinct column types used when building axis labels (headers)
                    $columns[] = $point['columns'];
                }
            }
            else
            {
                // we're reporting across one axis (rows)
                $formattedData[$point['rows']]  += $point['num'];
                $totals['rows'][$point['rows']] += $point['num'];

                if ($context != ReportWriter::DASHBOARD && !$packagedReport->override_security)
                {
                    $links['values'][$point['rows']] = 'app.php?action=drillin&rowsvalue='.$point['rows'];
                }
                elseif($context == ReportWriter::DASHBOARD)
                {
                    $links['values'][$point['rows']] = $GLOBALS['scripturl'].'?action=reportdesigner&designer_state=closed&recordid='.$_SESSION['CurrentReport']->recordid.(isset($_REQUEST['dbid']) && $_REQUEST['dbid'] != '' ? '&dash_id='.\Sanitize::SanitizeString($_REQUEST['dbid']).'&token='.\CSRFGuard::getCurrentToken() : '');
                }
            }

            if (!in_array($point['rows'], $rows))
            {
                // build a list of distinct row types used when building axis labels (headers)
                $rows[] = $point['rows'];
            }
        }

        // calculate the overall total
    	$totals['total'] = array_sum($totals['rows']);
    	
    	// build drill-in link for the overall total
    	$links['total'] = 'app.php?action=drillin&rowsvalue=DTX_TOTAL&columnsvalue=DTX_TOTAL';
        
        // build drill-in links for the row/column totals
        if ($report->columns != '')
        {
            foreach (array_keys($totals['rows']) as $row)
            {
                if ($reportType == 'src\reports\model\report\Crosstab')
                {
                    // Need to encode rowsvalue because we can have special characters in the value.
                    $links['row_totals'][$row] = 'app.php?action=drillin&rowsvalue='.urlencode($row).'&columnsvalue=DTX_TOTAL';
                }
                else
                {
                    $links['row_totals'][$row] = 'app.php?action=drillin&rowsvalue=' . $row . '&columnsvalue=DTX_TOTAL';
                }
            }
            
            foreach (array_keys($totals['columns']) as $column)
            {
                if ($reportType == 'src\reports\model\report\Crosstab')
                {
                    // Need to encode columnsvalue because we can have special characters in the value.
                    $links['column_totals'][$column] = 'app.php?action=drillin&rowsvalue=DTX_TOTAL&columnsvalue='.urlencode($column);
                }
                else
                {
                    $links['column_totals'][$column] = 'app.php?action=drillin&rowsvalue=DTX_TOTAL&columnsvalue=' . $column;
                }
            }
        }

        // build the array which contains all the information required to display the report
        $data = array(
            'data'          => $formattedData,
            'totals'        => $totals,
            'rowHeaders'    => $this->getHeaders($report, $rows, 'rows'),
            'columnHeaders' => !empty($columns) ? $this->getHeaders($report, $columns, 'columns') : array(),
            'links'         => $links
        );

        // apply any additional options formatting and return
        return $this->formatAdditionalOptions($report, $data);
    }

    /**
     * Retrieves the codes for the rows/columns fields, if they use codes.
     *
     * @param Report $report The report object.
     *
     * @throws exceptions\ReportException
     * @return array The row/column codes.
     */
    protected function getReportCodes(Report $report)
    {
        $rowCodes      = array();
        $columnCodes   = array();
        $fieldDefs     = $this->registry->getFieldDefs();

        if (null === ($rowsField = $fieldDefs[$report->rows]))
        {
            throw $this->invalidFieldException($report->rows);
        }
        
        if ($report->columns != '' && null === ($columnsField = $fieldDefs[$report->columns]))
        {
            throw $this->invalidFieldException($report->columns);
        }

        //Depending on the RISK_MATRIX global, the codes for some ram_... fields may come from incidents.
        if (in_array($rowsField->getName(), array('ram_cur_level', 'ram_after_level', 'ram_level')) && bYN(GetParm('RISK_MATRIX', 'N')))
        {
            $rowsField = $fieldDefs['incidents_main.inc_grade'];
        }
        if ($columnsField && in_array($columnsField->getName(), array('ram_cur_level', 'ram_after_level', 'ram_level')) && bYN(GetParm('RISK_MATRIX', 'N')))
        {
            $columnsField = $fieldDefs['incidents_main.inc_grade'];
        }

        if ($rowsField instanceof CodeFieldInterface)
        {
            $rowCodes = $rowsField->getCodes();
        }
        
        if ($columnsField instanceof CodeFieldInterface)
        {
            $columnCodes = $columnsField->getCodes();
        }
        
        return array($rowCodes, $columnCodes);
    }
    
    /**
     * Reformats raw report data that contains multicode values so that it can be processed consistently.
     * 
     * @param Report $report The report object.
     * @param array  $data   The raw report data.
     * 
     * @return array $data The formatted data.
     */
    protected function formatMultiCodeData(Report $report, array $data)
    {
        $fieldDefs = $this->registry->getFieldDefs();
        $rowDef    = $fieldDefs[$report->rows];

        if ($report->columns != '')
        {
            $columnDef = $fieldDefs[$report->columns];
        }
        
        if ($rowDef->getType() == FieldInterface::MULTICODE)
        {
            $data = $this->expandMultiCodes($data, 'rows');
        }
        
        if (isset($columnDef) && $columnDef->getType() == FieldInterface::MULTICODE)
        {
            $data = $this->expandMultiCodes($data, 'columns');
        }

        return $data;
    }
    
    /**
     * Expands multicode data so that each value has its own row.
     * 
     * @param array  $data The raw report data.
     * @param string $axis The axis we're extracting multicodes from (rows|columns).
     * 
     * @return array $explodedData The exploded data array.
     */
    protected function expandMultiCodes(array $data, $axis)
    {
        $explodedData = array();
        foreach ($data as $point)
        {
            $codes = explode(' ', $point[$axis]);
            foreach ($codes as $code)
            {
                $newPoint = $point;
                $newPoint[$axis] = $code;
                $explodedData[] = $newPoint;
            }
        }
        return $explodedData;
    }
    
    /**
     * Retrieves the axis labels used in the report.
     * 
     * @param Report $report     The report object.
     * @param array  $axisValues The values for this axis from the report data.
     * @param string $axis       rows|columns.
     * 
     * @throws ReportException If the field type is not valid.
     * 
     * @return array
     */
    protected function getHeaders(Report $report, array $axisValues, $axis)
    {
        $fieldDefs  = $this->registry->getFieldDefs();
        $fieldDef   = $fieldDefs[$report->$axis];

        $null_key = array_search(null, $axisValues);
        if($null_key !== false)
        {
            unset($axisValues[$null_key]);
        }
        
        // Each of the delegated header functions returns a full set of valid labels for a given field.
        // They are then subsequently filtered out with respect to the actual report data set.
        switch ($fieldDef->getType())
        {
            case FieldInterface::CODE:
            case FieldInterface::MULTICODE:
            case FieldInterface::YESNO:
                $headers = $this->getCodeHeaders($fieldDef, $axisValues);
                break;
                
            case FieldInterface::DATE:
                $headers = $this->getDateHeaders($report, $axis, $axisValues);
                break;
                
            case FieldInterface::MONEY:
                $headers = $this->getMoneyHeaders($axisValues);
                break;
                
            case FieldInterface::NUMBER:
                $headers = $this->getNumberHeaders($axisValues);
                break;
                
            default:
                throw new ReportException('Error generating axis labels - invalid field type.');
        }

        if($null_key !== false)
        {
            $headers[''] = _tk('reporting_no_value');
        }
        
        // filter out headers for data points that don't exist within this report
        if($fieldDef->getType() != FieldInterface::DATE)
        {
            foreach ($headers as $key => $header)
            {
                if (!in_array( (string) $key, $axisValues)) // if the key is 0, the result always will be true. @link https://bugs.php.net/bug.php?id=14343
                {
                    unset($headers[$key]);
                }
            }
        }

        return $headers;
    }
    
    /**
     * Retrieve axis labels for code/multicode fields.
     * 
     * @param CodeFieldInterface $field      The field used for this axis.
     * @param array              $axisValues The values (codes) for this axis from the report data.
     * 
     * @return array
     */
    protected function getCodeHeaders(CodeFieldInterface $field, array $axisValues)
    {
        $fieldDefs = $this->registry->getFieldDefs();

        //Depending on the RISK_MATRIX global, the codes for some ram_... fields may come from incidents.
        //TODO: this should probably go inside the ->getCodes() function on an entity set up specifically for these fields.
        if (in_array($field->getName(), array('ram_cur_level', 'ram_after_level', 'ram_level')) && bYN(GetParm('RISK_MATRIX', 'N')))
        {
            $field = $fieldDefs['incidents_main.inc_grade'];
        }

        $codes   = $field->getCodes();
        $headers = array();

        foreach ($codes as $code)
        {
        	$headers[$code->code] = $code->description ?: $code->code;
        }
        
        // add headers for codes that exist in the data for this report but not the code tables (because they've been subsequently deleted)
        $existingCodes = array_keys($headers);
        foreach ($axisValues as $code)
        {
            if (!in_array($code, $existingCodes))
            {
                $headers[$code] = $code;
            }
        }
        
        return $headers;
    }
    
    /**
     * Retrieve axis labels for code/multicode fields.
     * 
     * @param Report $report     The report object.
     * @param string $axis       The date report option.
     * @param array  $axisValues The values for this axis from the report data.
     * 
     * @throws ReportException If the dateOption is not recognised.
     * 
     * @return array $headers
     */
    protected function getDateHeaders(Report $report, $axis, array $axisValues)
    {
        $dateOption = $axis.'_date_option';
        switch ($report->$dateOption)
        {
            case Report::DAY_OF_WEEK:
                $headers = $this->datesEngine->getWeekDayHeaders();
                break;
                
            case Report::WEEK:
                $headers = $this->datesEngine->getWeekHeaders();
                break;
                
            case Report::WEEK_DATE:
            	$headers = $this->datesEngine->getWeekDateHeaders($axisValues);
            	break;
                
            case Report::MONTH:
                $headers = $this->datesEngine->getMonthHeaders();
                break;
                
            case Report::FINANCIAL_MONTH:
                $headers = $this->datesEngine->getFinancialMonthHeaders();
                break;

            case Report::QUARTER:
                $headers = $this->datesEngine->getQuarterHeaders($axisValues);
                break;
                
            case Report::FINANCIAL_QUARTER:
                $headers = $this->datesEngine->getQuarterHeaders($axisValues, true);
                break;
                
            case Report::YEAR:
            case Report::FINANCIAL_YEAR:
                $headers = $this->datesEngine->getYearHeaders($axisValues);
                break;
                
            case Report::MONTH_AND_YEAR:
                $headers = $this->datesEngine->getMonthAndYearHeaders($axisValues);
                break;
                
            default:
                throw new ReportException('Error generating axis labels - unable to determine date option');
        }
        return $headers;
    }
    
    /**
     * Retrieve axis labels for money fields.
     * 
     * @param array $axisValues The values for this axis from the report data.
     * 
     * @return array $headers
     */
    protected function getMoneyHeaders(array $axisValues)
    {
        natsort($axisValues);
        return array_combine($axisValues, array_map('FormatMoneyVal', $axisValues));
    }
    
    /**
     * Retrieve axis labels for number fields.
     * 
     * @param array $axisValues The values for this axis from the report data.
     * 
     * @return array $headers
     */
    protected function getNumberHeaders(array $axisValues)
    {
        natsort($axisValues);
        return array_combine($axisValues, array_map('floatval', $axisValues));
    }
    
    /**
     * Used when we're unable to locate the definition for a given field.
     * 
     * @param string $fieldName
     * 
     * @return ReportException
     */
    protected function invalidFieldException($fieldName)
    {
        $message = 'Unable to load definition for field: '.$fieldName;
        $this->registry->getLogger()->logEmergency($message);
        return new ReportException($message);
    }
    
    /**
     * Applies additional options that use post-query data formatting to the report data set.
     * 
     * @param Report $report The report object.
     * @param array  $data   The report data.
     * 
     * @return array $data 
     */
    protected function formatAdditionalOptions(Report $report, array $data)
    {
    	$data = $this->showNumberOfRecordsNoValueField($report, $data);
    	
        $data = $this->limitToTop($report, $data);

        $data = $this->showPercentages($report, $data);
        
        $data = $this->showToolTexts($report, $data);

        return $data;
    }
    
    /**
     * Applies the "limit to top" option to the report data.
     * 
     * Limit to top restricts the result set to the top n rows, in descending order of row totals.
     * 
     * @param Report $report The report object.
     * @param array  $data   The report data.
     * 
     * @return array $data
     */
    protected function limitToTop(Report $report, array $data)
    {
        if ((int) $report->limit_to_top > 0)
        {
            $formattedData = [];
            $rowHeaders    = [];
            
            // sort data array by descending row order and slice the top n rows
            arsort($data['totals']['rows']);
            $data['totals']['rows'] = array_slice($data['totals']['rows'], 0, $report->limit_to_top, true);
            $data['totals']['total'] = array_sum($data['totals']['rows']);
            
            foreach ($data['totals']['rows'] as $key => $value)
            {
                $formattedData[$key] = $data['data'][$key];
                $rowHeaders[$key] = $data['rowHeaders'][$key];
            }
            
            $data['data'] = $formattedData;
            $data['rowHeaders'] = $rowHeaders;
            
            if ($report->columns != '')
            {
                // we need to re-calculate the column totals
                $columnTotals = [];
                foreach ($formattedData as $columns)
                {
                    foreach ($columns as $key => $value)
                    {
                        $columnTotals[$key] += $value;
                    }
                }
                $data['totals']['columns'] = $columnTotals;
                
                // remove headers for columns that have no value
                $data['columnHeaders'] = array_intersect_key($data['columnHeaders'], $data['totals']['columns']);
            }
        }
        return $data;
    }
    
    /**
     * Applies the "toolText" option to the report data.
     * 
     * @author gszucs
     * @param Report $report
     * @param array $data
     * @return array
     */
    protected function showToolTexts(Report $report, array $data)
    {
    	$withHeaderText = !( $report instanceof Crosstab );
    	
    	foreach ($data['rowHeaders'] as $rowCode => $rowHeaderText)
    	{
    		if ($report->columns != '')
    		{
    			// change crosstab specific values
    			if( $report instanceof Crosstab )
    			{
    				$data['totals']['rows'][$rowCode] = $this->generateToolText($report, $data['totals']['rows'][$rowCode], '', (isset($data['percentages']['rowtotals'][$rowCode]) ? $data['percentages']['rowtotals'][$rowCode] : ''));
    			}
    			
    			foreach ($data['columnHeaders'] as $columnCode => $columnHeaderText)
    			{
    				$data['toolText'][$rowCode][$columnCode] = $this->generateToolText($report, $data['data'][$rowCode][$columnCode], ($withHeaderText ? $columnHeaderText : ''), (isset($data['percentages'][$rowCode][$columnCode]) ? $data['percentages'][$rowCode][$columnCode] : ''));
    			}
    		}
    		else 
    		{
    			// change crosstabb specific values
    			if( $report instanceof Crosstab )
    			{
    				$data['totals']['rows'][$rowCode] = $this->generateToolText($report, $data['totals']['rows'][$rowCode], '', (isset($data['percentages'][$rowCode]) ? $data['percentages'][$rowCode] : ''));
    			}
    			
    			$data['toolText'][$rowCode] = $this->generateToolText($report, $data['data'][$rowCode], ($withHeaderText ? $rowHeaderText : ''), (isset($data['percentages'][$rowCode]) ? $data['percentages'][$rowCode] : ''));
    		}
    	}
    	
    	// change crosstabb specific values
    	if( $report instanceof Crosstab )
    	{
	    	foreach ($data['totals']['columns'] as $colCode => &$value)
	    	{
	    		$value = $this->generateToolText($report, $value, '', (isset($data['percentages']['columntotals'][$colCode]) ? $data['percentages']['columntotals'][$colCode] : ''));
	    	}
	    	
	    	if( isset( $data['totals']['total'] ) )
	    	{
	    		$data['totals']['total'] = $this->generateToolText($report, $data['totals']['total'], '', 100);
	    	}
	    	
	    	// default null value
	    	$data['toolText']['null'] = $this->generateToolText($report, 0, '', 0);
    	}
    	
    	return $data;
    }

    /**
     * Generate the proper tooltext based on the "percentages" property of the report
     *
     * @author gszucs
     * @param Report $report
     * @param $value
     * @param $headerText
     * @param $percentage
     * @internal param array $data
     * @return array
     */
    protected function generateToolText(Report $report, $value, $headerText, $percentage = Report::TOOLTIP_VALUE_ONLY)
    {
        // handle money fields formatting
        $FieldDefs = $this->registry->getFieldDefs();

        $countField = '';
        $countFieldName = '';

        switch ($report->count_style)
        {
            case Report::COUNT_AVE:
                $countFieldName = $report->count_style_field_average;
                break;
            case Report::COUNT_SUM:
                $countFieldName = $report->count_style_field_sum;
                break;
        }

        $countField = $FieldDefs[$countFieldName];

        if (
            (!is_null($countField) && $countField->getType() == FieldInterface::MONEY)
            || substr($countFieldName, 0, 20) == 'claims_main.pay_type')
        {
            $value = FormatMoneyVal($value);
        }

        // Protect $value and $percentage against not showing any values
        $percentage = ($percentage == '' ? '0' : $percentage);

        if ($value == '')
        {
            $value = '0';

            if (!is_null($countField) && $countField->getType() == FieldInterface::MONEY)
            {
                $value = FormatMoneyVal($value);
            }
        }

        // handle percentages
    	switch ($report->percentages)
    	{
    		case Report::TOOLTIP_PERCENTAGE_ONLY:
    			return $percentage .'%'. ( $headerText !== '' ? ' ('. $headerText .')' : '' );
    			break;
    	
    		case Report::TOOLTIP_PERCENTAGE_AND_VALUE:
    			return $value .( $headerText !== '' ? ' ('. $headerText .')' : '' ) .' ('. $percentage .'%)';
    			break;
    	
    		case Report::TOOLTIP_VALUE_ONLY:
    		default:
    			return $value .( $headerText !== '' ? ' ('. $headerText .')' : '' );
    	}
    }

    /**
     * Applies the "Show percentages" option to the report data.
     *
     * Show percentages will show on the chart tooltip the percentages for each column.
     *
     * @param Report $report The report object.
     * @param array  $data   The report data.
     *
     * @return array $data
     */
    protected function showPercentages(Report $report, array $data)
    {
        if (in_array($report->percentages, array(Report::TOOLTIP_PERCENTAGE_AND_VALUE, Report::TOOLTIP_PERCENTAGE_ONLY)) || $report instanceof ParetoChart)
        {
            if ($data['totals']['total'] != 0)
            {
                foreach ($data['data'] as $rowKey => $rowValue)
                {
                    if ($report->columns != '')
                    {
                        foreach ($rowValue as $columnKey => $columnValue)
                        {
                            $data['percentages'][$rowKey][$columnKey] = round(($columnValue / $data['totals']['total']) * 100, 2);
                        }
                    }
                    else
                    {
                        $data['percentages'][$rowKey] = round(($rowValue / $data['totals']['total']) * 100, 2);
                    }
                }
                
                if ($report->columns != '')
                {
                    foreach ($data['totals']['rows'] as $rowKey => $rowValue)
                    {
                        $data['percentages']['rowtotals'][$rowKey] = round(($rowValue / $data['totals']['total']) * 100, 2);
                    }
                    
                    foreach ($data['totals']['columns'] as $columnKey => $columnValue)
                    {
                        $data['percentages']['columntotals'][$columnKey] = round(($columnValue / $data['totals']['total']) * 100, 2);
                    }
                }
            }
        }
        
        return $data;
    }

    /**
     * Applies the "Show number of records with no value selected" option to the report data.
     *
     * Show number of records with no value selected will show on the chart values equal to zero.
     *
     * @param Report $report The report object.
     * @param array  $data   The report data.
     *
     * @return array $data
     */
    protected function showNumberOfRecordsNoValueField(Report $report, array $data)
    {
        if ($report->null_values === true && $report->rows != '')
        {
            $data['rowHeaders'][''] = _tk('reporting_no_value');
        }

        if ($report->null_values === true && $report->columns != '')
        {
            $data['columnHeaders'][''] = _tk('reporting_no_value');
        }

        return $data;
    }

    
}
