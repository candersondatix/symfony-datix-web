<?php

namespace src\reports\model\listingreport;

use src\framework\model\Mapper;
use src\framework\query\Query;

abstract class OptionMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'reports_formats';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->select([
                           'reports_formats.rpf_field AS field',
                           'CASE WHEN field_formats.fmt_data_type = \'C\' AND field_formats.fmt_data_length > 6 THEN \'T\' ELSE reports_formats.rpf_data_type END AS type',
                           'reports_formats.rpf_form AS form',
                           'reports_formats.rpf_module AS module',
                           'subforms.sfm_tables AS tables'
                      ])
              ->join('subforms', 'reports_formats.rpf_form = subforms.sfm_form', 'LEFT')
              ->join('field_formats', 'reports_formats.rpf_module = field_formats.fmt_module AND subforms.sfm_form = field_formats.fmt_table AND reports_formats.rpf_field = field_formats.fmt_field', 'LEFT');
        
        return parent::select($query);
    }
}