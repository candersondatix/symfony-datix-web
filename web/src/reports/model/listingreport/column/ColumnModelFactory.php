<?php

namespace src\reports\model\listingreport\column;

use src\framework\model\ModelFactory;

class ColumnModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ColumnFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ColumnMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ColumnCollection($this->getMapper(), $this->getEntityFactory());
    }
}