<?php

namespace src\reports\model\listingreport\column;

use src\reports\model\listingreport\OptionFactory;

class ColumnFactory extends OptionFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\column\\Column';
    }
}