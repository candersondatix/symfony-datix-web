<?php

namespace src\reports\model\listingreport\column;

use src\reports\model\listingreport\OptionMapper;
use src\framework\query\Query;
use src\reports\model\listingreport\Listing;

class ColumnMapper extends OptionMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\column\\Column';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->where(['rpf_section' => 'DETAIL']);
              
        return parent::select($query);
    }
    
    /**
     * Retrieves the columns for a given listing report design.
     * 
     * @param Listing $report
     */
    public function getColumnsForListingReport(Listing $report)
    {
        $query = $this->factory->getQueryFactory()->getQuery();
        $query->where(['rpf_rep_id' => $report->base_listing_report])
              ->orderBy(['rpf_order']);
        
        $columns = $this->factory->getCollection();
        $columns->setQuery($query);
        
        return $columns;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getFieldMappings()
    {
        return [
            'title'           => 'rpf_title',
            'order'           => 'rpf_order',
            'width'           => 'rpf_width',
            'format'          => 'rpf_format',
            'showDescription' => 'rpf_descr'
        ];
    }
}