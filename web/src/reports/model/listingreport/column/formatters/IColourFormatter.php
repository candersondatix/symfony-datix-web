<?php

namespace src\reports\model\listingreport\column\formatters;

interface IColourFormatter
{
    /**
     * Returns the colour associated with a listing report cell's value.
     * 
     * @param string $rawData
     */
    public function getColour($rawData);
}