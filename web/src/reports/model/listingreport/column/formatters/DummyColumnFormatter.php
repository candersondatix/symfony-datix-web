<?php

namespace src\reports\model\listingreport\column\formatters;

/**
 * Used when no specific formatting is required - simply returns the raw data value.
 */
class DummyColumnFormatter extends ColumnFormatter
{
    /**
     * {@inheritdoc}
     */
    public function doFormatting($data, $context)
    {
        return $data;
    }
}