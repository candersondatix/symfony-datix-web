<?php

namespace src\reports\model\listingreport\column\formatters;

class YesNoColumnFormatter extends ColumnFormatter
{
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        switch ($data)
        {
            case 'Y':
                $data = _tk('yes');
                break;
                
            case 'N':
                $data = _tk('no');
                break;
                
            default:
                $data = '';
        }
        return $data;
    }
}