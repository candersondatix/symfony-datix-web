<?php

namespace src\reports\model\listingreport\column\formatters;

class TimeColumnFormatter extends CachedColumnFormatter
{
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        if (strlen($data) == 4)
        {
            $data = substr($data, 0, 2).':'.substr($data, 2, 2);
        }
        return $data;
    }
}