<?php

namespace src\reports\model\listingreport\column\formatters;

use src\reports\model\listingreport\Option;
use src\framework\registry\Registry;
use src\reports\model\report\Report;

class DateOptionformatter extends CachedColumnFormatter
{
    /**
     * @var Option
     */
    protected $option;
    
    public function __construct(Option $option, Registry $registry)
    {
        $this->option = $option;
        $this->registry = $registry;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        if ('' == $data)
        {
            return $data;
        }

        switch ($this->option->getDateOption())
        {
            case Report::MONTH_AND_YEAR:
                $date = \DateTime::createFromFormat('Ym', $data);
                return $date->format('M').' '.$date->format('Y');
                
            case Report::FINANCIAL_YEAR:
                return $data.' - '.($data+1);
                
            default:
                return $data;
        }
    }
}