<?php

namespace src\reports\model\listingreport\column\formatters;

use src\reports\controllers\ReportWriter;

class TextColumnFormatter extends ColumnFormatter
{
    /**
     * {@inheritdoc}
     */
    protected function escapeOutput($data, $context)
    {
        return $data;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        if (!in_array($context, [ReportWriter::EXPORT_EXCEL, ReportWriter::EXPORT_CSV]))
        {
            $data = \UnicodeString::str_ireplace("\n", "<br />", htmlspecialchars($data));
        }


        if($context == ReportWriter::EXPORT_EXCEL)
        {
            $data = preg_replace('/^=/', '\'=', $data);
        }

        return $data;
    }
}