<?php

namespace src\reports\model\listingreport\column\formatters;

/**
 * Automatically handles the caching of formatted values.
 */
abstract class CachedColumnFormatter extends ColumnFormatter
{
    /**
     * @var array
     */
    protected $cache;
    
    /**
     * {@inheritdoc}
     */
    public function format($data, $context)
    {
        if (null === $this->cache[$data])
        {
            $this->cache[$data] = parent::format($data, $context);
        }
        return $this->cache[$data];
    }
}