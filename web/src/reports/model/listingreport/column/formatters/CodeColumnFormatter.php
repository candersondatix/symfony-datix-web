<?php

namespace src\reports\model\listingreport\column\formatters;

use src\system\database\CodeFieldInterface;

class CodeColumnFormatter extends CachedColumnFormatter implements IColourFormatter
{
    /**
     * @var CodeCollection
     */
    protected $codes;
    
    /**
     * Maps the code/colour associations, so we don't have to access the object in the code collection each time.
     * 
     * @var array
     */
    protected $colours;
    
    /**
     * Whether or not we're outputting the description or the raw code.
     * 
     * @var boolean
     */
    protected $showDescription;
    
    public function __construct(CodeFieldInterface $field, $showDescription)
    {
        $this->codes = $field->getCodes();
        $this->colours = [];
        $this->showDescription = $showDescription;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        if ($this->showDescription != 'Y')
        {
            return $data;
        }
        
        if (null !== ($code = $this->codes[$data]))
        {
            return $code->description;
        }
        return $data;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getColour($rawData)
    {
        if (!isset($this->colours[$rawData]))
        {
            if (null !== ($code = $this->codes[$rawData]))
            {
                $this->colours[$rawData] = $code->cod_web_colour;
            }
            else
            {
                $this->colours[$rawData] = '';
            }
        }
        
        return $this->colours[$rawData];
    }
}