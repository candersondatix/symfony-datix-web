<?php

namespace src\reports\model\listingreport\column\formatters;

require_once 'Source/libs/Subs.php';

class DateColumnFormatter extends CachedColumnFormatter
{
    /**
     * {@inheritdoc}
     */
    protected function doFormatting($data, $context)
    {
        return FormatDateVal($data);
    }
}