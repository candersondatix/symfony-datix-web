<?php

namespace src\reports\model\listingreport\orderby;

use src\framework\model\EntityCollection;

class OrderByCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\orderby\\OrderBy';
    }
}