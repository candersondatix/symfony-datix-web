<?php

namespace src\reports\model\listingreport\orderby;

use src\framework\model\ModelFactory;

class OrderByModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new OrderByFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new OrderByMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new OrderByCollection($this->getMapper(), $this->getEntityFactory());
    }
}