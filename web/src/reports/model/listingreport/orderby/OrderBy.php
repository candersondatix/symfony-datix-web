<?php

namespace src\reports\model\listingreport\orderby;

use src\reports\model\listingreport\Option;

class OrderBy extends Option
{    
    /**
     * @var string
     */
    protected $direction;
    
    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        if (null === $this->alias)
        {
            $this->alias = 'orderby'.$this->order;
        }
        return $this->alias;
    }
    
    /**
     * Getter for direction.
     * 
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }
    
    /**
     * Setter for direction.
     * 
     * @param string $direction
     * 
     * @throws InvalidArgumentException If the direction isn't 'ASC', 'DESC', or an empty string.
     */
    public function setDirection($direction)
    {
        if (!in_array($direction, ['ASC','DESC','']))
        {
            throw new InvalidArgumentException('OrderBy direction property must be one of "ASC", "DESC", or and empty string.');
        }
        $this->direction = '' == $direction ? 'ASC' : $direction;
    }
}