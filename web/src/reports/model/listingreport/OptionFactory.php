<?php

namespace src\reports\model\listingreport;

use src\framework\model\EntityFactory;
use src\framework\model\Entity;
use src\framework\model\EntityCache;
use src\framework\registry\Registry;
use src\reports\exceptions\OptionFactoryException;
use src\system\database\FieldInterface;
use src\system\database\fielddef\FieldDefCollection;
use src\reports\model\listingreport\column\formatters\ColumnFormatterFactory;

/**
 * Provides base functionality for constructing the various objects which make up a listing report design,
 * i.e. colums, group by fields, and order by fields
 */
abstract class OptionFactory extends EntityFactory
{    
    /**
     * {@inheritdoc}
     */
    public function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null, Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();
        
        $data = $this->handleExtraFields($data, $registry);
        $data = $this->handleLocationTiers($data, $registry);
        $data = $this->handleType($data);
        $data = $this->handleCodeFields($data, $registry);
        $data = $this->handleComputedDateFields($data);
        $data = $this->handlePaymentSummaryFields($data);
        $data = $this->handleStandardsScore($data);
        
        $data['formatterFactory'] = new ColumnFormatterFactory();

        return parent::doCreateObject($data, $baseEntity, $cache, $registry);
    }
    
    /**
     * Handles data transformation for extra field options.
     * 
     * @param array    $data
     * @param Registry $registry
     * 
     * @return array $data
     */
    protected function handleExtraFields(array $data, Registry $registry)
    {
        if ('UDF' == $data['form'])
        {
            $moduleDefs    = $registry->getModuleDefs();
            $data['form']  = $moduleDefs[$data['module']]['VIEW'] ?: $moduleDefs[$data['module']]['TABLE'];
            $data['field'] = $data['form'].'.'.str_replace('U0', 'UDF', $data['field']);
            $data          = $this->getFieldDefObj($data, $registry);
        }
        return $data;
    }

    /**
     * Handles data transformation for location tier options.
     * 
     * @param array    $data
     * @param Registry $registry
     * 
     * @return array $data
     */
    protected function handleLocationTiers(array $data, Registry $registry)
    {
        if (in_array($data['module'], ['CQO','CQP','CQS']) && preg_match('/tier_(\d+)/u', $data['field'], $matches))
        {
            require_once 'Source/classes/Listings/CQCListing.php';
            
            $moduleDefs = $registry->getModuleDefs();
            
            $data['title'] = (new \CQCListing($data['module']))->GetColumnLabel($data['field'], '');
            $data['field'] = "dbo.fn_GetLocationTierValue('".$data['module']."', ".$moduleDefs[$data['module']]['VIEW'].".recordid, '".$matches[1]."')";
            $data['type']  = 'S';
        }
        return $data;
    }
    
    /**
     * Handles transformation of data type.
     * 
     * @param array $data
     * 
     * @return array $data
     */
    protected function handleType(array $data)
    {
        switch ($data['type'])
        {
            case 'C':
                $data['type'] = FieldInterface::CODE;
                break;
                
            case 'T':
                $data['type'] = FieldInterface::MULTICODE;
                break;
                
            case 'D':
                $data['type'] = FieldInterface::DATE;
                break;

            case 'S':
            case 'L':
                $data['type'] = FieldInterface::STRING;
                break;
                    
            case 'M':
                $data['type'] = FieldInterface::MONEY;
                break;
                    
            case 'N':
                $data['type'] = FieldInterface::NUMBER;
                break;
                    
            case 'Y':
                $data['type'] = FieldInterface::YESNO;
                break;
                
            default:
                throw new OptionFactoryException('Unable to determine the field type for listing report option '.$data['field']);
        }
        return $data;
    }
    
    /**
     * Fetches the relevant FieldDef object used to retieve codes/descriptions for this option.
     * 
     * @param array    $data
     * @param Registry $registry
     * 
     * @return array $data
     */
    protected function handleCodefields(array $data, Registry $registry)
    {
        if (!in_array($data['type'], [FieldInterface::CODE, FieldInterface::MULTICODE]))
        {
            return $data;
        }

        return $this->getFieldDefObj($data, $registry);
    }
    
    /**
     * Fetches the relevant FieldDef object for this option.
     * 
     * @param array    $data
     * @param Registry $registry
     * 
     * @return array $data
     */
    protected function getFieldDefObj(array $data, Registry $registry)
    {
        $fieldDefs = $registry->getFieldDefs();
        
        if (false !== strpos($data['field'], '.'))
        {
            $data['fieldDef'] = $fieldDefs[$data['field']];
        }
        else
        {
            $tables = '' == $data['tables'] ? [$data['form']] : explode(',', $data['tables']);
             
            foreach ($tables as $table)
            {
                $data['fieldDef'] = $fieldDefs[$table.'.'.$data['field']];
             
                if ($data['fieldDef'] instanceof FieldInterface)
                {
                    break;
                }
            }
        }
         
        if (!($data['fieldDef'] instanceof FieldInterface))
        {
            throw new OptionFactoryException('Unable to determine the field def object for listing report option '.$data['form'].'.'.$data['field']);
        }
        
        return $data;
    }
    
    /**
     * Handles data transformation for computed date fields (i.e. date1 - date2).
     * 
     * @param array $data
     * 
     * @return array $data
     */
    protected function handleComputedDateFields(array $data)
    {
        if ('' == $data['dateOption'] && strpos($data['field'], '-'))
        {
            $fields = explode('-', $data['field']);
            $data['field'] = "convert(varchar, ".$fields[0].", 21)+'|'+convert(varchar, ".$fields[1].", 21)";
            $data['type']  = FieldInterface::COMPUTEDDATE;
        }
        return $data;
    }
    
    /**
     * Handles data transformation for claims payment summary fields.
     * 
     * @param array $data
     * 
     * @return array $data
     */
    protected function handlePaymentSummaryFields(array $data)
    {
        if ('CLA' == $data['module'] && 'pay_type_' == substr($data['field'], 0, 9))
        {
            $fieldParts = explode('_', $data['field']);

            $data['field'] = 'dbo.fn_GetPaymentTypeTotal(claims_main.recordid, \''.strtoupper($fieldParts[2]).'\')';
        }
        return $data;
    }
    
    /**
     * Handles data transformation for standards score field.
     * 
     * @param array $data
     * 
     * @return array $data
     */
    protected function handleStandardsScore(array $data)
    {
        if ('stn_score' == $data['field'])
        {
            $data['field'] = 'CONVERT(int, ROUND(stn_score,0))';
        }
        return $data;
    }
}