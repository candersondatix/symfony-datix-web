<?php

namespace src\reports\model\listingreport\groupby;

use src\reports\model\listingreport\OptionMapper;
use src\framework\query\Query;
use src\reports\model\listingreport\Listing;

class GroupByMapper extends OptionMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\groupby\\GroupBy';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->where(['rpf_section' => 'GROUP']);
              
        return parent::select($query);
    }
    
    /**
     * Retrieves the GroupBys for a given listing report design.
     * 
     * @param Listing $report
     */
    public function getGroupBysForListingReport(Listing $report)
    {
        $query = $this->factory->getQueryFactory()->getQuery();
        $query->where(['rpf_rep_id' => $report->base_listing_report])
              ->orderBy(['rpf_order']);
        
        $GroupBys = $this->factory->getCollection();
        $GroupBys->setQuery($query);
        
        return $GroupBys;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getFieldMappings()
    {
        return [
            'title'           => 'rpf_title',
            'dateOption'      => 'rpf_date_format',
            'order'           => 'rpf_order'
        ];
    }
}