<?php

namespace src\reports\model\listingreport\groupby;

use src\reports\model\listingreport\OptionFactory;
use src\reports\DatesEngine;
use src\framework\query\Field;

class GroupByFactory extends OptionFactory
{
    /**
     * @var DatesEngine
     */
    protected $datesEngine;
    
    public function __construct(DatesEngine $datesEngine)
    {
        $this->datesEngine = $datesEngine;
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\listingreport\\groupby\\GroupBy';
    }
    
    /**
     * {@inheritdoc}
     */
    public function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null, Registry $registry = null)
    {
        if ('' != $data['dateOption'])
        {
            // wrap the date field with the correct date conversion function
            $field = new Field($this->datesEngine->getDateSelectField($data['field'], $data['dateOption']));
            $data['field'] = $field->getName();
        }
        
        return parent::doCreateObject($data, $baseEntity, $cache, $registry);
    }
}