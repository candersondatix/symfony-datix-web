<?php

namespace src\reports\model\listingreport\groupby;

use src\framework\model\ModelFactory;
use src\reports\model\packagedreport\PackagedReportModelFactory;

class GroupByModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        $datesEngine = (new PackagedReportModelFactory)->getDatesEngine();
        
        return new GroupByFactory($datesEngine);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new GroupByMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new GroupByCollection($this->getMapper(), $this->getEntityFactory());
    }
}