<?php

namespace src\reports\model\packagedreport;

use src\framework\model\ModelFactory;
use src\framework\query\Query;
use src\framework\query\QueryFactory;
use src\framework\query\SqlWriter;
use src\framework\registry\Registry;
use src\framework\controller\Loader;
use src\reports\AggregateReservesReportEngine;
use src\reports\GaugeReportEngine;
use src\reports\GraphicalReportEngine;
use src\reports\InitialLevelsReportEngine;
use src\reports\SqlServerDatesEngine;
use src\reports\ListingReportEngine;
use src\reports\querybuilder\ReportQueryBuilder;
use src\reports\querybuilder\builders\WeekDateReportQueryBuilder;
use src\reports\querybuilder\builders\RowsAndColumnsReportQueryBuilder;
use src\reports\querybuilder\builders\CLA\AggregateRemainingReserveReportQueryBuilder;
use src\reports\querybuilder\builders\CLA\AggregateReserveReportQueryBuilder;
use src\reports\querybuilder\builders\FromTableReportQueryBuilder;
use src\reports\querybuilder\builders\CLA\PayTypeReportQueryBuilder;
use src\reports\querybuilder\builders\CountFieldReportQueryBuilder;
use src\reports\querybuilder\builders\INC\InjuriesReportQueryBuilder;
use src\reports\querybuilder\builders\ReplaceAtPromptsReportQueryBuilder;
use src\reports\querybuilder\builders\OverrideSecurityReportQueryBuilder;
use src\reports\querybuilder\builders\ListingPagingReportQueryBuilder;
use src\reports\querybuilder\builders\ListingSubformJoinsReportQueryBuilder;
use src\reports\querybuilder\builders\ListingColumnReportQueryBuilder;
use src\reports\querybuilder\builders\ListingMergeRepeatedPagingReportQueryBuilder;
use src\reports\querybuilder\builders\ListingOrderByReportQueryBuilder;
use src\reports\querybuilder\builders\ListingGroupByReportQueryBuilder;
use src\reports\specifications\ReportSpecificationFactory;
use src\reports\model\report\GaugeChart;
use src\reports\model\listingreport\Listing;
use src\reports\model\report\Report;
use src\reports\model\report\Crosstab;
use src\reports\model\report\ReportModelFactory;
use src\reports\controllers\GraphicalReportWriter;
use src\reports\controllers\CrosstabReportWriter;
use src\savedqueries\model\SavedQueryModelFactory;
use src\users\model\User;
use src\users\exceptions\CurrentUserNotFoundException;
use src\system\database\table\LinkTableModelFactory;

class PackagedReportModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new PackagedReportFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new PackagedReportMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new PackagedReportCollection($this->getMapper(), $this->getEntityFactory());
    }
    
    /**
     * Constructs and returns a ReportQueryBuilder object.
     * 
     * TODO Factor this out to a separate Factory class
     * 
     * @return ReportQueryBuilder
     */
    public function getReportQueryBuilder(PackagedReport $packagedReport, ReportSpecificationFactory $rsf, Registry $registry)
    {
        if ($packagedReport->report instanceof Listing)
        {
            return $this->getListingReportQueryBuilder($packagedReport, $registry);
        }
        
        $moduleDefs = $registry->getModuleDefs();
        $fieldDefs  = $registry->getFieldDefs();
        
        $reportQueryBuilder = new ReportQueryBuilder(new \SplObjectStorage());
        
        $reportQueryBuilder->attach(new FromTableReportQueryBuilder($moduleDefs));
        $reportQueryBuilder->attach(new WeekDateReportQueryBuilder($registry));
        $reportQueryBuilder->attach(new InjuriesReportQueryBuilder($fieldDefs));
        
        // load "num" column query build step
        if ($rsf->getPayTypeSpecification()->isSatisfiedBy($packagedReport))
        {
            $reportQueryBuilder->attach(new PayTypeReportQueryBuilder($rsf, new QueryFactory()));
        }
        elseif ($rsf->getAggregateReserveOnSelectedDateSpecification()->isSatisfiedBy($packagedReport))
        {
            $reportQueryBuilder->attach(new AggregateReserveReportQueryBuilder($rsf, new QueryFactory(), $moduleDefs));
        }
        elseif ($rsf->getAggregateRemainingReserveSpecification()->isSatisfiedBy($packagedReport))
        {
            $reportQueryBuilder->attach(new AggregateRemainingReserveReportQueryBuilder($rsf));
        }
        else
        {
            $reportQueryBuilder->attach(new CountFieldReportQueryBuilder());
        }
        
        $reportQueryBuilder->attach(new OverrideSecurityReportQueryBuilder());
        $reportQueryBuilder->attach(new ReplaceAtPromptsReportQueryBuilder());
        
        if ($rsf->getGaugeReportSpecification()->not()->isSatisfiedBy($packagedReport))
        {
            $reportQueryBuilder->attach(new RowsAndColumnsReportQueryBuilder($this->getDatesEngine(), $fieldDefs, $this->getQueryFactory(), new LinkTableModelFactory(), $rsf, $moduleDefs));
        }

        return $reportQueryBuilder;
    }
    
    /**
     * Constructs and returns a ReportQueryBuilder object for listing reports.
     * 
     * @param PackagedReport             $packagedReport
     * @param ReportSpecificationFactory $rsf
     * @param Registry                   $registry
     * 
     * @return ReportQueryBuilder
     */
    public function getListingReportQueryBuilder(PackagedReport $packagedReport, Registry $registry)
    {
        $reportQueryBuilder = new ReportQueryBuilder(new \SplObjectStorage());
        
        $moduleDefs = $registry->getModuleDefs();
        
        $reportQueryBuilder->attach(new ListingColumnReportQueryBuilder($registry)); // deal with computed fields (e.g. date1 - date2)
        $reportQueryBuilder->attach(new FromTableReportQueryBuilder($moduleDefs));
        
        $reportQueryBuilder->attach(new ListingGroupByReportQueryBuilder($registry));
        
        $reportQueryBuilder->attach(new ListingOrderByReportQueryBuilder($registry));
        
        $reportQueryBuilder->attach(new ListingSubformJoinsReportQueryBuilder($registry));
        
        if (!$packagedReport->report->returnAllRecords)
        {
            $reportQueryBuilder->attach(new ListingPagingReportQueryBuilder($registry, new SqlWriter(), new \DatixDBQuery()));
        }
        
        $reportQueryBuilder->attach(new OverrideSecurityReportQueryBuilder());
        $reportQueryBuilder->attach(new ReplaceAtPromptsReportQueryBuilder());
        
        if (!$packagedReport->report->returnAllRecords && $packagedReport->report->merge_repeated_values)
        {
            $reportQueryBuilder->attach(new ListingMergeRepeatedPagingReportQueryBuilder($registry, new QueryFactory(), new \DatixDBQuery()));
        }
        
        return $reportQueryBuilder;
    }
    
    /**
     * Constructs and returns a ReportEngine object.
     * 
     * TODO Factor this out to a separate Factory class
     * 
     * @return ReportEngine
     */
    public function getReportEngine(PackagedReport $packagedReport)
    {
        $registry           = Registry::getInstance();
        $rsf                = new ReportSpecificationFactory();
        $reportQueryBuilder = $this->getReportQueryBuilder($packagedReport, $rsf, $registry);
        
        if ($packagedReport->report instanceof Listing)
        {
            return new ListingReportEngine(new \DatixDBQuery(), new QueryFactory(), $registry, $reportQueryBuilder);
        }
        
        $gaugeSpec = $rsf->getGaugeReportSpecification();
        $initialLevelsSpec = $rsf->getInitialLevelsSpecification();
        $aggregateRemainingReservesSpec = $rsf->getAggregateRemainingReserveSpecification();
        
        if ($gaugeSpec->isSatisfiedBy($packagedReport))
        {
            return new GaugeReportEngine(new \DatixDBQuery(), new QueryFactory(), $registry, $reportQueryBuilder, $this->getDatesEngine());
        }
        elseif ($initialLevelsSpec->isSatisfiedBy($packagedReport))
        {
            return new InitialLevelsReportEngine(new \DatixDBQuery(), new QueryFactory(), $registry, $reportQueryBuilder, $this->getDatesEngine(), $rsf);
        }
        elseif ($aggregateRemainingReservesSpec->isSatisfiedBy($packagedReport))
        {
            return new AggregateReservesReportEngine(new \DatixDBQuery(), new QueryFactory(), $registry, $reportQueryBuilder, $this->getDatesEngine());
        }
        else
        {
            return new GraphicalReportEngine(new \DatixDBQuery(), new QueryFactory(), $registry, $reportQueryBuilder, $this->getDatesEngine());
        }
    }
    
    /**
     * Constructs and returns a DatesEngine object.
     * 
     * TODO Factor this out to a separate Factory class
     * 
     * @return DatesEngine
     */
    public function getDatesEngine()
    {
        return new SqlServerDatesEngine(new \DatixDBQuery(), new QueryFactory(), Registry::getInstance());
    }
    
    /**
     * Constructs and returns a ReportWriter object.
     * 
     * @param Report $report The report object.
     * 
     * @return ReportWriter
     */
    public function getReportWriter(Report $report)
    {
        $loader = new Loader();
        if ($report instanceof Crosstab)
        {
            return $loader->getController(array('controller' => 'src\\reports\\controllers\\CrosstabReportWriter'));
        }
        elseif ($report instanceof Listing)
        {
            return $loader->getController(array('controller' => 'src\\reports\\controllers\\ListingReportWriter'));
        }
        return $loader->getController(array('controller' => 'src\\reports\\controllers\\GraphicalReportWriter'));
    }
    
    /**
     * Constructs and returns a ReportModelFactory object.
     * 
     * @return ReportModelFactory
     */
    public function getReportModelFactory()
    {
        return new ReportModelFactory();
    }
    
    /**
     * Constructs and returns a SavedQueryModelFactory object.
     * 
     * @return SavedQueryModelFactory
     */
    public function getSavedQueryModelFactory()
    {
        return new SavedQueryModelFactory();
    }
    
    /**
     * Fetches a collection of packaged reports objects to which the current user has access.
     * 
     * @param Query $query
     * 
     * @throws CurrentUserNotFoundException
     * 
     * @return PackagedReportCollection
     */
    public function getMyReportsCollection(Query $query = null)
    {
        $user = $_SESSION['CurrentUser'];
        if (!($user instanceof User))
        {
            throw new CurrentUserNotFoundException();
        }
        
        $qf = $this->getQueryFactory();
        
        // We want to get all reports for where this user has permissions to view those reports.
        $criteria = [
            'OR',
            $qf->getFieldCollection()
               ->field('web_packaged_reports.recordid')->in(
                   $qf->getQuery()->select( ['web_packaged_report_users.web_packaged_report_id'] )
                                  ->where( ['web_packaged_report_users.user_id' => $user->recordid] ))
        ];

        // And if the user is in any security groups, any reports where these groups have permission to view those reports.
        require_once 'Source/security/SecurityBase.php';
        $currentUserSecurityGroups = GetUserSecurityGroups($user->recordid); // TODO: move this function to the user entity.
        
        if (!empty($currentUserSecurityGroups))
        {
            $groupWhere = $qf->getWhere();
            $groupWhere->add($qf->getFieldCollection()->field('group_id')->in($currentUserSecurityGroups));
            
            $criteria[] = $qf->getFieldCollection()
                             ->field('web_packaged_reports.recordid')->in(
                                 $qf->getQuery()->select( ['web_packaged_report_groups.web_packaged_report_id'] )
                                                ->where($groupWhere)
            );
        }

        // And if the user is in a profile, any reports where that profile has permission to view those reports
        if ($user->sta_profile != '')
        {
            $criteria[] = $qf->getFieldCollection()
                             ->field('web_packaged_reports.recordid')->in(
                                 $qf->getQuery()->select( ['web_packaged_report_profiles.web_packaged_report_id'] )
                                                ->where( ['web_packaged_report_profiles.profile_id' => $user->sta_profile] )
            );
        }

        // And any reports without any security set on them.
        $criteria[] = $qf->getFieldCollection()
                         ->field('web_packaged_reports.recordid')->notIn($qf->getQuery()->select( ['web_packaged_report_users.web_packaged_report_id'] ))
                         ->field('web_packaged_reports.recordid')->notIn($qf->getQuery()->select( ['web_packaged_report_groups.web_packaged_report_id'] ))
                         ->field('web_packaged_reports.recordid')->notIn($qf->getQuery()->select( ['web_packaged_report_profiles.web_packaged_report_id'] ));

        $where = $qf->getWhere();
        $where->addArray($criteria);
        
        $query = $query ?: $qf->getQuery();
        $query->join('web_reports', 'web_packaged_reports.web_report_id = web_reports.recordid')
              ->where($where);

        $collection = $this->getCollection();
        $collection->setQuery($query);
        
        return $collection;
    }
    
    /**
     * This is a temporary alternative to PackagedReportModelFactory::getMyReportsCollection() which allows client code to
     * access and execute the SQL query for My Reports manually, rather than using an EntityCollection, for performance reasons.
     * 
     * Used for now pending improvements to the model which will allow for nested object data to be returned when hydrating 
     * Entities via Mappers.  This method should be removed when this work is complete.
     */
    public function getMyReportsSQL()
    {
        $user = $_SESSION['CurrentUser'];
        if (!($user instanceof User))
        {
            throw new CurrentUserNotFoundException();
        }
        
        $params = [];
    
        $select = '
            SELECT
                web_packaged_reports.recordid, web_packaged_reports.createdby, web_packaged_reports.createddate, 
                web_packaged_reports.name, web_packaged_reports.query_id, web_reports.type, contacts_main.fullname
            FROM
                web_packaged_reports
            INNER JOIN
                web_reports ON web_packaged_reports.web_report_id = web_reports.recordid
            LEFT JOIN
                contacts_main ON web_packaged_reports.createdby = contacts_main.initials
        ';
        
        $where = '
            WHERE
                (web_packaged_reports.recordid IN 
                    (
                        SELECT web_packaged_report_users.web_packaged_report_id FROM web_packaged_report_users WHERE web_packaged_report_users.user_id = :user_id
                    )
        ';
        
        $params['user_id'] = $user->recordid;
        
        require_once 'Source/security/SecurityBase.php';
        $currentUserSecurityGroups = GetUserSecurityGroups($user->recordid);
        
        if (!empty($currentUserSecurityGroups))
        {
            $where .= '
                OR
                    web_packaged_reports.recordid IN
                    (
                        SELECT web_packaged_report_groups.web_packaged_report_id FROM web_packaged_report_groups WHERE web_packaged_report_groups.group_id IN ('.implode(',', $currentUserSecurityGroups).')
                    )';
        }
        
        if ($user->sta_profile != '')
        {
            $where .= '
                OR
                    web_packaged_reports.recordid IN
                    (
                        SELECT web_packaged_report_profiles.web_packaged_report_id FROM web_packaged_report_profiles WHERE web_packaged_report_profiles.profile_id = :profile_id
                    )';
            
            $params['profile_id'] = $user->sta_profile;
        }
    
        $where .= '
                OR
                    (
                        web_packaged_reports.recordid NOT IN (SELECT web_packaged_report_users.web_packaged_report_id FROM web_packaged_report_users)
                        AND web_packaged_reports.recordid NOT IN (SELECT web_packaged_report_groups.web_packaged_report_id FROM web_packaged_report_groups)
                        AND web_packaged_reports.recordid NOT IN (SELECT web_packaged_report_profiles.web_packaged_report_id FROM web_packaged_report_profiles)
                    )
                )';
        
        $orderBy = '
            ORDER BY
                web_packaged_reports.name
        ';
        
        return [$select, $where, $orderBy, $params];
    }
}