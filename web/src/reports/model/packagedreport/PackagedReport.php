<?php
namespace src\reports\model\packagedreport;

use src\framework\model\DatixEntity;
use src\reports\ReportEngine;
use src\reports\model\report\Report;
use src\reports\exceptions\ReportException;
use src\users\model\UserModelFactory;
use src\security\groups\model\GroupModelFactory;
use src\profiles\model\profile\ProfileModelFactory;
use src\savedqueries\model\SavedQuery;
use src\savedqueries\model\SavedQueryModelFactory;

/**
 * A report design attached to a specific where clause.
 */
class PackagedReport extends DatixEntity
{
    /**
     * The initials of the user that created the report.
     * 
     * @var string
     */
    protected $createdby;
    
    /**
     * The date the report was created.
     * 
     * @var string
     */
    protected $createddate;
    
    /**
     * The name of the packaged report.
     * 
     * @var string
     */
    protected $name;
    
    /**
     * The object containing the report design settings.
     * 
     * @var Report
     */
    protected $report;
    
    /**
     * The query used to generate this report.
     * 
     * @var SavedQuery
     */
    protected $query;

    /**
     * Whether or not to pay attention to security where clauses when retrieving the report data.
     * 
     * @var boolean
     */
    protected $override_security;
    
    /**
     * The users that can access the report.
     * 
     * @var UserCollection
     */
    protected $users;
    
    /**
     * The groups that can access the report.
     * 
     * @var GroupCollection
     */
    protected $groups;
    
    /**
     * The profilesthat can access the report.
     * 
     * @var ProfileCollection
     */
    protected $profiles;

    /**
     * We need to clone the report and the where properties otherwise we get references to previous packaged report
     * properties when drilling down in crosstab reports.
     */
    public function __clone()
    {
        $report = $this->getReport();
        $query = $this->getQuery();

        // Listing reports allow editable base reports, so we do not want to create duplicates
        if ($report != null)
        {
            // For listing reports we don't want to clone the base report, since this cannot be altered in the
            // report designer. Cloning the base report means multiple web_report records point to the same
            // listing report rep_id, causing duplication in reports administration.
            if ($report->getReportType() != Report::LISTING)
            {
                $this->report = clone $this->report;
            }
        }

        if ($query != null)
        {
            $this->query = clone $this->query;
        }

        parent::__clone();
    }
    
    /**
     * Getter for report.
     * 
     * @param PackagedReportModelFactory $factory
     * 
     * @return Report
     */
    public function getReport(PackagedReportModelFactory $factory = null)
    {
        if ($this->report === null)
        {
            // we attempt to fetch this data on access for the purposes of lazy-loading
            $factory = $factory ?: new PackagedReportModelFactory();
            $this->report = $factory->getMapper()->getPackagedReportReport($this);
        }
        return $this->report;
    }
    
    /**
     * Setter for report.
     * 
     * @param Report $value
     */
    public function setReport(Report $value)
    {
        $this->report = $value;
    }
    
    /**
     * Getter for query.
     * 
     * @param PackagedReportModelFactory $factory
     * 
     * @return SavedQuery
     */
    public function getQuery(PackagedReportModelFactory $factory = null)
    {
        if ($this->query === null)
        {
            // we attempt to fetch this data on access for the purposes of lazy-loading
            $factory = $factory ?: new PackagedReportModelFactory();
            $this->query = $factory->getMapper()->getPackagedReportQuery($this);
        }
        return $this->query;
    }
    
    /**
     * Setter for query.
     * 
     * @param SavedQuery $savedQuery
     */
    public function setQuery(SavedQuery $savedQuery)
    {
        $this->query = $savedQuery;
    }
    
    /**
     * Setter for override_security.
     * 
     * @param boolean $value
     */
    public function setOverride_security($value)
    {
        $this->override_security = (bool) $value;
    }
    
    /**
     * Getter for users.
     * 
     * @return UserCollection
     */
    public function getUsers()
    {
        if ($this->users === null)
        {
            if (!((int) $this->recordid > 0))
            {
                throw new ReportException('Unable to retrieve UserCollection: PackagedReport.recordid is not set');
            }
            
            $factory = new UserModelFactory();
            list($query, $join, $fields) = $factory->getQueryFactory()->getQueryObjects();
            
            $fields->field('staff.recordid')->eq('web_packaged_report_users.user_id', true);
            $join->add($fields);
            
            $query->join('web_packaged_report_users', $join)
                  ->where(['web_packaged_report_users.web_packaged_report_id' => $this->recordid]);
            
            $this->users = $factory->getCollection();
            $this->users->setQuery($query);
        }
        return $this->users;
    }
    
    /**
     * Getter for groups.
     * 
     * @return GroupCollection.
     */
    public function getGroups()
    {
        if ($this->groups === null)
        {
            if (!((int) $this->recordid > 0))
            {
                throw new ReportException('Unable to retrieve GroupCollection: PackagedReport.recordid is not set');
            }
            
            $factory = new GroupModelFactory();
            list($query, $join, $fields) = $factory->getQueryFactory()->getQueryObjects();
            
            $fields->field('sec_groups.recordid')->eq('web_packaged_report_groups.group_id', true);
            $join->add($fields);
            
            $query->join('web_packaged_report_groups', $join)
                  ->where(['web_packaged_report_groups.web_packaged_report_id' => $this->recordid]);
            
            $this->groups = $factory->getCollection();
            $this->groups->setQuery($query);
        }
        return $this->groups;
    }
    
    /**
     * Getter for Profiles.
     * 
     * @return ProfileCollection
     */
    public function getProfiles()
    {
        if ($this->profiles === null)
        {
            if (!((int) $this->recordid > 0))
            {
                throw new ReportException('Unable to retrieve ProfileCollection: PackagedReport.recordid is not set');
            }
            
            $factory = new ProfileModelFactory();
            list($query, $join, $fields) = $factory->getQueryFactory()->getQueryObjects();
            
            $fields->field('profiles.recordid')->eq('web_packaged_report_profiles.profile_id', true);
            $join->add($fields);
            
            $query->join('web_packaged_report_profiles', $join)
                  ->where(['web_packaged_report_profiles.web_packaged_report_id' => $this->recordid]);
            
            $this->profiles = $factory->getCollection();
            $this->profiles->setQuery($query);
        }
        return $this->profiles;
    }
    
    /**
     * Builds an excel document object representing the data for this report.
     *
     * @param ReportEngine $engine
     * 
     * @return \PHPExcel
     */
    public function getExcelObject(ReportEngine $engine)
    {
        return $this->report->getExcelObject($engine->getData($this));
    }
    
    /**
     * Get properties to XML export
     *
     * @author gszucs
     * @return array
     */
    public function getExportData()
    {
    	return array_diff_key( $this->getVars(), array_flip(['report', 'query', 'users', 'groups', 'profiles', 'updateid', 'recordid', 'observers', 'queueEvents', 'addToCache']) ) + [ 'query_id' => $this->getQuery()->recordid ];
    }
    
    /**
     * Getter for name.
     * 
     * Return the base report title if the packaged report name hasn't been explicitly set.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name == '' ? $this->report->title : $this->name;
    }

    /**
     * Used to determine whether a given report is similar enough to this one to allow "override security" to be maintained.
     * Needed to ensure "override security" remains in place when reloading a report after changing @prompt values (see DW-11227)
     *
     * @param PackagedReport $comparisonReport
     */
    public function canPassOverrideSecurityTo(PackagedReport $comparisonReport)
    {
        $savedReportDesign = $this->report->getVars();
        $runReportDesign = $comparisonReport->report->getVars();

        unset($savedReportDesign['createdby']);
        unset($savedReportDesign['createddate']);
        unset($savedReportDesign['updateid']);
        unset($savedReportDesign['owner']);
        unset($savedReportDesign['packagedReports']);
        unset($savedReportDesign['updatedby']);
        unset($savedReportDesign['updateddate']);
        unset($savedReportDesign['recordid']);
        unset($savedReportDesign['title']);

        unset($runReportDesign['createdby']);
        unset($runReportDesign['createddate']);
        unset($runReportDesign['updateid']);
        unset($runReportDesign['owner']);
        unset($runReportDesign['packagedReports']);
        unset($runReportDesign['updatedby']);
        unset($runReportDesign['updateddate']);
        unset($runReportDesign['recordid']);
        unset($runReportDesign['title']);

        return count(array_diff($savedReportDesign,$runReportDesign)) == 0;
    }
}