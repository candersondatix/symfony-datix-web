<?php
namespace src\reports\model\field;

use src\system\database\field\CodeField;
use src\framework\query\Query;

/**
 * Displays a list of fields used when designing a report.
 */
class ReportField extends CodeField
{
    /**
     * The code for the module we're reporting on.
     * 
     * Used to determine which report fields are excluded.
     * 
     * @var string
     */
    protected $reportModule;
    
    /**
     * Whether or not this report field displays only those fields
     * that have been excluded from reports.
     * 
     * @var boolean
     */
    protected $showOnlyExcludedFields;

    /**
     * Whether or not this report field displays fields
     * that have been excluded from reports.
     *
     * @var boolean
     */
    protected $showExcludedFields;

    /**
     * The fieldset that this field belongs to when reporting.
     * @var string
     */
    protected $fieldset;
    
    public function __construct($fieldset, $reportModule, $showOnlyExcludedFields = false, $showExcludedFields = false)
    {
        parent::__construct();
        $this->fieldset = $fieldset;
        $this->reportModule = $reportModule;
        $this->showOnlyExcludedFields = (bool) $showOnlyExcludedFields;
        $this->showExcludedFields = (bool) $showExcludedFields;
    }
    
    /**
     * {@inherit}
     */
    protected function doGetCodes(Query $query = null)
    {
        $this->codes = (new ReportFieldCodeModelFactory($this))->getCollection();
        $query = $query ?: new Query();
        $this->codes->setQuery($query);
    }
    
    /**
     * Getter for reportModule.
     * 
     * @return string
     */
    public function getReportModule()
    {
        return $this->reportModule;
    }
    
    /**
     * Accessor method for showOnlyExcludedFields property.
     * 
     * @return boolean
     */
    public function showOnlyExcludedFields()
    {
        return $this->showOnlyExcludedFields;
    }

    /**
     * Accessor method for showExcludedFields property.
     *
     * @return boolean
     */
    public function showExcludedFields()
    {
        return $this->showExcludedFields;
    }
}