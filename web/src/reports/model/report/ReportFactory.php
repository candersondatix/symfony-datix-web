<?php
namespace src\reports\model\report;

use src\framework\model\Entity;
use src\framework\model\EntityFactory;
use src\framework\registry\Registry;
use src\reports\exceptions\ReportException;
use src\reports\model\field\ReportField;
use src\reports\model\report\Report;
use src\reports\observers\ReportDeletionCleaner;
use src\reports\model\listingreport\column\ColumnCollection;

class ReportFactory extends EntityFactory
{
    /**
     * Target class is set dynamically, since this factory creates multiple object types.
     * 
     * @var string
     */
    protected $targetClass;
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return $this->targetClass ?: 'src\\reports\\model\\report\\Report';
    }

    /**
     * Used to map fields from Report Designer Form to match what's expected in the rest of the reportFactory. Use when processing data send from the RD form
     *
     * @param $requestn - request object to use
     * @return Entity - report object
     */
    public function createFromFormRequest($request, Entity $baseEntity = null)
    {
        if($request->getParameter('type') == Report::TRAFFIC)
        {
            $request->setParameter('rows_field', $request->getParameter('traffic_field'));
        }

        if($request->getParameter('type') == Report::SPC)
        {
            $request->setParameter('type', $request->getParameter('spc_type'));
        }
        else if(in_array($request->getParameter('type'), array(Report::SPC_C_CHART, Report::SPC_I_CHART, Report::SPC_MOVING_RANGE, Report::SPC_RUN_CHART)))
        {
            $request->setParameter('spc_type', $request->getParameter('type'));
            $request->setParameter('type', 99);
        }

        if($request->getParameter('type') == Report::BAR)
        {
            $request->setParameter('style', $request->getParameter('bar_style') ?: '1');
        }

        if($request->getParameter('type') == Report::PIE)
        {
            $request->setParameter('style', $request->getParameter('pie_style') ?: '1');
        }

        return $this->doCreateObject($request->getParameters(), $baseEntity);
    }
    
    /**
     * {@inheritdoc}
     */
    public function doCreateObject(array $data = array(), Entity $baseEntity = null)
    {
        $ModuleDefs = Registry::getInstance()->getModuleDefs();
        $this->setTargetClass($data['type']);

        if ($data['type'] == Report::TRAFFIC && $data['traffic_field'] != null)
        {
            $data['rows'] = $data['traffic_field'];
        }

        if ($data['rows_table'] != '' && $data['rows_field'] != '' && $data['type'] != Report::TRAFFIC)
        {
            $data['rows_fieldset'] = $data['rows_table'];
            $data['rows'] = $data['rows_field'];
        }
        elseif (!isset($data['rows']) && $data['rows_table'] == '' && $data['rows_field'] == '' && $data['type'] != Report::TRAFFIC)
        {
            // We need this condition to change the report when we are editing it from the dashboard
            $data['rows_fieldset'] = '';
            $data['rows'] = '';
        }
        
        if ($data['columns_table'] != '' && $data['columns_field'] != '')
        {
            $data['columns_fieldset'] = $data['columns_table'];
            $data['columns'] = $data['columns_field'];
        }
        elseif (!isset($data['columns']) && $data['columns_table'] == '' && $data['columns_field'] == '')
        {
            // We need this condition to change the report when we are editing it from the dashboard
            $data['columns_fieldset'] = '';
            $data['columns'] = '';
        }

        if(!isset($data['null_values']))
        {
            $data['null_values'] = false;
        }

        // This is needed because of SPC charts
        // We need to map the types of charts to the constants of the SPC model class
        // TODO: we should probably move this out of this function
        switch($data['type'])
        {
            case Report::SPC_C_CHART:
                $data['type'] = SpcChart::C_CHART;
                break;
            case Report::SPC_I_CHART:
                $data['type'] = SpcChart::I_CHART;
                break;
            case Report::SPC_MOVING_RANGE:
                $data['type'] = SpcChart::MOVING_RANGE;
                break;
            case Report::SPC_RUN_CHART:
                $data['type'] = SpcChart::RUN_CHART;
                break;
            case Report::TRAFFIC:
                $data = $this->mapTrafficCodeColours($data);
                break;
        }
        
        if (isset($data['base_listing_report']) && $data['type'] != Report::LISTING)
        {
            unset($data['base_listing_report']);
        }
        
        if (Report::LISTING == $data['type'] && !($data['columns'] instanceof ColumnCollection))
        {
            unset($data['columns']);
        }

        $targetClass = $this->targetClass();

        //if we're building one report type based on another, the generic doCreateObject will
        //not use the base class as it doesn't match the type of object we are creating.
        if ($baseEntity && !($baseEntity instanceof $targetClass))
        {
            $blankObject = parent::doCreateObject();

            foreach (Report::getProperties() as $property)
            {
                // TODO: Thought 1 - Maybe have another method that retrieves properties that we are actually interested in setting
                // TODO: Thought 2 - There might be some other properties that interfere with this
                if ($property != 'packagedReports')
                {
                    $blankObject->$property = $baseEntity->$property;
                }
            }

            $baseEntity =  $blankObject;
        }

        $object = parent::doCreateObject($data, $baseEntity);

        //attach observers
        $object->attach(new ReportDeletionCleaner());

        return $object;
    }

    /**
     * Sets the target class for this factory.
     *
     * @param string $type
     * @throws \src\reports\exceptions\ReportException
     */
    protected function setTargetClass($type)
    {
        switch ($type)
        {
            case Report::CROSSTAB:
                $this->targetClass = 'src\\reports\\model\\report\\Crosstab';
                break;
                
            case Report::LISTING:
                $this->targetClass = 'src\\reports\\model\\listingreport\\Listing';
                break;

            case Report::BAR:
                $this->targetClass = 'src\\reports\\model\\report\\BarChart';
                break;

            case Report::PARETO:
                $this->targetClass = 'src\\reports\\model\\report\\ParetoChart';
                break;
                
            case Report::LINE:
                $this->targetClass = 'src\\reports\\model\\report\\LineChart';
				break;
			
            case Report::SPC_C_CHART:
            case Report::SPC_I_CHART:
            case Report::SPC_MOVING_RANGE:
            case Report::SPC_RUN_CHART:
                $this->targetClass = 'src\\reports\\model\\report\\SpcChart';
                break;
                
            case Report::PIE:
                $this->targetClass = 'src\\reports\\model\\report\\PieChart';
                break;

            case Report::TRAFFIC:
                $this->targetClass = 'src\\reports\\model\\report\\TrafficLightChart';
                break;

            case Report::GAUGE:
                $this->targetClass = 'src\\reports\\model\\report\\GaugeChart';
                break;

            default:
                throw new ReportException('Unable to determine report type.');
        }
    }

    /**
     * Used by traffic light chart to store the code colours of the data to display.
     *
     * @param $data
     *
     * @return mixed
     */
    private function mapTrafficCodeColours($data)
    {
        $trafficCodeColours = array();

        foreach ($data as $key => $value)
        {
            if (\UnicodeString::strpos($key, 'trafficline_code_colour') !== false)
            {
                $aKey = explode('_', $key);
                $trafficCodeColours[$aKey[3]] = $value;
            }
        }

        if (!empty($trafficCodeColours))
        {
            $data['trafficCodeColours'] = $trafficCodeColours;
        }

        return $data;
    }
}
