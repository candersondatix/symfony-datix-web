<?php
namespace src\reports\model\report;

use src\framework\model\DatixEntity;
use src\framework\query\Query;
use src\framework\query\Field;
use src\framework\registry\Registry;
use src\reports\ReportEngine;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\exceptions\ReportException;
use src\system\database\FieldInterface;

/**
 * Represents a report design.
 */
abstract class Report extends DatixEntity
{
    /**
     * Report types.
     * 
     * @var integer
     */
    const CROSSTAB         = 1;
    const BAR              = 2;
    const PARETO           = 3;
    const LINE             = 4;
    const SPC              = 99; // used to give a not obvious value for showing spc type selector -TODO probably a better way to do this
    const SPC_C_CHART      = 5;
    const SPC_I_CHART      = 6;
    const SPC_MOVING_RANGE = 7;
    const SPC_RUN_CHART    = 8;
    const PIE              = 9;
    const TRAFFIC          = 10;
    const GAUGE            = 11;
    const LISTING          = 12;
    
    /**
     * count styles.
     * 
     * @var integer
     */
    const COUNT_NUM = 1;
    const COUNT_SUM = 2;
    const COUNT_AVE = 3;
    
    /**
     * Tooltip types.
     * 
     * @var integer
     */
    const TOOLTIP_VALUE_ONLY = 1,
     	  TOOLTIP_PERCENTAGE_ONLY = 2,
          TOOLTIP_PERCENTAGE_AND_VALUE = 3;
    
    /**
     * Date options.
     * 
     * @var integer
     */
    const DAY_OF_WEEK       = 1;
    const WEEK              = 2;
    const WEEK_DATE         = 3;
    const MONTH             = 4;
    const FINANCIAL_MONTH   = 5;
    const QUARTER           = 6;
    const FINANCIAL_QUARTER = 7;
    const YEAR              = 8;
    const FINANCIAL_YEAR    = 9;
    const MONTH_AND_YEAR    = 10;

    /**
     * Calendar options
     *
     * @var integer
     */
    const TODAY       = 1;
    const SELECT_DATE = 2;

    /**
     * The initials of the user that created the report.
     * 
     * @var string
     */
    protected $createdby;
    
    /**
     * The date the report was created.
     * 
     * @var string
     */
    protected $createddate;
    
    /**
     * The report module.
     * 
     * @var string
     */
    protected $module;
    
    /**
     * The fieldset to use when retrieving this data (defines which route to use to link the db tables).
     * 
     * @var string
     */
    protected $rows_fieldset;
    
    /**
     * The fully-qualified name of the rows field.
     *
     * @var string
     */
    protected $rows;

    /**
     * The fieldset to use when retrieving this data (defines which route to use to link the db tables).
     *
     * @var string
     */
    protected $columns_fieldset;

    /**
     * The fully-qualified name of the columns field.
     * 
     * @var string
     */
    protected $columns;
    
    /**
     * The report title.
     * 
     * @var string
     */
    protected $title;
    
    /**
     * Unit used if rowsField is a date.
     * 
     * @var string
     */
    protected $rows_date_option;
    
    /**
     * Unit used if columnsField is a date.
     * 
     * @var string
     */
    protected $columns_date_option;
    
    /**
     * Include only the top n records in the report.
     * 
     * @var int
     */
    protected $limit_to_top = '';
    
    /**
     * The nature of the totals presented in the report (e.g. records count, sum of a specific record field etc).
     * 
     * @var int
     */
    protected $count_style = self::COUNT_NUM;
    
    /**
     * The name of the field to use when summing totals.
     * 
     * @var string
     */
    protected $count_style_field_sum;
    
    /**
     * The name of the field to use when averaging totals.
     *
     * @var string
     */
    protected $count_style_field_average;

    /**
     * The date to use when summing or averaging totals.
     *
     * @var int
     */
    protected $count_style_field_value_at = self::TODAY;

    /**
     * The date to use when summing or averaging totals.
     *
     * @var string
     */
    protected $count_style_field_calendar = null;

    /**
     * Displays the initial reserve level alongside the reserve level.
     *
     * @var string
     */
    protected $count_style_field_initial_levels;

    /**
     * Whether to display values as percentages.
     * 
     * @var boolean
     */
    protected $percentages = self::TOOLTIP_VALUE_ONLY;
    
    /**
     * Whether to include empty sets of data in the report.
     * 
     * @var boolean
     */
    protected $null_values;
    
    /**
     * The packaged reports for this report design.
     * 
     * @var PackagedReportCollection
     */
    protected $packagedReports;
    
    /**
     * The initials of the base report owner, set when adding to my reports.
     * 
     * If set, then only this user can see the base report.
     * This property can be manually altered in the Rich Client.
     * 
     * @var string
     */
    protected $owner;
    
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->count_style = self::COUNT_NUM;
    }

    /**
     * This function doesn't set the title parameter because it will set in the report designer the 'Custom title' field.
     *
     * @return string
     */
    public function getTitle()
    {
        $fieldDefs = Registry::getInstance()->getFieldDefs();

        if ($this->title == '' && $this->rows != '')
        {
            $title = '';

            $rowsLabel = \Labels_FieldLabel::getLabelFromFieldDef($fieldDefs[$this->rows]);

            if ($this->rows_date_option != '' && $fieldDefs[$this->rows]->getType() == FieldInterface::DATE)
            {
                $rowsLabel .= ' (' . _tk($this->getDateOptionString($this->rows_date_option)) . ')';
            }

            if ($rowsLabel != '')
            {
                //This should probably use a $ModuleDefs variable instead of using an inconsistent language key...
                if ($this->module == 'PAL')
                {
                    $title = _tk('PALSNamesTitle') . ' by ' . $rowsLabel;
                }
                else
                {
                    // not sure how this type of string construction will work from an i18n point of view...
                    $title = _tk($this->module.'NamesTitle') . ' by ' . $rowsLabel;
                }
            }

            if ($this->columns != '')
            {
                $columnsLabel = \Labels_FieldLabel::getLabelFromFieldDef($fieldDefs[$this->columns]);

                if ($this->columns_date_option != '' && $fieldDefs[$this->columns]->getType() == FieldInterface::DATE)
                {
                    $columnsLabel .= ' (' . _tk($this->getDateOptionString($this->columns_date_option)) . ')';
                }

                if ($columnsLabel != '')
                {
                    $title .= ' and ' . $columnsLabel;
                }
            }

            return ($title != '' ? $title : '');
        }
        else
        {
            return $this->title;
        }
    }
    
    /**
     * Converts a date option constant into a string.
     * 
     * @param int $option The date option constant.
     * 
     * @return string
     */
    protected function getDateOptionString($option)
    {
        switch ($option)
        {
            case Report::DAY_OF_WEEK:
                return 'day_of_week';
                break;
                
            case Report::WEEK:
                return 'week';
                break;
                
            case Report::WEEK_DATE:
            	return 'week_date';
            	break;
                
            case Report::MONTH:
                return 'month';
                break;
                
            case Report::FINANCIAL_MONTH:
                return 'financial_month';
                break;

            case Report::QUARTER:
                return 'quarter';
                break;
                
            case Report::FINANCIAL_QUARTER:
                return 'financial_quarter';
                break;
                
            case Report::YEAR:
                return 'year';
                break;
                
            case Report::FINANCIAL_YEAR:
                return 'financial_year';
                break;
                
            case Report::MONTH_AND_YEAR:
                return 'month_and_year';
                break;
        }
    }
    
    /**
     * Check for valid count styles.
     * 
     * @param int $value
     * 
     * @throws ReportException
     */
    public function setCount_style($value)
    {
        if($value == '')
        {
            $value = self::COUNT_NUM;
        }

        if (!in_array($value, array(self::COUNT_NUM, self::COUNT_SUM, self::COUNT_AVE)))
        {
            throw new ReportException('Invalid count style option.');
        }

        $this->count_style = $value;
    }

    /**
     * Getter for packagedReports.
     *
     * @throws \src\reports\exceptions\ReportException
     * @return PackagedReportCollection
     */
    public function getPackagedReports()
    {
        if ($this->packagedReports === null)
        {
            if (!((int) $this->recordid > 0))
            {
                throw new ReportException('Unable to retrieve PackagedReportCollection: Report.recordid is not set');
            }
            
            $this->packagedReports = (new PackagedReportModelFactory)->getCollection();
            $this->packagedReports->setQuery(
                (new Query)->where(['web_packaged_reports.web_report_id' => $this->recordid])
            );
        }
        return $this->packagedReports;
    }

    public function getReportData()
    {
        $data_array = array();

        foreach($this->getProperties() as $property)
        {
            $data_array[$property] = $this->{$property};
        }

        $data_array['type'] = $this->getReportType();

        return $data_array;
    }
    
    /**
     * Get properties to XML export
     * 
     * @author gszucs
     * @return array
     */
    public function getExportData()
    {
    	return array_diff_key( $this->getReportData(), array_flip(['packagedReports', 'observers', 'queueEvents', 'addToCache','recordid']) );
    }

    /**
     * Setter for percentages.
     *
     * @param boolean $value
     * @throws \src\reports\exceptions\ReportException
     */
    public function setPercentages($value)
    {
    	if($value == '')
    	{
    		$value = self::TOOLTIP_VALUE_ONLY;
    	}
    	
    	if (!in_array($value, array(self::TOOLTIP_VALUE_ONLY, self::TOOLTIP_PERCENTAGE_ONLY, self::TOOLTIP_PERCENTAGE_AND_VALUE)))
    	{
    		throw new ReportException('Invalid percentage type option.');
    	}
    	
        $this->percentages = (int) $value;
    }
    
    /**
     * Setter for null_values.
     * 
     * @param boolean $value
     */
    public function setNull_Values($value)
    {
        $this->null_values = (bool) $value;
    }

    /**
     * Gives the opportunity to set this label in overriding methods.
     * @return string
     */
    public function getXAxisLabel()
    {
        $rows = explode('.', $this->rows);

        return \Labels_FieldLabel::GetFieldLabel($rows[1], '', $rows[0]);
    }

    /**
     * Builds an excel document object representing the data for this report.
     *
     * @param array $data The report data.
     * 
     * @return \PHPExcel
     */
    public function getExcelObject(array $data)
    {
        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setTitle("DatixWeb Excel export");
        $objPHPExcel->getProperties()->setSubject("DatixWeb Excel export");
        $objPHPExcel->getProperties()->setDescription("DatixWeb Excel export");

        $objPHPExcel = $this->buildDataTab($objPHPExcel, $data);

        $objPHPExcel = $this->buildChartTab($objPHPExcel, $data);

        return $objPHPExcel;
    }

    /**
     * Takes an Excel document object and adds a tab containing the dataset from this report.
     *
     * @param \PHPExcel $objPHPExcel
     * @param array $data The report data.
     * @return \PHPExcel
     */
    protected function buildDataTab(\PHPExcel $objPHPExcel, array $data)
    {
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Data');

        //FusionCharts and excel have different ordering rules for "categories" ("columns"),
        //so we need to reorder the data in the event that we have columns. This only affects horizontal bar charts.
        if($this instanceof BarChart && $this->orientation == BarChart::HORIZONTAL)
        {
            $data['rowHeaders'] = array_reverse($data['rowHeaders'], true);
            $data['data'] = array_reverse($data['data'], true);
        }
        //Pareto orders codes differently from the standard.
        // TODO: this works, but should definitely not be here. Factor this out to somewhere more sensible.
        if ($this instanceof ParetoChart)
        {
            arsort($data['data']);

            foreach($data['data'] as $code => $value)
            {
                $newRowHeaderOrder[$code] = $data['rowHeaders'][$code];
            }

            $data['rowHeaders'] = $newRowHeaderOrder;
        }

        //Column headers
        if($this->columns)
        {
            $currentColumn = 1;
            foreach($data['columnHeaders'] as $colTitle)
            {
                $colLetter = self::ExcelConvertToLetter($currentColumn);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, 1, $colTitle);
                $objPHPExcel->getActiveSheet()->getColumnDimension($colLetter)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($colLetter.'1')->getFont()->setBold(true);
                $currentColumn++;
            }
        }
        else
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'X-axis');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Data');
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        }

        //Row Headers
        $currentRow = 2;
        foreach ($data['rowHeaders'] as $rowTitle)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $currentRow, $rowTitle);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$currentRow)->getFont()->setBold(true);
            $currentRow++;
        }

        //Report data
        $currentRow = 2;
        foreach ($data['rowHeaders'] as $rowKey => $rowTitle)
        {
            if ($this->columns)
            {
                $currentColumn = 1;
                foreach($data['columnHeaders'] as $colKey => $colTitle)
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($currentColumn, $currentRow, (int) $data['data'][$rowKey][$colKey]);
                    $currentColumn++;
                }
            }
            else
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $currentRow, (int) $data['data'][$rowKey]);
            }

            $currentRow++;
        }

        // Footer
        $objPHPExcel = $this->addFooter($objPHPExcel, $currentRow);

        // Auto size contents of A column
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        return $objPHPExcel;
    }

    /**
     * Takes an excel document object and adds a tab containing a chart built from the data on the data tab.
     *
     * @param \PHPExcel $objPHPExcel
     * @param array $data The report data.
     * @return \PHPExcel
     */
    protected function buildChartTab(\PHPExcel $objPHPExcel, array $data)
    {
        /** Create new sheet for the chart */
        $objPHPExcel->createSheet(1);
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('Graph');

        $chart = $this->getChartObject($data);

        //	Add the chart to the worksheet
        $objPHPExcel->getActiveSheet()->addChart($chart);

        // Footer
        $objPHPExcel = $this->addFooter($objPHPExcel, 36);

        // Auto size contents of A column
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        return $objPHPExcel;
    }

    /**
     * Constructs a PHPExcel_Chart object pointing at the "Data" tab and returns it.
     * @param $data
     * @return \PHPExcel_Chart
     */
    protected function getChartObject($data)
    {
        $dataSeriesObjects = $this->getDataSeriesValues($data);

        //	Build the dataseries
        $series = $this->getChartDataseries(
            $dataSeriesObjects['dataseriesLabels'],
            $dataSeriesObjects['xAxisTickValues'],
            $dataSeriesObjects['dataSeriesValues']);

        $layout = $this->getChartLayout();

        //	Set the series in the plot area
        $plotarea = new \PHPExcel_Chart_PlotArea($layout, ( is_array( $series ) ? $series : array($series) ) );

        //	Set the chart legend
        if($this->getLegendState($data))
        {
            $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        }

        $title = new \PHPExcel_Chart_Title($this->getTitle());

        //	Create the chart
        $chart = new \PHPExcel_Chart(
            'chart1',		// name
            $title,			// title
            $legend,		// legend
            $plotarea,		// plotArea
            true,			// plotVisibleOnly
            0				// displayBlanksAs
        );

        //	Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition('A1');
        $chart->setBottomRightPosition('Q36');

        return $chart;
    }

    /**
     * Identifies the cells on the "Data" tab to reference in the chart object that we are building
     * @param $data
     * @return array
     */
    protected function getDataSeriesValues($data)
    {
        //	Set the Labels for each data series we want to plot
        if($this->columns)
        {
            $colNumber = 1;
            foreach($data['columnHeaders'] as $colTitle)
            {
                $dataseriesLabels[] = new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$'.self::ExcelConvertToLetter($colNumber).'$1', NULL, 1);
                $colNumber++;
            }
        }
        else
        {
            $dataseriesLabels[] = new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$1', NULL, 1);
        }

        //	Set the X-Axis Labels
        $xAxisTickValues = array(
            new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$2:$A$'.(count($data['rowHeaders']) + 1), NULL, 4),	//	Q1 to Q4
        );

        //	Set the Data values for each data series we want to plot
        if($this->columns)
        {
            $colNumber = 1;
            foreach($data['columnHeaders'] as $colTitle)
            {
                $colLetter = self::ExcelConvertToLetter($colNumber);
                $dataSeriesValues[] = new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$'.$colLetter.'$2:$'.$colLetter.'$'.(count($data['rowHeaders']) + 1), NULL, 1);
                $colNumber++;
            }
        }
        else
        {
            $dataSeriesValues[] = new \PHPExcel_Chart_DataSeriesValues('Number', 'Data!$B$2:$B$'.(count($data['rowHeaders']) + 1), NULL, 1);
        }

        return array(
            'dataseriesLabels' => $dataseriesLabels,
            'xAxisTickValues' => $xAxisTickValues,
            'dataSeriesValues' => $dataSeriesValues);
    }

    /**
     * Combines the three sets of data (roughly column headers, row headers and data) into a PHPExcel_Chart_DataSeries object.
     *
     * @param $dataseriesLabels
     * @param $xAxisTickValues
     * @param $dataSeriesValues
     * @return \PHPExcel_Chart_DataSeries
     */
    protected function getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues)
    {
        $series = new \PHPExcel_Chart_DataSeries(
            null,		// plotType set in specific report classes
            null,	// plotGrouping set in specific report classes
            range(0, count($dataSeriesValues)-1),			// plotOrder
            $dataseriesLabels,								// plotLabel
            $xAxisTickValues,								// plotCategory
            $dataSeriesValues								// plotValues
        );

        return $series;
    }

    /**
     * If a PHPExcel_Chart_Layout object is appropriate, this method can be overwritten to add it at the appropriate point
     * @return null|/PHPExcel_Chart_Layout
     */
    protected function getChartLayout()
    {
        return null;
    }

    /**
     * Determines whether a legend should be added to the excel chart.
     * @param $data
     * @return bool
     */
    protected function getLegendState($data)
    {
        if ($this->columns)
        {
            return true;
        }
        else
        {
            if (count($data['data']) > 15)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /**
     * Converts an integer into the appropriate excel column letter (A, B, ...Z, AA, AB...)
     * @param int $pColumnIndex
     * @return string
     */
    protected static function ExcelConvertToLetter($pColumnIndex = 0)
    {
        if ($pColumnIndex < 26)
        {
            return chr(65 + $pColumnIndex);
        }

        return static::ExcelConvertToLetter((int)($pColumnIndex / 26) -1).chr(65 + $pColumnIndex%26);
    }

    /**
     * @return string A human-readable name for the report type
     */
    abstract public function getTypeName();

    /**
     * @return string The constant integer representing this report type
     */
    abstract public function getReportType();

    /**
     * Setter to convert the value of this field from user date to SQL date.
     *
     * @param string $value Date in user format.
     */
    public function setcount_style_field_calendar($value)
    {
        $this->count_style_field_calendar = UserDateToSQLDate($value);
    }

    /**
     * Adds the footer to an excel sheet.
     *
     * @param \PHPExcel $objPHPExcel
     * @param int       $currentRow
     *
     * @return \PHPExcel
     */
    protected function addFooter(\PHPExcel $objPHPExcel, $currentRow)
    {
        $footerPos      = $currentRow+5;
        $footer         = (isset($GLOBALS['dtxexcel_footer']) ? $GLOBALS['dtxexcel_footer'] : $GLOBALS['dtxprint_footer']);
        $breaks         = ["<br />", "<br>", "<br/>"];
        $zColumnPos     = 25;
        $footerRowMerge = $footerPos+5;

        // Replace HTML line breaks by UTF8 line breaks
        $footer = str_ireplace($breaks, "\n", $footer);

        // Strip HTML and PHP tags
        $footer = strip_tags($footer);

        // Add footer to Excel sheet
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $footerPos, $footer);

        // Merge cells by columns A to Z and by rows $footerPos to $footerPos+5
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $footerPos, $zColumnPos, $footerRowMerge);

        // Make the text inside the cell pay attention t the line breaks
        $objPHPExcel->getActiveSheet()->getStyle('A'.($footerPos))->getAlignment()->setWrapText(true);

        // Cel vertical and horizontal alignment
        $objPHPExcel->getActiveSheet()->getStyle('A'.($footerPos))->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A'.($footerPos))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        return $objPHPExcel;
    }

}
