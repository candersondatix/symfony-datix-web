<?php
namespace src\reports\model\report;

use src\framework\registry\Registry;
use src\reports\exceptions\InvalidOptionException;
use src\reports\exceptions\ReportException;

class PieChart extends Report
{
    // style constants
    const STANDARD = 1;
    const EXPLODED = 2;
    
    /**
     * Standard/exploded pie chart.
     * 
     * @var int
     */
    protected $style = self::STANDARD;

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('pie_chart');
    }

    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::PIE;
    }

    /**
     * Prevent columns property from being set.
     * 
     * @param $value
     * 
     * @throws InvalidOptionException
     */
    public function setColumns($value)
    {
        $this->columns = null;
        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns" property on Pie Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }
    
    /**
     * Prevent columns_date_option property from being set.
     * 
     * @param $value
     * 
     * @throws InvalidOptionException
     */
    public function setColumns_date_option($value)
    {
        $this->columns_date_option = null;
        if (!($value === null || $value === ''))
        {
            Registry::getInstance()->getLogger()->logDebug('Attempted to set "columns_date_option" property on Pie Chart object to non-null value ('.$value.'). This value has been discarded.');
        }
    }
    
    /**
     * Check for valid style values.
     * 
     * @param int $value
     * 
     * @throws ReportException
     */
    public function setStyle($value)
    {
        if (!in_array($value, array(self::STANDARD, self::EXPLODED)))
        {
            throw new ReportException('Invalid pie chart style value.');
        }

        $this->style = $value;
    }

    /**
     * {@inheritdoc}
     */
    protected function getLegendState($data)
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function getChartLayout()
    {
        $layout = new \PHPExcel_Chart_Layout();
        $layout->setShowCatName(TRUE);

        return $layout;
    }

    /**
     * {@inheritdoc}
     */
    protected function getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues)
    {
        $series = parent::getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues);

        $series->setPlotType(\PHPExcel_Chart_DataSeries::TYPE_PIECHART);

        if ($this->style == self::EXPLODED)
        {
            $series->setPlotStyle(true);
        }

        return $series;
    }
}