<?php
namespace src\reports\model\report;

use src\framework\model\ModelFactory;

class ReportModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ReportFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ReportMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ReportCollection($this->getMapper(), $this->getEntityFactory());
    }
}