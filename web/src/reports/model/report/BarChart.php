<?php
namespace src\reports\model\report;

use src\framework\registry\Registry;
use src\reports\exceptions\ReportException;

class BarChart extends Report
{
    // orientation contstants
    const VERTICAL   = 1;
    const HORIZONTAL = 2;
    
    // style constants
    const GROUPED = 1;
    const STACKED = 2;
    
    /**
     * Vertical/horizonal orientation.
     * 
     * @var int
     */
    protected $orientation = self::VERTICAL;
    
    /**
     * Grouped/stacked style.
     * 
     * @var int
     */
    protected $style = self::GROUPED;

    /**
     * {@inheritdoc}
     */
    public function getTypeName()
    {
        return _tk('bar_chart');
    }
    
    /**
     * {@inheritdoc}
     */
    public function getReportType()
    {
        return Report::BAR;
    }

    /**
     * Check for valid orientation values.
     * 
     * @param int $value
     * 
     * @throws ReportException
     */
    public function setOrientation($value)
    {
        if ($value && !in_array($value, array(self::VERTICAL, self::HORIZONTAL)))
        {
            throw new ReportException('Invalid bar chart orientation value.');
        }
        
        $this->orientation = $value;
    }

    /**
     * Check for valid style values.
     * 
     * @param int $value
     * 
     * @throws ReportException
     */
    public function setStyle($value)
    {
        if ($value && !in_array($value, array(self::GROUPED, self::STACKED)))
        {
            throw new ReportException('Invalid bar chart style value.');
        }
        
        $this->style = $value;
    }

    /**
     * {@inheritdoc}
     */
    protected function buildDataTab(\PHPExcel $objPHPExcel, array $data)
    {
        $objPHPExcel = parent::buildDataTab($objPHPExcel,$data);

        //if we have a stacked bar chart only using rows, we need to include the row field label for use on the chart.
        if ($this->columns == '' && $this->style == self::STACKED)
        {
            $fieldDefs = Registry::getInstance()->getFieldDefs();
            $rowObj = $fieldDefs[$this->rows];

            $rowLabel = \Labels_FieldLabel::GetFieldLabel($rowObj->fdr_name, '', $rowObj->fdr_table);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $rowLabel);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        }

        return $objPHPExcel;
    }


    /**
     * {@inheritdoc}
     */
    protected function getDataSeriesValues($data)
    {
        //if we have a stacked bar chart only using rows, excel needs some help to work out how we want the chart to look. (basically just switching rows and columns)
        if ($this->columns == '' && $this->style == self::STACKED)
        {
            $rowNumber = 2;
            foreach($data['rowHeaders'] as $rowTitle)
            {
                $dataseriesLabels[] = new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$'.$rowNumber, NULL, 1);
                $rowNumber++;
            }

            //	Set the X-Axis Labels
            $xAxisTickValues = array(
                new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$A$1', NULL, 1)
            );

            //	Set the Data values for each data series we want to plot
            $rowNumber = 2;
            foreach($data['rowHeaders'] as $rowTitle)
            {
                $dataSeriesValues[] = new \PHPExcel_Chart_DataSeriesValues('String', 'Data!$B$'.$rowNumber, NULL, 1);
                $rowNumber++;
            }

            return array(
                'dataseriesLabels' => $dataseriesLabels,
                'xAxisTickValues' => $xAxisTickValues,
                'dataSeriesValues' => $dataSeriesValues);

        }
        else
        {
            return parent::getDataSeriesValues($data);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getChartDataseries($dataseriesLabels, $xAxisTickValues, $dataSeriesValues)
    {
        //differences between fusionCharts and excel mean grouped charts end up with the categories in the wrong order.
        if($this->orientation == self::HORIZONTAL && $this->style == self::GROUPED)
        {
            $plotOrder = range(count($dataSeriesValues)-1, 0);
        }
        else
        {
            $plotOrder = range(0, count($dataSeriesValues)-1);
        }

        //can't just call parent::getChartDataseries here because the plot order doesn't have a setter, annoyingly
        $series = new \PHPExcel_Chart_DataSeries(
            \PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
            null,	// plotGrouping set below
            $plotOrder,			// plotOrder
            $dataseriesLabels,								// plotLabel
            $xAxisTickValues,								// plotCategory
            $dataSeriesValues								// plotValues
        );

        if($this->orientation == self::HORIZONTAL)
        {
            $series->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_HORIZONTAL);
        }
        else
        {
            $series->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_VERTICAL);
        }

        if($this->style == self::STACKED)
        {
            $series->setPlotGrouping(\PHPExcel_Chart_DataSeries::GROUPING_STACKED);
        }
        else
        {
            $series->setPlotGrouping(\PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED);
        }

        return $series;
    }
}