<?php
namespace src\reports\model\report;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\framework\query\Query;
use src\reports\model\report\TrafficLightChart;
use src\users\model\User;

class ReportMapper extends DatixEntityMapper
{
    /**
     * Keeps track of the actual Report class the mapper is dealing with.
     * 
     * @var string
     */
    protected $target_class;
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return $this->target_class ?: 'src\\reports\\model\\report\\Report';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'web_reports';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->select([
            $this->getTable().'.type',
            $this->getTable().'.gauge_max_1 as gauge_section1_max',
            $this->getTable().'.gauge_max_2 as gauge_section2_max',
            $this->getTable().'.gauge_max_3 as gauge_section3_max',
            $this->getTable().'.gauge_colour_1 as gauge_section1_colour',
            $this->getTable().'.gauge_colour_2 as gauge_section2_colour',
            $this->getTable().'.gauge_colour_3 as gauge_section3_colour',
            $this->getTable().'.base_listing_report',
            $this->getTable().'.orientation',
            $this->getTable().'.style',
            $this->getTable().'.merge_repeated_values'
        ]);
        return parent::select($query);
    }
    
    /**
     * {@inheritdoc}
     */
    public function save(Entity $object)
    {
        parent::save($object);
        if ($object instanceof TrafficLightChart)
        {
            // save traffic light colours
            $this->db->setSQL('DELETE FROM web_report_code_colours WHERE web_report_id = ?');
            $this->db->prepareAndExecute([$object->recordid]);
            
            $this->db->setSQL('INSERT INTO web_report_code_colours VALUES (?, ?, ?)');
            foreach ($object->trafficCodeColours as $code => $colour)
            {
                $this->db->prepareAndExecute([$object->recordid, $code, $colour]);
            }
        }
        return true;
    }
    
    /**
     * Fetches the code colours for a traffic traffic light report.
     * 
     * @param TrafficLightChart $report The report object.
     * 
     * @return array
     */
    public function getTrafficCodeColours(TrafficLightChart $report)
    {
        if ($report->recordid > 0)
        {
            $this->db->setSQL('
                SELECT 
                    code, colour
                FROM
                    web_report_code_colours
                WHERE
                    web_report_id = ?');
            
            $this->db->prepareAndExecute([$report->recordid]);
            return $this->db->PDOStatement->fetchAll(\PDO::FETCH_KEY_PAIR);
        }
        else
        {
            return array();
        }
    }
    
    /**
     * {@inheritdoc}
     */
    protected function updateEntity(Entity $object)
    {
        parent::updateEntity($object);
        
        if ( !((int) $object->recordid > 0) )
        {
            // TODO this should be factored out for other entities that use these properties, maybe into a Trait?
            if ($_SESSION['CurrentUser'] instanceof User)
            {
                $createdby = $_SESSION['CurrentUser']->initials;
            }
            else
            {
                $createdby = '';
                $this->registry->getLogger()->logEmergency('Unable to determine logged-in user.');
            }
            
            $object->createdby   = $createdby;
            $object->createddate = date('d-M-Y H:i:s');
        }
    }
    
    /**
     * {@inheritdoc}
     * 
     * Additionally generates the report type string based on the report class.
     */
    protected function getEntityProperties(Entity $object)
    {
        $fields = parent::getEntityProperties($object);
        
        switch (get_class($object))
        {
            case 'src\\reports\\model\\report\\Crosstab':
                $type = Report::CROSSTAB;
                break;
                
            case 'src\\reports\\model\\report\\BarChart':
                $type = Report::BAR;
                $fields['orientation'] = $object->orientation;
                $fields['style'] = $object->style;
                break;
                
            case 'src\\reports\\model\\report\\ParetoChart':
                $type = Report::PARETO;
                break;
                
            case 'src\\reports\\model\\report\\LineChart':
                $type = Report::LINE;
                break;
                
            case 'src\\reports\\model\\report\\SpcChart':
                switch ($object->getType())
                {
                    case SpcChart::C_CHART:
                        $type = Report::SPC_C_CHART;
                        break;
                        
                    case SpcChart::I_CHART:
                        $type = Report::SPC_I_CHART;
                        break;
                        
                    case SpcChart::MOVING_RANGE:
                        $type = Report::SPC_MOVING_RANGE;
                        break;
                        
                    case SpcChart::RUN_CHART:
                        $type = Report::SPC_RUN_CHART;
                        break;
                    
                    default:
                        throw new MapperException('Unable to determine SPC Report type.');
                }
                break;
                
            case 'src\\reports\\model\\report\\PieChart':
                $type = Report::PIE;
                $fields['style'] = $object->style;
                break;
                
            case 'src\\reports\\model\\report\\TrafficLightChart':
                $type = Report::TRAFFIC;
                break;
                
            case 'src\\reports\\model\\report\\GaugeChart':
                $type = Report::GAUGE;
                break;
                
            case 'src\\reports\\model\\listingreport\\Listing':
                $type = Report::LISTING;
                $fields['base_listing_report'] = $object->base_listing_report;
                
                // unset the columns property for listing reports, since this is not the same as the statistical report columns property
                unset($fields['columns']);
                break;

            default:
                throw new MapperException('Unable to determine Report type.');
        }
        
        $fields['type'] = $type;
        
        return $fields;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getFieldMappings()
    {
        return [
            'gauge_section1_max'    => 'gauge_max_1',
            'gauge_section2_max'    => 'gauge_max_2',
            'gauge_section3_max'    => 'gauge_max_3',
            'gauge_section1_colour' => 'gauge_colour_1',
            'gauge_section2_colour' => 'gauge_colour_2',
            'gauge_section3_colour' => 'gauge_colour_3'
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    protected function isObjectValid(Entity $object)
    {
        if (parent::isObjectValid($object))
        {
            $this->target_class = get_class($object);
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Finds a report using the id of the base listing report
     *
     * @param int $id The ID of the base listing report
     *
     * @return SavedQuery|null
     */
    public function findByBaseListingReportID($id)
    {
        list($query, $where, $fields) = $this->factory->getQueryFactory()->getQueryObjects();

        $where->add($fields->field('web_reports.base_listing_report')->eq($id));
        $query->where($where);

        $result = $this->select($query);

        if (!empty($result))
        {
            return $this->factory->getEntityFactory()->createObject($result[0]);
        }

        return null;
    }
}