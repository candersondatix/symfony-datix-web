<?php
namespace src\reports\model\report;

use src\framework\model\EntityCollection;
use src\framework\query\Query;
use src\framework\query\Where;
use src\framework\query\FieldCollection;
use src\users\model\User;

class ReportCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\reports\\model\\report\\Report';
    }
    
    /**
     * {@inherit}
     * 
     * Appends query conditions so that private base reports are only accessible to the owners.
     */
    public function setQuery(Query $query)
    {
        if ($_SESSION['CurrentUser'] instanceof User)
        {
            $where = new Where();
            $where->add(
                'OR',
                (new FieldCollection)->field('web_reports.owner')->isNull(),
                (new FieldCollection)->field('web_reports.owner')->isEmpty(),
                (new FieldCollection)->field('web_reports.owner')->eq($_SESSION['CurrentUser']->initials)
            );
            $query->where($where);
        }
        
        parent::setQuery($query);
    }
}