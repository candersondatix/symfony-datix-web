<script type="text/javascript">
	//if the report has been loaded invisibly in order to kick off a pdf export, do it here.
	jQuery(document).ready(function()
	{
	    <?php if ($this->export): ?>
	    	startFusionChartExport("<?php echo Sanitize::SanitizeString($this->format) ?>");
	    <?php endif ?>
	
	});
	function printChart()
	{
	    FusionCharts("myFusionChartReport").print();
	}
    function customReportTooltip(content)
    {
        content = content.replace( /{/g, '<').replace( /}/g, '>'); // IE hack
        jQuery("<div></div>")
            .addClass("customReportTooltipFusion")
            .css({"left" : mouseX, "top" : mouseY})
            .html(content)
            .appendTo(document.body)
            .bind("mouseleave", function(){
                jQuery(this).hide();
            })
    }

    var mouseX = 0;
    var mouseY = 0;
    jQuery(document).ready(function(){
        jQuery(document).mousemove(function(e){
            mouseX = e.pageX - 10;
            mouseY = e.pageY - 10;
        });
    });
</script>
<div id="chartContainer" style="height:640px;">
<?php $this->chart->renderChart(); ?>
</div>
<div class="button_wrapper">
    <?php if(!\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()){ ?><input type="button" value="<?php echo _tk('print')?>" onclick="printChart();" />
    <input type="button" onclick="
                            var buttons = new Array();
                            buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'exportRiskGradeTrackerReport(<?php echo $this->type; ?>, <?php echo $this->recordid; ?>, \'<?php echo $this->reporton; ?>\', \'<?php echo $this->startdate; ?>\', \'<?php echo $this->enddate; ?>\');'};
                            buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exportoptions\').CloseFloatingControl();'};

                            PopupDivFromURL('exportoptions', 'Export', 'app.php?action=httprequest&amp;type=getExportFormHTML', '', buttons, '');" value="Export"/><?php }?>
                            
    <input type="button" value="Back to record" onclick="SendTo('app.php?action=risk&recordid=<?php echo $this->recordid; ?>')" />
</div>