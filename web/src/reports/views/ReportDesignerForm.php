<div id="designer-wrapper"<?php echo ($this->states['designer_state'] == 'closed' && $this->context != 'dashboard' && $this->context != 'administration') ? ' style="display: none;"' : ''; echo $this->context ? ' class="' . $this->context . '"' : ''; ?>>
    <?php if ($this->context != 'administration' && $this->context != 'dashboard'): ?>
    <form method="post" id="designReport" name="designReport" action="app.php?action=reportdesigner&module=<?php echo $this->module ?>&token=<?php echo \CSRFGuard::getCurrentToken(); ?>">
    <?php endif; ?>
        <input type="hidden" id="designer-state" name="designer_state" class="state" value="<?php echo $this->states['designer_state'] ? $this->states['designer_state'] : 'open'; ?>" />
        <input type="hidden" id="scroll-pos" name="scroll_pos" value="<?php echo $this->states['scroll_pos']; ?>" />
        <input type="hidden" id="module" name="module" value="<?php echo $this->module ?>" />
        <input type="hidden" name="form_action" value="show_report" />
        <?php if ($this->newQueryID != ''): ?>
        <input type="hidden" name="new_query_id" value="<?php echo (int) $this->newQueryID ?>" />
        <?php endif ?>
        <?php if ($this->base_report != ''): ?>
        <input type="hidden" name="base_report" value="<?php echo (int) $this->base_report ?>" />
        <?php endif ?>
        <?php if ($this->context != 'administration' && $this->context != 'dashboard'): ?>
            <?php echo $this->heading; ?>
        <?php endif; ?>
        <div id="viewport">
        <div id="viewport-container">
        <?php if (!$this->atPromptOnly): ?>
        <div class="collapse">
            <div class="inner">
                <div name="report_type_title" id="report-type-title" class="section_title_row first">
                    <div class="section_title_group">
                        <div class="section_title"><?php echo _tk('report_type'); ?></div>
                    </div>
                </div>
                <div id="report-selector-container" class="target">
                    <div class="form-item">
                        <div id="report-type">
                            <?php
                            foreach($this->types_combined as $value => $data)
                            {
                                ?><span<?php if($data['default'] === true){ echo ' id="type-default"'; } ?> class="item-container"><a title="<?php echo $data['title']; ?>" class="parent item tooltip" data-name="<?php echo $data['title']; ?>" data-value="<?php echo $value; ?>" data-image="<?php echo $data['image']; ?>" tabindex="<?php echo getTabIndexValue(100); ?>"><img src="Images/new/report_designer_thumbs/<?php echo $data['image']; ?>.gif" draggable="false" alt="<?php echo $data['title']; ?>" /><?php
                                if($data['options'])
                                {
                                    ?><div class="more-options tooltip right" title="Chart options"></div><?php
                                }

                                ?></a><?php

                                if($data['options'])
                                {
                                    ?><div class="options"><?php
                                    foreach($data['options'] as $value => $data)
                                    {
                                        ?><a<?php if($data['default'] === true){ echo ' id="type-default"'; } ?> title="<?php echo $data['title']; ?>" class="item tooltip" data-name="<?php echo $data['title']; ?>" data-value="<?php echo $value; ?>" data-image="<?php echo $data['image']; ?>"><img src="Images/new/report_designer_thumbs/<?php echo $data['image']; ?>.gif" draggable="false" alt="<?php echo $data['title']; ?>" /></a><?php
                                    }
                                    ?></div><?php
                                }
                                ?></span><?php

                                $i++;
                            }
                            ?>
                        </div>
                        <div class="clearer"></div>
                    </div>
                </div>
                <div style="display: none;"><?php echo $this->fieldContent['type']->GetField(); ?></div>
            </div>
        </div>
        <div id="settings" class="collapse">
            <div name="columns_title_row" id="columns_title_row" class="section_title_row">
                <div class="section_title_group">
                    <div class="section_title"><?php echo _tk('report_settings') ?></div>
                </div>
            </div>
            <div class="target">
                    <div class="form-item" id="report-title">
                    <label class="codefield-label" for="title"><?php echo _tk('report_title'); ?></label><div class="codefield-container"><?php echo $this->fieldContent['title']->GetField(); ?></div>
                    <div class="clearer"></div>
                </div>
                <?php if ($this->context != 'administration' && $this->context != 'dashboard'): ?>
                <div class="form-item" id="search-option">
                    <label class="codefield-label" for="query_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('query'); ?></label>
                    <div class="codefield-container"><?php echo $this->fieldContent['query']->GetField(); ?></div>
                    <div class="clearer"></div>
                </div>
                <?php endif; ?>
                <div class="form-item" id="listing-option" style="display: none;">
                    <label class="codefield-label" for="base_listing_report_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('select_base_listing_report'); ?></label>
                    <div class="codefield-container"><?php echo $this->fieldContent['base_listing_report']->GetField(); ?></div>
                    <div class="clearer"></div>
                </div>
                <div class="form-item" id="spc-option" style="display: none;">
                    <label class="codefield-label" for="spc_type_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('select_spc_type'); ?></label>
                    <div class="codefield-container"><?php echo $this->fieldContent['spc_type']->GetField(); ?></div>
                    <div class="clearer"></div>
                </div>
            </div>
        </div>
        <?php endif; // atPromptOnly ?>
        <?php if ($this->context != 'administration' && $this->context != 'dashboard'): ?>
        <div id="atprompt" class="collapse"<?php echo $this->hasAtPrompt ? '' : ' style="display: none;"'; ?>>
            <input type="hidden" name="atprompt_toggle_state" class="state" value="<?php echo $this->states['atprompt_toggle_state'] ? $this->states['atprompt_toggle_state'] : 'open'; ?>" />
            <div id="atprompt-options">
                <div name="atprompt_title_row" id="atprompt_title_row" class="section_title_row">
                    <div class="section_title_group">
                        <div class="section_title"><?php echo _tk('at_prompt_values') ?></div><span class="trigger icon<?php if($this->states['type_toggle_state'] == 'open'){ echo ' minus'; }else{ echo ' plus'; } ?>" title="click to hide section"></span>
                    </div>
                </div>
                <div id="atprompt-fields" class="target"<?php echo $this->states['type_toggle_state'] == 'open' ? '' : ' style="display: none;"'; ?>>
                    <?php echo $this->atPromptSection ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if (!$this->atPromptOnly): ?>
        <div class="collapse">
            <div id="gauge-options" style="display: none;">
                <div name="gauge_title_row" id="gauge_title_row" class="section_title_row">
                    <div class="section_title_group">
                        <div class="section_title">Gauge options</div>
                    </div>
                </div>
                <div class="target">
                    <div class="form-item">
                        <div>
                            <label for="gaugesection_1_minvalue" id="gaugesection_1_minvalue_label">Min. value</label>
                            <input type="text" name="gauge_section1_min" id="gaugesection_1_minvalue" class="values" size="3" maxlength="10" value="0" readonly="readonly" disabled="disabled"/>
                            <label for="section1max" id="section1max_label"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" />Max. value</label>
                            <input type="text" name="gauge_section1_max" id="section1max" class="values" size="3" maxlength="10" value="<?php echo $this->data['gauge']['section1_max']; ?>" autocomplete="off" />
                            <label for="section1colour" id="section1colour_label">Colour</label>
                            <input type="text" name="gauge_section1_colour" id="section1colour" class="colour-picker" size="7" maxlength="7" value="<?php echo $this->data['gauge']['section1_colour']; ?>" autocomplete="off" style="background-color: #<?php echo $this->data['gauge']['section1_colour']; ?>; color: #<?php echo $this->data['gauge']['section1_colour']; ?>;"/>
                        </div>
                        <div>
                            <label for="gaugesection_2_minvalue" id="gaugesection_2_minvalue_label">Min. value</label>
                            <input type="text" name="gauge_section2_min" id="gaugesection_2_minvalue" class="values" size="3" maxlength="10" value="<?php echo $this->data['gauge']['section1_max']; ?>" readonly="readonly" disabled="disabled"/>
                            <label for="section2max" id="section2max_label"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" />Max. value</label>
                            <input type="text" name="gauge_section2_max" id="section2max" class="values" size="3" maxlength="10" value="<?php echo $this->data['gauge']['section2_max']; ?>" autocomplete="off" />
                            <label for="section2colour" id="section2colour_label">Colour</label>
                            <input type="text" name="gauge_section2_colour" id="section2colour" class="colour-picker" size="7" maxlength="7" value="<?php echo $this->data['gauge']['section2_colour']; ?>" autocomplete="off" style="background-color: #<?php echo $this->data['gauge']['section2_colour']; ?>; color: #<?php echo $this->data['gauge']['section2_colour']; ?>;"/>
                        </div>
                        <div>
                            <label for="gaugesection_3_minvalue" id="gaugesection_3_minvalue_label">Min. value</label>
                            <input type="text" name="gauge_section3_min" id="gaugesection_3_minvalue" class="values" size="3" maxlength="10" value="<?php echo $this->data['gauge']['section2_max']; ?>" readonly="readonly" disabled="disabled"/>
                            <label for="section3max" id="section3max_label"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" />Max. value</label>
                            <input type="text" name="gauge_section3_max" id="section3max" class="values" size="3" maxlength="10" value="<?php echo $this->data['gauge']['section3_max']; ?>" autocomplete="off" />
                            <label for="section3colour" id="section3colour_label">Colour</label>
                            <input type="text" name="gauge_section3_colour" id="section3colour" class="colour-picker" size="7" maxlength="7" value="<?php echo $this->data['gauge']['section3_colour']; ?>" autocomplete="off" style="background-color: #<?php echo $this->data['gauge']['section3_colour']; ?>; color: #<?php echo $this->data['gauge']['section3_colour']; ?>;"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rows" class="collapse"<?php if($this->states['rows_state'] == 'hide'){ echo ' style="display:none;"'; } ?>>
            <div class="inner">
                <div name="rows_title_row" id="rows_title_row" class="section_title_row">
                    <div class="section_title_group">
                        <div class="section_title"><?php echo _tk('field_1'); ?></div>
                    </div>
                </div>
                <div class="target">
                    <div class="ui-widget form-item">
                        <label class="codefield-label" for="rows_table_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('form_label'); ?></label>
                        <div class="codefield-container"><?php echo $this->fieldContent['rows_table']->GetField(); ?></div>
                        <div class="clearer"></div>
                    </div>
                    <div class="ui-widget form-item" id="module-combined">
                        <label class="codefield-label" for="rows_field_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('field_label'); ?></label>
                        <div class="codefield-container"><?php echo $this->fieldContent['rows_field']->GetField(); ?></div>
                        <div class="clearer"></div>
                    </div>

                            <div class="form-item" name="rows_row" id="rows_date_option_row"<?php if($this->states['rows_date_option_state'] == 'hide'){ echo ' style="display: none;"'; } ?>>
                        <label class="codefield-label" for="rows_date_option_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('date_label'); ?></label>
                        <div class="codefield-container"><?php echo $this->fieldContent['rows_date_option']->GetField(); ?></div>
                        <div class="clearer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="traffic-light-data" class="collapse"<?php if($this->states['traffic_state'] == 'hide'){ echo ' style="display:none;"'; } ?>>
            <div class="inner">
                <div name="traffic_title_row" id="traffic_title_row" class="section_title_row">
                    <div class="section_title_group">
                        <div class="section_title">Traffic light</div>
                    </div>
                </div>
                <div class="target">
                    <div class="ui-widget form-item" id="module-combined">
                        <label class="codefield-label" for="traffic_field_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo $this->text['field_label']; ?></label>
                        <div class="codefield-container">
                        	<?php echo $this->fieldContent['traffic_field']->GetField(); ?>
                        	<input type="hidden" name="rep_colours" id="rep_colours" value="<?php echo $this->data['rep_colours']; ?>">
                        </div>
                        <div class="clearer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="traffic-light-options"<?php if($this->states['traffic_state'] == 'hide'){ echo ' style="display:none;"'; } ?> class="collapse">
            <div name="traffic_title_row" id="traffic_title_row" class="section_title_row">
                <div class="section_title_group">
                    <div class="section_title">Traffic light options</div>
                </div>
            </div>
            <div id="traffic-options-content" class="form-item target"><div id="traffic_fields_colour_row" class="field_div">Please select a field from above</div></div>
        </div>
        <div id="columns" class="collapse"<?php if($this->states['columns_state'] == 'hide'){ echo ' style="display:none;"'; } ?>>
            <div class="inner">
                <div name="columns_title_row" id="columns_title_row" class="section_title_row">
                    <div class="section_title_group">
                        <div class="section_title"><?php echo _tk('field_2'); ?></div>
                    </div>
                </div>
                <div class="target">
                    <div class="ui-widget form-item">
                        <label class="codefield-label" for="columns_table_title"><?php echo $this->text['form_label']; ?></label>
                        <div class="codefield-container"><?php echo $this->fieldContent['columns_table']->GetField(); ?></div>
                        <div class="clearer"></div>
                    </div>
                    <div class="ui-widget form-item" id="module2-combined">
                        <label class="codefield-label" for="columns_field_title"><?php echo $this->text['field_label']; ?></label>
                        <div class="codefield-container"><?php echo $this->fieldContent['columns_field']->GetField(); ?></div>
                        <div class="clearer"></div>
                    </div>
                    <div class="form-item" name="columns_row" id="columns_date_option_row"<?php if($this->states['columns_date_option_state'] == 'hide'){ echo ' style="display: none;"'; } ?>>
                        <label class="codefield-label" for="columns_date_option_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('date_label'); ?></label>
                        <div class="codefield-container"><?php echo $this->fieldContent['columns_date_option']->GetField(); ?></div>
                        <div class="clearer"></div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(bYN(GetParm("ADDITIONAL_REPORT_OPTIONS", "Y"))): ?>
        <div class="collapse" id="additional-options"<?php if($this->states['additional_options_state'] == 'hide'){ echo ' style="display:none;"'; } ?>>
            <div class="inner">
                <div name="columns_title_row" id="columns_title_row" class="section_title_row">
                    <div class="section_title_group">
                        <div class="section_title"><?php echo _tk('show_additional_options'); ?></div>
                    </div>
                </div>
                <div class="target">
                    <div class="form-item" id="limit-option">
                        <label for="limit_to_top" id="limit_to_top_label"><?php echo _tk('display_top') ?> <input name="limit_to_top" id="limit_to_top" type="text" class="small" value="<?php echo $this->data['limit_to_top'] > 0 ? $this->data['limit_to_top'] : ''; ?>" maxlength="7" /> <?php echo _tk('display_top_items') ?></label>
                    </div>
                    <input type="hidden" name="orientation" id="orientation" value = "<?php echo $this->data['orientation'] ?: \src\reports\model\report\BarChart::VERTICAL; ?>" />
                    <input type="hidden" name="bar_style" id="bar-style" value = "<?php echo ($this->data['type'] === src\reports\model\report\Report::BAR && $this->data['style']) ? $this->data['style'] : \src\reports\model\report\BarChart::GROUPED; ?>" />
                    <input type="hidden" name="pie_style" id="pie-style" value = "<?php echo ($this->data['type'] === src\reports\model\report\Report::PIE && $this->data['style']) ? $this->data['style'] : \src\reports\model\report\PieChart::STANDARD; ?>" />
                    <div id="count-options">
                        <div class="ui-widget form-item control-group short">
                            <label class="codefield-label" for="count_style_title"><?php echo _tk('count_style'); ?></label>
                            <div class="codefield-container"><?php echo $this->fieldContent['count_style']->GetField(); ?></div>
                            <div class="clearer"></div>
                        </div>
                        <div class="ui-widget form-item short" id="count-style-sum"<?php if($this->states['count_style_field_sum_state'] == 'hide'){ echo ' style="display: none;"'; } ?>>
                            <label class="codefield-label" for="count_style_field_sum_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('field_to_sum'); ?></label>
                            <div class="codefield-container"><?php echo $this->fieldContent['count_style_field_sum']->GetField(); ?></div>
                            <div class="clearer"></div>
                        </div>
                        <div class="ui-widget form-item short" id="count-style-ave"<?php if($this->states['count_style_field_average_state'] == 'hide'){ echo ' style="display: none;"'; } ?>>
                            <label class="codefield-label" for="count_style_field_average_title"><img src="images/Warning.gif" alt="Mandatory" class="mandatory_image" /><?php echo _tk('field_to_ave'); ?></label>
                            <div class="codefield-container"><?php echo $this->fieldContent['count_style_field_average']->GetField(); ?></div>
                            <div class="clearer"></div>
                        </div>
                        <div class="ui-widget form-item short" id="count-style-value-at"<?php if($this->states['count_style_field_value_at_state'] == 'hide'){ echo ' style="display: none;"'; } ?>>
                            <label class="codefield-label" for="count_style_field_value_at_title"><?php echo _tk('field_value_at'); ?></label>
                            <div class="codefield-container"><?php echo $this->fieldContent['count_style_field_value_at']->GetField(); ?></div>
                            <div class="clearer"></div>
                        </div>
                        <div class="ui-widget form-item short" id="count-style-calendar"<?php if($this->states['count_style_field_calendar_state'] == 'hide'){ echo ' style="display: none;"'; } ?>>
                            <label class="codefield-label" for="count_style_field_calendar_title"></label>
                            <div class="codefield-container"><?php echo $this->fieldContent['count_style_field_calendar']->GetField(); ?></div>
                            <div class="clearer"></div>
                        </div>
                        <div class="ui-widget form-item" id="count-style-initial-levels"<?php if($this->states['count_style_field_initial_levels_state'] == 'hide'){ echo ' style="display: none;"'; } ?>>
                            <label class="check-label" for="count_style_field_initial_levels"><?php echo _tk('initial_levels'); ?> <span class="icon light help help-tooltip" title="<?php echo _tk('initial_levels_help'); ?>"></span></label>
                            <?php echo $this->fieldContent['count_style_field_initial_levels']->GetField(); ?>
                        </div>
                    </div>
                    <div class="form-item" id="percent-option">
                        <label class="codefield-label" for="percentages_title"><?php echo _tk('show_as_percentages'); ?> <span class="icon light info help-tooltip" title="<?php echo _tk('percentage_warning'); ?>"></span></label>
                        <div class="codefield-container"><?php echo $this->fieldContent['percentages']->GetField(); ?></div>
                        <div class="clearer"></div>
                    </div>
                    <div class="form-item" id="null-option">
                        <label class="check-label" for="null_values"><?php echo _tk('show_total_nulls'); ?> <span class="icon light help help-tooltip" title="<?php echo _tk('null_help'); ?>"></span></label><?php

                        echo $this->fieldContent['null_values']->GetField();
                        ?>
                    </div>
                </div>
            </div>
        </div>
                <?php endif; ?>
        <?php endif; // atPromptOnly ?>
        </div>
        </div>
        <?php if ($this->context != 'administration' && $this->context != 'dashboard'): ?>
            <div id="form-actions">
                <button id="run" type="submit" class="btn btn-primary"><?php echo $this->report_run ? _tk('update_report'): _tk('run_report'); ?></button>
                <?php if(!\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()) { ?>
                    <button id="export_no_render" type="button" class="btn btn-primary" onclick="
                        var fields = getFields();

                        if(validateFields(fields)) {

                            var buttons = [];
                            buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'if(setReturns(1)){var exportFormat = jQuery(\'input[name=reportoutputformat]:checked\').val();var exportOrientation = jQuery(\'input[name=orientation]:checked\').val();var exportPaperSize = jQuery(\'#papersize\').val();GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();exportNoRender(exportFormat, exportOrientation, exportPaperSize);}'};
                            buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exporttopdfoptions\').CloseFloatingControl();'};

                            PopupDivFromURL('exporttopdfoptions', 'Export', 'app.php?action=httprequest&amp;type=exporttopdfoptions&report_type=listing&recordid=<?= $this->recordid ?>', jQuery('#designReport').serialize(), buttons, '', function(){ removeSuggestCsvExport(); });

                        } return false;" value="Export"
                        <?php if (!($this->report instanceof src\reports\model\listingreport\Listing)): ?> style="display:none;"<?php endif ?>><?php echo _tk('btn_export'); ?></button>
                <?php } ?>
                <?php if (!$this->atPromptOnly): ?><button id="reset" type="reset" class="btn btn-primary"><?php echo $this->text['clear']; ?></button><?php endif; ?>
            </div>
    </form>
        <?php endif; ?>
</div>
<?php if ($this->context != 'administration' && $this->context != 'dashboard'): ?>
    <div class="collapse-designer"><span class="icon <?php echo $this->states['designer_state'] == 'open' ? 'open_designer' : 'close_designer'; ?>"></span></div>
<?php endif; ?>
