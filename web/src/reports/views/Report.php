<script type="text/javascript">
        // if the report has been loaded invisibly in order to kick off a pdf export, do it here.
        jQuery(document).ready(function()
        {
            <?php if ($this->export): ?>
            startFusionChartExport("<?php echo Sanitize::SanitizeString($this->format) ?>"<?php echo $this->recordid ? ', '.$this->recordid : '' ?>);
            <?php endif ?>

            jQuery('#add-to-dashboard').on('click', function(){
                if(hasChanges()) {

                    if(confirm(text['changes_confirm'])) {

                        PromptForReportTitle(jQuery('#current_rep_name').val(),'<?php echo $this->module; ?>', true, <?php echo ($_SESSION['CurrentReport']->recordid ? $_SESSION['CurrentReport']->recordid : '0'); ?>);
                    }
                }
                else {

                    PromptForReportTitle(jQuery('#current_rep_name').val(),'<?php echo $this->module; ?>', true, <?php echo ($_SESSION['CurrentReport']->recordid ? $_SESSION['CurrentReport']->recordid : '0'); ?>);
                }
            });

            jQuery('#save-report').on('click', function(){

                if(hasChanges()) {

                    var confirmText = text['changes_confirm'];
                }
                else {

                    var confirmText = text['save_confirm'];
                }

                if(confirm(confirmText)) {

                    PromptForReportTitle(jQuery('#current_rep_name').val(),'<?php echo $this->module ?>', false, <?php echo $this->recordid ?: $this->base_report ?: 0 ?>, 0);
                }
            });

            jQuery('#add-to-my-reports').on('click', function(){
                if(hasChanges()) {

                    if(confirm(text['changes_confirm'])) {

                        PromptForReportTitle(jQuery('#current_rep_name').val(),'<?php echo $this->module ?>', false, 0, 1);
                    }
                }
                else {

                    PromptForReportTitle(jQuery('#current_rep_name').val(),'<?php echo $this->module ?>', false, 0, 1);
                }
            });

            jQuery('#save-as-new').on('click', function(){

                if(hasChanges()) {

                    if(confirm(text['changes_confirm'])) {

                        PromptForReportTitle(jQuery('#current_rep_name').val() ,'<?php echo $this->module ?>', false, 0, 1);
                    }
                }
                else {

                    PromptForReportTitle(jQuery('#current_rep_name').val() ,'<?php echo $this->module ?>', false, 0, 1);
                }
            });
        });

        function exportListing()
        {
            SendTo('app.php?action=httprequest&responsetype=json&type=exportreport&format='+jQuery("input[name=reportoutputformat]:checked").val()
                +'&orientation='+jQuery("input[name=orientation]:checked").val()+'&papersize='+jQuery('#papersize').val());
            GetFloatingDiv('exportoptions').CloseFloatingControl();
        }
</script>
<div id="content-container">

    <?php if ($this->customReportBuilder || $this->hasAtPrompt): ?>
        <div id="designer-container"><?php echo $this->designer; ?></div>
    <?php endif; ?>
    <div id="report-container" class="report-<?php echo $this->reportNames[$this->type]; ?>">
        <?php if($this->report): ?>
            <?php if ($this->isCrosstab): ?>
                <div class="windowbg2" style="padding: 5px; border-top: grey 1px solid;">
                    <div style="float: left; margin: 5px 5px 0 0;"><b><?php echo _tk('drill_down'); ?>: </b></div>
                    <?php echo $this->drillDownField->getField(); ?>
                </div>
                <div class="crosstab-title"><?php echo htmlspecialchars($this->title);?></div>
            <?php endif ?>
            <?php if ($this->overrideSecurity): ?>
                <div style="border:1px black solid;padding:5px;">
                    <div id="OverrideSecMessage" align="center"><?php echo _tk('override_sec_warning'); ?></div>
                </div>
            <?php endif ?>
            <input type="hidden" id="current_rep_name" name="current_rep_name" value="<?php echo htmlspecialchars($_SESSION['CurrentReport']->name ?: $_SESSION['CurrentReport']->report->title, ENT_QUOTES); ?>" />
            <?php echo $this->report ?>
        <!-- Hard coded listing reports have their own buttons for the moment -->
        <?php if (!$this->isHardCodedListing): ?>
            <div class="button_wrapper">
                <?php if ( ! $this->isListing && ! $this->isCrosstab && ! in_array($this->type, array(\src\reports\model\report\Report::TRAFFIC, \src\reports\model\report\Report::GAUGE)) && !\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()): ?>
                    <input type="button" value="<?=_tk('print')?>" id="printButton" onclick="printChart();" disabled="disabled" />
                <?php endif ?>
                <?php if ($_SESSION['licensedModules'][MOD_DASHBOARD] && $this->dashPerms == 'DAS_FULL' && $this->customReportBuilder) : ?>
                	<input type="button" value="<?php echo _tk('add_to_dashboard'); ?>" id="add-to-dashboard" />
                <?php endif; ?>
                <?php if ($this->canSaveReport): ?>
                	<input type="button" value="<?=_tk('save_changes')?>" id="save-report" />
                <?php endif ?>
                <?php if ($this->recordid == '' &&  $this->base_report == '' && $this->saveMyReports): ?>
                	<input type="button" value="<?=_tk('add_to_my_reports')?>" id="add-to-my-reports" />
                <?php elseif (($this->recordid != '' || $this->base_report != '') && $this->saveMyReports && $this->customReportBuilder): ?>
                	<input type="button" value="<?=_tk('save_as_new')?>" id="save-as-new" />
                <?php endif ?>
                <?php if ($this->isCrosstab && $_SESSION['crosstab_current_drill_level'] > 0) : ?>
                    <input type="button" value="Back one level" onClick="SendTo('app.php?action=reportdesigner&drilllevel=<?php echo $_SESSION['crosstab_current_drill_level']-1; ?>&module=<?php echo $this->module; ?>');">
                    <input type="button" value="Back to top level" onClick="SendTo('app.php?action=reportdesigner&drilllevel=0&module=<?php echo $this->module; ?>');">
                <?php endif; ?>
                <?php if ($this->type == \src\reports\model\report\Report::LISTING && !\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()): ?>
                    <input type="button" onclick="
                        var buttons = new Array();
                        buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'exportListing()'};
                        buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'var div = GetFloatingDiv(\'exportoptions\');div.CloseFloatingControl();hideLoadPopup();enableAllButtons();destroyQtips();'};

                        PopupDivFromURL('exportoptions', 'Export', 'app.php?action=httprequest&type=exporttopdfoptions&report_type=listing&recordid=<?= $this->recordid ?>', '', buttons, '', function(){ removeSuggestCsvExport(); });" value="Export" />
                <?php elseif ($this->type != \src\reports\model\report\Report::TRAFFIC && !\src\framework\registry\Registry::getInstance()->getDeviceDetector()->isTablet()): ?>
                    <input type="button" onclick="
                            var buttons = new Array();
                            buttons[0]={'value':'<?php echo _tk('btn_export')?>','onclick':'exportReport(<?php echo $this->type; ?><?php echo $this->recordid ? ', '.$this->recordid : '' ?>);'};
                            buttons[1]={'value':'<?php echo _tk('btn_cancel')?>','onclick':'GetFloatingDiv(\'exportoptions\').CloseFloatingControl();'};

                            PopupDivFromURL('exportoptions', 'Export', 'app.php?action=httprequest&amp;type=getExportFormHTML', '', buttons, '');" value="Export" />
                <?php endif ?>
                <?php if ($this->dashboardId !== null && $this->dashboardId !== '') : ?>
                    <input type="button" value="<?php echo _tk('back_to_dashboard'); ?>" onclick="SendTo('app.php?action=dashboard&dash_id=<?php echo $this->dashboardId; ?>');">
                <?php endif; ?>
                <?php if ($this->fromMyReports !== null && $this->fromMyReports !== false) : ?>
                    <input type="button" value="<?php echo _tk('btn_back_to_reports_menu'); ?>" onclick="SendTo('app.php?action=listmyreports&module=<?php echo $this->module; ?>&form=predefined');">
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <span id="report-watermark" style="background-image: url(Images/new/watermark/bar-watermark.gif)"><?php echo $this->reportWatermark ?></span>
    <?php endif; ?>
    </div>
</div>
