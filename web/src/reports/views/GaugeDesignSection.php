<?php
    $gauge_section1_max = $this->data['gauge']['section1_max'] ? $this->data['gauge']['section1_max'] : '';
    $gauge_section2_max = $this->data['gauge']['section2_max'] ? $this->data['gauge']['section2_max'] : '';
    $gauge_section3_max = $this->data['gauge']['section3_max'] ? $this->data['gauge']['section3_max'] : '';
    $gauge_section1_colour = $this->data['gauge']['section1_colour'] ? $this->data['gauge']['section1_colour'] : '1df055';
    $gauge_section2_colour = $this->data['gauge']['section2_colour'] ? $this->data['gauge']['section2_colour'] : 'ffdf0f';
    $gauge_section3_colour = $this->data['gauge']['section3_colour'] ? $this->data['gauge']['section3_colour'] : 'ff0000';
?>
<div>
    <label for="gaugesection_1_minvalue" id="gaugesection_1_minvalue_label">Min. value</label>
    <input type="text" name="gauge_section1_min" id="gaugesection_1_minvalue" class="values" size="3" maxlength="10" value="0" readonly="readonly" disabled="disabled"/>
    <label for="section1max" id="section1max_label">Max. value</label>
    <input type="text" name="gauge_section1_max" id="section1max" class="values size="3" maxlength="10" value="<?php echo $gauge_section1_max; ?>" />
    <label for="section1colour" id="section1colour_label">Colour</label>
    <input type="text" name="gauge_section1_colour" id="section1colour" class="colour-picker" size="7" maxlength="7" value="<?php echo $gauge_section1_colour; ?>" style="background-color: #<?php echo $gauge_section1_colour; ?>;"/>
</div>
<div>
    <label for="gaugesection_2_minvalue" id="gaugesection_2_minvalue_label">Min. value</label>
    <input type="text" name="gauge_section2_min" id="gaugesection_2_minvalue" class="values size="3" maxlength="10" value="<?php echo $gauge_section1_max; ?>" readonly="readonly" disabled="disabled"/>
    <label for="section2max" id="section2max_label">Max. value</label>
    <input type="text" name="gauge_section2_max" id="section2max" class="values size="3" maxlength="10" value="<?php echo $gauge_section2_max; ?>" />
    <label for="section2colour" id="section2colour_label">Colour</label>
    <input type="text" name="gauge_section2_colour" id="section2colour" class="colour-picker" size="7" maxlength="7" value="<?php echo $gauge_section2_colour; ?>" style="background-color: #<?php echo $gauge_section2_colour; ?>;"/>
</div>
<div>
    <label for="gaugesection_3_minvalue" id="gaugesection_3_minvalue_label">Min. value</label>
    <input type="text" name="gauge_section3_min" id="gaugesection_3_minvalue" class="values size="3" maxlength="10" value="<?php echo $gauge_section2_max; ?>" readonly="readonly" disabled="disabled"/>
    <label for="section3max" id="section3max_label">Max. value</label>
    <input type="text" name="gauge_section3_max" id="section3max" class="values size="3" maxlength="10" value="<?php echo $gauge_section3_max; ?>" />
    <label for="section3colour" id="section3colour_label">Colour</label>
    <input type="text" name="gauge_section3_colour" id="section3colour" class="colour-picker" size="7" maxlength="7" value="<?php echo $gauge_section3_colour; ?>" style="background-color: #<?php echo $gauge_section3_colour; ?>;"/>
</div>