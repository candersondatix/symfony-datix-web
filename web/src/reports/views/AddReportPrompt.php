<div style="width:500px;text-align:left">
    <ol>
        <input type="hidden" name="new_rep_name" id="new_rep_name" value="<?=\Escape::EscapeEntities($this->name, ENT_QUOTES)?>"/>
        <?php
        if (isset($this->promptField))
        {
            echo GetDivFieldHTML(_tk('at_prompt_setting'), $this->promptField->getField());
        }
        ?>
        <?php if (isset($this->dashboardField)) : ?>
        <?php echo GetDivFieldHTML(_tk('add_to_this_dashboard'), $this->dashboardField->getField()); ?>
        <?php endif; ?>
    </ol>
</div>