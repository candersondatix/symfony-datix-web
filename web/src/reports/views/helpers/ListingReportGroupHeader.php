<?php

namespace src\reports\views\helpers;

/**
 * Represents a group header row of a listing report table.
 */
class ListingReportGroupHeader
{
    /**
     * The column title.
     * 
     * @var string
     */
    protected $title;
    
    /**
     * The column value that's contained within this group.
     * 
     * @var string
     */
    protected $value;
    
    /**
     * The amount of left hand indentation.
     * 
     * @var int
     */
    protected $indent;
    
    /**
     * The number of columns in the report.
     * 
     * @var int
     */
    protected $colspan;
    
    public function __construct($title, $value, $indent, $colspan)
    {
        $this->title   = $title;
        $this->value   = $value;
        $this->indent  = (int) $indent;
        $this->colspan = (int) $colspan;
    }
    
    /**
     * Getter for title.
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Getter for value.
     * 
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Getter for indent.
     * 
     * @return int
     */
    public function getIndent()
    {
        return $this->indent;
    }
    
    /**
     * Getter for colspan.
     * 
     * @return int
     */
    public function getColspan()
    {
        return $this->colspan;
    }
}