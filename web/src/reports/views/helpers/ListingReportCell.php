<?php

namespace src\reports\views\helpers;

/**
 * Represents a single cell of a listing report table.
 */
class ListingReportCell
{
    /**
     * @var string
     */
    protected $data;
    
    /**
     * @var string
     */
    protected $link;
    
    /**
     * @var string
     */
    protected $colour;
    
    /**
     * @var int
     */
    protected $rowSpan;
    
    /**
     * @var string
     */
    protected $align;
    
    public function __construct($data, $link = null, $colour = null, $align = 'left')
    {
        $this->data = $data;
        $this->link = $link;
        $this->colour = $colour;
        $this->align = $align;
        $this->rowSpan = 1;
    }
    
    /**
     * Getter for data.
     * 
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }
    
    /**
     * Getter for link.
     * 
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
    
    /**
     * Getter for colour.
     * 
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }
    
    /**
     * Getter for align.
     * 
     * @return string
     */
    public function getAlign()
    {
        return $this->align;
    }
    
    /**
     * Getter for row span.
     * 
     * @return int
     */
    public function getRowSpan()
    {
        return $this->rowSpan;
    }
    
    /**
     * Whether or not the cell has a hyperlink.
     * 
     * @return boolean
     */
    public function hasLink()
    {
        return $this->link != '';
    }
    
    /**
     * Whether or not the cell has a background colour.
     * 
     * @return boolean
     */
    public function hasColour()
    {
        return $this->colour != '';
    }
    
    /**
     * Increments to row span count for this cell.
     */
    public function incrementRowSpan()
    {
        $this->rowSpan++;
    }
}