<?php if (!empty($this->options)): ?>
<table style="margin:-1px 0px -1px 0px; display:block;" border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
    <tr style="display:block">
        <td class="windowbg" width="300px">
            <b><?php echo _tk('traffic_light_field_label') ?></b>
        </td>
        <td class="windowbg">
            <b><?php echo _tk('traffic_light_colour_label') ?></b>
        </td>
    </tr>
    <?php foreach ($this->options as $code => $option): ?>
    <tr id="trTrafficLine_<?php echo $code ?>" style="display:block">
        <td class="windowbg2" width="300px">
            <?php echo src\security\Escaper::escapeForHTML($option['description']) ?>
        </td>
        <td class="windowbg2">
            <input type="text" class="colour-picker" id="trafficline_code_colour_<?php echo $code ?>" name="trafficline_code_colour_<?php echo $code ?>" size="7" maxlength="7" value="<?php echo $option['colour'] ?>" style="background-color:#<?php echo $option['colour'] ?>;color:#<?php echo $option['colour'] ?>"/>
        </td>
    </tr>
    <?php endforeach ?>
</table>
<?php else: ?>
    <?php echo _tk('traffic_light_no_options') ?>
<?php endif ?>