var fusionChartId = fusionChartId || null;

/**
 * Stores the current row/column table values so they can be made available
 * to the server-side script which returns the row/column field codes.
 */
var tables = {};

/**
 * Fields defined here are used to toggle display of date options fields.
 */
var dateFields = {};

/**
 * Used to define the type of the report being rendered.
 */
var reportType = {};

/**
 * Used to list graph types that require two input field
 * @type {Array}
 */

var twoFieldsGraphs = [
    REPORT_CROSSTAB
];

/**
 * Used to list graph types that only require a single input field
 * @type {Array}
 */

var singleFieldGraphs = [
    REPORT_PIE,
    REPORT_PARETO,
    REPORT_SPC
];

/**
 * Used to list graph types that require no input field
 * @type {Array}
 */

var noFieldsGraphs = [
    REPORT_GAUGE,
    REPORT_LISTING,
    REPORT_TRAFFIC
];

var nonFusionCharts = [
    REPORT_LISTING,
    REPORT_CROSSTAB
];

/**
 * Used to list graph types that have settings in the report admin
 * @type {Array}
 */

var adminSettings = [
    REPORT_SPC,
    REPORT_LISTING
];

/**
 * List of fields which are reserve fields and can have intial level set for them
 * @type {string[]}
 */

if(typeof reserveFields === 'undefined') {

    var reserveFields = [];
}

/**
 * Regex for checking if charts is spc
 */

var spcRegex = /spc_/i;

/**
 * array of errors on the form
 * @type {Array}
 */

var fieldErrors = {};

/**
 * Used to check if resizing has finished or not
 * @type {number}
 */
var resizeCheck = 0;

var report_selector = null;

// stores value if context is report administration set in jQuery.ready()
var isAdmin = false,
    isDashboard = false,
    isCrosstab = isCrosstab ? true : false, // I think this is to convert any previously set numerical value to be true or false, not neccessary really, but clean I guess
    isListing = isListing ? true : false;

// used to store qTip objetcs
var qTips = {};

// FUNCTIONS

/**
 * Sets the current row/column table values.
 */
function setTableValues()
{
    tables["rows"] = jQuery("#rows_table").val();
    tables["columns"] = jQuery("#columns_table").val();
}

function getFields(){

    var fields = {},
        $form = jQuery('#designReport');

    $form.find('input, textarea, select').filter(function(){

        // filter to only check fields which have a name set
        return jQuery.type(jQuery(this).attr('name')) !== 'undefined';
    }).each(function(index, element){

        var field_name = jQuery(this).attr('name');

        fields[field_name] = jQuery(this).val();
    });

    return fields;
}

/**
 * Populates the dateFields object with all of the date fields for a given table.
 *
 * @param {string} table The table we're retrieving date codes for.
 */
function getDatefields(table)
{
    if (dateFields[table] === undefined)
    {
        if (table != '')
        {
            jQuery.get('app.php?action=getdatefields&table='+table+'&module='+jQuery('#module').val(), function (data) {
                dateFields[table] = data;
            }, "json");
        }
    }
}

/**
 * Shows/hides the date options field depending on whether the current selection is a date field.
 *
 * @param {string} field The current field selection
 * @param {string} axis  The axis the field has been selected for (rows/columns)
 */
function toggleDateOptions(field, axis)
{
    if(jQuery.inArray(field, dateFields[tables[axis]]) > -1) {

        showItems(["#"+axis+"_date_option_row"]);
    }
    else {

        hideItems(["#"+axis+"_date_option_row"]);
    }
}

// Used for preparing the chart for printing
function preparePrint()
{
    if(FusionCharts && (jQuery('#type').val() != REPORT_LISTING && jQuery('#type').val() != REPORT_CROSSTAB)) {
        FusionCharts.addEventListener("DrawComplete", function() {

            if(jQuery('#printButton').length > 0) {

                jQuery('#printButton').prop('disabled', false);
            }
        });
    }
    else {

        jQuery('#printButton').prop('disabled', false);
    }


    // WEBKIT SPECIFIC SECTION - CURRENTLY UNUSED AS NON IE BROWSERS NOT SUPPORTED
    /*FusionCharts.printManager.configure({
        invokeCSS: true
    });
    FusionCharts.printManager.enabled(true);

    FusionCharts.addEventListener (
        FusionChartsEvents.PrintReadyStateChange ,
        function (identifier, parameter) {
            if(parameter.ready){
                //alert("Chart is now ready for printing.");

                jQuery('#printButton').prop('disabled', false);
            }
    });*/
}

/**
 * Prints FusionChart report
 */
function printChart()
{
    /**
     * fusionChartId is a global variable sent from GraphicalReportWriter.php using the asset manager
     */
    FusionCharts(fusionChartId).print();
}

/**
 * Shows listed item selectors - includes small hack to ensure items in hidden parents still show
 * @param items array of item selectors to show
 * @param instantHide boolean sets if items hould be hidden instantly or animated
 * @param callback function to call after items have been shown
 */

function showItems(items, instantHide, callback)
{
    if(jQuery.type(instantHide) === 'undefined') {

        instantHide = false;
    }

    if(jQuery.type(callback) === 'undefined') {

        callback = null;
    }

    if(instantHide === true || (browser.isIE && parseInt(browser.version, 10) <= 8)) {

        duration = 0;
    }
    else {

        duration = 'normal';
    }

    for(var i = 0; i < items.length; i++)
    {
        var $elem = jQuery(items[i]);

        if( ! duration) {

            $elem.show();
        }
        else {

            $elem.slideDown(duration, function() {

                jQuery(this).show();
            });
        }
    }

    if(jQuery.type(callback) === 'function') {

        jQuery(items).promise().done(function() {

            callback();
        });
    }
}

/**
 * hides listed item selectors - includes small hack to ensure items in hidden parents still hide
 * @param items array of item selectors to hide
 * @param instantHide boolean sets if items should be hidden instantly or animated
 */
function hideItems(items, instantHide)
{
    if(jQuery.type(instantHide) === 'undefined') {

        instantHide = false;
    }

    if(instantHide === true || (browser.isIE && parseInt(browser.version, 10) <= 8)) {

        duration = 0;
    }
    else {

        duration = 'normal';
    }

    for(var i = 0; i < items.length; i++)
    {
        jQuery(items[i]).slideUp(duration, function(){

            jQuery(this).hide();

            // Hide qTips
            var $qtips = jQuery(this).find('.qtip-error');

            if($qtips.length > 0) {

                $qtips.qtip('hide');
            }

            removeErrors();
        });
    }
}

/**
 * clear listed item selectors so that values are not sent to processor
 * @param items array of item selectors to clear
 */
function clearItems(items) {

    for(var i = 0; i < items.length; i++)
    {
        jQuery(items[i]).val('');

        repositionQtips();
    }
}

/**
 * Shows/Hides fields according to the reportType.
 *
 * @param reportType The type of the report.
 */
function toggleRowsColumns(reportType) {

    // Hide Columns if reportType is in singleFieldGraphs/noFieldsGraph array or if reportType starts 'spc'
    if (jQuery.inArray(reportType, singleFieldGraphs) > -1 || jQuery.inArray(reportType, noFieldsGraphs) > -1 || spcRegex.test(reportType) || jQuery('#count_style_field_initial_levels').prop('checked')) {

        hideItems(['#columns']);
    }
    else
    {
        showItems(['#columns']);
    }

    toggleValueAt();

    // Hides Rows if reportType is in noFieldsGraph array
    if(jQuery.inArray(reportType, noFieldsGraphs) > -1) {

        hideItems(['#rows']);
    }
    else {

        showItems(['#rows']);
    }

    // Show/hide required field labels
    if(jQuery.inArray(reportType, twoFieldsGraphs) > -1) {

        var $target = jQuery('#columns').find('.form-item').not('#columns_date_option_row');

        $target.each(function() {

            if(jQuery(this).find('.mandatory_image').length == 0) {

                $label = jQuery(this).find('.codefield-label');

                $label.html('<img src="images/Warning.gif" alt="Mandatory" class="mandatory_image column-required" />' + $label.html());
            }
        });
    }
    else {

        jQuery('#columns').find('.column-required').remove();
    }

    // Show/hide traffic light options
    if(reportType == REPORT_TRAFFIC) {

        showItems(['#traffic-light-data', '#traffic-light-options']);
    }
    else
    {
        hideItems(['#traffic-light-data', '#traffic-light-options']);
    }

    // Show/hide listing options
    if(reportType == REPORT_LISTING) {

        showItems(['#listing-option']);
        hideItems(['#additional-options']);
    }
    else
    {
        showItems(['#additional-options']);
        hideItems(['#listing-option']);
    }

    // Show/hide listing options
    if(reportType == REPORT_SPC) {

        showItems(['#spc-option']);
    }
    else
    {
        hideItems(['#spc-option']);
    }

    // shows and hides bar/pie options
    if(reportType == REPORT_BAR) {

        showItems(['#bar-orient', '#bar-styles']);
        hideItems(['#pie-styles']);
    }
    else if(reportType == REPORT_PIE) {

        showItems(['#pie-styles']);
        hideItems(['#bar-styles', '#bar-orient']);
    }
    else {

        hideItems(['#pie-styles', '#bar-styles', '#bar-orient']);
    }

    // Show or hide other options depending on report type selected
    if(reportType == REPORT_GAUGE) {

        showItems(['#count-options']);
        hideItems(['#limit-option', '#percent-option', '#null-option']);
    }
    else if(reportType == REPORT_PARETO || spcRegex.test(reportType)) {

        showItems(['#limit-option', '#count-options', '#null-option']);
        hideItems(['#percent-option']);
        changeTitles('opt2');
    }
    else if(reportType == REPORT_TRAFFIC) {

        showItems(['#count-options', '#percent-option', '#null-option']);
        hideItems(['#limit-option']);
    }
    else if(reportType == REPORT_SPC) {
    	hideItems(['#percent-option']);
    }
    else {

        showItems(['#limit-option', '#count-options', '#percent-option', '#null-option']);
    }

    if(jQuery.inArray(reportType, singleFieldGraphs) != -1) {

        changeTitles('opt3');
    }
    else if(jQuery.inArray(reportType, [REPORT_BAR, REPORT_LINE]) != -1) {

        changeTitles('opt2');
    }
    else {

        changeTitles('opt1');
    }

    // If there are @prompt filters to show, wait for everything to finish being made visible, then add the message
    if(showPrompt) {

        addError(text.prompt, "atprompt_title_row");
    }
}

function changeTitles(option)
{
    jQuery('#rows').find('.section_title').html(text[option]['rows']);

    if(text[option]['columns']) {

        jQuery('#columns').find('.section_title').html(text[option]['columns']);
    }
}

function toggleSumAverage() {

    var value = jQuery('#count_style').val();

    if(value == '2')
    {
        showItems(['#count-style-sum']);
        hideItems(['#count-style-ave']);
    }
    else if(value == '3')
    {
        showItems(['#count-style-ave']);
        hideItems(['#count-style-sum']);
    }
    else
    {
        hideItems(['#count-style-sum', '#count-style-ave', '#count-style-value-at', '#count-style-calendar', 'count-style-initial-levels']);
    }

    toggleValueAt();
}

/**
 * Sets the type of the report.
 */
function setReportType() {

    reportType['type'] = parseInt(jQuery("#type").val(), 10);
}

/**
 * Used by traffic lights to get the report type field.
 *
 * @returns {string}
 */
function getReportFieldId() {

    if (jQuery("#type").length)
    {
        return 'type';
    }
    else
    {
        return 'rep_type_code';
    }
}

/**
 * Builds the fields and the colour used for traffic lights.
 */
function getTrafficLightCodedFieldsList() {

    var field       = jQuery('#traffic_field').val(),
        rep_id      = jQuery('#rep_id').length ? jQuery('#rep_id').val() : null,
        colours     = jQuery('#rep_colours').val(),
        report_type = parseInt(jQuery("#"+getReportFieldId()).val(), 10);

    if (field != '' && module != '' && ["traffic", "GW_TRAFFIC", REPORT_TRAFFIC].indexOf(report_type) > -1)
    {
        var data = {field: field, colours: colours};

        if (rep_id != null)
        {
            data = jQuery.extend(data, {rep_id: rep_id, screen: 'reportsadmin'});
        }

        jQuery("#traffic_fields_colour_row").slideUp(250, function()
        {
            jQuery('#traffic_fields_colour_row').load(scripturl + '?action=httprequest&type=getTrafficLightOptions', data, function(response, status, xhr)        
            {
                if (status == "error")
                {
                    alert("Error: unable to retrieve traffic light options");
                }
                else
                {
                    jQuery(this).slideDown();
                    // initialise the colour picker for use on the new HTML
                    initColorPicker();
                    return false;
                }
            });
        });
    }
}

/**
 * Show or hide gauge options if gauge selected
 */

function toggleGaugeOptions(type) {

    if(type == REPORT_GAUGE)
    {
        jQuery('#gauge-options').slideDown();
    }
    else
    {
        jQuery('#gauge-options').slideUp();
    }
}

/**
 * Checks the changes field to see if there have been any changes since report opened
 */
function hasChanges() {

    var output = false;

    jQuery("input[id^='CHANGED-']").each(function(){

        if(jQuery(this).val() == "1") {
            output = true;
        }
    });

    return output;
}

/**
 * Recreated as this version doesn't need to show an alert and accepts a value
 *
 * @param value - value from a date field
 * @returns {boolean}
 */
function isValidDate(value) {

    var day, month, year;
    // Only run this if value is set, otherwise just let it return true as we don't want this to fail if the date is blank as this is a valid option
    if(value != '') {

        // This will be the same for either dateFormat
        var datePat = /^(\d{1,2})(?:[\-/.])(\d{1,2})(?:[\-/.])(\d{2,4})$/;
        var matchArray = datePat.exec(value); // is the format ok?

        // If the date doesn't match the regex then it's not a correct date format
        if (matchArray == null) {

            return false;
        }

        // store the values for day month and year based on the order of the standard date formatting for the current dateFormat
        if(globals.dateFormatCode == 'US') {

            day = matchArray[2];
            month = matchArray[1];
            year = matchArray[3];
        }
        else {

            day = matchArray[1];
            month = matchArray[2];
            year = matchArray[3];
        }

        // Check that the year is 4 digits long
        if (year.length != 4)
        {
            return false;
        }

        // check the month is between 1 and 12
        if (month < 1 || month > 12) { // check month range
            return false;
        }

        // check the day is between 1 and 31
        if (day < 1 || day > 31)
        {
            return false;
        }

        // check that the day isn't 31 for months with only 30 days
        if ((month==4 || month==6 || month==9 || month==11) && day==31)
        {
            return false;
        }

        // check that the day is either 28 or 29 depending on if it's a leap year
        if (month == 2)
        {
            // check for february 29th
            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));

            if (day > 29 || (day==29 && !isleap))
            {
                return false;
            }
        }
    }

    return true; // date is valid
}

function isFutureDate(value) {

    var day, month, year;

    // Only run this if value is set, otherwise just let it return true as we don't want this to fail if the date is blank as this is a valid option
    if(value != '') {

        // This will be the same for either dateFormat
        var datePat = /^(\d{1,2})(?:[\-/.])(\d{1,2})(?:[\-/.])(\d{2,4})$/;
        var matchArray = datePat.exec(value); // is the format ok?

        // If the date doesn't match the regex then it's not a correct date format
        if (matchArray == null) {

            return false;
        }

        // store the values for day month and year based on the order of the standard date formatting for the current dateFormat
        if(FMT_DATE_WEB == 'US') {

            day = matchArray[2];
            month = matchArray[1];
            year = matchArray[3];
        }
        else {

            day = matchArray[1];
            month = matchArray[2];
            year = matchArray[3];
        }

        var today = new Date(),
            selected = new Date(year, month - 1, day);

        /**
         * Check if the date is
         */
        return (selected.getFullYear() > today.getFullYear() || (selected.getFullYear() == today.getFullYear() && (selected.getMonth() > today.getMonth() || (selected.getMonth() == today.getMonth() && selected.getDate() > today.getDate()))));
    }

    return false;
}

/**
 *
 * @param $target - jQuery object of element to use for validation
 * @returns {boolean} - returns validation output, true for pass, flase for fail
 */
function validateFieldTable($target) {

    var fields = {},
        validation = {};

    var tableInputName = $target.attr('id').replace(/^([^_]+).*$/i, "$1_table");

    fields[tableInputName] = jQuery('#' + tableInputName).val();
    validation[tableInputName] = {
        expect : "!empty",
        description : "Please select " + text['form'] + " first"
    };

    return validateFields(fields, validation);
}

/**
 *
 * @param fields - object of fields being submitted
 * @param validations - object of validations to run for each field in fields, must match fields object naming
 * @returns {boolean}
 */
function validateFields(fields, validations) {

    if(jQuery.type(fields) === 'undefined') {

        fields = null;
    }

    if(jQuery.type(validations) === 'undefined') {

        validations = null;
    }

    if(fields !== null || jQuery.isEmptyObject(fields)) {

        // reset fieldErrors
        fieldErrors = {};

        if(fields['type']) {

            fields['type'] = parseInt(fields['type'], 10);

            // check query field is filled if not an @prompt from My reports
            if ( ! jQuery("#atprompt-options").is(":visible")) {

                validate('!empty', 'query', fields);
            }
            else {

                jQuery('input.date').each(function(){

                    var elem = jQuery(this);

                    validate('isDate', elem.attr('name'), fields);
                });
            }

            validate(/^[0-9]*$/, 'limit_to_top', fields);

            switch(fields['type']) {

                case REPORT_BAR:
                case REPORT_LINE:
                case REPORT_PIE:
                case REPORT_PARETO:
                    validate('!empty', ['rows_table', 'rows_field'], fields);
                    break;
                case REPORT_SPC:
                    validate('!empty', ['spc_type', 'rows_table', 'rows_field'], fields);
                    break;
                case REPORT_CROSSTAB:

                    if (jQuery('#count_style_field_initial_levels').is(':checked') && (jQuery.inArray(fields['count_style_field_sum'], reserveFields) > -1 || jQuery.inArray(fields['count_style_field_average'], reserveFields) > -1)) {

                        validate('!empty', ['rows_table', 'rows_field'], fields);
                    }
                    else {

                        validate('!empty', ['rows_table', 'rows_field', 'columns_table', 'columns_field'], fields);
                    }
                    break;
                case REPORT_GAUGE:
                    validate(['!empty', /^[0-9]+$/], ['gauge_section1_max', 'gauge_section2_max', 'gauge_section3_max'], fields);
                    // Check max values are greater than min values
                    if(parseInt(jQuery('#section2max').val(), 10) < parseInt(jQuery('#section1max').val(), 10)){

                        addError('GT', 'gauge_section2_max');
                    }
                    if(parseInt(jQuery('#section3max').val(), 10) < parseInt(jQuery('#section2max').val(), 10)){

                        addError('GT', 'gauge_section3_max');
                    }
                    break;
                case REPORT_LISTING:
                    validate('!empty', 'base_listing_report', fields);
                    break;
                case REPORT_TRAFFIC:
                    validate('!empty', ['traffic_field'], fields);
            }

            setTableValues();

            var dateValidationTypes = [REPORT_BAR, REPORT_LINE, REPORT_PIE, REPORT_PARETO, REPORT_CROSSTAB, REPORT_SPC];

            if(jQuery.inArray(fields['type'], dateValidationTypes) != -1) {

                getDatefields(fields['rows_table']);

                if(jQuery.inArray(fields['rows_field'], dateFields[fields['rows_table']]) != -1) {

                    validate('!empty', 'rows_date_option', fields);
                }

                if(fields['columns_table'] !== 'undefined') {
                    getDatefields(fields['columns_table']);

                    if(jQuery.inArray(fields['columns_field'], dateFields[tables['columns']]) != -1) {

                        validate('!empty', 'columns_date_option', fields);
                    }
                }
            }

            if(fields['count_style'] == 2){

                validate('!empty', ['count_style_field_sum'], fields);
            }
            else if(fields['count_style'] == 3){

                validate('!empty', ['count_style_field_average'], fields);
            }

            if(fields['count_style_field_value_at'] == 2) {

                validate(['!empty', 'isDate', 'notFuture'], ['count_style_field_calendar'], fields);
            }
        }
        else if(validations !== null) {

            for(var prop in fields) {

                // important check that this is objects own property
                // not from prototype prop inherited
                if(fields.hasOwnProperty(prop)){

                    validate(validations[prop]['expect'], prop, fields, validations[prop]['description']);
                }
            }
        }

        if( ! jQuery.isEmptyObject(fieldErrors)){

            showErrors();
            return false;
        }
        else{

            removeErrors();
            return true; // SET TO TRUE WHEN READY TO CONTINUE
        }
    }
    else {

        return true
    }
}

/**
 * validate function to check
 * @param expect - string or array of what value should be. can be a string or regexp eg ([/^[a-z]+$/], '!empty') - put the most important check last
 * @param check - string or array of field names to check, should make fieldnames from form
 * @param fields - object containing fields with format field name: field value
 * @param customErrorDesc - a custom description that can be used to override the default ones
 * @returns {*}
 */
function validate(expect, check, fields, customErrorDesc) {

    if(jQuery.type(customErrorDesc) === 'undefined') {

        customErrorDesc = null;
    }

    if(jQuery.type(expect) === 'string' || jQuery.type(expect) === 'regexp') {

        expect = new Array(expect);
    }

    if(jQuery.type(check) === 'string') {

        check = new Array(check);
    }

    for(var x=0; x < expect.length; x++) {

        // set variable for current value to expect
        var expected = expect[x];
        for(var i=0; i < check.length; i++) {

            //set variables for current field to be checked
            var fieldName = check[i];

            if(jQuery.type(fields[fieldName]) !== 'undefined') {

                var fieldValue = fields[fieldName];

                if(jQuery.type(expected) === 'regexp') {

                    if(! expected.test(fieldValue)) {
                        addError(customErrorDesc ? customErrorDesc : expected, fieldName);
                    }
                }
                else {

                    switch(expected) {
                        case '!empty':
                            if(jQuery.type(fieldValue) === 'undefined' || fieldValue.length == 0) {

                                addError(customErrorDesc ? customErrorDesc : expected, fieldName);
                            }
                        break;
                        case 'isDate':
                            if( ! isValidDate(fieldValue)) {

                                addError(customErrorDesc ? customErrorDesc : expected, fieldName);
                            }
                            break;
                        case 'notFuture':
                            if( isFutureDate(fieldValue)) {

                                addError(customErrorDesc ? customErrorDesc : expected, fieldName);
                            }
                            break;
                    }
                }
            }
        }
    }
}

/**
 * Checks field doesn't already exist in fieldErrors array and adds if not
 * @param fieldName
 */

function addError(error, fieldName) {

    if(jQuery.isEmptyObject(fieldErrors[error])) {

        fieldErrors[error] = [];
    }

    if(jQuery.inArray(fieldName, fieldErrors.error) < 0) {
        var length = fieldErrors[error].push(fieldName);
    }
}

function showErrors() {

    var errorDesc = '';

    removeErrors();

    for(var x in fieldErrors) {

        for(var i=0; i < fieldErrors[x].length; i++) {

            var fieldName = fieldErrors[x][i];

            if(x == '!empty') {

                errorDesc = "this field is required";
            }
            else if(x == 'isDate') {

                errorDesc = "entered value is not a valid date";
            }
            else if(/\^\[0\-9\](\+|\*)\$/.test(x)) {

               errorDesc = "this field must be numeric";
            }
            else if(x == 'GT') {

                errorDesc = "max value must be greater than min value";
            }
            else if(x == 'notFuture') {

                errorDesc = "the date can not be in the future";
            }
            else {

                errorDesc = x;
            }

            var $input = jQuery("input[name='" + fieldName + "']");

            if($input.attr('type') == 'hidden') {

                var $target = $input.closest('.form-item').find('input:not(:hidden)').filter(':last');
            }
            else if (fieldName == "atprompt_title_row")
            {
                var $target = jQuery("div[name='" + fieldName + "']");
            }
            else {

                var $target = $input;
            }

            $target.addClass('qtip-error').attr('title', errorDesc);
        }
    }

    $qtip_error = jQuery('.qtip-error');

    qTips.errors = [];

    $qtip_error.each(function(){

        $target = jQuery(this);

        if(($target.position().top + $target.height()) > jQuery('#viewport').height() ||  ! $target.is(':visible')) {

            var $positionTarget = $target.closest('.collapse').find('.section_title_row');
        }
        else {

            var $positionTarget = $target;
        }

        if($target.hasClass('codefield')) {

            var positionOffset = 25;
        }
        else if($target.hasClass('hasDatepicker')) {

            var positionOffset = 20;
        }
        else
        {
            var positionOffset = 5;
        }

        if ($target.attr('id') === 'atprompt_title_row') {

            var qTipClass = 'qtip-blue';
        }
        else {

            var qTipClass = 'qtip-red';
        }

        qTipID = $target.qtip({
            show: true,
            hide: false,
            overwrite: false,
            style: {
                classes: qTipClass
            },
            position: {
                my: 'left center',
                at: 'right center',
                target: $positionTarget,
                adjust: {
                    x: positionOffset,
                    scroll: false,
                    resize: false
                }
            }
        });

        qTips.errors.push(qTipID.qtip('api'));
    });

    repositionQtips();
}

function removeErrors() {

    var qtip_errors = jQuery('.qtip-error');

    if(qtip_errors.length > 0) {

        qtip_errors.qtip('destroy', true);
        qtip_errors.removeClass('qtip-error')
    }
}

function repositionQtips($target) {

    if(jQuery.type($target) === 'undefined') {

        $target = [];
        $target[0] = null
    }

    $qtippedFields = jQuery('.qtip-error');

    if($qtippedFields.length > 0) {

        $qtippedFields.each(function() {

            $qtippedField = jQuery(this);

            if($qtippedField.hasClass('codefield') || $qtippedField.hasClass('hasDatepicker') ) {

                var positionOffset = 25;
            }
            else
            {
                var positionOffset = 5;
            }

            if($qtippedField.is(':visible')) {

                var $parentContainer = $qtippedField.closest('.collapse').find('.section_title_row');

                // Set the position of the tooltip to the top of the viewport if the field is offscreen
                if($qtippedField.position().top < 0)
                {
                    $qtippedField.qtip('show').qtip('set', {
                        'position.target': jQuery('#viewport'),
                        'position.my': 'topLeft',
                        'position.at': 'topRight',
                        'position.adjust.x' : 5,
                        'content.text' : $qtippedField.attr('id') === 'atprompt_title_row' ? $qtippedField.attr('oldtitle') : 'there are errors above'
                    }).qtip('reposition');
                }
                // Set the position of the tooltip to the bottom of the viewport if the field is offscreen
                else if(($qtippedField.position().top + $qtippedField.outerHeight()) > jQuery('#viewport').height())
                {
                    $qtippedField.qtip('show').qtip('set', {
                        'position.target': jQuery('#viewport'),
                        'position.my': 'bottomLeft',
                        'position.at': 'bottomRight',
                        'position.adjust.x' : 5,
                        'content.text' : $qtippedField.attr('id') === 'atprompt_title_row' ? $qtippedField.attr('oldtitle') : 'there are errors below'
                    }).qtip('reposition');
                }
                // position the tooltip at the section title if section collapsed
                else if($target && $qtippedField.closest('.collapse')[0] === $target[0]) {

                    $qtippedField.qtip('show').qtip('set', {
                        'position.target': $qtippedField.closest('.collapse').find('.section_title_row'),
                        'position.my': 'left center',
                        'position.at': 'right center',
                        'position.adjust.x' : 5,
                        'content.text' : 'the section contains errors'
                    }).qtip('reposition');
                }
                // position the tooltip beside the element
                else {

                    $qtippedField.qtip('set', {
                        'position.target': $qtippedField,
                        'position.my': 'left center',
                        'position.at': 'right center',
                        'position.adjust.x': positionOffset,
                        'content.text' : $qtippedField.attr('oldtitle')
                    }).qtip('reposition').qtip('show');
                }
            }
            else
            {
                var $target = $qtippedField.closest('.collapse').find('.section_title_row');

                if($target.position().top < 0)
                {
                    $qtippedField.qtip('show').qtip('set', {
                        'position.target': jQuery('#viewport'),
                        'position.my': 'topLeft',
                        'position.at': 'topRight',
                        'position.adjust.x' : 5,
                        'content.text' : 'there are errors above'
                    }).qtip('reposition');
                }
                // Set the position of the tooltip to the bottom of the viewport if the field is offscreen
                else if(($target.position().top + $target.height()) > jQuery('#viewport').height())
                {
                    $qtippedField.qtip('show').qtip('set', {
                        'position.target': jQuery('#viewport'),
                        'position.my': 'bottomLeft',
                        'position.at': 'bottomRight',
                        'position.adjust.x' : 5,
                        'content.text' : 'there are errors below'
                    }).qtip('reposition');
                }
                else {

                    $qtippedField.qtip('show').qtip('set', {
                        'position.target': $target,
                        'position.my': 'left center',
                        'position.at': 'right center',
                        'position.adjust.x' : 5,
                        'content.text' : 'the section contains errors'
                    }).qtip('reposition');
                }
            }
        });
    }
}

/**
 * Clear unrequired fields based on report type
 * @param fields - object of fields being submitted
 */

function clearFields(fields) {

    var type = fields['type'];
    var count_style = fields['count_style'];

    if(spcRegex.test(type)){

        clearItems(['#columns_field', '#columns_date_option', 'input:radio[\'name="orientation"\']', 'input:radio[\'name="style"\']', '#percentages']);
    }

    if(type != REPORT_TRAFFIC) {

        clearItems(['#traffic_field', '#traffic_date_option']);
    }

    if(type != REPORT_GAUGE) {

        clearItems(['section1max', 'section2max', 'section3max', 'gaugesection_1_minvalue', 'gaugesection_2_minvalue', 'gaugesection_3_minvalue', 'section1colour', 'section2colour', 'section3colour']);
    }

    switch(type) {

        case REPORT_CROSSTAB:
            clearItems(['input:radio[name="orientation"]', 'input:radio[name="style"]']);
            break;
        case REPORT_GAUGE:
            clearItems(['#rows_table', '#rows_field', '#rows_date_option', '#columns_table', '#columns_field', '#columns_date_option', 'input:radio[name="orientation"]', 'input:radio[name="style"]', '#limit_to_top', '#percentages', '#null_values']);
            break;
        case REPORT_LINE:
            clearItems(['input:radio[name="orientation"]', 'input:radio[name="style"]']);
            break;
        case REPORT_LISTING:
            clearItems(['#rows_table', '#rows_field', '#rows_date_option', '#columns_table', '#columns_field', '#columns_date_option', 'input:radio[name="orientation"]', 'input:radio[name="style"]', '#limit_to_top', '#count_style', '#count_style_field_sum', '#count_style_field_average', '#count_style_field_value_at', '#count_style_field_initial_levels', '#percentages', '#null_values']);
            break;
        case REPORT_PARETO:
            clearItems(['#columns_table', '#columns_field', '#columns_date_option', 'input:radio[name="orientation"]', 'input:radio[name="style"]', '#percentages']);
            break;
        case REPORT_PIE:
            clearItems(['#columns_table', '#columns_field', '#columns_date_option', 'input:radio[name="orientation"]']);
            break;
        case REPORT_TRAFFIC:
            clearItems(['#columns_field', '#columns_date_option', 'input:radio[name="orientation"]', 'input:radio[name="style"]', '#limit_to_top']);
            break;
    }

    switch(count_style) {

        case '2':
            clearItems(['#count_style_field_average']);
            break;
        case '3':
            clearItems(['#count_style_field_sum']);
            break;
        default:
            clearItems(['#count_style_field_average', '#count_style_field_sum', '#count_style_field_value_at', '#count_style_field_initial_levels']);
            break;
    }

    getDatefields(fields['rows_table']);

    if(jQuery.inArray(fields['rows_field'], dateFields[fields['rows_table']]) == -1) {

        clearItems(['#rows_date_option']);
    }

    getDatefields(fields['columns_table']);

    if(jQuery.inArray(fields['columns_field'], dateFields[fields['columns_table']]) == -1) {

        clearItems(['#columns_date_option']);
    }
}


function setReportWidthToAvailableSpace()
{
    if( ! isDashboard && ! isAdmin) {

        var content_width = jQuery('.content').width(),
            report_width = content_width - jQuery('#designer-container').outerWidth(true);

        // ADD 25px padding for Crosstab and listing reports so the designer close button doesn't overlay the report
        if(isCrosstab || isListing) {

            report_width = report_width - 25;
        }

        jQuery('#report-container').width(report_width);
        jQuery('#chartContainer').width(report_width);
        jQuery("[id^='myFusionChartReport']").width(report_width).find('div').filter(':first').width(report_width);
    }
}

function setReportViewportHeightToViewportHeight()
{
    var $reportViewport = jQuery('.report-viewport');

    if( ! isDashboard && ! isAdmin && isListing && $reportViewport.length) {

        var $reportButtons = jQuery('.button_wrapper'),
            windowHeight = jQuery(window).height(),
            availableHeight = windowHeight - $reportViewport.offset().top,
            viewportHeight = availableHeight - $reportButtons.outerHeight(true);


        $reportViewport.height(viewportHeight);
    }
}


/**
 * Sets the width of the FusionChart so that it shows in the space available and isn't covered by the Report Designer
 */
function setFusionChartWidthToAvailableSpace() {

    if( ! isDashboard && ! isAdmin) {

        /**
         * fusionChartId is a global variable sent from GraphicalReportWriter.php using the asset manager
         */

        var content_width = jQuery('.content').width(),
            report_width = content_width - jQuery('#designer-container').outerWidth(true),
            report_height = jQuery('#chartContainer').height(),
            $fusionChart = jQuery('#' + fusionChartId);

        if($fusionChart.length > 0) {

            var myChart = FusionCharts(fusionChartId);
            myChart.width = report_width;
            myChart.height = report_height;
            myChart.resizeTo(report_width, report_height);
        }
    }
}

/**
 * Sets the size of the content to the height of the report of report designer, whichever is largest. This is to ensure
 * the footer doesn't overlay the designer window, which is positioned aboslutely, if the report height is smaller.
 */
function setContentHeightToDesignerHeight()
{
    if( ! isDashboard && ! isAdmin) {
        var $designer = jQuery('#designer-container'),
            $reporter = jQuery('#report-container');

        $reporter.height('');

        var designer_height = $designer.outerHeight(true),
            report_height = $reporter.outerHeight(true);

        jQuery('#content-container').height(report_height > designer_height ? report_height : designer_height);

        if(report_height < designer_height){

            $reporter.height($designer.innerHeight());
        }
    }
}

function resetForm()
{
    if(confirm(text['clear_confirm'])) {

        // Clear all fields or reset to default values
        jQuery('#title').val('');
        jQuery('#query_title').clearField();
        jQuery('#rows_table_title').clearField();
        jQuery('#rows_field_title').clearField();
        jQuery('#rows_date_option_title').clearField();
        jQuery('#columns_table_title').clearField();
        jQuery('#columns_field_title').clearField();
        jQuery('#columns_date_option_title').clearField();
        jQuery('#traffic_field_title').clearField();
        jQuery('#rep_colours').val(''); // clears the stored colours for traffic lights
        jQuery('#spc_type_title').clearField();
        jQuery('#base_listing_report_title').clearField();
        jQuery('#traffic-options-content').html('<li id="traffic_fields_colour_row" class="field_div">Please select a field from above</li>');
        jQuery('#section1max').val('');
        jQuery('#gaugesection_2_minvalue').val('');
        jQuery('#section2max').val('');
        jQuery('#gaugesection_3_minvalue').val('');
        jQuery('#section3max').val('');
        jQuery('#section1colour').val('1df055').css({ backgroundColor: '#1df055', color: '#1df055'});
        jQuery('#section2colour').val('ffdf0f').css({ backgroundColor: '#ffdf0f', color: '#ffdf0f'});
        jQuery('#section3colour').val('ff0000').css({ backgroundColor: '#ff0000', color: '#ff0000'});
        jQuery('#limit_to_top').val('');
        jQuery('#null_values').prop('checked', false);
        jQuery('#count_style_field_sum_title').clearField();
        jQuery('#count_style_field_average_title').clearField();
        jQuery('#count_style_field_value_at_title').clearField();
        jQuery('#count_style_field_initial_levels').prop('checked', false);

        var countStyleList = jQuery('#count_style_title').getList(),
            percentageList = jQuery('#percentages_title').getList(),
            countValueAtList = jQuery('#count_style_field_value_at_title').getList();

        // TODO - this could be done by attaching the defualt value as data and then just triggering that for applicable fields
        var defaultCountStyle = countStyleList ? countStyleList.find('li').first() : null;
        var defaultPercentage = percentageList ? percentageList.find('li').first() : null;
        var defaultCountValueAt = countValueAtList ? countValueAtList.find('li').first() : null;

        if(defaultCountStyle) { jQuery('#count_style_title').selectItem(defaultCountStyle);}
        if(defaultPercentage) { jQuery('#percentages_title').selectItem(defaultPercentage);}
        if(defaultCountValueAt) { jQuery('#count_style_field_value_at_title').selectItem(defaultCountValueAt);}

        // hide any extra fields that might be open
        hideItems(['#rows_date_option_row', '#columns_date_option_row', '#count-style-sum', '#count-style-ave', '#count-style-value-at', '#count-style-calendar', '#count-style-initial-levels']);

        // reset the submit button and report frame
        jQuery('#run').html(text['report_button']['run']);
        jQuery('#report-container').html('<span id="report-watermark">No report to display</span>');

        // select the default chart type
        jQuery('#type-default').click();

        setDesignerScrollPositionToStoredValue();
    }
}

/**
 * Set the initial option values/view of the designer form.
 */
function setOptions()
{
    getDatefields(jQuery('#rows_table').val());
    getDatefields(jQuery('#columns_table').val());

    setTableValues();

    jQuery('#rows_field').change();
    jQuery('#columns_field').change();
    jQuery('#traffic_field').change();
    jQuery('#count_style').change();
    jQuery('#base_listing_report').change();

    setReportType();

    /**
     * Trigger the report-type to change so that fires off associated functions to setup the report designer for the specific report type selected
     */
    jQuery('#report-type').find('.item').filter('[data-value="' + reportType['type'] + '"]').click();

    // initiate traffic light colour selector
    getTrafficLightCodedFieldsList();
}

/**
 * Initialise the colorpicker control.
 */
function initColorPicker()
{
    if(globals.deviceDetect.isTablet) {

        jQuery('.colour-picker').prop('disabled', true);
    }
    else {

        jQuery('.colour-picker').ColorPicker({
            flat: false,
            onSubmit: function(hsb, hex, rgb, el)
            {
                jQuery(el).val(hex);
                jQuery(el).ColorPickerHide();
                jQuery(el).css({backgroundColor : '#'+hex, color : '#'+hex});
            },
            onBeforeShow: function ()
            {
                jQuery(this).ColorPickerSetColor(this.value);
            }
            })
            .bind('keyup', function()
            {
                jQuery(this).ColorPickerSetColor(this.value);
        });
    }
}

// NOT YET USABLE
function positionEditor() {

    // Selector Cache
    var $window = jQuery(this),
    $designerContainer = jQuery('#designer-container'),
    $reportContainer = jQuery('#report-container'),
    $footer = jQuery('#F'),
    $viewport = jQuery('#viewport'),
    $formActions = jQuery('#form-actions'),
    $viewport_container = jQuery('#viewport-container');

    // Variables
    var designerChromeHeight = $formActions.outerHeight(true) + $designerContainer.find('h2').outerHeight(true),
    maxReportSize = $reportContainer.width() + $reportContainer.offset().top + $footer.outerHeight(true);

    console.log(maxReportSize > $window.height(), ($window.scrollTop() + $window.height() > $footer.offset().top), ($designerContainer.height() > $window.height() - (($window.scrollTop() + $window.height()) - ($reportContainer.height() + $reportContainer.offset().top))));

    if($reportContainer.height() < ($window.height() - $reportContainer.offset().top)) {
    console.log('rC<wh');
        $designerContainer.css({

            position: 'relative',
            top: '',
            bottom: ''
        });

        $reportContainer.css({

            marginLeft: ''
        });

        $viewport.height($reportContainer.height() - designerChromeHeight);
    }
    // Fixed to bottom
    else if(maxReportSize > $window.height() && ($window.scrollTop() + $window.height() > $footer.offset().top && $designerContainer.height() > $window.height() - (($window.scrollTop() + $window.height()) - ($reportContainer.height() + $reportContainer.offset().top)))) {
console.log('footer');
        $designerContainer.css({

            position: 'fixed',
            top: '',
            bottom: ($window.scrollTop() + $window.height()) - ($reportContainer.height() + $reportContainer.offset().top)
        });

        $reportContainer.css({

            marginLeft: $designerContainer.width()
        });

        $viewport.height($window.height() - (designerChromeHeight + ($window.scrollTop() + $window.height()) - ($reportContainer.height() + $reportContainer.offset().top)));
    }
    // Fixed to top
    else if($window.scrollTop() > $reportContainer.offset().top) {
console.log('snap top');
        $designerContainer.css({

            position: 'fixed',
            top: 0,
            bottom: ''
        });

        if( ! $designerContainer.data('originalPosition')) {

            $designerContainer.data('originalPosition', $reportContainer.offset().top);
        }

        $reportContainer.css({

            marginLeft: $designerContainer.width()
        });

        $viewport.height($window.height() - designerChromeHeight);
    }
    // Normal Position
    else if(! $designerContainer.data('originalPosition') || $window.scrollTop() < $designerContainer.data('originalPosition')) {
console.log('default');
        $designerContainer.css({

            position: 'relative',
            top: '',
            bottom: ''
        }).data('originalPosition', '');

        $reportContainer.css({

            marginLeft: 0
        });

        $viewport.height($window.height() + $window.scrollTop() - (designerChromeHeight + $designerContainer.offset().top));
    }


    var content_height = $viewport_container.height(),
    viewport_height = $viewport.height(),
    scrollMax = ($viewport.get(0).scrollHeight - Math.ceil(viewport_height)) - 5, // give 5px leeway in case calculation doesn't quite work out
    scrollTop = $viewport.scrollTop(),
    scrollbarWidth = $viewport.width() - $viewport_container.outerWidth(true);

    if(content_height > viewport_height)
    {
        if(scrollTop < scrollMax) {

            $viewport.css({'overflow':'auto'}).width(450 + scrollbarWidth);
        }
    }
    else
    {
        $viewport.css({'overflow':'hidden'}).width(450);
    }
}

/**
 * sets the width of the designer to account for scrollbars if the content of the designer exceeds the available height
 */
function setDesignerWidthForScrollbars() {

    if( ! isAdmin) {

        var $designerWrapper = jQuery('#designer-wrapper'),
            $viewport_container = jQuery('#viewport-container'),
            $viewport = jQuery('#viewport'),
            $heading = $designerWrapper.find('.heading'),
            viewportWidth = 450;

        var content_height = $viewport_container.height(),
            viewport_height = $viewport.height(),
            scrollbarWidth = jQuery.scrollbarWidth();

        if(content_height > viewport_height)
        {
            var designerWidth = viewportWidth + scrollbarWidth,
                viewportOverflow = 'auto';
        }
        else
        {
            var designerWidth = viewportWidth,
                viewportOverflow = 'hidden';
        }

        $viewport.width(designerWidth).css({'overflow': viewportOverflow});

        var headerSidePadding = parseInt($heading.css("paddingLeft"), 10) + parseInt($heading.css("paddingRight"), 10);
        $heading.width(designerWidth - headerSidePadding);

        // If the widht of the designer has changed, the report size will also need to change
        setReportWidthToAvailableSpace();

        // Store the designer width to the element so that it can be used for sizing when collapsing the designer
        if($designerWrapper.data('width') === undefined || $designerWrapper.outerWidth(true) > 0) {

            $designerWrapper.data('width', $designerWrapper.outerWidth(true));
        }
    }
}

/**
 * Sets the height of the designer to either the viewport height if it's smaller than the max allowed height for the
 * Report designer, or to the max height
 */
function setDesignerHeightToViewportHeight() {

    if( ! isAdmin && ! isDashboard) {

        var $designer = jQuery('#designer-wrapper'),
            $viewport = jQuery('#viewport'),
            browserHeight = jQuery(window).height(),
            designerChromeHeight = $designer.find('#form-actions').outerHeight(true) + $designer.find('.heading').outerHeight(true);

        $viewport.height('');

        if($designer.outerHeight(true) +  $designer.offset().top > browserHeight) {

            if(browserHeight - designerChromeHeight - $designer.offset().top > 460)
            {
                var viewportHeight = browserHeight - designerChromeHeight - $designer.offset().top
            }
            else
            {
                var viewportHeight = 460;
            }

            $viewport.height(viewportHeight);
        }
    }
}

function setDesignerScrollPositionToStoredValue()
{
    if( ! isAdmin)
    {
        var $viewport = jQuery('#viewport'),
            $scrollPos = jQuery('#scroll-pos');

        if($viewport.scrollTop() !== parseInt($scrollPos.val(), 10))
        {
            $viewport.scrollTop($scrollPos.val());
        }
    }
}

function initialiseDesigner(fromDashboard) {

    isDashboard = fromDashboard || isDashboard;

    var $designer = jQuery('#designer-wrapper');

    setOptions();
    setDesignerWidthForScrollbars();
    setDesignerHeightToViewportHeight();
    setDesignerScrollPositionToStoredValue();
    preparePrint();
    initColorPicker();

    showErrors();

    toggleOpenInExcelButton();

    if($designer.is(':visible')){

        setContentHeightToDesignerHeight();
    }
}

/**
 * Gets the width of the scrollbar for the browser being used so this value can be used in width calculations
 * @returns {*}
 */
jQuery.scrollbarWidth = function() {
    var parent, child, width;

    if(width===undefined) {
        parent = jQuery('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');
        child=parent.children();
        width=child.innerWidth()-child.height(99).innerWidth();
        parent.remove();
    }

    return width;
};

// JQUERY READY

jQuery(function() {

    var $designer = jQuery('#designer-wrapper');

    /**
     * Set global variables
     */
    isAdmin = $designer.hasClass('administration');
    isDashboard = $designer.hasClass('dashboard');

    /**
     * Initialise the datepicker for the expenses field so it can control not selecting future dates
     */
    jQuery('#count_style_field_calendar').datepicker({
        showOn: "button",
        buttonImage: "Images/calendar.gif",
        buttonImageOnly: true,
        buttonText: 'Select date',
        dateFormat: globals.dateFormat,
        firstDay: globals.weekStartDay - 1,
        showOtherMonths: true,
        selectOtherMonths: true,
        showButtonPanel: true,
        constrainInput: false,
        changeMonth: true,
        changeYear: true,
        maxDate: 0,
        beforeShowDay: function(date) {

            // As we were utilising a bug in jQUery UI to set weekends before, we need to convert it to the proper]
            // date number that we want to use
            weekendDays = [(parseInt(globals.calendarWeekend) + 5) % 7, (parseInt(globals.calendarWeekend) + 6) % 7];

            var weekend = false;

            if(jQuery.inArray(date.getDay(), weekendDays) > -1) {

                weekend = true;
            }
            return [true, weekend ? 'highlight-weekend' : ''];
        }
    });

    setReportWidthToAvailableSpace();
    setReportViewportHeightToViewportHeight();

    //set options when page has loaded
    jQuery(window).on('load', function()
    {
        initialiseDesigner();
    });

    if( ! globals.deviceDetect.isTablet) {

        qTips.tooltips = [];

        jQuery('.tooltip').each(function() {

            $target = jQuery(this);

            if($target.closest('.options').length > 0 || $target.hasClass('right')){

                var my = 'left center';
                var at = 'right center';
            }
            else {

                var my = 'bottom center';
                var at = 'top center';
            }

            qTipID = $target.qtip({
                overwrite: false,
                style: {
                    classes: 'qtip-dark'
                },
                position: {
                    my: my,
                    at: at,
                    target: $target,
                    adjust: {
                        scroll: false,
                        resize: false
                    }
                }
            });

            qTips.tooltips.push(qTipID.qtip('api'));
        });

        qTips.help = [];

        jQuery('.help-tooltip').each(function() {

            $target = jQuery(this);

            if($target.hasClass('right')){

                var my = 'left center';
                var at = 'right center';
            }
            else {

                var my = 'bottom left';
                var at = 'top center';
            }

            qTipID = $target.qtip({
                overwrite: false,
                style: {
                    classes: 'qtip-blue'
                },
                position: {
                    my: my,
                    at: at,
                    target: $target,
                    adjust: {
                        x: 5,
                        scroll: false,
                        resize: false
                    }
                }
            });

            qTips.help.push(qTipID.qtip('api'));
        });
    }

    jQuery('#report-type').on('click keyup', '.item', function(e){

        if(e.type == 'keyup') {

            var keycode = (e.keyCode ? e.keyCode : e.which);
            if(keycode != 13) {

                return false;
            }
        }
        var $clicked = jQuery(this),
            $all_options = jQuery('.options'),
            $all_items = jQuery('#report-type').find('.item');

        $all_options.hide();

        jQuery('#active').remove();
        $all_items.removeClass('active');
        $clicked.addClass('active').prepend('<div id="active"></div>');
        jQuery('#type').val($clicked.data('value')).change();

        var $watermark = jQuery('#report-watermark'),
            watermarkName = $clicked.data('image');

        if($watermark.length > 0 && watermarkName) {

            $watermark.css({'background-image': 'url(Images/new/watermark/' + watermarkName + '-watermark.gif)'});
        }
    });

    jQuery(document).on('click', function(){

        var $all_options = jQuery('.options');

        $all_options.hide();
    });

    jQuery('.more-options').on('click', function(e){

        e.stopPropagation();
        var $clicked = jQuery(this);
        var $options = $clicked.closest('.item-container').find('.options');
        var $all_options = jQuery('.options').not($options);

        $all_options.hide();

        if($options.length > 0){

            $options.toggle();
        }
    });

    jQuery('.options').on('click', '.item', function(e){

        // stop the click bubbling up the DOM tree
        e.stopPropagation();

        var $clicked = jQuery(this),
            $options = $clicked.closest('.options'),
            $parent = $options.closest('.item-container').find('.item').filter(':first'),
            $watermark = jQuery('#report-watermark'),
            watermarkName = $clicked.data('image'),
            $orientation = jQuery('#orientation'),
            $barStyle = jQuery('#bar-style'),
            $pieStyle = jQuery('#pie-style');

        switch($clicked.data('value')) {

            case 'bar':
                $orientation.val(1);
                $barStyle.val(1);
                break;
            case 'bar-horiz':
                $orientation.val(2);
                $barStyle.val(1);
                break;
            case 'bar-stacked':
                $orientation.val(1);
                $barStyle.val(2);
                break;
            case 'bar-stacked-horiz':
                $orientation.val(2);
                $barStyle.val(2);
                break;
            case 'pie-standard':
                $pieStyle.val(1);
                break;
            case 'pie-exploded':
                $pieStyle.val(2);
                break;
        }

        $parent.click().data('image', watermarkName).find('img').filter(':first').attr('src', $clicked.find('img').attr('src'));

        if($watermark.length > 0 && watermarkName) {

            $watermark.css({'background-image': 'url(Images/new/watermark/' + watermarkName + '-watermark.gif)'});
        }

        $options.hide();
    });

    // toggle options when type is changes
    jQuery('#type').on('change', function(){

        var value = parseInt(jQuery('#type').val(), 10);

        toggleRowsColumns(value);
        toggleGaugeOptions(value);
        setReportType();

        toggleOpenInExcelButton();
    });

    jQuery('#count_style').on('change', function() {

        toggleSumAverage();
    });

    jQuery('#count_style_field_sum, #count_style_field_average').on('change', function() {

        toggleValueAt();
    });

    jQuery('#count_style_field_value_at').on('change', function() {

        toggleCalendar();
    });

    jQuery('#count_style_field_initial_levels').on('click', function() {

        if(jQuery(this).prop('checked')) {

            hideItems(['#columns']);
        }
        else {

            showItems(['#columns']);
        }

        jQuery('#columns_table_title').clearField();
        jQuery('#columns_field_title').clearField();
        jQuery('#columns_date_option_title').clearField();
    });

    /**
     * Hide open in excel button if base listing report selected is a hard coded listing report from the incidents or risk module
     */
    jQuery('#base_listing_report').on('change', function(){

       toggleOpenInExcelButton();
    });

    $designer.on('click focus change', '.qtip-error', function(){

        jQuery(this).qtip('destroy', true).removeClass('qtip-error').attr('title', '');
    });

    // set min value for gauge sections when values changed
    jQuery('#section1max').on('blur', function(){

        jQuery('#gaugesection_2_minvalue').val(jQuery(this).val());
    });

    jQuery('#section2max').on('blur', function(){

        jQuery('#gaugesection_3_minvalue').val(jQuery(this).val());
    });

    jQuery('#reset').on('click', function(){

        resetForm();

        return false;
    });

    // remove unused field values on submit for hidden sections in case user set values then chose another report type
    jQuery('#designReport').on('submit', function(){
        var fields = getFields();

        return validateFields(fields);
    });

    // Validates
    jQuery('#rows_field_title, #columns_field_title').on('keydown click change', function(){

        var $target = jQuery(this);

        // use function return value to control action
        return validateFieldTable($target);
    });

    jQuery('#rows_field, #columns_field').closest('.codefield-container').find('a').on('click', function(){

        var $target = jQuery(this).closest('.codefield-container').find('input:first');

        // use function return value to control action
        return validateFieldTable($target);
    });

    jQuery(window).on('resize', function() {

        setDesignerHeightToViewportHeight();
        setContentHeightToDesignerHeight();
        setDesignerWidthForScrollbars();
        setReportViewportHeightToViewportHeight();
    });

    // Checks if viewport container has changed size and shows a bar if more content can be scrolled to
    //TODO this could probably be better handled by triggering a check to see if the designer has changed sizes when an
    // option is changed and remove reliance on a plugin for checking if something has resized or not.
    jQuery('#viewport-container').on('resize', function(){

        //positionEditor();
        // Simple method to set the width of the designer if scrollbars are required.
        setDesignerWidthForScrollbars();
    });

    jQuery('#viewport').on('scroll', function(){

        repositionQtips();

        if( ! globals.deviceDetect.isTablet) {

            jQuery('.ff_select').each(function() {

                jQuery(this).closeList();
            });
        }

        jQuery('#scroll-pos').val(jQuery(this).scrollTop());
    });

    jQuery(window).on('scroll', function(){

        //positionEditor();
    });

    // In IE < 8 animations don't works so well, so duration is set to 0 in this function
        jQuery('.collapse-designer').on('click', function(){

            var $trigger = jQuery(this),
                $designer = jQuery('#designer-wrapper'),
                $designer_state = jQuery('#designer-state'),
                designerWidth = $designer.data('width'),
                designerHeight = $designer.height(),
                $designContainer = jQuery('#designer-container'),
                $report = jQuery('#report-container'),
                contentHeight = jQuery('.content').height(),
                duration = 500;

            // set amount and duration of report animation
            if($designer.is(':visible')){

                designerWidth = 0;
                var report_width = ($report.outerWidth(true) + designerWidth);
                removeErrors();
            }
            else
            {
                var report_width = ($report.outerWidth(true) - designerWidth);
            }

            if (browser.isIE && parseInt(browser.version, 10) <= 8) {

                duration = 0;
            }

            // animate report designer
            if( ! $designer.is(':animated')){

                $designer.animate({
                   width: 'toggle'
                }, {
                   duration: duration,
                   complete: function(){

                       if($designer.is(':visible')){

                           var title = "Click to hide Report Designer";
                           $designContainer.height('auto');
                           $designer_state.val('open');
                       }
                       else
                       {
                           var title = "Click to open Report Designer";
                           $designContainer.height(designerHeight);
                           $designer_state.val('closed');
                       }

                       $report.width(report_width);
                       setDesignerWidthForScrollbars();
                       setDesignerHeightToViewportHeight();
                       setContentHeightToDesignerHeight();

                       if(jQuery.inArray(reportType['type'], nonFusionCharts) == -1 ) {

                           setFusionChartWidthToAvailableSpace();
                       }

                       $trigger.attr('title', title).find('.icon').toggleClass('open_designer close_designer');
                   }
                });
            }
        });

    jQuery('.trigger').on('change', function() {

        var trigger = jQuery(this);
        var trigger_classes = trigger.find('option').filter(':selected').attr('class');
        var classes = [];

        if(jQuery.type(trigger_classes) !== 'undefined') {

            var classes = (trigger.find('option').filter(':selected').attr('class')).split(" ");
        }

        jQuery('[id^=' + trigger.attr('name') + '-]').each(function(){

            var pattern = new RegExp(trigger.attr('name')+'-(.*)','i');
            var id_part = jQuery(this).attr('id').match(pattern);

            if(jQuery.inArray(id_part[1], classes) >= 0){

                jQuery(this).slideDown();
            }
            else{

                jQuery(this).slideUp();
            }
        });
    });

    jQuery('.toggle-container').find('.trigger').on('click change', function(){

        if(jQuery(this).find('.value').length > 0)
        {
            if(jQuery(this).val() == jQuery(this).find('.value').val())
            {
                jQuery(this).closest('.toggle-container').find('.target').slideDown();
            }
            else
            {
                jQuery(this).closest('.toggle-container').find('.target').slideUp();
            }
        }
        else
        {
            jQuery('toggle-container').find('.target').toggle();
        }
    });

    jQuery('.ui-button').button({
        icons: {
            primary: "ui-icon-caret-1-n"
        },
        text: false
    });

    jQuery('.collapse').on('click', '.trigger', function(){

        var $trigger = jQuery(this);
        var $parent = $trigger.closest('.collapse');
        var $target = $parent.find('.target').filter(':first');
        var $state = $parent.find('.state');

        if($target.length > 0)
        {
            repositionQtips($parent);

            $target.toggle({
                effect: 'blind',
                duration: 400,
                complete: function(){

                    if(jQuery(this).is(':visible')){

                        var title = "click to hide section";
                        //var image = 'Images/new/icons/collapse.gif';
                        //var alt = "-";
                        var state = 'open';
                    }
                    else
                    {
                        var title = "click to open section";
                        //var image = 'Images/new/icons/expand.gif';
                        //var alt = "+";
                        var state = 'closed';
                    }

                    $trigger.attr({
                        'title': title
                    }).toggleClass('plus minus');

                    var $qtips = $parent.find('.qtip-error');

                    if($qtips.length > 0) {

                        if($state.val() == 'open') {

                            $qtips.qtip('set', {
                                'position.target': $parent.find('.section_title_row')
                            });

                        }
                        else {

                            $qtips.qtip('set', {
                                'position.target': $qtips
                            });
                            $qtips.qtip('show');
                        }
                    }

                    repositionQtips();

                    if($state.length > 0){

                        $state.val(state);
                    }
                }
            });
        }
    });
});

/**
 * Checks whether or not the current query uses @prompts and, if so, displays the @prompt section.
 * 
 * @param queryID int The ID of the currently selected query.
 */
function checkQueryForAtPrompt(queryID)
{
    removeErrors();

    jQuery('#query_title').addClass("datixSelect-loading");
    jQuery.get("app.php?action=queryhasatprompt&id="+queryID+"&module="+module, function(data)
    {
        if (data == "true")
        {
            jQuery.get("app.php?action=getatpromptsection&id="+queryID+"&module="+module+"&context=ajax", function(data)
            {
                jQuery("#atprompt-fields").html(SplitJSAndHTML(data));
                RunGlobalJavascript();
                initialiseCalendars();
                showItems(['#atprompt']);
                jQuery('#query_title').removeClass("datixSelect-loading");
            });
        }
        else
        {
            hideItems(['#atprompt']);
            jQuery('#query_title').removeClass("datixSelect-loading");
        }
    });
}

/**
 * Ensures the "Open in Excel" button isn't displayed for hardcoded listing reports that can't be exported to Excel.
 */
function toggleOpenInExcelButton()
{
    jQuery("#export_no_render").toggle(parseInt(jQuery('#type').val(), 10) == REPORT_LISTING && !((module == 'INC' || module == 'RAM') && /^report[0-9]+$/i.match(jQuery('#base_listing_report').val())));
}

/**
 * Shows/hides the value at field depending on the count style
 *
 * @param {string} value      The value of the count style field.
 * @param {int}    reportType The type of the report.
 */
function toggleValueAt()
{
    var reserves    = ["claims_main.fin_indemnity_reserve", "claims_main.fin_expenses_reserve", 'claims_main.fin_calc_reserve_1', 'claims_main.fin_calc_reserve_2'],
        reportTypes = [REPORT_BAR, REPORT_CROSSTAB, REPORT_LINE],
        reportType = parseInt(jQuery('#type').val(), 10),
        countStyle = jQuery('#count_style').val(),
        value = null;

    if(countStyle == 2) {

        value = jQuery('#count_style_field_sum').val();
    }
    else if(countStyle == 3) {

        value = jQuery('#count_style_field_average').val();
    }

    if (jQuery.inArray(value, reserves) > -1)
    {
        showItems(['#count-style-value-at']);

        if (jQuery.inArray(reportType, reportTypes) > -1)
        {
            showItems(['#count-style-initial-levels']);
        }
        else
        {
            jQuery('#count_style_field_initial_levels').prop('checked', false);
            hideItems(['#count-style-initial-levels']);
        }

        toggleCalendar();
    }
    else
    {
        hideItems(['#count-style-value-at', '#count-style-initial-levels', '#count-style-calendar']);
    }
}

/**
 * Shows/hides the calendar field depending on the value at
 *
 * @param {string} value The value of the value at field.
 */
function toggleCalendar() {

    var value = jQuery('#count_style_field_value_at').val();

    if (value == '2')
    {
        showItems(['#count-style-calendar']);
    }
    else
    {
        jQuery('#count_style_field_calendar').val('');
        hideItems(['#count-style-calendar']);
    }
}

function exportNoRender(exportFormat, exportOrientation, exportPaperSize)
{
    var designReport = jQuery("#designReport");

    var action = designReport.prop("action");

    designReport
        .prop("action", "app.php?action=openinexcel&reportoutputformat="+exportFormat+"&page_orientation="+exportOrientation+"&papersize="+exportPaperSize)
        .submit()
        .prop("action", action);
}