jQuery(function($) {

    var ListingReportPager = $('.report-listing').listingPager({
        startPosition: startPosition,
        endOfReport:   endOfReport,
        previousRow:   previousRow,
        columnTotals:  columnTotals
    });

    ListingReportPager.init();
    
    // get total main record count
    $.ajax({
        url: "app.php?action=getrecordcount",
        success: function(data)
        {
            //Data is returned formatted with ","s, so we need to ignore them when validating here.
            if (isNaN(data.split(",").join("")))
            {
                $("#record_count").text(globals.error_retrieving_count).css({display: "inline", color: "red"});
            }
            else
            {
                $("#record_count").text(data).css("display", "inline");
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            $("#record_count").text(globals.error_retrieving_count).css({display: "inline", color: "red"});
        }
    });
});