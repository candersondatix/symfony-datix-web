(function( $ ) {

    $.fn.report_selector = function( options ) {

        var $target = this,
            $clicked,
            find = null,
            items = null,
            all_items = null,
            viewport_width = null,
            item_width = null,
            full_width = null,
            total_items = 0,
            current_index = 0,
            $next_button = null,
            $prev_button = null,
            $wrapper = null,
            $active = null;

        var settings = $.extend({
            speed: 500,
            padding: 10,
            change: null,
            show: 3,
            move: 1,
            active: 1,
            initial: 1,
            item: {
                width: 100,
                height: 130
            },
            selectors: {
                next: '#report-selector-next',
                prev: '#report-selector-prev',
                wrapper: '.report-selector-viewport'
            }
        }, options);

        $next_button = $(settings.selectors.next);
        $prev_button = $(settings.selectors.prev);

        $target.prop('draggable', false);

        if($target.is('select')) {

            find = 'option';
        }
        else {

            find = 'li';
        }

        items = $target.find(find);

        total_items = items.length;
        item_width = settings.item.width + settings.padding;

        items.each(function() {

            $(this).addClass('item').width(settings.item.width).height(settings.item.height).css({ 'padding-right': settings.padding + 'px', position: 'relative' });
        });

        // add clone elements to the front of the set
        items.slice(-(settings.show)).clone().addClass('clone').prependTo(this);

        // add clone elements to the end of the set
        items.slice(0, settings.show).clone().addClass('clone').appendTo(this);

        all_items = $target.find(find);

        full_width = item_width * all_items.length;
        viewport_width = (settings.item.width * settings.show) + (settings.padding * (settings.show - 1));

        $target.addClass('report-selector').width(full_width).height(settings.item.height).wrap('<div class="' + settings.selectors.wrapper.replace('.', '') + '" style="width: ' + viewport_width + 'px; height: ' + settings.item.height + 'px; overflow: hidden; position: relative;" />');

        $wrapper = $(settings.selectors.wrapper);

        $wrapper.prop('draggable', false);

        move_to((settings.initial + settings.show), 0);

        all_items.on('click', function() {

            move_to($(this).index());
        });

        $next_button.on('click', function() {

            current_index = current_index + 1;

            move_to(current_index);
        });

        $prev_button.on('click', function() {

            current_index = current_index - 1;

            move_to(current_index);
        });

        if(typeof settings.change === "function") {


        }

        this.moveTo = function(index){

            move_to(index);
        };

        function move_to(index, speed) {

            if(typeof speed === 'undefined') {

                speed = settings.speed;
            }

            if(index < 0) {

                index = 0
            }
            else if(index > all_items.length) {

                index  = all_items.length;
            }

            var position = all_items.eq(index - settings.active).position().left;

            if(speed == 0 || ! $target.is(':animated')) {

                $('#active').remove();

                $target.animate( {
                    left: "-" + (position)
                }, speed, function() {

                    if(index == settings.active){

                        var reset_index = $target.find(find).filter(':not(.clone):last').index() - settings.active;

                        move_to(reset_index, 0);
                    }
                    else if(index == all_items.length - (settings.show - settings.active))
                    {
                        var reset_index = $target.find(find).filter(':not(.clone):first').index() + settings.active;

                        move_to(reset_index, 0);
                    }
                    /*if($target.position().left <= 0 - (item_width * (all_items.length - settings.show - settings.active))) {

                        var reset_index = $target.find(find).filter(':not(.clone):first').index();

                        move_to(reset_index, 0);
                    }
                    else if($target.position().left >= ((settings.active > 0 && settings.active < settings.show - 1) ? 0 - (item_width * settings.active)  : 0)) {

                        var reset_index = $target.find(find).filter(':not(.clone):last').index();

                        move_to(reset_index, 0);
                    }*/
                    else
                    {
                        current_index = reset_index ? reset_index : index;

                        all_items.removeClass('active').eq(current_index).addClass('active').prepend('<div id="active"></div>');

                        $active = $('#active');

                        $active.css( {

                            width: settings.item.width,
                            height: settings.item.width,
                            'z-index': 100,
                            top: '-2px',
                            left: '-2px'
                        });

                        // SET values for chart select drop down and update view

                        $clicked = all_items.eq(current_index);

                        $('#type_title').val($clicked.data('name'));
                        $('#type').val($clicked.data('value')).change();
                    }
                });
            }
        }

        return this;
        $('#type').change();
    };
}(jQuery));