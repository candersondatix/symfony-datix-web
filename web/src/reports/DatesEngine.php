<?php
namespace src\reports;

use src\framework\query\QueryFactory;
use src\framework\query\Where;
use src\framework\query\FieldCollection;
use src\framework\registry\Registry;
use src\reports\model\report\Report;

/**
 * Provides date-specific functionality and defines the interface for RDBMS-specific implementations required by the reporting engine.
 */
abstract class DatesEngine
{
    /**
     * The db wrapper object.
     * 
     * @var DatixDBQuery
     */
    protected $db;
    
    /**
     * Factory for query tools.
     * 
     * @var QueryFactory
     */
    protected $queryFactory;
    
    /**
     * The application registry.
     * 
     * @var Registry
     */
    protected $registry;
    
    public function __construct(\DatixDBQuery $db, QueryFactory $queryFactory, Registry $registry)
    {
        $this->db           = $db;
        $this->queryFactory = $queryFactory;
        $this->registry     = $registry;
    }
    
    /**
     * {@inherit}
     */
    public function getWeekHeaders()
    {
        $weeks = array();
        for ($i = 1; $i <= 53; $i++)
        {
            $weekNumber = ($i < 10 ? '0' : '') . $i;
            $weeks[$i] = $weekNumber;
        }
        
        return $weeks;
    }
    
    /**
     * Returns an array of calender months.
     * 
     * @return array
     */
    public function getMonthHeaders()
    {
        return $this->getMonths();
    }
    
    /**
     * Returns an array of financial months.
     * 
     * @return array $finMonths
     */
    public function getFinancialMonthHeaders()
    {
        $months = $this->getMonths();
        $finMonths = array();
        
        for ($i = $this->registry->getParm('FINYEAR_START_MONTH', 4); $i <= 12; $i++)
        {
            $finMonths[$i] = $months[$i];
        }
    
        for ($i = 1; $i < $this->registry->getParm('FINYEAR_START_MONTH', 4); $i++)
        {
            $finMonths[$i] = $months[$i];
        }

        return $finMonths;
    }
    
    /**
     * Returns an array of quarters between the lower/upper limit of the provided axis values.
     * 
     * Converts the provided axis values (a 5 digit int in the format yyyyq) to a readable format.
     * 
     * @param array  $axisValues The values for this axis from the report data.
     * @param string $financial  Whether or not we're reporting on financial quarters.
     * 
     * @return array $headers
     */
    public function getQuarterHeaders(array $axisValues, $financial = false)
    {
        sort($axisValues);
        
        $min = $axisValues[0];
        $max = end($axisValues);
        $headers = array();
        
        for ($i = $min; $i <= $max; $i++)
        {
            $year = substr($i, 0, 4);
            $quarter = substr($i, 4);
            
            if ($quarter == 5)
            {
                $i = ($year + 1) . 0;
                continue;
            }
            
            if ($financial)
            {
                $nextyear = $year + 1;
                $headers[$i] = substr($year, 2) . '/' . substr($nextyear, 2) . ' Q' . $quarter;
            }
            else
            {
                $headers[$i] = $year . ' Q' . $quarter;
            }
            
        }
        
        return $headers;
    }
    
    /**
     * Returns an array of years between the lower/upper limit of the provided axis values.
     * 
     * @param array $axisValues The values for this axis from the report data.
     * 
     * @return array
     */
    public function getYearHeaders(array $axisValues)
    {
        sort($axisValues);
        $range = range($axisValues[0], end($axisValues));
        return array_combine($range, $range);
    }
    
    /**
     * Returns an array of months and years between the lower/upper limit of the provided axis values.
     * 
     * @param array $axisValues The values for this axis from the report data.
     * 
     * @return array
     */
    public function getMonthAndYearHeaders(array $axisValues)
    {
        sort($axisValues);
        
        $min     = $axisValues[0];
        $max     = end($axisValues);
        $months  = $this->getMonths();
        $headers = array();
        
        for ($i = $min; $i <= $max; $i++)
        {
            $year = substr($i, 0, 4);
            $month = substr($i, 4);
            
            if ($month == 13)
            {
                $i = ($year + 1) . '00';
                continue;
            }
            
            if ($month{0} == '0')
            {
                $month = substr($month, 1);
            }
            
            $headers[$i] = $months[$month] . ' ' . $year;
        }
        
        return $headers;
    }
    
    /**
     * Returns an array of months.
     * 
     * @return array
     */
    protected function getMonths()
    {
        return array(
    		1  => _tk('jan'), 2  => _tk('feb'), 3  => _tk('mar'), 4  => _tk('apr'), 5  => _tk('may'), 6  => _tk('jun'),
    		7  => _tk('jul'), 8  => _tk('aug'), 9  => _tk('sep'), 10 => _tk('oct'), 11 => _tk('nov'), 12 => _tk('dec')
        );
    }
    
    /**
     * Wraps the field in the appropriate date function, depending on the date option chosen.
     * 
     * @param string $field      The field we're reporting on.
     * @param string $dateOption The reporting date option.
     * 
     * @return array The wrapper function(s) and field name(s)
     */
    public abstract function getDateSelectField($field, $dateOption);
    
    /**
     * Returns an array of days with keys corresponding to SQL server weekday keys
     * and ordered according to week_start_day global.
     * 
     * @return array
     */
    public abstract function getWeekDayHeaders();
    
    /**
     * Get week date headers
     *
     * @author gszucs
     * @param array $axisValues
     * @return array
     */
    public function getWeekDateHeaders( array $axisValues )
    {
    	$labels = array_map( function($v){
            if($v != null)
            {
    		    return FormatDateVal( $v .' 00:00:00.000');
            }
            else
            {
                return 'No value';
            }
    	}, $axisValues);

        $combinedArray = array_combine( $axisValues, $labels );
        ksort($combinedArray);

    	return $combinedArray;
    }
    
    /**
     * Creates the additional WHERE conditions required when drilling into reports with date fields.
     *
     * @param string $field      The fully qualified name (e.g: incidents_main.inc_dopened).
     * @param string $dateOption The option selected for the date field.
     * @param string $dateValue  The value of the date field.
     * @param Where  $where      The Where object.
     */
    public function setDateOptionWhere($field, $dateOption, $dateValue, Where $where)
    {
        if ($dateValue == '')
        {
            $where->add(
                'OR',
                (new FieldCollection)->field($field)->isNull(),
                (new FieldCollection)->field($field)->isEmpty()
            );
        }
        else
        {
            $fc = new FieldCollection();
            if ($dateOption == Report::WEEK_DATE)
            {
                // unfortunately, we can't reuse the SELECT statement for this case, so we have to manually construct the condition
                $fc->field($field)->gtEq($dateValue)->lt(date('Y-m-d', strtotime('1 week', strtotime($dateValue))));
            }
            else
            {
                $fc->field($this->getDateSelectField($field, $dateOption))
                   ->eq($dateValue);
            }
            $where->add($fc);
        }
    }
}