<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Tests that a packaged report conforms to the inverse of a given specification.
 */
class NotReportSpecification extends AbstractReportSpecification
{
    /**
     * The specification we're inverting.
     * 
     * @var AbstractReportSpecification
     */
    private $wrapped;
    
    public function __construct(AbstractReportSpecification $wrapped)
    {
        $this->wrapped = $wrapped;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return !$this->wrapped->isSatisfiedBy($packagedReport);
    }
}