<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report includes values for the initial levels of a sum/average field.
 */
class InitialLevelsReportSpecification extends AbstractReportSpecification
{
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return 'Y' == $packagedReport->report->count_style_field_initial_levels;
    }
}