<?php

namespace src\reports\specifications\CLA;

use src\reports\specifications\AbstractReportSpecification;
use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report is averaging across a Claims Payment Type code.
 */
class AverageOnPayTypeReportSpecification extends AbstractReportSpecification
{
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return 'claims_main.pay_type' == substr($packagedReport->report->count_style_field_average, 0, 20);
    }
}