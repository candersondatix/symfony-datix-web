<?php

namespace src\reports\specifications;

use src\reports\model\report\Report;
use src\reports\specifications\CLA\SumOnPayTypeReportSpecification;
use src\reports\specifications\CLA\AverageOnPayTypeReportSpecification;
use src\framework\registry\Registry;

class ReportSpecificationFactory
{
    /**
     * @param string $module
     * 
     * @return ModuleReportSpecification
     */
    public function getModuleSpecification($module)
    {
        return new ModuleReportSpecification($module);
    }
    
    /**
     * @param int $countStyle
     * 
     * @return CountStyleReportSpecification
     */
    public function getCountStyleSpecification($countStyle)
    {
        return new CountStyleReportSpecification($countStyle);
    }
    
    /**
     * @return AndReportSpecification
     */
    public function getSumOnPayTypeSpecification()
    {
        return (new ModuleReportSpecification('CLA'))->and_(
            (new CountStyleReportSpecification(Report::COUNT_SUM))->and_(
                new SumOnPayTypeReportSpecification()
            )
        );
    }
    
    /**
     * @return AndReportSpecification
     */
    public function getAverageOnPayTypeSpecification()
    {
        return (new ModuleReportSpecification('CLA'))->and_(
            (new CountStyleReportSpecification(Report::COUNT_AVE))->and_(
                new AverageOnPayTypeReportSpecification()
            )
        );
    }
    
    /**
     * Returns a report specification which determines whether we're doign a sum/average across Claims Payment Types.
     * 
     * @return OrReportSpecification
     */
    public function getPayTypeSpecification()
    {
        return $this->getSumOnPayTypeSpecification()->or_($this->getAverageOnPayTypeSpecification());
    }
    
    /**
     * Returns a report specification which determines if we're doing a sum/average count across the Claims reserve fields.
     * 
     * @return AndReportSpecification
     */
    public function getAggregateReserveSpecification()
    {
        return (new ModuleReportSpecification('CLA'))->and_(
            (new CountStyleReportSpecification(Report::COUNT_SUM))->and_(
                (new SumFieldReportSpecification('claims_main.fin_indemnity_reserve'))->or_(
                    new SumFieldReportSpecification('claims_main.fin_expenses_reserve')
                )
            )->or_(
                (new CountStyleReportSpecification(Report::COUNT_AVE))->and_(
                    (new AveFieldReportSpecification('claims_main.fin_indemnity_reserve'))->or_(
                        new AveFieldReportSpecification('claims_main.fin_expenses_reserve')
                    )
                )
            )
        );
    }
    
    /**
     * Returns a report specification which determines if we're doing a sum/average count across the Claims remaining reserve fields.
     * 
     * @return AndReportSpecification
     */
    public function getAggregateRemainingReserveSpecification()
    {
        return (new ModuleReportSpecification('CLA'))->and_(
            (new CountStyleReportSpecification(Report::COUNT_SUM))->and_(
                (new SumFieldReportSpecification('claims_main.fin_calc_reserve_1'))->or_(
                    new SumFieldReportSpecification('claims_main.fin_calc_reserve_2')
                )
            )->or_(
                (new CountStyleReportSpecification(Report::COUNT_AVE))->and_(
                    (new AveFieldReportSpecification('claims_main.fin_calc_reserve_1'))->or_(
                        new AveFieldReportSpecification('claims_main.fin_calc_reserve_2')
                    )
                )
            )
        );
    }
    
    /**
     * Returns a report specification which determines whether we're running an aggregate report across the Claims reserves audit.
     * 
     * @return AndReportSpecification
     */
    public function getAggregateReserveOnSelectedDateSpecification()
    {
        return $this->getAggregateReserveSpecification()->and_(new CountValueAtReportSpecification(Report::SELECT_DATE));
    }
    
    /**
     * Returns a report specification which determines whether we're running an aggregate report 
     * across the Claims reserves including the initial reserve levels.
     * 
     * @return AndReportSpecification
     */
    public function getAggregateReserveWithInitialLevelsSpecification()
    {
        return $this->getAggregateReserveSpecification()->and_(new InitialLevelsReportSpecification());
    }
    
    /**
     * Returns a report specification which determines whether we're running an aggregate report 
     * across the Claims reserves including the initial reserve levels.
     * 
     * @return AndReportSpecification
     */
    public function getAggregateRemainingReserveWithInitialLevelsSpecification()
    {
        return $this->getAggregateRemainingReserveSpecification()->and_(new InitialLevelsReportSpecification());
    }
    
    /**
     * Returns a report specification which determines whether we're running a Claims aggregate report across either
     * reserves or remaining reserves including initial levels.
     */
    public function getInitialLevelsSpecification()
    {
        return $this->getAggregateReserveWithInitialLevelsSpecification()->or_(
            $this->getAggregateRemainingReserveWithInitialLevelsSpecification()
        );
    }
    
    /**
     * @return GaugeReportSpecification
     */
    public function getGaugeReportSpecification()
    {
        return new GaugeReportSpecification();
    }
    
    /**
     * @return LinkedContactMainTableReportSpecification
     */
    public function getLinkedContactMainTableReportSpecification($contactLinkTable = 'link_contacts', Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();
        return new LinkedContactMainTableReportSpecification($registry->getFieldDefs(), $contactLinkTable);
    }
}