<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

abstract class AbstractReportSpecification
{
    /**
     * Determines whether a packaged report object complies with a defined set of conditions.
     * 
     * @param PackagedReport $packagedReport
     * 
     * @return boolean
     */
    abstract public function isSatisfiedBy(PackagedReport $packagedReport);
    
    /**
     * Test a combination of this and a given specification.
     * 
     * @param AbstractReportSpecification $specification
     * 
     * @return AndReportSpecification
     */
    public function and_(AbstractReportSpecification $specification)
    {
        return new AndReportSpecification($this, $specification);
    }

    /**
     * Test a combination of this or a given specification.
     * 
     * @param AbstractReportSpecification $specification
     * 
     * @return OrReportSpecification
     */
    public function or_(AbstractReportSpecification $specification)
    {
        return new OrReportSpecification($this, $specification);
    }

    /**
     * Test the inverse of this specification.
     * 
     * @return NotSpecification
     */
    public function not()
    {
       return new NotReportSpecification($this);
    }
}