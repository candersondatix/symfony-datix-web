<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report uses a given count style.
 */
class CountStyleReportSpecification extends AbstractReportSpecification
{
    /**
     * @var int
     */
    protected $countStyle;
    
    public function __construct($countStyle)
    {
        $this->countStyle = $countStyle;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return $this->countStyle == $packagedReport->report->count_style;
    }
}