<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report is created for a given module.
 */
class ModuleReportSpecification extends AbstractReportSpecification
{
    /**
     * @var string
     */
    protected $module;
    
    public function __construct($module)
    {
        $this->module = $module;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return $this->module == $packagedReport->report->module;
    }
}