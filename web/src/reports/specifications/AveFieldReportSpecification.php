<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report averages across a given field.
 */
class AveFieldReportSpecification extends AbstractReportSpecification
{
    /**
     * @var string
     */
    protected $aveField;
    
    public function __construct($aveField)
    {
        $this->aveField = $aveField;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return $this->aveField == $packagedReport->report->count_style_field_average;
    }
}