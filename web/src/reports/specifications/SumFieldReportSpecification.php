<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Determines whether a packaged report sums across a given field.
 */
class SumFieldReportSpecification extends AbstractReportSpecification
{
    /**
     * @var string
     */
    protected $sumField;
    
    public function __construct($sumField)
    {
        $this->sumField = $sumField;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return $this->sumField == $packagedReport->report->count_style_field_sum;
    }
}