<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;

/**
 * Tests that a packaged report conforms to each of two specifications.
 */
class AndReportSpecification extends AbstractReportSpecification
{
    /**
     * The first specification in the comparison.
     * 
     * @var AbstractReportSpecification
     */
    private $first;
    
    /**
     * The second specification in the comparison.
     * 
     * @var AbstractReportSpecification
     */
    private $second;
    
    public function __construct(AbstractReportSpecification $first, AbstractReportSpecification $second)
    {
        $this->first  = $first;
        $this->second = $second;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return $this->first->isSatisfiedBy($packagedReport) && $this->second->isSatisfiedBy($packagedReport);
    }
}