<?php

namespace src\reports\specifications;

use src\reports\model\packagedreport\PackagedReport;
use src\reports\model\report\Report;

/**
 * Specifies whether or not a report is a Gauge report.
 */
class GaugeReportSpecification extends AbstractReportSpecification
{
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy(PackagedReport $packagedReport)
    {
        return Report::GAUGE == $packagedReport->report->getReportType();
    }
}