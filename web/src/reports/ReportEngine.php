<?php
namespace src\reports;

use src\reports\controllers\ReportWriter;
use src\reports\model\packagedreport\PackagedReport;
use src\reports\querybuilder\IReportQueryBuilder;
use src\framework\query\QueryFactory;
use src\framework\registry\Registry;

abstract class ReportEngine
{
    /**
     * The db wrapper object.
     * 
     * @var DatixDBQuery
     */
    protected $db;
    
    /**
     * Factory for query tools.
     * 
     * @var QueryFactory
     */
    protected $queryFactory;
    
    /**
     * The application registry.
     * 
     * @var Registry
     */
    protected $registry;
    
    /**
     * @var IReportQueryBuilder
     */
    protected $queryBuilder;
    
    /**
     * @var int
     */
    protected $fetchStyle;
    
    public function __construct(\DatixDBQuery $db, QueryFactory $factory, Registry $registry, IReportQueryBuilder $queryBuilder)
    {
        $this->db           = $db;
        $this->queryFactory = $factory;
        $this->registry     = $registry;
        $this->queryBuilder = $queryBuilder;
        $this->fetchStyle   = \PDO::FETCH_ASSOC;
    }
    
    /**
     * Fetches the data used to generate a report.
     * 
     * @param PackagedReport $packagedReport The packaged report object.
     * @param int            $context        The context in which the report is being output.
     * 
     * @return array
     */
    public function getData(PackagedReport $packagedReport, $context = ReportWriter::FULL_SCREEN)
    {
        if (null !== ($currentUser = $_SESSION['CurrentUser']))
        {
            $login = $currentUser->login;
        }
        
        // build and execute the report query
        $query  = $this->queryBuilder->build($packagedReport);
        $writer = $this->queryFactory->getSqlWriter();
        list($sql, $parameters) = $writer->writeStatement($query);
        
        $rawQuery = $writer->combineSqlAndParameters($sql, $parameters);
        
        try
        {
            $this->db->setSQL($sql);
            
            $this->registry->getLogger()->logReporting('Packaged report ID '.$packagedReport->recordid.' ("'.$packagedReport->name.'") for '.$login.' query execute start ('.$rawQuery.').');
            
            $start = microtime(true);
            $this->db->prepareAndExecute($parameters);
            $duration = microtime(true) - $start;
            
            $this->registry->getLogger()->logReporting('Packaged report ID '.$packagedReport->recordid.' ("'.$packagedReport->name.'") for '.$login.' query execute end (duration: '.$duration.').');
            $this->registry->getLogger()->logReporting('Packaged report ID '.$packagedReport->recordid.' ("'.$packagedReport->name.'") for '.$login.' result fetch start.');
            
            $start = microtime(true);
            $data = $this->db->fetchAll($this->fetchStyle);
            $duration = microtime(true) - $start;
            
            $this->registry->getLogger()->logReporting('Packaged report ID '.$packagedReport->recordid.' ("'.$packagedReport->name.'") for '.$login.' result fetch end (duration: '.$duration.').');
        } 
        catch (\DatixDBQueryException $e)
        {
            if (\UnicodeString::stripos($e->getMessage(), 'Ambiguous column name') !== false && count($query->getWhereStrings()) > 0)
            {
                // we're using an incompatible "old" saved query - display information about possible remedies to the user
                fatal_error(_tk('ambiguous_column_message'));
            }
            else
            {
                throw $e;
            }
        }

        // return formatted report data
        return $this->formatData($packagedReport, $data, $context);
    }
    
    /**
     * Formats and adds metadata to the raw data set so it can be used to output a report.
     *
     * @param PackagedReport $packagedReport The packaged report object.
     * @param array          $data           The raw report data.
     * @param int            $context        The context where we are loading the report.
     *
     * @return array The formatted data.
     */
    protected abstract function formatData(PackagedReport $packagedReport, array $data, $context = ReportWriter::FULL_SCREEN);
}