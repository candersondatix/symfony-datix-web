<?php

namespace src\incidents\controllers;

use src\framework\controller\Controller;

class NewDIF1Controller extends Controller
{
    /**
     * Session variable incident_number holds the recordid of the current incident
     * being added.  This is used to ensure that only the current user can
     * edit the incident.
     */
    function newdif1()
    {
        $_SESSION['incident_number'] = '';

        if ($this->request->getParameter('form_id'))
        {
            if (FormIDExists($this->request->getParameter('form_id'), 'INC'))
            {
                $_SESSION['form_id']['INC'][1] = \Sanitize::SanitizeInt($this->request->getParameter('form_id'));
            }
            else
            {
                $IncorrectFormID = true;
            }
        }

        if ($IncorrectFormID)
        {
            fatal_error('There is no form with the specified ID.');
        }
        else
        {
            if (bYN(GetParm('DIF_1_CONTACT_REP')))
            {
                $ReporterArray = SetUpDefaultReporter();

                if (!empty($ReporterArray))
                {
                    $inc['con']['R'][] = $ReporterArray;
                }
            }
            else
            {
                // Need to use fields on main incident table.
                $inc = SetUpDefaultReporterINC();
            }

            $this->response = $this->call('src\incidents\controllers\IncidentFormTemplateController', 'ShowHoldingForm',
                array('data' => $inc));
            return;
        }

        obExit();
    }
}