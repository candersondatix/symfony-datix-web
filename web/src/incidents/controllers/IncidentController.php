<?php

namespace src\incidents\controllers;

use src\documents\controllers\DocumentController;
use src\framework\controller\Controller;
use src\ast\controllers\AssetsController;
use src\framework\controller\Loader;
use src\framework\query\Query;
use src\framework\query\QueryFactory;
use src\generic\controllers\MainRecordController;
use src\users\model\User;
use src\users\model\UserModelFactory;
use src\messagequeue\MessageQueueFactory;
use src\messagequeue\MessageQueue;

class IncidentController extends Controller
{
    function saveIncident()
    {
        global $scripturl, $yySetLocation, $yyheaderdone, $ClientFolder, $AdminEmail, $EmailUnapproved, $IncOrganisation, $HideFields,
               $OrderSections, $dtxdebug, $MandatoryFields, $SectionVisibility, $dtxtitle, $dtxwait;

        /**
         * TODO: check that this isn't required, if not remove it from here, if so then session_start() should be placed
         * somewhere so it's available when needed in controllers and not individually
         */
        session_start();

        $recordid               = $this->request->getParameter('recordid');
        $sendingEmailsOnUpdate  = $this->shouldRecordUpdateEmail();
        $queuingEmails          = $this->registry->getParm('QUEUE_EMAILS', 'N', false, true);
        $notANewRecord          = !in_array($this->get('rep_approved_old'), ['NEW','STCL']);
        
        if (!$queuingEmails && $sendingEmailsOnUpdate && $notANewRecord)
        {
            $AlreadyEmailedContacts = $this->GetAlreadyEmailedContacts($recordid,
                ($this->registry->getParm('DIF_PERMS') ? $this->registry->getParm('DIF_PERMS') : 'NONE'),
                $this->request->getParameter('formlevel')
            );
        }

        $ModuleDefs = $this->registry->getModuleDefs();

        //need to check $_POST specifically here rather than ->request, since the blanking out of the $_POST array
        //is what we are looking for, and ->request merges $_POST and $_GET.
        if (!is_array($_POST) || !$_POST)
        {
            //Under normal circumstances, something should always be posted here. If it is completely blank,
            //it suggests that something has gone wrong, e.g. an incorrect ini directive.
            $form_action = 'Cancel';
        }
        else
        {
            $form_action = $this->request->getParameter('rbWhat');
        }

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && ($this->request->getParameter('increp_recordid')
            || $this->request->getParameter('recordid')))
        {
            $main_table = 'incidents_main';

            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array(
                'table' => $main_table,
                'link_id' => ($this->request->getParameter('increp_recordid')
                    ? $this->request->getParameter('increp_recordid') : $this->request->getParameter('recordid'))
            ));
        }

        switch ($form_action)
        {
            case 'Cancel':
                if ($this->request->getParameter('fromsearch'))
                {
                    $yySetLocation = $scripturl.'?action=list&module=INC&listtype=search';
                }
                elseif ($this->request->getParameter('from_report'))
                {
                    $yySetLocation = $scripturl.'?action=list&module=INC&listtype=search&from_report=1&overdue='.$this->request->getParameter('overdue');
                }
                else
                {
                    $vars = array();

                    if ($this->request->getParameter('form_id') && is_numeric($this->request->getParameter('form_id')))
                    {
                        $vars['form_id'] = $this->request->getParameter('form_id');
                    }

                    $vars['module'] = 'INC';
                    $yySetLocation = $scripturl . '?' . http_build_query($vars);
                }

                redirectexit();
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');

                break;
            case BTN_CANCEL:
                if ($this->request->getParameter('fromsearch'))
                {
                    $RedirectLocation = '?action=list&module=INC&listtype=search';
                }
                else
                {
                    $RedirectLocation = '?module=INC';

                    if ($this->request->getParameter('form_id') && is_numeric($this->request->getParameter('form_id')))
                    {
                        $RedirectLocation .= '&form_id='.$this->request->getParameter('form_id');
                    }
                }

                $this->redirect('app.php'.$RedirectLocation);
            case 'ShowDIF1Values':
                $this->response = $this->call('src\incidents\controllers\ShowIncidentController', 'incident', array(
                    'recordid' => $this->request->getParameter('recordid'),
                    'show_dif1_values' => '1'
                ));
                return;
            case 'ShowAudit':
                $this->response = $this->call('src\incidents\controllers\ShowIncidentController', 'incident', array(
                    'recordid' => $this->request->getParameter('recordid'),
                    'full_audit' => '1'
                ));
                return;
        }

        $DIFPerms = GetParm('DIF_PERMS');

        if (($this->request->getParameter('session_date') != $_SESSION['lastsession'][$this->request->getParameter('session_form')])
            && !empty($_SESSION['lastsession']))
        {
            $error = true;
            $blankdata = true;

            AddSessionMessage('ERROR', _tk('form_submit_expired'));
            logErrorReport(array(
                'message' => $error['submit_error'],
                'details' => 'Attempted to submit session started on '.$this->request->getParameter('session_date').
                    '. Current session started on '.$_SESSION['lastsession'][$this->request->getParameter('session_form')]
            ));
        }

        // Can only save own incidents if not Admin user in generic form.
        $AdminPerms = $_SESSION['Perms']['ADM']['form'][0];
        $AdminUser = ($AdminPerms == '');

        $HoldingForm = ($this->request->getParameter('holding_form') == 1);

        $approved = GetValidApprovalStatusValueFromPOST(array('module' => 'INC'));

        if ($form_action == 'Search')
        {
            $this->response = $this->call('src\incidents\controllers\IncidentsDoSelectionController', 'incidentsdoselection');
            return;
        }

        $main_table = 'incidents_main';

        $HoldingLevel = $HoldingForm ? 1 : 2;

        GetSectionVisibility('INC', $HoldingLevel, $this->request->getParameters());
        BlankOutPostValues('INC', $HoldingLevel, null, null, $this->request);
        $this->BlankOutMedicationValues('INC', $HoldingLevel, null, null, $this->request);

        // This is "failover" validation as, in order to get this far,
        // the incident must have passed the JavaScript mandatory
        // field validation.  Also, currently only want to do the
        // mandatory field validation if we are in the holding form.
        $ValidationErrors = $this->ValidateIncidentData();

        // Contacts linked on DIF1
        if (max($this->request->getParameter('contact_max_suffix'), $this->request->getParameter('increp_contacts')) > 6)
        {
            $ContactError = ValidateLinkedContactData($this->request->getParameters(), 'INC');

            /*We need to ensure that fields that are not visible on save are not validated (otherwise you end
            up with validation messages you cannot see. The best way of doing this would be to have GetSectionVisibility()
            and BlankOutPostValues() run for each linked contact form. That's going to be pretty risky to force in now,
            so in the short term I'm doing a manual check of whether the field is visible here.
            TODO: make GetSectionVisibility and BlankOutPostValues generic enough that they can run on subforms within level 1 forms.
            */
            $ContactError = MainRecordController::RemoveHiddenContactValidation($ContactError, $this->request);

            $ValidationErrors = array_merge($ValidationErrors, $ContactError);
        }

        // Documents linked on DIF1
        if ($this->request->getParameter('max_doc_suffix'))
        {
            $DocError = DocumentController::ValidateLinkedDocumentData($this->request->getParameters());
            $ValidationErrors = array_merge($ValidationErrors, $DocError);
        }

        if ($ValidationErrors)
        {
            AddSessionMessage('ERROR', _tk('form_errors'));
            $error['Validation'] = $ValidationErrors;
        }

        if ($error)
        {
            if (!$blankdata)
            {
                $inc = $this->request->getParameters();

                //Need to reformat dates to ensure they are in SQL format to be interpreted when reloading the form.
                $aDateValues = CheckDatesFromArray(GetAllFieldsByType('INC', 'date'), $inc);
                $inc = array_merge($inc, $aDateValues);
            }

            $inc['error'] = $error;

            if ($HoldingForm)
            {
                $this->response = $this->call('src\incidents\controllers\IncidentFormTemplateController', 'ShowHoldingForm',
                    array('data' => $inc));
                return;
            }
            else
            {
                require_once 'Source/incidents/MainIncident.php';

                // Workaround to convert dates to SQL format
                // TODO: This needs to be removed when refactored
                $IncidentDateFields = array_merge(GetAllFieldsByType('INC', 'date'), getAllUdfFieldsByType('D', '', $inc));

                foreach ($IncidentDateFields as $IncidentDate)
                {
                    if (array_key_exists($IncidentDate, $inc) && $inc[$IncidentDate] != '')
                    {
                        $inc[$IncidentDate] = UserDateToSQLDate($inc[$IncidentDate]);

                        // Some extra field dates look directly at the post value, so need to blank it out here.
                        $this->request->unSetParameter($IncidentDate);
                        unset($_POST[$IncidentDate]);
                    }
                }

                $this->response = $this->call('src\incidents\controllers\IncidentFormTemplateController', 'ShowIncidentForm',
                    array('data' => $inc));
            }

            obExit();
        }

        // Only show the "please wait" dialogue with the holding form
        // and if the user has not clicked "Save to complete later"
        if ($HoldingForm && $form_action != 'Pending' && bYN(GetParm('WEB_PROGRESS_BAR', 'Y'))
            && !$_SESSION['logged_in'])
        {
            $dtxtitle = _tk('title_save_incident');
            $WaitTitle = "<big>"._tk('inc_being_submitted')."</big>";
            $WaitSubTitle = _tk('inc_being_submitted_sub');

            $Template = new \Template(array('no_menu' => true));
            $Progress = template_header($WaitTitle, $WaitSubTitle, bYN(GetParm('WEB_PROGRESS_BAR', 'Y')), '', $Template);
        }

        // Manager field may have a suffix because of the section
        // it has been included in.  Look for all inc_mgr_<n> fields
        // up to a maximum of 9!
        for ($i = 1; $i < 10; $i++)
        {
            if ($this->request->getParameter('inc_mgr_'.$i) != '')
            {
                $this->request->setParameter('inc_mgr', \Sanitize::SanitizeString($this->request->getParameter('inc_mgr_'.$i)));
                break;
            }
        }

        /**
         * If the contact is linked as a reporter, we need to set the inc_repname and inc_reportedby fields to be the name
         * and type of this contact because these fields will not be on the form to be set.
         */
        if ($HoldingForm && bYN(GetParm('DIF_1_CONTACT_REP')))
        {
            for ($i = 1; $i <= \Sanitize::SanitizeInt($this->request->getParameter('increp_contacts')); $i++)
            {
                if ($this->request->getParameter('link_role_'.$i) == GetParm('REPORTER_ROLE', 'REP'))
                {
                    /*
                     * Do not encode special characters when saving data to DB.
                     * The encoding/decoding should only be used for data display purposes.
                     */
                    if ($this->request->getParameter('con_surname_' . $i) != '')
                    {
                        // Save the index to use later
                        $_SESSION['DIF_1_CONTACT_REP_INDEX'] = $i;

                        $this->request->setParameter('inc_repname', $this->request->getParameter('con_forenames_' . $i)." ".$this->request->getParameter('con_surname_' . $i));
                        $this->request->setParameter('inc_rep_tel', \Sanitize::SanitizeString($this->request->getParameter('con_tel1_' . $i)));
                        $this->request->setParameter('inc_rep_email', \Sanitize::SanitizeString($this->request->getParameter('con_email_' . $i)));

                        // Can choose which code field contains the title of the person reporting.
                        $ReportedBy = $_SESSION['Globals']['DIF_2_REPORTEDBY'];

                        if ($ReportedBy == '')
                        {
                            $ReportedBy = 'con_subtype';
                        }

                        $this->request->setParameter('inc_reportedby', code_descr('CON', $ReportedBy,
                            \Sanitize::SanitizeString($this->request->getParameter("{$ReportedBy}_$i")), '', false));
                    }

                    break;
                }
            }
        }

        $this->request->setParameter('inc_cost', str_replace(',', '', \Sanitize::SanitizeString($this->request->getParameter('inc_cost'))));
        $this->request->setParameter('inc_cost', str_replace('£', '', \Sanitize::SanitizeString($this->request->getParameter('inc_cost'))));
        $this->request->setParameter('inc_cost', str_replace('$', '', \Sanitize::SanitizeString($this->request->getParameter('inc_cost'))));

        if ($this->request->getParameter('inc_cost') == '')
        {
            $this->request->setParameter('inc_cost', null);
        }

        if ($this->request->getParameter('inc_time'))
        {
            $this->request->setParameter('inc_time', str_replace(':', '', \Sanitize::SanitizeString($this->request->getParameter('inc_time'))));
        }

        // Change inc_mgr to current user if inc_mgr field is POSTed (i.e. is on the form) blank.
        if ($this->request->getParameter('inc_mgr') !== null && $this->request->getParameter('inc_mgr') == ''
            && $DIFPerms != 'DIF1')
        {
            $this->request->setParameter('inc_mgr', $_SESSION['initials']);
        }

        // Do NPSA mapping
        if ($this->request->getParameter('inc_type') && $this->request->getParameter('inc_category')
            && $this->request->getParameter('inc_subcategory'))
        {
            $sql = '
                SELECT
                    npsa_map1, npsa_map2, npsa_map3
			    FROM
			        npsa_map
			    WHERE
			        npsa_type = \'IN05\'
                    AND
                    npsa_field1 = :inc_type
                    AND
                    npsa_field2 = :inc_category
                    AND
                    npsa_field3 = :inc_subcategory
            ';

            if ($row = PDO_fetch($sql, array(
                'inc_type' => $this->request->getParameter('inc_type'),
                'inc_category' => $this->request->getParameter('inc_category'),
                'inc_subcategory' => $this->request->getParameter('inc_subcategory')
            )))
            {
                $this->request->setParameter('inc_carestage', $row['npsa_map1']);
                $this->request->setParameter('inc_clin_detail', $row['npsa_map2']);
                $this->request->setParameter('inc_clintype', $row['npsa_map3']);
            }
        }

        $this->request->setParameter('inc_unit_type', $this->getUnitType());

        if (bYN(GetParm('AUTO_POPULATE_REC_NAME_INC', 'Y')))
        {
            $IncrepContacts = max($this->request->getParameter('contact_max_suffix'), $this->request->getParameter('increp_contacts'));

            for ($i = 1; $i<= $IncrepContacts; $i++)
            {
                if (!$this->request->getParameter('inc_name') && $this->request->getParameter("con_surname_$i")
                    && $this->request->getParameter("link_type_$i") == 'A')
                {
                    if (!ContactHidden($this->request->getParameters(), $i))
                    {
                        $this->request->setParameter('inc_name', \UnicodeString::substr(\UnicodeString::strtoupper(\Sanitize::SanitizeRaw($this->request->getParameter("con_surname_$i")) . ($this->request->getParameter("con_forenames_$i") ? ' ' . \Sanitize::SanitizeRaw($this->request->getParameter("con_forenames_$i")) : '')), 0, 32));
                    }
                }
            }
        }

        // New record.
        if ($this->request->getParameter('recordid') == '')
        {
            // New incident
            $NewIncident = true;
            $recordid = GetNextRecordID($main_table, true, 'recordid', $_SESSION['initials']);
            $recordid = \Sanitize::SanitizeInt($recordid);
            ResetFormSession();

            $DIF1_recordid = GetNextRecordID('incidents_report', true, 'recordid', $_SESSION['initials']);

            if (!$_SESSION['logged_in'])
            {
                $_SESSION['INC']['LOGGEDOUTRECORDHASH'] = getRecordHash($recordid);
            }

            $this->request->setParameter('inc_submittedtime', date('Hi'));

            if ($this->request->getParameter('inc_ourref') == '' && $HoldingForm)
            {
                $this->request->setParameter('inc_ourref', GetParm('DIF_1_REF_PREFIX').$DIF1_recordid);
            }
        }
        else
        {
            $NewIncident = false;
            $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
            DoFullAudit('INC', 'incidents_main', $recordid);
        }

        $inc = $this->request->getParameters();

        $inc['rep_approved'] = $approved;

        if (isset($inc['inc_time']) && $inc['inc_time'] != '')
        {
            $NoTimeBand = true;
            
            $Limits = \DatixDBQuery::PDO_fetch_all("SELECT code, lower_limit, upper_limit
                                                    FROM code_time_band
                                                    WHERE cod_priv_level = 'Y' OR cod_priv_level IS NULL OR cod_priv_level = ''
                                                    ORDER BY lower_limit");

            foreach ($Limits as $TimeBand)
            {
                // Deal with bands that cross midnight.
                if (intval($TimeBand['lower_limit']) > intval($TimeBand['upper_limit']))
                {
                    if (intval($TimeBand['upper_limit']) >= intval($inc['inc_time'])
                        || intval($TimeBand['lower_limit']) <= intval($inc['inc_time']))
                    {
                        $inc['inc_time_band'] = $TimeBand['code'];
                        $NoTimeBand = false;
                        break;
                    }
                }
                elseif (intval($TimeBand['lower_limit']) <= intval($inc['inc_time'])
                    && intval($TimeBand['upper_limit']) >= intval($inc['inc_time']))
                {
                    $inc['inc_time_band'] = $TimeBand['code'];
                    $NoTimeBand = false;
                    break;
                }
            }
            
            if ($NoTimeBand)
            {
                $inc['inc_time_band'] = '';
            }
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'INC', 'level' => ($HoldingForm ? 1 : 2)));
        $FormDesign->LoadFormDesignIntoGlobals();

        $inc = ParseSaveData(array('module' => 'INC', 'data' => $inc));

        $inc = ParseRootCauses(array('module' => 'INC', 'data' => $inc));

        if ($inc['inc_dreported'] == '' && $HoldingForm)
        {
            $inc['inc_dreported'] = GetTodaysDate(true);
        }

        $sql = "UPDATE incidents_main SET ";

        $generatedSQL = GeneratePDOSQLFromArrays(
            array(
                'FieldArray' => $ModuleDefs['INC']['FIELD_ARRAY'],
                'DataArray' => $inc,
                'Module' => 'INC',
                'end_comma' => true
            ),
            $bindParams
        );

        $sql .= $generatedSQL;

        if (!$this->request->getParameter('recordid'))
        {
            $sql .= ' submit_login = :submit_login, ';
            $bindParams['submit_login'] = $_SESSION['login'];
        }

        $sql .= ' updatedby = :updatedby,
              updateid = :updateid,
              updateddate = \'' . date('d-M-Y H:i:s') . '\'';

        $sql .= ' WHERE recordid = :recordid and (updateid = :current_updateid OR updateid IS NULL)';

        $bindParams['updatedby'] = $_SESSION['initials'];
        $bindParams['updateid'] = GensUpdateID($this->request->getParameter('updateid'));
        $bindParams['recordid'] = $recordid;
        $bindParams['current_updateid'] = $this->request->getParameter('updateid');

        // Send notifications for changed handler and investigator
        // BEFORE the incident record is updated.
        // Functionality currently protected by global STAFF_CHANGE_EMAIL
        if (!$HoldingForm && bYN(GetParm('STAFF_CHANGE_EMAIL', 'N')))
        {
            $inc['recordid'] = $recordid; //needed so recordid can appear in email.

            require_once 'Source/generic/EmailChangedStaff.php';
            EmailNewHandler($inc, 'INC');
            EmailNewInvestigators($inc, 'INC');
        }

        $query = new \DatixDBQuery($sql);
        $result = $query->prepareAndExecute($bindParams);

        if (!$result)
        {
            $error = _tk('inc_save_sql_err'). " $sql";
        }
        elseif ($query->getRowsAffected() == 0)
        {
            if ($HoldingForm)
            {
                $error = _tk('cannot_save_inc_err');
            }
            else
            {
                $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=incident&recordid='.$recordid.'">here</a> to return to the record';
            }
        }
        else
        {
            if ($NewIncident === true)
            {
                $DIF1_sql = "
                    UPDATE
                        incidents_report
                    SET
                        main_recordid = :recordid, ".
                        $generatedSQL."
                        updateid = :updateid,
                        updateddate = '" . date('d-M-Y H:i:s') . "',
                        updatedby = :updatedby
                    WHERE
                        recordid = :dif1recordid
                        AND
                        (updateid = :current_updateid OR updateid IS NULL)
                ";

                unset($bindParams['submit_login']);
                $bindParams['dif1recordid'] = $DIF1_recordid;
                $bindParams['current_updateid'] = $this->request->getParameter('updateid');
                $result = PDO_query($DIF1_sql, $bindParams);
            }

        }

        if ($error != '')
        {
            SaveError($error);
        }

        $FormPrefix = $_SESSION['Globals']['DIF_1_REF_PREFIX'];
        $inc['inc_ourref'] = \Sanitize::SanitizeRaw($this->request->getParameter('inc_ourref'));
        $inc['recordid'] = $recordid;

        //Save notepad
        require_once 'Source/libs/notepad.php';
        $error = SaveNotes(array(
            'id_field' => 'inc_id',
            'id' => $recordid,
            'notes' => $inc['notes'],
            'new' => $NewIncident
        ));

        //Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => 'INC',
            'data'   => $inc
        ));

        //Save Rejection Notes
        if (bYN(GetParm('REJECT_REASON', 'Y')) && $inc['rep_approved'] == 'REJECT')
        {
            $this->call('src\reasons\controllers\ReasonsController', 'SaveReason', array(
                'module'  => 'INc',
                'link_id' => $recordid,
                'data'    => $inc,
            ));
        }

        if ($recordid)
        {
            require_once 'Source/libs/UDF.php';
            SaveUDFs($recordid, MOD_INCIDENTS);
        }

        //Save linked records
        foreach ($ModuleDefs['INC']['LINKED_RECORDS'] as $aDetails)
        {
            $aDetails['module'] = 'INC';
            $aDetails['main_recordid'] = $recordid;
            $error = SaveLinkedRecords($aDetails);
        }

        // If holding table, check to see if we need to add any documents,
        if ($HoldingForm)
        {
            if ($Progress)
            {
                $Progress->ChangeProgressText('mainrecord', '<img src=\\\'Images/tickedbox.gif\\\'" /> '._tk('INCNameTitle').' saved.');
                $Progress->SetProgressBar(10);
                $Progress->ChangeProgressText('documents', '<b>Currently checking documents.</b>');
            }

            if ($SectionVisibility['extra_document'])
            {
                DocumentController::SaveDocsFromLevel1($recordid, 'INC');
            }

            if ($Progress)
            {
                $Progress->ChangeProgressText('documents', '<img src=\\\'Images/tickedbox.gif\\\' /> Documents saved.');
            }
        }

        // If holding table, check to see if we need to add any increp_contacts,
        if ($HoldingForm)
        {
            if ($Progress)
            {
                $Progress->SetProgressBar(20);
                $Progress->ChangeProgressText('contacts', '<b>Currently Checking contacts.</b>');
            }

            //save contacts attached on DIF1.
            require_once 'Source/contacts/SaveContact.php';
            SaveContacts(array('main_recordid' => $recordid, 'module' => 'INC', 'formlevel' => ($HoldingForm? 1 : 2)));

            //save equipment attached on DIF1.
            $this->SaveEquipment(array(
                'main_recordid' => $recordid,
                'module' => 'INC',
                'formlevel' => ($HoldingForm? 1 : 2)
            ));

            // If the user has logged in from the contacts table, add them as the reporter of the incident.
            if ($_SESSION["contact_login_id"] && bYN(GetParm("DIF_1_CONTACT_REP")) && $_SESSION['DIF_1_CONTACT_REP_INDEX'] != null)
            {
                $this->AddReporter($recordid);

                unset($_SESSION['DIF_1_CONTACT_REP_INDEX']);
            }

            if ($Progress)
            {
                $Progress->ChangeProgressText('contacts', '<img src=\\\'Images/tickedbox.gif\\\' /> Contacts saved.');
            }
        }

        $_SESSION['incident_number'] = "$recordid";

        if (bYN(GetParm('EXPORT_INCIDENT_TO_XML', 'N')) && $main_table == 'incidents_main')
        {
            require_once $ClientFolder.'/DATIXConfig.php';
            $client = new \SoapClient($ClientFolder.'/datixinterface.wsdl', array('trace' => 1, 'exceptions' => 0));
            $incidentdetails = $client->getIncident($_SESSION['login'], '', $_SESSION['session_id'], 'recordid='.$recordid);

            $export_path = GetParm('EXPORT_INCIDENT_TO_XML_PATH', 'soap/incidents');
            $bXMLWriteOk = (writeToFile($incidentdetails, "$export_path/INC_$recordid.xml"));

        }

        // Experimental code below can be used to send message to external web service,
        // but may be replaced with triggers in future
        if ($NewIncident === true && bYN(GetParm('EXPORT_INCIDENT_TO_WEBSERVICE', 'N')))
        {
            require_once $ClientFolder.'/DATIXConfig.php';
            $client = new \SoapClient($ClientFolder.'/datixinterface.wsdl', array('trace' => 1, 'exceptions' => 0));
            $incidentdetails = ($client->getIncident($_SESSION['login'], '', $_SESSION['session_id'], 'recordid='.$recordid));

            require_once $ClientFolder.'/external_src_config.php';
            $server = new \SoapClient($_SESSION['CON']['EXTERNAL_WSDL'], array('trace' => 1, 'exceptions' => 0));
            $ServerResponse = ($server->insertRecord(GetParm('WS_EXTERNAL_LOGIN', ''), GetParm('WS_EXTERNAL_PWD', ''), '', $incidentdetails));
        }

        if (ModIsLicensed('HOT'))
        {
            require_once 'Source/generic_modules/HOT/ModuleFunctions.php';
            InsertIntoHotspotQueue('INC', $recordid);
        }

        $AdminEmail = $_SESSION['Globals']['DIF_ADMIN_EMAIL'];

        if ($Progress)
        {
            $Progress->ChangeProgressText('email', '<b>Currently Emailing Notifications.</b>');
        }
        
        if ($queuingEmails && ($HoldingForm || $sendingEmailsOnUpdate))
        {
            // construct a message and attempt to publish it to the e-mail job queue
            $message = json_encode([
                'recordid'  => $inc['recordid'],
                'perms'     => $DIFPerms ?: 'NONE',
                'formlevel' => $this->get('formlevel'),
                'emailtype' => $NewIncident ? 'NEW': 'UPDATE'
            ]);
            
            try
            {
                (new MessageQueueFactory)
                    ->getQueue()
                    ->publish(MessageQueue::EMAIL, $message);
                
                if ($Progress)
                {
                    $Progress->SetProgressBar(50);
                    echo '<script>document.getElementById("waiting").style.display="none";</script>';
                    ob_flush();
                    flush();
                }
            }
            catch (\Exception $e)
            {
                // log exceptions when attempting to queue e-mails and revert to old in-line method
                $this->registry->getLogger()->logEmergency('Unable to queue notification e-mails.  Message content: '.
                    $message.'.  Error ('.get_class($e).'): '.$e->getMessage());
                
                $emailQueueFailed = true;
            }
        }

        $ajaxEmails = ((bYN(GetParm($ModuleDefs['INC']['SHOW_EMAIL_GLOBAL'], 'N')) === false) ||
            (bYN(GetParm('SEND_EMAIL_NEW_INC_2', 'N')) && !bYN(GetParm('SHOW_EMAIL_INC_2', 'N')) &&
                $this->request->getParameter('formlevel') == 2 && $NewIncident));
        
        if ((!$queuingEmails || $emailQueueFailed) && !$ajaxEmails)
        {
            if (bYN(GetParm('DIF_SHOW_EMAIL', 'N')) && ($HoldingForm || $NewIncident))
            {
                require_once 'Source/libs/Email.php';
                $this->request->setParameter('recordid', $recordid);
                $Output = SendEmails(array(
                    'module' => 'INC',
                    'data' => $inc,
                    'progressbar' => $Progress,
                    'from' => $inc['rep_approved_old'],
                    'to' => $inc['rep_approved'],
                    'perms' => ($DIFPerms? $DIFPerms: 'NONE'),
                    'level' => $this->request->getParameter('formlevel')
                ));
            }
            elseif ($sendingEmailsOnUpdate && !$HoldingForm && !$NewIncident)
            {
                require_once 'Source/libs/Email.php';
                $this->request->setParameter('recordid', $recordid);
                $Output = SendEmails(array(
                    'module' => 'INC',
                    'data' => $inc,
                    'progressbar' => $Progress,
                    'noApproval' => true,
                    'from' => $inc['rep_approved_old'],
                    'to' => $inc['rep_approved'],
                    'perms' => ($DIFPerms? $DIFPerms: 'NONE'),
                    'level' => $this->request->getParameter('formlevel'),
                    'notList' => $AlreadyEmailedContacts,
                    'emailType' => $NewIncident? 'NEW' : 'UPDATE'
                ));
            }
            else
            {
                if ($Progress)
                {
                    $Progress->SetProgressBar(50);
                    echo '<script>document.getElementById("waiting").style.display="none";</script>';
                    ob_flush();
                    flush();
                }
            }
        }
        else if ($Progress)
        {
            $Progress->SetProgressBar(50);
            echo '<script>document.getElementById("waiting").style.display="none";</script>';
            ob_flush();
            flush();
        }

        //Check whether we should email progress of incident to reporter
        //This should really go into "send emails" alongside everything else, but I didn't want to mess with the logic in case I broke something. It can wait for refactoring of the email functionality.
        if ($inc['rep_approved'] == 'FA' && $inc['rep_approved_old'] != 'FA' && GetParm('EMT_INC_PRO'))
        {
            $reportersToEmail = \DatixDBQuery::PDO_fetch_all('SELECT link_contacts.con_id FROM contacts_main JOIN link_contacts ON contacts_main.recordid = link_contacts.con_id WHERE link_contacts.inc_id = :inc_id AND link_notify_progress = \'Y\' AND link_role = \''.GetParm('REPORTER_ROLE', 'REP').'\' AND contacts_main.con_email != \'\' AND contacts_main.con_email IS NOT NULL', array('inc_id' => $inc['recordid']), \PDO::FETCH_COLUMN);

            if (count($reportersToEmail) > 0)
            {
                require_once 'Source/libs/Email.php';
                SendProgressEmailToReporters(array('module' => 'INC', 'data' => $inc), $reportersToEmail);
            }
        }

        if ($Progress && (!$queuingEmails || $emailQueueFailed))
        {
            $Progress->ChangeProgressText('email', '<img src=\\\'Images/tickedbox.gif\\\' /> Emails Sent.');
        }

        if ($this->request->getParameter('form_id') && is_numeric($this->request->getParameter('form_id')))
        {
            $URLSuffix = '&form_id='.$this->request->getParameter('form_id');
        }

        $UnapprovedContactArray = GetUnapprovedContacts(array('module' => 'INC', 'recordid' => $recordid));

        // If we are about to merge the record with a document template, now is the time to do that.
        if ($_GET['merge_document_after_save'])
        {
            $this->response = $this->call('src\wordmergetemplate\controllers\WordMergeTemplateController', 'mergedocument', array(
                'select_template' => $this->request->getParameter('merge_document_after_save'),
                'recordid' => $inc['recordid'],
                'selected_contact' => $this->request->getParameter('selected_contact')
            ));
            return;
        }

        if ($HoldingForm && bYN(GetParm('SAVE_DIF1_SNAPSHOT', 'N')) && $inc['rep_approved'] != 'STCL') //we need to save a copy of the printed form for audit purposes
        {
            //This is pretty nasty: there are lots of globals that are used to keep track of things like the progress bar, and whether we're actually
            //outputting stuff for this template. These are all icky legacy bits and we need to get round them so we can catch the templated output of this
            //controller without the template getting messed up by stuff happening further up the save.
            $dtxwaitTemp = $dtxwait;
            $dtxwait = '';
            $dtxheadingTemp = $GLOBALS['dtxheading'];
            $GLOBALS['dtxheading'] = '';
            $yyheaderdoneTemp = $yyheaderdone;
            $yyheaderdone = 0;
            $_GET["print"] = 1;
            $_REQUEST["submitandprint"] = 1; //needed to allow HashesMatch check to work for e.g documents section.
            $inc['dif1_snapshot'] = 1; //needed to force contact data to be pulled from the post, maintaining original link types.
            ob_start();
            $this->call('src\incidents\controllers\IncidentFormTemplateController', 'ShowIncidentForm',
                array(
                    'form_action' => 'Print',
                    'level' => 1,
                    'data' => $inc
                )
            );

            /**
             * Store DIF1 html in a variable
             */

            $dif_content = ob_get_clean();

            /**
             * get styling links from the globals variable that holds them
             */

            $style_links = $GLOBALS['dtx_system_css'];

            /**
             * pull out the file names from the style links string ignoring print files and put into array
             */

            preg_match_all('/<link.*?href=(?:"|\')([a-z0-9\/\._-]+\.css)(?:\?v=[0-9]{10})?(?:"|\')(?! media=(?:"|\')print(?:"|\')).*?\/>/i', $style_links, $files);

            /**
             * Start generating inline style tag for styles
             */
            $css_code = '<style type="text/css">';

            /**
             * Output content from style files into the inline tag
             */

            foreach($files[1] as $file)
            {
                $css_code .= file_get_contents($file);
            }

            /**
             * close style tag
             */

            $css_code .= '</style>';

            /**
             * cleanup the css - the positioning of these replacements is important as some regex rely on a previous one
             * to a degree. for instance removing comments needs to happen before removing empty rules as otherwise it
             * will see the rule as not empty even if all items are commented out
             */

            $css_output = preg_replace(array(
                '/\/\*+.*?(?<=\*)\//isu', // removes comments
                '/[^\}]+\{[\s]*\}/isu', // remove any empty rules,
                '/((?<=({|;|,))[\s]+|[\s]+(?={))/isu' // put css rules on a single line
            ),
            ''
            , $css_code);

            //'/(?<!@media [a-z]+(?<!\r)[\s]+{)(?<={|;|,)(?<!\r)[\s]+|(?<!@media [a-z]+)(?<!\r)[\s]+(?={)/isu'//, // makes css rules show on all one line unless they're and @media rule, but it doesn't work

            if(preg_last_error() != PREG_NO_ERROR)
            {
                $css_output = $css_code;
            }

            /**
             * put the inline css style code into the DIF1 html after the head tag
             */

            $combined = \UnicodeString::substr_replace($dif_content, $css_output, stripos($dif_content, '<head>') + 6, 0);

            /**
             * cleanup the html - the positioning of these replacements is important as some regex rely on a previous one
             * to a degree. for instance removing comments needs to happen before removing extra line breaks, as otherwise
             * there will be new empty lines added from the removed comments
             */

            $html = preg_replace(array(

                '/^[\s]+/isu', // removes whitespace from the start
                '/\/\*+.*?(?<=\*)\//isu', // removes comments
                '/<script[^>]*>.*?<\/script>/isu', // removes script tags and content
                '/<link[^>]*>/isu', // removes link tags
                '/on(mousemove|click|change|blur|load|submit)+="[^"]*"/isu', // removes javascript on attributes
                '/<input type="hidden"[^>]+>/isu', // removes hidden inputs
                '/<div class="title_rhs_container">[\s]*<a href="javascript:ToggleFormDesignSection\(\'[^\']+\'\);">[\s]*<img id=\'twisty_image_[^\']+\' src="Images\/collapse\.gif" alt="\+" border="0"\/>[\s]*<\/a>[\s]*<\/div>/isu', // removes toggle buttons
                '/href="javascript:[^"]*"/isu', // remove javascript in href attributes
                '/([\r\n][\s]*)+/isu', // removes extra whitespace
                '/([ ]{4}|\t)+/isu' // removes indentation using spaces and tabs
            ), array(
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                "\r\n", // replace with windows newline syntax
                '' // replace with nothing, probably not needed as a mismatch in the search and replace array will result in the replace being '' anyway
            ), $combined);

            /**
             * if there was no error in the preg_replace output the updated html, otherwise use the original code generated
             */

            if(preg_last_error() == PREG_NO_ERROR)
            {
                $output = $html;
            }
            else
            {
                $output = $dif_content;
            }

            \DatixDBQuery::PDO_query('INSERT INTO inc_submission_snapshot (recordid, recorddata, createddate, createdby) VALUES (:recordid, :recorddata, :createddate, :createdby)',
                array('recordid' => $inc['recordid'],
                    'recorddata' => $output,
                    'createddate' => date('Y-m-d H:i:s'),
                    'createdby' => ($_SESSION['CurrentUser'] instanceof User ? $_SESSION['CurrentUser']->initials : '')));
            //reset globals
            $yyheaderdone = $yyheaderdoneTemp;
            $dtxwait = $dtxwaitTemp;
            $GLOBALS['dtxheading'] = $dtxheadingTemp;
            unset($_GET["print"]);
            unset($_REQUEST["submitandprint"]);
            unset($inc['dif1_snapshot']);
        }

        // Discount unapproved contacts if they are hidden, since the user will not be able to do anything about them.
        if (is_array($UnapprovedContactArray))
        {
            IncludeCurrentFormDesign('INC', 2);

            foreach ($UnapprovedContactArray as $key => $ContactDetails)
            {
                if ($GLOBALS['HideFields']['contacts_type_'.$ContactDetails['link_type']])
                {
                    unset($UnapprovedContactArray[$key]);
                }
            }
        }

        // This was moved here because when we have unapproved contacts in a record the actions chains were not being attached
        if (bYN(GetParm('WEB_TRIGGERS', 'N')))
        {
            require_once 'Source/libs/Triggers.php';
            ExecuteTriggers($recordid, $inc, $ModuleDefs['INC']['MOD_ID']);
        }

        if (CheckUnapprovedContactRedirect(array('from' => $this->request->getParameter('rep_approved_old'), 'to' => $this->request->getParameter('rep_approved'), 'module' => 'INC', 'access_level' => $DIFPerms))
            && count($UnapprovedContactArray) > 0 && $_SESSION['logged_in']) // Warn for unapproved contacts
        {
            DoUnapprovedContactRedirect(array(
                'module' => 'INC',
                'recordid' => $recordid,
                'contact_array' => $UnapprovedContactArray
            ));
        }
        else
        {
            $this->response = $this->call('src\generic\controllers\SaveRecordTemplateController', 'ShowSaveRecord', array(
                'aParams' => array(
                    'module' => 'INC',
                    'data' => $inc,
                    'message' => $Output,
                    'new_record' => $NewIncident
                ),
                'holding_form' => $this->request->getParameter('holding_form'),
                'email_queue_failed' => $emailQueueFailed
            ));
            obExit();
        }
    }

    /**
     * Only called if the current user has logged in from the contacts table.
     * Function checks to see if a link_contacts record has already been added with role REP - if not, it adds one.
     *
     * @param $RecordID
     */
    protected function AddReporter($RecordID)
    {
        $sql = "
            SELECT
                link_recordid
	        FROM
	            link_contacts
			WHERE
			    inc_id = :inc_id
                AND
                link_type = :link_type
                AND
                link_role = :link_role
        ";

        $result = \DatixDBQuery::PDO_fetch($sql, array(
            'inc_id' => $RecordID,
            'link_type' => 'N',
            'link_role' => GetParm('REPORTER_ROLE', 'REP')
        ));

        if (!$result)
        {
            $DIFPerms = GetParm('DIF_PERMS');

            $sql = "
                INSERT INTO
                    link_contacts
                    (inc_id, link_type, link_role, con_id)
                VALUES
                    (:inc_id, :link_type, :link_role, :con_id)
            ";

            \DatixDBQuery::PDO_insert($sql, array(
                'inc_id' => $RecordID,
                'link_type' => 'N',
                'link_role' => GetParm('REPORTER_ROLE', 'REP'),
                'con_id' => $_SESSION['contact_login_id']
            ));
        }
    }

    /**
     * Validates dates, times and money fields of an incident record.
     *
     * @return array The array that contains the validation error, if any, or an empty array if validation is ok.
     */
    protected function ValidateIncidentData()
    {
        require_once 'Date.php';

        if (bYN(GetParm('DETECT_TIMEZONES', 'N')))
        {
            $Today = new \DateTime();
            $Today->setTimezone(new \DateTimeZone('UTC'));
    
            $Today = new \Date($Today->format('Y-m-d H:i:s'));
            $Today->setTZbyID('Etc/GMT');
            $Today->convertTZbyID('Etc/GMT'.($_SESSION['Timezone'] < 0 ? '' : '+').$_SESSION['Timezone']);
        }
        else
        {
            $Today = new \DateTime();
            $Today = new \Date($Today->format('Y-m-d H:i:s'));
        }

        $errorDates = ValidatePostedDates('INC');
        $errorTimes = ValidatePostedTimes('INC');
        $errorMoney = ValidatePostedMoney('INC');

        $error = array_merge($errorDates, $errorTimes, $errorMoney);

        if ($this->request->getParameter('inc_dincident') != '' && $this->request->getParameter('inc_time') != '')
        {

            $Date = new \Date(UserDateToSQLDate($this->request->getParameter('inc_dincident')));

            if ($Date->year == $Today->year && $Date->month == $Today->month && $Date->day == $Today->day)
            {
                $Time = $Today->format('%H:%M');

                if (intval(str_replace(':','',$Time)) < intval(str_replace(':','',$_POST['inc_time'])))
                {
                    $error['inc_time'] = 'If '.GetFormFieldLabel('inc_dincident','','INC').' is today, '.
                        GetFormFieldLabel('inc_time','','INC').' cannot be later than current time';
                }
            }
        }

        $cost = $this->request->getParameter('inc_cost');
        $cost = str_replace(',', '', $cost);
        $cost = str_replace('£', '', $cost);
        $cost = str_replace('$', '', $cost);
        $cost = str_replace('.', '', $cost);
        $cost = str_replace(',', '', $cost);

        // act_cost - MS SQL decimal(13,2);
        if (\UnicodeString::strlen($cost) > 13)
        {
            $error['message'] = '<br/>' . GetFormFieldLabel('inc_cost','','INC').' cannot be longer than 13 digits.';
            $error['inc_cost'] = GetFormFieldLabel('inc_cost','','INC').' cannot be longer than 13 digits.';
        }

        return $error;
    }

    /**
     * Determines the unit type using the same algorithm as the rich client
     * i.e. locactual overrides unit overrides the global.
     *
     * @return string $unitType
     */
    protected function getUnitType()
    {
        if (!$this->request->getParameter('inc_locactual') && $this->request->getParameter('recordid'))
        {
            // Check value from DB if field is hidden and we're logged in
            $sql = 'SELECT inc_locactual FROM incidents_main WHERE recordid = :recordid';
            $locactual = \DatixDBQuery::PDO_fetch($sql, array(
                'recordid' => $this->request->getParameter('recordid')
            ), \PDO::FETCH_COLUMN);
        }
        else
        {
            $locactual = $this->request->getParameter('inc_locactual');
        }

        if (!$this->request->getParameter('inc_unit') && $this->request->getParameter('recordid'))
        {
            // Check value from DB if field is hidden and we're logged in
            $sql = 'SELECT inc_unit FROM incidents_main WHERE recordid = :recordid';
            $unit = \DatixDBQuery::PDO_fetch($sql, array(
                'recordid' => $this->request->getParameter('recordid')
            ), \PDO::FETCH_COLUMN);
        }
        else
        {
            $unit = $this->request->getParameter('inc_unit');
        }

        if ($locactual != '')
        {
            $sql = 'SELECT cod_npsa FROM code_locactual WHERE code = :inc_locactual';

            $unitType = \DatixDBQuery::PDO_fetch($sql, array('inc_locactual' => $locactual), \PDO::FETCH_COLUMN);
        }

        if ($unitType == '' && $unit != '')
        {
            $sql = 'SELECT cod_npsa FROM code_unit WHERE code = :inc_unit';

            $unitType = \DatixDBQuery::PDO_fetch($sql, array('inc_unit' => $unit), \PDO::FETCH_COLUMN);
        }

        if ($unitType == '')
        {
            $unitType = $_SESSION['Globals']['NPSA_SERVICE'];
        }

        // Truncate inc_unit_type value to 6 chars otherwise this will cause a SQL error and the record will be corrupted
        if (strlen($unitType) > 6)
        {
            $unitType = substr($unitType, 0, 5);
        }

        return $unitType;
    }

    /**
     * Saves an equipment attached to a DIF1.
     *
     * @param array $aParams
     */
    protected function SaveEquipment($aParams)
    {
        $MaxSuffix = $this->request->getParameter('equipment_max_suffix');

        for ($i = 0; $i <= $MaxSuffix; $i++)
        {
            $aParams['suffix'] = $i;

            if ($aParams['suffix'])
            {
                $aParams['suffixstring'] = '_'.$i;
            }

            if (!AssetsController::EquipmentHidden($this->request->getParameters(), $i, $aParams['module']))
            {
                $this->request->setParameter('ast_recordid'.$aParams['suffixstring'], \Sanitize::SanitizeString($this->request->getParameter('ast_id_'.$i)));

                $this->response = $this->call('src\ast\controllers\AssetsController', 'SaveLinkedEquipment', array(
                    'aParams' => $aParams,
                    'ast' => $this->request->getParameters(),
                    'no_redirect' => true //SaveLinkedEquipment is called from other parts of the code that need a post-save redirect. TODO: Get rid of this hack
                ));
            }
        }
    }
    function preg_errtxt($errcode)
    {
        static $errtext;

        if (!isset($errtxt))
        {
            $errtext = array();
            $constants = get_defined_constants(true);
            foreach ($constants['pcre'] as $c => $n) if (preg_match('/_ERROR$/', $c)) $errtext[$n] = $c;
        }

        return array_key_exists($errcode, $errtext)? $errtext[$errcode] : NULL;
    }
    /*
     * This function gets the contacts that were emailed in the last iteration of this record save.
     * It currently has 2 main execution paths, if $recordUpdateEmail == 'Y' then it essentially uses
     * the SendEmails function with the old record data. If set to 'Z' then it will use the new prototype
     * database table.
     *
     * @param   $recordid           The Incident recordid.
     * @param   $perms              The logged in users record permissions for incidents.
     * @param   $level              The level of the incident form (should be 2).
     * @param   $recordUpdateEmail  This variable holds the GLOBAL value for RECORD_UPDATE_EMAIL.
     * @return  array               An array of contact ID's already emailed in the last incident save iteration
     */
    public function GetAlreadyEmailedContacts($recordid, $perms, $level)
    {
        $updatedDateSQL = 'SELECT updateddate FROM incidents_main WHERE recordid = :recordid';
        $updatedDate = new \DateTime(\DatixDBQuery::PDO_fetch($updatedDateSQL, ['recordid' => $recordid])['updateddate']);

        $migrateDateString = $this->registry->getParm('RECORD_UPDATE_EMAIL_DATETIME');
        $migrateDateIsValid = $migrateDateString != '' && preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}(\.\d{3})?/', $migrateDateString) === 1;
        $migrateDate = new \DateTime($migrateDateString);

        if ($migrateDateIsValid && $migrateDate > $updatedDate)
        {
            require_once 'Source/libs/Email.php';
            return SendEmails(array(
                'recordid' => $recordid,
                'module' => 'INC',
                'perms' => $perms,
                'noApproval' => true,
                'level' => $level,
                'falseSend' => true
            ));
        }
        else
        {
            return (new UserModelFactory)->getMapper()->getNotificationEmailRecipients($recordid);
        }
    }

    /*
     * Checks whether or not the system is configured for sending update emails or not.
     * Requires that RECORD_UPDATE_EMAIL is set to 'Y' and that RECORD_UPDATE_EMAIL_DATE is
     * set as an acceptable format for DateTime.
     *
     * @return  bool
     */
    protected function shouldRecordUpdateEmail()
    {
        $shouldRecordUpdateEmail = false;
        $recordUpdateEmail = bYN($this->registry->getParm('RECORD_UPDATE_EMAIL'));

        if($recordUpdateEmail)
        {
            try
            {
                new \DateTime($this->registry->getParm('RECORD_UPDATE_EMAIL_DATETIME'));
                $shouldRecordUpdateEmail = true;
            }
            catch (\Exception $e)
            {
                $shouldRecordUpdateEmail = false;
            }
        }

        return $shouldRecordUpdateEmail;
    }

    /*
     *  This steals some code from BlankOutPostValues (Subs) to deal with medications which needs special
     *  functionality *sigh*. I've stripped out parts on the original function to the minimum requirements.
     *
     * @param $module           the module used for getting the form design
     * @param $level            the level of the form, used for getting the form design
     * @param $parentModule     the module of the parent form design, used for getting the form design
     * @param $parentLevel      the level of the parent form design, used for getting the form design
     * @param $request          the request object, which is also 'blanked out' if provided
     */
    public function BlankOutMedicationValues($module, $level, $parentModule = null, $parentLevel = null, $request = null)
    {
        global $ModuleDefs, $SectionVisibility, $HideFields, $DefaultValues;

        if($_POST['inc_medication_max_suffix'] > 0)
        {
            $FormDesign = \Forms_FormDesign::GetFormDesign(array(
                'module' => $module,
                'level' => $level,
                'parent_module' => $parentModule,
                'parent_level' => $parentLevel
            ));

            $FormFile = GetBasicFormFileName($module, $level);
            $FormArray = null;
            require $FormFile;

            $SectionArray = $FormArray['multi_medication_design'];

            for ($i = 1; $i <= $_POST['inc_medication_max_suffix']; $i++)
            {
                if ($SectionVisibility['multi_medication_record'] != true)
                {
                    foreach ($SectionArray['Rows'] as $Field)
                    {
                        if (is_array($Field))
                        {
                            $FieldName = $Field['Name'] . '_' . $i;
                        }
                        else
                        {
                            $FieldName = $Field . '_' . $i;
                        }

                        if ($FieldName)
                        {
                            unset($_POST[$FieldName]);
                            if (!is_null($request))
                            {
                                $request->unSetParameter($FieldName);
                            }
                        }
                    }
                }
                else
                {
                    foreach ($SectionArray['Rows'] as $Field)
                    {
                        if (is_array($Field))
                        {
                            $FieldNameSuffix = $Field['Name'] . '_' . $i;
                            $FieldName = $Field['Name'];
                        }
                        else
                        {
                            $FieldNameSuffix = $Field . '_' . $i;
                            $FieldName = $Field;
                        }

                        if (!($FormDesign->HideFields[$FieldName] && isset($FormDesign->DefaultValues[$FieldName])))
                        {
                            if (!$this->get('show_field_' . $FieldNameSuffix))
                            {
                                unset($_POST[$FieldNameSuffix]);
                                if (!is_null($request))
                                {
                                    $request->unSetParameter($FieldNameSuffix);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
