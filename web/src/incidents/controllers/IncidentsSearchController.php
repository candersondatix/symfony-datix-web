<?php

namespace src\incidents\controllers;

use src\framework\controller\Controller;

class IncidentsSearchController extends Controller
{
    function incidentssearch()
    {
        global $FormType;

        $FormType = 'Search';

        $_SESSION['INC']['PROMPT']['VALUES'] = array();

        if ($this->request->getParameter('validationerror'))
        {
            $message = _tk('search_invalid_chars_err');
        }
        else
        {
            $_SESSION['INC']['UDFARRAY'] = null;
        }

        if ($this->request->getParameter('searchtype') == 'lastsearch')
        {
            $inc = $_SESSION['INC']['LAST_SEARCH'];
        }
        else
        {
            $inc = '';
        }

        $this->response = $this->call('src\incidents\controllers\IncidentFormTemplateController', 'ShowIncidentForm',
            array(
                'form_action' => 'search',
                'data' => $inc
            )
        );
    }
}