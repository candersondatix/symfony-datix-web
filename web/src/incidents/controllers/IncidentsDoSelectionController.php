<?php

namespace src\incidents\controllers;

use src\framework\controller\Controller;
use src\reports\exceptions\ReportException;

class IncidentsDoSelectionController extends Controller
{
    function incidentsdoselection()
    {
        $FormAction = $this->request->getParameter('rbWhat');
        $FieldWhere = '';
        $Error = false;
        $search_where = array();
        ClearSearchSession('INC');

        if ($FormAction == 'Cancel' || $FormAction == BTN_CANCEL)
        {
            if (isset($_SESSION['security_group']['grp_id']))
            {
                $RedirectUrl = '?action=editgroup' .
                    '&grp_id=' . $_SESSION['security_group']['grp_id'] .
                    '&module=' . $_SESSION['security_group']['module'];
                $_SESSION['security_group']['success'] = false;
            }
            elseif (isset($_SESSION['packaged_report']))
            {
                $RedirectUrl = '?action=designapackagedreport';

                if ($_SESSION['packaged_report']['recordid'])
                {
                    $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
                }
                else
                {
                    $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
                }

                $_SESSION['packaged_report']['success'] = false;
            }
            elseif ($this->request->getParameter('qbe_recordid') != '')
            {
                // query by example for generic modules
                $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module')
                    . '&recordid=' . $this->request->getParameter('qbe_recordid');
            }
            else
            {
                $RedirectUrl = '?module=INC';
            }

            $this->redirect('app.php' . $RedirectUrl);
        }

        // construct and cache a new Where object
        try
        {
            $whereFactory = new \src\framework\query\WhereFactory();
            $where = $whereFactory->createFromRequest(new \src\framework\controller\Request());

            if (empty($where->getCriteria()['parameters'])) {
                $where = null;
            }

            $_SESSION['INC']['NEW_WHERE'] = $where;
        }
        catch(\InvalidDataException $e)
        {
            $Error = true;
            $this->registry->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');
        }
        catch(ReportException $e)
        {
            $Error = true;
        }

        // Find all values which start with ram_ and add them to array
        // if they are not empty.
        // To do: parse names.
        $inc = \Sanitize::SanitizeRawArray($this->request->getParameters());
        $_SESSION['INC']['LAST_SEARCH'] = $inc;

        $table = 'incidents_main';

        while (list($name, $value) = each($inc))
        {
            $explodeArray = explode('_', $name);

            if ($explodeArray[0] == 'UDF')
            {
                if (MakeUDFFieldWhere($explodeArray, $value, MOD_INCIDENTS, $FieldWhere) === false)
                {
                    $Error = true;
                    break;
                }
            }
            elseif ($explodeArray[0] == 'incrootcause')
            {
                if (MakeFieldWhere('INC', 'inc_root_causes', $value, $FieldWhere, $TablePrefix) === false)
                {
                    $Error = true;
                    break;
                }
            }
            elseif ($explodeArray[0] == 'caf')  //hack to allow searching on linked causal factors.
            {
                if (MakeFieldWhere('INC', $name, $value, $FieldWhere, 'causal_factors') === false)
                {
                    $Error = true;
                    break;
                }

                if ($FieldWhere)
                {
                    $CausalFactorsFieldWhere[] = $FieldWhere;
                }

                continue;
            }
            elseif ($explodeArray[0] == 'imed')  //hack to allow searching on multi-medications.
            {
                if($FormAction == 'Search')
                {
                    $name = preg_replace('/_search$/i', '', $name);
                }


                if (MakeFieldWhere('INC', $name, $value, $FieldWhere, 'inc_medications') === false)
                {
                    $Error = true;
                    break;
                }

                if ($FieldWhere)
                {
                    $MultiMedicationsFieldWhere[] = $FieldWhere;
                }

                continue;
            }
            elseif ($explodeArray[0] == 'searchtaggroup')
            {
                // @prompt on tags is not yet supported
                if (preg_match('/@prompt/iu',$value) == 1)
                {
                    $search_where[] = '1=2';
                }
                elseif ($value != '')
                {
                    $field = str_replace('searchtaggroup_'.$explodeArray[1].'_', '', $name);

                    // Need to find the codes associated with the selected tags.
                    $sql = '
                        SELECT DISTINCT
                            code
                        FROM
                            code_tag_links
                        WHERE
                            [group] = :group
                            AND
                            [table] = :table
                            AND
                            field = :field
                            AND
                            tag IN (\''.implode('\',\'', array_map('intval', explode('|', $value))).'\')
                    ';

                    $Codes = \DatixDBQuery::PDO_fetch_all($sql, array(
                        'group' => intval($explodeArray[1]),
                        'field' => $field,
                        'table' => 'incidents_main'
                    ), \PDO::FETCH_COLUMN);

                    if (!empty($Codes))
                    {
                        $search_where[] = $field.' IN (\''.implode('\',\'', $Codes).'\')';
                    }
                    else
                    {
                        // This tag hasn't been attached to any codes, so can't return any results.
                        $search_where[] = '1=2';
                    }
                }
            }
            else
            {
                if (MakeFieldWhere('INC', $name, $value, $FieldWhere, $table) === false)
                {
                    $Error = true;
                    break;
                }
            }

            if ($FieldWhere != '')
            {
                if (CheckForCorrectAtCodes($FieldWhere))
                {
                    $search_where[$name] = $FieldWhere;
                }
                else
                {
                    AddMangledSearchError($name, $value);
                    $Error = true;
                }
            }
        }

        // Combine linked causal factors search terms into a single subquery.
        if (!empty($CausalFactorsFieldWhere))
        {
            $search_where[] = $table.'.recordid IN (SELECT inc_id FROM causal_factors WHERE '.implode(" AND ", $CausalFactorsFieldWhere).')';
        }
        if (!empty($MultiMedicationsFieldWhere))
        {
            $search_where[] = $table.'.recordid IN (SELECT inc_id FROM inc_medications WHERE '.implode(" AND ", $MultiMedicationsFieldWhere).')';
        }

        if (!$Error && !ValidateWhereClause(implode(' AND ', $search_where), 'INC'))
        {
            $Error = true;
        }

        if ($Error === true)
        {
            AddSessionMessage('ERROR', _tk('search_errors'));

            if ($this->request->getParameter('udfRowArray'))
            {
                $_SESSION['INC']['UDFARRAY'] = $this->request->getParameter('udfRowArray');
            }

            $RedirectUrl = '?action=incidentssearch&searchtype=lastsearch';
            $this->redirect('app.php' . $RedirectUrl);
        }

        $_SESSION['INC']['UDFARRAY'] = NULL;

        $Where = '';

        if (empty($search_where) === false)
        {
            $Where = implode(' AND ', $search_where);
        }

        $_SESSION['INC']['WHERE'] = $Where;
        $_SESSION['INC']['TABLES'] = $table;

        // We are doing a new search so unset LASTQUERYID
        unset($_SESSION['INC']['LASTQUERYID']);

        if (isset($_SESSION['security_group']['grp_id']))
        {
            $RedirectUrl = '?action=editgroup' .
                '&grp_id=' . $_SESSION['security_group']['grp_id'] .
                '&module=' . $_SESSION['security_group']['module'];
            $_SESSION['security_group']['success'] = true;
        }
        elseif (isset($_SESSION['packaged_report']))
        {
            $RedirectUrl = '?action=designapackagedreport';

            if ($_SESSION['packaged_report']['recordid'])
            {
                $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
            }
            else
            {
                $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
            }

            $_SESSION['packaged_report']['success'] = true;
        }
        elseif ($this->request->getParameter('qbe_recordid') != '')
        {
            // query by example for generic modules
            $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module') .
                '&recordid=' . $this->request->getParameter('qbe_recordid') . '&qbe_success=true';
        }
        else
        {
            $RedirectUrl = '?action=list&module=INC&listtype=search';
            $_SESSION['INC']['SEARCHLISTURL'] = $RedirectUrl;
        }

        $this->redirect('app.php' . $RedirectUrl);
    }
}