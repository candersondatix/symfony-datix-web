<?php

namespace src\incidents\controllers;

use src\framework\controller\Controller;

class AddNewIncidentController extends Controller
{
    function addnewincident()
    {
        // Add reporter details
        if (bYN(GetParm('DIF2_REPORTER_POPULATE')))
        {
            $inc = SetUpDefaultReporterINC();
        }

        // Set default locations
        if ($_SESSION['sta_orgcode'] != '')
        {
            $inc['inc_organisation'] = $_SESSION['sta_orgcode'];
        }

        if ($_SESSION['sta_unit'] != '')
        {
            $inc['inc_unit'] = $_SESSION['sta_unit'];
        }

        if ($_SESSION['sta_clingroup'] != '')
        {
            $inc['inc_clingroup'] = $_SESSION['sta_clingroup'];
        }

        if ($_SESSION['sta_directorate'] != '')
        {
            $inc['inc_directorate'] = $_SESSION['sta_directorate'];
        }

        if ($_SESSION['sta_specialty'] != '')
        {
            $inc['inc_specialty'] = $_SESSION['sta_specialty'];
        }

        $this->response = $this->call('src\incidents\controllers\IncidentFormTemplateController', 'ShowIncidentForm',
            array('data' => $inc)
        );
    }
}