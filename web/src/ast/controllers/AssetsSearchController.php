<?php

namespace src\ast\controllers;

use src\framework\controller\Controller;

class AssetsSearchController extends Controller
{
    /**
     * Shows the Equipment search form.
     */
    function assetssearch()
    {
        if ($this->request->getParameter('searchtype') == 'lastsearch')
        {
            $ast = $_SESSION['AST']['LAST_SEARCH'];
        }
        else
        {
            $ast = '';
        }

        if ($this->request->getParameter('validationerror'))
        {
            $ast['error']['message'] = '<br />There are invalid characters in one of the fields you have searched on.';
        }

        $this->call('src\ast\controllers\ShowAssetFormTemplateController', 'showassetform', array(
            'FormType' => 'Search',
            'ast' => $ast
        ));
    }
}