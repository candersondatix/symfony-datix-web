<?php

namespace src\ast\controllers;

use src\framework\controller\Controller;

class ShowAssetController extends Controller
{
    /**
     * Shows a previously added Equipment.
     */
    function asset()
    {
        global $ast;

        $ModuleDefs = $this->registry->getModuleDefs();

        $module = \Sanitize::getModule($this->request->getParameter('module'));
        $link_id = \Sanitize::SanitizeInt($this->request->getParameter('link_id'));
        $ast_id = \Sanitize::SanitizeInt($this->request->getParameter('ast_id'));
        $main_recordid = \Sanitize::SanitizeInt($this->request->getParameter('main_recordid'));

        if (!$ast_id && $this->request->getParameter('recordid'))
        {
            $ast_id = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        }

        if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')))
        {
        	require_once 'Source/security/SecurityBase.php';
        	$ASTPerms = GetUserHighestAccessLvlForRecord('AST_PERMS', 'AST', $ast_id);
        }
        else
        {
        	$ASTPerms = GetParm('AST_PERMS');
        }

        $link_info = array(
            'SAB' => array(
                'foreignkey' =>'sab_id',
                'linktable' => 'sabs_main'
            ),
            'INC' => array(
                'foreignkey' =>'inc_id',
                'linktable' => 'incidents_main'
            ),
            'RAM' => array(
                'foreignkey' =>'ram_id',
                'linktable' => 'ra_main'
            ),
            'TRN' => array(
                'foreignkey' =>'crs_id',
                'linktable' => 'courses_main'
            )
        );

        if ($ast_id)
        {
            // Existing piece of equipment
            $sql = "
                SELECT
                    assets_main.recordid as ast_id, assets_main.updateid, assets_main.rep_approved,
                    assets_main.ast_name, assets_main.ast_ourref, assets_main.ast_organisation, assets_main.ast_unit,
                    assets_main.ast_clingroup, assets_main.ast_directorate, assets_main.ast_directorate, assets_main.ast_specialty,
                    assets_main.ast_loctype, assets_main.ast_locactual, assets_main.ast_type, assets_main.ast_product,
                    assets_main.ast_model, assets_main.ast_manufacturer, assets_main.ast_supplier, assets_main.ast_catalogue_no,
                    assets_main.ast_batch_no, assets_main.ast_serial_no, assets_main.ast_dmanufactured, assets_main.ast_dputinuse,
                    assets_main.ast_cemarking, assets_main.ast_location, assets_main.ast_quantity, assets_main.ast_dlastservice,
                    assets_main.ast_dnextservice, assets_main.ast_descr, assets_main.ast_category, assets_main.ast_cat_other_info, notes
                FROM
                    assets_main
                    LEFT JOIN
                    notepad on assets_main.recordid = notepad.ast_id
            ";

            $where[] = "assets_main.recordid = :recordid";

            $WhereClause = MakeSecurityWhereClause($where, 'AST',  $_SESSION['initials']);
            $sql .= " where $WhereClause";

            $ast = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $ast_id));

            if (!$ast || $ASTPerms == 'AST_INPUT_ONLY' || !$ASTPerms)
            {
                CheckRecordNotFound(array('module' => 'AST', 'recordid' => $ast_id));
            }

            // Since some generic functions require data against the "recordid" key
            $ast['recordid'] = $ast['ast_id'];

            if ($ASTPerms == 'AST_READ_ONLY')
            {
                $FormType = 'ReadOnly';
            }
            else
            {
                $FormType = 'edit';
            }

            if ($ast['recordid'])
            {
                // Documents
                $sql = "
                    SELECT
                        recordid,
                        doc_notes,
                        doc_dcreated,
                        doc_type,
                        typ.description as type_descr
                    FROM
                        documents_main,
                        code_doc_type typ
                    WHERE
                        ast_id = :ast_id
                        AND
                        doc_type = typ.code
                    ORDER BY
                        doc_dcreated DESC
                ";

                $ast['documents'] = \DatixDBQuery::PDO_fetch_all($sql, array('ast_id' => $ast_id));
            }
        }
        // New piece of equipment or new link
        elseif (!$ASTPerms || $ASTPerms == 'AST_READ_ONLY')
        {
            $msg = 'You do not have the necessary permissions to add new equipment.';
            fatal_error($msg, 'Information');
        }

        if ($link_id && $module && array_key_exists($module, $link_info))
        {
            // Check whether the link exists,
            // If it doesn't check whether it's a valid record id
            // Show 'Link' button if required
            $sql = "SELECT " . $link_info[$module]['linktable'] . ".recordid, link_assets."
                . $link_info[$module]['foreignkey'] . " FROM " . $link_info[$module]['linktable']
                . " LEFT JOIN link_assets ON " . $link_info[$module]['linktable']
                . ".recordid = link_assets." . $link_info[$module]['foreignkey'] . " AND link_assets.link_type = 'E'"
                . " AND link_assets.ast_id = '$ast_id'"
                . " WHERE " . $link_info[$module]['linktable'] . ".recordid = :link_id";

            $row = \DatixDBQuery::PDO_fetch($sql, array('link_id' => $link_id));

            if($row)
            {
                // Module and link_id are valid
                $ast['module'] = $module;
                $ast['link_id'] = $link_id;

                if (!$row[$link_info[$module]['foreignkey']])
                {
                    $ast['allow_linking'] = true;
                }
            }
        }

        // Check if we need to show full audit trail
        if ($ast['ast_id'] && $this->request->getParameter('full_audit'))
        {
            $FullAudit = GetFullAudit(array('Module' => 'AST', 'recordid' => $ast_id));

            if ($FullAudit)
            {
                $ast['full_audit'] = $FullAudit;
            }
        }

        if ($main_recordid)
        {
            // An already-linked piece of equipment
            $ast['link_exists'] = true;
        }

        $this->call('src\ast\controllers\ShowAssetFormTemplateController', 'showassetform', array(
            'FormType' => $FormType,
            'ast' => $ast,
            'main_recordid' => $main_recordid,
            'module' => $module
        ));
    }
}