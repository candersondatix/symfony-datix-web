<?php

namespace src\ast\controllers;

use src\framework\controller\TemplateController;

class ShowAssetFormTemplateController extends TemplateController
{
    /**
     * Shows the Equipment form.
     */
    public function showassetform()
    {
        global $scripturl, $TitleWidth, $FormTitle, $FormType;

        if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && $this->request->getParameter('action') == 'asset' && ($FormType != "Print" && $this->request->getParameter('print') != 1))
        {
            $this->addJs('src/generic/js/panelData.js');
        }

        if($FormType == "Print" || $this->request->getParameter('print') == 1)
        {
            $this->addJs('src/generic/js/print.js');
        }

        $ModuleDefs = $this->registry->getModuleDefs();

        $ast = ($this->request->getParameter('ast') ? $this->request->getParameter('ast') : null);
        $FormType = ($this->request->getParameter('FormType') ? $this->request->getParameter('FormType') : 'New');
        $link_id = ($this->request->getParameter('link_id') ? $this->request->getParameter('link_id') : '');
        $module = ($this->request->getParameter('module') ? $this->request->getParameter('module') : '');
        $main_recordid = ($this->request->getParameter('main_recordid') ? $this->request->getParameter('main_recordid') : '');
        $from_equipment_match = ($this->request->getParameter('from_equipment_match') ? $this->request->getParameter('from_equipment_match') : '');
        $equipment_match_link_id = ($this->request->getParameter('equipment_match_link_id') ? $this->request->getParameter('equipment_match_link_id') : '');
        $fromlisting = ($this->request->getParameter('fromlisting') ? $this->request->getParameter('fromlisting') : '');
        $fromsearch = ($this->request->getParameter('fromsearch') ? $this->request->getParameter('fromsearch') : '');
        $qbe_recordid = ($this->request->getParameter('qbe_recordid') ? $this->request->getParameter('qbe_recordid') : '');
        $qbe_return_module = ($this->request->getParameter('qbe_return_module') ? $this->request->getParameter('qbe_return_module') : '');
        $panel = ($this->request->getParameter('panel') ? $this->request->getParameter('panel') : null);
        $LinkMode = $this->request->getParameter('LinkMode') == 'linkequipment';

        if ($FormType == 'Search')
        {
            $form_action = 'search';
        }

        $LoggedIn = isset($_SESSION['logged_in']);
        $aReturn = array();

        // Pick up link details from before an equipment match
        if (is_array($_SESSION['AST']['LINK_DETAILS']))
        {
            foreach ($_SESSION['AST']['LINK_DETAILS'] as $fname => $fval)
            {
                if (!($ast[$fname] != '' && $fval == ''))  // don't overwrite a db value with a blank
                {
                    $ast[$fname] = $fval;
                }
            }
        }

        $_SESSION['AST']['LINK_DETAILS'] = '';
        if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')))
        {
        	require_once 'Source/security/SecurityBase.php';
        	$ASTPerms = GetUserHighestAccessLvlForRecord('AST_PERMS', 'AST', $ast['ast_id']);
        }
        else
        {
        	$ASTPerms = GetParm('AST_PERMS');
        }

        if (!$FormType)
        {
            $FormType = 'New';
        }

        if ($this->request->getParameter('print') == 1)
        {
            $FormType = 'Print';
        }

        // Not sure if we still need this.
        if ($FormType == 'search')
        {
            $FormType = 'Search';
        }

        $FormID = $this->request->getParameter('form_id');

        if ($ast['ast_id'])
        {
            $PrintView = "window.open('$scripturl?action=asset&module=" . $this->request->getParameter('module') . "&link_id={$ast['link_id']}&ast_id={$ast['ast_id']}&form_id={$FormID}&print=1&token=". \CSRFGuard::getCurrentToken() ."')";
        }

        SetUpFormTypeAndApproval('AST', $ast, $FormType, $form_action, $ASTPerms);

        $this->sendToJs('FormType', $FormType);

        CheckForRecordLocks('AST', $ast, $FormType, $sLockMessage);

        // Load AST form settings
        $formDesignSettings = array('module' => 'AST', 'level' => 2, 'form_type' => $FormType);

        if ($LinkMode)
        {
            $formDesignSettings = array_merge($formDesignSettings, array(
                'link_type' => 'tprop',
                'parent_module' => \Sanitize::getModule($this->request->getParameter('module'))
            ));
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign($formDesignSettings);

        SetUpApprovalArrays('AST', $ast, $CurrentApproveObj, $ApproveObj, $FormType, $FormDesign, '', $ASTPerms);

        $TitleWidth = 20;

        $IDFieldObj = new \FormField($FormType);

        if ($FormType == 'Search')
        {
            $IDField = '<input type="text" name="recordid" size="5" '.($ast['recordid'] ? 'value="'.$ast['recordid'].'"' : '').'>';
        }
        elseif (!$ast['ast_id'])
        {
            $IDField = '<font color="red">No ID assigned yet - adding new equipment</font>';
        }
        else
        {
            $IDField = $ast['ast_id'];
        }

        $IDFieldObj->MakeCustomField($IDField);

        require_once $ModuleDefs['AST']['BASIC_FORM_FILES'][2];

        $ast['returnURL'] = $_SERVER['REQUEST_URI'];

        // Make up the form
        $AstTable = new \FormTable($FormType, 'AST', $FormDesign);
        $AstTable->MakeForm($FormArray, $ast, 'AST', array('NoLinkFields' => !$LinkMode));

        $FormTitle = $FormDesign->FormTitle;

        if ($FormType == 'Search')
        {
            $FormTitle .= _tk('search_for_records');
        }

        $this->title = $FormTitle;
        $this->subtitle = $GLOBALS['FormTitleDescr'];
        $this->module = 'AST';

        $ButtonGroup = new \ButtonGroup();

        if ($FormType != 'Print')
        {
            if ($LinkMode)
            {
                // Viewing a piece of equipment without a link.
                if (!$ast['link_exists'])
                {
                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_check_match_equipment'),
                        'id' => 'btnCheck',
                        'name' => 'btnCheck',
                        'onclick' => 'MatchExistingAssets();',
                        'action' => 'SEARCH'
                    ));
                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_new_link_equipment'),
                        'id' => 'btnSave',
                        'name' => 'btnSave',
                        'onclick' => 'this.disabled=true;'.($OnSubmitJS['btnSave'] ? $OnSubmitJS['btnSave'] : '').'selectAllMultiCodes();submitClicked = true;document.frmAsset.rbWhat.value=\'SaveLink\';if(validateOnSubmit()){document.frmAsset.submit()}else{this.disabled=false}',
                        'action' => 'SAVE'
                    ));
                }
                elseif ($FormType != 'ReadOnly')
                {
                    // A linked, unapproved piece of equipment
                    if ($ast['rep_approved'] == 'UN')
                    {
                        $ButtonGroup->AddButton(array(
                            'label' => _tk('btn_check_match_equipment'),
                            'id' => 'btnCheck',
                            'name' => 'btnCheck',
                            'onclick' => 'MatchExistingAssets();',
                            'action' => 'SEARCH'
                        ));
                    }
                    // For any linked equipment
                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_save'),
                        'id' => 'btnSave',
                        'name' => 'btnSave',
                        'onclick' => 'Javascript:this.disabled=true;'.($OnSubmitJS['btnSave'] ? $OnSubmitJS['btnSave'] : '').'selectAllMultiCodes();submitClicked = true;document.frmAsset.rbWhat.value=\'SaveLink\';if(validateOnSubmit()){document.frmAsset.submit()}else{this.disabled=false}',
                        'action' => 'SAVE'
                    ));
                }

                if ($AdminUser = $_SESSION['AdminUser'] && $ast['link_exists'] && $FormType != 'ReadOnly')
                {
                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_unlink_equipment'),
                        'id' => 'btnUnLink',
                        'name' => 'btnUnLink',
                        'onclick' => 'submitClicked = true;document.frmAsset.rbWhat.value=\'Unlink\';document.frmAsset.submit()',
                        'action' => 'DELETE'
                    ));
                }

                if ($module && $main_recordid)
                {
                    $cancelUrl = 'app.php?action='.$ModuleDefs[$module]['ACTION'].'&recordid='.$main_recordid.'&panel=tprop';

                    $ButtonGroup->AddButton(array(
                        'label' => 'Back to '.$ModuleDefs[$this->request->getParameter('module')]['REC_NAME'],
                        'id' => 'btnBack',
                        'name' => 'btnBack',
                        'onclick' => 'if(confirm(\''._tk('confirm_or_cancel').'\')){SendTo(\''.$cancelUrl.'\');}',
                        'action' => 'BACK'
                    ));
                }
            }
            else
            {
                if ($FormType == 'Search')
                {
                    $ButtonGroup->AddButton(array(
                        'label' => (isset($_SESSION['security_group']['grp_id']) ? 'Continue' : 'Search'),
                        'id' => 'btnSearch',
                        'name' => 'btnSearch',
                        'onclick' => 'document.frmAsset.rbWhat.value = \'Search\'; submitClicked = true;document.frmAsset.submit();',
                        'action' => 'SEARCH'
                    ));
                }
                else
                {
                    if ($ast['module'] && $ast['link_id'] && !$ast['ast_id'])
                    {
                        $ButtonGroup->AddButton(array(
                            'label' => _tk('btn_search_for_equipment'),
                            'id' => 'btnSearch',
                            'name' => 'btnSearch',
                            'onclick' => 'MatchExistingAssets(); submitClicked = true;',
                            'action' => 'SEARCH'
                        ));
                    }

                    if ($FormType != 'ReadOnly')
                    {
                        $ButtonGroup->AddButton(array(
                            'label' => (($ast['allow_linking'] === true) ? 'Link to ' . $ModuleDefs[$this->request->getParameter('module')]['REC_NAME'] : _tk('btn_save')),
                            'id' => 'btnSave',
                            'name' => 'btnSave',
                            'onclick' => 'selectAllMultiCodes();submitClicked = true;if(validateOnSubmit()){document.frmAsset.submit()}else{this.disabled=false}',
                            'action' => 'SAVE'
                        ));
                    }
                }

                if (!($ast['allow_linking'] === true) && $this->request->getParameter('ast_id')
                    && $this->request->getParameter('link_id') && $this->request->getParameter('module'))
                {
                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_unlink_equipment'),
                        'id' => 'btnUnLink',
                        'name' => 'btnUnLink',
                        'onclick' => 'document.frmAsset.rbWhat.value = \'Unlink\'; submitClicked = true; document.frmAsset.submit()',
                        'action' => 'DELETE'
                    ));
                }

                //Add Cancel button
                if(isset($_SESSION['security_group']['grp_id']))
                {
                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_cancel'),
                        'id' => 'btnCancel',
                        'name' => 'btnCancel',
                        'onclick' => 'document.frmAsset.rbWhat.value = \'BackToGroup\'; submitClicked = true;document.frmAsset.submit();',
                        'action' => 'CANCEL'
                    ));
                }
                else
                {
                    if($this->request->getParameter('fromsearch'))
                    {
                        $cancelUrl = 'app.php?action=list&module=AST&listtype=search';
                    }
                    elseif ($module && $main_recordid)
                    {
                        $cancelUrl = 'app.php?action='.$ModuleDefs[$module]['ACTION'].'&recordid='.$main_recordid.'&panel=tprop';
                    }
                    else
                    {
                        $cancelUrl = 'index.php?module=AST';
                    }

                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_cancel'),
                        'id' => 'btnCancel',
                        'name' => 'btnCancel',
                        'onclick' => 'if(confirm(\''._tk('confirm_or_cancel').'\')){SendTo(\''.$cancelUrl.'\');}',
                        'action' => 'CANCEL'
                    ));
                }

                if ($this->request->getParameter('fromlisting'))
                {
                    $ButtonGroup->AddButton(array(
                        'label' => _tk('btn_back_to_report'),
                        'id' => 'btnBack',
                        'name' => 'btnBack',
                        'onclick' => 'submitClicked=true;selectAllMultiCodes();document.frmAsset.rbWhat.value=\'BackToListing\';document.frmAsset.submit();',
                        'action' => 'BACK'
                    ));
                }

                if ($module && $main_recordid)
                {
                    $cancelUrl = 'app.php?action='.$ModuleDefs[$module]['ACTION'].'&recordid='.$main_recordid.'&panel=tprop';

                    $ButtonGroup->AddButton(array(
                        'label' => 'Back to '.$ModuleDefs[$this->request->getParameter('module')]['REC_NAME'],
                        'id' => 'btnBack',
                        'name' => 'btnBack',
                        'onclick' => 'if(confirm(\''._tk('confirm_or_cancel').'\')){SendTo(\''.$cancelUrl.'\');}',
                        'action' => 'BACK'
                    ));
                }
            }
        }

        if ($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) &&!empty($_SESSION['AST']['RECORDLIST']) && (!isset($ModuleDefs['AST']['NO_NAV_ARROWS']) || $ModuleDefs['AST']['NO_NAV_ARROWS'] != true))
        {
            $CurrentIndex = $_SESSION['AST']['RECORDLIST']->getRecordIndex(array('recordid' => $ast['recordid']));

            if ($CurrentIndex !== false)
            {
                $ButtonGroup->AddNavigationButtons($_SESSION['AST']['RECORDLIST'], $ast['recordid'], 'AST');
            }
        }

        if ($FormType != 'Print')
        {
        	if ($LinkMode) //this is a special section for equipment, which needs this to make the print link
        	{
            	$this->menuParameters = array(
                	'table' => $AstTable,
                	'buttons' => $ButtonGroup,
                	'recordid' => $this->request->getParameter('ast_recordid'),
                    'isLinkedRecord' => true,
            		'print_link_params' => array('module' => $module, 'main_recordid' => $main_recordid, 'link_recordid' => $this->request->getParameter('link_recordid')	
            		));
        	}
        	else 
        	{
        		$this->menuParameters = array(
                    'table' => $AstTable,
                    'buttons' => $ButtonGroup,
                    'isLinkedRecord' => false,
                    'recordid' => $this->request->getParameter('ast_recordid')
                    );
        	}
        }
        else
        {
            $this->hasMenu = false;
        }


        if ($FormType != 'Print' && $FormType != 'Search')
        {
            $onSubmit = ' onsubmit="return (submitClicked &amp;&amp; validateOnSubmit())"';
        }

        $AstTable->MakeTable();
        $this->response->build('src/ast/views/ShowAssetForm.php', array(
            'FormType' => $FormType,
            'FormDesign' => $FormDesign,
            'onSubmit' => $onSubmit,
            'ast' => $ast,
            'link_id' => $link_id,
            'module' => $module,
            'main_recordid' => $main_recordid,
            'FormID' => $FormID,
            'from_equipment_match' => $from_equipment_match,
            'equipment_match_link_id' => $equipment_match_link_id,
            'fromlisting' => $fromlisting,
            'fromsearch' => $fromsearch,
            'qbe_recordid' => $qbe_recordid,
            'qbe_return_module' => $qbe_return_module,
            'sLockMessage' => $sLockMessage,
            'AstTable' => $AstTable,
            'ButtonGroup' => $ButtonGroup,
            'panel' => $panel
        ));
    }
}