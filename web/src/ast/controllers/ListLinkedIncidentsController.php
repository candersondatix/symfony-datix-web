<?php

namespace src\ast\controllers;

use src\framework\controller\Controller;

class ListLinkedIncidentsController extends Controller
{
    /**
     * Renders the Incidents section on the Equipment form.
     */
    public function ListIncidents()
    {
        //Different id keys are needed for equipment records accessed through another module vs through the equipment module.
        $equipmentRecordid = $this->request->getParameter('recordid') ?: $this->request->getParameter('ast_recordid');

        $linkedIncidents = \DatixDBQuery::PDO_fetch_all('SELECT inc_id FROM link_assets WHERE ast_id = :ast_id AND inc_id IS NOT NULL', array('ast_id' => $equipmentRecordid), \PDO::FETCH_COLUMN);

        $where = count($linkedIncidents) > 0 ? 'incidents_main.recordid in ('.implode(',', $linkedIncidents).')' : '1=2';
        
        require_once 'Source/libs/ListingClass.php';
        $Design = new \Listings_ModuleListingDesign(array('module' => 'INC', 'parent_module' => 'AST', 'link_type' => 'incidents'));
        $Design->LoadColumnsFromDB();

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'INC';
        $RecordList->Columns = $Design->Columns;
        $RecordList->WhereClause = MakeSecurityWhereClause($where, 'INC');
        $RecordList->OrderBy = array(new \Listings_ListingColumn('recordid', 'incidents_main'));
        $RecordList->Paging = false;
        $RecordList->RetrieveRecords();

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design);

        $this->response->setBody($ListingDisplay->GetListingHTML());
    }
}