<?php
if ($this->FormType != 'Print' && $this->FormType != 'Search') : ?>
<script language="JavaScript" type="text/javascript">
    <?php echo MakeJavaScriptValidation('AST', $this->FormDesign); ?>
    var submitClicked = false;
    AlertAfterChange = true;
</script>
<?php endif; ?>
<script type="text/javascript" language="javascript">
    function MatchExistingAssets(submitType)
    {
        document.frmAsset.rbWhat.value = 'Link';
        var url = '<?php echo $this->scripturl; ?>?action=assetlinkaction&token=<?php echo CSRFGuard::getCurrentToken() ?>';
        document.frmAsset.target = "wndMatchExisting";
        document.frmAsset.onsubmit = "window.open(url, 'wndMatchExisting', 'dependent,menubar=false,screenX=200,screenY=330,titlebar,scrollbars,resizable')";
        document.frmAsset.submit();
        document.frmAsset.target = "";
        document.frmAsset.rbWhat.value = 'Save';
    }
</script>
<form method="post" name="frmAsset" action="<?php echo $this->scripturl; ?>?action=assetlinkaction" <?php echo $this->onSubmit; ?>>
    <input type="hidden" name="form_type" value="<?php echo $this->FormType; ?>" />
    <input type="hidden" name="rbWhat" value="Save">
    <input type="hidden" name="ast_id" value="<?php echo Sanitize::SanitizeInt($this->ast['ast_id']); ?>" />
    <input type="hidden" name="link_id" value="<?php echo ($this->ast['link_id'] ? Sanitize::SanitizeInt($this->ast['link_id']) : Sanitize::SanitizeInt($this->link_id)); ?>" />
    <input type="hidden" name="link_recordid" value="<?php echo Sanitize::SanitizeInt($this->ast['link_recordid']); ?>" />
    <input type="hidden" name="link_exists" value="<?php echo Escape::EscapeEntities($this->ast['link_exists']); ?>" />
    <input type="hidden" name="module" value="<?php echo Sanitize::getModule($this->module); ?>" />
    <input type="hidden" name="main_recordid" value="<?php echo Sanitize::SanitizeInt($this->main_recordid); ?>" />
    <input type="hidden" name="form_id" value="<?php echo Sanitize::SanitizeInt($this->FormID); ?>" />
    <input type="hidden" name="updateid" value="<?php echo Escape::EscapeEntities($this->ast['updateid']); ?>">
    <input type="hidden" name="from_equipment_match" value="<?php echo Sanitize::SanitizeInt($this->from_equipment_match); ?>" />
    <input type="hidden" name="equipment_match_link_id" value="<?php echo Sanitize::SanitizeInt($this->equipment_match_link_id); ?>" />
    <input type="hidden" name="fromlisting" value="<?php echo Sanitize::SanitizeInt($this->fromlisting); ?>" />
    <input type="hidden" name="fromsearch" value="<?php echo Sanitize::SanitizeInt($this->fromsearch); ?>" />
    <input type="hidden" name="qbe_recordid" value="<?php echo Escape::EscapeEntities($this->qbe_recordid); ?>" />
    <input type="hidden" name="qbe_return_module" value="<?php echo Escape::EscapeEntities($this->qbe_return_module); ?>" />
    <input type="hidden" name="returnURL" value="<?php echo Escape::EscapeEntities($this->returnURL); ?>" />
    <?php if ($this->ast['error']) : ?>
    <div class="error_div"><?php echo Escape::EscapeEntities(_tk('form_errors')); ?></div>
    <?php endif; ?>
    <?php if ($this->sLockMessage) : ?>
    <div class="lock_message">
        <div id="LockMessage"><?php echo $this->sLockMessage; ?></div>
        <div id="userLockMessage"></div>
    </div>
    <?php endif; ?>
    <?php echo $this->AstTable->GetFormTable(); ?>
    <?php echo $this->ButtonGroup->getHTML(); ?>
</form>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect($this->FormDesign->Show_all_section, $this->panel, $this->AstTable); ?>
<?php endif; ?>