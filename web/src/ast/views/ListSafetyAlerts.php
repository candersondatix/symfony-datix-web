<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <tr>
        <td class="titlebg" colspan="8"><b>Safety alerts</b></td>
    </tr>
    <tr>
        <td class="windowbg" width="5%" align="center"><b>ID</b></td>
        <td class="windowbg" align="center"><b>Title</b></td>
        <td class="windowbg" align="center"><b>Reference</b></td>
    </tr>
    <?php if (count($this->resultArray) == 0) : ?>
    <tr>
        <td class="windowbg2" colspan="3">
            <b>No linked safety alerts.</b>
        </td>
    </tr>
    <?php else : ?>
        <?php foreach ($this->resultArray as $row) : ?>
            <?php if ($this->FormType != 'Print' && $this->FormType != 'ReadOnly' && $this->SABPerms != '') : ?>
                <tr>
                    <td class="windowbg2" valign="middle" align="center"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $row['sab_id']; ?>"><?php echo $row['sab_id']; ?></a></td>
                    <td class="windowbg2" valign="middle" align="left"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $row['sab_id']; ?>"><?php echo $row['sab_title']; ?></a></td>
                    <td class="windowbg2" valign="middle" align="left"><a href="<?php echo $this->scripturl; ?>?action=sabs&amp;recordid=<?php echo $row['sab_id']; ?>"><?php echo $row['sab_reference']; ?></a></td>
                </tr>
            <?php else : ?>
                <tr>
                    <td class="windowbg2" valign="middle" align="center"><?php echo $row['sab_id']; ?></td>
                    <td class="windowbg2" valign="middle" align="left"><?php echo $row['sab_title']; ?></td>
                    <td class="windowbg2" valign="middle" align="left"><?php echo $row['sab_reference']; ?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</table>
