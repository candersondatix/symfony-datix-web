/**
 * Function to unlink a respondent from a claim.
 * If the respondent still has payments attached the action will fail, otherwise the unlinking process os completed.
 *
 * @param {string} main_recordid The id of the claim record.
 * @param {string} link_recordid The id of the respondent.
 * @param {string} module        The module string.
 * @param {string} link_type     The type of linked respondent (contact or organisation).
 */
function unlinkRespondent(main_recordid, link_recordid, module, link_type)
{
    jQuery.ajax({
        type: 'POST',
        url: '?action=getrespondentpayments',
        dataType: 'json',
        data: {
            mainRecordId: main_recordid,
            linkRecordId: link_recordid
        }
    }).done(function(data) {
        if (data.numPayments != 0)
        {
            alert(data.message);
        }
        else
        {
            if (confirm('Are you sure you want to unlink this '+link_type+'?')) {
                if (link_type == 'organisation') {
                    SendTo(scripturl+'?action=unlinkrespondent&main_recordid='+main_recordid+'&link_recordid='+link_recordid+'&module='+module);
                }
                else if (link_type == 'contact') {
                    var $form = jQuery('[name="frmContact"]');

                    submitClicked = true;

                    $form.find('#rbWhat').val('unlink');
                    $form.submit();
                }
            }
        }
    });
}