<?php
namespace src\respondents\model;

use src\framework\model\RecordEntity;

/**
 * @codeCoverageIgnore
 */
class Respondent extends RecordEntity
{
    protected $link_type;

    protected $con_id;

    protected $org_id;

    protected $link_notes;

    protected $link_resp;

    protected $link_role;

    protected $main_recordid;

    protected $indemnity_reserve_assigned;

    protected $expenses_reserve_assigned;

    protected $remaining_indemnity_reserve_assigned;

    protected $remaining_expenses_reserve_assigned;

    protected $resp_total_paid;
}