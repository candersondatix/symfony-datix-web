<?php
namespace src\respondents\model;

use src\framework\model\ModelFactory;

class RespondentModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new RespondentFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new RespondentMapper(new \DatixDBQuery(), $this);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new RespondentCollection($this->getMapper(), $this->getEntityFactory());
    }
}