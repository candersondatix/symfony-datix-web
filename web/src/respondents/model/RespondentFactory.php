<?php
namespace src\respondents\model;

use src\framework\model\EntityFactory;

/**
 * @codeCoverageIgnore
 */
class RespondentFactory extends EntityFactory
{
    public function targetClass()
    {
        return 'src\\respondents\\model\\Respondent';
    }
}