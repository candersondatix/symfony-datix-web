<?php if (!empty($this->Data['org']) && !$this->Data['temp_record']) : ?>
    <?php if (!$this->MaxSuffixField) : ?>
    <?php echo $this->totalNumContacts; ?>
    <?php endif; ?>
<?php else : ?>
    <?php if (!$this->MaxSuffixField) : ?>
    <?php echo $this->maxSuffix; ?>
    <?php endif; ?>
<?php endif; ?>
<?php foreach ($this->organisationsSection as $suffix => $section) : ?>
<div id="organisation_section_div_<?php echo $suffix; ?>">
<?php echo $section; ?>
</div>
<?php endforeach; ?>
<?php if ($this->aParams['FormType'] != 'ReadOnly' && $this->aParams['FormType'] != 'Print' &&
    ($this->NumToShow < $this->level1OrgOptions || $this->level1OrgOptions == 0) ) : ?>
    <?php $spellChecker = ($this->webSpellChecker == 'Y' ? 'true' : 'false'); ?>
    <li class="new_windowbg" id="add_another_<?php echo $this->aParams['organisationType']; ?>_button_list">
        <input type="button" id="organisation_section_<?php echo $this->aParams['organisationType']; ?>" value="<?php echo _tk('add_another'); ?>" onclick="AddSectionToForm('organisation', '<?php echo $this->aParams['organisationType']; ?>', $('add_another_<?php echo $this->aParams['organisationType']; ?>_button_list'), '<?php echo $this->module; ?>', '', false, <?php echo $spellChecker; ?>, '<?php echo (is_numeric($this->aParams['form_id']) ? Sanitize::SanitizeInt($this->aParams['form_id']) : 'null'); ?>');" />
    </li>
<?php endif; ?>
<script language="JavaScript" type="text/javascript">dif1_section_suffix['organisation'] = 101;</script>