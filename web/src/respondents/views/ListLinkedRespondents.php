<?php if ($this->hasRecords && $this->totalResponsibility != 100): ?>
<div class="error_div"><?=_tk('responsibility_not_100');?></div>
<?php endif; ?>
<li>
    <?php echo $this->ListingDisplay->GetListingHTML(); ?>
    <?php if ($this->FormType != 'ReadOnly' && $this->FormType != 'Print' && $this->ListingDisplay->ReadOnly != true) : ?>
        <?php if ($this->canAddIndividuals) : ?>
        <div class="new_link_row">
            <a href="Javascript:SendTo(scripturl+'?action=linkcontactgeneral&module=<?php echo $this->module ?>&main_recordid=<?php echo $this->recordid ?>&formtype=Edit&link_type=O&from_parent_record=1');"><b><?php echo _tk('cla_individual_respondent_link')?></b></a>
        </div>
        <?php endif; ?>
        <?php if ($this->canAddOrganisations) : ?>
        <div class="new_link_row">
            <a href="Javascript:SendTo(scripturl+'?action=editorganisationlink&main_recordid=<?php echo $this->recordid ?>&level=2&module=<?php echo $this->module ?>&from_parent_record=1');"><b><?php echo _tk('cla_organisation_respondent_link')?></b></a>
        </div>
        <?php endif; ?>
    <?php endif; ?>
</li>