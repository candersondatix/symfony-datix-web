<?php

namespace src\tasks\controllers;

use src\framework\controller\Controller;
use src\framework\controller\Response;
use src\framework\controller\Request;

class TaskController extends Controller
{
    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
    }

    /**
     * Saves a task
     */
    public function savetask()
    {
        $Trigger = new \System_Trigger($this->request->getParameter('recordid'));
        $Trigger->UpdateFromPost();

        AddSessionMessage('INFO', 'Saved');

        $this->redirect('app.php?action=task&ID='.$Trigger->getID());
    }

    /**
     * Deletes a task
     */
    public function deletetask()
    {
        $Trigger = new \System_Trigger($this->request->getParameter('recordid'));
        $Trigger->DeleteTrigger();

        AddSessionMessage('INFO', 'Deleted');

        $this->redirect('app.php?action=listtasks');
    }
}

?>
