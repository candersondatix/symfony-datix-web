<?php
namespace src\tasks\model;

use src\framework\model\ModelFactory;

class TaskModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new TaskFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new TaskMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new TaskCollection($this->getMapper(), $this->getEntityFactory());
    }
}