<?php
namespace src\tasks\model;

use src\framework\model\Mapper;
use src\framework\model\Entity;
use src\framework\query\Query;

class TaskMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\tasks\\model\\Task';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'triggers_main';
    }
}