<script language="Javascript">
    AlertAfterChange = true;
    var submitClicked = false;
</script>
<form method="POST" id="triggerform" name="triggerform" action="<?php echo $this->scripturl; ?>?action=savetask">
    <?php echo $this->Table->GetFormTable(); ?>
</form>
<div class="button_wrapper">
    <input class="button" type="button" value="Save" onClick="submitClicked=true;if(validateOnSubmit()){selectAllMultiCodes();jQuery('#triggerform').submit();}">
    <?php if ($this->Data['recordid']): ?>
    <input class="button" type="button" value="Delete" onClick="if(confirm('<?php echo _tk('delete_confirm'); ?>')){SendTo('<?php echo $this->scripturl; ?>?action=deletetask&amp;recordid=<?php echo $this->Data['recordid']; ?>')}">
    <?php endif ?>
    <input class="button" type="button" value="<?php echo _tk('btn_cancel')?>" onClick="SendTo('<?php echo $this->scripturl; ?>?action=listtasks');">
</div>