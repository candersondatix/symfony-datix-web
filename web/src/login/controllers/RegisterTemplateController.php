<?php

namespace src\login\controllers;

use src\framework\controller\TemplateController;
use src\framework\registry\Registry;
use src\email\EmailSenderFactory;
use src\contacts\model\ContactModelFactory;

class RegisterTemplateController extends TemplateController
{
    function register()
    {
        global $scripturl, $UserSSOSettingsFile;

        // Check if registration is permitted using the global
        if (!bYN(GetParm('DIF_2_REGISTER', 'N')))
        {
            $this->redirect($scripturl);
        }

        $NetworkLogin = bYN(GetParm('WEB_NETWORK_LOGIN', 'N'));

        $con = $this->request->getParameters();

        if ($NetworkLogin && $UserSSOSettingsFile)
        {
            require_once $UserSSOSettingsFile;
            require_once 'Source/Login.php';

            $SSOAccountStatus = CheckSSOAccountStatus();
        }

        if ($SSOAccountStatus['no_reg_form'])
        {
            $this->title = 'Register';
            $this->module = 'ADM';
        }
        else
        {
            $con['con_forenames'] = FirstNonNull(array($con['con_forenames'], $UserArray['FirstNames']));
            $con['con_surname'] = FirstNonNull(array($con['con_surname'], $UserArray['Surname']));
            $con['con_email'] = FirstNonNull(array($con['con_email'], $UserArray['Email']));
            $con['login'] = FirstNonNull(array($con['login'], $UserArray['UserID']));

            require_once 'Source/contacts/BasicForm_Registration.php';

            // assume registration form design (from DatixWeb configuration)
            // if none is defined, assume default admin form
            $registrationFormDesign = GetParm('REG_FORM_DESIGN', '');
            $defaultAdminForm = GetParm('ADM_DEFAULT', 0);
            $formId = ($registrationFormDesign === '') ? $defaultAdminForm : $registrationFormDesign;

            $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'ADM', 'level' => 2, 'id' => $formId));

            StoreContactLabels();

            $ConTable = new \FormTable('New', 'ADM', $FormDesign);

            // Workaround to convert dates to SQL format
            // TODO: This needs to be removed when refactored
            $AdminDateFields = array_merge(GetAllFieldsByType('ADM', 'date'), getAllUdfFieldsByType('D', '', $con));

            foreach ($AdminDateFields as $AdminDate)
            {
                if (array_key_exists($AdminDate, $con) && $con[$AdminDate] != '')
                {
                    $con[$AdminDate] = UserDateToSQLDate($con[$AdminDate]);

                    // Some extra field dates look directly at the post value, so need to blank it out here.
                    unset($_POST[$AdminDate]);
                }
            }

            $ConTable->MakeForm($FormArray, $con, "ADM");

            $this->title = $GLOBALS['FormTitle'];
            $this->subtitle = $GLOBALS['FormTitleDescr'];
            $this->module = 'ADM';
            $this->hasMenu = false;

            $ConTable->MakeTable();
        }

        $this->response->build('src/login/views/Register.php', array(
            'SSOAccountStatus' => $SSOAccountStatus,
            'con' => $con,
            'ConTable' => $ConTable
        ));
    }

    function contactformregister2()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        require_once 'Source/Login.php';

        if ($this->request->getParameter('rbWhat') == 'cancel')
        {
            $this->redirect('app.php');
        }

        $this->request->setParameter('user_action', 'register');
        //TODO: bring ValidateUserData() into this class to eliminate need for this $_POST change.
        $_POST["user_action"] = 'register';
        $error = ValidateUserData();

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'ADM',
            'level' => 2,
            'id' => GetParm('REG_FORM_DESIGN', 0)
        ));

        $con = ParseSaveData(array(
                'module' => 'ADM',
                'data' => \Sanitize::SanitizeRawArray($this->request->getParameters()),
                'form_design' => $FormDesign)
        );

        $con['permission'] = '1792;3|256;3|1807;1|2560;1|271;1|527;1|512;3|1024;3|1039;1|2319;1|2304;3|783;1|2048;3|2063;1|1295;1|1280;3|1536;3|1551;1';
        // Unregistered user is signified by lockout = Y and login_tries = -1
        $con['lockout'] = 'Y';
        $con['login_tries'] = -1;
        $con['sta_user_type'] = 'WEB';
        $con['initials'] = ConstructInitials(array('data' => $con));
        $con['login'] = ConstructLogin(array('data' => $con));
        $con['rep_approved'] = 'UN';

        if (strlen($con['initials']) > 6)
        {
            $error = true;
            AddValidationMessage('initials', 'Character limit exceeded for initials ' . $con['initials']);
            AddValidationMessage('initials', 'Initials are already in use.');
        }

        if ($error)
        {
            $this->call('src\login\controllers\RegisterTemplateController', 'register', array('error' => $error));
            return;
        }

        $ConID = GetNextRecordID('contacts_main', true);

        $sql = 'UPDATE contacts_main SET ';

        $fieldArray = $ModuleDefs['ADM']['FIELD_ARRAY'];

        $sql .= GeneratePDOSQLFromArrays(
            array(
                'FieldArray' => $fieldArray,
                'DataArray' => $con,
                'Module' => 'ADM'
            ),
            $PDOParamsArray
        );

        $sql .= ' WHERE contacts_main.recordid = '.$ConID;

        \DatixDBQuery::PDO_query($sql, $PDOParamsArray);

        if ($con['password'] != '')
        {
            require_once 'Source/security/SecurityBase.php';
            SetUserPassword($con['login'], $con['password']);
        }

        require_once 'Source/libs/UDF.php';
        SaveUDFs($ConID, ModuleCodeToID('CON'));

        // Send out e-mail if required.
        //need to reassign values for default email (needs to work with both hard-coded and custom reg form.
        $_POST['title'] = $_POST['con_title'];
        $_POST['forenames'] = $_POST['con_forenames'];
        $_POST['surname'] = $_POST['con_surname'];
        $_POST['organisation'] = $_POST['con_organisation'];
        $_POST['email'] = $_POST['con_email'];

        $AdminEmail = $_SESSION['Globals']['DIF_ADMIN_EMAIL'];

        require_once 'Source/libs/Email.php';
        if ($AdminEmail != '')
        {
            $mailto = $AdminEmail;
            Registry::getInstance()->getLogger()->logEmail('E-mailing Admin ' . $mailto . ' of user registration submitted.');

            $emailSender = EmailSenderFactory::createEmailSender('INC', 'RegisterNotify');
            $emailSender->addRecipientEmail($_SESSION['Globals']['DIF_ADMIN_EMAIL']);
            $emailSender->setModuleToStore('ADM');
            $emailSender->sendEmails();
        }

        $failed_msg = '';
        if ($this->request->getParameter('con_email') != '')
        {
            $contact = (new ContactModelFactory)->getMapper()->find($ConID);

            $emailSender = EmailSenderFactory::createEmailSender('INC', 'RegisterUser');
            $emailSender->addRecipient($contact);
            $emailSender->setModuleToStore('ADM');
            $emailSender->sendEmails();

            count( $emailSender->getFailedDomainValidation() );

            if( count( $emailSender->getFailedDomainValidation() ) > 0 )
            {
                $failed_msg = _tk('registration_email_error') .' '. implode(', ', array_keys( $emailSender->getFailedDomainValidation() ) );
            }
        }

        SendNotificationEmails($emailSender, array('module' => 'ADM', 'recordid' => $ConID, 'data' => array('recordid' => $ConID)), $errors, $SuccessfulEmails, array($this->request->getParameter('con_email')));

        $this->hasMenu = false;
        $this->response->build('src/login/views/RegistrationSuccessful.php', array(
            'message' => $failed_msg
        ));

    }
}