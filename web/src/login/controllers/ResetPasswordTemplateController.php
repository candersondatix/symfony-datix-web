<?php

namespace src\login\controllers;

use src\framework\controller\TemplateController;
use src\security\PasswordHasher;
use src\users\model\UserModelFactory;

class ResetPasswordTemplateController extends TemplateController
{
    function reset_password()
    {
        // save token before destroying session
        $csrf_token = \CSRFGuard::getCurrentToken();
        $timezone = $_SESSION['Timezone'];
        $fileVersion = $_SESSION['DATIX_FILE_VERSION'];
        $databaseVersion = $_SESSION['DATIX_DATABASE_VERSION'];

        session_unset();
        @session_destroy();

        // now restore it
        session_start();
        GetParms();

        \CSRFGuard::setToken($csrf_token);
        $_SESSION['Timezone'] = $timezone;
        $_SESSION['DATIX_FILE_VERSION'] = $fileVersion;
        $_SESSION['DATIX_DATABASE_VERSION'] = $databaseVersion;

        $error = $this->request->getParameter('error');
        $user = $this->request->getParameter('user');

        $this->title = _tk('reset_password_title');

        $this->response->build('src/login/views/ResetPassword.php', array(
            'error' => $error,
            'user' => $user
        ));
    }

    function reset_password2()
    {
        global $scripturl;

        $username = $this->request->getParameter('username');

        if($username == '')
        {
            $this->redirect('app.php?action=reset_password&error=' . _tk('user_name_error'));
        }

        $Factory = new UserModelFactory();
        $User = $Factory->getMapper()->findByLogin($username);

        if ($User->login_tries == -1 || $User->con_email == '')
        {
            if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
            {
                //Even though the process has failed, we pretend the action was successful to avoid the risk of exposing user information.
                success_message(_tk('pwd_reset_success'));
            }
            else
            {
                $this->redirect('app.php?action=reset_password&error=' . _tk('user_name_error'));
            }
        }

        //We create a random string for the user to use to identify themselves when they return to set a new password
        $passwordHasher = new PasswordHasher();
        $md5_change_password = $passwordHasher->hash(time());

        $User->sta_pwd_reset_code = $md5_change_password;

        $sql = 'UPDATE staff
        SET sta_pwd_reset_code = :sta_pwd_reset_code
        WHERE login = :login';
        $result = \DatixDBQuery::PDO_query($sql, array("login" => $username, "sta_pwd_reset_code" => $md5_change_password));

        AuditLogin($username, "User requested password reset");

        $message = _tk('pwd_reset_email_1') . "\n\n" . _tk('pwd_reset_email_2') . "\n";
        $message .= "$scripturl?action=password&resetcode=$md5_change_password";

        if (bYN(GetParm('PREVENT_USERNAME_ENUMERATION', 'N')))
        {
            @\UnicodeString::mail($User->con_email, _tk('pwd_reset_subject'), $message, 'From: '.GetParm('DIF_ADMIN_EMAIL'));
            success_message(_tk('pwd_reset_success'));
        }
        else if (@\UnicodeString::mail($User->con_email, _tk('pwd_reset_subject'), $message, 'From: '.GetParm('DIF_ADMIN_EMAIL')))
        {
            success_message(_tk('pwd_reset_email_sent'));
        }
        else
        {
            success_message(_tk('email_problem_sending'));
        }
    }
}