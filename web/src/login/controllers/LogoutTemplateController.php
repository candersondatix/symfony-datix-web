<?php

namespace src\login\controllers;

use src\framework\controller\TemplateController;

class LogoutTemplateController extends TemplateController
{
    function logout()
    {
        global $scripturl;

        if ($_SESSION['logged_in'])
        {
            AuditLogin($_SESSION['user'], 'User logged out');

            if ($this->request->getParameter('timed_out') != '')
            {
                $TimedOut = true;
            }
        }

        $module = GetDefaultModule();

        if ($_SESSION['form_id'][$module][1])
        {
            $form_id = $_SESSION['form_id'][$module][1];
        }

        // Go back to the login page if DIF_LOGOUTTOIN is set, except when
        // WEB_NETWORK_LOGIN is set.
        // Need to set the address before the session is destroyed.
        if (bYN(GetParm('DIF_LOGOUTTOIN', 'N')) && !bYN(GetParm('WEB_NETWORK_LOGIN', 'N')))
        {
            $redirect = $scripturl . "?action=login";
        }
        elseif ($form_id)
        {
            $redirect = $scripturl . "?module=$module&form_id=$form_id";
        }
        else
        {
            $redirect = $scripturl . '?module=' . GetUserParm($_SESSION['login'], 'LOGOUT_DEFAULT_MODULE', 'INC');
        }

        if (bYN(GetParm('RECORD_LOCKING', 'N')))
        {
            \DatixDBQuery::CallStoredProcedure(array(
                'procedure' => 'EndSession',
                'parameters' => array(
                    array('@session_id', $_SESSION['session_id'], SQLINT4)
                )
            ));
        }

        session_unset();
        @session_destroy();

        if ($TimedOut)
        {
            $this->title = _tk('timed_out_title');
            $this->hasPadding = false;
            $this->hasMenu = false;

            $this->response->build('src/login/views/Logout.php', array(
                'TimedOut' => $TimedOut
            ));

        }
        elseif ($this->request->getParameter('no_session'))
        {
            $this->hasPadding = false;
            $this->hasMenu = false;

            $this->response->build('src/login/views/Logout.php', array(
                'no_session' => $this->request->getParameter('no_session')
            ));
        }
        elseif ($this->request->getParameter('disagreementmessage') == 'true')
        {
            $redirect = $scripturl . '?action=disagreementmessage';
        }
        elseif (GetParm('WEB_MAINTMODE', 'N', true) == 'Y')
        {
            $error = GetParm('WEB_MAINTMODE_MSG', 'The system is in maintenance mode.', true);

            if ($this->request->getParameter('action') == 'login')
            {
                $error = array(
                    $error,
                    'Users with full administrator-level access to the application can log in by clicking <a href="' . $scripturl . '?action=login&adminlogin=y">here</a>.'
                );
            }

            fatal_error($error, 'Information');
        }

        $this->redirect($redirect);
    }
}