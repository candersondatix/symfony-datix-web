<form name="frmReminder" action="<?php echo $this->scripturl; ?>?action=reset_password2" method="post">
    <table border="0" width="400" cellspacing="1" cellpadding="0" class="bordercolor" align="center">
        <tr>
            <td class="windowbg" width="100%">
                <table width="100%" cellspacing="0" cellpadding="3">
                    <tr>
                        <td class="titlebg" colspan="2">
                            <img src="<?php echo 'Images/login_sm.gif'; ?>" alt="" />
                            <font size="2" class="text1"><b><?php echo _tk('reset_password_title'); ?></b></font>
                        </td>
                    </tr>
                    <?php if ($this->error != '') : ?>
                    <tr>
                        <td class="windowbg" colspan="2"><font color="red"><b><?php echo $this->error; ?></b></font></td>
                    </tr>
                    <?php else : ?>
                    <tr>
                        <td class="windowbg" colspan="2"><?php echo _tk('reset_instructions'); ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td align="right" class="windowbg">
                            <font size="2"><b><?php echo _tk('user_name'); ?></b></font>
                        </td>
                        <td class="windowbg">
                            <font size="2">
                                <input type="text" name="username" maxlength="254" size="30" value="<?php echo Escape::EscapeEntities($this->user); ?>"/>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" class="windowbg">
                            <br /><input type="submit" value="<?php echo _tk('btn_reset_password'); ?>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>