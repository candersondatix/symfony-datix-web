<?php
namespace src\codetaggroups\model;

use src\framework\model\ModelFactory;

class CodeTagGroupModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new CodeTagGroupFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new CodeTagGroupMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new CodeTagGroupCollection($this->getMapper(), $this->getEntityFactory());
    }
}