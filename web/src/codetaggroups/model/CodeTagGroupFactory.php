<?php
namespace src\codetaggroups\model;

use src\framework\model\EntityFactory;

/**
 * Constructs CodeTagGroup objects.
 */
class CodeTagGroupFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\codetaggroups\\model\\CodeTagGroup';
    }
}