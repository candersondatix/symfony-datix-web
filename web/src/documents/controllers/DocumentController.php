<?php
namespace src\documents\controllers;

use src\framework\controller\Controller;
use src\documents\model\Document;
use src\documents\model\DocumentModelFactory;

class DocumentController extends Controller
{
    public function downloaddocument()
    {
        $Factory = new DocumentModelFactory();
        $Document = $Factory->getMapper()->find($this->request->getParameter('recordid'));

        if (!($Document instanceof Document))
        {
            throw new \PermissionDeniedException('You do not have access to this document.');
        }

        $Document->DownloadFile();
    }

    public function listlinkeddocuments()
    {
        $moduleDefs = $this->registry->getModuleDefs();

        $module = $this->request->getParameter('module');
        $data = $this->request->getParameter('data');
        $formType = $this->request->getParameter('FormType');
        $link_id = $data['recordid'];

        if ($this->request->getParameter('print') == 1)
        {
            $formType = "Print";
        }

        // get link types
        $LinkTypes = \DatixDBQuery::PDO_fetch_all('SELECT code, description FROM code_doc_type ORDER BY description', array(), \PDO::FETCH_KEY_PAIR);

        if($link_id)
        {
            $Factory = new DocumentModelFactory();

            // get documents
            $sql = 'SELECT recordid FROM documents_main WHERE ' . $moduleDefs[$module]['FK'] . ' = :link_id ORDER BY recordid DESC';
            $resultArray = \DatixDBQuery::PDO_fetch_all($sql, array('link_id' => $link_id), \PDO::FETCH_COLUMN);

            //TODO: Use a collection here
            foreach($resultArray as $recordid)
            {
                $overrideSecurity = ($this->request->getParameter('submitandprint') == 1 && HashesMatch($module,$link_id));

                if (($document = $Factory->getMapper()->find($recordid, $overrideSecurity)) instanceof Document)
                {
                    $DocumentObjArray[] = $document;
                }
            }
        }

        // When attaching documents to contacts we need to return the user to the linked record.
        if (!$data['returnURL'] && $data['link_recordid'] && $data['main_module'] && $data[$moduleDefs[$data['main_module']]['FK']] && $data['link_type'])
        {
            $data['returnURL'] = 'app.php?action='.$moduleDefs[$module]['LINK_ACTION'].
                '&module='.$data['main_module'].'&link_recordid='.$data['link_recordid'].
                '&link_type='.$data['link_type'].'&main_recordid='.$data[$moduleDefs[$data['main_module']]['FK']].
                '&panel=documents';
        }

        $this->response->build('src/documents/views/ListDocuments.php', array(
            'DocumentObjArray' => $DocumentObjArray,
            'formType' => $formType,
            'LinkTypes' => $LinkTypes,
            'linkId' => $link_id,
            'module' => $module,
            'returnURL' => $data['returnURL']
        ));
    }

    /**
     * Function checks for POST variable max_doc_suffix to see if any additional documents were included on the main form.
     * Adds them to the record if there are.
     *
     * @param int $recordid  The id of the record.
     * @param string $module The module.
     *
     * @return bool
     */
    static public function SaveDocsFromLevel1($recordid, $module)
    {
        global $ModuleDefs, $SectionVisibility;

        $NumDocs = $_POST['document_max_suffix'];

        if ($NumDocs != '0')
        {
            $DocNumber = 1;

            while ($DocNumber <= $NumDocs)
            {
                $DocumentHidden = !$SectionVisibility[$ModuleDefs[$module]['DOCUMENT_SECTION_KEY']];
                $DocumentNotFilledIn =  ($_POST["doc_type_$DocNumber"] == '' && $_POST["userfile_$DocNumber"] == '');

                // Need to check that at least some of the fields are filled in
                if (!$DocumentNotFilledIn && !$DocumentHidden)
                {
                    $Factory = new DocumentModelFactory();
                    $Document = $Factory->getEntityFactory()->createObject(array(
                        $ModuleDefs[$module]['FK'] => $recordid,
                        'doc_type'                 => $_POST["doc_type_$DocNumber"],
                        'doc_notes'                => $_POST["doc_notes_$DocNumber"]
                    ));

                    $fileLocation = $_FILES['userfile_'.$DocNumber]['tmp_name'];
                    $fileExtension = \UnicodeString::strtolower(\UnicodeString::strrchr($_FILES['userfile_'.$DocNumber]['name'], "."));

                    $Factory->getMapper()->save($Document, $fileLocation, $fileExtension);
                }

                $DocNumber++;
            }
        }

        return true;
    }

    /**
     * Validates documents uploaded.
     *
     * @param array $Data The array of posted variables.
     *
     * @return array|bool
     */
    static public function ValidateLinkedDocumentData($Data)
    {
        global $ModuleDefs;
        $error = array();

        $NumDocs = $Data['document_max_suffix'];

        if ($NumDocs == '0')
        {
            return true;
        }

        $DocNumber = 1;

        while ($DocNumber <= $NumDocs)
        {
            // Need to check that a document has been selected
            if ($_FILES["userfile_$DocNumber"]['size'] != 0)
            {
                $Factory = new DocumentModelFactory();
                $Document = $Factory->getEntityFactory()->createObject(array(
                    'doc_type'  => $Data['doc_type_'.$DocNumber],
                    'doc_notes' => $Data['doc_notes_'.$DocNumber]
                ));

                $fileLocation = $_FILES['userfile_'.$DocNumber]['tmp_name'];
                $fileExtension = \UnicodeString::strrchr(\Sanitize::SanitizeFilePath($_FILES['userfile_'.$DocNumber]["name"]), '.');

                $NewError = $Document->ValidateData($fileLocation, $fileExtension);

                foreach($NewError as $errorField => $errorMessage)
                {
                    $error[$errorField.'_'.$DocNumber] = $errorMessage;
                }
            }

            $DocNumber++;
        }

        return $error;
    }
}