<?php
namespace src\documents\controllers;

use src\framework\controller\TemplateController;
use src\documents\model\Document;
use src\documents\model\DocumentModelFactory;

class DocumentTemplateController extends TemplateController
{
    function deleteDocument()
    {
        $module = \Sanitize::getModule($this->request->getParameter('module'));
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $linkedRecordID = \Sanitize::SanitizeInt($this->request->getParameter('linkedrecordid'));

        $Factory = new DocumentModelFactory();

        if (($Document = $Factory->getMapper()->find($recordid)) instanceof Document)
        {
            $Factory->getMapper()->delete($Document);

            $ModuleDefs = $this->registry->getModuleDefs();

            $this->redirect('app.php?action=' . $ModuleDefs[$module]['ACTION'] .'&recordid='.$linkedRecordID.'&panel=documents');
        }
        else
        {
            throw new \PermissionDeniedException('You do not have permission to delete this document');
        }
    }

    function saveDocument()
    {
        $recordid = $this->request->getParameter('recordid');
        $linkedRecordID = $this->request->getParameter('linkedrecordid');
        $module = $this->request->getParameter('module');

        $ModuleDefs = $this->registry->getModuleDefs();

        $Factory = new DocumentModelFactory();
        if ($recordid)
        {
            $Document = $Factory->getMapper()->find($recordid);

            if (!($Document instanceof Document))
            {
                throw new \PermissionDeniedException('You do not have permission to save this document.');
            }

            $fileLocation = null;
            $fileExtension = null;
        }
        else
        {
            // New document
            $Document = $Factory->getEntityFactory()->createObject();
            $Document->$ModuleDefs[$module]['FK'] = $linkedRecordID;

            $fileLocation = $_FILES['userfile']["tmp_name"];
            $fileExtension = \UnicodeString::strrchr(\Sanitize::SanitizeFilePath($_FILES['userfile']["name"]),'.');

            //When creating a new document, the permission where clause will be out of date (as it is simply a list of ids). We need to force this to refresh.
            $_SESSION["permission_where"]['TEM'] = null;
        }

        $Document->doc_notes = $this->request->getParameter('doc_notes');
        $Document->doc_type = $this->request->getParameter('doc_type');

        $validationErrors = $Document->ValidateData($fileLocation, $fileExtension);

        if(!empty($validationErrors))
        {
            foreach($validationErrors as $field => $errorText)
            {
                AddValidationMessage($field, $errorText);
            }

            if($recordid)
            {
                $this->redirect('app.php?action=editdocument&recordid='.$recordid);
            }
            else
            {
                $this->call('src\\documents\\controllers\\DocumentTemplateController', 'editdocument', array('doc_type' => $Document->doc_type, 'doc_notes' => $Document->doc_notes));
                obExit();
            }
        }

        $Factory->getMapper()->save($Document, $fileLocation, $fileExtension);

        // Update record last updated
        $this->call('src\\generic\\controllers\\RecordController', 'updateRecordLastUpdated', [
            'module'   => $module,
            'recordId' => $linkedRecordID
        ]);

        $redirectUrl = ($_POST['returnURL'] ?: 'app.php?action=' . $ModuleDefs[$module]['ACTION'] . '&recordid=' . $linkedRecordID . '&panel=documents');

        $this->redirect($redirectUrl);
    }

    public function editdocument()
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid')); // The ID of the documents_main record itself
        $module = \Sanitize::getModule($this->request->getParameter('module'));    // The module code e.g. RAM, AST, etc.
        $linkedrecordid = \Sanitize::SanitizeInt($this->request->getParameter('link_id') ?: $this->request->getParameter('linkedrecordid'));     // The Id of the record to link the document to

        //We must have either a module and linkid or a recordid at minimum
        if(!$recordid && (!$module || !$linkedrecordid))
        {
            throw new \MissingParameterException('Neither an ID nor a module and linked record are specified for this document');
        }

        $main_table = $this->request->getParameter('main_table');
        $doc = $this->request->getParameters();

        // get link types for "Link as" drop down
        $LinkTypes = \DatixDBQuery::PDO_fetch_all('SELECT code, description FROM code_doc_type ORDER BY description', array(), \PDO::FETCH_KEY_PAIR);

        $Factory = new DocumentModelFactory();

        if ($recordid)
        {
            $Document = $Factory->getMapper()->find($recordid);

            if (!($Document instanceof Document))
            {
                throw new \PermissionDeniedException('You do not have permission to edit this document.');
            }
        }
        else
        {
            // New document
            $Document = $Factory->getEntityFactory()->createObject();
            $Document->$ModuleDefs[$module]['FK'] = $linkedrecordid;

            if($this->request->getParameter('doc_type'))
            {
                $Document->doc_type = $this->request->getParameter('doc_type');
            }
            if($this->request->getParameter('doc_notes'))
            {
                $Document->doc_notes = $this->request->getParameter('doc_notes');
            }
        }

        $TableObj = new \FormTable();
        $FieldObj = new \FormField();
        $TableObj->MakeTitleRow('<b>Attachment details</b>');

        $LinkTitle = '<label for="doc_type_title"><img src="images/Warning.gif" alt="Warning"> '._tk('doc_link_as').' '.GetValidationErrors($doc, "doc_type") . '</label>';
        $field = \Forms_SelectFieldFactory::createSelectField('doc_type', '', $Document->doc_type, '');
        $field->setCustomCodes($LinkTypes);
        $TableObj->MakeRow($LinkTitle, $field);

        $DescTitle = '<label for="doc_notes"><img src="images/Warning.gif" alt="Warning"> '._tk('doc_description').' '.GetValidationErrors($doc, "doc_notes") . '</label>';
        $TableObj->MakeRow($DescTitle, $FieldObj->MakeInputField('doc_notes', 50, 50, $Document->doc_notes, ""));

        if (!$doc["recordid"])
        {
            $UploadField  = '<input name="userfile" type="file"  size="50" title="Select file"/>'.($_FILES["userfile"]['name'] ? '<div><i>(You will need to re-select this file. The file was '.$_FILES["userfile"]['name'].')</i></div>' : '');
            $UploadTitle = '<label for="userfile"><img src="images/Warning.gif" alt="Warning"> '._tk('doc_attach_this_file').' '.GetValidationErrors($doc, "userfile") . '</label>';
            $TableObj->MakeRow($UploadTitle, $FieldObj->MakeCustomField($UploadField));
        }

        $TableObj->MakeTable();

        if ($this->request->getParameter('returnURL') == NULL)
        {
            // Can't use $_SERVER['HTTP_REFERER'] because it doesn't work on IE8.
            // Remove this when IE8 supports ends because it's an ugly workaround.
            switch ($module)
            {
                case 'INC':
                    $action = 'incident';
                    break;
                case 'RAM':
                    $action = 'risk';
                    break;
                case 'SAB':
                    $action = 'sabs';
                    break;
                case 'STN':
                    $action = 'standard';
                    break;
                case 'LIB':
                    $action = 'evidence';
                    break;
                case 'ACT':
                    $action = 'action';
                    break;
                case 'CON':
                    $action = 'editcontact';
                    break;
                case 'AST':
                    $action = 'asset';
                    break;
                default:
                    $action = 'record';
                    break;
            }

            $cancelLink = 'app.php?action='.$action.'&module='.$module.'&recordid='.$linkedrecordid.'&panel=documents';
        }
        else
        {
            $cancelLink = $this->request->getParameter('returnURL');
        }

        $this->title = ($recordid ? $ModuleDefs[$module]['NAME'].  " document" : 'New document');
        $this->module = $module;

        // Is user able to delete documents?
        $DeleteDocuments = GetUserParm($_SESSION['login'], $this->module . '_DELETE_DOCS', 'Y');

        $this->response->build('src/documents/views/Document.php', array(
            'recordid' => $recordid,
            'module' => $module,
            'linkedrecordid' => $linkedrecordid,
            'TableObj' => $TableObj,
            'cancelLink' => $cancelLink,
            'DeleteDocuments' => $DeleteDocuments
        ));
    }
}