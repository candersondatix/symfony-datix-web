<?php
namespace src\documents\model;

use src\framework\model\ModelFactory;

class DocumentModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new DocumentFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new DocumentMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new DocumentCollection($this->getMapper(), $this->getEntityFactory());
    }
}