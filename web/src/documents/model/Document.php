<?php
namespace src\documents\model;

use src\documents\exceptions\DocumentException;
use src\framework\model\RecordEntity;

/**
 * this entity models an entry in documents_main.
 */
class Document extends RecordEntity
{
    /**
     * The id of this document in the doc_documents table.
     */
    protected $doc_id;

    /**
     * The id of the incident this document is linked to.
     * Will be null if this document is not linked to an incident.
     */
    protected $inc_id;

    /**
     * The id of the risk this document is linked to.
     * Will be null if this document is not linked to a risk.
     */
    protected $ram_id;

    /**
     * The id of the complaint this document is linked to.
     * Will be null if this document is not linked to a complaint.
     */
    protected $com_id;

    /**
     * The id of the claim this document is linked to.
     * Will be null if this document is not linked to a claim.
     */
    protected $cla_id;

    /**
     * The id of the contact this document is linked to.
     * Will be null if this document is not linked to a contact.
     */
    protected $con_id;

    /**
     * The id of the assurance this document is linked to.
     * Will be null if this document is not linked to an assurance.
     */
    protected $ass_id;

    /**
     * The id of the pal record this document is linked to.
     * Will be null if this document is not linked to a pal record.
     */
    protected $pal_id;

    /**
     * The id of the standard this document is linked to.
     * Will be null if this document is not linked to a standard.
     */
    protected $stn_id;

    /**
     * The id of the library record this document is linked to.
     * Will be null if this document is not linked to a library record.
     */
    protected $lib_id;

    /**
     * The id of the safety alert this document is linked to.
     * Will be null if this document is not linked to a safety alert.
     */
    protected $sab_id;

    /**
     * The id of the equipment this document is linked to.
     * Will be null if this document is not linked to equipment.
     */
    protected $ast_id;

    /**
     * The id of the hotspot this document is linked to.
     * Will be null if this document is not linked to a hotspot.
     */
    protected $hot_id;

    /**
     * The id of the action this document is linked to.
     * Will be null if this document is not linked to an action.
     */
    protected $act_id;

    /**
     * The id of the policy this document is linked to.
     * Will be null if this document is not linked to an action.
     */
    protected $pol_id;

    /**
     * Free text information about the document.
     */
    protected $doc_notes;

    /**
     * Coded field representing the type of document held here.
     */
    protected $doc_type;

    /**
     * Documents are all stored on the server as .doc filed. This field
     * holds the original extension of the document.
     */
    protected $doc_extension;

    /**
     * Acts as a marker to ensure that other people's changes are not overwritten.
     *
     * @var string
     */
    protected $updateid;

    /**
     * The initials of the user who created the document.
     *
     * @var string
     */
    protected $doc_createdby;

    /**
     * The initials of the last user to update the document.
     *
     * @var string
     */
    protected $doc_updatedby;

    /**
     * The date the document was created.
     *
     * @var string
     */
    protected $doc_dcreated;

    /**
     * The date the document was last updated.
     *
     * @var string
     */
    protected $doc_dupdated;
    
    /**
     * The file name as saved on disk.
     * 
     * @var string
     */
    protected $path;

    /**
     * Takes a file and moves it to the correct server location, adding appropriate
     * metadata to the entity record in the process.
     *
     * @param string $fileLocation The current server location of the document
     * @param string $fileExtension The extension of the file.
     *
     * @throws \InvalidParameterException If invalid parameters are passed to the function
     * @throws \DatixDBQueryException If a query has not run correctly
     * @throws \FileNotFoundException If the document to link doesn't exist
     * @throws \Exception under other circumstances
     * @throws \src\documents\exceptions\DocumentException
     */
    public function savePhysicalDocument($fileLocation, $fileExtension)
    {
        if(!file_exists($fileLocation))
        {
            throw new \FileNotFoundException('Physical document cannot be found.');
        }
        if($fileExtension == '')
        {
            throw new \InvalidParameterException('File extension must be provided.');
        }

        $this->doc_extension = $fileExtension;

        $this->doc_id = GetNextRecordID("doc_documents", true);

        $result = \DatixDBQuery::PDO_query(
                    'UPDATE doc_documents SET path = :path, compressed = :compressed WHERE recordid = :recordid',
                    array(
                        'compressed' => 'N',
                        'path' => $this->doc_id . '.doc',
                        'recordid' => $this->doc_id
                    )
        );

        if (!$result)
        {
            throw new \DatixDBQueryException('Could not insert document');
        }

        $this->path = null;
        $toFile = $this->GetPath();

        if (!copy($fileLocation, $toFile))
        {
            $Factory = new \src\documents\model\DocumentModelFactory();
            $Factory->getMapper()->delete($this);

            throw new DocumentException('Cannot copy file to documents folder.  Please contact your IT department.');
        }
    }

    /**
     * Returns the full path (including filename) of the place where the physical document this entity
     * represents should be stored.
     * @return string
     * @throws \InvalidParameterException
     */
    public function GetPath()
    {
        if (!is_numeric($this->doc_id))
        {
            throw new \InvalidParameterException('Document ID must be a number.');
        }

        if ($this->path == '')
        {
            $this->path = $this->doc_id.'.doc';
        }
        
        $docDir = \DatixDBQuery::PDO_fetch(
            'SELECT doc_dir FROM doc_location WHERE doc_start_no <= :doc_id ORDER BY doc_location.recordid DESC',
            array('doc_id' => $this->doc_id),
            \PDO::FETCH_COLUMN
        );

        if (!$docDir)
        {
            $docDir = GetParm('PATH_DOCUMENTS') . "\\" . $this->path;
        }
        else
        {
            $docDir .= "\\" . $this->path;
        }

        return \Sanitize::SanitizeFilePath($docDir);
    }

    /**
     * @param null $fileLocation
     * @param null $fileExtension
     * @return array
     */
    public function ValidateData($fileLocation = null, $fileExtension = null)
    {
        $errors = array();

        if ($this->doc_type == "")
        {
            $errors['doc_type'] = 'You must enter a document type.';
        }

        if ($this->doc_notes == "")
        {
            $errors['doc_notes'] = 'You must enter a document description.';
        }

        //On new document records, we also need to provide a file.
        if (!isset($this->recordid) && $fileLocation == '')
        {
            $errors['userfile'] = 'You must select a file to upload.';
        }
        else
        {
            if ($fileExtension && !$this->ExtensionValid($fileLocation, $fileExtension))
            {
                $errors['userfile'] = "This file has a restricted extension ($fileExtension). For security reasons you cannot link a file of this type to a Datix record.";
            }
        }

        return $errors;
    }

    /**
     * Ensure that document extension passes validation whitelist and blacklist tests.
     *
     * @param string $fileLocation Temporary location of the uploaded file.
     * @param string $fileExtension The extension of the uploaded file.
     * @return bool
     */
    function ExtensionValid($fileLocation, $fileExtension)
    {
        require_once __DIR__.'/../../../Source/libs/Subs.php';

        // Get file mime type
        $finfo = new \finfo();
        $fileInfo = $finfo->file($fileLocation, FILEINFO_MIME_TYPE);

        $fileExtension = \UnicodeString::substr(\UnicodeString::strtolower($fileExtension), 1);

        $ValidTypes = \UnicodeString::strtolower(str_replace(' ', '', GetParm('VALID_FILE_TYPES')));
        $InvalidTypes = \UnicodeString::strtolower(str_replace(' ', '', GetParm('INVALID_FILE_TYPES')));

        if ($ValidTypes)
        {
            if (!in_array($fileExtension, explode(',', $ValidTypes)))
            {
                return false;
            }
        }
        elseif ($InvalidTypes)
        {
            if (in_array($fileExtension, explode(',', $InvalidTypes)))
            {
                return false;
            }
        }
        
        if (bYN(GetParm('CHECK_FILE_MIME_TYPE', 'N')))
        {
            // This is to allow empty documents to be uploaded
            if ($fileInfo != 'inode/x-empty')
            {
                if ($this->getMimeTypeForExtension($fileExtension) != $fileInfo)
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Returns the mime type for a specific file extension
     * @param  string $extension 
     * @return string mime type
     */
    public function getMimeTypeForExtension ($extension)
    {
        $mimeTypesMapper = array(
            '323'     => 'text/h323',
            'acx'     => 'application/internet-property-stream',
            'ai'      => 'application/postscript',
            'aif'     => 'audio/x-aiff',
            'aifc'    => 'audio/x-aiff',
            'aiff'    => 'audio/x-aiff',
            'asf'     => 'video/x-ms-asf',
            'asr'     => 'video/x-ms-asf',
            'asx'     => 'video/x-ms-asf',
            'au'      => 'audio/basic',
            'avi'     => 'video/x-msvideo',
            'axs'     => 'application/olescript',
            'bas'     => 'text/plain',
            'bcpio'   => 'application/x-bcpio',
            'bin'     => 'application/octet-stream',
            'bmp'     => 'image/bmp',
            'bz'      => 'application/x-bzip',
            'bz2'     => 'application/x-bzip2',
            'c'       => 'text/plain',
            'cat'     => 'application/vnd.ms-pkiseccat',
            'cdf'     => 'application/x-cdf',
            'cer'     => 'application/x-x509-ca-cert',
            'class'   => 'application/octet-stream',
            'clp'     => 'application/x-msclip',
            'cmx'     => 'image/x-cmx',
            'cod'     => 'image/cis-cod',
            'cpio'    => 'application/x-cpio',
            'crd'     => 'application/x-mscardfile',
            'crl'     => 'application/pkix-crl',
            'crt'     => 'application/x-x509-ca-cert',
            'csh'     => 'application/x-csh',
            'css'     => 'text/css',
            'csv'     => 'text/plain',
            'dcr'     => 'application/x-director',
            'der'     => 'application/x-x509-ca-cert',
            'dir'     => 'application/x-director',
            'dll'     => 'application/x-msdownload',
            'dms'     => 'application/octet-stream',
            // Office 2003
            'doc'     => 'application/msword',
            // Office 2007, 2010
            'docx'    => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'dot'     => 'application/msword',
            'dvi'     => 'application/x-dvi',
            'dxf'     => 'application/dxf',
            'dxr'     => 'application/x-director',
            'eps'     => 'application/postscript',
            'etx'     => 'text/x-setext',
            'evy'     => 'application/envoy',
            'exe'     => 'application/octet-stream',
            'fif'     => 'application/fractals',
            'flr'     => 'x-world/x-vrml',
            'gif'     => 'image/gif',
            'gtar'    => 'application/x-gtar',
            'gz'      => 'application/x-gzip',
            'gzip'    => 'application/x-gzip',
            'h'       => 'text/plain',
            'hdf'     => 'application/x-hdf',
            'hlp'     => 'application/winhlp',
            'hqx'     => 'application/mac-binhex40',
            'hta'     => 'application/hta',
            'htc'     => 'text/x-component',
            'htm'     => 'text/html',
            'html'    => 'text/html',
            'htt'     => 'text/webviewhtml',
            'ico'     => 'image/x-icon',
            'ief'     => 'image/ief',
            'iii'     => 'application/x-iphone',
            'ins'     => 'application/x-internet-signup',
            'isp'     => 'application/x-internet-signup',
            'jfif'    => 'image/pipeg',
            'jpe'     => 'image/jpeg',
            'jpeg'    => 'image/jpeg',
            'jpg'     => 'image/jpeg',
            'latex'   => 'application/x-latex',
            'lha'     => 'application/octet-stream',
            'lsf'     => 'video/x-la-asf',
            'lsx'     => 'video/x-la-asf',
            'lzh'     => 'application/octet-stream',
            'm13'     => 'application/x-msmediaview',
            'm14'     => 'application/x-msmediaview',
            'm3u'     => 'audio/x-mpegurl',
            'm4a'     => 'audio/mp4',
            'man'     => 'application/x-troff-man',
            // Office 2003
            'mdb'     => 'application/x-msaccess',
            'me'      => 'application/x-troff-me',
            'mht'     => 'message/rfc822',
            'mhtml'   => 'message/rfc822',
            'mid'     => 'audio/mid',
            'mny'     => 'application/x-msmoney',
            'mov'     => 'video/quicktime',
            'movie'   => 'video/x-sgi-movie',
            'mp2'     => 'video/mpeg',
            'mp3'     => 'audio/mpeg',
            'mp4'     => 'video/mp4',
            'mpa'     => 'video/mpeg',
            'mpe'     => 'video/mpeg',
            'mpeg'    => 'video/mpeg',
            'mpg'     => 'video/mpeg',
            'mpp'     => 'application/vnd.ms-project',
            'mpv2'    => 'video/mpeg',
            'ms'      => 'application/x-troff-ms',
            'mvb'     => 'application/x-msmediaview',
            'nws'     => 'message/rfc822',
            'oda'     => 'application/oda',
            'p10'     => 'application/pkcs10',
            'p12'     => 'application/x-pkcs12',
            'p7b'     => 'application/x-pkcs7-certificates',
            'p7c'     => 'application/x-pkcs7-mime',
            'p7m'     => 'application/x-pkcs7-mime',
            'p7r'     => 'application/x-pkcs7-certreqresp',
            'p7s'     => 'application/x-pkcs7-signature',
            'pbm'     => 'image/x-portable-bitmap',
            'pdf'     => 'application/pdf',
            'pfx'     => 'application/x-pkcs12',
            'pgm'     => 'image/x-portable-graymap',
            'pko'     => 'application/ynd.ms-pkipko',
            'pma'     => 'application/x-perfmon',
            'pmc'     => 'application/x-perfmon',
            'pml'     => 'application/x-perfmon',
            'pmr'     => 'application/x-perfmon',
            'pmw'     => 'application/x-perfmon',
            'png'     => 'image/png',
            'pnm'     => 'image/x-portable-anymap',
            'pot'     => 'application/vnd.ms-powerpoint',
            'ppa'     => 'application/vnd.ms-powerpoint',
            'ppm'     => 'image/x-portable-pixmap',
            'pps'     => 'application/vnd.ms-powerpoint',
            // Office 2003
            'ppt'     => 'application/vnd.ms-powerpoint',
            // Office 2007, 2010
            'pptx'    => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'prf'     => 'application/pics-rules',
            'ps'      => 'application/postscript',
            // Office 2003
            'pub'     => 'application/vnd.ms-office',
            'qt'      => 'video/quicktime',
            'ra'      => 'audio/x-pn-realaudio',
            'ram'     => 'audio/x-pn-realaudio',
            'ras'     => 'image/x-cmu-raster',
            'rgb'     => 'image/x-rgb',
            'rmi'     => 'audio/mid',
            'roff'    => 'application/x-troff',
            'rtf'     => 'application/rtf',
            'rtx'     => 'text/richtext',
            'scd'     => 'application/x-msschedule',
            'sct'     => 'text/scriptlet',
            'setpay'  => 'application/set-payment-initiation',
            'setreg'  => 'application/set-registration-initiation',
            'sh'      => 'application/x-sh',
            'shar'    => 'application/x-shar',
            'shtml'   => 'text/html',
            'sit'     => 'application/x-stuffit',
            'snd'     => 'audio/basic',
            'spc'     => 'application/x-pkcs7-certificates',
            'spl'     => 'application/futuresplash',
            'sql'     => 'text/plain',
            'src'     => 'application/x-wais-source',
            'sst'     => 'application/vnd.ms-pkicertstore',
            'stl'     => 'application/vnd.ms-pkistl',
            'stm'     => 'text/html',
            'svg'     => 'image/svg+xml',
            'sv4cpio' => 'application/x-sv4cpio',
            'sv4crc'  => 'application/x-sv4crc',
            'swf'     => 'application/x-shockwave-flash',
            't'       => 'application/x-troff',
            'tar'     => 'application/x-tar',
            'tcl'     => 'application/x-tcl',
            'tex'     => 'application/x-tex',
            'texi'    => 'application/x-texinfo',
            'texinfo' => 'application/x-texinfo',
            'tgz'     => 'application/x-compressed',
            'tif'     => 'image/tiff',
            'tiff'    => 'image/tiff',
            'tr'      => 'application/x-troff',
            'trm'     => 'application/x-msterminal',
            'tsv'     => 'text/tab-separated-values',
            'txt'     => 'text/plain',
            'uls'     => 'text/iuls',
            'ustar'   => 'application/x-ustar',
            'vcf'     => 'text/x-vcard',
            'vrml'    => 'x-world/x-vrml',
            'wav'     => 'audio/x-wav',
            'wcm'     => 'application/vnd.ms-works',
            'wdb'     => 'application/vnd.ms-works',
            'wks'     => 'application/vnd.ms-works',
            'wma'     => 'audio/x-ms-wma',
            'wmf'     => 'application/x-msmetafile',
            'wmv'     => 'video/x-ms-wmv',
            'wps'     => 'application/vnd.ms-works',
            'wri'     => 'application/x-mswrite',
            'wrl'     => 'x-world/x-vrml',
            'wrz'     => 'x-world/x-vrml',
            'wsdl'    => 'application/xml',
            'xaf'     => 'x-world/x-vrml',
            'xbm'     => 'image/x-xbitmap',
            'xla'     => 'application/vnd.ms-excel',
            'xlc'     => 'application/vnd.ms-excel',
            'xlm'     => 'application/vnd.ms-excel',
            // Office 2003
            'xls'     => 'application/vnd.ms-excel',
            // Office 2007, 2010
            'xlsx'    => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xlt'     => 'application/vnd.ms-excel',
            'xlw'     => 'application/vnd.ms-excel',
            'xof'     => 'x-world/x-vrml',
            'xml'     => 'application/xml',
            'xpm'     => 'image/x-xpixmap',
            'xwd'     => 'image/x-xwindowdump',
            'z'       => 'application/x-compress',
            'zip'     => 'application/zip'
        );

        return $mimeTypesMapper[$extension];
    }

    /**
     * @throws \FileNotFoundException
     */
    function DownloadFile()
    {
        //First, see if the file exists
        if (!is_file($this->GetPath()))
        {
            throw new \FileNotFoundException('The document you have selected cannot be found.  This may be because it has been deleted or its location has been changed.  Please contact your Datix administrator');
        }
        else
        {
            //Gather relevent info about file
            $len = filesize($this->GetPath());

            $file_extension = \UnicodeString::strtolower($this->doc_extension);

            //This will set the Content-Type to the appropriate setting for the file
            switch ($file_extension)
            {
                case "pdf": $ctype="application/pdf"; break;
                case "exe": $ctype="application/octet-stream"; break;
                case "zip": $ctype="application/zip"; break;
                case "doc": $ctype="application/msword"; break;
                case "docx": $ctype="application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
                case "xls": $ctype="application/vnd.ms-excel"; break;
                case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
                case "gif": $ctype="image/gif"; break;
                case "png": $ctype="image/png"; break;
                case "jpeg":
                case "jpg": $ctype="image/jpeg"; break;
                case "mp3": $ctype="audio/mpeg"; break;
                case "wav": $ctype="audio/x-wav"; break;
                case "mpeg":
                case "mpg":
                case "mpe": $ctype="video/mpeg"; break;
                case "mov": $ctype="video/quicktime"; break;
                case "avi": $ctype="video/x-msvideo"; break;
                //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
                case "php":
                case "js": die("<b>Cannot be used for ". $file_extension ." files!</b>"); break;
                default: $ctype="application/force-download";
            }

            //Begin writing headers
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");

            //Use the switch-generated Content-Type
            header("Content-Type: $ctype");

            //Force the download
            //$header="Content-Disposition: attachment; filename=".$this->doc_id.'.'.$this->doc_extension.";";
            header("Content-Disposition: attachment; filename=" . $this->doc_id . '.' . $this->doc_extension);
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".$len);
            @readfile($this->GetPath());
            exit;
        }
    }

    /**
     * Checks if a document exists on a folder.
     *
     * @param string $fileLocation The current server location of the document.
     * @return bool True if document exists, false otherwise.
     */
    public function Exists($fileLocation)
    {
        if (file_exists($fileLocation))
        {
            return true;
        }

        return false;
    }

    /**
     * Returns the module code for the module the record this document is linked to belongs.
     */
    public function getModule()
    {
        if ($this->inc_id)
        {
            return 'INC';
        }
        if ($this->ram_id)
        {
            return 'RAM';
        }
        if ($this->com_id)
        {
            return 'COM';
        }
        if ($this->cla_id)
        {
            return 'CLA';
        }
        if ($this->con_id)
        {
            return 'CON';
        }
        if ($this->ass_id)
        {
            return 'ASS';
        }
        if ($this->pal_id)
        {
            return 'PAL';
        }
        if ($this->stn_id)
        {
            return 'STN';
        }
        if ($this->lib_id)
        {
            return 'LIB';
        }
        if ($this->sab_id)
        {
            return 'SAB';
        }
        if ($this->ast_id)
        {
            return 'AST';
        }
        if ($this->hot_id)
        {
            return 'HOT';
        }
        if ($this->act_id)
        {
            return 'ACT';
        }
        if ($this->pol_id)
        {
            return 'POL';
        }
    }

    /**
     * Returns the id of the record to which this document is linked.
     */
    public function getMainRecordId()
    {
        if ($this->inc_id)
        {
            return $this->inc_id;
        }
        if ($this->ram_id)
        {
            return $this->ram_id;
        }
        if ($this->com_id)
        {
            return $this->com_id;
        }
        if ($this->cla_id)
        {
            return $this->cla_id;
        }
        if ($this->con_id)
        {
            return $this->con_id;
        }
        if ($this->ass_id)
        {
            return $this->ass_id;
        }
        if ($this->pal_id)
        {
            return $this->pal_id;
        }
        if ($this->stn_id)
        {
            return $this->stn_id;
        }
        if ($this->lib_id)
        {
            return $this->lib_id;
        }
        if ($this->sab_id)
        {
            return $this->sab_id;
        }
        if ($this->ast_id)
        {
            return $this->ast_id;
        }
        if ($this->hot_id)
        {
            return $this->hot_id;
        }
        if ($this->act_id)
        {
            return $this->act_id;
        }
        if ($this->pol_id)
        {
            return $this->pol_id;
        }
    }
}