<?php
namespace src\logger;

interface iLogger
{
    public function logDebug($message);

    public function logEmergency($message);
}