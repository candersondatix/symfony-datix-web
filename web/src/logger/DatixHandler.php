<?php
namespace src\logger;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

/**
 * Retains the current working directory before script shutdown in order to facilitate cleanup of old log files.
 */
class DatixHandler extends RotatingFileHandler
{
    /**
     * The current working directory.
     * 
     * @var string
     */
    protected $cwd;
    
    public function __construct($filename, $maxFiles = 0, $level = Logger::DEBUG, $bubble = true)
    {
        $this->cwd = getcwd();
        parent::__construct($filename, $maxFiles, $level, $bubble);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getGlobPattern()
    {
        $glob = parent::getGlobPattern();
        $logFiles = glob($glob);
        
        if (empty($logFiles) && getcwd() != $this->cwd)
        {
            // only prepend the cached cwd when no files are found using the original glob pattern
            // (the cwd may not be needed if we have an absolute path to the log file)
            $glob = $this->cwd.DIRECTORY_SEPARATOR.$glob;
        }
        
        return $glob;
    }
}