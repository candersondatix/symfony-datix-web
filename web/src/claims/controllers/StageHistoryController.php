<?php

namespace src\claims\controllers;

use src\framework\controller\Controller;

class StageHistoryController extends Controller
{
    /**
     * Creates and echos the HTML for the add/edit stage history popup dialogue.
     */
    function editStageHistory()
    {
        $recordId = $this->request->getParameter('id');
        $casId    = $this->request->getParameter('cas_id');

        if (isset($recordId) && is_numeric($recordId))
        {
            $stage = \DatixDBQuery::PDO_fetch('SELECT eventtype, eventdate FROM events_history WHERE recordid = :id', array('id' => $recordId));
        }

        $curStage = \Forms_SelectFieldFactory::createSelectField('stage_curstage', 'CLA', $stage['eventtype'], '');
        $curStage->setFieldFormatsName('cla_curstage');
        $curStage->setTable('claims_main');
        $stageLabel = \Labels_FieldLabel::GetFieldLabel('cla_curstage');

        require_once 'Source/libs/FormClasses.php';
        $date = new \FormField();
        $date->MakeDateField('stage_date', $stage['eventdate']);

        $dateLabel = _tk('stage_history_date');

        $callback = \Sanitize::SanitizeString($this->request->getParameter('callback'));

        if (GetParm('FMT_DATE_WEB') == 'US')
        {
            $date_fmt = 'mm/dd/yy';
        }
        else
        {
            $date_fmt = 'dd/mm/yy';
        }

        $this->response->build('src/claims/views/StageHistoryDialogue.php', array(
            'id' => $recordId,
            'cas_id' => $casId,
            'curstage' => $curStage,
            'stageLabel' => $stageLabel,
            'date' => $date,
            'dateLabel' => $dateLabel,
            'callback' => $callback,
            'table' => 'claims_main'
        ));

    }

    /**
     * Saves stage history records to the DB.
     */
    function saveStageHistory()
    {
        $stageDate = $this->request->getParameter('stage_date');
        $recordId  = $this->request->getParameter('id');
        $casId     = $this->request->getParameter('cas_id');
        $callback  = $this->request->getParameter('callback');

        $params = array(
            'eventtype' => $this->request->getParameter('stage_curstage'),
            'eventdate' => $stageDate == '' ? null : UserDateToSQLDate($stageDate)
        );

        if (isset($recordId) && is_numeric($recordId))
        {
            // update
            $params['recordid'] = $recordId;
            \DatixDBQuery::PDO_query('UPDATE events_history SET eventtype = :eventtype, eventdate = :eventdate WHERE recordid = :recordid', $params);
        }
        elseif (isset($casId) && is_numeric($casId))
        {
            // insert new
            $params['cas_id'] = $casId;
            $params['chainname'] = 'CSTG';
            $params['recordid'] = GetNextRecordID('events_history', false);
            \DatixDBQuery::PDO_build_and_insert('events_history', $params);
        }

        // Update record last updated
        $this->call('src\\generic\\controllers\\RecordController', 'updateRecordLastUpdated', [
            'module'   => 'CLA',
            'recordId' => $casId
        ]);

        if (isset($callback))
        {
            $this->redirect('app.php?'.$callback);
        }
    }

    /**
     * Deletes a selected stage history record from the DB.
     */
    function deleteStageHistory()
    {
        $recordId     = $this->request->getParameter('id');
        $mainRecordId = $this->request->getParameter('mainrecordid');

        if (isset($recordId) && is_numeric($recordId))
        {
            \DatixDBQuery::PDO_query('DELETE FROM events_history WHERE recordid = :recordid', array('recordid' => $recordId));
        }

        // Update record last updated
        if (isset($mainRecordId) && is_numeric($mainRecordId))
        {
            $this->call('src\\generic\\controllers\\RecordController', 'updateRecordLastUpdated', [
                'module'   => 'CLA',
                'recordId' => $mainRecordId
            ]);
        }
    }

    /**
     * Section used to display history of stages for claims module.
     */
    function stageHistory()
    {
        $print    = $this->request->getParameter('print');
        $Data     = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('FormType');
        $readonly = false;

        if ($print == 1 || $FormType == 'Print' || $FormType == 'ReadOnly' || $FormType == 'Locked')
        {
            $readonly = true;
        }

        $sql = '
            SELECT
                e.recordid, e.eventtype, e.eventdate, c.description
            FROM
                events_history e
            LEFT JOIN
                code_stages c ON c.code = e.eventtype
            WHERE
                chainname = \'CSTG\' AND cas_id = :id
            ORDER BY
                e.eventdate DESC
        ';

        $historyRecords = \DatixDBQuery::PDO_fetch_all($sql, ['id' => $Data['recordid']]);

        $cas_id = \Sanitize::SanitizeInt($Data['recordid']);

        // add Edit/Delete columns
        if (!$readonly)
        {
            foreach ($historyRecords as $key => $value)
            {
                $recordid = $historyRecords[$key]['recordid'];

                $historyRecords[$key]['edit'] = $this->call($this, 'stageHistoryEditButton', ['recordid' => $recordid]);
                $historyRecords[$key]['delete'] = $this->call($this, 'stageHistoryDeleteButton', ['recordid' => $recordid]);
            }
        }

        $RecordList = new \RecordLists_RecordList();
        $RecordList->AddRecordData($historyRecords);

        $columns = [
            'columns' => [
                new \Fields_DummyField(['name' => 'eventtype', 'label' => \Labels_FieldLabel::GetFieldLabel('cla_curstage'), 'type' => 'S']),
                new \Fields_DummyField(['name' => 'description', 'label' => _tk('stage_history_description'), 'type' => 'S']),
                new \Fields_DummyField(['name' => 'eventdate', 'label' => _tk('stage_history_date'), 'type' => 'D'])
            ]
        ];

        if (!$readonly)
        {
            $columns['columns'][] = new \Fields_DummyField(['name' => 'edit', 'label' => '', 'type' => 'X']);
            $columns['columns'][] = new \Fields_DummyField(['name' => 'delete', 'label' => '', 'type' => 'X']);
        }

        $Design = new \Listings_ListingDesign($columns);
        $listing = new \Listings_ListingDisplay($RecordList, $Design);

        $listing->ReadOnly = true;
        $callback = $_SERVER['QUERY_STRING'];

        if (\UnicodeString::strpos($callback, 'panel='))
        {
            $callback = preg_replace('/panel=[a-z_]+/u', 'panel=stagehistory', $callback);
        }
        else
        {
            $callback .= '&panel=stagehistory';
        }

        $callback = addslashes(urlencode($callback));

        if (GetParm('FMT_DATE_WEB') == 'US')
        {
            $date_fmt = 'mm/dd/yy';
        }
        else
        {
            $date_fmt = 'dd/mm/yy';
        }

        $this->response->build('src\claims\views\StageHistoryList.php', [
            'listing' => $listing,
            'readonly' => $readonly,
            'cas_id' => $cas_id,
            'callback' => $callback,
            'calendarOnClick' => GetParm('CALENDAR_ONCLICK', 'N'),
            'date_fmt' => $date_fmt,
            'calendarWeekend' => GetParm('CALENDAR_WEEKEND', 1),
            'weekStartDay' => GetParm('WEEK_START_DAY', 2)
        ]);
    }

    /**
     * Displays the edit button for the Stage History table.
     */
    public function stageHistoryEditButton()
    {
        $this->response->setBody('<a href="javascript:editStageHistory('.$this->request->getParameter('recordid').')">'._tk('stage_history_edit').'</a>');
    }

    /**
     * Displays the delete button for the Stage History table.
     */
    public function stageHistoryDeleteButton()
    {
        $this->response->setBody('<a id="stage_history_'.$this->request->getParameter('recordid').'" href="javascript:deleteStageHistory('.$this->request->getParameter('recordid').', '.$_REQUEST['recordid'].')">'._tk('stage_history_delete').'</a>');
    }
}