<?php

namespace src\claims\controllers;

use src\framework\controller\Controller;

class FinanceController extends Controller
{
    function ReserveAudit()
    {
        $reserveAudit = $this->getReserveAuditData();

        $this->response->build('src/claims/views/ReserveAudit.php', array(
            'reserveAudit' => $reserveAudit,
            'finIndemnityReserveLabel' => GetFieldLabel('fin_indemnity_reserve'),
            'finExpensesReserveLabel' => GetFieldLabel('fin_expenses_reserve')
        ));
    }

    private function getReserveAuditData()
    {
        // Initialise data
        $originalValue = null;
        $auditData     = array();

        // Get fin_indemnity_reserve audit
        $sql = '
            SELECT
                fin_indemnity_reserve_audit.field_value, fin_indemnity_reserve_audit.change_value, staff.fullname,
                fin_indemnity_reserve_audit.datetime_from, fin_indemnity_reserve_audit.reason_for_change
            FROM
                fin_indemnity_reserve_audit
            LEFT JOIN
                staff ON fin_indemnity_reserve_audit.changed_by = staff.initials
            WHERE
                fin_indemnity_reserve_audit.cla_id = :cla_id
            ORDER BY
              fin_indemnity_reserve_audit.datetime_from DESC
        ';

        $finIndemnityReserve = \DatixDBQuery::PDO_fetch_all($sql, array('cla_id' => $this->request->getParameter('recordid')));

        // Get fin_expenses_reserve audit
        $sql = '
            SELECT
                fin_expenses_reserve_audit.field_value, fin_expenses_reserve_audit.change_value, staff.fullname,
                fin_expenses_reserve_audit.datetime_from, fin_expenses_reserve_audit.reason_for_change
            FROM
                fin_expenses_reserve_audit
            LEFT JOIN
                staff ON fin_expenses_reserve_audit.changed_by = staff.initials
            WHERE
                fin_expenses_reserve_audit.cla_id = :cla_id
            ORDER BY
              fin_expenses_reserve_audit.datetime_from DESC
        ';

        $finExpensesReserve = \DatixDBQuery::PDO_fetch_all($sql, array('cla_id' => $this->request->getParameter('recordid')));

        foreach ($finIndemnityReserve as $row)
        {
            $auditData[$row['datetime_from']]['fin_indemnity_reserve'] = $row;
        }

        foreach ($finExpensesReserve as $row)
        {
            $auditData[$row['datetime_from']]['fin_expenses_reserve'] = $row;
        }

        foreach ($auditData as $dateTimeFrom => $row)
        {
            if (count($row) == 1)
            {
                if (array_key_exists('fin_indemnity_reserve', $row))
                {
                    $auditData[$dateTimeFrom]['fin_expenses_reserve'] = array('field_value' => _tk('no_change'));
                }
                elseif (array_key_exists('fin_expenses_reserve', $row))
                {
                    $auditData[$dateTimeFrom]['fin_indemnity_reserve'] = array('field_value' => _tk('no_change'));
                }
            }
        }

        // Sort the audit data by the dateTimeFrom field
        ksort($auditData);

        // Sort the audit data by the most recent dateTimeFrom
        $auditData = array_reverse($auditData, true);

        return $auditData;
    }

    public function updateReserveAuditData()
    {
        $data         = $this->request->getParameter('data');
        $dateTimeFrom = (new \DateTime())->format('Y-m-d H:i:s');
        foreach(array('indemnity', 'expenses') as $reserve)
        {
            if ($data['CHANGED-fin_'.$reserve.'_reserve'] == '1')
            {
                $oldValSQL = 'SELECT TOP 1 field_value
                          FROM fin_' . $reserve . '_reserve_audit
                          WHERE cla_id = :cla_id
                          ORDER BY datetime_from DESC';
                $oldVal = \DatixDBQuery::PDO_fetch($oldValSQL, array('cla_id' => $data['recordid']));
                $changeValue = ($oldVal ? $data['fin_' . $reserve . '_reserve'] - $oldVal['field_value'] : $data['fin_' . $reserve . '_reserve']);

                $sql = 'INSERT INTO fin_' . $reserve . '_reserve_audit
                    (cla_id, field_value, change_value, changed_by, datetime_from, reason_for_change)
                        VALUES
                    (:cla_id, :field_value, :change_value, :changed_by, :datetime_from, :reason_for_change)';

                $PDOParams = array(
                    'cla_id' => $data['recordid'],
                    'field_value' => $data['fin_' . $reserve . '_reserve'],
                    'change_value' => $changeValue,
                    'changed_by' => $_SESSION['initials'],
                    'datetime_from' => $dateTimeFrom,
                    'reason_for_change' => $data['reason_fin_' . $reserve . '_reserve']
                );

                \DatixDBQuery::PDO_insert($sql, $PDOParams);
            }
        }
    }

    /**
     * Displays the section "Payments summary" on a claim record.
     */
    public function paymentsSummary()
    {
        $searchType = $this->request->getParameter('searchtype');
        $Data       = $this->request->getParameter('data');
        $FormType   = $this->request->getParameter('FormType');
        $Module     = $this->request->getParameter('module');
        $payments   = [];

        if ($FormType != 'Search')
        {
            $SummaryTotal = 0;

            $sql = "
                SELECT
                    code, COALESCE(total, CAST(0 as DECIMAL(13,2))) AS total
                FROM
                    code_fin_type LEFT OUTER JOIN (
                        SELECT
                            pay_type, SUM(pay_calc_total) AS total
                        FROM
                            payments
                        WHERE
                            cla_id = :cla_id
                        GROUP BY
                            pay_type
                    ) p ON code_fin_type.code = p.pay_type
                WHERE
                    cod_priv_level IS NULL OR
                    (cod_priv_level != 'N' AND cod_priv_level != 'X')
            ";

            $PaymentTotals = \DatixDBQuery::PDO_fetch_all($sql, ['cla_id' => $Data['recordid']]);
            $FormType = 'ReadOnly';
        }
        else
        {
            $SummaryTotal = null;

            $sql = "
                SELECT
                    code, NULL as total
                FROM
                    code_fin_type
                WHERE
                    cod_priv_level IS NULL OR
                    (cod_priv_level != 'N' AND cod_priv_level != 'X')
            ";

            $PaymentTotals = \DatixDBQuery::PDO_fetch_all($sql);

            if ($FormType == 'Search' && $searchType == 'lastsearch')
            {
                foreach ($PaymentTotals as $key => $PaymentTotal)
                {
                    $PaymentTotals[$key]['total'] = $_SESSION[$Module]['LAST_SEARCH']['pay_type_'.$PaymentTotal['code']];
                }

                $SummaryTotal = $_SESSION[$Module]['LAST_SEARCH']['pay_type_PAYTOTAL'];
            }

        }

        foreach ($PaymentTotals as $PaymentTotal)
        {
            $TotalField = new \FormField($FormType);
            $TotalField->MakeMoneyField('pay_type_'.$PaymentTotal['code'], $PaymentTotal['total']);

            $payments[] = [
                'field' => $TotalField,
                'code'  => $PaymentTotal['code']
            ];

            if ($PaymentTotal['total'] && $FormType != 'Search')
            {
                $SummaryTotal += $PaymentTotal['total'];
            }
        }

        $TotalField = new \FormField($FormType);
        $TotalField->MakeMoneyField('pay_type_PAYTOTAL', $SummaryTotal);

        $this->response->build('src\claims\views\PaymentsSummary.php', [
            'payments'   => $payments,
            'TotalField' => $TotalField
        ]);
    }
}