<form id="stageHistoryForm" method="POST" action="<?php echo $this->scripturl; ?>?action=httprequest&type=savestagehistory&responsetype=json">
    <input name="id" type="hidden" value="<?php echo $this->id;  ?>" />
    <input name="cas_id" type="hidden" value="<?php echo $this->cas_id;  ?>" />
    <input name="callback" type="hidden" value="<?php echo $this->callback;  ?>" />
    <input name="table" type="hidden" value="<?php echo $this->table;  ?>" />
    <div style="clear:both; padding:10px; margin-bottom:10px;">
        <div class="field_label" style="float:left; height:100%; margin-right:10px; width:100px;"><?php echo $this->stageLabel; ?>:</div>
        <div style="float:left;"><?php echo $this->curstage->getField(); ?></div>
    </div>
    <div style="clear:both; padding:10px; margin-bottom:15px;">
        <div class="field_label" style="float:left; height:100%; margin-right:10px; width:100px;"><?php echo $this->dateLabel; ?>:</div>
        <div style="float:left;"><?php echo $this->date->getField(); ?></div>
    </div>
</form>