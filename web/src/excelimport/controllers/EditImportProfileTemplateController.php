<?php

namespace src\excelimport\controllers;

use src\framework\controller\TemplateController;

class EditImportProfileTemplateController extends TemplateController
{
    function editimportprofile()
    {
        global $mappingGridIDs;

        require_once 'Source/setups/ExcelImport.php';

        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        $recordid = ($this->request->getParameter('exp_profile_id') ? $this->request->getParameter('exp_profile_id') : '');

        $Profile = new \ImportProfile(new \DatixDBQuery(''), $recordid, $this->request->getParameter('module'));

        $FormArray = array(
            'profile_details' => array(
                'Title' => 'Import Profile Details',
                'Rows' => array(
                    array(
                        'Name' => 'exp_profile_id',
                        'Title' => 'ID',
                        'ReadOnly' => true,
                        'Type' => 'string'
                    ),
                    array(
                        'Name' => 'exp_profile_module',
                        'Title' => 'Module',
                        'ReadOnly' => true,
                        'Type' => 'ff_select',
                        'CustomCodes' => getModArray()
                    ),
                    array(
                        'Name' => 'exp_profile_name',
                        'Title' => 'Name',
                        'Type' => 'string'
                    ),
                    array(
                        'Name' => 'exp_start_row',
                        'Title' => 'Start at row',
                        'Type' => 'number'
                    ),
                    array(
                        'Name' => 'exp_profile_type',
                        'Title' => 'Import file type',
                        'Type' => 'ff_select',
                        'CustomCodes' => array('CSV' => 'CSV', 'EXC' => 'Excel')
                    ),
                    array(
                        'Name' => 'exp_profile_rowheader',
                        'Title' => 'Row header value',
                        'Type' => 'string'
                    ),
                )
            ),
            'columns' => array(
                'Title' => 'Column options',
                'Function' => 'ColumnOptionsSection',
                'Condition' => $Profile->getID() != '',
                'Rows' => array()
            ),
            'transformations' => array(
                'Title' => 'Transformations',
                'Function' => 'ColumnMappingSection',
                'Condition' => $Profile->getID() != '',
                'Rows' => array()
            )
        );

        $FormDesign = new \Forms_FormDesign();
        $FormDesign->MandatoryFields['exp_profile_type'] = 'profile_details';
        $FormDesign->MandatoryFields['exp_profile_name'] = 'profile_details';
        $FormDesign->DefaultValues['exp_profile_type'] = 'EXC';

        $JSValidation = MakeJavaScriptValidation('', $FormDesign, array(
            'exp_profile_type' => 'Import file type',
            'exp_profile_name' => 'Name'
        ));

        if ($Profile->getID())
        {
            $this->title = _tk('edit-import-profile');
        }
        else
        {
            $this->title = _tk('new-import-profile');
        }

        $Data = $Profile->getDataArray();

        $Mode = ($Data['exp_profile_id'] ? 'Edit' : 'New');

        $Table = new \FormTable($Mode, 'ADM', $FormDesign);
        $Table->MakeForm($FormArray, $Data, 'ADM');
        $Table->MakeTable();

        $this->module = 'ADM';

        $this->response->build('src/excelimport/views/EditImportProfile.php', array(
            'Table' => $Table,
            'Data' => $Data,
            'mappingGridIDs' => $mappingGridIDs,
            'JSValidation' => $JSValidation
        ));
    }
}