<?php

namespace src\excelimport\controllers;

use src\framework\controller\TemplateController;

class ImportImportProfileTemplateController extends TemplateController
{
    function importimportprofile()
    {
        $this->title = 'Import Profile';
        $this->module = 'ADM';
        $this->menuParameters = array(
            'extra_links' => array(array('label' => 'Import Profile', 'link' => 'action=importimportprofile'))
        );

        $this->response->build('src/excelimport/views/ImportImportProfile.php', array());
    }
}