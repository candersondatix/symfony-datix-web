<?php
namespace src\excelimport\model;

use src\framework\model\Mapper;
use src\framework\model\ModelFactory;
use src\framework\query\Query;

class ExcelProfileMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\excelimport\\model\\ExcelProfile';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'excel_profile';
    }
}