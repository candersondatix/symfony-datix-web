<?php

namespace src\extrafields\controllers;

use src\framework\controller\Controller;

class ExtraFieldController extends Controller
{
    public function checkMappingChanged()
    {
        $recordId = $this->request->getParameter('recordid');
        $field = $this->request->getParameter('field');

        $extraField = new \Fields_ExtraField($recordId);
        $extraFieldCode = $extraField->getCodeLike();

        if ($recordId && $field && $field != $extraFieldCode)
        {
            $this->response->setBody(_tk('check_mapping_changed'));
            return;
        }
    }
}