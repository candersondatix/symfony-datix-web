<?php

namespace src\contacts\controllers;

use src\framework\controller\Controller;

class ContactCheckController extends Controller
{
    function contactlist()
    {
        global $ClientFolder, $NoMainMenu, $FieldDefs;
        
        $ModuleDefs  = $this->registry->getModuleDefs();
        $contactRows = null;
        
        $NoMainMenu = true;

        $module = \Sanitize::getModule($this->request->getParameter('module'));

        if ($module == '')
        {
            $module = 'CON';
        }

        $modId = ModuleCodeToID($module);
        $fieldname = \Escape::EscapeEntities($this->request->getParameter('fieldname'));
        $ContactSuffix = \Escape::EscapeEntities($this->request->getParameter('suffix'));
        $FieldList = explode(',', \Sanitize::SanitizeString($this->request->getParameter('fieldlist')));
        $listingColumns = \Listings_LevelOneConMatchListing::GetListingColumns($this->request->getParameter('parentmodule'));
        $extraFieldList = array();
        
        foreach ($FieldList as $key => $field)
        {
            if (\UnicodeString::substr($field, 0, 3) == 'UDF')
            {
                $extraFieldList[] = $field;
                unset($FieldList[$key]);
            }
        }

        // so we know which UDFs to select values for - $_POST["fieldlist"] will only contain UDFs used as search criteria
        if (is_array($this->request->getParameter('extraFieldsOnForm')))
        {
            $extraFieldsOnForm = \Sanitize::SanitizeStringArray($this->request->getParameter('extraFieldsOnForm'));
        }

        $dateFields = array(
            'con_dob',
            'con_dod'
        );

        $pdoParams = array();
        
        $sql = '
            SELECT
            '.implode(', ', $ModuleDefs['CON']['FIELD_ARRAY']) . ',
            link_age = DATEDIFF(hour, con_dob, GETDATE())/8766,
            age_at_death = DATEDIFF(year, con_dob, con_dod),
            con_gender,
            recordid AS [con_id],
            updateid
        ';

        if (!empty($extraFieldsOnForm))
        {
            $join = '';

            foreach ($extraFieldsOnForm as $key => $extraField)
            {
                $udfParts = explode('_', $extraField);
                $udfColumn = $this->getUDFValueColumn($udfParts[1]);

                // remove suffix
                array_pop($udfParts);
                $extraField = implode('_', $udfParts);

                $sql .= ', udf'.$key.'.'.$udfColumn.' AS '.$extraField;
                $join .= ' LEFT JOIN udf_values udf'.$key.' ON udf'.$key.'.mod_id = :mod_id'.$key.'
                AND udf'.$key.'.cas_id = recordid
                AND udf'.$key.'.group_id = :group_id'.$key.'
                AND udf'.$key.'.field_id = :field_id'.$key.'';

                $pdoParams['mod_id'.$key] = $modId;
                $pdoParams['group_id'.$key] = $udfParts[2];
                $pdoParams['field_id'.$key] = $udfParts[3];

                if (in_array($extraField, $extraFieldList))
                {
                    // this UDF is being used for search criteria, so also construct WHERE clause portion
                    $FieldCriteria = $_POST[$extraField];

                    if (is_array($FieldCriteria))
                    {
                        $FieldCriteria = implode(' ', $FieldCriteria);
                    }

                    $FieldCriteria = str_replace('*', '%', $FieldCriteria);
                    $FieldCriteria = str_replace('?', '_', $FieldCriteria);

                    if ($udfParts[1] == 'D')
                    {
                        $FieldCriteria = UserDateToSQLDate($FieldCriteria);
                    }

                    $sqlwhere[] = 'udf'.$key.'.'.$udfColumn.' = :' . $extraField;
                    $pdoParams[$extraField] = $FieldCriteria;
                }
            }
        }

        $sqlwhere[] = 'rep_approved = \'FA\'';

        foreach ($FieldList as $field)
        {
            $FieldCriteria = $_POST[$field];

            if (is_array($_POST[$field]))
            {
                $FieldCriteria = implode(' ', $FieldCriteria);
            }

            $FieldCriteria = str_replace(array('*','?'), array('%','_'), $FieldCriteria);

            // Remove spaces only if is the NHS number (we don't want to remove spaces if it's not the NHS number because other fields can have space like forenames)
            if ($field == 'con_nhsno')
            {
                $FieldCriteria = str_replace(' ', '', $FieldCriteria);
            }

            if (in_array($field, $dateFields))
            {
                $FieldCriteria = UserDateToSQLDate($_POST[$field]);
            }

            if( in_array($field, array('con_nhsno', 'con_number')))
            {
            	$sqlwhere[] = "Replace(". $field .", ' ', '' ) = :" . $field;
            }
            else 
            {
            	$sqlwhere[] = $field . ' = :' . $field;
            }
            $pdoParams[$field] = $FieldCriteria;
        }

        $sql .= ' FROM contacts_main' . $join;

        if (!empty($sqlwhere))
        {
            $sql .= ' WHERE ' . implode(' AND ', $sqlwhere);
        }

        $sql .= ' ORDER BY con_surname';
        
        $resultArray = \DatixDBQuery::PDO_fetch_all($sql, $pdoParams);

        if (bYN(GetParm('EXTERNAL_CONTACTS', 'N')))
        {
            if (isset($GLOBALS['external_contact_address']))
            {
                // send http request to external system (e.g. Mirth)
                $externalRequest = $GLOBALS['external_contact_address'];
                $externalRequest .=  \UnicodeString::strpos($externalRequest, '?') === false ? '?' : '&';

                foreach (array_merge($FieldList, $extraFieldList) as $field)
                {
                    $externalRequest .= $field.'='.urlencode($_POST[$field]).'&';
                }

                $externalRequest = \UnicodeString::substr($externalRequest, 0, -1);
                $contactdetails = file_get_contents($externalRequest);
            }
            else
            {
                require_once $ClientFolder . '/external_src_config.php';
                $client = new \SoapClient($_SESSION['CON']['EXTERNAL_WSDL'], array('trace' => 1, 'exceptions' => 0));
                $contactdetails = ($client->getContact($_SESSION['login'], '', $_SESSION['session_id'], $_SESSION['CON']['EXTERNAL_WHERE']));
            }

            require_once 'soap/interfaceclass.php';

            $DatixInterface = new \DatixInterface();
            $DatixInterface->LoadXMLDoc($contactdetails);
            $DatixInterface->parseXMLDataToArray($request);
            $ExternalContactCount = 0;
        }

        if (count($resultArray) > 0)
        {
            $contactRows = $this->outputContactRows($resultArray, $module, $fieldname, $ContactSuffix, $listingColumns);
        }

        if (is_array($request) && !empty($request))
        {
            $contactRows = $this->outputContactRows($request, $module, $fieldname, $ContactSuffix, $listingColumns);
        }

        $this->response->build('src/contacts/views/ContactCheck.php', array(
            'listingColumns' => $listingColumns,
            'FieldDefs' => $FieldDefs,
            'resultArray' => $resultArray,
            'module' => $module,
            'fieldname' => $fieldname,
            'ContactSuffix' => $ContactSuffix,
            'request' => $request,
            'contactRows' => $contactRows
        ));
    }

    /**
     * Returns the udf_values table column used to store data for this extra field, based on the field type.
     *
     * @param string $type The extra field type.
     * @return string $column The corresponding database column name
     */
    private function getUDFValueColumn($type)
    {
        switch ($type)
        {
            case 'C':
            case 'T':
            case 'S':
            case 'Y':
                $column = 'udv_string';
                break;
            case 'N':
                $column = 'udv_number';
                break;
            case 'D':
                $column = 'udv_date';
                break;
            case 'M':
                $column = 'udv_money';
                break;
            case 'L':
                $column = 'udv_text';
                break;
        }

        return $column;
    }

    private function outputContactRows($contacts, $module, $fieldName, $contactSuffix, $listingColumns)
    {
        global $FieldDefs;

        $contactsArray       = array();
        $listingColumnsArray = array();

        foreach ($contacts as $id => $row)
        {
            if (!empty($row['con_dob']))
            {
                $row['con_dob'] = FormatDateVal($row['con_dob']);
            }

            if (!empty($row['con_dod']))
            {
                $row['con_dod'] = FormatDateVal($row['con_dod']);
                $row['link_age'] = $row['age_at_death'];
            }

            if (!empty($row['con_dopened']))
            {
                $row['con_dopened'] = FormatDateVal($row['con_dopened']);
            }

            if (!empty($row['con_dclosed']))
            {
                $row['con_dclosed'] = FormatDateVal($row['con_dclosed']);
            }

            foreach ($row as $Field => $FieldValue)
            {
                if ($FieldValue == '' || (is_array($FieldValue) && empty($FieldValue)))
                {
                    continue;
                }

                if (ini_get('magic_quotes_sybase'))
                {
                    $FieldValue = str_replace("'", "\\'", $row[$Field]);
                }
                elseif (ini_get('magic_quotes_gpc'))
                {
                    $FieldValue = addslashes($row[$Field]);
                }
                else
                {
                    $FieldValue = str_replace("'", "\\'", $row[$Field]);
                }

                $FieldValue = str_replace("\r\n", "\\n", $FieldValue);

                $FieldValue = str_replace("\"", "&quot;", $FieldValue);

                $FieldValue = str_replace('&#13;&#10;', '\n', $FieldValue);

                $FieldValue = str_replace("&#39;", "\\'", $FieldValue);

                if (\UnicodeString::substr($Field, 0, 3) == 'UDF')
                {
                    $udfParts = explode('_', $Field);
                    $fieldType = $udfParts[1];

                    if ($fieldType == 'D')
                    {
                        $FieldValue = FormatDateVal($FieldValue);
                    }
                }
                else
                {
                    $fieldType = $FieldDefs[$module][$Field]['Type'];
                }

                $contactsArray[$id][$Field] = array(
                    'field'      => $Field,
                    'fieldValue' => $FieldValue,
                    'fieldType'  => $fieldType
                );
            }

            foreach ($listingColumns as $column)
            {
                $field = $column->getName() == 'recordid' ? 'con_id' : $column->getName();

                switch ($FieldDefs['CON'][$field]['Type'])
                {
                    case 'ff_select':
                        $value = \src\security\Escaper::escapeForHTML(code_descr('CON', $field, $row[$field], '', false));
                        break;
                    case 'multilistbox':
                        $values = array();
                        $codes = explode(' ', $row[$field]);

                        foreach ($codes as $code)
                        {
                            $values[] = \src\security\Escaper::escapeForHTML(code_descr('CON', $field, $code, '', false));
                        }

                        $value = implode('<br />', $values);
                        break;
                    case 'time':
                        $value = $row[$field]{0} . $row[$field]{1} . ":" . $row[$field]{2} . $row[$field]{3};
                        break;
                    default:
                        $value = \src\security\Escaper::escapeForHTML($row[$field]);
                        break;
                }

                if ($field == 'link_age' && !empty($row["con_dod"]))
                {
                    $value .= ' (Age at death)';
                }

                $listingColumnsArray[$id][] = $value;
            }
        }

        $this->response->build('src/contacts/views/ContactRows.php', array(
            'contactsArray' => $contactsArray,
            'fieldName' => $fieldName,
            'contactSuffix' => $contactSuffix,
            'module' => $module,
            'listingColumnsArray' => $listingColumnsArray
        ));

        return $this->response;
    }
}