<?php

namespace src\contacts\controllers;

use src\framework\controller\Controller;

class ContactsSearchController extends Controller
{
    function contactssearch()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $_SESSION['CON']['PROMPT']['VALUES'] = array();

        if ($this->request->getParameter('validationerror'))
        {
            $error['message'] = '<br />There are invalid characters in one of the fields you have searched on.';
        }

        if ($this->request->getParameter('from_module'))
        {
            $_SESSION['FROMMODULE'] = $this->request->getParameter('from_module');
            $_SESSION[$_SESSION['FROMMODULE']]['CURRENTID'] = $this->request->getParameter($ModuleDefs[$_SESSION['FROMMODULE']]['FK']);
            $_SESSION[$_SESSION['FROMMODULE']]['CURRENTPANEL'] = $ModuleDefs[$_SESSION['FROMMODULE']]['CONTACTTYPES'][$this->request->getParameter('linktype')]['PanelName'];
            $_SESSION[$_SESSION['FROMMODULE']]['CURRENTLINKTYPE'] = $this->request->getParameter('linktype');
        }
        else
        {
            $_SESSION['FROMMODULE'] = '';
        }

        if ($this->request->getParameter('searchtype') == 'lastsearch')
        {
            $data = $_SESSION['CON']['LAST_SEARCH'];
        }
        else
        {
            $data = '';
        }

        $Parameters = array(
            'action' => 'newcontact',
            'form_action' => 'search'
        );

        if ($data != '')
        {
            $Parameters['con'] = $data;
        }

        if (isset($error) && $error != '')
        {
            $Parameters['error'] = $error;
        }

        $this->call('src\contacts\controllers\ShowContactFormTemplateController', 'newcontact', $Parameters);
    }
}