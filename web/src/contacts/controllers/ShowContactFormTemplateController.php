<?php

namespace src\contacts\controllers;

use src\framework\controller\TemplateController;

class ShowContactFormTemplateController extends TemplateController
{
    /**
     * Used for displaying the contact record from the Contacts module.
     */
    function newcontact()
    {
        global $Show_all_section, $DIF1UDFGroups, $FormTitle, $FormTitleDescr,
               $AccessLvlDefs, $FormType, $JSFunctions;

        if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && $this->request->getParameter('action') == 'editcontact' && ($FormType != "Print" && $this->request->getParameter('print') != 1))
        {
            $this->addJs('src/generic/js/panelData.js');
        }

        if($FormType == "Print" || $this->request->getParameter('print') == 1)
        {
            $this->addJs('src/generic/js/print.js');
        }

        $ModuleDefs = $this->registry->getModuleDefs();

        $aReturn = array();
        $con = $this->request->getParameter('data');
        $inc_id = $this->request->getParameter('inc_id');
        $error = $this->request->getParameter('error');
        $form_action = $this->request->getParameter('form_action');
        $qbe_recordid = $this->request->getParameter('qbe_recordid');

        AuditOpenRecord('CON', $con['recordid'], '');

        $CONPerms = GetParm('CON_PERMS');
        $MainModule = ($this->request->getParameter('from_module') ? \Sanitize::getModule($this->request->getParameter('from_module')) : 'CON');
        $inc_id = \Sanitize::SanitizeInt($inc_id);

        SetUpFormTypeAndApproval('CON', $con, $FormType, $form_action, $CONPerms);

        $this->sendToJs('FormType', $FormType);

        $LoggedIn = isset($_SESSION['logged_in']);

        CheckForRecordLocks('CON', $con, $FormType, $sLockMessage);

        // Load CON form settings
        $FormID = $this->request->getParameter('form_id');

        if ($this->request->getParameter('action') == 'newcontact' && $this->request->getParameter('form_action') != 'search')
        {
            $con['con_dopened'] = date('Y-m-d H:i:s.000');
        }

        if ($MainModule == 'SAB' && $_SESSION['Globals']['SAB_PERMS'] == 'SAB1')
        {
            $MainModuleLevel = 1;
        }
        else
        {
            $MainModuleLevel = 2;
        }

        $formDesignSettings = array(
            'module' => 'CON',
            'level' => 2,
            'form_type' => $FormType
        );

        if ($this->request->getParameter('linktype'))
        {
            // Needed for SAB search screen if nothing else.
            $formDesignSettings = array_merge($formDesignSettings, array(
                'link_type' => \Sanitize::SanitizeString($this->request->getParameter('linktype')),
                'parent_module' => $MainModule,
                'parent_level' => $MainModuleLevel
            ));
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign($formDesignSettings);
        $FormDesign->LoadFormDesignIntoGlobals();

        SetUpApprovalArrays('CON', $con, $CurrentApproveObj, $ApproveObj, $FormType, $FormDesign);

        include ($ModuleDefs['CON']['BASIC_FORM_FILES'][2]);

        if ($LoggedIn && $FormType != 'New')
        {
            $FormDesign->UnsetDefaultValues();
        }

        // Make the form here so it can be used to create the menu
        $ConTable = new \FormTable($FormType, 'CON', $FormDesign);
        $ConTable->MakeForm($FormArray, $con, "CON", array('NoLinkFields' => true));

        $this->sendToJs(array(
                'FormType' => $FormType,
                'printSections' => $ConTable->printSections)
        );

        $FormTitle = $FormDesign->FormTitle;

        if ($FormType == 'Search')
        {
            $FormTitle .= _tk('search_for_records');
        }
        elseif ($FormType == 'Print')
        {
            $FormTitle .= ' - Contact details';
        }
        elseif ($this->request->getParameter('action') == 'newcontact')
        {
            $FormTitle .= ' - Add new contact';
        }
        elseif ($this->request->getParameter('action') == 'editcontact')
        {
            $FormTitle .= ' - Edit contact';
        }
        else
        {
            $FormTitle .= ' - Contacts module';
        }

        $this->title = $FormTitle;
        $this->subtitle = $FormDesign->FormTitleDescr;
        $this->module = 'CON';
        $this->titleParameters = array(
            'data' => $con,
            'formtype' => $FormType
        );

        $ButtonGroup = new \ButtonGroup();

        if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Locked')
        {
            if ($FormType == 'Search')
            {
                $ButtonGroup->AddButton(array(
                    'label' => (isset($_SESSION['security_group']['grp_id']) ? 'Continue' : 'Search'),
                    'id' => 'btnSearch',
                    'name' => 'btnSearch',
                    'onclick' => 'document.frmContact.form_action.value=\'search\'; submitClicked = true; document.frmContact.submit();',
                    'action' => 'SEARCH'
                ));

                if ($_SESSION['FROMMODULE'] && $_SESSION[$_SESSION['FROMMODULE']]['CURRENTID'])
                {
                    $ButtonGroup->AddButton(array(
                        'label' => 'Cancel',
                        'id' => 'btnCancel',
                        'name' => 'btnCancel',
                        'onclick' => 'document.frmContact.form_action.value=\'BackTo\'; document.frmContact.rbWhat.value=\'BackTo\'; submitClicked = true; document.frmContact.submit();',
                        'action' => 'CANCEL'
                    ));
                }
                else
                {
                    $ButtonGroup->AddButton(array(
                        'label' => 'Cancel',
                        'id' => 'btnCancel',
                        'name' => 'btnCancel',
                        'onclick' => 'document.frmContact.form_action.value=\'Cancel\'; document.frmContact.rbWhat.value=\'Cancel\'; submitClicked = true; document.frmContact.submit();',
                        'action' => 'CANCEL'
                    ));
                }
            }
            elseif ($FormType == 'Edit')
            {
                $ButtonGroup->AddButton(array(
                    'label' => 'Save',
                    'id' => 'btnSave',
                    'name' => 'btnSave',
                    'onclick' => 'submitClicked = true; if(validateOnSubmit()){document.frmContact.form_action.value=\'editupdate\'; selectAllMultiCodes(); document.frmContact.submit();}',
                    'action' => 'SAVE'
                ));
                $ButtonGroup->AddButton(array(
                    'label' => 'Cancel',
                    'id' => 'btnCancel',
                    'name' => 'btnCancel',
                    'onclick' => 'document.frmContact.form_action.value=\'editcancel\'; '.getConfirmCancelJavascript('frmContact'),
                    'action' => 'CANCEL'
                ));
            }
            elseif ($FormType == 'New')
            {
                $ButtonGroup->AddButton(array(
                    'label' => 'Save',
                    'id' => 'btnSave',
                    'name' => 'btnSave',
                    'onclick' => 'submitClicked = true; if(validateOnSubmit()){document.frmContact.form_action.value=\'addnew\'; selectAllMultiCodes(); document.frmContact.submit();}',
                    'action' => 'SAVE'
                ));
                $ButtonGroup->AddButton(array(
                    'label' => 'Cancel',
                    'id' => 'btnCancel',
                    'name' => 'btnCancel',
                    'onclick' => 'document.frmContact.form_action.value=\'addnewcancel\'; '.getConfirmCancelJavascript('frmContact'),
                    'action' => 'CANCEL'
                ));
            }
        }
        elseif ($FormType != 'Print')
        {
            $ButtonGroup->AddButton(array(
                'label' => _tk('back_to_contacts_list'),
                'id' => 'btnCancel',
                'name' => 'btnCancel',
                'onclick' => 'document.frmContact.form_action.value=\'editcancel\'; document.frmContact.rbWhat.value=\'Cancel\'; submitClicked = true; document.frmContact.submit();',
                'action' => 'BACK'
            ));
        }

        if ($this->request->getParameter('fromlisting') == 1)
        {
            $ButtonGroup->AddButton(array(
                'label' => _tk('btn_back_to_report'),
                'id' => 'btnBack',
                'name' => 'btnBack',
                'onclick' => 'document.frmContact.form_action.value=\'BackToListing\'; document.frmContact.rbWhat.value=\'BackToListing\'; submitClicked = true; document.frmContact.submit();',
                'action' => 'BACK'
            ));
        }

        if ($_SESSION['CON']['RECORDLIST'] && $FormType != 'Print')
        {
            $CurrentIndex = $_SESSION['CON']['RECORDLIST']->getRecordIndex(array('recordid' => $con['recordid']));

            if ($CurrentIndex !== false)
            {
                $ButtonGroup->AddNavigationButtons($_SESSION['CON']['RECORDLIST'], $con['recordid'], 'CON');
            }
        }

        // If we are in "Print" mode, we don't want to display the menu
        if ($FormType != 'Print')
        {
            $this->menuParameters = array(
                'table' => $ConTable,
                'buttons' => $ButtonGroup,
                'no_audit' => true,
                'extra_links' => $ExtraLinks
            );
        }
        else
        {
            $this->hasMenu = false;
        }

        $qbe_return_module = filter_var($this->request->getParameter('qbe_return_module'), FILTER_SANITIZE_STRING);

        if (!$FormType)
        {
            $FormType = "edit";
        }

        $ConTable->MakeTable();

        $FormAction = '';
        $FormAction .= ($FormType=='Search' ? 'searchcontact' : 'contactlinkmainaction');
        $FormAction .= ($FormID ? '&form_id=' . \Sanitize::SanitizeInt($FormID) : '');
        $FormAction .= ($MainModule ? '&module=' . $MainModule : '');

        $FormOnSubmit = (($FormType != 'Print' && $FormType != 'Search') ? 'onsubmit="return submitClicked;"' : '');

        if (bYN($this->registry->getParm('VALID_NHSNO', 'Y')))
        {
            $JSFunctions[] = 'formatNhsNo(jQuery(\'#con_nhsno\'))';
        }

        $this->response->build('src/contacts/views/ShowContactForm.php', array(
            'FormType' => $FormType,
            'FormAction' => $FormAction,
            'FormOnSubmit' => $FormOnSubmit,
            'inc_id' => $inc_id,
            'con' => $con,
            'FormID' => $FormID,
            'fromlisting' => $this->request->getParameter('fromlisting'),
            'qbe_recordid' => $qbe_recordid,
            'qbe_return_module' => $qbe_return_module,
            'error' => $error,
            'sLockMessage' => $sLockMessage,
            'ConTable' => $ConTable,
            'ButtonGroup' => $ButtonGroup,
            'Show_all_section' => $GLOBALS['Show_all_section'],
            'panel' => $this->request->getParameter('panel')
        ));
    }
}