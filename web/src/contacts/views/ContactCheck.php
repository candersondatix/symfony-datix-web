<div id="contactlist_title">
    <span style="cursor: default;">
        <font size="3"><b>&nbsp;Matching contacts</b></font>
        <span style="cursor: pointer;" class="edit_options" onclick="ContactSelectionCtrl.destroy();">[x]</span>
    </span>
</div>
<div id="contactlist_container">
    <table id="contactlist_table" class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
        <tr class="tableHeader">
            <th class="titlebg" width="6%" align="center"><b>Choose</b></th>
            <?php foreach ($this->listingColumns as $column) : ?>
            <?php $defaultLabel = $column->getName() == 'link_age' ? 'Age' : $this->FieldDefs['CON'][$column->getName()]['Title']; ?>
            <th class="titlebg" align="left"><b><?php echo GetColumnLabel($column->getName(), $defaultLabel); ?></b></th>
            <?php endforeach; ?>
        </tr>
        <?php if (count($this->resultArray) > 0) : ?>
        <?php echo $this->contactRows; ?>
        <?php elseif (bYN(GetParm('EXTERNAL_CONTACTS', 'N'))) : ?>
            <?php if (is_array($this->request) && !empty($this->request)) : ?>
            <?php echo $this->contactRows; ?>
            <?php else : ?>
            <tr>
                <td class="windowbg2" colspan="4" align="left"><b>No matching contacts found</b></td>
            </tr>
            <?php endif; ?>
        <?php else : ?>
        <tr>
            <td class="windowbg2" colspan="4" align="left"><b>No matching contacts found</b></td>
        </tr>
        <?php endif; ?>
    </table>
</div>