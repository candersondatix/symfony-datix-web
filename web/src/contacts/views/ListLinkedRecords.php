<li name="linked_<?php echo $this->aParams['link_module']; ?>records_row" id="linked_<?php echo $this->aParams['link_module']; ?>records_row">
    <ol>
        <li class="section_title_row row_above_table">
            <?php echo $this->recNamePluralTitle.' linked to contact number '.$this->con_id; ?>
        </li>
        <?php if (count($this->LinkedRecords) == 0) : ?>
        <li class="section_link_row">
            <b>No linked <?php echo $this->recNamePlural; ?>.</b>
        </li>
        <?php else : ?>
        <li>
            <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
                <tr>
                    <?php foreach ($this->LinkedFields as $Field) : ?>
                    <td class="windowbg" align="center" width="6%"><b><?php echo GetFieldLabel($Field); ?></b></td>
                    <?php endforeach; ?>
                </tr>
                <?php foreach ($this->LinkedRecords as $row) : ?>
                <tr>
                    <?php foreach ($this->LinkedFields as $Field) : ?>
                    <td class="windowbg2" align="left" width="6%">
                        <?php echo (($this->print != 1 && $this->FormType != 'ReadOnly') ? '<a href="'.$this->scripturl.'?action='.$this->action.'&module='.$this->aParams['link_module'].'&recordid='.$row['recordid'].'">' : ''); ?>
                        <?php
                        switch ($this->FieldDefsExtra[$this->aParams["link_module"]][$Field]['Type'])
                        {
                            case 'ff_select':
                                echo \src\security\Escaper::escapeForHTML(code_descr($this->aParams["link_module"], $Field, $row[$Field], false));
                                break;
                            case 'date':
                                echo FormatDateVal($row[$Field]);
                                break;
                            default:
                                echo \src\security\Escaper::escapeForHTML($row[$Field]);
                        }
                        ?>
                        <?php echo (($this->print != 1 && $this->FormType != 'ReadOnly')? '</a>' : ''); ?>
                    </td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </table>
        </li>
        <?php endif; ?>
    </ol>
</li>