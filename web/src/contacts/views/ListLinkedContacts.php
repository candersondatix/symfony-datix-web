<li>
    <ol>
        <li name="contacts_row" id="contacts_row">
            <input type="hidden" name="contact_url" value="" />
            <?php if($this->panel == "contacts" && $this->contactwarning) : ?>
            <div class="new_windowbg">
                <b>The report has been saved. The reference number is <?php echo $this->recordid; ?></b>
            </div>
            <div class="new_windowbg form_error">
                There are still unapproved contacts attached to this record. They are listed below.
            </div>
            <?php endif; ?>
            <?php echo $this->Listing->GetListingHTML($this->FormType); ?>
            <?php if ($this->addNewContactUrl) : ?>
            <div class="new_link_row">
                <a href="javascript:if(CheckChange()){SendTo('<?php echo $this->addNewContactUrl; ?>');}" >
                    <b>
                        <?php if(_tk('create_new_link_'.$this->ExtraParameters['link_type'])) : ?>
                        <?php echo _tk('create_new_link_'.$this->ExtraParameters['link_type']); ?>
                        <?php else : ?>
                        <?php echo $this->ContactGroup['CreateNew']; ?>
                        <?php endif; ?>
                    </b>
                </a>
            </div>
            <?php endif; ?>
        </li>
    </ol>
</li>