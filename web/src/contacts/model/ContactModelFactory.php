<?php
namespace src\contacts\model;

use src\framework\model\ModelFactory;

class ContactModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ContactFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ContactMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ContactCollection($this->getMapper(), $this->getEntityFactory());
    }
}