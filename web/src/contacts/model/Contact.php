<?php
namespace src\contacts\model;

use src\framework\model\DatixEntity;

/**
 * This entity models a record in templates_main.
 */
class Contact extends DatixEntity
{
    /**
     * Mr/Mrs/Ms etc.
     *
     * @var string
     */
    protected $con_title;

    /**
     * Contact first/middle names
     *
     * @var string
     */
    protected $con_forenames;

    /**
     * Contact surname
     *
     * @var string
     */
    protected $con_surname;

    /**
     * Contact's job title
     *
     * @var string
     */
    protected $con_jobtitle;

    /**
     * The contact's organisation (note: this is a string, not a coded location field)
     *
     * @var string
     */
    protected $con_organisation;

    /**
     * The contact's address.
     *
     * @var string
     */
    protected $con_address;

    /**
     * The contact's postcode
     *
     * @var string
     */
    protected $con_postcode;

    /**
     * The contact's telephone number
     *
     * @var string
     */
    protected $con_tel1;

    /**
     * Alternate telephone number
     *
     * @var string
     */
    protected $con_tel2;

    /**
     * The contact's fax number
     *
     * @var string
     */
    protected $con_fax;

    /**
     * The contact's email address
     *
     * @var string
     */
    protected $con_email;

    /**
     * Coded field to classify contact.
     *
     * @var string
     */
    protected $con_type;

    /**
     * Coded field to classify contact.
     *
     * @var string
     */
    protected $con_subtype;

    /**
     * Coded field to classify contact.
     *
     * @var string
     */
    protected $con_empl_grade;

    /**
     * Contact payroll number.
     *
     * @var string
     */
    protected $con_payroll;

    /**
     * Contact nhs number. This has a specific format and shouldn't be reused for anything else.
     *
     * @var string
     */
    protected $con_nhsno;

    /**
     * Contact's date of birth
     *
     * @var date
     */
    protected $con_dob;

    /**
     * Contact's date of death
     *
     * @var date
     */
    protected $con_dod;

    /**
     * Date contact record was "opened"
     *
     * @var date
     */
    protected $con_dopened;

    /**
     * Date contact record was "closed"
     *
     * @var date
     */
    protected $con_dclosed;

    /**
     * Contact's gender (coded field)
     *
     * @var string
     */
    protected $con_gender;

    /**
     * Contact's ethnicity (coded field)
     *
     * @var string
     */
    protected $con_ethnicity;

    /**
     * Contact's language (coded field)
     *
     * @var string
     */
    protected $con_language;

    /**
     * Contact's disability (coded field)
     *
     * @var string
     */
    protected $con_disability;

    /**
     * Contact's religion (coded field)
     *
     * @var string
     */
    protected $con_religion;

    /**
     * Contact's sexual orientation (coded field)
     *
     * @var string
     */
    protected $con_sex_orientation;

    /**
     * Hospital ID, e.g. Staff/Patient number
     *
     * @var number
     */
    protected $con_number;

    /**
     * Approval status of contact
     *
     * @var string
     */
    protected $rep_approved;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_orgcode;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_clingroup;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_unit;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_directorate;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_specialty;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_loctype;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_locactual;

    /**
     * The set of locations this contact belongs to. Only applies if using hierarchical locations.
     *
     * @var string
     */
    protected $con_hier_location;

    /**
     * YesNo field indicating whether a contact should be included in staff dropdowns.
     *
     * @var string
     */
    protected $con_staff_include;

    /**
     * Unique ID from the AD used to identify contact when syncing with LDAP.
     *
     * @var string
     */
    protected $con_sid;

    /**
     * Domain that this contact has been synced from (used when syncing with LDAP).
     *
     * @var string
     */
    protected $con_domain;

    /**
     * The Police ID number for this contact. Only really applies if this contact is a member of the police.
     *
     * @var string
     */
    protected $con_police_number;

    /**
     * Has this contact been assessed as to the suitability of them working without supervision?.
     *
     * @var string
     */
    protected $con_work_alone_assessed;

    /**
     * Free text field for general contact notes.
     *
     * @var string
     */
    protected $con_notes;

    /**
     * String holding the combined first and surname of the contact.
     *
     * @var string
     */
    protected $fullname;

}