<?php
namespace src\contacts\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;

/**
 * Manages the persistance of Contact objects.
 */
class ContactMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\contacts\\model\\Contact';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'contacts_main';
    }
}