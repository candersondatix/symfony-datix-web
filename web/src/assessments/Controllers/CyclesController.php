<?php
namespace src\assessments\controllers;

use src\framework\controller\TemplateController;
use src\framework\query\Query;
use src\assessments\model\AssignedAssessmentTemplateModelFactory;
use src\assessments\model\exceptions\AssignedAssessmentExistsException;

/**
* Controls the process of redistibuting assessments for subsequent cycle years.
*/
class CyclesController extends TemplateController
{
    /**
    * Takes a set of flagged records from an assigned assessment listing and attempts to create
    * "OPEN" duplicates of them for the next cycle and assessment years.
    *
    * If successful, takes the user to a list of newly created assigned assessments.
    * If unsucessful, returns user to their assigned assessment listing with error messages
    */
    public function asmRedistributeTemplates()
    {
        global $scripturl;

        $ModuleDefs = $this->registry->getModuleDefs();
        
        //Ensure that some records have been selected
        if (empty($_SESSION['ATI']['RECORDLIST']) || empty($_SESSION['ATI']['RECORDLIST']->FlaggedRecords))
        {
            AddSessionMessage('ERROR', _tk('cycle_controller_select_records'));
            $this->redirect('?action=list&module=ATI');
        }

        //validate selected assigned assessment records:
        // -- assigned assessments must be "Reviewed"
        $numUnReviewed = \DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM asm_templates WHERE rep_approved != \'REVIEW\' and recordid IN ('.implode(',', $_SESSION['ATI']['RECORDLIST']->FlaggedRecords).')', array(), \PDO::FETCH_COLUMN);

        if ($numUnReviewed > 0)
        {
            AddSessionMessage('ERROR', _tk('cycle_controller_no_review'));
            $this->redirect('?action=list&module=ATI');
        }

        // -- assigned assessments cannot be in their final cycle year
        $finalCycleSql = '
            SELECT
                count(*) AS num
            FROM
                asm_templates
            WHERE
                ati_cycle IN (SELECT top 1 code FROM code_asm_cycle WHERE (cod_priv_level != \'X\' AND cod_priv_level != \'N\' OR cod_priv_level IS NULL) ORDER BY cod_listorder DESC, description DESC) AND
                recordid IN ('.implode(',', $_SESSION['ATI']['RECORDLIST']->FlaggedRecords).')
        ';
        $numFinalCycle = \DatixDBQuery::PDO_fetch($finalCycleSql, array(), \PDO::FETCH_COLUMN);

        if($numFinalCycle > 0)
        {
            AddSessionMessage('ERROR', _tk('cycle_controller_final_year'));
            $this->redirect('?action=list&module=ATI');
        }

        // -- assigned assessments cannot be in their final assessment period
        $finalPeriodSql = '
            SELECT
                count(*) AS num
            FROM
                asm_templates
            WHERE
                adm_year IN (SELECT top 1 code FROM code_asm_year WHERE (cod_priv_level != \'X\' AND cod_priv_level != \'N\' OR cod_priv_level IS NULL) ORDER BY cod_listorder DESC, description DESC) AND
                recordid IN ('.implode(',', $_SESSION['ATI']['RECORDLIST']->FlaggedRecords).')
        ';
        $numFinalPeriod = \DatixDBQuery::PDO_fetch($finalPeriodSql, array(), \PDO::FETCH_COLUMN);

        if ($numFinalPeriod > 0)
        {
            AddSessionMessage('ERROR', _tk('cycle_controller_final_period'));
            $this->redirect('?action=list&module=ATI');
        }

        // -- assigned assessments must have values for cycle year and assessment year
        $numIncompleteRecords = \DatixDBQuery::PDO_fetch('SELECT count(*) as num FROM asm_templates WHERE (adm_year = \'\' OR adm_year IS NULL OR ati_cycle = \'\' OR ati_cycle IS NULL) AND recordid IN ('.implode(',', $_SESSION['ATI']['RECORDLIST']->FlaggedRecords).')', array(), \PDO::FETCH_COLUMN);

        if($numIncompleteRecords > 0)
        {
            AddSessionMessage('ERROR', _tk('cycle_controller_missing_years'));
            $this->redirect('?action=list&module=ATI');
        }

        //initial validation complete: assessments can now be re-distributed to the next cycle and period as long as they are not duplicates.

        //First we need to set up mappings for the cycle and assessment year codes so they can be easily incremented
        $Cycles = \DatixDBQuery::PDO_fetch_all('SELECT code FROM code_asm_cycle WHERE cod_priv_level IN (\'\', \'Y\') OR cod_priv_level IS NULL ORDER BY cod_listorder asc, code asc', array(), \PDO::FETCH_COLUMN);

        foreach ($Cycles as $Code)
        {
            if ($PreviousCycleCode)
            {
                $CycleMappings[$PreviousCycleCode] = $Code;
            }
            $PreviousCycleCode = $Code;
        }

        $Years = \DatixDBQuery::PDO_fetch_all('SELECT code FROM code_asm_year WHERE cod_priv_level IN (\'\', \'Y\') OR cod_priv_level IS NULL ORDER BY cod_listorder asc, code asc', array(), \PDO::FETCH_COLUMN);

        foreach ($Years as $Code)
        {
            if ($PreviousYearCode)
            {
                $YearMappings[$PreviousYearCode] = $Code;
            }
            $PreviousYearCode = $Code;
        }
        
        $assignedTemplateModelFactory = new AssignedAssessmentTemplateModelFactory();
        $assignedTemplateMapper = $assignedTemplateModelFactory->getMapper();
        
        // retrieve a collection of assigned assessment templates, based on the selected records
        $assignedTemplates = $assignedTemplateModelFactory->getCollection();
        
        $query = new Query();
        $query->where(array('asm_templates.recordid' => $_SESSION['ATI']['RECORDLIST']->FlaggedRecords));
        
        $assignedTemplates->setQuery($query);
        
        // newly-created records are stored in order to produce the listing of new records once the process is complete
        $newIds = array();
        
        $db = new \DatixDBQuery();
        $db->beginTransaction();
        
        foreach ($assignedTemplates as $assignedTemplate)
        {
            // copy the assigned assessment template
            $newAssignedTemplate = clone $assignedTemplate;
            
            // defer notification e-mails until after all records have been created
            $newAssignedTemplate->setQueueEvents(true);
            
            // increment the cycle/assessment year values
            $newAssignedTemplate->ati_cycle = $CycleMappings[$assignedTemplate->ati_cycle];
            $newAssignedTemplate->adm_year  = $YearMappings[$assignedTemplate->adm_year];
            
            // insert the new assigned assessment templates (this process also handles insertion of nested assessment/criterion records)
            try
            {
                $assignedTemplateMapper->insert($newAssignedTemplate);
            }
            catch (AssignedAssessmentExistsException $e)
            {
                $db->rollBack();
                AddSessionMessage('ERROR', _tk('cycle_controller_already_redistributed'));
                $this->redirect('app.php?action=list&module=ATI');
            }
            $newIds[] = $newAssignedTemplate->recordid;
        }
        
        $db->commit();
        
        //If we are here then the re-distribution has succeeded
        AddSessionMessage('INFO', count($assignedTemplates).' assigned assessments created.');

        //Need to construct a listing to display the records created (the ids of which are stored in $DatixInterface->CreatedRecordids
        $Design = new \Listings_ModuleListingDesign(array('module' => 'ATI'));
        $Design->LoadColumnsFromDB();

        $RecordList = new \RecordLists_ModuleRecordList();
        $RecordList->Module = 'ATI';
        $RecordList->Columns = $Design->Columns;

        if (!empty($newIds))
        {
            $RecordList->WhereClause = MakeSecurityWhereClause('recordid IN ('.implode(',', $newIds).')', 'ATI');
            $RecordList->OrderBy = array(new \Listings_ListingColumn('recordid', 'asm_templates'));
            $RecordList->Paging = false;
            $RecordList->RetrieveRecords();

            //since we've created new records, we potentially need to update the session recordlist, otherwise the new records will not be "flaggable"
            $FlaggedRecordIds = $_SESSION['ATI']['RECORDLIST']->FlaggedRecords;
            $_SESSION['ATI']['RECORDLIST'] = \RecordLists_RecordListShell::CreateForModule('ATI', $_SESSION['ATI']['WHERE']);
            foreach($FlaggedRecordIds as $Recordid)
            {
                $_SESSION['ATI']['RECORDLIST']->FlagRecord($Recordid);
            }
        }

        $ListingDisplay = new \Listings_ListingDisplay($RecordList, $Design);

        $this->title = _tk('ati_listing');
        $this->module = 'ATI';

        $this->response->build('src/assessments/views/asmRedistributeTemplates.php', array(
            'ListingDisplay' => $ListingDisplay
        ));
    }
}
