<?php
namespace src\assessments\model;

use src\framework\model\ModelFactory;

class CriterionModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new CriterionFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new CriterionMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new CriterionCollection($this->getMapper(), $this->getEntityFactory());
    }
}