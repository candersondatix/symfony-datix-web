<?php
namespace src\assessments\model;

use src\framework\model\DatixEntity;


/**
 * A question on the assessment.
 */
class Criterion extends DatixEntity
{
    /**
     * The template that this instance is based on.
     * 
     * @var int
     */
    protected $asm_question_template_id;
    
    /**
     * The id of the parent module record.
     * 
     * @var int
     */
    protected $asm_module_id;
    
    /**
     * The user responsible assessiong the criterion.
     * 
     * @var string
     */
    protected $adq_end_user;
    
    /**
     * The compliance of the organisation with the criterion.
     * 
     * @var string
     */
    protected $adq_compliance;
    
    /**
     * The action plan.
     * 
     * @var string
     */
    protected $adq_action_plan;
    
    /**
     * General comments.
     * 
     * @var string
     */
    protected $adq_comments;
    
    
    /**
     * {@inheritdoc}
     */
    public function __clone()
    {
        parent::__clone();
        $this->asm_module_id = null;
    }
}