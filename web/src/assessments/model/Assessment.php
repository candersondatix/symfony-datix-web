<?php
namespace src\assessments\model;

use src\framework\model\DatixWorkflowEntity;
use src\framework\query\Query;

/**
 * An assessment to be conducted by a specific location.
 */
class Assessment extends DatixWorkflowEntity
{
    /**
     * The id of the assigned template used to generate the assessment instance.
     * 
     * @var int
     */
    protected $asm_template_instance_id;
    
    /**
     * The assessment location.
     * 
     * @var int
     */
    protected $adm_location;
    
    /**
     * The assessment coordinator's initials.
     * 
     * @var string
     */
    protected $adm_coordinator;
    
    /**
     * The assessment sub-coordinator's initials.
     * 
     * @var string
     */
    protected $adm_sub_coordinator;
    
    /**
     * The assessment end user's initials.
     * 
     * @var string
     */
    protected $adm_end_user;
    
    /**
     * The initials of the user who generated the assessment record.
     * 
     * @var unknown
     */
    protected $generatedby;

    /**
     * General comments for this assessment.
     * 
     * @var string
     */
    protected $adm_comments;
    
    /**
     * The criteria that make up this assessment.
     * 
     * @var CriterionCollection
     */
    protected $criteria;
    
    /**
     * Used to track when the sub coordinator is updated.
     * 
     * @var Boolean
     */
    protected $sub_coordinator_changed;
    
    /**
     * Used to track when the end user is updated.
     * 
     * @var Boolean
     */
    protected $end_user_changed;
    
    public function __construct($id = null)
    {
        $criterionFactory              = new CriterionModelFactory();
        $this->criteria                = $criterionFactory->getCollection();
        $this->sub_coordinator_changed = false;
        $this->end_user_changed        = false;
        parent::__construct($id);
    }
    
    /**
     * {@inheritdoc}
     */
    public function __clone()
    {
        parent::__clone();
        $this->asm_template_instance_id = null;
        $this->generatedby              = null;
        $this->rep_approved             = 'OPEN';
    }
    
    /**
     * {@inheritdoc}
     */
    public function setRecordid($id)
    {
        $this->recordid = (int) $id;
        
        // define how the criteria collection is linked to this entity
        $query = new Query();
        $this->criteria->setQuery($query->where(array('asm_questions.asm_module_id' => $this->recordid)));
    }
    
    /**
     * Setter for adm_sub_coordinator.
     *
     * @param string $adm_sub_coordinator
     */
    public function setAdm_sub_coordinator($adm_sub_coordinator)
    {
        if ($this->adm_sub_coordinator !== null)
        {
            $this->sub_coordinator_changed = true;
        }
        $this->adm_sub_coordinator = $adm_sub_coordinator;
    }
    
    /**
     * Setter for adm_end_user.
     *
     * @param string $adm_end_user
     */
    public function setAdm_end_user($adm_end_user) 
    {
        if ($this->adm_end_user !== null)
        {
            $this->end_user_changed = true;
        }
        $this->adm_end_user = $adm_end_user;
    }
    
    /**
     * Getter for sub_coordinator_changed.
     *
     * @return Boolean
     */
    public function getSub_coordinator_changed()
    {
        return $this->sub_coordinator_changed;
    }
    
    /**
     * Getter for end_user_changed.
     *
     * @return Boolean
     */
    public function getEnd_user_changed()
    {
        return $this->end_user_changed;
    }
}