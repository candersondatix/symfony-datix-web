<?php
namespace src\assessments\model;

use src\framework\model\ModelFactory;

class AssignedAssessmentTemplateModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new AssignedAssessmentTemplateFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        $assessmentModelFactory = new AssessmentModelFactory();
        return new AssignedAssessmentTemplateMapper(new \DatixDBQuery(), $this, $assessmentModelFactory->getMapper());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new AssignedAssessmentTemplateCollection($this->getMapper(), $this->getEntityFactory());
    }
}