<?php
namespace src\assessments\filters;

use src\framework\controller\ControllerFilter;

/**
* Secures controllers on the basis of users having the global ADM_GROUP_SETUP set.
*/
class CanCreateInstancesFilter extends ControllerFilter
{
    /**
    * Checks that the user does not have the global ADM_GROUP_SETUP set to "N"
    * 
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException
    */
    public function doAction($action)
    {
        if (CanSeeModule('LOC') && (CanSeeModule('ATM') || CanSeeModule('ATI')) && bYN(GetParm('ATM_CAI', 'N')))
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        } 
    }
}