<?php
namespace src\assessments\emails;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\assessments\model\AssignedAssessmentTemplate;
use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

// TODO - the duplicated function emailLocations() (src/assessments/controllers/AssessmentTemplateController.php)
//        needs to be removed once the accreditation module has been converted to use the new model structure.
class AtiCreateNotifier implements Observer
{
    /**
     * {@inheritdoc}
     * 
     * Email users that are assigned to the assessment template's location.
     * 
     * @throws InvalidArgumentException If the subject is not an AssignedAssessmentTemplate.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof AssignedAssessmentTemplate))
        {
            throw new \InvalidArgumentException('Object of type AssignedAssessmentTemplate expected, '.get_class($subject).' given.');
        }

        if ($event != Entity::EVENT_INSERT)
        {
            // this observer only listens to insert events
            return;
        }
        
        $data = $this->buildData($subject);
        $data['scripturl'] = $GLOBALS['scripturl'] . '?action=record&module=ATI&recordid=' . $subject->recordid;

        $usersDetails = $this->getUsers($data['ati_location']);

        $userFactory = new UserModelFactory();

        foreach ($usersDetails as $user)
        {
            if ($user['email'] != '' && empty($user['con_dod']) && bYN(GetUserParm($user['login'], 'ATI_LOC_EMAIL', 'N')))
            {
                Registry::getInstance()->getLogger()->logEmail('E-mailing user ' . $user['fullname'] . '(' . $user['email'] . ') assigned to the Assessment template ' . $data['recordid'] . ' location.');

                $recipient = $userFactory->getMapper()->findById($user['recordid']);

                $emailSender = EmailSenderFactory::createEmailSender('ATI', 'NewLocation');
                $emailSender->addRecipient($recipient);
                $emailSender->sendEmails(array_merge($recipient->getVars(), $data));
            }
        }
    }
    
    /**
     * Retrieves the data that is made available to merge into the e-mail.
     * 
     * TODO - the parent template information should be made available via the assigned template object itself.
     * 
     * @param AssignedAssessmentTemplate $template
     * 
     * @return array
     */
    protected function buildData(AssignedAssessmentTemplate $template)
    {
        // delegate to the additional location notifier function, rather than duplicate here
        $obj = new AtiAddLocationNotifier();
        return $obj->buildData($template);
    }
    
    /**
     * Retrieves a set of users assigned to a location.
     * 
     * @param int $location
     * 
     * @return array
     */
    protected function getUsers($location)
    {
        // delegate to the additional location notifier function, rather than duplicate here
        $obj = new AtiAddLocationNotifier();
        return $obj->getUserPerLocation($location);
    }
}