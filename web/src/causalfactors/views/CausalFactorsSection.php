<?php echo $this->CausalFactorsHTML; //There was an if statement surrounding this, which was blocking this coming up in searches ?>
<script language="javascript">
    dif1_section_suffix['<?php echo $this->ExtraParams['causal_factor_name']; ?>'] = <?php echo ($this->NumCausalFactors + 1); ?>
</script>
<?php if ($this->FormType != 'Print' && $this->FormType != 'ReadOnly' && $this->FormType != 'Search') : ?>
<li class="new_windowbg" id="add_another_<?php echo $this->ExtraParams['causal_factor_name']; ?>_button_list">
    <input type="button" id="section_<?php echo $this->ExtraParams['causal_factor_name']; ?>_button" value="<?php echo _tk('btn_add_another')?>" onclick="AddSectionToForm('<?php echo $this->ExtraParams['causal_factor_name']; ?>', '', $('add_another_<?php echo $this->ExtraParams['causal_factor_name']; ?>_button_list'), '<?php echo $this->module; ?>', '', false, <?php echo $this->spellChecker; ?>, <?php echo (is_numeric($this->form_id) ? Sanitize::SanitizeInt($this->form_id) : 'null'); ?>);">
</li>
<?php endif; ?>