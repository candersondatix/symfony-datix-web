<?php

namespace src\causalfactors\controllers;

use src\framework\controller\Controller;

class CausalFactorsController extends Controller
{
    /**
     * Output Overall Causal Factors section
     */
    public function DoCausalFactorsSection()
    {
        $data = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('FormType');
        $module = $this->request->getParameter('module');
        $ExtraParams = $this->request->getParameter('extraParameters');
        $CausalFactorsHTML = '';

        //need to work out how many Causal factors to display, and then display them.
        if ($FormType != 'Search')
        {
            if (isset($data['error']))
            {
                $aCausalFactors = $this->getLinkedCausalFactorsFromPost($data, $module);
            }
            else
            {
                $aCausalFactors = $this->getLinkedCausalFactors(array(
                    'recordid' => $data['recordid'],
                    'module' => $module
                ));
            }

            foreach ($aCausalFactors as $id => $LinkedCausalFactor)
            {
                $CausalFactorsHTML .= $this->getCausalFactorsSectionHTML(array(
                    'module' => $module,
                    'data' => $LinkedCausalFactor,
                    'formtype' => $FormType,
                    'suffix' => (intval($id) + 1),
                    'clearsection' => ($id == 0),
                    'causal_factor_name' => $ExtraParams['causal_factor_name']
                ));
            }

            $NumCausalFactors = count($aCausalFactors);
        }

        if (count($aCausalFactors) == 0) //no linked Causal factors
        {
            $CausalFactorsHTML = $this->getCausalFactorsSectionHTML(array(
                'module' => $module,
                'data' => $data,
                'formtype' => $FormType,
                'suffix' => 1,
                'clearsection' => true,
                'causal_factor_name' => $ExtraParams['causal_factor_name']
            ));
            $NumCausalFactors = 1;
        }

        if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Search')
        {
            $spellChecker = $_SESSION['Globals']['WEB_SPELLCHECKER'] == 'Y' ? 'true' : 'false';
        }

        $this->response->build('src\causalfactors\views\CausalFactorsSection.php', array(
            'FormType' => $FormType,
            'aCausalFactors' => $aCausalFactors,
            'module' => $module,
            'ExtraParams' => $ExtraParams,
            'data' => $data,
            'NumCausalFactors' => $NumCausalFactors,
            'spellChecker' => $spellChecker,
            'form_id' => $this->request->getParameter('form_id'),
            'CausalFactorsHTML' => $CausalFactorsHTML
        ));
    }

    /**
     * Retrieve linked causal factors
     * TODO: This should be refatored once we have entities
     *
     * @param array $aParams
     * @return array
     */
    private function getLinkedCausalFactors($aParams)
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $module = $aParams['module'];
        $recordid = $aParams['recordid'];
        $aCausalFactors = array();

        if ($module && $recordid)
        {
            $Rows = $this->getCausalFactorFieldsForSave(array('module' => $module));

            $sql = '
                SELECT
                    recordid as causal_factor_id, '.implode(', ', $Rows['Rows']).'
                FROM
                    causal_factors
                WHERE ' .
                    $ModuleDefs[$module]['FK'] . ' = :fk_id
                ORDER BY
                    listorder
            ';

            $result = \DatixDBQuery::PDO_fetch_all($sql, array('fk_id' => $recordid));

            foreach ($result as $row)
            {
                $aCausalFactors[] = $row;
            }
        }

        return $aCausalFactors;
    }

    /**
     * Creates an array of causal records.
     * Used to populate the Subjects section when returning to the form after a validation error.
     * TODO: This should be refatored once we have entities
     *
     * @param  array  $data     The POST data.
     * @param  string $module   The current module.
     * @return array  $causalfactors
     */
    private function getLinkedCausalFactorsFromPost($data, $module)
    {
        $causalfactors = array();
        $fields = $this->getCausalFactorFieldsForSave(array('module' => $module));

        for ($i = 1; $i < $data[\UnicodeString::strtolower($module).'_causal_factor_max_suffix']; $i++)
        {
            $causalfactor = array();

            foreach ($fields['Rows'] as $field)
            {
                $causalfactor[$field] = $field == 'listorder' ? $data[\UnicodeString::strtolower($module).'_causal_factor_listorder'.'_'.$i] : $data[$field.'_'.$i];
            }

            $causalfactors[] = $causalfactor;
        }

        return $causalfactors;
    }

    /**
     * Output Overall Causal Factors section
     *
     * @param array $aParams
     */
    public function getCausalFactorsSectionHTML($aParams)
    {
        global $formlevel;
        if($formlevel === null)
        {
            $formlevel = ($this->request->getParameter('form_level') ?: 1);
        }

        $aParams = ($this->request->getParameter('aParams') ? $this->request->getParameter('aParams') : $aParams);

        $SavedFormDesignSettings = saveFormDesignSettings();
        unsetFormDesignSettings();

        $AJAX = $aParams['ajax'];

        if ($aParams['formtype'] == 'Search')
        {
            $aParams['suffix'] = null;
        }

        if ($AJAX)
        {
            $formlevel = $aParams['level'];
        }

        $aCausalFactorRows = $this->getBasicCausalFactorForm($aParams);

        $CausalFactorsFormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => $aParams['module'],
            'level' => $formlevel
        ));
        $CausalFactorsFormDesign = $this->ModifyFormDesignForCausalFactors($CausalFactorsFormDesign);

        if ($aParams['suffix'])
        {
            $CausalFactorsFormDesign->AddSuffixToFormDesign($aParams['suffix']);
            $aParams['data'] = AddSuffixToData($aParams['data'], $aParams['suffix'], $this->getCausalFactorFieldsForSave(array('module' => $aParams['module'])));
        }

        $oCausalFactorsTable = new \FormTable($aParams['formtype'], $aParams['module'], $CausalFactorsFormDesign);
        $oCausalFactorsTable->ffTable = $aParams['module'] . 'CAF';
        $oCausalFactorsTable->MakeForm($aCausalFactorRows, $aParams['data'], $aParams['module'], array('dynamic_section' => true));

        loadFormDesignSettings($SavedFormDesignSettings);

        $this->response->build('src\causalfactors\views\OverallCausalFactorsSection.php', array(
            'AJAX' => $AJAX,
            'aParams' => $aParams,
            'oCausalFactorsTable' => $oCausalFactorsTable
        ));

        return $this->response->__toString();
    }

    /**
     * Return basic form definitions for causal factors
     * TODO: This should be refatored once we have entities
     *
     * @param array $aParams
     * @return array
     */
    public function getBasicCausalFactorForm($aParams)
    {
        $aParams = ($this->request->getParameter('aParams') ? $this->request->getParameter('aParams') : $aParams);
        $ModuleDefs = $this->registry->getModuleDefs();

        $aParams['causal_factor_name'] = $ModuleDefs[$aParams['module']]['CAUSAL_FACTOR_TYPE'];
        $RowList = $this->GetCausalFactorRowList($aParams);

        if ($aParams['rows'])
        {
            return array('Rows' => $RowList);
        }
        else
        {
            return array(
                'Parameters' => array('Suffix' => $aParams['suffix']),
                'causal_factor'.($aParams['suffix'] ? '_'.$aParams['suffix'] : '') => array(
                    'Title' => 'Causal factor',
                    'OrderField' => array(
                        'id' => $aParams['causal_factor_name'].'_listorder_'.$aParams['suffix'],
                        'value' => $aParams['data']['listorder']
                    ),
                    'ClearSectionOption' => $aParams['clearsection'],
                    'DeleteSectionOption' => !$aParams['clearsection'],
                    'ContactSuffix' => $aParams['suffix'],
                    'DynamicSectionType' => $aParams['causal_factor_name'],
                    'Rows' => $RowList,
                    'ContainedIn' => 'causal_factor',
                )
            );
        }
    }

    /**
     * Return list of fields for Causal factors
     * TODO: This should be refatored once we have entities
     *
     * @param array $aParams
     * @return mixed
     */
    private function GetCausalFactorRowList($aParams)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $RowList = $ModuleDefs[$aParams['module']]['LINKED_RECORDS'][$ModuleDefs[$aParams['module']]['CAUSAL_FACTOR_TYPE']]['basic_form']['Rows'];

        return $RowList;
    }

    /**
     * Return list of fields for Causal factors for saving.
     * TODO: This should be refatored once we have entities
     *
     * @param mixed $aParams
     * @return mixed Array containing list of fields for Causal factor
     */
    private function getCausalFactorFieldsForSave($aParams)
    {
        $RowList = $this->GetCausalFactorRowList($aParams);
        $RowList[] = 'listorder';

        return array('Rows' => $RowList);
    }

    /**
     * Return form design for Causal factors
     * TODO: This should be refatored once we have entities
     *
     * @param \Forms_FormDesign $design
     * @return \Forms_FormDesign
     */
    public function ModifyFormDesignForCausalFactors(\Forms_FormDesign $design)
    {
        if (is_array($design->ExpandSections))
        {
            $NewArray = array();

            foreach ($design->ExpandSections as $FieldName => $SectionArray)
            {
                foreach ($SectionArray as $Details)
                {
                    if ($Details['section'] != 'causal_factor')
                    {
                        $NewArray[$FieldName][] = $Details;
                    }
                }
            }

            $design->ExpandSections = $NewArray;
        }

        // remove extra sections/extra fields
        unset($design->ExtraSections);
        unset($design->ExtraFields);

        return $design;
    }
}