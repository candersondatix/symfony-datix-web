<?php
namespace src\payments\model;
use src\framework\model\EntityCollection;

class PaymentCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\payments\\model\\Payment';
    }
}