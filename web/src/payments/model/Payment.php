<?php
namespace src\payments\model;
use src\framework\model\DatixEntity;
/**
 * This entity models a payment in the system
 */
class Payment extends DatixEntity
{
    /**
     * The date the payment was made on
     *
     * @var string
     */
    protected $pay_date;

    /**
     * Codes for the type of payment
     *
     * @var string
     */
    protected $pay_type;

    /**
     * Code for subtype of payment
     *
     * @var string
     */
    protected $pay_subtype;

    /**
     * Amount of the payment before VAT
     *
     * @var string
     */
    protected $pay_amount;

    /**
     * Percentage value for VAT to add to the payment
     *
     * @var string
     */
    protected $pay_vat_rate;

    /**
     * Calculated field showing the pay_amount plus VAT
     *
     * @var string
     */
    protected $pay_total;

    /**
     * Notes for payment
     *
     * @var string
     */
    protected $pay_notes;

    /**
     * Literal amount for VAT to add to the payment
     *
     * @var string
     */
    protected $pay_calc_vat_amount;

    /**
     * Total payment (amount + VAT)
     *
     * @var string
     */
    protected $pay_calc_total;

    /**
     * Module payment is against
     *
     * @var string
     */
    protected $pay_module;

    /**
     * ID for linked data on payment
     *
     * @var string
     */
    protected $pay_cas_id;

    /**
     * Title of payment as linked data
     *
     * @var string
     */
    protected $pay_link_title;

    /**
     * ID of the respondent that made the payment
     *
     * @var int
     */
    protected $resp_id;

    /**
     * ID of the recipient for the payment
     *
     * @var int
     */
    protected $pay_recipient;

}