<?php
namespace src\payments\model\respondentfield;

use src\system\database\code\CodeMapper;
use src\framework\query\Query;

class RespondentFieldCodeMapper extends CodeMapper
{
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->select(['resp_id AS code', 'name AS description'])
            ->from('vw_respondents_joined')
            ->orderBy(array('name'));

        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }
}
