<?php
namespace src\payments\model\respondentfield;

use src\system\database\code\CodeModelFactory;

class RespondentFieldCodeModelFactory extends CodeModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new RespondentFieldCodeMapper(new \DatixDBQuery(), $this);
    }
}