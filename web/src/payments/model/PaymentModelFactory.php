<?php
namespace src\payments\model;

use src\framework\model\ModelFactory;

class PaymentModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new PaymentFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new PaymentMapper(new \DatixDBQuery(), $this);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new PaymentCollection($this->getMapper(), $this->getEntityFactory());
    }
}