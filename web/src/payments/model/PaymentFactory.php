<?php
namespace src\payments\model;
use src\framework\model\EntityFactory;

/**
 * Constructs CodeTagGroup objects.
 */
class PaymentFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\payments\\model\\Payment';
    }
}