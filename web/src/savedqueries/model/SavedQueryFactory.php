<?php
namespace src\savedqueries\model;

use src\framework\model\EntityFactory;
use src\framework\model\Entity;
use src\framework\model\EntityCache;
use src\framework\registry\Registry;

class SavedQueryFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\savedqueries\\model\\SavedQuery';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null, Registry $registry = null)
    {
        $savedQuery = parent::doCreateObject($data, $baseEntity, $cache, $registry);
        
        if ((int) $savedQuery->recordid == 0 && (int) $savedQuery->old_query_id == 0) // i.e. we're using "current criteria"
        {
            if ($_SESSION[$data['module']]['list_type'] != '')
            {
                // the query is based on a predefined listing
                $savedQuery->list_type = $_SESSION[$data['module']]['list_type'];
            }
            
            if ($_SESSION[$data['module']]['OVERDUE'])
            {
                // the query is based on an overdue listing
                $savedQuery->is_overdue = true;
            }
        }
        
        return $savedQuery;
    }
}