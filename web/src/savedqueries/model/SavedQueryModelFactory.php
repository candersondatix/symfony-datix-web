<?php
namespace src\savedqueries\model;

use src\framework\model\ModelFactory;

class SavedQueryModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new SavedQueryFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new SavedQueryMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new SavedQueryCollection($this->getMapper(), $this->getEntityFactory());
    }
}