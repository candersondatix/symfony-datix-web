<?php
namespace src\savedqueries\controllers;

use src\framework\controller\Controller;
use src\framework\query\SqlWriter;
use src\savedqueries\model\SavedQueryModelFactory;

class SavedQueriesController extends Controller
{
    /**
     * AJAX function echos out where clause for a query.
     * @return mixed
     */
    function getquerywhere()
    {
        //We need to use the old queries table here, because new query objects do not necessarily have independent where
        //clauses, e.g. for extra fields.
        $sqlWhereClause = \DatixDBQuery::PDO_fetch('SELECT sq_where FROM queries WHERE recordid = :recordid', array('recordid' => $this->request->getParameter('recordid')), \PDO::FETCH_COLUMN);

        echo json_encode(array('where' => $sqlWhereClause));
    }

    public function isQueryPinned()
    {
        $module       = $this->request->getParameter('module');
        $qryId        = $this->request->getParameter('qryId');
        $pinnedGlobal = GetParm($module.'_SAVED_QUERIES', false);
        $isPinned     = false;

        if ($pinnedGlobal != '')
        {
            $pinnedQueries = explode(' ', $pinnedGlobal);

            foreach ($pinnedQueries as $queryId)
            {
                if ($queryId == $qryId)
                {
                    $isPinned = true;
                    break;
                }
            }
        }

        echo $isPinned;
    }
}