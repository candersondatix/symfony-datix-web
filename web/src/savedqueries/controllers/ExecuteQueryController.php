<?php
namespace src\savedqueries\controllers;

use src\framework\controller\Controller;

class ExecuteQueryController extends Controller
{
    function executequery()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        if ($this->request->getParameter('rbWhat') == 'cancel')
        {
            $this->redirect('app.php?action=home&module='.\Sanitize::getModule($this->request->getParameter('module')));
        }

        if (!$this->request->getParameter('qry_recordid') && !$this->request->getParameter('qry_name'))
        {
            AddValidationMessage('qry_recordid', 'Please select a query');
            $this->redirect('app.php?action=savedqueries&module='.$this->request->getParameter('module'));
        }
        elseif ($this->request->getParameter('rbWhat') == 'edit')
        {
            $this->call('src\\savedqueries\\controllers\\ShowSavedQueriesTemplateController', 'showSavedQueryForm', []);
            obExit();
        }
        else
        {
            $query_recordid = \Sanitize::SanitizeInt($this->request->getParameter('qry_recordid'));

            if (!$query_recordid)
            {
                $query_recordid = \Sanitize::SanitizeString($this->request->getParameter('qry_name'));
            }

            if ($query_recordid)
            {
                $sql = '
                    SELECT
                        sq_where, sq_tables, sq_type, sq_orderby
			        FROM
			            queries
                    WHERE
                        recordid = :recordid
                ';

                $SavedQueryWhere = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $query_recordid));

                if ($SavedQueryWhere['sq_type'] == 'WH')
                {
                    $table = 'holding';
                }
                else
                {
                    $table = 'main';
                }

                $_SESSION[$this->request->getParameter('module')]['TABLES'] = ($SavedQueryWhere['sq_tables'] ? $SavedQueryWhere['sq_tables'] : $ModuleDefs[$this->request->getParameter('module')]['TABLE']);
                $_SESSION[$this->request->getParameter('module')]['WHERE'] = $SavedQueryWhere['sq_where'];
                ClearAtPromptSession($this->request->getParameter('module'));

                if ($ModuleDefs[$this->request->getParameter('module')]['LISTING_FILTERS'])
                {
                    // set the filter format
                    $_SESSION[$this->request->getParameter('module')]['FILTER'] = unserialize($SavedQueryWhere['sq_orderby']);
                }

                if ($this->request->getParameter('module') == 'CON')
                {
                    $RedirectLocation = '?action=listcontacts&module=CON&fromsearch=1&query='.$query_recordid;
                }
                else
                {
                    $RedirectLocation = '?action=list&module='.$this->request->getParameter('module').'&table='.$table.'&fromsearch=1&listtype=search&query='.$query_recordid;
                }

                if ($this->request->getParameter('fromhomescreen') == '1')
                {
                    $RedirectLocation.= '&fromhomescreen=1';
                }

                $_SESSION[$this->request->getParameter('module')]['LASTQUERYID'] = $query_recordid;
                $_SESSION[$this->request->getParameter('module')]['SEARCHLISTURL'] = $RedirectLocation;

                $this->redirect('app.php'.$RedirectLocation);
            }
        }
    }
}