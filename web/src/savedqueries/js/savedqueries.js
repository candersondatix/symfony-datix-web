function confirmDelete()
{
    var form = document.forms[0];

    form.rbWhat.value = 'delete';

    jQuery.ajax({
        type: "POST",
        url: "app.php?action=isQueryPinned",
        data: {
            qryId: jQuery("input[name=qry_recordid]").val(),
            module: jQuery("input[name=module]").val()
        }
    }).done(function(response) {
        if (response == "1") {
            if (confirm('This saved query is also a pinned query. Are you sure you want to delete it?')) {
                form.submit();
            }
        } else {
            if (confirm('Delete?')) {
                form.submit();
            }
        }
    });
}