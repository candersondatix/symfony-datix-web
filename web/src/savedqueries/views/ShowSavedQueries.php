<?php if ($this->hasPermissions) : ?>
<form method="post" name="frmSavedQueries" action="<?php echo $this->scripturl; ?>?action=executequery&amp;module=<?php echo Sanitize::getModule($this->module); ?>">
    <input type="hidden" name="module" value="<?php echo Escape::EscapeEntities($this->module); ?>" />
    <input type="hidden" name="qry_id" value="<?php echo is_numeric($this->qry_id ? (int) $this->qry_id : '');  ?>" />
    <input type="hidden" name="qry_name" value="<?php echo Escape::EscapeEntities($this->qry_name); ?>" />
    <input type="hidden" name="qry_recordid" value="<?php echo is_numeric($this->qry_recordid ? (int) $this->qry_recordid : ''); ?>" />
    <div class="new_windowbg">
        <?php SavedQueriesSection($this->qry, $this->FormAction, $this->module); ?>
    </div>
    <div class="button_wrapper">
        <input type="hidden" name="rbWhat" value="Save" />
        <input type="submit" value="<?php echo _tk('run_query'); ?>" onClick="document.forms[0].rbWhat.value='go';" />
        <input type="submit" value="<?php echo _tk('edit_query'); ?>" onClick="document.forms[0].rbWhat.value='edit';" />
        <input type="submit" value="<?php echo _tk('btn_cancel'); ?>" onClick="document.forms[0].rbWhat.value='cancel';" />
    </div>
</form>
<?php endif; ?>