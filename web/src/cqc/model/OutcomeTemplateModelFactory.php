<?php
namespace src\cqc\model;

use src\framework\model\ModelFactory;

class OutcomeTemplateModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new OutcomeTemplateFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new OutcomeTemplateMapper(new \DatixDBQuery(), $this);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new OutcomeTemplateCollection($this->getMapper(), $this->getEntityFactory());
    }
}