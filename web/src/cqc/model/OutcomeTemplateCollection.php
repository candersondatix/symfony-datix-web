<?php
namespace src\cqc\model;

use src\framework\model\EntityCollection;

class OutcomeTemplateCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\cqc\\model\\OutcomeTemplate';
    }
}