<?php
namespace src\cqc\model;

use src\framework\model\Mapper;
use src\framework\model\ModelFactory;
use src\framework\query\Query;

class OutcomeTemplateMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\cqc\\model\\OutcomeTemplate';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'cqc_template_outcomes';
    }
}