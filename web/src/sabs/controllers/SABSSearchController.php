<?php

namespace src\sabs\controllers;

use src\framework\controller\Controller;

class SABSSearchController extends Controller
{
    /**
     * Shows Safety Alert search form.
     */
    function sabssearch()
    {
        $_SESSION['SAB']['PROMPT']['VALUES'] = array();

        if ($this->request->getParameter('searchtype') == 'lastsearch')
        {
            $sab = $_SESSION['SAB']['LAST_SEARCH'];
        }
        else
        {
            $sab = array();
        }

        $Parameters = array(
            'FormType' => 'Search',
            'PrePopulatedData' => $sab
        );

        $this->call('src\sabs\controllers\NewSABSTemplateController', 'addnewsabs', $Parameters);
    }
}