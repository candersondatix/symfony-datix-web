<?php

namespace src\sabs\controllers;

use src\framework\controller\Controller;

class SABSContactLinkActionController extends Controller
{
    function sabscontactlinkaction()
    {
        $form_action = $this->request->getParameter('form_action');

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $this->request->getParameter('con_id'))
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'CONTACTS_MAIN', 'link_id' => $this->request->getParameter('con_id')));
        }

        switch ($form_action)
        {
            case 'link':
                $this->call('src\contacts\controllers\ContactsDoSelectionController', 'searchcontact', array());
                return;
                break;
            case 'reject':
                require_once 'Source/contacts/SABSContactForm.php';
                ContactReject();
                break;
            case 'new':
                require_once 'Source/contacts/SABSContactForm.php';
                SaveContactLinkToMain();
                break;
            case 'batch':
                require_once 'Source/contacts/SABSContactForm.php';
                ContactBatchLink();
                break;
            case 'distrib':
                require_once 'Source/contacts/SABSContactForm.php';
                ContactDistributionLink();
                break;
            case 'unlink':
                require_once 'Source/contacts/SABSContactForm.php';
                UnlinkContact();
                break;
            case 'cancel':
                $Parameters = array(
                    'recordid' => $this->request->getParameter('sab_id')
                );

                if ($this->request->getParameter('link_type'))
                {
                    $Parameters['panel'] = 'contacts_type_' . $this->request->getParameter('link_type');
                }

                $this->call('src\sabs\controllers\ShowMainSABSController', 'sabs', $Parameters);
                return;
                break;
            case 'email':
                require_once 'Source/contacts/SABSContactForm.php';
                SaveContactLinkToMain();

                require_once 'Source/sabs/SABSEmail.php';

                $sql = '
                    SELECT
                        sab_reference, sab_title, sab_source, sab_issued_date,
                        recordid, sab_descr
                    FROM
                        sabs_main
                    WHERE
                        recordid = :sab_id
                ';

                $sab = \DatixDBQuery::PDO_fetch($sql, array('sab_id' => $this->request->getParameter('sab_id')));
                $EmailsSent = EmailSABSAlert($sab, \Sanitize::SanitizeInt($this->request->getParameter('con_id')));

                ShowEmailedSABS($sab, '', $EmailsSent);
                break;
        }
    }
}