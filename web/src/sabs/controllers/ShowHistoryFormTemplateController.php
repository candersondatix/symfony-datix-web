<?php

namespace src\sabs\controllers;

use src\framework\controller\TemplateController;

class ShowHistoryFormTemplateController extends TemplateController
{
    function sabshistory()
    {
        if ($this->request->getParameter('his_id'))
        {
            // Open existing document recordid
            $sql = '
                SELECT
                    SAB_HISTORY.recordid AS his_id,
                    con_title, con_forenames, con_surname, his_email, his_datetime, his_type,
                    his_comments, his_subject, his_body
                FROM
                    SAB_HISTORY,
                    CONTACTS_MAIN
                WHERE
                    CONTACTS_MAIN.RECORDID = SAB_HISTORY.CON_ID
                    AND
                    SAB_HISTORY.recordid = :his_id
            ';

            $his = \DatixDBQuery::PDO_fetch($sql, array('his_id' => $this->request->getParameter('his_id')));
        }

        if ($this->request->getParameter('sab_id'))
        {
            $sqlsab = '
                SELECT
                    sab_reference
                FROM
                    sabs_main
                WHERE
                    recordid = :sab_id
            ';

            $sab =  \DatixDBQuery::PDO_fetch($sqlsab, array('sab_id' => $this->request->getParameter('sab_id')));
        }

        $his['error'] = $error;

        $this->title = 'E-mail details for Safety Alert';

        if ($sab['sab_reference'])
        {
            $this->title .= ' - Reference ' . $sab['sab_reference'];
        }

        $this->module = 'SAB';

        $HistoryDetailsSection = $this->HistoryDetailsSection($his);

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array('label' => 'Back', 'onclick' => 'this.form.submit()'));

        $this->response->build('src/sabs/views/ShowHistoryForm.php', array(
            'sab_id' => $this->request->getParameter('sab_id'),
            'his_id' => $this->request->getParameter('his_id'),
            'his' => $his,
            'HistoryDetailsSection' => $HistoryDetailsSection,
            'ButtonGroup' => $ButtonGroup
        ));
    }

    function HistoryDetailsSection($his)
    {

        $CTable = new \FormTable();
        $FieldObj = new \FormField();
        $FieldObj->SetFieldMode('Print');
        $CTable->MakeTitleRow('<b>History details</b>');

        if ($error)
        {
            $CTable->Contents .= '<font color="red"><b>' . $error['message'] . '</b></font><br /><br />';
        }

        $CTable->MakeRow($error['his_type'] . '<b>Type</b>', $FieldObj->MakeInputField('his_type', 50, 50, $his['his_type'], ''));
        $CTable->MakeRow($error['his_email'] . '<b>Email address:</b>', $FieldObj->MakeInputField('his_email', 50, 50, $his['his_email'], ''));
        $CTable->MakeRow($error['his_datetime'] . '<b>Date/time:</b>', $FieldObj->MakeInputField('his_datetime', 50, 50, $his['his_datetime']));
        $CTable->MakeRow($error['his_subject'] . '<b>Subject:</b>', $FieldObj->MakeInputField('his_subject', 50, 50, $his['his_subject'], ''));
        $CTable->MakeRow($error['his_body'] . '<b>Body:</b>', $FieldObj->MakeTextAreaField('his_body', 3, 70, 254, $his['his_body']));

        $CTable->MakeTable();

        return $CTable;
    }
}