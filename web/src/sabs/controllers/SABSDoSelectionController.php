<?php

namespace src\sabs\controllers;

use src\framework\controller\Controller;
use src\reports\exceptions\ReportException;

class SABSDoSelectionController extends Controller
{
    /**
     * Performs a search in Safety Alerts.
     */
    function sabsdoselection()
    {
        global $scripturl;

        ClearSearchSession('SAB');

        $FormAction = $this->request->getParameter('rbWhat');

        if ($FormAction == 'Cancel')
        {
            if (isset($_SESSION['security_group']['grp_id']))
            {
                $_SESSION['security_group']['success'] = false;

                $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                    'recordid' => $_SESSION['security_group']['grp_id'],
                    'module' => $_SESSION['security_group']['module']
                ));
                return;
            }
            elseif (isset($_SESSION['packaged_report']))
            {
                $RedirectLocation = '?action=designapackagedreport';

                if ($_SESSION['packaged_report']['recordid'])
                {
                    $RedirectLocation .= '&recordid='.$_SESSION['packaged_report']['recordid'];
                }
                else
                {
                    $RedirectLocation .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
                }

                $_SESSION['packaged_report']['success'] = false;
                $this->redirect('app.php' . $RedirectLocation);
            }
            elseif ($this->request->getParameter('qbe_recordid') != '')
            {
                // query by example for generic modules
                $this->call('src\generic\controllers\ShowRecordController', 'record', array(
                    'module' => $this->request->getParameter('qbe_return_module'),
                    'recordid' => $this->request->getParameter('qbe_recordid')
                ));
                return;
            }
            else
            {
                $this->redirect('app.php?module=SAB');
            }
        }

        // construct and cache a new Where object
        try
        {
            $whereFactory = new \src\framework\query\WhereFactory();
            $where = $whereFactory->createFromRequest($this->request);

            if (empty($where->getCriteria()['parameters'])) {
                $where = null;
            }

            $_SESSION['SAB']['NEW_WHERE'] = $where;
        }
        catch (\InvalidDataException $e)
        {
            $Error = true;
            $this->registry->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');
        }
        catch(ReportException $e)
        {
            $Error = true;
        }

        // Find all values which start with inc_ and add them to array
        // if they are not empty.
        // To do: parse names.
        $sab = QuotePostArray($this->request->getParameters());
        $_SESSION['SAB']['LAST_SEARCH'] = $this->request->getParameters();

        $table = 'sabs_main';

        while (list($name, $value) = each($sab))
        {
            $explodeArray = explode('_', $name);

            if ($explodeArray[0] == 'searchtaggroup')
            {
                // @prompt on tags is not yet supported
                if (preg_match('/@prompt/iu',$value) == 1)
                {
                    $search_where[] = '1=2';
                }
                elseif ($value != '')
                {
                    $field = str_replace('searchtaggroup_'.$explodeArray[1].'_', '', $name);

                    // Need to find the codes associated with the selected tags.
                    $sql = '
                        SELECT DISTINCT
                            code
                        FROM
                            code_tag_links
                        WHERE
                            [group] = :group
                            AND
                            [table] = :table
                            AND
                            field = :field
                            AND
                            tag IN (\''.implode('\',\'', array_map('intval', explode('|', $value))).'\')
                    ';

                    $Codes = \DatixDBQuery::PDO_fetch_all($sql, array(
                        'group' => intval($explodeArray[1]),
                        'field' => $field,
                        'table' => 'contacts_main'
                    ), \PDO::FETCH_COLUMN);

                    if (!empty($Codes))
                    {
                        $search_where[] = $field.' IN (\''.implode('\',\'', $Codes).'\')';
                    }
                    else
                    {
                        // This tag hasn't been attached to any codes, so can't return any results.
                        $search_where[] = '1=2';
                    }
                }
            }
            else
            {
                $FieldWhere = CheckMakeFieldWhere($name, $value, 'SAB', $table);

                if ($FieldWhere === false)
                {
                    $Error = true;
                    break;
                }
            }

            if ($FieldWhere != '')
            {
                if (CheckForCorrectAtCodes($FieldWhere))
                {
                    $sab_search[$name] = $FieldWhere;
                }
                else
                {
                    AddMangledSearchError($name, $value);
                    $Error = true;
                }
            }
        }

        if (!$Error && !ValidateWhereClause(implode(' AND ', $sab_search), 'SAB'))
        {
            $Error = true;
        }

        if ($Error == true)
        {
            AddSessionMessage('ERROR', _tk('search_errors'));
            $this->redirect('app.php?action=sabssearch&searchtype=lastsearch');
        }

        $Where = '';

        if (empty($sab_search) === false)
        {
            $where = implode(' AND ', $sab_search);
        }

        $_SESSION['SAB']['WHERE'] = $where;

        // We are doing a new search so unset LASTQUERYID
        unset($_SESSION['SAB']['LASTQUERYID']);

        if (isset($_SESSION['security_group']['grp_id']))
        {
            $_SESSION['security_group']['success'] = true;

            $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                'recordid' => $_SESSION['security_group']['grp_id'],
                'module' => $_SESSION['security_group']['module']
            ));
            return;
        }
        elseif (isset($_SESSION['packaged_report']))
        {
            $RedirectLocation = '?action=designapackagedreport';

            if ($_SESSION['packaged_report']['recordid'])
            {
                $RedirectLocation .= '&recordid='.$_SESSION['packaged_report']['recordid'];
            }
            else
            {
                $RedirectLocation .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
            }

            $_SESSION['packaged_report']['success'] = true;
            $this->redirect('app.php'.$RedirectLocation);
        }
        elseif ($this->request->getParameter('qbe_recordid') != '')
        {
            // query by example for generic modules
            $this->call('src\generic\controllers\ShowRecordController', 'record', array(
                'module' => $this->request->getParameter('qbe_return_module'),
                'recordid' => $this->request->getParameter('qbe_recordid'),
                'qbe_success' => true
            ));
            return;
        }
        else
        {
            $RedirectLocation = $scripturl . '?action=list&module=SAB&table=main&listtype=search';
            $_SESSION['SAB']['SEARCHLISTURL'] = $RedirectLocation;
            $this->redirect($RedirectLocation);
        }
    }
}