<?php

namespace src\sabs\controllers;

use src\framework\controller\Controller;

class HistoryActionController extends Controller
{
    function sabshistoryaction()
    {
        if ($this->request->getParameter('rbWhat') == 'back')
        {
            $this->call('src\sabs\controllers\ShowMainSABSController', 'sabs', array(
                'recordid' => $this->request->getParameter('sab_id'),
                'panel' => 'sab_history'
            ));
        }
    }
}