<?php

namespace src\progressnotes\controllers;

use src\framework\controller\Controller;

class ProgressNotesController extends Controller
{
    /**
     * Creates and displays the Progress notes form section.
     */
    function showProgressNotes()
    {
        $recordId = $this->request->getParameter('recordid');
        $module   = $this->request->getParameter('module');
        $formType = $this->request->getParameter('formType');

        $progNotesField     = new \FormField($formType);
        $progNotesFieldEdit = new \FormField($formType);

        $notes = $this->getProgressNotes($recordId, $module);

        if ($this->request->getParameter('full_audit'))
        {
            $audit = $this->getProgressNotesAudit($recordId, $module);
        }

        $editPerms = GetParm('PROG_NOTES_EDIT', 'OWN');

        if ($formType != 'ReadOnly' && $formType != 'Print' && $formType != 'Locked')
        {
            $progNotesField->MakeTextAreaField('prog_notes', 10, 70, false, '');
        }

        $this->response->build('src/progressnotes/views/DisplayProgressNotes.php', array(
            'formType'           => $formType,
            'progNotesField'     => $progNotesField,
            'notes'              => $notes,
            'progNotesFieldEdit' => $progNotesFieldEdit,
            'editPerms'          => $editPerms,
            'fullAudit'          => \Sanitize::SanitizeInt($this->request->getParameter('full_audit')),
            'audit'              => $audit
        ));
    }

    /**
     * Retrieves progress notes for a given record.
     *
     * @param string $recordid The record's ID.
     * @param string $module   The record's module.
     *
     * @return array           The record's progress notes.
     */
    public function getProgressNotes($recordid, $module)
    {
        $sql = '
            SELECT
                recordid, pno_progress_notes, pno_createdby, pno_createddate, pno_updatedby, pno_updateddate
            FROM
                progress_notes
            WHERE
                pno_link_module = :module AND
                pno_link_id = :recordid
            ORDER BY
                pno_createddate DESC
        ';

        return \DatixDBQuery::PDO_fetch_all($sql, array('module' => $module, 'recordid' => $recordid));
    }

    /**
     * Retrieves progress notes audit entries for a given record.
     *
     * @param string $recordid The record's ID.
     * @param string $module   The record's module.
     *
     * @return array           The record's progress notes.
     */
    public function getProgressNotesAudit($recordid, $module)
    {
        $sql = '
            SELECT
                aud_login, aud_date, aud_action, aud_detail
            FROM
                full_audit
            WHERE
                aud_module = :module AND
                aud_record = :recordid AND
                aud_action LIKE \'WEB:prog_notes%\'
            ORDER BY
                aud_action, aud_date
        ';

        return \DatixDBQuery::PDO_fetch_all($sql, array('module' => $module, 'recordid' => $recordid));
    }

    /**
     * Saves the form's progress notes.
     */
    public function saveProgressNotes()
    {
        $module = $this->request->getParameter('module');
        $data   = $this->request->getParameter('data');

        // determine record's unique ID
        switch ($module)
        {
            case 'AST':
                $id = 'ast_id';
                break;
            default:
                $id = 'recordid';
        }

        foreach ($data as $field => $value)
        {
            if (\UnicodeString::strpos($field, 'prog_notes') !== false)
            {
                if ($data['CHANGED-'.$field])
                {
                    // update an existing progress note
                    $progParts = explode('_', $field);
                    $progId = $progParts[2];

                    // get current progress note info for security checks/auditing
                    $currentNote = \DatixDBQuery::PDO_fetch('SELECT pno_progress_notes, pno_createdby FROM progress_notes WHERE recordid = :recordid', array('recordid' => $progId));
                    $editPerms = GetParm('PROG_NOTES_EDIT', 'OWN');

                    if ($editPerms == 'ALL' || ($editPerms == 'OWN' && $currentNote['pno_createdby'] == $_SESSION['initials']))
                    {
                        \DatixDBQuery::PDO_query('
                            INSERT INTO
                                full_audit (aud_module, aud_record, aud_login, aud_date, aud_action, aud_detail)
                            VALUES
                                (:module, :recordid, :initials, GETDATE(), :action, :detail)
                        ', array(
                            'module'   => $module,
                            'recordid' => $data[$id],
                            'initials' => $_SESSION['initials'],
                            'action'   => 'WEB:'.$field,
                            'detail'   => $currentNote['pno_progress_notes']
                        ));

                        \DatixDBQuery::PDO_query('
                            UPDATE
                                progress_notes
                            SET
                                pno_progress_notes = :notes, pno_updatedby = :updatedby, pno_updateddate = GETDATE()
                            WHERE
                                recordid = :recordid
                        ', array(
                            'notes'     => $value,
                            'updatedby' => $_SESSION['initials'],
                            'recordid'  => $progId
                        ));
                    }
                }
                elseif ($field == 'prog_notes' && $value != '')
                {
                    // add a new progress note
                    \DatixDBQuery::PDO_query('
                        INSERT INTO
                            progress_notes (pno_link_id, pno_link_module, pno_progress_notes, pno_createdby, pno_createddate, pno_updatedby, pno_updateddate)
                        VALUES
                            (:recordid, :module, :notes, :createdby, GETDATE(), NULL, GETDATE())
                    ', array(
                        'recordid'  => $data[$id],
                        'module'    => $module,
                        'notes'     => $value,
                        'createdby' => $_SESSION['initials']
                    ));
                }
            }
        }
    }
}