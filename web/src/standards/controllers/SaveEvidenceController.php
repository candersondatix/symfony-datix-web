<?php

namespace src\standards\controllers;

use src\documents\controllers\DocumentController;
use src\framework\controller\Controller;

class SaveEvidenceController extends Controller
{
    function saveevidence()
    {
        global $scripturl, $SectionVisibility;

        session_start();

        $ModuleDefs = $this->registry->getModuleDefs();

        $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'LIB', 'level' => 2));
        $FormDesign->LoadFormDesignIntoGlobals();

        // Prevent issues with auditing record by defining a FIELD_ARRAY for this 'module' here
        $ModuleDefs['LIB']['FIELD_ARRAY'] = array(
            'lib_name', 'lib_ourref', 'lib_type', 'lib_subtype',
            'lib_loctype', 'lib_locactual', 'lib_manager', 'lib_handler', 'lib_dopened',
            'lib_dclosed', 'lib_descr', 'lib_references', 'lib_web_link','lib_dreview',
            'lib_organisation','lib_clingroup','lib_directorate','lib_specialty','lib_unit'
        );

        $form_action = \Sanitize::SanitizeString($this->request->getParameter('rbWhat'));
        $lib = \Sanitize::SanitizeRawArray($this->request->getParameters());
        $lib = ParseSaveData(array('module' => 'LIB', 'data' => $lib));
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $recordid)
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'LIBRARY_MAIN', 'link_id' => $recordid));
        }

        switch ($form_action)
        {
            case 'Cancel':
                if ($this->request->getParameter('fromsearch'))
                {
                    $RedirectUrl = '?action=list&module=LIB&listtype=search';
                }
                elseif ($ModuleDefs[$lib['main_module']]['ACTION'])
                {
                    $RedirectUrl = '?action='.$ModuleDefs[$lib['main_module']]['ACTION'].'&recordid=' . urlencode($lib['main_recordid']);
                }
                else
                {
                    $RedirectUrl = '?module=LIB';
                }

                $this->redirect('app.php' . $RedirectUrl);
                break;
            case 'ShowAudit':
                $Parameters = array(
                    'recordid' => urlencode($this->request->getParameter('recordid')),
                    'full_audit' => 1
                );

                if ($lib['pro_id'])
                {
                    $Parameters['pro_id'] = urlencode($this->request->getParameter('pro_id'));
                }
                elseif ($lib['ele_id'])
                {
                    $Parameters['ele_id'] = urlencode($this->request->getParameter('ele_id'));
                }
                elseif ($lib['stn_id'])
                {
                    $Parameters['stn_id'] = urlencode($this->request->getParameter('stn_id'));
                }

                $this->call('src\standards\controllers\ShowEvidenceController', 'evidence', $Parameters);
                return;
                break;
            case 'Unlink':
                $sql = '
                    DELETE FROM
                        link_library
                    WHERE
                        link_recordid = :link_recordid
                ';

                \DatixDBQuery::PDO_query($sql, array('link_recordid' => $this->request->getParameter('link_recordid')));

                if ($ModuleDefs[$lib['main_module']]['ACTION'])
                {
                    $RedirectUrl = '?action='.$ModuleDefs[$lib['main_module']]['ACTION'].'&recordid=' . urlencode($lib['main_recordid']);
                }
                else
                {
                    $RedirectUrl = '?module=LIB';
                }

                $this->redirect('app.php' . $RedirectUrl);
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');
        }

        GetSectionVisibility('LIB', 2, $this->request->getParameters(), $this->request->getParameter('main_module'));

        $ValidationErrors = ValidatePostedDates('LIB');

        // Documents linked on new linked forms
        if ($this->request->getParameter('max_doc_suffix'))
        {
            $DocError = DocumentController::ValidateLinkedDocumentData($this->request->getParameters());
            $ValidationErrors = array_merge($ValidationErrors, $DocError);

            $this->request->setParameter('document_max_suffix', $this->request->getParameter('document_max_suffix') + 1);

            // We need to set the values on the $_POST array also because the documents function still use it
            // TODO: Remove this once documents save functions are refactored.
            $_POST['document_max_suffix'] = $this->request->getParameter('document_max_suffix');
        }

        if ($ValidationErrors)
        {
            $error['Validation'] = $ValidationErrors;
        }

        if (!empty($error))
        {
            $data = \Sanitize::SanitizeStringArray($this->request->getParameters());
            $data['error'] = $error;
            $this->call('src\standards\controllers\ShowEvidenceController', 'evidence', array());
            return;
        }

        if (!$recordid)
        {
            $recordid = GetNextRecordID('library_main', true);
            $newEvidence = true;
        }
        else
        {
            $newEvidence = false;
        }

        $lib_dopened = '\''.UserDateToSQLDate($lib['lib_dopened']).'\'';
        $lib_dclosed = '\''.UserDateToSQLDate($lib['lib_dclosed']).'\'';

        $FullAudit = true;

        if ($FullAudit)
        {
            DoFullAudit('LIB', 'library_main', $recordid);
        }

        if ($recordid > 0)
        {
            $sql = "
                UPDATE
                    library_main
                SET
                    updatedby = '" . $_SESSION['initials'] . "',
                    updateddate = '" . date('d-M-Y H:i:s') . "',
                    updateid = '" . GensUpdateID($lib['updateid']) . "', ";

            $sql .= GeneratePDOSQLFromArrays(
                array(
                    'FieldArray' => $ModuleDefs['LIB']['FIELD_ARRAY'],
                    'DataArray' => $lib,
                    'Module' => 'LIB',
                    'end_comma' => false
                ),
                $bindParams
            );

            $sql .= " WHERE recordid = $recordid";

            if (array_key_exists('updateid', $lib) && $lib['updateid'])
            {
                $sql .= " and updateid = '" . $lib['updateid'] . "'";
            }

            $query = new \DatixDBQuery($sql);
            $result = $query->prepareAndExecute($bindParams);

            if (!$result)
            {
                $error = "An error has occurred when trying to save.\nPlease report the following to the Datix administrator:\n$sql";
            }
            elseif ($query->getRowsAffected() == 0)
            {
                $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=evidence&recordid='.$recordid.'">here</a> to return to the record';
            }
        }

        if ($recordid)
        {
            require_once 'Source/libs/UDF.php';
            SaveUDFs($recordid, MOD_LIBRARY);
        }

        // Handle links for old standards module
        if ($this->request->getParameter('manageLinksTree'))
        {
            $lib['link_recordid'] = $this->saveTreeLinks($recordid, $this->request->getParameters());
        }

        // Handle links for non-old standards modules, or if not using the manage links tree
        if ($lib['main_module'] != '')
        {
            if ($lib['link_recordid'] == '')
            {
                // create new link
                $data = array(
                    'updateddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $_SESSION['initials'],
                    'link_type' => 'E',
                    \UnicodeString::strtolower($lib['main_module']).'_id' => $this->request->getParameter('main_recordid'),
                    'lib_id' => $recordid,
                    'link_notes' => $this->request->getParameter('link_notes')
                );

                $lib['link_recordid'] = \DatixDBQuery::PDO_build_and_insert('link_library', $data);
            }
            else
            {
                // Update existing link details
                $sql = '
                    UPDATE
                        link_library
                    SET
                        updateddate = :updateddate,
                        updatedby = :updatedby,
                        link_notes = :link_notes
                    WHERE
                        link_recordid = :link_recordid
                ';

                \DatixDBQuery::PDO_query($sql, array(
                    'updateddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $_SESSION['initials'],
                    'link_notes' => $this->request->getParameter('link_notes'),
                    'link_recordid' => $lib['link_recordid']
                ));
            }
        }

        $lib['recordid'] = $recordid;

        $_SESSION['lib_number'] = $lib['recordid'];

        // Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => 'LIB',
            'data'   => $lib
        ));

        if ($SectionVisibility['extra_document'])
        {
            // Save documents attached on new linked records.
            DocumentController::SaveDocsFromLevel1($recordid, 'LIB');
        }

        $_SESSION['STANDARDS']['lib'] = $lib;

        AddSessionMessage('INFO',_tk('STN_evidence_save_message'));

        $this->redirect('app.php?action=evidence&recordid='.$recordid.
            '&link_recordid='.$this->request->getParameter('link_recordid').
            '&main_recordid='.$this->request->getParameter('main_recordid').
            '&module='.$this->request->getParameter('module'));
    }

    /**
     * Handles the saving of library link info when using the manage links tree (old standards).
     *
     * @param int $recordid The id of the library record we're creating links for.
     * @param array $Parameters Variables on $_POST
     *
     * @return string $thisLink The link id for the main record (i.e. if navigating to the library form from another module).
     */
    function saveTreeLinks($recordid, $Parameters)
    {
        $standardsLinks = array();
        $elementLinks = array();
        $complianceLinks = array();
        $doNotDeleteIds = array();

        // New links will be saved below. For a new evidence record, the only link
        // will be to the main record that spawned it - we need to add this to make sure it is saved.
        switch ($Parameters['main_module'])
        {
            case 'STN':
                $standardsLinks[] = $Parameters['main_recordid'];
                $Parameters['standard_notes_'.$Parameters['main_recordid']] = $Parameters['link_notes'];
                break;
            case 'ELE':
                $elementLinks[] = $Parameters['main_recordid'];
                $Parameters['element_notes_'.$Parameters['main_recordid']] = $Parameters['link_notes'];
                break;
            case 'PRO':
                $complianceLinks[] = $Parameters['main_recordid'];
                $Parameters['compliance_notes_'.$Parameters['main_recordid']] = $Parameters['link_notes'];
                break;
        }

        // Build link arrays
        foreach ($Parameters as $field => $value)
        {
            if (\UnicodeString::strpos($field, 'standard_link_') !== false && $value == 'Y')
            {
                $fieldParts = explode('_', $field);
                $standardsLinks[] = $fieldParts[2];
            }
            elseif (\UnicodeString::strpos($field, 'element_link_') !== false && $value == 'Y')
            {
                $fieldParts = explode('_', $field);
                $elementLinks[] = $fieldParts[2];
            }
            elseif (\UnicodeString::strpos($field, 'compliance_link_') !== false && $value == 'Y')
            {
                $fieldParts = explode('_', $field);
                $complianceLinks[] = $fieldParts[2];
            }
        }

        $standardsLinks = array_unique($standardsLinks);
        $elementLinks = array_unique($elementLinks);
        $complianceLinks = array_unique($complianceLinks);

        // Insert links
        if (!empty($complianceLinks))
        {
            foreach ($complianceLinks as $pro_id)
            {
                // Retrieve corresponding element and standard IDs
                $sql = '
                    SELECT
                        sm.recordid as stn_id,
                        se.recordid as ele_id
                    FROM
                        standards_main sm
                    INNER JOIN
                        standard_elements se ON se.ele_stn_id = sm.recordid
                    INNER JOIN
                        standard_prompts sp ON sp.pro_ele_id = se.recordid
                    WHERE
                        sp.recordid = :recordid
                ';
                $row = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $pro_id));

                // Build array of data to insert into link table
                $data = array(
                    'updateddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $_SESSION['initials'],
                    'link_type' => 'E',
                    'stn_id' => $row['stn_id'],
                    'ele_id' => $row['ele_id'],
                    'pro_id' => $pro_id,
                    'lib_id' => $recordid,
                    'link_notes' => $Parameters['compliance_notes_'.$pro_id]
                );

                $link_recordid = \DatixDBQuery::PDO_build_and_insert('link_library', $data);

                if ($pro_id == $Parameters['main_recordid'] && $Parameters['main_module'] == 'PRO')
                {
                    $thisLink = $link_recordid;
                }

                // Insert record and retrieve new id
                $doNotDeleteIds[] = $link_recordid;
            }
        }

        if (!empty($elementLinks))
        {
            foreach ($elementLinks as $ele_id)
            {
                // Retrieve corresponding standard ID
                $sql = '
                    SELECT
                        sm.recordid as stn_id
                    FROM
                        standards_main sm
                    INNER JOIN
                        standard_elements se ON se.ele_stn_id = sm.recordid
                    WHERE
                        se.recordid = :recordid
                ';
                $stn_id = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $ele_id), \PDO::FETCH_COLUMN);

                // Build array of data to insert into link table
                $data = array(
                    'updateddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $_SESSION['initials'],
                    'link_type' => 'E',
                    'stn_id' => $stn_id,
                    'ele_id' => $ele_id,
                    'lib_id' => $recordid,
                    'link_notes' => $Parameters['element_notes_'.$ele_id]
                );

                // insert record and retrieve new id
                $link_recordid = \DatixDBQuery::PDO_build_and_insert('link_library', $data);

                if ($ele_id == $Parameters['main_recordid'] && $Parameters['main_module'] == 'ELE')
                {
                    $thisLink = $link_recordid;
                }

                // Insert record and retrieve new id
                $doNotDeleteIds[] = $link_recordid;
            }
        }

        if (!empty($standardsLinks))
        {
            foreach ($standardsLinks as $stn_id)
            {
                // Build array of data to insert into link table
                $data = array(
                    'updateddate' => date('Y-m-d H:i:s'),
                    'updatedby' => $_SESSION['initials'],
                    'link_type' => 'E',
                    'stn_id' => $stn_id,
                    'lib_id' => $recordid,
                    'link_notes' => $Parameters['standard_notes_'.$stn_id]
                );

                // Insert record and retrieve new id
                $link_recordid = \DatixDBQuery::PDO_build_and_insert('link_library', $data);

                if ($stn_id == $Parameters['main_recordid'] && $Parameters['main_module'] == 'STN')
                {
                    $thisLink = $link_recordid;
                }

                // Insert record and retrieve new id
                $doNotDeleteIds[] = $link_recordid;
            }
        }

        // Remove previous standards links for this library record
        $sql = '
            DELETE FROM
                link_library
            WHERE
                link_type = \'E\'
                AND
                lib_id = :recordid'.(!empty($doNotDeleteIds) ? '
                AND
                link_recordid NOT IN (' . implode(',', $doNotDeleteIds) . ')' : '').'
                AND
                (stn_id IS NOT NULL OR ele_id IS NOT NULL OR pro_id IS NOT NULL)
        ';

        \DatixDBQuery::PDO_query($sql, array('recordid' => $recordid));

        return $thisLink;
    }
}