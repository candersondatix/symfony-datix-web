<?php

namespace src\standards\controllers;

use src\framework\controller\TemplateController;

class ShowSavePromptTemplateController extends TemplateController
{
    function showsaveprompt()
    {
        $pro = ($_SESSION['STANDARDS']['pro'] ? $_SESSION['STANDARDS']['pro'] : '');

        $pro_number = $pro['recordid'];

        if ($pro_number == '')
        {
            $this->redirect('app.php');
        }

        $this->title = 'Standards';
        $this->module = 'STN';
        $this->hasPadding = false;

        $this->response->build('src/standards/views/ShowSavePrompt.php', array(
            'pro_number' => $pro_number,
            'pro_ele_id' => $pro['pro_ele_id']
        ));
    }
}