<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class SavePromptController extends Controller
{
    function saveprompt()
    {
        global $scripturl;

        session_start();

        $form_action = \Escape::EscapeEntities($this->request->getParameter('rbWhat'));
        $pro = \Sanitize::SanitizeRawArray($this->request->getParameters());
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        if (!$pro['pro_max_score'])
        {
            $pro['pro_max_score'] = 100;
        }

        if ($pro['pro_status'] == 'N/A')
        {
            $pro['pro_act_score'] = '';
        }

        if ($pro['pro_status'] == 'NO')
        {
            $pro['pro_act_score'] = 0;
        }

        if ($pro['pro_status'] == 'YES')
        {
            $pro['pro_act_score'] = $pro['pro_max_score'];
        }

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $recordid)
        {
            $sSQL = '
                SELECT
                    ele_stn_id
                FROM
                    standard_prompts,
                    standard_elements
                WHERE
                    standard_prompts.recordid = :recordid
                    AND
                    standard_prompts.pro_ele_id = standard_elements.recordid
            ';

            $row = \DatixDBQuery::PDO_fetch($sSQL, array('recordid' => $recordid));

            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'STANDARDS_MAIN', 'link_id' => $row['ele_stn_id']));
        }

        switch ($form_action)
        {
            case 'Cancel':
                $this->call('src\standards\controllers\ShowElementController', 'element', array(
                    'recordid' => $pro['pro_ele_id']
                ));
                return;
                break;
            case "ShowAudit":
                $this->call('src\standards\controllers\ShowPromptController', 'prompt', array(
                    'recordid' => $_POST['recordid'],
                    'full_audit' => 1
                ));
                return;
                break;
        }

        if (!$recordid)
        {
            $recordid = GetNextRecordID('standard_prompts', true);
        }

        $FullAudit = true;

        if ($FullAudit)
        {
            DoFullAudit('PRO', 'standard_prompts', $recordid);
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'PRO',
            'link_type' => 'compliance',
            'level' => 2
        ));
        $FormDesign->LoadFormDesignIntoGlobals();

        $pro = ParseSaveData(array('module' => 'PRO', 'data' => $pro));

        if ($recordid > 0)
        {
            $sql = "
                UPDATE
                    standard_prompts
                SET
                    updatedby = '" . $_SESSION['initials'] . "',
                    updateddate = '" . date('d-M-Y H:i:s') . "',
                    updateid = '" . GensUpdateID($pro['updateid']) . "',
                    pro_ele_id = " . $pro['pro_ele_id'];

            if (array_key_exists('pro_act_score', $pro))
            {
                if (isset($pro['pro_act_score']))
                {
                    $sql .= ', pro_act_score = ' . round($pro['pro_act_score'], 0);
                }
                else
                {
                    $sql .= ', pro_act_score = NULL';
                }
            }

            $sql_fields = array(
                'pro_comments', 'pro_code', 'pro_descr', 'pro_manager',
                'pro_handler', 'pro_status', 'pro_level', 'pro_max_score', 'pro_listorder'
            );

            $sql .= ', '.GeneratePDOSQLFromArrays(
                array(
                    'FieldArray' => $sql_fields,
                    'DataArray' => $pro,
                    'Module' => 'PRO',
                    'end_comma' => false
                ),
                $bindParams
            );

            $sql .= " WHERE recordid = $recordid";

            if (array_key_exists('updateid', $pro) && $pro['updateid'])
            {
                $sql .= " and updateid = '" . $pro['updateid'] . "'";
            }

            $query = new \DatixDBQuery($sql);
            $result = $query->prepareAndExecute($bindParams);

            if (!$result)
            {
                $error = "An error has occurred when trying to save.  Please report the following to the Datix administrator: $sql";
            }
            elseif ($query->getRowsAffected() == 0)
            {
                $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=prompt&recordid='.$recordid.'">here</a> to return to the record';
            }

            // Update the parent element score and the parent standard score and status
            if (!$error)
            {
                // retrieve prompt scores
                $sql = "
                    SELECT
                        SUM(pro_act_score) as pro_act_score,
                        SUM(pro_max_score) as pro_max_score
                    FROM
                        standard_prompts
                    WHERE
                        pro_ele_id = :pro_ele_id
                        AND
                        (pro_status != 'N/A' OR pro_status is null)
                        AND
                        pro_max_score IS NOT NULL
                        AND
                        pro_max_score > 0
                        AND
                        pro_act_score IS NOT NULL
                ";

                $row = \DatixDBQuery::PDO_fetch($sql, array('pro_ele_id' => $pro['pro_ele_id']));

                if ($row)
                {
                    // Update parent element score
                    $sql = "
                        SELECT
                            updateid
                        FROM
                            standard_elements
                        WHERE
                            recordid = :recordid
                    ";
                    $ele = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $pro['pro_ele_id']));

                    if ($ele)
                    {
                        if ($row['pro_max_score'] == 0)
                        {
                            $ele_score = 0;
                        }
                        else
                        {
                            $ele_score = round($row['pro_act_score'] / $row['pro_max_score'] * 100);
                        }

                        $sql = "
                            UPDATE
                                standard_elements
                            SET
                                updatedby = '" . $_SESSION['initials'] . "',
                                updateddate = '" . date('d-M-Y H:i:s') . "',
                                updateid = '" . GensUpdateID($ele['updateid']) . "',
                                ele_score = $ele_score
                            WHERE
                                recordid = $pro[pro_ele_id]
                                AND
                                (updateid = '$ele[updateid]' OR updateid IS NULL)";

                        \DatixDBQuery::PDO_query($sql);
                    }
                }
            }
        }

        if ($error != '')
        {
            SaveError($error);
        }

        $pro['recordid'] = $recordid;

        $_SESSION['pro_number'] = $pro['recordid'];

        if ($recordid)
        {
            require_once 'Source/libs/UDF.php';
            SaveUDFs($recordid, MOD_PROMPTS);
        }

        // Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => 'PRO',
            'data'   => $pro
        ));

        $_SESSION['STANDARDS']['pro'] = $pro;
        $this->call('src\standards\controllers\ShowSavePromptTemplateController', 'showsaveprompt', array());
    }
}