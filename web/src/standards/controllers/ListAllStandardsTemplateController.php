<?php

namespace src\standards\controllers;

use src\framework\controller\TemplateController;

class ListAllStandardsTemplateController extends TemplateController
{
    /**
     * Lists all standards
     */
    function liststandards()
    {
        $ownonly = ($this->request->getParameter('ownonly') ? $this->request->getParameter('ownonly') : false);
        $pending = ($this->request->getParameter('pending') ? $this->request->getParameter('pending') : false);
        $Data = $this->request->getParameters();

        $Perms = GetParm('STN_PERMS');

        if ($ownonly)
        {
            if ($pending)
            {
                $url_action = 'listmypendingelements';
                $this->title = _tk('list_my_pending_elements');
            }
            else
            {
                $url_action = 'listmyelements';
                $this->title = _tk('list_my_elements');
            }
        }
        else
        {
            if ($pending)
            {
                $url_action = 'listpendingelements';
                $this->title = _tk('list_pending_elements');
            }
            else
            {
                $url_action = 'liststandards';
                $this->title = _tk('list_all_standards');
            }
        }

        if (is_array($Data))
        {
            $stn = \Sanitize::SanitizeRawArray($Data);
        }

        $url_expanded = $stn['url_expanded'];

        if ($stn['expand_action'] == 1)
        {
            if (!is_array($url_expanded) || !in_array($stn['url_expand'], $url_expanded))
            {
                $url_expanded[] = $stn['url_expand'];
            }
        }
        else
        {
            if (is_array($url_expanded))
            {
                $i = array_search($stn['url_expand'], $url_expanded);

                if ($i !== false)
                {
                    unset($url_expanded[$i]);
                }
            }
        }

        if (is_array($url_expanded))
        {
            foreach($url_expanded as $index => $expanded)
            {
                // Also retrieve standard IDs if any
                if (\UnicodeString::strpos($expanded, 'stn_id=') !== false)
                {
                    $pos = \UnicodeString::strpos($expanded, 'stn_id=');
                    $stn_ids[] = \UnicodeString::substr($expanded, $pos+\UnicodeString::strlen('stn_id='));
                }

                $url_expanded[$index] = $expanded;
            }
        }

        $sql = '
            SELECT
                standards_main.recordid as stn_id,
                standards_main.stn_descr,
                standards_main.stn_ourref,
                standards_main.stn_name,
                standards_main.stn_handler,
                standards_main.stn_domain,
                standards_main.stn_set,
                standards_main.stn_version,
                standards_main.stn_organisation,
                standards_main.stn_assessment,
                code_domain.cod_descr as domain_descr,
                code_set.cod_descr as set_descr
        ';

        if (is_array($stn_ids))
        {
            $sql.= '
                ,
                ele.recordid as ele_id,
                ele.ele_code,
                ele.ele_descr,
                ele.ele_score,
                ele.ele_listorder
            ';
        }

        $sql.= '
            FROM
                standards_main
            left join code_types code_domain on standards_main.stn_domain = code_domain.cod_code and code_domain.cod_type = \'STNDOM\'
            left join code_types code_set on standards_main.stn_set = code_set.cod_code and code_set.cod_type = \'STNSET\'
            left join code_types code_version on standards_main.stn_version = code_set.cod_code and code_set.cod_type = \'STNVER\'
            left join code_types code_organisation on standards_main.stn_organisation = code_set.cod_code and code_set.cod_type = \'ORG\'
            left join code_types code_assessment on standards_main.stn_assessment = code_set.cod_code and code_set.cod_type = \'STNASS\'
        ';

        if (is_array($stn_ids))
        {
            $sql .= " left join standard_elements ele on ele.ele_stn_id = standards_main.recordid and ele.ele_stn_id in (" . implode(',', $stn_ids) . ")";

            if ($pending)
            {
                $joinwhere[] = "(ele.ele_score is null)";
            }

            if (is_array($joinwhere))
            {
                $sql .= ' and ' . implode(' and ', $joinwhere);
            }
        }

        $where = array();

        if ($ownonly == 'true')
        {
            $where[] = "(standards_main.stn_handler = '$_SESSION[initials]' or standards_main.recordid in (select sub_ele.ele_stn_id from standard_elements sub_ele where sub_ele.ele_handler = '$_SESSION[initials]'))";
        }

        if ($pending == 'true')
        {
            $where[] = "(standards_main.recordid in (select sub_ele.ele_stn_id from standard_elements sub_ele where sub_ele.ele_score is null))";
        }

        $permWhere = MakeSecurityWhereClause("", "STN", $_SESSION["initials"]);

        if ($permWhere)
        {
            $where[] = $permWhere;
        }

        if (!empty($where))
        {
            $sql .= ' where ' . implode(' and ', $where);
        }

        $sql.= " order by
            code_set.cod_listorder, set_descr,
            code_version.cod_listorder, standards_main.stn_version,
            code_organisation.cod_listorder, standards_main.stn_organisation,
            code_assessment.cod_listorder, standards_main.stn_assessment,
            code_domain.cod_listorder, domain_descr,
            standards_main.stn_listorder, standards_main.stn_ourref";

        if (is_array($stn_ids))
        {
            $sql .= ", ele_listorder, ele.ele_code";
        }

        $resultset = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($resultset as $id => $row)
        {
            $sSQL = '
                SELECT
                    cod_web_colour
                FROM
                    code_stn_status,
                    standards_status
                WHERE
                    standards_status.stn_id = :stn_id
                    AND
                    code_stn_status.code = standards_status.stn_status
            ';

            $resultset[$id]['colour_row'] = \DatixDBQuery::PDO_fetch($sSQL, array('stn_id' => $row['stn_id']));
        }

        $this->module = 'STN';

        $this->response->build('src/standards/views/ListAllStandards.php', array(
            'resultset' => $resultset,
            'pending' => $pending,
            'ownonly' => $ownonly,
            'url_expanded' => $url_expanded,
            'url_action' => $url_action
        ));
    }
}