<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class ShowPromptController extends Controller
{
    function prompt()
    {
        global $scripturl, $pro, $FormArray;

        if (GetParm("STN_PERMS") == '')
    	{
    	    // no access to standards module
            $this->redirect('app.php');
    	}
        
        $pro_id = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        $sql =
           'SELECT
               standard_prompts.recordid, standard_prompts.updateid, standard_prompts.updatedby,
    			standard_prompts.pro_listorder,
    			standard_prompts.pro_code, standard_prompts.pro_descr,
    			standard_prompts.pro_manager, standard_prompts.pro_handler,
    			standard_prompts.pro_level, standard_prompts.pro_status,
    			standard_prompts.pro_act_score, standard_prompts.pro_max_score,
    			standard_prompts.pro_ele_id,
    			standard_elements.ele_stn_id,
    			standard_prompts.pro_comments,
    			standard_elements.ele_handler, standards_main.stn_handler
    		FROM
        		standard_prompts
        		INNER JOIN
        			standard_elements ON standard_prompts.pro_ele_id = standard_elements.recordid
        		INNER JOIN
        			standards_main ON standard_elements.ele_stn_id = standards_main.recordid
        	WHERE
                   '.MakeSecurityWhereClause("standard_prompts.recordid = :pro_id", "STN",  $_SESSION["initials"]);
    
        if ($pro_id)
        {
            $pro = \DatixDBQuery::PDO_fetch($sql, array('pro_id' => $pro_id));
           
            if (empty($pro))
    		{
    		    $msg = "You do not have the necessary permissions to view Prompt ID $pro_id.";
                if ($GLOBALS['dtxdebug'])
                {
                    $msg .= '<br><br>'.$sql;
                }
                fatal_error($msg, "Information");
    		}
        }
        else
        {
            $pro['pro_ele_id'] = \Sanitize::SanitizeInt($this->request->getParameter('ele_id'));
        }

        // Check if we need to show full audit trail
        if ($pro_id && $this->request->getParameter('full_audit'))
        {
            $sql = "
                SELECT
                    aud_login, aud_date, aud_action, aud_detail
                FROM
                    full_audit
                WHERE
                    aud_module = 'PRO'
                    AND
                    aud_record = :pro_id
                    AND
                    aud_action LIKE 'WEB%'
                ORDER BY
                    aud_date ASC
            ";

            $resultArray = \DatixDBQuery::PDO_fetch_all($sql, array('pro_id' => $pro_id));

            foreach ($resultArray as $row)
            {
                $Action = explode(':', $row['aud_action']);
                $FieldName = $Action[1];
                $FullAudit[$FieldName][] = $row;
            }

            if ($FullAudit)
            {
                $pro['full_audit'] = $FullAudit;
            }
        }

        require_once 'Source/standards/PromptForm.php';

        ShowPromptForm($pro, (($pro_id) ? 'edit' : 'New'));
    }
}