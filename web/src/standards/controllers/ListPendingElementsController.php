<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class ListPendingElementsController extends Controller
{
    /**
     * Lists pending elements in Standards module
     */
    function listpendingelements()
    {
        $Parameters = array(
            'ownonly' => false,
            'pending' => true
        );

        $this->call('src\standards\controllers\ListAllStandardsTemplateController', 'liststandards', $Parameters);
    }
}