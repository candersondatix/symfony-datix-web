<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

class ListMyElementsController extends Controller
{
    /**
     * Lists my elements in Standards module
     */
    function listmyelements()
    {
        $Parameters = array(
            'ownonly' => true
        );

        $this->call('src\standards\controllers\ListAllStandardsTemplateController', 'liststandards', $Parameters);
    }
}