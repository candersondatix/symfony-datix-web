<?php

namespace src\standards\controllers;

use src\framework\controller\Controller;

use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

class SaveElementController extends Controller
{
    function saveelement()
    {
        global $scripturl;

        session_start();

        $form_action = \Escape::EscapeEntities($this->request->getParameter('rbWhat'));
        $ele = \Sanitize::SanitizeRawArray($this->request->getParameters());
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $recordid)
        {
            $sSQL = '
                SELECT
                    ele_stn_id
                FROM
                    standard_elements
                WHERE
                    recordid = :recordid
            ';

            $row = \DatixDBQuery::PDO_fetch($sSQL, array('recordid' => $recordid));

            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'STANDARDS_MAIN', 'link_id' => $row['ele_stn_id']));
        }

        switch ($form_action)
        {
            case 'Cancel':
                $this->call('src\standards\controllers\ShowStandardController', 'standard', array(
                    'recordid' => $ele['ele_stn_id']
                ));
                return;
                break;
            case "ShowAudit":
                $this->call('src\standards\controllers\ShowElementController', 'element', array(
                    'recordid' => $this->request->getParameter('recordid'),
                    'full_audit' => 1
                ));
                return;
                break;
        }

        if (!$recordid)
        {
            $recordid = GetNextRecordID('standard_elements', true);
        }

        $FullAudit = true;

        if ($FullAudit)
        {
            DoFullAudit('ELE', 'standard_elements', $recordid);
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'ELE',
            'link_type' => 'elements',
            'level' => 2
        ));
        $FormDesign->LoadFormDesignIntoGlobals();

        $ele = ParseSaveData(array('module' => 'ELE', 'data' => $ele));

        // Send notifications for changed handler and investigator
        // BEFORE the incident record is updated.
        // Functionality currently protected by global STAFF_CHANGE_EMAIL
        if (bYN(GetParm('STAFF_CHANGE_EMAIL_ELE', 'N')))
        {
            // Needed so recordid can appear in email.
            $ele['recordid'] = $recordid;

            // Don't bother sending an e-mail if the field is empty
            if ($ele['CHANGED-ele_handler'] && $ele['ele_handler'] != '')
            {
                $Factory = new UserModelFactory();
                $User = $Factory->getMapper()->findByInitials($ele['ele_handler']);

                $emailSender = EmailSenderFactory::createEmailSender('ELE', 'NewHandler');
                $emailSender->addRecipient($User);
                $emailSender->sendEmails(array_merge($User->getVars(), $ele));
            }
        }

        if ($recordid > 0)
        {
            $sql = "
                UPDATE
                    standard_elements
                SET
                    updatedby = '" . $_SESSION['initials'] . "',
                    updateddate = '" . date('d-M-Y H:i:s') . "',
                    updateid = '" . GensUpdateID($ele['updateid']) . "',
                    ele_stn_id = " . $ele['ele_stn_id'];

            if (array_key_exists('ele_score', $ele))
            {
                if ($ele['ele_score'])
                {
                    $sql .= ', ele_score = ' . round($ele['ele_score'], 0);
                }
                else
                {
                    $sql .= ', ele_score = NULL';
                }
            }

            $sql_fields = array(
                'ele_comments', 'ele_code', 'ele_descr', 'ele_manager',
                'ele_handler', 'ele_sources', 'ele_guidance', 'ele_examples', 'ele_listorder',
                'ele_priority', 'ele_action_taken', 'ele_dassigned', 'ele_ddue', 'ele_dreviewed'
            );

            $sql .= ', '.GeneratePDOSQLFromArrays(
                array(
                    'FieldArray' => $sql_fields,
                    'DataArray' => $ele,
                    'Module' => 'ELE',
                    'end_comma' => false
                ),
                $bindParams
            );

            $sql .= " WHERE recordid = $recordid";

            if (array_key_exists('updateid', $ele) && $ele['updateid'])
            {
                $sql .= " and updateid = '" . $ele['updateid'] . "'";
            }

            $query = new \DatixDBQuery($sql);
            $result = $query->prepareAndExecute($bindParams);

            if (!$result)
            {
                $error = "An error has occurred when trying to save the element.  Please report the following to the Datix administrator: $sql";
            }
            elseif ($query->getRowsAffected() == 0)
            {
                $error = _tk('cannot_save_err').'<br><br>Click <a href="'.$scripturl.'?action=element&recordid='.$recordid.'">here</a> to return to the record';
            }
        }

        if ($error != '')
        {
            SaveError($error);
        }

        $ele['recordid'] = $recordid;

        $_SESSION['ele_number'] = $ele['recordid'];

        if ($recordid)
        {
            require_once 'Source/libs/UDF.php';
            SaveUDFs($recordid, MOD_ELEMENTS);
        }

        // Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => 'ELE',
            'data'   => $ele
        ));

        $_SESSION['STANDARDS']['ele'] = $ele;
        $this->call('src\standards\controllers\ShowSaveElementTemplateController', 'showsaveelement',array());
    }
}
