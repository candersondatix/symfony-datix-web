<div class="section_title_row"><b><?php echo _tk('ELENameTitle'); ?></b></div>
<div class="windowbg2 padded_wrapper">
    <?php echo _tk('element_save_message'); ?>
</div>
<div class="button_wrapper">
    <input type="button" value="<?php echo _tk('back_to_element'); ?>" name="btnElement" onClick="SendTo('<?php echo $this->scripturl; ?>?action=element&recordid=<?php echo Escape::EscapeEntities($this->ele_number); ?>');">
    <input type="button" value="<?php echo _tk('back_to_standard'); ?>" name="btnStandard" onClick="SendTo('<?php echo $this->scripturl; ?>?action=standard&recordid=<?php echo Escape::EscapeEntities($this->ele_stn_id); ?>');">
</div>