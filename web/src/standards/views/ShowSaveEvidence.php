<div class="section_title_row"><b>Evidence</b></div>
<div class="windowbg2 padded_wrapper">
    The evidence has been saved successfully. The reference number is <?php echo $this->lib_number; ?>
</div>
<div class="button_wrapper">
    <?php if ($this->Perms == 'LIB_INPUT') : ?>
    <input type="button" value="<?php echo _tk('btn_add_another_record')?>" name="btnEvidence" onClick="SendTo('<?php echo $this->scripturl; ?>?action=addnew&module=LIB&level=2');">
    <?php else : ?>
        <?php if ($this->link_recordid) : ?>
        <input type="button" value="Back to evidence" name="btnEvidence" onClick="SendTo('<?php echo $this->scripturl; ?>?action=evidence&link_recordid=<?php echo $this->link_recordid; ?>');">
        <?php elseif ($this->lib_number) : ?>
        <input type="button" value="Back to evidence" name="btnEvidence" onClick="SendTo('<?php echo $this->scripturl; ?>?action=evidence&recordid=<?php echo $this->lib_number; ?>&module=<?php echo $this->module; ?>');">
        <?php endif; ?>
        <?php if ($this->main_module && $this->main_recordid) : ?>
        <input type="button" value="Back to <?php echo $this->main_module_name; ?>" name="btnModule" onClick="SendTo('<?php echo $this->scripturl; ?>?action=<?php echo $this->main_module_action; ?>&recordid=<?php echo htmlspecialchars($this->main_recordid); ?>');">
        <?php endif; ?>
    <?php endif; ?>
</div>