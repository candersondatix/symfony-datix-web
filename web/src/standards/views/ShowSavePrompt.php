<div class="section_title_row"><b><?php echo _tk('PRONameTitle'); ?></b></div>
<div class="windowbg2 padded_wrapper">
    <?php echo _tk('prompt_save_message'); ?>
</div>
<div class="button_wrapper">
    <input type="button" value="<?php echo _tk('back_to_prompt'); ?>" name="btnPrompt" onClick="SendTo('<?php echo $this->scripturl; ?>?action=prompt&recordid=<?php echo Escape::EscapeEntities($this->pro_number); ?>');">
    <input type="button" value="<?php echo _tk('back_to_element'); ?>" name="btnElement" onClick="SendTo('<?php echo $this->scripturl; ?>?action=element&recordid=<?php echo Escape::EscapeEntities($this->pro_ele_id); ?>');">
</div>