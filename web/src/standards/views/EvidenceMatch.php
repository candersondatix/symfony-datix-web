<?php if(count($this->matches) == 0): ?>
<p>No matching evidence found.</p>
<?php else: ?>
<table class="gridlines bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
    <tr class="tableHeader">
        <th width="6%"><b><a>Choose</a></b></th>
        <th><b><a>ID</b></b></th>
        <th><b><a>Ref.</b></b></th>
        <th><b><a>Title</b></b></th>
    </tr>
    <?php foreach($this->matches as $match): ?>
    <tr>
        <td>
            <?php if ($match['main_recordid']): ?>
            <span>Link already exists</span>
            <?php else: ?>
            <input type="button" value="Choose" onclick="SendTo('<?php echo $this->scripturl; ?>?action=evidence&amp;main_module=<?php echo Sanitize::getModule($this->main_module); ?>&amp;main_recordid=<?php echo Sanitize::SanitizeInt($this->main_recordid); ?>&amp;recordid=<?php echo intval($match['recordid']); ?>');" />
            <?php endif ?>
        </td>
        <td><?php echo intval($match['recordid']); ?></td>
        <td><?php echo htmlspecialchars($match['lib_ourref']); ?></td>
        <td><?php echo htmlspecialchars($match['lib_name']); ?></td>
    </tr>
    <?php endforeach ?>
</table>
<?php endif ?>