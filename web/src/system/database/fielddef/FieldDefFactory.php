<?php
namespace src\system\database\fielddef;

use src\framework\model\EntityFactory;
use src\system\database\field\FieldFactory;
use src\system\database\extrafield\ExtraFieldFactory;

/**
 * Delegates to the Field/ExtraField Factories to construct objects for combined FieldDef collection.
 */
class FieldDefFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\FieldInterface';
    }
    
    /**
     * {@inherit}
     */
    protected function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null)
    {
        if ($data['fdr_name'])
        {
            // standard field
            return (new FieldFactory)->createObject($data);
        }
        else
        {
            // extra field
            return (new ExtraFieldFactory)->createObject($data);
        }
    }
}