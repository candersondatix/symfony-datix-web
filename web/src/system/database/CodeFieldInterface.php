<?php
namespace src\system\database;

use src\framework\query\Query;
use src\savedqueries\model\SavedQueryModelFactory;

interface CodeFieldInterface
{
    /**
     * Getter for the codes for this field.
     * 
     * @param Query                  $query
     * @param SavedQueryModelFactory $factory
     * 
     * @return CodeCollection
     */
    public function getCodes(Query $query = null, SavedQueryModelFactory $factory = null);
    
    /**
     * Getter for the name of the table that stores the codes for this field.
     * 
     * @return string
     */
    public function getCodeTable();
    
    /**
     * Getter for the database field names for this code table.
     * 
     * @return array
     */
    public function getCodeFields();
    
    /**
     * Getter for the database field that holds the code order values for this field.
     * 
     * @return string
     */
    public function getCodeOrderColumn();

    /**
     * Getter for the database field that holds the name of the code column for this field.
     *
     * @return string|array
     */
    public function getCodeColumn();

    /**
     * Getter for the database field that holds the name of the description column for this field.
     *
     * @return string|array
     */
    public function getDescriptionColumn();
    
    /**
     * Used to flag whether this field should apply a form context-specific query (if defined) when returning codes.
     * 
     * @param boolean
     */
    public function useInFormContext($val = true);
    
    /**
     * Setter for the form data property used when building a form context-specific query.
     * 
     * @param array $data
     */
    public function setFormData(array $data);
    
    /**
     * Getter for the parent fields.
     * 
     * @return array
     */
    public function getParents();
    
    /**
     * Getter for the child fields.
     * 
     * @return array
     */
    public function getChildren();
    
    /**
     * Whether or not the codes for this field are defined in a non-standard code table.
     * 
     * @return boolean
     */
    public function usesCustomCodes();
}