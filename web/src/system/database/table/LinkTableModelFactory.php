<?php
namespace src\system\database\table;

use src\framework\model\ModelFactory;
use src\framework\query\Query;

class LinkTableModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new LinkTableFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new LinkTableMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new LinkTableCollection($this->getMapper(), $this->getEntityFactory());
    }
}