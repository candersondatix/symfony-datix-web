<?php
namespace src\system\database\table;

use src\framework\model\KeyedEntityCollection;

class TableCollection extends KeyedEntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\Table';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function keyProperty()
    {
        return array('tdr_name');
    }
}