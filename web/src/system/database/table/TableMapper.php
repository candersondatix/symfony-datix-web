<?php
namespace src\system\database\table;

use src\framework\model\Mapper;
use src\framework\query\Query;

class TableMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\Table';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'table_directory';
    }
}