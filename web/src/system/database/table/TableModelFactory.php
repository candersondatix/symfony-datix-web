<?php
namespace src\system\database\table;

use src\framework\model\ModelFactory;
use src\framework\query\Query;

class TableModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new TableFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new TableMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        $collection = new TableCollection($this->getMapper(), $this->getEntityFactory());
        $collection->setQuery(new Query());
        
        return $collection;
    }
}