<?php
namespace src\system\database\table;

use src\framework\model\EntityCollection;

class LinkCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\Link';
    }
}