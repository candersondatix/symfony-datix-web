<?php
namespace src\system\database\table;

use src\framework\model\Mapper;
use src\framework\query\Query;

class LinkTableMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\LinkTable';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'table_routes';
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->select(array('trt_table2 AS name'))
              ->from($this->getTable())
              ->groupBy(array('trt_table2'));
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Exactly the same as parent, but without (int) requirement for id
     */
    public function find($id, $overrideSecurity = false)
    {
        $class = $this->targetClass();
        $identity = $class::getIdentity();

        if (count($identity) > 1)
        {
            // if the identity is defined by one property (e.g. record id) then $id should be a single value, if not it should be an array
            throw new \MapperException('Mapper::find() is only compatible with entities which are identified by a single property');
        }

        // return the cached entity, if it exists
        if ($entity = $this->cache->get($class, $id))
        {
            return $entity;
        }

        // attempt to fetch the entity from the database
        list($query, $where, $fields) = $this->factory->getQueryFactory()->getQueryObjects();

        $where->add($fields->field($this->getDBObjectForSelect().'.'.$this->mapField($identity[0]))->eq($id));
        $query->where($where);

        if ($overrideSecurity === true)
        {
            $query->overrideSecurity();
        }

        $result = $this->select($query);

        if (!empty($result))
        {
            $object = $this->factory->getEntityFactory()->createObject($result[0]);
            $this->cache->set($object);
        }

        return $object;
    }

    /**
     * Fetches a list of fieldset IDs which describe contacts of a crtain link type.
     * 
     * @return array
     */
    public function getContactsTypeFieldsets()
    {
        $this->db->setSQL('SELECT DISTINCT fieldset FROM table_routes WHERE trt_table2 = \'contacts_main\' AND trt_default = 0');
        $this->db->prepareAndExecute();
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_COLUMN);
    }
}