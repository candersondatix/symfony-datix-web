<?php
namespace src\system\database\table;

use src\framework\model\EntityFactory;

class TableFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\table\\Table';
    }
}