<?php
namespace src\system\database\table;

use src\framework\model\Entity;
use src\framework\query\Query;
use src\framework\model\exceptions\ModelException;

class LinkTable extends Entity
{
    /**
     * A reference to the originating table that this table links to.
     * 
     * @var TableDef
     */
    protected $origin;
    
    /**
     * The name of the link table.
     * 
     * @var string
     */
    protected $name;
    
    /**
     * The collection of links used to join this table.
     * 
     * @var LinkCollection
     */
    protected $route;
    
    public function __construct()
    {
        parent::__construct();
        $factory          = new LinkModelFactory();
        $this->route      = $factory->getCollection();
        $this->addToCache = false;
    }
    
    /**
     * Once set, use the table name and the originating table to set the query condition for the route.
     * 
     * @param TableDef $value
     */
    public function setOrigin(Table $value)
    {
        $this->origin = $value;
        
        $query = new Query();
        $this->route->setQuery($query->where(array('table_routes.trt_table1' => $this->origin->tdr_name, 'table_routes.trt_table2' => $this->name)));
    }
    
    /**
     * Once set, use the table name and the originating table to set the query condition for the route.
     *
     * @param TableDef $value
     */
    public function setFieldset($fieldset)
    {
        $query = new Query();
        $this->route->setQuery($query->where(array('table_routes.fieldset' => $fieldset)));
    }

    /**
     * {@inherit}
     */
    protected static function setIdentity()
    {
        return array('fieldset');
    }
}