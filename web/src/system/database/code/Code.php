<?php
namespace src\system\database\code;

use src\framework\model\Entity;

/**
 * Represents an individual option for a coded field.
 */
class Code extends Entity
{
    // status constants
    const ENABLED    = 1; // available everywhere
    const DISABLED   = 2; // not available anywhere
    const SEARCHABLE = 3; // available when searching/reporting
    
    /**
     * The identifier for the code (a six-character string)
     * 
     * @var string
     */
    protected $code;
    
    /**
     * The description of the code.
     * 
     * @var string
     */
    protected $description;
    
    /**
     * The first parent (combo-linked) field.
     * 
     * @var string
     */
    protected $cod_parent;
    
    /**
     * The second parent (combo-linked) field.
     * 
     * @var string
     */
    protected $cod_parent2;
    
    /**
     * The status of the code, as follows:
     * 
     * X       - not available anywhere in the system.
     * N       - only available for searching/reporting.
     * <other> - available everywhere.
     * 
     * @var string
     */
    protected $cod_priv_level;
    
    /**
     * The order that the code appears in a list.
     * 
     * @var int
     */
    protected $cod_listorder;
    
    /**
     * The hex of the colour assigned to this code.
     * 
     * @var string
     */
    protected $cod_web_colour;
    
    /**
     * Returns the current status of this code.
     * 
     * @return int
     */
    public function getStatus()
    {
        switch ($this->cod_priv_level)
        {
            case 'X':
                $status = Code::DISABLED;
                break;
                
            case 'N':
                $status = Code::SEARCHABLE;
                break;
                
            default:
                $status = Code::ENABLED;
        }
        return $status;
    }
    
    /**
     * {@inherit}
     */
    protected static function setIdentity()
    {
        return array('code');
    }
}