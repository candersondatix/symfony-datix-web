<?php
namespace src\system\database\code;

use src\framework\model\KeyedEntityCollection;

class CodeCollection extends KeyedEntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\code\\Code';
    }
    
    /**
     * Outputs the collection as a JSON-encoded set of codes/descriptions,
     * in a format consumable by the DatixSelect javascript controls.
     * 
     * @return string
     */
    public function toJSON()
    {
        $this->initCollection();
        
        $data = $this->data;
        $json = [];
        
        if (empty($data))
        {
            $data[] = [
                'code'        => '!NOCODES!',
                'description' => _tk('no_codes_available')
            ];
        }
        
        foreach ($data as $code)
        {
            $value = htmlspecialchars($code['code'], ENT_QUOTES);
            
            $description = htmlspecialchars($code['description'], ENT_COMPAT, 'UTF-8', false);
            $description = \UnicodeString::rtrim($description, ' ,');
            
            $colour = ($code['colour'] != '' ? htmlspecialchars($code['colour']) : 'FFF');
            
            $json[] = [
                'value'       => $value,
                'description' => $description,
                'colour'      => $colour
            ];
        }
        
        return json_encode($json);
    }
    
    /**
     * {@inherit}
     */
    protected function keyProperty()
    {
        return array('code');
    }
}