<?php

namespace src\system\database\combolink;

use src\framework\model\Mapper;
use src\framework\query\Query;
use src\framework\model\exceptions\MapperException;

class ComboLinkMapper extends Mapper
{
    /**
     * {@inherit}
     */
    public function targetClass()
    {
        return 'src\\system\\database\\combolink\\ComboLink';
    }
    
    /**
     * {@inherit}
     */
    public function getTable()
    {
        return 'combo_links';
    }
    
    /**
     * {@inheritdoc}
     */
    public function find($id, $overrideSecurity = false)
    {
        throw new MapperException('ComboLinkMapper::find() not implemented');
    }
    
    /**
     * {@inheritdoc}
     * 
     * Custom implementation required to pull both hardcoded combo links from field_directory as well as user-defined ones from combo_links.
     */
    public function select(Query $query)
    {
        $sql = "
            SELECT
            	fdr_table AS 'table',
            	fdr_name AS child,
            	COALESCE(
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links WHERE cmb_table = fdr_table AND cmb_child = fdr_name) parent WHERE row = 1),
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links INNER JOIN subforms ON cmb_table = sfm_form WHERE sfm_tables = fdr_table AND cmb_child = fdr_name) parent WHERE row = 1), 
            		fdr_code_parent
            	) AS parent,
            	COALESCE(
            		(SELECT recordid FROM (SELECT recordid, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links WHERE cmb_table = fdr_table AND cmb_child = fdr_name) recordid WHERE row = 1),
            		(SELECT recordid FROM (SELECT recordid, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links INNER JOIN subforms ON cmb_table = sfm_form WHERE sfm_tables = fdr_table AND cmb_child = fdr_name) recordid WHERE row = 1),
            		 -1
            	) AS recordid
            FROM
            	field_directory
            WHERE
            	COALESCE(
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links WHERE cmb_table = fdr_table AND cmb_child = fdr_name) parent WHERE row = 1),
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links INNER JOIN subforms ON cmb_table = sfm_form WHERE sfm_tables = fdr_table AND cmb_child = fdr_name) parent WHERE row = 1), 
            		fdr_code_parent
            	) != ''
            
            UNION
            
            SELECT
            	fdr_table AS 'table',
            	fdr_name AS child,
            	COALESCE(
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links WHERE cmb_table = fdr_table AND cmb_child = fdr_name) parent WHERE row = 2),
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links INNER JOIN subforms ON cmb_table = sfm_form WHERE sfm_tables = fdr_table AND cmb_child = fdr_name) parent WHERE row = 2), 
            		fdr_code_parent2
            	) AS parent,
            	COALESCE(
            		(SELECT recordid FROM (SELECT recordid, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links WHERE cmb_table = fdr_table AND cmb_child = fdr_name) recordid WHERE row = 2),
            		(SELECT recordid FROM (SELECT recordid, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links INNER JOIN subforms ON cmb_table = sfm_form WHERE sfm_tables = fdr_table AND cmb_child = fdr_name) recordid WHERE row = 2),
            		 -1
            	) AS recordid
            FROM
            	field_directory
            WHERE
            	COALESCE(
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links WHERE cmb_table = fdr_table AND cmb_child = fdr_name) parent WHERE row = 2),
            		(SELECT cmb_parent FROM (SELECT cmb_parent, ROW_NUMBER() OVER (ORDER BY recordid) AS row FROM combo_links INNER JOIN subforms ON cmb_table = sfm_form WHERE sfm_tables = fdr_table AND cmb_child = fdr_name) parent WHERE row = 2), 
            		fdr_code_parent2
            	) != ''";
        
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute();
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doInsert(Entity $object)
    {
        if (!$object->isEditable())
        {
            throw new MapperException('Unable to insert hard-coded combo link');
        }
        parent::doInsert($object);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate(Entity $object)
    {
        if (!$object->isEditable())
        {
            throw new MapperException('Unable to update hard-coded combo link');
        }
        parent::doUpdate($object);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doDelete(Entity &$object)
    {
        if (!$object->isEditable())
        {
            throw new MapperException('Unable to delete hard-coded combo link');
        }
        parent::doDelete($object);
    }
}