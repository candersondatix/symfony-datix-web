<?php

namespace src\system\database\combolink;

use src\framework\model\RecordEntity;

/**
 * Models a single parent-child relationship between two (coded) fields.
 * 
 * In contrast to src\combolinking\model\ComboLink, which appears to model a subform  :\ .
 */
class ComboLink extends RecordEntity
{
    /**
     * The table the child/parent fields belong to.
     * 
     * @var string
     */
    protected $table;
    
    /**
     * The name of the child field.
     * 
     * @var string
     */
    protected $child;
    
    /**
     * The name of the parent field.
     * 
     * @var string
     */
    protected $parent;
    
    /**
     * Whether or not the combo link is user-editable.
     * 
     * Hard-coded combo links (i.e. those defined in field_directory) are given a recordid of -1 for this purpose.
     * 
     * @return boolean
     */
    public function isEditable()
    {
        return $this->recordid = -1;
    }
}