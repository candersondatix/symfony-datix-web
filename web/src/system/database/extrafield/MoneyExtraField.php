<?php
namespace src\system\database\extrafield;

use src\system\database\FieldInterface;

class MoneyExtraField extends ExtraField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::MONEY;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isNumeric()
    {
        return true;
    }
}