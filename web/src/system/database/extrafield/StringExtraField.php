<?php
namespace src\system\database\extrafield;

use src\system\database\FieldInterface;

class StringExtraField extends ExtraField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::STRING;
    }
}