<?php

namespace src\system\database\extrafield;

use src\framework\model\ModelFactory;

class ExtraFieldModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ExtraFieldFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ExtraFieldMapper(new \DatixDBQuery(), $this);
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ExtraFieldCollection($this->getMapper(), $this->getEntityFactory());
    }
}