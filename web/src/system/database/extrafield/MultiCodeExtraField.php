<?php
namespace src\system\database\extrafield;

use src\system\database\FieldInterface;

class MultiCodeExtraField extends CodeExtraField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::MULTICODE;
    }
}