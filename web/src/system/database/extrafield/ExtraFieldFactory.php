<?php

namespace src\system\database\extrafield;

use src\framework\model\EntityFactory;
use src\framework\model\exceptions\ModelException;

class ExtraFieldFactory extends EntityFactory
{
    /**
     * Target class is set dynamically, since this factory creates multiple object types.
     * 
     * @var string
     */
    protected $targetClass;
    
    public function __construct()
    {
        $this->targetClass = 'src\\system\\database\\extrafield\\ExtraField';
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return $this->targetClass;
    }
    
    /**
     * {@inherit}
     */
    protected function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null)
    {
        switch (trim($data['fld_type']))
        {
            case 'C':
                $this->targetClass = 'src\\system\\database\\extrafield\\CodeExtraField';
                break;
                
            case 'T':
                $this->targetClass = 'src\\system\\database\\extrafield\\MultiCodeExtraField';
                break;
                
            case 'D':
                $this->targetClass = 'src\\system\\database\\extrafield\\DateExtraField';
                break;
                
            case 'L':
                $this->targetClass = 'src\\system\\database\\extrafield\\TextExtraField';
                break;
                
            case 'M':
                $this->targetClass = 'src\\system\\database\\extrafield\\MoneyExtraField';
                break;
                
            case 'N':
                $this->targetClass = 'src\\system\\database\\extrafield\\NumberExtraField';
                break;
                
            case 'S':
                $this->targetClass = 'src\\system\\database\\extrafield\\StringExtraField';
                break;
                
            case 'Y':
                $this->targetClass = 'src\\system\\database\\extrafield\\YesNoExtraField';
                break;
                
            default:
                throw new ModelException('Invalid field type.');
        }

        return parent::doCreateObject($data);
    }
}