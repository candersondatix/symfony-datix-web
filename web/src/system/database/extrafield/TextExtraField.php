<?php
namespace src\system\database\extrafield;

use src\system\database\FieldInterface;

class TextExtraField extends ExtraField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::TEXT;
    }
}