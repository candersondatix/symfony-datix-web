<?php

namespace src\system\database\exceptions;

class InvalidDatabaseColumnNameException extends \Exception{}