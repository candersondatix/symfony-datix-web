<?php

namespace src\system\database\field;

use src\framework\query\Query;

class ModuleField extends CodeField
{
    /**
     * {@inheritdoc}
     */
    protected function doGetCodes(Query $query)
    {
        parent::doGetCodes($query);
        
        // swap in relabelling from language files
        foreach ($this->codes as $code)
        {
           $code->description = _tk('mod_'.strtolower($code->description).'_title') ?: $code->description;
        }
    }
}