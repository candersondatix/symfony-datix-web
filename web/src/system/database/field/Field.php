<?php
namespace src\system\database\field;

use src\framework\model\Entity;
use src\framework\registry\Registry;
use src\system\database\FieldInterface;

abstract class Field extends Entity implements FieldInterface
{
    /**
     * The field name.
     * 
     * @var string
     */
    protected $fdr_name;
    
    /**
     * The default field label.
     * 
     * @var string
     */
    protected $fdr_label;
    
    /**
     * The table the field belongs to.
     * 
     * @var string
     */
    protected $fdr_table;
    
    /**
     * The length of the field in the database.
     * 
     * @var int
     */
    protected $fdr_data_length;
    
    /**
     * The format of the data (if applicable).
     * 
     * @var string
     */
    protected $fdr_format;
    
    /**
     * Whether this is a calculated field.
     * 
     * @var string
     */
    protected $fdr_calculated_field;
    
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry = null)
    {
        parent::__construct();
        $this->addToCache = false;
        $this->registry = $registry ?: Registry::getInstance();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->fdr_name;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return $this->fdr_table;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormat()
    {
        return $this->fdr_format;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isNumeric()
    {
        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isCalculated()
    {
        return $this->fdr_calculated_field == 'Y';
    }
    
    /**
     * Gets the code for the module that this field belongs to.
     * 
     * @return string
     */
    public function getModule()
    {
        if ($this->fdr_table !== '')
        {
            foreach ($this->registry->getModuleDefs() as $module)
            {
                if ($this->fdr_table == $module['TABLE'] || $this->fdr_table == $module['VIEW'])
                {
                    return $module['CODE'];
                }
            }
        }

        return;
    }
    
    /**
     * {@inherit}
     */
    protected static function setIdentity()
    {
        return array('fdr_table','fdr_name');
    }
}