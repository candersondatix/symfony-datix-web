<?php
namespace src\system\database\field;

use src\framework\query\Query;
use src\framework\registry\Registry;
use src\framework\model\exceptions\ModelException;
use src\savedqueries\model\SavedQueryModelFactory;

class ApprovalStatusField extends CodeField
{
    /**
     * {@inheritdoc}
     */
    protected function doGetCodes(Query $query)
    {
        if (null !== ($module = $this->getModule()))
        {
            parent::doGetCodes($query);
            
            $this->codes->setQuery($query->where(array('module' => $module, 'workflow' => GetWorkflowID($module)))); // TODO move GetWorkflowID to Registry?
            
            // swap in approval status relabelling from custom language files
            foreach ($this->codes as $code)
            {
                $label = _tk('approval_status_'.$module.'_'.$code->code);
                if ($label !== null)
                {
                    $code->description = $label;
                }
            }
        }
        else
        {
            throw new ModelException('Unable to determine the module for this approval status field.');
        }
    }
    
    /**
     * {@inherit}
     */
    protected function applyFormQuery(Query $query, SavedQueryModelFactory $factory = null)
    {
        parent::applyFormQuery($query, $factory);
        
        // TODO use $this->formData to filter codes on the basis of the current status/access levels etc.
    }
}