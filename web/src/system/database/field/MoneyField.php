<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;

class MoneyField extends Field
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::MONEY;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isNumeric()
    {
        return true;
    }
}