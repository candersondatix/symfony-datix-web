<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;

class MultiCodeField extends CodeField
{
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::MULTICODE;
    }
}