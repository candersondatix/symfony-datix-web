<?php
namespace src\system\database\field;

use src\system\database\FieldInterface;
use src\system\database\CodeFieldInterface;
use src\system\database\code\CodeModelFactory;
use src\system\database\CodeFieldTrait;
use src\framework\query\Query;

class CodeField extends Field implements CodeFieldInterface
{
    use CodeFieldTrait;
    
    /**
     * The code table for this field.
     * 
     * @var string
     */
    protected $fdr_code_table;
    
    /**
     * The name of the description field in the code table.
     * 
     * @var string
     */
    protected $fdr_code_descr;
    
    /**
     * The name of the code field in the code table.
     * 
     * @var string
     */
    protected $fdr_code_field;
    
    /**
     * A specific WHERE clause used to retrieve the codes.
     * 
     * @var string
     */
    protected $fdr_code_where;
    
    /**
     * The order field in the code table.
     * 
     * @var string
     */
    protected $fdr_code_order;
    
    /**
     * Whether there is a code setup available for this field.
     * 
     * @var string
     */
    protected $fdr_setup;
    
    /**
     * The condition under which the setup is available.
     * 
     * @var string
     */
    protected $fdr_setup_condition;
    
    /**
     * Whether the codes are retrieved in a non-standard (i.e. from normal code tables) manner.
     * 
     * @var string
     */
    protected $fdr_custom_code;
    
    /**
     * {@inherit}
     */
    public function getType()
    {
        return FieldInterface::CODE;
    }
    
    /**
     * {@inherit}
     */
    public function getCodeTable()
    {
        return $this->fdr_code_table[0] == '!' ? 'code_types' : $this->fdr_code_table;
    }
    
    /**
     * {@inherit}
     */
    public function getCodeFields()
    {
        // alternatives to this list for specific field types should be encapsulated within child CodeField classes, rather than here
        if (!isset($this->codeFields))
        {
            $this->codeFields = [$this->getCodeColumn(), $this->getDescriptionColumn()];
            
            if (!$this->usesCustomCodes())
            {
                $this->codeFields = array_merge($this->codeFields, ['cod_parent', 'cod_parent2', 'cod_priv_level', 'cod_listorder', 'cod_web_colour']);
            }
        }
        return $this->codeFields;
    }
    
    /**
     * {@inherit}
     */
    public function getCodeOrderColumn()
    {
        if ($this->getCodeTable() == 'code_types')
        {
            return 'cod_listorder';
        }
        else if ($this->fdr_code_order != '')
        {
            return $this->fdr_code_order;
        }
        else if ($this->fdr_custom_code != 'Y')
        {
            return 'cod_listorder';
        }
        else
        {
            return $this->getDescriptionColumn();
        }
    }
    
    /**
     * {@inherit}
     */
    public function getCodeColumn()
    {
        if ('code_types' == $this->getCodeTable())
        {
            return 'cod_code';
        }
        return $this->fdr_code_field ?: 'code';
    }
    
    /**
     * {@inherit}
     */
    public function getDescriptionColumn()
    {
        if ('code_types' == $this->getCodeTable())
        {
            return 'cod_descr';
        }
        return $this->fdr_code_descr ?: 'description';
    }
    
    /**
     * {@inherit}
     */
    public function usesCustomCodes()
    {
        return 'Y' == $this->fdr_custom_code;
    }
    
    /**
     * {@inherit}
     */
    protected function doGetCodes(Query $query)
    {
        $this->codes = (new CodeModelFactory($this))->getCollection();
            
        if ('code_types' == $this->getCodeTable())
        {
            $query->where(array('cod_type' => substr($this->fdr_code_table, 1)));
        }
        
        if ('' != $this->fdr_code_where)
        {
            $query->setWhereStrings([$this->fdr_code_where]);
        }

        $this->codes->setQuery($query);
    }
}