<?php
namespace src\system\database\field;

use src\framework\model\EntityFactory;
use src\framework\model\exceptions\ModelException;

class FieldFactory extends EntityFactory
{
    /**
     * Target class is set dynamically, since this factory creates multiple object types.
     * 
     * @var string
     */
    protected $targetClass;
    
    /**
     * Maps field names to their field class.
     * 
     * @var array
     */
    protected $nameMap;
    
    public function __construct()
    {
        $this->targetClass = 'src\\system\\database\\field\\Field';
        
        $this->nameMap = [
            'rep_approved'    => 'src\\system\\database\\field\\ApprovalStatusField',
            'initial_current' => 'src\\system\\database\\field\\InitialCurrentField',
            'inc_med_drug'    => 'src\\system\\database\\field\\CodeField', // string_search field. TODO: create a StringSearchField class to handle code retrieval for this field type
            'inc_med_drug_rt' => 'src\\system\\database\\field\\CodeField', // string_search field
            'ram_level'       => 'src\\riskregister\\model\\field\\RiskLevelField',
            'ram_cur_level'   => 'src\\riskregister\\model\\field\\RiskLevelField',
            'ram_after_level' => 'src\\riskregister\\model\\field\\RiskLevelField',
            'pay_module'      => 'src\\system\\database\\field\\ModuleField',
            'inc_reportedby'  => 'src\\system\\database\\field\\CodeField'
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return $this->targetClass;
    }
    
    /**
     * {@inherit}
     */
    protected function doCreateObject(array $data = array(), Entity $baseEntity = null, EntityCache $cache = null)
    {
        switch ($data['fdr_data_type'])
        {
            case 'C':
                if (null !== ($this->targetClass = $this->nameMap[$data['fdr_name']]))
                {
                    return parent::doCreateObject($data);
                }
                
                if ($data['fdr_code_table'] == 'vw_staff_combos')
                {
                    if ($data['fdr_data_length'] > 6)
                    {
                        $this->targetClass = 'src\\users\\model\\field\\MultiCodeStaffField';
                    }
                    else
                    {
                        $this->targetClass = 'src\\users\\model\\field\\StaffField';
                    }
                    return parent::doCreateObject($data);
                }
                
                $this->targetClass = $data['fdr_data_length'] > 6 ? 'src\\system\\database\\field\\MultiCodeField' : 'src\\system\\database\\field\\CodeField';
                break;
                
            case 'D':
                $this->targetClass = 'src\\system\\database\\field\\DateField';
                break;
                
            case 'L':
                $this->targetClass = 'src\\system\\database\\field\\TextField';
                break;
                
            case 'M':
                $this->targetClass = 'src\\system\\database\\field\\MoneyField';
                break;
                
            case 'N':
                $this->targetClass = 'src\\system\\database\\field\\NumberField';
                break;
                
            case 'S':
                $this->targetClass = 'src\\system\\database\\field\\StringField';
                break;
                
            case 'Y':
                $this->targetClass = 'src\\system\\database\\field\\YesNoField';
                break;
                
            default:
                throw new ModelException('Invalid field type.');
        }
        return parent::doCreateObject($data);
    }
}
