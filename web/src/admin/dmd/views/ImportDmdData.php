<?php if ($this->error): ?>
<div class="form_error_wrapper">
    <div class="form_error"><?php echo htmlspecialchars($this->error); ?></div>
</div>
<?php else: ?>
<form method="post" enctype="multipart/form-data" action="app.php?action=importdmd" id="dmdform">
    <?php echo $this->table->GetFormTable(); ?>
    <div class="button_wrapper">
        <input id="btnImport" type="button" value="<?=_tk('btn_import')?>" onclick="importDmdData();" />
        <input id="btnBack" type="button" value="<?=_tk('btn_back')?>" onclick="SendTo('app.php?action=home&module=MED');" />
    </div>
</form>
<?php endif; ?>