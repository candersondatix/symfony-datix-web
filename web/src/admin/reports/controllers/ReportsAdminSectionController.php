<?php

namespace src\admin\reports\controllers;

use src\framework\controller\Controller;
use src\framework\query\FieldCollection;
use src\framework\query\Query;
use src\framework\query\SqlWriter;
use src\framework\query\Where;
use src\profiles\model\profile\ProfileModelFactory;
use src\reports\exceptions\ReportException;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\savedqueries\model\SavedQuery;
use src\security\groups\model\GroupModelFactory;
use src\users\model\UserCollection;
use src\users\model\UserModelFactory;

class ReportsAdminSectionController extends Controller
{
    public function packagedreporteditsection()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $data = $this->request->getParameter('data');

        if ($data['recordid'])
        {
            $packagedReport = (new PackagedReportModelFactory)->getMapper()->find($data['recordid']);
        }
        else
        {
            //new packaged report - we should have the report id to work with.
            $reportId = $this->request->getParameter('web_report_id');

            $packagedReport = (new PackagedReportModelFactory)->getEntityFactory()->createObject(array('web_report_id' => $reportId));
        }

        $module = $packagedReport->report->module;

        if ($packagedReport->name == "")
        {
            $packagedReport->name = $packagedReport->report->title;
        }

        $ModuleWhere = $_SESSION[$module]["WHERE"];
        $QBE = (isset($_SESSION['packaged_report']) && $_SESSION['packaged_report']['success'] == true && $_SESSION['packaged_report']['recordid'] == $packagedReport->recordid);
        unset($_SESSION['packaged_report']);

        $titleField = new \FormField();
        $titleField->MakeInputField('name', 70, 256, $packagedReport->name, "");

        include_once 'Source/libs/QueriesBase.php';
        $saved_queries = get_saved_queries($module);

        if (!$saved_queries)
        {
            $saved_queries = array();
        }

        $queryField = \Forms_SelectFieldFactory::createSelectField('query', $module, $packagedReport->query->old_query_id, '');
        $queryField->setCustomCodes($saved_queries);
        $queryField->setOnChangeExtra('initDropdown(jQuery("#query_title"));getQueryString(jQuery("#query_title").data("datixselect").field.val(), "'.$module.'")');
        $queryField->setSuppressCodeDisplay();

        $sqlWhereClause = '';
        if ($packagedReport->query instanceof SavedQuery)
        {
            $query = clone $packagedReport->query->createQuery();
            $query->overrideSecurity();
            
            $sw = new SqlWriter();
            $sw->retainAtCodes();
            
            if (null != $query->getWhere())
            {
                $sqlWhereClause = $sw->writeLiteralWhereClause($query);
            }
            else
            {
                $sqlWhereClause = $query->getWhereStrings()[0];
            }
        }

        $whereField = new \FormField();
        $whereField->MakeTextAreaField('where', 7, 70, '', ($QBE ? $ModuleWhere : $sqlWhereClause), false);

        $searchFieldHTML = '
        <input type="hidden" name="searchwhere" id="searchwhere" value="' . \Escape::EscapeEntities($ModuleWhere) . '" />
        <input type="hidden" name="CHANGED-where" id="CHANGED-where" value="'. ($QBE ? '1' : '0'). '" />
        '.($QBE ? '<script type="text/javascript">jQuery(function(){ setChanged("where") });</script>' : '').'
        <input type="hidden" name="QBE" id="QBE" value="'.($QBE == true ? '1' : '0').'" />
        <input type="button" value="Use current search criteria"' .
            ' onclick="javascript:document.getElementById(\'where\').value = document.getElementById(\'searchwhere\').value;jQuery(\'#CHANGED-where\').val(\'1\');">&nbsp;
        <input type="button" value="Define criteria using query by example" onclick="this.form.action=\'app.php?action=querybyexampleforpackagedreport\';this.form.submit();" />';

        $searchField = new \FormField();
        $searchField->MakeCustomField($searchFieldHTML);

        $securityField = new \FormField();
        $securityField = $securityField->MakeYesNoSelect('override_security', '', $packagedReport->override_security ? 'Y' : 'N');

        $this->response->build('src/reports/views/PackagedReportEditSection.php', array(
            'titleField'     => $titleField,
            'queryField'     => $queryField,
            'whereField'     => $whereField,
            'searchField'    => $searchField,
            'securityField'  => $securityField,
            'QBE'            => $QBE,
        ));
    }

    public function packagedreportpermissionsection()
    {
        $recordid = $this->request->getParameter('recordid');

        // Allows us to use existing security permissions screens. Once these screens are refactored, we can remove this.
        $_SESSION['record_permissions']['url_from'] = 'app.php?action=designapackagedreport&recordid='.$recordid;

        $packagedReport = (new PackagedReportModelFactory)->getMapper()->find($recordid);

        $userIds = \DatixDBQuery::PDO_fetch_all('SELECT user_id FROM web_packaged_report_users WHERE web_packaged_report_id = :recordid', array('recordid' => $packagedReport->recordid), \PDO::FETCH_COLUMN);

        $query = new Query();
        $where = new Where();
        $where->add((new FieldCollection)->field('staff.recordid')->in($userIds));
        $query->where($where);
        $userCollection = (new UserModelFactory)->getCollection();
        $userCollection->setQuery($query);

        $groupIds = \DatixDBQuery::PDO_fetch_all('SELECT group_id FROM web_packaged_report_groups WHERE web_packaged_report_id = :recordid', array('recordid' => $packagedReport->recordid), \PDO::FETCH_COLUMN);

        $query = new Query();
        $where = new Where();
        $where->add((new FieldCollection)->field('sec_groups.recordid')->in($groupIds));
        $query->where($where);
        $groupCollection = (new GroupModelFactory)->getCollection();
        $groupCollection->setQuery($query);

        $profileIds = \DatixDBQuery::PDO_fetch_all('SELECT profile_id FROM web_packaged_report_profiles WHERE web_packaged_report_id = :recordid', array('recordid' => $packagedReport->recordid), \PDO::FETCH_COLUMN);

        $query = new Query();
        $where = new Where();
        $where->add((new FieldCollection)->field('profiles.recordid')->in($profileIds));
        $query->where($where);
        $profileCollection = (new ProfileModelFactory)->getCollection();
        $profileCollection->setQuery($query);

        $this->response->build('src/reports/views/PackagedReportPermissionSection.php', array(
            'packagedReport'     => $packagedReport,
            'userCollection'     => $userCollection,
            'groupCollection'    => $groupCollection,
            'profileCollection'    => $profileCollection
        ));
    }

}