<?php

namespace src\admin\reports\controllers;

use src\framework\controller\Response;
use src\framework\controller\Request;
use src\framework\controller\TemplateController;
use src\framework\query\QueryFactory;
use src\reports\model\report\Report;
use src\reports\model\report\ReportFactory;
use src\reports\model\report\ReportModelFactory;
use src\reports\model\packagedreport\PackagedReportModelFactory;
use src\reports\controllers\ReportDesignFormController;
use src\reports\model\field\ReportField;
use src\savedqueries\model\SavedQueryModelFactory;
use src\framework\query\SqlWriter;

class ReportsAdminController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);

        $this->title = _tk('reports_administration_title');
        $this->module = $this->request->getParameter('module');
        $this->image = 'Images/icons/icon_cog_reports.png';

        if ($this->module == '')
        {
            $this->module = array_shift(array_flip(getModArray()));
        }
        $fieldsetup = null;
        if (IsFullAdmin())
        {
            $fieldsetup = ['label' => _tk('reporting_fields_setup'), 'link' => 'action=excludefields'];
        }
        $this->menuParameters = [
            'module' => 'ADM',
            'action' => $this->request->getParameter('action'),
            'show_excluded' => $this->request->getParameter('show_excluded'),
            'menu_array' => [
                ['label' => _tk('design_graphical_reports'), 'link' => 'action=reportsadminlist'],
                ['label' => _tk('btn_new_report'), 'link' => 'action=designabasereport&module=' . $this->module],
                ['label' => _tk('design_listing_reports'), 'link' => 'action=listlistingreports'],
                ['label' => _tk('btn_new_listing_report'), 'link' => 'action=reportsadminaction&form_action=new&module=' . $this->module],
                $fieldsetup,
            ]
        ];

        $this->hasPadding = false;
    }

    /**
     * @desc Shows a list of saved reports per module
     */
    public function reportsadminlist()
    {
        if (IsFullAdmin() || bYN(GetParm('ADM_NO_ADMIN_REPORTS'), 'N'))
        {
            $this->title .= ' - '._tk('graphical_reports');
            list($orderby, $order) = $this->getOrdering();

            // Administrator can see reports that are made private by other users so we don't filter by report security here
            $reportCollection = (new ReportModelFactory)->getCollection();

            $where = (new QueryFactory)->getWhere();
            $where->add((new QueryFactory)->getFieldCollection()->field('web_reports.module')->eq($this->module)->field('web_reports.type')->notEq(Report::LISTING));

            if(!IsFullAdmin())
            {
                $where->add((new QueryFactory)->getFieldCollection()->field('web_reports.createdby')->eq($_SESSION['initials']));
            }

            $query = (new QueryFactory)->getQuery();
            $query
                ->select(array('web_reports.title', 'web_reports.type'))
                ->where($where)
                ->orderBy(array(array($orderby, $order)));

            $reportCollection->setQuery($query);

            $this->response->build('src/admin/reports/views/ReportsAdminList.php', array(
                'ModuleDropDown' => $this->getModuleDropdown(),
                'reportCollection' => $reportCollection,
                'LastGroup' => '',
                'colspan' => 4,
                'message' => $this->request->getParameter('message'),
                'module' => $this->module,
                'orderby' => $orderby,
                'order' => $order
            ));
        }
        else
        {
            throw new \URLNotFoundException();
        }
    }

    /**
    * @desc Shows a list of saved reports per module
    */
    public function listlistingreports()
    {
        $this->title .= ' - '._tk('listing_reports');

        list($orderby, $order) = $this->getOrdering();

        // Administrator can see reports that are made private by other users so we don't filter by report security here
        $reportCollection = (new ReportModelFactory)->getCollection();

        $where = (new QueryFactory)->getWhere();

        $where->add((new QueryFactory)->getFieldCollection()
        	->field('web_reports.module')->eq($this->module)
        	->field('web_reports.type')->eq(Report::LISTING));

        if(!IsFullAdmin())
        {
            $where->add((new QueryFactory)->getFieldCollection()->field('web_reports.createdby')->eq($_SESSION['initials']));
        }

        $query = (new QueryFactory)->getQuery();
        $query->select(array('web_reports.title'))
        	->where($where)
        	->orderBy(array(array($orderby, $order)));

        $reportCollection->setQuery($query);

        $this->response->build('src/admin/reports/views/ReportsAdminListingList.php', array(
            'ModuleDropDown' => $this->getModuleDropdown('showModule(this);document.reportslistmodule.submit();'),
        	'reportCollection' => $reportCollection,
            'LastGroup' => '',
            'colspan' => 4,
            'message' => $this->request->getParameter('message'),
            'module' => $this->module,
            'orderby' => $orderby,
            'order' => $order
        ));
    }

    /**
     * Returns the markup for the module selector.
     *
     * @return string
     */
    protected function getModuleDropdown($onChange = '')
    {
        if ($onChange == '')
        {
            $onChange = 'showModule(this);document.reportslistmodule.submit();';
        }

        $modulesWithoutPermissions = [];

        if (!IsFullAdmin())
        {
            $modulesArray = array_keys(getModArray());
            $moduleDefs   = $this->registry->getModuleDefs();

            // Loop through DatixWeb modules and restrict access based on user permissions
            foreach ($modulesArray as $module)
            {
                if (!$this->registry->getParm($moduleDefs[$module]['PERM_GLOBAL']))
                {
                    $modulesWithoutPermissions[] = $module;
                }
            }
        }

        return getModuleDropdown(array(
            'name' => 'lbModule',
            'onchange' => $onChange,
            'current' => $this->module,
            'not' => array_merge(array('DAS','DST','HSA','ADM','ELE','PRO','TOD','ATM','ATQ','LOC','ORG'), $modulesWithoutPermissions)
        ));
    }

    /**
     * Provides ordering parameters for reports listings.
     *
     * @return array
     */
    protected function getOrdering()
    {
        $orderby = $this->request->getParameter('orderby') ?: 'web_reports.title';
        $order = $this->request->getParameter('order') == 'DESC' ? 'DESC' : 'ASC';

        return [$orderby, $order];
    }

    /**
     * @desc Design a report screen via reports administration.
     */
    function designabasereport()
    {
        if (IsFullAdmin() || bYN(GetParm('ADM_NO_ADMIN_REPORTS')))
        {
            $recordid = (int) $this->request->getParameter('recordid');
            $showExcluded = ($this->request->getParameter('show_excluded') !== null ? $this->request->getParameter('show_excluded') : $_SESSION['reports_administration:show_excluded']);

            // Default value is hide forms/fields
            if (null === $showExcluded)
            {
                $showExcluded = '0';
            }

            $_SESSION['reports_administration:show_excluded'] = $showExcluded;

            if ($this->request->getParameter('validation_error'))
            {
                $report = clone $this->request->getParameter('report');
                $module = $report->module;
            }
            else if ($recordid)
            {
                $report = (new ReportModelFactory())->getMapper()->find($recordid);
                $module = $report->module;
            }
            else
            {
                $module = $this->request->getParameter('module');
                $report = (new ReportModelFactory)->getEntityFactory()->createObject(array('type' => Report::BAR, 'module' => $module));
            }

            if ($report->getReportType() == Report::LISTING)
            {
                //listing reports have a different edit screen
                $this->redirect('app.php?action=reportsadminaction&rep_id='.$report->base_listing_report.'&form_action=edit');
            }

            if($recordid)
            {
                //need the list of packaged reports that use this base report.
                $collection = (new PackagedReportModelFactory)->getCollection();
                $query = (new QueryFactory)->getQuery();
                $where = (new QueryFactory)->getWhere();
                $where->add((new QueryFactory)->getFieldCollection()->field('web_packaged_reports.web_report_id')->eq($recordid));

                $collection->setQuery(
                    $query->select(array('web_packaged_reports.name'))
                        ->where($where)
                );

                $listing = new \RecordLists_RecordList();
                $listing->AddRecordData($collection);

                $listingDesign = new \Listings_ListingDesign(
                    array('columns' => array(
                        new \Fields_DummyField(array('name' => 'name', 'label' => 'Name', 'type' => 'S'))
                    )));

                $listingDisplay = new \Listings_ListingDisplay($listing, $listingDesign);
                $listingDisplay->Action = 'designapackagedreport';
            }

            $ButtonGroup = new \ButtonGroup();
            $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'type' => 'submit', 'action' => 'SAVE'));
            $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'if(confirm(\'Are you sure you want to cancel?\')){SendTo(\'app.php?action=reportsadminlist\')}', 'action' => 'CANCEL'));

            if ($recordid)
            {
                $ButtonGroup->AddButton(array('id' => 'btnDelete', 'name' => 'btnDelete', 'label' => _tk('delete_base_report'), 'onclick' => 'if(confirm(\'Delete Base Report?\\nPlease confirm you wish to delete this report\')){SendTo(\'app.php?action=deleteabasereport&recordid='.$recordid.'\')}', 'action' => 'DELETE'));
                $ButtonGroup->AddButton(array('id' => 'btnExport', 'name' => 'btnExport', 'label' => _tk('export_report_design'), 'onclick' => 'SendTo(\'app.php?action=exportabasereport&rep_id=' . $recordid . '\');', 'action' => 'BACK', 'icon' => 'images/icons/toolbar-export.png'));
            }

            // We can't assign this id earlier otherwise the cache gets confused and overwrites the changes the user has made in this object.
            if ($this->request->getParameter('validation_error'))
            {
                $report->recordid = $recordid;
            }

            $packagedReport = (new PackagedReportModelFactory)->getEntityFactory()->createObject(array('report' => $report));

            $form = $this->call('src\\reports\\controllers\\ReportDesignFormController', 'reportDesignerForm',
                array('packagedReport'=>$packagedReport, 'context' => 'administration', 'show_excluded' => $showExcluded));

            $this->response->build('src/admin/reports/views/DesignABaseReport.php', array(
                'form'  => $form,
                'module' => $module,
                'recordid' => $recordid,
                'buttonGroup'    => $ButtonGroup,
                'listingDisplay' => $listingDisplay
            ));
        }
        else
        {
            throw new \URLNotFoundException();
        }
    }

    public function saveabasereport()
    {
        $recordid = $this->request->getParameter('recordid');

        $this->request->setParameter('report', (new ReportFactory)->createFromFormRequest($this->request));

        if ($recordid)
        {
            $currentReport = (new ReportModelFactory)->getMapper()->find($recordid);

            $report = (new ReportModelFactory)->getEntityFactory()->createFromRequest($this->request, $currentReport);
        }
        else
        {
            $report = (new ReportModelFactory)->getEntityFactory()->createFromRequest($this->request);
        }

        //Before we save the object, we need to validate the information provided.
        $validationFailure = false;

        if ($report->title == '')
        {
            AddSessionMessage('ERROR', 'You must enter a report name.');
            $validationFailure = true;
        }

        if ($validationFailure)
        {
            $this->response = $this->call('src\\admin\\reports\\controllers\\ReportsAdminController' , 'designabasereport',
                array('validation_error' => true, 'report' => $report));
            exit;
            //$this->redirect('app.php?action=designabasereport&recordid='.$report->recordid);
        }
        else
        {
            AddSessionMessage('INFO', 'Report saved.');
            (new ReportModelFactory)->getMapper()->save($report);

            $this->redirect('app.php?action=designabasereport&recordid='.$report->recordid);
        }
    }

    /**
     * Export a base report to XML
     *
     * @author gszucs
     * @throws \InvalidArgumentException
     */
    public function exportABaseReport()
    {
        $recordid = $this->request->getParameter('rep_id');


        if( ! is_scalar( $recordid ) || ! ( ( $report = (new ReportModelFactory)->getMapper()->find($recordid) ) || ( $report = (new ReportModelFactory)->getMapper()->findByBaseListingReportID($recordid) ) ) )
        {
            throw new \InvalidArgumentException('Report does not exist.');
        }

        $simpxml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><DATIXDATA/>');

        $table = $simpxml->addChild('TABLE');
        $table->addAttribute('name', (new ReportModelFactory)->getMapper()->getTable() );
        $table->addAttribute('module', 'ADM' );
        $table->addAttribute('recordid_req', '0' );

        $record = $table->addChild('RECORD');

        foreach( $report->getExportData() as $property => $value )
        {
            $column = $record->addChild('COLUMN');
            $column->addAttribute('name', $property );
            $column->addAttribute('value', $value );
        }

        // add packaged reports data
        if( $report->getPackagedReports()->count() > 0 )
        {
            $linktable = $record->addChild('LINKTABLE');
            $linktable->addAttribute('name', 'web_packaged_reports' );
            $linktable->addAttribute('module', 'ADM' );
            $linktable->addAttribute('recordid_req', '0' );
            $linktable->addAttribute('parent_link_field', 'web_report_id' );

            foreach ($report->getPackagedReports() as $packageReport)
            {
                $linktableRecord = $linktable->addChild('RECORD');

                foreach( $packageReport->getExportData() as $property => $value )
                {
                    $column = $linktableRecord->addChild('COLUMN');
                    $column->addAttribute('name', $property );
                    $column->addAttribute('value', $value );
                    if( $property == 'name' )
                    {
                        $column->addAttribute('iskeycolumn', 1 );
                    }
                }
            }
        }

        header("Content-Type: text/xml");
        header("Content-Disposition: attachment; filename=DATIXWebReportDesign_$recordid.xml");
        header("Content-Encoding: none");

        echo $simpxml->asXML();
        die();
    }

    public function designapackagedreport()
    {
        $recordid = (int) $this->request->getParameter('recordid');

        if ($recordid)
        {
            $packagedReport = (new PackagedReportModelFactory)->getMapper()->find($recordid);
        }
        else
        {
            //new packaged report - should have the base report id
            $web_report_id = $this->request->getParameter('web_report_id');

            $packagedReport = (new PackagedReportModelFactory)->getEntityFactory()->createObject(array('web_report_id' => $web_report_id));
        }

        $this->module = 'ADM';
        $module = $packagedReport->report->module;

        $this->title = _tk('reports_administration_title');

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'onclick' => 'this.form.submit()', 'action' => 'SAVE'));
        $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'SendTo(\'app.php?action=designabasereport&recordid='.$packagedReport->report->recordid.'\')', 'action' => 'CANCEL'));

        if ($recordid)
        {
            $ButtonGroup->AddButton(array('id' => 'btnDelete', 'name' => 'btnDelete', 'label' => _tk('delete_packaged_report'), 'onclick' => 'if(confirm(\''. _tk('delete_packaged_report_q') .'\')){SendTo(\'app.php?action=deleteapackagedreport&recordid='.$recordid.'\');}', 'action' => 'DELETE'));
        }

        GetSideMenuHTML(array('module' => 'ADM', 'buttons' => $ButtonGroup));

        $FormArray = array (
            "Parameters" => array("Panels" => "True", "Condition" => false),
            "name" => array(
                "Title" => _tk('packaged_report_settings_title'),
                "MenuTitle" => _tk('packaged_report_admin_menu_title'),
                "NewPanel" => true,
                'ControllerAction' => array(
                    'packagedreporteditsection' => array(
                        'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminSectionController'
                    )
                ),
                "Rows" => array()
            ),
            "permissions" => array(
                "Title" => "Permissions",
                "NewPanel" => false,
                "NoFieldAdditions" => true,
                'ControllerAction' => array(
                    'packagedreportpermissionsection' => array(
                        'controller' => 'src\\admin\\reports\\controllers\\ReportsAdminSectionController'
                    )
                ),
                "Condition" => $recordid,
                "NotModes" => array('New'),
                "Rows" => array()
            )
        );

        $reportTable = new \FormTable('Edit');
        $reportTable->MakeForm($FormArray, $packagedReport, $module);

        $reportTable->MakeTable();
        $form = $reportTable->GetFormTable();

        $this->response->build('src/admin/reports/views/DesignAPackagedReport.php', array(
            'form'   => $form,
            'module' => $module,
            'recordid' => $recordid,
            'web_report_id' => $web_report_id,
            'buttonGroup'    => $ButtonGroup
        ));
    }

    /**
     * Needed because sometimes we are doing a QBE without saving the report, and so need to keep track of all of the report variables in the session.
     */
    public function querybyexampleforpackagedreport()
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $module = $this->request->getParameter('module');

        $searchUrl = 'index.php';
        if (isset($ModuleDefs[$module]['SEARCH_URL']))
        {
            $searchUrl .= '?' . $ModuleDefs[$module]['SEARCH_URL'] . '&module='.$module.'&from_packaged_report=1';
        }
        else if ($ModuleDefs[$module]['GENERIC'])
        {
            $searchUrl .= '?action=search&module='.$module.'&from_packaged_report=1';
        }

        $_SESSION['packaged_report']['name'] = $this->request->getParameter('name');
        $_SESSION['packaged_report']['recordid'] = $this->request->getParameter('recordid');
        $_SESSION['packaged_report']['web_report_id'] = $this->request->getParameter('web_report_id');

        $this->redirect($searchUrl);
    }

    public function saveapackagedreport()
    {
        $recordid = $this->request->getParameter('recordid');

        $query = $this->request->getParameter('query');

        // We don't want to blank out the query portion of the report object: if no query has been selected,
        // we should keep the old query.
        if ($query == '')
        {
            $this->request->unSetParameter('query');
        }

        //override_user_security is saved as a bool, but posted as Y/N, so need to translate here:
        $this->request->setParameter('override_security', ($this->request->getParameter('override_security') == 'Y'));

        if ($recordid)
        {
            $currentReport = (new PackagedReportModelFactory)->getMapper()->find($recordid);
            $report = (new PackagedReportModelFactory)->getEntityFactory()->createFromRequest($this->request, $currentReport);
        }
        else
        {
            $report = (new PackagedReportModelFactory)->getEntityFactory()->createFromRequest($this->request);
        }

        // if $_SESSION[ $report->report->module ]['list_type'] has been set it will cause a problem...extra where string will be added to the where clause
        if ($_SESSION[ $report->report->module ]['list_type'] != '')
        {
        	$temp_list_type = $_SESSION[ $report->report->module ]['list_type'];
        	unset( $_SESSION[ $report->report->module ]['list_type'] );
        }

        $whereClauseChanged = ($this->request->getParameter('CHANGED-where') == '1');

        if ($whereClauseChanged)
        {
            //need to decide whether to save as a linked SavedQuery or as a where string
            $savedQueryId = $this->request->getParameter('query');
            $whereClause = $this->request->getParameter('where');

            if ($savedQueryId)
            {
                //a saved query was selected, so need to compare the string: if it doesn't match the saved query we need to save a new "string" where clause
                $savedQueryInDb = (new SavedQueryModelFactory)->getMapper()->find($savedQueryId);

                if ($savedQueryInDb->where != $whereClause)
                {
                    if ($whereClause == '')
                    {
                        //no indication as to what the where clause should be - set it to 1=1 as default.
                        $whereClause = '1=1';
                    }

                    $report->query = (new SavedQueryModelFactory)->getEntityFactory()->createObject(
                        array(
                            'module' => $report->report->module,
                            'where_string' => $whereClause
                        ));
                }
            }
            else
            {
                if ($whereClause == '')
                {
                    //no indication as to what the where clause should be - set it to 1=1 as default.
                    $whereClause = '1=1';
                }

                $report->query = (new SavedQueryModelFactory)->getEntityFactory()->createObject(
                    array(
                        'module' => $report->report->module,
                        'where_string' => $whereClause
                    ));
            }
        }
        else if ($this->request->getParameter('QBE') == 1 && ($_SESSION[$report->report->module]['NEW_WHERE'] instanceof Where))
        {
            //Query by example used, and where string not changed, so can use the contents of $_SESSION['NEW_WHERE'] to save a new saved query.
            $report->query = (new SavedQueryModelFactory)->getEntityFactory()->createObject(array(
                'module' => $report->report->module,
                'where' => $_SESSION[$report->report->module]['NEW_WHERE']
            ));
        }
        else if ($query == '' && $this->request->getParameter('where') == '')
        {
            //no indication as to what the where clause should be - set it to 1=1 as default.
            $whereClause = '1=1';

            $report->query = (new SavedQueryModelFactory)->getEntityFactory()->createObject(
                array(
                    'module' => $report->report->module,
                    'where_string' => $whereClause
                ));
        }

        if(isset($temp_list_type))
        {
        	$_SESSION[ $report->report->module ]['list_type'] = $temp_list_type;
        }

        //Before we save the object, we need to validate the information provided.
        $validationFailure = false;

        if ($report->name == '')
        {
            AddSessionMessage('ERROR', 'You must enter a report name.');
            $validationFailure = true;
        }

        //Check WHERE syntax
        list($sql, $parameters) = (new SqlWriter)->writeWhereClause($report->query->createQuery());
        //need to remove the "WHERE " characters from the start of the string
        $sqlWhereClause = substr(\UnicodeString::trim((new SqlWriter)->combineSqlAndParameters($sql, $parameters)), 6);

        if ($report->query instanceof SavedQuery && !ValidateWhereClause($sqlWhereClause, $report->report->module))
        {
            AddSessionMessage('ERROR', '"SQL where clause" contains a syntax error');
            $validationFailure = true;
        }

        if ($validationFailure)
        {
            if ($report->recordid)
            {
                $this->redirect('app.php?action=designapackagedreport&recordid='.$report->recordid);
            }
            else
            {
                $this->redirect('app.php?action=designapackagedreport&web_report_id='.$this->request->getParameter('web_report_id'));
            }
        }
        else
        {
            AddSessionMessage('INFO', 'Packaged report saved.');
            (new PackagedReportModelFactory)->getMapper()->save($report);

            if (count($report->users) == 0 && count($report->groups) == 0 && count($report->profiles) == 0)
            {
                AddSessionMessage('INFO', _tk('packaged_report_saved_permission_warning'));
            }

            $this->redirect('app.php?action=designapackagedreport&recordid='.$report->recordid);
        }
    }

    public function deleteabasereport()
    {
        $recordid = $this->request->getParameter('recordid');

        $baseReport = (new ReportModelFactory)->getMapper()->find($recordid);
        (new ReportModelFactory)->getMapper()->delete($baseReport);

        $this->redirect('app.php?action=reportsadminlist');
    }

    public function deleteapackagedreport()
    {
        $recordid = $this->request->getParameter('recordid');

        $packagedReport = (new PackagedReportModelFactory)->getMapper()->find($recordid);

        $baseReportId = $packagedReport->report->recordid;

        (new PackagedReportModelFactory)->getMapper()->delete($packagedReport);

        $this->redirect('app.php?action=designabasereport&recordid='.$baseReportId);
    }

    /**
     * The reporting field setup page.
     */
    public function excludeFields()
    {
        if ($this->request->getMethod() == 'POST')
        {
            $this->saveExcludeFields();
        }
        $js = array('previousModule' => $this->module,
                'saveChangesMessage' => _tk('confirm_save_batch_update'));
        $this->sendToJs($js);
        $this->addCss('src/admin/reports/css/excludefields.css');
        $this->addJs('src/admin/reports/js/excludefields.js');


        $this->title .= ' - '._tk('reporting_fields_setup');
        $this->module = $this->request->getParameter('lbModule') ?: 'INC';

        $availableFields = [];
        $excludedFields  = [];
        $expanded        = [];

        // TODO ReportDesignFormController::getReportableFieldSets() should be the responsibility of a CodeField class instead
        $fieldsets = (new ReportDesignFormController($this->request, $this->response))->getReportableFieldSets($this->module);

        foreach (array_keys($fieldsets) as $fieldset)
        {
            $availableFields[$fieldset] = (new ReportField($fieldset, $this->module))->getCodes();
            $excludedFields[$fieldset] = (new ReportField($fieldset, $this->module, true))->getCodes();
            $expanded[$fieldset] = $this->request->getParameter($fieldset.'_expanded');
        }

        // TODO ReportDesignFormController::getTables() should be the responsibility of a CodeField class instead
        $tables = (new ReportDesignFormController($this->request, $this->response))->getTables($this->module);

        $excludedTables = \DatixDBQuery::PDO_fetch_all(
            'SELECT rex_fieldset, rex_module FROM report_fieldset_exclusions WHERE rex_module = :module',
            array('module' => $this->module), \PDO::FETCH_KEY_PAIR);

        foreach (array_keys($tables) as $table)
        {
            if(in_array($table, array_keys($excludedTables)))
            {
                $excludedTables[$table] = true;
            }
            else
            {
                $excludedTables[$table] = false;
            }

            $availableFields[$table] = (new ReportField($table, $this->module))->getCodes();
            $excludedFields[$table] = (new ReportField($table, $this->module, true))->getCodes();
            $expanded[$table] = $this->request->getParameter($table.'_expanded');
        }

        $this->response->build('src/admin/reports/views/ExcludeFields.php', array(
            'moduleSelect'    => $this->getModuleDropdown('return false;'),
            'fieldsets'       => $fieldsets,
            'tables'          => $tables,
            'excludedTables'  => $excludedTables,
            'availableFields' => $availableFields,
            'excludedFields'  => $excludedFields,
            'expanded'        => $expanded,
        	'module_switch'	=> $this->module
        ));
    }

    /**
     * Handles the save of the reporting field setup options.
     */
    protected function saveExcludeFields()
    {
        $module = $this->request->getParameter('lbModule');

        // TODO ReportDesignFormController::getReportableFieldSets() should be the responsibility of a CodeField class instead
        $fieldsets = (new ReportDesignFormController($this->request, $this->response))->getReportableFieldSets($module);

        // TODO move the DB access logic elsewhere
        foreach (array_keys($fieldsets) as $fieldset)
        {
            if ($this->request->getParameter('checkbox_'.$fieldset) === 'on' || $fieldset === 0)
            {
                \DatixDBQuery::PDO_query('DELETE FROM report_fieldset_exclusions WHERE rex_module = ? AND rex_fieldset = ?', [$module, $fieldset]);
            }
            else
            {
                \DatixDBQuery::PDO_query('
                    IF NOT EXISTS (SELECT * FROM report_fieldset_exclusions WHERE rex_module = ? AND rex_fieldset = ?)
                    INSERT INTO report_fieldset_exclusions VALUES (?, ?)', [$module, $fieldset, $module, $fieldset]);
            }

            if (null !== ($excludedFields = $this->request->getParameter($fieldset.'_excluded')))
            {
                $deleteParams = array_merge([ $module, $fieldset ], $excludedFields);

                \DatixDBQuery::PDO_query('DELETE FROM report_exclusions WHERE rex_module = ?
                    AND rex_fieldset = ? AND rex_field NOT IN ('.implode(',', array_fill(0, count($excludedFields), '?')).')', $deleteParams);

                foreach ($excludedFields as $excludedField)
                {
                    \DatixDBQuery::PDO_query('
                        IF NOT EXISTS (SELECT * FROM report_exclusions WHERE rex_module = ? AND rex_fieldset = ? AND rex_field = ?)
                            INSERT INTO report_exclusions (rex_module, rex_field, rex_fieldset) VALUES (?, ?, ?)
                    ', [ $module, $fieldset, $excludedField, $module, $excludedField, $fieldset ]);
                }
            }
            else
            {
                \DatixDBQuery::PDO_query('DELETE FROM report_exclusions WHERE rex_module = ? AND rex_fieldset = ?', [ $module, $fieldset ]);
            }
        }

        AddSessionMessage('INFO', _tk('reporting_fields_save_message'));
    }


}