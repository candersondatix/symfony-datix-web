<script type="text/javascript">
    var previousModule = '<?php echo $this->module_switch?>';
    var saveChangesMessage = '<?php echo _tk('confirm_save_batch_update') ?>';
</script>

<form method="post" action="app.php?action=excludefields">
    <input type="hidden" name="checkbox_<?php echo $this->mainTable ?>" value="on" />
    <div class="clear">
        <div class="new_titlebg">
            <div class="title_text_wrapper"><b>Module:</b></div>
            <div class="title_select_wrapper" style="padding:2px">
                <?php echo $this->moduleSelect ?>
            </div>
        </div>
    <div class="field_label left_div">
        <div class="left_div_content center" style="font-weight:bold;"><?php echo _tk('reporting_fields_available')?></div>
    </div>
    <div class="middle_div">
        <div class="center">
            <div class="clear"></div>
        </div>
    </div>
    <div class="field_label right_div">
        <div class="right_div_content center" style="font-weight:bold;"><?php echo _tk('reporting_fields_excluded')?></div>
    </div>
</div>


    <div class="clear">
        <div class="field_label left_div">
            <div class="left_div_content center">

                <div>
                    <span class="field_label reporting_field_label"><?php echo _tk('field') ?></span>
                    <select class="codefield reporting_field_select" id="0_available" name="0_available[]" multiple="multiple" size="12">
                        <?php foreach ($this->availableFields[0] as $availableField): ?>
                        <option value="<?php echo $availableField->code ?>"><?php echo htmlspecialchars($availableField->description) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="middle_div">
            <div class="center">
                <div class="clear">
                    <div class="reporting_field_button button_top">
                        <input style="width:100%" type="button" value="<?php echo _tk('reporting_fields_exclude_button') ?>" onclick="moveOptions('0_available', '0_excluded');" />
                    </div>
                    <div class="reporting_field_button">
                        <input style="width:100%;" type="button" value="<?php echo _tk('reporting_fields_include_button') ?>" onclick="moveOptions('0_excluded', '0_available');" />
                    </div>
                </div>
            </div>
        </div>
        <div class="right_div">
            <div class="right_div_content center">
                <div>
                    <select class="codefield reporting_field_select" id="0_excluded" name="0_excluded[]" multiple="multiple" size="12">
                        <?php foreach($this->excludedFields[0] as $excludedField): ?>
                        <option value="<?php echo $excludedField->code ?>"><?php echo htmlspecialchars($excludedField->description) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    
<?php foreach($this->fieldsets as $fieldset => $fieldsetName): ?>
    <?php if ($fieldset != 0): ?>
    <div class="clear">
        <div class="section_title_row<?php echo $this->excludedTables[$fieldset] ? ' section-greyed' : ''?>">
            <div style="float:left;width:25%;padding:3px;margin-top:3px;font-weight:bold;"><span style="text-align: center; display: inline-block; vertical-align: middle; margin: 0 15px 0 0; font-weight: normal;">show<br /><input type="checkbox" title="Hide section in reporting" id="checkbox_<?php echo $fieldset?>" class="toggle-section" name="checkbox_<?php echo $fieldset?>"<?php echo ! $this->excludedTables[$fieldset] ? ' checked="checked"' : ''?> /></span><?php echo $fieldsetName ?></div>
            <div style="float:right;width:74%">
                <div class="title_rhs_container" style="padding: 5px;">
                    <img id="twisty_image_name" src="Images/<?php if ($this->expanded[$fieldset] == 1): ?>collapse<?php else: ?>expand<?php endif ?>.gif" alt="+" border="0" style="cursor:pointer;" onclick="javascript:toggleSections('section_<?php echo $fieldset ?>', this);" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="clear" id="section_<?php echo $fieldset ?>"<?php if ($this->expanded[$fieldset] != 1): ?> style="display:none;"<?php endif ?>>
        <div class="field_label left_div">
            <div class="left_div_content center">
                <div>
                    <span class="field_label reporting_field_label"><?php echo _tk('field') ?></span>
                    <select class="codefield reporting_field_select" id="<?php echo $fieldset ?>_available" name="<?php echo $fieldset ?>_available[]" multiple="multiple" size="12">
                        <?php foreach ($this->availableFields[$fieldset] as $availableField): ?>
                        <option value="<?php echo $availableField->code ?>"><?php echo htmlspecialchars($availableField->description) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="middle_div">
            <div class="center">
                <div class="clear">
                    <div class="reporting_field_button button_top">
                        <input style="width:100%" type="button" value="<?php echo _tk('reporting_fields_exclude_button') ?>" onclick="moveOptions('<?php echo $fieldset ?>_available', '<?php echo $fieldset ?>_excluded');" />
                    </div>
                    <div class="reporting_field_button">
                        <input style="width:100%;" type="button" value="<?php echo _tk('reporting_fields_include_button') ?>" onclick="moveOptions('<?php echo $fieldset ?>_excluded', '<?php echo $fieldset ?>_available');" />
                    </div>
                </div>
            </div>
        </div>
        <div class="right_div">
            <div class="right_div_content center">
                <div>
                    <select class="codefield reporting_field_select" id="<?php echo $fieldset ?>_excluded" name="<?php echo $fieldset ?>_excluded[]" multiple="multiple" size="12">
                        <?php foreach($this->excludedFields[$fieldset] as $excludedField): ?>
                        <option value="<?php echo $excludedField->code ?>"><?php echo htmlspecialchars($excludedField->description) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    
    <?php endif ?>
<?php endforeach ?>

    <div class="clear">
        <div class="button_wrapper">
            <input type="button" value="<?php echo _tk('reporting_fields_save_button') ?>" class="button" onclick="selectAllMultiCodes(); this.form.submit();">
            <input type="button" value="<?php echo _tk('reporting_fields_cancel_button') ?>" class="button" onclick="if (confirm('<?php echo _tk('cancel_confirm') ?>')){ SendTo('app.php?action=reportsadminlist', '');}">
        </div>
    </div>
</form>