<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class CodeSetupsTemplateController extends TemplateController
{
    /**
     * @desc displays a list of all coded fields with available setups for each module.
     *
     * @codeCoverageIgnoreStart
     * No unit test, since it deals with constructing HTML
     */
    public function codesetupslist()
    {
        $ModuleGroups = GetModuleGroups();

        if ($this->request->getParameter('module') != '')
        {
            if(in_array($this->request->getParameter('module'), array_keys($ModuleGroups)))
            {
                $module = $this->request->getParameter('module');
            }
            else
            {
                $module = \Sanitize::getModule($this->request->getParameter('module'));
            }
        }
        else
        {
            $module = GetDefaultModule(array('default' => 'INC'));
        }

        $message = \Sanitize::SanitizeString($this->request->getParameter('message'));
        $orderby = ($this->request->getParameter('orderby')) ? \Sanitize::SanitizeString($this->request->getParameter('orderby')) : 'fdr_label';
        $order = ($this->request->getParameter('order') && $this->request->getParameter('order') == 'DESC') ? 'DESC' : 'ASC';
        $NotArray = array('DAS','DST','HSA','ADM','HOT','TOD');
        $ModArray = getModArray($NotArray, true);

        if(!isset($ModArray[$module]))
        {
            $module = null;
        }

        //Check user has setup permissions to available modules, and that requested module is valid.
        foreach ($ModArray as $code => $name)
        {
            if (!HasSetupPermissions($code))
            {
                if (!in_array($code, $NotArray))
                {
                    $NotArray[] = $code;
                }
            }
            elseif (!$module || !HasSetupPermissions($module))
            {
                //if the requested module is not valid, default to the first one in the list.
                $module = $code;
            }
        }

        if (!$module)
        {
            // no code setup permissions.
            fatal_error('You do not have permission to set up codes for any module.');
        }

        $resultArray = $this->getCodeSetupFieldListForModule($module, $orderby, $order);

        // create page
        $this->title = _tk('code_setups');
        $this->module = 'ADM';
        $this->image = 'Images/icons/icon_cog_reports.png';
        $this->hasPadding = false;

        $this->response->build('src/admin/views/CodeSetups.php', array(
            'message' => $message,
            'orderby' => $orderby,
            'order' => $order,
            'module' => $module,
            'NotArray' => $NotArray,
            'resultArray' => $resultArray
        ));
    }

    /**
     * @static
     * @param $module
     * @param string $orderby
     * @param string $order
     * @param bool $RemoveDuplicateCodeTables - Prevents in-module duplicates (e.g inc_unit, con_unit) being removed when setting up tag sets.
     * @param bool $IncludeHardCodedFields - Prevents hard coded fields being made available when setting up tag sets.
     *
     * Gets an array of fields that have user-definable code setups. Also used to identify available fields for
     * linking to tag sets, and in need of a bit of refactoring work.
     *
     * @return array
     */
    public function getCodeSetupFieldListForModule($module, $orderby = 'fdr_label', $order = 'ASC', $RemoveDuplicateCodeTables = true, $IncludeHardCodedFields = true)
    {
        global $scripturl, $FieldDefs;

        $ModuleDefs = $this->registry->getModuleDefs();

        $ModuleGroups = GetModuleGroups();
        $dup_check   = array();

        //if this is a module group, we want to collect all sub-module fields.
        $setup_module_list = array();
        if(in_array($module, array_keys($ModuleGroups)))
        {
            foreach(getModArray() as $subModule => $name)
            {
                if($ModuleDefs[$subModule]['MODULE_GROUP'] == $module)
                {
                    $setup_module_list[] = $subModule;
                }
            }
        }
        else
        {
            $setup_module_list = array($module);
        }

        $setup_module_in = implode("','", $setup_module_list);

        // Currently, the SQL statement below needs to join on field_formats in order to pull out a list of
        // fields per module.  field_formats has some pretty weird looking definitions which means that some
        // fields are appearing under modules that they shouldn't do (e.g. con_type under RAM).  I'm loathe
        // to remove these definitions because I have no idea if any parts of DatixWeb/Rich Client rely on
        // them.  SO...  I've opted for the far messier but safer option of hard-coding a list of fields
        // which need to be excluded from the returned result set below.
        $excludeFields = array(
            'RAM' => array(
                'con_type',
                'link_role',
                'link_age_band',
            ),
            'INC' => array(
                'con_religion',
                'con_sex_orientation',
            ),
            'SAB' => array(
                'link_role',
            ),
            'LIB' => array(
                'link_role',
            ),
            'COM' => array(
                'link_role',
            ),
            'CLA' => array(
                'link_role',
                'con_disability',
            ),
            'PAL' => array(
                'link_role',
                'con_disability',
            ),
            'STD' => array(
                'link_role',
            ),
            'CON' => array(
                'con_hier_location'
            ),
            'ACR' => array(
                'loc_type'
            )
        );

        //Still need to link to field_formats for module information until we have an alternative method.
        $sql = "
            SELECT
                fdr_label,
                fdr_name,
                fdr_code_table,
                fdr_table,
                fdr_setup_condition,
                lcf.locked
            FROM
                field_directory
            JOIN
                field_formats ON fdr_name = fmt_field AND fmt_module in ('$setup_module_in')
            LEFT JOIN
                locked_code_fields lcf ON fdr_name = lcf.code_field AND fdr_table = lcf.code_table
            WHERE
                (fdr_table = fmt_table ".($module != 'POL' ? " OR (SELECT COUNT(*) FROM field_formats where fmt_field = fdr_name AND fmt_module IN ('$setup_module_in')) = 1 AND fdr_table != 'staff'" : '').")
                AND fdr_data_type = 'C' AND fdr_setup = 'Y'
                AND fdr_table not in (SELECT sfm_form FROM subforms WHERE sfm_form = sfm_tables AND sfm_module NOT IN ('$setup_module_in'))
        ";

        if (isset($excludeFields[$module]))
        {
            $sql.= " AND fdr_name NOT IN ('".implode("','", $excludeFields[$module])."')";
        }

        $sql.= " ORDER BY $orderby $order";

        $resultArray = \DatixDBQuery::PDO_fetch_all($sql);

        require_once 'Source/setups/CodeSetups.php';

        if($IncludeHardCodedFields)
        {
            foreach (getHardCodedFields($module) as $code)
            {
                $resultArray[] = $code;
            }
        }

        //Need to check for relabelling re-assign before sorting.
        foreach ($resultArray as $index => $row)
        {
            $resultArray[$index]['fdr_label'] = \Labels_FieldLabel::GetFieldLabel($row['fdr_name'], $row['fdr_label'], $row['fdr_table'], '');
        }

        $resultArray = sortCodeFieldListing($resultArray, $orderby, $order);

        $sysFieldsGroups = \Setups_CodeSetup::getSystemWideFieldsList();
        $lockedSysFields = CodeSetupsTemplateController::getLockedSystemFields();
        
        foreach($resultArray as $index => $row)
        {
            // Is this a system wide field?
            foreach ($sysFieldsGroups as $sysField => $childFields) 
            {
                if (in_array ($row['fdr_name'], $childFields)) 
                {
                    // If so, is it locked?
                    if (in_array ($sysField, $lockedSysFields)) {
                        $resultArray[$index]['locked'] = '1';
                    }
                }
            }
            
            //Check if a global controls access to a setup for a field and if so, check to relevant global value.
            if (!empty($row['fdr_setup_condition']))
            {
                $GlobalCondition = explode('=', $row['fdr_setup_condition']);

                if (GetParm($GlobalCondition[0], '') != $GlobalCondition[1])
                {
                    unset($resultArray[$index]);
                    continue;
                }
            }

            if($RemoveDuplicateCodeTables)
            {
                // Check and remove duplicates
                if (($dup_check[$row['fdr_name']] == $row['fdr_code_table']) || (in_array($row['fdr_code_table'], $dup_check)))
                {
                    //location codes act weirdly for contacts and incidents because of duplications in fformats:
                    //Awful workaround here:
                    if(isset($FieldDefs[$module][$row['fdr_name']]))
                    {
                        $duplicateField = array_search($row['fdr_code_table'], $dup_check);
                        unset($dup_check[$duplicateField]);
                        unset($resultArray[$dup_check_indexes[$duplicateField]]);
                        unset($dup_check_indexes[$duplicateField]);
                    }
                    else
                    {
                        unset($resultArray[$index]);
                        continue;
                    }
                }

                $dup_check[$row['fdr_name']] = $row['fdr_code_table'];
                $dup_check_indexes[$row['fdr_name']] = $index;
            }

            // need to use full URL because of the permissions screen generic redirection
            if (!isset($resultArray[$index]['url']))
            {
                $resultArray[$index]['url'] = $scripturl . '?action=codesetupsaction&amp;code=' . $row['fdr_code_table'] . '&amp;module=' . $module . '&amp;codefieldname=' . $row['fdr_name'] . '&fdr_table=' . $row['fdr_table'];
            }
        }

        return $resultArray;
    }


    public function getLockedSystemFields()
    {
        if (!IsCentrallyAdminSys())
        {
            return array();
        }
        
        $list = \DatixDBQuery::PDO_fetch_all('SELECT code_field FROM locked_code_fields WHERE code_field LIKE \'!%\'');

        $result = array ();
        foreach ($list as $row) {
            $result[] = $row['code_field'];
        }

        return $result; 
    }
}