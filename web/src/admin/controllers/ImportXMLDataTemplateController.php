<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ImportXMLDataTemplateController extends TemplateController
{
    function import_form($error = '')
    {
        $this->title = 'Import data';
        $this->module = 'ADM';
        $this->hasPadding = false;

        $CTable = new \FormTable();
        $FieldObj = new \FormField();
        $CTable->MakeTitleRow('<b>Import XML data</b>');

        if ($error)
        {
            $CTable->Contents.= '<tr><td class="windowbg2" colspan="2"><font color="red"><b>' . $error["message"] . '</b></font></td></tr>';
        }

        $UploadField = '<input name="userfile" type="file"  size="50"/>';
        $CTable->MakeRow('<img src="images/Warning.gif" alt="Warning"> <b>Import XML data from this file</b>', $FieldObj->MakeCustomField($UploadField));

        $CTable->MakeTable();

        $this->response->build('src/admin/views/ShowImportXMLDataForm.php', array(
            'CTable' => $CTable,
            'data' => $data
        ));
    }
}