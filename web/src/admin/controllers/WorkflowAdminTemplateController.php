<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class WorkflowAdminTemplateController extends TemplateController
{
    function workflowadmin()
    {
        global $ModuleListOrder;

        $this->addCss('js_functions/colorpicker/css/colorpicker.css');

        $this->addJs(array(
                'js_functions/colorpicker/js/colorpicker.js',
                "<script type=\"text/javascript\">
                    jQuery(function() {

                    jQuery('.cp').ColorPicker({
                        flat: false,
                        onSubmit: function(hsb, hex, rgb, el) {
                            saveID = el.id.replace('_display', '');

                            jQuery('#'+saveID).val(hex);
                            jQuery(el).ColorPickerHide();
                            jQuery(el).css({backgroundColor : '#'+hex});
                        },
                        onBeforeShow: function () {
                            jQuery(this).ColorPickerSetColor(this.value);
                        }
                        })
                        .bind('keyup', function() {
                            jQuery(this).ColorPickerSetColor(this.value);
                    });
                });
                </script>"
            )
        );
        $ModuleDefs = $this->registry->getModuleDefs();

        $ApprovalStatusCodes = $this->GetListOfApprovalStatusCodes();

        unset($ApprovalStatusCodes['AMO']);
        unset($ApprovalStatusCodes['ATI']);

        foreach($ModuleListOrder as $Module)
        {
            if(isset($ApprovalStatusCodes[$Module]))
            {
                $PanelArray[$Module] = array('Label' => _t($ModuleDefs[$Module]['NAME']));

                if (!$CurrentModule)
                {
                    $CurrentModule = $Module;
                }
            }
        }

        $this->title = _tk('workflow-administration');
        $this->module = 'ADM';
        $this->menuParameters = $this->getMenuArray($PanelArray, $CurrentModule);

        $this->response->build('src/admin/views/WorkflowAdmin.php', array(
            'ApprovalStatusCodes' => $ApprovalStatusCodes,
            'CurrentModule' => $CurrentModule
        ));
    }

    /**
     * Defines and returns the global menu configuration for this controller.
     *
     * @return array
     */
    protected function getMenuArray($PanelArray, $CurrentModule)
    {
        return array(
            'panels' => $PanelArray,
            'current_panel' => $CurrentModule,
            'floating_menu' => false
        );
    }

    protected function GetListOfApprovalStatusCodes()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        foreach (getModArray() as $Module => $Name)
        {
            if ($ModuleDefs[$Module]['USES_APPROVAL_STATUSES'])
            {
                $Modules[] = $Module;
            }
        }

        $sql = '
            SELECT
                code,
                module,
                cod_web_colour
            FROM
                code_approval_status
            WHERE
                module IN (\''.implode('\', \'', $Modules).'\')
            ORDER BY
                cod_listorder
        ';

        $Codes = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($Codes as $Details)
        {
            $ApprovalStatusCodes[$Details['module']][$Details['code']] = array('label' => GetApprovalStatusDescription($Details['module'], $Details['code']), 'colour' => $Details['cod_web_colour']);
        }

        return $ApprovalStatusCodes;
    }

    function workflowadminsave()
    {
        $ApprovalStatusCodes = $this->GetListOfApprovalStatusCodes();

        foreach($ApprovalStatusCodes as $Module => $Codes)
        {
            foreach($Codes as $Code => $Details)
            {
                if(isset($_POST[$Module . '_' . $Code]))
                {
                    $UpdateArray[] = array('module' => $Module, 'code' => $Code, 'colour' => $_POST[$Module.'_'.$Code]);
                }
            }
        }

        \DatixDBQuery::PDO_query('UPDATE code_approval_status SET cod_colour = NULL');

        foreach($UpdateArray as $UpdateDetails)
        {
            \DatixDBQuery::PDO_query('UPDATE code_approval_status SET cod_web_colour = :colour WHERE module = :module and code = :code', $UpdateDetails);
        }

        AddSessionMessage('INFO', 'Saved');

        $this->redirect('app.php?action=workflowadmin');
    }
}