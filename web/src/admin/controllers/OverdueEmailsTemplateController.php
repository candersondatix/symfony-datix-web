<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class OverdueEmailsTemplateController extends TemplateController
{
    /**
     * Sends overdue reminder emails based on configured approval statuses
     */
    function overdueemails()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $this->title = _tk('overdue_email_title');
        $this->module = 'ADM';
        $this->hasPadding = false;

        $overdueModule = $this->request->getParameter('module') ?: 'INC';

        $this->title = 'Form Design';
        $this->module = 'ADM';
        $this->menuParameters = array(
            'panels' => array('INC' => array('Label' => _tk('mod_incidents_title')), 'COM' => array('Label' => _tk('mod_complaints_title'))),
            'current_panel' => $overdueModule,
            'floating_menu' => false
        );

        $Settings = GetParms('', false);

        $ALLTable['INC'] = new \FormTable();

        $field = \Forms_SelectFieldFactory::createSelectField('OVERDUE_EMAIL_STATUS', 'ADM', $Settings["OVERDUE_EMAIL_STATUS"], '', true);
        $field->setCustomCodes(getOverdueStatuses(array('module' => 'INC', 'db_only' => true)));
        $field->setIgnoreMaxLength();
        $ALLTable['INC']->MakeRow('<b>Statuses for overdue e-mail notifications</b><br />
            Select the statuses for which overdue email notifications will be sent.',
            $field);

        $field = \Forms_SelectFieldFactory::createSelectField('OVERDUE_EMAIL_USERS', 'ADM', $Settings["OVERDUE_EMAIL_USERS"], '', true);
        $field->setCustomCodes(array('MGR' => 'Incident Manager', 'HDLR' => 'Incident Handler', 'INV' => 'Investigators'));
        $field->setSuppressCodeDisplay();
        $field->setIgnoreMaxLength();
        $ALLTable['INC']->MakeRow('<b>Who will receive overdue email notifications?</b><br />
            Select the individuals involved in the '._tk('INCName').' who will receive overdue email notifications.',
            $field);

        $ALLTable['INC']->MakeTable();

        //Same for complaints. If we need to do this again, this will need to be made generic.
        $ALLTable['COM'] = new \FormTable();

        $field = \Forms_SelectFieldFactory::createSelectField('OVERDUE_EMAIL_STATUS_COM', 'ADM', $Settings["OVERDUE_EMAIL_STATUS_COM"], '', true);
        $field->setCustomCodes(getOverdueStatuses(array('module' => 'INC', 'db_only' => true)));

        $overdueStatuses = array();
        foreach ($ModuleDefs['COM']['HARD_CODED_LISTINGS'] as $key => $listingDef)
        {
            if (isset($listingDef['OverdueWhere']))
            {
                $overdueStatuses[$key] = $listingDef['Title'];
            }
        }
        $field->setCustomCodes($overdueStatuses);

        $field->setIgnoreMaxLength();
        $ALLTable['COM']->MakeRow('<b>Statuses for overdue e-mail notifications</b><br />
            Select the statuses for which overdue email notifications will be sent.',
            $field);

        $field = \Forms_SelectFieldFactory::createSelectField('OVERDUE_EMAIL_USERS_COM', 'ADM', $Settings["OVERDUE_EMAIL_USERS_COM"], '', true);

        $field->setCustomCodes(array('MGR' => 'Complaint Manager', 'HDLR' => 'Complaint Handler', 'INV' => 'Investigators'));

        $field->setSuppressCodeDisplay();
        $field->setIgnoreMaxLength();
        $ALLTable['COM']->MakeRow('<b>Who will receive overdue email notifications?</b><br />
            Select the individuals involved in the '._tk('COMName').' who will receive overdue email notifications.',
            $field);

        $ALLTable['COM']->MakeTable();

        $this->response->build('src/admin/views/OverdueEmails.php', array(
            'overdueModule' => $overdueModule,
            'ALLTable' => $ALLTable
        ));
    }
}