<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class LockedOutUsersController extends Controller
{
    /**
     * Unlocks a set of users
     * Expects an array of record ids to be passed as an array via request parameter
     *
     * @return void
     */
    public function unlockUsers()
    {
        $UserRecords = $this->request->getParameter('records');

        if (isset($UserRecords) && !empty($UserRecords))
        {
            $clean = array();

            foreach ($UserRecords as $UserRecordID)
            {
                $UserRecordsClean[] = (int) $UserRecordID;
            }

            $result = $this->unlockUserRecords($UserRecordsClean);

            if (!$result)
            {
                AddSessionMessage('INFO', _tk('user-cant-be-unlocked'));
            }
            else
            {
                AddSessionMessage('INFO', _tk('user-unlocked'));
            }
        }

        $this->redirect('app.php?action=locked_users');
    }


    /**
     * Performs database query for unlocking one or more records
     *
     * @param array $records The set of records to unlock
     * @return boolean The success of the database query
     */
    protected function unlockUserRecords(array $records)
    {
        $sql = 'UPDATE staff
				SET lockout = \'N\', login_tries = 0, sta_last_login = NULL, sta_lockout_dt = NULL, sta_lockout_reason = NULL
				WHERE recordid = ?';

        $result = false;
        $recordCount = count($records) - 1; // already accounted for the first one in the initial query

        $sql .= str_repeat(' OR recordid = ?', $recordCount);

        $result = \DatixDBQuery::PDO_query($sql, $records);

        return $result;
    }

}