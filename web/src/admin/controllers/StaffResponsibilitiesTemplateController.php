<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;

class StaffResponsibilitiesTemplateController extends TemplateController
{
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'INC';
        $this->hasPadding = false;

        parent::__construct($request, $response);
    }

    public function staffResponsibilities()
	{
        $this->title = _tk('staff_responsibility_title');
        $this->module = 'INC';
        $this->image = 'Images/icons/icon_INC_list.png';

        $PrintMode = ($this->request->getParameter('print') == '1');

        if(!$PrintMode)
        {
            $this->hasMenu = true;
        }

        // Get the listing configuration file to get columns to display and their settings
        $ListingDesign = new \Listings_ModuleListingDesign(array('module' => 'INC'));
        $ListingDesign->LoadColumnsFromDB();

        foreach($ListingDesign->Columns as $col)
        {
            $selectfields[$col->getName()] = $col->getName();
            $list_columns[$col->getName()] = $col->getName();
        }

        if(is_array($selectfields))
        {
            if (!in_array('recordid', $selectfields))
            {
                $selectfields[] = 'recordid';
            }

            $selectfields = implode(", ", $selectfields);
        }
        else
        {
            $selectfields = "recordid";
        }

        $_SESSION["INC"]["staffrespresponse"]["selectfields"] = $selectfields;
        $_SESSION["INC"]["staffrespresponse"]["list_columns"] = $list_columns;

        $DifPerms = GetParm("DIF_PERMS");

        // Filter staff for the listing by the logged in user's where clause
        $con_where = MakeSecurityWhereClause("", "CON", "", "", "");

        $today = getdate();
        $todayStr = "$today[year]-$today[mon]-$today[mday] 00:00:00";

        // Get staff with DIF2 or RM rights.
        $sql = "
            SELECT initials, login, con_surname, con_forenames, con_title FROM contacts_main
            WHERE initials is not null AND initials != '' AND
            (sta_daccessend = '' OR sta_daccessend IS NULL OR sta_daccessend >= '$todayStr')";

        if($con_where != "")
            $sql .= " AND " . $con_where;

        $sql .= "
            AND
            (
                initials IN
                (
                    SELECT contacts_main.initials
                    FROM sec_staff_group JOIN contacts_main
                    ON sec_staff_group.con_id = contacts_main.recordid
                    WHERE sec_staff_group.grp_id IN (
                        select grp_id from sec_group_permissions
                        where item_code = 'DIF_PERMS'
                        and (perm_value = 'RM' or perm_value = 'DIF2')
                    )
                )
                OR
                initials IN
                (
                    SELECT staff.initials
                    FROM staff, user_parms
                    WHERE staff.login = user_parms.login
                    AND user_parms.parameter = 'DIF_PERMS'
                    AND (user_parms.parmvalue = 'DIF2' OR user_parms.parmvalue = 'RM')
                )
            )
            ORDER BY con_surname";

        $MatchingStaff = array();

        $UserDetails = \DatixDBQuery::PDO_fetch_all($sql);
        foreach ($UserDetails as $row)
        {
            $MatchingStaff[$row['initials']] = "$row[con_surname], $row[con_forenames] $row[con_title]";
        }

        $this->response->build('src/admin/views/StaffResponsibilities.php',
            array(
                'MatchingStaff' => $MatchingStaff,
                'PrintMode' => $PrintMode,

            ));
	}
}