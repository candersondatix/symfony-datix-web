<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;
use src\codetaggroups\model\CodeTagGroupModelFactory;


/**
 * Controls the display of user interfaces to manage groups of tags that can be attached to codes
 */
class CodeTagGroupTemplateController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);

        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        $this->menuParameters = array(
            'menu_array' => array(
                array('label' => 'New '._tk('tag_set'), 'link' => 'action=edittaggroup'),
            )
        );
    }
	
    /**
     * Constructs a list of existing tag sets.
     */
	public function listTagGroups()
	{
        $this->hasPadding = false;
        $this->title = _tk('tag_sets_title');
        
        $factory   = new CodeTagGroupModelFactory();
        $query     = $factory->getQueryFactory()->getQuery();
        
        $TagGroups = $factory->getCollection();
        $TagGroups->setQuery($query->orderBy(array('code_tag_groups.name')));

        $TagGroupRecordList = new \RecordLists_RecordList();
        $TagGroupRecordList->AddRecordData($TagGroups);

        $TagGroupListingDesign = new \Listings_ListingDesign(
            array('columns' => array(
                new \Fields_DummyField(array('name' => 'name', 'label' => 'Name', 'type' => 'S')),
                new \Fields_DummyField(array('name' => 'description', 'label' => 'Description', 'type' => 'S'))
            )));

        $TagGroupListingDisplay = new \Listings_ListingDisplay($TagGroupRecordList, $TagGroupListingDesign);
        $TagGroupListingDisplay->Action = 'edittaggroup';

        $this->response->build('src/admin/views/ListTagGroups.php', array(
            'listing' => $TagGroupListingDisplay,
		));
	}

    /**
     * Displays a form to allow tag set information to be edited and individual tags to be created under it.
     */
	public function editTagGroup()
	{
        global $scripturl;
        
        $ModuleDefs = $this->registry->getModuleDefs();

        $TagGroupID      = $this->request->getParameter('recordid');
        $TagGroupFactory = new CodeTagGroupModelFactory();

        if (!isset($TagGroupID))
        {
            // new record - instantiate an empty CodeTagGroup object
            $TagGroup = $TagGroupFactory->getEntityFactory()->createObject();
        }
        else
        {
            // edit record - fetch the existing CodeTagGroup object
            $TagGroupMapper = $TagGroupFactory->getMapper();
            $TagGroup      = $TagGroupMapper->find($TagGroupID);
            if (!isset($TagGroup))
            {
                throw new \URLNotFoundException();
            }
        }

        //get current linked fields
        $LinkedFields = \DatixDBQuery::PDO_fetch_all('SELECT field FROM code_tag_linked_fields WHERE [group] = :group', array('group' => $TagGroupID), \PDO::FETCH_COLUMN);

        $Loader = new \src\framework\controller\Loader();
        $CodeSetupsTemplateController = $Loader->getController(array('controller' => 'src\admin\controllers\CodeSetupsTemplateController'));

        //determine set of possible linked fields
        foreach(getModArray(array('DAS','DST','HSA','ADM','HOT','TOD','CQO','CQP','CQS','ATM','ATQ','ATI','AMO','AQU')) as $Module => $Name)
        {
            foreach($CodeSetupsTemplateController->getCodeSetupFieldListForModule($Module, 'fdr_label', 'ASC', false, false) as $FieldDetails)
            {
                $LinkedFieldArray[$FieldDetails['fdr_name']] = \Labels_FieldLabel::GetFieldLabel($FieldDetails['fdr_name']).' ('.$ModuleDefs[$Module]['NAME'].')';
            }
        }

        $this->title = (isset($TagGroupID) ? _tk('edit_tag_set') : _tk('new_tag_set'));

        $formArray = array(
            'tag_set' => array(
                'Title' => _tk('tag_set_details'),
                'Rows' => array(
                    array('Name' => 'recordid', 'Title' => 'Group ID', 'ReadOnly' => true, 'Type' => 'string', 'Condition' => isset($TagGroupID)),
                    array('Name' => 'name', 'Title' => 'Name', 'Type' => 'string', 'Width' => 50, 'MaxLength' => 50),
                    array('Name' => 'description', 'Title' => 'Description', 'Type' => 'textarea', 'Rows' => 3, 'Columns' => 70),
                )
            ),
            'linkedfields' => array(
                'Title' => _tk('tag_set_linked_fields'),
                'Rows' => array(
                    array('Name' => 'linked_fields', 'Title' => 'Fields', 'Type' => 'multilistbox', 'CustomCodes' => $LinkedFieldArray, 'IgnoreMaxLength' => true),
                )
            ),
            'tags' => array(
                'Title' => _tk('tag_list'),
                'Condition' => isset($TagGroupID),
                'ControllerAction' => array(
                    'listlinkedtags' => array(
                        'controller' => 'src\\admin\\controllers\\CodeTagGroupController')),
                'Rows' => array()
            )
        );

        //set up dummy form design object to mark certain fields as mandatory
        $formDesign = new \Forms_FormDesign();
        $formDesign->MandatoryFields = array(
            'name' => 'tag_set',
            'description'   => 'tag_set',
        );

        //set up form data array to be consumed by FormTable class
        $data = array(
            'recordid'      => $TagGroupID,
            'name'          => $TagGroup->name,
            'description'   => $TagGroup->description,
            'linked_fields' => implode(' ', $LinkedFields),
        );

        $form = new \FormTable($mode, 'ADM', $formDesign);
        $form->MakeForm($formArray, $data, 'ADM');
        $form->MakeTable();

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'onclick' => 'selectAllMultiCodes();submitClicked=true;if(validateOnSubmit()){document.frmTagSet.submit()}', 'action' => 'SAVE'));
        $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'SendTo(\''.$scripturl.'?action=listtaggroups\');', 'action' => 'CANCEL'));

        if(isset($TagGroupID))
        {
            $ButtonGroup->AddButton(array('id' => 'btnDelete', 'name' => 'btnDelete', 'label' => _tk('btn_delete_tag_set'), 'onclick' => 'deleteTagSet()', 'action' => 'DELETE'));
        }

        $this->menuParameters = array(
            'table' => $form,
            'buttons' => $ButtonGroup,
            'no_audit' => true,
            'no_print' => true
        );

        $this->response->build('src/admin/views/EditTagGroup.php', array(
            'data' => $data,
            'form' => $form,
            'form_design' => $formDesign,
            'ButtonGroup' => $ButtonGroup,
		));
	}
}