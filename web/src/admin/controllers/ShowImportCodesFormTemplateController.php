<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ShowImportCodesFormTemplateController extends TemplateController
{
    /**
     * Displays Import options for code setups
     */
    function importcodesform()
    {
        global $FieldDefsExtra;

        require_once 'Source/setups/ImportCodes.php';

        $Module    = \Sanitize::getModule($this->request->getParameter('module'));
        $FieldName = \Sanitize::SanitizeString($this->request->getParameter('fieldname'));
        $Table     = \Sanitize::SanitizeString($this->request->getParameter('table'));
        $error     = $this->request->getParameter('error');
        $PreviousModule = $Module;

        // Special case for Accreditation modules
        if ($Module == 'ACR')
        {
            $PreviousModule = 'ACR';
            $Module = getAccreditationModule($FieldDefsExtra, $FieldName);
        }

        if (empty($FieldDefsExtra[$Module][$FieldName]) || HasSetupPermissions($Module) === false)
        {
            $this->redirect('app.php');
        }

        if ($PreviousModule == 'ACR')
        {
            $Module = 'ACR';
        }

        $this->title      = _tk('import_code_setups') . ' - ' . GetFieldLabel($this->request->getParameter('fieldname'));
        $this->module     = 'ADM';
        $this->hasPadding = false;

        $CTable   = new \FormTable();
        $FieldObj = new \FormField();

        $CTable->MakeTitleRow('<b>' . _tk('import_code_setups') . ' XML data</b>');

        if ($error)
        {
            $CTable->Contents .= '<tr><td class="windowbg2" colspan="2"><font color="red"><b>' . $error['message'] . '</b></font></td></tr>';
        }

        $FieldObj->MakeRadioButtons('import_action', 'APPEND', '', false, '', '', array('APPEND' => 'Append only new codes from load file.',
            'UPDATE' => 'Update existing codes matching those in the load file and append any new codes.',
            'OVERWRITE' => 'Overwrite all existing codes with those contained within the load file.'));
        $CTable->MakeRow("<b>" . _tk('import_options') . "</b>", $FieldObj);

        $UploadField  = '<input name="userfile" type="file"  size="50"/>';
        $CTable->MakeRow('<img src="images/Warning.gif" alt="Warning"> <b>Import code setup XML data from this file</b>', $FieldObj->MakeCustomField($UploadField));

        $CTable->MakeTable();

        $this->response->build('src/admin/views/ShowImportCodesForm.php', array(
            'Module' => $Module,
            'FieldName' => $FieldName,
            'Table' => $Table,
            'error' => $error,
            'CTable' => $CTable
        ));
    }
}