<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\users\model\UserModelFactory;

class ListUsersTemplateController extends TemplateController
{
    function listusers()
    {
        global $FieldDefsExtra;

        $this->title = _tk('user_list_title');
        $this->module = 'ADM';
        $this->hasPadding = false;
        $this->menuParameters = array(
            'menu_array' => array(array('label' => 'Add new User', 'link' => 'action=edituser'))
        );
        
        $activeOnly = $this->request->getParameter('active_only');
        if (isset($activeOnly))
        {
            $_SESSION['list_active_users_only'] = (bool) $activeOnly;
        }

        $this->menuParameters['menu_array'][] = array('label' => 'Search for user', 'link' => 'action=listusers');

        /**
         * If we have access to the temporary user search, then we don't want to load a letter-based list unless it
         * has been explicitly clicked.
         */

        if ($this->request->getParameter('match') == '' && $this->request->getParameter('usersearch') == '')
        {
            $this->response->build('src/admin/views/ListUsersSearch.php', array('usersearch' => $this->request->getParameter('usersearch')));
        }
        else
        {
            if ($this->request->getParameter('orderby'))
            {
                $MatchField = \Sanitize::SanitizeString($this->request->getParameter('orderby'));
            }

            if ($MatchField == '')
            {
                $MatchField = 'sta_surname';
            }

            $MatchField = EscapeQuotes($MatchField);

            if ($this->request->getParameter('JumpToName'))
            {
                $MatchLetter = \UnicodeString::strtoupper($this->request->getParameter('JumpToName'));
            }

            $MatchFieldData = \Sanitize::SanitizeString($this->request->getParameter($MatchField));

            if ($MatchFieldData)
            {
                $MatchLetter = \UnicodeString::strtoupper($MatchFieldData{0});
            }
            elseif ($this->request->getParameter('match'))
            {
                $MatchLetter = \Sanitize::SanitizeString($this->request->getParameter('match'));
            }

            if ($MatchLetter != 'all' && ($MatchLetter < 'A' || $MatchLetter > 'Z'))
            {
                $MatchLetter = 'A';
            }

            $MatchLetter = EscapeQuotes($MatchLetter);

            //Cannot sort on these columns Alphabetically, so revert to list all.
            if ($MatchField == 'logins' || $MatchField == 'last_date' || $MatchField == 'recordid')
            {
                $MatchLetter = 'all';
            }

            // get sort order default to fullname
            $orderby = $_GET["orderby"];

            if (!$orderby)
            {
                $orderby = 'sta_surname';
            }

            $sorttype = \Sanitize::SanitizeString($this->request->getParameter('order'));

            if (\UnicodeString::strtolower($sorttype) != 'desc')
            {
                $sorttype = 'asc';
            }

            $ListingDesign = \Listings_ModuleListingDesignFactory::getListingDesign(array('module' => 'ADM'));
            $ListingDesign->LoadColumnsFromDB();

        // Remove UDF columns
        // TODO: Once UDFs are working again in the admin module we can remove this.
        foreach ($ListingDesign->Columns as $key => $column)
        {
            if ($column instanceof \Listings_UDFListingColumn)
            {
                $udfColumns[] = $column;
                unset($ListingDesign->Columns[$key]);
            }
        }

            $ListableCols = $ListingDesign->getColumns();

            //The sql used for this listing involves subqueries with grouping, which is a bit too complex for the current sqlwriter,
            //so we just push the constructed sql in ourselves.
            $sql = 'SELECT '.implode(', ', $ListableCols).', ISNULL(logins, 0) as logins, last_date
                FROM staff LEFT OUTER JOIN (SELECT lau_login, count(*) as logins, max(lau_date) AS last_date
                                FROM login_audit
                                WHERE lau_description LIKE \'WEB % User logged in\'
                                GROUP BY lau_login) login_count
                            ON staff.login = login_count.lau_login
                ';

            $ListingDesign->AddAdditionalColumns(
                array(
                    new \Fields_DummyField(array('name' => 'logins', 'label' => 'Logins', 'type' => 'N', 'width' => 4)),
                    new \Fields_DummyField(array('name' => 'last_date', 'label' => 'Last login', 'type' => 'D', 'width' => 8)),
                )
            );

            $Where[] = '(login_tries != -1 or login_tries IS NULL)';

            $sqlParameters = array();

            if ($this->request->getParameter('usersearch') != '')
            {
                if ($this->request->getParameter('login') != '')
                {
                    $Where[] = "login LIKE :login";
                    $sqlParameters['login'] = '%'.$this->request->getParameter('login').'%';
                }
                if ($this->request->getParameter('con_surname') != '')
                {
                    $Where[] = "con_surname LIKE :surname";
                    $sqlParameters['surname'] = '%'.$this->request->getParameter('con_surname').'%';
                }
            }
            else
            {
                if ($MatchLetter != 'all' && $MatchField != 'logins')
                {
                    $Where[] = "$MatchField LIKE '$MatchLetter%'";
                }
            }

        if ($_SESSION['list_active_users_only'] === true)
        {
            // I'm defining "active" here as within access start/end dates (if defined).
            // There is no consistent definition for an active user across the system, so this may well change.
            $Where[] = '((sta_daccessstart IS NULL OR sta_daccessstart <= GETDATE()) AND (sta_daccessend IS NULL OR sta_daccessend > GETDATE()))';
            $Where[] = '(sta_dclosed IS NULL OR sta_dclosed > GETDATE())';
            $Where[] = '(rep_approved = \'FA\')';
        }

            $WhereClause = MakeSecurityWhereClause($Where,'ADM');

            $sql .= 'WHERE '.$WhereClause.' ORDER BY '.$MatchField.' '.$sorttype;

            $users = \DatixDBQuery::PDO_fetch_all($sql, $sqlParameters);

            $RecordList = new \RecordLists_ModuleRecordList();
            $RecordList->Module = 'ADM';
            $RecordList->AddRecordData($users);

            $ListingDisplay = new \Listings_ListingDisplay($RecordList, $ListingDesign);
            $ListingDisplay->setOrder(($this->request->getParameter('orderby')) ? $this->request->getParameter('orderby') : 'recordid');
            $ListingDisplay->setDirection(($this->request->getParameter('order')) ? $this->request->getParameter('order') : 'ASC');
            $ListingDisplay->setSortAction(\Sanitize::SanitizeURL($this->request->getParameter('action').'&match='.$MatchLetter));


            if ($this->request->getParameter('usersearch'))
            {
                if ($_SESSION['list_active_users_only'] === true)
                {
                    $this->title .= _tk('active_users_only_title');
                    $this->menuParameters['extra_links'] = array(
                        array(
                            'label' => _tk('list_all_users'),
                            'onclick' => 'jQuery(\'#usersearchform\').attr(\'action\',\'app.php?action=listusers&usersearch=1&active_only=0\');jQuery(\'#usersearchform\').submit()'
                        ));
                }
                else
                {
                    $this->menuParameters['extra_links'] = array(
                        array(
                            'label' => _tk('list_active_users_only'),
                            'onclick' => 'jQuery(\'#usersearchform\').attr(\'action\',\'app.php?action=listusers&usersearch=1&active_only=1\');jQuery(\'#usersearchform\').submit()'
                        ));
                }
            }
            else
            {
                if ($_SESSION['list_active_users_only'] === true)
                {
                    $this->title .= _tk('active_users_only_title');
                    $this->menuParameters['extra_links'] = array(array('label' => _tk('list_all_users'), 'link' => 'action=listusers&active_only=0&match='.$MatchLetter));
                }
                else
                {
                    $this->menuParameters['extra_links'] = array(array('label' => _tk('list_active_users_only'), 'link' => 'action=listusers&active_only=1&match='.$MatchLetter));
                }
            }

            require_once 'Source/libs/LDAP.php';
            $ldap = new \datixLDAP();

            $numLDAPUsers = \DatixDBQuery::PDO_fetch('SELECT count(*) from sec_ldap_all', array(), \PDO::FETCH_COLUMN);

            $this->response->build('src/admin/views/ListUsers.php', array(
                'MatchField' => $MatchField,
                'sorttype' => $sorttype,
                'MatchLetter' => $MatchLetter,
                'FieldDefsExtra' => $FieldDefsExtra,
                'ListingDisplay' => $ListingDisplay,
                'ldap' => $ldap,
                'numLDAPUsers' => $numLDAPUsers,
                'login' => $this->request->getParameter('login'),
                'con_surname' => $this->request->getParameter('con_surname'),
                'usersearch' => $this->request->getParameter('usersearch')
            ));
        }
    }
}