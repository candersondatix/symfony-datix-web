<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class LockCodeSetupController extends Controller
{
    function lockcodesetup()
    {
        $fieldname  = $this->request->getParameter('fieldname');
        $fieldtable = $this->request->getParameter('table');
        $module     = $this->request->getParameter('module');

        $CodeSetup = new \Setups_CodeSetup($fieldname, $fieldtable);
        $CodeSetup->useSystemWideFieldNames();
        $CodeSetup->lockCodeSetup();

        $Parameters = array(
            'module' => $module
        );

        $this->call('src\admin\controllers\CodeSetupsTemplateController', 'codesetupslist', $Parameters);
    }
}