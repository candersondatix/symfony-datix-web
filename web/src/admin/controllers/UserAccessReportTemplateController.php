<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class UserAccessReportTemplateController extends TemplateController
{
    function usergroupreport()
    {
        $module = \Sanitize::getModule($this->request->getParameter('module'));

        $this->title = _tk('user_access_report');
        $this->module = 'ADM';
        $this->hasPadding = false;

        if ($this->request->getMethod() == 'GET' && $this->request->getParameter('print') == 1)
        {
            $PrintMode = true;
        }

        if (!$module)
        {
            $module = 'INC';
        }

        $ModuleDropDownHTML = getModuleDropdown(
            array(
                'name' => 'lbModule',
                'onchange' => 'showColumns(this);document.editcolmodule.submit()',
                'current' => $module
            )
        );

        $sql = "
            SELECT
                recordid,
                grp_code,
                grp_description
            FROM
                sec_groups
            WHERE
                (grp_type = 1 OR grp_type = 3)
        ";

        $result2 = \DatixDBQuery::PDO_fetch_all($sql, array());

        $MatchingStaff = array();

        foreach ($result2 as $row)
        {
            $MatchingStaff[] = $row["recordid"];
            $MatchingStaffName[$row["recordid"]][0] = "$row[grp_code]";
            $MatchingStaffName[$row["recordid"]][1] = "$row[grp_description]";
        }

        $this->response->build('src/admin/views/UserAccessReport.php', array(
            'module' => $module,
            'ModuleDropDownHTML' => $ModuleDropDownHTML,
            'MatchingStaff' => $MatchingStaff,
            'PrintMode' => $PrintMode,
            'MatchingStaffName' => $MatchingStaffName
        ));
    }
}