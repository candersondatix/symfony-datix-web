<?php
namespace src\admin\controllers;

use src\framework\controller\Response;
use src\framework\controller\Request;
use src\framework\controller\TemplateController;
use src\emailtemplates\model\EmailTemplateModelFactory;

/**
 * Controls the email template administration functionality.
 */
class EmailTemplateController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
        $this->title          = 'Email Templates';
        $this->module         = 'ADM';
        $this->menuParameters = array(
            'menu_array' => $this->getMenuArray()
        );
    }
    
    /**
     * Produces a list of all the email templates defined in the system.
     */
    public function listEmailTemplates()
    {
        $this->hasPadding = false;
        
        $orderby = $this->request->getParameter('orderby') ?: 'recordid';
        $order   = $this->request->getParameter('order') == 'DESC' ? 'DESC' : 'ASC';
        
        $factory = new EmailTemplateModelFactory();
        $query   = $factory->getQueryFactory()->getQuery();
        
        $query->where(array('emt_module' => array_keys(getModArray())))
              ->orderBy(array(array($orderby, $order)));
        
        $templates = $factory->getCollection();
        $templates->setQuery($query);
        
        $this->response->build('src/admin/views/ListEmailTemplates.php', array(
            'deleted'    => $this->request->getParameter('deleted'),
            'order'      => $order,
            'orderby'    => $orderby,
            'templates'  => $templates,
            'initials'   => $_SESSION["initials"],
            'adminUser'  => $_SESSION["AdminUser"],
            'ModuleDefs' => $this->registry->getModuleDefs()
        ));
    }
    
    /**
     * Create new/edit existing templates.
     * 
     * @throws \URLNotFoundException If the template ID doesn't exist.
     */
    public function emailTemplate()
    {
        if ($this->request->getMethod() == 'POST')
        {
            if ($this->request->getParameter('rbWhat') == 'Save')
            {
                $this->save();
            }
            
            if ($this->request->getParameter('rbWhat') == 'Delete')
            {
                $this->delete();
            }
        }
        
        $id         = $this->request->getParameter('recordid');
        $factory    = new EmailTemplateModelFactory();
        $moduleDefs = $this->registry->getModuleDefs();
        
        if (!isset($id))
        {
            // new record - instantiate an empty EmailTemplate object
            $emailTemplate = $factory->getEntityFactory()->createObject();
        }
        else
        {
            // edit record - fetch the existing EmailTemplate object
            $mapper        = $factory->getMapper();
            $emailTemplate = $mapper->find($id);
            if (!isset($emailTemplate))
            {
                throw new \URLNotFoundException();
            }
        }
        
        $author = $emailTemplate->emt_author ?: $_SESSION['initials'];
        if ($_SESSION["initials"] != $author && $author != 'Datix')
        {
            // the form should be read-only if the user wasn't the original creator
            $mode = "ReadOnly";
        }

        $ValidModules = array();
        foreach(getModArray() as $Module => $Name)
        {
            if($moduleDefs[$Module]['CAN_CREATE_EMAIL_TEMPLATES'] === true)
            {
                $ValidModules[$Module] = $Name;
            }
        }

        $formArray = array(
            'email_template' => array(
                'Title' => $id ? _tk('edit_email_template') : _tk('add_email_template'),
                'Rows' => array(
                    array('Name' => 'recordid', 'Title' => 'Template ID', 'ReadOnly' => true, 'Type' => 'string', 'Condition' => isset($id)),
                    array('Name' => 'emt_module', 'Title' => 'Module', 'ReadOnly' => isset($id), 'Type' => 'ff_select', 'CustomCodes' => $ValidModules, 'OnChangeExtra' => 'filterTemplateTypes("emt_module");', 'SuppressCodeDisplay' => true),
                    array('Name' => 'emt_type', 'Title' => 'Template Type', 'ReadOnly' => isset($id), 'Type' => 'custom', 'HTML' => $this->getTypeField($emailTemplate->emt_type, $emailTemplate->emt_module)),
                    array('Name' => 'emt_name', 'Title' => 'Template Name', 'Type' => 'string', 'Width' => 50, 'MaxLength' => 50),
                    array('Name' => 'emt_notes', 'Title' => 'Template Description', 'Type' => 'textarea', 'Rows' => 3, 'Columns' => 70),
                    array('Name' => 'emt_author', 'Title' => 'Template Author', 'Type' => 'string', 'ReadOnly' => true)
                )
            ),
            'content' => array(
                'Title' => _tk('email_contents'),
                'Rows' => array(
                    array('Name' => 'emt_subject', 'Title' => 'Email Subject', 'Type' => 'textarea', 'Rows' => 2, 'Columns' => 70),
                    array('Name' => 'emt_text', 'Title' => 'Email Body', 'Type' => 'textarea', 'Rows' => 8, 'Columns' => 70),
                    array('Name' => 'emt_content_type', 'Title' => 'Content type', 'Type' => 'ff_select', 'CustomCodes' => array('TEXT' => 'TEXT', 'HTML' => 'HTML'), 'SuppressCodeDisplay' => true),
                )
            )
        );
        
        $formDesign = new \Forms_FormDesign();
        $formDesign->MandatoryFields = array(
            'emt_module' => 'email_template',
            'emt_type'   => 'email_template',
            'emt_name'   => 'email_template',
        );
        
        $data = array_merge($emailTemplate->getVars(), array('emt_author' => $author));
        
        $form = new \FormTable($mode, 'ADM', $formDesign);
        $form->MakeForm($formArray, $data, 'ADM');
        $form->MakeTable();
        
        $this->response->build('src/admin/views/EmailTemplate.php', array(
            'id'       => $id,
            'form'     => $form,
            'mode'     => $mode,
            'updateid' => $emailTemplate->updateid
        ));
    }

    /**
     * Prints out the configuration page for the templates.
     */
    public function configureemailtemplates()
    {
        global $scripturl, $ModuleDefs, $dtxtitle;

        require_once 'Source/libs/EmailTemplates.php';

        $sql = '
            SELECT
                recordid,
                emt_module,
                emt_name,
                emt_type,
                emt_notes,
                emt_author
            FROM
                EMAIL_TEMPLATES
            ORDER BY
                recordid
        ';

        $TemplateArray = \DatixDBQuery::PDO_fetch_all($sql);

        $ConfigArray = include_config_array();

        if ($this->request->getParameter('module'))
        {
            $CurrentModule = $this->request->getParameter('module');
        }
        else
        {
            $CurrentModule = array_shift(array_keys(include_config_array()));
        }

        $this->title = 'Configure Email Templates';
        $this->module = 'ADM';

        foreach (getModArray() as $mod => $name)
        {
            if (isset($ConfigArray[$mod]) && $mod != 'HOT')
            {
                $PanelArray[$mod] = array('Label' => $name);
                $DefaultArray[$mod] = $ConfigArray[$mod];
            }
        }

        $this->menuParameters = array(
            'panels' => $PanelArray,
            'menu_array' => GetMenuArray(),
            'floating_menu' => false
        );

        $this->hasPadding = false;

        $OptionHTML = array();

        foreach ($DefaultArray as $module => $GlobalArray)
        {
            foreach ($GlobalArray as $global => $aDetails)
            {
                $sql = "SELECT parmvalue FROM GLOBALS WHERE parameter = :parameter";
                $row = \DatixDBQuery::PDO_fetch($sql, array('parameter' => $global));

                $OptionHTML[$global] = '
                    <select id="'.$global.'" name="'.$global.'">
                ';
                
                if ($module !== 'ACT')
                {
                    $OptionHTML[$global] .= ' <option value="">Choose</option>';
                }

                if (is_array($TemplateArray))
                {
                    foreach ($TemplateArray as $template)
                    {
                        if ($template['emt_module'] == $module && $template['emt_type'] == $global)
                        {
                            $OptionHTML[$global] .= '
                                <option value="'.$template['recordid'].'"'.($row['parmvalue'] == $template['recordid'] ? ' SELECTED' : '').'>'.$template['emt_name'].'</option>
                            ';
                        }
                    }
                }

                if ($module == 'ALL' && $global == 'EMAIL_TEMPLATES')
                {
                    $OptionHTML[$global] .= '<option value="Y"'.($row['parmvalue'] == 'Y' ? ' SELECTED' : '').'>Yes</option>';
                    $OptionHTML[$global] .= '<option value="N"'.($row['parmvalue'] != 'Y' ? ' SELECTED' : '').'>No</option>';
                }

                $OptionHTML[$global] .= '</select>';
            }
        }

        $this->response->build('src/admin/views/ConfigureEmailTemplates.php', array(
            'recordid' => $this->request->getParameter('recordid'),
            'saved' => $this->request->getParameter('saved'),
            'DefaultArray' => $DefaultArray,
            'CurrentModule' => $CurrentModule,
            'OptionHTML' => $OptionHTML
        ));
    }

    /**
     * Deals with form submission when a user clicks 'Save', 'Cancel' etc.
     */
    public function emailtemplateoptions()
    {
        require_once 'Source/libs/EmailTemplates.php';

        $form_action = $this->request->getParameter('rbWhat');
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $posted = \Sanitize::SanitizeRawArray($this->request->getParameters());

        switch ($form_action)
        {
            case 'SaveConfig':
                SaveTemplateOptions(array('posted' => $posted));
                break;
            case 'Save':
                SaveTemplate(array('recordid' => $recordid, 'posted' => $posted));
                break;
            case 'Cancel':
                $this->redirect('app.php?action=listemailtemplates');
                break;
            case 'MainMenu':
                $this->redirect('app.php');
                break;
            case 'Delete':
                DeleteTemplate(array('recordid' => $recordid));
                break;
        }
    }
    
    /**
     * Save the email template record.
     */
    protected function save()
    {
        $factory       = new EmailTemplateModelFactory();
        $mapper        = $factory->getMapper();
        $emailTemplate = $factory->getEntityFactory()->createFromRequest($this->request);

        // Replace \r\n in the email body by char(13) to make newlines persistent
        if ($this->request->getParameter('emt_content_type') == 'TEXT' && $this->request->getParameter('emt_text'))
        {
            $this->request->setParameter('emt_text', str_replace('\r\n', '+char(13)+', $this->request->getParameter('emt_text')));
        }
        
        $mapper->save($emailTemplate);
        
        AddSessionMessage('INFO', _tk('email_template_saved'));
        $this->redirect('app.php?action=emailtemplate&recordid='.$emailTemplate->recordid);
    }
    
    /**
     * Delete the email template record.
     */
    protected function delete()
    {
        $factory       = new EmailTemplateModelFactory();
        $mapper        = $factory->getMapper();
        $emailTemplate = $factory->getEntityFactory()->createObject(array('recordid' => $this->request->getParameter('recordid')));
        
        $mapper->delete($emailTemplate);
        
        AddSessionMessage('INFO', _tk('email_template_deleted'));
        $this->redirect('app.php?action=listemailtemplates');
    }
    
    /**
     * Defines and returns the global menu configuration for this controller.
     * 
     * @return array
     */
    protected function getMenuArray()
    {
        return array(
            array('label' => 'New Template', 'link' => 'action=emailtemplate'),
            array('label' => 'List All Templates', 'link' => 'action=listemailtemplates'),
            array('label' => 'Configure Email Templates', 'link' => 'action=configureemailtemplates'),
        );
    }
    
    /**
     * Please ignore this function.  It does not exist.
     */
    private function getTypeField($type, $module)
    {
        // TODO - bin this and devise a more elegant solution to build the type field
        include 'Source/libs/EmailTemplates.php';
        if ($type)
        {
            $TypeArray = include_config_array();
            $html = $TypeArray[$module][$type]["title"].'<input type="hidden" id="emt_type" name="emt_type" value="'.$type.'">';
        }
        elseif ($module)
        {
            $html = $this->call('src/emailtemplates/EmailTemplateTypeController', 'gettemplatetypes', array('module' => $module));
        }
        else
        {
            $html = '<div id="emt_type_div"><select style="width:200px" disabled="true"></select></div>';
        }
        
        return $html;
    }
}
