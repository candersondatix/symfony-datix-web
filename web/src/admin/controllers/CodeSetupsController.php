<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class CodeSetupsController extends Controller
{
    public function exportcodesetup()
    {
        $fieldname = $this->request->getParameter('fieldname');
        $fieldtable = $this->request->getParameter('table');
    
        $CodeSetup = new \Setups_CodeSetup($fieldname, $fieldtable);
        $CodeSetup->OutputCodeSetup();
    }
    
    /**
    * Function called to pre-process/validate import options and read in the XML data ready to pass into main import function.
    *
    */
    public function importcodesetup()
    {
        global $FieldDefsExtra;
    
        $Module    = $this->request->getParameter('module');
        $FieldName = $this->request->getParameter('fieldname');
        $Table     = $this->request->getParameter('table');
        $PreviousModule = $Module;
    
        // Special case for Accreditation modules
        if ($Module == 'ACR')
        {
            require_once 'Source/setups/ImportCodes.php';
            
            $PreviousModule = 'ACR';
            $Module = getAccreditationModule($FieldDefsExtra, $FieldName);
        }
    
        if ($_POST["rbWhat"] == 'cancel' || empty($FieldDefsExtra[$Module][$FieldName]))
        {
            $FDR_info = new \Fields_Field($FieldName, $Table);
            $CodeTable = $FDR_info->getCodeTable();
    
            if ($PreviousModule == 'ACR')
            {
                $Module = 'ACR';
            }
            $this->redirect("app.php?action=codesetupsaction&code=$CodeTable&module=$Module&codefieldname=$FieldName&fdr_table=$Table&form_action=edit");
        }
    
        if ($PreviousModule == 'ACR')
        {
            $Module = 'ACR';
        }
    
        if (!empty($_FILES["userfile"]["tmp_name"]))
        {
            $RawXMLData = file_get_contents ( $_FILES["userfile"]["tmp_name"]);
        }
        else
        {
            $ImportResult === false;
            AddSessionMessage('ERROR', 'No import file selected');
    
            $redirect_url = 'app.php?action=importcodesform&module=' . $Module . '&fieldname=' . $FieldName . '&table=' . $Table;
    
            if (isset($error) && $error != '')
            {
                $redirect_url .= '&error=' . $error;
            }
    
            $this->redirect($redirect_url);
        }
    
        $import_action = $_POST['import_action'];
    
        $ImportResult = $this->DoImportCodeSetupXMLdata($RawXMLData, $import_action, $Module, $FieldName, $Table);
    
        if ($ImportResult === true)
        {
            AddSessionMessage('INFO', 'Import successful.');
        }
        else
        {
            $ErrorMessage = 'Could not import XML data.';
    
            AddSessionMessage('ERROR', $ErrorMessage);
        }
    
        $redirect_url =  'app.php?action=importcodesform&module=' . $Module . '&fieldname=' . $FieldName . '&table=' . $Table;
    
        if (isset($error) && $error != '')
        {
            $redirect_url .= '&error=' . $error;
        }
    
        $this->redirect($redirect_url);
    }
    
    /**
    * Main import function which uses the Importcodes class to perform the XML import
    *
    * @param string $RawXMLData XML string
    * @param string $import_action Import methods are APPEND|UPDATE|OVWERWRITE
    * @param mixed $Module Module for setup field
    * @param mixed $FieldName Field name of dropdown
    * @param mixed $Table Table of $Fieldname
    */
    private function DoImportCodeSetupXMLdata($RawXMLData, $import_action = 'APPEND', $Module, $FieldName, $Table)
    {
        $CodeSetup = new \Setups_CodeSetup($FieldName, $Table);
        $CodeSetup->ImportCodeSetup($RawXMLData, $import_action);
    
        if (!empty($CodeSetup->ErrorMsgs))
        {
            foreach ($CodeSetup->ErrorMsgs as $i => $ErrorMessage)
            {
                AddSessionMessage('ERROR', $ErrorMessage);
            }
    
            return false;
        }
    
        return true;
    }    
    
    /**
     * Renders the form used to edit a single value on an old school multi pick list.
     * 
     * Currently hard-coded to work with Adverse Event NPSA mappings - may well not ever be expanded beyond that,
     * since this is a fairly clunky way of handling this sort of mapping activity and is really a temporary solution.
     */
    public function editMultiCodeValue()
    {
        if ($this->request->getParameter('fieldName') != 'code_types_5')
        {
            $error = 'Error: this field does not support code editing.';
        }
        
        $selectField = \Forms_SelectFieldFactory::createSelectField('code', '', $this->request->getParameter('value'), '');
        
        $codes = \DatixDBQuery::PDO_fetch_all('SELECT cod_code, cod_code + \': \' + cod_descr FROM code_npsa_types WHERE cod_type =\'IN05\'', array(), \PDO::FETCH_KEY_PAIR);
        $codes = array_map('htmlspecialchars', $codes);

        $selectField->setCustomCodes($codes);
        
        $this->response->build('src/admin/views/EditMultiCodeValue.php', array(
            'error'       => $error,
            'selectField' => $selectField
        ));
    }
    
    /**
     * Outputs an Excel file that documents which IN05 mappings are now obsolete following the update to the NPSA dataset. 
     */
    public function exportObsoleteNPSAMappings()
    {
        ini_set('include_path', ini_get('include_path').';../Classes/');
        require_once 'export/PHPExcel.php';
        require_once 'export/PHPExcel/Writer/Excel2007.php';
        require_once 'export/PHPExcel/IOFactory.php';
        
        header("Pragma: public");
        header("Cache-Control: max-age=0");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="obsolete_nrls_mappings.xlsx"');
        header("Content-Encoding: none");
        
        $objPHPExcel = new \PHPExcel();
        
        $adverseEventCodes = \DatixDBQuery::PDO_fetch_all('SELECT cod_code, cod_descr, cod_parent, cod_npsa, cod_source FROM code_types 
            WHERE cod_type = \'CLINTYPE\' AND cod_npsa IS NOT NULL AND cod_npsa != \'\'');
        
        $in05Codes = \DatixDBQuery::PDO_fetch_all('SELECT cod_code FROM code_npsa_types WHERE cod_type = \'IN05\'', array(), \PDO::FETCH_COLUMN);
        
        $obsoleteCodes = array();
        
        foreach ($adverseEventCodes as $aeCodeKey => $aeCode)
        {
            $npsaCodes = explode(' ', $aeCode['cod_npsa']);
            foreach ($npsaCodes as $npsaCodeKey => $npsaCode)
            {
                if (!in_array($npsaCode, $in05Codes))
                {
                    // store the position of the obsolete code so it can be highlighted in the output
                    $obsoleteCodes[$aeCodeKey][] = $npsaCodeKey;
                }
            }
        }
        
        $objPHPExcel->getProperties()->setTitle("Obsolete NRLS Mappings");
        $objPHPExcel->getProperties()->setSubject("Obsolete NRLS Mappings");
        $objPHPExcel->getProperties()->setDescription("Obsolete NRLS Mappings");

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Obsolete codes');
        
        if (empty($obsoleteCodes))
        {
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'No obsolete NRLS codes were identified');
        }
        else
        {
            $numCodes = count($obsoleteCodes);
            
            // headers
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(9);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
            
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', \Fields_Field::getFieldLabel('inc_clin_detail', 'incidents_main'));
            $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Code');
            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Description');
            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'NRLS');
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Source');
            
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D1DCE2');
            
            $objPHPExcel->getActiveSheet()->getStyle('A2:E'.($numCodes+1))->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E7EDF0');
            
            $objPHPExcel->getActiveSheet()->getStyle('A1:E'.($numCodes+1))->applyFromArray(array(
                'borders' => array(
            		'allborders' => array(
            			'style' => \PHPExcel_Style_Border::BORDER_THIN,
            			'color' => array('argb' => '000000')
            		),
            	)
            ));
                
            $row = 2;
            foreach ($obsoleteCodes as $obCodeKey => $obsoleteCode)
            {
                // detail cell
                $detailCodes = explode(' ', $adverseEventCodes[$obCodeKey]['cod_parent']);
                $objRichText = new \PHPExcel_RichText();
                
                foreach ($detailCodes as $detailCodeKey => $detailCode)
                {
                    if ($detailCode == null)
                    {
                        $detailCode = '_';
                    }
                    
                    if (in_array($detailCodeKey, $obsoleteCode))
                    {
                        $objObsoleteDetail = $objRichText->createTextRun($detailCode);
                        $objObsoleteDetail->getFont()->setColor(new \PHPExcel_Style_Color(\PHPExcel_Style_Color::COLOR_RED));
                    }
                    else
                    {
                        $objRichText->createText($detailCode);
                    }
                    $objRichText->createText(' ');
                }
                
                $objPHPExcel->getActiveSheet()->getCell('A'.$row)->setValue($objRichText);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setWrapText(true);
                
                // code cell
                $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $adverseEventCodes[$obCodeKey]['cod_code']);
                
                // description cell
                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $adverseEventCodes[$obCodeKey]['cod_descr']);
                
                // nrls cell
                $npsaCodes = explode(' ', $adverseEventCodes[$obCodeKey]['cod_npsa']);
                $objRichText = new \PHPExcel_RichText();
                
                foreach ($npsaCodes as $npsaCodeKey => $npsaCode)
                {
                    if ($npsaCode == null)
                    {
                        $npsaCode = '_';
                    }
                    
                    if (in_array($npsaCodeKey, $obsoleteCode))
                    {
                        $objObsoleteNRLS = $objRichText->createTextRun($npsaCode);
                        $objObsoleteNRLS->getFont()->setColor(new \PHPExcel_Style_Color(\PHPExcel_Style_Color::COLOR_RED));
                    }
                    else
                    {
                        $objRichText->createText($npsaCode);
                    }
                    $objRichText->createText(' ');
                }
                
                $objPHPExcel->getActiveSheet()->getCell('D'.$row)->setValue($objRichText);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setWrapText(true);
                
                // source cell
                $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $adverseEventCodes[$obCodeKey]['cod_source']);
                
                $row++;
            }
        }
        
        $excelWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $excelWriter->save('php://output');
        exit;
    }
}