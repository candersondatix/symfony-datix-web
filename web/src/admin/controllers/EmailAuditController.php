<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class EmailAuditController extends TemplateController
{
    function email_audit()
    {
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        $Title = _tk('email_audit_title');
        $this->title = $Title;
        $this->module = 'ADM';

        $table = 'email_history';

        $data = array();

        $FormArray = array (
            'Parameters' => array(
                'Panels' => 'True',
                'Condition' => false
            ),
            'name' => array(
                'Title' => $Title,
                'MenuTitle' => $Title,
                'NewPanel' => true,
                'Rows' => array()
            ),
        );

        $SetupTable = new \FormTable();
        $SetupTable->MakeForm($FormArray, $data, $this->module);

        $SetupTable->MakeTable();

        $this->response->build('src/admin/views/EmailAudit.php', array(
            'data' => $data,
            'table' => $table
        ));
    }

    /**
     * @desc Defines the fields used in the Live grid for the Email Audit and displays the grid
     */
    static public function DefineFields()
    {
        global $oForm;

        $oForm->options['FilterLocation'] = -1;

        $SentFieldSQL = '
          SELECT 
              CASE t.emh_not_sent 
                  WHEN \'1\' THEN \'N\'
                  ELSE \'Y\'
              END';
        $oForm->AddCalculatedField($SentFieldSQL, 'Sent');
       // $oForm->AddEntryFieldW('emh_not_sent', 'Failure', 'B', '', 75);
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->CurrentField['width'] = 60;
        
        $oForm->AddEntryFieldW('mod_id', 'Module', 'S', '', 125);
        $oForm->CurrentField['visible'] = false;

        $ModuleFieldSql = 'SELECT mod_title FROM modules m WHERE m.mod_id = t.mod_id';
        $oForm->AddCalculatedField($ModuleFieldSql, 'Module');
        $oForm->CurrentField['width'] = 80;
        $oForm->CurrentField['filterUI'] = 't';

        $oForm->AddEntryFieldW("recordid", 'Record ID', 'B', '', 125);
        $oForm->CurrentField['visible'] = false;

        $oForm->AddEntryFieldW("link_id", 'Record ID', 'B', '', 125);
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->CurrentField['width'] = 80;

        // Note: There's duplicate entries on the CASE statement because of legacy e-mail type codes that
        // were being truncated at 6 characters
        $EmailTypeFieldSql = '
            SELECT
                CASE t.mod_id
                    WHEN 22 THEN
                        (CASE t.emh_type
                            WHEN \'NOTIFY\' THEN \'For action by\'
                            WHEN \'FIO\' THEN \'For information only\'
                            WHEN \'REMIND\' THEN \'For action by: Reminder\'
                            WHEN \'FAIL\' THEN \'Send e-mail failed\'
                            END
                            )
                    ELSE
                        (CASE t.emh_type
                            WHEN \'OVD\' THEN \'Overdue notification\'
                            WHEN \'OVERDUE\' THEN \'Overdue notification\'
                            WHEN \'ACKNOW\' THEN \'Reporter acknowledgement\'
                            WHEN \'ACKNOWLEDGE\' THEN \'Reporter acknowledgement\'
                            WHEN \'NEW\' THEN \'New Action/Action Chain\'
                            WHEN \'UPDATE\' THEN \'Updated Action/Action Chain\'
                            WHEN \'UPDATEDRECORD\' THEN \'Record Updated\'
                            WHEN \'COMPLE\' THEN \'Completed Action/Action Chain\'
                            WHEN \'COMPLETE\' THEN \'Completed Action/Action Chain\'
                            WHEN \'NEWHAN\' THEN \'New Handler\'
                            WHEN \'NEWHANDLER\' THEN \'New Handler\'
                            WHEN \'NEWINV\' THEN \'New Investigator\'
                            WHEN \'NEWINVESTIGATOR\' THEN \'New Investigator\'
                            WHEN \'NOTIFY\' THEN \'Notification\'
                            WHEN \'NOT\' THEN \'Notification\'
                            WHEN \'FBK_R\' THEN \'Communication and feedback message\'
                            WHEN \'FBK_H\' THEN \'Communication and feedback message\'
                            WHEN \'FBK_M\' THEN \'Communication and feedback message\'
                            WHEN \'FBK_O\' THEN \'Communication and feedback message\'
                            WHEN \'REGISTERUSER\' THEN \'User registration acknowledgement\'
                            WHEN \'REGISTERNOTIFY\' THEN \'User registration submitted\'
                            WHEN \'REGISTRATIONAPPROVED\' THEN \'User registration approved\'
                            WHEN \'REGISTRATIONREJECTED\' THEN \'User registration rejected\'
                            WHEN \'NEWMASTERSTAFF\' THEN \'Assigned assessment template submitted\'
                            WHEN \'NEWLOCATION\' THEN \'Assigned assessment template assigned to location\'
                            WHEN \'NEWSTAFF\' THEN \'New Staff\'
                            WHEN \'REVIEWEDINSTANCES\' THEN \'Assigned assessment template reviewed\'
                            WHEN \'NEWINSTANCES\' THEN \'Assessment created to location\'
                            WHEN \'SUBMITTEDASSESSMENTINSTANCE\' THEN \'Assessment submitted\'
                            WHEN \'REPPROGRESS\' THEN \'Reporter progress update\'
                            WHEN \'REM\' THEN \'Reminder notification\'
                        END
                        )
                END
        ';
        $oForm->AddCalculatedField($EmailTypeFieldSql, 'E-mail type');
        $oForm->CurrentField['width'] = 175;
        $oForm->CurrentField['filterUI'] = 't';

        $RecipientNameFieldSql = 'SELECT con_surname + \', \' + con_forenames + \' \' + con_title FROM contacts_main c WHERE c.recordid = t.con_id';
        $oForm->AddCalculatedField($RecipientNameFieldSql, 'Recipient name');
        $oForm->CurrentField['width'] = 175;
        $oForm->CurrentField['filterUI'] = 't';

        $oForm->AddEntryFieldW('emh_email', 'Recipient e-mail address', 'B', '', 175);
        $oForm->CurrentField['filterUI'] = 't';

        $oForm->AddEntryFieldW('emh_error', 'Error message', 'B', '', 350);
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->CurrentField['width'] = 100;

        $oForm->AddEntryFieldW('emh_dsent', 'Date/Time', 'D', '', 125);
        $oForm->CurrentField['dateFmt'] = 'yyyy-mm-dd HH:nn:ss';
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->CurrentField['width'] = 100;

        $oForm->AddEntryFieldW('con_id', 'Contact ID', 'B', '', 125);
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->CurrentField['width'] = 100;

        $oForm->AddSort('emh_dsent', 'DESC');

        $oForm->overrideKeys['mod_id'] = true;
        $oForm->overrideKeys['recordid'] = true;
        $oForm->overrideKeys['emh_dsent'] = true;

        $oForm->DisplayPage();
    }
}