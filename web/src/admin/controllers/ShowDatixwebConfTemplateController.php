<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\registry\Registry;

class ShowDatixwebConfTemplateController extends TemplateController
{
    function setup()
    {
        global $FieldDefs;

        $ModuleDefs = $this->registry->getModuleDefs();

        $this->addJs('src/admin/js/validation.js');

        if ($this->request->getParameter('rbWhat') == 'Cancel')
        {
            $this->redirect('?module=ADM');
        }

        $Settings = GetParms('', false); // get globals, ignoring user parameters, without setting SESSION globals.

        $module = $this->request->getParameter('module');

        if (!$module)
        {
            $module = 'ALL';
        }

        $this->title = 'DatixWeb Configuration';
        $this->module = 'ADM';

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array(
            'label' => 'Save settings',
            'name' => 'btnSubmitIncident',
            'onclick' => 'selectAllMultiCodes();checkDatixSelects();if(validateOnSubmit()){document.SettingsPage.submit();}',
            'action' => 'SAVE'
        ));
        $ButtonGroup->AddButton(array(
            'label' => _tk('btn_cancel'),
            'name' => 'btnSubmitIncident',
            'onclick' => 'SendTo(\'index.php?module=ADM\');',
            'action' => 'CANCEL'
        ));

        $PanelArray['ALL'] = array('Label' => 'All modules');

        foreach(getModArray(array('ADM', 'DST', 'ELE', 'PRO', 'HSA', 'HOT', 'LIB', 'DAS', 'LOC'), true) as $mod => $name)
        {
            $PanelArray[$mod] = array('Label' => $name);
        }

        $this->menuParameters = array(
            'module' => 'ADM',
            'panels' => $PanelArray,
            'buttons' => $ButtonGroup
        );

        $FieldObj = new \FormField();

        // All modules
        $ALLTable = new \FormTable();

        $ALLTable->MakeTitleRow('<b>System Wide Settings</b>');

        $ALLTable->MakeRow('<b>Admin user\'s e-mail address</b><br />
            You must set this to a valid e-mail address as this is who automatic '._tk("INCName").' report e-mails will appear to be sent from.  Users need to be able to reply to this address.',
            $FieldObj->MakeEmailField("DIF_ADMIN_EMAIL", 60, 254, $Settings["DIF_ADMIN_EMAIL"], "isPermittedAdminEmail('DIF_ADMIN_EMAIL', 'PERMITTED_EMAIL_DOMAINS');", true));

        $ALLTable->MakeRow('<b>Maintenance mode?</b><br />
            If DatixWeb is in Maintenance mode, all DatixWeb users, with the exception of Admin users, will be prevented from logging in.  If they are already logged in, they will be logged out.  A message will be displayed to non-login users who attempt to access input forms.',
            $FieldObj->MakeDivYesNo('WEB_MAINTMODE', "N", $Settings["WEB_MAINTMODE"], '',
                'if(jQuery(\'#WEB_MAINTMODE\').val() == \'Y\'){
                 if(!confirm(\'You are about to put the system into maintenance mode. This could cause any logged in user to lose the data they are currently working on.\n\nClick \"OK\" to confirm, or \"Cancel\" to cancel.\')){
                jQuery(\'#WEB_MAINTMODE_title\').selectItem(jQuery(\'<li id="N">'._tk('no').'</li>\'))
                }
                }'));

        $ALLTable->MakeRow('<b>Maintenance mode message</b><br />
            If this message is saved as blank, a default message will be displayed.',
            $FieldObj->MakeTextAreaField('WEB_MAINTMODE_MSG', 3, 60, 254,  $Settings["WEB_MAINTMODE_MSG"]));

        $ALLTable->MakeRow('<b>Display login message?</b><br />
            If this option is set, a message will be displayed each time a user logs in.',
            $FieldObj->MakeDivYesNo('DIF_LOGIN_MSG', "", $Settings["DIF_LOGIN_MSG"]));

        $MultipleTextAreas = GetLoginMessageControl('LoginMessage', 'Additional message');

        $ALLTable->MakeRow('<b>Login messages to display</b><br />
            Add the text of the message to be displayed. Select the \'Additional message\' option to configure multiple messages. If the first message is saved as blank, a default message will be displayed.  Saving subsequent messages as blank will cause them to be removed.',
            $FieldObj->MakeCustomField($MultipleTextAreas));

        $field = \Forms_SelectFieldFactory::createSelectField('LOGIN_DEFAULT_MODULE', '', $Settings['LOGIN_DEFAULT_MODULE'], '');
        $field->setCustomCodes(getModArray(array('ELE', 'PRO'), true));
        $field->setSuppressCodeDisplay();

        $ALLTable->MakeRow('<b>Default module at login</b><br />
            Select module to be presented to users upon logging in. This default can be overridden for individual profiles and users.', $field);

        $field = $FieldObj->MakeNumberField("DIF_TIMER_MINS", $Settings["DIF_TIMER_MINS"], 13, 0, "", "", false, "isPermittedTimeoutValue('DIF_TIMER_MINS')");

        $ALLTable->MakeRow('<b>Minutes to timeout</b><br />
            A user will be logged out after this many minutes of inactivity.  A level 1 form will also be cleared after this many minutes.  If this option is left blank, it will default to 10 minutes.  A value of -1 minutes means the timeout is disabled.',
            $field);

        $ALLTable->MakeRow('<b>Disable timeout on level 1 forms</b><br />
            Tick this box if you do not want the level 1 form to clear itself after the above number of minutes of inactivity.  The level 2 form will still time out, however.',
            $FieldObj->MakeDivYesNo('DIF_1_NO_TIMEOUT', "", $Settings["DIF_1_NO_TIMEOUT"]));

        if (bYN($Settings['WEB_MAINTMODE']))
        {
            $RecordLockField = $FieldObj->MakeDivYesNo('RECORD_LOCKING', "", $Settings["RECORD_LOCKING"]);
        }
        else
        {
            $DisplayText = (bYN($Settings["RECORD_LOCKING"]) ? _tk('yes') : _tk('no'));
            $RecordLockField = $FieldObj->MakeCustomField('<input type="hidden" name="RECORD_LOCKING" value="'.$Settings["RECORD_LOCKING"].'">'.$DisplayText);
        }

        $ALLTable->MakeRow('<b>Enable Record Locking</b><br />
            Record Locking ensures that only one user can edit a record at a time.<br><br>Note: this setting can be changed only when your system is placed in Maintenance mode',
            $RecordLockField);

        if (bYN($Settings['RECORD_LOCKING']))
        {
            $RecordLockTimeoutField = $FieldObj->MakeNumberField('RECORDLOCK_TIMEOUT', $Settings["RECORDLOCK_TIMEOUT"]);
        }
        else
        {
            $RecordLockTimeoutField = $FieldObj->MakeCustomField('<input type="hidden" name="RECORDLOCK_TIMEOUT" value="'.$Settings["RECORDLOCK_TIMEOUT"].'">'.$Settings["RECORDLOCK_TIMEOUT"]);
        }

        $ALLTable->MakeRow('<b>Record lock timeout</b><br />
            A user will be asked to confirm that they are still editing a record after this many minutes of inactivity. A time of -1 will disable this timeout.',
            $RecordLockTimeoutField);

        $ALLTable->MakeRow('<b>Hide top menu when not logged in</b><br />
            This option switches off the display of the top menu bar (i.e. the New Form and Login options) when a user is not logged in.',
            $FieldObj->MakeDivYesNo('DIF_NOMAINMENU', "", $Settings["DIF_NOMAINMENU"]));

        $ALLTable->MakeRow('<b>Logout redirects to Login page</b><br />
            If this option is set, when a user logs out, they will be taken back to the login page, instead of a new blank form.',
            $FieldObj->MakeDivYesNo('DIF_LOGOUTTOIN', "", $Settings["DIF_LOGOUTTOIN"]));

        $ALLTable->MakeRow('<b>Allow registration requests</b><br />
            This option allows a member of staff to request access to the system using a special registration form.',
            $FieldObj->MakeDivYesNo('DIF_2_REGISTER', "", $Settings["DIF_2_REGISTER"]));

        $field = \Forms_SelectFieldFactory::createSelectField('REG_FORM_DESIGN', '', $Settings['REG_FORM_DESIGN'], '');
        $field->setCustomCodes(GetListOfFormDesigns(array('module' => 'ADM', 'level' => 2)));
        $field->setSuppressCodeDisplay();

        $ALLTable->MakeRow('<b>Registration Form Design</b><br />
            Identify the User Admin form design which will be used for the registration form.', $field);

        // get domain whitelist 
        require_once __DIR__.'/../../../Source/libs/EmailDomainWhitelistClass.php';
        $whitelist = new \EmailDomainWhitelist ();
        $domains = join("\n", $whitelist->getAllowedDomains());

        $field = $FieldObj->MakeTextAreaField("PERMITTED_EMAIL_DOMAINS", 3, 60, 254, $domains, true, true, "", "", "", "", "isPermittedDomainWhitelist('PERMITTED_EMAIL_DOMAINS', 'DIF_ADMIN_EMAIL');");

        $ALLTable->MakeRow('<b>'._tk('permitted_domains').'</b><br />
            Separate each domain with a carriage return. If blank, there is no domain restriction'.AddHelpBubble(array('field' => 'PERMITTED_EMAIL_DOMAINS', 'title' => _tk('permitted_domains'), 'help' => 'Use this setting to specify permitted domains, to which e-mails may be sent by the application. E-mails will not be sent to other domains. This setting covers all e-mails handled by the application.')),
            $field);

        $ALLTable->MakeRow('<b>Send e-mail notifications for new user accounts</b><br />
            Choose whether new users will receive e-mail notification when their account is created or registration request is rejected.',
            $FieldObj->MakeDivYesNo('EMAIL_NEW_ACCOUNTS', '', $Settings['EMAIL_NEW_ACCOUNTS']));

        $GroupEmailOptions = array(
            'Y' => 'Individual e-mails',
            'N' => 'Group e-mail',
            'B' => 'Group e-mail (bcc)'
        );

        $field = \Forms_SelectFieldFactory::createSelectField('INDIVIDUAL_EMAILS', '', $Settings["INDIVIDUAL_EMAILS"], '');
        $field->setCustomCodes($GroupEmailOptions);

        $ALLTable->MakeRow('<b>E-mail grouping</b><br />
            This option determines if e-mails from DatixWeb are sent as individual e-mails to each recipient or as a group e-mail to all recipients.'.AddHelpBubble(array('field' => 'DIF_SHOW_EMAIL', 'title' => 'E-mail grouping', 'help' => 'Note that sending individual e-mails will increase system processing time.<br><br>Setting this option to "Group e-mail" means that processing is minimised, and all recipients will see all other recipents\' e-mail addresses.<br><br>Setting this option to "Group e-mail (bcc)" maintains minimal processing while ensuring that recipients do not see other users\' e-mail addresses')),
            $field);

        $ALLTable->MakeRow('<b>Display both code and description in dropdowns</b><br />
            Tick this box if you want to display codes next to descriptions in dropdown lists.',
            $FieldObj->MakeDivYesNo('WEB_SHOW_CODE', "", $Settings["WEB_SHOW_CODE"]));

        $StaffOptions = array(
            'N' => 'Do not display',
            'B' => 'Display before name',
            'A' => 'Display after name'
        );

        $field = \Forms_SelectFieldFactory::createSelectField('STAFF_NAME_DROPDOWN', '', $Settings["STAFF_NAME_DROPDOWN"], '');
        $field->setCustomCodes($StaffOptions);

        $ALLTable->MakeRow('<b>Display job title in staff drop-downs?</b><br />
            Specify if, and where, the job title should be displayed against staff names in staff drop-downs.',
            $field);

        $ALLTable->MakeRow('<b>Number of records to display</b><br />
            This option allows a user to define the number of records to be displayed on one listing page. Default is 20.',
            $FieldObj->MakeInputField('LISTING_DISPLAY_NUM', 10, 254, $Settings["LISTING_DISPLAY_NUM"]));

        $DateFormats = array(
            "GB" => "GB (dd/MM/yyyy)",
            "US" => "US (MM/dd/yyyy)"
        );

        $field = \Forms_SelectFieldFactory::createSelectField('FMT_DATE_WEB', '', $Settings["FMT_DATE_WEB"], '');
        $field->setCustomCodes($DateFormats);

        $ALLTable->MakeRow('<b>Date format in DatixWeb</b><br />
            This option controls the date format in DatixWeb.',
            $field);

        $ALLTable->MakeRow('<b>Click in date field to display calendar</b><br />
            If this option is set, the calendar button will not appear. When the user clicks in the date field, the calendar will be displayed automatically.',
            $FieldObj->MakeDivYesNo('CALENDAR_ONCLICK', "", $Settings["CALENDAR_ONCLICK"]));

        $CalendarWeekends = array(
            '1' => 'Saturday/Sunday',
            '2' => 'Sunday/Monday',
            '3' => 'Monday/Tuesday',
            '4' => 'Tuesday/Wednesday',
            '5' => 'Wednesday/Thursday',
            '6' => 'Thursday/Friday',
            '0' => 'Friday/Saturday',
        );

        $field = \Forms_SelectFieldFactory::createSelectField('CALENDAR_WEEKEND', '', $Settings["CALENDAR_WEEKEND"], '');
        $field->setCustomCodes($CalendarWeekends);

        $ALLTable->MakeRow('<b>Weekend days</b><br />
            Use these days to display weekends in the calendar pop-up for date fields.',
            $field);

        $ALLTable->MakeRow('<b>Give users access to the customised report builder?</b><br />
            Identify whether users will have access to the customised report building section when accessing the reporting area. This option can be overridden for specific users on their user details screen.',
            $FieldObj->MakeDivYesNo('CUSTOM_REPORT_BUILDER', "", $Settings["CUSTOM_REPORT_BUILDER"]));

        $ALLTable->MakeRow('<b>Give users access to additional reporting options?</b><br />
            This option can be overridden for specific users on their user details screen',
            $FieldObj->MakeDivYesNo('ADDITIONAL_REPORT_OPTIONS', "", $Settings["ADDITIONAL_REPORT_OPTIONS"]));

        $ALLTable->MakeRow('<b>Permitted file extensions</b><br />
            If this option is set, only documents with extensions in this list can be added to Datix. Add entries as a comma separated list (e.g. DOC,XLS,JPG).',
            $FieldObj->MakeInputField('VALID_FILE_TYPES', "20", "254", $Settings["VALID_FILE_TYPES"]));

        $ALLTable->MakeRow('<b>Restricted file extensions</b><br />
            If this option is set and the above option is not, documents with extensions on this list will be rejected. Add entries as a comma separated list (e.g. EXE,COM,JS).',
            $FieldObj->MakeInputField('INVALID_FILE_TYPES', "20", "50", $Settings["INVALID_FILE_TYPES"]));

        $ALLTable->MakeRow('<b>Enable DatixWeb triggers</b><br />
            If this option is set to yes, e-mail and message triggers setup in the main application will also apply within DatixWeb.',
            $FieldObj->MakeDivYesNo('WEB_TRIGGERS', "", $Settings["WEB_TRIGGERS"]));

        $ALLTable->MakeRow('<b>Enable record level security</b><br />
            If this option is set to yes, the record level security section will appear when viewing records.',
            $FieldObj->MakeDivYesNo('SEC_RECORD_PERMS', "", $Settings["SEC_RECORD_PERMS"]));

        $field = \Forms_SelectFieldFactory::createSelectField('PROG_NOTES_EDIT', '', $Settings["PROG_NOTES_EDIT"], '');
        $field->setCustomCodes(array('OWN' => 'Edit own notes only', 'ALL' => 'Edit all notes'));

        $ALLTable->MakeRow('<b>Set permissions for editing progress notes</b>
            '.AddHelpBubble(array(
                'title' => 'Set permissions for editing progress notes',
                'field' => 'PROG_NOTES_EDIT',
                'help' => 'Users can be granted access to edit any progress note, or only those that they have created themselves.')).'<br />',
            $field);

        $ALLTable->MakeRow('<b>Allow cross module generation of records</b>
            '.AddHelpBubble(array(
                'title' => 'Enable users to generate new records',
                'field' => 'ENABLE_GENERATE',
                'help' => 'Users can be granted access to generate a new record in another module if available and they have permission to add records.')).'<br />',
            $FieldObj->MakeDivYesNo('ENABLE_GENERATE', "", $Settings["ENABLE_GENERATE"]));

        $ALLTable->MakeRow('<b>Number of days back to limit the Full and User audit views</b><br />
            This option will set a limit to the number of days back the user can view in the Full and User Audit screens. Default is 180 days',
            $FieldObj->MakeInputField('AUDIT_DISPLAY_DAYS', "20", "50", $Settings["AUDIT_DISPLAY_DAYS"]));

        $ALLTable->MakeRow('<b>Symbol to use before currency values</b><br />
            Default is &pound;',
            $FieldObj->MakeInputField('CURRENCY_CHAR', "3", "3", $Settings["CURRENCY_CHAR"]));

        $field = \Forms_SelectFieldFactory::createSelectField('WORD_MERGE_ENGINE', '', $Settings["WORD_MERGE_ENGINE"], '');
        $field->setCustomCodes(array('OLD' => 'Client-side merge (ActiveX)', 'NEW' => 'Server-side merge'));

        $ALLTable->MakeRow('<b>Document template merge method</b><br />
            Select the method for merging document templates in DatixWeb. Selecting Server-side merge enables the merging of documents without the need for ActiveX controls. Client-side merge still requires ActiveX.',
            $field);

        $field = \Forms_SelectFieldFactory::createSelectField('DEFAULT_PAPER_SIZE', '', $Settings['DEFAULT_PAPER_SIZE'], '');
        $field->setCustomCodes(array('A4' => 'A4', 'A3' => 'A3', 'LETTER' => 'Letter'));
        $ALLTable->MakeRow('<b>Set default paper size for exporting listing reports</b>', $field);

        $field = \Forms_SelectFieldFactory::createSelectField('DEFAULT_PAPER_ORIENTATION', '', $Settings['DEFAULT_PAPER_ORIENTATION'], '');
        $field->setCustomCodes(array('P' => 'Portrait', 'L' => 'Landscape'));
        $ALLTable->MakeRow('<b>Set default paper orientation for exporting listing reports</b>', $field);

        $ALLTable->MakeRow('<b>Show indicator on forms for panels which contain data inputted by a user</b>',
            $FieldObj->MakeDivYesNo('SHOW_PANEL_INDICATORS', "", $Settings["SHOW_PANEL_INDICATORS"]));
        
        $ALLTable->MakeRow('<b>Listing report initial load</b><br />
            Set the number of record entries initially displayed on screen when users run a listing report. Default is 100. Additional entries are loaded as the user scrolls down the page.',
            $FieldObj->MakeInputField('LISTING_REPORT_DISPLAY_NUM', 10, 10, $Settings["LISTING_REPORT_DISPLAY_NUM"]));

        $ALLTable->MakeRow('<b>Show advanced settings?</b>',
            $FieldObj->MakeDivCheckBox('show_advanced_all', 'advanced_all', false, 'show_advanced_all'));

        // ADVANCED SETTINGS SECTION
        $ALLAdvTable = new \FormTable;
        $ALLAdvTable->MakeTitleRow('<b>Advanced settings</b>');

        $ALLAdvTable->MakeRow('<b>Display usage agreement?</b>',
            $FieldObj->MakeDivYesNo('LOGIN_USAGE_AGREEMENT', "", $Settings["LOGIN_USAGE_AGREEMENT"]));

        $MultipleTextAreas = GetLoginMessageControl('UsageAgreementMessage', 'Additional agreement');

        $ALLAdvTable->MakeRow('<b>Usage agreements to display</b><br /><div class="field_error" id="errUsageAgreementMessage" style="display:none; font-weight:bold;">You must enter a value in this field</div>
            Add the text of the message to be displayed. Select the \'Additional agreement\' option to configure multiple messages.  If this option is switched on, a message must be specified. Saving additional messages as blank will cause them to be removed.',
            $FieldObj->MakeCustomField($MultipleTextAreas));

        $ALLAdvTable->MakeRow('<b>Disagreement message</b><br /><div class="field_error" id="errDISAGREEMENT_MSG" style="display:none; font-weight:bold;">You must enter a value in this field</div>
            Add the text of the message to be displayed if the user disagrees with any of the usage agreements specified above.',
            $FieldObj->MakeTextAreaField('DISAGREEMENT_MSG', 6, 60, 254,  $Settings["DISAGREEMENT_MSG"]));

        $ALLAdvTable->MakeRow('<b>Support Email Address</b><br />Add in the support address to send emails to. For multiple email addresses please seperate with a comma.',
            $FieldObj->MakeEmailField('SUPPORT_EMAIL_ADDR', "50", "255", $Settings["SUPPORT_EMAIL_ADDR"]));

        $formElement = $FieldObj->MakeDivYesNo('SYSTEM_ERROR_LOGGING', "N", $Settings["SYSTEM_ERROR_LOGGING"]);
        $formElement->setOnChangeExtra('if (jQuery("input#SYSTEM_ERROR_LOGGING").val() == "N") { alert("Please be aware that saving this value to \'No\' will delete any Error log file previously created. "); }');

        $ALLAdvTable->MakeRow('<b>System Error Logging</b><br/>If \'Y\' errors are logged. This may prove useful for backtracing in the event of an error. When swiched to \'N\' errors are no longer logged, and any logs previously created are deleted', $formElement);

        $ALLAdvTable->MakeRow('<b>Hide sensitive data in config.xml?</b>',
            $FieldObj->MakeDivYesNo('HIDE_SENSITIVE_CONFIG_DATA', "", $Settings["HIDE_SENSITIVE_CONFIG_DATA"]));

        $ALLAdvTable->MakeRow('<b>Display information about previous login attempts on login?</b>',
            $FieldObj->MakeDivYesNo('DISPLAY_LAST_LOGIN', "", $Settings["DISPLAY_LAST_LOGIN"]));

        if (IsCentrallyAdminSys() && IsCentralAdmin())
        {
            $ALLAdvTable->MakeRow('<b>Remove local administrators access to user parameters?</b>',
                $FieldObj->MakeDivYesNo('REMOVE_USER_PARAM_ACCESS', "", $Settings["REMOVE_USER_PARAM_ACCESS"]));
        }

        $ALLTable->MakeTable();
        $ALLAdvTable->MakeTable();
        $ALLAdvTable->MakeSection('advanced_all', false);

        // Incident Module Settings
        $Table = new \FormTable();

        $Table->MakeTitleRow('<b>General Settings</b>');

        $Table->MakeRow('<b>Restrict handlers to approving and viewing their own '._tk("INCNames").'</b><br />
            If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',
            $FieldObj->MakeDivYesNo('DIF_OWN_ONLY', "", GetParm('DIF_OWN_ONLY', '', true)));

        $Table->MakeRow('<b>Allow users to reject incidents</b><br />
            With this option set to \'Yes\', users will be able to reject incidents.  This setting can be overridden for specific users on their user record.',
            $FieldObj->MakeDivYesNo('DIF_SHOW_REJECT_BTN', "", GetParm('DIF_SHOW_REJECT_BTN', '', true)));

        $Table->MakeRow('<b>'._tk('enable_saved_queries_home_screen').'</b>',
            $FieldObj->MakeDivYesNo('INC_SAVED_QUERIES_HOME_SCREEN', '', GetParm('INC_SAVED_QUERIES_HOME_SCREEN', 'N', true)));

        $incSavedQueries = explode(',', GetParm('INC_SAVED_QUERIES', '', true));

        $field = new \Forms_MultiSelectField('INC_SAVED_QUERIES', 'INC', $incSavedQueries, 'New');

        require_once 'Source/libs/SavedQueries.php';
        $values = getSavedQueriesAccessibleToEveryone('INC');

        if (!$values)
        {
            $values = [];
        }

        $field->setCustomCodes($values);

        $Table->MakeRow('<b>'._tk('choose_saved_queries').'</b><br />', $field);

        $Table->MakeRow('<b>'._tk('enable_saved_queries_num_records').'</b>',
            $FieldObj->MakeDivYesNo('INC_SAVED_QUERIES_NUM_RECORDS', '', GetParm('INC_SAVED_QUERIES_NUM_RECORDS', 'N', true)));

        $Table->MakeTitleRow('<b>DIF1 settings</b>');

        $field = \Forms_SelectFieldFactory::createSelectField('DIF1_ONLY_FORM', '', $Settings['DIF1_ONLY_FORM'], '');
        $field->setCustomCodes(GetListOfFormDesigns(array('level' => 1, 'module' => 'INC')));
        $field->setSuppressCodeDisplay();

        $HelpText = 'A DIF1 form design selected against a profile or individual user account will override the one selected here.';
        $Table->MakeRow('<b>DIF1 design for logged-in users:</b>
            '.AddHelpBubble(array('title' => 'DIF1 design for logged-in users', 'field' => 'DIF1_ONLY_FORM', 'help' => $HelpText)).'
            <br />Select the DIF1 form design to be presented to users who log in to submit a new incident.',
            $field);

        $Table->MakeRow('<b>DIF1 reference number prefix</b>
            <br />If you would like '._tk("INCName").' reports submitted via the web to be distinguished from those entered from a paper form, enter some characters here which will automatically be prepended to the numeric reference number that is automatically generated.',
            $FieldObj->MakeInputField("DIF_1_REF_PREFIX", 10, 20, $Settings["DIF_1_REF_PREFIX"]));

        $Table->MakeRow('<b>Email '._tk("INCName").' to reporter\'s manager</b><br />
            If this option is set, a dropdown will appear in DIF1 asking the reporter of the '._tk("INCName").' to select their manager.  That manager will then be e-mailed details of the '._tk("INCName").'.',
            $FieldObj->MakeDivYesNo('DIF_EMAIL_MGR', "", $Settings["DIF_EMAIL_MGR"]));

        $Table->MakeRow('<b>E-mail acknowledgment to reporter?</b><br />
            Tick this box if you want an additional field for e-mail address to appear in the Reporter section.  An acknowledgment e-mail will then be sent to this address.',
            $FieldObj->MakeDivYesNo('DIF_1_ACKNOWLEDGE', "", $Settings["DIF_1_ACKNOWLEDGE"]));

        $DIF1CCSArray = array(
            "NONE" => "Display only "._tk("INCNameTitle")." Type on form",
            "TCS" => "Display Type/Category/Subcategory fields only",
            "CCS" => "Display Datix CCS fields only",
            "TCS_CCS" => "Display both Type/Category/Subcategory and Datix CCS code fields"
        );

        $field = \Forms_SelectFieldFactory::createSelectField('DIF_1_CCS', '', $Settings["DIF_1_CCS"], '');
        $field->setCustomCodes($DIF1CCSArray);

        $Table->MakeRow('<b>Display '._tk("INCName").' classification fields</b><br />
            This option controls whether the Datix CCS codes, the Type/Category/Subcategory codes, a combination of both or neither are shown on the DIF1 form.',
            $field);

        $Table->MakeRow('<b> Do not give users the "'._tk("btn_submit_and_print_incident").'" option</b>
            <br />If this option is set, the "'._tk("btn_submit_and_print_incident").'" option will not be displayed on the DIF1.',
            $FieldObj->MakeDivYesNo('DIF_1_NO_PRINT', "", $Settings["DIF_1_NO_PRINT"]));

        $Table->MakeRow('<b>Show reporter e-mail notifications?</b><br />
            Tick this box if you want the reporter to be shown the names and job titles of managers who are being sent e-mail notifications from DIF1 forms.',
            $FieldObj->MakeDivYesNo('DIF_SHOW_EMAIL', "", $Settings["DIF_SHOW_EMAIL"]));

        $Table->MakeRow('<b>Contact matching listing design</b>',
            MakeListingFormDesignField('CON', $Settings["INC_L1_CON_MATCH_LISTING_ID"], 1, '', 'INC_L1_CON_MATCH_LISTING_ID'));

        $Table->MakeRow('<b>Autopopulate record name?</b><br />
            If set to \'Yes\', the record name will be automatically populated with the name of the first Person Affected linked to the record.',
            $FieldObj->MakeDivYesNo('AUTO_POPULATE_REC_NAME_INC', "", $Settings["AUTO_POPULATE_REC_NAME_INC"]));

        $Table->MakeTitleRow('<b>DIF2 settings</b>');

        $Table->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'INC_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('INC', \Forms_FormDesign::getGlobalFormDesignID('INC', 2, 'Search')));

        $DIF_2_CCSArray = array (
            'TCS' => "Display Type/Category/Subcategory fields only",
            'CCS' => "Display Datix CCS fields only",
            'TCS_CCS' => "Display both Type/Category/Subcategory and Datix CCS code fields"
        );

        $field = \Forms_SelectFieldFactory::createSelectField('DIF_2_CCS', '', $Settings["DIF_2_CCS"], '');
        $field->setCustomCodes($DIF_2_CCSArray);

        $Table->MakeRow('<b>Display '._tk("INCName").' classification fields</b><br />
            This option controls whether the Datix CCS codes, the Type/Category/Subcategory codes or a combination of both are shown on the DIF2 form.',
            $field);

        $Table->MakeRow('<b>Show "Add new" option</b><br />
            Display "Add new" option to add a new record using DIF2.  This is switched on by default.',
            $FieldObj->MakeDivYesNo('DIF2_ADD_NEW', "", $Settings["DIF2_ADD_NEW"]));

        $Table->MakeRow('<b>Enable default listing reports</b><br />
            If this option is set, the default "correctable cause" listing reports will be included in the "Listing report" dropdown.',
            $FieldObj->MakeDivYesNo('DIF2_DEFAULT_REPORTS', "", $Settings["DIF2_DEFAULT_REPORTS"]));

        $Table->MakeRow('<b>Do not show groups on the Causes page</b><br />
            If you are using your own root causes (i.e. INC_INV_RC is set), use this option to determine whether to display root cause group headers.',
            $FieldObj->MakeDivYesNo('DIF_2_NORCGS', "", $Settings["DIF_2_NORCGS"]));

        $OverdueDaysSection = GetOverdueDaysSection('INC');

        $HelpText = '
            <strong>Field: Overdue days</strong><br /><br />
            A record becomes overdue if one of the following is true:<br /><br />
            1. The number of days from date reported to the current date is equal to or greater than the number of days set in the \'Overall\' field.<br /><br />
            2. The number of days that a record has been held at its current approval status is equal to or greater than the number of days set for that approval status.<br /><br />
            To disable any of the overdue settings, add <br />\'-1\' to the field, or blank it out.';

        $Table->MakeRow('<b>Overdue days</b>'.AddHelpBubble(array('title' => 'Overdue days', 'field' => 'DIF_OVERDUE_DAYS', 'help' => $HelpText)).'<br />
            <div class="field_error" id="errDIF_OVERDUE_DAYS" style="display:none; font-weight:bold;">Overdue days must be a numeric value greater than or equal to -1</div><br />
            Set overdue timescales in this section.  Click the Help icon to view details on how this facility operates.',
            $FieldObj->MakeCustomField($OverdueDaysSection));

        $PermissionOptions = array(
            'N' => 'Never',
            'Y' => 'If current handler',
            'A' => 'Always',

        );

        $field = \Forms_SelectFieldFactory::createSelectField('REASSIGN_HANDLER', '', $Settings["REASSIGN_HANDLER"], '');
        $field->setCustomCodes($PermissionOptions);

        $Table->MakeRow('<b>Reassigning '._tk("INCNames").'</b><br />
            Specify if and when users with \'DIF2 access and review of DIF1\' permissions are able to change the Handler on '._tk("INCNames"),
            $field);

        if (bYN(GetParm('SEC_RECORD_PERMS', 'N')))
        {
            $PermissionOptions = array(
                'A' => 'Administrator Only',
                'H' => 'Handler and Administrator'
            );

            $field = \Forms_SelectFieldFactory::createSelectField('GRANT_ACCESS_PERMISSIONS', '', $Settings["GRANT_ACCESS_PERMISSIONS"], '');
            $field->setCustomCodes($PermissionOptions);
            $Table->MakeRow('<b>Which users can grant record level access permissions?</b><br />
                Select which users can grant record-level access permissions.  This option can be overridden by a user\'s individual security settings.',
                $field);
        }

        $Table->MakeRow('<b>Hide Contacts tab</b><br />
            If this option is set, the Contacts side-menu item and linked contacts page will be hidden for users with read-only access to DIF2 and users with access to DIF2 but no access to review '._tk('INCNames').' submitted on DIF1.  It will not affect users with other access permissions.',
            $FieldObj->MakeDivYesNo('DIF2_HIDE_CONTACTS', "" , GetParm('DIF2_HIDE_CONTACTS', '', true)));

        $Table->MakeRow('<b>Populate \'Reporter\' section of DIF2 with logged-in user\'s information?</b>',
            $FieldObj->MakeDivYesNo('DIF2_REPORTER_POPULATE', "" , GetParm('DIF2_REPORTER_POPULATE', '', true)));

        $Table->MakeRow('<b>Allow all users to see audited incident data?</b>',
            $FieldObj->MakeDivYesNo('INC_SHOW_AUDIT', "" , GetParm('INC_SHOW_AUDIT', '', true)));

        $Table->MakeRow('<b>Use DIF1 form for new '._tk('INCNames').'?</b><br />
            If this option is set to "Yes", the DIF1 form will always be used when adding a new '._tk('INCName').'. If set to "No", a DIF2 form will sometimes be used, depending on the access level of the user.',
            $FieldObj->MakeDivYesNo('NEW_RECORD_LVL1_INC', "", $Settings["NEW_RECORD_LVL1_INC"]));

        $Table->MakeRow('<b>Display '._tk('INCName').' CCS2 fields?</b>', $FieldObj->MakeDivYesNo('CCS2_INC', "", $Settings["CCS2_INC"]));

        $Table->MakeRow('<b>Show advanced settings?</b>',
            $FieldObj->MakeDivCheckbox('show_advanced', 'advanced', false, 'show_advanced'));

        // ADVANCED SETTINGS SECTION
        $AdvTable = new \FormTable;

        $AdvTable->MakeTitleRow('<b>Advanced settings</b>');

        $AdvTable->MakeRow('<b>Reporter is linked as a contact</b><br />
            Tick this box if you want the reporter of the '._tk("INCName").' to be created as a linked contact within the Datix database, rather than in the "Reported by" fields.',
            $FieldObj->MakeDivYesNo('DIF_1_CONTACT_REP', "", $Settings["DIF_1_CONTACT_REP"]));

        $ReportedByArray = array(
            "con_type" => "Contact type",
            "con_subtype" => "Contact subtype/status"
        );

        $field = \Forms_SelectFieldFactory::createSelectField('DIF_2_REPORTEDBY', '', $Settings["DIF_2_REPORTEDBY"], '');
        $field->setCustomCodes($ReportedByArray);

        $AdvTable->MakeRow('<b>Field to use for the reporter\'s job title</b><br />
            Only applies if the reporter is linked as a contact. Determines which of the contact type fields will by used to populate the "Title" field in the "Details of person reporting the '._tk("INCNames").'" section on DIF2. ',
            $field);

        $AdvTable->MakeRow('<b>Use the WHERE clause when determining who to e-mail</b><br />
            If this option is set, the WHERE clause from a user\'s Permissions section in the main application will determine whether the user receives an email on submission of a DIF1',
            $FieldObj->MakeDivYesNo('DIF_1_EMAIL_SELECT', "", $Settings["DIF_1_EMAIL_SELECT"]));

        $AdvTable->MakeRow('<b>WHERE clause overrides user\'s location and type settings</b><br />
            If this option is set, the WHERE clause from a user\'s security settings will be used instead of any location and type settings that are specified on the user\'s settings page.  If no WHERE clause exists for the user, the location and type settings will still be used.',
            $FieldObj->MakeDivYesNo('DIF_WHERE_OVERRIDE', "", $Settings["DIF_WHERE_OVERRIDE"]));

        $AdvTable->MakeRow('<b>Users must log in to submit an '._tk("INCName").' report</b><br />
            If this option is set, users must have a login in order to submit an '._tk("INCName").' report form.',
            $FieldObj->MakeDivYesNo('DIF_NO_OPEN', "", $Settings["DIF_NO_OPEN"]));

        $HelpText = '
            <strong>Field: Send email notifications to new Handler and Investigators</strong><br /><br />
            If this option is set to \'Yes\', email notifications will be sent to the new Handler and to new Investigators when the record is saved after updating these fields.
            <br /><br />
            If this option is set to \'No\' or \'Choose\', emails will not be sent
        ';

        $AdvTable->MakeRow('<b>Send email notifications to new Handler and Investigators.</b>'.AddHelpBubble(array('title' => 'Send email notifications to new Handler and Investigators', 'field' => 'STAFF_CHANGE_EMAIL', 'help' => $HelpText)).'<br />',
            $FieldObj->MakeDivYesNo('STAFF_CHANGE_EMAIL', "", $Settings["STAFF_CHANGE_EMAIL"]));

        $HelpText = 'Users will receive notifications only if their security group settings match the record data and they were not emailed at the previous save. This will include new users added since the record was last saved and any user whose security group settings match following an update to the record.';

        $AdvTable->MakeRow("<b>Send email notifications for updated incident records</b>".AddHelpBubble(array('title' => 'Send email notifications for updated incident records', 'field' => 'RECORD_UPDATE_EMAIL', 'help' => $HelpText))."<br/>
        If this option is turned on, email notifications will be sent based upon 'Email notification' security groups, when the record is saved at the DIF2 stage",
            $FieldObj->MakeDivYesNo('RECORD_UPDATE_EMAIL', "", $Settings["RECORD_UPDATE_EMAIL"]));

        if (!$Settings["WORKFLOW"])
        {
            // Make it obvious which workflow is used by default.
            $Settings["WORKFLOW"] = 2;
        }

        $WorkflowArray = array(
            "1" => "Workflow 1",
            "2" => "Workflow 2",
            "3" => "Workflow 3",
            "4" => "Workflow 4",
            "5" => "Workflow 5",
            "6" => "Workflow 6",
            "7" => "Workflow 7",
        );

        $field = \Forms_SelectFieldFactory::createSelectField('WORKFLOW', '', $Settings["WORKFLOW"], '');
        $field->setCustomCodes($WorkflowArray);
        $field->setOnChangeExtra('setChanged(\'WORKFLOW\')');
        $field->setSuppressCodeDisplay();

        $AdvTable->MakeRow('<b>Please select the workflow you would like to use</b><br />
            Select the workflow to be used for the management of '._tk('INCNames').'. This setting will determine which approval statuses will be displayed to users of this module; be aware that a change to the workflow could mean that an approval status is removed and that any records at that approval status will no longer be displayed. Ensure that you read the appropriate section of the manual before making any change to this setting. <input type="hidden" value="0" id="CHANGED-WORKFLOW" name="CHANGED-WORKFLOW" />',
            $field);
        
        $AdvTable->MakeRow('<b>Use CCS2 for XML Export to NRLS</b>', $FieldObj->MakeDivYesNo('EXPORT_CCS2', "", $Settings["EXPORT_CCS2"]));

        $Table->MakeTable();
        $AdvTable->MakeTable();
        $AdvTable->MakeSection('advanced', false);

        // PALS Module Settings
        $PALTable = new \FormTable();

        $PALTable->MakeTitleRow('<b>General Settings</b>');

        $PALTable->MakeRow('<b>Email '._tk("PALSName").' to reporter\'s manager</b><br />
            If this option is set, a dropdown will appear in PAL1 asking the reporter of the PALS record to select their manager.  That manager will then be e-mailed details of the PALS record.',
            $FieldObj->MakeDivYesNo('PAL_EMAIL_MGR', "", $Settings["PAL_EMAIL_MGR"]));

        $PALTable->MakeRow('<b>Restrict handlers to approving and viewing their own PALS records</b><br />
            If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',
            $FieldObj->MakeDivYesNo('PAL_OWN_ONLY', "", GetParm('PAL_OWN_ONLY', '', true)));

        $PALTable->MakeTitleRow('<b>PAL1 settings</b>');

        $PALTable->MakeRow('<b>PAL1 reference number prefix</b>
            <br />If you would like PALS records submitted via the web to be distinguished from those entered from a paper form, enter some characters here which will automatically be prepended to the numeric reference number that is automatically generated.',
            $FieldObj->MakeInputField("PAL_1_REF_PREFIX", 10, 20, $Settings["PAL_1_REF_PREFIX"]));

        $PALTable->MakeRow('<b>Users must log in to submit a PAL1 form</b><br />
            If this option is set, users must be logged in to Datix in order to submit a PAL1 form.',
            $FieldObj->MakeDivYesNo('PAL_NO_OPEN', "", $Settings["PAL_NO_OPEN"]));

        $PALTable->MakeRow('<b>Show reporter e-mail notifications?</b><br />
            Tick this box if you want the reporter to be shown the names and job titles of managers who are being sent e-mail notifications.',
            $FieldObj->MakeDivYesNo('PAL_SHOW_EMAIL', "", $Settings["PAL_SHOW_EMAIL"]));

        $PALTable->MakeRow('<b>Autopopulate record name?</b><br />
            If set to \'Yes\', the record name will be automatically populated with the name of the first Person Affected linked to the record.',
            $FieldObj->MakeDivYesNo('AUTO_POPULATE_REC_NAME_PAL', "", $Settings["AUTO_POPULATE_REC_NAME_PAL"]));

        $PALTable->MakeRow('<b>Contact matching listing design</b>',
            MakeListingFormDesignField('CON', $Settings["PAL_L1_CON_MATCH_LISTING_ID"], 1, '', 'PAL_L1_CON_MATCH_LISTING_ID'));

        $PALTable->MakeTitleRow('<b>PAL2 settings</b>');

        $PALTable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'PAL_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('PAL', \Forms_FormDesign::getGlobalFormDesignID('PAL', 2, 'Search')));

        $PALTable->MakeRow('<b>Allow all users to see audited PALS record data?</b>',
            $FieldObj->MakeDivYesNo('PAL_SHOW_AUDIT', "" , GetParm('PAL_SHOW_AUDIT', '', true)));

        $PALTable->MakeRow('<b>Use PAL1 form for new '._tk('PALSNames').'?</b><br />
            If this option is set to "Yes", the PAL1 form will always be used when adding a new '._tk('PALSName').'. If set to "No", a PAL2 form will sometimes be used, depending on the access level of the user.',
            $FieldObj->MakeDivYesNo('NEW_RECORD_LVL1_PAL', "", $Settings["NEW_RECORD_LVL1_PAL"]));

        $PALTable->MakeTable();

        // Claims Module Settings
        $CLATable = new \FormTable();

        $CLATable->MakeTitleRow('<b>General Settings</b>');

        $ExtraFields = \DatixDBQuery::PDO_fetch_all('SELECT recordid, fld_name FROM udf_fields LEFT JOIN UDF_MOD_LINK ON uml_id = recordid WHERE fld_type = \'M\' AND uml_module = \'CLA\'', array(), \PDO::FETCH_KEY_PAIR);

        $field = \Forms_SelectFieldFactory::createSelectField('CLA_FINANCE_UDFS', 'ADM', GetParm('CLA_FINANCE_UDFS', '', true), '', true);
        $field->setCustomCodes($ExtraFields);
        $field->setSuppressCodeDisplay();

        $CLATable->MakeRow('<b>Select extra fields to add to financial total</b><br />
            Fields selected here will be added to the standard finance fields when calculating the total. Note that any fields selected here will not appear automatically in the finance section of the form.', $field);

        $CLATable->MakeTitleRow('<b>CLAIM1 settings</b>');

        $CLATable->MakeRow('<b>Show reporter e-mail notifications?</b><br />
            Tick this box if you want the reporter to be shown the names and job titles of managers who are being sent e-mail notifications.',
            $FieldObj->MakeDivYesNo('CLA_SHOW_EMAIL', "", $Settings["CLA_SHOW_EMAIL"]));

        $CLATable->MakeRow('<b>Organisations matching listing design</b>',
            MakeListingFormDesignField('ORG', $Settings['CLA_L1_ORG_MATCH_LISTING_ID'], 1, '', 'CLA_L1_ORG_MATCH_LISTING_ID'));

        $CLATable->MakeTitleRow('<b>CLAIM2 settings</b>');

        $CLATable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'CLA_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('CLA', \Forms_FormDesign::getGlobalFormDesignID('CLA', 2, 'Search')));

        $CLATable->MakeRow('<b>Use CLAIM1 form for new '._tk('CLANames').'?</b><br />
            If this option is set to "Yes", the CLAIM1 form will always be used when adding a new '._tk('CLAName').'. If set to "No", a CLAIM2 form will sometimes be used, depending on the access level of the user.',
            $FieldObj->MakeDivYesNo('NEW_RECORD_LVL1_CLA', "", $Settings["NEW_RECORD_LVL1_CLA"]));

        $HelpText = '
            <strong>Field: Send email notifications to new Handler and Investigators</strong><br /><br />
            If this option is set to \'Yes\', email notifications will be sent to the new Handler and to new Investigators when the record is saved after updating these fields.
            <br /><br />
            If this option is set to \'No\' or \'Choose\', emails will not be sent
        ';

        $CLATable->MakeRow('<b>Send email notifications to new Handler and Investigators.</b>'.AddHelpBubble(array('title' => 'Send email notifications to new Handler and Investigators', 'field' => 'STAFF_CHANGE_EMAIL_CLA', 'help' => $HelpText)).'<br />',
            $FieldObj->MakeDivYesNo('STAFF_CHANGE_EMAIL_CLA', "", $Settings["STAFF_CHANGE_EMAIL_CLA"]));

        $CLATable->MakeRow('<b>Display '._tk('CLAName').' CCS2 fields?</b>', $FieldObj->MakeDivYesNo('CCS2_CLA', "", $Settings["CCS2_CLA"]));

        $ReserveReasons = array(
            'N' => _tk('reserve_reasons_N'),
            'O' => _tk('reserve_reasons_O'),
            'M' => _tk('reserve_reasons_M')
        );

        $field = \Forms_SelectFieldFactory::createSelectField('RESERVE_REASON_CLA', '', $Settings['RESERVE_REASON_CLA'], '');
        $field->setCustomCodes($ReserveReasons);

        $CLATable->MakeRow('<b>'._tk('CLA_reserve_text').'</b><br />'._tk('CLA_reserve_extratext'), $field);

        $claimCloseDate = array(
            'NONE' => 'None selected',
            'CLAIM' => 'Claim closed date only',
            'RES' => 'Reserve closed dates only',
            'BOTH' => 'Both claim and reserve closed dates'
        );
        $field = \Forms_SelectFieldFactory::createSelectField('CLAIM_CLOSED_DATES', '', $Settings['CLAIM_CLOSED_DATES'], '');
        $field->setCustomCodes($claimCloseDate);

        $HelpText = 'Select the closed dates to be populated with the current date when the '._tk('CLAName').' status is set to \'Closed\'<br/><br/>';
        $CLATable->MakeRow('<b>Populate closed dates when '._tk('CLAName').' is closed</b>'.AddHelpBubble(array('title'=>'Populate closed dates when '._tk('CLAName').' is closed','field' => 'CLAIM_CLOSED_DATES', 'help' => $HelpText)), $field);

        if (!$Settings["CLA_WORKFLOW"])
        {
            // Make it obvious which workflow is used by default.
            $Settings["CLA_WORKFLOW"] = 0;
        }

        $WorkflowArray = array(
            "0" => "Workflow 1",
            "1" => "Workflow 2",
        );

        $field = \Forms_SelectFieldFactory::createSelectField('CLA_WORKFLOW', '', $Settings["CLA_WORKFLOW"], '');
        $field->setCustomCodes($WorkflowArray);
        $field->setOnChangeExtra('setChanged(\'CLA_WORKFLOW\')');
        $field->setSuppressCodeDisplay();

        $CLATable->MakeRow('<b>Please select the workflow you would like to use</b><br />
            Select the workflow to be used for the management of '._tk('CLANames').'. This setting will determine which approval statuses will be displayed to users of this module; be aware that a change to the workflow could mean that an approval status is removed and that any records at that approval status will no longer be displayed. Ensure that you read the appropriate section of the manual before making any change to this setting. <input type="hidden" value="0" id="CHANGED-WORKFLOW" name="CHANGED-WORKFLOW" />',
            $field);

        $CLATable->MakeTable();

        $CLATable->MakeRow('<b>Show existing claims?</b><br/>If set to \'Yes\', existing claims with the same name will be showed when creating/editing a claim',
            $FieldObj->MakeDivYesNo('CLA_SHOW_DUPLICATES', "", $Settings["CLA_SHOW_DUPLICATES"]));


        // Complaints Module Settings
        $COMTable = new \FormTable();

        $COMTable->MakeTitleRow('<b>General Settings</b>');

        $COMTable->MakeRow('<b>Email '._tk("COMName").' to reporter\'s manager</b><br />
            If this option is set, a dropdown will appear in COM1 asking the reporter of the '._tk("COMName").' to select their manager.  That manager will then be e-mailed details of the '._tk("COMName").'.',
            $FieldObj->MakeDivYesNo('COM_EMAIL_MGR', "", $Settings["COM_EMAIL_MGR"]));

        $COMTable->MakeRow('<b>Restrict handlers to approving and viewing their own '._tk("COMNames").'</b><br />
            If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',
            $FieldObj->MakeDivYesNo('COM_OWN_ONLY', "", GetParm('COM_OWN_ONLY', '', true)));

        $COMTable->MakeRow('<b>Display '._tk('COMName').' CCS2 fields?</b>', $FieldObj->MakeDivYesNo('CCS2_COM', '', $Settings['CCS2_COM']));

        $COMTable->MakeTitleRow('<b>COM1 settings</b>');

        $COMTable->MakeRow('<b>COM1 reference number prefix</b>
            <br />If you would like '._tk("COMNames").' submitted via the web to be distinguished from those entered from a paper form, enter some characters here which will automatically be prepended to the numeric reference number that is automatically generated.',
            $FieldObj->MakeInputField("COM_1_REF_PREFIX", 10, 20, $Settings["COM_1_REF_PREFIX"]));

        $COMTable->MakeRow('<b>Users must log in to submit a COM1 form</b><br />
            If this option is set, users must be logged in to Datix in order to submit a COM1 form.',
            $FieldObj->MakeDivYesNo('COM_NO_OPEN', "", $Settings["COM_NO_OPEN"]));

        $COMTable->MakeRow('<b>Show reporter e-mail notifications?</b><br />
            Tick this box if you want the reporter to be shown the names and job titles of managers who are being sent e-mail notifications.',
            $FieldObj->MakeDivYesNo('COM_SHOW_EMAIL', "", $Settings["COM_SHOW_EMAIL"]));

        $COMTable->MakeRow('<b>Autopopulate record name?</b><br />
            If set to \'Yes\', the record name will be automatically populated with the name of the first Person Affected linked to the record.',
            $FieldObj->MakeDivYesNo('AUTO_POPULATE_REC_NAME_COM', "", $Settings["AUTO_POPULATE_REC_NAME_COM"]));

        $COMTable->MakeRow('<b>Contact matching listing design</b>',
            MakeListingFormDesignField('CON', $Settings["COM_L1_CON_MATCH_LISTING_ID"], 1, '', 'COM_L1_CON_MATCH_LISTING_ID'));

        $COMTable->MakeTitleRow('<b>COM2 settings</b>');
        $COMTable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'COM_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('COM', \Forms_FormDesign::getGlobalFormDesignID('COM', 2, 'Search')));

        $COMTable->MakeRow('<b>Allow all users to see audited complaints data?</b>',
            $FieldObj->MakeDivYesNo('COM_SHOW_AUDIT', "" , GetParm('COM_SHOW_AUDIT', '', true)));

        $COMTable->MakeRow('<b>Use COM1 form for new '._tk('COMNames').'?</b><br />
            If this option is set to "Yes", the COM1 form will always be used when adding a new '._tk('COMName').'. If set to "No", a COM2 form will sometimes be used, depending on the access level of the user.',
            $FieldObj->MakeDivYesNo('NEW_RECORD_LVL1_COM', "", $Settings["NEW_RECORD_LVL1_COM"]));

        $HelpText = '
            <strong>Field: Send email notifications to new Handler and Investigators</strong><br /><br />
            If this option is set to \'Yes\', email notifications will be sent to the new Handler and to new Investigators when the record is saved after updating these fields.
            <br /><br />
            If this option is set to \'No\' or \'Choose\', emails will not be sent
        ';

        $COMTable->MakeRow('<b>Send email notifications to new Handler and Investigators.</b>'.AddHelpBubble(array('title' => 'Send email notifications to new Handler and Investigators', 'field' => 'STAFF_CHANGE_EMAIL_COM', 'help' => $HelpText)).'<br />',
            $FieldObj->MakeDivYesNo('STAFF_CHANGE_EMAIL_COM', "", $Settings["STAFF_CHANGE_EMAIL_COM"]));

        $COMTable->MakeTitleRow('<b>Complainant Chain settings</b>');

        $calendarOrWorking = [
            'C' => 'Calendar days',
            'W' => 'Working Days'
        ];

        $field = \Forms_SelectFieldFactory::createSelectField('COM_COMPLAINT_CHAIN_TIMESCALES', '', GetParm('COM_COMPLAINT_CHAIN_TIMESCALES', 'W', true), '');
        $field->setCustomCodes($calendarOrWorking);
        $field->setSuppressCodeDisplay();

        $COMTable->MakeRow('<b>' . _tk('complainant_chain_days') . '</b>'.AddHelpBubble(array('title' => _tk('complainant_chain_days'), 'field' => 'COM_COMPLAINT_CHAIN_TIMESCALES', 'help' => _tk('complainant_chain_days_help'))), $field);

        $COMTable->MakeRow('<b>Include Acknowledged?</b>',
            $FieldObj->MakeDivYesNo('SHOW_ACKNOWLEDGED', "" , GetParm('SHOW_ACKNOWLEDGED', 'Y', true)));

        $COMTable->MakeRow('<b>Include Actioned?</b>',
            $FieldObj->MakeDivYesNo('SHOW_ACTIONED', "" , GetParm('SHOW_ACTIONED', 'Y', true)));

        $COMTable->MakeRow('<b>Include Response?</b>',
            $FieldObj->MakeDivYesNo('SHOW_RESPONSE', "" , GetParm('SHOW_RESPONSE', 'Y', true)));

        $COMTable->MakeRow('<b>Include Holding?</b>',
            $FieldObj->MakeDivYesNo('SHOW_HOLDING', "" , GetParm('SHOW_HOLDING', 'Y', true)));

        $COMTable->MakeRow('<b>Include Replied?</b>',
            $FieldObj->MakeDivYesNo('SHOW_REPLIED', "" , GetParm('SHOW_REPLIED', 'Y', true)));

        $COMTable->MakeTitleRow('<b>Health Care Commission settings</b>');

        $COMTable->MakeRow('<b>Days between Date of request and Initial review decision due date</b>
            '.AddHelpBubble(array(
                'title' => 'Days between Date of request and Initial review decision due date.',
                'field' => 'REV_DECISION_TIME',
                'help' => 'The number of days from the Date of request used to calculate the Initial review decision due date.'
            )),
            $FieldObj->MakeNumberField('REV_DECISION_TIME', GetParm('REV_DECISION_TIME', '20', true)));

        $COMTable->MakeRow('<b>Days between the Initial review decision date and HC terms due date.</b>
            '.AddHelpBubble(array(
                'title' => 'Days between the Initial review decision date and the HC terms due date.',
                'field' => 'REV_TERMS_TIME',
                'help' => 'The number of days from the Decision taken to calculate the HC terms due date.'
            )),
            $FieldObj->MakeNumberField('REV_TERMS_TIME', GetParm('REV_TERMS_TIME', '10', true)));

        $COMTable->MakeRow('<b>Days between the HC terms sent and Terms of reference comments due date.</b>
            '.AddHelpBubble(array(
                'title' => 'Days between the HC terms sent and Terms of reference comments due date.',
                'field' => 'REV_COMMENTS_TIME',
                'help' => 'The number of days from the HC terms sent to calculate the Terms of reference comments due date.'
            )),
            $FieldObj->MakeNumberField('REV_COMMENTS_TIME', GetParm('REV_COMMENTS_TIME', '10', true)));

        $COMTable->MakeRow('<b>Days between the Terms of reference comments date and Investigation completion due date.</b>
            '.AddHelpBubble(array(
                'title' => 'Days between the Terms of reference comments date and Investigation completion due date.',
                'field' => 'REV_INV_TIME',
                'help' => 'The number of days from the Terms of reference comments date to calculate the Investigation completion due date.'
            )),
            $FieldObj->MakeNumberField('REV_INV_TIME', GetParm('REV_INV_TIME', '180', true)));

        $COMTable->MakeTitleRow('<b>Independent Review settings</b>');

        $COMTable->MakeRow('<b>Days between Date received and Request acknowledged due.</b>',
            $FieldObj->MakeNumberField('IR_ACKDAYS', GetParm('IR_ACKDAYS', '2', true)));

        $COMTable->MakeRow('<b>Days between Request acknowledged due and Lay chair appointed due.</b>',
            $FieldObj->MakeNumberField('IR_LAYCHAIRDAYS', GetParm('IR_LAYCHAIRDAYS', '4', true)));

        $COMTable->MakeRow('<b>Days between Lay chair appointed due and Decision due.</b>',
            $FieldObj->MakeNumberField('IR_DECISIONDAYS', GetParm('IR_DECISIONDAYS', '20', true)));

        $COMTable->MakeRow('<b>Days between Decision due and Panel appointed due.</b>',
            $FieldObj->MakeNumberField('IR_PANELDAYS', GetParm('IR_PANELDAYS', '20', true)));

        $COMTable->MakeRow('<b>Days between Panel appointed due and Draft report published due.</b>',
            $FieldObj->MakeNumberField('IR_DRAFTDAYS', GetParm('IR_DRAFTDAYS', '50', true)));

        $COMTable->MakeRow('<b>Days between Draft report published due and Final report published due.</b>',
            $FieldObj->MakeNumberField('IR_FINALDAYS', GetParm('IR_FINALDAYS', '10', true)));

        $COMTable->MakeRow('<b>Days between Final report published due and C.E. reply to complainant due.</b>',
            $FieldObj->MakeNumberField('IR_REPLYDAYS', GetParm('IR_REPLYDAYS', '20', true)));

        if (GetParm('COUNTRY', 'ENGLAND') == 'SCOTLAND' || bYN(GetParm('ENABLE_ISD', 'N')) == 'Y')
        {
            $field = \Forms_SelectFieldFactory::createSelectField('ISD_A2', '', GetParm('ISD_A2', 'REF', true), '');
            $field->setCustomCodes(array('REF' => 'Reference', 'ID' => 'ID'));
            $field->setSuppressCodeDisplay();

            $COMTable->MakeTitleRow('<b>ISD export settings</b>');

            $COMTable->MakeRow('<b>Include ID or Reference field in export.</b>',
                $field);
        }

        $COMTable->MakeTable();

        // Risk Module Settings
        $RAMTable = new \FormTable();

        $RAMTable->MakeTitleRow('<b>General Settings</b>');

        $RAMTable->MakeRow('<b>Restrict handlers to approving and viewing their own risks</b><br />
            If you set this option, the user will only be able to see records where their name appears in the Handler dropdown. This option does not apply to users with input-only and final approval access.',
            $FieldObj->MakeDivYesNo('RISK_OWN_ONLY', "", GetParm('RISK_OWN_ONLY', '', true)));

        $RAMTable->MakeRow('<b>Display \'Assurance Framework\' report in main menu?</b><br />
            If you set this option, \'Assurance Framework\' report will be available on the main Risk menu.',
            $FieldObj->MakeDivYesNo('ASSURANCE_FRAMEWORK', "", GetParm('ASSURANCE_FRAMEWORK', '', true)));

        $RAMTable->MakeTitleRow('<b>RISK1 settings</b>');

        $RAMTable->MakeRow('<b>Email risk to handler</b><br />
            If this option is set, the person selected as the handler of the risk will be e-mailed details of the risk.',
            $FieldObj->MakeDivYesNo('RISK_EMAIL_MGR', "", $Settings["RISK_EMAIL_MGR"]));

        $RAMTable->MakeRow('<b>E-mail acknowledgment to reporter?</b><br />
            If this option is set, an acknowledgment e-mail will be sent to the person submitting the risk.',
            $FieldObj->MakeDivYesNo('RISK_1_ACKNOWLEDGE', "", $Settings["RISK_1_ACKNOWLEDGE"]));

        $RAMTable->MakeRow('<b>Show reporter e-mail notifications?</b><br />
            Tick this box if you want the reporter to be shown the names and job titles of managers who are being sent e-mail notifications.',
            $FieldObj->MakeDivYesNo('RAM_SHOW_EMAIL', "", $Settings["RAM_SHOW_EMAIL"]));

        $RAMTable->MakeRow('<b>Contact matching listing design</b>',
            MakeListingFormDesignField('CON', $Settings["RAM_L1_CON_MATCH_LISTING_ID"], 1, '', 'RAM_L1_CON_MATCH_LISTING_ID'));

        $RAMTable->MakeTitleRow('<b>RISK2 settings</b>');

        $RAMTable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'RAM_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('RAM', \Forms_FormDesign::getGlobalFormDesignID('RAM', 2, 'Search')));

        $RAMTable->MakeRow('<b>Allow all users to see audited risk data?</b>',
            $FieldObj->MakeDivYesNo('RAM_SHOW_AUDIT', "" , GetParm('RAM_SHOW_AUDIT', '', true)));

        $RAMTable->MakeRow('<b>Use RISK1 form for new '._tk('RAMNames').'?</b><br />
            If this option is set to "Yes", the RISK1 form will always be used when adding a new '._tk('RAMName').'. If set to "No", a RISK2 form will sometimes be used, depending on the access level of the user.',
            $FieldObj->MakeDivYesNo('NEW_RECORD_LVL1_RAM', "", $Settings["NEW_RECORD_LVL1_RAM"]));

        $RAMTable->MakeRow('<b>Show advanced settings?</b>',
            $FieldObj->MakeDivCheckBox('show_advanced_risk', 'advanced_risk', false, 'show_advanced_risk'));

        $RAMTable->MakeTable();

        // ADVANCED SETTINGS SECTION
        $RAMAdvTable = new \FormTable;

        $RAMAdvTable->MakeTitleRow('<b>Advanced settings</b>');

        $RAMAdvTable->MakeRow('<b>Use the risk register WHERE clause when determining who to e-mail</b><br />
            If this option is set, the risk register WHERE clause from a user\'s security settings will determine whether the user receives an e-mail on submission of a RISK1s form.',
            $FieldObj->MakeDivYesNo('RISK_1_EMAIL_SELECT', "", $Settings["RISK_1_EMAIL_SELECT"]));

        $RAMAdvTable->MakeRow('<b>Risk register WHERE clause overrides user\'s location settings</b><br />
            If this option is set, the WHERE clause from a user\'s security settings will be used instead of any location settings that are specified on the user\'s settings page.  If no WHERE clause exists for the user, the location settings will still be used.',
            $FieldObj->MakeDivYesNo('RISK_WHERE_OVERRIDE', "", $Settings["RISK_WHERE_OVERRIDE"]));

        $RAMAdvTable->MakeRow('<b>Users must log in to submit a risk</b><br />
            If this option is set, users must have a login in order to submit a risk report form.',
            $FieldObj->MakeDivYesNo('RISK_NO_OPEN', "", $Settings["RISK_NO_OPEN"]));

        $RAMAdvTable->MakeTable();
        $RAMAdvTable->MakeSection('advanced_risk', false);

        // SABS Module Settings
        $SABTable = new \FormTable();

        $SABTable->MakeTitleRow('<b>General Settings</b>');

        $SABTable->MakeRow('<b>Safety Alerts administrator e-mail address</b><br />
            Only specify a value here if you want all safety alerts to be sent from the same address.  If you leave this blank, they will be sent from the address of the user who is logged in.',
            $FieldObj->MakeEmailField('SABS_E_MAIL_ADDRESS', 50, 254, $Settings["SABS_E_MAIL_ADDRESS"]));

        $SABTable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'SAB_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
        	MakeSearchFormDesignField('SAB', \Forms_FormDesign::getGlobalFormDesignID('SAB', 1, 'Search'),1));

        $SABTable->MakeTable();

        // Actions module settings
        $ACTTable = new \FormTable();

        $ACTTable->MakeTitleRow('<b>General Settings</b>');

        $ACTTable->MakeRow('<b>Restrict user to viewing their own actions</b><br />
            If you set this option, users will be able to access only those actions where their name appears in the \'Responsibility\' field.',
            $FieldObj->MakeDivYesNo('ACT_OWN_ONLY', "", GetParm('ACT_OWN_ONLY', '', true)));

        $HelpText = '
            <strong>Field: Send email notifications when '._tk("ACTNames").' are created, updated and closed</strong><br /><br />
            If this option is set to \'Yes\', email notifications will be sent to:<br />
            The user who has created the action<br />
            The user who is responsible for the action<br />
            (except where the recipient is the user logged in)
            <br /><br />
            If this option is set to \'No\' or \'Choose\', emails will not be sent
        ';

        $ACTTable->MakeRow('<b>Send email notifications when '._tk("ACTNames").' are created, updated and closed.</b>'.AddHelpBubble(array('title' => 'Send email notifications when '._tk('ACTNames').' are created, updated and closed', 'field' => 'ACT_EMAIL', 'help' => $HelpText)).'<br />',
            $FieldObj->MakeDivYesNo('ACT_EMAIL', "", $Settings["ACT_EMAIL"]));

        $ACTTable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'ACT_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('ACT', \Forms_FormDesign::getGlobalFormDesignID('ACT', 2, 'Search')));

        $ACTTable->MakeRow('<b>Copy locations?</b><br/>If set to \'Yes\', location information from the linked record will be automatically copied over when creating a new action',
            $FieldObj->MakeDivYesNo('ACT_COPY_LOCATIONS', "", $Settings["ACT_COPY_LOCATIONS"]));

        $ACTTable->MakeRow('<b>Allow all users to see audited action data?</b>',
            $FieldObj->MakeDivYesNo('ACT_SHOW_AUDIT', "" , GetParm('ACT_SHOW_AUDIT', '', true)));

        $ACTTable->MakeRow('<b>The number of days before the due date that the email reminder will be sent. (This is in calendar days)</b><br />
            If this option is set, users in the \'Responsibility\' field will receive email reminders for upcoming actions. A Datix service will need to be configured in order for this functionality to work.',
            $FieldObj->MakeNumberField('REM_DAYS_ACT', GetParm('REM_DAYS_ACT', '2', true)));

        $ACTTable->MakeTable();

        // Contacts Module Settings
        $CONTable = new \FormTable();

        $CONTable->MakeTitleRow('<b>CON1 settings</b>');

        $CONTable->MakeRow(
            '<b>' . _tk('enable_contact_searching') . '</b>',
            $FieldObj->MakeDivYesNo('CON_PAS_CHECK', "", GetParm('CON_PAS_CHECK', '', true))
        );

        $conPasChkFields = explode(',', GetParm('CON_PAS_CHK_FIELDS', '', true));

        $field = new \Forms_MultiSelectField('CON_PAS_CHK_FIELDS', 'ADM', $conPasChkFields, 'New');

        $values = array();

        foreach ($FieldDefs['CON'] as $key => $fieldDef)
        {
            if (in_array($key, $ModuleDefs['INC']['DIF1_CON_SEARCHING_FIELDS']))
            {
                $values[$key] = $fieldDef['Title'];
            }
        }

        $sql = "
            SELECT
                ('UDF_' + udf_fields.fld_type + '_0_' + CAST(udf_fields.recordid AS VARCHAR(32))) AS fmt_field, fld_name
            FROM
                udf_fields
            LEFT JOIN
                udf_mod_link ON udf_mod_link.uml_id = udf_fields.recordid
            WHERE
                udf_mod_link.uml_module = 'CON' AND udf_fields.fld_type != 'L'
        ";

        $extraFieldValues = \DatixDBQuery::PDO_fetch_all($sql, array(), \PDO::FETCH_KEY_PAIR);

        $values = array_merge($values, $extraFieldValues);
        natcasesort($values);

        $field->setCustomCodes($values);

        $CONTable->MakeRow(
            '<b>' . _tk('contact_searching_fields') . '</b><br />' . _tk('contact_searching_hint'),
            $field
        );

        $codes = Registry::getInstance()->getFieldDefs()['link_contacts.link_role']->getCodes();

        $field = \Forms_SelectFieldFactory::createSelectField('REPORTER_ROLE', '', $Settings["REPORTER_ROLE"], '');
        $field->setCustomCodeCollection($codes);

        $CONTable->MakeRow('<b>Role representing the reporter</b><br />
            Select the role that is used to represent the reporter of a record. Acknowledgement e-mails etc. will be triggered based on this setting.',
            $field);

        $CONTable->MakeTitleRow('<b>CON2 settings</b>');

        $CONTable->MakeRow(
            '<b>' . _tk('search_form_design') . '</b>' . AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'CON_SEARCH_FORM', 'help' => _tk('search_form_design_help'))) . '<br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('CON', \Forms_FormDesign::getGlobalFormDesignID('CON', 2, 'Search'))
        );

        $CONTable->MakeRow('<b>Use soundex when matching contacts</b>',
            $FieldObj->MakeDivYesNo('CON_SEARCH_SOUNDEX', "", $Settings["CON_SEARCH_SOUNDEX"]));

        $CONTable->MakeRow(
            '<b>Allow users to reject contacts</b><br />With this option set to \'Yes\', users will be able to reject contacts.  This setting can be overridden for specific users on their user record.',
            $FieldObj->MakeDivYesNo('CON_SHOW_REJECT_BTN', "", GetParm('CON_SHOW_REJECT_BTN', '', true))
        );

        $field = \Forms_SelectFieldFactory::createSelectField('CON_MATCH_LISTING_ID', '', $Settings['CON_MATCH_LISTING_ID'], '');
        $field->setCustomCodes(GetListOfListingDesigns(array('module' => 'CON', 'level' => 2)));
        $field->setSuppressCodeDisplay();

        $CONTable->MakeRow('<b>' . _tk('con_match_listing') . '</b>', $field);

        $CONTable->MakeTable();

        // Equipment Module Settings
        $ASTTable = new \FormTable();

        $ASTTable->MakeTitleRow('<b>General Settings</b>');

        $ASTTable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'AST_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('AST', \Forms_FormDesign::getGlobalFormDesignID('AST', 2, 'Search')));

        $ASTTable->MakeTable();

        // Standards Module Settings
        $STNTable = new \FormTable();

        $STNTable->MakeTitleRow('<b>General Settings</b>');

        $STNTable->MakeRow('<b>Show "Add new" option</b><br />
            Display "Add new" option to add a new record.  This is switched on by default.',
            $FieldObj->MakeDivYesNo('STN_ADD_NEW', "", $Settings["STN_ADD_NEW"]));

        $HelpText = '
            <strong>Field: Send email notifications to new '._tk('ELEName').' Handler</strong><br /><br />
            If this option is set to \'Yes\', an email notification will be sent to the new Handler when the '._tk('ELEName').' is saved.
            <br /><br />
            If this option is set to \'No\' or \'Choose\', the email will not be sent
        ';

        $STNTable->MakeRow('<b>Send email notification to new '._tk('ELEName').' Handler.</b>'.AddHelpBubble(array('title' => 'Send email notification to new Handler', 'field' => 'STAFF_CHANGE_EMAIL_ELE', 'help' => $HelpText)).'<br />',
            $FieldObj->MakeDivYesNo('STAFF_CHANGE_EMAIL_ELE', "", $Settings["STAFF_CHANGE_EMAIL_ELE"]));

        $STNTable->MakeTable();

         // CQC Module Settings
        $CQCTable = new \FormTable();

        $CQCTable->MakeTitleRow('<b>General Settings</b>');

        if (!$Settings['CQC_ROLLUP_TYPE'])
        {
            $Settings['CQC_ROLLUP_TYPE'] = 'NONE';
        }

        $field = \Forms_SelectFieldFactory::createSelectField('CQC_ROLLUP_TYPE', '', $Settings['CQC_ROLLUP_TYPE'], '');
        $field->setCustomCodes(array('NONE' => 'No Scoring', 'AVE' => 'Average scoring', 'WORST' => 'Worst-case scoring'));
        $field->setSuppressCodeDisplay();

        $CQCTable->MakeRow('<b>Status scoring regime</b><br />
            This option defines the scoring regime used for calculating the status of parent Outcomes and Prompts.' .
            AddHelpBubble(array(
                'title' => 'Status scoring regime',
                'field' => 'CQC_ROLLUP_TYPE',
                'help' => '<p>This option defines the scoring regime used for calculating the status of parent Outcomes and Prompts.</p>
                            <p>If \'No scoring\' is selected, a parent record (Outcome or Prompt) will not inherit scores from its associated child records.</p>
                            <p>If \'Average scoring\' is selected, a parent record (Outcome or Prompt) will inherit the average status of all its associated child records.</p>
                            <p>If \'Worst-case scoring\' is selected, a parent record (Outcome or Prompt) will inherit the lowest status recorded across its associated child records.</p>'
                )),
            $field);

        $CQCTable->MakeTable();

        // Medication Module Settings
        $MEDTable = new \FormTable();

        $MEDTable->MakeTitleRow('<b>General Settings</b>');

        $MEDTable->MakeRow('<b>Use Soundex for Medications searching</b><br />
            This is switched on by default.',
            $FieldObj->MakeDivYesNo('MED_SEARCH_SOUNDEX', "", $Settings["MED_SEARCH_SOUNDEX"]));

        $MEDTable->MakeRow('<b>'._tk('use_dmd').'</b><br />'._tk('use_dmd_tip'),
            $FieldObj->MakeDivYesNo('DMD_ENABLED', "", $Settings["DMD_ENABLED"]));

        $MEDTable->MakeTable();

        // Payments Module Settings
        $PAYTable = new \FormTable();

        $PAYTable->MakeTitleRow('<b>General Settings</b>');

        $PAYTable->MakeRow('<b>'._tk('search_form_design').'</b>
            '.AddHelpBubble(array('title' => _tk('search_form_design'), 'field' => 'PAY_SEARCH_FORM', 'help' => _tk('search_form_design_help'))).'
            <br />' . _tk('search_form_design_explanatory'),
            MakeSearchFormDesignField('PAY', \Forms_FormDesign::getGlobalFormDesignID('PAY', 2, 'Search')));

        $PAYTable->MakeRow('<b>Allow users to add new recipients?</b><br/>If set to \'Yes\', logged in users will be able to add new recipients for payments',
            $FieldObj->MakeDivYesNo('PAY_ADD_NEW_RECIPIENT', "", $Settings["PAY_ADD_NEW_RECIPIENT"]));

        $PAYTable->MakeTable();

        // To Do List Module Settings
        $TODTable = new \FormTable();

        $TODTable->MakeTitleRow('<b>To Do List settings</b>');

        $TOD_USERS_ACT_Fields = explode(',', GetParm('TOD_USERS_ACT', '', true));

        $field = new \Forms_MultiSelectField('TOD_USERS_ACT', 'ADM', $TOD_USERS_ACT_Fields, 'New');

        $values = array();

        foreach ($FieldDefs['ACT'] as $key => $fieldDef)
        {
            if (in_array($key, $ModuleDefs['TOD']['TOD_USERS_FIELDS']['ACT']))
            {
                $values[$key] = $fieldDef['Title'];
            }
        }

        $field->setCustomCodes($values);

        $TODTable->MakeRow(
            '<b>' . _tk('todo_list_staff_fields_act') . '</b><br />' . _tk('todo_list_staff_fields_hint'),
            $field
        );

        $TOD_USERS_INC_Fields = explode(',', GetParm('TOD_USERS_INC', '', true));

        $field = new \Forms_MultiSelectField('TOD_USERS_INC', 'ADM', $TOD_USERS_INC_Fields, 'New');

        $values = array();

        foreach ($FieldDefs['INC'] as $key => $fieldDef)
        {
            if (in_array($key, $ModuleDefs['TOD']['TOD_USERS_FIELDS']['INC']))
            {
                $values[$key] = $fieldDef['Title'];
            }
        }

        $field->setCustomCodes($values);

        $TODTable->MakeRow(
            '<b>' . _tk('todo_list_staff_fields_inc') . '</b><br />' . _tk('todo_list_staff_fields_hint'),
            $field
        );

        $TOD_USERS_COM_Fields = explode(',', GetParm('TOD_USERS_COM', '', true));

        $field = new \Forms_MultiSelectField('TOD_USERS_COM', 'ADM', $TOD_USERS_COM_Fields, 'New');

        $values = array();

        foreach ($FieldDefs['COM'] as $key => $fieldDef)
        {
            if (in_array($key, $ModuleDefs['TOD']['TOD_USERS_FIELDS']['COM']))
            {
                $values[$key] = $fieldDef['Title'];
            }
        }

        $field->setCustomCodes($values);

        $TODTable->MakeRow(
            '<b>' . _tk('todo_list_staff_fields_com') . '</b><br />' . _tk('todo_list_staff_fields_hint'),
            $field
        );

        $TOD_USERS_RAM_Fields = explode(',', GetParm('TOD_USERS_RAM', '', true));

        $field = new \Forms_MultiSelectField('TOD_USERS_RAM', 'ADM', $TOD_USERS_RAM_Fields, 'New');

        $values = array();

        foreach ($FieldDefs['RAM'] as $key => $fieldDef)
        {
            if (in_array($key, $ModuleDefs['TOD']['TOD_USERS_FIELDS']['RAM']))
            {
                $values[$key] = $fieldDef['Title'];
            }
        }

        $field->setCustomCodes($values);

        $TODTable->MakeRow(
            '<b>' . _tk('todo_list_staff_fields_ram') . '</b><br />' . _tk('todo_list_staff_fields_hint'),
            $field
        );

        $todUsersCQCFields = explode(',', GetParm('TOD_USERS_CQC', '', true));

        $field = new \Forms_MultiSelectField('TOD_USERS_CQC', 'ADM', $todUsersCQCFields, 'New');
        $field->setCustomCodes(array('cqc_handler' => 'Handlers', 'cqc_manager' => 'Manager'));

        $TODTable->MakeRow(
            '<b>' . _tk('todo_list_staff_fields_cqc') . '</b><br />' . _tk('todo_list_staff_fields_hint'),
            $field
        );

        $TODTable->MakeTable();

        // Accreditation Module Settings
        $AMOTable = new \FormTable();

        $AMOTable->MakeTitleRow('<b>General Settings</b>');

        $field = \Forms_SelectFieldFactory::createSelectField('AMO_ROLLUP_TYPE', '', $Settings['AMO_ROLLUP_TYPE'], '');
        $field->setCustomCodes(array('AVE' => 'Average scoring', 'WORST' => 'Worst-case scoring'));
        $field->setSuppressCodeDisplay();

        $AMOTable->MakeRow('<b>Compliance level scoring regime</b><br />
            This option defines the scoring regime used for calculating the compliance level of assessments.' .
            AddHelpBubble(array('title' => 'Compliance level scoring regime',
                'field' => 'AMO_ROLLUP_TYPE',
                'help' => '<p>This option determines the method used for calculating assessment-level compliance.</p>
                            <p>Worst-case: The compliance displayed on the assessment record will be the lowest-scoring compliance level recorded against any of the criteria in the assessment. </p>
                            <p>Average: The compliance displayed on the assessment record will be the average compliance level (based upon average score) recorded against the criteria in the assessment.</p>
                        <p>The default setting is \'Average scoring\' </p>'
                )),
            $field);

        $AMOTable->MakeTable();

        // Organisations Module Settings
        $ORGTable = new \FormTable();

        $ORGTable->MakeTitleRow('<b>ORG1 settings</b>');

        $orgPasChkFields = explode(',', GetParm('ORG_PAS_CHK_FIELDS', '', true));

        $field = new \Forms_MultiSelectField('ORG_PAS_CHK_FIELDS', 'ORG', $orgPasChkFields, 'New');

        $values = array();

        foreach ($FieldDefs['ORG'] as $key => $fieldDef)
        {
            if (in_array($key, $ModuleDefs['CLA']['CLA1_ORG_SEARCHING_FIELDS']))
            {
                $values[$key] = $fieldDef['Title'];
            }
        }

        $sql = "
            SELECT
                ('UDF_' + udf_fields.fld_type + '_0_' + CAST(udf_fields.recordid AS VARCHAR(32))) AS fmt_field, fld_name
            FROM
                udf_fields
            LEFT JOIN
                udf_mod_link ON udf_mod_link.uml_id = udf_fields.recordid
            WHERE
                udf_mod_link.uml_module = 'ORG' AND udf_fields.fld_type != 'L'
        ";

        $extraFieldValues = \DatixDBQuery::PDO_fetch_all($sql, array(), \PDO::FETCH_KEY_PAIR);

        $values = array_merge($values, $extraFieldValues);
        natcasesort($values);

        $field->setCustomCodes($values);
        $field->setSuppressCodeDisplay();

        $ORGTable->MakeRow(
            '<b>' . _tk('organisation_searching_fields') . '</b><br />' . _tk('organisation_searching_hint'),
            $field
        );

        $this->response->build('src/admin/views/ShowDatixwebConf.php', array(
            'ALLTable' => $ALLTable,
            'ALLAdvTable' => $ALLAdvTable,
            'Table' => $Table,
            'AdvTable' => $AdvTable,
            'PALTable' => $PALTable,
            'CLATable' => $CLATable,
            'COMTable' => $COMTable,
            'RAMTable' => $RAMTable,
            'RAMAdvTable' => $RAMAdvTable,
            'SABTable' => $SABTable,
            'ACTTable' => $ACTTable,
            'CONTable' => $CONTable,
            'ASTTable' => $ASTTable,
            'STNTable' => $STNTable,
            'CQCTable' => $CQCTable,
            'MEDTable' => $MEDTable,
            'PAYTable' => $PAYTable,
            'TODTable' => $TODTable,
            'AMOTable' => $AMOTable,
            'ORGTable' => $ORGTable,
            'module' => $module,
            'ButtonGroup' => $ButtonGroup
        ));
    }
}
