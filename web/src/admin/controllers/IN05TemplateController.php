<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class IN05TemplateController extends TemplateController
{
    public function in05()
    {
        if ($error)
        {
            echo 'ERROR: '.$error;
            obExit();
        }

        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        $this->title = _tk('in05');
        $this->module = 'ADM';

        $FormArray = array (
            "Parameters" => array(
                "Panels" => "True",
                "Condition" => false
            ),
            "name" => array(
                "Title" => $this->title,
                "Function" => "MakeNPSATable",
                "Rows" => array()
            )
        );

        $IN05Table = new \FormTable();
        $IN05Table->MakeForm($FormArray, $data, $this->module);
        $IN05Table->MakeTable();

        $showDesc = $_GET['showdesc'] == 'y';
        $carestage = $showDesc ? 'npsa_map_6' : 'npsa_map_3';
        $clindetail = $showDesc ? 'npsa_map_8' : 'npsa_map_4';
        $clintype = $showDesc ? 'npsa_map_10' : 'npsa_map_5';

        $this->response->build('src/admin/views/ShowIN05Form.php', array(
            'showDesc' => $showDesc,
            'carestage' => $carestage,
            'clindetail' => $clindetail,
            'clintype' => $clintype
        ));
    }

    /**
     * Constructs the fields used in the Rico table.
     *
     * @param boolean $showDesc Whether or not we're showing descriptions as well as codes.
     */
    public static function DefineFields($showDesc)
    {
        global $oForm;

        $oForm->AddSort('npsa_field1', 'ASC');
        $oForm->AddSort('npsa_field2', 'ASC');
        $oForm->AddSort('npsa_field3', 'ASC');

        $oForm->ConfirmDeleteColumn();
        $oForm->options['canAdd'] = false;
        $oForm->overrideKeys["NPSA_FIELD1"] = true;
        $oForm->overrideKeys["NPSA_FIELD2"] = true;
        $oForm->overrideKeys["NPSA_FIELD3"] = true;
        $oForm->options["FilterLocation"] = -1;

        $oForm->AddEntryFieldW('npsa_field1', \Labels_FieldLabel::GetFieldLabel('inc_type'), 'S', '', 110);
        $oForm->CurrentField['FormView'] = 'hidden';
        $oForm->CurrentField["filterUI"] = 't';

        if ($showDesc)
        {
            $oForm->AddCalculatedField('SELECT description FROM code_inc_type WHERE t.npsa_field1 = code', '');
            $oForm->CurrentField["width"] = 110;
            $oForm->CurrentField["filterUI"] = 't';
        }

        $oForm->AddEntryFieldW('npsa_field2', \Labels_FieldLabel::GetFieldLabel('inc_category'), 'S', '', 110);
        $oForm->CurrentField['FormView'] = 'hidden';
        $oForm->CurrentField["filterUI"] = 't';

        if ($showDesc)
        {
            $oForm->AddCalculatedField('SELECT description FROM code_inc_cat WHERE t.npsa_field2 = code', '');
            $oForm->CurrentField["width"] = 110;
            $oForm->CurrentField["filterUI"] = 't';
        }

        $oForm->AddEntryFieldW('npsa_field3', \Labels_FieldLabel::GetFieldLabel('inc_subcategory'), 'S', '', 110);
        $oForm->CurrentField['FormView'] = 'hidden';
        $oForm->CurrentField["filterUI"] = 't';

        if ($showDesc)
        {
            $oForm->AddCalculatedField('SELECT description FROM code_inc_subcat WHERE t.npsa_field3 = code', '');
            $oForm->CurrentField["width"] = 110;
            $oForm->CurrentField["filterUI"] = 't';
        }

        $oForm->AddEntryFieldW('npsa_map1', \Labels_FieldLabel::GetFieldLabel('inc_carestage'), 'DS', '', 110);
        $oForm->CurrentField["filterUI"] = 't';

        if ($showDesc)
        {
            $oForm->AddCalculatedField('SELECT description FROM code_carestage WHERE t.npsa_map1 = code', '');
            $oForm->CurrentField["width"] = 110;
            $oForm->CurrentField["filterUI"] = 't';
        }

        $oForm->AddEntryFieldW('npsa_map2', \Labels_FieldLabel::GetFieldLabel('inc_clin_detail'), 'DS', '', 110);
        $oForm->CurrentField["filterUI"] = 't';

        if ($showDesc)
        {
            $oForm->AddCalculatedField('SELECT cod_descr FROM code_types WHERE cod_type = \'CLINDT\' AND t.npsa_map2 = cod_code', '');
            $oForm->CurrentField["width"] = 110;
            $oForm->CurrentField["filterUI"] = 't';
        }

        $oForm->AddEntryFieldW('npsa_map3', \Labels_FieldLabel::GetFieldLabel('inc_clintype'), 'DS', '', 110);
        $oForm->CurrentField["filterUI"] = 't';

        if ($showDesc)
        {
            $oForm->AddCalculatedField('SELECT cod_descr FROM code_types WHERE cod_type = \'CLINTYPE\' AND t.npsa_map3 = cod_code', '');
            $oForm->CurrentField["width"] = 110;
            $oForm->CurrentField["filterUI"] = 't';
        }

        $oForm->AddCalculatedField(self::buildNpsaSql(false), 'NPSA');
        $oForm->CurrentField["width"] = 110;
        $oForm->CurrentField["filterUI"] = 't';

        if ($showDesc)
        {
            $oForm->AddCalculatedField(self::buildNpsaSql(true), '');
            $oForm->CurrentField["width"] = 110;
            $oForm->CurrentField["filterUI"] = 't';
        }

        $oForm->DisplayPage();
    }

    /**
     * Builds the SQL statement needed to retrieve the correct NPSA code for the RICO table.
     *
     * NPSA codes are stored in an "array" as a space-delimited string (code_types.cod_npsa).  The mapped code is determined by matching
     * its position to the equivalent element in the "array" of parent codes, also a space-delimited string (code_types.cod_parent),
     * where the parent is the inc_clin_detail value for this record (npsa_map.npsa_field2).
     *
     * @param boolean $showDesc True if we're returning the description, rather than the code.
     *
     * @return string $sql
     */
    public static function buildNpsaSql($showDesc)
    {
        // build SQL used to generate numbers table for tracking position of codes within cod_parent/cod_npsa fields
        $nbrSQL = '
        SELECT 0 AS n UNION SELECT 1 AS n UNION SELECT 2 AS n UNION SELECT 3 AS n UNION SELECT 4 AS n UNION
        SELECT 5 AS n UNION SELECT 6 AS n UNION SELECT 7 AS n UNION SELECT 8 AS n UNION SELECT 9 AS n';

        $nbrsSQL = '
        SELECT nbr3.n * 100 + nbr2.n * 10 + nbr1.n + 1 AS n FROM ('.$nbrSQL.') AS nbr1, ('.$nbrSQL.') AS nbr2, ('.$nbrSQL.') AS nbr3';

        // build main SQL statement used for determining NPSA value
        $sql = '
        SELECT
            '.($showDesc ? 'code_npsa_types.cod_descr' : 'npsa.value').'
        FROM
        (
            SELECT
                SUBSTRING( \' \' + cod_parent + \' \', n + 1, CHARINDEX( \' \', \' \' + cod_parent + \' \', n + 1 ) - n - 1 ) AS "value",
                ROW_NUMBER() OVER ( ORDER BY n ) AS "pos"
            FROM
                code_types, ('.$nbrsSQL.') AS Nbrs
            WHERE
                SUBSTRING( \' \' + cod_parent + \' \', n, 1 ) = \' \' AND
                n < LEN( \' \' + cod_parent + \' \' ) AND
                cod_type = \'CLINTYPE\' AND
                cod_code = t.npsa_map3
        ) AS clintype
        INNER JOIN
        (
            SELECT
                SUBSTRING( \' \' + cod_npsa + \' \', n + 1, CHARINDEX( \' \', \' \' + cod_npsa + \' \', n + 1 ) - n - 1 ) AS "value",
                ROW_NUMBER() OVER ( ORDER BY n ) AS "pos"
            FROM
                code_types, ('.$nbrsSQL.') AS Nbrs
            WHERE
                SUBSTRING( \' \' + cod_npsa + \' \', n, 1 ) = \' \'
                AND n < LEN( \' \' + cod_npsa + \' \' )
                AND cod_type = \'CLINTYPE\'
                AND cod_code = t.npsa_map3
            ) AS npsa ON clintype.pos = npsa.pos'.($showDesc ? '
        INNER JOIN
            code_npsa_types ON npsa.value = code_npsa_types.cod_code' : '').'
        WHERE
            clintype.value = t.npsa_map2'.($showDesc ? ' AND
            code_npsa_types.cod_type = \'IN05\'' : '');

        return $sql;
    }
}