<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;

use src\users\model\UserModelFactory;

/**
 * Controls the process of managing profiles
 */
class RegistrationController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
	
    /**
     * Constructs a list of users awaiting approval
     */
	public function listAwaitingApproval()
	{
        $this->hasPadding = false;
        $this->title = _tk('approve_reg_title');
        $this->image = 'images/icons/icon_cog_approved.png';

        $Factory = new UserModelFactory();

        list($query, $where, $fields) = $Factory->getQueryFactory()->getQueryObjects();

        $fields
            ->field('staff.lockout')->eq('Y')
            ->field('staff.login_tries')->eq(-1);
        $where->add($fields);

        $query->where($where);
        $query->orderby(array('staff.login'));

        $users = $Factory->getCollection();
        $users->setQuery($query);

        $this->response->build('src/admin/views/ListAwaitingApproval.php', array(
            'users' => $users,
		));
	}

    /**
     * Constructs a list of existing profiles.
     */
	public function editProfile()
	{
        global $scripturl;

        $NewProfile = ($this->request->getParameter('recordid') == '');
        $this->title = ($NewProfile ? 'New Profile' : 'Edit Profile');

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'onclick' => 'selectAllMultiCodes();submitClicked=true;if(validateOnSubmit()){document.frmProfile.submit()}', 'action' => 'SAVE'));
        if (!$NewProfile)
        {
            $ButtonGroup->AddButton(array('id' => 'btnCopy', 'name' => 'btnCopy', 'label' => _tk('btn_copy'), 'onclick' => 'copyProfile()', 'action' => 'BACK', 'icon' => 'images/icons/toolbar-copy.png'));
        }
        $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'SendTo(\''.$scripturl.'?action=listprofiles\');', 'action' => 'CANCEL'));

        if(!empty($data))
        {
            $ButtonGroup->AddButton(array('id' => 'btnDelete', 'name' => 'btnDelete', 'label' => 'Delete profile', 'onclick' => 'deleteProfile()', 'action' => 'DELETE'));
        }

        $this->menuParameters = array(
            'buttons' => $ButtonGroup,
            'no_audit' => true,
            'no_print' => true
        );

        if($this->request->getParameter('recordid'))
        {
            //get profile-specific data
            $sql = 'SELECT recordid, pfl_name, pfl_description, updateid FROM profiles WHERE recordid = :recordid';
            $data = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $_GET['recordid']));

            //get parameter data
            $sql = 'SELECT lpp_parameter, lpp_value FROM link_profile_param WHERE lpp_profile = :recordid';
            $resultArray = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $_GET['recordid']));

            foreach ($resultArray as $row)
            {
                $data[$row['lpp_parameter']] = $row['lpp_value'];
            }

            //get security group data
            $sql = 'SELECT lpg_group FROM link_profile_group WHERE lpg_profile = :recordid';
            $resultArray = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $_GET['recordid']));

            foreach ($resultArray as $row)
            {
                $Groups[] = $row['lpg_group'];
            }

            if(is_array($Groups))
            {
                $data['sec_groups'] = join(' ', $Groups);
            }

            // Retrieve LDAP mappings
            $sql = 'SELECT ldap_dn FROM sec_ldap_profiles WHERE pfl_id = :recordid';
            $resultArray = \DatixDBQuery::PDO_fetch_all($sql, array('recordid' => $_GET['recordid']));

            foreach ($resultArray as $row)
            {
                $data['ldap_dn'][] = $row["ldap_dn"];
            }
        }

        $FormMode = ($NewProfile ? 'New' : 'Edit');

        $FormDesign = new \Forms_FormDesign(array('module' => 'ADM', 'level' => 2));
        $FormDesign->MandatoryFields['pfl_name'] = 'details';
        $FormDesign->MandatoryFields['pfl_description'] = 'details';
        $FormDesign->HelpTexts = array('ATI_REVIEWED_EMAIL' => 'E-mails will be sent only for the main location for which the assigned assessment has been created. Users at other locations selected against the assigned assessment will not receive notification.');

        $Table = new \FormTable($FormMode, 'ADM', $FormDesign);

        include('Source/security/BasicFormProfile.php');
        $Table->MakeForm($FormArray, $data, 'ADM');
        $Table->MakeTable();

        $this->response->build('src/admin/views/EditProfile.php', array(
            'data' => $data,
            'Table' => $Table,
            'ButtonGroup' => $ButtonGroup,
		));
	}
}