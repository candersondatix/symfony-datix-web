<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;

/**
 * Controls the process of managing profiles
 */
class GlobalsTemplateController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);

        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'rico/src/ricoCommon.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));
    }

    /**
     * Constructs a list of existing profiles.
     */
	public function listGlobals()
	{
        // is global admin area locked?
        $globalSetupLocked = false;
        if (IsCentrallyAdminSys() && bYN(GetParm('GLOBALS_ADMIN_LOCKED', 'N'))) 
        {
            if (!IsCentralAdmin()) {
                $this->redirect('?action=home');
            } else {
                $globalSetupLocked = true;
            }
        }
        
        $this->title = _tk('globals');
        $where = "parameter NOT IN ('LDAP_USER', 'LDAP_PASSWORD', 'RECORD_UPDATE_EMAIL', 'RECORD_UPDATE_EMAIL_DATETIME')";

        $this->response->build('src/admin/views/ListGlobals.php', array(
            'centralAdmin' => (IsCentrallyAdminSys() && IsCentralAdmin()),
            'locked'       => $globalSetupLocked,
            'login'        => '',
            'table'        => 'globals',
            'where'        => $where
		));
	}

	public function listUserGlobals()
	{
        $UserID = (int) $this->request->getParameter('user');

        $sql = 'SELECT login FROM contacts_main WHERE recordid = :UserID';
        $row = \DatixDBQuery::PDO_fetch($sql, array("UserID" => $UserID));

        $this->title = _tk('userparms') . ' - ' . $row['login'];

        $data['login'] = $row["login"];
        $data['table'] = 'user_parms';
        $data['where'] = "login = '" . $data['login'] . "'";

        $FormArray = array(
            "Parameters" => array("Panels" => "True", "Condition" => false),
            "name"       => array("Title"  => $this->title,
                "MenuTitle" => $this->title,
                "NewPanel"  => true,
                "Rows"      => array()),
        );

        $SetupTable = new \FormTable();
        $SetupTable->MakeForm($FormArray, $data, $this->module);
        $SetupTable->MakeTable();

        $this->response->build('src/admin/views/ListGlobals.php', array(
            'login'  => $data['login'],
            'userId' => $UserID,
            'table'  => $data['table'],
            'where'  => $data['where']
		));
	}

    public function lockglobals ()
    {
        SetGlobal('GLOBALS_ADMIN_LOCKED', 'Y', true);
        $this->redirect('?action=listglobals');
    }
    
    public function unlockglobals ()
    {
        SetGlobal('GLOBALS_ADMIN_LOCKED', 'N', true);
        $this->redirect('?action=listglobals');
    }
    
    /**
    * @desc Defines the fields used in the Live grid for the globals/user parameters setup and displays the grid
    * 
    */
    function DefineFields($table, $login) 
    {
      global $oForm;

      $oForm->options["FilterLocation"]=-1; 

      $oForm->AddEntryFieldW("parameter", "Parameter Name", "B", "",250);  
      $oForm->AddSort("parameter", "ASC");
      $oForm->CurrentField["required"] = true; 
      $oForm->CurrentField["filterUI"]="t";
      $oForm->CurrentField["uppercase"] = true;
      $oForm->CurrentField["unique"] = true;
      $oForm->ConfirmDeleteColumn();

      $oForm->AddEntryFieldW("parmvalue", "Value", "B", "", 250); 
      $oForm->CurrentField["filterUI"]="t";  
      $oForm->overrideKeys["PARAMETER"] = true;

      if ($table == 'user_parms')
      {
        $oForm->AddEntryFieldW("login", "Login", "H", $login, 128);
        $oForm->overrideKeys["LOGIN"] = true;  
      }   
      $oForm->DisplayPage();
    }
}