<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class LoginAuditTemplateController extends TemplateController
{
    /**
     * @desc Constructs HTML representation of the Login audit screens using Rico based LiveGrid
     *
     * @codeCoverageIgnoreStart
     * No unit test, since it deals with constructing HTML
     */
    function show_login_audit()
    {
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        if ($this->request->getParameter('user') != '')
        {
            $data['SetupType'] = 'User';

            $UserID = (int) $this->request->getParameter('user');

            $sql = '
                SELECT
                    login
                FROM
                    contacts_main
                WHERE
                    recordid = :UserID
            ';

            $row = \DatixDBQuery::PDO_fetch($sql, array('UserID' => $UserID));

            $this->title = _tk('login_audit') . ' - ' . $row['login'];
            $data['where'] = "lau_login = '" . $row['login'] . "'";
        }
        else
        {
            $data['SetupType'] = 'Full';
            $this->title = _tk('login_audit');
        }

        $this->module = 'ADM';

        $FormArray = array (
            "Parameters" => array(
                "Panels" => "True",
                "Condition" => false
            ),
            "name" => array(
                "Title" => $this->title,
                "MenuTitle" => $this->title,
                "NewPanel" => true,
                "Function" => "MakeLoginAuditTable",
                "Rows" => array()
            ),
        );

        $table = 'login_audit';
        //Need to add where clause to restrict number of days to look back on based on AUDIT_DISPLAY_DAYS global
        $where = 'lau_date >= (GETDATE()-' . GetParm('AUDIT_DISPLAY_DAYS', 180) . ')';

        if (!empty($data['where']))
        {
            $where.= ' AND ' . $data['where'];
        }

        $SetupTable = new \FormTable();
        $SetupTable->MakeForm($FormArray, $data, $this->module);

        $SetupTable->MakeTable();

        $this->response->build('src/admin/views/LoginAudit.php', array(
            'data' => $data,
            'table' => $table,
            'where' => $where,
            'user' => $this->request->getParameter('user')
        ));
    }

    /**
     * @desc Defines the fields used in the Live grid for the Login Audit and displays the grid
     */
    static public function DefineFields()
    {
        global $oForm;

        $oForm->options['FilterLocation'] = -1;

        $oForm->AddEntryFieldW('lau_date', 'Date/Time', 'D', '', 125);
        $oForm->CurrentField['dateFmt'] = 'yyyy-mm-dd HH:nn:ss';
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->AddEntryFieldW('lau_login', 'User', 'B', '', 125);
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->AddEntryFieldW('lau_description', 'Event', 'TA', '', 500);
        $oForm->CurrentField['filterUI'] = 't';
        $oForm->AddSort('lau_date', 'DESC');

        $oForm->overrideKeys['lau_date'] = true;
        $oForm->overrideKeys['lau_login'] = true;
        $oForm->overrideKeys['lau_description'] = true;

        $oForm->DisplayPage();
    }
}