<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class IN05Controller extends Controller
{
    /**
     * Calculates the number of NPSA IN05 mapping records which will be generated given the current code setup.
     */
    function countin05()
    {
        $count = 0;

        $sql = '
            SELECT
                code
            FROM
                code_inc_type
            WHERE
                cod_priv_level != \'N\'
                OR
                cod_priv_level is NULL
        ';

        $types = \DatixDBQuery::PDO_fetch_all($sql, array(), \PDO::FETCH_COLUMN);

        foreach ($types as $type)
        {
            $categories = \DatixDBQuery::PDO_fetch_all('
            SELECT code FROM code_inc_cat WHERE (cod_priv_level != \'N\' OR cod_priv_level IS NULL) AND (
                cod_parent = \''.$type.'\' OR
                cod_parent LIKE \'% '.$type.' %\' OR
                cod_parent LIKE \''.$type.' %\' OR
                cod_parent LIKE \'% '.$type.'\' OR
                cod_parent IS NULL OR
                cod_parent = \'\')
            ', array(), \PDO::FETCH_COLUMN);

            foreach ($categories as $category)
            {
                $count+= \DatixDBQuery::PDO_fetch('
                SELECT count(*) FROM code_inc_subcat WHERE (cod_priv_level != \'N\' OR cod_priv_level IS NULL) AND (
                    cod_parent = \''.$category.'\' OR
                    cod_parent LIKE \'% '.$category. '%\' OR
                    cod_parent LIKE \''.$category.' %\' OR
                    cod_parent LIKE \'% '.$category.'\' OR
                    cod_parent IS NULL OR
                    cod_parent = \'\')
                ', array(), \PDO::FETCH_COLUMN);
            }
        }
        echo $count;
    }

    /**
     * Generates NPSA IN05 mapping records based on the current code setup.
     */
    function generatein05()
    {
        $count = 0;
        $types = \DatixDBQuery::PDO_fetch_all('SELECT code FROM code_inc_type WHERE cod_priv_level != \'N\' or cod_priv_level is NULL', array(), \PDO::FETCH_COLUMN);

        foreach ($types as $type)
        {
            $categories = \DatixDBQuery::PDO_fetch_all('
            SELECT code FROM code_inc_cat WHERE (cod_priv_level != \'N\' OR cod_priv_level IS NULL) AND (
                cod_parent = \''.$type.'\' OR
                cod_parent LIKE \'% '.$type.' %\' OR
                cod_parent LIKE \''.$type.' %\' OR
                cod_parent LIKE \'% '.$type.'\' OR
                cod_parent IS NULL OR
                cod_parent = \'\')
            ', array(), \PDO::FETCH_COLUMN);

            foreach ($categories as $category)
            {
                $subCategories = \DatixDBQuery::PDO_fetch_all('
                SELECT code FROM code_inc_subcat WHERE (cod_priv_level != \'N\' OR cod_priv_level IS NULL) AND (
                    cod_parent = \''.$category.'\' OR
                    cod_parent LIKE \'% '.$category. '%\' OR
                    cod_parent LIKE \''.$category.' %\' OR
                    cod_parent LIKE \'% '.$category.'\' OR
                    cod_parent IS NULL OR
                    cod_parent = \'\')
                ', array(), \PDO::FETCH_COLUMN);

                foreach ($subCategories as $subCategory)
                {
                    if (\DatixDBQuery::PDO_fetch('
                    SELECT count(*) FROM npsa_map WHERE npsa_type = \'IN05\' AND
                        npsa_field1 = \''.$type.'\' AND
                        npsa_field2 = \''.$category.'\' AND
                        npsa_field3 = \''.$subCategory.'\'
                    ', array(), \PDO::FETCH_COLUMN) == 0)
                    {
                        \DatixDBQuery::PDO_query('INSERT INTO npsa_map (npsa_type, npsa_field1, npsa_field2, npsa_field3) VALUES (\'IN05\', \''.$type.'\', \''.$category.'\', \''.$subCategory.'\')');
                        $count++;
                    }
                }
            }
        }
        echo $count;
    }
}