<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

/**
 * Handles the display and adjustment of p_a_s_s_w_o_r_d policy settings
 */
class PasswordPolicyTemplateController extends TemplateController
{
    /**
     * Renders HTML form for amending the p_a_s_s_w_o_r_d policy settings
     *
     * @param array $defaultValues A set of values to populate the form with (used when showing the form after an unsuccessful update attempt). Array keys should match the globals parameter values
     * @param array $errors An array of errors to show along side invalid field names. Keys should correspond to the HTML 'name' attribute of the field
     * @return void
     */
    public function password_policy($defaultValues = array(), $errors = array())
    {
        $output   = array();
        $messages = GetSessionMessages();
        $this->title = _tk('password-policy');
        $this->module = 'ADM';
        $this->hasPadding = false;

        $globals = array(
            'PWD_MIN_LENGTH' => '6',
            'PWD_NUMBERS' => '0',
            'PWD_UPPERCASE' => '0',
            'PWD_LOWERCASE' => '0',
            'PWD_SPECIAL' => '0',
            'PWD_EXPIRY_DAYS' => '90',
            'PWD_UNIQUE' => 'Y',
            'PWD_UNIQUE_LAST' => '0'
        );

        foreach ($globals as $global => $default)
        {
            if (isset($defaultValues[$global]))
            {
                $value = $defaultValues[$global];
            }
            else
            {
                $value = GetParm($global, $default, true);
            }

            $output[$global] = \Escape::EscapeEntities($value);
        }

        $this->response->build('src/admin/views/PasswordPolicy.php', array(
            'messages' => $messages,
            'output' => $output,
            'errors' => $errors
        ));
    }

    /**
     * Updates the p_a_s_s_w_o_r_d policy settings and redirects to the index
     *
     * @return void
     */
    public function password_policy_update()
    {
        if (!$_POST)
        {
            $this->password_policy();
        }

        if (isset($_POST['cancel']))
        {
            $this->redirect ('app.php?action=home&module=ADM');
            exit();
        }

        $errors = array();
        $valid  = true;
        require_once(dirname(__FILE__) . '/../../../Source/classes/PasswordPolicy.php');
        $policy = new \PasswordPolicy;

        // Need to wrap each mutation around a try catch statement so that we can catch all error messages in one submission
        try
        {
            // set the minimum length first otherwise additional settings will throw exceptions
            $policy->setMinLength($_POST['password-length']);
        }
        catch (\Exception_PwdPolicy_InvalidValue $e)
        {
            $errors['password-length'] = $e->getMessage();
        }
        catch (\Exception_PwdPolicy_MinRequiredLength $e)
        {
            $errors['min-length'] = $e->getMessage();
        }
        catch (\Exception $e)
        {
            // do nothing, shouldn't ever get here
        }

        try
        {
            $policy->setNumbers($_POST['password-numbers']);
        }
        catch (\Exception_PwdPolicy_InvalidValue $e)
        {
            $errors['password-numbers'] = $e->getMessage();
        }
        catch (\Exception_PwdPolicy_MinRequiredLength $e)
        {
            if (!isset($errors['min-length']))
            {
                $errors['min-length'] = $e->getMessage(); // we only want to get the min length issue once
            }
        }
        catch (\Exception $e) {}

        try
        {
            $policy->setSymbols($_POST['password-symbols']);
        }
        catch (\Exception_PwdPolicy_InvalidValue $e)
        {
            $errors['password-symbols'] = $e->getMessage();
        }
        catch (\Exception_PwdPolicy_MinRequiredLength $e)
        {
            if (!isset($errors['min-length']))
            {
                $errors['min-length'] = $e->getMessage(); // we only want to get the min length issue once
            }
        }
        catch (\Exception $e) {}

        try
        {
            $policy->setUcLetters($_POST['password-ucletters']);
        }
        catch (\Exception_PwdPolicy_InvalidValue $e)
        {
            $errors['password-ucletters'] = $e->getMessage();
        }
        catch (\Exception_PwdPolicy_MinRequiredLength $e)
        {
            if (!isset($errors['min-length']))
            {
                $errors['min-length'] = $e->getMessage(); // we only want to get the min length issue once
            }
        }
        catch (\Exception $e) {}

        try
        {
            $policy->setLcLetters($_POST['password-lcletters']);
        }
        catch (\Exception_PwdPolicy_InvalidValue $e)
        {
            $errors['password-lcletters'] = $e->getMessage();
        }
        catch (\Exception_PwdPolicy_MinRequiredLength $e)
        {
            if (!isset($errors['min-length']))
            {
                $errors['min-length'] = $e->getMessage(); // we only want to get the min length issue once
            }
        }
        catch (\Exception $e) {}

        try
        {
            $policy->setExpiry($_POST['password-expiry']);
        }
        catch (\Exception $e)
        {
            $errors['password-expiry'] = $e->getMessage();
        }

        try
        {
            if (isset($_POST['unique-passwords-disable']) && $_POST['unique-passwords-disable'] == true)
            {
                $policy->setUniquePasswordAmount(0);
            }
            else
            {
                $policy->setUniquePasswordAmount($_POST['unique-passwords'] ? $_POST['unique-passwords'] : 0);
            }
        }
        catch (\Exception $e)
        {
            $errors['unique-passwords'] = $e->getMessage();
        }

        if (!empty($errors))
        {
            AddSessionMessage('ERROR', implode('. ', $errors));

            // re-key the $_POST data with db names so that it can populate the form when re-showing the index screen
            $originalValues = array(
                'PWD_EXPIRY_DAYS' => $_POST['password-expiry'],
                'PWD_LOWERCASE'   => $_POST['password-lcletters'],
                'PWD_UPPERCASE'   => $_POST['password-ucletters'],
                'PWD_MIN_LENGTH'  => $_POST['password-length'],
                'PWD_NUMBERS'     => $_POST['password-numbers'],
                'PWD_SPECIAL'     => $_POST['password-symbols'],
                'PWD_UNIQUE'      => ($_POST['unique-passwords-disable'] == true) ? 'N' : 'Y',
                'PWD_UNIQUE_LAST' => $_POST['unique-passwords']
            );

            $this->password_policy($originalValues, $errors);
        }

        // UDPATE DB
        try
        {
            // using SetGlobal instead of PDO updates directly because we cannot gaurantee that a particular global exists
            // unfortunately, SetGlobal doesn't return anything, so not sure if the query has been successful
            // also, can't use transactions
            SetGlobal('PWD_EXPIRY_DAYS', $policy->getExpiry());
            SetGlobal('PWD_LOWERCASE', $policy->getLcLetters());
            SetGlobal('PWD_UPPERCASE', $policy->getUcLetters());
            SetGlobal('PWD_MIN_LENGTH', $policy->getMinLength());
            SetGlobal('PWD_NUMBERS', $policy->getNumbers());
            SetGlobal('PWD_SPECIAL', $policy->getSymbols());

            if (isset($_POST['unique-passwords-disable']) && $_POST['unique-passwords-disable'] == true)
            {
                SetGlobal('PWD_UNIQUE', 'N');
            }
            else
            {
                SetGlobal('PWD_UNIQUE', 'Y');
                SetGlobal('PWD_UNIQUE_LAST', $policy->getUniquePasswordAmount());
            }

            AddSessionMessage('INFO', _tk('password-policy-settings-updated-successfully'));
        }
        catch (\Exception $e)
        {
            AddSessionMessage('ERROR', _tk('there-was-an-error-whilst-trying-to-update-the-password-policy-settings'));
        }

        $this->password_policy();
    }
}