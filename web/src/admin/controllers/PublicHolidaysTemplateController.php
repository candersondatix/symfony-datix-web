<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class PublicHolidaysTemplateController extends TemplateController
{
    /**
     * Index screen for public holidays
     *
     * @return void
     */
    public function publicholidays()
    {
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));


        // create js settings
        $dateFmt         = GetParm("FMT_DATE_WEB");
        $weekStartDay    = GetParm("WEEK_START_DAY", 2);
        $calendarOnClick = GetParm('CALENDAR_ONCLICK', 'N');
        $calendarWeekend = GetParm('CALENDAR_WEEKEND', 1);

        $ricoDateFmt     = ($dateFmt == 'US') ? 'mm/dd/yyyy' : 'dd/mm/yyyy';
        $calendarDateFmt = ($dateFmt == 'US') ? 'mm/dd/yy' : 'dd/mm/yy';

        $jsSettings = array(
            'calendarDateFmt' => $calendarDateFmt,
            'ricoDateFmt'     => $ricoDateFmt,
            'weekStartDay'    => $weekStartDay,
            'calendarOnClick' => $calendarOnClick,
            'calendarWeekend' => $calendarWeekend
        );
		
        if (!empty($_POST)) {
            $result = false;
            $errorMsg = _tk('there-was-an-error-processing-your-request');

            $id = $this->request->getParameter('_k0');
            $holidays_0 = $this->request->getParameter('holidays_0');
            $holidays_1 = $this->request->getParameter('holidays_1');

			switch(\UnicodeString::strtolower($this->request->getParameter('holidays__action'))) {
				case 'ins':
					$action = 'added';
					
					try {
						$result = $this->insert($holidays_0, $holidays_1);
					} catch (\HolidayExistsException $e) {
						$errorMsg = $e->getMessage();
                        echo $errorMsg;
                        exit;
					}
					break;
					
				case 'del':
					$action = 'deleted';
					$result = $this->delete($id);
					break;
					
				case 'upd':
					$action = 'updated';
                    try {
                        $result = $this->updateHoliday($id, $holidays_0, $holidays_1);
                    } catch (\HolidayExistsException $e) {
                        $errorMsg = $e->getMessage();
                        echo $errorMsg;
                        exit;
                    }
                    break;
					
				case 'back':
					$this->redirect ('app.php?action=home&module=ADM');
					exit();
					break;
					
				default:
					$result = false;
					break;
			}
		}

        $this->title  = _tk('public-holidays');
        $this->module = 'ADM';
        $this->image  = '';
 
        $this->response->build('src/admin/views/PublicHolidays.php', array(
            'result' => $result,
            'jsSettings' => $jsSettings,
            'action' => $action,
            'errorMsg' => $errorMsg
		));

//		GetSideMenuHTML(array('module' => 'ADM'));
	}
	
	/**
	 * Endpoint for the Rico LiveGrid updates
	 * 
	 * @return void
	 * @todo Currently, dates are formatted using the FormatDateVal in Subs.php. However, this will only format the date to either US or UK convention
	 * We should use in-built date formatting using the country global but setlocale doesn't recognise the value 'england'
	 */
	public function publicholidaysupdate()
	{
		$offset   = $this->request->getParameter("offset")    ? \Sanitize::SanitizeInt($this->request->getParameter("offset")) : "0";
		$size     = $this->request->getParameter("page_size") ? \Sanitize::SanitizeInt($this->request->getParameter("page_size")) : "";
		$total    = $this->request->getParameter("get_total") ? \UnicodeString::strtolower($this->request->getParameter("get_total")) : false;
		
		if ($this->request->getParameter("s0")) {
			$sort = 'ORDER BY HOL_DATE ' . \Sanitize::SanitizeString($this->request->getParameter("s0"));
		} else if ($_GET["s1"]) {
			$sort = 'ORDER BY HOL_NAME ' . \Sanitize::SanitizeString($this->request->getParameter("s1"));
		} else {
			$sort = 'ORDER BY HOL_DATE ASC';
		}
		
		$sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER ($sort) AS rico_rownum, t.HOL_DATE, t.HOL_NAME FROM HOLIDAYS t WHERE (HOL_NAME LIKE '[^!]%')) AS rico_Main WHERE rico_rownum > $offset AND rico_rownum <= ($offset + $size)";
		$result = \DatixDBQuery::PDO_fetch_all($sql);
		$rows = array();
		$rowCount = FALSE;
		
		foreach ($result as $r) {
			$rows[] = array(FormatDateVal($r['HOL_DATE']), $r['HOL_NAME']);
		}
		
		if ($total) {
			$sql = "SELECT COUNT(*) AS 'total' FROM HOLIDAYS WHERE (HOL_NAME LIKE '[^!]%')";
			$result = \DatixDBQuery::PDO_fetch($sql);
			$rowCount = $result['total'];
		}
		
		$return = array(
			"update_ui" => TRUE,
			"offset" => $offset,
			"rows" => $rows,
		);
		
		if ($rowCount) {
			$return['rowCount'] = $rowCount;
		}
		
		echo json_encode($return);
		obExit();
	}
	
	/**
	 * Adds a public holiday to the database
	 *
	 * @param string $date The date the holiday occurs
	 * @param string $holiday The name of the holiday
	 * @return boolean Returns the success or failure of the insert
	 * @throws HolidayExistsException if the a holiday or weekend has already been registered on given date
	 * @throws Exception if there was an exception thrown when attempting the insert
	 */
	protected function insert($date, $holiday)
	{
		// Check for existence of holiday first
		$date = $this->formatDate($date);
		$sql  = 'SELECT * FROM HOLIDAYS WHERE HOL_DATE = ?';
		
		$result = \DatixDBQuery::PDO_fetch($sql, array($date));
		
		if ($result) {
			if (preg_match('#^!#u', $result['HOL_NAME'])) {
				$msg = _tk('publicholidays-weekend-set');
			} else {
				$msg = _tk('publicholidays-holiday-set');
			}
			
			throw new \HolidayExistsException($msg);
		}
		
		// attempt insert
        $sql  = 'INSERT INTO HOLIDAYS (HOL_DATE, HOL_NAME) VALUES (?, ?)';
        $result = \DatixDBQuery::PDO_query($sql, array($date, $holiday));

		return $result;
	}
	
	/**
	 * Removes a holiday record from the database
	 *
	 * @param string $date The date of the record to delete
	 * @return boolean The result of the database delete query
	 */
	protected function delete($date)
	{
		$sql  = "DELETE FROM HOLIDAYS WHERE HOL_DATE = ?";
		$date = $this->formatDate($date);
		
		try {
			$result = \DatixDBQuery::PDO_query($sql, array($date));
		} catch (Exception $e) {
			$result = FALSE;
		}
		
		return $result;
	}
	
	/**
	 * Updates a holiday record
	 *
	 * @param string $key The current date of the public holiday record (which is used as the primary key)
	 * @param string $date The new date of the holiday record
	 * @param string $holiday The new holiday name
	 * @return boolean Result of database update
	 */
	protected function updateHoliday($key, $date, $holiday)
	{
        // Check for existence of holiday first
        $date = $this->formatDate($date);
        $sql  = 'SELECT * FROM HOLIDAYS WHERE HOL_DATE = ?';

        $result = \DatixDBQuery::PDO_fetch($sql, array($date));

        if ($result && preg_match('#^!#u', $result['HOL_NAME']))
        {
            $msg = _tk('publicholidays-weekend-set');
            throw new \HolidayExistsException($msg);
        }

        $sql  = "UPDATE HOLIDAYS SET HOL_NAME = ?, HOL_DATE = ? WHERE HOL_DATE = ?";
		$key  = $this->formatDate($key);

		try {
			$result = \DatixDBQuery::PDO_query($sql, array($holiday, $date, $key));
		} catch (\Exception $e) {
			$result = FALSE;
		}
		
		return $result;
	}
    
	
	/**
	 * Formats a textual representation of a date
	 * Uses UserDateToSQLDate for parsing $date (see Subs.php)
	 *
	 * @see UserDateToSQLDate
	 * @param string $date Textual representation of a date
	 * @return string Datetime stamp
	 */
	private function formatDate($date)
    {
		return UserDateToSQLDate($date);
	}
}