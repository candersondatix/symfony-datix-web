<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class DeleteListingDesignController extends Controller
{
    function deletelistingdesign()
    {
        require_once 'Source/libs/ListingClass.php';

        $Listing = new \Listing(\Sanitize::getModule($this->request->getParameter('module')), \Sanitize::SanitizeInt($this->request->getParameter('form_id')));

        $Listing->DeleteFromDB();

        AddSessionMessage('INFO', 'Listing design deleted');

        $Parameters = array(
            'module' => \Sanitize::getModule($this->request->getParameter('module'))
        );

        $this->call('src\\admin\\controllers\\ListListingDesignsTemplateController', 'listlistingdesigns', $Parameters);
    }
}