<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class NewListingDesignTemplateController extends TemplateController
{
    /**
     * Provides the user with options for the creation of a new listing design.
     */
    function newlistingdesign()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $module = \Sanitize::getModule($this->request->getParameter('module'));

        if (!$module)
        {
            $module = 'INC';
        }

        if ($this->request->getParameter('formaction') == 'cancel')
        {
            $this->redirect('app.php?action=listlistingdesigns&module=' . $module);
        }

        $this->title = 'Create a new '. $ModuleDefs[$module]['REC_NAME'] .' listing';
        $this->module = 'ADM';
        $this->hasPadding = false;

        $ListingDesigns = \DatixDBQuery::PDO_fetch_all('SELECT recordid, LST_TITLE from WEB_LISTING_DESIGNS WHERE lst_module = :module', array('module' => $module));

        $this->response->build('src/admin/views/NewListingDesign.php', array(
            'ListingDesigns' => $ListingDesigns,
            'module' => $module
        ));
    }
}