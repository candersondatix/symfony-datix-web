<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;
use src\combolinking\model\ComboLinkModelFactory;

/**
 * Controls the process of managing profiles
 */
class ComboLinkingTemplateController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
	
    /**
     * Constructs a list of existing profiles.
     */
	public function listComboLinks()
	{
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'rico/src/ricoCommon.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        $ModuleDefs = $this->registry->getModuleDefs();

        $this->title = _tk('combo_linking');
        $this->menuParameters = array(
            'floating_menu' => false
        );

        $webModules = array_keys(getModArray(array('LOC','HOT')));

        // TODO - This should be refactored to a safer way of excluding modules from combo linking
        $excludeForms = array(175, 103);

        $Factory = new ComboLinkModelFactory();
        $Query = $Factory->getQueryFactory()->getQuery();
        $ComboLinks = $Factory->getCollection();
        $FieldCollection = $Factory->getQueryFactory()->getFieldCollection();
        $FieldCollection2 = $Factory->getQueryFactory()->getFieldCollection();
        $FieldCollection3 = $Factory->getQueryFactory()->getFieldCollection();
        $FieldCollection->field('sfm_type')->like('%C%');
        $FieldCollection2->field('sfm_module')->in($webModules);
        $FieldCollection3->field('sfm_form_id')->notIn($excludeForms);
        $Where = $Factory->getQueryFactory()->getWhere();
        $Where->add('AND', $FieldCollection, $FieldCollection2, $FieldCollection3);
        $Query->where($Where)->orderBy(array('sfm_title'));
        $ComboLinks->setQuery($Query);

        /*
         * Need to remove duplicate entries for the same table - easier to do this in PHP since SQL Server
         * won't let you group by sfm_tables in this instance
         */
        $subformTables = array();
        $subformCodes = array();

        foreach ($ComboLinks as $ComboLink)
        {
            if (!in_array($ComboLink->sfm_tables, $subformTables))
            {
                //If main module title, use DatixWeb module title in order to take into account any Module re-labelling.
                if ($ComboLink->sfm_tables == $ComboLink->sfm_form)
                {
                    $ComboLink->sfm_title = $ModuleDefs[$ComboLink->sfm_module]['NAME'];
                }

                $subformCodes[$ComboLink->sfm_form_id] = $ComboLink->sfm_title;
                $subformTables[] = $ComboLink->sfm_tables;
            }
        }

        $subformField = \Forms_SelectFieldFactory::createSelectField('subform', '', $this->request->getParameter('subform'), '');
        $subformField->setCustomCodes($subformCodes);
        $subformField->setOnChangeExtra('setForm()');
        $subformField->setSuppressCodeDisplay();

        $this->response->build('src/admin/views/ListComboLinks.php', array(
            'subformField' => $subformField
		));
	}
}