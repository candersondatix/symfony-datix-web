<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class ResetColumnsController extends Controller
{
    function resetcolumns()
    {
        require_once 'Source/libs/ListingClass.php';

        $Listing = new \Listing($this->request->getParameter('module'));

        $Listing->LoadColumnsFromDB(0);

        $Listing->ListingId = \Sanitize::SanitizeInt($this->request->getParameter('form_id'));

        $Listing->SaveToDB();

        AddSessionMessage('INFO', 'Listing design reset to default');

        $Parameters = array(
            'module' => $Listing->Module,
            'listing_id' => $Listing->ListingId
        );

        $this->call('src\admin\controllers\EditColumnsTemplateController', 'editcolumns', $Parameters);
    }
}