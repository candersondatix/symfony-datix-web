<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class ExcludeBatchUpdateTemplateController extends TemplateController
{
    function excludebatchupdate()
    {
        global $FieldDefs;
        
        $ModuleDefs = $this->registry->getModuleDefs();

        $module = \Sanitize::getModule($this->request->getParameter('module'));

        if ($module == '')
        {
            $module = 'INC';
        }

        $this->addJs('src/admin/reports/js/excludefields.js');

        $this->title = 'Batch update field set up';
        $this->module = 'ADM';
        $this->hasPadding = false;

        $ModuleDropDownHTML = getModuleDropdown(array(
            'name' => 'lbModule',
            'current' => $module,
            'onchange' => 'return false;',
            'not' => array('TOD', 'CQO', 'CQP', 'CQS', 'LOC', 'DAS', 'ADM', 'ELE', 'PRO')
        ));

        foreach ($ModuleDefs[$module]['FIELD_ARRAY'] as $row)
        {
            if (!\Fields_Field::isFieldCalculated($module, $row))
            {
                $availCols[$row] = GetFieldLabel($row);
            }
        }

        asort($availCols);

        $currentCols = array();

        // Get list of excluded fields
        $sql = '
            SELECT
                bex_field
            FROM
                batch_exclusions
            WHERE
                bex_module = :bex_module
                AND
                bex_table = :bex_table
        ';

        $result = \DatixDBQuery::PDO_fetch_all($sql, array('bex_module' => $module, 'bex_table' => $ModuleDefs[$module]['TABLE']));

        foreach ($result as $cols)
        {
            $currentCols[$cols['bex_field']] = GetFieldLabel($cols['bex_field'], $FieldDefs[$module][$cols['bex_field']]["Title"]);
        }

        $this->response->build('src/admin/views/ExcludeBatchUpdate.php', array(
            'module' => $module,
            'ModuleDropDownHTML' => $ModuleDropDownHTML,
            'availCols' => $availCols,
            'currentCols' => $currentCols,
        	'confirm_clear' => _tk('confirm_save_batch_update')
        ));
    }
    

    function saveexcludebatchupdate($reset = "")
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $form_action = $this->request->getParameter('rbWhat');

        if ($reset == 'Reset')
        {
            $form_action = 'Reset';
        }

        $module = \Sanitize::getModule($this->request->getParameter('module'));

        if (!$module)
        {
            $module = 'INC';
        }

        if ($form_action == 'Cancel')
        {
            $this->redirect('app.php?module=ADM');
        }

        if ($form_action == 'Save')
        {
            $sql = '
                DELETE FROM
                    batch_exclusions
                WHERE
                    bex_module = :module
                AND
                    bex_table = :table
            ';

            $Parameters = array('module' => $module, 'table' => $ModuleDefs[$module]["TABLE"]);
            \DatixDBQuery::PDO_query($sql,$Parameters);

            if (sizeof($this->request->getParameter('columns_list2')) > 0)
            {
                foreach ($this->request->getParameter('columns_list2') as $colname)
                {
                    $sql = '
                        INSERT INTO
                            batch_exclusions (bex_module, bex_field, bex_table)
                        VALUES
                            (:module, :colname, :table)
                    ';

                    \DatixDBQuery::PDO_query($sql, array('module' => $module, 'colname' => $colname, 'table' => $ModuleDefs[$module]["TABLE"]));
                }
            }
        }

        AddSessionMessage('INFO', 'Settings have been saved.');

        $this->redirect('app.php?action=excludebatchupdate&module=' . $module);
    }
}