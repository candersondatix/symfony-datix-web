<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;


/**
 * Controls the display of code tag set user interfaces. The user interfaces will need to be reviewed when we have a
 * more consistent interface design in general, and specifically when we have a better alternative to the rico tables.
 */
class CodeTagLinkTemplateController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
	
    /**
     * Constructs a list of fields already linked to tag sets.
     */
	public function listTagFields()
	{
        $ModuleDefs = $this->registry->getModuleDefs();

        $this->hasPadding = false;
        $this->title = _tk('assign_tags');

        $sql = "SELECT DISTINCT field, [table]
                FROM code_tag_linked_fields
                WHERE [table] != 'aud_reasons'
                    AND [table] != 'documents_main'
                    AND [table] != 'causal_factors'
                ORDER BY [table]";

        $LinkedFieldData = \DatixDBQuery::PDO_fetch_all($sql);

        foreach($LinkedFieldData as $FieldDetails)
        {
            $LinkedFields[] = array('field' => $FieldDetails['field'],
                'table' => $FieldDetails['table'],
                'module' => $this->getModuleForTag($FieldDetails['table']),
                'name' => \Labels_FieldLabel::GetFieldLabel($FieldDetails['field']));
        }

        if(is_array($LinkedFields))
        {
            $LinkedFieldRecordList = new \RecordLists_RecordList();
            $LinkedFieldRecordList->AddRecordData($LinkedFields);
        }

        $LinkedFieldListingDesign = new \Listings_ListingDesign(
            array('columns' => array(
                new \Fields_DummyField(array('name' => 'name', 'label' => 'Field', 'type' => 'S')),
                new \Fields_DummyField(array('name' => 'module', 'label' => 'Module', 'type' => 'S')),
            )));

        $LinkedFieldListingDisplay = new \Listings_ListingDisplay($LinkedFieldRecordList, $LinkedFieldListingDesign);
        $LinkedFieldListingDisplay->Action = 'linktagfield';
        $LinkedFieldListingDisplay->URLParameterArray = array('field','table');
        $LinkedFieldListingDisplay->EmptyMessage = _tk('no_tags');

        $this->response->build('src/admin/views/ListTagFields.php', array(
            'listing' => $LinkedFieldListingDisplay,
		));
	}

    /**
     * For a given field, this allows tags to be set up for each code.
     */
	public function linkTagField()
	{
        global $scripturl;

        $this->hasPadding = false;
        $this->title = _tk('assign_tags').' ('.\Labels_FieldLabel::GetFieldLabel($this->request->getParameter('field')).')';

        $CodeTable = \DatixDBQuery::PDO_fetch('SELECT fdr_code_table FROM field_directory WHERE fdr_name = :field AND fdr_table = :table', array('field' => $this->request->getParameter('field'),'table' => $this->request->getParameter('table')), \PDO::FETCH_COLUMN);

        //find list of codes to be displayed
        if ($CodeTable{0} == '!')
        {
            $CodeList = \DatixDBQuery::PDO_fetch_all('SELECT cod_code, cod_descr FROM code_types WHERE cod_type = :code_type', array('code_type' => \UnicodeString::substr($CodeTable, 1)), \PDO::FETCH_KEY_PAIR);
        }
        else
        {
            $CodeList = \DatixDBQuery::PDO_fetch_all('SELECT code, description FROM '.$CodeTable, array(), \PDO::FETCH_KEY_PAIR);
        }

        //find existing tags attached to these codes
        $ExistingTagLinks = \DatixDBQuery::PDO_fetch_all('SELECT code, tag, [group] FROM code_tag_links WHERE [table] = :table AND field =:field', array('field' => $this->request->getParameter('field'),'table' => $this->request->getParameter('table')));

        //combine information into structured array that can be accessed later when constructing multi-select elements
        foreach($ExistingTagLinks as $TagDetails)
        {
            $CodeTagLinkArray[$TagDetails['code']][$TagDetails['group']][] = $TagDetails['tag'];
        }

        //get tag sets already linked this field
        $GroupsToLink = \DatixDBQuery::PDO_fetch_all('SELECT [group] FROM code_tag_linked_fields WHERE [table] = :table AND field =:field', array('field' => $this->request->getParameter('field'),'table' => $this->request->getParameter('table')), \PDO::FETCH_COLUMN);

        //get available tags for each group
        $AvailableTagData = \DatixDBQuery::PDO_fetch_all('SELECT recordid, name, group_id FROM code_tags WHERE group_id IN ('.implode(', ', $GroupsToLink).')');
        foreach($AvailableTagData as $TagDetails)
        {
            $AvailableTags[$TagDetails['group_id']][$TagDetails['recordid']] = $TagDetails['name'];
        }

        //construct a group id->name lookup for reference later
        $GroupList = \DatixDBQuery::PDO_fetch_all('SELECT recordid, name FROM code_tag_groups', array(), \PDO::FETCH_KEY_PAIR);

        //for each code, build a set of multiselect elements to allow the user to choose a set of tags
        //from each tag set.
        foreach($CodeList as $Code => $Description)
        {
            $RecordDataItem = array();
            $RecordDataItem['code'] = $Code;
            $RecordDataItem['description'] = $Description;

            $EditJS = array();
            foreach($GroupsToLink as $GroupID)
            {
                $EditJS[] = 'EditCodeTagLinkRow(\''.$this->request->getParameter('table').'\',\''.$this->request->getParameter('field').'\', '.$GroupID.', \''.$Code.'\')';
                $GroupMulticode = \Forms_SelectFieldFactory::createSelectField(
                    'codetags_'.$Code.'_'.$GroupID,
                    '',
                    (!is_array($CodeTagLinkArray[$Code][$GroupID]) ? '' : implode(' ',$CodeTagLinkArray[$Code][$GroupID])),
                    'ReadOnly',
                    true);
                $GroupMulticode->setIgnoreMaxLength();
                $GroupMulticode->setCustomCodes((!empty($AvailableTags[$GroupID]) ? $AvailableTags[$GroupID] : array()));
                $RecordDataItem['group_'.$GroupID] = '<span id="tagcell_'.$Code.'_'.$GroupID.'">'.$GroupMulticode->getField().'</span>';
            }

            $RecordDataItem['edit'] = '<span style="cursor:pointer" id="edit_'.$Code.'_'.$GroupID.'" onclick="jQuery(\'#edit_'.$Code.'_'.$GroupID.'\').hide();'.implode(';', $EditJS).'">[edit]</span>';

            $RecordData[] = $RecordDataItem;
        }

        //The page is presented as a listing rather than a form, since that seemed a more natural way of displaying a list of codes.
        $LinkedFieldRecordList = new \RecordLists_RecordList();
        $LinkedFieldRecordList->AddRecordData($RecordData);

        //we want one column for the code, and then one column each for the associated tag sets
        $ListingColumns = array(new \Fields_DummyField(array('name' => 'description', 'label' => 'Code', 'type' => 'S')));
        foreach($GroupsToLink as $GroupID)
        {
            $ListingColumns[] = new \Fields_DummyField(array('name' => 'group_'.$GroupID, 'label' => $GroupList[$GroupID], 'type' => 'X'));
        }
        //the rightmost column should contain an edit link
        $ListingColumns[] = new \Fields_DummyField(array('name' => 'edit', 'label' => '', 'type' => 'X'));

        $LinkedFieldListingDesign = new \Listings_ListingDesign(array('columns' => $ListingColumns));

        $LinkedFieldListingDisplay = new \Listings_ListingDisplay($LinkedFieldRecordList, $LinkedFieldListingDesign);
        $LinkedFieldListingDisplay->ReadOnly = true;

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'onclick' => 'selectAllMultiCodes();document.frmTagField.submit()', 'action' => 'SAVE'));
        $ButtonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'SendTo(\''.$scripturl.'?action=listtagfields\');', 'action' => 'CANCEL'));

        $this->menuParameters = array(
            'buttons' => $ButtonGroup,
        );

        $this->response->build('src/admin/views/LinkTagField.php', array(
            'listing' => $LinkedFieldListingDisplay,
            'buttons' => $ButtonGroup,
            'field' => $this->request->getParameter('field'),
            'table' => $this->request->getParameter('table'),
		));
	}

    function getModuleForTag($table)
    {
        global $ModuleDefs;

        if(!is_null($ModuleDefs[getModuleFromTable($table)]['NAME']))
        {
            return $ModuleDefs[getModuleFromTable($table)]['NAME'];
        }
        else
        {
            $returnTable = array(
                'inc_injuries'              => 'Injuries',
                'vw_inc_injuries'           => 'Injuries',
                'compl_isd_issues'          => 'Complaints',
                'compl_subjects'            => 'Complaints',
                'rev_main'                  => 'Complaints',
                'vw_respondents_joined'     => 'Claims',
                'inc_medications'           => 'Medications',
                'link_assets'               => 'Contacts',
                'link_respondents'          => 'Contacts',
                'link_contacts'             => 'Contacts',
                'vw_staff_combos'           => 'Contacts',
                'pals_subjects'             => 'PALS',
                'standards_status'          => 'Standards',
                'sab_link_contacts'         => 'Safety Alerts',
                'web_links'                 => 'Safety Alerts'

            );

            return $returnTable[$table] ?: 'NO MODULE FOUND';
        }
    }
}