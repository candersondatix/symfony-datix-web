<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class UnlockCodeSetupController extends Controller
{
    function unlockcodesetup()
    {
        $fieldname  = $this->request->getParameter('fieldname');
        $fieldtable = $this->request->getParameter('table');
        $module     = $this->request->getParameter('module');

        $CodeSetup = new \Setups_CodeSetup($fieldname, $fieldtable);
        $CodeSetup->useSystemWideFieldNames();
        $CodeSetup->unlockCodeSetup();

        $Parameters = array(
            'module' => $module
        );

        $this->call('src\admin\controllers\CodeSetupsTemplateController', 'codesetupslist', $Parameters);
    }
}