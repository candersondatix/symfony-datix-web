<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class LockedRecordsTemplateController extends TemplateController
{
    function lockedrecords()
    {
        if($this->request->getParameter('delete_lock') != '' && bYN(GetParm('RECORD_LOCKING'), 'N'))
        {
            if(!$_SESSION['AdminUser'])
            {
                $SessId = $_SESSION['session_id'];
            }
            else
            {
                $SessId = '';
            }

            \DatixDBQuery::CallStoredProcedure(array(
                'procedure' => 'UnlockRecord',
                'parameters' => array(
                    array('@session_id', $SessId, SQLINT4),
                    array('@lock_id', $this->request->getParameter('delete_lock'), SQLINT4)
                )
            ));
        }

        $this->title = (!IsSubAdmin() ? 'Your ' : '' ) . 'Locked Records';
        $this->module = 'ADM';
        $this->hasPadding = false;
        $this->image = 'Images/icons/icon_cog_locked_records.png';

        if(IsSubAdmin())
        {
            $ADMWhere = MakeSecurityWhereClause('','ADM');

            $sSQL = '
                SELECT
                    record_locks.recordid,
                    lck_linkid,
                    lck_table,
                    lck_sessionid,
                    fullname,
                    lck_time,
                    convert(varchar, lck_time, 120) as real_time
                FROM
                    record_locks,
                    sessions,
                    staff
                WHERE
                    lck_sessionid = sessions.recordid
                    AND
                    ses_userid = staff.recordid' . ($ADMWhere ? ' AND staff.recordid IN (SELECT recordid FROM staff WHERE '.$ADMWhere.')' : '');
        }
        else
        {
            $sSQL = "
                SELECT
                    record_locks.recordid,
                    lck_linkid,
                    lck_table,
                    lck_sessionid,
                    fullname,
                    lck_time,
                    convert(varchar, lck_time, 120) as real_time
                FROM
                    record_locks,
                    sessions,
                    contacts_main
                WHERE
                    lck_sessionid = sessions.recordid
                    AND
                    ses_userid = contacts_main.recordid
                    AND
                    contacts_main.recordid = '" . $_SESSION['contact_login_id'] . "'";
        }

        $resultArray = \DatixDBQuery::PDO_fetch_all($sSQL);

        $this->response->build('src/admin/views/RecordLocks.php', array(
            'resultArray' => $resultArray
        ));
    }
}