<?php
namespace src\admin\controllers;

use src\framework\controller\Controller;
use src\framework\controller\Request;
use src\framework\controller\Response;
use src\codetaggroups\model\CodeTagGroupModelFactory;

/**
 * Controls the process of managing profiles
 */
class CodeTagGroupController extends Controller
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
	
    /**
     * Deletes a tag set record.
     */
	public function deleteTagSet()
	{
        $RecordidToDelete = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        if($RecordidToDelete > 0)
        {
            //delete tag set record
            \DatixDBQuery::PDO_query('DELETE FROM code_tag_groups WHERE recordid = :recordid', array('recordid' => $RecordidToDelete));

            //delete linked tags
            \DatixDBQuery::PDO_query('DELETE FROM code_tags WHERE group_id = :recordid', array('recordid' => $RecordidToDelete));

            //delete linked fields
            \DatixDBQuery::PDO_query('DELETE FROM code_tag_linked_fields WHERE [group] = :recordid', array('recordid' => $RecordidToDelete));

            //delete linked field-tag links for this set
            \DatixDBQuery::PDO_query('DELETE FROM code_tag_links WHERE [group] = :recordid', array('recordid' => $RecordidToDelete));
        }

        AddSessionMessage('INFO', _tk('tag_set_deleted'));
        $this->redirect('app.php?action=listtaggroups');
    }

    /**
     * Saves a tag set record.
     */
	public function saveTagGroup()
	{
        $factory       = new CodeTagGroupModelFactory();
        $mapper        = $factory->getMapper();
        $tagGroup      = $factory->getEntityFactory()->createFromRequest($this->request);

        $mapper->save($tagGroup);

        //delete existing linked fields and then save posted ones.
        \DatixDBQuery::PDO_query('DELETE FROM code_tag_linked_fields WHERE [group] = :group', array('group' => $this->request->getParameter('recordid')));
        if($this->request->getParameter('linked_fields') != '')
        {
            foreach($this->request->getParameter('linked_fields') as $field)
            {
                $Field = new \Fields_Field($field);

                // TODO: We need to find a better solution to deal with fields that are attached to views
                $table = ($Field->getTable() != 'staff' ? $Field->getTable() : 'contacts_main');

                \DatixDBQuery::PDO_query('INSERT INTO code_tag_linked_fields ([group], field, [table]) VALUES (:group, :field, :table)', array('group' => $tagGroup->recordid, 'field' => $field, 'table' => $table));
            }
        }

        AddSessionMessage('INFO', _tk('tag_set_saved'));
        $this->redirect('app.php?action=edittaggroup&recordid='.$tagGroup->recordid);
    }

    /**
     * Saves a tag set record.
     */
	public function listLinkedTags()
	{
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        $this->response->build('src/admin/views/ListLinkedTags.php', array(
            'group' => $this->request->getParameter('recordid'),
        ));
    }

    public static function DefineFields($GroupId)
    {
        global $oForm;

        $oForm->options["FilterLocation"]=-1;

        $oForm->AddEntryField("recordid", 'ID', "H", '');
        $oForm->CurrentField['recordid_table'] = 'code_tags';
        $oForm->CurrentField['ReadOnly'] = true;
        $oForm->CurrentField["visible"] = false;
        $oForm->CurrentField["ForceHide"] = true;
        $oForm->overrideKeys["recordid"] = true;

        $oForm->AddEntryField("group_id", 'Group ID', "H", $GroupId);
        $oForm->CurrentField['recordid_table'] = 'code_tags';
        $oForm->CurrentField['ReadOnly'] = true;
        $oForm->CurrentField["visible"] = false;
        $oForm->CurrentField["ForceHide"] = true;


        $oForm->ConfirmDeleteColumn();
        $oForm->AddEntryFieldW("name", "Tag", "B", "", 500);
        $oForm->CurrentField["filterUI"]="t";
        $oForm->CurrentField["required"] = true;

        $oForm->AddSort("name", "ASC");

        $oForm->DisplayPage();
    }




}