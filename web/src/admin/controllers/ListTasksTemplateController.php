<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;
use src\tasks\model\TaskModelFactory;

class ListTasksTemplateController extends TemplateController
{
    public function listtasks ()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        // sanitize input
        $orderby = $this->request->getParameter('orderby');
        $orderby = \Sanitize::SanitizeString((!is_null($orderby)) ? $orderby : 'recordid');

        $order   = $this->request->getParameter('order');
        $order   = ($order == 'DESC') ? 'DESC' : 'ASC';
        
        $CurrentModule = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $ExcludeArray = array('MED', 'ELE', 'PRO', 'DAS', 'ADM', 'CON', 'ACT', 'DST', 'PAY', 'LIB', 'HSA', 'TOD', 'LOC', 'ATM', 'ATQ', 'ATI', 'AMO', 'AQU');

        $action = \Sanitize::SanitizeURL($_GET['action']);
        
        // create listing table
        $Design = new \Listings_ListingDesign(array('columns' => array(
            new \Fields_DummyField(array('name' => 'recordid', 'label' => 'ID', 'type' => 'S')),
            new \Fields_DummyField(array('name' => 'trg_message', 'label' => 'Name', 'type' => 'S')),
            )
        ));

        $factory = new TaskModelFactory();

        foreach (getModArray($ExcludeArray) as $Module => $ModName)
        {
            $moduleCode = ModuleCodeToID($Module);

            $query = $factory->getQueryFactory()->getQuery();
            $query->where(array('mod_id' => $moduleCode, 'trg_type' => 'AC'))->orderBy(array(array($orderby, $order)));

            $tasks = $factory->getCollection();
            $tasks->setQuery($query);
            
            $RecordList = new \RecordLists_RecordList();
            $RecordList->AddRecordData($tasks);

            $Listings[$Module] = new \Listings_ListingDisplay($RecordList, $Design);
            $Listings[$Module]->Action = 'task';
            $Listings[$Module]->EmptyMessage = sprintf(_tk('no_tasks_created'), $ModuleDefs[$Module]['NAME']);
            $Listings[$Module]->setOrder($orderby);
            $Listings[$Module]->setDirection($order);
            $Listings[$Module]->setSortAction($action . '&amp;module=' . $Module);

            $PanelArray[$Module] = array('Label' => $ModuleDefs[$Module]['NAME']);

            if (!$CurrentModule)
            {
                $CurrentModule = $Module;
            }
        }


        // prepare output
        $this->menuParameters = array(
            'module' => 'ADM',
            'panels' => $PanelArray,
            'current_panel' => $CurrentModule,
            'floating_menu' => false,
        );
        $this->title      = _tk('tasks');
        $this->module     = 'ADM';
        $this->hasMenu    = true;
        $this->hasPadding = true;

        $params = array ();
        $params['Listings']       = $Listings;
        $params['CurrentModule'] = $CurrentModule;

        $this->response->build('src/admin/views/ListTasks.php', $params);
    }
}

