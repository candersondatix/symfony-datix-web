<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

/**
 * @desc Constructs HTML representation of the code setup list of codes using Rico based LiveGrid
 *
 */
class CodeSetupsActionTemplateController extends TemplateController
{
    public function codesetupsaction()
    {
        global $FieldDefsExtra;

        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

        $code = htmlspecialchars($this->request->getParameter('code'));
        $module = \Sanitize::getModule($this->request->getParameter('module'));
        $table = htmlspecialchars($this->request->getParameter('fdr_table'));
        $codefieldname = \Sanitize::SanitizeString($this->request->getParameter('codefieldname'));
    
        try
        {
            $CodeFieldtitle = \Fields_field::getFieldLabel($codefieldname, $table);
        }
        catch (\Exception $e)
        {
            require_once 'Source/setups/CodeSetups.php';
             
            // this is for RCG, which isn't a field in the usual sense and isn't stored in the DB
            $hcFields = getHardCodedFields($module);
            $CodeFieldtitle = '';
    
            foreach ($hcFields as $field)
            {
                if ($field['fdr_name'] == $codefieldname)
                {
                    $CodeFieldtitle = $field['fdr_label'];
                    break;
                }
            }
        }
    
        //Clear cache so that any changes take effect without logging out. Ideally this should be after save, but
        // I do not want to alter saving in Rico libraries to do this.
        unset($_SESSION['CachedValues']['cod_info'][$module][$codefieldname]);

        $this->title = _tk('code_setups') . " - " . $CodeFieldtitle;
        $this->module = 'ADM';
        
        $FormArray = array (
            "Parameters" => array(
                "Panels" => "True",
                "Condition" => false
            ),
            "name" => array(
                "Title" => _tk('code_setups'),
                "MenuTitle" => _tk('code_setups'),
                "NewPanel" => true,
                "Rows" => array()
            ),
        );
    
        $data['module'] = $module;
        $data['table'] = $table;
        
        $SetupTable = new \FormTable();
        $SetupTable->MakeForm($FormArray, $data, $module);
        $SetupTable->MakeTable();

        //Need to convert "ACR" into "ATM", "AMO"... etc. in order to correctly retrieve parents.
        if($module == 'ACR')
        {
            require_once 'Source/setups/ImportCodes.php';
            $parentModule = getAccreditationModule($FieldDefsExtra, $codefieldname);
        }
        else
        {
            $parentModule = $module;
        }
        
    	//hacky workaround because the subjects fields are passed in the url as com_, but referenced for parenting as csu_.
        //can't do anything more general with CheckFieldMappings because the location fields are passed as com_ but must not be changed to csu_
        if($codefieldname == 'com_subject' || $codefieldname == 'com_subsubject' || $codefieldname == 'com_stafftype')
        {
            $Parents = GetParents(array("module" => $parentModule, "field" => CheckFieldMappings($module, $codefieldname)));
        }
        else
        {
            $Parents = GetParents(array("module" => $parentModule, "field" => $codefieldname));
        }
    	$code_type     = '';
        $ShowExportImport = true;
        $CCS2Fields = array(
            'inc_affecting_tier_zero',
            'inc_type_tier_one',
            'inc_type_tier_two',
            'inc_type_tier_three',
            'inc_level_intervention',
            'inc_level_harm',
            'cla_affecting_tier_zero',
            'cla_type_tier_one',
            'cla_type_tier_two',
            'cla_type_tier_three',
            'cla_level_intervention',
            'cla_level_harm',
            'com_affecting_tier_zero',
            'com_type_tier_one',
            'com_type_tier_two',
            'com_type_tier_three'
        );
        $CCS2CodeTables = array(
            'code_ccs2_tier_zero',
            'code_ccs2_tier_one',
            'code_ccs2_tier_two',
            'code_ccs2_tier_three',
            'code_ccs2_level_intervention',
            'code_ccs2_level_harm'
        );
    
        // Code setups for CCS2 fields doesn't allow export and import
        if (in_array($codefieldname, $CCS2Fields))
        {
            $ShowExportImport = false;
        }
    
        $Parents[0] = CheckFieldMappings($module, $Parents[0], true);
        $Parents[1] = CheckFieldMappings($module, $Parents[1], true);
    
        if ($code[0] == "!")
        {
            $code_type = \UnicodeString::substr($code, 1, \UnicodeString::strlen($code));
            $code_where = "cod_type = '$code_type'";
            $code = "code_types";
        }
    
        $FieldSetupMappings = new \Setups_FieldSetupMappings($code, $code_type);
    
    	$jsSettings = array(
    		'code_table' => $code,
    		'module'     => $module,
    	);

        $mappedCode = ($this->request->getParameter('mapped_code') == '1');

        if ($mappedCode)
        {
            AddSessionMessage('INFO', _tk('mapped_code_read_only_msg'));
        }
    	
        $this->response->build('src/admin/views/CodeSetupsAction.php', array(
            'module' => $module,
            'table' => $table,
            'codefieldname' => $codefieldname,
            'Parents' => $Parents,
            'ShowExportImport' => $ShowExportImport,
            'CCS2CodeTables' => $CCS2CodeTables,
            'code_where' => $code_where,
            'code_type' => $code_type,
            'code' => $code,
            'FieldSetupMappings' => $FieldSetupMappings,
            'jsSettings' => $jsSettings,
            'readOnly' => ($this->request->getParameter('form_action') == 'readonly'),
            'mappedCode' => $mappedCode,
            'recordId' => ($this->request->getParameter('recordid') ?: false)
        ));
    }

    /**
    * @desc Defimes the fields used in the Live grid for the code setup and displays the grid
    *
    * @param string $module The module of the field definition in field_formats.
    * @param string $field The field name.
    * @param string $code_table The code_table value for the field definition in field_formats.
    * @param string $code_type The code_type if the code uses the code_types table.
    * @param string Array $Parents Array of parent fields.
    *
    */
    public static function DefineFields($module, $field, $code_table, $code_type = "" , $Parents = array(), $Parents_table = "")
    {
        global $oForm;
    
        $CCS2Fields = array(
            'inc_affecting_tier_zero',
            'inc_type_tier_one',
            'inc_type_tier_two',
            'inc_type_tier_three',
            'inc_level_intervention',
            'inc_level_harm',
            'cla_affecting_tier_zero',
            'cla_type_tier_one',
            'cla_type_tier_two',
            'cla_type_tier_three',
            'cla_level_intervention',
            'cla_level_harm',
            'com_affecting_tier_zero',
            'com_type_tier_one',
            'com_type_tier_two',
            'com_type_tier_three'
        );

        if ($field == 'cdo_status')
        {
            $oForm->options["canAdd"] = false;
            $oForm->options["canDelete"] = false;
        }
    
        if (in_array($code_table, array('code_inc_likeli','code_ra_likeli','code_inc_conseq','code_ra_conseq')))
        {
            $listorderColumn = 'listorder';
        }
        else
        {
            $listorderColumn = 'cod_listorder';
        }
    
        $FieldSetupMappings = new \Setups_FieldSetupMappings($code_table, $code_type);
    
        $oForm->options["FilterLocation"] = -1;
        $oForm->convertCharSet = false;
    
        if (!empty($Parents[0]))
        {
            $oForm->AddEntryFieldW("cod_parent", \Fields_Field::getFieldLabel($Parents[0], $Parents_table), "MS", "",200);
            $oForm->CurrentField["SelectField"] = $Parents[0];
            $oForm->CurrentField["filterUI"] = "t";
    
            if (in_array($field, $CCS2Fields))
            {
                $oForm->CurrentField['ReadOnly'] = true;
            }
            
            if ($field == 'inc_clintype')
            {
                // display numbers to aid NPSA code mapping
                $oForm->CurrentField["showNumbers"] = true;
            }
        }
    
        if (!empty($Parents[1]))
        {
            $oForm->AddEntryFieldW("cod_parent2", \Fields_Field::getFieldLabel($Parents[1], $Parents_table), "MS", "",200);
            $oForm->CurrentField["SelectField"] = $Parents[1];
            $oForm->CurrentField["filterUI"] = "t";
    
            if (in_array($field, $CCS2Fields))
            {
                $oForm->CurrentField['ReadOnly'] = true;
            }
        }
    
        $oForm->AddSort($listorderColumn, "ASC");

        $hideCodeField = ($field == 'link_patrelation');

        if (!$hideCodeField)
        {
            if ($code_table == "code_types")
            {
                $oForm->AddEntryFieldW("cod_code", "Code", "B", "",125);
                $oForm->AddSort("cod_code", "ASC");
            }
            else
            {
                $oForm->AddEntryFieldW("code", "Code", "B", "",125);
                $oForm->AddSort("code", "ASC");
            }

            if (in_array($field, $CCS2Fields))
            {
                $oForm->CurrentField['ReadOnly'] = true;
            }

            $oForm->CurrentField["required"] = true;
            $oForm->CurrentField["unique"] = true;
            $oForm->CurrentField["filterUI"] = "t";
            $oForm->CurrentField["uppercase"] = true;
            $oForm->CurrentField["LengthOverride"] = 6;
            $oForm->CurrentField["pattern"] = '^[^\*\?&\|\\\"\'\\s!<>=\-\+%\(\)\,;]*$';
            $oForm->CurrentField["Help"] = 'Codes cannot contain any of the following characters: * ? | & \ " \' space ! < > = - + % ( ) , ;';

            if ($field == 'cdo_status')
            {
                $oForm->CurrentField["ReadOnly"] = true;
            }

            $oForm->ConfirmDeleteColumn();
        }
    
        if ($code_table == 'code_types')
        {
            $oForm->AddEntryFieldW("cod_descr", "Description", "B", "", 500);
            $oForm->CurrentField["filterUI"] = "t";

            $oForm->AddEntryFieldW("cod_type", "Type", "H", ($code_type) ? $code_type : "", 250);

            $oForm->overrideKeys["COD_CODE"] = true;
            $oForm->overrideKeys["COD_TYPE"] = true;
        }
        else
        {
            $oForm->AddEntryFieldW("description", "Description", "B", "", 500);
            $oForm->CurrentField["LengthOverride"] = 254;
            $oForm->CurrentField["filterUI"] = "t";

            if ($hideCodeField)
            {
                $oForm->overrideKeys["DESCRIPTION"] = true;
                $oForm->CurrentField["required"] = true;
                $oForm->CurrentField["unique"] = true;
            }
            else
            {
                $oForm->overrideKeys["CODE"] = true;
            }
        }
    
        if (in_array($field, $CCS2Fields))
        {
            $oForm->CurrentField['ReadOnly'] = true;
        }

        if($code_table == 'code_fin_type')
        {
            $oForm->AddEntryFieldW("cod_reserve", "Cost type", "S", "", 50);
            $oForm->CurrentField["SelectValues"] = "1,2";
            $oForm->CurrentField["SelectDescriptions"] = "Indemnity,Expenses";
        }
    
        foreach ($FieldSetupMappings->SetupMappings as $i => $fsmap)
        {
            $oForm->AddEntryFieldW($fsmap['fsm_setup_col'], $fsmap['fsm_setup_col_title'], $fsmap['fsm_setup_col_type'], "", 125);
            $oForm->CurrentField["SelectField"] = $fsmap['fsm_setup_col'];
            $oForm->CurrentField["SelectSql"] = $fsmap['fsm_setup_col_select_sql'];
            
            if ($field == 'inc_clintype' && $fsmap['fsm_setup_col'] == 'cod_npsa')
            {
                // display numbers to aid NPSA code mapping
                $oForm->CurrentField["showNumbers"] = true;
            }
        }
    
        $oForm->AddEntryFieldW($listorderColumn, "Order", "I", "", 50);
        $oForm->DefaultSort = "$listorderColumn ASC";

        if ($field == 'cdo_status')
        {
            $oForm->CurrentField["ReadOnly"] = true;
        }
    
        $oForm->AddEntryFieldW("cod_priv_level", "Active?", "S", "", 50);
        $oForm->CurrentField["SelectValues"] = "Y,N,X";

        $oForm->AddEntryFieldW("cod_web_colour", "Colour", "CP", "", 100);
        $oForm->CurrentField["colour"] = true;
        $oForm->CurrentField["pattern"] = '^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$';
    
        $oForm->DisplayPage();
    }

    public function excessSetup()
    {
        $this->title = _tk('code_setups').' - '._tk('claims_insurer_excess');
        $this->module = 'ADM';

        $FormArray = array (
            'Parameters' => array(
                'Panels'    => 'True',
                'Condition' => false
            ),
            'name' => array(
                'ControllerAction' => array(
                    'makeInsurerExcessTable' => array(
                        'controller' => 'src\\admin\\controllers\\CodeSetupsActionTemplateController'
                    )
                ),
                'Rows'     => array()
            )
        );

        $ExcessTable = new \FormTable();
        $ExcessTable->MakeForm($FormArray, array(), '');

        $ExcessTable->MakeTable();

        $this->response->build('src/admin/views/InsurerExcessCodeSetup.php', array(
            'excessTable' => $ExcessTable
        ));
    }

    public function makeInsurerExcessTable()
    {
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css'
        ), array('attrs' => 'media="screen"'));

        $this->response->build('src/admin/views/InsurerExcessTable.php', array());
    }

    public static function InsurerExcessDefineFields()
    {
        global $oForm;

        if (GetParm('FMT_DATE_WEB') == 'US')
        {
            $dateRegex = '\b(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/(19|20)?[0-9]{2}\b';
        }
        else
        {
            $dateRegex = '\b(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/(19|20)?[0-9]{2}\b';
        }

        $oForm->options['FilterLocation'] = -1;
        $oForm->overrideKeys['RECORDID'] = true;

        $oForm->AddEntryField('recordid', 'ID', 'RC', '');
        $oForm->CurrentField['recordid_table'] = 'claims_insurer_excess';
        $oForm->CurrentField['ReadOnly'] = true;
        $oForm->CurrentField['visible'] = false;
        $oForm->CurrentField['ForceHide'] = true;

        $oForm->AddEntryFieldW('cie_insurer', \Labels_FieldLabel::GetFieldLabel('cla_insurer'), 'S', '', 125);
        $oForm->AddSort('cie_insurer', 'ASC');
        $oForm->CurrentField['SelectField'] = 'cie_insurer';
        $oForm->CurrentField['SelectSql'] = 'SELECT code, description FROM code_cla_insurer ORDER BY cod_listorder, description';
        $oForm->CurrentField['filterUI'] = 't';

        $oForm->AddEntryFieldW('cie_start_date', _tk('excess_start_date'), 'D', '',125);
        $oForm->CurrentField['Help'] = 'Enter date as '.$_SESSION['DATE_FORMAT'];
        $oForm->CurrentField['pattern'] = $dateRegex;
        $oForm->CurrentField['filterUI'] = 't';

        $oForm->AddEntryFieldW('cie_end_date', _tk('excess_end_date'), 'D', '', 125);
        $oForm->CurrentField['Help'] = 'Enter date as '.$_SESSION['DATE_FORMAT'];
        $oForm->CurrentField['pattern'] = $dateRegex;
        $oForm->CurrentField['filterUI'] = 't';

        $oForm->AddEntryFieldW('cie_excess', _tk('excess'), 'F', '', 125);
        $oForm->CurrentField['filterUI'] = 't';
    }
}