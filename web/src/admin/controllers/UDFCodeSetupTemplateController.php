<?php
namespace src\admin\controllers;

use src\framework\controller\TemplateController;

/**
 * Controls the code setup section of the extra field edit form.
 */
class UDFCodeSetupTemplateController extends TemplateController
{
	/**
	* @desc Constructs UDF code setup list of codes using Rico based LiveGrid
	*/
	function udfsetup()
	{
        /**
         * Add rico table assets
         */
        $this->addJs(array(
            'rico/src/rico.js',
            'rico/src/ricoMenu.js',
            'rico/src/ricoGridCommon.js',
            'rico/src/ricoLiveGrid.js',
            'rico/src/ricoLiveGridAjax.js',
            'rico/src/ricoLiveGridJSON.js',
            'rico/src/ricoLiveGridMenu.js',
            'rico/src/ricoLiveGridForms.js',
            'js_functions/colorpicker/js/colorpicker.js'
        ));
        $this->addCss(array(
            'rico/src/css/min.rico.css',
            'rico/src/css/DatixLiveGrid.css',
            'rico/src/css/DatixLiveGridForms.css',
            'js_functions/colorpicker/css/colorpicker.css'
        ), array('attrs' => 'media="screen"'));

	    $udf_id = $this->request->getParameter('udfid');   
	
	    $UDFInfo = new \Fields_ExtraField($udf_id);
	    $CodeFieldtitle = $UDFInfo->getLabel(); 
	
        $this->title = _tk('code_setups') . " - " . $CodeFieldtitle;
        $this->module = 'ADM';

        $FormArray = array (
            "Parameters" => array(
                "Panels" => "True",
                "Condition" => false
            ),
            "name" => array(
                "Title" => _tk('code_setups'),
                "MenuTitle" => _tk('code_setups'),
                "NewPanel" => true,
                "Rows" => array()
            ),
        );
        
	    $data['udfid'] = $udf_id;

        $IsFieldLocked = (bYN($UDFInfo->getCentralLocked()) && IsCentrallyAdminSys() && !IsCentralAdmin());
        $mode = (($IsFieldLocked || $UDFInfo->getCodeLike()) ? 'r' : 'rw');

        if ($UDFInfo->getCodeLike())
        {
            AddSessionMessage('INFO', _tk('code_setup_read_only_msg'));
        }
	
        $SetupTable = new \FormTable();
        $SetupTable->MakeForm($FormArray, $data, $this->module);

        $SetupTable->MakeTable();

        $this->response->build('src/admin/views/UDFCodeSetup.php', array(
            'data' => $data,
        	'udf_id' => $udf_id,
            'table' => 'udf_codes',
            'where' => "field_id = " . $udf_id,
        	'mode' => $mode
        ));
	
	}    

	/**
	* @desc Defines the fields used in the Live grid for the UDF code setup and displays the grid
	*
	* @param int $udf_id The UDF field_id.
	* @todo Factor duplicated validation logic into a single spot
	*
	*/
	static public function DefineFields($udf_id) 
	{
	  global $oForm;
	
	  $oForm->options["FilterLocation"]=-1;
      $oForm->options["NoResize"]=true;

	  $oForm->AddEntryFieldW("field_id", "Type", "H", $udf_id, 25);
	  
	  $oForm->AddEntryFieldW("udc_code", "Code", "B", "",125);
	  $oForm->AddSort("udc_code", "ASC");
	  $oForm->CurrentField["required"] = true;
	  $oForm->CurrentField["unique"] = true;
	  $oForm->CurrentField["filterUI"]="t";
	  $oForm->CurrentField["uppercase"] = true;
	  $oForm->CurrentField["LengthOverride"] = 6;
	  // TODO: the validation logic is cuat n' pasted from CodeSetups.php
	  // the validation logic should be applied in one spot to avoid the duplication of effort if it ever needs to be changed
	  // perhaps put the validation in a code model
	  $oForm->CurrentField["pattern"] = '^[^\*\?&\|\\\"\'\\s!<>=\-\+%\(\)\,;]*$';
	  $oForm->CurrentField["Help"] = 'Codes cannot contain any of the following characters: * ? | & \ " \' space ! < > = - + % ( ) , ;';
	  
	  $oForm->ConfirmDeleteColumn();
	  $oForm->AddEntryFieldW("udc_description", "Description", "TA", "", 500);
	  $oForm->CurrentField["filterUI"]="t";
	  $oForm->AddEntryFieldW("listorder", "Order", "I", "", 80);
	  $oForm->DefaultSort = "listorder ASC";
      $oForm->AddEntryFieldW("active", "Active?", "S", "", 50);
      $oForm->CurrentField["SelectValues"] = "Y,N,X";
	  
	  $oForm->overrideKeys["FIELD_ID"] = true;
	  $oForm->overrideKeys["UDC_CODE"] = true;
	  $oForm->ResizeRowIdx = -1;
	  //$oForm->SortAsc();
	  $oForm->DisplayPage();
	}
}