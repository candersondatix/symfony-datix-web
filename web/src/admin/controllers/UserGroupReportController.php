<?php

namespace src\admin\controllers;

use src\framework\controller\Controller;

class UserGroupReportController extends Controller
{
    /**
     * Displays the users listing in the User Access Report screen.
     */
    public function userGroupRepResponse()
    {
        global $scripturl, $ModuleDefs;

        $grp_id    = \Sanitize::SanitizeInt($this->request->getParameter('grp_id'));
        $module    = \Sanitize::getModule($this->request->getParameter('module'));
        $PrintMode = ($this->request->getParameter('print') == '1' ? true : false);
        $today     = getdate();

        $todayStr = $today['year'].'-'.$today['mon'].'-'.$today['mday'].' 00:00:00';

        $colspan = 5;

        $ADMWhere = MakeSecurityWhereClause('', 'ADM');

        $sql = '
            SELECT
                staff.recordid, staff.login, sta_surname, sta_forenames, sta_daccessstart, sta_daccessend, lockout,
                sec_group_permissions.perm_value
            FROM
                staff LEFT JOIN
                sec_staff_group ON sec_staff_group.con_id = staff.recordid LEFT JOIN
                sec_group_permissions ON sec_group_permissions.grp_id = sec_staff_group.grp_id
            WHERE
                sec_group_permissions.item_code = \''.$ModuleDefs[$module]['PERM_GLOBAL'].'\' AND
                sec_staff_group.grp_id IN (
                    SELECT
                        recordid
                    FROM
                        sec_groups
                    WHERE
                        (grp_type = 1 OR grp_type = 3) AND recordid = '.$grp_id.'
                )
        ';

        if ($ADMWhere)
        {
            $sql .= ' AND staff.recordid IN (SELECT recordid FROM staff WHERE '.$ADMWhere.')';
        }

        $resultArray = \DatixDBQuery::PDO_fetch_all($sql);

        // Columns
        if (count($resultArray) > 0)
        {
            foreach($resultArray as $id => $row)
            {
                $url = $scripturl.'?action=edituser&recordid='.$row["recordid"];
                $con_url = \CSRFGuard::addTokenToURL($url);
                $sta_daccessend = strtotime($row['sta_daccessend']);
                $status = (($sta_daccessend == '' || !$sta_daccessend || $sta_daccessend >= $todayStr) && $row['lockout'] != 'Y');

                $resultArray[$id]['con_url'] = $con_url;
                $resultArray[$id]['status'] = $status;
            }
        }

        $this->response->build('src/admin/views/UserGroupReport.php', array(
            'resultArray' => $resultArray,
            'colspan'     => $colspan,
            'PrintMode'   => $PrintMode
        ));
    }
}