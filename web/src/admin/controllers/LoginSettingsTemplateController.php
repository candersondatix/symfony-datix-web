<?php

namespace src\admin\controllers;

use src\framework\controller\TemplateController;

class LoginSettingsTemplateController extends TemplateController
{
    /**
     * Login and Audit action
     * Outputs form for updating the login and audit settings
     *
     * @return void
     */
    public function login_settings()
    {
        $output = array();

        $sql = "
            SELECT
                parameter,
                parmvalue
            FROM
                globals
            WHERE
                parameter = 'LOGIN_TRY'
                OR
                parameter = 'LOGIN_INACTIVITY_DAYS'
        ";

        $result = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($result as $row)
        {
            $key = $row['parameter'];
            $output[$key] = htmlspecialchars($row['parmvalue']);
        }

        if (!empty($_SESSION['MESSAGES']['ERROR']))
        {
            // if either of the values have failed validation then we display
            // the previous value entered, rather than the one saved in the DB
            $output['LOGIN_TRY'] = $this->request->getParameter('login-attempts');
            $output['LOGIN_INACTIVITY_DAYS'] = ($this->request->getParameter('login-inactivity') ? $this->request->getParameter('login-inactivity') : -1);
        }

        $messages = GetSessionMessages();

        $this->title = _tk('login_settings-title');
        $this->module = 'ADM';
        $this->hasPadding = false;

        $this->response->build('src/admin/views/LoginSettings.php', array(
            'messages' => $messages,
            'output' => $output,
            'login_inactivity' => $this->request->getParameter('login-inactivity')
        ));
    }

    /**
     * Login and Audit update action
     * Updates the login and audit settings and redirects to the index function
     *
     * @return void
     */
    public function login_settings_update()
    {
        if ($this->request->getMethod() != 'POST')
        {
            $this->redirect('app.php?action=login_settings');
        }

        if ($this->request->getMethod() == 'POST' && $this->request->getParameter('cancel'))
        {
            $this->redirect('app.php?action=home&module=ADM');
        }

        $clean   = array();
        $valid   = true;

        if (!is_numeric($this->request->getParameter('login-attempts')) || $this->request->getParameter('login-attempts')  < 0)
        {
            AddSessionMessage('ERROR', _tk('login_audit-max-logins-error'));
            $valid = false;
        }
        else
        {
            $clean['login-attempts'] = (int) $this->request->getParameter('login-attempts');
            SetGlobal('LOGIN_TRY', $clean['login-attempts']);
        }

        if ($this->request->getParameter('login-inactivity-disable') && $this->request->getParameter('login-inactivity-disable') == true)
        {
            $clean['login-inactivity'] = -1;
        }
        else if ($this->request->getParameter('login-inactivity'))
        {
            if (!is_numeric($this->request->getParameter('login-inactivity')) || $this->request->getParameter('login-inactivity') < 0)
            {
                AddSessionMessage('ERROR', _tk('login_audit-days-inactive-error'));
                $valid = false;
            }

            $clean['login-inactivity'] = (int) $this->request->getParameter('login-inactivity');
        }
        else
        {
            $clean['login-inactivity'] = -1;
        }

        if (!$valid)
        {
            $this->redirect('app.php?action=login_settings');
        }

        SetGlobal('LOGIN_INACTIVITY_DAYS', $clean['login-inactivity']);

        AddSessionMessage('INFO', _tk('login_audit-update-success'));

        $this->redirect('app.php?action=login_settings');
    }
}