<div class="panel">
    <div class="padded_div">
        <form id="cqcoutcomeform" name="cqcoutcomeform" action="<?= $this->scripturl ?>?action=cqccheckduplicateoutcomes" method="post">
            Selected Locations:
            <?php foreach ($this->Locations as $ID => $Name): ?>
            <div><?= $Name ?><input type="hidden" name="locations[]" value="<?= $ID ?>"></div>
            <?php endforeach ?>
            <br>
            Choose Outcomes:
            <div id="selectalloutcomes" style="cursor:pointer"><a onclick="jQuery.each(jQuery('.outcome_checkbox'), function(){jQuery(this).attr('checked', true);jQuery('.below_outcome_'+jQuery(this).attr('value')).attr('checked', true)}); jQuery('#selectalloutcomes').hide(); jQuery('#deselectalloutcomes').show();">Select all</a></div>
            <div id="deselectalloutcomes" style="display:none;cursor:pointer"><a onclick="jQuery('.outcome_checkbox').attr('checked',false);jQuery('.prompt_checkbox').attr('checked',false); jQuery('#deselectalloutcomes').hide(); jQuery('#selectalloutcomes').show();">Deselect all</a></div>
            <?php foreach ($this->Outcomes as $OutcomeID => $OutcomeName): ?>
                <div>
                    <img src="Images/expand_windowbg.gif" id="img_expand_<?= $OutcomeID ?>" onclick="jQuery('.div_below_outcome_<?= $OutcomeID ?>').show();jQuery('#img_expand_<?= $OutcomeID ?>').hide();jQuery('#img_collapse_<?= $OutcomeID ?>').show()">
                    <img src="Images/collapse_windowbg.gif" id="img_collapse_<?= $OutcomeID ?>" style="display:none" onclick="jQuery('.div_below_outcome_<?= $OutcomeID ?>').hide();jQuery('#img_collapse_<?= $OutcomeID ?>').hide();jQuery('#img_expand_<?= $OutcomeID ?>').show()">


                    <?php if(!in_array($OutcomeID, $this->ExistingOutcomes)): ?>
                        <input type="checkbox" class="outcome_checkbox" id="outcome<?= $OutcomeID ?>" name="outcomes[]" value="<?= $OutcomeID ?>" onclick="jQuery('.below_outcome_<?= $OutcomeID ?>').prop('checked', jQuery('#outcome<?= $OutcomeID ?>').prop('checked'))">
                    <?php else: ?>
                        <input type="checkbox" disabled="disabled" checked="checked">
                    <?php endif; ?>
                      <?= $OutcomeName ?>
                </div>
                <?php foreach ($this->Prompts[$OutcomeID] as $PromptID => $PromptName): ?>
                    <div style="padding-left: 35px;display:none" class="div_below_outcome_<?= $OutcomeID ?>">
                        <?php if(!in_array($OutcomeID, $this->ExistingOutcomes)): ?>
                            <input type="checkbox" class="prompt_checkbox below_outcome_<?= $OutcomeID ?>" name="prompts[]" value="<?= $PromptID ?>" onclick="if(jQuery('.below_outcome_<?= $OutcomeID ?>:checked').length == 0){jQuery('#outcome<?= $OutcomeID ?>').prop('checked', false)}else{jQuery('#outcome<?= $OutcomeID ?>').prop('checked', true)}">
                        <?php else: ?>
                            <?php if(!is_array($this->ExistingPrompts[$OutcomeID]) || !in_array($PromptID, $this->ExistingPrompts[$OutcomeID])): ?>
                                <input type="checkbox" disabled="disabled">
                            <?php else: ?>
                                <input type="checkbox" disabled="disabled" checked="checked">
                            <?php endif; ?>
                        <?php endif; ?>
                        <?= $PromptName ?>
                     </div>
                <?php endforeach ?>
            <?php endforeach ?>
        </form>
    </div>
    <div class="button_wrapper">
        <input type="button" value="<?= _tk('apply_outcomes') ?>" onclick="jQuery('#cqcoutcomeform').submit();">
        <?php if(isset($_GET['location'])): ?>
            <input type="button" value="<?= _tk('btn_cancel') ?>" onclick="SendTo('<?= getRecordURL(array('module' => 'LOC', 'recordid' => intval($_GET['location']))) ?>');">
        <?php else: ?>
            <input type="button" value="<?= _tk('btn_cancel') ?>" onclick="SendTo('<?= $this->scripturl ?>?action=cqcselectlocations');">
        <?php endif ?>
    </div>
</div>
