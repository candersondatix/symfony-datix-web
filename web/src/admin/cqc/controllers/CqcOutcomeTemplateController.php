<?php
namespace src\admin\cqc\controllers;

use src\cqc\model\OutcomeTemplateModelFactory;

use src\framework\controller\TemplateController;
use src\framework\controller\Response;
use src\framework\controller\Request;

class CqcOutcomeTemplateController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);
    }
    
    /**
     * Displays a listing of CQC Outcome Template records.
     */
    public function listCqcTemplates()
    {
        $this->title  = _tk('cqc_outcome_templates');
        
        $Design = new \Listings_ListingDesign(array('columns' => array(
            new \Fields_DummyField(array('name' => 'recordid', 'label' => 'ID', 'type' => 'S')),
            new \Fields_DummyField(array('name' => 'cto_theme', 'label' => 'Theme', 'type' => 'C', 'codes' => \DatixDBQuery::PDO_fetch_all('SELECT cod_code, cod_descr FROM code_types WHERE cod_type = \'CQCTHM\'', array(), \PDO::FETCH_KEY_PAIR))),
            new \Fields_DummyField(array('name' => 'cto_desc', 'label' => 'Description', 'type' => 'S')),
            new \Fields_DummyField(array('name' => 'cto_dpub', 'label' => 'Date published', 'type' => 'D')),
            )
        ));

        
        $factory = new OutcomeTemplateModelFactory();
        $query   = $factory->getQueryFactory()->getQuery();
        
        $query->orderBy(array('recordid'));
        
        $Outcomes = $factory->getCollection();
        $Outcomes->setQuery($query);
        
        
        $RecordList = new \RecordLists_RecordList();
        $RecordList->AddRecordData($Outcomes);

        $OutcomeListing = new \Listings_ListingDisplay($RecordList, $Design);
        $OutcomeListing->Action = 'editcqcoutcometemplate';

        $this->response->build('src/admin/cqc/views/ListCqcTemplates.php', array(
            'OutcomeListing' => $OutcomeListing
        ));
    }
    
    /**
     * Displays the CQC Outcome Template form.
     */
    public function editCqcOutcomeTemplate()
    {
        $id = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $OutcomeTemplate = new \CQC_OutcomeTemplate($id);
        
        if ($this->request->getMethod() == 'POST')
        {
            $OutcomeTemplate->UpdateFromPost();
            $OutcomeTemplate->SaveConfigurationToDatabase();
            AddSessionMessage('INFO', 'Saved');
            $this->redirect('app.php?action=editcqcoutcometemplate&recordid='.$OutcomeTemplate->GetID());
        }
        
        $buttonGroup = new \ButtonGroup();
        $buttonGroup->AddButton(array('id' => 'btnSave', 'name' => 'btnSave', 'label' => _tk('btn_save'), 'onclick' => 'submitClicked=true;if(validateOnSubmit()){selectAllMultiCodes();jQuery(\'#outcometemplateform\').submit()}', 'action' => 'SAVE'));
        $buttonGroup->AddButton(array('id' => 'btnCancel', 'name' => 'btnCancel', 'label' => _tk('btn_cancel'), 'onclick' => 'SendTo(\'app.php?action=listcqctemplates\');', 'action' => 'CANCEL'));

        $this->title          = ($id ? _tk('edit_outcome_template') : _tk('new_outcome_template'));
        $this->menuParameters = array('buttons' => $buttonGroup);
        $this->hasPadding     = false;
        
        $trail = new \BreadCrumbs();
        $trail->add($OutcomeTemplate->data['cto_desc_short'], 'app.php?action=editcqcoutcometemplate&recordid=' . $id, 0);

        $FormArray = array(
            'outcome' => array(
                'Title' => 'Template details',
                'Rows' => array(
                    'recordid',
                    'cto_ref',
                    'cto_desc',
                    'cto_desc_short',
                    'cto_dpub',
                    'cto_guidance',
                    'cto_theme',
                    'cto_active',
                )
            ),
            'prompts' => array(
                'Title' => 'Prompt templates',
                "Class" => 'CQC_OutcomeTemplate',
                "Function" => "ListPromptTemplates",
                'Rows' => array()
            )
        );

        $FormDesign = new \Forms_FormDesign();
        $FormDesign->MandatoryFields = array(
            'cto_desc' => 'outcome',
        );
        $FormDesign->UserExtraText = array(
            'cto_active' => 'The outcome will appear in the list to apply to registered locations.',
        );

        $Table = new \FormTable('', 'CQO', $FormDesign);
        $Table->MakeForm($FormArray, $OutcomeTemplate->data, 'CQO');
        $Table->MakeTable();

        $GLOBALS['JSFunctions'][] = MakeJavaScriptValidation('ADM', $FormDesign);

        $this->response->build('src/admin/cqc/views/EditCqcOutcomeTemplate.php', array(
            'Table'      => $Table,
            'breadcrumb' => $trail->output()
        ));
    }

    /**
     * Custom CQC outcomes listing report. Only ever run against one outcome instance record.
     */
    public function cqcOutcomeSummaryReport()
    {
        $outcomeId = ($this->request->getParameter('outcome_id') ?: null);

        if ($outcomeId === null)
        {
            fatal_error('No outcome data provided');
        }

        $where = MakeSecurityWhereClause('cqc_outcomes.recordid = '.intval($outcomeId), 'CQO');

        $Listing = new \Listings_ModuleListingDesign(array('module' => 'CQO'));
        $Listing->LoadColumnsFromDB();

        foreach ($Listing->getColumns() as $ListingColumn)
        {
            if (\UnicodeString::substr($ListingColumn, 0, 4) == 'tier')
            {
                $Columns[] = 'cdo_location';
            }
            else
            {
                $Columns[] = $ListingColumn;
            }
        }

        $sql = '
            SELECT cqc_outcomes.recordid, '.implode(', ', $Columns).'
            FROM
            cqc_outcomes
            '.
            ($where ? ' WHERE ('.$where.')' : '');

        $MainData = \DatixDBQuery::PDO_fetch_all($sql);

        if (empty($MainData))
        {
            fatal_error('No outcome data provided');
        }

        foreach ($MainData as $Record)
        {
            $OutcomeIDs[] = $Record['recordid'];
            $OutcomeData = $Record;
        }

        $Listing = new \Listings_ModuleListingDesign(array('module' => 'CQP'));
        $Listing->LoadColumnsFromDB();

        $Columns = array();

        foreach ($Listing->getColumns() as $ListingColumn)
        {
            if (\UnicodeString::substr($ListingColumn, 0, 4) == 'tier')
            {
                $Columns[] = 'cdo_location';
            }
            else
            {
                $Columns[] = $ListingColumn;
            }
        }

        $sql = '
            SELECT
                cqc_prompts.cdo_location, cqc_prompts.cqc_outcome_id, cqc_prompts.recordid, '.implode(', ', $Columns).'
            FROM
                cqc_prompts
            WHERE
                cqc_prompts.cqc_outcome_id IN ('.implode(',', $OutcomeIDs).')
        ';

        $MainData = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($MainData as $Record)
        {
            $PromptIDs[] = $Record['recordid'];
            $PromptOutcomeRef[$Record['recordid']] = $Record['cqc_outcome_id'];
            $OutcomeData['prompts'][$Record['recordid']] = $Record;
        }

        $Listing = new \Listings_ModuleListingDesign(array('module' => 'CQS'));
        $Listing->LoadColumnsFromDB();

        $Columns = array();

        foreach($Listing->getColumns() as $ListingColumn)
        {
            if (\UnicodeString::substr($ListingColumn, 0, 4) == 'tier')
            {
                $Columns[] = 'cdo_location';
            }
            else
            {
                $Columns[] = $ListingColumn;
            }
        }

        $sql = '
            SELECT
                cqc_subprompts.cdo_location, cqc_subprompts.cqc_outcome_id, cqc_subprompts.cqc_prompt_id,
                cqc_subprompts.recordid, '.implode(', ', $Columns).'
            FROM
                cqc_subprompts
            WHERE
                cqc_subprompts.cqc_prompt_id IN ('.implode(',', $PromptIDs).')
        ';

        $MainData = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($MainData as $Record)
        {
            $SubpromptIDs[] = $Record['recordid'];
            $SubpromptPromptRef[$Record['recordid']] = $Record['cqc_prompt_id'];
            $OutcomeData['prompts'][$Record['cqc_prompt_id']]['subprompts'][$Record['recordid']] = $Record;
        }

        $Listing = new \Listings_ModuleListingDesign(array('module' => 'ACT'));
        $Listing->LoadColumnsFromDB();

        $sql = '
            SELECT
                act_cas_id, recordid, '.implode(', ', $Listing->getColumns()).'
            FROM
                ca_actions
            WHERE
                (act_module = \'CQO\' AND act_cas_id IN ('.implode(',', $OutcomeIDs).')) OR
                (act_module = \'CQP\' AND act_cas_id IN ('.implode(',', $PromptIDs).')) OR
                (act_module = \'CQS\' AND act_cas_id IN ('.implode(',', $SubpromptIDs).'))
        ';

        $ActionData = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($ActionData as $Record)
        {
            if ($Record['act_module'] == 'CQO')
            {
                $OutcomeData['actions'][] = $Record;
            }

            if ($Record['act_module'] == 'CQP')
            {
                $OutcomeData['prompts'][$Record['act_cas_id']]['actions'][] = $Record;
            }

            if ($Record['act_module'] == 'CQS')
            {
                $OutcomeData['prompts'][$SubpromptPromptRef[$Record['act_cas_id']]]['subprompts'][$Record['act_cas_id']]['actions'][] = $Record;
            }
        }

        $Listing = new \Listings_ModuleListingDesign(array('module' => 'LIB'));
        $Listing->LoadColumnsFromDB();

        $sql = '
            SELECT
                CASE
                WHEN cqo_id IS NOT NULL THEN \'CQO\'
                WHEN cqp_id IS NOT NULL THEN \'CQP\'
                WHEN cqs_id IS NOT NULL THEN \'CQS\'
                END as lib_module,
                  COALESCE(cqo_id, cqp_id, cqs_id) as lib_cas_id, '.implode(', ', $Listing->getColumns(true)).',
                  documents_main.doc_type, documents_main.doc_notes
            FROM
                link_library
                LEFT JOIN library_main ON link_library.lib_id = library_main.recordid
                LEFT JOIN documents_main ON library_main.recordid = documents_main.lib_id
            WHERE
                (cqo_id IN ('.implode(',', $OutcomeIDs).')) OR
                (cqp_id IN ('.implode(',', $PromptIDs).')) OR
                (cqs_id IN ('.implode(',', $SubpromptIDs).'))
        ';

        $EvidenceData = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($EvidenceData as $Record)
        {
            if ($Record['lib_module'] == 'CQO')
            {
                $OutcomeData['evidence'][] = $Record;
            }

            if ($Record['lib_module'] == 'CQP')
            {
                $OutcomeData['prompts'][$Record['lib_cas_id']]['evidence'][] = $Record;
            }

            if ($Record['lib_module'] == 'CQS')
            {
                $OutcomeData['prompts'][$SubpromptPromptRef[$Record['lib_cas_id']]]['subprompts'][$Record['lib_cas_id']]['evidence'][] = $Record;
            }
        }

        $this->title = 'CQC Outcomes - Full listing for outcome reference '.$OutcomeData['cto_ref'].' ('.code_descr('CQO', 'cdo_location', $OutcomeData['cdo_location']).')';

        ini_set('include_path', ini_get('include_path').';../Classes/');
        require_once 'export/PHPExcel.php';
        require_once 'export/PHPExcel/Writer/Excel2007.php';
        require_once 'export/PHPExcel/IOFactory.php';
        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setTitle("DatixWeb Excel export");
        $objPHPExcel->getProperties()->setSubject("DatixWeb Excel export");
        $objPHPExcel->getProperties()->setDescription("DatixWeb Excel export");

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Datix CQC Outcome Summary');

        $HTML = '<table width="100%" align="center" border="0" id="listingtable">';

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', $this->title);

        $excelRow = 3;

        require_once 'Source/reporting/CustomListingReports.php';
        $HTML .= constructCQCreportsections(array($OutcomeData), 'CQO', $objPHPExcel, $excelRow);

        $HTML .= '</table>';

        $HTML .= '</table>';

        // Store PHPExcel object in session
        $_SESSION['crosstabtablePHPExcel'] = serialize($objPHPExcel);
        unset($objPHPExcel);

        // Store HTML in session to allow export to PDF
        $_SESSION['listingreportstream'] = $HTML;

        $this->hasMenu = false;

        $this->response->build('src/admin/cqc/views/OutcomeSummaryReport.php', array(
            'saved_query' => $this->request->getParameter('saved_query'),
            'html'        => $HTML,
            'recordid'    => $outcomeId
        ));
    }
}