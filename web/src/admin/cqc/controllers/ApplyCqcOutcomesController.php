<?php
namespace src\admin\cqc\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;

class ApplyCqcOutcomesController extends TemplateController
{
    public function __construct(Request $request, Response $response)
    {
        $this->module     = 'ADM';
        $this->hasPadding = false;
        parent::__construct($request, $response);
    }
    
    /**
     * Displays a list of locations with checkboxes for user to select before choosing outcomes to link.
     */
    public function cqcSelectLocations()
    {
        $this->title = _tk('select_cqc_locations');
        $this->response->build('src/admin/cqc/views/SelectLocations.php', array(
            'Locations' => \DatixDBQuery::PDO_fetch_all('SELECT recordid, loc_name FROM vw_locations_main WHERE loc_tier_depth = (SELECT MAX(loc_tier_depth) from vw_locations_main)', array(), \PDO::FETCH_KEY_PAIR),     
        ));
    }
    
    /**
    * Displays a list of outcomes with checkboxes for user to select having chosen locations.
    */
    public function cqcSelectOutcomes()
    {
        if ($this->request->getParameter('locations') == '' && $this->request->getParameter('location') == '')
        {
            AddSessionMessage('ERROR', 'Please select at least one location');
            $this->redirect('app.php?action=cqcselectlocations');
        }

        $outcomes = \DatixDBQuery::PDO_fetch_all('SELECT recordid, cto_ref, cto_desc FROM cqc_template_outcomes WHERE cto_active = \'Y\'');

        if (empty($outcomes))
        {
            AddSessionMessage('ERROR', 'Your system contains no CQC templates. You will need to load these templates in before continuing.');

            if ($this->request->getParameter('location') == '')
            {
                $this->redirect('app.php?action=cqcselectlocations');
            }
            else
            {
                $this->redirect(getRecordURL(array('module' => 'LOC', 'recordid' => $this->request->getParameter('location'))));
            }
        }
        
        $this->title = _tk('apply_cqc_outcomes');
        
        $selectedLocations = array();
        if ($this->request->getParameter('location'))
        {
            $selectedLocations[] = $this->request->getParameter('location');
        }
        else
        {
            $selectedLocations = $this->request->getParameter('locations');
        }
        
        

        $Locations = \DatixDBQuery::PDO_fetch_all('SELECT recordid, loc_name FROM locations_main WHERE recordid IN('.implode(', ',$selectedLocations).')', array(), \PDO::FETCH_KEY_PAIR);

        foreach ($outcomes as $OutcomeDetails)
        {
            $Outcomes[$OutcomeDetails['recordid']] = $OutcomeDetails['cto_ref'] . ': ' . $OutcomeDetails['cto_desc'];
        }

        $PromptData = \DatixDBQuery::PDO_fetch_all('SELECT recordid, cqc_outcome_template_id, ctp_ref, ctp_desc FROM cqc_template_prompts WHERE cqc_outcome_template_id IN ('.implode(', ', array_keys($Outcomes)).')');

        foreach ($PromptData as $PromptDetails)
        {
            $Prompts[$PromptDetails['cqc_outcome_template_id']][$PromptDetails['recordid']] = $PromptDetails['ctp_ref'] . ': ' . $PromptDetails['ctp_desc'];
        }

        $ExistingOutcomes = \DatixDBQuery::PDO_fetch_all('SELECT cqc_outcome_template_id FROM CQC_DATA_OUTCOMES WHERE cdo_location IN ('.implode(', ', $selectedLocations).') GROUP BY cqc_outcome_template_id HAVING count(*) = :count', array('count' => count($selectedLocations)), \PDO::FETCH_COLUMN);

        foreach ($ExistingOutcomes as $OutcomeID)
        {
            $ExistingPrompts[$OutcomeID] = \DatixDBQuery::PDO_fetch_all('SELECT cqc_prompt_template_id FROM CQC_PROMPTS WHERE cqc_outcome_template_id = :outcome_id', array('outcome_id' => $OutcomeID), \PDO::FETCH_COLUMN);
        }

        $this->response->build('src/admin/cqc/views/SelectOutcomes.php', array(
            'Locations'        => $Locations,
            'Outcomes'         => $Outcomes,
            'Prompts'          => $Prompts,
            'ExistingOutcomes' => $ExistingOutcomes,
            'ExistingPrompts'  => $ExistingPrompts
        ));
    }
}