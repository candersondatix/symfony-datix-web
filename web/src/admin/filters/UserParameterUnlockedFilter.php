<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
* Secures controllers on the basis of users having access to user parameters.
*/
class UserParameterUnlockedFilter extends ControllerFilter
{
    /**
    * Checks that the user is able to access user parameters
    * 
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException
    */
    public function doAction($action)
    {
        /*
         * user parameters are only inaccessible if the user is a non-central admin in a centrally administered
         * system and the REMOVE_USER_PARAM_ACCESS global is Y.
         */
        if(IsCentrallyAdminSys() && !IsCentralAdmin() && bYN(GetParm('REMOVE_USER_PARAM_ACCESS', 'N')))
        {
            throw new \URLNotFoundException();
        }
        else
        {
            return $this->proceed($action);
        }
    }
}