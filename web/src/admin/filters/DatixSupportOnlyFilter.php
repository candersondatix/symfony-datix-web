<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
 * Restricts access to Datix Support accounts only.
 */
class DatixSupportOnlyFilter extends ControllerFilter
{
    public function doAction($action)
    {
        if (!bYN(GetParm('DATIX_SUPPORT_ACCOUNT', 'N')))
        {
            throw new \PermissionDeniedException();
        }
        return $this->proceed($action);
    }
}