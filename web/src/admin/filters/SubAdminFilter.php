<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
* Secures controllers on the basis of users being a Full Admin.
*/
class SubAdminFilter extends ControllerFilter
{
    /**
    * Checks if the user is has sub- or greater admin permissions
    * 
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException
    */
    public function doAction($action)
    {
        if ($_SESSION['AdminUser'])
        {
            return $this->proceed($action);
        }
        else if (GetAccessLevel('ADM') == 'ADM_USER_ADMIN')
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();  
        } 
    }
}