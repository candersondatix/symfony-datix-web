<?php
namespace src\admin\filters;

use src\framework\controller\ControllerFilter;

/**
* Secures controllers on the basis of users having the global ADM_GROUP_SETUP set.
*/
class ADM_GROUP_SETUPGlobalFilter extends ControllerFilter
{
    /**
    * Checks that the user does not have the global ADM_GROUP_SETUP set to "N"
    * 
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException
    */
    public function doAction($action)
    {
        if (bYN(GetParm('ADM_GROUP_SETUP', 'Y')))
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        } 
    }
}