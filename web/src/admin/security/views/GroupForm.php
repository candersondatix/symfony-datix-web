
<form name="frmEditGroup" action="app.php?action=savegroup" method="post">
<input type="hidden" name="updateid" value="<?php echo htmlspecialchars($this->grp['updateid']) ?>" />
<input type="hidden" name="grp_id" value="<?php echo Sanitize::SanitizeInt($this->grp['grp_id']) ?>" />
<input type="hidden" name="con_id" value="<?php echo Sanitize::SanitizeInt($this->grp['con_id']) ?>" />
<input type="hidden" name="pfl_id" value="<?php echo Sanitize::SanitizeInt($this->grp['pfl_id']) ?>" />
<input type="hidden" name="search_module" value="" />
<input type="hidden" name="user_action" value="save" />



<script language="JavaScript" type="text/javascript">
    window.onload = function showFirstModuleSettings()
    {
        document.getElementById('div'+document.getElementById("lbModule").value).style.display = "block";
    }
    		
    function showModuleSettings(selectedModule)
    {
        <?php foreach ($this->HiddenDivs as $code): ?>
        if (document.getElementById("div<?php echo $code; ?>")) {document.getElementById("div<?php echo $code; ?>").style.display = "none"};
        <?php endforeach; ?>
        
        // Show div for selected module
        document.getElementById("div"+selectedModule).style.display = "block";
    }
    		
    function showAccessLevels(groupType)
    {
    	if (groupType & <?= GRP_TYPE_ACCESS ?> == <?= GRP_TYPE_ACCESS ?>)
    	{
            var display = "rowEle.style.display = document.getElementById('tr_group_type').style.display";
    	}
    	else
    	{
    		var display = "rowEle.style.display = 'none'";
    	}
    	var rowElements = document.getElementsByName('tr_access_level');
    	var rowEle = null;
    	for (var i = 0; i < rowElements.length; i++)
    	{
    		var rowEle = rowElements[i];
    		eval(display);
    	}
    }
    
       
    <?php if ($this->$LDAPEnabled): ?>
    function dnLDAPSyncPopup()
    {
    	mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
    	mywindow.location.href = 'app.php?action=ldapsync&groupid=<?=  Sanitize::SanitizeInt($this->grp["grp_id"]) ?>&token=<?= CSRFGuard::getCurrentToken() ?>';
    	if (mywindow.opener == null) {
    		mywindow.opener = self;
    	}
    }
    <?php endif; ?>			    
</script>

    
<?php if ($this->grp['error_message'] != ''): ?>
<div class="error_div"><?php echo htmlspecialchars($this->grp['error_message']) ?></div>
<?php endif ?>
    
<?php echo $this->GroupTable->GetFormTable(); ?>
<?php echo $this->ButtonGroup->getHTML(); ?>
		
</form>
<?php if ($this->FormType != "Print"): ?>
<?php echo JavascriptPanelSelect(false, Sanitize::SanitizeString($_GET['panel']), $this->GroupTable); ?>
<?php endif ?>
