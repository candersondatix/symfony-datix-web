<?php
namespace src\admin\security\controllers;

use src\framework\controller\TemplateController;
use src\framework\controller\Request;
use src\framework\controller\Response;
use src\security\groups\model\Group;
use src\security\groups\model\GroupModelFactory;

/**
 * Controls the process of managing security groups.
 */
class GroupTemplateController extends TemplateController
{
	/**
	 * Constructor - sets the module as 'ADM' globally for this controller.
	 * 
	 * @param Request  $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response)
    {
        $this->module = 'ADM';
        parent::__construct($request, $response);

        $this->hasPadding = false;
    }
	
    /**
     * Constructs a list of existing groups.
     */
	public function listGroups()
	{
	    
	    $factory = new GroupModelFactory();
	    
	    list($query, $where, $fields) = $factory->getQueryFactory()->getQueryObjects();

        $this->title = _tk('group_list_title');
        $this->image = 'images/icons/icon_cog_group.png';
        $this->menuParameters = array(
            'menu_array' => array(
                array(
                    'label' => 'Add new Group',
                    'link' => 'action=editgroup'
                ),
                array(
                    'label' => 'List blank security groups',
                    'link' => 'action=listblanksecuritygroups',
                    'condition' => bYN(GetParm('LIST_BLANK_SEC_GROUPS', 'N'))
                )
            ),
            'floating_menu' => false
        );

        $MatchLetter = ($this->request->getParameter('match') != '' && !($this->request->getParameter('match') < 'A' || $this->request->getParameter('match') > 'Z') ? \Sanitize::SanitizeString($this->request->getParameter('match')) : 'all');
        $MatchField = ($this->request->getParameter('field') != '' ? \Sanitize::SanitizeString($this->request->getParameter('field')) : 'grp_code');
        $sorttype = ($this->request->getParameter('sort') != '' ? \UnicodeString::strtoupper(\Sanitize::SanitizeString($this->request->getParameter('sort'))) : 'ASC');

        $MatchLetter = EscapeQuotes($MatchLetter);
        $MatchField = EscapeQuotes($MatchField);

        if ($MatchField == 'recordid')
        {
            $MatchLetter = 'all';
        }

        // Need to check for LDAP authentication to display LDAP mapping section
        require_once("Source/libs/LDAP.php");
        $ldap = new \datixLDAP();
        if ($ldap->LDAPEnabled())
        {
            $colspan = 7;
        }
        else
        {
            $colspan = 6;
        }

        if ($MatchField == 'grp_type' && $MatchLetter == 'R')
        {
            $FieldCollection = $factory->getQueryFactory()->getFieldCollection();
            $FieldCollection2 = $factory->getQueryFactory()->getFieldCollection();
            $FieldCollection->field($MatchField)->eq(GRP_TYPE_ACCESS);
            $FieldCollection2->field($MatchField)->eq(GRP_TYPE_ACCESS + GRP_TYPE_EMAIL);
            $where->add('OR', $FieldCollection, $FieldCollection2);
        }
	    else if ($MatchField == 'grp_type' && $MatchLetter == 'D')
        {
            $fields->field($MatchField)->eq(GRP_TYPE_DENY_ACCESS);
            $where->add($fields);
        }
        else if ($MatchField == 'grp_type' && $MatchLetter == 'E')
        {
            $fields->field($MatchField)->eq(GRP_TYPE_EMAIL);
            $where->add($fields);
        }
        else if ($MatchLetter != 'all')
        {
            $fields->field($MatchField)->like($MatchLetter.'%');
            $where->add($fields);
        }
        
        
        //if ordering by type, need to set a custom ordering, since the numbers don't relate to an alphabetical order
        if($MatchField == 'grp_type')
        {
            $query->orderBy(array(
                array(
                    array('CASE WHEN ? = 3 THEN 1
                                WHEN ? = 1 THEN 2
                                WHEN ? = 2 THEN 3
                                WHEN ? = 4 THEN 4
                                ELSE 5
                           END', 'grp_type'),
                    $sorttype)
                )
            );
        }
        else
        {
            $query->orderBy(array(array($MatchField, $sorttype)));
        }

        $query->where($where);
        $groups = $factory->getCollection();
        $groups->setQuery($query);
        
        
		$this->response->build('src/admin/views/ListGroups.php', array(
            'data' => $groups,
            'colspan' => $colspan,
            'matchletter' => $MatchLetter,
            'matchfield' => $MatchField,
            'sorttype' => $sorttype,
            'ldap' => $ldap,
            'sql' => $sql,
            'dtxdebug' => $GLOBALS['dtxdebug']
		));
	}
	
	function editgroup()
	{
	    $this->hasPadding = true;
	    
		$recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
		
		if (!empty($recordid))
		{
			$grp_id = $recordid;
		}
		
		if ($this->request->getParameter('grp_id'))
		{
			$grp_id = $this->request->getParameter('grp_id') ?: '';
		}
		
		$error = $this->request->getParameter('error');
		
		if (!empty($grp_id) && empty($error))
		{
			
            $factory = new GroupModelFactory();
            $group = $factory->getMapper()->find($grp_id);
            $grp = $group->getVars();
            $grp['permission'] = $group->getPermissions();
            $grp['grp_id'] = $grp_id;
	
			// Retrieve LDAP mappings
			$sql = '
                SELECT
                    ldap_dn
                FROM
                    sec_ldap_groups
                WHERE
                    grp_id = :grp_id
            ';
	
			$data = \DatixDBQuery::PDO_fetch_all($sql, array('grp_id' => $grp['grp_id']));
	
			foreach ($data as $row)
			{
				$grp['ldap_dn'][] = $row["ldap_dn"];
			}
	
			// Select all permissions for security group
			$sql = '
                SELECT
                    item_code,
                    perm_value
                FROM
                    sec_group_permissions
                WHERE
                    grp_id = :grp_id
            ';
	
			$data = \DatixDBQuery::PDO_fetch_all($sql, array('grp_id' => $grp['grp_id']));
	
			foreach ($data as $perm_value)
			{
				$grp[\UnicodeString::strtoupper($perm_value['item_code'])] = $perm_value['perm_value'];
			}
		} else {
		    $grp = \Sanitize::SanitizeRawArray($this->request->getParameters());
		}
	
		if ($this->request->getParameter('con_id'))
		{
			$grp['con_id'] = $this->request->getParameter('con_id');
		}
	
		if ($this->request->getParameter('pfl_id'))
		{
			$grp['pfl_id'] = $this->request->getParameter('pfl_id');
		}

		if ($this->request->getParameter('module'))
		{
            $grp['default_selected_module'] = $this->request->getParameter('module');
		}

		
		
		
		//require_once 'Source/security/SecurityBase.php';
		require_once 'Source/security/SecurityGroups.php'; // needed for panelGroupDetails panelUsers, panelProfiles
		
		$FormArray = array(
		                'Parameters' => array('Panels' => true,
		                                'Condition' => false
		                ),
		                'details' => array('Title' => 'Group details',
		                                'NewPanel' => true,
		                                'NoTitle' => true,
		                                'Function' => 'panelGroupDetails',
		                                'Rows' => array()
		                )
		);
		
		// If there isn't a defined group ID then dont display the users menu
		if (!empty($grp['grp_id']))
		{
		    $FormArray['users'] = array(
		                    'Title' => 'Users',
		                    'NewPanel' => true,
		                    'Condition' => $grp['grp_id'],
		                    'Function' => 'panelUsers',
		                    'Rows' => array()
		    );
		
		    if(IsFullAdmin())
            {
                $FormArray['profiles'] = array(
                                'Title' => 'Profiles',
                                'NewPanel' => true,
                                'Condition' => $grp['grp_id'],
                                'Function' => 'panelProfiles',
                                'Rows' => array()
                );
            }
		}

		if ($grp["con_id"])
		{
		    $CancelLabel = 'Back to user';
            $cancelAction = 'SendTo(\'app.php?action=edituser&recordid='.$grp["con_id"].'\');';
		}
		elseif ($grp["pfl_id"])
		{
		    $CancelLabel = 'Back to profile';
            $cancelAction = 'SendTo(\'app.php?action=editprofile&recordid='.$grp["pfl_id"].'\');';
		}
		else
		{
		    $CancelLabel = _tk('btn_cancel');
            $cancelAction = 'SendTo(\'app.php?action=listgroups\');';
		}
		
		$ButtonGroup = new \ButtonGroup();
		
		if (IsFullAdmin())
		{
		    $ButtonGroup->AddButton(array(
		                    'label'   => 'Save',
		                    'onclick' => 'selectAllMultiCodes();checkDatixSelects();document.frmEditGroup.submit();',
		                    'action'  => 'SAVE'
		    ));

            $ButtonGroup->AddButton(array(
                'label'   => $CancelLabel,
                'onclick' => 'if(confirm(\'Are you sure you want to leave this record? Any changes will be lost.\')){'.$cancelAction.'};',
                'action'  => 'CANCEL'
            ));
		}
		else
        {
            $ButtonGroup->AddButton(array(
                            'label'   => $CancelLabel,
                            'onclick' => $cancelAction,
                            'action'  => 'CANCEL'
            ));
        }
		
		if ($grp["grp_id"] && IsFullAdmin())
		{
		    $ButtonGroup->AddButton(array(
		                    'label'   => 'Delete group',
		                    'onclick' => 'if(confirm(\'Remove this group from the system?  Press OK to confirm or Cancel to return to the group screen.\')){SendTo(\'app.php?action=deletegroup&recordid=' . $grp['grp_id'] . '\');}',
		                    'action'  => 'DELETE'
		    ));
		}
		
		$this->title = ($grp['grp_id'] ? 'Edit Group Details' : 'Add New Group');
		$this->menuParameters = array(
		                'buttons' => $ButtonGroup,
		                'formarray'   => $FormArray,
		                'module'  => 'ADM'
		);
		
		
		
		// hidden divs
		$ExcludeArray = array('ELE', 'PRO');
		
		$HiddenDivs = array();
		
		foreach ($this->registry->getModuleDefs() as $Module => $Defs)
		{
		    if (ModIsLicensed($Module) && !$Defs['DUMMY_MODULE'] && !in_array($Module, $ExcludeArray))
		    {
		        $HiddenDivs[] = $Defs['CODE'];
		    }
		}
		
		$FormType = (IsFullAdmin() ? 'Edit' : 'ReadOnly');
		
		$GroupTable = new \FormTable($FormType, 'ADM');
		$GroupTable->MakeForm($FormArray, $grp, array());
		$GroupTable->MakeTable();
		
		// Need to check for LDAP authentication to display LDAP mapping section
		require_once 'Source/libs/LDAP.php';
		$ldap = new \datixLDAP();
		
		if (isset($_SESSION['security_group']))
		{
		    unset($_SESSION['security_group']);
		}
		
		$_POST["recordid"] = $this->grp["grp_id"];
		
		
		$this->response->build('src/admin/security/views/GroupForm.php', array(
		                'LDAPEnabled' => $ldap->LDAPEnabled(),
		                'grp'         => $grp,
		                'ButtonGroup' => $ButtonGroup,
		                'FormArray'   => $FormArray,
		                'HiddenDivs'  => $HiddenDivs,
		                'GroupTable'  => $GroupTable,
		                'FormType'   => $FormType
		));
		
	}

    public function listblanksecuritygroups()
    {
        $this->title = 'List blank security groups';
        $this->image = 'images/icons/icon_cog_group.png';
        $this->hasPadding = true;
        $this->menuParameters = array(
            'menu_array' => array(
                array(
                    'label' => 'Add new Group',
                    'link' => 'action=editgroup'
                ),
                array(
                    'label' => 'List blank security groups',
                    'link' => 'action=listblanksecuritygroups',
                    'condition' => bYN(GetParm('LIST_BLANK_SEC_GROUPS', 'N'))
                )
            ),
            'floating_menu' => false
        );

        $this->response->build('src/admin/security/views/ListBlankSecurityGroups.php', array(
            'groups' => Group::getGroupsWithBlankWhereClauses(),
            'ModuleDefs' => $this->registry->getModuleDefs()
        ));

    }
}