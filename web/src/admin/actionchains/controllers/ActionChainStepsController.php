<?php
namespace src\admin\actionchains\controllers;

use src\framework\controller\Controller;
use src\actionchains\model\ActionChainModelFactory;
use src\actionchains\model\StepModelFactory;

class ActionChainStepsController extends Controller
{
    /**
     * Renders the Steps section of the Action Chain design form.
     */
    public function stepsSection()
    {
        $id = $this->request->getParameter('recordid');
        
        if (isset($id))
        {
            $factory = new ActionChainModelFactory();
            $chain   = $factory->getMapper()->find($id);
            
            if (!isset($chain))
            {
                throw new \URLNotFoundException();
            }
            
            foreach ($chain->steps as $key => $step)
            {
                $steps .= $this->call(get_class($this), 'getStep', array('id' => $step->recordid, 'suffix' => $key + 1));
            }
            $maxSuffix = count($chain->steps) <= 1 ? 2 : count($chain->steps) + 1;
        }
        else
        {
            // new form
            $maxSuffix = 2;
            $steps .= $this->call(get_class($this), 'getStep');
        }
        
        $this->response->build('src/admin/actionchains/views/GetStepsSection.php', array(
            'steps'     => $steps,
            'maxSuffix' => $maxSuffix
        ));
    }
    
    /**
     * Renders an individual step on the Action Chain design form.
     */
    public function getStep()
    {
        $suffix   = $this->request->getParameter('suffix') ?: 1;
        $factory  = new StepModelFactory();
        $expanded = false;
        
        if ($this->request->getParameter('id') !== null)
        {
            $step = $factory->getMapper()->find($this->request->getParameter('id'));
            if (!isset($step))
            {
                throw new \URLNotFoundException();
            }
        }
        else
        {
            $stepNumber = $this->request->getParameter('maxorder') !== null ? $this->request->getParameter('maxorder') + 1 : 1;
            $step = $factory->getEntityFactory()->createObject(array('acs_order' => $stepNumber));
            $expanded = true;
        }
        
        $typeFieldLabel = \Labels_FormLabel::GetFormFieldLabel('act_type', 'Type', 'ACT');
        $typeField      = \Forms_SelectFieldFactory::createSelectField('acs_act_type', 'ACT', $step->acs_act_type, '', false, $typeFieldLabel, '', '', $suffix);
        $typeField->setFieldFormatsName('act_type');
        $typeField->setTable('ca_actions');
        
        if ($suffix > 1)
        {
            // We delegate to other controller actions a fair amount in rendering this view in order to fit around the existing form-building logic.
            // While a little convoluted, this at least still enables us to keep our views separate and our controllers cleaner.
            $rightHandLink['onclick'] = $this->call(get_class($this), 'getStepTitleLinkJs', array('suffix' => $suffix));
            $rightHandLink['text']    = _tk('delete_section');
        }
        
        $FormArray = array(
            'Parameters' => array(
                'Condition' => false,
                'Suffix' => $suffix),
            'step_'.$suffix => array(
                'Title' => $this->call(get_class($this), 'getStepTitle', array('expanded' => ($expanded ? 1 : 0))),
                'ContactSuffix' => $suffix,
                'right_hand_link' => $rightHandLink,
                'Rows' => array(
                    array('Name' => 'acs_order', 'Title' => 'Step number', 'Type' => 'number', 'Width' => 5, 'MinValue' => 1),
                    array('Name' => 'acs_act_type', 'Title' => $typeFieldLabel, 'Type' => 'formfield', 'FormField' => $typeField),
                    array('Name' => 'acs_act_desc', 'Title' => 'Description', 'Type' => 'string', 'MaxLength' => 254, 'Width' => 50),
                    array('Name' => 'start_date', 'Title' => 'Start date', 'Type' => 'custom', 'HTML' => $this->call(get_class($this), 'getStartDateRow', array('id' => $step->recordid, 'suffix' => $suffix))),
                    array('Name' => 'due_date', 'Title' => 'Due date', 'Type' => 'custom', 'HTML' => $this->call(get_class($this), 'getDueDateRow', array('id' => $step->recordid, 'suffix' => $suffix))),
                    array('Name' => 'reminder', 'Title' => 'Reminder', 'Type' => 'custom', 'HTML' => $this->call(get_class($this), 'getReminderRow', array('id' => $step->recordid, 'suffix' => $suffix)))
                )
            )
        );
        
        foreach ($step->getVars() as $property => $value)
        {
            $Data[$property.'_'.$suffix] = $value;
        }
        
        if (!$expanded)
        {
            $GLOBALS['FieldDefsExtra']['ADM']['acs_order']['Show']    = false;
            $GLOBALS['FieldDefsExtra']['ADM']['acs_act_type']['Show'] = false;
            $GLOBALS['FieldDefsExtra']['ADM']['acs_act_desc']['Show'] = false;
            $GLOBALS['FieldDefsExtra']['ADM']['start_date']['Show']   = false;
            $GLOBALS['FieldDefsExtra']['ADM']['due_date']['Show']     = false;
            $GLOBALS['FieldDefsExtra']['ADM']['reminder']['Show']     = false;
        }

        $FormDesign = new \Forms_FormDesign();
        $FormDesign->MandatoryFields['acs_order_'.$suffix]    = 'step_section_div_'.$suffix;
        $FormDesign->MandatoryFields['acs_act_type_'.$suffix] = 'step_section_div_'.$suffix;
        $FormDesign->MandatoryFields['acs_act_desc_'.$suffix] = 'step_section_div_'.$suffix;
        
        $Table = new \FormTable('', 'ADM', $FormDesign);
        $Table->MakeForm($FormArray, $Data, 'ADM');
        
        $this->response->build('src/admin/actionchains/views/GetStep.php', array(
            'Table'    => $Table,
            'recordid' => $step->recordid,
            'suffix'   => $suffix,
            'actType'  => $typeFieldLabel,
            'actDesc'  => \Labels_FormLabel::GetFormFieldLabel('act_descr', 'Description', 'ACT')
        ));
    }
    
    /**
     * Renders the Javascript used by the step title right hand link.
     */
    public function getStepTitleLinkJs()
    {
        $this->response->build('src/admin/actionchains/views/StepTitleLinkJS.php', array('suffix' => (int) $this->request->getParameter('suffix')));
    }
    
    /**
     * Renders the title portion of an individual step section.
     */
    public function getStepTitle()
    {
        $this->response->build('src/admin/actionchains/views/StepTitle.php', array('expanded' => ($this->request->getParameter('expanded') == 1)));
    }
    
    /**
     * Creates the Start date row of the step form.
     */
    public function getStartDateRow()
    {
        $factory  = new StepModelFactory();
        if ($this->request->getParameter('id') !== null)
        {
            $step = $factory->getMapper()->find($this->request->getParameter('id'));
            if (!isset($step))
            {
                throw new \URLNotFoundException();
            }
        }
        else
        {
            $step = $factory->getEntityFactory()->createObject();
        }
        
        $suffix = $this->request->getParameter('suffix');
        
        $daysTypes = array('CAL' => 'Calendar', 'WOR' => 'Working');
        $afterTypes = array('START' => 'Chain commencement', 'STEP' => 'Completion of step');
        
        $startNo = new \FormField();
        $startNo->MakeNumberField('acs_start_no_'.$suffix, $step->acs_start_no, 5, 0, '', 0);
        
        $startDaysType = \Forms_SelectFieldFactory::createSelectField('acs_start_days_type', 'ADM', $step->acs_start_days_type, '', false, '', '', '', $suffix);
        $startDaysType->setCustomCodes($daysTypes);
        $startDaysType->setSuppressCodeDisplay();
        $startDaysType->setWidth(100);
        $startDaysType->setNoResize();
        
        $startAfterType = \Forms_SelectFieldFactory::createSelectField('acs_start_after_type', 'ADM', $step->acs_start_after_type, '', false, '', '', '', $suffix);
        $startAfterType->setCustomCodes($afterTypes);
        $startAfterType->setSuppressCodeDisplay();
        $startAfterType->setWidth(140);
        $startAfterType->setNoResize();
        $startAfterType->setOnChangeExtra($this->call(get_class($this), 'getStartAfterTypeJS', array('suffix' => $suffix)));
        
        $startAfterStep = new \FormField();
        $startAfterStep->MakeNumberField('acs_start_after_step_'.$suffix, $step->acs_start_after_step, 5, 0, '', '', ($step->acs_start_after_type == 'START' || $step->acs_start_after_type == ''), 'checkAfterStepNo(this);');

        $this->response->build('src/admin/actionchains/views/GetStartDateRow.php', array(
            'startNo'        => $startNo,
            'startDaysType'  => $startDaysType,
            'startAfterType' => $startAfterType,
            'startAfterStep' => $startAfterStep
        ));
    }
    
    /**
     * Creates the Javascript for the acs_start_after_step select.
     */
    public function getStartAfterTypeJS()
    {
        $this->response->build('src/admin/actionchains/views/StartAfterTypeJS.php', array('suffix' => $this->request->getParameter('suffix')));
    }
    
    /**
    * Creates the Due date row of the step form.
    */
    public function getDueDateRow()
    {
        $factory  = new StepModelFactory();
        if ($this->request->getParameter('id') !== null)
        {
            $step = $factory->getMapper()->find($this->request->getParameter('id'));
            if (!isset($step))
            {
                throw new \URLNotFoundException();
            }
        }
        else
        {
            $step = $factory->getEntityFactory()->createObject();
        }
        
        $suffix = $this->request->getParameter('suffix');
        
        $daysTypes  = array('CAL' => 'Calendar', 'WOR' => 'Working');
        $afterTypes = array('START' => 'Chain commencement', 'STEP' => 'Completion of step');
        
        $dueNo = new \FormField();
        $dueNo->MakeNumberField('acs_due_no_'.$suffix, $step->acs_due_no, 5, 0, '', 0);
        
        $dueDaysType = \Forms_SelectFieldFactory::createSelectField('acs_due_days_type', 'ADM', $step->acs_due_days_type, '', false, '', '', '', $suffix);
        $dueDaysType->setCustomCodes($daysTypes);
        $dueDaysType->setSuppressCodeDisplay();
        $dueDaysType->setWidth(100);
        $dueDaysType->setNoResize();
        
        $this->response->build('src/admin/actionchains/views/GetDueDateRow.php', array(
            'dueNo'       => $dueNo,
            'dueDaysType' => $dueDaysType
        ));
    }
    
    /**
    * Creates the Reminder row of the step form.
    */
    public function getReminderRow()
    {
        $factory  = new StepModelFactory();
        if ($this->request->getParameter('id') !== null)
        {
            $step = $factory->getMapper()->find($this->request->getParameter('id'));
            if (!isset($step))
            {
                throw new \URLNotFoundException();
            }
        }
        else
        {
            $step = $factory->getEntityFactory()->createObject();
        }
        
        $suffix = $this->request->getParameter('suffix');
        
        $daysTypes = array('CAL' => 'Calendar', 'WOR' => 'Working');
        
        $reminderNo = new \FormField();
        $reminderNo->MakeNumberField('acs_reminder_no_'.$suffix, $step->acs_reminder_no, 5, 0, '', 0);
        
        $reminderDaysType = \Forms_SelectFieldFactory::createSelectField('acs_reminder_days_type', 'ADM', $step->acs_reminder_days_type, '', false, '', '', '', $suffix);
        $reminderDaysType->setCustomCodes($daysTypes);
        $reminderDaysType->setSuppressCodeDisplay();
        $reminderDaysType->setWidth(100);
        $reminderDaysType->setNoResize();
        
        $this->response->build('src/admin/actionchains/views/GetReminderRow.php', array(
            'reminderNo'       => $reminderNo,
            'reminderDaysType' => $reminderDaysType,
        ));
    }
}