<script type="text/javascript">
/**
* Handles the expand/collapse of each step when clikcin toggle button.
* 
* @param object element The toggle button.
*/
function toggleStep(element)
{
    var $trigger = jQuery(element),
        $group = $trigger.closest('.toggle-group'),
        $target = $group.find('.toggle-target').first();

    if($trigger.data('render')) {

        $group.find("li.field_div").toggle();
        $target.data('rendered', true);
    }
}

/**
* Retrieves the current highest order number on the form 
* so we can automatically poplulate the order number when adding steps.
* 
* @return string
*/
function getMaxOrderNo()
{
    var max = 1;
    jQuery("input[id^=acs_order_]").each(function()
    {
        if (parseInt(jQuery(this).val()) > max)
        {
            max = parseInt(jQuery(this).val());   
        }
    });
    return "&maxorder=" + max;
}

/**
* Checks that the "after step" number is valid, 
* e.g. is lower than the current step number and exists on the form.
* 
* @param Object element The after step number field.
*/
function checkAfterStepNo(element)
{
    var stepExists = false,
        suffix = jQuery(element).attr("id").split("_")[4];
    
    if (parseInt(jQuery(element).val()) >= parseInt(jQuery("#acs_order_"+suffix).val()))
    {
        jQuery(element).val("");
        alert("The step number must be lower than the current step");
        return false;        
    }
    
    
    jQuery("input[id^=acs_order]").each(function()
    {
        if (jQuery(this).val() == jQuery(element).val())
        {
            stepExists = true;
            return false; // break out of loop   
        }    
    });
    
    if (!stepExists)
    {
        jQuery(element).val("");
        alert("The step number doesn't exist");
        return false;       
    }
    else
    {
        return true;
    }
}

/**
* When a step is removed, updates any other steps whose start date is based upon the removed step
* by changing it to the nearest available previous step defined on the form.
* 
* @param int stepNo The number of the step which has been removed.
*/
function updateStepNumbers(stepNo)
{
    if (stepNo != "")
    {
        var previousSteps = new Array();
        jQuery("input[id^=acs_order_]").each(function()
        {
            if (parseInt(jQuery(this).val()) < parseInt(stepNo))
            {
                previousSteps.push(jQuery(this).val());   
            }        
        });
        
        if (previousSteps.length > 0)
        {
            previousSteps.sort();
            var previousStep = previousSteps[previousSteps.length-1];
            jQuery("input[id^=acs_start_after_step_]").each(function()
            {
                if (jQuery(this).val() == stepNo)
                {
                    jQuery(this).val(previousStep);           
                }               
            });       
        }   
    }        
}

// force all fields to be "shown" as far as the validate script is concerned, 
// so mandatory fields are still flagged for collapsed steps
jQuery(document).ready(function(){jQuery("input[id^=show_field_]").val(1)});
</script>
<input type="hidden" id="step_max_suffix" name="step_max_suffix" value="<?php echo $this->maxSuffix ?>" />
<?php echo $this->steps ?>
<?php if ($this->formType != 'ReadOnly' && $this->formType != 'Print'): ?>
<li class="new_windowbg" id="add_step_button_list">
    <input type="button" id="step_section" value="Add step" onclick="AddSectionToForm('step', '', $('add_step_button_list'), 'ADM', getMaxOrderNo(), false, false, null);" />
</li>                 
<?php endif ?>