<style type=text/css>
    ol {_display: inline-block;}
    ol {_display: block;}
</style>
<script language="Javascript" type="text/javascript">
    AlertAfterChange = true;
    var submitClicked = false;
    jQuery(document).ready
    (function()
    {
    	var temp = mandatoryArray.pop();
    	mandatoryArray.unshift(mandatoryArray.pop());
    	mandatoryArray.unshift(temp);
    })
</script>
<form method="POST" id="actionchainform" name="actionchainform" action="app.php?action=saveactionchain">
    <input type="hidden" id="recordid" name="recordid" value="<?php echo (int) $this->recordid ?>" />
    <?php echo $this->Table->GetFormTable(); ?>
    <div class="button_wrapper">
        <input class="button" type="button" value="<?php echo _tk('action_chain_save_button'); ?>" onclick="submitClicked=true;if(validateSteps()){if(validateOnSubmit()){selectAllMultiCodes();jQuery('#actionchainform').submit();}}">
        <?php if (!empty($this->recordid)) : ?>
        <input class="button" type="button" value="<?php echo _tk('action_chain_delete_button'); ?>" onclick="if(confirm('<?php echo _tk('action_chain_delete_confirm'); ?>')){jQuery('#actionchainform').attr('action', 'app.php?action=deleteactionchain');jQuery('#actionchainform').submit();}">
        <?php endif; ?>
        <input class="button" type="button" value="<?php echo _tk('action_chain_cancel_button'); ?>" onclick="SendTo('app.php?action=listactionchains');">
    </div>
</form>