<?php
namespace src\admin\model;

use src\framework\model\Entity;

/**
 * Value object containing a value and a rating (GOOD, BAD).
 */
class SystemState extends Entity
{
    /**
     * @var string An HTML message describing any additional detail alongside the state. Used when displaying the result of the flag.
     */
    protected $message;

    /**
     * @var int A number representing the state of this particular aspect of the system.
     */
    protected $rating;

    const BAD = 0;
    const GOOD = 1;

    /**
     * Setter for the message property
     * @param $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Setter for the rating property
     * @param $rating
     * @throws \Exception if rating is not one of the contants defined for this class.
     */
    public function setRating($rating)
    {
        if (in_array($rating, [self::BAD, self::GOOD]))
        {
            $this->rating = $rating;
        }
        else
        {
            throw new \Exception ('PerformanceState::Rating cannot take value '.$rating);
        }
    }

    /**
     * Getter for message property
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Getter for rating property
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Shortcut to check for a positive state
     * @return bool
     */
    public function isGood()
    {
        return $this->getRating() == self::GOOD;
    }

    /**
     * Shortcut to check for a negative state
     * @return bool
     */
    public function isBad()
    {
        return $this->getRating() == self::BAD;
    }
}