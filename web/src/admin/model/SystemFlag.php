<?php
namespace src\admin\model;

use src\framework\model\Entity;

/**
 * An aspect of the system that can be checked for performance risk.
 */
abstract class SystemFlag extends Entity
{
    /**
     * @return SystemState
     *
     * Checks the state of an aspect of the system and returns a value object representing the state
     */
    public abstract function getState();

    /**
     * @return SystemState
     *
     * Gets a hard-coded title for this flag for use when displaying it.
     */
    public abstract function getTitle();

    /**
     * @return SystemState
     *
     * Gets a hard-coded description for this flag for use when displaying it.
     */
    public abstract function getDescription();

}