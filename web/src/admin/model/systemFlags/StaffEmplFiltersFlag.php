<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\admin\model\SystemState;
use src\framework\registry\Registry;

/**
 * Checks whether the STAFF_EMPL_FILTERS global is set
 */
class StaffEmplFiltersFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'STAFF_EMPL_FILTERS configuration option';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'If the STAFF_EMPL_FILTERS configuration option is not set to Y, the performance of the level 2 forms can suffer.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $performanceState = new SystemState();

        $globalValue = bYN(Registry::getInstance()->getParm('STAFF_EMPL_FILTERS', 'N', true));

        $performanceState->setRating(($globalValue ? SystemState::GOOD : SystemState::BAD));

        return $performanceState;
    }

}