<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks that there are no fields in Rich-Client groups (i.e. groups with id != 0).
 */
class ExtraFieldsInGroupsFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'Extra fields in a group other than 0.';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'Extra fields placed in groups in the Rich Client can cause unwanted behaviour when used in conjunction with DatixWeb.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        $state = new SystemState();

        $fieldIds = $this->getFieldIds();

        if (count($fieldIds) > 0)
        {
            $state->setRating(SystemState::BAD);

            $state->setMessage('There are '.count($fieldIds).' fields which appear in groups.');
        }
        else
        {
            $state->setRating(SystemState::GOOD);
        }

        return $state;
    }

    /**
     * @return array array of field ids which are linked to groups other than the DatixWeb default group 0
     */
    public function getFieldIds()
    {
        return \DatixDBQuery::PDO_fetch_all('SELECT distinct field_id FROM udf_links WHERE group_id != 0', [], \PDO::FETCH_COLUMN);
    }

}