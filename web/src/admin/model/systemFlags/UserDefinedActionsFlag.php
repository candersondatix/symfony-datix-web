<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemState;
use src\framework\registry\Registry;

/**
 * Checks for form designs with too many user defined actions on them.
 */
class UserDefinedActionsFlag extends FormDesignFlag
{
    /**
     * Limit chosen arbitrarily during initial development: can be updated as appropriate.
     */
    const LIMIT = 15;

    /**
     * @inherit
     */
    public function getTitle()
    {
        return 'Number of user defined field and section actions';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'Too many user defined field and section actions on a form design can impact performance. (Limit set at '.self::LIMIT.')';
    }

    /**
     * @inherit
     */
    public function getState()
    {
        $moduleDefs = Registry::getInstance()->getModuleDefs();
        $formDesigns = $this->getAllFormDesigns();

        //Set rating as good by default: alter to bad if we find a failing form design below.
        $performanceState = new SystemState();
        $performanceState->setRating(SystemState::GOOD);

        $flaggedDesigns = [];

        foreach ($formDesigns as $formDesign)
        {
            $numberOfActions = 0;

            foreach ($formDesign->ExpandSections as $actions)
            {
                $numberOfActions += count($actions);
            }
            foreach ($formDesign->ExpandFields as $actions)
            {
                $numberOfActions += count($actions);
            }

            if ($numberOfActions > self::LIMIT)
            {
                $performanceState->setRating(SystemState::BAD);
                //save array of links to form design in question
                $flaggedDesigns[$formDesign->Module][] =  '<a href="app.php?action=formdesignsetup&module='.$formDesign->Module.'&form_id='.$formDesign->ID.'&formlevel=2">'.$formDesign->ID.'</a>';
            }
        }

        if ($performanceState->isBad())
        {
            $message = 'Form IDs affected: ';

            foreach ($flaggedDesigns as $module => $links)
            {
                $message .= '<div>' . implode(',', $links) . ' (' . $moduleDefs[$module]['NAME'] . ')</div>';
            }

            $performanceState->setMessage($message);
        }

        return $performanceState;
    }

}