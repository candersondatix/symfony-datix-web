<?php
namespace src\admin\model\systemFlags;

use src\admin\model\SystemFlag;
use src\admin\model\SystemState;

/**
 * Checks whether the $logLevel global is set.
 */
class LogLevelFlag extends SystemFlag
{
    /**
     * @inherit
     */
    public function getTitle()
    {
        return '$logLevel debug setting';
    }

    /**
     * @inherit
     */
    public function getDescription()
    {
        return 'The $logLevel DatixConfig.php setting should only be given a value if an investigation is underway.';
    }


    /**
     * @inherit
     */
    public function getState()
    {
        global $logLevel;

        $performanceState = new SystemState();

        $performanceState->setRating((isset($logLevel) ? SystemState::BAD : SystemState::GOOD));

        return $performanceState;
    }

}