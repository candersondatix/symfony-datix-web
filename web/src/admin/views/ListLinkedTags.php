<style type="text/css">
    div.ricoLG_cell {
        white-space:nowrap;
    }
</style>
<?php

require "rico/applib.php";
require "rico/plugins/php/ricoLiveGridForms.php";

if (OpenGridForm("", 'code_tags', 'group_id = '.\Sanitize::SanitizeInt($this->group)))
{
    \src\admin\controllers\CodeTagGroupController::DefineFields($this->group);
}
else
{
    echo 'open failed';
}
CloseApp();