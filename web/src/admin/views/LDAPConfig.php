<form method="post" action="<?php echo $this->scripturl; ?>?action=ldapconfig">
    <a name="datix-content" id="datix-content"></a>
    <div class="external_border">
        <div class="internal_border">
            <ol>
                <li name="_title_row" id="_title_row" class="section_title_row">
                    <b><?php echo _tk('ldap_configuration'); ?></b>
                </li>
                <li class="field_div" name="_row" id="_row">
                    <div class="field_label_div" style="width:25%">
                        <b><?php echo _tk('ldap_username'); ?></b>
                    </div>
                    <div class="field_input_div" style="width:70%; word-wrap: break-word;">
                        <input id="ldap_user" name="ldap_user" type="text" value="<?php echo $this->username; ?>" size="50">
                    </div>
                </li>
                <li class="field_div" name="_row" id="_row">
                    <div class="field_label_div" style="width:25%">
                        <b><?php echo _tk('ldap_password'); ?></b>
                    </div>
                    <div class="field_input_div" style="width:70%; word-wrap: break-word;">
                        <input id="ldap_password" name="ldap_password" type="password" size="50">
                    </div>
                </li>
                <li class="field_div" name="ldap_all_row" id="_row">
                    <div class="field_label_div" style="width:25%">
                        <b>What should sync via LDAP?</b>
                    </div>
                    <div class="field_input_div" style="width:70%; word-wrap: break-word;">
                        <?php echo $this->LDAPTypeFieldObj->GetField(); ?>
                    </div>
                </li>
                <?php if(GetParm('LDAP_TYPE', 'GROUPS') == 'USERS'): ?>
                <li class="field_div" name="ldap_all_row" id="_row">
                    <div class="field_label_div" style="width:25%">
                        <b>Choose AD groups to sync with</b>
                    </div>
                    <div class="field_input_div" style="width:70%; word-wrap: break-word;">
                        <?php echo $this->ADFieldObj->GetField(); ?>
                    </div>
                </li>
                <?php endif; ?>
            </ol>
        </div>
    </div>
    <div class="button_wrapper">
        <input type="submit" value="<?php echo _tk('ldap_btn_save'); ?>" onclick="jQuery('#ldap_dn').each(function(){jQuery('#ldap_dn option').attr('selected','selected');});">
    </div>
</form>