<table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
    <?php if (count($this->users) == 0): ?>
        <tr>
            <td class="windowbg2"><b><?php echo _tk('no_reg_requests_found') ?></b></td>
        </tr>
    <?php else: ?>
        <tr class="tableHeader">
            <th class="windowbg"><?php echo _tk('user_name_header') ?></th>
            <th class="windowbg"><?php echo _tk('full_name_header') ?></th>
            <th class="windowbg"><?php echo _tk('e_mail_addr_header') ?></th>
        </tr>

        <?php foreach ($this->users as $user): ?>
            <?php $link = $this->scripturl.'?action=viewregistrationform&recordid='.$user->recordid; ?>

            <tr>
                <td class="windowbg2"><a href="<?php echo $link ?>"><?php echo htmlspecialchars($user->login) ?></a></td>
                <td class="windowbg2"><a href="<?php echo $link ?>"><?php echo htmlspecialchars($user->fullname) ?></a></td>
                <td class="windowbg2"><a href="<?php echo $link ?>"><?php echo htmlspecialchars($user->con_email) ?></a></td>
            </tr>

        <?php endforeach ?>
    <?php endif ?>
</table>