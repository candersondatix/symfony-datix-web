<?php $addMinExtension = ($GLOBALS['MinifierDisabled'] ? '' : '.min'); ?>
<link type="text/css" rel="stylesheet" href="js_functions/qTip/jQuery.qtip<?php echo $addMinExtension; ?>.css" />
<script type="text/javascript" src="js_functions/qTip/jQuery.qtip<?php echo $addMinExtension; ?>.js"></script>
<script type="text/javascript">
jQuery(function( $ ) {

    $('#con-search, #login-search').on('keyup', function() {

        var $target = $(this),
            limit = 4;

        if( ! $.isNumeric($target.data('hasqtip')) && $target.val().length > 0 && $target.val().length < limit) {

            $target.qtip({
                content: 'Providing fewer characters may result in long wait times as there could be a large number of results',
                show: {
                    event: false,
                    ready: true,
                    solo: true
                },
                hide: false,
                style: {
                    classes: 'qtip-blue'
                },
                position: {
                    my: 'left center',
                    at: 'right center'
                }
            });
        }
        else if($.isNumeric($target.data('hasqtip')) && $target.val().length > 0 && $target.val().length < limit)
        {
            $target.qtip('show');
        }
        else
        {
            $target.qtip('hide');
        }
    });
});
</script>
<style type="text/css">
.userSearchForm {
    margin-top: 10px;
    margin-bottom: 10px;
}
table.userSearchTable {
    border: 0;
}
table.userSearchTable td {
    border: 0;
}
table.userSearchTable td.buttonCell {
    text-align: right;
}
</style>
<?php
//Remove the list of alphabetical links when we're in the process of searching for a user. CA
// @see https://datixltd.atlassian.net/browse/DW-11196
if(!$this->usersearch) :
?>
<div class="page-numbers">
    <!-- This bit of PHP code needs to be like this because of the spacing between letters on the HTML output -->
    <?php
    for ($a = ord('A'); $a <= ord('Z'); $a++)
    {
        echo '<a href="' . $this->scripturl . '?action=listusers&amp;match='
            . chr($a) . '&amp;field=' . $this->MatchField . '&amp;sort=' . $this->sorttype . '">';
        echo chr($a);

        echo '</a>&nbsp;&nbsp;';
    }
    ?>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo $this->scripturl; ?>?action=listusers&amp;match=all&amp;sort=<?php echo $this->sorttype; ?>">All</a>
</div>
<?php endif; ?>

<div class="userSearchForm">
    <form action="app.php?action=listusers&usersearch=1" method="post">
        <table class="userSearchTable" align="center">
            <tr>
                <td><?php echo Labels_FieldLabel::GetFieldLabel('login'); ?>:</td>
                <td><input id="login-search" name="login" type="string" /></td>
            </tr>
            <tr>
                <td><?php echo Labels_FieldLabel::GetFieldLabel('con_surname'); ?>:</td>
                <td><input id="con-search" name="con_surname" type="string" /></td>
            </tr>
            <tr>
                <td colspan="2" class="buttonCell"><input type="button" value="Search" onclick="this.form.submit()" /></td>
            </tr>
        </table>
    </form>
</div>