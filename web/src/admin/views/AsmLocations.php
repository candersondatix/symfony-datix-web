<div id="location-hierarchy" class="columnview">
    <form method="get" action="<?php echo $this->scripturl ?>" class="search-form">
        <input type="hidden" name="action" value="asmshowsearchlocation">
        <input type="text" name="location">
        <button type="submit">Search</button>
    </form>
    <div class="viewport">
        <ul id="tiers" class="tiers">
        <?php foreach($this->tiers as $tier): ?>
            <li class="">
                <span title="Double-click to edit"><?php echo $tier['lti_name'] ?></span>
                <a href="#" class="new-node">Add</a>
                <?php if ($tier['deletable']): ?>
                    <a href="#" class="delete-tier">Delete tier</a>
                <?php endif ?>
            </li>
        <?php endforeach ?>
        </ul>
        <div id="locations" class="columns"></div>
    </div>
    <div id="node-form">
        <form action="index_submit" method="post" accept-charset="utf-8">
            <input type="hidden" id="recordid" name="recordid">
            <input type="hidden" id="parent-id" name="parent_id">
            <div class="external_border">
                <div class="internal_border">
                    <ol>
                        <li class="section_title_row">
                            <div class="section_title">Location details</div>
                        </li>
                        <li class="field_div">
                            <div class="field_label_div">
                                <label for="parent">Parent</label>
                            </div>
                            <div class="field_input_div">
                                <input id="parent" type="text" readonly disabled="disabled">
                            </div>
                        </li>
                        <li class="field_div">
                            <div class="field_label_div">
                                <label for="name"><img alt="Mandatory" src="images/Warning.gif"/> Name</label>
                            </div>
                            <div class="field_input_div">
                                <input type="text" id="name" name="loc_name" maxlength="50">
                            </div>
                        </li>
                        <li class="field_div">
                            <div class="field_label_div">
                                <label for="location-type">Location type</label>
                            </div>
                            <div class="field_input_div">
                                <select id="location-type" name="loc_type">
                                    <?php foreach($this->locTypes as $locType): ?>
                                        <option value="<?php echo $locType['cod_code'] ?>"><?php echo $locType['cod_descr']?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </li>
                        <li class="field_div">
                            <div class="field_label_div">
                                <label for="address">Address</label>
                            </div>
                            <div class="field_input_div">
                                <textarea id="address" name="loc_address"></textarea>
                            </div>
                        </li>
                        <li class="field_div">
                            <div class="field_label_div">
                                <label for="active">Active?</label>
                            </div>
                            <div class="field_label_div">
                                <select id="active" name="loc_active">
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>
                                </select>
                                <span id="info_active" style="display: none;">
                                	<p>Deactivated by ancestor</p>
                                </span>
                            </div>
                        </li>
                        <li class="button_wrapper">
                            <button type="submit">Save</button>
                            <button type="button" class="cancel">Cancel</button>
                        </li>
                    </ol>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="js_functions/jquery/jquery.datix.columnview<?php echo $this->addMinExtension; ?>.js" type="text/javascript"></script>
<script src="js_functions/jquery/jquery.qtip-1.0.0-rc3<?php echo $this->addMinExtension; ?>.js" type="text/javascript"></script>
<script src="js_functions/location-hierarchy<?php echo $this->addMinExtension; ?>.js" type="text/javascript"></script>
<script type="text/javascript">
    LocationHierarchy.init(document.getElementById('location-hierarchy'), <?php echo $this->data ?> <?php echo ($this->selectedLocation) ? ', ' . $this->selectedLocation : '' ?>);
</script>