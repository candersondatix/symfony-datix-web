<?php require_once 'Source/libs/ListingClass.php'; ?>
<?php foreach ($this->ModuleDefs as $ModuleCode => $ModuleDetails) : ?>
    <?php $ListingDesigns = DatixDBQuery::PDO_fetch_all($this->SelectSql, array('module' => $ModuleCode)); ?>
    <div id="panel-<?php echo $ModuleCode; ?>" class="panel"<?php echo ($ModuleCode != $this->module ? ' style="display:none"' : ''); ?>">
        <div class="section_title_row row_above_table">
            <b>
                <?php echo \UnicodeString::ucfirst($this->ModuleDefs[$ModuleCode]['REC_NAME']).' listings:'; ?>
            </b>
        </div>
        <div class="new_windowbg">
            <table class="gridlines" border="0" width="100%" cellspacing="1" cellpadding="4">
                <tr>
                    <td class="windowbg" width="2%" nowrap="nowrap">0</td>
                    <td class="windowbg" width="30%">
                        <a href="<?php echo $this->scripturl; ?>?action=editcolumns&amp;form_id=0&amp;module=<?php echo $ModuleCode; ?>">Datix Listing Design Template</a>
                        <?php if (Listing::GetGlobalDefaultListing($ModuleCode) == '0') : ?>
                            [default listing]
                        <?php endif; ?>
                    </td>
                    <td class="windowbg"></td>
                </tr>
                <?php foreach ($ListingDesigns as $Details) : ?>
                    <tr>
                        <td class="windowbg2" width="2%" nowrap="nowrap"><?php echo $Details['recordid']; ?></td>
                        <td class="windowbg2" width="30%">
                            <a href="<?php echo $this->scripturl; ?>?action=editcolumns&amp;form_id=<?php echo $Details['recordid']; ?>&amp;module=<?php echo $ModuleCode; ?>"><?php echo htmlspecialchars($Details['LST_TITLE']); ?></a>
                            <?php if (Listing::GetGlobalDefaultListing($ModuleCode) == (string)$Details['recordid']) : ?>
                                [default listing]
                            <?php endif; ?>
                        </td>
                        <td class="windowbg2"></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="new_link_row">
            <a href="<?php echo $this->scripturl; ?>?action=newlistingdesign&module=<?php echo $ModuleCode; ?>"><b>Design a new <?php echo $this->ModuleDefs[$ModuleCode]['REC_NAME']; ?> listing</b></a>
        </div>
    </div>
<?php endforeach; ?>
</form>