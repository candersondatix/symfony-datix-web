<form enctype="multipart/form-data" method="post" name="frmDocument" action="<?php echo $this->scripturl; ?>?action=import">
    <input type="hidden" name="rbWhat" value="import" />
    <?php if ($this->data['error']) : ?>
        <div class="form_error">The form you submitted contained the following errors.  Please correct them and try again.</div>
        <div class="form_error"><?php echo $this->data['error']['message']; ?></div>
    <?php endif; ?>
    <tr>
        <td><?php echo $this->CTable->GetFormTable(); ?></td>
    </tr>
    <div class="button_wrapper">
        <input type="submit" value="<?php echo _tk('btn_import')?>" onclick="document.frmDocument.rbWhat.value='save'" />
        <input type="submit" value="<?php echo _tk('btn_cancel')?>" onclick="document.frmDocument.rbWhat.value='cancel'" />
    </div>
</form>