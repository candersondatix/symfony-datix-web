<table>
<?php foreach ($this->flags as $flag): ?>
  <tr>
    <td>
    <div><b><?php echo $flag->getTitle(); ?></b></div>
    <div><?php echo $flag->getDescription(); ?></div>
    </td>
    <td style="background-color: <?php echo ($flag->getState()->isGood() ? '#008800' : '#D95252'); ?>">
      <?php echo ($flag->getState()->isGood() ? 'PASS' : 'FAIL'); ?>
    </td>
    <td>
      <?php echo $flag->getState()->getMessage(); ?>
    </td>
  </tr>
<?php endforeach; ?>
</table>