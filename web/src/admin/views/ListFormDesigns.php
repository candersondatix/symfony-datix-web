<?php foreach ($this->ModuleDefs as $ModuleCode => $ModuleDetails) : ?>
    <?php
        $ModuleTitle = $ModuleDetails['NAME'];
        $FormFiles = "";
        $FormFiles2 = "";

        $FormsFileName = GetFormsFilename($ModuleCode);

        if (file_exists($FormsFileName))
        {
            include($FormsFileName);
        }

        if (!is_array($FormFiles))
        {
            if (is_array($this->ModuleDefs[$ModuleCode]["FORMS"][1]))
            {
                $Code = GetFormCode(array('module' => $ModuleCode, 'level' => 1));
                $FormFiles = array(0 => "Default {$this->ModuleDefs[$ModuleCode]['REC_NAME']}".($Code ? ' ('.$Code.')' : '')." form");
            }
        }

        if (!is_array($FormFiles2))
        {
            if (is_array($this->ModuleDefs[$ModuleCode]["FORMS"][2]))
            {
                $Code = GetFormCode(array('module' => $ModuleCode, 'level' => 2));
                $FormFiles2 = array(0 => "Default {$this->ModuleDefs[$ModuleCode]['REC_NAME']}".($Code ? ' ('.$Code.')' : '')." form");
            }
        }
    ?>
        <div id="panel-<?php echo $ModuleCode; ?>" class="panel" style="display:<?php echo ($ModuleCode == $this->CurrentModule ? 'block' : 'none'); ?>">
            <?php if (is_array($this->ModuleDefs[$ModuleCode]["FORMS"][1])) : ?>
                <?php $Code = GetFormCode(array('module' => $ModuleCode, 'level' => 1)); ?>
                <!--   DIF1  -->
                <div class="section_title_row row_above_table">
                    <b>
                        <?php
                            if ($this->ModuleDefs[$ModuleCode]["FORMS"][1]["TITLE"])
                            {
                                echo $this->ModuleDefs[$ModuleCode]["FORMS"][1]["TITLE"] . ($Code ? ' ('.$Code.')' : '');
                            }
                            else
                            {
                                echo $ModuleTitle . ' forms' . ($Code ? ' ('.$Code.')' : '');
                            }
                        ?>
                    </b>
                </div>
                <div class="new_windowbg">
                    <table class="gridlines" border="0" width="100%" cellspacing="1" cellpadding="4">
                        <?php if ($FormFiles) : ?>
                            <?php
                                if ($this->ModuleDefs[$ModuleCode]["FORMS"][1]["CODE"] != "")
                                {
                                    $sL2FormDesignID = Forms_FormDesign::getGlobalFormDesignID($ModuleCode, 1);
                                }
                                else
                                {
                                    $sL2FormDesignID = 0;
                                }
                            ?>
                                <?php foreach ($FormFiles as $Number => $Name) : ?>
                                    <tr>
                                        <td class="windowbg2" width="2%" nowrap="nowrap">
                                            <?php echo $Number; ?>
                                        </td>
                                        <td class="windowbg2" width="30%">
                                            <a href="<?php echo $this->scripturl; ?>?action=formdesignsetup&amp;form_id=<?php echo $Number; ?>&amp;module=<?php echo $ModuleCode; ?>"><?php echo htmlspecialchars($Name); ?></a>
                                            <?php if($sL2FormDesignID == (string)$Number) : ?>
                                                [form in current use]
                                            <?php endif; ?>
                                        </td>
                                        <td class="windowbg2">
                                            <?php if($this->ModuleDefs[$ModuleCode]['LOGGED_OUT_LEVEL1']) : ?>
                                                <?php echo $this->scripturl; ?>?form_id=<?php echo $Number; ?>&amp;module=<?php echo $ModuleCode; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                </div>
                <div class="new_link_row">
                    <a href="<?php echo $this->scripturl; ?>?action=newformdesign&module=<?php echo $ModuleCode; ?>"><b><?php echo ($this->ModuleDefs[$ModuleCode]["FORMS"][1]['LINK_TEXT'] ? $this->ModuleDefs[$ModuleCode]["FORMS"][1]['LINK_TEXT'] : 'Design a new '.($Code ? $Code : $ModuleTitle).' form'); ?></b></a>
                </div>
            <?php endif; ?>
            <!--   DIF2  -->
            <?php if(is_array($this->ModuleDefs[$ModuleCode]["FORMS"][2])) : ?>
                <?php
                    $Code = GetFormCode(array('module' => $ModuleCode, 'level' => 2));
                    $sL2FormDesignID = Forms_FormDesign::getGlobalFormDesignID($ModuleCode, 2);
                ?>
                <div class="section_title_row row_above_table">
                    <b>
                        <?php
                        if($this->ModuleDefs[$ModuleCode]["FORMS"][2]["TITLE"])
                        {
                            echo $this->ModuleDefs[$ModuleCode]["FORMS"][2]["TITLE"] . ($Code ? ' ('.$Code.')' : '');
                        }
                        else
                        {
                            echo $ModuleTitle . ' forms' . ($Code ? ' ('.$Code.')' : '');
                        }
                        ?>
                    </b>
                </div>
                <div class="new_windowbg">
                    <table class="gridlines" border="0" width="100%" cellspacing="1" cellpadding="4">
                        <?php if ($FormFiles2) : ?>
                            <?php foreach ($FormFiles2 as $Number => $Name) : ?>
                                <tr>
                                    <td class="windowbg2" width="2%" nowrap="nowrap"><?= $Number ?></td>
                                    <td class="windowbg2" width="30%">
                                        <a href="<?php echo $this->scripturl; ?>?action=formdesignsetup&amp;form_id=<?php echo $Number; ?>&amp;module=<?php echo $ModuleCode; ?>&amp;formlevel=2"><?php echo $Name; ?></a>
                                        <?php if ($sL2FormDesignID == (string)$Number) : ?>
                                            [form in current use]
                                        <?php endif; ?>
                                    </td>
                                    <td class="windowbg2"></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                </div>
                <div class="new_link_row">
                    <a href="<?php echo $this->scripturl; ?>?action=newformdesign&module=<?php echo $ModuleCode; ?>&formlevel=2"><b><?php echo ($this->ModuleDefs[$ModuleCode]["FORMS"][2]['LINK_TEXT'] ? $this->ModuleDefs[$ModuleCode]["FORMS"][2]['LINK_TEXT'] : 'Design a new '.($Code ? $Code : $ModuleTitle).' form'); ?></b></a>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</form>