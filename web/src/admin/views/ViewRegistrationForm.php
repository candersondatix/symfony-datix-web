<script type="text/javascript" language="javascript">
    <?php echo MakeJavaScriptValidation('CON'); ?>
    var submitClicked = false;
</script>
<form method="post" name="frmContact" action="<?php echo $this->scripturl; ?>?action=approveuserregistration" onsubmit="return(submitClicked && validateOnSubmit())" >
    <input type="hidden" id="recordid" name="recordid" value="<?php echo Sanitize::SanitizeInt($this->recordid); ?>">
    <?php
    $this->ConTable->MakeTable();
    echo $this->ConTable->GetFormTable();
    ?>
    <div class="button_wrapper">
        <input type="hidden" id="rbWhat" name="rbWhat" value="add">
        <input type="button" id="btnSave" value="<?php echo _tk('btn_submit_reg_review'); ?>" onclick="submitClicked = true; if (validateOnSubmit()) { this.form.submit(); }" />
        <input type="button" id="btnCancel" value="<?php echo _tk('btn_cancel'); ?>" onclick="SendTo('app.php?action=listawaitingapproval');" />
    </div>
</form>