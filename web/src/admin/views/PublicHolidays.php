<?php

    if ($this->result) {
        echo '<p class="ricoFormResponse ' . $this->action . 'Successfully"></p>';
    } else {
        echo '<p class="error">' . $this->errorMsg . '</p>';
    }

?>
<p class="ricoBookmark"><span id="holidays_bookmark"> </span></p>
<span class="ricoSaveMsg" id="holidays_savemsg"></span>
<table id="holidays" style="width: 600px">
	<tr>
		<th><?php echo _tk('date') ?></th>
		<th><?php echo _tk('holiday') ?></th>
	</tr>
</table>
<div class="button_wrapper">
	<form method="POST" action="app.php?action=publicholidays">
    	<button type="submit" name="holidays__action" value="back"><?php echo _tk('back') ?></button>
	</form>
</div>
<script type="text/javascript">var publicHolidays = <?php echo json_encode($this->jsSettings); ?>;</script>
<script type="text/javascript" src="js_functions/public-holidays<?php echo $this->addMinExtension; ?>.js"></script>