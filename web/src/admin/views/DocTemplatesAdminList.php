<?php if ($this->message) : ?>
    <div class="form_error_wrapper">
        <div class="form_error"><?php echo $this->message; ?></div>
    </div>
<?php endif; ?>
<div class="new_titlebg"><div class="title_text_wrapper"><b>Module:</b></div>
    <div class="title_select_wrapper" style="padding:2px">
        <form name="doctemplatelist" method="post" action="<?php echo $this->scripturl; ?>?action=doctemplateadminlist">
            <input type="hidden" id="module" name="module" value="<?php echo $this->module; ?>" />
            <?php echo $this->ModuleDropDownHTML; ?>
        </form>
    </div>
</div>
<form method="post" name="editdoctemplate" action="<?php echo $this->scripturl; ?>?action=doctemplateaction">
    <input type="hidden" name="form_action" value="edit" />
    <input type="hidden" name="module" value="<?php echo $this->module; ?>" />
    <input type="hidden" name="tem_doc_id" value="" />
    <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
        <tr class="tableHeader">
            <th width="68%" align="left"><a href="<?php echo $this->scripturl; ?>?action=doctemplateadminlist&amp;module=<?php echo $this->module; ?>&amp;orderby=tem_notes&order=<?php echo ($this->order == 'ASC' ? 'DESC' : "ASC"); ?>" class="<?php echo ($this->orderby == 'tem_notes' ? 'sort ' . \UnicodeString::strtolower($this->order) : ''); ?>">Name</a></th>
            <th width="20%" align="left"><a href="<?php echo $this->scripturl; ?>?action=doctemplateadminlist&amp;module=<?php echo $this->module; ?>&amp;orderby=tem_type&order=<?php echo ($this->order == 'ASC' ? 'DESC' : "ASC"); ?>" class="<?php echo ($this->orderby == 'tem_type' ? 'sort ' . \UnicodeString::strtolower($this->order) : ''); ?>">Type</a></th>
            <th width="6%" align="left"><b></b></th>
            <th width="6%" align="left"><b></b></th>
        </tr>
        <?php foreach($this->resultArray as $row) : ?>
            <?php if ($this->orderby && (($this->LastGroup != $row->tem_type)) && ($this->orderby == 'tem_type')) : ?>
            <tr>
                <td class="titlebg" colspan="4">
                    <b><?php echo $this->TemplateType[$row->tem_type]; ?></b>
                </td>
            </tr>
            <?php endif; ?>
            <?php $this->LastGroup = $row->tem_type; ?>
            <tr class="listing-row">
                <?php $repurl = $this->scripturl . '?action=doctemplateaction&amp;tem_doc_id='.Sanitize::SanitizeInt($row->recordid).'&amp;module='.$this->module; ?>
                <td class="windowbg2 <?php echo ($this->orderby == 'tem_notes' ? 'sort' : ''); ?>"><a href="<?php echo $repurl; ?>&amp;form_action=edit"><?php echo htmlspecialchars($row->tem_notes); ?></a></td>
                <td class="windowbg2 <?php echo ($this->orderby == 'tem_type' ? 'sort' : ''); ?>"><?php echo $this->TemplateType[$row->tem_type]; ?></td>
                <td class="windowbg2" valign="right" border="0">
                    <a href="<?php echo $repurl; ?>&amp;form_action=edit"> [edit]</a>
                </td>
                <td class="windowbg2" valign="right" border="0">
                    <a href="javascript:if(confirm('<?php echo _tk('delete_template_q'); ?>')){document.forms[1].form_action.value='delete';document.forms[1].tem_doc_id.value=<?php echo $row->recordid; ?>;document.forms[1].submit();}"> [delete]</a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if (count($this->resultArray) == 0) : ?>
            <tr>
                <td class="rowtitlebg" colspan="5">
                    <b>No templates.</b>
                </td>
            </tr>
        <?php endif; ?>
    </table>
</form>
<script language="JavaScript" type="text/javascript">
    function showModule(selectedModule)
    {
        var module = $("lbModule").value;
        setQueryStringParam("module", module);
    }
</script>
