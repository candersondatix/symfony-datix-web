<script language="javascript" type="text/javascript">
    // TODO - somehow doubt this is useful anymore, so should probably be removed
    var NS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) < 5);

	var optionsChanged = false;
	var previousModule = '<?php echo $this->module?>';
	
    function addOption(theSel, theText, theValue)
    {
        var newOpt = new Option(theText, theValue);
        var selLength = theSel.length;
        theSel.options[selLength] = newOpt;
    }

    function deleteOption(theSel, theIndex)
    {
        var selLength = theSel.length;

        if (selLength > 0)
        {
            theSel.options[theIndex] = null;
        }
    }

    function moveOptions(theSelFrom, theSelTo)
    {
        var selLength = theSelFrom.length,
            selectedText = new Array(),
            selectedValues = new Array(),
            selectedCount = 0,
            i,
            moveit = true;
        optionsChanged = true;

        for (i = selLength-1; i >= 0; i--)
        {
            if (theSelFrom.options[i].selected)
            {
                if (theSelFrom.options[i].style.color == 'red')
                {
                    alert('This column is mandatory, cannot be removed.');
                    moveit = false;
                }
                else
                {
                    selectedText[selectedCount] = theSelFrom.options[i].text;
                    selectedValues[selectedCount] = theSelFrom.options[i].value;
                    deleteOption(theSelFrom, i);
                }

                selectedCount++;
            }
        }

        for (i = selectedCount-1; i >= 0; i--)
        {
            if (moveit)
            {
                addOption(theSelTo, selectedText[i], selectedValues[i]);
            }
        }

        sortListBox(theSelTo);

        if (NS4)
        {
            history.go(0);
        }
    }

    function showColumns(selectedModule)
    {
        document.getElementById("module").value = document.getElementById("lbModule").value;
    }

    function saveForm()
    {
    	selectAllMultiple(document.getElementById('columns_list2')); 
    	document.forms.editcol.rbWhat.value='Save';
    }

    function saveChanges()
    {
        if(optionsChanged)
        {
    		if(window.confirm("<?php echo $this->confirm_clear ?>"))
			{
    			showColumns(this); 
				document.editcolmodule.submit();
				optionsChanged = false;
    		}
    		else
    		{
    			changeModuleValue(previousModule);
    		}
        }
        else
        {
        	showColumns(this); 
			document.editcolmodule.submit();
        }
    }
    
   function changeModuleValue(module)
	{
    	var description = '';

    	jQuery.each(customCodes['lbModule'], function(key,value) {

        	if (value['value'] == module) {
            	description = value['description'];
            	return;
        	}
    	});

    	jQuery('#lbModule_title').selectItem('<li id="'+module+'">'+description+'</li>');
	}

    jQuery(document).ready(function() {

        jQuery('#lbModule_title').change(function() {

            saveChanges();
        });
    });
</script>
<form name="editcolmodule" method="post" action="<?php echo $this->scripturl ; ?>?action=excludebatchupdate">
    <input type="hidden" id="module" name="module" value="<?php echo $this->module; ?>" />
    <table class="gridlines" align="center" border="0" cellpadding="4" cellspacing="1" width="100%">
        <tr>
            <td class="titlebg" colspan="2"><span style="float:left;margin-right:5px;margin-top:5px;"><b>Module:</b></span>
                <?php echo $this->ModuleDropDownHTML; ?>
            </td>
        </tr>
    </table>
</form>
<form name="editcol" method="post" action="<?php echo $this->scripturl; ?>?action=saveexcludebatchupdate">
    <input type="hidden" id="module" name="module" value="<?php echo $this->module; ?>" />
    <input type="hidden" id="rbWhat" name="rbWhat" value="Preview" />
    <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
        <tr>
            <td class="windowbg2" align="left">
                <b>Available fields for batch update:</b><br/>
                Use this facility to identify which fields should appear in batch update.
            </td>
            <td class="windowbg2" align="left"></td>
            <td class="windowbg2" align="left">
                <b>Excluded fields:</b><br />
                The list of fields below will not be available.
            </td>
        </tr>
        <tr>
            <td class="windowbg2" align="center">
                <select id="columns_list" name="columns_list[]" multiple="multiple" size="12" id="columns_list"style="width: 300px">
                    <?php foreach ($this->availCols as $Code => $Val) : ?>
                        <?php if (!array_key_exists($Code, $this->currentCols) && $Val != '') : ?>
                            <option value="<?php echo $Code; ?>"><?php echo $Val; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            </td>
            <td class="windowbg2" align="center">
                <input type="button" value="Exclude >>>" onclick="moveOptions(this.form.columns_list, this.form.columns_list2);" /><br /><br />
                <input type="button" value="<<< Include" onclick="moveOptions(this.form.columns_list2, this.form.columns_list);" />
            </td>
            <td class="windowbg2" align="center">
                <table>
                    <tr>
                        <td></td>
                        <td>
                            <select id="columns_list2" name="columns_list2[]" multiple="multiple" size="12" id="columns_list2" style="width: 300px">
                                <?php foreach($this->currentCols as $Code => $Val) : ?>
                                    <option value="<?php echo $Code; ?>"><?php echo $Val; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="windowbg2" align="center"></td>
            <td class="windowbg2" align="center">
                <input type="submit" value="Save" onclick="saveForm();" />
            </td>
            <td class="windowbg2" align="left"></td>
        </tr>
    </table>
</form>