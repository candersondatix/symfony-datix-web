<script language="JavaScript" type="text/javascript">
    function dnLDAPUserSyncAllPopup()
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?php $this->scripturl ?>?action=ldapusersyncall&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }

    function dnLDAPUserSyncExistingPopup()
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?php $this->scripturl ?>?action=ldapusersyncall&updateonly=1&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }
</script>
<?php
//Remove the list of alphabetical links when we're in the process of searching for a user. CA
    // @see https://datixltd.atlassian.net/browse/DW-11196
    if(!$this->usersearch) :
    ?>
    <div class="page-numbers">
        <!-- This bit of PHP code needs to be like this because of the spacing between letters on the HTML output -->
        <?php
        for ($a = ord('A'); $a <= ord('Z'); $a++)
        {
            echo '<a href="' . $this->scripturl . '?action=listusers&amp;match='
                . chr($a) . '&amp;field=' . $this->MatchField . '&amp;sort=' . $this->sorttype . '">';
            if (ord($this->MatchLetter) == $a)
            {
                echo "<b>[" . chr($a) . "]</b>";
            }
            else
            {
                echo chr($a);
            }
            echo '</a>&nbsp;&nbsp;';
        }
        ?>
        &nbsp;&nbsp;&nbsp;
        <?php if ($this->MatchLetter == 'all') : ?>
            <b>[
        <?php endif; ?>
        <a href="<?php echo $this->scripturl; ?>?action=listusers&amp;match=all&amp;sort=<?php echo $this->sorttype; ?>">All</a>
        <?php if ($this->MatchLetter == 'all') : ?>
            ]</b>
        <?php endif; ?>
    </div>
<?php endif; ?>

<form id="usersearchform" action="app.php?action=listusers&usersearch=1" method="post">
    <input type="hidden" name="login" value="<?php echo $this->login ?>">
    <input type="hidden" name="con_surname" value="<?php echo $this->con_surname ?>">
</form>
<?php echo $this->ListingDisplay->GetListingHTML(); ?>

<?php if ($this->ldap->LDAPEnabled() && IsFullAdmin() && GetParm('LDAP_TYPE', 'GROUPS') == 'USERS' && $this->numLDAPUsers > 0): ?>
    <div style="padding-top: 5px;padding-left: 5px"><a href="javascript:dnLDAPUserSyncAllPopup()" ><?php echo _tk('ldap_sync_all_users') ?></a></div>
    <div style="padding: 5px"><a href="javascript:dnLDAPUserSyncExistingPopup()" ><?php echo _tk('ldap_sync_existing_users') ?></a></div>
<?php endif; ?>