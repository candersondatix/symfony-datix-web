<?php
require_once 'rico/applib.php';
require_once 'rico/plugins/php/ricoLiveGridForms.php';
?>

<?php
    if (OpenGridForm('', $this->table))
    {
        \src\admin\controllers\RiskMatrixSetupTemplateController::DefineFields();
    }
    else
    {
        echo 'open failed';
    }

    CloseApp();
?>


        <li>
            <div class="button_wrapper">
                <input type="button" value="Generate" border="10" class="button" name="btnGenerate" onClick="generate()">
                <input type="button" value="Back" border="10" class="button" name="btnBack" onClick="SendTo('<?php echo $this->scripturl ?>?action=codesetupslist');">
            </div>
        </li>
    </ol>
</form>
 
<script type="text/javascript">
function generate()
{
    displayLoadPopup();
    jQuery.get(scripturl+"?action=httprequest&type=generateriskmatrix&responsetype=json", function()
    {
        hideLoadPopup();
        location.reload(true);
    });
}
</script>
 