<script language="Javascript">
AlertAfterChange=true;
var submitClicked = false;

jQuery(document).ready(function()
{
	jQuery("#fld_length").blur(function()
	{
		if (jQuery(this).val() != "" && (jQuery(this).val() < 0 || jQuery(this).val() > 254))
		{
			alert("<?php echo $this->udfLengthAlert ?>");
            if(jQuery(this).val() < 0) {

                jQuery(this).val(0);
            }
            else if(jQuery(this).val() > 254) {

                jQuery(this).val(254);
            }
		}
	});
});
</script>
<style type="text/css">
    #fld_code_like_module_row label, #fld_code_like_row label, #code_setup_row .field_label_div a, #profile_select_row label, #map_fld_link_row label, #profile_select_row .field_extra_text
    {
        padding-left: 19px;
    }
</style>
<form method="POST" id="extrafieldform" name="extrafieldform" action="<?php echo $this->scripturl ?>?action=saveextrafield">
<?php echo $this->Table->GetFormTable(); ?>
</form>
<div class="button_wrapper">
<?php if(!$this->IsFieldLocked) : ?>
    <input class="button" type="button" value="Save" onClick="submitClicked=true;if(validateOnSubmit()){selectAllMultiCodes();jQuery('#extrafieldform').submit()}">
<?php endif; ?>
    <input class="button" type="button" value="<?php echo _tk('return_to_extra_fields_list'); ?>" 
    onClick="if(confirm('<?php echo addslashes(_tk('confirm_or_cancel')) ?>')){SendTo('<?php echo $this->scripturl ?>?action=listextrafields');}">
</div>