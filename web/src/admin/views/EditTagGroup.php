<script language="Javascript">
    var submitClicked = false;
    function deleteTagSet()
    {
        if(confirm('Are you sure you want to delete this <?php echo _tk('tag_set') ?>?'))
        {
            jQuery('#frmTagSet').attr('action', '<?php echo $this->scripturl ?>?action=deletetagset');
            $('frmTagSet').submit();
        }
    }

    <?php echo MakeJavaScriptValidation('ADM', $this->form_design, array('name' => 'Name', 'description' => 'Description')) ?>

</script>

<form method="post" name="frmTagSet" id="frmTagSet" enctype="multipart/form-data" action="<?php echo $this->scripturl ?>?action=savetaggroup">

    <input type="hidden" id="updateid" name="updateid" value="<?php echo htmlspecialchars($this->data['updateid']) ?>'" />
    <input type="hidden" id="recordid" name="recordid" value="<?php echo \Sanitize::SanitizeInt($this->data['recordid']) ?>" />

    <?php echo $this->form->GetFormTable(); ?>

    <?php echo $this->ButtonGroup->getHTML(); ?>

</form>