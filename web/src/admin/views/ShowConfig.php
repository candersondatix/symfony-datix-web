<!-- General settings -->
<table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
    <tr height="20">
        <td class="titlebg" colspan="1">
            <a href="Javascript:ToggleTwistyExpand('indexphp', 'twisty_indexphp', 'Images/expand.gif', 'Images/collapse.gif')">
                <img hspace="2" id="twisty_indexphp" src="Images/collapse.gif" alt="*" border="0" /></a>
            <b>General settings</b>
        </td>
    </tr>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="windowbg2" align="center">
    <tr>
        <td>
            <div id="indexphp" >
                <table class="gridlines" cellpadding="4" cellspacing="1">
                    <tr><td class="windowbg2">Client name: </td><td class="windowbg2"><?php echo $this->ClientName; ?></td></tr>
                    <tr><td class="windowbg2">Licence key: </td><td class="windowbg2"><?php echo $this->LicenceKey; ?></td></tr>
                    <?php if ($this->ExpiryDate) : ?>
                        <tr><td class="windowbg2">Expiry date: </td><td class="windowbg2"><?php echo $this->ExpiryDate; ?></td></tr>
                    <?php endif; ?>
                    <?php if (!empty($this->modules)) : ?>
                        <tr>
                            <td class="windowbg2">Licensed modules:</td>
                            <td class="windowbg2"><?php echo $this->modulesStr; ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr><td class="windowbg2"><br /></td><td class="windowbg2"></td></tr>
                    <tr><td class="windowbg2">DatixWeb version: </td><td class="windowbg2"><?php echo $this->dtxversion; ?></td></tr>
                    <tr><td class="windowbg2" width="20%">Datix database version: </td><td class="windowbg2"><?php echo $this->dbversion; ?></td></tr>
                    <tr><td class="windowbg2">Database server name: </td><td class="windowbg2"><?php echo $this->dbservername; ?></td></tr>
                    <tr><td class="windowbg2">Database type: </td><td class="windowbg2"><?php echo $this->dbtype; ?></td></tr>
                    <tr><td class="windowbg2">Database name: </td><td class="windowbg2"><?php echo $this->dbname; ?></td></tr>
                    <tr><td class="windowbg2">Script URL: </td><td class="windowbg2"><?php echo $this->scripturl; ?></td></tr>
                </table>
            </div>
        </td>
    </tr>
</table>
<!-- Configuration files -->
<table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
    <tr height="20">
        <td class="titlebg" colspan="1">
            <form method="post" name="sendconfig" action="<?php echo $this->scripturl; ?>?action=showconfig">
                <a href="Javascript:ToggleTwistyExpand('designforms', 'twisty_designforms', 'Images/expand.gif', 'Images/collapse.gif')">
                    <img hspace="2" id="twisty_designforms" src="Images/collapse.gif" alt="*" border="0" /></a>
                <b>Configuration files</b><br />
                Send these configuration files to Datix support:
                <input type="submit" name="send" id="send" value="Send"/>
            </form>
        </td>
    </tr>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="windowbg2" align="center">
    <tr>
        <td>
            <div id="designforms">
                <table class="gridlines" cellpadding="4" cellspacing="1" width="100%">
                    <tr>
                        <td class="windowbg2" width="20%">index.php</td>
                        <td class="windowbg2">
                            <a href="#" onclick="exportFile('<?php echo $this->scripturl; ?>?service=Export&event=exportConfigFile&file=index.php')"> [download]</a>
                        </td>
                    </tr>
                    <?php if ($handle = opendir($this->ClientFolder)) : ?>
                        <?php while (false !== ($file = readdir($handle))) : ?>
                            <?php if ($file != "." && $file != ".." && (preg_match("/User/u", $file) || $file == "config.xml" || $file == "SQLErrors.xml")) : ?>
                                <tr>
                                    <td class="windowbg2" width="20%"><?php echo $file; ?></td>
                                    <td class="windowbg2">
                                        <a href="#" onclick="exportFile('<?php echo $this->scripturl; ?>?service=Export&event=exportConfigFile&file=<?php echo $file; ?>')"> [download]</a>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        <?php closedir($handle); ?>
                    <?php endif; ?>
                </table>
            </div>
        </td>
    </tr>
</table>
<!-- PHP info -->
<table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
    <tr height="20">
        <td class="titlebg" colspan="1">
            <a href="Javascript:ToggleTwistyExpand('phpinfo', 'twisty_phpinfo', 'Images/expand.gif', 'Images/collapse.gif')">
                <img hspace="2" id="twisty_phpinfo" src="Images/collapse.gif" alt="*" border="0" /></a>
            <b>PHP</b>
        </td>
    </tr>
</table>
<!-- </td> -->
<!-- </tr> -->
<!-- <tr> -->
    <!-- <td> -->
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="windowbg2" align="center">
    <tr>
        <td>
            <div id="phpinfo">
                <table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
                    <?php foreach ($this->phpini as $SettingName => $SettingValue) : ?>
                        <tr>
                            <td class='windowbg2'><?php echo $SettingName; ?></td>
                            <td class='windowbg2'><?php echo $SettingValue; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </td>
    </tr>
</table>
<!-- DATIX globals -->
<table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
    <tr height="20">
        <td class="titlebg" colspan="1">
            <a href="Javascript:ToggleTwistyExpand('datixglobals', 'twisty_datixglobals', 'Images/expand.gif', 'Images/collapse.gif')">
                <img hspace="2" id="twisty_datixglobals" src="Images/collapse.gif" alt="*" border="0" /></a>
            <b>Datix globals</b>
        </td>
    </tr>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="windowbg2" align="center">
    <tr>
        <td>
            <div id="datixglobals" >
                <table class="gridlines" cellpadding="4" cellspacing="1">
                    <?php foreach ($this->Globals as $Global) : ?>
                        <tr>
                            <td class='windowbg2'><?php echo $Global['parameter']; ?>: </td>
                            <td class='windowbg2'><font color="red"><?php echo $Global['parmvalue']; ?></font></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </td>
    </tr>
</table>
<!-- USER params -->
<table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
    <tr height="20">
        <td class="titlebg" colspan="1">
            <a href="Javascript:ToggleTwistyExpand('userparms', 'twisty_userparms', 'Images/expand.gif', 'Images/collapse.gif')">
                <img hspace="2" id="twisty_userparms" src="Images/collapse.gif" alt="*" border="0" /></a>
            <b>User params</b>
        </td>
    </tr>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="windowbg2" align="center">
    <tr>
        <td>
            <div id="userparms" >
                <table class="gridlines" cellpadding="4" cellspacing="1">
                    <?php foreach ($this->UserParms as $UserParm) : ?>
                        <tr>
                            <td class='windowbg2'><?php echo $UserParm['login'] . ' ' . $UserParm['parameter']; ?>: </td>
                            <td class='windowbg2'><font color="red"><?php echo $UserParm['parmvalue']; ?></font></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </td>
    </tr>
</table>
<!-- Loadfiles version -->
<table border="0" width="100%" cellspacing="0" cellpadding="4" class="windowbg2" align="center">
    <tr height="20">
        <td class="titlebg" colspan="1">
            <a href="Javascript:ToggleTwistyExpand('loadfilesversion', 'twisty_loadfilesversion', 'Images/expand.gif', 'Images/collapse.gif')">
                <img hspace="2" id="twisty_loadfilesversion" src="Images/collapse.gif" alt="*" border="0" /></a>
            <b>Loadfiles Version</b>
        </td>
    </tr>
</table>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="windowbg2" align="center">
    <tr>
        <td>
            <div id="loadfilesversion">
                <table class="gridlines" cellpadding="4" cellspacing="1">
                    <tr>
                        <td class="windowbg2">CCS2 Version: </td>
                        <td class="windowbg2"><?php echo $this->ccs2Version; ?></td>
                    </tr>
                </table>
                <?php if($this->dmdVersion): ?>
                <table class="gridlines" cellpadding="4" cellspacing="1">
                    <tr>
                        <td class="windowbg2">DM+D Version: </td>
                        <td class="windowbg2"><?php echo $this->dmdVersion; ?></td>
                    </tr>
                </table>
                <?php endif;?>
            </div>
        </td>
    </tr>
</table>
<?php if ($this->showJS) : ?>
    <?php if ($this->showSuccessJS) : ?>
        <script type="text/javascript">jQuery(document).ready(function(){alert("Email has been successfully sent")})</script>
    <?php else : ?>
        <script type="text/javascript">jQuery(document).ready(function(){alert("ERROR: Support email address is not set, no email has been sent")})</script>
    <?php endif; ?>
<?php endif; ?>