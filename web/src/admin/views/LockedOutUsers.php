<?php if (!empty($this->users)): ?>
<form method="POST" action="<?php echo $this->scripturl ?>?action=unlockusers" onsubmit="return checkUsers()">
    <table class="locked-users gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
	    <thead>
		    <tr class="tableHeader">
                <th class="titlebg selector" scope="col"><input type="checkbox" id="select_all" name="select_all" onclick="ToggleCheckAllClass('record-checkbox', this.checked)" /></th>
			    <th class="titlebg" scope="col"><a href="<?php echo $this->scripturl . '?action=locked_users&amp;orderby=login&amp;order=' . ($this->order == 'ASC' ? 'DESC' : "ASC") ?>" class="<?php echo ($this->orderby == 'login') ? 'sort ' . \UnicodeString::strtolower($this->order) : '' ?>"><?php echo _tk('username') ?></a></th>
			    <th class="titlebg" scope="col"><a href="<?php echo $this->scripturl . '?action=locked_users&amp;orderby=fullname&amp;order=' . ($this->order == 'ASC' ? 'DESC' : "ASC") ?>" class="<?php echo ($this->orderby == 'fullname') ? 'sort ' . \UnicodeString::strtolower($this->order) : '' ?>"><?php echo _tk('fullname') ?></a></th>
		    </tr>
	    </thead>
	    <tbody>
		    <?php foreach($this->users as $user): ?>
		    <tr class="listing-row">
                <td><input type="checkbox" class="record-checkbox" name="records[]" value="<?php echo $user->recordid ?>" /></td>
			    <th scope="row" class="<?php echo ($this->orderby == 'login') ? 'sort' : '' ?>"><?php echo htmlspecialchars($user->login) ?></th>
			    <td class="<?php echo ($this->orderby == 'fullname') ? 'sort' : '' ?>"><?php echo htmlspecialchars($user->fullname) ?></td>
		    </tr>
		    <?php endforeach ?>
	    </tbody>
    </table>
    <div class="button_wrapper">
        <input type="submit" value="<?php echo _tk('unlock') ?>" />
    </div>
</form>
<?php endif ?>
<script type="text/javascript">
function checkUsers()
{
    if (!jQuery(".record-checkbox:checked").length)
    {
        alert("<?php echo _tk('no-users-selected') ?>");
        return false;
    }
    else
    {
        return true;
    }
}
</script>