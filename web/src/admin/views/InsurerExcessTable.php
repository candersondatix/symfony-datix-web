<?php

global $oForm;

require "rico/applib.php";
require "rico/plugins/php/ricoLiveGridForms.php";

if (OpenGridForm('', 'claims_insurer_excess'))
{
    \src\admin\controllers\CodeSetupsActionTemplateController::InsurerExcessDefineFields();
}
else
{
    $error = _tk('rico_table_open_error');
}

$oForm->DisplayPage();

?>
<li>
    <div class="button_wrapper">
        <input type="button" value="Back" border="10" class="button" name="btnBack" onClick="SendTo('<?php echo $this->scripturl; ?>?action=codesetupslist&module=CLA');">
    </div>
</li>
</ol>
</form>

<?php CloseApp(); ?>