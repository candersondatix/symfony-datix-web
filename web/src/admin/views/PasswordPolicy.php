<?php echo $this->messages; ?>
<form action="<?php echo $this->scripturl; ?>?action=password_policy_update" method="POST" id="password-policy">
    <ul>
        <li class="section_title_row">
            <div class="section_title_group">
                <div class="section_title"><?php echo _tk('password-format'); ?></div>
            </div>
        </li>
        <li class="field_div">
            <?php $input = '<input name="password-length" value="' . $this->output['PWD_MIN_LENGTH'] . '" type="text" size="1" maxlength="3" onkeyup="this.value=this.value.replace(/[^\d]/,\'\')" />'; ?>
            <p><?php echo sprintf(_tk('password-must-be-characters-long'), $input); ?></p>
            <?php if (array_key_exists('password-length', $this->errors)): ?>
            <span class="field_error"><?php echo $this->errors['password-length']; ?></span>
            <?php endif ?>
        </li>
        <li class="field_div">
            <p><?php echo _tk('and-must-contain-at-least'); ?></p>
            <div class="inline_input_div">
                <input id="password-numbers" name="password-numbers" value="<?php echo $this->output['PWD_NUMBERS']; ?>" type="text" size="1" maxlength="3" onkeyup="this.value=this.value.replace(/[^\d]/,'')"/>
                <label for="password-numbers"><?php echo _tk('numbers'); ?></label>
                <?php if (array_key_exists('password-numbers', $this->errors)): ?>
                <span class="field_error"><?php echo $this->errors['password-numbers']; ?></span>
                <?php endif ?>
            </div>
            <div class="inline_input_div">
                <input id="password-ucletters" name="password-ucletters" value="<?php echo $this->output['PWD_UPPERCASE']; ?>" type="text" size="1" maxlength="3" onkeyup="this.value=this.value.replace(/[^\d]/,'')" />
                <label for="password-ucletters"><?php echo _tk('uppercase-letters'); ?></label>
                <?php if (array_key_exists('password-ucletters', $this->errors)): ?>
                <span class="field_error"><?php echo $this->errors['password-ucletters']; ?></span>
                <?php endif ?>
            </div>
            <div class="inline_input_div">	
                <input id="password-lcletters" name="password-lcletters" value="<?php echo $this->output['PWD_LOWERCASE']; ?>" type="text" size="1" maxlength="3" onkeyup="this.value=this.value.replace(/[^\d]/,'')"/>
                <label for="password-lcletters"><?php echo _tk('lowercase-letters'); ?></label>
                <?php if (array_key_exists('password-lcletters', $this->errors)): ?>
                <span class="field_error"><?php echo $this->errors['password-lcletters']; ?></span>
                <?php endif ?>
            </div>
            <div class="inline_input_div">
                <input id="password-symbols" name="password-symbols" value="<?php echo $this->output['PWD_SPECIAL']; ?>" type="text" size="1" maxlength="3" onkeyup="this.value=this.value.replace(/[^\d]/,'')"/>
                <label for="password-symbols"><?php echo _tk('symbols'); ?></label>
                <?php if (array_key_exists('password-symbols', $this->errors)): ?>
                <span class="field_error"><?php echo $this->errors['password-symbols']; ?></span>
                <?php endif ?>
            </div>
        </li>
        <li class="section_title_row">
            <div class="section_title_group">
                <div class="section_title"><?php echo _tk('password-settings'); ?></div>
            </div>
        </li>
        <li class="field_div">
            <div class="field_label_div" id="password-expiry-field" style="width: 15%">
                <label for="password-expiry" class="field_label"><?php echo _tk('expiry-in-days'); ?></label>
                <?php if (array_key_exists('password-expiry', $this->errors)): ?>
                <span class="field_error"><?php echo $this->errors['password-expiry']; ?></span>
                <?php endif ?>
            </div>
            <div class="field_input_div">
                <input id="password-expiry" name="password-expiry" type="text" size="3" maxlength="3" value="<?php echo $this->output['PWD_EXPIRY_DAYS']; ?>" onkeyup="this.value=this.value.replace(/[^\d]/,'')"/>
            </div>
        </li>
        <li class="field_div" id="unique-passwords-field">
            <div class="field_label_div" style="width: 15%">
                <label for="unique-passwords" class="field_label"><?php echo _tk('number-of-unique-passwords'); ?></label>
                <?php if (array_key_exists('unique-passwords', $this->errors)): ?>
                <span class="field_error"><?php echo $this->errors['unique-passwords']; ?></span>
                <?php endif ?>
            </div>
            <div class="field_input_div">
                <input id="unique-passwords" name="unique-passwords" type="text" size="3" maxlength="3" value="<?php echo ($this->output['PWD_UNIQUE'] != 'N') ? $this->output['PWD_UNIQUE_LAST'] : ''; ?>" onkeyup="this.value=this.value.replace(/[^\d]/,'')"/>
                <div class="field_related_input">
                    <input id="unique-passwords-disable" name="unique-passwords-disable" value="true" type="checkbox" <?php echo ($this->output['PWD_UNIQUE'] == 'N') ? 'checked' : ''; ?> />
                    <label for="unique-passwords-disable"><?php echo _tk('disable-unique-passwords'); ?></label>
                </div>
            </div>
        </li>
        <li>
            <div class="button_wrapper">
                <input id="btnSubmit" class="button" value="<?php echo _tk('btn_save'); ?>" name="submit" type="submit" />
                <input id="btnCancel" class="button" value="<?php echo _tk('btn_cancel'); ?>" name="cancel" type="submit" />
            </div>
        </li>
</form>
<script type="text/javascript" src="js_functions/password-policy<?php echo $this->addMinExtension; ?>.js"></script>