<table border="0" width="100%" cellspacing="1" class="gridlines" align="center">
    <?php if (!empty($this->Errors)) : ?>
        <?php foreach($this->Errors as $Error) : ?>
            <tr><td><?php echo $Error; ?></td></tr>
        <?php endforeach; ?>
    <?php else : ?>
        <?php if($this->ErrorLog) : ?>
            <?php echo Sanitize::SanitizeRaw($this->ErrorLog); ?>
        <?php else : ?>
            <tr><td><div class="info_div">The error log is empty</div></td></tr>
        <?php endif; ?>
    <?php endif; ?>
</table>