<script language='javascript' src='email<?php echo $this->addMinExtension; ?>.js'></script>
<table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
<?php if ($this->deleted): ?>
    <tr class="windowbg2">
        <td colspan="5">Template <?php echo \Escape::EscapeEntities($this->deleted) ?> was successfully deleted</td>
    </tr>
<?php endif ?>
    <tr class="tableHeader">
        <th><a href="<?php echo $this->scripturl ?>?action=listemailtemplates&amp;orderby=recordid&amp;order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->orderby == 'recordid' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">ID</a></th>
        <th><a href="<?php echo $this->scripturl ?>?action=listemailtemplates&amp;orderby=emt_name&amp;order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->orderby == 'emt_name' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">Name</a></th>
        <th><a href="<?php echo $this->scripturl ?>?action=listemailtemplates&amp;orderby=emt_notes&amp;order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->orderby == 'emt_notes' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">Description</a></th>
        <th><a href="<?php echo $this->scripturl ?>?action=listemailtemplates&amp;orderby=emt_module&amp;order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->orderby == 'emt_module' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">Module</a></th>
        <th><a href="<?php echo $this->scripturl ?>?action=listemailtemplates&amp;orderby=emt_author&amp;order=<?php echo ($this->order == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo ($this->orderby == 'emt_author' ? 'sort ' . \UnicodeString::strtolower($this->order) : '') ?>">Created By</a></th>
    </tr>
<?php if (count($this->templates) == 0): ?>
    <tr class="windowbg2">
        <td> No Templates have been created. </td>
    </tr>
<?php else: ?>
    <?php foreach ($this->templates as $template): ?>
        <?php if (!$template->emt_author || $this->initials == $template->emt_author || $template->emt_author == 'Datix' || $this->adminUser): ?>
    <tr class="listing-row">
        <td class="<?php echo ($this->orderby == 'recordid' ? 'sort' : '') ?>"><a href="<?php $this->scripturl ?>?action=emailtemplate&recordid=<?php echo $template->recordid ?>"><?php echo $template->recordid ?></a></td>
        <td class="<?php echo ($this->orderby == 'emt_name' ? 'sort' : '') ?>"><a href="<?php $this->scripturl ?>?action=emailtemplate&recordid=<?php echo $template->recordid ?>"><?php echo $template->emt_name ?></a></td>
        <td class="<?php echo ($this->orderby == 'emt_notes' ? 'sort' : '') ?>"><a href="<?php $this->scripturl ?>?action=emailtemplate&recordid=<?php echo $template->recordid ?>"><?php echo $template->emt_notes ?></a></td>
        <td class="<?php echo ($this->orderby == 'emt_module' ? 'sort' : '') ?>"><a href="<?php $this->scripturl ?>?action=emailtemplate&recordid=<?php echo $template->recordid ?>"><?php echo $this->ModuleDefs[$template->emt_module]["NAME"] ?></a></td>
        <td class="<?php echo ($this->orderby == 'emt_author' ? 'sort' : '') ?>"><a href="<?php $this->scripturl ?>?action=emailtemplate&recordid=<?php echo $template->recordid ?>"><?php echo $template->emt_author ?></a></td>
    </tr>
        <?php else: ?>
    <tr class="listing-row">
        <td class="<?php echo ($this->orderby == 'recordid' ? 'sort' : '') ?>"><?php echo $template->recordid ?></td>
        <td class="<?php echo ($this->orderby == 'emt_name' ? 'sort' : '') ?>"><?php echo $template->emt_name ?></td>
        <td class="<?php echo ($this->orderby == 'emt_notes' ? 'sort' : '') ?>"><?php echo $template->emt_notes ?></td>
        <td class="<?php echo ($this->orderby == 'emt_module' ? 'sort' : '') ?>"><?php echo $this->ModuleDefs[$template->emt_module]["NAME"] ?></td>
        <td class="<?php echo ($this->orderby == 'emt_author' ? 'sort' : '') ?>"><?php echo $template->emt_author ?></td>
    </tr>
        <?php endif ?>
    <?php endforeach ?>
<?php endif ?>
</table>