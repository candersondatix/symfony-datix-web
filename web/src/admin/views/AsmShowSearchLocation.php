<div id="search-box-div" class="search-box-div">
    <form id="asmsearchlocationform" action="<?php echo $this->scripturl ?>" method="get">
        <input type="hidden" name="action" value="asmshowsearchlocation">
        <input id="search-box" type="text" class="search-box" name="location">
        <button type="submit">Search</button>
    </form>
</div>
<div id="search-results" class="search-results">
    <?php if (count($this->locations) < 1): ?>
        <div class="search-no-records"><?php echo _tk('no_records_found'); ?></div>
    <?php else: ?>
        <ul>
            <?php foreach ($this->locations as $location): ?>
                <li><a href="<?php echo $this->scripturl ?>?action=asmlocations&amp;location_id=<?php echo $location['recordid'] ?>"><?php echo $location['loc_name'] ?></a></li>
            <?php endforeach ?>
        </ul>
    <?php endif ?>
</div>