<form name="editcolmodule" method="post" action="<?php echo $this->scripturl; ?>?action=usergroupreport">
    <input type="hidden" id="module" name="module" value="<?php echo $this->module; ?>" />
    <table class="gridlines" align="center" border="1" cellpadding="4" cellspacing="0" width="100%">
        <div class="new_titlebg"><span style="float:left;margin-right:5px;margin-top:5px;"><b>Module:</b></span>
            <?php echo $this->ModuleDropDownHTML; ?>
            <a href="Javascript:ExpandAll();" alt="" style="color:black">[expand all sections]</a>
            <a href="Javascript:CollapseAll();" alt="" style="color:black">[collapse all sections]</a>
        </div>
        <?php foreach($this->MatchingStaff as $grp_id) : ?>
            <div class="show-staff-title" onclick="jQuery('#<?php echo $grp_id; ?>').toggle();getUserGroupRecords('<?php echo $grp_id; ?>', '<?php echo ($this->PrintMode == true ? '1' : '0'); ?>', '<?php echo $this->module; ?>')">
                <b>
                    <img hspace="2" id="twisty_<?php echo $grp_id; ?>" src="Images/icons/icon_group.png" alt="*" border="0" onclick="getUserGroupRecords('<?php echo $grp_id; ?>', <?php echo ($this->PrintMode == true ? '1' : '0'); ?>, '<?php echo $this->module; ?>')" />
                    <?php echo $this->MatchingStaffName[$grp_id][0]; ?>
                </b>
                <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->MatchingStaffName[$grp_id][1]; ?>
            </div>
            <table style="display:none"></table>
            <div id="<?php echo $grp_id; ?>" class="display-group" style="display:<?php echo ($this->PrintMode ? "block" : "none"); ?>"></div>
        <?php endforeach; ?>
    </table>
</form>
<script language='javascript' type='text/javascript'>
    function getUserGroupRecords(grp_id, printmode, module)
    {
        new Ajax.Updater(
                grp_id,
                scripturl+'?action=httprequest&type=usergrouprepresponse', {
                    parameters: {
                        grp_id : grp_id,
                        print : printmode,
                        module: module
                    }
                }
        );
    }
</script>
<script language="JavaScript" type="text/javascript">
    function showColumns(selectedModule)
    {
        document.getElementById("module").value = document.getElementById("lbModule").value;
    }

    function ExpandAll()
    {
        jQuery('.display-group').show();

        <?php foreach($this->MatchingStaff as $grp_id) : ?>
            getUserGroupRecords('<?php echo $grp_id; ?>', '<?php echo ($this->PrintMode == true ? '1' : '0'); ?>', '<?php echo $this->module; ?>');
        <?php endforeach; ?>
    }

    function CollapseAll()
    {
        jQuery('.display-group').hide();
    }
</script>