<script language="JavaScript" type="text/javascript">
    function dnLDAPSyncAllPopup()
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?= $this->scripturl ?>?action=ldapsyncall&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }

    function dnLDAPSyncExistingPopup()
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?= $this->scripturl ?>?action=ldapsyncall&updateonly=1&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }

    function dnLDAPSyncPopup(grp_id)
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?= $this->scripturl ?>?action=ldapsync&groupid=' + grp_id + '&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }

    function dnLDAPUpdateOnly(grp_id)
    {
        mywindow=open('index.php','myname','resizable,width=600,height=370,scrollbars=1');
        mywindow.location.href = '<?= $this->scripturl ?>?action=ldapsync&groupid=' + grp_id +'&updateonly=1&token=<?= CSRFGuard::getCurrentToken() ?>';
        if (mywindow.opener == null) {
            mywindow.opener = self;
        }
    }
</script>

<div class="page-numbers">
    <?php for ($a = ord('A'); $a <= ord('Z'); $a++): ?>
        <a href="?action=listgroups&match=<?php echo chr($a) ?>&field=<?php echo $this->matchfield ?>&sort=<?php echo $this->sorttype ?>">
            <?php if (ord($this->matchletter) == $a): ?>
                <b>[<?php echo chr($a) ?>]</b>
            <?php else: ?>
                <?php echo chr($a) ?>
            <?php endif ?>
        </a>&nbsp;
    <?php endfor ?>
    &nbsp;&nbsp;&nbsp;
    <?php if ($this->matchletter == 'all'): ?><b>[<?php endif ?>
    <a href="?action=listgroups&match=all&sort=<?php echo $this->sorttype ?>">All</a>
    <?php if ($this->matchletter == 'all'): ?>]</b><?php endif ?>
</div>

<div class="new_windowbg">
    <table border="0" width="100%" cellspacing="1" cellpadding="4" class="formbg" align="center">
        <tr class="tableHeader">
            <th class="windowbg" width="5%">
                <a href="?action=listgroups&match=<?php echo $this->matchletter ?>&field=recordid&sort=<?php echo ($this->matchfield == 'recordid' && $this->sorttype == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo (($this->matchfield == 'recordid') ? 'sort ' . \UnicodeString::strtolower($this->sorttype) : '')?>">ID</a>
            </th>
            <th class="windowbg" width="20%">
                <a href="?action=listgroups&match=<?php echo $this->matchletter ?>&field=grp_code&sort=<?php echo ($this->matchfield == 'grp_code' && $this->sorttype == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo (($this->matchfield == 'grp_code') ? 'sort ' . \UnicodeString::strtolower($this->sorttype) : '')?>">Name</a>
            </th>
            <th class="windowbg">
                <a href="?action=listgroups&match=<?php echo $this->matchletter ?>&field=grp_description&sort=<?php echo ($this->matchfield == 'grp_description' && $this->sorttype == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo (($this->matchfield == 'grp_description') ? 'sort ' . \UnicodeString::strtolower($this->sorttype) : '')?>">Description</a>
            </th>
            <th class="windowbg">
                <a href="?action=listgroups&match=<?php echo $this->matchletter ?>&field=grp_type&sort=<?php echo ($this->matchfield == 'grp_type' && $this->sorttype == 'ASC' ? 'DESC' : 'ASC') ?>" class="<?php echo (($this->matchfield == 'grp_type') ? 'sort ' . \UnicodeString::strtolower($this->sorttype) : '')?>">Type</a>
            </th>

            <?php if ($this->ldap->LDAPEnabled() && IsFullAdmin() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS'): ?>
                <th class="windowbg"></td><td class="windowbg"></th>
            <?php endif ?>

        </tr>

        <?php foreach ($this->data as $row): ?>
        <?php $url = filter_var($this->scripturl.'?action=editgroup&grp_id=' . $row->recordid, FILTER_SANITIZE_URL); ?>
        <tr class="listing-row">
            <td class="windowbg2 <?php echo ($this->matchfield == 'recordid') ? ' sort' : '' ?>"><a href="<?php echo $url ?>"><?php echo \Sanitize::SanitizeInt($row->recordid)?></a></td>
            <td class="windowbg2 <?php echo ($this->matchfield == 'grp_code') ? ' sort' : '' ?>"><a href="<?php echo $url ?>"><?php echo htmlspecialchars($row->grp_code)?></a></td>
            <td class="windowbg2 <?php echo ($this->matchfield == 'grp_description') ? ' sort' : '' ?>"><a href="<?php echo $url ?>"><?php echo htmlspecialchars($row->grp_description)?></a></td>
            <td class="windowbg2 <?php echo ($this->matchfield == 'grp_type') ? ' sort' : '' ?>"><a href="<?php echo $url ?>">
                <?php switch($row->grp_type)
                {
                    case (GRP_TYPE_ACCESS | GRP_TYPE_EMAIL): echo 'Record access and e-mail notification'; break;
                    case GRP_TYPE_ACCESS: echo 'Record access'; break;
                    case GRP_TYPE_EMAIL: echo 'E-mail notification'; break;
                    case GRP_TYPE_DENY_ACCESS: echo 'Deny access'; break;
                }?>
                </a></td>

            <?php if ($this->ldap->LDAPEnabled() && IsFullAdmin() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS'): ?>
                <td class="windowbg2" align="center" width="10%"><a href="javascript:dnLDAPSyncPopup(<?php echo \Sanitize::SanitizeInt($row->recordid)?>)"><?php echo _tk('ldap_sync_all_group')?></a></td>
                <td class="windowbg2" align="center" width="10%"><a href="javascript:dnLDAPUpdateOnly(<?php echo \Sanitize::SanitizeInt($row->recordid)?>)"><?php echo _tk('ldap_sync_existing_group')?></a></td>
            <?php endif ?>
        </tr>
        <?php endforeach ?>

        <?php if ($this->ldap->LDAPEnabled() && IsFullAdmin() && GetParm('LDAP_TYPE', 'GROUPS') == 'GROUPS'): ?>
            <tr>
                <td class="windowbg" align="left" colspan="<?=$this->colspan?>"><a href="javascript:dnLDAPSyncAllPopup()" ><?php echo _tk('ldap_sync_all_groups')?></a></td>
            </tr>
            <tr>
                <td class="windowbg" align="left" colspan="<?=$this->colspan?>"><a href="javascript:dnLDAPSyncExistingPopup()" ><?php echo _tk('ldap_sync_existing_groups')?></a></td>
            </tr>
        <?php endif ?>
    </table>
</div>