<?php
namespace src\actionchains\filters;

use src\framework\controller\ControllerFilter;

class DeleteActionChainFilter extends ControllerFilter
{
    /**
    * Secures the process of removing Action Chains from records.
    * 
    * @param string $action The subsequent action to perform on success.
    * 
    * @throws URLNotFoundException
    */
    public function doAction($action)
    {
        if (bYN(GetParm('DELETE_ACT_CHAIN', 'Y')))
        {
            return $this->proceed($action);
        }
        else
        {
            throw new \URLNotFoundException();
        }
    }
}