<?php
namespace src\actionchains\model;

use src\framework\model\Mapper;
use src\framework\model\ModelFactory;
use src\framework\model\Entity;
use src\framework\query\Query;

class ActionChainMapper extends Mapper
{
    /**
     * Handles the mapping for the action chain steps.
     * 
     * @var StepMapper
     */
    protected $stepMapper;
    
    public function __construct(\DatixDBQuery $db, ModelFactory $factory, StepMapper $stepMapper)
    {
        parent::__construct($db, $factory);
        $this->stepMapper = $stepMapper;
    }
    
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actionchains\\model\\ActionChain';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
       return 'action_chains'; 
    }
    
    /**
     * Adds the condition that only chains that aren't archived are selected.
     * 
     * {@inheritdoc}
     */
    public function select(Query $query)
    {
        $query->where(array('action_chains.ach_archive' => 0));
        return parent::select($query);
    }
    
    /**
     * Also handle action chain steps.
     * 
     * {@inheritdoc}
     */
    public function doInsert(Entity $object)
    {
        $id = parent::doInsert($object);
        foreach ($object->steps as $step)
        {
            $step->acs_chain_id = $id;
            $this->stepMapper->save($step);
        }
        return $id;
    }
    
    /**
     * Also handle action chain steps.
     * 
     * {@inheritdoc}
     */
    public function doUpdate(Entity $object)
    {
        $result = parent::doUpdate($object);
        
        $ids = array();
        foreach ($object->steps as $step)
        {
            $this->stepMapper->save($step);
            $ids[] = $step->recordid;
        }
        
        // remove steps that are no longer associated with this object
        // TODO devise a common method for dealing with nested collections when persisiting entities.
        list($query, $where, $fields) = $this->factory->getQueryFactory()->getQueryObjects();
        
        $fields->field('action_chain_steps.acs_chain_id')->eq($object->recordid)
               ->field('action_chain_steps.recordid')->notIn($ids);
        
        $where->add($fields);
        
        $query->delete()
              ->from('action_chain_steps')
              ->where($where);
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        return $this->db->prepareAndExecute($parameters) && $result;
    }
    
    /**
     * Action chains are soft-deleted (archived) because concrete chains still need to reference the title etc.
     * 
     * {@inheritdoc}
     */
    public function doDelete(Entity $object)
    {
        $query = $this->factory->getQueryFactory()->getQuery();
        
        $query->update($this->getTable())
              ->set(array('action_chains.ach_archive' => 1))
              ->where(array('action_chains.recordid' => $object->recordid));
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
        
        $this->db->setSQL($sql);
        return $this->db->prepareAndExecute($parameters);
    }
    
    /**
     * Retrieves the ID of the next instance of an action chain to be created against a main module record.
     * 
     * Action chains can be created against records more than once, so the ID is used to identify the exact chain instance.
     * 
     * @param int    $chainID          The ID of the action chain being used to create the action records.
     * @param int    $mainRecordID     The ID of the record we're creating the chain against.
     * @param string $mainRecordModule The module of the record we're creating the chain against.
     * 
     * @return int The next instance ID.
     */
    public function getNextInstanceId($chainID, $mainRecordID, $mainRecordModule)
    {
        $query = $this->factory->getQueryFactory()->getQuery();
        
        $query->select(array('MAX(act_chain_instance)'))
              ->from('ca_actions')
              ->where(array('act_module' => $mainRecordModule, 'act_cas_id' => $mainRecordID, 'act_chain_id' => $chainID));
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
                
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->fetch(\PDO::FETCH_COLUMN) + 1;
    }
    
    /**
    * Retrieves the Handler from the main module record.
    *
    * @param string $module   The module code.
    * @param int    $recordid The ID of the record we're creating an action chain against.
    * 
    * @return string $initials The initials of the record's handler.
    */
    public function getHandler($module, $recordid)
    {
        $moduleDefs = $this->registry->getModuleDefs();
        $initials   = '';
        
        if (isset($moduleDefs[$module]['FIELD_NAMES']['HANDLER']))
        {
            $query = $this->factory->getQueryFactory()->getQuery();
        
            $query->select(array($moduleDefs[$module]['FIELD_NAMES']['HANDLER']))
                  ->from($moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'])
                  ->where(array('recordid' => $recordid));
            
            list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
                    
            $this->db->setSQL($sql);
            $this->db->prepareAndExecute($parameters);
            $initials = $this->db->fetch(\PDO::FETCH_COLUMN);
        }

        // Make sure that we have only one handler because actions can only have one handler
        if (\UnicodeString::stripos($initials, ' '))
        {
            $auxInitials = explode(' ', $initials);
            $initials = $auxInitials[0];
        }
        
        return $initials;
    }
    
    /**
    * Removes a concrete action chain from a record.
    *
    * @param int    $main_recordid  The id of the record we're deleting the chain from.
    * @param string $module         The module of the record we're deleting the chain from.
    * @param int    $chain_id       The action chain id.
    * @param int    $chain_instance The action chain instance number.
    * 
    * @return boolean Whether or not the operation was successful.
    */
    public function deleteConcreteActionChain($main_recordid, $module, $chain_id, $chain_instance)
    {
        $query = $this->factory->getQueryFactory()->getQuery();
        
        $query->delete()
              ->from('ca_actions')
              ->where(array('act_cas_id' => $main_recordid, 'act_module' => $module, 'act_chain_id' => $chain_id, 'act_chain_instance' => $chain_instance));
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);
                
        $this->db->setSQL($sql);
        return $this->db->prepareAndExecute($parameters);
    }

    /**
     * Retrieves the location fields from the main module record.
     *
     * @param string $module         The module code.
     * @param int    $recordId       The ID of the record we're creating an action chain against.
     *
     * @return array $locationFields The location fields of the record.
     */
    public function getLocationFields($module, $recordId)
    {
        $moduleDefs = $this->registry->getModuleDefs();
        $fields     = array_keys($moduleDefs[$module]['STAFF_EMPL_FILTER_MAPPINGS']);

        $query = $this->factory->getQueryFactory()->getQuery();

        $query->select($fields)
            ->from($moduleDefs[$module]['VIEW'] ?: $moduleDefs[$module]['TABLE'])
            ->where(array('recordid' => $recordId));

        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        $locationFields = $this->db->fetch(\PDO::FETCH_ASSOC);

        return $locationFields;
    }
}