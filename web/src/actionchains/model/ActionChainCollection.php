<?php
namespace src\actionchains\model;

use src\framework\model\EntityCollection;

class ActionChainCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actionchains\\model\\ActionChain';
    }
}