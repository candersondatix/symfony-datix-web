<?php
namespace src\actionchains\model;

use src\framework\model\RecordEntity;

/**
 * A step in an action chain.  Used as a template to create actions when the chain is applied to a main module record.
 */
class Step extends RecordEntity
{
    /**
     * The id of the chain this step belongs to.
     * 
     * @var int
     */
    protected $acs_chain_id;
    
    /**
     * The order of this step in the chain.
     * 
     * @var int
     */
    protected $acs_order;
    
    /**
     * The action type (act_type code).
     * 
     * @var string
     */
    protected $acs_act_type;
    
    /**
     * The action description (act_descr).
     * 
     * @var string
     */
    protected $acs_act_desc;
    
    /**
     * The number of days after the event specified by acs_start_after_type that the action starts (act_dstart).
     * 
     * @var int
     */
    protected $acs_start_no;
    
    /**
     * How acs_start_no is calculated (calendar days or working days).
     * 
     * @var string
     */
    protected $acs_start_days_type;
    
    /**
     * The event which triggers the start of the action (chain commencement or completion of a previous action).
     * 
     * @var string
     */
    protected $acs_start_after_type;
    
    /**
     * The ID of the previous step when acs_start_after_type is completion of a previous action.
     * 
     * @var int
     */
    protected $acs_start_after_step;
    
    /**
     * The number of days after the start date that the action is due (act_ddue).
     * 
     * @var int
     */
    protected $acs_due_no;
    
    /**
     * How acs_due_no is calculated (calendar days or working days).
     * 
     * @var string
     */
    protected $acs_due_days_type;
    
    /**
     * The number of days before the due date a reminder is sent out for the action (act_reminder_days_no).
     * 
     * @var int
     */
    protected $acs_reminder_no;
    
    /**
     * How acs_reminder_no is calculated (calendar days or working days).
     * 
     * @var string
     */
    protected $acs_reminder_days_type;
}