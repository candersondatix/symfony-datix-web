<li name="causes_row" id="causes_row">
    <input type="hidden" name="CHANGED-<?php echo $this->RootCauseField; ?>" id="CHANGED-<?php echo $this->RootCauseField; ?>" />
    <ul>
        <li class="section_title_row" id="causes_subtitle_row" name="causes_subtitle_row">
            <div class="section_title_group">
                <div class="section_title">
                    <?php echo $this->titlerow; ?>
                    <?php if (!bYN(GetParm('INC_INV_RC', 'N')) && !($this->FormType == 'Print' || $this->FormType == 'ReadOnly')) : ?>
                    <br /><span style="font-weight:normal;">Choose one option from each group. If more than one option applies, choose the higher number.</span>
                    <?php endif; ?>
                </div>
            </div>
        </li>
    </ul>
    <ul>
    <?php if (!empty($this->root_causes)) : ?>
        <?php $RootCauseNo = 0; ?>
        <?php foreach ($this->root_causes as $code => $rc_row) : ?>
            <?php
            $description = $rc_row['description'];
            $group_no = $rc_row['cause_group'];
            ?>
            <?php if (!bYN(GetParm('DIF_2_NORCGS')) && $LastGroup != $group_no) : ?>
            <?php if($RootCauseNo > 0): ?>
            </ul></li>
            <?php endif; ?>
            <li><div class="field_div"><div class="field_label_div nofloat"><b><?php echo $group_no; ?>.  <?php echo $this->root_cause_groups[$group_no]['descr']; ?></b><?php echo $this->root_cause_groups[$group_no]['notes'] ? '<br />' . $this->root_cause_groups[$group_no]['notes'] : ''; ?></div></div><ul>
                <?php if (!bYN(GetParm('INC_INV_RC', 'N'))) : ?>
                    <li class="field_div"><div class="field_label_div nofloat">
                            <?php if (!($this->FormType == 'Print' || $this->FormType == 'ReadOnly')) : ?>
                                <input type="radio" name="<?php echo \UnicodeString::strtolower($this->module); ?>rootcause_<?php echo $group_no; ?>" value="" checked="checked" onchange="jQuery('#CHANGED-<?php echo $this->RootCauseField; ?>').val('1');" />
                            <?php else : ?>
                                <?php if ($this->radioset[$group_no]) : ?>
                                    <img border="0" src="Images/radiobuttonnotset.gif" alt="" />
                                <?php else : ?>
                                    <img border="0" src="Images/radiobutton.gif" alt="" />
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php echo $group_no; ?>.0 None of these factors implicated or no information
                        </div></li>
                <?php endif; ?>
                <?php endif; ?>
                <?php $LastGroup = $group_no; ?>
                <li class="field_div"><div class="field_label_div nofloat">
                        <?php if (!($this->FormType == 'Print' || $this->FormType == 'ReadOnly')) : ?>
                            <?php if (bYN(GetParm('INC_INV_RC'))) : ?>
                                <input type="checkbox" name="<?php echo \UnicodeString::strtolower($this->module); ?>rootcause_<?php echo $RootCauseNo; ?>" value="<?php echo $code; ?>"<?php echo $rc_row['isChecked'] ? " checked=\"checked\"" : ""; ?> onchange="jQuery('#CHANGED-<?php echo $this->RootCauseField; ?>').val('1');setChanged('<?php echo \UnicodeString::strtolower($this->module); ?>rootcause_<?php echo $RootCauseNo; ?>');" title="<?php echo $description; ?>" />
                            <?php else : ?>
                                <input type="radio" name="<?php echo \UnicodeString::strtolower($this->module); ?>rootcause_<?php echo $group_no; ?>" value="<?php echo $code; ?>"<?php echo $rc_row['isChecked'] ? " checked=\"checked\"" : ""; ?> onchange="jQuery('#CHANGED-<?php echo $this->RootCauseField; ?>').val('1');setChanged('<?php echo \UnicodeString::strtolower($this->module); ?>rootcause_<?php echo $RootCauseNo; ?>');" title="<?php echo $description; ?>" />
                            <?php endif; ?>
                        <?php else : ?>
                            <?php if (in_array($code, $this->causes) && bYN(GetParm('INC_INV_RC'))) : ?>
                                <img border="0" src="Images/tickedbox.gif" alt="" />
                                <input type="hidden" name="<?php echo \UnicodeString::strtolower($this->module); ?>rootcause_<?php echo $RootCauseNo; ?>" value="<?php echo $code; ?>" />
                            <?php endif; ?>
                            <?php if (in_array($code, $this->causes) && !bYN(GetParm('INC_INV_RC'))) : ?>
                                <img border="0" src="Images/radiobutton.gif" alt="" />
                                <input type="hidden" name="<?php echo \UnicodeString::strtolower($this->module); ?>rootcause_<?php echo $group_no; ?>" value="<?php echo $code; ?>" />
                            <?php endif; ?>
                            <?php if (!in_array($code, $this->causes) && !bYN(GetParm('INC_INV_RC'))) : ?>
                                <img border="0" src="Images/radiobuttonnotset.gif" alt="" />
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php echo $code; ?> <?php echo $description; ?>
                    </div></li>
                <?php $RootCauseNo++; ?>
            <?php if($RootCauseNo == count($this->root_causes)): ?>
            </ul></li>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php else : ?>
        <b><?php echo _tk('no_causes'); ?></b>
    <?php endif; ?>
    </ul>
    <?php if ($this->FormType == 'Print') : ?>
    <?php else : ?>
    <input type="hidden" name="numrootcauses" value="<?php echo $this->NumberRootCauses; ?>" />
    <?php endif; ?>
</li>
<?php if (is_array($this->data['full_audit'][$this->RootCauseField])) : ?>
<?php foreach ($this->data['full_audit'][$this->RootCauseField] as $Audit) : ?>
<li class="field_div" id="aud_<?php echo \UnicodeString::strtolower($this->module); ?>_root_causes">
    <div class="field_label_div" style="width: 25%;">
        <?php echo code_descr($this->module, $this->HANDLER, $Audit['aud_login']) . " " . FormatDateVal($Audit['aud_date'], true); ?>
    </div>
    <div class="field_input_div" style="width: 70%;">
        <?php if ($Audit['aud_detail']) : ?>
        <?php foreach(explode(' ', $Audit['aud_detail']) as $Detail) : ?>
        <?php echo $Detail.' '.$this->root_causes[$Detail]['description']; ?><br />
        <?php endforeach; ?>
        <?php else : ?>
        &lt;no value&gt;
        <?php endif; ?>
    </div>
</li>
<?php endforeach; ?>
<?php endif; ?>