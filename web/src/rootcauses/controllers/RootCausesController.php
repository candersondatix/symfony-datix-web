<?php

namespace src\rootcauses\controllers;

use src\framework\controller\Controller;

class RootCausesController extends Controller
{
    /**
     * Create Legacy root causes section for generic modules. similar to Incidents function,
     * but can be used for other modules if required.
     */
    public function SectionRootCauseDetails()
    {
        global $UserLabels;

        $ModuleDefs = $this->registry->getModuleDefs();
        $data = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('FormType');
        $module = $this->request->getParameter('module');

        $RootCauseField = \UnicodeString::strtolower($module) . '_root_causes';
        // Parse root causes, if any
        $causes = explode(' ', $data[$RootCauseField]);

        if ($causes == array(''))
        {
            // values could exist as incrootcause_1 etc if we've been redirected back to the form because of an error
            $causes = array();

            foreach ($data as $field => $value)
            {
                if (\UnicodeString::strpos($field, \UnicodeString::strtolower($module).'rootcause_') !== false)
                {
                    $causes[] = $data[$field];
                }
            }

            natsort($causes);
        }

        // Have to order by cause group listorder and then by cause if INC_INV_RC is set
        if (bYN(GetParm('INC_INV_RC')) && !bYN(GetParm('DIF_2_NORCGS')))
        {
            $sql = "
                SELECT
                    code, description, cause_group
                FROM
                    code_inc_cause, code_types
                WHERE
                    cod_code = cause_group
                    AND
                    cod_type = 'RCG'
                    AND
                    ((".($FormType != 'Search' ? "code_inc_cause.cod_priv_level <> 'N' AND " : '')."code_inc_cause.cod_priv_level <> 'X') OR code_inc_cause.cod_priv_level IS NULL)
                    AND
                    ((".($FormType != 'Search' ? "code_types.cod_priv_level <> 'N' AND " : '')."code_types.cod_priv_level <> 'X') OR code_types.cod_priv_level IS NULL)
                ORDER BY
                    code_types.cod_listorder, cause_group, code_inc_cause.cod_listorder, code
            ";
        }
        else
        {
            $sql = "
                SELECT
                    code, description, cause_group
                FROM
                    code_inc_cause
                WHERE
                    ((".($FormType != 'Search' ? "cod_priv_level <> 'N' AND " : '')."cod_priv_level <> 'X') OR cod_priv_level IS NULL)
                ORDER BY
                    cod_listorder, cause_group, code
            ";
        }

        $RootCauseArray = \DatixDBQuery::PDO_fetch_all($sql);

        $NumberRootCauses = 0;
        //This is a workaround for comparing the values in $causes to $root_causes. Unfortunately even when attempting to cast both values as strings, PHP 
        //cannot tell the difference between 10.1, 10.10 and 10.100, which in this context we consider to all be correct unique values. Adding 'a' to the 
        //end of each line allows for them to always be compared as string values
        foreach ($causes as $key => $cause)
        {
        	$comparisonCauses[$key] = $cause.'a';
        }
        foreach ($RootCauseArray as $row)
        {
            $root_causes[$row['code']] = $row;
            if(in_array((string)$root_causes[$row['code']]['code'].'a', $comparisonCauses))
            {		
            	$root_causes[$row['code']]['isChecked'] = true;
            }
            else
            {
            	$root_causes[$row['code']]['isChecked'] = false;
            }
            $NumberRootCauses++;
        }

        if (!bYN(GetParm('INC_INV_RC')))
        {
            $root_cause_groups = array(
                "1" => array(
                    "descr" => "Identification of higher risk",
                    "notes" => "Use this section to identify reasons why a higher risk patient or procedure was not identified or treated as such."
                ),
                "2" => array(
                    "descr" => "Management of higher risks",
                    "notes" => "Use this section to identify any shortcomings there may have been in the process of matching a higher risk patient or procedure to an appropriately skilled clinician."
                ),
                "3" => array(
                    "descr" => "Guidelines or protocols",
                    "notes" => "Use this section to identify any shortcomings in the use of guidelines or protocols that may have been implicated in the incident."
                ),
                "4" => array (
                    "descr" => "Credentialling: skills deficit not identified in the course of:",
                    "notes" => "Use this section where staff did not exercise skills required of their grade and the skills deficit was not identified/remedied during the process which has MOST RECENTLY been applied"
                ),
                "5" => array(
                    "descr" => "Team factors",
                    "notes" => "Use this section to identify any shortcomings in teamwork that may have been implicated in the incident."
                ),
                "6" => array(
                    "descr" => "Work environment/resources",
                    "notes" => "Use this section to identify any shortcomings in the work environment or available resources that may have been implicated in the incident"
                )
            );
        }

        $sql = "
            SELECT
                cod_code, cod_descr
            FROM
                code_types
            WHERE
                cod_type = 'RCG'
            ORDER BY
                cod_listorder, cod_code
        ";

        $CodeArray = \DatixDBQuery::PDO_fetch_all($sql);

        foreach ($CodeArray as $row)
        {
            if (!$root_cause_groups[$row['cod_code']]['descr'])
            {
                $root_cause_groups[$row['cod_code']]['descr'] = $row['cod_descr'];
            }

            $LastGroup = $row['cod_code'];
        }

        if ($UserLabels['causes'])
        {
            $titlerow = $UserLabels['causes'];
        }
        else
        {
            $titlerow = _tk('cause_analysis_title');
        }

        if (!empty($root_causes))
        {
            //where are the radio buttons set?
            foreach ($root_causes as $code => $rc_row)
            {
                $description = $rc_row['description'];
                $group_no = $rc_row['cause_group'];

                if (!bYN(GetParm('INC_INV_RC')))
                {
                    if (in_array($code, $causes))
                    {
                        $radioset[$group_no] = true;
                    }

                    continue;
                }
            }
        }

        $this->response->build('src\rootcauses\views\SectionRootCauseDetails.php', array(
            'RootCauseField' => $RootCauseField,
            'titlerow' => $titlerow,
            'FormType' => $FormType,
            'root_causes' => $root_causes,
            'root_cause_groups' => $root_cause_groups,
            'module' => $module,
            'radioset' => $radioset,
            'causes' => $causes,
            'NumberRootCauses' => $NumberRootCauses,
            'data' => $data,
            'HANDLER' => $ModuleDefs[$module]['FIELD_NAMES']['HANDLER']
        ));
    }
}