<?php

namespace src\search\controllers;

use src\framework\controller\Controller;

class ClearSearchController extends Controller
{
    function clearsearch()
    {
        ClearSearchSession($this->request->getParameter('module'));

        // clear duplicate contacts search
        unset($_SESSION['CON']['DUPLICATE_SEARCH']);
        unset($_SESSION['CON']['LAST_SEARCH_DUPLICATE_FIELDS']);

        $RedirectUrl = ($this->request->getParameter('module') ? '?module=' . $this->request->getParameter('module') : '');

        $this->redirect('app.php' . $RedirectUrl);
    }
}