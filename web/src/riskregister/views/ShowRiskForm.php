<script language="JavaScript" type="text/javascript">
    var submitClicked = false;
    <?php if ($this->FormType != 'Search') : ?>
    AlertAfterChange = true;
    <?php endif; ?>

    function riskGradeTracker(recordid)
    {
        var buttons = new Array();
        buttons[0] = {"value":"<?php echo _tk('run_report')?>","onclick":"document.riskGradeTrackerForm.submit()"};
        buttons[1] = {"value":"<?php echo _tk('btn_cancel')?>","onclick":"GetFloatingDiv(\'riskGradeTracker\').CloseFloatingControl();"};
        PopupDivFromURL("riskGradeTracker","<?php echo _tk('risk_grade_tracker_report')?>", scripturl+"?action=httprequest&type=riskgradetracker&recordid="+recordid, "", buttons, "350px");
        createCalendar(jQuery('.date'), "<?php echo $this->CALENDAR_ONCLICK; ?>", "<?php echo $this->date_fmt; ?>", <?php echo $this->WEEK_START_DAY; ?>, <?php echo $this->CALENDAR_WEEKEND; ?>);
    }
</script>
<form method="post" id="RAMform" name="RAMform" <?php echo (($this->FormType != 'Print' && $this->FormType != 'ReadOnly') ? 'enctype="multipart/form-data"' : ''); ?> action="<?php echo $this->scripturl; ?>?action=<?php echo ($this->FormType == 'Search' ? 'risksdoselection' : 'saverisk'); ?>" onsubmit="<?php echo $this->onSubmit; ?>">
    <input type="hidden" name="recordid" value="<?php echo Sanitize::SanitizeInt($this->ram['recordid']); ?>" />
    <input type="hidden" id="form-panel" name="panel" value="<?php echo ($this->panel ?: ''); ?>" />
    <input type="hidden" name="fromsearch" value="<?php echo Sanitize::SanitizeInt($this->fromsearch); ?>" />
    <input type="hidden" name="fromlisting" value="<?php echo Escape::EscapeEntities($this->fromlisting); ?>" />
    <input type="hidden" name="from_report" value="<?php echo Escape::EscapeEntities($this->from_report); ?>" />
    <input type="hidden" name="updateid" value="<?php echo Escape::EscapeEntities($this->ram['updateid']); ?>" />
    <input type="hidden" name="formname" value="risk2" />
    <input type="hidden" name="ramrep_recordid" value="<?php echo Sanitize::SanitizeInt($this->ram['ramrep_recordid']); ?>" />
    <input type="hidden" name="assurance_framework_clicked" id="assurance_framework_clicked" value="0" />
    <input type="hidden" name="session_form" value="<?php echo Escape::EscapeEntities($this->session_form); ?>" />
    <input type="hidden" name="session_date" value="<?php echo Escape::EscapeEntities($this->lastsession); ?>" />
    <input type="hidden" name="module" value="<?php echo Sanitize::getModule($this->module); ?>" />
    <input type="hidden" name="form_id" value="<?php echo Sanitize::SanitizeInt($this->FormDesign->ID); ?>" />
    <input type="hidden" name="rep_approved_old" value="<?php echo Escape::EscapeEntities($this->ram['rep_approved_old'] ? $this->ram['rep_approved_old'] : ($this->ram['rep_approved'] ? $this->ram['rep_approved'] : 'NEW')); ?>" />
    <input type="hidden" name="qbe_recordid" value="<?php echo Sanitize::SanitizeInt($this->qbe_recordid); ?>" />
    <input type="hidden" name="qbe_return_module" value="<?php echo Escape::EscapeEntities($this->qbe_return_module); ?>" />
    <input type="hidden" name="formlevel" value="<?php echo Sanitize::SanitizeInt($this->formlevel); ?>" />
    <?php if ($this->formlevel == 1) : ?>
    <input type="hidden" name="holding_form" value="1" />
    <?php endif; ?>
    <?php if ($this->sLockMessage) : ?>
    <div class="lock_message">
        <div id="LockMessage"><?php echo $this->sLockMessage; ?></div>
        <div id="userLockMessage"></div>
    </div>
    <?php endif; ?>
    <?php echo $this->RamTable->GetFormTable(); ?>
    <input type="hidden" id="printAfterSubmit" name="printAfterSubmit" value="0" />
    <input type="hidden" id="rbWhat" name="rbWhat" value="Save">
    <?php echo $this->ButtonGroup->getHTML(); ?>
</form>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect($this->FormDesign->Show_all_section, $this->panel, $this->RamTable); ?>
<?php endif; ?>