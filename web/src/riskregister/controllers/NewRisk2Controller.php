<?php

namespace src\riskregister\controllers;

use src\framework\controller\Controller;

class NewRisk2Controller extends Controller
{
    function newrisk2()
    {
        $this->call('src\riskregister\controllers\ShowRiskFormTemplateController', 'showriskform', array());
    }
}