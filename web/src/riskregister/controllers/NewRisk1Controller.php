<?php

namespace src\riskregister\controllers;

use src\framework\controller\Controller;

class NewRisk1Controller extends Controller
{
    function newrisk1()
    {
        $FormId = $this->request->getParameter('form_id');

        if (isset($FormId))
        {
            if (FormIDExists($FormId, 'RAM'))
            {
                $_SESSION['form_id']['RAM'][1] = $FormId;
            }
            else
            {
                $IncorrectFormID = true;
            }
        }

        if ($IncorrectFormID)
        {
            fatal_error('There is no form with the specified ID.');
        }
        else
        {
            $ReporterArray = SetUpDefaultReporter();

            if (!empty($ReporterArray))
            {
                $ram['con']['R'][] = $ReporterArray;
            }

            $this->request->setParameter('module', 'RAM');

            $this->call('src\riskregister\controllers\ShowRiskFormTemplateController', 'showriskform',
                array(
                    'level' => 1,
                    'data' => $ram
                ));
        }
    }
}