<?php

namespace src\riskregister\controllers;

use src\framework\controller\TemplateController;
use src\reasons\controllers\ReasonsController;

class ShowRiskFormTemplateController extends TemplateController
{
    function showriskform()
    {
        global $FormType, $formlevel, $ModuleDefs, $JSFunctions;

        if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && ($FormType != "Print" && $this->request->getParameter('print') != 1))
        {
            $this->addJs('src/generic/js/panelData.js');
        }

        if($FormType == "Print" || $this->request->getParameter('print') == 1)
        {
            $this->addJs('src/generic/js/print.js');
        }

        $ram = $this->request->getParameter('data');
        $form_action = $this->request->getParameter('form_action') ?: '';
        $level = $this->request->getParameter('level') ?: 2;

        if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && $form_action != 'search')
        {
            require_once 'Source/security/SecurityBase.php';
            $recordid = (int) $this->request->getParameter('recordid');
            $RISKPerms = GetUserHighestAccessLvlForRecord('RISK_PERMS', 'RAM', $recordid);
        }
        else
        {
            $RISKPerms = GetParm('RISK_PERMS');
        }

        $LoggedIn = isset($_SESSION['logged_in']);

        $formlevel = $level;

        $FormType = 'Edit'; // Default, overwrite as required

        SetUpFormTypeAndApproval('RAM', $ram, $FormType, $form_action, $RISKPerms);

        CheckForRecordLocks('RAM', $ram, $FormType, $sLockMessage);

        if ($ram['rep_approved'] == 'REJECT' && bYN(GetParm("REJECT_REASON",'Y')))
        {
            $ram = ReasonsController::GetReasonsData('RAM', $ram['recordid'], $ram, $FormType);
        }

        // Load RISK2 form settings
        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'RAM',
            'level' => $level,
            'form_type' => $FormType
        ));

        $_SESSION['LASTUSEDFORMDESIGN'] = $FormDesign->GetFilename();
        $_SESSION['LASTUSEDFORMDESIGNFORMLEVEL'] = $formlevel;

        if ($LoggedIn && $this->request->getParameter('action') != 'newrisk1' &&
            $this->request->getParameter('action') != 'newrisk2')
        {
            $FormDesign->UnsetDefaultValues();
        }

        SetUpApprovalArrays('RAM', $ram, $CurrentApproveObj, $ApproveObj, $FormType, $FormDesign, '', $RISKPerms);

        $notsaved = ($this->request->getParameter('action') == 'newrisk2' || $this->request->getParameter('action') == 'risksearch');

        require_once($ModuleDefs['RAM']['BASIC_FORM_FILES'][$level]);
        require_once 'Source/risks/MainRisk.php';

        $RamTable = New \FormTable($FormType, 'RAM', $FormDesign);
        $RamTable->MakeForm($FormArray, $ram, 'RAM');

        $this->sendToJs(array(
                'FormType' => $FormType,
                'printSections' => $RamTable->printSections)
        );

        $FormDesign->FormTitle .= GetRejectedSuffix($ram, 'RAM', $ModuleDefs['RAM']['FIELD_NAMES']['HANDLER'], $ram['rea_con_name']);

        $title_suffix = $this->getTitleSuffix('RAM', $ram, !empty($FormDesign->FormTitle));

        $this->title = $FormDesign->FormTitle;
        $this->recordInfo = $title_suffix;
        $this->subtitle = $FormDesign->FormTitleDescr;
        $this->module = 'RAM';

        $ButtonGroup = new \ButtonGroup();
        $ButtonGroup->PopulateWithStandardFormButtons(array(
            'module' => 'RAM',
            'formtype' => $FormType,
            'data'=> $ram,
            'form_id' => 'RAMform',
            'level' => $level
        ));

        if ($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) &&!empty($_SESSION['RAM']['RECORDLIST']) && (!isset($ModuleDefs['RAM']['NO_NAV_ARROWS']) || $ModuleDefs['RAM']['NO_NAV_ARROWS'] != true))
        {
            $CurrentIndex = $_SESSION['RAM']['RECORDLIST']->getRecordIndex(array('recordid' => $ram['recordid']));

            if ($CurrentIndex !== false)
            {
                $ButtonGroup->AddNavigationButtons($_SESSION['RAM']['RECORDLIST'], $ram['recordid'], 'RAM');
            }
        }

        // If we are in "Print" mode, we don't want to display the menu
        if ($FormType != 'Print' && $LoggedIn)
        {
            $this->menuParameters = array(
                'table' => $RamTable,
                'buttons' => $ButtonGroup
            );
        }
        else
        {
            $this->hasMenu = false;
        }

        if ($FormType != 'Print' && $FormType != 'Search')
        {
            $StatusesNoMandatory = GetStatusesNoMandatory(
                array(
                    'module' => 'RAM',
                    'level' => $RISKPerms,
                    'from' => $ram['rep_approved']
                )
            );

            if (is_array($StatusesNoMandatory) && !empty($StatusesNoMandatory))
            {
                $JSFunctions[] = 'StatusesNoMandatory = [\''.implode("','",$StatusesNoMandatory).'\'];';
            }

            $JSFunctions[] = MakeJavaScriptValidation('RAM', $FormDesign);
        }

        $onSubmit = 'return(submitClicked';

        if ($FormType != 'Print' && $FormType != 'Search' && $FormType != 'ReadOnly')
        {
            $onSubmit.= ' && validateOnSubmit()';

            SetFormSession($session_form, $lastsession);
        }

        $onSubmit.= ')';

        $RamTable->MakeTable();

        if (GetParm('FMT_DATE_WEB') == 'US')
        {
            $date_fmt= 'mm/dd/yy';
        }
        else
        {
            $date_fmt = 'dd/mm/yy';
        }

        $this->response->build('src/riskregister/views/ShowRiskForm.php', array(
            'FormType' => $FormType,
            'QueryString' => urlencode($_SERVER['QUERY_STRING']),
            'onSubmit' => $onSubmit,
            'ram' => $ram,
            'fromsearch' => $this->request->getParameter('fromsearch'),
            'fromlisting' => $this->request->getParameter('fromlisting'),
        	'from_report' => $this->request->getParameter('from_report'),
            'session_form' => $session_form,
            'lastsession' => $lastsession,
            'module' => $this->request->getParameter('module'),
            'FormDesign' => $FormDesign,
            'qbe_recordid' => $this->request->getParameter('qbe_recordid'),
            'qbe_return_module' => $this->request->getParameter('qbe_return_module'),
            'formlevel' => $formlevel,
            'sLockMessage' => $sLockMessage,
            'RamTable' => $RamTable,
            'ButtonGroup' => $ButtonGroup,
            'panel' => $this->request->getParameter('panel'),
            'CALENDAR_ONCLICK' => GetParm('CALENDAR_ONCLICK', 'N'),
            'date_fmt' => $date_fmt,
            'CALENDAR_WEEKEND' => GetParm('CALENDAR_WEEKEND', 1),
            'WEEK_START_DAY' => GetParm('WEEK_START_DAY', 2)
        ));
    }
}