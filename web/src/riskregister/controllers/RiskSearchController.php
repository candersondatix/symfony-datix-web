<?php

namespace src\riskregister\controllers;

use src\framework\controller\Controller;

class RiskSearchController extends Controller
{
    function risksearch()
    {
        if ($this->request->getParameter('validationerror'))
        {
            $ram['error']['message'] = '<br />There are invalid characters in one of the fields you have searched on.';
        }
        else
        {
            $_SESSION['RAM']['UDFARRAY'] = NULL;
        }

        if ($this->request->getParameter('searchtype') == 'lastsearch')
        {
            $ram = $_SESSION['RAM']['LAST_SEARCH'];
        }
        else
        {
            $ram = '';
        }

        $_SESSION['RAM']['PROMPT']['VALUES'] = array();

        $this->call('src\riskregister\controllers\ShowRiskFormTemplateController', 'showriskform',
            array(
                'form_action' => 'search',
                'data' => $ram
            )
        );
    }
}