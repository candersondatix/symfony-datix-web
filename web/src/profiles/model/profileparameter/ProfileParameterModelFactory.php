<?php
namespace src\profiles\model\profileparameter;

use src\framework\model\ModelFactory;

class ProfileParameterModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ProfileParameterFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ProfileParameterMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ProfileParameterCollection($this->getMapper(), $this->getEntityFactory());
    }
}