<?php
namespace src\profiles\model\profileparameter;

use src\framework\model\EntityCollection;

class ProfileParameterCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\profiles\\model\\profileparameter\\ProfileParameter';
    }
}