<?php
namespace src\profiles\model\profile;

use src\framework\model\EntityCollection;

class ProfileCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\profiles\\model\\profile\\Profile';
    }
}