<?php
namespace src\profiles\model\profile;

use src\framework\model\DatixEntity;

use src\profiles\model\profileparameter\ProfileParameterModelFactory;
use src\security\groups\model\GroupModelFactory;
use src\framework\model\exceptions\UndefinedPropertyException;

/**
 * This entity models a user who can log in to the system
 */
class Profile extends DatixEntity
{
    /**
     * A human-readable name for the profile
     *
     * @var string
     */
    protected $pfl_name;

    /**
     * The initials of the user who was logged in when the profile was created
     *
     * @var string
     */
    protected $createdby;

    /**
     * The order of precedence to use when a user is linked to multiple profiles in the AD and an LDAP
     * sync is performed. Needed because a user is limited to a single profile in Datix.
     *
     * @var int
     */
    protected $pfl_ldap_order;

    /**
     * Text field containing a description of the profile.
     *
     * @var string
     */
    protected $pfl_description;

    /**
     *  Collection of configuration parameters (from link_profile_params table)
     *
     * @var ProfileParameterCollection
     */
    protected $configuration_parameters;

    /**
     *  Collection of configuration parameters (from link_profile_params table)
     *
     * @var GroupCollection
     */
    protected $groups;

    /**
     * {@inheritdoc}
     */
    public function getLinkedConfigurationParameters()
    {
        if($this->recordid == '')
        {
            throw new UndefinedPropertyException('Cannot get profile configuration settings without profile ID.');
        }

        if (!isset($this->configuration_parameters))
        {
            $factory = new ProfileParameterModelFactory();
            list($query, $where, $fields) = $factory->getQueryFactory()->getQueryObjects();
            $where->add($fields->field('link_profile_param.lpp_profile')->eq($this->recordid));
            $query->where($where);

            $this->configuration_parameters = $factory->getCollection();
            $this->configuration_parameters->setQuery($query);
        }

        return $this->configuration_parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getLinkedGroups()
    {
        if($this->recordid == '')
        {
            throw new UndefinedPropertyException('Cannot get linked security groups without profile ID.');
        }

        if (!isset($this->groups))
        {
            $groupIds = \DatixDBQuery::PDO_fetch_all('SELECT lpg_group FROM link_profile_group WHERE lpg_profile = :recordid', array('recordid' => $this->recordid), \PDO::FETCH_COLUMN);

            $factory = new GroupModelFactory();
            list($query, $where, $fields) = $factory->getQueryFactory()->getQueryObjects();
            
            if(empty($groupIds))
            {
                return null;
            }
            
            $where->add($fields->field('sec_groups.recordid')->in($groupIds));
            $query->where($where);

            $this->groups = $factory->getCollection();
            $this->groups->setQuery($query);
        }

        return $this->groups;
    }
}