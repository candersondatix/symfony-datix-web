<?php
namespace src\profiles\observers;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\framework\model\Entity;
use src\profiles\model\profile\Profile;

class ProfileDeletionAuditor implements Observer
{
    /**
     * {@inheritdoc}
     *
     * Audits the deletion of a profile
     *
     * @throws \InvalidArgumentException If the subject is not a Profile.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof Profile))
        {
            throw new \InvalidArgumentException('Object of type Profile expected, '.get_class($subject).' given.');
        }

        if ($event != Entity::EVENT_DELETE)
        {
            // this observer only listens to delete events
            return;
        }

        InsertFullAuditRecord('ADM', $subject->recordid, 'DELETE', 'Profile deleted: ' . $subject->pfl_name);
    }
}