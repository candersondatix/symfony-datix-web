<?php
namespace src\profiles\observers;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\framework\model\Entity;
use src\profiles\model\profile\Profile;

class ProfileDeletionCleaner implements Observer
{
    /**
     * {@inheritdoc}
     *
     * Removes orphan data when deleting a profile
     *
     * @throws InvalidArgumentException If the subject is not a Profile.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof Profile))
        {
            throw new \InvalidArgumentException('Object of type Profile expected, '.get_class($subject).' given.');
        }

        if ($event != Entity::EVENT_DELETE)
        {
            // this observer only listens to delete events
            return;
        }

        //delete linked configuration parameter data
        \DatixDBQuery::PDO_query('DELETE FROM link_profile_param WHERE lpp_profile = :recordid', array('recordid' => $subject->recordid));

        //delete linked security group data
        \DatixDBQuery::PDO_query('DELETE FROM link_profile_group WHERE lpg_profile = :recordid', array('recordid' => $subject->recordid));

        //make sure no one is linked to this profile
        \DatixDBQuery::PDO_query('UPDATE contacts_main SET sta_profile = :sta_profile WHERE sta_profile = :recordid', array('recordid' => $subject->recordid, 'sta_profile' => null));
    }
}