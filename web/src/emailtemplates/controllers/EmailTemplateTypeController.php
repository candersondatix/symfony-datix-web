<?php
namespace src\emailtemplates\controllers;

use src\framework\controller\Controller;

class EmailTemplateTypeController extends Controller
{
    public function gettemplatetypes()
    {
        require_once 'Source/libs/EmailTemplates.php';
        $ConfigArray = include_config_array();

        if ($this->request->getParameter('module') && is_array($ConfigArray[$this->request->getParameter('module')]))
        {
            foreach ($ConfigArray[$this->request->getParameter('module')] as $global => $aDetails)
            {
                $TypeArray[$global] = $aDetails["title"];
            }
        }
        $this->response->build('src/emailtemplates/views/EmailTemplateTypeView.php', array('TypeArray' => $TypeArray));
    }
}