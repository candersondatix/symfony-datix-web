<?php
namespace src\emailtemplates\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\framework\query\Query;

/**
 * Manages the persistance of EmailTemplate objects.
 */
class EmailTemplateMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\emailtemplates\\model\\EmailTemplate';
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return 'email_templates';
    }
    
    /**
     * {@inheritdoc}
     * 
     * Strips the XML tags from the subject/text fields.
     */
    public function select(Query $query)
    {
        $result = parent::select($query);
        foreach ($result as $key => $record)
        {
            $result[$key]['emt_subject'] = $this->stripXmlTags($record['emt_subject']);
            $result[$key]['emt_text']    = $this->stripXmlTags($record['emt_text']);
        }
        return $result;
    }
    
    /**
     * {@inheritdoc}
     * 
     * Add XML tags to subject/text fields.
     */
    protected function doInsert(Entity $object)
    {
        $this->addTags($object);
        return parent::doInsert($object);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function doUpdate(Entity $object)
    {
        $this->addTags($object);
        return parent::doUpdate($object);
    }
    
    /**
     * Adds XML tags to subject/text properties.
     * 
     * @param EmailTemplate $object
     */
    protected function addTags(EmailTemplate $object)
    {
        $object->emt_subject = $this->addXmlTags($object->emt_subject);
        $object->emt_text    = $this->addXmlTags($object->emt_text);
    }
    
    /**
     * Subject/text data is persisted wrapped with XML tags, which need to be removed.
     * 
     * NB - I'm not sure why the data is persisted in this way - is this something we can do away with?
     * 
     * @param string $str
     * 
     * @return string $str
     */
    protected function stripXmlTags($str)
    {
        if (\UnicodeString::strpos($str, '<xml>') === 0)
        {
            $str = \UnicodeString::substr($str, 5, \UnicodeString::strlen($str) - 11);
        }
        return $str;
    }
    
    /**
     * Wraps a given string with xml tags.
     * 
     * @param string $str
     * 
     * @return string
     */
    protected function addXmlTags($str)
    {
        return '<xml>'.$str.'</xml>';
    }
}