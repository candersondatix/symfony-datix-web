<?php
namespace src\emailtemplates\model;

use src\framework\model\DatixEntity;

/**
 * Email templates allow users to customise the content of system emails.
 */
class EmailTemplate extends DatixEntity 
{
    /**
     * The module the template's attached to.
     * 
     * @var string
     */
    protected $emt_module;
    
    /**
     * A short description of the template.
     * 
     * @var string
     */
    protected $emt_name;
    
    /**
     * The email type (e.g. notification).
     * 
     * @var string
     */
    protected $emt_type;
    
    /**
     * A long description of the template.
     * 
     * @var string
     */
    protected $emt_notes;
    
    /**
     * The email subject line.
     * 
     * @var string
     */
    protected $emt_subject;
    
    /**
     * The email body.
     * 
     * @var string
     */
    protected $emt_text;
    
    /**
     * The email content type (e.g. text/html)
     * 
     * @var string
     */
    protected $emt_content_type;
    
    /**
     * The creator of the template.
     * 
     * @var string
     */
    protected $emt_author;

}
