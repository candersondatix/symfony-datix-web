<?php
namespace src\emailtemplates\model;

use src\framework\model\EntityFactory;

class EmailTemplateFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\emailtemplates\\model\\EmailTemplate';
    }
}