<li>
    <ol>
        <?php foreach($this->HistoryRecords as $HistoryRecord) : ?>
        <?php echo GetDivFieldHTML(GetFieldLabel('lcom_dreceived', 'Date Received'), FormatDateVal($HistoryRecord['lcom_dreceived'])); ?>
        <?php echo GetDivFieldHTML(GetFieldLabel('lcom_dreopened', 'Re-opened'), FormatDateVal($HistoryRecord['lcom_dreopened'])); ?>
        <li>
            <table class="datatable" cellspacing="0">
                <tr>
                    <td></td>
                    <td><b><?php echo _tk('com_chain_due'); ?></b></td>
                    <td><b><?php echo _tk('com_chain_done'); ?></b></td>
                </tr>
                <?php if (bYN(GetParm("SHOW_ACKNOWLEDGED",'Y'))) : ?>
                <tr>
                    <td><b><?php echo _tk('com_chain_acknowledged'); ?>:</b></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_ddueack']); ?></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_dack']); ?></td>
                </tr>
                <?php endif; ?>
                <?php if (bYN(GetParm("SHOW_ACTIONED",'Y'))) : ?>
                <tr>
                    <td><b><?php echo _tk('com_chain_actioned'); ?>:</b></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_ddueact']); ?></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_dactioned']); ?></td>
                </tr>
                <?php endif; ?>
                <?php if (bYN(GetParm("SHOW_RESPONSE",'Y'))) : ?>
                <tr>
                    <td><b><?php echo _tk('com_chain_response'); ?>:</b></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_ddueresp']); ?></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_dresponse']); ?></td>
                </tr>
                <?php endif; ?>
                <?php if (bYN(GetParm("SHOW_HOLDING",'Y'))) : ?>
                <tr>
                    <td><b><?php echo _tk('com_chain_holding'); ?>:</b></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_dduehold']); ?></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_dholding']); ?></td>
                </tr>
                <?php endif; ?>
                <?php if (bYN(GetParm("SHOW_REPLIED",'Y'))) : ?>
                <tr>
                    <td><b><?php echo _tk('com_chain_replied'); ?>:</b></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_dduerepl']); ?></td>
                    <td><?php echo FormatDateVal($HistoryRecord['lcom_dreplied']); ?></td>
                </tr>
                <?php endif; ?>
            </table>
        </li>
        <?php endforeach; ?>
    </ol>
</li>