<?php
namespace src\policies\model;

use src\framework\model\EntityCollection;

class PolicyCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\policies\\model\\Policy';
    }
}