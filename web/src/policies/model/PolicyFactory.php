<?php
namespace src\policies\model;

use src\framework\model\EntityFactory;

use src\profiles\observers\ProfileDeletionAuditor;
use src\profiles\observers\ProfileDeletionCleaner;

/**
 * Constructs CodeTagGroup objects.
 */
class PolicyFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\policies\\model\\Policy';
    }

}