<?php
namespace src\policies\controllers;

use src\framework\controller\TemplateController;
use src\framework\query\Query;
use src\framework\query\Where;
use src\framework\query\WhereFactory;
use src\framework\query\FieldCollection;
use src\policies\model\Policy;
use src\policies\model\PolicyModelFactory;

class PoliciesTemplateController extends TemplateController
{
    /**
     * Displays the form used to create links to policy records.
     */
    public function displayPolicyLinkForm()
    {
        $this->module = 'POL';
        $this->title  = _tk('link_a_policy_to_a_respondent');
        
        $this->addJs('src/policies/js/DisplayPolicyLinkForm.js');
        
        list($main_recordid, $link_recordid, $type, $org_id) = $this->getPolicyLinkInfo();
        
        $policy_id = (int) $this->request->getParameter('policy_id');
        
        if ($policy_id > 0)
        {
            $policy = (new PolicyModelFactory)->getMapper()->find($policy_id);
            $policyIsValid = $policy->TestPolicyBasisDate($policy->start_date,$policy->end_date,$policy->policy_basis, $main_recordid);
            if ($policy === null)
            {
                throw new \URLNotFoundException();
            }
            else
            {
                $FormType = 'ReadOnly';
                $data = $policy->getVars();
            }
        }
        else
        {
            $FormType = 'Search';
            $data = [];
        }
        
        $formDesign = \Forms_FormDesign::GetFormDesign(['module' => 'POL', 'form_type' => $formMode, 'level' => 2]);
        $formDesign->LoadFormDesignIntoGlobals();

        require 'Source/generic_modules/POL/BasicForm2.php';

        $form = new \FormTable($FormType, 'POL', $formDesign);
        $form->MakeForm($FormArray, $data, 'POL');
        $form->MakeTable();

        //Begin generating floating menu
        $level = $this->request->getParameter('level') ?: '2';
        $loggedIn = isset($_SESSION['logged_in']);

        $buttonGroup = new \ButtonGroup();

        $buttonGroup->AddButton(array('label' => _tk('check_for_matching_policies'), 'id' => 'check_for_matching_policies', 'onclick' => 'checkForMatchingPolicies()', 'action' => 'SEARCH'));
        $buttonGroup->AddButton(array('label' => _tk('btn_cancel'), 'id' => 'cancel', 'onclick' => 'policyCancelLinking()', 'action' => 'BACK'));

        // If we are in "Print" mode, we don't want to display the menu
        if ($loggedIn && !in_array($FormType, ['Print', 'ReadOnly']))
        {
            $this->menuParameters = array(
                'module' => 'POL',
                'table' => $form,
                'buttons' => $buttonGroup,
                'recordid' => $data['recordid']
            );
        }
        else
        {
            $this->hasMenu = false;
        }
        //End generating floating menu

        if($org_id == null)
        {
            $org_id = 0;
        }
        $this->response->build('src/policies/views/DisplayPolicyLinkForm.php', array(
            'form'          => $form,
            'policy_id'     => $policy_id,
            'link_recordid' => $link_recordid,
            'main_recordid' => $main_recordid,
            'policyIsValid' => $policyIsValid,
            'type'          => $type,
            'org_id'        => $org_id,
            'module'        => 'CLA'
        ));
    }
    
    /**
     * Controls actions on Policy links (creating/deleting).
     * 
     * @throws \Exception If the request parameters are invalid.
     */
    public function editPolicyLink()
    {
        $policy_id = (int) $this->request->getParameter('policy_id');
        $action    = $this->request->getParameter('link_action');
        $factory   = new PolicyModelFactory();
        
        list($main_recordid, $link_recordid, $type, $org_id) = $this->getPolicyLinkInfo();
        
        if ($factory->getMapper()->find($policy_id) === null)
        {
            // either the policy doesn't exist or this user does not have access via their security setup
            throw new \Exception('Invalid policy id');
        }
        
        switch ($action)
        {
            case 'create':
                (new PolicyModelFactory)->getMapper()->insertPolicyRespondentLink($policy_id, $link_recordid);
                AddSessionMessage('INFO', _tk('policy_link_created'));
                break;
                
            case 'delete':
                (new PolicyModelFactory)->getMapper()->deletePolicyRespondentLink($policy_id, $link_recordid);
                AddSessionMessage('INFO', _tk('policy_link_deleted'));
                break;
                
            default:
                throw new \Exception('Invalid action');
        }
        
        switch ($type)
        {
            case 'ORG':
                $url = 'app.php?action=editorganisationlink&module=CLA&recordid='.$org_id.'&main_recordid='.$main_recordid.'&link_recordid='.$link_recordid.'&panel=policies';
                break;
                
            case 'CON':
                $url = 'app.php?action=linkcontactgeneral&module=CLA&link_recordid='.$link_recordid.'&link_type=O&main_recordid='.$main_recordid.'&panel=policies';
                break;
                
            default:
                $url = 'index.php';
        }
        
        $this->redirect($url);
    }
    
    /**
     * Displays the listing used to link existing policies to a respondent.
     */
    public function listMatchingPolicies()
    {
        $this->hasMenu    = false;
        $this->hasFooter  = false;
        $this->hasPadding = false;
        $this->hasHeader  = false;
        $this->title      = _tk('select_a_policy');
        
        list($main_recordid, $link_recordid, $type, $org_id) = $this->getPolicyLinkInfo();
        
        // fetch the Where object created by DoSelectionController::doselection()
        $where = $_SESSION['POL']['NEW_WHERE'];
        if (!($where instanceof Where))
        {
            // we're running a blank search
            $where = new Where();
        }
        
        // construct the condition to hide policies already linked to this respondent
        $where->add(
            (new FieldCollection)->field('policies.recordid')->notIn(
                (new Query)->select(['policy_defendant_links.policy'])->where(['policy_defendant_links.defendant_link_id' => $link_recordid])
            )
        );
        
        $policies = (new PolicyModelFactory)->getCollection();
        $policies->setQuery((new Query)->where($where));
        
        // construct the listing
        $recordList = new \RecordLists_ModuleRecordList();
        $recordList->Module = 'POL';
        $recordList->AddRecordData($policies);

        $listingDesign = new \Listings_ModuleListingDesign(array('module' => 'POL'));
        $listingDesign->LoadColumnsFromDB();
        
        $chooseColumn = new \Fields_DummyField(['name' => 'choose', 'label' => _tk('choose'), 'type' => 'X', 'width' => '1']);
        $chooseColumn->setControllerAction('src\\policies\\controllers\\PoliciesController', 'choosePolicyButton');
        
        $listingDesign->AddAdditionalColumns([$chooseColumn], \Listings_ListingDesign::PREPEND);

        $listing = new \Listings_ListingDisplay($recordList, $listingDesign);
        
        $listing->ReadOnly = true;
        $this->response->build('src/policies/views/ListMatchingPolicies.php', [
            'listing'       => $listing,
            'main_recordid' => $main_recordid,
            'link_recordid' => $link_recordid,
            'type'          => $type,
            'org_id'        => $org_id,
            'token'         => \CSRFGuard::getCurrentToken(),
            'module'        => 'CLA'
        ]);
    }
    
    /**
     * Validates, sanitises, and returns the relevant link info required from the request when managing policy-respondent links.
     * 
     * @throws \Exception If the link info contained in the request is invalid.
     */
    protected function getPolicyLinkInfo()
    {
        // this is the claim recordid
        $main_recordid = (int) $this->request->getParameter('main_recordid');
        if (!($main_recordid > 0))
        {
            throw new \Exception('Invalid main_recordid');
        }
        
        // this is the respondent link id
        $link_recordid = (int) $this->request->getParameter('link_recordid');
        if (!($link_recordid > 0))
        {
            throw new \Exception('Invalid link_recordid');
        }
        
        // this is the respondent type
        $type = $this->request->getParameter('type');
        if (!in_array($type, ['CON','ORG']))
        {
            throw new \Exception('Invalid type');
        }
        
        if ($type == 'ORG')
        {
            // this is the organisation id, required to go back to the respondent form
            $org_id = (int) $this->request->getParameter('org_id');
            if ($type == 'ORG' && !($org_id > 0))
            {
                throw new \Exception('Invalid org_id');
            }
        }
        
        return [$main_recordid, $link_recordid, $type, $org_id];
    }
}
