<?php
namespace src\policies\controllers;

use src\framework\controller\Controller;
use src\framework\query\QueryFactory;
use src\policies\model\PolicyModelFactory;
use src\policies\model\Policy;

class PoliciesController extends Controller
{
    /**
     * Outputs a list of policies linked to a respondent.
     */
    public function listPoliciesForRespondent()
    {
        $link_recordid = (int) $this->request->getParameter('link_recordid');
        $main_recordid = (int) $this->request->getParameter('main_recordid');
        $org_id        = (int) $this->request->getParameter('recordid'); // only relevant if we're on an organisation form
        
        switch ($this->request->getParameter('action'))
        {
            case 'editorganisationlink':
                $type = 'ORG';
                break;
                
            case 'linkcontactgeneral':
                $type = 'CON';
                break;
        
            default:
                $this->registry->getLogger()->logEmergency('Error rendering policies listing: unable to determine respondent type from action '.$this->request->getParameter('action'));
                $this->response->setBody('Error: Unable to determine respondent type');
                return;
        }
        
        list($query, $where, $fc) = (new QueryFactory)->getQueryObjects();
        
        $fc->field('policies.recordid')->eq('policy_defendant_links.policy', true);
        
        $where->add($fc);
        
        $query->join('policy_defendant_links', $where);
        $query->where(array('policy_defendant_links.defendant_link_id' => $link_recordid));

        $invalidPoliciesTotal = 0;
        $policies = (new PolicyModelFactory)->getCollection();
        $policies->setQuery($query);

        foreach ($policies as $pol)
        {
            if ($pol->TestPolicyBasisDate($pol->start_date, $pol->end_date, $pol->policy_basis, $main_recordid) > 0)
            {
                $invalidPoliciesTotal += 1;
            }
        }

        $recordList = new \RecordLists_ModuleRecordList();
        $recordList->Module = 'POL';
        $recordList->Paging = false;
        $recordList->AddRecordData($policies);

        $listingDesign = new \Listings_ModuleListingDesign(array('module' => 'POL', 'parent_module' => $type, 'link_type' => 'policies'));
        $listingDesign->LoadColumnsFromDB();

        $chooseColumn = new \Fields_DummyField(array('name' => 'inaccurateIcon', 'label' =>'', 'type' => 'X', 'width' => '1'));
        $chooseColumn->setControllerAction('src\\policies\\controllers\\PoliciesController', 'InaccuratePolicyIcon', array('main_recordid' => $main_recordid));

        $listingDesign->AddAdditionalColumns([$chooseColumn], \Listings_ListingDesign::PREPEND);

        $listing = new \Listings_ListingDisplay($recordList, $listingDesign);
        $listing->URLParameterArray = [
            'link_recordid' => $link_recordid,
            'main_recordid' => $main_recordid,
            'org_id'        => $org_id,
            'type'          => $type,
        ];

        $userCanAddRecords = $_SESSION['CurrentUser']->canAddRecords('POL');

        $this->response->build('src/policies/views/ListPoliciesForRespondent.php', array(
            'listing'            => $listing,
            'link_recordid'      => $link_recordid,
            'main_recordid'      => $main_recordid,
            'hasInvalidPolicies' => $invalidPoliciesTotal > 0,
            'org_id'             => $org_id,
            'type'               => $type,
            'module'             => 'CLA',
            'userCanAddRecords'  => $userCanAddRecords
        ));
    }
    
    /**
     * Renders the choose button on the "check for matching policies" listing.
     */
    public function choosePolicyButton()
    {
        $policy = $this->request->getParameter('data');
        if (!($policy instanceof Policy))
        {
            throw new \Exception('Policy entity not found');
        }
        
        $this->response->setBody('<input type="button" onclick="choosePolicy('.$policy->recordid.')" value="'._tk('choose').'" />');
    }
    
    public function InaccuratePolicyIcon($main_recordid)
    {
        $policy = $this->request->getParameter('data');
        if (!($policy instanceof Policy))
        {
            throw new \Exception('Policy entity not found');
        }
        if ($policy->TestPolicyBasisDate($policy->start_date, $policy->end_date, $policy->policy_basis, $this->request->getParameter('additional_data')['main_recordid']) > 0)
        {
            $this->response->setBody('<img src="Images/icons/icon_duetoday20x20.png"');
        }
    }
    
    /**
     * Renders the linked claims section of the Policy form.
     */
    public function listClaimsForPolicy()
    {
        $policy_id = (int) $this->request->getParameter('recordid');
        
        // there's no model for claims currently, so we'll just use the policy mapper for now
        $claims = (new PolicyModelFactory)->getMapper()->getLinkedClaims($policy_id);
        
        $recordList = new \RecordLists_ModuleRecordList();
        $recordList->Module = 'CLA';
        $recordList->AddRecordData($claims);

        $listingDesign = new \Listings_ModuleListingDesign(array('module' => 'CLA', 'parent_module' => 'POL', 'link_type' => 'claims'));
        $listingDesign->LoadColumnsFromDB();
        
        $listing = new \Listings_ListingDisplay($recordList, $listingDesign);
        
        $this->response->setBody($listing->GetListingHTML());
    }
}
