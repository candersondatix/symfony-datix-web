<script type="text/javascript">
/**
 * Executes the action of choosing a policy record when checking for matching policies to link to respondents.
 * 
 * @param int id The policy recordid.
 */
function choosePolicy(id)
{
    var url = "?action=displaypolicylinkform&link_recordid=<?php echo $this->link_recordid ?><?php echo ($this->module) ? "&module=" . $this->module . "&" : ""; ?>&policy_id="+id+"&main_recordid=<?php echo $this->main_recordid ?>&type=<?php echo $this->type ?>&org_id=<?php echo $this->org_id ?>";
<?php if ($this->token): ?>
    url = url + '&token=<?php echo $this->token ?>';
<?php endif; ?>

    opener.location.href = url;
    window.close();
}
</script>
<?php echo $this->listing->GetListingHTML() ?>