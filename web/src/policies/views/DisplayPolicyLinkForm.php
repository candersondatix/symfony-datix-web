<form method="POST" id="policy_link_form" name="policy_link_form" action="<?php echo $this->scripturl ?>?action=editpolicylink<?php echo ($this->module) ? "&module=" . $this->module : ""; ?>">
    <input type="hidden" name="policy_id" id="policy_id" value="<?php echo $this->policy_id ?>" />
    <input type="hidden" name="link_recordid" id="link_recordid" value="<?php echo $this->link_recordid ?>" />
    <input type="hidden" name="main_recordid" id="main_recordid" value="<?php echo $this->main_recordid ?>" />
    <input type="hidden" name="policyIsValid" id="policyIsValid" value="<?php echo $this->policyIsValid ?>" />
    <input type="hidden" name="type" id="type" value="<?php echo $this->type ?>" />
    <input type="hidden" name="org_id" id="org_id" value="<?php echo $this->org_id ?>" />
    <input type="hidden" name="link_action" id="link_action" value="create" />
    <?php echo $this->form->GetFormTable(); ?>
    <div class="button_wrapper">
        <?php if ($this->policy_id > 0): ?>
        <input class="button" type="button" value="<?php echo _tk('create_new_link') ?>" id="create_new_link_button">
        <!-- <input class="button" type="button" value="<?php echo _tk('check_for_matching_policies') ?>" onclick="if(confirm('<?=addslashes(_tk('unlink_organisation_confirm'))?>')){ SendTo(scripturl+'?action=unlinkrespondent&main_recordid=<?=$this->main_recordid?>&link_recordid=<?=$this->link_recordid?>'); }"> -->
        <?php else :?>
        <input class="button" type="button" id="check_for_matching_policies" value="<?php echo _tk('check_for_matching_policies') ?>" <?php echo ($this->module) ? "data-module=" . $this->module : ""; ?>>
        <?php endif ?>
        <input class="button" type="button" id="cancel" value="<?php echo _tk('btn_cancel') ?>">
    </div>
</form>
<script type="text/javascript">var confirmMsg1 = "<?php echo _tk('policy_inaccurate1') ?>"; var confirmMsg2 = "<?php echo _tk('policy_inaccurate2') ?>";var confirmMsg3 = "<?php echo _tk('policy_inaccurate3') ?>";</script>
