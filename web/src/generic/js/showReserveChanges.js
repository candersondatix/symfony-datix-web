(function( $ ) {

    $.fn.showReserveChanges = function($reserve) {

        var $obj = $(this),
            $parent = $obj.closest('.field_input_div'),
            $reserveParent = $reserve.closest('.field_input_div');

        $obj.data('originalValueUnformatted', unformatCurrency($obj.val()));

        $obj.on('keyup change blur input', function(e) {

            var originalValue = $obj.data('originalValueUnformatted'),
                reserveValue = unformatCurrency($reserve.val()),
                newValue = unformatCurrency($obj.val()),
                difference = (newValue < originalValue ? '-' : '+') + formatCurrency(Math.abs(originalValue - newValue)),
                newReserve = formatCurrency(reserveValue + (newValue - originalValue));

            if(newValue == originalValue) {

                $parent.find('.warning-msg').hide();
                $reserveParent.find('.warning-msg').hide();
            }
            else {

                if( ! $parent.find('.warning-msg').length) {

                    var $changeMsg = $('<span/>', {
                        'class': "warning-msg",
                        'text': "Change: " + difference
                    });

                    $obj.after($changeMsg);
                }
                else
                {
                    $changeMsg = $parent.find('.warning-msg');

                    $changeMsg.text("Change: " + difference).show();
                }

                if( ! $reserveParent.find('.warning-msg').length) {

                    var $changeMsg = $('<span/>', {
                        'class': "warning-msg",
                        'text': "New value: " + newReserve
                    });

                    $reserve.after($changeMsg);
                }
                else
                {
                    $changeMsg = $reserveParent.find('.warning-msg');

                    $changeMsg.text("New value: " + newReserve).show();
                }
            }
        });

        $obj.data('showReserveChanges', $obj);

        return $obj;
    };

    $(function(){

        $('#fin_indemnity_reserve').showReserveChanges($('#fin_calc_reserve_1'));
        $('#fin_expenses_reserve').showReserveChanges($('#fin_calc_reserve_2'));
    });

}(jQuery));
