function buildPrintDialog() {

    if(globals.printSections.length > 1) {

        var id = 'print-options';
        var printDialog = AddNewFloatingDiv(id);

        var sectionList = '<ul id="print-options">';

        jQuery.each(globals.printSections, function(index, sectionId) {

            var $this = jQuery('#' + sectionId).find('.section_title').first(),
                $parent = $this.closest('.section_div');

            sectionList += '<li id="option-' + sectionId + '-container"><input type="checkbox" id="option-' + index + '" class="section-toggle" ' + ($parent.is(':visible') ? 'checked="checked"' : '') + ' value="' + sectionId + '" /><label for="option-' + index + '">' + $this.text() + '</label></li>';
        });

        sectionList += '</ul>';

        printDialog.setTitle('Print options');
        printDialog.setContents('<div style="margin: 0 0 10px;">Select the form sections you wish to print</div>' +
            '<div style="margin: 0 0 10px; font-weight: bold;"><span  id="num-showing">' + globals.printSections.length + '</span> of ' + globals.printSections.length + ' sections selected for print</div>' +
            '<div style="margin: 0 0 5px;"><label for="searchtext" style="margin: 0 5px 0 0;">Search</label><span style="position: relative; display: inline-block;"><input id="searchtext" type="text" value="" /><span id="clear-search" class="ui-icon ui-icon-close hex666666" title="Clear search">x</span></span></div>' +
            '<div id="toggle-container" style="margin: 0 0 5px;"><input type="checkbox" id="toggle-all" /><label for="toggle-all">Toggle all</label></div>' +
            sectionList);

        buttons  = [];

        buttons[0]={'value':'Print','onclick':'hidePrintDialog(); print(); return false;'};
        buttons[1]={'value':'Close','onclick':'hidePrintDialog(); return false;'};

        printDialog.setButtons(buttons);

        printDialog.hideCloseControl();

        printDialog.display();

        setContentSize(true);

        setMasterCheckState();
    }
}

function setSectionCount() {

    jQuery('#num-showing').text(jQuery('#print-options').find('input:checked').length);
}

function setContentSize(fixPos) {

    if(typeof fixPos == 'undefined') {

        fixPos = false;
    }

    var id = 'print-options',
        printDialog = GetFloatingDiv(id),
        $printOptions = jQuery('#print-options'),
        $floatingWindow = jQuery('#floating_window_print-options'),
        viewportHeight = jQuery(window).height();

    if($floatingWindow.outerHeight() > viewportHeight) {

        var printOptionsHeight = viewportHeight - ($floatingWindow.outerHeight() - $printOptions.outerHeight());

        $printOptions.height(printOptionsHeight).css('overflow', 'auto');

        var scrollbarWidth = $printOptions.width() - $printOptions.find('li').outerWidth(true);

        /**
         * Add the scrollbar width to the width of the options to ensure the content isn't being squished; add 10 to make IE play nicely
         */
        $printOptions.width($printOptions.width() + scrollbarWidth + 10);
    }
    else
    {
        $printOptions.height('').width('').css('overflow', 'visible');
    }

    if(fixPos) {

        printDialog.fixPosition();
    }
}

function resetContentSize(fixPos) {

    jQuery('#print-options').height('').width('').css('overflow', 'visible');
}

function showPrintDialog() {

    var $showprintDialog = jQuery('#show-print-options');

    $showprintDialog.hide();

    buildPrintDialog();
}

function hidePrintDialog() {

    var printDialog = GetFloatingDiv('print-options'),
        $showprintDialog = jQuery('#show-print-options');

    if($showprintDialog.length) {

        $showprintDialog.show();
    }
    else {

        jQuery('#wrapper').prepend('<span id="show-print-options">Show print options</span>');
    }

    if(printDialog) {

        printDialog.CloseFloatingControl();
    }
}

function setMasterCheckState() {

    var $master = jQuery('#toggle-all'),
        $allChecked = jQuery('.section-toggle:not(:checked)'),
        $masterChecked =  jQuery('#toggle-all:checked');

    if( ! $allChecked.length && ! $masterChecked.length) {

        $master.prop('checked', true);
    }
    else if($allChecked.length && $masterChecked.length) {

        $master.prop('checked', false);
    }
}

jQuery(function( $ ) {

    var $body = $('body');

    buildPrintDialog();

    $body.on('click', '#show-print-options', function() {

        showPrintDialog();
    });

    $body.on('click', '#closeControl_print-options', function( e ) {

        e.preventDefault();
        hidePrintDialog();
    });

    $body.on('click', '#toggle-all', function() {

        var $this = $(this),
            $inputs = $('#print-options').find('input'),
            $section = $('#' + $inputs.val());

        if($this.is(':checked')) {

            $inputs.prop('checked', true);
        }
        else {

            $inputs.prop('checked', false);
        }

        $inputs.each(function() {

            var $this = $(this),
                $section = $('#' + $this.val());

            if($this.is(':checked')) {

                $section.show();
            }
            else {

                $section.hide();
            }
        });

        setSectionCount();
    });

    $body.on('change', '.section-toggle', function() {

        var $this = $(this),
            $section = $('#' + $this.val());

        if($this.is(':checked')) {

            $section.show();
        }
        else {

            $section.hide();
        }

        setMasterCheckState();
        setSectionCount();
    });

    $body.on('change keyup', '#searchtext', function() {

        resetContentSize();

        var $this = $(this),
            term = $.trim($this.val()),
            $toggleContainer = $('#toggle-container'),
            $optionContainer = $('#print-options'),
            $options = $optionContainer.find('li'),
            pattern = new RegExp('\\b' + term + '.*?\\b', 'i'),
            $clearSearch = $('#clear-search'),
            $noResults = $('#no-search-results').length ? $('#no-search-results') : $('<div id="no-search-results">No matches</div>');

        if(term) {

            $clearSearch.show();

            var $matches = $options.filter(function(){

                return pattern.test(jQuery(this).find('label').text());
            });

            if($matches.length) {

                $noResults.remove();
                $matches.show();
                $toggleContainer.show();
                $options.not($matches).hide();
            }
            else if( ! $('#floating_window_contents_print-options').has($noResults).length) {

                $options.hide();
                $toggleContainer.hide();
                $optionContainer.after($noResults);
            }
        }
        else {

            $clearSearch.hide();
            $noResults.remove();
            $toggleContainer.show();
            $options.show();
        }

        setContentSize();
    });

    $body.on('click', '#clear-search', function() {

        $('#searchtext').val('').trigger('change');
    });
});