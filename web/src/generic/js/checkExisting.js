(function( $ ) {

    $.fn.checkExisting = function() {

        return this.each(function() {

            var $obj = $(this);

            if($obj.data('duplicate-check')) {

                var initialValue = $obj.val(),
                    $parent = $obj.closest('.field_div'),
                    ajax,
                    $loading = $(),
                    $resultsMsg = $(),
                    $results = $(),
                    tableData,
                    tableFields,
                    text,
                    keypressGrace,
                    keycode = {};

                    keycode.ENTER = 13;

                $obj.on('keyup blur', function(e) {

                    var searchTerm = $.trim($obj.val());

                    if(searchTerm != initialValue) {

                        checkForDuplicates(e.type == 'blur' || e.which == keycode.ENTER ? 0 : 750);
                    }
                });

                function checkForDuplicates( delay ) {

                    clearTimeout(keypressGrace);

                    abortAjax();

                    hideLoading();

                    keypressGrace = setTimeout(function() {

                        $results.slideUp();

                        var searchTerm = $.trim($obj.val());

                        if(searchTerm.length) {

                            $results.slideUp();
                            removeResultsMsg();

                            showLoading('Searching existing claims&hellip;');

                            var formLevel = $('#formlevel').val();

                            ajax = $.ajax({
                                url: scripturl + '?action=httprequest&type=duplicaterecordcheck',
                                data: {
                                    module: module,
                                    fieldname: $obj.attr('id'),
                                    value: $obj.val(),
                                    recordid: $('#recordid').val(),
                                    formLevel: formLevel ? formLevel : 1
                                },
                                cache: false,
                                dataType: 'json',
                                success: function(data) {

                                    if(data['status'] == 'success') {

                                        hideLoading();

                                        /**
                                         * Show results message to say if there are matches or not
                                          */
                                        showResultsMsg(data);

                                        if(data['numResults'] > 0) {

                                            /**
                                             * Remove the previous results, if any, and insert the new results table frame
                                             */
                                            $results.remove();
                                            $results = $(data['html']);
                                            $parent.after($results);
                                            $results.hide();

                                            tableFields = data['fields'];
                                            tableData = data['rows'];
                                            text = data['text'];
                                        }

                                        initialValue = $obj.val();
                                    }
                                }
                            });
                        }
                        else {

                            hideResultsMsg();
                        }
                    }, delay);
                }

                function showLoading(loadingMsg) {

                    if(typeof loadingMsg === 'undefined') {

                        loadingMsg = '';
                    }

                    $loading = $parent.find('.loading');

                    if( ! $loading.length) {

                        /**
                         * Create the loading icon and message and append to the end of the .field_input_div for the input
                         * @type {*|HTMLElement}
                         */
                        $loading = $('<span class="loading">' + loadingMsg + '</span>');

                        $obj.closest('.field_input_div').append($loading);
                    }
                    else
                    {
                        $loading.show();
                    }
                }

                function hideLoading() {

                    $loading.remove();
                }

                function showResultsMsg(data) {

                    if( ! $parent.find('.results-msg').length && typeof data != 'undefined') {

                        if(data['numResults'] > 0) {

                            var message = data['numResults'] + ' existing record' + (data['numResults'] > 1 ? 's' : '');

                            $resultsMsg = $('<span class="results-msg show-results warning-msg" title="click to show records"><span class="ui-icon ui-icon-alert"></span>' + message + '<span class="ui-icon ui-icon-carat-1-s"></span></span><span class="results-msg close-results warning-msg" style="display: none;" title="click to hide records"><span class="ui-icon ui-icon-alert"></span>' + message + '<span class="ui-icon ui-icon-carat-1-n"></span></span><span class="loading" style="display: none;"></span>');
                        }
                        else {

                            $resultsMsg = $('<span class="results-msg status-msg"><span class="ui-icon ui-icon-check"></span>No existing records found</span>');
                        }

                        $parent.on('click', '.show-results', function(){

                            showResults();
                        });

                        $obj.after($resultsMsg);
                    }
                    else
                    {
                        $parent.find('.show-results').show();
                        $parent.find('.close-results').hide();
                    }
                }

                function hideResultsMsg() {

                    $parent.find('.results-msg').hide();
                }

                function removeResultsMsg() {

                    $parent.find('.results-msg').remove();
                }

                function toggleResultsMsg() {

                    $parent.find('.show-results').hide();
                    $parent.find('.close-results').show();
                }

                // Needs to be a public function
                function showResults() {

                    if( ! $results.is(':animated')) {

                        toggleResultsMsg();

                        if( ! $results.hasClass('existing-reports')) {

                            formatTable();
                        }

                        $results.slideDown();

                        $('.close-results').on('click', function() {

                            hideResults();
                        });
                    }
                }

                function setCellWidths() {

                    if( ! $results.data('widths-set')) {

                        /**
                         * make the content available on the page for manipulation, but invisible to the user
                         */
                        $results.show().css({
                            'visibility': 'hidden',
                            'height': '0px',
                            'overflow': 'hidden'
                        });

                        var $viewport = $results.find('.results-viewport'),
                            $header = $results.find('.results-viewport thead'),
                            $headerRow = $header.find('tr'),
                            $headerColumns = $headerRow.find('th'),
                            $clonedHeader = $results.find('.cloned-header'),
                            $clonedHeaderRow = $clonedHeader.find('thead tr').last(),
                            $body = $results.find('.results-viewport table'),
                            headerWidth = $clonedHeader.width(),
                            bodyWidth = $body.width(),
                            scrollbarWidth = headerWidth - bodyWidth;

                        /**
                         * set the max height to none so the scrollbar doesn't show
                         */
                        $viewport.css({
                            'max-height': 'none'
                        });

                        $headerColumns.each(function( index ) {

                            var headerCellWidth = $headerColumns.eq(index).width();

                            /**
                             * Set widths for cloned header cells
                             */
                            $clonedHeaderRow.find('th').eq(index).attr('width', headerCellWidth);

                            /**
                             * Set widths on the first data row cells
                             */
                            $results.find('.results-viewport tbody').find('tr').filter(function() {

                                return ! $(this).data('unauthorised') == true;
                            }).first().find('td').eq(index).attr('width', headerCellWidth + ( index < $headerColumns.length - 1 ? 0 : - scrollbarWidth));
                        });

                        /**
                         * Hide the original header
                         */
                        $header.hide();

                        /**
                         * reset values set earlier, so the table will be visible when needed
                         */
                        $results.data('widths-set', true).hide().css({
                            'visibility': '',
                            'height': '',
                            'overflow': ''
                        });

                        $viewport.css({
                            'max-height': ''
                        });
                    }
                }

                function hideResults() {

                    if( ! $results.is(':animated')) {

                        $results.slideUp();

                        showResultsMsg();
                    }
                }

                function formatTable() {

                    var $table = $results.addClass('muted-table').show(),
                        $thead = $table.find('thead'),
                        $tbody = $table.find('tbody'),
                        numCols = $thead.find('th').length;

                    /**
                     * Detatch the body content so it can have stuff added to it without manipulating the DOM
                     */
                    $tbody.detach();

                    $.each(tableData, function(index, row) {

                        var $newRow = $('<tr><td><input type="button" value="Choose" data-record-id="' + row.recordid + '" onclick="if(confirm(\'' + text.confirmMsg + '\')){SendTo(\'index.php?module=' + module + '&action=record&recordid=' + row.recordid + '\');}"/></td></tr>');

                        $.each(tableFields, function(field_name, label) {

                            $newRow.append('<td>' + row[field_name] + '</td>');
                        });

                        $tbody.append($newRow);
                    });

                    /**
                     * Put it back
                     */
                    $thead.after($tbody);

                    var $clonedThead = $thead.clone();

                    var $tableFrame = $table.clone().children().remove().end().addClass('cloned-header');

                    var $wrappedThead = $tableFrame.html($clonedThead.prepend('<tr class="tableHeader head2"><th colspan="' + numCols + '"><div style="float:left;">Existing records</div><span class="close-results" title="hide results">x</span><div class="clearfix"></div></th></tr>'));

                    $table.wrap('<div class="results-viewport"></div>');

                    var $viewport = $table.closest('.results-viewport');

                    $viewport.wrap('<div class="existing-reports"></div>').before($wrappedThead);

                    $results = $table.closest('.existing-reports');

                    $parent.after($results);

                    setCellWidths();
                }

                function abortAjax() {

                    if(ajax) {

                        ajax.abort();
                    }
                }

                $obj.data('checkForDuplicates', $obj);
            }

            return $obj;
        });
    };

    $(function(){

        $('input').not('[type="hidden"]').checkExisting();
    });

}(jQuery));
