(function( $ ) {

    $.fn.limitTextareaChars = function() {

        return this.each(function() {

            var $obj = globals.WEB_SPELLCHECKER && $(this).hasClass('spellcheck') ? $(this).closest('.field_input_div').find('.livespell_textarea'): $(this),
                spellchecker = globals.WEB_SPELLCHECKER && $(this).hasClass('spellcheck'),
                limit = $(this).data('max-length');

            if(limit && limit > 0) {

                $obj.on('keyup blur input', function(e) {

                    /**
                     * Get value from textarea or text from spellchecker spoof field
                     * @type {*|XMLList}
                     */
                    var value = spellchecker ? $obj.text() : $obj.val();

                    /**
                     * Replaces new lines with two pipe (|) characters so that they can be counted properly as part of the limit
                     * @type {*}
                     */
                        value = value.replace(/(\r\n|\r|\n)/g, "||");

                    if (value.length > limit)
                    {
                        var limited = value.replace(/(^\s+|\s+$)/g, '').slice(0, limit);

                        /**
                         * Replaces the pipes with a new line and removes any single pipes that may have been split by the limit
                         * @type {string}
                         */
                        var replaceText = limited.replace(/\|\|/g, "\r\n").replace('|', '').replace(/(^\s+|\s+$)/g, '');

                        if(spellchecker) {

                            $obj.text(replaceText);
                        }
                        else {

                            $obj.val(replaceText);
                        }

                        OldAlert('You have reached the character limit set for this field');
                    }
                });

                $obj.data('limitTextareaChars', $obj);
            }

            return $obj;
        });
    };

    $(function(){

        $('textarea').limitTextareaChars();
    });

}(jQuery));