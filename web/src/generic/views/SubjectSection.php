<li>
    <ol>
        <?php foreach ($this->aSubjects as $id => $LinkedSubject) : ?>
        <?php echo getSubjectSectionHTML(array(
            'module' => $this->module,
            'data' => $LinkedSubject,
            'formtype' => $this->FormType,
            'suffix' => (intval($id) + 1),
            'clearsection' => ($id == 0),
            'subject_name' => $this->ExtraParams['subject_name']
        )); ?>
        <?php endforeach; ?>
        <?php if (count($this->aSubjects) == 0) : ?>
        <?php echo getSubjectSectionHTML(array(
            'module' => $this->module,
            'data' => $this->data,
            'formtype' => ($this->FormType == 'Search' ? $this->FormType : 'New'),
            'suffix' => 1,
            'clearsection' => true,
            'subject_name' => $this->ExtraParams['subject_name']
        )); ?>
        <?php endif; ?>
        <script language="javascript" type="text/javascript">
            dif1_section_suffix['<?php echo $this->ExtraParams['subject_name']; ?>'] = <?php echo ($this->NumSubjects + 1); ?>
        </script>
        <?php if ($this->FormType != 'Print' && $this->FormType != 'ReadOnly' && $this->FormType != 'Search') : ?>
        <li class="new_windowbg" id="add_another_<?php echo $this->ExtraParams['subject_name']; ?>_button_list">
            <input type="button" id="section_<?php echo $this->ExtraParams['subject_name']; ?>_button" value="<?php echo _tk('btn_add_another')?>" onclick="AddSectionToForm('<?php echo $this->ExtraParams['subject_name']; ?>', '', $('<?php echo $this->ExtraParams['subject_name']; ?>_section_div_'+(dif1_section_suffix['<?php echo $this->ExtraParams['subject_name']; ?>']-1)), '<?php echo Sanitize::getModule($this->module); ?>', '', true, <?php echo $this->spellChecker; ?>, <?php echo (isset($this->form_id) ? Sanitize::SanitizeInt($this->form_id) : 'null'); ?>);">
        </li>
        <?php endif; ?>
    </ol>
</li>