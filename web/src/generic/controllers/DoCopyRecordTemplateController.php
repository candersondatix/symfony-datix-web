<?php

namespace src\generic\controllers;

use src\framework\controller\TemplateController;

class DoCopyRecordTemplateController extends TemplateController
{
    /**
     * Handles redirection if user confirms copy or cancels.
     */
    function docopyrecord()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        if ($this->request->getMethod() == 'POST')
        {
            $FormValues = $this->request->getParameters();
        }

        if (!is_array($FormValues) || empty($FormValues))
        {
            $form_action = 'Cancel';
        }
        else
        {
            $form_action = $this->request->getParameter('rbWhat');
        }

        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));

        switch ($form_action)
        {
            case 'cancel':
            case BTN_CANCEL:
                $RedirectLocation = '?action=list&module=' . $module . '&listtype=search';
                $this->redirect('app.php' . $RedirectLocation);
        }

        $aCopyParams['module'] = $module;

        if ($this->request->getParameter('recordid') != '')
        {
            $Where = $ModuleDefs[$module]['TABLE'].'.recordid = '.$this->request->getParameter('recordid');
            $aCopyParams['where'] = MakeSecurityWhereClause($Where, $module, $_SESSION['initials']);
        }
        else
        {
            $aCopyParams['join'] = $_SESSION[$module]['PROMPT']['ORIGINAL_JOIN'];

            include_once('Source/libs/SavedQueries.php');
            DoPromptSection(array('module' => $module));

            if ($_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"])
            {
                $aCopyParams['where'] = $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"];
                $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"] = '';
            }

            if (empty($aCopyParams['where']))
            {
                $aCopyParams['where'] = $_SESSION[$_GET['module']]["WHERE"];
            }

            $aCopyParams['where'] = MakeSecurityWhereClause($aCopyParams['where'], $module, $_SESSION['initials']);
        }

        $aCopyParams['copies'] = $this->request->getParameter('copies');
        $aCopyParams['copy_linked_contacts'] = $this->request->getParameter('copy_linked_contacts');
        $aCopyParams['copy_actions'] = $this->request->getParameter('copy_actions');
        $aCopyParams['copy_documents'] = $this->request->getParameter('copy_documents');
        $aCopyParams['copy_extra_fields'] = $this->request->getParameter('copy_extra_fields');
        $aCopyParams['copy_notepad'] = $this->request->getParameter('copy_notepad');
        $aCopyParams['copy_link_to_master'] = $this->request->getParameter('copy_link_to_master');
        $aCopyParams['copy_scores'] = $this->request->getParameter('copy_scores');
        $aCopyParams['copy_comments'] = $this->request->getParameter('copy_comments');
        $aCopyParams['copy_evidence'] = $this->request->getParameter('copy_evidence');
        $aCopyParams['copy_evidence_link_notes'] = $this->request->getParameter('copy_evidence_link_notes');
        $aCopyParams['copy_linked_assets'] = $this->request->getParameter('copy_linked_assets');
        $aCopyParams['copy_medications'] = $this->request->getParameter('copy_medications');
        $aCopyParams['copy_causal_factors'] = $this->request->getParameter('copy_causal_factors');

        if (!empty($ModuleDefs[$module]['ADDITIONAL_COPY_OVERRIDE_FIELD_OPTIONS']))
        {
            foreach ($ModuleDefs[$module]['ADDITIONAL_COPY_OVERRIDE_FIELD_OPTIONS'] as $fieldname)
            {
                if ($this->request->getParameter($fieldname) && $this->request->getParameter($fieldname) != '')
                {
                    $aCopyParams['OVERRIDE_VALUES'][$fieldname] = $this->request->getParameter($fieldname);
                }
            }
        }

        $CopyRecords = new \CopyRecords($aCopyParams);
        $CopyRecords->CopyMainTable();

        $this->title = 'Records copied';
        $this->image = 'Images/'.$ModuleDefs[$module]['ICON'];
        $this->module = $module;

        $this->response->build('src/generic/views/ShowCopyFinished.php', array(
            'RecName' => $ModuleDefs[$module]['REC_NAME'],
            'module' => $this->module
        ));
    }
}