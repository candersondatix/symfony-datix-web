<?php

namespace src\generic\controllers;

use src\framework\controller\TemplateController;

class SaveRecordTemplateController extends TemplateController
{
    /**
     * Displays a page confirming that a record has been saved and offering the user an option
     * to go back to the record.
     * Also triggers emails to be sent if they are being sent via AJAX call.
     *
     * @param array $aParams Array of parameters
     */
    function ShowSaveRecord()
    {
        global $scripturl, $FormLevel;

        $ModuleDefs  = $this->registry->getModuleDefs();
        $aParams     = ($this->request->getParameter('aParams') ?: array());
        $HoldingForm = ($this->request->getParameter('holding_form') == 1);

        $FormLevel = is_numeric($this->request->getParameter('formlevel')) ? (int) $this->request->getParameter('formlevel') : '';

        $module  = in_array($aParams['module'], GetModuleIdCodeList()) ? htmlspecialchars($aParams['module']) : '';
        $data    = $aParams['data'];
        $message = $aParams['message'];

        $recordid = is_int($data['recordid']) ? (int) $data['recordid'] : '';
        $this->module = $module;

        if (!$_SESSION['logged_in'])
        {
            $this->hasMenu = false;
        }
        else
        {
            $this->title = 'Saved '.$ModuleDefs[$module]['REC_NAME'].' '.$recordid;
        }

        $formID = (isset($aParams['form_id']) && is_numeric($aParams['form_id'])) ? (int) $aParams['form_id'] : '';

        if ($recordid == '')
        {
            $RedirectLocation = "?module=$module";

            if (!empty($formID))
            {
                $RedirectLocation .= "&form_id=$formID";
            }

            $this->redirect('app.php'.$RedirectLocation);
        }

        $EditAction = $ModuleDefs[$module]['ACTION'];

        // If we are in "Print" mode, we don't want to display the menu
        if ($FormType == 'Print')
        {
            $this->hasMenu = false;
        }

        if (bYN(GetParm('NEW_RECORD_LVL1_'.$module, 'N')) && $aParams['new_record'])
        {
            $FormLevel = 1;
        }
        else
        {
            $FormLevel = GetUserFormLevel($module, GetParm($ModuleDefs[$module]['PERM_GLOBAL']));
        }

        if ($ModuleDefs[$module]['LINKED_MODULE'] && $this->request->getParameter('fromparent'))
        {
            $recordurl = $scripturl.'?module='.$module.'&action='.$EditAction.'&recordid='.
                $recordid.'&link_module='.$data['link_module'].'&main_recordid='.$data['main_recordid'];
        }
        else
        {
            $recordurl = $scripturl.'?module='.$module.'&action='.$EditAction.'&recordid='.$recordid;
        }

        if ($this->request->getParameter('fromsearch'))
        {
            $recordurl .= '&fromsearch=1';
        }

        if ($this->request->getParameter('from_report'))
        {
            $recordurl .= '&from_report=1';
        }

        if ($this->request->getParameter('panel'))
        {
            $recordurl .= '&panel=' . $this->request->getParameter('panel');
        }

        $ajaxEmails = ((bYN(GetParm($ModuleDefs[$module]['SHOW_EMAIL_GLOBAL'], 'N')) === false) ||
            (bYN(GetParm('SEND_EMAIL_NEW_'.$module.'_2', 'N')) && !bYN(GetParm('SHOW_EMAIL_'.$module.'_2', 'N')) &&
                $FormLevel == 2 && $aParams['new_record']));
        
        if ($this->registry->getParm('QUEUE_EMAILS', 'N', false, true) && !$this->get('email_queue_failed'))
        {
            // don't send e-mails via ajax if we've successfully added to the queue
            $ajaxEmails = false;
        }

        if($_SESSION['logged_in'])
        {
            if ($FormLevel == 1)
            {
                if ($module == 'INC')
                {
                    $status_message = \Sanitize::SanitizeRaw(_tk('INC_report_saved').$data['inc_ourref']);
                }
                else
                {
                    $status_message = \Sanitize::SanitizeRaw(_tk($this->module.'_report_saved'));
                }
            }
            else
            {
                $status_message = _tk($module . '_2_record_saved');
                /*if ($ModuleDefs[$module]['USES_APPROVAL_STATUSES'] !== false)
                {
                    $status_message .= '"' . (_tk('approval_status_'.$module.'_'.$data['rep_approved']) ?: code_descr($module, 'rep_approved', $data['rep_approved'])) . '"';
                }
                if (!in_array($module, array('CQO', 'CQP', 'CQS')))
                {
                    $status_message .= " - " . _tk('level_2_record_ref');
                }*/
            }
            if ($message)
            {
                $status_message .= $message;
            }

            AddSessionMessage('INFO', $status_message);

            if ($ajaxEmails)
            {
                $_SESSION['ajaxEmailsToSend'][] = "SendEmailAlertsMessage('$module', $recordid, '".htmlspecialchars($data['rep_approved'])."', '".htmlspecialchars($data['rep_approved_old'])."', '$FormLevel')";
            }

            $this->redirect($recordurl);
        }
        else
        {
            $this->response->build('src/generic/views/SaveRecord.php', array(
                'ajaxEmails' => $ajaxEmails,
                'module' => $module,
                'FormLevel' => $FormLevel,
                'aParams' => $aParams,
                'recordid' => $recordid,
                'data' => $data,
                'formID' => $formID,
                'USES_APPROVAL_STATUSES' => $ModuleDefs[$module]['USES_APPROVAL_STATUSES'],
                'message' => $message,
                'INPUT_ONLY_LEVELS' => $ModuleDefs[$module]['INPUT_ONLY_LEVELS'],
                'EditAction' => $EditAction,
                'recordurl' => $recordurl,
                'printAfterSubmit' => $this->request->getParameter('printAfterSubmit')
            ));
        }
    }
}