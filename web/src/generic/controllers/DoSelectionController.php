<?php

namespace src\generic\controllers;

use src\framework\controller\Controller;
use src\reports\exceptions\ReportException;
use src\framework\query\WhereFactory;

class DoSelectionController extends Controller
{
    function doselection()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $Module   = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $linkMode = ($this->request->getParameter('link') !== null);

        // Select from view if it exists
        $ModuleTable = ($ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE']);
        $Perms = GetParm($ModuleDefs[$Module]['PERM_GLOBAL']);

        $FormAction = $this->request->getParameter('rbWhat');
        $FieldWhere = '';
        $Error = false;
        $search_where = array();
        ClearSearchSession($Module);

        if ($FormAction == 'Cancel' || $FormAction == BTN_CANCEL)
        {
            if (isset($_SESSION['security_group']['grp_id']))
            {
                $_SESSION['security_group']['success'] = false;
                $Parameters = array(
                    'grp_id' => $_SESSION['security_group']['grp_id'],
                    'module' => $_SESSION['security_group']['module']
                );
                $this->response = $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', $Parameters);
                return;
            }
            elseif (isset($_SESSION['packaged_report']))
            {
                $RedirectUrl = '?action=designapackagedreport';

                if ($_SESSION['packaged_report']['recordid'])
                {
                    $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
                }
                else
                {
                    $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
                }

                $_SESSION['packaged_report']['success'] = false;
            }
            elseif ($this->request->getParameter('qbe_recordid') != '')
            {
                // query by example for generic modules
                $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module') .
                    '&recordid=' . $this->request->getParameter('qbe_recordid');
            }
            else
            {
                $RedirectUrl = '?module=' . $Module;
            }

            $this->redirect('app.php' . $RedirectUrl);
        }

        // construct and cache a new Where object
        try
        {
            $whereFactory = new WhereFactory();
            $where = $whereFactory->createFromRequest($this->request);

            if (empty($where->getCriteria()['parameters'])) {
                $where = null;
            }

            $_SESSION[$Module]['NEW_WHERE'] = $where;
        }
        catch (\InvalidDataException $e)
        {
            $Error = true;
            $this->registry->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');
        }
        catch(ReportException $e)
        {
            $Error = true;
        }

        // Find all values which start with ram_ and add them to array
        // if they are not empty.
        // To do: parse names.
        $data = $this->request->getParameters();
        $_SESSION[$Module]['LAST_SEARCH'] = $data;

        $table = ($ModuleDefs[$Module]['VIEW'] ? $ModuleDefs[$Module]['VIEW'] : $ModuleDefs[$Module]['TABLE']);

        while (list($name, $value) = each($data))
        {
            $explodeArray = explode('_', $name);

            if ($explodeArray[0] == 'UDF')
            {
                if (MakeUDFFieldWhere($explodeArray, $value, GetModIDFromShortName($Module), $FieldWhere) === false)
                {
                    $Error = true;
                    break;
                }
            }
            elseif ($explodeArray[0] == 'clarootcause')
            {
                if (MakeFieldWhere('CLA', 'cla_root_causes', $value, $FieldWhere, $TablePrefix) === false)
                {
                    $Error = true;
                    break;
                }
            }
            elseif ($explodeArray[0] == 'searchtaggroup')
            {
                // @prompt on tags is not yet supported
                if(preg_match('/@prompt/iu',$value) == 1)
                {
                    $search_where[] = '1=2';
                }
                elseif ($value != '')
                {
                    $field = str_replace('searchtaggroup_'.$explodeArray[1].'_', '', $name);

                    // Need to find the codes associated with the selected tags.
                    $sql = '
                        SELECT DISTINCT
                            code
                        FROM
                            code_tag_links
                        WHERE
                            [group] = :group
                            AND [table] = :table
                            AND field = :field
                            AND tag IN (\''.implode('\',\'', array_map('intval', explode('|', $value))).'\')
                    ';

                    $Codes = \DatixDBQuery::PDO_fetch_all($sql, array(
                        'group' => intval($explodeArray[1]),
                        'field' => $field,
                        'table' => $ModuleDefs[$this->request->getParameter('module')]['TABLE']
                        ),
                        \PDO::FETCH_COLUMN
                    );

                    if (!empty($Codes))
                    {
                        $search_where[] = $field.' IN (\''.implode('\',\'', $Codes).'\')';
                    }
                    else
                    {
                        // This tag hasn't been attached to any codes, so can't return any results.
                        $search_where[] = '1=2';
                    }
                }
            }
            elseif ($explodeArray[0] == 'csu')  // Hack to allow searching on linked subjects.
            {
                // Check for suffix and remove
                if (is_numeric($explodeArray[count($explodeArray) - 1]))
                {
                    unset($explodeArray[count($explodeArray) - 1]);
                    $name = implode('_', $explodeArray);
                }

                $name = CheckFieldMappings('COM', $name, true);

                if (MakeFieldWhere($Module, $name, $value, $FieldWhere, 'compl_subjects') === false)
                {
                    $Error = true;
                    break;
                }

                if ($FieldWhere)
                {
                    $ComplSubjFieldWhere[] = $FieldWhere;
                }

                continue;
            }
            elseif ($explodeArray[0] == 'pay' && $explodeArray[1] == 'type' && $name != 'pay_type') // Hack to allow searching on payment summary
            {
                // We need to add a fake field to fielddefs to ensure that makefieldwhere will work.
                if (MakeFieldWhere($Module, $name, $value, $FieldWhere, 'payments') === false)
                {
                    $Error = true;
                    break;
                }

                if ($FieldWhere)
                {
                    $FieldWhere = str_replace('payments.'.$name, 'dbo.fn_GetPaymentTypeTotal(claims_main.recordid,\''.$explodeArray[2].'\')', $FieldWhere);
                }

                if ($FieldWhere)
                {
                    $search_where[] = $FieldWhere;
                }
            }
            else
            {
                if (MakeFieldWhere($Module, $name, $value, $FieldWhere, $table) === false)
                {
                    $Error = true;
                    break;
                }
            }

            if ($FieldWhere != '')
            {
                if (CheckForCorrectAtCodes($FieldWhere))
                {
                    // on link mode skip link_* fields
                    if (!($linkMode && substr($name, 0, 5) == 'link_'))
                    {
                        $search_where[$name] = $FieldWhere;
                    }
                }
                else
                {
                    AddMangledSearchError($name, $value);
                    $Error = true;
                }
            }
        }

        // Combine linked subjects search terms into a single subquery.
        if (!empty($ComplSubjFieldWhere))
        {
            $search_where[] = $table.'.recordid IN (SELECT com_id FROM compl_subjects WHERE '.implode(" AND ", $ComplSubjFieldWhere).')';
        }

        if (!$Error && !ValidateWhereClause(implode(' AND ', $search_where), $Module))
        {
            $Error = true;
        }

        if ($Error === true)
        {
            AddSessionMessage('ERROR', _tk('search_errors'));

            if ($this->request->getParameter('udfRowArray'))
            {
                $_SESSION[$Module]['UDFARRAY'] = $this->request->getParameter('udfRowArray');
            }
            
            if ($this->request->getParameter('response') == 'ajax')
            {
                $this->returnAjaxErrors();
                return;
            }

            $this->redirect('app.php?action=search&module=' . $Module . '&searchtype=lastsearch');
        }

        $_SESSION[$Module]['UDFARRAY'] = NULL;

        $Where = '';

        if (empty($search_where) === false)
        {
            $Where = implode(' AND ', $search_where);
        }

        $_SESSION[$Module]['WHERE'] = $Where;
        $_SESSION[$Module]['TABLES'] = $table;

        // We are doing a new search so unset LASTQUERYID
        unset($_SESSION[$Module]['LASTQUERYID']);

        if (isset($_SESSION['security_group']['grp_id']))
        {
            $_SESSION['security_group']['success'] = true;
            $Parameters = array(
                'grp_id' => $_SESSION['security_group']['grp_id'],
                'module' => $_SESSION['security_group']['module']
            );
            $this->response = $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', $Parameters);
            return;
        }
        elseif (isset($_SESSION['packaged_report']))
        {
            $RedirectUrl = '?action=designapackagedreport';

            if ($_SESSION['packaged_report']['recordid'])
            {
                $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
            }
            else
            {
                $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
            }

            $_SESSION['packaged_report']['success'] = true;
        }
        elseif ($this->request->getParameter('qbe_recordid') != '')
        {
            // query by example for generic modules
            $RedirectUrl = '?action=record&module=' . $this->request->getParameter('qbe_return_module') .
                '&recordid=' . $this->request->getParameter('qbe_recordid') . '&qbe_success=true';
        }
        elseif ($this->request->getParameter('response') == 'ajax')
        {
            $this->response->setBody(json_encode([]));
            return;
        }
        else
        {
            $RedirectUrl = "?action=list&module=$Module&listtype=search";
            $_SESSION[$Module]['SEARCHLISTURL'] = $RedirectUrl;
        }

        if ($linkMode) {
            $RedirectUrl .= '&link=1';
        }

        $this->redirect('app.php' . $RedirectUrl);
    }
    
    /**
     * Returns search error messages if we're invoking the controller form an ajax context.
     */
    protected function returnAjaxErrors()
    {
        $errors = $_SESSION['MESSAGES']['ERROR'];
        unset($_SESSION['MESSAGES']['ERROR']);
        $this->response->setBody(json_encode(['errors' => $errors]));
    }
}