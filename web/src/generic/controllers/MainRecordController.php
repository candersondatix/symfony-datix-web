<?php

namespace src\generic\controllers;

use src\documents\controllers\DocumentController;
use src\framework\controller\Controller;
use src\framework\controller\Request;
use src\framework\registry\Registry;

/**
 *  @brief Generic module controller
 */

class MainRecordController extends Controller
{

    /**
     * Displays the form for creating a new record
     */
    public function addnew ()
    {
        $Level = ($this->request->getParameter('level') ? \Sanitize::SanitizeInt($this->request->getParameter('level')) : 1);

        $IncorrectFormID = false;

        if (!$_SESSION['CurrentUser']->canAddRecords($this->request->getParameter('module')))
        {
            throw new \PermissionDeniedException('You do not have permission to perform this action.');
        }
        
        if($Level == 2)
        {
            LoggedIn();
        }
        else
        {
            $form_id = $this->request->getParameter('form_id');
            $module  = $this->request->getParameter('module');
            
            if(!empty($form_id))
            {
                if(FormIDExists($form_id, $module))
                {
                    $_SESSION['form_id'][$module][$Level] = $form_id;
                }
                else
                {
                    $IncorrectFormID = true;
                }
            }
        }
        
        
        $main_recordid = $this->request->getParameter('main_recordid');

        if (!empty($main_recordid))
        {
            $data = getAssessmentTemplateData (\Sanitize::SanitizeString ($main_recordid));
        }

        
        if($IncorrectFormID)
        {
            fatal_error("There is no form with the specified ID.");
        }
        else
        {
            $ReporterArray = SetUpDefaultReporter();
            if(!empty($ReporterArray))
            {
                $data['con']['R'][] = $ReporterArray;
            }

            require_once 'Source/generic/MainRecord.php';
            ShowForm($data,'',$Level);
        }
    }

    /**
     * Display search filters form
     */
    public function search ()
    {
        $ModuleDefs      = $this->registry->getModuleDefs();
        $Module          = $this->request->getParameter('module');
        $validationError = $this->request->getParameter('validationerror');
        $searchType      = $this->request->getParameter('searchtype');

        LoggedIn();

        // 1: User is logged in, but cannot see current module
        // 2: User has clicked on 'Define criteria using query by example in a security details page. In situations like
        //    this they are allowed to view this page, even if they dont have permissions to see it. See DW-11231
        if ($_SESSION['CurrentUser'] && !$_SESSION['CurrentUser']->canSeeModule($Module) //1
            && !isset($_SESSION['security_group']['grp_id'])) //2
        {
            throw new \PermissionDeniedException('You do not have permissions to access this module.');
        }

        $_SESSION[$Module]['PROMPT']['VALUES'] = array();

        if ($validationError)
        {
           $data["error"]["message"] = '<br />'._tk('invalid_characters_in_fields');
        }
        else
        {
            $_SESSION[$Module]['UDFARRAY'] = NULL;
        }

        if($searchType == "lastsearch" )
        {
            $data = $_SESSION[$Module]["LAST_SEARCH"];
        }
        else
        {
            $data = "";
        }

        require_once 'Source/generic/MainRecord.php';
        ShowForm ($data, "search");
    }

    /**
     * Saves a generic record.
     */
    public function saveRecord()
    {
        global $Perms, $SectionVisibility, $ModuleDefs, $FieldDefs;

        session_start();

        $Module        = \Sanitize::getModule($this->request->getParameter('module'));
        $ModuleTable   = $ModuleDefs[$Module]['TABLE'];
        $Perms         = GetParm($ModuleDefs[$Module]['PERM_GLOBAL']);
        $ModuleActions = $ModuleDefs[$Module]['EXTRA_FORM_ACTIONS'];
        $recordId      = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        $parameters    = $this->request->getParameters();

        if (!is_array($parameters) || empty($parameters))
        {
            $form_action = 'Cancel';
        }
        else
        {
            $form_action = $this->request->getParameter('rbWhat');
        }

        Unlock($Module, $this->request->getParameter('recordid'));

        switch ($form_action)
        {
            case 'Cancel':
            case BTN_CANCEL:
                if ($this->request->getParameter('fromsearch') && !strpos($_SESSION['LAST_PAGE'], 'action=listcontacts'))
                {
                    $redirectUrl = $_SESSION['LAST_PAGE'];
                }
                elseif(strpos($_SESSION['LAST_PAGE'], 'action=listcontacts'))
                {
                    if ($ModuleDefs[$Module]['MODULE_GROUP'])
                    {
                        $redirectUrl = 'app.php?action=list&listtype=search&module='.$ModuleDefs[$Module]['MODULE_GROUP'];
                    }
                    else
                    {
                        $redirectUrl = "app.php?action=list&listtype=search&module=$Module";
                    }
                }
                /* TODO: REPLACE WITH PREJUDICE
                 * Apologies to all involved. Another hard coded exception. Policies needs to return to its
                 * parent module when saving and so we do some excellent string manipulation.
                 */
                elseif($Module == 'POL' && strpos($_SERVER['HTTP_REFERER'], 'org_id='))
                {
                    $parts = explode('&', $_SERVER['HTTP_REFERER']);
                    $urlParts = array();
                    foreach ($parts as $key => $val)
                    {
                        $part = explode('=', $val);
                        $urlParts[$part[0]] = $part[1];
                    }
                    if(strpos($_SERVER['HTTP_REFERER'], 'type=CON'))
                    {
                        $redirectUrl = 'app.php?action=linkcontactgeneral&module=CLA&main_recordid='.$urlParts['main_recordid'].'&link_recordid='.$urlParts['link_recordid'].'&link_type=O&panel=policies';
                    }
                    else
                    {
                        $redirectUrl = 'app.php?action=editorganisationlink&module=CLA&main_recordid='.$urlParts['main_recordid'].'&link_recordid='.$urlParts['link_recordid'].'&recordid='.$urlParts['org_id'].'&panel=policies';
                    }
                }
                elseif ($this->request->getParameter('from_report'))
                {
                    $redirectUrl = 'app.php?action=list&module='. $Module .'&listtype=search&from_report=1&overdue='.$this->request->getParameter('overdue');
                }
                else
                {
                    if ($ModuleDefs[$Module]['MODULE_GROUP'])
                    {
                        $redirectUrl = 'index.php?module='.$ModuleDefs[$Module]['MODULE_GROUP'];
                    }
                    else
                    {
                        $redirectUrl = "index.php?module=$Module";
                    }

                    if ($this->request->getParameter('form_id'))
                    {
                        $redirectUrl .= '&form_id=' . $this->request->getParameter('form_id');
                    }
                }

                $this->redirect($redirectUrl);
                break;
            case 'ShowLevel1Values':
                $this->redirect('app.php?action=record&recordid='.$recordId.'&show_level1_values=1');
                break;
            case 'ShowAudit':
                $this->redirect('app.php?action=record&recordid='.$recordId.'&full_audit=1');
                break;
            case 'Search':
                $this->redirect('app.php?action=doselection');
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner');
                break;
            default:
                // default case to catch any custom module actions
                if (is_array($ModuleActions) && array_key_exists($form_action, $ModuleActions))
                {
                    require_once $ModuleActions[$form_action]['sourceFile'];
                    $ModuleActions[$form_action]['callback']();
                }

                break;
        }

        $HoldingForm = ($this->request->getParameter('holding_form') == 1);

        if ($ModuleDefs[$Module]['USES_APPROVAL_STATUSES'] !== false)
        {
            $approved = GetValidApprovalStatusValueFromPOST(array('module' => $Module));
        }

        GetSectionVisibility($Module, ($HoldingForm? 1 : 2), $this->request->getParameters(), $this->request->getParameter('link_module'));
        BlankOutPostValues($Module, ($HoldingForm? 1 : 2), $this->request->getParameter('link_module'), null, $this->request);

        $ValidationErrors = $this->validateData($Module);

        $ContactError     = ValidateLinkedContactData($this->request->getParameters(), $Module);

        /*We need to ensure that fields that are not visible on save are not validated (otherwise you end
        up with validation messages you cannot see. The best way of doing this would be to have GetSectionVisibility()
        and BlankOutPostValues() run for each linked contact form. That's going to be pretty risky to force in now,
        so in the short term I'm doing a manual check of whether the field is visible here.
        TODO: make GetSectionVisibility and BlankOutPostValues generic enough that they can run on subforms within level 1 forms.
        */
        $ContactError = self::RemoveHiddenContactValidation($ContactError, $this->request);

        $ValidationErrors = array_merge($ValidationErrors, $ContactError);

        // Documents linked on level 1
        if ($this->request->getParameter('max_doc_suffix'))
        {
            $DocError = DocumentController::ValidateLinkedDocumentData($this->request->getParameters());
            $ValidationErrors = array_merge($ValidationErrors, $DocError);
        }

        if ($ValidationErrors)
        {
            $error['Validation'] = $ValidationErrors;
        }

        if (!empty($error))
        {
            $data                 = $this->request->getParameters();
            $data['error']        = $error;
            $data['rep_approved'] = $approved;
            $data['submit_stage'] = $submit_stage;

            // Workaround to convert dates to SQL format
            // TODO: This needs to be removed when refactored
            $ModuleDateFields = array_merge(GetAllFieldsByType($Module, 'date'), getAllUdfFieldsByType('D', '', $data));

            foreach ($ModuleDateFields as $Date)
            {
                if (array_key_exists($Date, $data) && $data[$Date] != '')
                {
                    $data[$Date] = UserDateToSQLDate($data[$Date]);
                    //some extra field dates look directly at the post value, so need to blank it out here.
                    unset($_POST[$Date]);
                    $this->request->unSetParameter($Date);
                }
            }

            if ($HoldingForm)
            {
                require_once 'Source/generic/MainRecord.php';
                ShowForm($data, '',1);
            }
            else
            {
                require_once 'Source/generic/MainRecord.php';
                ShowForm($data);
            }

            obExit();
        }

        if (bYN(GetParm('AUTO_POPULATE_REC_NAME_'.$Module, 'N')))
        {
            if ($ModuleDefs[$Module]['RECORD_NAME_FROM_CONTACT'])
            {
                $NumContacts = max($this->request->getParameter('contact_max_suffix'), $this->request->getParameter('increp_contacts'));

                for ($i = 1; $i <= $NumContacts; $i++)
                {
                    if ($Module == 'COM' && $this->request->getParameter("lcom_iscomplpat_$i") == 'Y')
                    {
                        $ChangeName = ($ModuleDefs[$Module]['FIELD_NAMES']['NAME'] && !$this->request->getParameter($ModuleDefs[$Module]['FIELD_NAMES']['NAME']) && $this->request->getParameter("con_surname_$i") && $this->request->getParameter("link_type_$i") == 'C');
                    }
                    else
                    {
                        $ChangeName = ($ModuleDefs[$Module]['FIELD_NAMES']['NAME'] && !$this->request->getParameter($ModuleDefs[$Module]['FIELD_NAMES']['NAME']) && $this->request->getParameter("con_surname_$i") && $this->request->getParameter("link_type_$i") == $ModuleDefs[$Module]['RECORD_NAME_FROM_CONTACT']);
                    }

                    if ($ChangeName && !ContactHidden($this->request->getParameters(), $i))
                    {
                        $_POST[$ModuleDefs[$Module]['FIELD_NAMES']['NAME']] = \UnicodeString::substr(\UnicodeString::strtoupper(\Sanitize::SanitizeRaw($this->request->getParameter("con_surname_$i")) . ($this->request->getParameter("con_forenames_$i") ? " " . \Sanitize::SanitizeRaw($this->request->getParameter("con_forenames_$i")) : "")), 0, 32);
                        $this->request->setParameter($ModuleDefs[$Module]['FIELD_NAMES']['NAME'], \UnicodeString::substr(\UnicodeString::strtoupper(\Sanitize::SanitizeRaw($this->request->getParameter("con_surname_$i")) . ($this->request->getParameter("con_forenames_$i") ? " " . \Sanitize::SanitizeRaw($this->request->getParameter("con_forenames_$i")) : "")), 0, 32));
                    }
                }
            }
        }

        if (!$recordId)
        {
            $newRecord = true;

            if ($FieldDefs[$Module]['recordid']['IdentityCol'] == true)
            {
                $sql = 'INSERT INTO ' . $ModuleTable . ' (createdby) VALUES (:createdby)';
                $recordId = \DatixDBQuery::PDO_insert($sql, array('createdby' => $_SESSION['initials']));
            }
            else
            {
                $recordId = GetNextRecordID($ModuleTable, true, 'recordid', $_SESSION['initials']);
            }

            if (!$_SESSION['logged_in'])
            {
                $_SESSION[$Module]['LOGGEDOUTRECORDHASH'] = getRecordHash($recordId);
            }
        }
        else
        {
            $newRecord = false;
            $DateTime = null;
            DoFullAudit($Module, $ModuleTable, $recordId, '', '', $DateTime);
        }

        $data = $this->request->getParameters();
        if($DateTime != null)
        {
            $data['full_audit_date_time'] = $DateTime;
        }

        $data['rep_approved'] = $approved;

        $Prefix = GetParm($Module.'_1_REF_PREFIX');

        if ($Prefix && $HoldingForm && $data[$ModuleDefs[$Module]['FIELD_NAMES']['REF']] == '')
        {
            $data[$ModuleDefs[$Module]['FIELD_NAMES']['REF']] = $Prefix.$recordId;
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module'        => $Module,
            'level'         => ($HoldingForm? 1 : 2),
            'parent_module' => $this->request->getParameter('link_module')
        ));
        $FormDesign->LoadFormDesignIntoGlobals();

        $data = ProcessNewCodeFieldValues ($Module, $data, $FieldDefs);
        $data = ParseSaveData(array('module' => $Module, 'data' => $data));

        $data = $this->ParseRootCausesGeneric(array('module' => $Module, 'data' => $data));

        // Send notifications for changed handler and investigator
        // BEFORE the record is updated (needs to check old values).
        if (!$newRecord && bYN(GetParm('STAFF_CHANGE_EMAIL_'.$Module, 'N')))
        {
            $data['recordid'] = $recordId; // needed so recordid can appear in email.
            require_once 'Source/generic/EmailChangedStaff.php';
            EmailNewHandler($data, $Module);
            EmailNewInvestigators($data, $Module);
        }

        if (is_array($ModuleDefs[$Module]['EMAIL_AFTER_SAVE_FUNCTIONS']))
        {
            foreach ($ModuleDefs[$Module]['EMAIL_AFTER_SAVE_FUNCTIONS'] as $Function)
            {
                $data['recordid'] = $recordId; //needed so recordid can appear in email.
                require_once 'Source/generic_modules/'.$Module.'/ModuleFunctions.php';
                $Function($data);
            }
        }

        if (is_array($ModuleDefs[$Module]['BEFORE_SAVE_FUNCTIONS']))
        {
            foreach ($ModuleDefs[$Module]['BEFORE_SAVE_FUNCTIONS'] as $Function)
            {
                $data['recordid'] = $recordId;
                require_once 'source/generic_modules/'.$Module.'/ModuleFunctions.php';
                $data = $Function($data);
            }
        }

        $sql = "UPDATE $ModuleTable SET ";

        $FieldArray = $ModuleDefs[$Module]['FIELD_ARRAY'];

        if ($ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'][$this->request->getParameter('link_module')])
        {
            $FieldArray[] = $ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'][$this->request->getParameter('link_module')];
            $data[$ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'][$this->request->getParameter('link_module')]] = $this->request->getParameter('main_recordid');
        }

        $sql .= GeneratePDOSQLFromArrays(array(
            'FieldArray' => $FieldArray,
            'DataArray'  => $data,
            'Module'     => $Module,
            'end_comma'  => true
        ), $PDOParamsArray);

        $PDOParamsArray['updatedby'] = $_SESSION['initials'];
        $PDOParamsArray['updateid'] = GensUpdateID($_POST['updateid']);
        $PDOParamsArray['updateddate'] = date('d-M-Y H:i:s');

        $sql .= ' updatedby = :updatedby, updateid = :updateid, updateddate = :updateddate';

        $PDOParamsArray['where_recordid'] = $recordId;
        $PDOParamsArray['postedupdateid'] = $this->request->getParameter('updateid');
        $sql .= ' WHERE recordid = :where_recordid and (updateid = :postedupdateid OR updateid IS NULL)';

        $result = \DatixDBQuery::PDO_query($sql, $PDOParamsArray);

        if (!$result)
        {
            $error = "An error has occurred when trying to save the record.  Please report the following to the Datix administrator: $sql";
        }

        if (!empty($error))
        {
            SaveError($error);
        }

        $data['recordid'] = $recordId;

        // Save UDFs
        require_once 'Source/libs/UDF.php';
        SaveUDFs($recordId, GetModIDFromShortName($Module));

        // Save notepad
        require_once 'Source/libs/notepad.php';
        $error = SaveNotes(array(
            'id_field' => $ModuleDefs[$Module]['FK'],
            'id'       => $recordId,
            'notes'    => $data['notes'],
            'new'      => $newRecord
        ));

        // Save Progress Notes
        $this->call('src\progressnotes\controllers\ProgressNotesController', 'saveProgressNotes', array(
            'module' => $Module,
            'data'   => $data
        ));

        // Save Rejection Notes
        if (bYN(GetParm('REJECT_REASON', 'Y')) && $data['rep_approved'] == 'REJECT')
        {
            $this->call('src\reasons\controllers\ReasonsController', 'SaveReason', array(
                'module'  => $Module,
                'link_id' => $recordId,
                'data'    => $data,
            ));
        }

        // Save contacts attached on level1.
        require_once 'Source/contacts/SaveContact.php';
        SaveContacts(array(
            'main_recordid' => $recordId,
            'module'        => $Module,
            'formlevel'     => ($HoldingForm? 1 : 2)
        ));

        // Save organisations attached on level 1
        if ($Module == 'CLA' && $HoldingForm === true)
        {
            $this->call('src\organisations\controllers\OrganisationsController', 'saveLinkedOrganisations', array(
                'main_recordid' => $recordId
            ));
        }

        // Save linked records
        if (is_array($ModuleDefs[$Module]['LINKED_RECORDS']))
        {
            foreach ($ModuleDefs[$Module]['LINKED_RECORDS'] as $Type => $aDetails)
            {
                $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => $Module, 'level' => ($HoldingForm? 1 : 2)));

                if (!$FormDesign->HideFields[$aDetails['section']])
                {
                    $aDetails['module'] = $Module;
                    $aDetails['main_recordid'] = $recordId;
                    $error = SaveLinkedRecords($aDetails);
                }
            }
        }

        // Custom functions for saving additional data
        if (is_array($ModuleDefs[$Module]['EXTRA_SAVE_INCLUDES']))
        {
            foreach ($ModuleDefs[$Module]['EXTRA_SAVE_INCLUDES'] as $includeFile)
            {
                require_once $includeFile;
            }
        }

        if (is_array($ModuleDefs[$Module]['EXTRA_SAVE_FUNCTIONS']))
        {
            foreach ($ModuleDefs[$Module]['EXTRA_SAVE_FUNCTIONS'] as $function)
            {
                $function($data);
            }
        }

        if (ModIsLicensed('HOT'))
        {
            require_once 'Source/generic_modules/HOT/ModuleFunctions.php';
            InsertIntoHotspotQueue($Module, $recordId);
        }

        if ($ModuleDefs[$Module]['LINKED_DOCUMENTS'])
        {
            if ($SectionVisibility['documents'] || $SectionVisibility['extra_document'])
            {
                // Save documents attached on level1.
                DocumentController::SaveDocsFromLevel1($recordId, $Module);
            }
        }

        // Adding a new record that is not represented in the session recordlist object can cause
        // problems with the session checkboxes (since they can't point to a record that didn't exist
        // when the object was created, so we reset the selection here.
        if ($newRecord && isset($_SESSION[$Module]['RECORDLIST']))
        {
            unset($_SESSION[$Module]['RECORDLIST']);
        }

        // e-mail the handler of the record when:
        // - submitting a risk without logging in
        // - submitting a risk by a logged in RISK1 user
        // $approved is hardcoded, could be set in 'email sending' or 'module' settings or similar
        if (bYN(GetParm($ModuleDefs[$Module]['SHOW_EMAIL_GLOBAL'],'N')) && $HoldingForm)
        {
            require_once 'Source/libs/Email.php';
            //We need to send "POST" rather than $ram data here, to avoid sending dates in the wrong format.

            // Workaround to convert dates to SQL format
            // TODO: This needs to be removed when refactored
            $DateFields = GetAllFieldsByType($Module, 'date');

            foreach ($DateFields as $Date)
            {
                if (array_key_exists($Date, $_POST) && $_POST[$Date] != '')
                {
                    $_POST[$Date] = UserDateToSQLDate($_POST[$Date]);
                }
            }

            $_POST['recordid'] = $recordId;
            $Output = SendEmails(array(
                'module'      => $Module,
                'data'        => $data,
                'progressbar' => $Progress,
                'from'        => $data['rep_approved_old'],
                'to'          => $data['rep_approved'],
                'perms'       => ($Perms? $Perms: 'NONE'),
                'level'       => $this->get('formlevel')
            ));
        }

        if ($ModuleDefs[$Module]['LINKED_CONTACTS'] !== false)
        {
            $UnapprovedContactArray = GetUnapprovedContacts(array('module' => $Module, 'recordid' => $recordId));

            // Discount unapproved contacts if they are hidden, since the user will not be able to do anything about them.
            if (is_array($UnapprovedContactArray))
            {
                IncludeCurrentFormDesign($Module, 2);

                foreach ($UnapprovedContactArray as $key => $ContactDetails)
                {
                    if ($GLOBALS['HideFields']['contacts_type_'.$ContactDetails['link_type']])
                    {
                        unset($UnapprovedContactArray[$key]);
                    }
                }
            }
        }

        // This was moved here because when we have unapproved contacts in a record the actions chains were not being attached
        if (bYN(GetParm('WEB_TRIGGERS', 'N')))
        {
            require_once 'Source/libs/Triggers.php';
            ExecuteTriggers($recordId, $data, $ModuleDefs[$Module]['MOD_ID']);
        }

        // Save reserve audit
        if ($Module == 'CLA')
        {
            //todo convert this into a procedural function in AFTER_SAVE_FUNCTIONS
            $this->call('src\claims\controllers\FinanceController', 'updateReserveAuditData', array('data' => $data));
        }

        /* This is not used by anything anymore... but it could be.
           Will you be the one? */
        if (is_array($ModuleDefs[$Module]['AFTER_SAVE_FUNCTIONS']))
        {
            foreach ($ModuleDefs[$Module]['AFTER_SAVE_FUNCTIONS'] as $Function)
            {
                $data['recordid'] = $recordId;
                require_once 'source/generic_modules/'.$Module.'/ModuleFunctions.php';
                //while the data may be edited by these functions, any changes will not be saved as the save has already taken place
                $data = $Function($data);
            }
        }

        if ($form_action == 'QBE')
        {
            // redirect to appropriate search form to create a Query By Example
            if (isset($ModuleDefs[$this->request->getParameter('qbe_search_module')]['SEARCH_URL']))
            {
                $redirectUrl = '?' . $ModuleDefs[$this->request->getParameter('qbe_search_module')]['SEARCH_URL'] .
                    '&module=' . $this->request->getParameter('qbe_search_module') .
                    '&qbe_recordid=' . $recordId . '&qbe_return_module=' . $Module;
            }

            $this->redirect('app.php'.$redirectUrl);
        }

        if (CheckUnapprovedContactRedirect(array('from' => $this->request->getParameter('rep_approved_old'), 'to' => $this->request->getParameter('rep_approved'), 'module' => $Module, 'access_level' => $Perms))
            && count($UnapprovedContactArray) > 0) // Warn for unapproved contacts
        {
            DoUnapprovedContactRedirect(array('module' => $Module, 'recordid' => $recordId, 'contact_array' => $UnapprovedContactArray));
        }
        elseif ($ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'] && !isset($ModuleDefs[$Module]['SAVERECORD_POST_SAVE']))
        {
            if (!$this->request->getParameter('from_parent_record'))
            {
                // Record save module based strings in english.php have the level
                if (_tk($Module.'_'.$FormDesign->Level.'_record_saved'))
                {
                    AddSessionMessage('INFO', _tk($Module.'_'.$FormDesign->Level.'_record_saved'));
                }
                elseif (_tk($Module.'_record_saved'))
                {
                    AddSessionMessage('INFO', _tk($Module.'_record_saved'));
                }
                else
                {
                    AddSessionMessage('INFO', _tk('record_saved'));
                }

                $redirectUrl = getRecordURL(array('module' => $Module, 'recordid' => $recordId, 'panel' => $_POST['panel']));
            }
            else
            {
                AddSectionMessage($ModuleDefs[$Module]['LINKED_MODULE']['panel'], 'INFO', _tk("linked_{$Module}_saved"));

                $main_recordid = $data[$ModuleDefs[$Module]['LINKED_MODULE']['parent_ids'][$data['link_module']]] ?: $this->request->getParameter('main_recordid');

                $redirectUrl = 'app.php?action='.$ModuleDefs[$data['link_module']]['ACTION'].'&recordid='.$main_recordid.'&panel='.$ModuleDefs[$Module]['LINKED_MODULE']['panel'];
            }

            $this->redirect($redirectUrl);
        }
        elseif ($ModuleDefs[$Module]['SAVERECORD_POST_SAVE'])
        {
            $callback = $ModuleDefs[$Module]['SAVERECORD_POST_SAVE'];

            if (is_array($callback) && method_exists($callback[0], $callback[1]))
            {
                call_user_func($callback, $recordId);
            }
            elseif (is_string($callback) && function_exists($callback))
            {
                call_user_func($callback, $recordId);
            }
            elseif (is_callable($callback)) // for lambdas
            {
                $callback($recordId);
            }
        }
        else
        {
            $this->call('src\generic\controllers\SaveRecordTemplateController', 'ShowSaveRecord', array(
                'aParams' => array(
                    'module'  => $Module,
                    'data'    => $data,
                    'message' => $Output,
                    'form_id' => $this->request->getParameter('form_id')
                )
            ));
        }
    }

    /**
     * Calls module-specific data validation functions (if any).
     *
     * @global array  $ModuleDefs
     *
     * @param  string $module      The module code.
     *
     * @return array  $error       Any generated error messages.
     */
    public function validateData($module)
    {
        global $ModuleDefs;

        $error = ValidatePostedDates($module);

        $MoneyError = ValidatePostedMoney($module);

        if (is_array($MoneyError))
        {
            $error = array_merge($error, $MoneyError);
        }

        if ($ModuleDefs[$module]['DATA_VALIDATION_INCLUDES'])
        {
            foreach ($ModuleDefs[$module]['DATA_VALIDATION_INCLUDES'] as $includeFile)
            {
                require_once $includeFile;
            }
        }

        if ($ModuleDefs[$module]['DATA_VALIDATION_FUNCTIONS'])
        {
            foreach ($ModuleDefs[$module]['DATA_VALIDATION_FUNCTIONS'] as $function)
            {
                $newError = $function();

                if (is_array($newError))
                {
                    $error = array_merge($error, $newError);
                }
            }
        }

        return $error;
    }

    /**
     * Parses data for Root causes read y for saving. Similar to ParseRootCauses, but allows generic modules to use if required.
     *
     * @param string $aParams Data to parse
     *
     * @return mixed data including parsed root causes.
     */
    private function ParseRootCausesGeneric($aParams)
    {
        $module = $aParams['module'];

        if ($this->request->getParameter('numrootcauses')) //otherwise no data sent - means readonly or hidden
        {
            // Create root causes list from radio buttons
            // only if global INC_INV_RC is NOT set.
            // Also, check to see if $_POST["inc_root_causes"] is set:
            // if it is, use this instead of trying the other values
            if ($this->request->getParameter(\UnicodeString::strtolower($module).'_root_causes'))
            {
                $root_cause_array = \Sanitize::SanitizeRaw($this->request->getParameter(\UnicodeString::strtolower($module).'_root_causes'));
            }
            else
            {
                $NumRootCauses = $this->request->getParameter('numrootcauses');

                if (GetParm('INC_INV_RC', 'N') == 'N')
                {
                    $cause_group = 1;

                    while ($cause_group <= $NumRootCauses)
                    {
                        if (($cause = \Sanitize::SanitizeRaw($this->request->getParameter(\UnicodeString::strtolower($module)."rootcause_$cause_group"))) != '')
                        {
                            $root_cause_array[] = $cause;
                        }

                        $cause_group ++;
                    }
                }
                else
                {
                    for ($i = 1; $i < $NumRootCauses+1; $i++)
                    {
                        if ($RootCause = \Sanitize::SanitizeRaw($this->request->getParameter(\UnicodeString::strtolower($module)."rootcause_$i")))
                        {
                            $root_cause_array[] = $RootCause;
                        }
                    }
                }
            }

            if ($root_cause_array)
            {
                $root_causes = implode(" ", $root_cause_array);
            }

            \UnicodeString::ltrim($root_causes);

            if (!$this->request->getParameter(\UnicodeString::strtolower($module)."_root_causes"))
            {
                $aParams['data'][\UnicodeString::strtolower($module).'_root_causes'] = $root_causes;
            }
        }

        return $aParams['data'];
    }

    /**
     * Removes validation messages for contact fields that were hidden at the time of submission.
     * @param $contactValidationErrors
     * @param Request $request
     * @return mixed
     */
    public static function RemoveHiddenContactValidation($contactValidationErrors, Request $request)
    {
        foreach ($contactValidationErrors as $field => $validationError)
        {
            if ($request->getParameter('show_field_'.$field) == '0')
            {
                unset($contactValidationErrors[$field]);
            }
        }

        return $contactValidationErrors;
    }
}