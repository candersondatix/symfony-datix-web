<?php

namespace src\generic\controllers;

use PermissionDeniedException;
use src\framework\controller\TemplateController;

class RecordTemplateController extends TemplateController
{
    /**
     * Displays Generate options form for generating module records from another module.
     */
    public function generaterecord()
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $module = \Sanitize::getModule($this->request->getParameter('module'));
        $recordid = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));

        $imageFileName = $ModuleDefs[$module]['ICON'];

        $this->hasMenu = false;
        $this->title = _tk('generate_selected_records');
        $this->image = 'Images/'.$imageFileName;

        // Display the section    
        $CTable = $this->GenerateOptionsSection($module);

        $this->response->build('src\generic\views\GenerateRecord.php', array(
            'module' => $module,
            'recordid' => $recordid,
            'CTable' => $CTable
        ));
    }

    /**
     * Outputs Generate options section for Generate options form.
     *
     * @param string $module Current module from which to generate from.
     *
     * @throws PermissionDeniedException
     *
     * @return \FormTable The Generate options section.
     */
    private function GenerateOptionsSection($module)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $CTable = new \FormTable();
        $FieldObj = new \FormField();
        $CTable->MakeTitleRow('<b>' . _tk('generate_options') . '</b>');

        $excludeArray = array();

        foreach (array_keys(getModArray()) as $checkMod)
        {
            $genModPerms = GetParm($ModuleDefs[$checkMod]['PERM_GLOBAL']);
    
            if (!is_array($ModuleDefs[$module]['GENERATE_MAPPINGS'][$checkMod]) || empty($ModuleDefs[$checkMod]['LEVEL1_PERMS']) || empty($genModPerms))
            {
                $excludeArray[] = $checkMod;  //modules not to be shown
            }
            else
            {
                $keepArray[] = $checkMod;  //modules to be shown
            }
        }

        if (is_array($keepArray))
        {
            foreach ($keepArray as $checkMod)
            {
                //get the available approval status's that the user has perms to 'generate to'
                $codesList = GetLevelsTo($checkMod, GetAccessLevel($checkMod), 'NEW');

                if (is_array($codesList) && !empty($codesList))
                {
                    //if there are codes continue,
                    continue;
                }
                else
                {
                    //otherwise there are no available status's and exclude this module
                    $excludeArray[] = $checkMod;
                }

            }
        }

        if (count(getModArray()) == count($excludeArray))
        {
            throw new PermissionDeniedException('You do not have permission to view this page');
        }

        //This makes the coded list of appropriate modules to generate to
        $CTable->MakeRow("<img src='images/Warning.gif' alt='Warning'>&nbsp;<b>" . _tk('generate_module') . "</b>", $FieldObj->MakeCustomField(getModuleDropdown(array(
                'name' => 'generate_module',
                'not' => $excludeArray,
                'include_choose' => false,
                'onchange' => "jQuery('#relevant_approval_status_title').clearField()",
                'checkParms' => true
            )))
        );

        //This makes the list of appropriate approval status's based on user perms and selected module
        $approvalStatusSelect = \Forms_SelectFieldFactory::createSelectField('relevant_approval_status', $module, _tk('generate_default_approval'), 'mode', false);
        $approvalStatusSelect->setParents(array('generate_module'));
        $approvalStatusSelect->setSelectFunction('getRelevantApprovalStatus');
        $approvalStatusSelect->setDisableCache();
        $CTable->MakeRow("<b>" . _tk('approval_status') . "</b>", $approvalStatusSelect);

        if ($error)
        {
            $CTable->Contents .= '<font color="red"><b>' . $error['message'] . '</b></font><br /><br />';
        }

        //tick box options
        $CTable->MakeRow("<b>" . _tk('copy_linked_contacts') . "</b>", $FieldObj->MakeDivCheckbox("copy_linked_contacts", "", true));
        $CTable->MakeRow("<b>" . _tk('generate_link_to_master') . "</b>", $FieldObj->MakeDivCheckbox("generate_link_to_master", "", true));

        $CTable->MakeTable();

        return $CTable;
    }

    /**
     * Displays confirmation that generation has succeeded.
     *
     * @param string $module Module short name of source record.
     * @param string $generate_module Module short name of destination record.
     * @param int $GeneratedRecordID Recordid on new generated record.
     */
    function ShowGenerateFinished($aParams, $GeneratedRecordID)
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $aParams = $this->request->getParameter('aParams');
        $GeneratedRecordID = $this->request->getParameter('generatedRecordID');

        $module = $aParams['module'];
        $generate_module = $aParams['generate_module'];

        $this->hasMenu = false;
        $this->title = 'Records copied';

        $this->response->build('src\generic\views\ShowGenerateFinished.php', array(
            'REC_NAME' => $ModuleDefs[$generate_module]['REC_NAME'],
            'GeneratedRecordID' => $GeneratedRecordID,
            'module' => $module,
            'action_src_record' => $ModuleDefs[$module]['ACTION'],
            'action_generated_record' => $ModuleDefs[$generate_module]['ACTION'],
            'recordid' => $aParams['recordid']
        ));
    }
}