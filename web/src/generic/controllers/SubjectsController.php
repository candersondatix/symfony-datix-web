<?php

namespace src\generic\controllers;

use src\framework\controller\Controller;

class SubjectsController extends Controller
{
    public function DoSubjectSection()
    {
        require_once 'Source/generic/Subjects.php';

        $data = $this->request->getParameter('data');
        $FormType = $this->request->getParameter('FormType');
        $module = $this->request->getParameter('module');
        $ExtraParams = ($this->request->getParameter('extraParameters') ? $this->request->getParameter('extraParameters') : array());

        // Need to work out how many subjects to display, and then display them.
        $aSubjects = array();

        if ($FormType != 'Search')
        {
            if (isset($data['error']))
            {
                $aSubjects = $this->getLinkedSubjectsFromPost($data, $module);
            }
            else
            {
                $aSubjects = $this->getLinkedSubjects(array('recordid' => $data['recordid'], 'module' => $module));
            }
        }

        $NumSubjects = count($aSubjects);

        if (count($aSubjects) == 0) //no linked subjects
        {
            if($FormType != 'Search')
            {
                $data = array();
            }

            // Want defaults in subject fields if this is a "new" subject section, even if the parent form isn't new.
            $NumSubjects = 1;
        }

        if ($FormType != 'Print' && $FormType != 'ReadOnly' && $FormType != 'Search')
        {
            $spellChecker = $_SESSION["Globals"]["WEB_SPELLCHECKER"] == "Y" ? 'true' : 'false';
        }

        $this->response->build('src\generic\views\SubjectSection.php', array(
            'aSubjects' => $aSubjects,
            'module' => $module,
            'FormType' => $FormType,
            'ExtraParams' => $ExtraParams,
            'data' => $data,
            'NumSubjects' => $NumSubjects,
            'spellChecker' => $spellChecker,
            'form_id' => $this->request->getParameter('form_id')
        ));
    }

    private function getLinkedSubjects($aParams)
    {
        require_once 'Source/generic/Subjects.php';

        $module = $aParams['module'];
        $recordid = $aParams['recordid'];
        $aSubjects = array();

        if ($module && $recordid)
        {
            // Get ordered subjects
            if ($module == 'PAL')
            {
                $Rows = getSubjectFieldsForSave(array('module' => $module));
                $sql = '
                    SELECT
                        recordid as subject_id,
                        '.implode(', ', $Rows['Rows']).'
                    FROM
                        pals_subjects
                    WHERE
                        psu_pal_id = '.$recordid.'
                        AND
                        listorder > 0
                    ORDER BY
                        listorder, recordid
                ';
            }
            elseif ($module == 'COM')
            {
                $Rows = getSubjectFieldsForSave(array('module' => $module));
                $sql = '
                    SELECT
                        recordid as subject_id,
                        '.implode(', ', $Rows['Rows']).'
                    FROM
                        VW_COMPL_SUBJ_WEB
                    WHERE
                        com_id = '.$recordid.'
                        AND
                        listorder > 0
                    ORDER BY
                        listorder, recordid
                ';
            }

            $Subjects = \DatixDBQuery::PDO_fetch_all($sql);

            foreach ($Subjects as $row)
            {
                $aSubjects[] = $row;
            }

            // Get unordered subjects
            if($module == 'PAL')
            {
                $Rows = getSubjectFieldsForSave(array('module' => $module));
                $sql = '
                    SELECT
                        recordid as subject_id,
                        '.implode(', ', $Rows['Rows']).'
                    FROM
                        pals_subjects
                    WHERE
                        psu_pal_id = '.$recordid.'
                        AND
                        (listorder = 0 OR listorder IS NULL)
                    ORDER BY
                        recordid
                ';
            }
            elseif ($module == 'COM')
            {
                $Rows = getSubjectFieldsForSave(array('module' => $module));
                $sql = '
                    SELECT
                        recordid as subject_id,
                        '.implode(', ', $Rows['Rows']).'
                    FROM
                        VW_COMPL_SUBJ_WEB
                    WHERE
                        com_id = '.$recordid.'
                        AND
                        (listorder = 0 OR listorder IS NULL)
                    ORDER BY
                        recordid
                ';
            }

            $Subjects = \DatixDBQuery::PDO_fetch_all($sql);

            foreach ($Subjects as $row)
            {
                $aSubjects[] = $row;
            }
        }

        return $aSubjects;
    }

    /**
     * Creates an array of subject records from POST data.
     * Used to populate the Subjects section when returning to the form after a validation error.
     *
     * @param  array  $data     The POST data.
     * @param  string $module   The current module.
     * @return array  $subjects
     */
    private function getLinkedSubjectsFromPost($data, $module)
    {
        global $FieldDefs;

        require_once 'Source/generic/Subjects.php';

        $subjects = array();
        $fields = getSubjectFieldsForSave(array('module' => $module));

        for ($i = 1; $i < $data[\UnicodeString::strtolower($module).'_subject_max_suffix']; $i++)
        {
            $subject = array();

            foreach ($fields['Rows'] as $field)
            {
                $subject[$field] = $field == 'listorder' ? $data[\UnicodeString::strtolower($module).'_subject_listorder'.'_'.$i] : $data[$field.'_'.$i];

                if ($FieldDefs[$module][$field]['Type'] == 'textarea')
                {
                    //hack to deal with timestamped notes fields
                    if ($data['CURRENT_'.$field.'_'.$i])
                    {
                        $subject['CURRENT_'.$field.'_'.$i] = $data['CURRENT_'.$field.'_'.$i];
                    }
                    else
                    {
                        $subject['CURRENT_'.$field.'_'.$i] = '';
                    }
                }
            }

            $subjects[] = $subject;
        }

        return $subjects;
    }
}