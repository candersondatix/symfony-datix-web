<?php
namespace src\generic\controllers;

use src\admin\model\systemFlags\RecordUpdateEmailDatetimeFlag;
use src\admin\model\SystemState;
use src\framework\controller\TemplateController;

class HomeScreens extends TemplateController
{
    /**
     * @desc Main homescreen function - accessed by action=home. Displays the appropriate
     * page depending on the module we are in.
     */
    function home()
    {
        global $ModuleDefs, $dtxtitle, $scripturl, $MinifierDisabled;

        $this->hasMenu = false;
        $this->hasPadding = false;

        $addMinExtension = ($MinifierDisabled ? '' : '.min');

        //Pick up module requested by the user.
        $UserModule = FirstNonNull(array($_GET['module'], GetParm('LOGIN_DEFAULT_MODULE')));

        $ModuleToDisplay = GetDefaultModule(array('default' => $UserModule, 'needs_homescreen' => true));

        if($ModuleToDisplay == 'DAS')
        {
            $this->redirect($scripturl.'?action=dashboard');
        }

        if ($ModuleToDisplay == 'TOD')
        {
            $this->redirect($scripturl.'?action=list&module=TOD');
        }

        /*
        I think this should go further in the future - this script should be able to handle both Modules and Module Groups, but for now
        this is the least disruptive way of getting this in at this point in time. Refactoring ticket DW-6608 added to cover this in the future.
        */
        if($ModuleToDisplay == 'ACR')
        {
            $ModuleToDisplay = 'AMO';
        }
        if($ModuleToDisplay == 'CQC')
        {
            $ModuleToDisplay = 'CQO';
        }

        $dtxtitle = $ModuleDefs[$ModuleToDisplay]['MENU_NAME'] ?: $ModuleDefs[$ModuleToDisplay]['NAME'];

        $this->title = $dtxtitle;
        $this->module = $ModuleToDisplay;

        ob_start();

        if($ModuleToDisplay == 'ADM')
        {
            echo $this->GetAdminPage();
        }
        else
        {
            $Perms = GetParm($ModuleDefs[$ModuleToDisplay]['PERM_GLOBAL']);

            $MenuItems = GetMenuItems(array('module' => $ModuleToDisplay));

            $StatusList = ($ModuleDefs[$ModuleToDisplay]['HOME_SCREEN_STATUS_LIST']  && (!$ModuleDefs[$ModuleToDisplay]['INPUT_ONLY_LEVELS'] || !in_array($Perms, $ModuleDefs[$ModuleToDisplay]['INPUT_ONLY_LEVELS']))); //we need a status list on the right hand side of the screen.


            echo '<div id="landing-page" class="row">
                        <div class="col '.($StatusList ? 'gu2' : 'gu3').'">
                                <div class="content">

                                 <h1>'._tk('options').'</h1>
                                    <ul class="icon-bg">';

            foreach($MenuItems as $details)
            {
                if (isset($details['menu_name']))
                {
                    if (displaySectionHeading($details['items']))
                    {
                        echo '<li><div class="links background_pos' . $details['sprite_id'] . '">' . $details['menu_name'] . '</div><ul style="padding-left: 10px;">';

                        foreach ($details['items'] as $Items)
                        {
                            if($Items['condition'] !== false)
                            {
                                echo '<li><div class="nested-links background_pos'.$Items['sprite_id'].'">';

                                if ($Items['onclick'])
                                {
                                    echo '<a href="#" onclick="'.$Items['onclick'].'">';
                                }
                                else
                                {
                                    echo '<a href="'.($Items['external_link'] ? '' : $scripturl.'?').$Items['link'].'"'.($Items['new_window'] ? ' target="_blank"' : '').'>';
                                }

                                echo ($Items['label']).'</a></div></li>';
                            }
                        }

                        echo '</ul></li>';
                    }
                }
                else
                {
                    if($details['condition'] !== false)
                    {
                        echo '<li><div class="links background_pos'.$details['sprite_id'].'">';

                        if ($details['onclick'])
                        {
                            echo '<a href="#" onclick="'.$details['onclick'].'">';
                        }
                        else
                        {
                            echo '<a href="'.($details['external_link'] ? '' : $scripturl.'?').$details['link'].'"'.($details['new_window'] ? ' target="_blank"' : '').'>';
                        }

                        echo _t($details['label']).'</a></div></li>';
                    }
                }
            }

            echo '
                                    </ul>
                                   </div><!--content-->
                                <div class="clearfix"></div>
                        </div><!--col-->';


            if ($StatusList)
            {
                if ($ModuleDefs[$ModuleToDisplay]['APPROVAL_LEVELS'])
                {
                    require_once 'Source/Security/SecurityBase.php';
                    $secGroups = GetUserSecurityGroups($_SESSION['contact_login_id']);
                    if (bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS','N')) && count($secGroups) > 0)
                    {
                        $unorderedApprovalStatuses = array();

                        $sql = 'SELECT
                            perm_value as access_level
                            from sec_groups
                            left join sec_group_permissions on sec_group_permissions.grp_id = sec_groups.recordid and item_code = :item_code
                            WHERE sec_groups.recordid IN ('.implode(' ,', $secGroups).')';

                        $accessLevels = \DatixDBQuery::PDO_fetch_all($sql, array('item_code' => $ModuleDefs[$ModuleToDisplay]['PERM_GLOBAL']), \PDO::FETCH_COLUMN);

                        foreach($accessLevels as $accessLevel)
                        {
                            $unorderedApprovalStatuses = array_merge($unorderedApprovalStatuses, GetLevelstoView($ModuleToDisplay, $accessLevel));
                        }

                        $sql = "
                            SELECT code_approval_status.code as code
                            FROM code_approval_status
                            JOIN link_access_approvalstatus
                                ON
                                  code_approval_status.code = link_access_approvalstatus.code
                                AND
                                  code_approval_status.module = link_access_approvalstatus.module
                                AND
                                  code_approval_status.workflow = link_access_approvalstatus.las_workflow
                            WHERE
                                  code_approval_status.module like '$ModuleToDisplay'
                                AND
                                  code_approval_status.workflow = ".GetWorkflowID($ModuleToDisplay)."
                            ORDER BY code_approval_status.cod_listorder";

                        $orderedApprovalStatuses = \DatixDBQuery::PDO_fetch_all($sql,array(), \PDO::FETCH_COLUMN);

                        foreach ($orderedApprovalStatuses as $orderedApprovalStatusCode)
                        {
                            if (isset($unorderedApprovalStatuses[$orderedApprovalStatusCode]))
                            {
                                $ApprovalStatuses[$orderedApprovalStatusCode] = $unorderedApprovalStatuses[$orderedApprovalStatusCode];
                            }
                        }
                    }
                    else
                    {
                        $ApprovalStatuses = GetLevelstoView($ModuleToDisplay, GetAccessLevel($ModuleToDisplay));
                    }

                    if (is_array($ApprovalStatuses))
                    {
                        unset($ApprovalStatuses['NEW']); //only want listable statuses.

                        $OverdueStatuses = array_keys(getOverdueStatuses(array('module' => $ModuleToDisplay)));

                        foreach($ApprovalStatuses as $Status => $properties)
                        {
                            $NumRecords = CountRecordsGeneric($ModuleToDisplay, "(rep_approved like '$Status')");

                            $ApprovalStatuses[$Status] = array(
                                'Label' => $properties['description'],
                                'Link' => $scripturl.'?action=list&module='.$ModuleToDisplay.'&listref='.$Status,
                                'RecordCount' => $NumRecords,
                                'Colour' => $properties['colour']
                            );

                            if (in_array($Status, $OverdueStatuses))
                            {
                                if(bYN(GetParm($ModuleToDisplay.'_HIDE_OVERDUE_NUMBERS')))
                                {
                                    $Details['NoOverdue'] = CheckRecordsExistGeneric($ModuleToDisplay, "(rep_approved like '$Status')", true, $Status) ? false : true;
                                }
                                else
                                {
                                    $NumRecordsOverdue = CountRecordsGeneric($ModuleToDisplay, "(rep_approved like '$Status')", true, $Status);
                                }

                                $ApprovalStatuses[$Status]['OverdueLink'] = $scripturl.'?action=list&module='.$ModuleToDisplay.'&listref='.$Status.'&overdue=1';
                                $ApprovalStatuses[$Status]['OverdueRecordCount'] = $NumRecordsOverdue;
                            }

                        }
                    }
                }
                else if (is_array($ModuleDefs[$ModuleToDisplay]['HARD_CODED_LISTINGS']))
                {
                    foreach ($ModuleDefs[$ModuleToDisplay]['HARD_CODED_LISTINGS'] as $Listing => $Details)
                    {
                        if((isset($ModuleDefs[$ModuleToDisplay]['WORKFLOW_GLOBAL']) && isset($Details['workflows']) && in_array(GetParm($ModuleDefs[$ModuleToDisplay]['WORKFLOW_GLOBAL'], $ModuleDefs[$ModuleToDisplay]['DEFAULT_WORKFLOW']), $Details['workflows']))
                        || (!isset($ModuleDefs[$ModuleToDisplay]['WORKFLOW_GLOBAL']) || !isset($Details['workflows'])))
                        {
                            $NumRecords = CountRecordsGeneric($ModuleToDisplay, $Details['Where']);
                            $NumRecordsOverdue = 0;
                            if (!empty($Details['OverdueWhere']))
                            {
                                if(bYN(GetParm($ModuleToDisplay.'_HIDE_OVERDUE_NUMBERS')))
                                {
                                    $Details['NoOverdue'] = CheckRecordsExistGeneric($ModuleToDisplay, TranslateWhereCom($Details['OverdueWhere'])) ? false : true;
                                }
                                else
                                {
                                    $NumRecordsOverdue = CountRecordsGeneric($ModuleToDisplay, TranslateWhereCom($Details['OverdueWhere']));
                                }
                            }

                            $ApprovalStatuses[$Listing] = array(
                                'Label' => $Details['Link'],
                                'Link' => $scripturl . '?action=list&amp;module='.$ModuleToDisplay.'&amp;listtype='.$Listing,
                                'RecordCount' => $NumRecords,
                                'OverdueRecordCount' => $NumRecordsOverdue,
                                'Colour' => $Details['Colour']
                            );

                            if(!$Details['NoOverdue'])
                            {
                                $ApprovalStatuses[$Listing]['OverdueLink'] = $scripturl . '?action=list&amp;module='.$ModuleToDisplay.'&amp;listtype='.$Listing.'&overdue=1';
                            }
                        }
                    }
                }

                $pinnedGlobal = $this->registry->getParm($ModuleToDisplay.'_SAVED_QUERIES');

                $hasQueries = (bYN($this->registry->getParm($ModuleToDisplay.'_SAVED_QUERIES_HOME_SCREEN', 'N')) && ! empty($pinnedGlobal)) ? true : false;

                ?>
                <div class="col gu2-2 last">
                <div class="content">
                    <h1><?php echo $hasQueries ? _tk('searches'): _tk('statuses'); ?></h1>
                    <ul class="icon-bg">
                        <?php if(is_array($ApprovalStatuses)) :

                            if($hasQueries): ?><li><div class="status-wrapper bold"><?php echo _tk('statuses'); ?></div></li><?php endif;

                            $first = true;

                            foreach ($ApprovalStatuses as $Status => $Details) :

                                $has_overdue = (!empty($Details['OverdueLink']));

                                ?><li>
                                <div class="status-wrapper<?php echo ($first ? ' first' : ''); ?>">
                                        <span class="overdue-background">
                                            <span class="records-background<?php echo ($has_overdue ? '' : ' no-overdue'); ?>">
                                                <span class="name">
                                                    <a href="<?php echo $Details['Link']; ?>">
                                                        <span class="status-colour-square"<?php echo ($Details['Colour'] ? '  style="background-color:#'.$Details['Colour'].'"' : ''); ?>></span><?php echo _t($Details['Label']); ?>
                                                    </a>
                                                </span><span class="records">
                                                    <a href="<?php echo $Details['Link']; ?>"><?php echo $Details['RecordCount'].' '._tk('records'); ?></a>
                                                </span>
                                            </span><?php // .record-background
                                            if($has_overdue):
                                                ?><span class="overdue">
                                                <a href="<?php echo $Details['OverdueLink']; ?>"><?php echo (bYN(GetParm($ModuleToDisplay.'_HIDE_OVERDUE_NUMBERS')) ? "" : $Details['OverdueRecordCount'] . ' ') ._tk('overdue'); ?></a>
                                                </span><?php
                                            endif; ?>
                                        </span><?php // .overdue-background ?>
                                    <div class="clearfix"></div>
                                </div><!--status-wrapper-->
                                </li><?php

                                $first = false;

                            endforeach;

                        endif;?>
                    </ul>
                    <?php

                    /*
                     * PINNED SAVED QUERIES
                     */
                    if ($hasQueries)
                    {
                        $pinnedQueries = [];

                        foreach(explode(' ', $pinnedGlobal) as $pq)
                        {
                            $pinnedQueries[] = intval($pq);
                        }

                        /* Limit down the query ID's by queries that actually exist. Because a user can effectively
                           put any numbers in here that they want, we need to validate against the database */
                        $pinnedQuerySQL = "SELECT recordid from queries
                                            WHERE sq_module = '".$ModuleToDisplay."'
                                            AND recordid IN(".implode(', ', $pinnedQueries).")";

                        $pinnedQueries = \DatixDBQuery::PDO_fetch_all($pinnedQuerySQL, [], \PDO::FETCH_COLUMN);

                        // We need to go over the $pinnedQueries array to order it the same has configurations panels are
                        $pinnedQueriesOrdered = [];

                        // If this causes a massive performance issue we need to call natsort before saving the values to the global
                        foreach ($pinnedQueries as $query_id)
                        {
                            $SavedQuery = (new \src\savedqueries\model\SavedQueryModelFactory())->getMapper()->findByOldQueryID($query_id);

                            $pinnedQueriesOrdered[$query_id] = htmlspecialchars($SavedQuery->name, ENT_COMPAT, 'UTF-8', false);
                        }

                        natsort($pinnedQueriesOrdered);

                        if (!empty($pinnedQueries))
                        {
                            ?><ul class="icon-bg">
                            <li><div class="status-wrapper bold"><?php echo _tk('pinned_queries'); ?></div></li>
                            <?php foreach ($pinnedQueriesOrdered as $query_id => $queryName) :

                            $SavedQuery = (new \src\savedqueries\model\SavedQueryModelFactory())->getMapper()->findByOldQueryID($query_id);
                            $SavedQueryHasAtPrompt = $SavedQuery->hasAtPrompt();

                            if (bYN(GetParm($ModuleToDisplay.'_SAVED_QUERIES_NUM_RECORDS', 'N')) && ($SavedQueryHasAtPrompt === false || $SavedQueryHasAtPrompt === null))
                            {
                                $query = $SavedQuery->createQuery();
                                $query->select([['COUNT(?) AS num', '*']]);

                                $writer = new \src\framework\query\QueryFactory();
                                $writer = $writer->getSqlWriter();
                                list($sql, $parameters) = $writer->writeStatement($query);

                                $result = \DatixDBQuery::PDO_fetch($sql, $parameters);
                                $numRecords = $result['num'];
                            }

                            $hasRecords = (bYN(GetParm($ModuleToDisplay.'_SAVED_QUERIES_NUM_RECORDS', 'N')) && ($SavedQueryHasAtPrompt === false || $SavedQueryHasAtPrompt === null)) ? true : false;
                            ?>
                            <li>
                                <div class="status-wrapper">
                                <span class="records-background">
                                    <span class="name<?php echo ( ! $hasRecords)? ' no-records' : ''; ?>">
                                        <span class="search-icon"><img src="images/icon_search_blue.gif" alt=""></span><a href="<?php echo "$scripturl?action=executequery&module=$ModuleToDisplay&qry_recordid=".$query_id."&fromhomescreen=1"; ?>"><?php echo $queryName; ?></a>
                                    </span><?php
                                    if ($hasRecords) :
                                        ?><span class="records">
                                        <a href="<?php echo "$scripturl?action=executequery&module=$ModuleToDisplay&qry_recordid=".$query_id."&fromhomescreen=1"; ?>"><?php echo (isset($numRecords) ? $numRecords : '0') .' '._tk('records'); ?></a>
                                        </span><?php
                                    endif; ?>
                                </span><?php // .records-background ?>
                                    <div class="clearfix"></div>
                                </div>

                            </li>
                        <?php endforeach; ?>
                            </ul><?php
                        }
                    }
                    ?></div>
                </div><?php
            }
            ?></div><?php

        }

        $output = ob_get_clean();

        $this->response->build('src\generic\views\HomeScreens.php', array(
            'output' => $output,
        ));
    }

    function GetAdminPage()
    {
        global $ModuleDefs;

        $recordUpdateEmailFlag = new RecordUpdateEmailDatetimeFlag();
        if ($recordUpdateEmailFlag->getState()->isBad())
        {
            AddSessionMessage('ERROR', _tk('record_update_email_error'));
        }

        $isGlobalsAdminHidden = IsCentrallyAdminSys() && !IsCentralAdmin() && bYN(GetParm('GLOBALS_ADMIN_LOCKED', 'N'));

        $AdminLinkArray = array(
            'profile' => array(
                'label' => _t('Profile Settings'),
                'contents' => array(
                    array('label' => _tk('add_new_user'), 'link' => 'action=edituser', 'sprite_id' => 1, 'condition' => IsSubAdmin()),
                    array('label' => _tk('list_users'), 'link' => 'action=listusers', 'sprite_id' => 2, 'condition' => IsSubAdmin()),
                    array('label' => _tk('manage_profiles'), 'link' => 'action=listprofiles', 'sprite_id' => 3, 'condition' => IsFullAdmin()),
                    array('label' => _tk('list_groups'), 'link' => 'action=listgroups', 'sprite_id' => 4, 'condition' => IsSubAdmin() && bYN(GetParm('ADM_GROUP_SETUP', 'Y'))),
                    array('label' => _tk('change_pwd'), 'link' => 'action=password', 'sprite_id' => 5, 'condition' => !bYN(GetParm('WEB_NETWORK_LOGIN')) && $_SESSION['CurrentUser']->con_sid == ''),
                    array('label' => _tk('approve_reg_reqs'), 'link' => 'action=listawaitingapproval', 'sprite_id' => 18, 'condition' => (IsSubAdmin() && bYN(GetParm('DIF_2_REGISTER', 'N')))),
                    array('label' => _tk('view_own_user'), 'link' => 'action=edituser&recordid='.$_SESSION['contact_login_id'], 'sprite_id' => 4, 'condition' => (bYN(GetParm('ADM_VIEW_OWN_USER', 'N'))))
                ),
            ),
            'system' => array(
                'label' => _t('System Settings'),
                'contents' => array(
                    array('label' => _tk('datixweb_config'), 'link' => 'action=setup', 'sprite_id' => 6, 'condition' => IsFullAdmin()),
                    array('label' => _tk('show_config_settings'), 'link' => 'action=showconfig', 'sprite_id' => 7, 'condition' => IsFullAdmin()),
                    array('label' => _tk('show_error_log'), 'link' => 'action=showerrorlog', 'sprite_id' => 8, 'condition' => IsFullAdmin()),
                    array('label' => _tk('show_locked_records'), 'link' => 'action=lockedrecords', 'sprite_id' => 9, 'condition' => bYN(GetParm("RECORD_LOCKING","N"))),
                    array('label' => _tk('show_user_sessions'), 'link' => 'action=usersessions', 'sprite_id' => 10, 'condition' => (IsSubAdmin() && bYN(GetParm('RECORD_LOCKING', 'N')))),
                    array('label' => _tk('import_xml_data'), 'link' => 'action=import_form', 'sprite_id' => 11, 'condition' => (IsFullAdmin() && bYN(GetParm('WEB_XML_IMPORT', 'Y',true)))),
                    array('label' => _tk('import_table_data'), 'link' => 'action=importtabledata', 'sprite_id' => 33, 'condition' => IsFullAdmin()),
                    array('label' => _t('List extra fields'), 'link' => 'action=listextrafields', 'sprite_id' => 2, 'condition' => IsFullAdmin()),
                    array('label' => _tk('globals'), 'link' => 'action=listglobals', 'sprite_id' => 6, 'condition' => (IsFullAdmin() && !$isGlobalsAdminHidden)),
                    array('label' => _tk('public-holidays'), 'link' => 'action=publicholidays', 'sprite_id' => 38,  'condition' => IsFullAdmin()),
                    array('label' => _tk('workflow-administration'), 'link' => 'action=workflowadmin', 'sprite_id' => 42,  'condition' => IsFullAdmin() && (ModIsLicensed('INC') || ModIsLicensed('RAM') || ModIsLicensed('PAL') || ModIsLicensed('COM') || ModIsLicensed('CON'))),
                    array('label' => _tk('ldap_configuration'), 'link' => 'action=ldapconfig', 'sprite_id' => 41,  'condition' => IsFullAdmin()),
                    array(
                        'label' => _t('System Security'),
                        'contents' => array(
                            array('label' => _tk('login_settings-title'), 'link' => 'action=login_settings', 'sprite_id' => 37, 'condition' => IsFullAdmin()),
                            array('label' => _tk('locked-users-title'), 'link' => 'action=locked_users', 'sprite_id' => 36, 'condition' => IsFullAdmin()),
                            array('label' => _tk('full_audit_title'), 'link' => 'action=full_audit', 'sprite_id' => 21, 'condition' => IsFullAdmin()),
                            array('label' => _tk('login_audit'), 'link' => 'action=show_login_audit', 'sprite_id' => 21, 'condition' => IsFullAdmin()),
                            array('label' => _tk('email_audit_title'), 'link' => 'action=email_audit', 'sprite_id' => 21, 'condition' => IsFullAdmin()),
                            array('label' => _tk('password-policy'), 'link' => 'action=password_policy', 'sprite_id' => 6, 'condition' => IsFullAdmin())
                        ),
                        'condition' => true
                    ),
                ),
            ),
            'custom' => array(
                'label' => _t('Custom Settings'),
                'contents' => array(
                    array('label' => _tk('reports_administration'), 'link' => 'action=reportsadminlist', 'sprite_id' => 12, 'condition' => bYN(GetParm('ADM_NO_ADMIN_REPORTS'), 'N') || IsFullAdmin()),
                    array('label' => _tk('document_template_administration'), 'link' => 'action=doctemplateadminlist', 'sprite_id' => 13, 'condition' => (IsFullAdmin() && bYN(GetParm('WORD_MERGE_WEB', 'N')) && (ModIsLicensed('INC') || ModIsLicensed('RAM') || ModIsLicensed('PAL') || ModIsLicensed('COM') || ModIsLicensed('CLA') || ModIsLicensed('STN') || ModIsLicensed('SAB')))),
                    array('label' => _tk('design_forms'), 'link' => 'action=listformdesigns', 'sprite_id' => 14, 'condition' => IsFullAdmin()),
                    array('label' => _tk('design_listing_pages'), 'link' => 'action=listlistingdesigns', 'sprite_id' => 15, 'condition' => IsFullAdmin()),
                    array('label' => _tk('manage_email_templates'), 'link' => 'action=listemailtemplates', 'sprite_id' => 16, 'condition' => (IsFullAdmin() && bYN(GetParm('EMAIL_TEMPLATES', 'N')))),
                    array('label' => _tk('overdue_email'), 'link' => 'action=overdueemails', 'sprite_id' => 17, 'condition' => (IsFullAdmin() && ModIsLicensed('INC'))),
                    array('label' => _tk('code_setups'), 'link' => 'action=codesetupslist', 'sprite_id' => 6, 'condition' => (IsFullAdmin() || $this->CheckSetupAllowed())),
                    array('label' => _tk('combo_linking'), 'link' => 'action=listcombolinks', 'sprite_id' => 34, 'condition' => IsFullAdmin()),
                    array('label' => _tk('list_tag_sets'), 'link' => 'action=listtaggroups', 'sprite_id' => 52, 'condition' => IsFullAdmin() && CanUseCodeTags()),
                    array('label' => _tk('assign_tags'), 'link' => 'action=listtagfields', 'sprite_id' => 53, 'condition' => IsFullAdmin() && CanUseCodeTags()),
                    array('label' => _tk('in05'), 'link' => 'action=in05', 'sprite_id' => 35, 'condition' => (ModIsLicensed('INC') && HasSetupPermissions("INC") && bYN(GetParm('SHOW_NPSA_SETTINGS', 'Y')))),
                    // array('label' => _tk('field_labels'), 'link' => 'action=fieldlabels', 'sprite_id' => 6, 'condition' => (IsFullAdmin() && $_SESSION["WEB_LABELS_ONLY") === true)),
                    array('label' => _tk('excel-import-mapping-profiles'), 'link' => 'action=listimportprofiles', 'sprite_id' => 43, 'condition' => IsFullAdmin() && bYN(GetParm('EXCEL_IMPORT', 'N'))),
                    array('label' => _tk('import-using-profile'), 'link' => 'service=importexcel&event=setupimport', 'sprite_id' => 44, 'condition' => IsFullAdmin() && bYN(GetParm('EXCEL_IMPORT', 'N'))),
                    array('label' => _tk('action_chains'), 'link' => 'action=listactionchains', 'sprite_id' => 46,  'condition' => IsFullAdmin()),
                    array('label' => _tk('task-generator'), 'link' => 'action=listtasks', 'sprite_id' => 47,  'condition' => (IsFullAdmin() && bYN(GetParm("WEB_TRIGGERS","N")))),
                    array('label' => _tk('cqc_location_setup'), 'link' => 'action=cqclocations', 'sprite_id' => 48,  'condition' => (IsFullAdmin() && (ModIsLicensed('CQO') || ModIsLicensed('ACR')))),
                    array('label' => _tk('apply_cqc_outcomes'), 'link' => 'action=cqcselectlocations', 'icon_blue' => 'images/icon_save_blue.gif', 'sprite_id' => 49, 'condition' => (IsFullAdmin() && ModIsLicensed('CQO'))),
                    array('label' => _tk('edit_cqc_templates'), 'link' => 'action=listcqctemplates', 'icon_blue' => 'images/icon_save_blue.gif', 'sprite_id' => 50, 'condition' => (IsFullAdmin() && ModIsLicensed('CQO'))),
                    array('label' => _tk('LOCNamesTitle'), 'link' => 'action=asmlocations', 'icon_blue' => 'images/icon_save_blue.gif', 'sprite_id' => 50, 'condition' => (IsFullAdmin() && (ModIsLicensed('CQO') || ModIsLicensed('ACR')))),
                    array('label' => 'Batch update field set up', 'link' => 'action=excludebatchupdate', 'icon_blue' => 'images/icon_save_blue.gif', 'sprite_id' => 50, 'condition' => IsFullAdmin()),
                    array('label' => _tk('automatically_merge_contacts'), 'link' => 'action=automaticcontactmerge', 'icon_blue' => 'images/icon_save_blue.gif', 'sprite_id' => 50, 'condition' => IsFullAdmin() && bYN(GetParm('AUTOMATIC_CONTACT_MERGE', 'N'))),
                ),
            ),
            'software' => array(
                'label' => _t('Software Settings'),
                'contents' => array(
                    array('label' => _tk('software_licensing'), 'link' => 'action=licence', 'sprite_id' => 19, 'condition' => IsFullAdmin()),
                    array('label' => _tk('send_config_to_datix'), 'link' => 'action=sendconfigxml', 'sprite_id' => 20, 'condition' => IsFullAdmin()),
                    array('label' => _tk('user_access_report'), 'link' => 'action=usergroupreport', 'sprite_id' => 21, 'condition' => IsSubAdmin()),
                    array('label' => _tk('diagnose_system_problems'), 'link' => 'action=diagnosesystemproblems', 'sprite_id' => 21, 'condition' => IsFullAdmin() && bYN($this->registry->getParm('DATIX_SUPPORT_ACCOUNT', 'N'))),
                    array('label' => _tk('help_alt'), 'icon' => 'images/icon_help.gif', 'icon_blue' => 'images/icon_help_blue.gif', 'link' => '?action=helpfinder&module=ADM', 'new_window' => true, 'external_link' => true, 'sprite_id' => 51, 'condition' => IsFullAdmin())
                ),
            ),
        );

        $HTML = '<div id="landing-page" class="row">';

        if (isset($_GET['configSendStatus']))
        {
            switch($_GET['configSendStatus'])
            {
                case 'true':
                    $HTML .= '<script language="javascript" type="text/javascript">jQuery(document).ready(function(){alert("Email has been successfully sent")})</script>';
                    break;
                case 'false':
                    $HTML .= '<script language="javascript" type="text/javascript">jQuery(document).ready(function(){alert("ERROR: Support email address is not set, no email has been sent")})</script>';
                    break;
            }
        }


        foreach ($AdminLinkArray as $col => $colDetails)
        {
            $HTML .= '<div class="col gu1'.($col == array_pop(array_keys($AdminLinkArray)) ? ' final' : '').'">';
            $HTML .= $this->_renderNavSection($colDetails);
            $HTML .= '</div><!--col-->';
        }

        $HTML .= '</div>';

        return $HTML;
    }

    function _renderNavSection(array $section)
    {
        global $scripturl;

        $html = '<div class="content">
                    <h1><span>' . $section['label'] . '</span></h1>

                    <ul class="icon-bg">';

        foreach($section['contents'] as $linkDetails)
        {
            if($linkDetails['condition'] === true)
            {
                if (array_key_exists('contents', $linkDetails) && is_array($linkDetails['contents'])) {
                    $html .= '<li>';
                    $html .= $this->_renderNavSection($linkDetails);
                    $html .= '</li>';
                } else {
                    $link = ($linkDetails['external_link']) ? $linkDetails['link'] : $scripturl.'?'.$linkDetails['link'];
                    $html .= '<li><div class="links background_pos' . $linkDetails['sprite_id'] . '"><a href="' . $link . '"' . ($linkDetails['new_window'] ? ' target="_blank"' : '').'><span>'.$linkDetails['label'].'</span></a></div></li>';
                }
            }
        }

        $html .= '</ul>
                </div><!--content-->';

        return  $html;
    }

    function CheckSetupAllowed()
    {
        $ModArray = getModArray();
        $NotArray = array('DAS','DST','HSA','ADM');

        foreach($ModArray as $code => $name)
        {
            if(HasSetupPermissions($code))
            {
                return true;
            }
        }

        return false;
    }
}
?>