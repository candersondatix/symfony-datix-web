<?php

namespace src\generic\controllers;

use src\framework\controller\TemplateController;

class CopyRecordTemplateController extends TemplateController
{
    /**
     * Displays copy options form for duplicating module records.
     */
    function copyrecord()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $imageFileName = $ModuleDefs[$this->request->getParameter('module')]['ICON'];

        $this->title = _tk('copy_selected_records');
        $this->module = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $recordid = ($this->request->getParameter('recordid') != '' ? \Sanitize::SanitizeInt($this->request->getParameter('recordid')) : null);
        $this->image = 'Images/' . $imageFileName;

        // Get number of records to copy
        if ($recordid)
        {
            $numSelectedRecords = 1;
 	 	}
 	 	else
 	 	{
            $sql = "SELECT recordid, ".implode(', ',$ModuleDefs[$this->module]['FIELD_ARRAY'])." FROM ".$ModuleDefs[$this->module]['TABLE'];

            if (!empty($_SESSION[$this->module]['PROMPT']['ORIGINAL_JOIN']))
            {
                $sql .= " ".$_SESSION[$this->module]['PROMPT']['ORIGINAL_JOIN'];
 	 	 	}

            include_once('Source/libs/SavedQueries.php');
            DoPromptSection(array('module' => $this->module));

            if ($_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"])
            {
                $whereClause = $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"];
                $_SESSION[$_GET['module']]["PROMPT"]["NEW_WHERE"] = '';
            }

            if (empty($whereClause))
            {
                $whereClause = $_SESSION[$_GET['module']]["WHERE"];
            }

            $whereClause = MakeSecurityWhereClause($whereClause, $_GET['module'], $_SESSION['initials']);

            if (!empty($whereClause))
            {
                $sql .= " WHERE ".$whereClause;
 	 	 	}

            $result = \DatixDBQuery::PDO_fetch_all($sql);
 	 	 	$numSelectedRecords = sizeof($result);
        }

        $CopyOptionsSection = $this->CopyOptionsSection();

        AddSessionMessage('INFO', 'You have selected '.$numSelectedRecords.' record(s) to be copied');

        $this->response->build('src/generic/views/CopyRecord.php', array(
            'module' => $this->module,
            'CopyOptionsSection' => $CopyOptionsSection,
            'recordid' => $recordid,
            'numSelectedRecords' => $numSelectedRecords
        ));
    }

    /**
     * Outputs Copy options section for Copy options form.
     */
    private function CopyOptionsSection()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $CTable = new \FormTable();
        $FieldObj = new \FormField();
        $CTable->MakeTitleRow('<b>' . _tk('copy_options') . '</b>');

        $CTable->MakeRow("<b>" . _tk('copies') . "</b>" . GetValidationErrors(array(), 'qry_recordid'), $FieldObj->MakeNumberField("copies", "1", 5, 0, "", 1 ));
        $CTable->MakeRow("<b>" . _tk('copy_extra_fields') . "</b>", $FieldObj->MakeDivCheckbox("copy_extra_fields", "", true));

        if ($ModuleDefs[$this->module]['STANDARD_COPY_CHECK_OPTIONS'] === true || $ModuleDefs[$this->module]['STANDARD_COPY_CHECK_OPTIONS'] === null)
        {
            $CTable->MakeRow("<b>" . _tk('copy_linked_contacts') . "</b>", $FieldObj->MakeDivCheckbox("copy_linked_contacts", "", true));
            $CTable->MakeRow("<b>" . _tk('copy_actions') . "</b>", $FieldObj->MakeDivCheckbox("copy_actions", "", true));
            $CTable->MakeRow("<b>" . _tk('copy_documents') . "</b>", $FieldObj->MakeDivCheckbox("copy_documents", "", true));
            $CTable->MakeRow("<b>" . _tk('copy_notepad') . "</b>", $FieldObj->MakeDivCheckbox("copy_notepad", "", true));
            $CTable->MakeRow("<b>" . _tk('copy_link_to_master') . "</b>", $FieldObj->MakeDivCheckbox("copy_link_to_master", "", true));
        }

        if (!empty($ModuleDefs[$this->module]['ADDITIONAL_COPY_CHECK_OPTIONS']))
        {
            foreach ($ModuleDefs[$this->module]['ADDITIONAL_COPY_CHECK_OPTIONS'] as $option => $default)
            {
                $CTable->MakeRow('<b>' . _tk($option) . '</b>', $FieldObj->MakeDivCheckbox($option, '', $default));
            }
        }

        if (!empty($ModuleDefs[$this->module]['ADDITIONAL_COPY_OVERRIDE_FIELD_OPTIONS']))
        {
            $CTable->MakeTitleRow('<b>Override fields</b>');

            foreach ($ModuleDefs[$this->module]['ADDITIONAL_COPY_OVERRIDE_FIELD_OPTIONS'] as $fieldname)
            {
                $CTable->MakeFieldRow($fieldname, '', '', '', '', '', array(), $this->module);
            }
        }

        $CTable->MakeTable();

        return $CTable;
    }
}