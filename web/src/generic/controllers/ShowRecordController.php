<?php

namespace src\generic\controllers;

use src\framework\controller\Controller;

class ShowRecordController extends Controller
{
    function record()
    {
        $sideMenuModule = ($this->request->getParameter('sidemenu') && $this->request->getParameter('sidemenu') != '' ? \Sanitize::SanitizeString($this->request->getParameter('sidemenu')) : '');

         require_once 'Source/generic/MainRecord.php';

        //Hack to get around the fact that recordid needs to be available in GetGenericMenuItems()
        //TODO: Fix this by passing the recordid properly...
        $_GET['recordid'] = $this->request->getParameter('recordid');

        ShowMainRecord(\Sanitize::SanitizeInt($this->request->getParameter('recordid')), '', $sideMenuModule);
    }
}