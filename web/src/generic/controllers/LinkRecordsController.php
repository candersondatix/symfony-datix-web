<?php
namespace src\generic\controllers;

use src\framework\controller\Controller;

class LinkRecordsController extends Controller
{
    /**
     * Links records
     */
    function linkrecords()
    {
        require_once 'Source/libs/ModuleLinks.php';

        $ModuleDefs = $this->registry->getModuleDefs();

        $current_id = \Sanitize::SanitizeInt($this->request->getParameter('currentID'));
        $current_mod = \Sanitize::SanitizeString($this->request->getParameter('currentMod'));
        $linkNotes = \Sanitize::SanitizeRaw($this->request->getParameter('link_notes'));
        $linked_id = \Sanitize::SanitizeInt($this->request->getParameter('link_id'));
        $linked_mod = \Sanitize::SanitizeString($this->request->getParameter('link_module'));
        $edit_mod = \Sanitize::SanitizeString($this->request->getParameter('editMod'));
        $edit_id = \Sanitize::SanitizeInt($this->request->getParameter('editID'));

        switch ($this->request->getParameter('rbWhat'))
        {
            case 'Link':
                InsertLink($current_id,$current_mod, $linked_id,$linked_mod, $linkNotes);
                break;
            case 'Unlink':
                RemoveLink($current_id, $current_mod, $edit_id, $edit_mod);
                break;
            case 'Add':
                $this->redirect('app.php?action=editlinkedrecord&currentID='.$current_id.'&currentMod='.$current_mod);
                break;
            case 'Edit':
                $this->redirect('app.php?action=editlinkedrecord&currentID='.$current_id.'&currentMod='.$current_mod.'&editMod='.$edit_mod.'&editID='.$edit_id);
                break;
            case 'Save': //saving edited link info
                SaveLink($current_id, $current_mod, $edit_id, $edit_mod, $linkNotes);
                break;
            case 'Cancel':
                $this->redirect('app.php?action='.$ModuleDefs[$current_mod]['ACTION'].'&recordid='.$this->request->getParameter('recordid').'&panel=linked_records');
                break;
        }
    }
}