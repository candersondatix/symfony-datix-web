<?php if ($this->error) : ?>
<div class="form_error">The form you submitted contained the following errors.  Please correct them and try again.</div>
<div class="form_error"><?php echo htmlspecialchars($this->error['message']); ?></div>
<?php endif; ?>
<form enctype="multipart/form-data" method="post" name="frmSABSWebLink" action="<?php echo $this->scripturl; ?>?action=weblinkaction">
    <input type="hidden" name="wlk_id" value="<?php echo htmlspecialchars($this->wlk_id); ?>" />
    <input type="hidden" name="link_id" value="<?php echo htmlspecialchars($this->link_id); ?>" />
    <input type="hidden" name="module" value="<?php echo htmlspecialchars($this->module); ?>" />
    <tr>
        <td><?php echo $this->CTable->GetFormTable(); ?></td>
    </tr>
    <div class="button_wrapper">
        <input type="hidden" name="rbWhat" value="Save" />
        <input type="submit" value="Save" onClick="document.forms[0].rbWhat.value='save';" />
        <input type="submit" value="<?php echo _tk('btn_cancel')?>" onClick="document.forms[0].rbWhat.value='cancel';" />
        <?php if ($this->wlk_id) : ?>
            <input name="rbWhat" type="submit" value="Delete"  onClick="return confirmAndDelete();"/>

            <script>
                function confirmAndDelete ()
                {
                    if (confirm("<?php echo _tk('delete_weblink'); ?>"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            </script>
        <?php endif; ?>
    </div>
</form>