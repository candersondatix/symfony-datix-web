<?php
namespace src\weblinks\controllers;

use src\framework\controller\Controller;

class SaveWebLinkController extends Controller
{
    /**
     * Action to perform on web link form
     */
    public function weblinkaction()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $module = $this->request->getParameter('module');

        if ($this->request->getParameter('rbWhat') == 'cancel')
        {
            $this->redirect('app.php?' . $ModuleDefs[$module]['MAIN_URL'] . '&recordid=' . $this->request->getParameter('link_id') . '&panel=documents');
        }
        elseif ($this->request->getParameter('rbWhat') == ucfirst('delete'))
        {
            $this->DeleteWebLink($this->request->getParameters());
        }
        else
        {
            $this->SaveWebLinkToMain($this->request->getParameters());
        }
    }

    /**
     * Deletes a Web Link
     *
     * @param array $Parameters The form $_POST
     */
    private function DeleteWebLink($Parameters)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $wlk_id = \Sanitize::SanitizeInt($Parameters['wlk_id']);
        $link_id = \Sanitize::SanitizeInt($Parameters['link_id']);
        $module = \Sanitize::getModule($Parameters['module']);

        if ($link_id == '')
        {
            fatal_error('No ID');
        }

        if ($wlk_id)
        {
            $sql = '
                DELETE FROM
                    web_links
                WHERE
                    recordid = :recordid
            ';

            if (!\DatixDBQuery::PDO_query($sql, array('recordid' => $wlk_id)))
            {
                fatal_error('Could not delete web link' . $sql);
            }
        }

        $this->redirect('app.php?' . $ModuleDefs[$module]['MAIN_URL'] .'&recordid=' . $link_id . '&panel=documents');
    }

    /**
     * Saves a Web Link
     *
     * @param array $Parameters The form $_POST
     */
    private function SaveWebLinkToMain($Parameters)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $wlk_id = \Sanitize::SanitizeInt($Parameters['wlk_id']);
        $link_id = \Sanitize::SanitizeInt($Parameters['link_id']);
        $module = \Sanitize::getModule($Parameters['module']);

        if ($link_id == '')
        {
            fatal_error('No ID');
        }

        $ErrorMark = '<font size="3" color="red"><b>*</b></font>';

        if ($Parameters['wlk_type'] == '')
        {
            $error['message'] = 'You must enter a web link type.';
            $error['wlk_type'] = $ErrorMark;
        }

        if ($Parameters['wlk_descr'] == '')
        {
            $error['message'] = 'You must enter web link description.';
            $error['wlk_descr'] = $ErrorMark;
        }

        if ($Parameters['wlk_web_link'] == '')
        {
            $error['message'] = 'You must enter a web link.';
            $error['wlk_web_link'] = $ErrorMark;
        }

        if ($error)
        {
            $_SESSION['WEBLINKS']['error'] = $error;
            $this->response = $this->call('src\\weblinks\\controllers\\WebLinksTemplateController', 'linkweblink', array(
                'module'  => $module,
                'link_id' => $link_id,
                'wlk_id'  => $wlk_id
            ));
            return;
        }

        $wlk = QuotePostArray($Parameters);

        if (!$wlk_id)
        {
            $sql = "
                INSERT INTO
                    web_links (
                        wlk_type, wlk_descr, wlk_web_link,
		                updateddate, updatedby,
                        link_mod, link_id
                    )
		       VALUES (
		            :wlk_type, :wlk_descr, :wlk_web_link,
		            :updateddate, :updatedby,
                    :link_mod, :link_id
                )
            ";

            $PDOParams = array(
                'wlk_type' => $wlk['wlk_type'],
                'wlk_descr' => $wlk['wlk_descr'],
                'wlk_web_link' => $wlk['wlk_web_link'],
                'updateddate' => date('d-M-Y H:i:s'),
                'updatedby' => $_SESSION['initials'],
                'link_mod' => $module,
                'link_id' => $link_id
            );
        }
        else
        {
            $sql = "
                UPDATE
                    web_links
                SET
                    wlk_type = :wlk_type,
                    wlk_descr = :wlk_descr,
                    wlk_web_link = :wlk_web_link,
		            updateddate = :updateddate,
                    updatedby = :updatedby,
                    link_mod = :link_mod,
                    link_id = :link_id
                WHERE
                    recordid = :recordid
            ";

            $PDOParams = array(
                'wlk_type' => $wlk['wlk_type'],
                'wlk_descr' => $wlk['wlk_descr'],
                'wlk_web_link' => $wlk['wlk_web_link'],
                'updateddate' => date('d-M-Y H:i:s'),
                'updatedby' => $_SESSION['initials'],
                'link_mod' => $module,
                'link_id' => $link_id,
                'recordid' => $wlk_id
            );
        }

        if (!\DatixDBQuery::PDO_query($sql, $PDOParams))
        {
            fatal_error('Could not insert web link' . $sql);
        }

        $this->redirect('app.php?' . $ModuleDefs[$module]['MAIN_URL'] . '&recordid=' . $link_id . '&panel=documents');
    }
}