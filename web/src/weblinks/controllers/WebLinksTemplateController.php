<?php
namespace src\weblinks\controllers;

use src\framework\controller\TemplateController;

class WebLinksTemplateController extends TemplateController
{
    /**
     * Shows web links form
     */
    public function linkweblink()
    {
        $ModuleDefs = $this->registry->getModuleDefs();
        $error = ($_SESSION['WEBLINKS']['error'] ? $_SESSION['WEBLINKS']['error'] : '');

        // Clear error before processing
        unset($_SESSION['WEBLINKS']['error']);

        if ($error != '')
        {
            // The ID of the documents_main record itself
            $wlk_id = \Sanitize::SanitizeInt($this->request->getParameter('doc_id'));
            // The module code e.g. RAM, AST, etc.
            $module = \Sanitize::getModule($this->request->getParameter('module'));
            // The Id of the record to link the document to
            $link_id = \Sanitize::SanitizeInt($this->request->getParameter('link_id'));
            $wlk['wlk_type'] = $this->request->getParameter('wlk_type');
            $wlk['wlk_descr'] = $this->request->getParameter('wlk_descr');
            $wlk['wlk_web_link'] = $this->request->getParameter('wlk_web_link');
        }
        else
        {
            // The ID of the documents_main record itself
            $wlk_id = \Sanitize::SanitizeInt($this->request->getParameter('wlk_id'));
            // The module code e.g. RAM, AST, etc.
            $module = \Sanitize::getModule($this->request->getParameter('module'));
            // The Id of the record to link the document to
            $link_id = \Sanitize::SanitizeInt($this->request->getParameter('link_id'));
        }

        $LinkTypes = array();

        // Get link types for "Link as" drop down
        $sql = '
            SELECT
                cod_code,
                cod_descr
            FROM
                code_types
            WHERE
                cod_type = \'WLKTYPE\'
                AND
                (cod_priv_level <> \'N\' OR cod_priv_level IS NULL)
            ORDER BY
                cod_listorder, cod_descr
        ';

        $result = \DatixDBQuery::PDO_fetch_all($sql, array());

        foreach ($result as $LinkType)
        {
            $LinkTypes[$LinkType['cod_code']] = $LinkType['cod_descr'];
        }

        if ($wlk_id)
        {
            // Open existing document recordid
            $sql = "
                SELECT
                    wlk.recordid AS wlk_id,
                    wlk.WLK_TYPE AS wlk_type,
                    wlk.WLK_DESCR AS wlk_descr,
                    wlk_web_link,
                    typ.cod_descr AS type_descr,
                    wlk.link_id AS sab_id
                FROM
                    web_links AS wlk,
                    code_types typ
                WHERE
                    wlk.wlk_type = typ.cod_code
                    AND
                    typ.cod_type = 'WLKTYPE'
                    AND
                    wlk.link_mod = 'SAB'
                    AND
                    wlk.recordid = :wlk_id";

            $wlk = \DatixDBQuery::PDO_fetch($sql, array('wlk_id' => $wlk_id));
        }
        else
        {
            // New document, only get evidence id
            $wlk[$ModuleDefs[$module]['FK']] = $link_id;
        }

        $wlk['linktypes'] = $LinkTypes;
        $wlk['error'] = $error;

        $this->title = 'New web link';
        $this->module = $module;
        $this->image = 'Images/weblink24.png';
        $this->hasPadding = false;

        $CTable = $this->WebLinksDetailsSection($wlk);

        $this->response->build('src/weblinks/views/LinkWebLink.php', array(
            'error' => $error,
            'wlk_id' => $wlk_id,
            'link_id' => $link_id,
            'module' => $module,
            'CTable' => $CTable
        ));
    }

    private function WebLinksDetailsSection($wlk)
    {
        $LinkTypes = $wlk['linktypes'];

        $CTable = new \FormTable();
        $FieldObj = new \FormField();
        $CTable->MakeTitleRow('<b>Web link details</b>');

        if ($wlk['error'])
        {
            $CTable->Contents .= '<font color="red"><b>' . $wlk['error']['message'] . '</b></font><br /><br />';
        }

        $field = \Forms_SelectFieldFactory::createSelectField('wlk_type', '', $wlk['wlk_type'], null);
        $field->setCustomCodes($LinkTypes);
        $CTable->MakeRow($wlk['error']['wlk_type'] . "Link as", $field);
        $CTable->MakeRow($wlk['error']['wlk_descr'] . "Description", $FieldObj->MakeTextAreaField('wlk_descr', 3, 70, 254, $wlk['wlk_descr']));
        $CTable->MakeRow($wlk['error']['wlk_web_link'] . "Web link", $FieldObj->MakeTextAreaField('wlk_web_link', 3, 70, 254, $wlk['wlk_web_link']));

        $CTable->MakeTable();

        return $CTable;
    }
}