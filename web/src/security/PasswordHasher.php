<?php
namespace src\security;
use src\passwords\exceptions\InvalidPasswordEncryptionTypeException;

/**
 * Class PasswordHasher
 * Covers encryption of old php md5, sql md5, sql sha1 and bcrypt
 * Seemed overkill to split into a separate class for each encryption type, but we can do this later if needed.
 */
class PasswordHasher
{
    protected $type;

    /**
     * @param string $type ('MD5', 'E', 'SHA1', 'M' or 'BCRYPT')
     * @param \DatixDBQuery $db
     */
    public function __construct($type = "SHA1", \DatixDBQuery $db = null)
    {
        $this->type = $type;
        $this->db = $db ?: new \DatixDBQuery();
    }

    /**
     * Hashes a password using the encryption type set at the constructor
     * @param string $plain password
     * @throws \src\passwords\exceptions\InvalidPasswordEncryptionTypeException
     * @return string hash
     */
    public function hash($plain)
    {
        switch($this->type)
        {
            case 'MD5':
                $hashed = strtoupper($this->db->PDO_fetch('SELECT CAST(HashBytes(\'MD5\', \'' . str_replace("'", "''", $plain) . '\') as VARBINARY) as hashedpwd', array(), \PDO::FETCH_COLUMN));
                break;

            case 'SHA1':
                $hashed = strtoupper($this->db->PDO_fetch('SELECT CAST(HashBytes(\'SHA1\', \'' . str_replace("'", "''", $plain) . '\') as VARBINARY) as hashedpwd', array(), \PDO::FETCH_COLUMN));
                break;

            case 'BCRYPT':
                require_once __DIR__.'/../../thirdpartylibs/password_compat/lib/password.php';
                $hashed = password_hash($plain, PASSWORD_BCRYPT);
                break;

            case 'M':
                $key = 'DATIX';

                if (\UnicodeString::strlen($key) > 64)
                {
                    $key = pack('H*', md5($key));
                }

                $key  = str_pad($key, 64, chr(0x00));

                $k_ipad = $key ^ str_repeat(chr(0x36), 64);
                $k_opad = $key ^ str_repeat(chr(0x5c), 64);

                $hashed = md5($k_opad . pack('H*', md5($k_ipad . $plain)));

                break;

            default:
                throw new InvalidPasswordEncryptionTypeException();
                break;
        }

        return $hashed;
    }

    /**
     * Validates password against hash
     * @param string $password plain text
     * @param string $hash hashed password
     * @return bool
     */
    public function validate ($password, $hash)
    {
        if ($this->type == 'BCRYPT')
        {
            require_once __DIR__.'/../../thirdpartylibs/password_compat/lib/password.php';
            return password_verify ($password, $hash);
        }
        else
        {
            return ($hash == $this->hash($password));
        }
    }

    /**
     * Checks whether $password exists in list
     * @param array $password_list hashed passwords
     * @param string $password plain text
     * @return bool
     */
    public function isPasswordInList ($password_list, $password)
    {
        if ($this->type == 'BCRYPT')
        {
            foreach ($password_list as $item)
            {
                if ($this->validate($password, $item)) {
                    return true;
                }
            }

            return false;
        }
        else
        {
            $hash = $this->hash($password);
            return in_array($hash, $password_list);
        }
    }
}