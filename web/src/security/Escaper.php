<?php
namespace src\security;

use thirdpartylibs\OWASP\Reform;

class Escaper
{

    public static function escapeForHTML($data)
    {
        return Reform::HtmlEncode($data);
    }

    public static function escapeForHTMLParameter($data)
    {
        return Reform::HtmlAttributeEncode($data);
    }

    public static function escapeForJS($data)
    {
        return Reform::JsString($data);
    }
}