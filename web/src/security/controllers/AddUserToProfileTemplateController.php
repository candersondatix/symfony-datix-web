<?php

namespace src\security\controllers;

use src\framework\controller\TemplateController;

class AddUserToProfileTemplateController extends TemplateController
{
    function add_user_to_profile()
    {
        require_once 'Source/security/SecurityUserGroupLinks.php';

        $Link['pfl_id'] = \Sanitize::SanitizeInt($this->request->getParameter('pfl_id'));
        $Action = \Sanitize::SanitizeString($this->request->getParameter('action'));

        $this->title = 'Add user';
        $this->module = 'ADM';

        $this->response->build('src/security/views/AddUserToProfile.php', array(
            'Action' => $Action,
            'Link' => $Link
        ));
    }
}