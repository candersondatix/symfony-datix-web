<?php
namespace src\security\controllers;

use src\framework\controller\Controller;

class SaveUserGroupLinkController extends Controller
{
    /**
     * Attaches users to a profile
     */
    function save_user_group_link()
    {

		$Link = \Sanitize::SanitizeRawArray($this->request->getParameters());
	
		if ($Link["rbWhat"] == 'cancel')
		{
	        if ($Link['action'] == 'add_group_for_user')
	        {
	        	$this->redirect('app.php?action=edituser&recordid='.$Link['con_id']);
	        }
			elseif ($Link['action'] == 'add_group_for_profile')
	        {
	        	$this->redirect('app.php?action=editprofile&recordid='.$Link['pfl_id']);
	        }
			else
	        {
	        	$this->redirect('app.php?action=editgroup&panel=users&grp_id='.$Link['grp_id']);
	        }
		}
		else
		{
			if (!$Link['grp_id'])
			{
				AddSessionMessage('ERROR', _tk('select_group_err'));
				if($Link['pfl_id']) {
                    //Return user to linked profile's record via id
                    $this->redirect('app.php?action=add_group_for_profile&pfl_id=' . $Link['pfl_id']);
                } else if($Link['con_id']) {
                    //Return user to linked users's record via id
                    $this->redirect('app.php?action=add_group_for_user&recordid=' . $Link['con_id']);
                } else {
                    //Return user to new user record
                    $this->redirect('app.php?action=add_group_for_user');
                }

			}
			elseif ($Link['action'] == 'add_user_to_group' && !$Link['con_id'])
			{
				require_once 'Source/security/SecurityGroups.php';
	            AddSessionMessage('ERROR', 'Please select the users you wish to add to this group.');
                $this->redirect('app.php?action=add_user_to_group&grp_id=' . $Link["grp_id"]);
			}
	
	        if ($Link['action'] == "add_user_to_group")
	        {
	            // Check whether a link already exists between the users and the group
	            $sql = "SELECT
	                contacts_main.fullname,
	                sec_groups.grp_code
	            FROM
	                sec_staff_group
	                join sec_groups on sec_staff_group.grp_id = sec_groups.recordid
	                join contacts_main on sec_staff_group.con_id = contacts_main.recordid
	            WHERE
	                grp_id = $Link[grp_id]
	                AND con_id IN (".implode(', ', $Link['con_id']).")";
	        }
	        elseif ($Link['action'] == "add_group_for_user")
	        {
			    // Check whether a link already exists between the groups and the user
			    $sql = "SELECT
					contacts_main.fullname,
					sec_groups.grp_code
				FROM
					sec_staff_group
					join sec_groups on sec_staff_group.grp_id = sec_groups.recordid
					join contacts_main on sec_staff_group.con_id = contacts_main.recordid
				WHERE
					grp_id IN (".implode(', ', $Link['grp_id']).")
					AND con_id = " . $Link['con_id'];
	        }
	        elseif ($Link['action'] == "add_group_for_profile")
	        {
	            // Check whether a link already exists between the groups and the profile
	            $sql = "SELECT
	                profiles.pfl_name,
	                sec_groups.grp_code
	            FROM
	                link_profile_group
	                join sec_groups on link_profile_group.lpg_group = sec_groups.recordid
	                join profiles on link_profile_group.lpg_profile = profiles.recordid
	            WHERE
	                lpg_group IN (".implode(', ', $Link['grp_id']).")
	                AND lpg_profile = $Link[pfl_id]";
	        }
	
			if ($Link['recordid'])
			{
				$sql .= " AND recordid != $Link[recordid]";
			}
	
			
			$row = \DatixDBQuery::PDO_fetch($sql);
			
			if ($row)
			{
	            if ($Link['action'] == "add_group_for_profile")
	            {
	                AddSessionMessage('ERROR', 'The profile <b>"' . $row['pfl_name'] . '"</b> is already linked to at least one of the groups you selected.');
	            }
	            else
	            {
	                AddSessionMessage('ERROR', 'The user <b>"' . $row['fullname'] . '"</b> is already a member of at least one of the groups you selected.');
				}
	
				$this->redirect('app.php?action=add_group_for_user');
			}
	
	        if ($Link['action'] == "add_user_to_group")
	        {
	            foreach ($Link["con_id"] as $ConID)
	            {
	                if ($Link['recordid'])
			        {
				        $sql = "update sec_staff_group set
				        grp_id = $Link[grp_id],
	                    con_id = $ConID,
				        updatedby = '$_SESSION[initials]',
				        updateddate = '" . date('d-M-Y H:i:s') . "',
				        updateid = '" . GensUpdateID($row["updateid"]) . "'
				        where
				        recordid = $Link[recordid]";
			        }
			        else
			        {
				        $sql = "insert into sec_staff_group (grp_id, con_id, updatedby, updateddate, updateid)
				         values ( $Link[grp_id],
				         $ConID,
				         '$_SESSION[initials]',
				         '" . date('d-M-Y H:i:s') . "',
				         '" . GensUpdateID($row["updateid"]) . "')";
			        }
	
		            \DatixDBQuery::PDO_query($sql);
	            }
	        }
	        elseif ($Link['action'] == "add_group_for_user")
	        {
	            foreach ($Link["grp_id"] as $GrpID)
	            {
	                if ($Link['recordid'])
	                {
	                    $sql = "update sec_staff_group set
	                    grp_id = $GrpID,
	                    con_id = $Link[con_id],
	                    updatedby = '$_SESSION[initials]',
	                    updateddate = '" . date('d-M-Y H:i:s') . "',
	                    updateid = '" . GensUpdateID($row["updateid"]) . "'
	                    where
	                    recordid = $Link[recordid]";
	                }
	                else
	                {
	                    $sql = "insert into sec_staff_group (grp_id, con_id, updatedby, updateddate, updateid)
	                    values ( $GrpID,
	                    $Link[con_id],
	                    '$_SESSION[initials]',
	                    '" . date('d-M-Y H:i:s') . "',
	                    '" . GensUpdateID($row["updateid"]) . "')";
	                }
	
	                \DatixDBQuery::PDO_query($sql);
	            }
	        }
	        elseif ($Link['action'] == "add_group_for_profile")
	        {
	            foreach ($Link["grp_id"] as $GrpID)
	            {
	                if ($Link['recordid'])
	                {
	                    $sql = "update link_profile_group set
	                    lpg_group = $GrpID,
	                    lpg_profile = $Link[pfl_id],
	                    updatedby = '$_SESSION[initials]',
	                    updateddate = '" . date('d-M-Y H:i:s') . "',
	                    updateid = '" . GensUpdateID($row["updateid"]) . "'
	                    where
	                    recordid = $Link[recordid]";
	                }
	                else
	                {
	                    $sql = "insert into link_profile_group (lpg_group, lpg_profile, updatedby, updateddate, updateid)
	                    values ( $GrpID,
	                    $Link[pfl_id],
	                    '$_SESSION[initials]',
	                    '" . date('d-M-Y H:i:s') . "',
	                    '" . GensUpdateID($row["updateid"]) . "')";
	                }
	
	                \DatixDBQuery::PDO_query($sql);
	            }
	        }
	
	    }
	    if ($Link['action'] == 'add_group_for_user')
	    {
	    	$this->redirect('app.php?action=edituser&recordid=' . $Link['con_id']);
	    }
		else if ($Link['action'] == 'add_group_for_profile')
	    {
	    	$this->redirect('app.php?action=editprofile&recordid=' . $Link['pfl_id']);
	    }
		else
	    {
	    	$this->redirect('app.php?action=editgroup&panel=users&grp_id=' . $Link['grp_id']);
	    }
    }
}