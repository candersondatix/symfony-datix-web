<?php
namespace src\security\groups\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;

/**
 * Manages the persistance of WordMergeTemplate objects.
 */
class GroupMapper extends DatixEntityMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\security\\groups\\model\\Group';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
    	return 'sec_groups';
    }    
    
    public function delete(Entity $object)
    {
    	
    	$recordid = $object->recordid;
    	
    	// delete relations
    	\DatixDBQuery::PDO_query('DELETE FROM sec_ldap_groups WHERE grp_id = :grp_id', array('grp_id' => $recordid));
    	\DatixDBQuery::PDO_query('DELETE FROM sec_group_permissions WHERE grp_id = :grp_id', array('grp_id' => $recordid));
    	\DatixDBQuery::PDO_query('DELETE FROM sec_staff_group WHERE grp_id = :grp_id', array('grp_id' => $recordid));
    	 
    	
    	parent::delete($object);
    }
    
    

}