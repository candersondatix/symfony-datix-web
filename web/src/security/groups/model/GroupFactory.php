<?php
namespace src\security\groups\model;

use src\framework\model\EntityFactory;

/**
 * Constructs CodeTagGroup objects.
 */
class GroupFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\security\\groups\\model\\Group';
    }
}