<?php
namespace src\security\groups\model;

use src\framework\model\DatixEntity;
use src\framework\query\Query;
use src\framework\registry\Registry;

/**
 * This entity models a user who can log in to the system
 */
class Group extends DatixEntity
{
    /**
     * Group code
     *
     * @var string
     */
    protected $grp_code;

    /**
     * Group description
     *
     * @var string
     */
    protected $grp_description;
    
    /**
     * Group permissions
     *
     * @var string
     */
    protected $permission;
    
    /**
     * Group access start date
     *
     * @var string
     */
    protected $grp_daccessstart;
    
    /**
     * Group access end date
     *
     * @var string
     */
    protected $grp_daccessend;

    /**
     * Group type. This supposed to be bitwise.
	 *
     * @var int
     */
    protected $grp_type;
    
    public function getPermissions()
    {
        // Get module WHERE clauses
        require_once 'Source/security/SecurityBase.php';
        return ParsePermString($this->permission);
    }

    public static function getGroupsWithBlankWhereClauses()
    {
        $ModuleDefs = Registry::getInstance()->getModuleDefs();
        $AccessLvlDefs = Registry::getInstance()->getAccessLvlDefs();

        require_once 'Source/Security/SecurityBase.php';

        $output = array();

        $groups = (new GroupModelFactory)->getCollection();
        $groups->setQuery(new Query());

        foreach ($groups as $group)
        {
            $permissions = ParsePermString($group->permission);

            $accessLevels = \DatixDBQuery::PDO_fetch_all('SELECT item_code, perm_value FROM sec_group_permissions WHERE grp_id = :group_id', array('group_id' => $group->recordid), \PDO::FETCH_KEY_PAIR);

            foreach ($permissions as $module => $permission)
            {
                if (!in_array($module, array('DAS', 'TOD')) // ignore dashboards and To Do, because they don't have security where clauses
                    && in_array($ModuleDefs[$module]['PERM_GLOBAL'], array_keys($accessLevels)) // this security group has an access level set for this module
                    && in_array($accessLevels[$ModuleDefs[$module]['PERM_GLOBAL']], array_keys($AccessLvlDefs[$ModuleDefs[$module]['PERM_GLOBAL']])) // the access level set is valid (i.e. exists within the access level definitions)
                    && !isset($permission['where']) // the security where clause is blank
                )
                {
                    if (!isset($output[$group->recordid]))
                    {
                        $output[$group->recordid] = array('name' => $group->grp_code, 'modules' => array());
                    }
                    $output[$group->recordid]['modules'][] = $module;
                }
            }
        }

        return $output;
    }
}