<div id="medicationlist_title">
    <span style="cursor: default;">
        <font size="3"><b>&nbsp;Matching Medication</b></font>
        <span style="cursor: pointer;" class="edit_options" onclick="MedicationSelectionCtrl.destroy();">[x]</span>
    </span>
</div>
<div id="medicationlist_container" >
    <table id="medicationlist_table" class="bordercolor" cellspacing="1" cellpadding="4" align="center" border="0">
        <tr class="tableHeader">
            <td class="titlebg" width="6%" align="center"><b>Choose</b></td>
            <?php foreach ($this->MEDColumns as $MEDCol) : ?>
            <th class="titlebg" align="left"><b><?php echo Labels_FormLabel::GetFormFieldLabel($MEDCol, '', 'MED'); ?></b></th>
            <?php endforeach; ?>
        </tr>
        <?php if (count($this->resultArray)) : ?>
            <?php foreach ($this->resultArray as $row) : ?>
            <tr >
                <td class="windowbg2">
                    <input type="button" value="Choose" onclick="
                    if(document.getElementById('<?php echo $this->fieldname; ?>')){
                    <?php foreach($row as $Field => $FieldValue) : ?>
                        <?php
                        if (empty($FieldValue))
                        {
                            continue;
                        }

                        if (ini_get('magic_quotes_sybase'))
                        {
                            $FieldValue = str_replace("'", "\\'", $row[$Field]);
                        }
                        elseif(ini_get('magic_quotes_gpc'))
                        {
                            $FieldValue = addslashes($row[$Field]);
                        }
                        else
                        {
                            $FieldValue = str_replace("'", "\\'", $row[$Field]);
                        }

                        $FieldValue = str_replace("\r\n", "\\n", $FieldValue);

                        $FieldValue = str_replace("\"", "&quot;", $FieldValue);

                        $fieldType = $this->FieldDefsExtra[$this->module][$Field]['Type'];

                        $Field = $this->ModuleDefs['MED']['SEARCH_FIELD_MAPPING'][$this->SearchMappingType][$Field];

                        if ($this->FieldDefs[$this->module][$Field]['FieldFormatsSrc'])
                        {
                            $FieldFormatsName = $this->FieldDefs[$this->module][$Field]['FieldFormatsSrc'];
                        }
                        else
                        {
                            $FieldFormatsName = $Field;
                        }
                        ?>
                        <?php if ($fieldType == 'ff_select') : ?>
                        var field = jQuery('input[id=<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_title]');
                        if (field.length)
                        {
                            field.selectItem(jQuery('<li id=<?php echo $FieldValue; ?>><?php echo str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr('MED', $FieldFormatsName, $FieldValue, '', false))); ?></li>'));
                            field.disable();
                        }
                        else if (jQuery('input[name=<?php echo $Field ?>_<?php echo $this->MedicationSuffix; ?>]:radio').length > 0)
                        {
                            // represented by a radio button
                            jQuery('input[name=<?php echo $Field ?>_<?php echo $this->MedicationSuffix; ?>][value=<?php echo $FieldValue ?>]:radio').prop('checked', true);
                            jQuery('input[name=<?php echo $Field ?>_<?php echo $this->MedicationSuffix; ?>]:radio').prop('disabled', true);
                        }
                        else if (jQuery('#<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>').length)
                        {
                            // e.g. the field is read-only
                            jQuery('#<?php echo $Field; ?>_<?php echo $this->MedicationSuffix ?>').val('<?php echo $FieldValue; ?>');

                            if (jQuery('span[id=<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_title]').length)
                            {
                            jQuery('span[id=<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_title]').html('<?php echo str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr('MED', $FieldFormatsName, $FieldValue, '', false))); ?>');
                            }
                        }
                        <?php elseif ($fieldType == 'multilistbox') : ?>
                        var field = jQuery('input[id=<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_title]');
                        if (field.length)
                        {
                            initDropdown(field);
                            field.data('datixselect').activeValues = [];
                            field.data('datixselect').valuesList.find('li').each(function(i, item)
                            {
                                field.data('datixselect').activeValues.push(jQuery(item).attr('id'));
                            });
                            field.deleteSelectedItems();
                            field.data('datixselect').activeItem = null;
                            field.data('datixselect').activeItems = {};
                            <?php $valueArray = explode(' ', $FieldValue); ?>
                            <?php foreach ($valueArray as $value) : ?>
                            field.selectItem(jQuery('<li id=<?php echo $value; ?>><?php echo str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr('MED', $FieldFormatsName, $value))); ?></li>'));
                            <?php endforeach; ?>
                            field.addSelectedItems();
                            field.disable();
                        }
                        else if (getCheckboxByName('<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>').length > 0)
                        {
                            getCheckboxByName('<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>').prop('checked', false);

                            <?php foreach ($valueArray as $value) : ?>
                                getCheckboxByNameAndValue('<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>', '<?php echo $value; ?>').prop('checked', true);
                            <?php endforeach; ?>

                            updateFieldWithCheckboxesValues('<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>');
                            getCheckboxByName('<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>').prop('disabled', true);
                        }
                        else if (jQuery('#<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>').length)
                        {
                            // e.g. the field is read-only
                            var description = new Array();
                            <?php foreach ($valueArray as $value) : ?>
                            <?php $description = str_replace("'", '\\\'', Sanitize::htmlentities_once(code_descr('MED', $FieldFormatsName, $value))); ?>
                            addCodeDescr('<?php echo $value; ?>', '<?php echo $description; ?>', document.getElementById('<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>'), false);
                            description.push('<?php echo $description; ?>');
                            <?php endforeach; ?>
                            if (jQuery('div[id=<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_title]').length && description.length > 0)
                            {
                                jQuery('div[id=<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_title]').html(description.join('<br />'));
                            }
                        }
                        <?php else : ?>
                        var field = jQuery('#<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>');
                        if (field.length)
                        {
                            field.val('<?php echo $FieldValue; ?>');
                            field.attr('disabled', true);
                            AddHiddenField($('<?php echo $Field;?>_<?php echo $this->MedicationSuffix; ?>'));
                            if (field.attr('type') == 'hidden' && field.parent().attr('class') == 'field_input_div')
                            {
                                // add description to read-only field
                                field.parent().append('<?php echo $FieldValue; ?>');
                            }
                        }
                        if (jQuery('#img_cal_<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>').length)
                        {
                            jQuery('#img_cal_<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>').css('visibility', 'hidden');
                        }
                        <?php endif; ?>
                        AddCustomHiddenField('<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_disabled', '<?php echo $Field; ?>_<?php echo $this->MedicationSuffix; ?>_disabled', '1');
                    <?php endforeach; ?>
                    // This is a workaround ofr IE for displaying a different colour when fields are disabled
                    // find all disabled fields
                    jQuery('input:disabled, textarea:disabled, select:disabled').each(function(index) {
                        // add the class is_disabled
                        jQuery(this).addClass('is_disabled');
                        // remove the attribut disabled
                        jQuery(this).removeAttr('disabled');
                        // add new attribut readonly
                        jQuery(this).attr('readOnly', 'readOnly');
                    });
                    // now don\'t let anyone click into the fields
                    // this will simulate the \'disabled\' functionality
                    jQuery('.is_disabled').click(function() {
                        jQuery('.is_disabled').blur();
                    });
                    document.forms[0].<?php echo $this->fieldname; ?>.value = '';
                    document.forms[0].<?php echo $this->fieldname; ?>.disabled = 'true';
                    document.forms[0].check_btn_<?php echo $this->fieldname; ?>.disabled = 'true';
                }
                MedicationSelectionCtrl.destroy();" />
                </td>
                <?php foreach ($this->MEDColumns as $MEDCol) : ?>
                <td class="windowbg2"><?php echo $row[$MEDCol . "_descr"]; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        <?php else : ?>
            <?php if ($this->dtxdebug) : ?>
            <tr>
                <td class="windowbg2" colspan="4" align="left"><b><?php echo \src\security\Escaper::escapeForHTML($this->sql); ?></b></td>
            </tr>
            <?php endif; ?>
            <tr>
                <td class="windowbg2" colspan="5" align="left"><b>No matching medication found</b></td>
            </tr>
        <?php endif; ?>
    </table>
</div>