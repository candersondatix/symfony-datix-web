<?php

namespace src\med\controllers;

use src\framework\controller\Controller;

class SaveMedicationController extends Controller
{
    /**
     * Saves a Medication.
     */
    function medicationlinkaction()
    {
        $form_action = $this->request->getParameter('rbWhat');
        $med = QuotePostArray($this->request->getParameters());
        $med_id = $med['med_id'];
        $link_id = $med['link_id'];
        $module = $med['module'];

        if (bYN(GetParm('RECORD_LOCKING', 'N')) && $med_id)
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array('table' => 'MEDICATIONS_MAIN', 'link_id' => $med_id));
        }

        switch ($form_action)
        {
            case 'Document':
            case 'Save':
                //require_once 'Source/medications/SaveMedication.php';
                $this->SaveMedication();
                break;
            case 'Unlink':
                require_once 'Source/medications/MainMedications.php';
                UnlinkMedication($med_id, $link_id, $module);
                break;
            case 'Cancel':
                if ($this->request->getParameter('fromsearch'))
                {
                    $RedirectLocation = '?action=list&module=MED&listtype=search';
                }
                elseif (isset($_SESSION['security_group']['grp_id']))
                {
                    $_SESSION['security_group']['success'] = false;

                    $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                        'grp_id' => $_SESSION['security_group']['grp_id'],
                        'module' => $_SESSION['security_group']['module']
                    ));
                    return;
                }
                else
                {
                    $RedirectLocation = '?module=MED';
                }

                $this->redirect('app.php' . $RedirectLocation);
                break;
            case 'ShowAudit':
                $this->call('src\med\controllers\ShowMedicationController', 'medication', array(
                    'med_id' => $med_id,
                    'full_audit' => '1'
                ));
                return;
                break;
            case 'Search':
            case 'Link':
                require_once 'Source/medications/SearchMedications.php';
                MedicationsDoSelection(\UnicodeString::strtolower($form_action));
                obExit();
                break;
            case 'BackToListing':
                $this->redirect('app.php?action=reportdesigner&from_report=1');

                break;
        }
    }

    function SaveMedication()
    {
        global $scripturl, $yySetLocation;
        global $dtxdebug, $ModuleDefs, $FieldDefs;

        session_start();

        $form_action = $_POST["rbWhat"];
        $med = QuotePostArray(\Sanitize::SanitizeRawArray($_POST));
        $med_id = $med["med_id"];
        $link_id = $med["link_id"];
        $module = $med["module"];

        if (!$med_id)
            $med_id = GetNextRecordID("medications_main", true);

        $FullAudit = true;

        if ($FullAudit)
            DoFullAudit("MED", "medications_main", $med_id);

        if(bYN(GetParm("RECORD_LOCKING","N")))
        {
            require_once 'Source/libs/RecordLocks.php';
            UnlockRecord(array("table" => "medications_main", "link_id"=> $med_id));
        }

        $ValidationErrors = $this->ValidateMedicationData();

        if($ValidationErrors)
        {
            $error['Validation'] = $ValidationErrors;
        }

        if ($error)
        {
            $med = \Sanitize::SanitizeRawArray($_POST);
            $med["error"] = $error;

            AddSessionMessage('ERROR', _tk('missing_data_error'));

            require_once 'Source/medications/MainMedications.php';
            ShowMedicationForm($med);

            obExit();
        }

        $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'MED', 'level' => ($_POST["holding_form"] ? 1 : 2)));
        $FormDesign->LoadFormDesignIntoGlobals();

        $med = ParseSaveData(array('module' => 'MED', 'data' => $med));

        if ($med_id)
        {
            $sql = "update medications_main set
			updatedby = '$_SESSION[initials]',
			updateddate = '" . date('d-M-Y H:i:s') . "',
    		updateid = '" . GensUpdateID($med["updateid"]) . "' ";

            $sql_fields = $ModuleDefs['MED']['FIELD_ARRAY'];

            foreach($sql_fields as $field)
            {
                if (array_key_exists($field, $med))
                {
                    if ($FieldDefs["MED"][$field]["Type"] == "number" || $FieldDefs["MED"][$field]["Type"] == "money")
                    {
                        $sql .= ", $field = $med[$field]";
                    }
                    elseif ($FieldDefs["MED"][$field]["Type"] == "date")
                    {
                        $sql .= ", $field = " . UserDateToSQLDate($med[$field]);
                    }
                    else
                    {
                        $sql .= ", $field = '$med[$field]'";
                    }
                }
            }

            $sql .= " where recordid = $med_id";

            if ($med["updateid"])
            {
                $sql .= " and updateid = '$med[updateid]'";
            }

            //this can be changed to PDO by introducing a medication entity
            $result = db_query($sql);

            if (!$result)
            {
                $error = "An error has occurred when trying to save the medication record. Please report the following to the Datix administrator: $sql";
            }
            elseif (affected_rows($result) == 0)
            {
                $error = _tk("cannot_save_err").'<br><br>Click <a href="'.$scripturl.'?action=medication&module='.$med['module'].'&link_id='.$med['link_id'].'&med_id='.$med['med_id'].'">here</a> to return to the record';
            }
        }

        if ($error != "")
        {
            SaveError($error);
        }

        $med["med_id"] = $med_id;

        $_SESSION["med_id"] = $med['med_id'];

        if($LinkInfo)
        {
            $this->ShowSaveMedication($med, $Warning, $LinkInfo);
        }
        else
        {
            $status_message = "The medication record has been saved successfully";

            $recordurl = "$scripturl?action=medication&module=$med[module]&link_id=$med[link_id]&med_id=$med[med_id]";

            AddSessionMessage('INFO', $status_message);

            $this->redirect($recordurl);
        }

        if ($Warning)
            $yySetLocation .= "&warning=" . urlencode($Warning);

        redirectexit();

    }

    function ShowSaveMedication($med = "", $Warning = "", $LinkInfo = "")
    {
        global $dtxtitle, $person_action, $scripturl, $yySetLocation, $ModuleDefs;

        if (!$med["med_id"])
        {
            $yySetLocation = "$scripturl";
            redirectexit();
        }

        $dtxtitle = _tk('MEDNameTitle');

        getPageTitleHTML(array(
            'title' => $dtxtitle,
            'module' => 'MED'
        ));

        GetSideMenuHTML(array('module' => 'MED', 'table' => $MedTable, 'buttons' => $ButtonGroup));

        template_header();

        ?>
        <form method="post" name="frmSaveMedication" action="<?= "$scripturl?action=list" ?>">
            <input type="hidden" name="form_action" value="">
            <input type="hidden" name="recordid" value="<?= $med["med_id"] ?>">
            <table class="gridlines" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
                <tr>
                    <td class="titlebg" colspan="5">
                        <b>Medication</b>
                    </td>
                </tr>
                <tr>
                    <td class="windowbg2"><b>
                            <?php
                            if ($Warning)
                                echo $Warning . "<br /><br />";
                            ?>
                            The medication record has been saved successfully. The reference number is <?= $med["med_id"] ?>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td class="titlebg" align = "center">
                        <?php
                        if (is_array($LinkInfo)) {
                            ?>
                            <input TYPE="button" VALUE="Back to <?= $ModuleDefs[$LinkInfo['module']]['REC_NAME'] ?>" name="btnBackToModule" onClick="SendTo('<?=
                            "$scripturl?action={$ModuleDefs[$LinkInfo[module]][ACTION]}&amp;recordid=$LinkInfo[recordid]&amp;panel=medication"
                            ?>');">
                        <?php
                        }
                        ?>
                        <input TYPE="button" VALUE="Back to medication" name="btnBack" onClick="SendTo('<?=
                        "$scripturl?action=medication&amp;module=$med[module]&amp;link_id=$med[link_id]&amp;med_id=$med[med_id]"
                        ?>');">
                    </td>
                </tr>
            </table>

        </form>
        <?php
        footer();
        obExit();
    }

    function ValidateMedicationData()
    {
        $error = ValidatePostedDates('MED');

        return $error;
    }
}