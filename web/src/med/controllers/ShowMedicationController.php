<?php

namespace src\med\controllers;

use src\framework\controller\Controller;

class ShowMedicationController extends Controller
{
    /**
     * Shows an existing Medication record.
     */
    function medication()
    {
        global $ast, $dtxdebug;

        $ModuleDefs = $this->registry->getModuleDefs();

        $module = \Sanitize::getModule($this->request->getParameter('module'));
        $link_id = \Sanitize::SanitizeInt($this->request->getParameter('link_id'));
        $med_id = \Sanitize::SanitizeInt($this->request->getParameter('med_id'));

        if (!$med_id && $this->request->getParameter('recordid'))
        {
            $med_id = \Sanitize::SanitizeInt($this->request->getParameter('recordid'));
        }

        $MEDPerms = GetParm('MED_PERMS');

        if ($med_id)
        {
            // Existing medication
            $sql = "
                SELECT
                    medications_main.recordid as med_id,
                    medications_main.updateid," .
                    implode(',', $ModuleDefs['MED']['FIELD_ARRAY']) . "
                FROM
                    medications_main
            ";

            $where[] = 'medications_main.recordid = :recordid';

            $WhereClause = MakeSecurityWhereClause($where, 'MED',  $_SESSION['initials']);
            $sql .= " WHERE $WhereClause";

            $med = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $med_id));

            if (!$med || $MEDPerms == 'MED_INPUT_ONLY' || !$MEDPerms)
            {
                $msg = 'You do not have the necessary permissions to view medication ID ' . $med_id . '.';

                if ($dtxdebug)
                {
                    $msg .= "\n$sql";
                }

                fatal_error($msg, 'Information');
            }

            // Since some generic functions require data against the "recordid" key
            $med['recordid'] = $med['med_id'];

            if ($MEDPerms == 'MED_READ_ONLY')
            {
                $FormType = 'ReadOnly';
            }
            else
            {
                $FormType = 'edit';
            }
        }
        // New medication or new link
        elseif(!$MEDPerms || $MEDPerms == 'MED_READ_ONLY')
        {
            $msg = 'You do not have the necessary permissions to add new medications.';
            fatal_error($msg, 'Information');
        }

        // Check if we need to show full audit trail
        if ($med['med_id'] && $this->request->getParameter('full_audit'))
        {
            $FullAudit = GetFullAudit(array('Module' => 'MED', 'recordid' => $med_id));

            if ($FullAudit)
            {
                $med['full_audit'] = $FullAudit;
            }
        }

        require_once 'Source/medications/MainMedications.php';
        ShowMedicationForm($med, $FormType);
    }
}