<?php
namespace src\listingfilters\controllers;

use src\framework\controller\Controller;
use Source\classes\Filters\Container;

/**
* Controller class for filters.
*
* @codeCoverageIgnore
*/
class FilterController extends Controller
{
    /**
    * Creates a listing based on the filter options selected.
    */
    public function doFilter()
    {
        $module = \Sanitize::getModule($this->request->getParameter('module'));
        $sideMenuModule = \Sanitize::SanitizeString($this->request->getParameter('side_menu_module'));
        $filterParameters = $this->getFilterParameters();

        $filter = Container::getFilter($module, false);
        $whereClause = $filter->createWhereClause($filterParameters);

        //if the where clause has changed, we need to re-create the session recordlist
        if(!isset($_SESSION[$module]['RECORDLIST']) || ($whereClause != $_SESSION[$module]['WHERE']))
        {
            $_SESSION[$module]['RECORDLIST'] = \RecordLists_RecordListShell::CreateForModule($module, $whereClause);
            $_SESSION[$module]['RECORDLIST']->FlagAllRecords();
        }

        $_SESSION[$module]['WHERE']  = $whereClause;
        $_SESSION[$module]['FILTER'] = $filter->filterDesignToArray($filterParameters);

        //Any $_GET parameters should be retained when we move to the filetered listing
        $yySetLocation = '?action=list&listtype=search&' . getGetString(array('action', 'listtype'));

        $this->redirect($yySetLocation);
    }

    /**
    * Extracts the appropriate values from the request which define the filter being run.
    *
    * @return array $parameters
    */
    protected function getFilterParameters()
    {
        $parameters = array('filter_max_suffix' => $this->request->getParameter('filter_max_suffix'));
        for ($i = 0; $i < $parameters['filter_max_suffix']; $i++)
        {
            $parameters['filter_field_'.$i] = $this->request->getParameter('filter_field_'.$i);
            $parameters['filter_value_'.$i] = $this->request->getParameter('filter_value_'.$i);
        }
        return $parameters;
    }
}
