<?php
namespace src\dashboard\controllers;

use src\framework\controller\TemplateController;
use src\framework\registry\Registry;

class DashboardTemplateController extends TemplateController
{
    // ---------------------
    //@codeCoverageIgnoreStart
    // ---------------------

    /**
     * Called from main.php - this is the basic dashboard display page. It will output the dashboard
     * framework and initialise the javascript objects.
     *
     * [no unit test because this is an HTML-construction function, and so there is no clear output to check for]
     */
    function dashboard()
    {
        require_once 'Source/dashboards/dashboard.php';

        $this->addCss(array(
            'js_functions/qTip/jquery.qtip.min.css',
            'src/reports/css/reportDesigner.css',
            'js_functions/colorpicker/css/colorpicker.css',
            'src/dashboard/css/dashboard.css',
            'src/reports/css/reports.css'
            )
        );

        $this->addJs(array(
            'thirdpartylibs/fusioncharts/FusionCharts.js',
            'js_functions/dashboard.js',
            'src/dashboard/js/jquery.dashboard.js',
            'src/dashboard/js/jquery.widget.js',
            'src/reports/js/ListingReportPager.js',
            'js_functions/AdobeFlashDetection.js',
            'js_functions/colorpicker/js/colorpicker.js'
            )
        );

        $this->sendToJs(array(
            'error_loading_report_data' => _tk('error_loading_report_data'),
            'end_of_report' => _tk('end_of_report'),
            'retry_text' => _tk('retry_text')
        ));

        // Due to problems setting screen size in all places going to the Dashboard, it can either be done using a GET or a POST
        $screenwidth = ($this->request->getParameter('screenwidth') ? $this->request->getParameter('screenwidth') : $this->request->getParameter('width'));
        $screenheight = ($this->request->getParameter('screenheight') ? $this->request->getParameter('screenheight') : $this->request->getParameter('height'));

        if($_SESSION['CurrentUser']->canAddRecords('DAS', $this->registry))
        {
            MakeWidgetPanelHTML();
        }

        $this->hasMenu = false;
        $this->hasPadding = false;

        require_once 'Source/dashboards/dashboardclass.php';
        require_once 'Source/dashboards/widgetclass.php';

        $dashboardArray = GetUserDashboards();

        //user has no dashboards set up - we should give them a new, blank one
        if (empty($dashboardArray) && $_SESSION['CurrentUser']->canAddRecords('DAS', $this->registry))
        {
            $firstDashboard = new \Dashboard();
            $firstDashboard->Data['dash_name'] = 'My First Dashboard';
            $firstDashboard->SaveAsNewDashboard();
            $dashboardArray[] = array(
                'recordid' => $firstDashboard->Recordid,
                'dash_name' => 'My First Dashboard',
                'editable' => $_SESSION['CurrentUser']->canAddRecords('DAS', $this->registry)
            );
        }

        $defaultDashboard = GetDefaultDashboard();

        $dashId = $this->request->getParameter('dash_id') ?: $defaultDashboard ?: $dashboardArray[0]['recordid'];

        $this->response->build('src/dashboard/views/Dashboard.php', array(
            'DashboardArray' => $dashboardArray,
            'DefaultDashboard' => $defaultDashboard,
            'screenwidth' => $screenwidth,
            'screenheight' => $screenheight,
            'dashId' => $dashId,
            'canAddNew' => $_SESSION['CurrentUser']->canAddRecords('DAS', $this->registry)
        ));
    }

    // ---------------------
    //@codeCoverageIgnoreEnd
    // ---------------------
}