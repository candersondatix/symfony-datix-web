<?php if (is_array($this->DashboardArray) && !empty($this->DashboardArray)) : ?>
    <div class="tabs">
        <ul class="tabNavigation">
            <?php foreach ($this->DashboardArray as $Details) : ?>
            <li class="dashboard_tab" id="dashboard_tab_<?php echo $Details['recordid']; ?>" >
                <a onClick="DashboardObjects[<?php echo $Details['recordid']; ?>].SetAsCurrent();">
                    <span>
                        <span class="default_dash_marker" id="default_dash_marker_<?php echo $Details['recordid']; ?>" <?php echo ($this->DefaultDashboard == $Details['recordid'] ? '' : 'style="display:none"'); ?>><img src="Images/tick<?php echo ($this->DefaultDashboard == $Details['recordid'] ? '1' : '0'); ?>.png" height="11" width="11"></span>
                        <?php echo htmlspecialchars($Details['dash_name'] ? $Details['dash_name'] : '<untitled>'); ?>
                    </span>
                    <?php if ($Details['editable']) : ?>
                    <span onClick="DashboardObjects[<?php echo $Details['recordid']; ?>].Delete();" class="dashboard_tab_x">x</span>
                    <?php endif; ?>
                </a>
            </li>
            <?php endforeach; ?>
            <?php if ($this->canAddNew): ?>
            <li id="dashboard_tab_new" onClick="CreateNewDashboard();">
                <a>
                    <span title="Add New">
                        <img src="images/plus.gif" alt="Add new dashboard" />
                    </span>
                </a>
            </li>
            <?php endif; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
<?php endif; ?>
<?php if (is_array($this->DashboardArray) && !empty($this->DashboardArray)) : ?>
    <?php foreach ($this->DashboardArray as $Details) : ?>
        <?php
        if (!$this->DefaultDashboard)
        {
            $DefaultDashboard = $Details['recordid'];
        }

        $Dashboard = new Dashboard();
        $Dashboard->SetRecordid($Details['recordid']);
        ?>
        <script type="text/javascript">
            <?php //js dashboard object - held in session array "DashboardObjects" keyed by the database recordid. ?>

            // TODO - candidates for asset management sendToJs()
            jQuery(function(){

                var NewjQueryDashboard = jQuery('#dashboard_<?php echo $Details['recordid']; ?>').datixDashboard({

                    id: <?php echo ($Details['recordid'] ?: 'null'); ?>,
                    numCols: <?php echo ($Dashboard->NumCols ?: 3); ?>,
                    name: "<?php echo addslashes(htmlspecialchars($Dashboard->Data['dash_name'])); ?>",
                    editable: <?php echo ($Dashboard->CurrentUserIsOwner() ? 'true' : 'false'); ?>
                });

                <?php if (count($this->DashboardArray) == 1):?>
                <?php $firstElement = reset ($this->DashboardArray); ?>
                jQuery(window).on('load', function() {

                    DashboardObjects[<?php echo $firstElement['recordid']; ?>].SetAsCurrent();
                });
                <?php elseif (count($this->DashboardArray) != 1 && $Details['recordid'] == GetCurrentDashboard()) : ?>
                /**
                 * Wait until the page has loaded before triggering this, as we need to make sure all the widgets have been loaded in properly
                 */
                jQuery(window).on('load', function() {

                    NewjQueryDashboard.SetAsCurrent();
                    DefaultDashboard = <?php echo (int) $this->DefaultDashboard; ?>;
                });
                <?php endif; ?>

                var winHeight = jQuery(window).height();

                jQuery('div.hidden-panel').each(function() {
                    if (winHeight >= jQuery(this).height())
                    {
                        jQuery(this).css('height', (winHeight - 30));
                    }
                });
            });
        </script>
        <div id="dashboard_body_<?php echo $Details['recordid'] ?>">
        <?php

        if ($this->dashId && $this->dashId == $Details['recordid'])
        {
            // Add basic construction information to the dashboard object
            $Dashboard->InitialiseDashboard(($this->screenwidth), ($this->screenheight));
            $Dashboard->Hidden = false;
            echo $Dashboard->GetDashboardHTML();
        }
        else
        {
            $Dashboard->Hidden = true;
        }
        ?>
        </div>
    <?php endforeach; ?>
<?php else: ?>
<div class="empty_dashboard_message">
    <?php echo _tk('no_dashboards_message'); ?>
</div>
<?php endif; ?>