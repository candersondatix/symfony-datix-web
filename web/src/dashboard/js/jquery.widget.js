// The semicolon is to prevent any poorly closed plugin or class from causing problems
;
//global array to store widgets in. Widgets are keyed by their recordid in the reports_dash_links table.
var WidgetObjects = {};

/**
 * Function to trigger the hasRendered() functionevent when charts have rendered triggered by FusionChart event.
 * @param eventObject
 * @param argumentsObject
 */
function chart_rendered(eventObject, argumentsObject) {

    if(eventObject) {

        var widgetId = jQuery('#' + eventObject.sender.id).closest('.widget').attr('id').replace(/^widget_/,'');

        WidgetObjects[widgetId].hasRendered();
    }
}

(function( $ ) {

    $.fn.datixWidget = function( options ) {

        var $obj = this,
            defaults = {
                id: null,
                reportId: null,
                title: '',
                dashboardId: null,
                editable: false,
                ajax: false
            },
            settings = $.extend({}, defaults, options );

        var $dashboard = $('#dashboard_' + settings.dashboardId),
            $widget = $dashboard.find('#widget_' + settings.id),
            $widgetViewport = $widget.find('.widget-viewport'),
            $widgetBody = $widget.find('#widget_body_' + settings.id),
            $widgetWait = $widget.find('#widget_wait_' + settings.id),
            $widgetMask = $widget.find('.widget-mask').length ? $widget.find('.widget-mask') : $('<div/>', {
                'class': 'widget-mask'
            }),
            $widgetError = $widget.find('.widget-error').length ? $widget.find('.widget-error') : $('<div/>', {
                'class': 'widget-error',
                'text': 'There was an error loading the widget'
            }).css({
                'display': 'none'
            }),
            $listingPager = null;

        WidgetObjects[settings.id] = $obj;

        this.Initialise = function() {

            /**
             * Fix the height of the widget so that it doesn't snap when changing content
             */
            $widget.css({minHeight: $widget.height()});

            $widgetError.hide();

            if( ! $widget.find('.widget-mask').length) {

                $widgetViewport.prepend($widgetMask);

                if( ! Modernizr.backgroundsize) {

                    var image = /^url\((['"])?([^\1]+)\1\)/i.exec($widgetMask.css('background-image'));

                    /**
                     * these values reflect the available height for the widget window after taking into account the
                     * widget header and border
                     */
                    if($widget.hasClass('traffic_light_chart'))
                    {
                        widgetHeight = 129;
                    }
                    else
                    {
                        widgetHeight = 299;
                    }

                    if(image && image.length > 1) {

                        var $img = $('<img/>', {
                            src: image[2],
                            width: 300,
                            height: widgetHeight
                        }).width(300).height(widgetHeight);

                        $widgetMask.append($img).css({
                            background: '#fff',
                            textAlign: 'center',
                            height: 'auto'
                        });
                    }
                }
            }

            $widgetWait.slideDown(200, function(){

                /**
                 * scripturl is a global
                 */
                var ajaxurl = scripturl + '?action=httprequest&type=getwidgetcontents&responsetype=json&id=' + settings.id + '&dbid=' + settings.dashboardId;

                //TODO this should probably have a callback if the ajax fails to inform the user something didn't work
                settings.ajax = jQuery.ajax({
                    url: ajaxurl,
                    cache: false,
                    async: true,
                    dataType: "html",
                    success: function(html) {

                        var widgetWidth = $widget.innerWidth();

                        $widgetBody.width(widgetWidth);

                        if (html.indexOf("FusionChart") >= 0) {

                            //TODO this whole thing is pretty nasty and could probably be done better
                            // extract the fusionchart js
                            $widgetBody.html($.trim(SplitJSAndHTML(html, '<script type="text/javascript" >')));

                            var $chartContainer = $widgetBody.find('#chartContainer');

                            $chartContainer.width(widgetWidth);

                            // clean and execute the extracted javascript - ewwwwwwwww
                            var fcscript = JavascriptToExecute.pop();
                            fcscript = fcscript.replace('<!--', '');
                            fcscript = fcscript.replace('// Instantiate the Chart', '');
                            fcscript = fcscript.replace('-->', '');

                            eval(fcscript);

                            /**
                             * chart_rendered() is triggered from evalled code as it is added to a listener for the rendered event on the fusionchart object
                             * though, this doesn't seem to be very reliable
                             */

                            var x = 0;

                            while( ! $obj.data('rendered') && x < 9999) {

                                if($chartContainer.length && $chartContainer.children().length) {

                                    /**
                                     * If the chartContainer has content, then say that it's rendered or iterate until it does
                                     */
                                    $obj.data('rendered', true);
                                }

                                x++;
                            }

                            if( ! $obj.data('rendered')) {

                                $obj.showError();
                            }
                            else
                            {
                                $obj.hasRendered();
                            }

                            $('#myFusionChartReport_' + settings.dashboardId + '_' + settings.reportId).width(widgetWidth).height($widgetBody.height());
                        }
                        else if($widget.hasClass('listing_report')) {

                            try {

                                eval("var response = " + html + ";");

                                if($listingPager) {

                                    $listingPager.destroy();
                                }

                                $widgetBody.html(response.output);

                                $listingPager = $widget.find('#listingtable').listingPager({
                                    viewport:      $widgetBody,
                                    recordid:      settings.reportId,
                                    context:       2,
                                    startPosition: response.startPosition,
                                    endOfReport:   response.endOfReport,
                                    previousRow:   response.previousRow,
                                    columnTotals:  response.columnTotals
                                });

                                $listingPager.init();

                                $obj.hasRendered();
                            } catch (e) {

                                $obj.showError();
                            }
                        }
                        else {

                            $widgetBody.html(html).height('auto');
                            $widgetViewport.height('auto');

                            $obj.hasRendered();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                        if(textStatus != 'abort') {

                            $obj.showError();
                        }
                    }
                });
            });
        };

        this.redisplay = function() {

        var $chartContainer = $widgetBody.find('#chartContainer');

            if($chartContainer.length) {

                $widgetMask.show();

                if($obj.data('rendered')) {

                    /**
                     * Nasty was to stop weird popping of the mask when showing an already loaded dashboard
                     */
                    setTimeout( function(){ $widgetMask.fadeOut()}, 1000);
                }
            }
        };

        this.hasRendered = function() {

            $obj.data('rendered', true);

            $widgetBody.css({minHeight: ''});
            $widget.css({minHeight: ''});

            $widgetWait.slideUp();

            $widgetMask.fadeOut();
        };

        this.showError = function() {

            /*var $dashboard = jQuery('#dashboard_' + settings.dashboardId),
                $widget = $dashboard.find('#widget_' + settings.id),
                $widgetWait = $widget.find('#widget_wait_' + settings.id);*/

            $widgetWait.slideUp();

            if( ! $widget.find('.widget-error').length) {

                $widgetViewport.prepend($widgetError);
            }

            $widgetError.slideDown();
        };

        /**
         * Called when a user clicks "Delete" on a widget menu:
         * Confirms the delete, then makes an ajax call to delete the widget and removes the element from the screen.
         */
        this.Delete = function() {

            if(confirm("Are you sure you want to delete this widget (" + settings.title + ")?")) {

                jQuery.get(scripturl + '?action=deletewidget&id=' + settings.id);

                // Remove widget element
                jQuery('#widget_' + settings.id).remove();

                // If there are no widgets left, we need to display the "empty dashboard" message
                DashboardObjects[settings.dashboardId].CheckEmptyDashboardMessage()
            }
        };

        /**
         * Called when a user clicks "Move to".
         * Confirms the move, then makes an ajax call to delete the widget from the current dashboard and add it to the new one.
         */
        this.Move = function(NewDashboard) {

            if (confirm("Are you sure you want to move this widget (" + settings.title + ")?")) {

                //if there are no widgets left, we need to display the "empty dashboard" message
                jQuery('#widget_' + settings.id).remove();
                DashboardObjects[settings.dashboardId].CheckEmptyDashboardMessage()

                jQuery.getJSON(scripturl + '?action=httprequest&type=movewidget&responsetype=json&widget_id=' + settings.id + '&dash_id=' + NewDashboard, function(data) {
                    if(data.success) {

                        var sHTML = SplitJSAndHTML(data.html.toString());
                        RunGlobalJavascript();
                        jQuery('#dashboard_' + data.dash_id).find('.column').first().append(sHTML);
                        DashboardObjects[data.dash_id].initialiseNewWidget();
                        DashboardObjects[data.dash_id].CheckEmptyDashboardMessage()
                    }
                    else {

                        OldAlert('Error: ' + data.message);
                    }
                });
            }
        };

        /**
         * Called when a user clicks "Edit" on a widget menu:
         * Loads an edit screen in a floating div, allowing them to change any aspect of the report.
         */
        this.Edit = function() {

            AlertWindow = AddNewFloatingDiv('WidgetEditPopup');

            jQuery.ajax( {

                url: scripturl + '?action=httprequest&type=editwidget&id=' + settings.id,
                success: function(data) {

                    var sHTML = SplitJSAndHTML(data);

                    AlertWindow.setWidth(490+'px');
                    AlertWindow.setContents(sHTML);
                    AlertWindow.setTitle("Edit widget");
                    AlertWindow.OnCloseAction = function() { jQuery('div[id^=collorpicker_]').remove();};

                    var buttonHTML = '';

                    if (settings.editable) {

                        buttonHTML += '<input type="button" value="Save" onClick="WidgetObjects[' + settings.id + '].Save();jQuery(\'div[id^=collorpicker_]\').remove();" />&nbsp;';
                    }

                    buttonHTML += '<input type="button" value="Save As New" onClick="WidgetObjects[' + settings.id + '].SaveAsNew();jQuery(\'div[id^=collorpicker_]\').remove();" />&nbsp;';
                    buttonHTML += '<input type="button" value="Cancel" onClick="var div = GetFloatingDiv(\'WidgetEditPopup\');div.CloseFloatingControl();destroyQtips();" />';
                    AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);
                    AlertWindow.display();

                    RunGlobalJavascript(function() {
                        initialiseDesigner(true);
                    });
                },
                error: function(request, status, error) {

                    AlertWindow.CloseFloatingControl();

                    OldAlert(request.responseText);
                }
            });
        };

        this.Save = function() {

            var fields = getFields();

            if(validateFields(fields)) {

                if(confirm(text['save_confirm'])) {
                    clearFields(fields);
                    destroyQtips();
                }
                else
                {
                    return false;
                }
            }
            else{

                return false;
            }

            jQuery.getJSON(scripturl+'?action=savewidget&responsetype=json&id=' + settings.id + '&' + jQuery('#designReport').serialize(), function(data) {

                var div = GetFloatingDiv('WidgetEditPopup');
                div.CloseFloatingControl();
                WidgetObjects[data.id].Initialise();

                if(data.title) //in case title has changed
                {
                    jQuery('#widget_title_' + data.id).text(data.title);
                }
            });
        };

        this.SaveAsNew = function() {

            var fields = getFields();

            if(validateFields(fields)) {

                clearFields(fields);
                destroyQtips();
            }
            else{

                return false;
            }

            jQuery.getJSON(scripturl + '?action=savewidgetasnew&responsetype=json&id=' + settings.id + '&dash_id=' + settings.dashboardId + '&' + jQuery('#designReport').serialize(), function(data)
            {
                if ( ! data.success)
                {
                    alert("An error occurred - unable to save new report");
                }
                else
                {
                    DashboardObjects[data.dash_id].AddToDashboard(data.report_id);

                    var div = GetFloatingDiv('WidgetEditPopup');
                    div.CloseFloatingControl();
                }
            });
        };

        this.AbortAjax = function() {

            if(settings.ajax) {

                settings.ajax.abort();
            }
        };

        return $obj;
    };

}( jQuery ));