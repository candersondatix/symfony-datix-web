<script type="text/javascript">
    /**
     * Check that the locations field and at least one of the CQC module fields are populated.
     *
     * @return boolean
     */
    function validateOnSubmit()
    {
        var error = "";

        if (jQuery("#locations_title").isEmpty())
        {
            error = "<?php echo addslashes(_tk("locations")) ?><br />";
        }

        if (jQuery("#outcomes_title").isEmpty() && jQuery("#prompts_title").isEmpty() && jQuery("#subprompts_title").isEmpty())
        {
            error += "At least one of <?php echo addslashes(_tk("cqc_outcomes")) ?>, <?php echo addslashes(_tk("cqc_prompts")) ?>, or <?php echo addslashes(_tk("cqc_subprompts")) ?>";
        }

        if (error != "")
        {
            error = txt['alert_message'] + "<br /><br />" + error;
            alert(error);
            return false;
        }
        else
        {
            return true;
        }
    }
</script>
<form method="POST" id="cqclinkform" name="cqclinkform" action="<?php echo $this->scripturl ?>?action=savecqclinks">
    <input type="hidden" name="lib_id" value="<?php echo $this->libId ?>" />
    <input type="hidden" name="callback" value="<?php echo addslashes($this->callback) ?>" />
    <?php echo $this->table->GetFormTable(); ?>
</form>
<div class="button_wrapper">
    <input class="button" type="button" value="<?php echo _tk('cqc_new_link_save') ?>" onclick="if(validateOnSubmit()){selectAllMultiCodes();jQuery('#cqclinkform').submit()}">
    <input class="button" type="button" value="<?php echo _tk('cqc_new_link_cancel') ?>" onclick="SendTo(scripturl+'?<?php echo addslashes($this->callback) ?>');">
</div>