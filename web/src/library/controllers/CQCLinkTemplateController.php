<?php
namespace src\library\controllers;

use src\framework\controller\TemplateController;

class CQCLinkTemplateController extends TemplateController
{
    /**
    * Creates and displays the "New Evidence Link" screen.
    *
    * @global ZendView $DatixView
    * @global array    $ModuleDefs
    */
    public function newcqclink()
    {
        global $DatixView;

        $ModuleDefs = $this->registry->getModuleDefs();

        $this->title = _tk('cqc_new_link_title');
        $this->module = 'LIB';

        $locationsField = \Forms_SelectFieldFactory::createSelectField('locations', '', '', '', true);
        $locationsField->setSelectFunction('getLocations');
        $locationsField->setSuppressCodeDisplay();
        $locationsField->setDisableCache();
        $locationsField->setIgnoreMaxLength();
        $DatixView->fieldNames = array('outcomes','prompts','subprompts');
        $locationsField->setOnChangeExtra($DatixView->render('classes/LIB/ManageCQCLinks/newLinkOnChange.php'));

        $outcomesField = \Forms_SelectFieldFactory::createSelectField('outcomes', '', '', '', true);
        $outcomesField->setSelectFunction('getOutcomes');
        $outcomesField->setSuppressCodeDisplay();
        $outcomesField->setDisableCache();
        $outcomesField->setParents(array('locations'));
        $outcomesField->setIgnoreMaxLength();
        $DatixView->fieldNames = array('prompts','subprompts');
        $outcomesField->setOnChangeExtra($DatixView->render('classes/LIB/ManageCQCLinks/newLinkOnChange.php'));

        $promptsField = \Forms_SelectFieldFactory::createSelectField('prompts', '', '', '', true);
        $promptsField->setSelectFunction('getPrompts');
        $promptsField->setSuppressCodeDisplay();
        $promptsField->setDisableCache();
        $promptsField->setParents(array('locations','outcomes'));
        $promptsField->setIgnoreMaxLength();
        $DatixView->fieldNames = array('subprompts');
        $promptsField->setOnChangeExtra($DatixView->render('classes/LIB/ManageCQCLinks/newLinkOnChange.php'));

        $subpromptsField = \Forms_SelectFieldFactory::createSelectField('subprompts', '', '', '', true);
        $subpromptsField->setSelectFunction('getSubprompts');
        $subpromptsField->setSuppressCodeDisplay();
        $subpromptsField->setDisableCache();
        $subpromptsField->setParents(array('locations','outcomes','prompts'));
        $subpromptsField->setIgnoreMaxLength();

        $FormArray = array(
            'details' => array(
                'Title' => _tk('cqc_new_link_subtitle'),
                'Rows' => array(
                    array('Type' => 'formfield',
                        'Name' => 'locations',
                        'Title' => _tk("locations"),
                        'FormField' => $locationsField),
                    array('Type' => 'formfield',
                        'Name' => 'outcomes',
                        'Title' => _tk("cqc_outcomes"),
                        'FormField' => $outcomesField,
                        'Condition' => GetParm($ModuleDefs['CQO']['PERM_GLOBAL']) == 'CQO_FULL'),
                    array('Type' => 'formfield',
                        'Name' => 'prompts',
                        'Title' => _tk("cqc_prompts"),
                        'FormField' => $promptsField,
                        'Condition' => GetParm($ModuleDefs['CQP']['PERM_GLOBAL']) == 'CQP_FULL'),
                    array('Type' => 'formfield',
                        'Name' => 'subprompts',
                        'Title' => _tk("cqc_subprompts"),
                        'FormField' => $subpromptsField,
                        'Condition' => GetParm($ModuleDefs['CQS']['PERM_GLOBAL']) == 'CQS_FULL'),
                    array('Type' => 'textarea',
                        'Name' => 'link_notes')
                )
            )
        );

        $Table = new \FormTable('', 'LIB');
        $Table->MakeForm($FormArray, array(), 'LIB');
        $Table->MakeTable();

        $this->response->build('src/library/views/NewCQCLink.php', array(
            'table' => $Table,
            'callback' => $this->request->getParameter('callback'),
            'libId' => \Sanitize::SanitizeInt($this->request->getParameter('lib_id')),
        ));
    }
}

