<?php
namespace src\library\controllers;

use src\framework\controller\Controller;

/**
 * Controls the process of redistibuting assessments for subsequent cycle years.
 */
class CQCLinkController extends Controller
{
    /**
     * Removes the library record link.
     *
     * Called via ajax when clicking on an "unlink" button on the CQC links listing.
     */
    public function cqcunlink()
    {
        // check user has permissions to unlink this record first
        $hasPermissions = false;

        $cqc_ids = \DatixDBQuery::PDO_fetch('SELECT cqo_id, cqp_id, cqs_id FROM link_library WHERE link_recordid = :id', array('id' => $this->request->getParameter('linkid')));

        if ($cqc_ids['cqo_id'] != '')
        {
            $hasPermissions = $this->userCanUnlink($cqc_ids['cqo_id'], 'CQO');
        }
        elseif ($cqc_ids['cqp_id'] != '')
        {
            $hasPermissions = $this->userCanUnlink($cqc_ids['cqp_id'], 'CQP');
        }
        elseif ($cqc_ids['cqs_id'] != '')
        {
            $hasPermissions = $this->userCanUnlink($cqc_ids['cqs_id'], 'CQS');
        }

        if ($hasPermissions)
        {
            \DatixDBQuery::PDO_query('DELETE FROM link_library WHERE link_recordid = :id', array('id' => $this->request->getParameter('linkid')));
        }
    }

    /**
     * Check that the user has write access to this CQC record.
     *
     * @param int          $id     The CQC recordid.
     * @param string       $module The CQC module code.
     *
     * @return boolean
     */
    protected function userCanUnlink($id, $module)
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $secWhere = MakeSecurityWhereClause('', $module, $_SESSION['initials']);
        if ($secWhere != '')
        {
            $secWhere = ' AND ' . $secWhere;
        }

        $recordid = \DatixDBQuery::PDO_fetch('SELECT recordid FROM '.$ModuleDefs[$module]['VIEW'].' WHERE recordid = :id'.$secWhere, array('id' => $id), \PDO::FETCH_COLUMN);

        return GetParm($ModuleDefs[$module]['PERM_GLOBAL']) == $module.'_FULL' && $recordid == $id;
    }

    /**
     * Handles the saving of new library links.
     */
    public function savecqclinks()
    {
        global $yySetLocation, $scripturl, $ModuleDefs;

        // check user has write access
        if (GetParm($ModuleDefs['CQO']['PERM_GLOBAL']) == 'CQO_FULL')
        {
            $outcomes = $this->request->getParameter('outcomes');
        }

        if (GetParm($ModuleDefs['CQP']['PERM_GLOBAL']) == 'CQP_FULL')
        {
            $prompts = $this->request->getParameter('prompts');
        }

        if (GetParm($ModuleDefs['CQS']['PERM_GLOBAL']) == 'CQS_FULL')
        {
            $subprompts = $this->request->getParameter('subprompts');
        }

        // sanitize and check user has permissions for the locations submitted
        $locSecWhere = MakeSecurityWhereClause('', 'LOC', $_SESSION['initials']);

        $validLocations = \DatixDBQuery::PDO_fetch_all('SELECT recordid FROM vw_locations_main'.($locSecWhere != '' ? ' WHERE '.$locSecWhere : ''), array(), \PDO::FETCH_COLUMN);

        $locations = array();
        foreach ($this->request->getParameter('locations') as $location)
        {
            if (ctype_digit($location) && in_array($location, $validLocations))
            {
                $locations[] = $location;
            }
        }

        require_once('Source/classes/LIB/ManageCQCLinks.php');

        if (!empty($locations))
        {
            // create link to the lowest-level CQC records only
            if (is_array($subprompts))
            {
                \Source\classes\LIB\ManageCQCLinks::createCQCLinks('CQS', $locations, $subprompts, new \DatixDBQuery(''));
            }
            elseif (is_array($prompts))
            {
                // if no sub-prompts selected, create links to prompts
                \Source\classes\LIB\ManageCQCLinks::createCQCLinks('CQP', $locations, $prompts, new \DatixDBQuery(''));
            }
            elseif (is_array($outcomes))
            {
                // if no prompts or sub-prompts selected, create links to outcomes
                \Source\classes\LIB\ManageCQCLinks::createCQCLinks('CQO', $locations, $outcomes, new \DatixDBQuery(''));
            }
        }

        $this->redirect('app.php?' . $this->request->getParameter('callback'));
    }


}
