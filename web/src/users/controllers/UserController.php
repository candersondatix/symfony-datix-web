<?php
namespace src\users\controllers;

use src\framework\controller\Controller;
use src\framework\query\Query;
use src\users\model\UserModelFactory;
use src\email\EmailSenderFactory;

class UserController extends Controller
{
    /**
     * Deletes a user based on their recordid
     */
    function deleteuser()
    {
        $recordid = (int) $this->request->getParameter('recordid');

        if($recordid < 1)
        {
            throw new \MissingParameterException('No valid recordid found.');
        }

        $factory = new UserModelFactory();

        $user = $factory->getMapper()->find($recordid);

        $successMessage = sprintf(_tk('user_deleted'), $user->login);
        $failureMessage = sprintf(_tk('err_user_not_deleted'), $user->login);

        if($factory->getMapper()->delete($user))
        {
            AddSessionMessage('INFO', $successMessage);
            $this->redirect('app.php?action=listusers');
        }
        else
        {
            AddSessionMessage('ERROR', $failureMessage);
            $this->redirect('app.php?action=edituser&recordid='.$user->recordid);
        }
    }

    /**
     * Saves a user record and redirects to users listing
     */
    function saveuser()
    {
        require_once 'Source/Security/SecurityBase.php';
        require_once 'Source/Security/SecurityUsers.php';
        require_once 'Source/libs/UDF.php';
        require_once 'Source/libs/Email.php';

        $Factory = new UserModelFactory();

        //Posted password information contains data that is either not yet stored in the user entity (password) or
        //will never be (repeated password field). Therefore this validation must be done outside the object.
        $ValidationError = validateUserPasswordFromPost();

        //These validation elements can be removed when we get rid of the Location/Type settings screen
        if ($this->request->getParameter('DIF_STA_EMAIL_LOCS') &&
            ($this->request->getParameter('DIF_PERMS') != 'RM' &&
                $this->request->getParameter('DIF_PERMS') != 'DIF2'))
        {
            AddSessionMessage('ERROR', _tk('inc_email_option_err'));
            $Error = true;
        }

        if ($this->request->getParameter('RISK_STA_EMAIL_LOCS') &&
            ($this->request->getParameter('RISK_PERMS') != 'RM' &&
                $this->request->getParameter('RISK_PERMS') != 'RISK2'))
        {
            AddSessionMessage('ERROR', _tk('ram_email_option_err'));
            $Error = true;
        }

        if ($this->request->getParameter('PAL_STA_EMAIL_LOCS') &&
            ($this->request->getParameter('PAL_PERMS') != 'RM' &&
                $this->request->getParameter('PAL_PERMS') != 'PAL2'))
        {
            AddSessionMessage('ERROR', _tk('pal_email_option_err'));
            $Error = true;
        }

        if ($this->request->getParameter('COM_STA_EMAIL_LOCS') &&
            ($this->request->getParameter('COM_PERMS') != 'RM' &&
                $this->request->getParameter('COM_PERMS') != 'COM2'))
        {
            AddSessionMessage('ERROR', _tk('com_email_option_err'));
            $Error = true;
        }

        if ($this->request->getParameter('CLA_STA_EMAIL_LOCS') &&
            ($this->request->getParameter('COM_PERMS') != 'RM' &&
                $this->request->getParameter('CLA_PERMS') != 'CLA2'))
        {
            AddSessionMessage('ERROR', _tk('cla_email_option_err'));
            $Error = true;
        }

        if ($ValidationError || $Error)
        {
            //need to reformat dates before sending them to the form.
            $aDateValues = CheckDatesFromArray(GetAllFieldsByType('ADM', 'date'),$this->request->getParameters());
            $data = SafelyMergeArrays(array($this->request->getParameters(), $aDateValues));

            $this->call('src\\users\\controllers\\UserTemplateController', 'edituser', $data);
            obExit();
        }

        GetSectionVisibility('ADM', 2, $this->request->getParameters());
        BlankOutPostValues('ADM', 2, null, null, $this->request);

        $FormDesign = \Forms_FormDesign::GetFormDesign(array('module' => 'ADM', 'level' => 2));
        $FormDesign->LoadFormDesignIntoGlobals();

        $sta = ParseSaveData(array('module' => 'ADM', 'data' => $this->request->getParameters(), 'form_design' => $FormDesign));
        $saveLocType = true;

        if ($sta['recordid'] == '')
        {
            $NewUser = true;
            $user = $Factory->getEntityFactory()->createObject($sta);
            $user->permission = GetDefaultPermissionString(); // Main application default permission string
        }
        else
        {
            $NewUser = false;
            $currentUser = $Factory->getMapper()->find($sta['recordid']);
            $user = $Factory->getEntityFactory()->createObject($sta, $currentUser);
            DoFullAudit('ADM', 'contacts_main', \Sanitize::SanitizeInt($sta['recordid']));

            $saveLocType = count(GetUserSecurityGroups($sta['recordid'])) == 0 && !$this->request->getParameter('no_loctype_save');
        }

        //Ensure user object is valid
        $userValidationErrors = $user->getValidationErrors();

        if (!empty($userValidationErrors))
        {
            foreach ($userValidationErrors as $field => $errors)
            {
                foreach ($errors as $error)
                {
                    AddValidationMessage($field, $error);
                }
            }

            $this->response = $this->call('src\\users\\controllers\\UserTemplateController', 'edituser', $user->getVars());
            obExit();
        }

        //this can be removed when we remove the Location/Type settings screen
        if ($saveLocType)
        {
            $user->sta_orgcode = $sta['inc_organisation'];
            $user->sta_unit = $sta['inc_unit'];
            $user->sta_clingroup = $sta['inc_clingroup'];
            $user->sta_specialty = $sta['inc_specialty'];
            $user->sta_directorate = $sta['inc_directorate'];
        }
        else
        {
            unset($sta['inc_organisation']);
            unset($sta['inc_unit']);
            unset($sta['inc_clingroup']);
            unset($sta['inc_specialty']);
            unset($sta['inc_directorate']);
        }

        if ($sta['CHANGED-lockout'] && $sta['lockout'] == 'N')
        {
            $user->login_tries = 0;
            $user->sta_lockout_dt = '';
            $user->sta_lockout_reason = '';
            $user->sta_last_login = '';
        }

        $currentlocations = \DatixDBQuery::PDO_fetch('SELECT con_hier_location FROM contacts_main WHERE recordid = :recordid', array('recordid' => $user->recordid), \PDO::FETCH_COLUMN);

        $user->con_hier_location = AddRestrictedLocations($user->con_hier_location, $currentlocations);

        //Validation is now complete- we can start saving the passed data. A lot of the subsequent steps
        //can be moved eventually to the model, once the elements being saved have themselves been modelled.
        if (!$Factory->getMapper()->save($user))
        {
            AddSessionMessage('ERROR', _tk('err_cannot_save_user'));
            $this->redirect('app.php?action=edituser');
        }
        else if($NewUser)
        {
            $this->setInitialUpdate($user->recordid);
        }

        //Set new password
        if ($this->request->getParameter('password') != '')
        {
            SetUserPassword($user->login, $this->request->getParameter('password'));
        }

        if ($this->request->getParameter('clearUserSettings'))
        {
            // Delete configuration parameters from the main page (sometimes used when adding user to a profile)
            deleteConfigurationParameters($sta);
            deleteSecurityGroups($sta);
            deleteLocTypeSettings($sta);
        }
        else
        {
            // Save configuration parameters from the main page
            saveConfigurationParameters($sta);

            // Save information from the location/type settings panel
            // if the user is not a member of any security group
            // if there are no groups, but no_loctype_save is passed, then
            // the groups have just been deleted, so we can't save the
            // location/type values, since they won't have been passsed.
            if ($saveLocType)
            {
                saveLocationTypeSettings($sta);
            }
        }

        SaveUDFs($user->recordid, MOD_CONTACTS);

        // Send e-mail notifications for new user accounts
        // This could be implemented as an observer, once we're sure we know everywhere new user entities are created.
        // Oherwise we risk sending unwanted emails.
        if ($NewUser && bYN(GetParm('EMAIL_NEW_ACCOUNTS', 'N')) && $user->con_email != '')
        {
            $emailSender = EmailSenderFactory::createEmailSender('INC', 'RegistrationApproved');
            $emailSender->setModuleToStore('ADM');
            $emailSender->addRecipient($user);
            $emailSender->sendEmails($this->request->getParameters());

            if (count($emailSender->getFailedDomainValidation()) > 0)
            {
                AddSessionMessage('ERROR', 'An e-mail was not sent to the user as their domain is not permitted.');
            }
        }

        //This is not very nice - TODO: identify more clearly the routing options available here
        if ($sta['redirect_url'])
        {
            $this->redirect($sta['redirect_url'] . "&recordid=" . $user->recordid);
        }

        AddSessionMessage('INFO', sprintf(_tk('msg_user_saved'), $user->login));

        $this->redirect('app.php?action=listusers&JumpToName=' . $user->con_surname);
    }

    /*
     * Used for setting a new users 'updatedby' and 'updateddate' on the database
     *
     * @param $recordid The recordid of the user to be updated
     */
    private function setInitialUpdate($recordid)
    {
        $now = new \DateTime();

        $qf = new \src\framework\query\QueryFactory();
        $updateQuery = new \src\framework\query\Query($qf);

        $updateQuery->update('contacts_main')->set(array('contacts_main.updateddate' => $now->format('Y-m-d H:i:s.000'),
                        'contacts_main.updatedby' => $_SESSION['initials'],
                        'contacts_main.updateid' => '1'))
            ->where(array('contacts_main.recordid' => $recordid));
        list($sql, $parameters) = $qf->getSqlWriter()->writeStatement($updateQuery);

        $db = new \DatixDBQuery('');

        $db->setSQL($sql);
        $db->prepareAndExecute($parameters);
    }
}