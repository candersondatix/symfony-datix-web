<?php
namespace src\users\model\field;

use src\system\database\field\CodeField;
use src\framework\query\Query;
use src\framework\query\QueryFactory;
use src\framework\model\exceptions\ModelException;
use src\savedqueries\model\SavedQueryModelFactory;

/**
 * Displays staff members.
 */
class StaffField extends CodeField
{    
    /**
     * {@inheritdoc}
     */
    public function getDescriptionColumn()
    {
        if ($this->registry->getParm('CONTACT_SURNAME_SORT', 'Y', false, true))
        {
            $pattern = "COALESCE(?, '') + ', ' + COALESCE(?, '')  + ' ' + COALESCE(?, '')";
            $fields  = ['sta_surname', 'sta_title', 'sta_forenames'];
        }
        else
        {
            $pattern = "COALESCE(?, '') + ' ' + COALESCE(?, '')  + ' ' + COALESCE(?, '')";
            $fields  = ['sta_title', 'sta_forenames', 'sta_surname'];
        }
        
        if ($this->registry->getParm('STAFF_NAME_DROPDOWN', 'N') == 'A')
        {
            $pattern .= " + ' - ' + COALESCE(?, '')";
            $fields[] = 'jobtitle';
        }
        else if ($this->registry->getParm('STAFF_NAME_DROPDOWN', 'N') == 'B')
        {
            $pattern  = "COALESCE(?, '') + ' - ' + " . $pattern;
            array_unshift($fields, 'jobtitle');
        }
        
        return [$pattern, $fields];
    }
    
    /**
     * {@inherit}
     */
    public function getCodeOrderColumn()
    {
        return 'description';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParents()
    {
        if (!$this->registry->getParm('STAFF_EMPL_FILTERS', 'N', true, true))
        {
            return [];
        }
        return parent::getParents();
    }

    /**
     * {@inherit}
     */
    protected function applyFormQuery(Query $query, SavedQueryModelFactory $factory = null)
    {
        $factory = $factory ?: new SavedQueryModelFactory();
        $qf      = $factory->getQueryFactory();
        
        parent::applyFormQuery($query, $factory);
        
        $this->applyLevelOneQuery($query);

        $this->applyContactsSecurityQuery($query, $qf);
        
        $this->applyActiveStaffQuery($query, $qf);
    }

    /**
     * Appends the conditions used to filter out inactive codes. This is triggered by the STAFF_EMPL_FILTERS
     * global being set to N for staff fields.
     *
     * @param Query        $query   The query object we're appending conditions to.
     * @param QueryFactory $factory Used to access the Query objects.
     */
    protected function applyCodeStatusQuery(Query $query, QueryFactory $factory)
    {
        if (!bYN(GetParm('STAFF_EMPL_FILTERS', 'N')) && 1 != $this->formData['childcheck'])
        {
            $query->join('code_parents', null, 'LEFT', 'code_parents.cop_field = \''.$this->getName().'\' AND code_parents.cop_code = initials');

            $statusConditions = $factory->getFieldCollection()->field('cod_priv_level')->notEq('X');

            if ('search' != strtolower($this->formData['fieldmode']))
            {
                $statusConditions->notEq('N');
            }

            $where = $factory->getWhere();
            $where->add($statusConditions);

            $query->where($where);
        }
    }


    /**
     * Appends the conditions used to filter staff on level one forms.
     * 
     * NB - I'm not 100% happy with the raw SQL defined here, but some of the query statements required are
     *      currently too complex to be defined within Query objects and it's probably overkill to do so anyway.
     *      It might make more sense for a StaffCodeMapper to handle this, but that would be a bit of a faff because the
     *      Mapper would then have to be made aware of the form data too, and in any case the purpose of applyFormQuery() 
     *      (and, by extension, this method) is to create and append query conditions based on the form data before passing the query
     *      off to the Mapper (via a CodeCollection), so in that sense what we're doing here is at least consistent with that idea.
     * 
     * @param Query $query The query object we're appending conditions to.
     */
    protected function applyLevelOneQuery(Query $query)
    {
        if (!$_SESSION['logged_in'] || 'newdif1' == $this->formData['action'] || '1' == $this->formData['level'])
        {
            $module = $this->formData['module'];
            $moduleDefs = $this->registry->getModuleDefs();
            
            // there are no conventions with regards to access levels, so we need to hard-code a list of
            // "level 2 access and level 1 review" statuses for each module
            $levels = [
                'INC' => ['DIF2', 'RM'],
                'RAM' => ['RISK2', 'RM'],
                'PAL' => ['PAL2', 'RM'],
                'COM' => ['COM2'],
                'CLA' => ['CLA2'],
            ];
            
            if (is_array($levels[$module]))
            {
                $accessLevelWhere = " AND sec_group_permissions.perm_value in ('".implode("', '", $levels[$module])."')";
                $userParmsWhere   = " AND user_parms.parmvalue in ('".implode("', '", $levels[$module])."')";
            }
            
            $query->setWhereStrings([
                $this->getCodeTable().".recordid IN (
                    SELECT
                        sec_staff_group.con_id
                    FROM
                        sec_group_permissions
                    INNER JOIN
                        sec_staff_group ON sec_group_permissions.grp_id = sec_staff_group.grp_id
                    WHERE
                        sec_group_permissions.item_code = '".$moduleDefs[$module]['PERM_GLOBAL']."'"
                    .$accessLevelWhere."

                    UNION

                    SELECT
                       contacts_main.recordid
                    FROM
                       contacts_main
                    INNER JOIN
                       profiles ON contacts_main.sta_profile = profiles.recordid
                    INNER JOIN
                       link_profile_group ON profiles.recordid = link_profile_group.lpg_profile
                    INNER JOIN
                       sec_group_permissions ON link_profile_group.lpg_group = sec_group_permissions.grp_id
                    WHERE
                       sec_group_permissions.item_code = '".$moduleDefs[$module]['PERM_GLOBAL']."'"
                    .$accessLevelWhere."
                )
                OR
                (
                   (SELECT 
                        count(*)
                    FROM 
                        sec_staff_group
                    INNER JOIN
                        sec_groups on sec_staff_group.grp_id = sec_groups.recordid
                    WHERE
                        sec_staff_group.con_id = ".$this->getCodeTable().".recordid
                    AND 
                        sec_groups.grp_type = (sec_groups.grp_type | 0)
                    ) = 0
                            
                    AND ".$this->getCodeTable().".login IN (
                        SELECT
                            user_parms.login
                        FROM
                            user_parms
                        WHERE
                            user_parms.parameter = '".$moduleDefs[$module]['PERM_GLOBAL']."'"
                        .$userParmsWhere."
                    )
                )
            "]);
        }
    }
    
    /**
     * Appends the Contacts module security where clause to the code query 
     * when using the CONTACTS_CLAUSE_AFFECTS_STAFF_FIELDS global.
     * 
     * @param Query        $query   The query object we're appending conditions to.
     * @param QueryFactory $factory Used to access the Query objects.
     */
    protected function applyContactsSecurityQuery(Query $query, QueryFactory $factory)
    {
        if ($_SESSION['logged_in'] && $this->registry->getParm('CONTACTS_CLAUSE_AFFECTS_STAFF_FIELDS', 'Y', false, true))
        {
            list($conSubQuery, $conSecWhere, $conSecConditions) = $factory->getQueryObjects();
            
            $conSecWhere->add(
                $conSecConditions->field($this->getCodeTable().'.recordid')->in(
                    $conSubQuery->select(['contacts_main.recordid']) // the security where clause is automatically appended by the SQL Writer
                )
            );
            
            $query->where($conSecWhere);
        }
    }
    
    /**
     * Appends the conditions used to filter out inactive staff members.
     * 
     * @param Query        $query   The query object we're appending conditions to.
     * @param QueryFactory $factory Used to access the Query objects.
     */
    protected function applyActiveStaffQuery(Query $query, QueryFactory $factory)
    {
        $today = (new \DateTime)->format('Y-m-d H:i:s');
        
        $activeWhere = $factory->getWhere();
        $activeWhere->add(
            $activeWhere->nest('OR',
                $factory->getFieldCollection()->field($this->getCodeTable().'.sta_dclosed')->isNull(),
                $factory->getFieldCollection()->field($this->getCodeTable().'.sta_dclosed')->gt($today)
            ),
            $activeWhere->nest('OR',
                $factory->getFieldCollection()->field($this->getCodeTable().'.sta_dopened')->isNull(),
                $factory->getFieldCollection()->field($this->getCodeTable().'.sta_dopened')->lt($today)
            )  
        );

        $query->where($activeWhere);
    }
    
    /**
     * {@inherit}
     * 
     * @throws ModelException If the mapped staff property is not found.
     */
    protected function getCodeParentColumn($num)
    {
        // map system-wide field parents to staff property equivalents
        $moduleDefs = $this->registry->getModuleDefs();
        
        $map = $moduleDefs[$this->formData['module']]['STAFF_EMPL_FILTER_MAPPINGS'][$this->formData['parent'.$num]];
        
        if ('' == $map)
        {
            $message = 'Cannot retrieve codes for '.$this->getName().': unable to find mapped staff field for '.$this->formData['parent'.$num];
            $this->registry->getLogger()->logEmergency($message);
            throw new ModelException($message);
        }
        
        return $this->getCodeTable().'.'.$map;
    }
}