<?php
namespace src\users\model;

use src\email\EmailSender;
use src\framework\model\DatixEntity;
use src\framework\registry\Registry;

/**
 * This entity models a user who can log in to the system
 */
class User extends DatixEntity
{
    /**
     * The approval status of the contact associated with this user
     *
     * @var string
     */
    protected $rep_approved;

    /**
     * The user's initials: used as a unique id in some link tables
     *
     * @var string
     */
    protected $initials;

    /**
     * The user's username if they want to access Datix
     *
     * @var string
     */
    protected $login;

    /**
     * Yes/No field indicating whether the user is locked out of the system or not
     *
     * @var string
     */
    protected $lockout;

    /**
     * Number of unsuccessful login attempts on this account.
     *
     * @var string
     */
    protected $login_tries;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $sta_orgcode;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $sta_clingroup;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $sta_unit;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $sta_directorate;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $sta_specialty;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $sta_loctype;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $sta_locactual;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $logins;

    /**
     * Location field used for location-based user security
     *
     * @var string
     */
    protected $last_date;

    /**
     * The domain part of the user's login
     *
     * @var string
     */
    protected $sta_domain;

    /**
     * The user's login without the domain part
     *
     * @var string
     */
    protected $login_no_domain;

    /**
     * The date on which the user's access to the system will start.
     *
     * @var string
     */
    protected $sta_daccessstart;

    /**
     * The date on which the user's access to the system will end.
     *
     * @var string
     */
    protected $sta_daccessend;

    /**
     * YesNo field indicating whether the user will have to change their password when they next login.
     *
     * @var string
     */
    protected $sta_pwd_change;

    /**
     * Date that the user was locked out of the system
     *
     * @var date
     */
    protected $sta_lockout_dt;

    /**
     * Reason that the user was last locked out of the system
     *
     * @var string
     */
    protected $sta_lockout_reason;

    /**
     * Last login date
     *
     * @var date
     */
    protected $sta_last_login;


    //The following are columns that are simply renamed contacts properties.

    /**
     *  Calculated field that determines the full contact name
     *
     * @var string
     */
    protected $fullname;


    /**
     * Contact's job title
     *
     * @var string
     */
    protected $con_jobtitle;

    /**
     * The contact's telephone number
     *
     * @var string
     */
    protected $con_tel1;

    /**
     * Alternate telephone number
     *
     * @var string
     */
    protected $con_tel2;

    /**
     * The contact's fax number
     *
     * @var string
     */
    protected $con_fax;

    /**
     * Date contact record was "opened"
     *
     * @var date
     */
    protected $con_dopened;

    /**
     * Date contact record was "closed"
     *
     * @var date
     */
    protected $con_dclosed;

    /**
     * Coded field to classify contact.
     *
     * @var string
     */
    protected $con_empl_grade;

    /**
     * Contact payroll number.
     *
     * @var string
     */
    protected $con_payroll;

    /**
     * The contact's email address
     *
     * @var string
     */
    protected $con_email;

    /**
     * A unique id from the AD. Used when syncing via LDAP
     *
     * @var string
     */
    protected $con_sid;

    /**
     *  The profile that this user is in
     *
     * @var string
     */
    protected $sta_profile;

    /**
     *  ???
     *
     * @var string
     */
    protected $sta_user_type;

    /**
     *  ???
     *
     * @var string
     */
    protected $sta_activate_code;

    /**
     * Mr/Mrs/Ms etc.
     *
     * @var string
     */
    protected $con_title;

    /**
     * Contact first/middle names
     *
     * @var string
     */
    protected $con_forenames;

    /**
     * Contact surname
     *
     * @var string
     */
    protected $con_surname;

    /**
     * The contact's organisation (note: this is a string, not a coded location field)
     *
     * @var string
     */
    protected $con_organisation;

    /**
     * The contact's address.
     *
     * @var string
     */
    protected $con_address;

    /**
     * The contact's postcode
     *
     * @var string
     */
    protected $con_postcode;

    /**
     * Contact's gender (coded field)
     *
     * @var string
     */
    protected $con_gender;

    /**
     * Contact's date of birth
     *
     * @var date
     */
    protected $con_dob;

    /**
     * Contact's date of death
     *
     * @var date
     */
    protected $con_dod;

    /**
     * Hospital ID, e.g. Staff/Patient number
     *
     * @var number
     */
    protected $con_number;

    /**
     * Contact's ethnicity (coded field)
     *
     * @var string
     */
    protected $con_ethnicity;

    /**
     * Contact's language (coded field)
     *
     * @var string
     */
    protected $con_language;

    /**
     * Contact's disability (coded field)
     *
     * @var string
     */
    protected $con_disability;

    /**
     * Coded field to classify contact.
     *
     * @var string
     */
    protected $con_type;

    /**
     * Coded field to classify contact.
     *
     * @var string
     */
    protected $con_subtype;

    /**
     * Free text field for general contact notes.
     *
     * @var string
     */
    protected $con_notes;

    /**
     * The Police ID number for this contact. Only really applies if this contact is a member of the police.
     *
     * @var string
     */
    protected $con_police_number;

    /**
     * Has this contact been assessed as to the suitability of them working without supervision?.
     *
     * @var string
     */
    protected $con_work_alone_assessed;

    /**
     * The set of locations this contact belongs to. Only applies if using hierarchical locations.
     *
     * @var string
     */
    protected $con_hier_location;

    /**
     * YesNo field indicating whether a contact should be included in staff dropdowns.
     *
     * @var string
     */
    protected $con_staff_include;

    /**
     * Contact nhs number. This has a specific format and shouldn't be reused for anything else.
     *
     * @var string
     */
    protected $con_nhsno;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_orgcode;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_clingroup;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_unit;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_directorate;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_specialty;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_loctype;

    /**
     * Multicode location field indicating employee's area of work.
     *
     * @var string
     */
    protected $con_locactual;

    /**
     * Text field holding permission string from RC
     *
     * @var string
     */
    protected $permission;

    /**
     * Holds random hash used to identify user when resetting password
     *
     * @var string
     */
    protected $sta_pwd_reset_code;

    /**
     * @varHolds an array of access levels that this user belongs to
     *
     * @var array
     */
    protected $accessLevels = array();

    /**
     * Once globals are represented as entities, this will be a collection of those.
     * For now it is just an array of global name => value for this user
     * @var array
     */
    protected $user_parameters;

    public function getsta_title()
    {
        return $this->con_title;
    }

    public function getsta_surname()
    {
        return $this->con_surname;
    }

    public function getsta_forenames()
    {
        return $this->con_forenames;
    }

    public function getemail()
    {
        return $this->con_email;
    }

    public function getjobtitle()
    {
        return $this->con_jobtitle;
    }

    /*
     * Users are always assumed to be finally approved.
     */
    public function getrep_approved()
    {
        return 'FA';
    }

    /*
     * Not sure why this is required, but it was specifically coded for when saving users.
     * TODO: Check whether this matters.
     */
    public function getsta_profile()
    {
        if($this->sta_profile == '')
        {
            return null;
        }

        return $this->sta_profile;
    }

    /**
     * @desc Checks object data for validity.
     *
     * @return bool true if problems are found, false otherwise
     */
    function getValidationErrors()
    {
        $validationErrors = array();

        //TODO: Overwrite this for newly registering users
        if ($this->initials == '')
        {
            $validationErrors['initials'][] = 'Initials cannot be blank';
        }

        if ($this->login == '')
        {
            $validationErrors['login'][] = 'Login name cannot be blank';
        }

        require_once 'Date.php';

        if ($this->sta_daccessstart && $this->sta_daccessend)
        {
            if (\Date_Calc::IsDateGreater($this->sta_daccessstart, $this->sta_daccessend) == 1)
            {
                $validationErrors['sta_daccessend'][] = 'The access end date cannot be earlier than the access start date';
            }
        }

        if ($this->initials && preg_match('/[\[\]\*\?\|\&\\\"\'\!\<\>\=\-\+\%\(\)\,\:@~#\ `\/]/u', $this->initials))
        {
            $validationErrors['initials'][] = _tk('invalid_char_initials_err');
        }

        if (\UnicodeString::strlen($this->login) && \UnicodeString::trim($this->login) == '')
        {
            $validationErrors['login'][] = 'Invalid character in login name.';
        }

        // Exclude characters not permitted in windows usernames.
        // Old expression: (!preg_match("/^([0-9A-Za-z-]{0,})([\\\\]{0,1})([0-9A-Za-z#%,-\.:=?@^_������������������������������]+)$/", $username))
        if ($this->login &&
            !preg_match("/^([0-9A-Za-z-_]{0,})([\\\\]{0,1})([^\"\\\:\;\|\=\,\+\*\?\<\>\(\)]+)$/u", $this->login))
        {
            $validationErrors['login'][] = 'Invalid character in login name.';
        }

        //If validation errors have been spotted so far, we don't want to run any sql queries with this data
        if ($this->recordid == '' && empty($validationErrors) && $this->initials != '')
        {
            //search for other users with same initials
            $Factory = new UserModelFactory();
            $User = $Factory->getMapper()->findByInitials($this->initials, true);

            if ($User != null)
            {
                $validationErrors['initials'][] = 'Initials ' . $this->initials . ' are already in use.';
            }
        }

        if (empty($validationErrors) && $this->login != '')
        {
            // Check that login name does not already exist
            $sql = 'SELECT login FROM contacts_main WHERE login = :login';
            $PDOParam = array('login' => $this->login);

            if ($this->recordid)
            {
                $sql .= " AND recordid != :recordid";
                $PDOParam["recordid"] = \Sanitize::SanitizeInt($this->recordid);
            }

            $row = \DatixDBQuery::PDO_fetch($sql, $PDOParam);

            if (!empty($row))
            {
                $validationErrors['login'][] = 'Login name '.$this->login.' is already in use.';
            }
        }

        return $validationErrors;
    }

    /**
     * Checks whether the currently logged in user can edit this user object.
     *
     * @return bool
     */
    public function isEditable()
    {
        if($this->recordid == '')
        {
            return true;
        }

        //If the current user is a full admin, they can edit all users
        if(IsFullAdmin(true))
        {
            return true;
        }

        //If the current user is not an admin, they can't edit users
        if(!IsSubAdmin(false))
        {
            return false;
        }

        $sql = 'SELECT count(*) as num FROM staff';
        $WhereClause = MakeSecurityWhereClause(array('recordid = '.$this->recordid), 'ADM');
        $sql .= ' WHERE '.$WhereClause;

        $resultarray = \DatixDBQuery::PDO_fetch($sql);

        return (intval($resultarray['num']) > 0);
    }
    

    /**
     * Returns true if user has permissions to add new records to the passed module and false if not.
     * In case of Complaints module we need to have into account the global COM_SHOW_ADD_NEW. If this global is set
     * to Y and the user has permissions to add new records then this will return true, otherwise this will return false.
     *
     * @param $module
     * @param $registry Registry
     * @return bool
     */
    public function canAddRecords($module, Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();

        $canAddRecords = false;

        if ($module == 'INC' && !$registry->getParm('DIF2_ADD_NEW', 'Y', false, true) && !in_array('DIF1', $this->getAccessLevels($module)))
        {
            return false;
        }

        if ($this->canCreateNewRecords($module))
        {
            $canAddRecords = true;
        }

        if ($module == 'COM' && !$registry->getUserParm($_SESSION['login'], 'COM_SHOW_ADD_NEW', 'Y', true))
        {
            $canAddRecords = false;
        }

        return $canAddRecords;
    }

    /**
     * Returns true if this user has permission to see the passed module (i.e. has any access to it at all, even if they can't see any records).
     * @param $module
     * @param $registry Registry
     * @return bool
     */
    public function canSeeModule($module, Registry $registry = null)
    {
        $registry = $registry ?: Registry::getInstance();

        switch($module)
        {
            case 'ACT':
                $HasPerms = (count($this->getAccessLevels($module)) > 0 &&
                    $this->notOnlyAccessLevel('ACT', 'ACT_INPUT_ONLY'));
                break;
            case 'MED':
                $HasPerms = (count($this->getAccessLevels($module)) > 0 && 
                    $registry->getParm('MULTI_MEDICATIONS', 'N', false, true));
                break;
            case 'ADM':
                $HasPerms = !(bYN(GetParm('WEB_NETWORK_LOGIN')) && 
                    !$registry->getParm('RECORD_LOCKING', '', false, true) &&
                    !IsSubAdmin());
                break;
            default:
                $HasPerms = count($this->getAccessLevels($module)) > 0;
                break;
        }

        return $HasPerms;
    }

    /**
     * Returns an array containing the codes of all access levels assigned to this contact. If only one access level is permitted
     * (i.e. USE_ADVANCED_ACCESS_LEVELS is N), the array will only contain the highest level.
     *
     * Caches the value in the $accessLevels property.
     * @param $module
     * @param $getOnlyHighestAccessLevel bool If set to true, will only return one access level: the highest set for that user.
     * @return array
     */
    public function getAccessLevels($module, $getOnlyHighestAccessLevel = false)
    {
        if(!$this->accessLevels[$module][$getOnlyHighestAccessLevel])
        {
            $ModuleDefs = Registry::getInstance()->getModuleDefs();

            // If the user is member of at least one security group then check highest Access level for record
            require_once 'Source/security/SecurityBase.php';
            $groups = GetUserSecurityGroups($this->recordid, GRP_TYPE_ACCESS);

            $accessLevels = array();

            if (count($groups))
            {
                foreach ($groups as $grp_id)
                {
                    $sql = '
                    SELECT
                        perm_value
                    FROM
                        sec_group_permissions
                    WHERE
                        grp_id = :grp_id AND item_code = :param AND perm_value != \'\' AND perm_value is not null';

                    $accessLevel = \DatixDBQuery::PDO_fetch($sql, array('grp_id' => $grp_id, 'param' => $ModuleDefs[$module]['PERM_GLOBAL']), \PDO::FETCH_COLUMN);

                    if ($accessLevel != '')
                    {
                        $accessLevels[] = $accessLevel;
                    }
                }

                $accessLevels = array_unique($accessLevels);

                //Dashboard module has no where clause, so the highest access level should always be used.
                if($getOnlyHighestAccessLevel || (count(array_unique($accessLevels)) > 1 && !bYN(GetParm('USE_ADVANCED_ACCESS_LEVELS', 'N'))) || $module == 'DAS')
                {
                    $topAccessLevel = \DatixDBQuery::PDO_fetch('SELECT TOP 1 acl_code FROM ACCESS_LEVELS WHERE acl_code IN (\''.implode('\',\'', $accessLevels).'\') ORDER BY acl_order DESC', array(), \PDO::FETCH_COLUMN);

                    $accessLevels = array($topAccessLevel);
                }
            }
            // not in any groups, but might have a location/type setting.
            else if (GetParm($ModuleDefs[$module]['PERM_GLOBAL']))
            {
                $accessLevels = array(GetParm($ModuleDefs[$module]['PERM_GLOBAL']));
            }

            $this->accessLevels[$module][$getOnlyHighestAccessLevel] = $accessLevels;
        }

        return $this->accessLevels[$module][$getOnlyHighestAccessLevel];    }

    /**
     * Used as a shortcut function to avoid having multiple messy conditions elswhere in the code.
     * Returns false if the user is in the passed access level and only in that access level.
     *
     * @param $module
     * @param $accessLevel
     * @return bool
     */
    public function notOnlyAccessLevel($module, $accessLevel)
    {
        if(count($_SESSION['CurrentUser']->getAccessLevels($module)) == 1 && in_array($accessLevel, $_SESSION['CurrentUser']->getAccessLevels($module)))
        {
            return false;
        }

        return true;
    }

    /**
     * Returns a single valid email for this user.
     * Used when multiple email addresses are not appropriate (e.g. setting the From: header on an email.)
     */
    public function getFirstValidEmail()
    {
        return trim(explode(',', $this->con_email)[0]);
    }

    /**
     * @param $module
     * @return boolean
     */
    public function canCreateNewRecords($module)
    {
        $moduleDefs = Registry::getInstance()->getModuleDefs();

        if (!ModIsLicensed($module) || !CanSeeModule($module))
        {
            return false;
        }

        if (!$moduleDefs[$module]['USES_APPROVAL_STATUSES'])
        {
            if ($module == 'DAS')
            {
                if (!in_array('DAS_FULL', $this->getAccessLevels($module)))
                {
                    return false;
                }
            }

            if ($module == 'POL')
            {
                if (!in_array('POL_FULL', $this->getAccessLevels($module)))
                {
                    return false;
                }
            }

            if ($module == 'ORG')
            {
                if (!in_array('ORG_FULL', $this->getAccessLevels($module)))
                {
                    return false;
                }
            }

            return true; //not enough information on specific modules to declare otherwise: eventually this will need to cater for all custom module logic.
        }

        $accessLevels = implode("','", $this->getAccessLevels($module));

        $accessLevelsAllowingNewRecords =
            \DatixDBQuery::PDO_fetch('SELECT count(*) FROM approval_action WHERE apac_from = :apac_from AND module=:module AND access_level IN (\''.$accessLevels.'\')',
                array('module' => $module, 'apac_from' => 'NEW'),
                \PDO::FETCH_COLUMN
            );

        return ($accessLevelsAllowingNewRecords > 0);
    }

    /**
     * Checks if this User is emailable.
     *
     * @return bool
     */
    public function isEmailable()
    {
        // If the User is different from the person logged in, they have an email address and they are not dead then it is emailable.
        $isEmailable = ($this->initials != $_SESSION['initials'] && $this->con_email != '' && empty($this->con_dod));

        return $isEmailable;
    }
}