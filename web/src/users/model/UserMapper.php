<?php
namespace src\users\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\contacts\model\ContactMapper;

/**
 * Manages the persistance of WordMergeTemplate objects.
 */
class UserMapper extends ContactMapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\users\\model\\User';
    }

    /**
     * {@inheritdoc}
     */
    public function getView()
    {
        return 'staff';
    }


    /**
     * {@inheritdoc}
     *
     * We need to override this here because not all properties in this entity correspond to columns in the
     * table to save (since a view is used). In general, a column mapping could be used here, but until we know
     * how common this situation is, I'm implementing it as an overridden method.
     */
    protected function doUpdate(Entity $object)
    {
        $identity = $object::getIdentity();
        $where    = array();
        foreach ($identity as $id)
        {
            $where[$id] = $object->$id;
        }

        $properties = $this->getEntityProperties($object);

        foreach(array('login_no_domain','sta_title','sta_forenames', 'sta_surname', 'email', 'jobtitle') as $staffField)
        {
            unset($properties[$staffField]);
        }

        $query = $this->factory->getQueryFactory()->getQuery();
        $query->update($this->getTable())
            ->set($properties)
            ->where($where);

        // We need to override the query security because the module is being set and the where security clause
        // is causing problems creating users.
        $query->overrideSecurity();

        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);

        $this->db->setSQL($sql);
        return $this->db->prepareAndExecute($parameters);
    }
    
    
    /**
     * {@inheritdoc}
     *
     * We need to override this here because we need to clear user parameters and we need to override the query security.
     */
    protected function doDelete(Entity $object)
    {
        \DatixDBQuery::PDO_query('DELETE FROM user_parms WHERE login = :login', array('login' => $object->login));

        \DatixDBQuery::PDO_query('DELETE FROM PASSWORDS WHERE PWD_LOGIN = :PWD_LOGIN', array('PWD_LOGIN' => '_'.$object->login));
        
        $identity = $object::getIdentity();
        $where    = array();

        foreach ($identity as $id)
        {
            $where[$this->mapField($id)] = $object->$id;
        }

        $query = $this->factory->getQueryFactory()->getQuery();
        $query->delete()
            ->from($this->getTable())
            ->where($where);

        $query->overrideSecurity();

        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);

        $this->db->setSQL($sql);
        return $this->db->prepareAndExecute($parameters);
    }    


    /**
     * {@inheritdoc}
     *
     * We need to provide multiple interfaces to the find functionality because they are sometimes referred
     * to by initials, sometimes by login and sometimes by id.W
     *
     * @return Entity|NULL
     */
    public function findById($id, $overrideSecurity = false)
    {
        return parent::find($id, $overrideSecurity);
    }

    /**
     * {@inheritdoc}
     *
     * We need to provide multiple interfaces to the find functionality because they are sometimes referred
     * to by initials, sometimes by login and sometimes by id.
     *
     * This function retrieves a User entity by initials. It does not support the entity cache.
     *
     * @return Entity|NULL
     */
    public function findByInitials($initials, $overrideSecurity = false)
    {
        // attempt to fetch the entity from the database
        list($query, $where, $fields) = $this->factory->getQueryFactory()->getQueryObjects();

        $where->add($fields->field($this->getDBObjectForSelect().'.initials')->eq($initials));
        $query->where($where);

        if ($overrideSecurity)
        {
            $query->overrideSecurity();
        }

        $result = $this->select($query);

        if (!empty($result))
        {
            return $this->factory->getEntityFactory()->createObject($result[0]);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     *
     * We need to provide multiple interfaces to the find functionality because they are sometimes referred
     * to by initials, sometimes by login and sometimes by id.
     *
     * This function retrieves a User entity by login. It does not support the entity cache.
     *
     * @return Entity|NULL
     */
    public function findByLogin($login, $overrideSecurity = false)
    {
        // attempt to fetch the entity from the database
        list($query, $where, $fields) = $this->factory->getQueryFactory()->getQueryObjects();

        $where->add($fields->field($this->getDBObjectForSelect().'.login')->eq($login));
        $query->where($where);

        if ($overrideSecurity)
        {
            $query->overrideSecurity();
        }

        $result = $this->select($query);

        if (!empty($result))
        {
            return $this->factory->getEntityFactory()->createObject($result[0]);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     *
     * Used to hydrate the current user entity after login.
     * Ignores the security where clause for Administration since not all users will have one.
     *
     * @return Entity|NULL
     */
    public function findLoggedInUser($id)
    {
        return parent::find($id, true);
    }
    
    /**
     * Gets a list of recordids of contacts who have previously 
     * received a notification e-mail for a given record.
     * 
     * @param int $recordid The ID of the record users have been notified of.
     * 
     * @return array
     */
    public function getNotificationEmailRecipients($recordid)
    {
        $query = $this->factory->getQueryFactory()->getQuery();

        $query->select(['con_id'])
              ->from('contact_email_history')
              ->where(['inc_id' => $recordid]);
        
        list($sql, $parameters) = $this->factory->getQueryFactory()->getSqlWriter()->writeStatement($query);

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($parameters);
        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_COLUMN);
    }
}