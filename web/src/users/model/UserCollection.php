<?php
namespace src\users\model;

use src\framework\model\EntityCollection;

class UserCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\users\\model\\User';
    }
}