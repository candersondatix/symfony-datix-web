<?php
namespace src\users\observers;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\framework\model\Entity;
use src\users\model\User;

class UserDeletionAuditor implements Observer
{
    /**
     * {@inheritdoc}
     *
     * Audits the deletion of a user
     *
     * @throws InvalidArgumentException If the subject is not a User.
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof User))
        {
            throw new \InvalidArgumentException('Object of type User expected, '.get_class($subject).' given.');
        }

        if ($event != Entity::EVENT_DELETE)
        {
            // this observer only listens to delete events
            return;
        }

        InsertFullAuditRecord('ADM', $subject->recordid, 'DELETE', 'User login: ' . $subject->login);
    }
}