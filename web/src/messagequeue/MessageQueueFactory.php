<?php

namespace src\messagequeue;

require_once __DIR__ . '/../../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use src\framework\registry\Registry;
use src\messagequeue\workers\Worker;

class MessageQueueFactory
{
    /**
     * @var Registry
     */
    protected $registry;
    
    public function __construct(Registry $registry = null)
    {
        $this->registry = $registry ?: Registry::getInstance();
    }
    
    /**
     * Constructs and returns a MessageQueue object.
     * 
     * @return MessageQueue
     */
    public function getQueue($server = 'localhost', $port = 5672, $username = 'guest', $password = 'guest')
    {
        $connection = new AMQPConnection($server, $port, $username, $password);
        return new MessageQueue($connection->channel(), $this);
    }
    
    /**
     * Constructs and returns a message object.
     * 
     * The message will be persisted by default.
     * 
     * @param string $body       Message content.
     * @param array  $properties Message properties.
     * 
     * @return AMQPMessage
     */
    public function getMessage($body, array $properties = ['delivery_mode' => 2])
    {
        return new AMQPMessage($body, $properties);
    }
    
    /**
     * Constructs and returns a queue worker object.
     * 
     * @param int    $queue      The queue that this worker consumes.
     * @param string $action     The controller action responsible for processing messages from this queue.
     * @param array  $parameters The message content we're forwarding to the controller.
     */
    public function getWorker($queue, $action, array $parameters = [])
    {
        return new Worker($queue, $action, $parameters, $this, $this->registry->getLogger());
    }
}