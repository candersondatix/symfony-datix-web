<?php

namespace src\messagequeue;

require_once __DIR__ . '/../../vendor/autoload.php';

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use src\messagequeue\exceptions\MessageQueueException;

class MessageQueue
{
    const EMAIL = 1;
    
    /**
     * @var AMQPChannel
     */
    protected $channel;
    
    /**
     * @var MessageQueueFactory
     */
    protected $factory;
    
    public function __construct(AMQPChannel $channel, MessageQueueFactory $factory)
    {
        $this->channel = $channel;
        $this->factory = $factory;
    }
    
    /**
     * Add a message to the queue.
     * 
     * @param int    $queue   The queue we're adding the message to.
     * @param string $message The message contents.
     */
    public function publish($queue, $message)
    {
        $this->checkQueue($queue);
        $this->declareQueue($queue);
        
        $this->channel->basic_publish($this->factory->getMessage($message), '', $this->getQueueName($queue));
    }
    
    /**
     * Set up the queue consumer.
     * 
     * @param int      $queue    The queue we're consuming messages from.
     * @param callable $callback The function to execute on the message.
     */
    public function consume($queue, callable $callback)
    {
        $this->checkQueue($queue);
        $this->declareQueue($queue);
        
        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume($this->getQueueName($queue), '', false, false, false, false, $callback);
        
        while (count($this->channel->callbacks))
        {
            $this->channel->wait();
        }
    }
    
    /**
     * Checks that the queue being accessed is valid.
     * 
     * @param int $queue The queue that's being accessed.
     * 
     * @throws MessageQueueException If the queue is invalid.
     */
    protected function checkQueue($queue)
    {
        if (!in_array($queue, [self::EMAIL]))
        {
            throw new MessageQueueException($queue.' is not a valid queue.');
        }
    }
    
    /**
     * Declares the queue for use.
     * 
     * @param int $queue The queue that's being used.
     */
    protected function declareQueue($queue)
    {
        $this->channel->queue_declare($this->getQueueName($queue), false, true, false, false);
    }
    
    /**
     * Get the string version of the queue name.
     * 
     * @param int $queue
     * 
     * @return string
     */
    protected function getQueueName($queue)
    {
       $queueNames = [1 => 'email_queue'];
       return $queueNames[$queue];
    }
}