<?php

namespace src\formdesign\controllers;

use src\framework\controller\Controller;

class ShowFieldHelpController extends Controller
{
    function fieldhelp()
    {
        $field = $this->request->getParameter('field');

        $FormDesign = new \Forms_FormDesign($this->request->getParameters());
        $FormDesign->PopulateFormDesign(array('NonExistentFile' => 'DEFAULT'));

        $this->response->build('src/formdesign/views/ShowFieldHelp.php', array(
            'field' => $field,
            'FormDesign' => $FormDesign
        ));
    }
}