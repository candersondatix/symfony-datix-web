<?php

namespace src\formdesign\controllers;

use src\framework\controller\Controller;

class RenderFieldsController extends Controller
{
    /*
     * This is the php half of render fields used for adding a section to a page using AJAX,
     * after the page has already been loaded.
     */
    function renderfields()
    {
        global $MaxSuffixField;

        /*This is because httprequest stuff. It doesn't type nulls or bools correctly,
        they come out as strings when sent as an array from JS AJAX request*/
        foreach ($this->request->getParameters() as $key => $val)
        {
            if ($val == 'null')
            {
                $this->request->unSetParameter($key);
            }
        }

        /*If the maxSuffixField does not exist, set the global appropriately
        (used for adding new contacts to a form with the 'add another' button) */
        $MaxSuffixField = $this->request->getParameter('maxSuffixExists') == "true";

        // This is used on BasicForm files to populate $FormArray
        $ModuleDefs = $this->registry->getModuleDefs();
        $FormType = $this->request->getParameter('form_type');
        $FormMode = $FormType;

        // This is the form design used to render the section
        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => $this->request->getParameter('module'),
            'id' => $this->request->getParameter('form_id'),
            'level' => $this->request->getParameter('form_level'),
            'readonly' => $this->request->getParameter('readonly')
        ));

        $GLOBALS['formlevel'] = $this->request->getParameter('form_level');
        $SectionName = $this->request->getParameter('section_name');

        //this pattern match finds out if there are suffixes on the data
        $pattern = '/_([0-9])+$/';
        preg_match($pattern, $SectionName, $matches, PREG_OFFSET_CAPTURE);
        if(isset($matches) && !empty($matches))
        {
            $SuffixString = $matches[0][0];
            $Suffix = substr($SuffixString, 1);
        }
        $SectionNameNoSuffix = preg_replace($pattern, '', $SectionName);

        $FormDesign->LoadFormDesignIntoGlobals();

        // This is needed so the sections that uses suffixes (e.g contacts) can load the appropriate properties (hidden, etc,).
        if ($Suffix)
        {
            $FormDesign->AddSuffixToFormDesign($Suffix);
        }

        //Included variable
        $FormArray = null;
        require_once 'Source/libs/FormClasses.php';
        require_once(GetBasicFormFileName($this->request->getParameter('module'), $this->request->getParameter('form_level')));

        //set up for the form table that is used to render the section
        $FormTable = new \FormTable($FormType, $this->request->getParameter('module'), $FormDesign);
        $FormTable->setCurrentSection($SectionNameNoSuffix);
        $FormTable->SetFormArray($FormArray);
        $FormTable->addUDFFields();
        $FormTable->CheckFormArrayKeys();
        $FormTable->MoveFieldsToSections();
        if($FormType != 'Design')
        {
            $FormTable->setupInitialFormVisibility(array('module' => $this->request->getParameter('module')));
        }
        $FormTable->renderFields($SectionName, $this->request->getParameter('module'), array('recordid' => $this->request->getParameter('recordid')));

        echo $FormTable->Contents;
    }
}