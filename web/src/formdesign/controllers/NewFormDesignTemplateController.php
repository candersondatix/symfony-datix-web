<?php

namespace src\formdesign\controllers;

use src\framework\controller\TemplateController;

class NewFormDesignTemplateController extends TemplateController
{
    function newformdesign()
    {
        global $scripturl;

        $ModuleDefs = $this->registry->getModuleDefs();

        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));

        // Clear sections expanded session variable
        if ($this->request->getParameter('sectionsexpanded') === null)
        {
            unset($_SESSION['SECTIONEXPANDED']);
        }

        switch ($this->request->getParameter('formlevel'))
        {
            case '1':
            case '2':
                $formlevel = $this->request->getParameter('formlevel');
                break;
        }

        if (!$module)
        {
            $module = 'INC';
        }

        if ($this->request->getParameter('formaction') == 'cancel')
        {
            $this->redirect($scripturl . '?action=listformdesigns&module=' . $module);
        }

        $this->title = 'Create a new ' . $ModuleDefs[$module]['REC_NAME'] . ($Code ? ' (' . $Code . ')' : '') . ' form';
        $this->module = 'ADM';
        $this->hasPadding = false;

        $FormsFileName = GetFormsFilename($module);

        if (file_exists($FormsFileName))
        {
            require_once $FormsFileName;
        }
        else
        {
            $FormFiles = '';
            $FormFiles2 = '';
        }

        if (!is_array($FormFiles))
        {
            $Code = GetFormCode(array('module' => $module, 'level' => 1));
            $FormFiles = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Code ? ' ('.$Code.')' : '')." form");
        }

        if (!is_array($FormFiles2))
        {
            $Code = GetFormCode(array('module' => $module, 'level' => 2));
            $FormFiles2 = array(0 => "Default {$ModuleDefs[$module][REC_NAME]}".($Code ? ' ('.$Code.')' : '')." form");
        }

        $values = array();

        if($formlevel == 2)
        {
            foreach ($FormFiles2 as $Number => $Name){

                $values[$Number] = $Name;
                /*?><option value="<?php echo $Number; ?>"><?php echo $Name; ?></option><?php*/
            }
        }
        else
        {
            foreach ($FormFiles as $Number => $Name)
            {
                $values[$Number] = $Name;
                /*><option value="<?php echo $Number; ?>"><?php echo $Name; ?></option><?php*/
            }
        }

        $fields = array(
            array(
                'Name'                => 'copy_from',
                'Title'               => 'Copy design from which form?',
                'Type'                => 'ff_select',
                'CustomCodes'         => $values,
                'SuppressCodeDisplay' => true,
                'OnChangeExtra'       => ''
            )
        );

        $data['copy_from'] = array_shift(array_keys($values));

        foreach($fields as $key => $field)
        {
            $fieldObj = new \FormField();
            $make_field = new \FormTable('', $module);
            $fieldContent[strtolower($field['Name'])] = $make_field->call_makeField($field['Type'], $field, $field['Name'], $data, 'new', '', $field['Name'], '', $fieldObj, '', '', $module, '');

            if (in_array($field['Name'], [ 'rows_field', 'columns_field', 'traffic_field' ]))
            {
                $fieldContent[strtolower($field['Name'])]->setDescription($this->getReportFieldDescription($field['Name'], $data, $module));
            }
        }

        $this->response->build('src/formdesign/views/NewFormDesign.php', array(
            'formlevel' => $formlevel,
            'FormFiles2' => $FormFiles2,
            'FormFiles' => $FormFiles,
            'fieldContents' => $fieldContent,
            'module' => $module
        ));
    }
}