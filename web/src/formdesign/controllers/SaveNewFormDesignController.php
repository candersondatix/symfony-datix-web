<?php

namespace src\formdesign\controllers;

use src\framework\controller\Controller;

class SaveNewFormDesignController extends Controller
{
    function savenewformdesign()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $module = FirstNonNull(array($this->request->getParameter('module'), 'INC'));
        $formlevel = FirstNonNull(array($this->request->getParameter('formlevel'), 1));

        if ($this->request->getParameter('formaction') == 'cancel')
        {
            $Parameters = array(
                'module' => $module
            );
            $this->call('src\admin\controllers\ListFormDesignsTemplateController', 'listformdesigns', $Parameters);
            return;
        }

        $CopyFrom = $this->request->getParameter('copy_from');

        $FormDesign = new \Forms_FormDesign(array(
            'module' => $module,
            'level' => $formlevel,
            'id' => $CopyFrom
        ));

        $NewFormDesign = $FormDesign->CopyToNew();

        \Forms_FormDesign::ShowFormDesign($module, $NewFormDesign->GetID(), $formlevel);

        obExit();
    }
}