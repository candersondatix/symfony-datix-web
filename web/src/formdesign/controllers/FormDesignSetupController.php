<?php

namespace src\formdesign\controllers;

use src\framework\controller\Controller;

class FormDesignSetupController extends Controller
{
    function formdesignsetup()
    {
        $FormID = \Sanitize::SanitizeInt($this->request->getParameter('form_id'));
        $Module = \Sanitize::getModule($this->request->getParameter('module'));
        $FormLevel = \Sanitize::SanitizeInt($this->request->getParameter('formlevel'));

        // Clear sections expanded session variable
        if ($this->request->getParameter('sectionsexpanded') === null)
        {
            unset($_SESSION['SECTIONEXPANDED']);
        }

        // Set formlevel to 1 if it isn't set.  This is so we can use it
        // to get the basic form filename
        if ($FormLevel == '' || $FormLevel == 0)
        {
            $FormLevel = 1;
        }

        if (!$Module)
        {
            $this->redirect('app.php');
        }

        \Forms_FormDesign::ShowFormDesign($Module, $FormID, $FormLevel);

        obExit();
    }

    /**
     * @desc Called when "save" is clicked on the form design screen. Processes and saves the form design file.
     */
    function saveDesignSettings()
    {
        $FormID    = htmlspecialchars($this->request->getParameter('form_id'));
        $formlevel = ($this->request->getParameter('formlevel') ? $this->request->getParameter('formlevel') : 1);
        $module    = htmlspecialchars($this->request->getParameter('module'));

        if ($module == 'DIF1')
        {
            $module = 'INC';
        }

        // Create empty form design object
        $FormDesign = new \Forms_FormDesign(array('module' => $module, 'level' => $formlevel, 'id' => $FormID));

        // Reset to default/delete
        if ($this->request->getParameter('rbWhat') == 'ClearDesign')
        {
            $FormDesign->ClearDesign();

            if ($FormDesign->FileExists() || $FormID == 0)
            {
                AddSessionMessage('INFO', 'The form design has been updated successfully.');
                $this->redirect('app.php'.$FormDesign->getDesignLink());
            }
            else
            {
                $this->redirect('app.php?action=listformdesigns&module='.$module);
            }
        }

        // Cancel
        if ($this->request->getParameter('rbWhat') == 'CancelDesign')
        {
            $this->redirect('app.php?action=listformdesigns&module='.$module);
        }

        // Populate FormDesign object with the contents of the existing form design file
        $FormDesign->PopulateFormDesign(array('NonExistentFile' => 'DEFAULT'));

        // Replace FormDesign properties with posted variables
        $FormDesign->PopulateFromPOST();

        // Set as default if required
        if ($this->request->getParameter('use_design') != '')
        {
            if ($this->request->getParameter('use_design') == 'N' && GetParm($FormDesign->GetGlobalName(), 0, true) == $FormDesign->GetID())
            {
                SetGlobal($FormDesign->GetGlobalName(), 0);
            }
            elseif ($this->request->getParameter('use_design') == 'Y')
            {
                SetGlobal($FormDesign->GetGlobalName(), $FormDesign->GetID());
            }
        }

        $FormDesign->SaveToFile();

        GetParms($_SESSION['user']);

        if ($this->request->getParameter('expandall') === null)
        {
            AddSessionMessage('INFO', _tk('form_design_save_confirm'));
        }

        $url = $FormDesign->getDesignLink();

        if (isset($_SESSION['SECTIONEXPANDED']))
        {
            $url.= '&sectionsexpanded=1';
        }

        $this->redirect('app.php'.$url);
    }
}