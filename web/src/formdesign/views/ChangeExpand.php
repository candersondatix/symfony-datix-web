<script language="JavaScript" type="text/javascript">
    function setReturns()
    {
        var sectionListBox = document.getElementById("<?php echo ($this->TargetType == 'section' ? 'sections' : 'fields'); ?>");
        var multiListBox = document.getElementById("valuesselect");
        var alerttext = document.getElementById("alerttext").value;
        var valuesReturn = "";

        if ((sectionListBox.value == "" && alerttext == "") || multiListBox.length == 0)
        {
            // This is needed because IE6 doesn't disable the FloatingWindow div when the alert is presented.
            GetFloatingDiv("<?php echo ($this->TargetType == 'section' ? 'expandsection' : 'expandfield'); ?>").CloseFloatingControl();
            alert("You must choose a value");
            return false;
        }

        for (var i = 0; i < multiListBox.length; i++)
        {
            valuesReturn = valuesReturn + " " + multiListBox.options[i].value;
        }

        <?php if ($this->TargetType == 'section') : ?>
        if (sectionListBox.value || sectionListBox.selectedIndex)
        {
            $("EXPANDSECTION-<?php echo Escape::EscapeEntities($this->sectionindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = sectionListBox.value || sectionListBox.options[sectionListBox.selectedIndex].value;
        }
        else
        {
            $("EXPANDSECTION-<?php echo Escape::EscapeEntities($this->sectionindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = "";
        }

        $("EXPANDVALUES-<?php echo Escape::EscapeEntities($this->sectionindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = valuesReturn;
        $("EXPANDALERTTEXT-<?php echo Escape::EscapeEntities($this->sectionindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = alerttext;
        document.forms[0].btnSaveDesign.click();
        return true;
        <?php else : ?>
        if (sectionListBox.value || sectionListBox.selectedIndex)
        {
            $("EXPANDFIELD-<?php echo Escape::EscapeEntities($this->fieldindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = sectionListBox.value || sectionListBox.options[sectionListBox.selectedIndex].value;
        }
        else
        {
            $("EXPANDFIELD-<?php echo Escape::EscapeEntities($this->fieldindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = "";
        }

        $("EXPANDFIELDVALUES-<?php echo Escape::EscapeEntities($this->fieldindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = valuesReturn;
        $("EXPANDFIELDALERTTEXT-<?php echo Escape::EscapeEntities($this->fieldindex); ?>-<?php echo Escape::EscapeEntities($this->inputfield); ?>").value = alerttext;
        submitClicked = true;
        document.forms[0].btnSaveDesign.click();
        return true;
        <?php endif; ?>
    }

    // Fields share IDs, so need to clear their data whenever a popup window is created to avoid conflicts (e.g. with custom codes)
    jQuery("#sections_title").removeData();
    jQuery("#fields_title").removeData();
    jQuery("#valuesselect_title").removeData();
</script>
<table width="600px">
    <tr>
        <td>
            <table class="bordercolor" cellspacing="1" cellpadding="4" width="100%" align="center" border="0">
                <tr>
                    <td class="windowbg2">
                        <b>
                            <?php echo ($this->TargetType == 'section' ? 'Section to link to this field' : 'Field to link to this field'); ?>
                        </b>
                    </td>
                    <td class="windowbg2">
                        <?php echo $this->targetDropdown->getField(); ?>
                    </td>
                </tr>
                <tr>
                    <td class="windowbg2">
                        <b>
                            <?php echo ($this->TargetType == 'section' ? 'Values that will cause this section to be shown' : 'Values that will cause the field to be shown'); ?>
                        </b>
                    </td>
                    <td class="windowbg2">
                        <?php echo $this->valuesSelect->getField(); ?>
                    </td>
                </tr>
                <tr>
                    <td class="windowbg2">
                        <b>Message to display when one of these values is selected</b>
                    </td>
                    <td class="windowbg2">
                        <input type="text" size="50" name="alerttext" id="alerttext" value="<?php echo Sanitize::SanitizeRaw($this->alerttext); ?>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>