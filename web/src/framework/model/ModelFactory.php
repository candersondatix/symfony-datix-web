<?php
namespace src\framework\model;

use src\framework\query\QueryFactory;

/**
 * Abstract Factory for retrieving object creation/persistence tools.
 * @codeCoverageIgnore
 */
abstract class ModelFactory
{
    /**
     * Creates and returns an EntityFactory object.
     * 
     * @return EntityFactory
     */
    public abstract function getEntityFactory();
    
    /**
     * Creates and returns a Mapper object.
     * 
     * @return Mapper
     */
    public abstract function getMapper();
    
    /**
     * Creates and returns an EntityCollection object.
     *
     * @return EntityCollection
     */
    public abstract function getCollection();
    
    /**
     * Creates and returns a QueryFactory object.
     * 
     * @return QueryFactory
     */
    public function getQueryFactory()
    {
        return new QueryFactory();
    }
}