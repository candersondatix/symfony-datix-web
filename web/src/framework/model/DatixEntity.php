<?php
namespace src\framework\model;

use src\framework\model\Entity;

/**
 * Extends the base Entity class by adding properties common to Datix entities.
 */
abstract class DatixEntity extends RecordEntity
{
    /**
     * Acts as a marker to ensure that other people's changes are not overwritten.
     * 
     * @var string
     */
    protected $updateid;
    
    /**
     * The initials of the last user to update the entity.
     * 
     * @var string
     */
    protected $updatedby;
    
    /**
     * The date the entity was last updated.
     * 
     * @var string
     */
    protected $updateddate;
    
    /**
     * {@inheritdoc}
     */
    public function __clone()
    {
        parent::__clone();
        $this->updateid    = null;
        $this->updatedby   = null;
        $this->updateddate = null;
    }

    /**
     * Generates a new two-character updateid based on the existing one (if any) for this record.
     *
     * NB - copy of GensUpdateID() in Subs.php
     *
     * @return string The new updateid.
     */
    public function incrementUpdateId()
    {
        if ($this->updateid == '')
        {
            $char1 = 0;
            $char2 = 0;
        }
        else
        {
            $char1 = ord($this->updateid{0});
            $char2 = ord($this->updateid{1});
        }

        $char2 = $this->incrementChar($char2);

        if ($char2 == 48)
        {
            $char1 = $this->incrementChar($char1);
        }

        if ($char1 == 0)
        {
            $this->updateid = chr($char2);
        }
        else
        {
            $this->updateid = chr($char1) . chr($char2);
        }
    }

    /**
     * Increments an ASCII value within a defined range, used when generating updateids.
     *
     * NB - copy of IncUpdateIDChar() in Subs.php
     *
     * @param int $val The ASCII value of the character we're incrementing.
     *
     * @return int $val
     */
    public function incrementChar($val)
    {
        $val++;
        if ($val < 48)
        {
            $val = 49;
        }
        elseif ($val > 57 && $val < 65)
        {
            $val = 65;
        }
        elseif ($val > 90 && $val < 97)
        {
            $val = 97;
        }
        elseif ($val > 122)
        {
            $val = 48;
        }
        return $val;
    }
}