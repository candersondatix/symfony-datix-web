<?php
namespace src\framework\model;

use src\framework\events\Subject;
use src\framework\events\Observer;
use src\framework\events\EventQueue;
use src\framework\model\exceptions\UndefinedPropertyException;
use src\framework\model\exceptions\ModelException;

/**
 * Base class for domain objects.
 */
abstract class Entity implements Subject
{
    // Event constants used when notifying observers
    const EVENT_INSERT = 1;
    const EVENT_UPDATE = 2;
    const EVENT_DELETE = 3;

    /**
     * The observer store.
     * 
     * @var array
     */
    private $observers;
    
    /**
     * Whether or not to defer the execution of observers.
     * 
     * @var Boolean
     */
    protected $queueEvents;
    
    /**
     * Whether or not to cache the instance per-request.
     * 
     * @var boolean
     */
    protected $addToCache;
    
    public function __construct()
    {
        $this->observers   = array();
        $this->queueEvents = false;
        $this->addToCache  = true;
    }

    /**
     * {@inherit}
     */
    public function __get($name)
    {
        $this->validateProperty($name);
        
        // allow getters to be explicitly defined per-property if necessary
        if (method_exists($this, 'get'.$name))
        {
            $getter = 'get'.$name;
            return $this->$getter();
        }
        
        return $this->$name;
    }
    
    /**
     * {@inherit}
     */
    public function __set($name, $value)
    {
        $this->validateProperty($name);
        
        // allow setters to be explicitly defined per-property if necessary
        if (method_exists($this, 'set'.$name))
        {
            $setter = 'set'.$name;
            $this->$setter($value);
        }
        else
        {
            $this->$name = $value;
        }
    }
    
    /**
     * {@inherit}
     */
    public function __isset($name)
    {
        $this->validateProperty($name);
        return $this->$name != '';
    }
    
    /**
     * Checks that the property being accessed is valid.
     * 
     * @param string $name The property name.
     * 
     * @throws UndefinedPropertyException    If the property hasn't been explicitly defined.
     */
    private function validateProperty($name)
    {
        if (!property_exists($this, $name))
        {
            throw new UndefinedPropertyException('The property "'.$name.'" is not defined for this entity.');
        }
    }
    
    /**
     * Attaches an observer.
     *
     * @param Observer $observer The observer to attach.
     */
    public function attach(Observer $observer)
    {
        $this->observers[spl_object_hash($observer)] = $observer;
    }

    /**
     * Detaches an observer.
     *
     * @param Observer $observer The observer to detach.
     */
    public function detach(Observer $observer)
    {
        unset($this->observers[spl_object_hash($observer)]);
    }

    /**
     * Notify all observers.
     * 
     * @param int $event The event type.
     */
    public function notify($event)
    {
        foreach($this->observers as $observer)
        {
            if ($this->getQueueEvents())
            {
                EventQueue::getInstance()->add($observer, $this, $event);
            }
            else
            {
                $observer->update($this, $event);
            }
        }
    }
    
    /**
     * Setter for queueEvents.
     * 
     * @param Boolean $bool
     */
    public function setQueueEvents($bool)
    {
        $this->queueEvents = (bool) $bool;
        
        // automatically set queue events for all collection properties
        foreach (array_keys(get_object_vars($this)) as $property)
        {
            if ($this->$property instanceof EntityCollection)
            {
                $this->$property->setQueueEvents((bool) $bool);
            }
        }
    }

    /**
     * Getter for queueEvents boolean
     *
     * @return boolean
     */
    public function getQueueEvents()
    {
        return $this->queueEvents;
    }
    
    /**
     * {@inherit}
     */
    public function __clone()
    {
        // automatically clone all collection properties
        foreach (array_keys(get_object_vars($this)) as $property)
        {
            if ($this->$property instanceof EntityCollection)
            {
                $this->$property = clone $this->$property;
            }
        }
    }
    
    /**
     * Provides access to entity properties.
     * 
     * @return array
     */
    public function getVars()
    {
        return get_object_vars($this);
    }
    
    /**
     * Whether or not this entity can be added to/retrieved from the cache.
     * 
     * @return boolean
     */
    public function isCacheable()
    {
        return $this->addToCache;
    }
    
    /**
     * Exposes the properties defined for this entity.
     * 
     * @return array
     */
    public static function getProperties()
    {
        return array_keys(get_class_vars(get_called_class()));
    }
    
    /**
     * Returns the identity key for this instance, or null if the not all identity properties are set.
     * 
     * @return string|null
     */
    public function getIdentityKey()
    {
        $identity = $this->getIdentity();
        $ids = array();
        foreach ($identity as $id)
        {
            if ($this->$id == '')
            {
                // not all identity properties are set
                return;
            }
            $ids[] = $this->$id;
        }
        $id = implode('.', $ids);
        return get_class($this).'.'.$id;
    }
    
    /**
     * Returns an array of properties that are used to uniquely identify the entity.
     * 
     * Provides a public interface to the value defined in Entity::setIdentity().
     * 
     * @throws ModelException If the identity has not been defined as an array.
     * 
     * @return array
     */
    public static function getIdentity()
    {
        $identity = static::setIdentity();
        if (!is_array($identity))
        {
            throw new ModelException('Entity::setIdentity() must return an array.');
        }
        return $identity;
    }
    
    /**
     * Used to define and return the identity array for the entity.
     * 
     * The identity array is a list of the properties that combine to uniquely identify the object.
     * 
     * @throws ModelException If the concrete Entity class hasn't overridden this method.
     */
    protected static function setIdentity()
    {
        throw new ModelException('Entity::setIdentity() must be overridden in concrete entity classes.');
    }
}