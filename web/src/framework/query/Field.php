<?php
namespace src\framework\query;

use src\framework\query\exceptions\InvalidFieldException;
use src\framework\query\exceptions\QueryException;
use src\framework\registry\Registry;
use src\system\database\FieldInterface;

/**
 * A field and associated comparisons as part of a query.
 */
class Field
{
    /**
     * The field name(s).
     *
     * @var array
     */
    protected $names;
    
    /**
     * The statements that wraps the field name(s) (alias, SQL function, etc).
     * 
     * @var string
     */
    protected $wrapper;
    
    /**
     * The comparisons (operator and value) on this field.
     *
     * @var array
     */
    protected $comps;
    
    /**
     * Keeps track of original field names once they are converted (e.g. to aliases).
     * 
     * Useful so we can still access the actual field name to inspect FieldDefs, etc.
     * 
     * @var array
     */
    protected $originalNames;
    
    /**
     * Constructor.
     * 
     * @param string|array $name Either a single field name, or an array where the first element is a "wrapper"
     *                           (alias, SQL function etc) and the second element is the field name(s).  The wrapper contains
     *                           one or more placeholders (? characters) which are replaced in turn by the field names.
     *                           When passing an array, the second element can either be a string if only a single field name is being wrapped,
     *                           or an array if multiple field names are being wrapped.
     * 
     * @throws InvalidFieldException If the field name(s) is invalid.
     */
    public function __construct($name)
    {
        if (is_array($name))
        {
            $this->wrapper = $name[0];
            if (is_array($name[1]))
            {
                $this->names = $name[1];
            }
            else
            {
                $this->names[] = $name[1];
            }
        }
        else
        {
            $this->wrapper = '?';
            $this->names[] = $name;
        }
        
        foreach ($this->names as $name)
        {
            if (!$this->validateField($name))
            {
                throw new InvalidFieldException('Invalid field name');
            }   
        }
        
        $this->comps = array();
    }
    
    /**
     * Add a comparison to the field.
     *
     * @param int                $operator The comparison operator.
     * @param string|array|Query $value    The value we're comparing against.
     * @param boolean            $isField  Whether or not the value is another field.
     * 
     * @throws InvalidFieldException If the value is a field and the field name is not valid.
     */
    public function addComp($operator, $value, $isField = false)
    {
        if ($isField && !$this->validateField($value))
        {
            throw new InvalidFieldException('Invalid field name');
        }
        
        if (!is_array($value) && !($value instanceof Query))
        {
            // for consistency, value is always stored as an array
            $value = [$value];
        }
        
        $this->comps[] = array(
            'operator' => $operator,
            'value'    => $value,
            'isField'  => (bool) $isField
        );
    }
    
    /**
     * Getter for comps.
     *
     * @return array
     */
    public function getComps()
    {
        return $this->comps;
    }
    
    /**
     * Getter for name.
     * 
     * Returns the name(s) replaced within the wrapper (if any).
     *
     * @return string
     */
    public function getName()
    {
        foreach ($this->names as $name)
        {
            $output = $this->evaluateWrap( $name );
        }        
        
        return isset( $output ) ? $output : $this->wrapper;
    }
    
    /**
     * Returns the raw field names(s), without their wrapper.
     * 
     * @return array
     */
    public function getRawNames()
    {
        return $this->names;
    }
    
    /**
     * Returns the raw field names(s), without their wrapper, in their original format 
     * (e.g. before they've been converted into aliases, etc).
     * 
     * @return array
     */
    public function getOriginalNames()
    {
        foreach ($this->names as $key => $value)
        {
            $this->names[$key] = $this->originalNames[$value] ?: $value;
        }

        return $this->names;
    }

    /**
     * Getter for wrapper.
     * 
     * @return string
     */
    public function getWrapper()
    {
        return $this->wrapper;
    }
    
    /**
     * Reolve the wrapper with a value
     * 
     * @author gszucs
     * @param string $value
     * @return string
     */
    public function evaluateWrap( $value )
    {
    	return implode($value, explode('?', $this->wrapper));
    }
    
    /**
     * Used to convert the field name.
     * 
     * Useful when mapping entity properties or dealing with aliases.
     * 
     * @param string $current The current field name.
     * @param string $new     The new field name.
     */
    public function convertFieldName($current, $new)
    {
        if (in_array($current, $this->names))
        {
            $this->names = array_replace($this->names,
                array_fill_keys(
                    array_keys($this->names, $current),
                    $new
                )
            );
            
            $this->originalNames[$new] = $current;
        }
    }
    
    /**
     * Whether or not this field is ready to be used in a query (i.e. it has comparisons defined).
     *
     * @return boolean
     */
    public function isComplete()
    {
        return !empty($this->comps);
    }
    
    /**
     * Returns the FieldDef object associated with this field.
     * 
     * @return FieldDef|null
     */
    public function getFieldDef(Registry $registry = null)
    {
        if (count($this->names) != 1)
        {
            // we can't return a FieldDef object because we don't know what name to use
            return null;
        }
        
        $registry = $registry ?: Registry::getInstance();
        $fieldDefs = $registry->getFieldDefs();
        
        // return the FieldDef associated with this field name, or original field name in tha case of converted fields.
        return $fieldDefs[$this->names[0]] ?: $fieldDefs[$this->originalNames[$this->names[0]]] ?: null;
    }
    
    /**
     * Replace @prompt codes in each field with their literal values.
     * 
     * @throws QueryException If the @prompt codes can't be replaced.
     */
    public function replaceAtPrompts()
    {
        foreach ($this->comps as $key => $comp)
        {
            $hasAtPrompt = false;
            foreach ($comp['value'] as $value)
            {
                if (strtoupper($value) == '@PROMPT')
                {
                    $hasAtPrompt = true;
                    break;
                }
            }
            
            if (!$hasAtPrompt)
            {
                continue;
            }
            
            if (count($comp['value']) != 1)
            {
                // there should only be one comparison value when using @prompt
                throw new QueryException('Unable to replace @prompt codes: multiple comparison values found');
            }
            
            if (null === ($fieldDef = $this->getFieldDef()))
            {
                throw new QueryException('Unable to replace @prompt codes: FieldDef object does not exist');
            }
            
            $fieldName = $this->originalNames[$this->names[0]] ?: $this->names[0];
            
            switch ($fieldDef->getType())
            {
                case FieldInterface::DATE:
    
                    if ($_SESSION['NEW_PROMPT_VALUES'][$fieldName]['start'] != null)
                    {
                        if ($_SESSION['NEW_PROMPT_VALUES'][$fieldName]['end'] != null)
                        {
                            $this->comps[$key]['operator'] = FieldCollection::BETWEEN;
                            $this->comps[$key]['value'] = array(
                                UserDateToSQLDate($_SESSION['NEW_PROMPT_VALUES'][$fieldName]['start']),
                                UserDateToSQLDate($_SESSION['NEW_PROMPT_VALUES'][$fieldName]['end'])
                            );
                        }
                        else
                        {
                            $this->comps[$key]['operator'] = FieldCollection::GT_EQ;
                            $this->comps[$key]['value'] = [UserDateToSQLDate($_SESSION['NEW_PROMPT_VALUES'][$fieldName]['start'])];
                        }
                    }
                    else
                    {
                        if ($_SESSION['NEW_PROMPT_VALUES'][$fieldName]['end'] != null)
                        {
                            $this->comps[$key]['operator'] = FieldCollection::LT_EQ;
                            $this->comps[$key]['value'] = [UserDateToSQLDate($_SESSION['NEW_PROMPT_VALUES'][$fieldName]['end'])];
                        }
                        else
                        {
                            $this->comps[$key]['operator'] = FieldCollection::ALL;
                            $this->comps[$key]['value'] = [null];
                        }
                    }
                    break;
                
                default:
                    
                    if ($_SESSION['NEW_PROMPT_VALUES'][$fieldName] == '')
                    {
                        $this->comps[$key]['operator'] = FieldCollection::ALL;
                        $this->comps[$key]['value'] = [null];
                    }
                    else
                    {
                        $this->comps[$key]['operator'] = FieldCollection::IN;
                        $this->comps[$key]['value'] = explode('|', $_SESSION['NEW_PROMPT_VALUES'][$fieldName]);
                    }
                    
                    break;
            }
        }
    }
    
    /**
     * Used to sanitize field names.
     * 
     * @param string $name The field name.
     * 
     * @return boolean
     */
    protected function validateField($name)
    {
        // we have a problem here in that some very important fields aren't in field_directory at the moment (e.g recordid), so for now...
        return true;
        
        // TODO remove the above!
        
        // check for UDF format
        if (preg_match('/^UDF_\d+S/u', $name))
        {
            return true;
        }
        
        // check for fully-qualified field name
        if (\UnicodeString::strpos($name, '.') === false)
        {
            return false;
        }
        
        // extract table/field names
        list($table, $field) = explode('.', $name);
        
        // check for standard field names
        // TODO can probably use the registry for this instead...
        try
        {
            $FDR_info = new \Fields_Field($field, $table);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
        
        // TODO validate the table name also
    }
}