<?php

namespace src\framework\query;

use src\framework\model\EntityFactory;
use src\framework\registry\Registry;
use src\reports\exceptions\ReportException;
use src\savedqueries\model\SavedQueryModelFactory;
use src\system\database\CodeFieldInterface;
use src\framework\query\exceptions\QueryException;
use src\system\database\extrafield\DateExtraField;
use src\system\database\extrafield\MoneyExtraField;
use src\system\database\extrafield\MultiCodeExtraField;
use src\system\database\extrafield\NumberExtraField;
use src\system\database\extrafield\StringExtraField;
use src\system\database\extrafield\TextExtraField;
use src\system\database\field\DateField;
use src\system\database\field\MoneyField;
use src\system\database\field\MultiCodeField;
use src\system\database\field\NumberField;
use src\system\database\field\StringField;
use src\system\database\field\TextField;
use src\system\database\field\YesNoField;
use src\system\database\FieldInterface;

use src\framework\validate\Date as DateValidator;

class WhereFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\framework\\query\\Where';
    }
    
    /**
     * {@inheritdoc}
     */
    public function createObject(array $data, Entity $object = null, Registry $registry = null)
    {
        $registry  = $registry ?: Registry::getInstance();
        $fieldDefs = $registry->getFieldDefs();
        $ModuleDefs = $registry->getModuleDefs();
        $qf = new QueryFactory();
        
        if ($object === null)
        {
            $object = new Where();
        }
        
        foreach ($data as $field => $value)
        {
            // Map subjects fields for complaints because they are not real fields.
            // Otherwise field definition will be null and we can't use these fields for reporting.
            if ($data['module'] == 'COM' && preg_match('/^csu_/ui', $field))
            {
                $fieldReplaced = $field;
                $field = $ModuleDefs[$data['module']]['PRIMARY_SUBJECT_MAPPINGS'][$field];
            }

            if (\UnicodeString::stripos($field, '.') === false)
            {
                $field = $this->getTable($data, $registry) . '.' . $field;

                if ($fieldReplaced)
                {
                    $fieldReplaced = $this->getTable($data, $registry) . '.' . $fieldReplaced;
                }
            }
            $fieldDef = $fieldDefs[$field];
            if($fieldDef === null)
            {
                continue;
            }
            if($value != '')
            {
                //True if the value provided uses an 'OR' boolean operator
                $hasOrOperator = \UnicodeString::strpos($value, '|');
                //True if the value provided uses an 'AND' boolean operator
                $hasAndOperator = \UnicodeString::strpos($value, '&');
                //True if the field is one of the pre-approved field types for use with an 'AND' boolean operator
                //(And yes this is pretty elongated, but hopefully straightforward)
                $fieldIsCompatibleWithAndOperator = $fieldDef->getType() == FieldInterface::NUMBER
                                                ||  $fieldDef->getType() == FieldInterface::STRING
                                                ||  $fieldDef->getType() == FieldInterface::DATE
                                                ||  $fieldDef->getType() == FieldInterface::MULTICODE
                                                ||  $fieldDef->getType() == FieldInterface::CODE
                                                ||  $fieldDef->getType() == FieldInterface::TEXT
                                                ||  $fieldDef->getType() == FieldInterface::MONEY;
                //if |'s are used, we need to interpret each subclause recursively, as long as it's not a simple ORing of basic values.
                if ($hasOrOperator && preg_match('/^[A-Za-z0-9]*(\|[A-Za-z0-9]*)*$/', $value) != 1)
                {
                    $splitValue = explode('|', $value);

                    $subWhereObj = new Where();

                    $conditions = ['OR'];
                    foreach($splitValue as $subValue)
                    {
                        $conditions[] = self::createObject(array($field => $subValue, 'action' => $data['action']));
                    }

                    $subWhereObj->addArray($conditions);
                    $object->add($subWhereObj);
                }
                else if ($hasAndOperator && $fieldIsCompatibleWithAndOperator)
                {
                    $splitValue = explode('&', $value);

                    $subWhereObj = new Where();

                    $conditions = ['AND'];
                    foreach($splitValue as $subValue)
                    {
                        $conditions[] = self::createObject(array($field => $subValue, 'action' => $data['action']));
                    }

                    $subWhereObj->addArray($conditions);
                    $object->add($subWhereObj);
                }
                //no complex |'s, so we can interpret this as a single statement.
                else
                {
                    $fields = new FieldCollection();

                    //Hack to deal with searching on complaint subjects.
                    if (preg_match('/^compl_main.csu_/ui', $fieldReplaced))
                    {
                        $subSelectWhere = $this->createObject(array(str_replace('compl_main.csu_', 'compl_subjects.com_', $fieldReplaced) => $value, 'action' => $data['action']));

                        $subSelectQuery = new Query();
                        $subSelectQuery->select(array('compl_subjects.com_id'))
                            ->where($subSelectWhere);

                        $fields->field('compl_main.recordid')->in($subSelectQuery);
                        $object->add($fields);
                    }
                    //Hack to deal with searching on incident causal factors.
                    else if (preg_match('/^incidents_main.caf_/ui', $field))
                    {
                        $subSelectWhere = $this->createObject(array(str_replace('incidents_main.caf_', 'causal_factors.caf_', $field) => $value, 'action' => $data['action']));

                        $subSelectQuery = $qf->getQuery();
                        $subSelectQuery->select(array('causal_factors.inc_id'))
                            ->where($subSelectWhere);

                        $fields->field('incidents_main.recordid')->in($subSelectQuery);
                        $object->add($fields);
                    }
                    //Hack to deal with searching on code tag groups
                    else if (preg_match('/\.searchtaggroup_/ui', $field))
                    {
                        $values = explode('|', $value);

                        $group = explode('_', explode('.', $field)[1])[1];
                        $table = explode('.', $field)[0];
                        $fieldName = \UnicodeString::str_ireplace('searchtaggroup_'.$group.'_', '', explode('.', $field)[1]);

                        $subSelectWhere = $qf->getWhere();
                        $subSelectWhere->add(
                            $qf->getFieldCollection()
                                ->field('code_tag_links.field')->eq($fieldName)
                                ->field('code_tag_links.[group]')->eq($group)
                                ->field('code_tag_links.tag')->in($values)
                        );

                        $subSelectQuery = $qf->getQuery();
                        $subSelectQuery->select(array('code_tag_links.code'))
                            ->where($subSelectWhere);

                        //Not convinced about using ModuleDefs here: is there a better way of getting the fully qualified field name?
                        $fields->field($table.'.'.$fieldName)->in($subSelectQuery);
                        $object->add($fields);
                    }
                    else
                    {
                        //Hack to deal with incrootcause/clarootcause
                        if (preg_match('/^incidents_main.incrootcause_/ui', $field))
                        {
                            $field = 'incidents_main.inc_root_causes';
                        }
                        if (preg_match('/^claims_main.clarootcause_/ui', $field))
                        {
                            $field = 'claims_main.cla_root_causes';
                        }

                        if (isset($fieldDef) || preg_match('/^claims_main.pay_type_/', $field))
                        {
                            //Hack to deal with searching on claims payment types.
                            if (preg_match('/^claims_main.pay_type_/ui', $field)) // Hack to allow searching on payment summary
                            {
                                $paymentType = explode('_', explode('.',$field)[1])[2];
                                $fields->field('dbo.fn_GetPaymentTypeTotal(claims_main.recordid,\''.$paymentType.'\')', array('claims_main.recordid'));
                                $fieldDef = $fieldDefs['payments.pay_amount'];
                            }
                            else
                            {
                                $fields->field($field);
                            }

                            //check for special search terms
                            if ($value == '=' || \UnicodeString::strtolower($value) == 'is null')
                            {
                                if($fieldDef->isNumeric())
                                {
                                    $fields->isNull();
                                    $object->add($fields);
                                }
                                else
                                {
                                    $object->add(
                                        $object->nest('OR',
                                            $qf->getFieldCollection()
                                                ->field($field)->isNull(),
                                            $qf->getFieldCollection()
                                                ->field($field)->isEmpty())
                                    );
                                }
                            }
                            else if ($value == '==' || \UnicodeString::strtolower($value) == 'is not null')
                            {
                                if($fieldDef->isNumeric())
                                {
                                    $fields->NotNull();
                                }
                                else
                                {
                                    $fields->NotNull()->notEmpty();
                                }

                                $object->add($fields);
                            }
                            //multicode fields with & characters
                            else if ($fieldDef instanceof MultiCodeField && \UnicodeString::strpos($value, '&'))
                            {
                                foreach (explode('&', $value) as $andCode)
                                {
                                    $fields = new FieldCollection();
                                    $fields->field($field);
                                    $fields->like($andCode);
                                    $object->add($fields);
                                }
                            }
                            // searches of the form <date1>:<date2> etc.
                            else if (($fieldDef->getType() == FieldInterface::DATE || $fieldDef->getType() == FieldInterface::NUMBER || $fieldDef->getType() == FieldInterface::MONEY) && preg_match('/.*:.*/', $value) == 1)
                            {
                                $valueArray = explode(':', $value);

                                if ($fieldDef->getType() == FieldInterface::DATE && \UnicodeString::strpos($value, '@') === false)
                                {
                                    $error = false;
                                    
                                    $validator = new DateValidator();
                                    if(GetParm("FMT_DATE_WEB") == "US") 
                                    {
                                        $validator->setFormat('m/d/Y');
                                    }
                                    else
                                    {
                                        $validator->setFormat('d/m/Y');
                                    }
                                    
                                    array_walk($valueArray, function($value) use ($validator, &$error) {
                                        if(false === $validator->isValid($value)) 
                                        {
                                            $error = true;
                                        }
                                    });

                                    if(false === $error) 
                                    {
                                        //format properly, but only if it is not an @TODAY search.
                                        $valueArray = array_map(UserDateToSQLDate, $valueArray);
                                    }
                                }
                                else if ($fieldDef->getType() == FieldInterface::NUMBER || $fieldDef->getType() == FieldInterface::MONEY)
                                {
                                    $valueArray[0] = preg_replace('/[^0-9\.]/', '',$valueArray[0]);
                                    $valueArray[1] = preg_replace('/[^0-9\.]/', '',$valueArray[1]);
                                }

                                $fields->between($valueArray);
                                $object->add($fields);
                            }
                            // searches with < or > parts
                            else if (preg_match('/[><]=?(.*)/', $value, $matches) == 1 && ($fieldDef->getType() == FieldInterface::DATE || $fieldDef->getType() == FieldInterface::NUMBER || $fieldDef->getType() == FieldInterface::MONEY))
                            {
                                if ($fieldDef->getType() == FieldInterface::DATE && \UnicodeString::strpos($matches[1], '@') === false)
                                {
                                    //format properly, but only if it is not an @TODAY search.
                                    $matches[1] = UserDateToSQLDate($matches[1]);
                                }
                                else if ($fieldDef->getType() == FieldInterface::MONEY || $fieldDef->getType() == FieldInterface::NUMBER)
                                {
                                    //remove non-numeric characters
                                    $matches[1] = preg_replace('/[^0-9\.]/', '',$matches[1]);
                                }

                                if ($value[0] == '<')
                                {
                                    if($value[1] == '=')
                                    {
                                        $fields->ltEq($matches[1]);
                                    }
                                    else
                                    {
                                        $fields->lt($matches[1]);
                                    }
                                }
                                else
                                {
                                    if($value[1] == '=')
                                    {
                                        $fields->gtEq($matches[1]);
                                    }
                                    else
                                    {
                                        $fields->gt($matches[1]);
                                    }
                                }
                                $object->add($fields);
                            }
                            // searches containing a wildcard
                            else if (preg_match('/.*\*.*/', $value) == 1)
                            {
                                if (!($fieldDef->getType() == FieldInterface::STRING || $fieldDef->getType() == FieldInterface::TEXT || $fieldDef->getType() == FieldInterface::YESNO || $fieldDef->getType() == FieldInterface::CODE || $fieldDef->getType() == FieldInterface::MULTICODE))
                                {
                                    throw new ReportException('Wildcard character cannot be used in fields of type '.get_class($fieldDef));
                                }

                                if (substr($value, 0, 2) == '!=')
                                {
                                    $fields->notLike(\UnicodeString::str_ireplace('*', '%', substr($value,2)));
                                }
                                else
                                {
                                    $fields->like(\UnicodeString::str_ireplace('*', '%', $value));
                                }
                                $object->add($fields);
                            }
                            //"not equals" searches
                            else if (preg_match('/^\!\=(.*)$/', $value, $matches) == 1)
                            {
                                if($fieldDef->getType() == FieldInterface::DATE && \UnicodeString::strpos($matches[1], '@') === false)
                                {
                                    //format properly, but only if it is not an @TODAY search.
                                    $matches[1] = UserDateToSQLDate($matches[1]);
                                }
                                else if ($fieldDef->getType() == FieldInterface::MONEY || $fieldDef->getType() == FieldInterface::NUMBER)
                                {
                                    //remove non-numeric characters
                                    $matches[1] = preg_replace('/[^0-9\.]/', '',$matches[1]);
                                }

                                if ($fieldDef->getType() == FieldInterface::STRING || $fieldDef->getType() == FieldInterface::TEXT)
                                {
                                    $fields->notLike($matches[1]);
                                }
                                else
                                {
                                    $fields->notEq($matches[1]);
                                }

                                $object->add($fields);
                            }
                            else
                            {
                                $values = explode('|', $value);

                                foreach ($values as $index => $value)
                                {
                                    //format properly, but only if it is not an @TODAY search.
                                    if($fieldDef->GetType() == FieldInterface::DATE && \UnicodeString::strpos($value, '@') === false)
                                    {
                                        //need to make sure any InvalidDataException doesn't bubble up.
                                        try
                                        {
                                            $values[$index] = UserDateToSQLDate($value);
                                        }
                                        catch (\InvalidDataException $e)
                                        {}
                                    }
                                    else if ($fieldDef->getType() == FieldInterface::MONEY || $fieldDef->getType() == FieldInterface::NUMBER)
                                    {
                                        //remove non-numeric characters
                                        $values[$index] = preg_replace('/[^0-9\.]/', '', $values[$index]);
                                    }
                                }

                                $fields->in($values);

                                $object->add($fields);
                            }
                        }
                    }
                }
            }
        }
        return $object;
    }

    /**
     * Used to create a Where object using data fetched by the Mapper.
     * 
     * @param array $data
     * 
     * @return Where|null
     */
    public function createFromMapper(array $data)
    {
        if (!empty($data))
        {
            $where = new Where();
            
            foreach ($data as $criteria)
            {
                $parameters[] = $this->createCriteria($criteria);
            }
            
            $where->setCriteria(['parameters' => $parameters, 'condition' => 'AND']);    
        }
        
        return $where;
    }
    
    /**
     * Recursively creates the criteria for the Where object from the raw data set(s).
     * 
     * @param array $data
     * 
     * @return array
     */
    protected function createCriteria(array $data)
    {
        $parameters = [];
        $field = [];
        $comparisons = [];
        $names = [];
        
        $condition = '';
        $criterionID = '';
        $fieldID = '';
        $nameID = '';
        $compID = '';
        
        foreach ($data as $criterion)
        {
            if ($condition == '')
            {
                $condition = $criterion['condition'] ?: 'AND';
            }
            
            if ($criterion['comp_id'] != $compID)
            {
                if (!empty($comp))
                {
                    $value = array_values($value);
                    $comp['value'] = count($value) == 1 ? $value[0] : $value;
                    $comparisons[] = $comp;
                }
            }
            
            if ($criterion['field_id'] != $fieldID)
            {
                if (!empty($field))
                {
                    if ($criterion['subquery_id'] && $criterion['subquery_field'])
                    {
                        $field[] = $names;
                        $fieldCollection->field($field);

                        $where = (new SavedQueryModelFactory)->getMapper()->find($criterion['subquery_id'])->createQuery()->getWhere();
                        $query = new Query();
                        $query->select($criterion['subquery_field'])->where($where);

                        foreach ($comparisons as $comparison)
                        {
                            $fieldCollection->addComparison($comparison['operator'], $query, 0);
                        }
                    }
                    else
                    {
                        $field[] = $names;
                        $fieldCollection->field($field);

                        foreach ($comparisons as $comparison)
                        {
                            $fieldCollection->addComparison($comparison['operator'], $comparison['value'], $comparison['isField']);
                        }
                    }
                }
            }
            
            if ($criterion['criterion_id'] != $criterionID)
            {
                $criterionID = $criterion['criterion_id'];
                
                if (isset($fieldCollection))
                {
                    $parameters[] = $fieldCollection;
                }
                
                $fieldCollection = new FieldCollection();
            }
            
            if (is_array($criterion[0]))
            {
                $parameters[] = $this->createCriteria($criterion);
                $field = [];
                $comp = [];
            }
            else
            {
                if ($criterion['field_id'] != $fieldID)
                {
                    $fieldID = $criterion['field_id'];
                    $field = [$criterion['wrapper']];
                    $comparisons = [];
                    $names = [];
                }
                
                if ($criterion['name_id'] != $nameID)
                {
                    $names[] = $criterion['name'];
                    $nameID = $criterion['name_id'];
                }
                
                if ($criterion['comp_id'] != $compID)
                {
                    $comp = ['operator' => $criterion['operator'], 'isField' => ($criterion['is_field'] == 1 ? true : false)];
                    $value = [];
                    $compID = $criterion['comp_id'];
                }
                
                $value[$criterion['value_id']] = $criterion['value'];
            }
        }
        
        if (!is_array($criterion[0]))
        {
            if (!empty($comp))
            {
                $value = array_values($value);
                $comp['value'] = count($value) == 1 ? $value[0] : $value;
                $comparisons[] = $comp;
            }

            if ($criterion['subquery_id'] && $criterion['subquery_field'])
            {
                $field[] = $names;
                $fieldCollection->field($field);

                $where = (new SavedQueryModelFactory)->getMapper()->find($criterion['subquery_id'])->createQuery()->getWhere();
                $query = new Query();
                $query->select([ $criterion['subquery_field'] ])->where($where);

                foreach ($comparisons as $comparison)
                {
                    $fieldCollection->addComparison($comparison['operator'], $query, 0);
                }
            }
            else
            {
                $field[] = $names;
                $fieldCollection->field($field);

                foreach ($comparisons as $comparison)
                {
                    $fieldCollection->addComparison($comparison['operator'], $comparison['value'], $comparison['isField']);
                }
            }
            
            $parameters[] = $fieldCollection;
        }
        
        return ['parameters' => $parameters, 'condition' => $condition];
    }

    protected function getSplitValue($valueStr)
    {
        $splitValue = array();

        while (\UnicodeString::strlen($valueStr) > 0)
        {
            // Get the position and type of the first separator
            $sepPipePos = \UnicodeString::strpos($valueStr, '|');
            $sepAmpPos = \UnicodeString::strpos($valueStr, '&');

            if ($sepPipePos !== false || $sepAmpPos !== false)
            {
                if ($sepPipePos !== false && $sepAmpPos !== false)
                {
                    $sepPos = ($sepPipePos < $sepAmpPos ? $sepPipePos : $sepAmpPos);
                    $sep = ($sepPipePos < $sepAmpPos ? '|' : '&');
                }
                elseif ($sepPipePos !== false)
                {
                    $sepPos = $sepPipePos;
                    $sep = '|';
                }
                else
                {
                    // ($sepAmpPos !== false)
                    $sepPos = $sepAmpPos;
                    $sep = '&';
                }
            }
            else
            {
                $sepPos = false;
                $sep = '';
            }

            // Extract the value on the left of the separator (if there is one) and build where clause
            if ($sepPos !== false)
            {
                $valueStrLeft = \UnicodeString::substr($valueStr, 0, $sepPos);
            }
            else
            {
                // No more separators this will be the last pass,
                // use the whole of the remaining string
                $valueStrLeft = $valueStr;
            }

            $splitValue[] = array('operator' => ($sep == '' ? '' : ($sep == '|' ? 'OR' : 'AND')), 'value' => $valueStrLeft);

            if ($sepPos !== false)
            {
                $valueStr = \UnicodeString::substr($valueStr, $sepPos+1);
            }
            else
            {
                $valueStr = '';
            }
        }

        return $splitValue;
    }
    
    /**
     * Oh dear lord - there has to be a better way of doing this.
     * Probably the field names in the search form need to be fully-qualified database field names.
     * But for now...
     * 
     * @param array $data
     * @param Registry $registry
     * 
     * @throws QueryException If we can't figure out what the table should be.
     * 
     * @return string $table
     */
    protected function getTable(array $data, Registry $registry)
    {
        $moduleDefs = $registry->getModuleDefs();
        
        if(isset($data['service']) && $data['service'] == 'DuplicateSearch')
        {
        	return isset( $moduleDefs['CON']['VIEW'] ) ? $moduleDefs['CON']['VIEW'] : $moduleDefs['CON']['TABLE'];
        }
        
        switch ($data['action'])
        {
            case 'incidentsdoselection':
                $table = 'incidents_main';
                break;
            case 'sabsdoselection':
                $table = 'sabs_main';
                break;
            case 'risksdoselection':
                $table = 'ra_main';
                break;
            case 'searchcontact':
                $table = 'contacts_main';
                break;
            case 'actionsdoselection':
                $table = 'ca_actions';
                break;
            case 'assetlinkaction':
                $table = 'assets_main';
                break;
            case 'medicationlinkaction':
                $table = 'medications_main';
                break;
            case 'organisationsdoselection':
                $table = 'organisations_main';
                break;
            case 'doselection':
            	// the view shold be used preferentially if one is defined for this module
                $table = isset( $moduleDefs[$data['module']]['VIEW'] ) ? $moduleDefs[$data['module']]['VIEW'] : $moduleDefs[$data['module']]['TABLE'];
                break;
            default:
                throw new QueryException('Unable to identity the table name.');
        }
        return $table;
    }
}
