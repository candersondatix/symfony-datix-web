<?php
namespace src\framework\query;

use src\framework\query\exceptions\IncompleteQueryException;
use src\framework\query\exceptions\QueryException;
use src\framework\registry\Registry;
use src\system\database\field\CodeField;
use src\system\database\FieldInterface;
use src\system\database\extrafield\ExtraField;
use src\system\database\fielddef\FieldDefFactory;

/**
 * Constructs and returns a SQL statement and associated bind parameters from a supplied Query object.
 * 
 * This implementation is SQL Server-specific.
 */
class SqlWriter
{
    /**
     * @var Registry
     */
    protected $registry;
    
    /**
     * @var QueryFactory
     */
    protected $queryFactory;
    
    /**
     * @var DateTime
     */
    protected $dateToday;
    
    /**
     * Flag to prevent @codes from being replaced in the written query.
     * 
     * @var boolean
     */
    protected $retainAtCodes;

    public function __construct(Registry $registry = null, QueryFactory $queryFactory = null, \DateTime $dateToday = null)
    {
        $this->registry      = $registry ?: Registry::getInstance();
        $this->queryFactory  = $queryFactory ?: new QueryFactory();
        $this->dateToday     = $dateToday ?: new \DateTime();
        $this->retainAtCodes = false;
    }
    
    /**
     * Constructs a SQL statement from a Query object.
     * 
     * @param Query $query The query object.
     * 
     * @throws IncompleteQueryException If the passed query object is incomplete.
     * 
     * @return array The SQL string and associated parameter values.
     */
    public function writeStatement(Query $query)
    {
        if ($query->getQueryString())
        {
            return array($query->getQueryString(), array());
        }

        if (!$query->isComplete())
        {
            throw new IncompleteQueryException('Unable to build SQL statement; query is incomplete.');
        }
        
        if (!$this->retainAtCodes)
        {
            $query->replaceAtCodes();
        }
        
        switch ($query->getType())
        {
            case Query::SELECT:
                list($statement, $parameters) = $this->writeSelect($query);
                break;
                
            case Query::INSERT:
                list($statement, $parameters) = $this->writeInsert($query);
                break;
                
            case Query::UPDATE:
                list($statement, $parameters) = $this->writeUpdate($query);
                break;
                
            case Query::DELETE:
                list($statement, $parameters) = $this->writeDelete($query);
                break;
        }
        
        return array($statement, $parameters);
    }
    
    /**
     * Provides a means to combine select queries using the UNION clause.
     * 
     * @param Query $query1 [, Query $... ] Any number of Query objects to combine.
     * 
     * @throws QueryException If any of the arguments passed are not Query objects of type SELECT.
     * 
     * @return array The SQL string and associated parameter values.
     */
    public function writeUnionStatement()
    {
        $args = func_get_args();
        $sql = '';
        $parameters = array();
        
        foreach ($args as $num => $query)
        {
            if (!($query instanceof Query) || $query->getType() !== Query::SELECT)
            {
                throw new \InvalidArgumentException('SqlWriter::writeUnionStatement() arguments must be Query objects of type Query::SELECT');
            }
            
            list($statement, $conditions) = $this->writeStatement($query);
            
            $sql .= $statement;
            $parameters = array_merge($parameters, $conditions);
            
            if (($num + 1) < count($args))
            {
                $sql .= ' UNION ';
            }
        }
        
        return array($sql, $parameters);
    }
    
    /**
     * Builds a SELECT statement.
     * 
     * @param Query $query
     * 
     * @return array
     */
    protected function writeSelect(Query $query)
    {
        // select clause
        $sql = $this->writeSelectClause($query);
        
        // join clause(s)
        list($clause, $parameters) = $this->writeJoinClause($query);
        $sql .= $clause;
        
        // cross apply clause(s)
        list($clause, $conditions) = $this->writeCrossApplyClause($query);
        $sql .= $clause;
        $parameters = array_merge($parameters, $conditions);
        
        // where clause
        list($clause, $conditions) = $this->writeWhereClause($query);
        $sql .= $clause;
        $parameters = array_merge($parameters, $conditions);
        
        // group by clause
        $sql .= $this->writeGroupByClause($query);

        // order by clause
        $sql .= $this->writeOrderByClause($query);
        
        // limit clause
        $sql = $this->writeLimitClause($query, $sql);
        
        // santa claus
        return array($sql, $parameters);
    }
    
    /**
     * Constructs the SELECT clause for a SELECT statement.
     * 
     * @param Query $query The query object.
     * 
     * @return string $sql
     */
    protected function writeSelectClause(Query $query)
    {
        if ($query->getTop() !== null)
        {
            $top = 'TOP '.$query->getTop().' ';
        }
        
        if ($query->getLimit() !== null)
        {
            $limit = 'ROW_NUMBER() OVER ('.$this->writeOrderByClause($query).') AS row_num,';
            $query->orderBy([], true);
        }
        
        $sql = 'SELECT '.$top.$limit.$this->resolveFields($query->getFields()).' FROM '.$query->getTable();
        
        if( $query->getDateFirst() )
        {
        	$query->setQueryString('SET DATEFIRST '. $query->getDateFirst() .'; '. $query->getQueryString());
        }
       
        return $sql;
    }
    
    /**
     * Adds the subquery table prefix to fields in order to build statement for paging.
     * 
     * @param string|array $val    The field (can be an array if the field is contained within a more complex statement, e.g. function, alias etc).
     * @param string       $prefix The alias of the subquery result
     * 
     * @return string|array The converted field name.
     */
    protected function addSubSelectPrefix($val, $prefix)
    {
        $field = $prefix.'.'.str_replace('.', '_', (is_array($val) ? $val[1] : $val));
        return is_array($val) ? array($val[0], $field) : $field;
    }
    
    /**
     * Converts an array of fields into a comma-separated list.
     * 
     * @param array   $fields    Each element can be either a string for the field name on its own, or an array 
     *                           where the first element is a clause that the field name is wrapped in (using ? as a placeholder),
     *                           and the second element is the actual field name, e.g. array('? AS alias', 'table.field').
     *                           The second element can also be an array in situations where we're using more than one field in the expression,
     *                           e.g. array('COALESCE(?, ?) AS alias', array('table.field1', 'table.field2'))
     * @param boolean $fieldOnly Whether or not to return the basic field name only, regardless of if its used within a function etc.
     *                      
     * @return string
     */
    protected function resolveFields(array $fields = array(), $fieldOnly = false)
    {
        $fieldList = array();
        foreach ($fields as $field)
        {
            if (is_array($field))
            {
                if (is_array($field[1]))
                {
                    foreach ($field[1] as $f)
                    {
                        $field[0] = implode($f, explode('?', $field[0], 2));
                    }
                    $fieldList[] = $field[0];
                }
                else
                {
                    $fieldList[] = $fieldOnly ? $field[1] : \UnicodeString::str_ireplace('?', $field[1], $field[0]);
                }
            }
            else
            {
                $fieldList[] = $field;
            }
        }
        
        if ($fieldOnly)
        {
            $fieldList = array_unique($fieldList);
        }
        
        return implode(',', $fieldList);
    }
    
    /**
     * Constructs the JOIN clause(s) for a SELECT statement.
     * 
     * @param Query $query The query object.
     * 
     * @return array
     */
    protected function writeJoinClause(Query $query)
    {
        $sql = '';
        $parameters = array();
        
        if (count($query->getJoins()) > 0)
        {
            foreach ($query->getJoins() as $table => $join)
            {
                $subSql = '';
                if (isset($join['subQuery']) && $join['subQuery'] != '')
                {
                    list($subSql, $conditions) = $this->writeStatement($join['subQuery']);
                    $parameters = array_merge($parameters, $conditions);
                }

                if ($join['where'] instanceof Where)
                {
                    list($where, $conditions) = $this->doWriteWhere($join['where']->getCriteria());
                    $parameters = array_merge($parameters, $conditions);

                    if($join['wherestring'] != '')
                    {
                        $where = '('.$where.') AND ('.$join['wherestring'].')';
                    }
                }
                else
                {
                    if($join['wherestring'] != '' && $join['where'] != '')
                    {
                        $where = '('.$join['where'].') AND ('.$join['wherestring'].')';
                    }
                    else if ($join['where'] != '')
                    {
                        $where = $join['where'];
                    }
                    else
                    {
                        $where = $join['wherestring'];
                    }
                }

                $sql .= $this->getJoinClauseString($join, $subSql, $table, $where);
            }
        }

        return array($sql, $parameters);
    }

    protected function getJoinClauseString($join, $subSql, $table, $where)
    {
        return ' '.$join['type'].' JOIN '.(isset($subSql) && $subSql != '' ? '('.$subSql.') AS ' : '').$table.(!empty($join['alias']) ? ' AS '.$join['alias'] : '').' ON '.$where;
    }
    
    /**
     * Constructs the CROSS APPLY clause(s) for a SELECT statement.
     * 
     * @param Query $query The query object.
     * 
     * @return array
     */
    protected function writeCrossApplyClause(Query $query)
    {
        $sql = '';
        $parameters = array();
        
        if (count($query->getCrossApply()) > 0)
        {
            foreach ($query->getCrossApply() as $alias => $query)
            {
                list($subQuery, $conditions) = $this->writeSelect($query);
                $sql .= ' CROSS APPLY ('.$subQuery.') '.$alias.' ';
                $parameters = array_merge($parameters, $conditions);
            }
        }
        return array($sql, $parameters);
    }
    
    /**
     * Constructs the GROUP BY clause for a SELECT statement.
     * 
     * @param Query $query The query object.
     * 
     * @return string $sql
     */
    protected function writeGroupByClause(Query $query)
    {
        $sql = '';
        if (count($query->getGroupBy()) > 0)
        {
            $sql = ' GROUP BY ';
            foreach ($query->getGroupBy() as $value)
            {
                $sql .= $this->resolveFields(array($value)).', ';
            }
            $sql = \UnicodeString::substr($sql, 0, -2);
        }
        return $sql;
    }
    
    /**
     * Constructs the ORDER BY clause for a SELECT statement.
     * 
     * @param Query $query The query object.
     * 
     * @return string $sql
     */
    protected function writeOrderByClause(Query $query)
    {
        $sql = '';
        if (count($query->getOrderBy()) > 0)
        {
            $sql = ' ORDER BY ';
            foreach ($query->getOrderBy() as $value)
            {
                $sql .= $this->resolveFields(array($value[0])).' '.$value[1].', ';
            }
            $sql = \UnicodeString::substr($sql, 0, -2);
        }
        return $sql;
    }
    
    /**
     * Constructs the LIMIT clause for a SELECT statement.
     * 
     * @param Query  $query The query object.
     * @param string $sql   The SQL statement to be executed.
     * 
     * @return string $sql
     */
    protected function writeLimitClause(Query $query, $sql)
    {
        if ($query->getLimit() != null)
        {
            list($start, $number) = $query->getLimit();
            
            $sql = 'SELECT * FROM ('.$sql.') AS PagedResult WHERE row_num > '.$start.' AND row_num <= '.($start+$number).' ORDER BY row_num';
        }
        return $sql;
    }
    
    /**
     * Builds an INSERT statement.
     * 
     * @param Query $query
     * 
     * @return array
     */
    protected function writeInsert(Query $query)
    {
        $sql = 'INSERT INTO ' . $query->getTable() .
            ' ('.implode(',', array_keys($query->getFields())) . ')' .
            ' VALUES' . 
            ' ('.implode(',', array_map(function($val){return ':'.$val;}, array_keys($query->getFields()))) . ')' .
            ';SELECT SCOPE_IDENTITY() AS \'identity\'';
        
        return array($sql, $query->getFields());
    }
    
    /**
     * Builds an UPDATE statement.
     * 
     * @param Query $query
     * 
     * @return array
     */
    protected function writeUpdate(Query $query)
    {
        $sql = 'UPDATE ' . $query->getTable() . ' SET ';
        
        foreach (array_keys($query->getFields()) as $field)
        {
            $sql .= $field . ' = ?, ';
        }
        $sql = \UnicodeString::substr($sql, 0, -2);
        
        $parameters = $query->getFields();
        
        if ($query->getWhere() !== null)
        {
            list($where, $whereParameters) = $this->writeWhereClause($query);
            $sql .= $where;
            $parameters = array_merge(array_values($parameters), $whereParameters);
        }
        
        return array($sql, $parameters);
    }
    
    /**
     * Builds a DELETE statement.
     * 
     * @param Query $query
     * 
     * @return array
     */
    protected function writeDelete(Query $query)
    {
        $parameters = array();
        
        $sql = 'DELETE FROM ' . $query->getTable();
        
        if ($query->getWhere() !== null)
        {
            list($where, $parameters) = $this->writeWhereClause($query);
            $sql .= $where;
        }
        
        return array($sql, $parameters);
    }
    
    /**
     * Constructs a SQL WHERE clause.
     *
     * @param Query $query The query object.
     *
     * @return array The WHERE clause string and associated parameter values.
     */
    public function writeWhereClause(Query $query)
    {
        $sql = '';
        $parameters = array();
        $where = array();
        
        // build WHERE clause from Where object
        if ($query->getWhere() !== null)
        {
            list($clause, $parameters) = $this->doWriteWhere($query->getWhere()->getCriteria());

            if ($clause != '')
            {
                $where[] = $clause;
            }
        }
        
        // append manually constructed WHERE strings to clause
        if (count($query->getwhereStrings()) > 0)
        {
            $where[] = '(' . implode(' AND ', $query->getwhereStrings()) . ')';
        }
        
        // add security WHERE clause(s) for this user/module
        if (!$query->isSecurityOverriden())
        {
            foreach ($query->getModules() as $module)
            {
                $SecurityWhereClause = MakeSecurityWhereClause('', $module, $_SESSION['initials']);

                if ($SecurityWhereClause != '')
                {
                    $where[] =  '(' . $SecurityWhereClause . ')';
                }
            }
        }
        
        // combine components to create clause
        if (!empty($where))
        {
            $sql = ' WHERE ' . implode(' AND ', $where);
        }
        
        return array($sql, $parameters);
    }

    /**
     * Merges sql string and parameter array for the purposes of presenting a human-readable version.
     *
     * @param $sql
     * @param $parameters
     * @return mixed
     */
    public function combineSqlAndParameters($sql, $parameters)
    {
        // Replace all occurrences of ? with values on the where clause
        $needle = '?';

        foreach ($parameters as $parameter)
        {
            $parameterPos = \UnicodeString::strpos($sql, $needle);

            if ($parameterPos !== false)
            {
                $sql = \UnicodeString::substr_replace($sql, \DatixDBQuery::quote($parameter), $parameterPos, \UnicodeString::strlen($needle));
            }
        }

        return $sql;
    }
    
    /**
     * Resolves a Query object into a where string with the literal parameters stitched in.
     * 
     * Used to output the query in parts of the application where we display a user-editable where clause.
     * 
     * @param Query $query
     * 
     * @return string
     */
    public function writeLiteralWhereClause(Query $query)
    {
        // we shouldn't ever need to include a security where clause here since  
        // it should be appended at runtime when (if) the query is ever executed
        $query->overrideSecurity();
        
        $table = $query->getTable();
        
        $query->select([$table.'.recordid'], true);
        
        list($sql, $parameters) = $this->writeStatement($query);
        return $table.'.recordid IN ('.$this->combineSqlAndParameters($sql, $parameters).')';
    }
    
    /**
     * Recursive function which loops through the provided criteria array in order to build up the WHERE clause.
     * 
     * @param array $criteria A set of (potentially nested) arrays of FieldCollections, and the condition which joins them.
     * @param array $values   The values of the parameters to be bound to the WHERE clause - can be built up by passing it recursively to this method.
     * 
     * @return array The WHERE clause string and associated parameter values.
     */
    protected function doWriteWhere(array $criteria, array $values = array())
    {
    	$clauses = array();
        
        foreach ($criteria['parameters'] as $parameter)
        {
            if ($parameter instanceof FieldCollection)
            {
                list($newWhere, $newValues) = $this->resolveFieldCollection($parameter);
                
                if ($newWhere != '')
                {
                    $clauses[] = $newWhere;
                    $values = array_merge($values, $newValues);
                }
            }
            else
            {
                list($newWhere, $values) = $this->doWriteWhere($parameter, $values);
                
                if ($newWhere != '')
                {
                    $clauses[] = $newWhere;
                }
            }
        }

        if (!empty($clauses))
        {
            $where = '(' . implode(' '.$criteria['condition'].' ', $clauses) . ')';
        }

        return array($where, $values);
    }

    /**
     * @param array $comps
     * @param DateTime $today Today's date: used as basis for relative date calculations.
     * Needed so we can test with specific scenarios, rather than relying on "current date" testing.
     * @return array
     *
     * Takes an array containing a 'value' and 'operator' part and checks to see whether the value is an @code.
     * If it is, the value and (if necessary) operator are modified to contain appropriate data for a select statement.
     *
     * TODO: error checking/validation needed.
     * TODO: move this all to somewhere else - preferably before the query itself starts getting built, to allow the query structure to be more easily modified.
     */
    protected function replaceAtCodes($comps)
    {
        if ($comps instanceof Query)
        {
            return;
        }
        
        //determine whether there are @codes to be processed.
        //TODO: Make this check more specific: emails etc can contain @ character without being @codes
        $atCodesPresent = false;
        foreach ($comps['value'] as $value)
        {
            if (\UnicodeString::strpos($value, '@') !== false)
            {
                $atCodesPresent = true;
            }
        }

        if ($atCodesPresent)
        {
            //there should never be more than one value here if there is an @code search, as they should have been split on | in the WhereFactory.
            if (count($comps['value']) > 1)
            {
                throw new QueryException('Too many parameters when processing @codes.');
            }
            else
            {
                $comps['value'] = $comps['value'][0];
            }

            $today = $this->dateToday;

            $comps['value'] = \UnicodeString::str_ireplace('@USER_INITIALS', $_SESSION['initials'], $comps['value']);

            while (preg_match("/@TODAY/iu", $comps['value']) > 0)
            {
                if (\UnicodeString::strtoupper($comps['value']) == '@TODAY')
                {
                    $comps['value'] = $today->format('Y-m-d 00:00:00.000');
                }
                else
                {
                    //TODO: remove reliance on this library.
                    require_once 'Source/libs/SearchSymbols.php';
                    $comps['value'] = GetAtTodayDate($comps['value']);
                }
            }

            switch (\UnicodeString::strtoupper($comps['value']))
            {
                case '@PROMPT':
                    // This is now dealt within the Query classes.  Eventually all this logic should be moved across.
                    break;
                    
                case '@WEEK':
                    //date range running from the start of the current week (in the past) to the end of the current week (in the future)
                    $currentWeekDay = (int) $today->format('w');
                    $weekStartDay = (int) GetParm('WEEK_START_DAY', 2) - 1;

                    if ($currentWeekDay >= $weekStartDay)
                    {
                        $daysToSubtract = ($currentWeekDay - $weekStartDay);
                    }
                    else
                    {
                        $daysToSubtract = (7 - $weekStartDay + $currentWeekDay);
                    }

                    $thisWeekStart = clone($today);
                    $thisWeekStart->sub(new \DateInterval('P'.$daysToSubtract.'D'));

                    $thisWeekEnd = clone($thisWeekStart);
                    $thisWeekEnd->add(new \DateInterval('P6D'));

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($thisWeekStart->format('Y-m-d 00:00:00.000'), $thisWeekEnd->format('Y-m-d 00:00:00.000'));

                    break;
                case '@LASTWEEK':
                    //date range starting at the beginning of the last complete week to have occurred and consisting of 7 full days.
                    $currentWeekDay = (int) $today->format('w');
                    $weekStartDay = (int) GetParm('WEEK_START_DAY', 2) - 1;

                    if ($currentWeekDay >= $weekStartDay)
                    {
                        $daysToSubtract = ($currentWeekDay - $weekStartDay);
                    }
                    else
                    {
                        $daysToSubtract = (7 - $weekStartDay + $currentWeekDay);
                    }

                    $thisWeekStart = clone($today);
                    $thisWeekStart->sub(new \DateInterval('P'.$daysToSubtract.'D'));

                    $lastWeekEnd = clone($thisWeekStart);
                    $lastWeekEnd->sub(new \DateInterval('P1D'));

                    $lastWeekStart = clone($lastWeekEnd);
                    $lastWeekStart->sub(new \DateInterval('P6D'));

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($lastWeekStart->format('Y-m-d 00:00:00.000'), $lastWeekEnd->format('Y-m-d 00:00:00.000'));
                    break;

                case '@MONTH':
                    $yearMonth = $today->format('Y-m');
                    $monthStart = new \DateTime($yearMonth.'-01');

                    $monthEnd = clone($monthStart);
                    $monthEnd->add(new \DateInterval('P1M'));
                    $monthEnd->sub(new \DateInterval('P1D'));

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($monthStart->format('Y-m-d 00:00:00.000'), $monthEnd->format('Y-m-d 00:00:00.000'));
                    break;
                case '@LASTMONTH':
                    $yearMonth = $today->format('Y-m');
                    $monthStart = new \DateTime($yearMonth.'-01');
                    $monthStart->sub(new \DateInterval('P1M'));

                    $monthEnd = clone($monthStart);
                    $monthEnd->add(new \DateInterval('P1M'));
                    $monthEnd->sub(new \DateInterval('P1D'));

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($monthStart->format('Y-m-d 00:00:00.000'), $monthEnd->format('Y-m-d 00:00:00.000'));
                    break;

                case '@QUARTER':
                    $year = $today->format('Y');

                    if ((int) $today->format('m') >= 10)
                    {
                        $quarterStart = new \DateTime('first day of October '.$year);
                        $quarterEnd = new \DateTime('last day of December '.$year);
                    }
                    else if ((int) $today->format('m') >= 7)
                    {
                        $quarterStart = new \DateTime('first day of July '.$year);
                        $quarterEnd = new \DateTime('last day of September '.$year);
                    }
                    else if ((int) $today->format('m') >= 4)
                    {
                        $quarterStart = new \DateTime('first day of April '.$year);
                        $quarterEnd = new \DateTime('last day of June '.$year);
                    }
                    else if ((int) $today->format('m') >= 1)
                    {
                        $quarterStart = new \DateTime('first day of January '.$year);
                        $quarterEnd = new \DateTime('last day of March '.$year);
                    }

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($quarterStart->format('Y-m-d 00:00:00.000'), $quarterEnd->format('Y-m-d 00:00:00.000'));
                    break;
                case '@LASTQUARTER':
                    $year = $today->format('Y');

                    if ((int) $today->format('m') >= 10)
                    {
                        $quarterStart = new \DateTime('first day of July '.$year);
                        $quarterEnd = new \DateTime('last day of September '.$year);
                    }
                    else if ((int) $today->format('m') >= 7)
                    {
                        $quarterStart = new \DateTime('first day of April '.$year);
                        $quarterEnd = new \DateTime('last day of June '.$year);
                    }
                    else if ((int) $today->format('m') >= 4)
                    {
                        $quarterStart = new \DateTime('first day of January '.$year);
                        $quarterEnd = new \DateTime('last day of March '.$year);
                    }
                    else if ((int) $today->format('m') >= 1)
                    {
                        $quarterStart = new \DateTime('first day of October '.($year-1));
                        $quarterEnd = new \DateTime('last day of December '.($year-1));
                    }

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($quarterStart->format('Y-m-d 00:00:00.000'), $quarterEnd->format('Y-m-d 00:00:00.000'));
                    break;

                case '@YEAR':
                    $year = $today->format('Y');
                    //current calendar year (1 Jan - 31 Dec)
                    $yearStart = new \DateTime('first day of January '.$year);
                    $yearEnd = new \DateTime('last day of December '.$year);

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($yearStart->format('Y-m-d 00:00:00.000'), $yearEnd->format('Y-m-d 00:00:00.000'));
                    break;
                case '@LASTYEAR': //previous calendar year (1 Jan - 31 Dec)
                    $year = $today->format('Y');
                    //start with the current year
                    $yearStart = new \DateTime('first day of January '.$year);
                    $yearEnd = new \DateTime('last day of December '.$year);

                    //subtract one year;
                    $yearStart->sub(new \DateInterval('P1Y'));
                    $yearEnd->sub(new \DateInterval('P1Y'));

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($yearStart->format('Y-m-d 00:00:00.000'), $yearEnd->format('Y-m-d 00:00:00.000'));
                    break;

                case '@FINYEAR':
                    $year = $today->format('Y');

                    $finMonthStart = GetParm("FINYEAR_START_MONTH", "4");
                    $finYearStart = new \DateTime($year.'-'.$finMonthStart.'-01');

                    $finYearEnd = clone($finYearStart);
                    $finYearEnd->add(new \DateInterval('P1Y'));
                    $finYearEnd->sub(new \DateInterval('P1D'));

                    if ($today->format('m') < $finMonthStart)
                    {
                        $finYearStart->sub(new \DateInterval('P1Y'));
                        $finYearEnd->sub(new \DateInterval('P1Y'));
                    }

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($finYearStart->format('Y-m-d 00:00:00.000'), $finYearEnd->format('Y-m-d 00:00:00.000'));
                    break;
                case '@LASTFINYEAR':
                    $year = $today->format('Y');

                    $finMonthStart = GetParm("FINYEAR_START_MONTH", "4");

                    //End of the last financial year is one day before the start of this years'
                    $finYearEnd = new \DateTime($year.'-'.$finMonthStart.'-01');
                    $finYearEnd->sub(new \DateInterval('P1D'));

                    $finYearStart = clone($finYearEnd);
                    $finYearStart->sub(new \DateInterval('P1Y'));
                    $finYearStart->add(new \DateInterval('P1D'));

                    //need to offset by one year, as the financial year for this calendar year is the next one, not the current one.
                    if ($today->format('m') < $finMonthStart)
                    {
                        $finYearStart->sub(new \DateInterval('P1Y'));
                        $finYearEnd->sub(new \DateInterval('P1Y'));
                    }

                    $comps['operator'] = FieldCollection::BETWEEN;
                    $comps['value'] = array($finYearStart->format('Y-m-d 00:00:00.000'), $finYearEnd->format('Y-m-d 00:00:00.000'));
                    break;
            }

            //TODO: @USER_CON... codes.

            //if we've changed the operator to between, we don't want to revert to the passed array strucutre, which will have too many levels.
            if (!is_array($comps['value']))
            {
                $comps['value'] = array($comps['value']);
            }
        }

        return $comps;
    }
    
    /**
     * Constructs and returns a WHERE clause string and associated parameter values from a FieldCollection.
     * 
     * @param FieldCollection $collection
     * 
     * @return array The WHERE clause string and associated parameter values.
     */
    protected function resolveFieldCollection(FieldCollection $collection)
    {
        $where       = '';
        $comparisons = array();
        $values      = array();
        $fieldDefs   = $this->registry->getFieldDefs();

        foreach ($collection->getFields() as $field)
        {
            $fieldObj = $field->getFieldDef();
            
            foreach ($field->getComps() as $comps)
            {
                //replace @ codes with appropriate values.
                //Can cause problems for text/string fields since they can contain '@' character.
                if ((!$fieldObj || in_array($fieldObj->getType(), array(FieldInterface::CODE, FieldInterface::MULTICODE, FieldInterface::YESNO, FieldInterface::DATE))) && !$this->retainAtCodes)
                {
                    $comps = $this->replaceAtCodes($comps);
                }

                if ($fieldObj && $fieldObj->getType() == FieldInterface::MULTICODE && 
                        in_array($comps['operator'], [FieldCollection::EQ, FieldCollection::LIKE, FieldCollection::IN]) &&
                        !($comps['value'] instanceof Query))
                {
                    foreach ($comps['value'] as $value)
                    {
                        $multicodeComparison[] =
                        $field->getName() . ' LIKE ? OR ' .
                        $field->getName() . ' LIKE ? OR ' .
                        $field->getName() . ' LIKE ? OR ' .
                        $field->getName() . ' LIKE ?';

                        $values[] = $value;
                        $values[] = '% '.$value;
                        $values[] = $value.' %';
                        $values[] = '% '.$value.' %';
                    }

                    $comparison = '('.implode(') OR (', $multicodeComparison).')';
                }
                elseif ($fieldObj && $fieldObj->getType() == FieldInterface::MULTICODE &&
                    in_array($comps['operator'], [FieldCollection::NOT_EQ]) &&
                    !($comps['value'] instanceof Query))
                {
                    foreach ($comps['value'] as $value)
                    {
                        // Check if the value has more than one comparison
                        if (strlen($value) > 0 && (strpos($value, '|') || strpos($value, '&')))
                        {
                            if (strpos($value, '|'))
                            {
                                $delimiter = '|';
                            }
                            elseif (strpos($value, '&'))
                            {
                                $delimiter = '&';
                            }

                            $moreComparisons = explode($delimiter, $value);

                            // Count the number of comparison occurrences
                            $numOccurrences = substr_count($value, $delimiter);

                            // Iterate through the comparisons and add them to the array
                            for ($i = 0; $i <= $numOccurrences; $i++)
                            {
                                $multicodeComparison[] =
                                    'NOT ('.
                                    $field->getName() . ' LIKE ? OR ' .
                                    $field->getName() . ' LIKE ? OR ' .
                                    $field->getName() . ' LIKE ? OR ' .
                                    $field->getName() . ' LIKE ?' .
                                    ')';

                                // get first and second chars to check the type of comparison
                                $moreComparisons[$i] = trim($moreComparisons[$i]);
                                $chars = \UnicodeString::str_split($moreComparisons[$i]);

                                if (($chars[1] == '=' || $chars[1] == '>' ) && ($chars[0] == '<' || $chars[0] == '>' || $chars[0] == '!'))
                                {
                                    $moreComparisons[$i] = \UnicodeString::trim(\UnicodeString::substr($moreComparisons[$i], 2, \UnicodeString::strlen($moreComparisons[$i])-2));
                                }

                                $values[] = $moreComparisons[$i];
                                $values[] = '% '.$moreComparisons[$i];
                                $values[] = $moreComparisons[$i].' %';
                                $values[] = '% '.$moreComparisons[$i].' %';
                            }

                            $comparison = '('.implode(') '.($delimiter == '&' ? 'AND' : 'OR').' (', $multicodeComparison).')';
                        }
                        else
                        {
                            $multicodeComparison[] =
                                $field->getName() . ' NOT LIKE ? AND ' .
                                $field->getName() . ' NOT LIKE ? AND ' .
                                $field->getName() . ' NOT LIKE ? AND ' .
                                $field->getName() . ' NOT LIKE ?';

                            $values[] = $value;
                            $values[] = '% '.$value;
                            $values[] = $value.' %';
                            $values[] = '% '.$value.' %';

                            $comparison = '('.implode(') AND (', $multicodeComparison).')';
                        }
                    }
                }
                else if ($comps['operator'] == FieldCollection::ALL)
                {
                    $comparison = '(1=1)';
                }
                else if ($comps['operator'] == FieldCollection::NONE)
                {
                    $comparison = '(1=2)';
                }
                else
                {
                    if ($fieldObj && $fieldObj->getType() == FieldInterface::DATE)
                    {
                        $comparison = $field->evaluateWrap( 'CAST( FLOOR( CAST( '. array_shift( $field->getRawNames() ) .' AS FLOAT ) ) AS DATETIME) ' ) . $this->convertOperator($comps['operator']);
                    }
                    else
                    {
                        $comparison = $field->getName() . ' ' . $this->convertOperator($comps['operator']);
                    }

                    if (!in_array($comps['operator'], [ FieldCollection::NOT_NULL, FieldCollection::NOT_EMPTY, FieldCollection::IS_NULL, FieldCollection::IS_EMPTY ]))
                    {
                        if (in_array($comps['operator'], [ FieldCollection::IN, FieldCollection::NOT_IN ]))
                        {
                            // in/not in comparisons
                            if ($comps['value'] instanceof Query)
                            {
                                // build the sub-select statement
                                list($sql, $parameters) = $this->writeStatement($comps['value']);
                                $comparison .= ' ('.$sql.')';
                                $values = array_merge($values, $parameters);
                            }
                            else
                            {
                                $comparison .= ' ('.implode(',', array_map(function(){return '?';}, $comps['value'])).')';
                                $values = array_merge($values, $comps['value']);
                            }
                        }
                        else if ($comps['operator'] == FieldCollection::BETWEEN)
                        {
                            $comparison .= ' ? AND ?';
                            $values = array_merge($values, $comps['value']);
                        }
                        else
                        {
                            // all other comparisons
                            if ($comps['isField'])
                            {
                                $comparison .= ' '.$comps['value'][0];
                            }
                            else
                            {
                                $comparison .= ' ?';
                                $values[] = $comps['value'][0];
                            }
                        }

                        if (in_array($comps['operator'], [ FieldCollection::NOT_EQ, FieldCollection::NOT_IN ]) && !($comps['value'] instanceof Query))
                        {
                            // include NULLs when using not equal comparisons
                            $comparison .= ' OR ' . $field->getName() . ' IS NULL';

                            // need to bracket off this subclause so that we don't end up with (x OR y AND z)
                            // when it should be ((x OR y) AND z)
                            $comparison = '('.$comparison.')';
                        }
                    }
                }

                $comparisons[] = $comparison;
            }
        }

        // within a collection, all parameters are combined using AND
        if (!empty($comparisons))
        {
            $where .= '(' . implode(' AND ', $comparisons) . ')';
        }

        return array($where, $values);
    }
    
    /**
     * Converts a FieldCollection operator constant to the SQL string equivalent.
     * 
     * @param int $operator
     * 
     * @throws QueryException If the $operator is not valid.
     * 
     * @return string $sql
     */
    protected function convertOperator($operator)
    {        
        switch ($operator)
        {
            case FieldCollection::EQ:
                $sql = "=";
                break;
                
            case FieldCollection::NOT_EQ:
                $sql = "!=";
                break;
            
            case FieldCollection::GT:
                $sql = ">";
                break;
                
            case FieldCollection::GT_EQ:
                $sql = ">=";
                break;
                
            case FieldCollection::LT:
                $sql = "<";
                break;
            
            case FieldCollection::LT_EQ:
                $sql = "<=";
                break;
                
            case FieldCollection::IN:
                $sql = "IN";
                break;
                
            case FieldCollection::NOT_IN:
                $sql = "NOT IN";
                break;
            
            case FieldCollection::IS_NULL:
                $sql = "IS NULL";
                break;
                
            case FieldCollection::NOT_NULL:
                $sql = "IS NOT NULL";
                break;
                
            case FieldCollection::IS_EMPTY:
                $sql = "= ''";
                break;
            
            case FieldCollection::NOT_EMPTY:
                $sql = "!= ''";
                break;
                
            case FieldCollection::LIKE:
                $sql = "LIKE";
                break;
                
            case FieldCollection::NOT_LIKE:
                $sql = "NOT LIKE";
                break;
                
            case FieldCollection::BETWEEN:
                $sql = "BETWEEN";
                break;
            
            default:
                throw new QueryException('Operator '.$operator.' is invalid');
        }
        return $sql;
    }
    
    /**
     * Setter for retainAtCodes.
     * 
     * @param boolean $value
     */
    public function retainAtCodes($value = true)
    {
        $this->retainAtCodes = (bool) $value;
    }
}
