<?php
namespace src\framework\query\exceptions;

class IncompleteQueryException extends QueryException{}