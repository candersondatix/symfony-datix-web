<?php
namespace src\framework\query\exceptions;

class InvalidQueryTypeException extends QueryException{}