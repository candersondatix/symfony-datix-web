<?php
namespace src\framework\query\exceptions;

class InvalidComparisonException extends QueryException{}