<?php
namespace src\framework\query;

use src\reports\model\packagedreport\PackagedReport;

class QueryFactory
{
    /**
     * Creates and returns a FieldCollection object.
     * 
     * @return FieldCollection
     */
    public function getFieldCollection()
    {
        return new FieldCollection();
    }
    
    /**
     * Creates and returns a Where object.
     * 
     * @return Where
     */
    public function getWhere()
    {
        return new Where();
    }
    
    /**
     * Creates and returns a Query object.
     * 
     * @return Query
     */
    public function getQuery()
    {
        return new Query();
    }

    /**
     * Creates and returns a SqlWriter object.
     * 
     * @return SqlWriter
     */
    public function getSqlWriter()
    {
        return new SqlWriter();
    }

    /**
     * Convenience method to return most commonly used query objects.
     * 
     * @return array
     */
    public function getQueryObjects()
    {
        return array($this->getQuery(), $this->getWhere(), $this->getFieldCollection());
    }
}