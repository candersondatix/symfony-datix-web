/**
 * Add the variable globals to the global scope so it can be used with the asset manager
 *
 * @type {{origin: (*|string|origin|globals.origin|w.origin), pathname: string, queryString: (Function|string), queryObj: *}}
 */
var globals = {
    origin: window.location.origin,
    pathname: window.location.pathname,
    queryString: window.location.search,
    queryObj: splitQuery(window.location.search)
};

/**
 * Add the variable txt to the global scope so it can be used with the asset manager - currently only through addToJs
 * but ideally this would be done with a dedicated function
 *
 * @type {{}}
 */
var txt = {};

function splitQuery(string) {

    var pattern = /(?:\?|&)([^=]+)=([^&]+)/ig;

    var object = {};

    while (match = pattern.exec(string)) {

        object[match[1]] = match[2];
    }

    return object;
}