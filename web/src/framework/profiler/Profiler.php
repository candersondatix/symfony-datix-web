<?php
namespace src\framework\profiler;

class Profiler
{
    const XHPROF_ROOT = 'tests/vendor/facebook/xhprof/';
    
    /**
     * The name of this profile.
     * 
     * @var string
     */
    protected $name;
    
    /**
     * The profile data.
     * 
     * @var array
     */
    protected $data;
    
    /**
     * Whether or not profiling has been enabled.
     * 
     * @var boolean
     */
    protected $enabled;
    
    public function __construct($name = 'datixweb')
    {
        $this->name = $name;
        $this->enabled = (bool) $GLOBALS['profiler_enabled'];
        
        if ($this->enabled)
        {
            if (!extension_loaded('xhprof'))
            {
                throw new ProfilerException('Unable to run profiler - php_xhprof.dll is not installed');
            }
            
            if (!is_writable(ini_get('xhprof.output_dir')))
            {
                throw new ProfilerException('Unable to run profiler - xhprof.output_dir is not correctly configured');
            }
            
            if (!file_exists(self::XHPROF_ROOT))
            {
                throw new ProfilerException('Unable to run profiler - xhprof UI files not found (run "tests/composer.phar install" to install)');
            }
        }
    }
    
    /**
     * Starts the profiler.
     */
    public function start()
    {
        if ($this->enabled)
        {
            xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
        }
    }
    
    /**
     * Stops the profiler.
     */
    public function stop()
    {
        if ($this->enabled)
        {
            $this->data = xhprof_disable();
        }
    }
    
    /**
     * Publishes the profile data.
     * 
     * @return string The script snippet to open the profile view.
     */
    public function publish()
    {
        if ($this->enabled && null !== $this->data)
        {
            require_once self::XHPROF_ROOT.'xhprof_lib/utils/xhprof_lib.php';
            require_once self::XHPROF_ROOT.'xhprof_lib/utils/xhprof_runs.php';
            
            $run_id = (new \XHProfRuns_Default)->save_run($this->data, $this->name);
            $profile_url = self::XHPROF_ROOT.'xhprof_html/index.php?run='.$run_id.'&source='.$this->name;
            
            return '<script type="text/javascript">window.open("'.$profile_url.'");</script>';
        }
    }
    
    /**
     * Convenience function that calls both Profiler::stop() and Profiler::publish().
     * 
     * @return string The URL to access the profile data.
     */
    public function stopAndPublish()
    {
        $this->stop();
        return $this->publish();
    }
}