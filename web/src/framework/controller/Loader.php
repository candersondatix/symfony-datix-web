<?php
namespace src\framework\controller;

/**
* Responsible for constructing and returning Controllers.
*/
class Loader
{
    /**
    * Constructs and returns the appropriate Controller object for this request.
    * 
    * @param array $controllerInfo The name of the Controller and associated filters (the method of providing this info will likely change as the routing system is improved)
    * 
    * @return $controller Controller
    */
    public function getController(array $controllerInfo)
    {
        $controller = new $controllerInfo['controller'](new Request(), new Response(new View()));
        $controller->setLoader($this);
        
        if (isset($controllerInfo['filters']))
        {
            // Instantiate each of the filters - filter tests are carried out in sequence before the controller action is run
            foreach ($controllerInfo['filters'] as $filter)
            {
                $controller = new $filter($controller);
            }
        }
        
        return $controller;
    }
}