<?php
namespace src\framework\controller;

use src\framework\controller\helper\FlashMessage;
use src\framework\events\EventQueue;
use src\framework\registry\Registry;

/**
* Base controller class.
*/
abstract class Controller implements ControllerInterface
{
    /**
    * The submitted request.
    * 
    * @var Request
    */
    protected $request;
    
    /**
    * The returned response.
    * 
    * @var Response
    */
    protected $response;
    
    /**
     * The event queue object.
     * 
     * The queue is processed at the end of the controller action.
     * 
     * @var EventQueue
     */
    protected $eventQueue;
    
    /**
     * The registry object used to access global config data.
     * 
     * @var Registry
     */
    protected $registry;
    
    /**
     * An instance of the Loader which can be used 
     * to call controllers from within controllers.
     * 
     * @var Loader
     */
    private $loader;

    /**
     * An instance of the AssetManager Class to store
     * and manage required assets
     * @var AssetManager
     */
    private $assets;


    public $flashmessage;

    /**
    * Constructor.
    * 
    * @param Request    $request
    * @param Response   $response
    * @param EventQueue $eventQueue
    * @param Registry   $registry
    */
    public function __construct(Request $request, Response $response, EventQueue $eventQueue = null, Registry $registry = null)
    {       
        $this->request    = $request;
        $this->response   = $response;
        $this->eventQueue = $eventQueue ?: EventQueue::getInstance();
        $this->registry   = $registry ?: Registry::getInstance();
        $this->assets     = new AssetManager();

        $this->flashmessage = new FlashMessage();
    }
    
    /**
    * Handles the request and returns the response object.
    * 
    * @param string $action The requested action.
    * 
    * @return Response $response The response object.
    */
    public function doAction($action, $outputAssets = false)
    {
        $this->callAction($action);
        $this->eventQueue->execute();

        $response = null;

        // Start of code to integrate css and js called in controllers into main template
        if($outputAssets === true)
        {
            $response = $this->assets->buildCss();
            $response .= $this->assets->outputVarsToJs();
            $response .= $this->assets->buildJs();
        }

        $response .= $this->response;

        return $response;
    }
    
    /**
    * Calls the controller action.
    * 
    * @param string $action The requested action.
    */
    protected function callAction($action)
    {
        call_user_func(array($this, $action));    
    }
    
    /**
     * Setter for loader.
     */
    public function setLoader(Loader $loader)
    {
        $this->loader = $loader;
    }
    
    /**
    * Redirects the request.
    * 
    * @param string $url The URL to redirect to
    */
    protected function redirect($url)
    {
        if (bYN(GetParm('CSRF_PREVENTION','N'))) {
            // add CSRF prevention token to url
            $query = parse_url($url, PHP_URL_QUERY);
            $url .= ($query ? '&' : '?').'token='.\CSRFGuard::getCurrentToken();
        }

        header('Location: ' . \Sanitize::SanitizeURL(str_replace(' ', '%20', $url)));
        obExit();
    }

    /**
     * Allows controllers to invoke an action on a different controller and make use of the response.
     *
     * @param string $controller The fully-qualified controller name
     * @param string $action     The controller action to call
     * @param array  $parameters Request parameters for the new controller, in the form $parameters['name'] = 'value'.
     *
     * @return Response The response object from the controller action.
     *
     * @throws \UndefinedLoaderException If the loader parameter has not been set for this controller instance.
     */
    protected function call($controller, $action, $parameters = array())
    {
        if (!isset($this->loader))
        {
            throw new \UndefinedLoaderException('The loader parameter needs to be set in order to call other controller methods.');
        }
        $controller = $this->loader->getController(array('controller' => $controller));

        if (!empty($parameters))
        {
            foreach ($parameters as $name => $value)
            {
                $controller->request->setParameter($name, $value);
            }
        }

        $output = $controller->doAction($action);

        /*
         * If the controller being called has assets, pass them back to the parent controller
         */
        $this->assets->setCss(array_merge($this->assets->getCss(), $controller->assets->getCss()));
        $controller->assets->unsetCss();

        $this->assets->setJs(array_merge($this->assets->getJs(), $controller->assets->getJs()));
        $controller->assets->unsetJs();

        $this->assets->setJsVars(array_merge($this->assets->getJsVars(), $controller->assets->getJsVars()));
        $controller->assets->unsetJsVars();

        return $output;
    }

    /**
     * Sets a parameter of the request after the controller has been instantiated
     * @param $parameter
     * @param $value
     */
    public function setRequestParameter($parameter, $value)
    {
        $this->request->setParameter($parameter, $value);
    }

    /**
     * Adds a CSS file to the assets list to be added when the template is rendered
     *
     * @param string $file either a string url to the file or an array of files
     * @param array $params any additional attributes to add to the css tag
     */
    public function addCss($file, $params = null)
    {
        $this->assets->addCss($file, $params);
    }

    /**
     * Adds a JS file to the assets list to be added when the template is rendered
     *
     * @param string $file either a string url to the file or an array of files
     * @param array $params any additioanl attributes to add to the js tag
     */
    public function addJs($file, $params = null)
    {
        $this->assets->addJs($file, $params);
    }

    /**
     * Function to send variables from PHP to JS so they can be used for client side processing
     *
     * @param string $var_name The variable name to be used in JS
     * @param string $value The value of the variable. This can be an array, int, string or boolean value which will be outputted correctly to JSON
     * @param bool $global Whether to prefix the variable with globals. to help avoid overwriting other global variables
     */
    public function sendToJs($var_name, $value = null, $global = true)
    {
        $this->assets->sendToJs($var_name, $value, $global);
    }

    public function buildCss()
    {
        return $this->assets->buildCss();
    }

    public function buildJs($sub_array = null)
    {
        return $this->assets->buildJs($sub_array);
    }

    public function outputVarsToJs()
    {
        return $this->assets->outputVarsToJs();
    }
    
    /**
     * Shortcut for $this-request->getParameter().
     * 
     * @param string $key The name of the parameter required.
     * 
     * @return mixed
     */
    public function get($key)
    {
        return $this->request->getParameter($key);
    }
}