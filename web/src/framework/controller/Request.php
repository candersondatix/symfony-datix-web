<?php
namespace src\framework\controller;

/**
* Used by controllers to access request parameters.
*/
class Request 
{
    /**
    * Holds the parameters sent with the request.
    * 
    * @var array
    */
    protected $parameters;
    
    /**
     * The request method (e.g. GET, POST)
     * 
     * @var string
     */
    protected $method;
    
    /**
    * Constuctor: sets request parameters depending on request type.
    */
    public function __construct() 
    {
        $this->parameters = array();
        
        // HTTP request
        if (isset($_SERVER['REQUEST_METHOD'])) 
        {
            $this->parameters = $_REQUEST;
            $this->method = $_SERVER['REQUEST_METHOD'];
            return;
        }
        
        // command line request
        foreach($_SERVER['argv'] as $arg) 
        {
            if (\UnicodeString::strpos($arg, '=')) 
            {
                list($key, $value) = explode("=", $arg);
                $this->setParameter($key, $value);
            }
        }
    }
    
    /**
    * Getter for request parameters.
    * 
    * @param string $key The name of the parameter required.
    * 
    * @return mixed
    */
    public function getParameter($key)
    {
        if (isset($this->parameters[$key]))
        {
            return $this->parameters[$key];
        }
        return null;
    }
    
    /**
    * Setter for request parameters.
    * 
    * @param string $key   The parameter name.
    * @param mixed  $value The parameter value.
    */
    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;
    }

    /**
     * Getter for all request parameters.
     *
     * @return mixed An array containing all request parameters
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Getter for method property.
     * 
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Unset a parameter.
     *
     * @param string $key The parameter name.
     */
    public function unSetParameter($key)
    {
        unset($this->parameters[$key]);
    }

    /**
     * Unset all parameters
     */
    public function unsetAllParameters()
    {
        foreach(array_keys($this->parameters) as $key)
        {
            $this->unSetParameter($key);
        }
    }
}