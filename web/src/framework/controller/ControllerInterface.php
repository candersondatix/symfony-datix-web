<?php
namespace src\framework\controller;

/**
* Implemented by controller classes (Controller/ConrtollerFilter)
*/
interface ControllerInterface
{
    public function doAction($action);
}