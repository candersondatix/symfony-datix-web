<?php
namespace src\framework\controller;


/**
* Base controller class.
*/
class AssetManager
{
    /**
     * Array of files to be loaded when the page loads
     */
    private $css = array();

    /**
     * Array of files to be loaded when the page loads
     */
    private $js = array();

    /**
     * @var array array of variables to be made available in js
     */
    private $jsVars = array();

    private $addMinExtension = null;
    
    /**
    * Constructor.
    * 
    * @param Request    $request
    * @param Response   $response
    * @param EventQueue $eventQueue
    * @param Registry   $registry
    */
    public function __construct()
    {       
        $this->addMinExtension = ($GLOBALS['MinifierDisabled'] ? null : '.min');
    }

    private function addFile($file, $params, $file_array)
    {
        $sub_array = $params['sub_array'] ?: null;
        $attrs = $params['attrs'] ?: null;
        $conditions = isset($params['conditions']) ? $params['conditions'] : true;

        /**
         * If the file is an array then add each item to the asset array
         */
        if(is_array($file))
        {
            foreach($file as $single_file)
            {
                /**
                 * If the single file is still an array then use it's file and attr options
                 */
                if(is_array($single_file))
                {
                    $file_name = $single_file['file'];
                    $file_params['attrs'] = $single_file['attrs'] ?: $attrs;
                    $file_params['conditions'] = isset($single_file['conditions']) ? $single_file['conditions'] : $conditions;
                    $file_params['sub_array'] = $single_file['sub_array'] ?: $sub_array;
                }
                else
                {
                    $file_name = $single_file;
                    $file_params['attrs'] = $attrs;
                    $file_params['conditions'] = $conditions;
                    $file_params['sub_array'] = $sub_array;

                }

                $file_array = $this->addFile($file_name, $file_params, $file_array);
            }
        }

        if($conditions && is_file($file) && ! $this->fileExists($file, $file_array))
        {
            if(isset($sub_array))
            {
                $file_array[$sub_array][] = array('file' => $file, 'attrs' => $attrs);
            }
            else
            {
                $file_array[] = array('file' => $file, 'attrs' => $attrs);
            }
        }
        elseif($conditions && preg_match('#^(<!--\[if [^\]]+\]>\s*)?<script.*?>.*?<\/script>(\s*<!\[endif\]-->)?$#is', $file))
        {
            if(isset($sub_array))
            {
                $file_array[$sub_array][] = array('content' => $file);
            }
            else
            {
                $file_array[] = array('content' => $file);
            }
        }

        return $file_array;
    }

    public function getCss()
    {
        return $this->css;
    }

    public function setCss($value)
    {
        $this->css = $value;
    }

    public function unsetCss()
    {
        $this->css = array();
    }

    /**
     * Adds a CSS file to the assets list to be added when the template is rendered
     *
     * @param string $file either a string url to the file or an array of files
     * @param array $params any additional attributes to add to the css tag
     */
    public function addCss($file, $params = null)
    {
        $this->css = $this->addFile($file, $params, $this->css);
    }

    public function getJs()
    {
        return $this->js;
    }

    public function setJs($value)
    {
        $this->js = $value;
    }

    public function unsetJs()
    {
        $this->js = array();
    }

    public function getJsVars()
    {
        return $this->jsVars;
    }

    public function setJsVars($value)
    {
        $this->jsVars = $value;
    }

    public function unsetJsVars()
    {
        $this->jsVars = array();
    }

    /**
     * Adds a JS file to the assets list to be added when the template is rendered
     *
     * @param $file either a string url to the file or an array of files
     * @param null $attrs any additioanl attributes to add to the js tag
     */
    public function addJs($file, $params = null)
    {
        $this->js = $this->addFile($file, $params, $this->js);
    }

    private function fileExists($file, $array)
    {
        /**
         * If the file is a minified version, remove .min and check if the file exists, if it does add it instead
         */
        if(preg_match('#\.min\.[a-z0-9]+$#i', $file))
        {
            $unmin_file = preg_replace('#\.min(\.[a-z0-9]+)$#i', '$1', $file);

            if(is_file($unmin_file))
            {
                $file = $unmin_file;
            }
        }

        $file_array = array();

        foreach($array as $key => $value)
        {
            if(is_int($key))
            {
                $file_array[] = $value['file'];
            }
            else
            {
                foreach($value as $key2 => $value2)
                {
                    $file_array[] = $value2['file'];
                }
            }
        }

        return in_array($file, $file_array);
    }

    private function buildFileList($file_array)
    {
        if( ! empty($file_array))
        {
            $output = null;

            if( ! empty($file_array['first']))
            {
                $build_first = $this->buildFileList($file_array['first']);

                unset($file_array['first']);

                $output .= $build_first['output'];
            }

            foreach($file_array as $key => $file)
            {
                /**
                 * Check that the key is an integer to ensure than sub arrays, such as first and bottom aren't processed
                 */
                if(is_int($key))
                {
                    /**
                     * If the system requires .min to be added, then do it here and check to see if the minified version of the file already exists in the file list
                     */
                    if(isset($this->addMinExtension))
                    {
                        $min_file = preg_replace('#(?<!' . preg_quote($this->addMinExtension) . ')(\.[a-z0-9]+)$#i', $this->addMinExtension . '$1', $file['file']);

                        /*if($min_file !== $file['file'] && $this->fileExists($min_file, $file_array))
                        {
                            $file['file'] = null;
                        }
                        else*/if(is_file($min_file))
                        {
                            $file['file'] = $min_file;
                        }
                    }
                    /**
                     * If the file is a .min version, check to see if it already exists in the file list without .min and remove it if required
                     */
                    elseif(preg_match('#\.min\.[a-z0-9]+$#i', $file['file']) && $this->fileExists(preg_replace('#\.min(\.[a-z0-9]+)$#i', '$1', $file['file']), $file_array))
                    {
                        $file['file'] = null;
                    }

                    if(is_file($file['file']))
                    {
                        /**
                         * If the file is valid then add it using it's appropriate tag type. the ?v=#### is a cache busting method
                         */
                        if(preg_match('/\.js$/isu', $file['file']))
                        {
                            /**
                             * Use $GLOBALS['dtxdebug'] to know whether to set the cachebuster or not for easier js debugging
                             */
                            $output .= '<script type="text/javascript" src="' . $file['file'] . ( ! $GLOBALS['dtxdebug'] ? '?v=' . filemtime($file['file']) : '') . '"></script>' . "\r\n";
                        }
                        elseif(preg_match('/\.css$/isu', $file['file']))
                        {
                            /**
                             * Use $GLOBALS['dtxdebug'] to know whether to set the cachebuster or not for easier js debugging
                             */
                            $output .= '<link rel="stylesheet" type="text/css" href="' . $file['file'] . ( ! $GLOBALS['dtxdebug'] ? '?v=' . filemtime($file['file']) : '') . '"' . ($file['attrs'] ? ' ' . trim($file['attrs']) : ' media="screen"') . ' />' . "\r\n";
                        }
                    }
                    elseif(preg_match('#^(<!--\[if [^\]]+\]>\s*)?<script.*?>.*?<\/script>(\s*<!\[endif\]-->)?$#is', $file['content']))
                    {
                        $output .= $file['content'] . "\r\n";;
                    }

                    unset($file_array[$key]);
                }
            }

            // unset the array as we're done with it and we don't want to duplicate output if this is used later on

            return array(
                'array' => $file_array,
                'output' => $output
            );
        }
        else
        {
            return false;
        }
    }

    public function buildCss()
    {
        $buildCss = $this->buildFileList($this->css);

        $this->css = $buildCss['array'];

        return $buildCss['output'];
    }

    public function buildJs($sub_array = null)
    {
        $buildJs = $this->buildFileList($sub_array ? $this->js[$sub_array] : $this->js);

        if($sub_array)
        {
            $this->js[$sub_array] = $buildJs['array'];
        }
        else
        {
            $this->js = $buildJs['array'];
        }

        return $buildJs['output'];
    }

    /**
     * Function to send variables from PHP to JS so they can be used for client side processing
     * can be used as sendToJs('string', 'string', bool) or sendToJs(array, bool) with the second param being $appendGlobals
     *
     * @param string $var_name The variable name to be used in JS
     * @param string $value The value of the variable. This can be an array, int, string or boolean value which will be outputted correctly to JSON
     * @param bool $global turn on or off adding globals. before each variable sent to avoid any clashing with local variables
     */
    public function sendToJs($var_name, $value = null, $appendGlobals = true)
    {
        if(is_array($var_name) && ($value === null || is_bool($value)))
        {
            if(is_bool($value))
            {
                $appendGlobals = $value;
            }

            foreach($var_name as $var => $val)
            {
                $this->jsVars[($appendGlobals ? 'globals.' : '' ) . $var] = $val;
            }
        }
        else
        {
            $this->jsVars[($appendGlobals ? 'globals.' : '' ) . $var_name] = $value;
        }
    }

    /**
     * function to format and output PHP variables to be used within JS. The PHP variable values will be properly formatted
     * for JS before being outputted to the page
     * @return bool|string
     */
    public function outputVarsToJs()
    {
        if( ! empty($this->jsVars))
        {
            $output = '<script type="text/javascript">' . "\r\n";

            foreach($this->jsVars as $var_name => $value)
            {
                $output .=  (preg_match('/[\.\[]/isu', $var_name) ? '' : 'var ' ) . $var_name . ' = ' . json_encode($value, JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT) . ';' . "\r\n";
            }

            $output .= '</script>';

            return $output;
        }
        return false;
    }
}