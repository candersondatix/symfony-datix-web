<?php echo GetDivFieldHTML('Name:', $this->FormFieldTemNotes->GetField()); ?>
<?php echo GetDivFieldHTML('Template type:', $this->FieldTemType->getField()); ?>
<?php if ($this->rep['path'] != '') : ?>
<?php echo GetDivFieldHTML('Template file:', $this->FormFieldPath->GetField(), '', '', 'rep_file_row'); ?>
<?php echo GetDivFieldHTML('Import new template file:', $this->FormFieldUserFile->GetField(), '', '', 'userfile_row'); ?>
<?php else : ?>
<?php echo GetDivFieldHTML('Template file to import:', $this->FormFieldUserFile->GetField(), '', '', 'userfile_row'); ?>
<?php endif; ?>