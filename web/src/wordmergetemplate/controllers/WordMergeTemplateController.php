<?php
namespace src\wordmergetemplate\controllers;

use src\framework\controller\Controller;
use src\framework\controller\Request;
use src\framework\controller\Response;
use src\wordmergetemplate\model\WordMergeTemplateModelFactory;
use src\wordmergetemplate\helper\WordMerger;
use src\documents\model\DocumentModelFactory;

/**
 * Controls the process of merging document templates and records
 */
class WordMergeTemplateController extends Controller
{
    /**
     * Called from BasicForm files to display a form section allowing users to merge
     * the record with an MS Word template.
     */
    public function wordmergesection()
    {
        $module = $this->request->getParameter('module');
        $data = $this->request->getParameter('data');
        $FormType = 'Edit';

        ///////////////////////////////////////////////////////////////////////////////
        // document templates are not accessible to anyone by default, the administrator
        // has to give access to users and groups by the record level security panel
        // called 'permissions' in Document template administration.
        ///////////////////////////////////////////////////////////////////////////////
        $DocTemplatesWhereClause = MakeSecurityWhereClause("(tm.tem_module = :module OR tm.tem_module = '' OR tm.tem_module is null)", 'TEM', $_SESSION["initials"], 'templates_main', "", "", true, $module);
        $DocTemplatesWhereClause = str_replace('templates_main.', 'tm.', $DocTemplatesWhereClause);

        $sql = 'SELECT tm.recordid, tm.tem_notes FROM templates_main tm WHERE '.$DocTemplatesWhereClause.' ORDER BY tm.tem_notes';

        $doctem_array = \DatixDBQuery::PDO_fetch_all($sql, array('module' => $module), \PDO::FETCH_KEY_PAIR);

        $Table = new \FormTable($FormType, $module);
        $FieldObj = new \FormField($FormType);

        $field = \Forms_SelectFieldFactory::createSelectField('select_template', '', '', $FormType);
        $field->setCustomCodes($doctem_array);
        $field->setSuppressCodeDisplay();
        $field->setSuppressFieldChangedFlag();
        $Table->MakeRow("<b>Word template</b><br />Choose a document template", $field);

        $CustomField = '<input type="button" value="'._tk('btn_merge_in_msword').'"
            onclick="MergeWordDocument(\''.$data['recordid'].'\', \''.$module.'\')" />';
        $Table->MakeTitleRow($CustomField, "windowbg2");

        $Table->MakeTable();

        $this->response->build('src/wordmergetemplate/views/WordMergeSection.php', array(
            'recordid' => $data['recordid'],
            'module' => $module,
            'Table' => $Table,
        ));
    }

    /**
     * Merges a document template and a module record, saving the resulting document as a linked document
     * against the record and then redirecting the user to the record.
     *
     * @param int select_template The id of the document template to use in the merge.
     * @param int recordid The id of the record to use in the merge. Document templates belong to a particular
     * module, so we do not need to pass the module separately.
     *
     * @throws MissingParameterException if either parameter is missing.
     **/
    public function mergedocument()
    {
        $docID = $this->request->getParameter('select_template');
        if($docID == '')
        {
            throw new \MissingParameterException('No document template provided.');
        }

        $recordid = $this->request->getParameter('recordid');
        if($recordid == '')
        {
            throw new \MissingParameterException('No recordid provided.');
        }

        //Build a WordMergeTemplate object to manipulate
        $Factory = new WordMergeTemplateModelFactory();
        $WordMergeTemplate = $Factory->getMapper()->find($docID);

        //Use the WordMerger helper class to merge the template and the data, returning an instance of src/documents/model/Document
        $WordMerger = new WordMerger($WordMergeTemplate, $recordid, $this->request->getParameter('selected_contact'));
        $WordMerger->merge();

        //Redirect the user back to the record and display the documents section.
        AddSessionMessage('INFO', 'Document added.');
        $RecordURL = getRecordURL(array('module' => $WordMergeTemplate->tem_module, 'recordid' => $recordid));
        $this->redirect($RecordURL.'&panel=documents');
    }

    /**
     * Calculates whether the user needs to be prompted to choose a contact for word merging and
     * returns the set of possible contacts as a JSON array if so.
     *
     * using:
     * int recordid The id of the main record
     * string module Module code
     */
    public function getContactChoiceArray()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $recordid = $this->request->getParameter('recordid');
        $module = $this->request->getParameter('module');

        $Factory = new WordMergeTemplateModelFactory;
        $WordMergeTemplate = $Factory->getMapper()->find($this->request->getParameter('template_id'));

        $ContactMergeCodes = array();
        foreach($WordMergeTemplate->getMergeCodes() as $MergeCode)
        {
            $FieldRef[$MergeCode]['FULL'] = $MergeCode;

            //This merge code will be of the form FIELDNAME [ATTRIBUTE1:VALUE1] [ATTRIBUTE2:VALUE2]...
            $SplitField = explode(' ', $MergeCode);

            $FieldRef[$MergeCode]['DB'] = \UnicodeString::trim(array_shift($SplitField));
            try
            {
                $FieldRef[$MergeCode]['OBJ'] = new \Fields_Field($FieldRef[$MergeCode]['DB']);

                $dbFields[] = $FieldRef[$MergeCode]['DB'];

                foreach($SplitField as $attributevalue)
                {
                    list($attribute, $value) = explode(':', $attributevalue);
                    $FieldRef[$MergeCode]['DETAILS'][\UnicodeString::strtoupper($attribute)] = $value;
                }

                if(in_array($FieldRef[$MergeCode]['OBJ']->getTable(), array('link_contacts','contacts_main','staff','link_compl')) && $module != 'CON')
                {
                    $ContactMergeCodes[] = $MergeCode;
                }
            }
            catch(\Exception $e)
            {
                //Field is not recognised. Ignore it for now.
                unset($FieldRef[$MergeCode]);
            }
        }

        //if there are no contact merge codes on the document, we don't need to link any contacts
        if(empty($ContactMergeCodes))
        {
            echo json_encode(array());
            exit;
        }

        $LinkedContacts = \DatixDBQuery::PDO_fetch_all(
                'SELECT con_id, link_role, con_surname, con_forenames from link_contacts
                LEFT JOIN contacts_main ON contacts_main.recordid = link_contacts.con_id
                WHERE '.$ModuleDefs[$WordMergeTemplate->getModule()]['FK'].' = :recordid', array('recordid' => $recordid));

        foreach($LinkedContacts as $LinkedContact)
        {
            $LinkedContactsByRole[$LinkedContact['link_role']][$LinkedContact['con_id']] = '['.code_descr('INC', 'link_role', $LinkedContact['link_role']).'] '.$LinkedContact['con_surname'].', '.$LinkedContact['con_forenames'];
            $LinkedContactsByRole['NO_LINK_ROLE'][$LinkedContact['con_id']] = '['.code_descr('INC', 'link_role', $LinkedContact['link_role']).'] '.$LinkedContact['con_surname'].', '.$LinkedContact['con_forenames'];
        }

        $ContactChoices = array();

        if(count($LinkedContacts) > 0)
        {
            foreach($ContactMergeCodes as $FullMergeCode)
            {
                if(!empty($FieldRef[$FullMergeCode]['DETAILS']['ROLE']))
                {
                    if (!$FieldRef[$FullMergeCode]['DETAILS']['CONTACTNUM'] && count($LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']]) > 1)
                    {
                        //A role is specified, but not a contact number, and there is more than one contact for this role.
                        $ContactChoices[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']] = $LinkedContactsByRole[$FieldRef[$FullMergeCode]['DETAILS']['ROLE']];
                    }
                }
                else if(count($LinkedContacts) > 1)
                {
                    //No role specified, and more than one possible contact
                    $ContactChoices['NO_LINK_ROLE'] = $LinkedContactsByRole['NO_LINK_ROLE'];
                }
            }
        }

        echo json_encode($ContactChoices);
    }
}