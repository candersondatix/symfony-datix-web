<?php
namespace src\wordmergetemplate\model;

use src\framework\model\ModelFactory;

class WordMergeTemplateModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new WordMergeTemplateFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new WordMergeTemplateMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new WordMergeTemplateCollection($this->getMapper(), $this->getEntityFactory());
    }
}