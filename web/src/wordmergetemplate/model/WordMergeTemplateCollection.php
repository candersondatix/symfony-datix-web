<?php
namespace src\wordmergetemplate\model;

use src\framework\model\EntityCollection;

class WordMergeTemplateCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\wordmergetemplate\\model\\WordMergeTemplate';
    }
}