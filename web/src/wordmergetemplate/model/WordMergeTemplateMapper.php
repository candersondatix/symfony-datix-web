<?php
namespace src\wordmergetemplate\model;

use src\framework\model\Mapper;
use src\framework\model\Entity;

/**
 * Manages the persistance of WordMergeTemplate objects.
 */
class WordMergeTemplateMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\wordmergetemplate\\model\\WordMergeTemplate';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'templates_main';
    }

    /**
     * Convenience function which inserts/updates the entity, depending on
     * whether or not it already exists in the database.
     *
     * Overriden here becuase you can't insert a linked document without a
     * physical document to link to, so this must be provided on call
     *
     * @param Entity $object
     *
     * @return boolean Whether or not the operation was successful.
     *
     * @throws \InvalidArgumentException When the object is of an incorrect type for this mapper.
     */
    public function save(Entity $object, $fileLocation, $fileExtension)
    {
        if (!$this->isObjectValid($object))
        {
            throw $this->invalidObjectException($object);
        }

        if ($object->recordid)
        {
            return $this->update($object);
        }
        else
        {
            return $this->insert($object, $fileLocation, $fileExtension);
        }
    }

    /**
     * @param Entity $object
     * @param $document
     *
     * Can't insert a linked document without a physical document to link to, so
     * this must be provided on call
     */
    public function insert(Entity $object, $fileLocation, $fileExtension)
    {
        $object->savePhysicalDocument($fileLocation, $fileExtension);

        parent::insert($object);
    }

    /**
     * {@inheritdoc}
     *
     * @param $document File location of the uploaded document being referenced by this entity.
     *
     * Sets various audit values before insert.
     */
    protected function doInsert(Entity $object)
    {
        $object->doc_dcreated = date('Y-m-d H:i:s.000');
        $object->doc_createdby  = $_SESSION['initials'];
        $this->updateEntity($object);
        return parent::doInsert($object);
    }

    /**
     * {@inheritdoc}
     *
     * Sets doc_updatedby, doc_dupdated, and updateid properties before update.
     */
    protected function doUpdate(Entity $object)
    {
        $this->updateEntity($object);
        $object->updateid = $this->generateUpdateId();
        return parent::doUpdate($object);
    }

    /**
     * When deleting a document, we need to also delete the physical document and doc_documents record.
     *
     * @param Entity $object
     *
     * @return boolean Whether or not the operation was successful.
     */
    protected function doDelete(Entity $object)
    {
        $fileName = $object->GetPath();

        if(file_exists($fileName) && !unlink($fileName))
        {
            throw new \Exception('Could not delete physical document.');
        }

        \DatixDBQuery::PDO_query('DELETE FROM doc_documents WHERE recordid = :doc_id', array('doc_id' => $object->doc_id));

        return parent::doDelete($object);
    }


    /**
     * Sets the doc_updatedby and doc_dupdated properties.
     *
     * @param Entity $object
     */
    protected function updateEntity(Entity $object)
    {
        $object->doc_updatedby = $_SESSION['initials'];
        $object->doc_dupdated  = date('Y-m-d H:i:s.000');
    }

    /**
     * Generates a new two-character updateid based on the existing one (if any) for this record.
     * Document entity needs its own version of this because it is not a DatixEntity descendant.
     *
     * NB - copy of GensUpdateID() in Subs.php
     *
     * @return string The new updateid.
     */
    protected function generateUpdateId()
    {
        if ($this->updateid == '')
        {
            $char1 = 0;
            $char2 = 0;
        }
        else
        {
            $char1 = \UnicodeString::ord($this->updateid{0});
            $char2 = \UnicodeString::ord($this->updateid{1});
        }

        $char2 = $this->incrementChar($char2);

        if ($char2 == 48)
        {
            $char1 = $this->incrementChar($char1);
        }

        if ($char1 == 0)
        {
            $this->updateid = \UnicodeString::chr($char2);
        }
        else
        {
            $this->updateid = \UnicodeString::chr($char1) . \UnicodeString::chr($char2);
        }

        return $this->updateid;
    }

    /**
     * Increments an ASCII value within a defined range, used when generating updateids.
     * Document entity needs its own version of this because it is not a DatixEntity descendant.
     *
     * NB - copy of IncUpdateIDChar() in Subs.php
     *
     * @param int $val The ASCII value of the character we're incrementing.
     *
     * @return int $val
     */
    protected function incrementChar($val)
    {
        $val++;
        if ($val < 48)
        {
            $val = 49;
        }
        elseif ($val > 57 && $val < 65)
        {
            $val = 65;
        }
        elseif ($val > 90 && $val < 97)
        {
            $val = 97;
        }
        elseif ($val > 122)
        {
            $val = 48;
        }
        return $val;
    }

}