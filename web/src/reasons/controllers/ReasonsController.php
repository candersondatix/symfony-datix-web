<?php

namespace src\reasons\controllers;

use src\framework\controller\Controller;

class ReasonsController extends Controller
{
    /**
     * Displays the section Reasons for Rejection - History
     */
    function SectionRejectionHistory()
    {
        global $ModuleDefs;

        $data           = $this->request->getParameter('data');
        $module         = $this->request->getParameter('module');
        $HistoricalRows = array();

        $sql = '
            SELECT
                '.$ModuleDefs[$module]['TABLE'].'.rep_approved, aud_reasons.recordid, rea_code, rea_text,
                initials as inc_mgr, rea_dlogged, con_title, con_forenames, con_surname
            FROM
                aud_reasons LEFT JOIN
                contacts_main ON aud_reasons.con_id=contacts_main.recordid LEFT JOIN
                '.$ModuleDefs[$module]['TABLE'].' ON '.$ModuleDefs[$module]['TABLE'].'.recordid = aud_reasons.link_id
            WHERE
                aud_reasons.mod_id= :module AND
                aud_reasons.link_id=:recordid AND
                aud_reasons.rea_type=\'REJ\'
            ORDER BY
                rea_dlogged DESC
        ';

        $RejectionRows = \DatixDBQuery::PDO_fetch_all($sql, array(
            'recordid' => $data['recordid'],
            'module'   => $module
        ));

        foreach ($RejectionRows as $RejectionRow)
        {
            if ($RejectionRow['rep_approved'] == 'REJECT' && !isset($CurrentRejectionRow))
            {
                $CurrentRejectionRow = $RejectionRow;
            }
            else
            {
                $HistoricalRows[] = $RejectionRow;
            }
        }

        require_once 'Source/libs/ListingClass.php';
        $Listing = new \Listing('INC');

        $Listing->LoadColumnsFromArray(
            array(
                'rea_dlogged' => array('width' => '10', 'type' => 'date'),
                'inc_mgr'     => array('width' => '10', 'title' => 'Rejected by'),
                'rea_code'    => array('width' => '10'),
                'rea_text'    => array('width' => '50'),
            )
        );

        $Listing->LoadData($HistoricalRows);
        $Listing->ReadOnly = true;

        $this->response->build('src/reasons/views/DisplayRejectionHistory.php', array(
            'Listing' => $Listing
        ));
    }

    /**
     * Saves a rejection reason
     */
    public function SaveReason()
    {
        $module  = $this->request->getParameter('module');
        $link_id = $this->request->getParameter('link_id');
        $data    = $this->request->getParameter('data');

        $data['rea_type'] = 'REJ';

        $sql = '
                SELECT
                    TOP 1 recordid
                FROM
                    AUD_REASONS
                WHERE
                    mod_id = :module AND
                    link_id = :link_id
                ORDER BY
                    recordid DESC
            ';

        $ReasonID = \DatixDBQuery::PDO_fetch($sql, array('module' => $module, 'link_id' => $link_id), \PDO::FETCH_COLUMN);

        if ($ReasonID == '' || $data['rep_approved_old'] != 'REJECT') //this is a new reason for rejection
        {
            $PDOParamsArray['rea_code']    = $data['rea_code'];
            $PDOParamsArray['rea_text']    = $data['rea_text'];
            $PDOParamsArray['module']      = $module;
            $PDOParamsArray['link_id']     = $link_id;
            $PDOParamsArray['con_id']      = $_SESSION['contact_login_id'];
            $PDOParamsArray['rea_type']    = $data['rea_type'];
            $PDOParamsArray['rea_dlogged'] = date('d-M-Y H:i:s');

            $sql = '
                INSERT INTO
                    AUD_REASONS
                    (mod_id, link_id, rea_code, rea_text, con_id, rea_type, rea_dlogged)
                VALUES
                    (:module, :link_id, :rea_code, :rea_text, :con_id, :rea_type, :rea_dlogged)
            ';

            \DatixDBQuery::PDO_query($sql, $PDOParamsArray);
        }
        else
        {
            $sql = '
                UPDATE
                    AUD_REASONS
                SET
                    rea_code = :rea_code, rea_text = :rea_text
                WHERE
                    recordid = :recordid
            ';

            \DatixDBQuery::PDO_query($sql, array(
                'recordid' => $ReasonID,
                'rea_code' => $data['rea_code'],
                'rea_text' => $data['rea_text']
            ));
        }
    }

    /**
     * Functions that gets the rejection reasons and adds it ot the record's data array.
     *
     * @param string $module   The record's module
     * @param string $recordid The record's id
     * @param array  $data     The record's data
     * @param string $formType The type of the form
     * @param string $type     The type of rejection
     *
     * @return array %data     The record's data with the rejection reasons added.
     */
    static public function GetReasonsData($module, $recordid, $data, $formType, $type = 'REJ')
    {
        $PDOParamsArray['module'] = $module;
        $PDOParamsArray['recordid'] = $recordid;
        $PDOParamsArray['rea_type'] = $type;

        $sql = '
            SELECT
                TOP 1 aud_reasons.recordid, rea_code, rea_text, con_id, rea_dlogged, con_title, con_forenames, con_surname
            FROM
                aud_reasons LEFT JOIN
                contacts_main ON aud_reasons.con_id = contacts_main.recordid
            WHERE
                aud_reasons.mod_id = :module AND
                aud_reasons.link_id = :recordid AND
                aud_reasons.rea_type = :rea_type
            ORDER BY
                rea_dlogged DESC
        ';

        $CurrentRejectionRow = \DatixDBQuery::PDO_fetch($sql, $PDOParamsArray);

        if ($CurrentRejectionRow['con_id'] == '')
        {
            $data['rea_con_name'] = code_descr('INC', 'inc_mgr', $data['updatedby']);
        }
        elseif ($CurrentRejectionRow['con_title'] || $CurrentRejectionRow['con_forenames'] || $CurrentRejectionRow['con_surname'])
        {
            $data['rea_con_name'] = $CurrentRejectionRow['con_title'].' '.$CurrentRejectionRow['con_forenames'].' '.$CurrentRejectionRow['con_surname'];
        }

        // Conditions under which form is editable
        if (($_SESSION['contact_login_id'] == $CurrentRejectionRow['con_id']
                || $data['con_id'] == null // contact no longer exists
                || ($CurrentRejectionRow['con_id'] == '' && ($_SESSION['initials'] == $data['updatedby'] || $data['updatedby'] == '')))
            && $formType != 'Print')
        {
            $data['ReasonSectionEditable'] = true;
        }
        else
        {
            $data['ReasonSectionEditable'] = false;
        }

        if ($CurrentRejectionRow['rea_dlogged'] == '')
        {
            $data['rea_dlogged'] = $data['updateddate'];
        }
        else
        {
            $data['rea_dlogged'] = $CurrentRejectionRow['rea_dlogged'];
        }

        $data['rea_code'] = $CurrentRejectionRow['rea_code'];
        $data['rea_text'] = $CurrentRejectionRow['rea_text'];

        return $data;
    }
}