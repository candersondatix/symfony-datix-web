<?php
namespace src\passwords\exceptions;

class InvalidPasswordEncryptionTypeException extends \Exception {}