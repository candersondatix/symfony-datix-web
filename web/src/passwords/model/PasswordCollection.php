<?php
namespace src\passwords\model;

use src\framework\model\EntityCollection;

class PasswordCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\passwords\\model\\Password';
    }
}