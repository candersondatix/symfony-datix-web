<?php
namespace src\passwords\model;

use src\framework\model\DatixEntityMapper;
use src\framework\model\Entity;
use src\framework\model\Mapper;
use src\passwords\exceptions\InvalidPasswordException;
use src\security\PasswordHasher;

/**
 * Manages the persistance of password objects.
 */
class PasswordMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\passwords\\model\\Password';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'passwords';
    }

    /**
     * {@inheritdoc}
     */
    protected function getFieldMappings()
    {
        return array (
                'login'           => 'pwd_login',
                'password'        => 'pwd_password',
                'expire_date'     => 'pwd_dexpires',
                'current'         => 'pwd_current',
                'encryption_type' => 'pwd_encryption'
            );
    }

    /**
     * Fetch current password for $login
     * @param $login
     * @return array
     */
    public function getCurrentPassword ($login)
    {
        if ($this->registry->getParm('PWD_SHARED', 'N', true, true)) {
            $params = ['login' => $login];
        } else {
            $params = ['login' => $this->getWebUserLogin($login)];
        }

        $sql = "SELECT pwd_login, pwd_password, pwd_dexpires, pwd_current, pwd_encryption FROM passwords WHERE pwd_login LIKE :login AND pwd_current = 'Y'";
        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($params);
        $result = $this->db->PDOStatement->fetch(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Deactivates the given user current passwords (so a new one can be created and take its place)
     * @param $login
     */
    public function deactivateUserCurrentPassword ($login)
    {
        if ($this->registry->getParm('PWD_SHARED', 'N', false, true))
        {
            $sql = "UPDATE passwords SET pwd_current = 'N' WHERE pwd_login LIKE :rc_login or pwd_login LIKE :dw_login";
            $params = [
                'rc_login' => $login,                          // richclient login
                'dw_login' => $this->getWebUserLogin($login)   // datixweb login
            ];
        }
        else
        {
            $sql = "UPDATE passwords SET pwd_current = 'N' WHERE pwd_login LIKE :dw_login";
            $params = [
                'dw_login' => $this->getWebUserLogin($login)   // datixweb login
            ];
        }

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($params);
    }

    /**
     * Get a datixweb user login for the given login
     * @param $login
     * @return string datixweb login
     */
    private function getWebUserLogin ($login)
    {
        // datixweb logins are prefixed with underscore
        return '_'.$login;
    }

    /**
     * Set new password for user
     * @param Password $password
     * @throws \src\passwords\exceptions\InvalidPasswordException
     */
    public function setNewPassword (Password $password)
    {
        if (empty($password->password)) {
            throw new InvalidPasswordException();
        }

        $this->deactivateUserCurrentPassword ($password->login);

        // encryption type
        if (empty($password->encryption_type)) {
            $password->encryption_type = $this->registry->getEncryptionType ();
        }

        // if NOT shared passwords
        if (!$this->registry->getParm('PWD_SHARED', 'N', false, true)) {
            $password->login = $this->getWebUserLogin($password->login);
        }

        $password->current = 'Y';

        $this->insert($password);
    }

    /**
     * Insert new password
     * @param \src\passwords\model\Password $object
     * @return void
     */
    public function doInsert(Entity $object)
    {
        $fieldNames = [];
        $values     = [];

        foreach ($this->getFieldMappings() as $property => $field)
        {
            $fieldNames[]   = $field;
            $values[$field] = $object->$property;
        }

        $sql = "INSERT INTO passwords (".join(',', $fieldNames).") VALUES (:".join(', :', $fieldNames).")";

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute($values);
    }

    /**
     * Check if password should be rehashed
     * @param Password $object
     * @return bool
     */
    public function shouldPasswordBeRehashed (Password $object)
    {
        $currentPassword      = $this->getCurrentPassword ($object->login);
        $systemEncryptionType = $this->registry->getEncryptionType();
        $sharedPasswords      = $this->registry->getParm('PWD_SHARED', 'N', false, true);

        // password and system encryption types don't match
        if ($currentPassword['pwd_encryption'] != $systemEncryptionType)
        {
            // cases that need re-hashing
            $movedFromOtherToBcrypt  = ($systemEncryptionType == 'BCRYPT' && $currentPassword['pwd_encryption'] != 'BCRYPT');
            $movedFromBcryptToShared = ($sharedPasswords && $currentPassword['pwd_encryption'] == 'BCRYPT');

            if ($movedFromBcryptToShared OR $movedFromOtherToBcrypt)
            {
                return true;
            }
        }

        return false;
    }

    /** 
     * Checks if new password is unique
     * @param string $login user login
     * @param string $password unencrypted
     * @return bool
     */
    public function isNewPasswordUnique ($login, $password)
    {
        $hasToBeUnique = $this->registry->getParm('PWD_UNIQUE', 'N', true, true);

        // If PWD_UNIQUE is set to Y, the password has to be different from all previous
        // passwords. If not, it only has to be different from the current password.

        if ($hasToBeUnique) {
            $password_list = $this->getPasswords ($login);
        } else {
            $password_list = array($this->getCurrentPassword ($login));
        }

        // create a list with the hashed passwords
        $hash_list = [];
        foreach ($password_list as $item) {
            $hash_list[] = $item['pwd_password'];
        }

        // Look for the password in the list
        $hasher = $this->getPasswordHasher ($this->registry->getEncryptionType());
        $isPresent = $hasher->isPasswordInList ($hash_list, $password);

        return (!$isPresent);
    }

    /** 
     * Get a PasswordHasher instance. Exists only to make code more testable.
     * @param string $encryption_type 
     * @return PasswordHasher
     */
    public function getPasswordHasher ($encryption_type)
    {
        return new PasswordHasher ($encryption_type);
    }

    /** 
     * Gets list of user passwords, current and old (for the current encryption type)
     * @param string $login user login
     * @return array 
     */
    public function getPasswords ($login)
    {
        $numberOfUniquePasswords = $this->registry->getParm('PWD_UNIQUE_LAST', '', true);

        $limit = $numberOfUniquePasswords ? 'TOP '.$numberOfUniquePasswords : '';
        $sql = "SELECT {$limit} pwd_password FROM passwords WHERE pwd_login LIKE :login AND pwd_encryption = :encryption_type ORDER BY pwd_dexpires DESC";

        if (!$this->registry->getParm('PWD_SHARED', 'N', true, true)) {
            $login = $this->getWebUserLogin($login);
        }

        $this->db->setSQL($sql);
        $this->db->prepareAndExecute([
            'login' => $login, 
            'encryption_type' => $this->registry->getEncryptionType()
        ]);

        return $this->db->PDOStatement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Whether a user has a current password or not (this might happen when changing PWD_SHARED)
     * @param $login username
     * @return bool
     */
    public function userHasPassword ($login)
    {
        if (!$this->registry->getParm('PWD_SHARED', 'N', true, true))
        {
            $login = $this->getWebUserLogin($login);
        }

        //We cannot match the RC "E" hash, so in the event that we have moved to shared passwords, users will need to reset their pwd.
        $this->db->setSQL("SELECT COUNT(*) FROM passwords WHERE pwd_login LIKE :login AND pwd_current = 'Y' AND pwd_encryption != 'E'");
        $this->db->prepareAndExecute(['login' => $login]);

        $count = $this->db->PDOStatement->fetch(\PDO::FETCH_COLUMN, 0);
        return ($count > 0);
    }
}

