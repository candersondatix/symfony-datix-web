<?php
namespace src\passwords\model;

use src\framework\model\ModelFactory;

class PasswordModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new PasswordFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new PasswordMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new PasswordCollection($this->getMapper(), $this->getEntityFactory());
    }
}