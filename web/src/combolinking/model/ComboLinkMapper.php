<?php

namespace src\combolinking\model;

use src\framework\model\Mapper;
use src\framework\query\Query;

class ComboLinkMapper extends Mapper
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\combolinking\\model\\ComboLink';
    }

    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return 'subforms';
    }
}