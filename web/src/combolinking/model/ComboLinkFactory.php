<?php

namespace src\combolinking\model;

use src\framework\model\EntityFactory;

class ComboLinkFactory extends EntityFactory
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\combolinking\\model\\ComboLink';
    }
}