<?php

namespace src\combolinking\model;

use src\framework\model\RecordEntity;
use src\framework\registry\Registry;
use src\framework\query\QueryFactory;

/**
 * A sub form.  Not a combo link.
 */
class ComboLink extends RecordEntity
{
    /**
     * The Combo Link module.
     * @var string
     */
    protected $sfm_module;

    /**
     * @var string
     */
    protected $sfm_form;

    /**
     * The Combo Link title.
     *
     * @var string
     */
    protected $sfm_title;

    /**
     * The tables of the Combo Link.
     *
     * @var string
     */
    protected $sfm_tables;

    /**
     * The join condition of the Combo Link.
     *
     * @var string
     */
    protected $sfm_join;

    /**
     * @var string
     */
    protected $sfm_dest_form;

    /**
     * @var string
     */
    protected $sfm_type;

    /**
     * The id of the Combo Link.
     *
     * @var int
     */
    protected $sfm_form_id;

    /**
     * @var string
     */
    protected $sfm_holding_tables;

    /**
     * @var string
     */
    protected $sfm_holding_join;
    
    /**
     * The name of the database column(s) that will
     * uniquely identify a record within this sub form.
     * 
     * @var string
     */
    protected $id_column;
    
    /**
     * Constructs and returns a sub query and the associated join conditions used to
     * join to this subform when constructing a query object.
     * 
     * @param Registry     $registry
     * @param QueryFactory $queryFactory
     * 
     * @return array
     */
    public function getSubQuery(Registry $registry = null, QueryFactory $queryFactory = null)
    {
        $registry = $registry ?: Registry::getInstance();
        $queryFactory = $queryFactory ?: new QueryFactory();
        $moduleDefs = $registry->getModuleDefs();
        
        $table = $moduleDefs[$this->sfm_module]['VIEW'] ?: $moduleDefs[$this->sfm_module]['TABLE'];
        
        $subQuery = $queryFactory->getQuery();
        
        $foreignKey = '';
        $joinCondition = [];
        $subQueryWhere = [];
        $tables = explode(',', $this->sfm_tables);
        
        $conditions = preg_split('/ and /i', $this->sfm_join);
        
        foreach ($conditions as $condition)
        {
            $isJoinCondition = false;
            $splitCondition  = explode('=', $condition);
            
            foreach ($splitCondition as $key => $param)
            {
                $param = trim($param);
                
                // conditions that involve the main table recordid are used as the sub query join condition
                if ($table.'.recordid' == $param)
                {
                    $isJoinCondition = true;
                    
                    // the other parameter in this condition is the foreign key, so we need to include this in the sub query select clause
                    $foreignKey = trim($splitCondition[(int) !$key]);
                    
                    // we need to add the subquery alias to the foreign key in the join condition
                    if (false === ($dotPos = strpos($foreignKey, '.')))
                    {
                        $foreignKeyWithAlias = $this->sfm_form.'.'.$foreignKey;
                    }
                    else
                    {
                        $foreignKeyWithAlias = $this->sfm_form.substr($foreignKey, $dotPos);
                    }

                    $joinCondition[] = str_replace($foreignKey, $foreignKeyWithAlias, $condition);
                    break;
                }
            }
            
            if (!$isJoinCondition)
            {
                $subQueryWhere[] = $condition;
            }
        }
        
        $subQueryWhere = implode(' AND ', $subQueryWhere);
        
        $subQuery->select([$foreignKey])
                 ->from($this->sfm_tables);
        
        if ('' != $subQueryWhere)
        {
            $subQuery->setWhereStrings([$subQueryWhere]);
        }
        
        return [$subQuery, implode(' AND ', $joinCondition)];
    }

    /**
     * {@inherit}
     */
    protected static function setIdentity()
    {
        return array('sfm_form_id');
    }
}