<?php
if ($this->FormType != 'Print' && $this->FormType != 'Search' && $this->FormType != 'ReadOnly') : ?>
<script language="JavaScript" type="text/javascript">
    <?php echo MakeJavaScriptValidation('ACT'); ?>
    var submitClicked = false;
    AlertAfterChange = true;
</script>
<?php endif; ?>
<form method="post" id="frmAction" name="frmAction" action="<?php echo $this->SubmitURL; ?>" <?php echo $this->onSubmit; ?>>
    <input type="hidden" name="recordid" value="<?php echo $this->act['recordid']; ?>" />
    <input type="hidden" id="form-panel" name="panel" value="<?php echo ($this->panel ?: ''); ?>" />
    <input type="hidden" name="updateid" value="<?php echo $this->act['updateid']; ?>" />
    <input type="hidden" id="act_module" name="act_module" value="<?php echo $this->module; ?>" />
    <input type="hidden" name="act_cas_id" value="<?php echo $this->act_cas_id; ?>" />
    <input type="hidden" name="form_id" value="<?php echo $this->FormID; ?>" />
    <input type="hidden" name="rbWhat" value="Save" />
    <input type="hidden" name="formaction" value="<?php echo Escape::EscapeEntities($this->FormType); ?>" />
    <input type="hidden" name="fromlisting" value="<?php echo Escape::EscapeEntities($this->fromlisting); ?>" />
    <input type="hidden" name="frommainrecord" value="<?php echo Escape::EscapeEntities($this->frommainrecord); ?>" />
    <input type="hidden" name="actionchain" value="<?php echo (!empty($this->act['act_chain_id']) ? '1' : '0'); ?>" />
    <input type="hidden" name="fromsearch" value="<?php echo Escape::EscapeEntities($this->fromsearch); ?>" />
    <input type="hidden" name="from_report" value="<?php echo Escape::EscapeEntities($this->from_report); ?>" />
    <?php if ($this->error) : ?>
    <div class="error_div"><?php echo _tk('form_errors'); ?></div>
    <?php endif; ?>
    <?php if ($this->sLockMessage) : ?>
    <div class="lock_message">
        <div id="LockMessage"><?php echo $this->sLockMessage; ?></div>
        <div id="userLockMessage"></div>
    </div>
    <?php endif; ?>
    <?php echo $this->ActTable->GetFormTable(); ?>
    <?php echo $this->ButtonGroup->getHTML(); ?>
</form>
<?php if ($this->FormType != 'Print') : ?>
<?php echo JavascriptPanelSelect($GLOBALS['Show_all_section'], $this->panel, $this->ActTable); ?>
<?php endif; ?>