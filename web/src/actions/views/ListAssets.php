<li>
    <table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines" align="center">
        <tr>
            <td class="titlebg" colspan="6">
                <table width="100%">
                    <tr>
                        <td class="titlebg">
                            <b>Equipment</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if (count($this->AssetsActions) == 0) : ?>
            <div class="padded_div windowbg2"><b><?php echo _tk('no_records_found'); ?></b></div>
        <?php else: ?>
        <tr>
            <td class="windowbg" align="center" width="6%"><b>ID</b></td>
            <td class="windowbg" align="center" width="50%"><b>Name</b></td>
            <td class="windowbg" align="center" width="10%"><b>Last service</b></td>
            <td class="windowbg" align="center" width="10%"><b>Next service</b></td>
        </tr>
        <?php foreach ($this->AssetsActions as $AssetsAction) : ?>
            <?php if($this->print == 1 || $this->FormAction == 'ReadOnly') : ?>
            <tr>
                <td class="windowbg2" align="center"><?php $AssetsAction['recordid']; ?></td>
                <td class="windowbg2"><?php echo htmlspecialchars($AssetsAction['ast_name']); ?></td>
                <td class="windowbg2"><?php echo FormatDateVal($AssetsAction['ast_dlastservice']); ?></td>
                <td class="windowbg2"><?php echo FormatDateVal($AssetsAction['ast_dnextservice']); ?></td>
            </tr>
            <?php else : ?>
            <?php $ASTURL = '<a href="'.$this->scripturl.'?action=asset&amp;recordid='.$AssetsAction['recordid'].'">'; ?>
            <tr>
                <td class="windowbg2" align="center"><?php echo $ASTURL.$AssetsAction['recordid']; ?></a></td>
                <td class="windowbg2"><?php echo $ASTURL.htmlspecialchars($AssetsAction['ast_name']); ?></a></td>
                <td class="windowbg2"><?php echo $ASTURL.FormatDateVal($AssetsAction['ast_dlastservice']); ?></a></td>
                <td class="windowbg2"><?php echo $ASTURL.FormatDateVal($AssetsAction['ast_dnextservice']); ?></a></td>
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
    </table>
</li>