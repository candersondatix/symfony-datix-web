<table border="0" width="100%" cellspacing="1" cellpadding="4" class="gridlines">
    <tr>
        <td class="titlebg" colspan="6">
            <table width="100%">
                <tr>
                    <td class="titlebg">
                        <b><?php echo $this->ACT_MODULE_NAME; ?></b>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php if (count($this->records) == 0) : ?>
        <tr>
            <td class="windowbg"><div class="padded_div windowbg2"><b><?php echo _tk('no_records_found'); ?></b></div></td>
        </tr>
    <?php else: ?>
    <tr>
        <?php foreach ($this->ACTION_LINK_LIST_FIELDS as $details) : ?>
        <td class="windowbg" width="<?php echo $details['width']; ?>%" align="center">
            <b><?php echo GetFormFieldLabel($details['field']); ?></b>
        </td>
        <?php endforeach; ?>
    </tr>
    <?php foreach ($this->records as $row) : ?>
    <tr>
        <?php foreach ($this->ACTION_LINK_LIST_FIELDS as $details) : ?>
        <?php
        $fformat = getFieldFormat($details['field'], $this->data['act_module'], $this->ACT_MODULE_TABLE);

        if ($fformat['fmt_data_type'] == 'C' || $fformat['fmt_data_type'] == 'T')
        {
            $value = code_descr($this->data['act_module'], $details['field'], $row[$details['field']], $this->ACT_MODULE_TABLE);
        }
        else if($fformat['fmt_data_type'] == 'D')
        {
        	$value = FormatDateVal($row[$details['field']]);
        }
        else
        {
            $value = $row[$details['field']];
        }
        ?>
        <td class="windowbg2" valign="top" align="left">
            <?php if ($row['url']) : ?>
            <a href="<?php echo $row['url']; ?>"><?php echo $value; ?></a>
            <?php else : ?>
            <?php echo $value; ?>
            <?php endif; ?>
        </td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>