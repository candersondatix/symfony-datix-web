<?php echo FormatDateVal(date('Y-m-d H:i:s.000')); ?>
<?php if (!$this->response['result']) : ?>
<script>
    alert ('<?php echo sprintf(_tk('emails_not_set_domain_not_permitted'), $this->response['blocked']); ?>');
</script>
<?php endif; ?>