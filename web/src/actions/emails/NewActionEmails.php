<?php

namespace src\actions\emails;

use src\framework\events\Observer;
use src\framework\events\Subject;
use src\framework\model\Entity;
use src\actions\model\Action;
use src\email\EmailSender;
use src\logger\iLogger;
use src\users\model\UserModelFactory;

class NewActionEmails implements Observer
{
    /**
     * @var EmailSender
     */
    protected $emailSender;
    
    /**
     * @var iLogger
     */
    protected $logger;
    
    /**
     * @var UserModelFactory
     */
    protected $userFactory;
    
    public function __construct(EmailSender $emailSender, iLogger $logger, UserModelFactory $userFactory)
    {
        $this->emailSender = $emailSender;
        $this->logger = $logger;
        $this->userFactory = $userFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function update(Subject $subject, $event)
    {
        if (!($subject instanceof Action))
        {
            throw new \InvalidArgumentException('Object of type Action expected, '.get_class($subject).' given.');
        }

        if ($event != Entity::EVENT_INSERT)
        {
            // this observer only listens to insert events
            return;
        }
        
        $userAssignedToAction = $this->userFactory->getMapper()->findByInitials($subject->act_to_inits);
        
        // send an e-mail if there's a user assigned to the action, they're not the currently logged-in uder, they're not dead, and they have an e-mail address
        $sendEmail = null !== $userAssignedToAction && 
                     $userAssignedToAction->initials != $_SESSION['initials'] && 
                     empty($userAssignedToAction->con_dod) &&
                     !empty($userAssignedToAction->con_email);

        if ($sendEmail)
        {
            $this->logger->logEmail('E-mailing the To user, unless they are the same as the logged-in user, for new Action/Action Chain '.$subject->recordid.'.');

            $this->emailSender->addRecipient($userAssignedToAction);
            $result = $this->emailSender->sendEmails(array_merge($userAssignedToAction->getVars(), $subject->getVars()));
            
            if (!$result)
            {
                foreach ($this->emailSender->getFailedDomainValidation() as $failedRecipient)
                {
                    $failed[] = $failedRecipient->fullname;
                }
                
                $this->logger->logEmergency(sprintf(_tk('emails_not_set_domain_not_permitted'), implode(', ', $failed)));
            }
        }
    }
}