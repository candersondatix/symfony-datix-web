<?php

namespace src\actions\controllers;

use src\framework\controller\Controller;

class NewActionController extends Controller
{
    function newaction()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $module = \Sanitize::getModule($this->request->getParameter('act_module'));
        $act_cas_id = \Sanitize::SanitizeInt($this->request->getParameter('act_cas_id'));

        if ($module == '')
        {
            $module = \Sanitize::getModule($this->request->getParameter('module'));
        }

        if (!$module || !$act_cas_id)
        {
            fatal_error('No module or case ID given for action.');
        }

        $act['act_cas_id'] = $act_cas_id;

        if (bYN(GetParm('ACT_COPY_LOCATIONS', 'Y'))  && !empty($ModuleDefs[$module]['LOCATION_FIELD_PREFIX']))
        {
            $prefix = $ModuleDefs[$module]['LOCATION_FIELD_PREFIX'];

            $sql = '
                SELECT '.
                $prefix.'organisation as act_organisation, '.
                $prefix.'unit as act_unit, '.
                $prefix.'clingroup as act_clingroup, '.
                $prefix.'directorate as act_directorate, '.
                $prefix.'specialty as act_specialty, '.
                (($module == 'RAM' || $module == 'CLA') ? $prefix.'location as act_loctype, ' : $prefix.'loctype as act_loctype, ').
                $prefix.'locactual as act_locactual
                FROM '.$ModuleDefs[$module]['TABLE'].' WHERE recordid  = :recordid';

            $record = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $act_cas_id));

            $act = array_merge($act, $record);
        }

        $Parameters = array(
            'FormType' => 'New',
            'module' => $module,
            'act_cas_id' => $act_cas_id
        );

        $_SESSION['ACTIONS']['act'] = $act;

        $this->call('src\actions\controllers\ShowActionFormTemplateController', 'showactionform', $Parameters);
    }
}