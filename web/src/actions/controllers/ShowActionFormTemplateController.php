<?php

namespace src\actions\controllers;

use src\framework\controller\TemplateController;

class ShowActionFormTemplateController extends TemplateController
{
    function showactionform()
    {
        global $scripturl, $Show_all_section, $FormTitle, $FormTitleDescr, $DIF1UDFGroups, $FormType;

        if(bYN(GetParm('SHOW_PANEL_INDICATORS', 'N')) && ($FormType != "Print" && $this->request->getParameter('print') != 1))
        {
            $this->addJs('src/generic/js/panelData.js');
        }

        if($FormType == "Print" || $this->request->getParameter('print') == 1)
        {
            $this->addJs('src/generic/js/print.js');
        }

        $ModuleDefs = $this->registry->getModuleDefs();

        $LoggedIn = isset($_SESSION['logged_in']);
        $aReturn = array();
        $FormType = $this->request->getParameter('FormType');
        $module = ($this->request->getParameter('module') ? $this->request->getParameter('module') : '');
        $act_cas_id = ($this->request->getParameter('act_cas_id') ? $this->request->getParameter('act_cas_id') : '');
        $error = ($this->request->getParameter('error') ? $this->request->getParameter('error') : '');
        $act = ($_SESSION['ACTIONS']['act'] ? $_SESSION['ACTIONS']['act'] : '');

        if ($_SESSION['ReadOnlyMode'] || $FormAction == 'ReadOnly')
        {
            $ReadOnly = true;
            $FormType = 'ReadOnly';
        }

        $act['error'] = $error;

        CheckForRecordLocks('ACT', $act, $FormType, $sLockMessage);

        // USE APPVARs to get module title
        if (!$act || ($module && $FormType != 'New'))
        {
            $act['mod_title'] = $ModuleDefs[$module]['NAME'];
        }

        $FormID = is_numeric($this->request->getParameter('form_id')) ? (int) $this->request->getParameter('form_id') : '';

        $ParentLevel = 2;

        // This is the only situation in which we'd be coming from a "level 1" form.
        if ($module == 'SAB' && GetAccessLevel('SAB') == 'SAB1')
        {
            $ParentLevel = 1;
        }

        // Get the linked action form design.
        $FormDesign = \Forms_FormDesign::GetFormDesign(array(
            'module' => 'ACT',
            'link_type' => ($act['act_chain_id'] != '' ? 'action_chains' : 'linked_actions'),
            'level' => 2,
            'parent_module' => $module,
            'parent_level' => $ParentLevel,
            'form_type' => $FormType
        ));
        $FormDesign->LoadFormDesignIntoGlobals();
        $_SESSION['LASTUSEDFORMDESIGN'] = $FormDesign->GetFilename();

        // Set appropriate fields as read-only if this is an action chain step
        if ($act['act_chain_id'] != '')
        {
            if ($act['act_start_after_type'] != '' && $act['act_start_after_days_no'] != '')
            {
                // start/due dates are read-only if set automatically
                $FormDesign->ReadOnlyFields['act_dstart'] = true;

                if ($act['act_due_days_no'] != '')
                {
                    $FormDesign->ReadOnlyFields['act_ddue'] = true;
                }
            }

            // Other values set be the chain design are read-only
            $FormDesign->ReadOnlyFields['act_type'] = true;
            $FormDesign->ReadOnlyFields['act_descr'] = true;
        }

        if ($FormType == 'Search')
        {
            $IDField = '<input type="text" id="recordid" name="recordid" size="5" />';
        }
        else
        {
            $IDField = $act['recordid'];
        }

        $IDFieldObj = new \FormField();
        $IDFieldObj->MakeCustomField($IDField);

        if ($FormType == 'Search')
        {
            $ExcludeArray = getExcludeArray('actions');

            $ModuleField = getModuleDropdown(
                array(
                    'name' => 'act_module',
                    'current' => $act['act_module'],
                    'not' => $ExcludeArray,
                    'include_choose' => true
                )
            );
        }
        else
        {
            $ModuleField = $act["mod_title"];
        }

        $ModuleFieldObj = new \FormField();
        $ModuleFieldObj->MakeCustomField($ModuleField);

        if ($FormType != 'Search' && $act['act_cas_id'])
        {
            //We need to know whether the user has permissions to see the linked record.
            $whereClause = MakeSecurityWhereClause('recordid = '.$act['act_cas_id'], $act['act_module']);
            $act['permissions_to_linked_record'] = (\DatixDBQuery::PDO_fetch('select count(*) FROM '.
                    ($ModuleDefs[$module]['VIEW'] ?: $ModuleDefs[$module]['TABLE']) .
                    ' WHERE '.$whereClause, array(), \PDO::FETCH_COLUMN) == '1');
        }

        // Make up the form, but don't display it yet
        require_once $ModuleDefs['ACT']['BASIC_FORM_FILES'][2];

        // TODO: Remove this after refactoring "Function" entry on BasicForm files
        // This is needed because FormClasses.php needs to call functions defined in non refactored files
        require_once 'Source/actions/ActionForm.php';

        $ActTable = new \FormTable($FormType, 'ACT', $FormDesign);
        $ActTable->MakeForm($FormArray, $act, 'ACT');

        $this->sendToJs(array(
                'FormType' => $FormType,
                'printSections' => $ActTable->printSections)
        );

        $this->title = $GLOBALS['FormTitle'];
        $this->subtitle = $GLOBALS['FormTitleDescr'];
        $this->module = 'ACT';

        $ButtonGroup = new \ButtonGroup();

        if ($FormType != 'Print')
        {
            if ($act['recordid'] == '' && $FormType != 'Search' && !$ReadOnly)
            {
                $ButtonGroup->AddButton(array(
                    'label' => 'Submit action',
                    'id' => 'btnSave',
                    'name' => 'btnSave',
                    'onclick' => 'selectAllMultiCodes(); submitClicked = true;if(validateOnSubmit()){jQuery(\'#frmAction\').submit();}',
                    'action' => 'SAVE'
                ));
            }
            else
            {
                if ($FormType == 'Search')
                {
                    $ButtonGroup->AddButton(array(
                        'label' => (isset($_SESSION['security_group']['grp_id']) ? 'Continue' : 'Search'),
                        'id' => 'btnSearch',
                        'name' => 'btnSearch',
                        'onclick' => 'document.frmAction.rbWhat.value = \'Search\';submitClicked = true;jQuery(\'#frmAction\').submit();',
                        'action' => 'SEARCH'
                    ));
                }
                elseif (!$ReadOnly && $FormType != 'ReadOnly')
                {
                    $ButtonGroup->AddButton(array(
                        'label' => 'Save',
                        'id' => 'btnSave',
                        'name' => 'btnSave',
                        'onclick' => 'selectAllMultiCodes();document.frmAction.rbWhat.value=\'Save\';submitClicked = true;if(validateOnSubmit()){jQuery(\'#frmAction\').submit();}',
                        'action' => 'SAVE'
                    ));
                }
            }

            if (!in_array($FormType, array('ReadOnly', 'Locked', 'Search')))
            {
                $ButtonGroup->AddButton(array(
                    'label' => _tk('btn_cancel'),
                    'id' => 'btnCancel',
                    'name' => 'btnCancel',
                    'onclick' => getConfirmCancelJavascript('frmAction'),
                    'action' => 'CANCEL'
                ));
            }
            else
            {
                $ButtonGroup->AddButton(array(
                    'label' => _tk('btn_cancel'),
                    'id' => 'btnCancel',
                    'name' => 'btnCancel',
                    'onclick' => 'document.frmAction.rbWhat.value = \'Cancel\';submitClicked = true;jQuery(\'#frmAction\').submit();',
                    'action' => 'CANCEL'
                ));
            }

            if ($this->request->getParameter('fromlisting'))
            {
                $ButtonGroup->AddButton(array(
                    'label' => _tk('btn_back_to_report'),
                    'id' => 'btnBack',
                    'name' => 'btnBack',
                    'onclick' => 'submitClicked=true;selectAllMultiCodes();document.frmAction.rbWhat.value=\'BackToListing\';jQuery(\'#frmAction\').submit();',
                    'action' => 'BACK'
                ));
            }
        }

        if ($FormType != 'Search' && $FormType != 'Print' && ($_REQUEST['fromsearch'] || $_GET['from_report']) &&!empty($_SESSION['ACT']['RECORDLIST']) && (!isset($ModuleDefs['ACT']['NO_NAV_ARROWS']) || $ModuleDefs['ACT']['NO_NAV_ARROWS'] != true))
        {
            $CurrentIndex = $_SESSION['ACT']['RECORDLIST']->getRecordIndex(array('recordid' => $act['recordid']));

            if ($CurrentIndex !== false)
            {
                $ButtonGroup->AddNavigationButtons($_SESSION['ACT']['RECORDLIST'], $act['recordid'], 'ACT');
            }
        }

        if ($FormType != 'Print')
        {
            $this->menuParameters = array(
                'table' => $ActTable,
                'buttons' => $ButtonGroup
            );
        }
        else
        {
            $this->hasMenu = false;
        }

        // Make up submit URL, including form ID
        $SubmitURL = $scripturl . '?action=';

        if ($FormType == 'Search')
        {
            $SubmitURL .= 'actionsdoselection';
        }
        else
        {
            $SubmitURL .= 'saveaction';
        }

        if ($FormID)
        {
            $SubmitURL .= '&form_id=' . $FormID;
        }

        if ($FormType != 'Print' && $FormType != 'Search' && $FormType != 'ReadOnly')
        {
            $onSubmit = ' onsubmit="return (submitClicked)"';
        }

        $fromlisting = is_numeric($this->request->getParameter('fromlisting')) ? (int) $this->request->getParameter('fromlisting') : '';
        $frommainrecord = is_numeric($this->request->getParameter('frommainrecord')) ? (int) $this->request->getParameter('frommainrecord') : '';
        $fromsearch = ($this->request->getParameter('fromsearch') ? $this->request->getParameter('fromsearch') : '');
        $from_report = ($this->request->getParameter('from_report') ? $this->request->getParameter('from_report') : '');
        $panel = ($this->request->getParameter('panel') ? $this->request->getParameter('panel') : null);

        $ActTable->MakeTable();

        $this->response->build('src/actions/views/ShowActionForm.php', array(
            'FormType' => $FormType,
            'SubmitURL' => $SubmitURL,
            'onSubmit' => $onSubmit,
            'act' => $act,
            'module' => $module,
            'act_cas_id' => $act_cas_id,
            'FormID' => $FormID,
            'fromlisting' => $fromlisting,
            'frommainrecord' => $frommainrecord,
            'fromsearch' => $fromsearch,
            'from_report' => $from_report,
            'error' => $error,
            'sLockMessage' => $sLockMessage,
            'ActTable' => $ActTable,
            'ButtonGroup' => $ButtonGroup,
            'panel' => $panel
        ));
    }
}