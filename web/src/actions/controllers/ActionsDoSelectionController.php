<?php

namespace src\actions\controllers;

use src\framework\controller\Controller;
use src\reports\exceptions\ReportException;

class ActionsDoSelectionController extends Controller
{
    function actionsdoselection()
    {
        $FormAction = $this->request->getParameter('rbWhat');
        $FieldWhere = '';
        $Error = false;
        $search_where = array();
        ClearSearchSession('ACT');

        if ($FormAction == 'Cancel')
        {
            if (isset($_SESSION['security_group']['grp_id']))
            {
                $_SESSION['security_group']['success'] = false;
                $this->response = $this->call('src\admin\security\controllers\GroupTemplateController', 'editgroup', array(
                    'recordid' => $_SESSION['security_group']['grp_id'],
                    'module' => $_SESSION['security_group']['module']
                ));
                return;
            }
            elseif (isset($_SESSION['packaged_report']))
            {
                $RedirectUrl = '?action=designapackagedreport';

                if ($_SESSION['packaged_report']['recordid'])
                {
                    $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
                }
                else
                {
                    $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
                }

                $_SESSION['packaged_report']['success'] = false;
            }
            elseif ($this->request->getParameter('qbe_recordid') != '')
            {
                // query by example for generic modules
                $this->response = $this->call('src\generic\controllers\ShowRecordController', 'record', array(
                    'module' => $this->request->getParameter('qbe_return_module'),
                    'recordid' => $this->request->getParameter('qbe_recordid')
                ));
                return;
            }
            else
            {
                $RedirectUrl = '?module=ACT';
            }

            $this->redirect('app.php' . $RedirectUrl);
        }

        // construct and cache a new Where object
        try
        {
            $whereFactory = new \src\framework\query\WhereFactory();
            $where = $whereFactory->createFromRequest($this->request);

            if (empty($where->getCriteria()['parameters'])) {
                $where = null;
            }

            $_SESSION['ACT']['NEW_WHERE'] = $where;
        }
        catch (\InvalidDataException $e)
        {
            $Error = true;
            $this->registry->getLogger()->logEmergency($e->getMessage().' ('.$e->getFile().':'.$e->getLine().')');
        }
        catch(ReportException $e)
        {
            $Error = true;
        }

        // Find all values which start with act_ and add them to array
        // if they are not empty.
        // To do: parse names.
        $act = QuotePostArray($this->request->getParameters());
        $_SESSION['ACT']['LAST_SEARCH'] = $this->request->getParameters();

        $table = 'ca_actions';

        while (list($name, $value) = each($act))
        {
            $explodeArray = explode('_', $name);

            if ($explodeArray[0] == 'UDF')
            {
                if (MakeUDFFieldWhere($explodeArray, $value, MOD_ACTIONS, $FieldWhere) === false)
                {
                    $Error = true;
                    break;
                }
            }
            elseif ($explodeArray[0] == 'searchtaggroup')
            {
                // @prompt on tags is not yet supported
                if (preg_match('/@prompt/iu', $value) == 1)
                {
                    $search_where[] = '1=2';
                }
                elseif ($value != '')
                {
                    $field = str_replace('searchtaggroup_'.$explodeArray[1].'_', '', $name);

                    // Need to find the codes associated with the selected tags.
                    $sql = '
                        SELECT DISTINCT
                            code
                        FROM
                            code_tag_links
                        WHERE
                            [group] = :group
                            AND
                            [table] = :table
                            AND
                            field = :field
                            AND
                            tag IN (\''.implode('\',\'', array_map('intval', explode('|', $value))).'\')
                    ';

                    $Codes = \DatixDBQuery::PDO_fetch_all($sql, array(
                        'group' => intval($explodeArray[1]),
                        'field' => $field,
                        'table' => 'ca_actions'
                    ), \PDO::FETCH_COLUMN);

                    if (!empty($Codes))
                    {
                        $search_where[] = $field.' IN (\''.implode('\',\'', $Codes).'\')';
                    }
                    else
                    {
                        // This tag hasn't been attached to any codes, so can't return any results.
                        $search_where[] = '1=2';
                    }
                }
            }
            else
            {
                if (MakeFieldWhere('ACT', $name, $value, $FieldWhere, $table) === false)
                {
                    $Error = true;
                    break;
                }
            }

            if ($FieldWhere != '')
            {
                $search_where[$name] = $FieldWhere;
            }
        }

        if (!$Error && !ValidateWhereClause(implode(' AND ', $search_where), 'ACT'))
        {
            $Error = true;
        }
        
        if ($Error === true)
        {
            AddSessionMessage('ERROR', _tk('search_errors'));
            $this->call('src\actions\controllers\ActionsSearchController', 'actionssearch', array(
                'searchtype' => 'lastsearch',
                'validationerror' => 1
            ));
            return;
        }

        $Where = '';

        if (empty($search_where) === false)
        {
            $Where = implode(' AND ', $search_where);
        }

        $_SESSION['ACT']['WHERE'] = $Where;

        // We are doing a new search so unset LASTQUERYID
        unset($_SESSION['ACT']['LASTQUERYID']);

        if (isset($_SESSION['security_group']['grp_id']))
        {
            $_SESSION['security_group']['success'] = true;
            $this->call('src\\admin\\security\\controllers\\GroupTemplateController', 'editgroup', array(
                'recordid' => $_SESSION['security_group']['grp_id'],
                'module' => $_SESSION['security_group']['module']
            ));
            return;
        }
        elseif (isset($_SESSION['packaged_report']))
        {
            $RedirectUrl = '?action=designapackagedreport';

            if ($_SESSION['packaged_report']['recordid'])
            {
                $RedirectUrl .= '&recordid='.$_SESSION['packaged_report']['recordid'];
            }
            else
            {
                $RedirectUrl .= '&web_report_id='.$_SESSION['packaged_report']['web_report_id'];
            }

            $_SESSION['packaged_report']['success'] = true;
        }
        elseif ($this->request->getParameter('qbe_recordid') != '')
        {
            // query by example for generic modules
            $this->call('src\generic\controllers\ShowRecordController', 'record', array(
                'module' => $this->request->getParameter('qbe_return_module'),
                'recordid' => $this->request->getParameter('qbe_recordid'),
                'qbe_success' => true
            ));
            return;
        }
        else
        {
            $RedirectUrl = '?action=list&module=ACT&listtype=search';
            $_SESSION['ACT']['SEARCHLISTURL'] = $RedirectUrl;
        }

        $this->redirect('app.php' . $RedirectUrl);
    }
}