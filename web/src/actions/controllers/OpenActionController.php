<?php

namespace src\actions\controllers;

use src\framework\controller\Controller;

class OpenActionController extends Controller
{
    function action()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        //set default form mode:
        $FormAction = 'Edit';

        $recordid = ($this->request->getParameter('recordid') ? $this->request->getParameter('recordid') : '');

        $ACTPerms = GetParm('ACT_PERMS');

        if ($recordid == '')
        {
            $recordid = \Sanitize::SanitizeInt($recordid);
        }

        $ACTWhereClause = MakeSecurityWhereClause($ActionsWhere, 'ACT', $_SESSION['initials']);

        $sql = '
            SELECT
                recordid, act_module, act_cas_id, act_from_inits, act_to_inits, act_by_inits,
                act_organisation, act_unit, act_clingroup, act_directorate, act_specialty, act_loctype, act_locactual,
                act_ddue, act_ddone, act_descr, act_score, act_cost, act_type,
                act_cost_type, act_dstart, act_synopsis, act_progress, act_cost_min, act_cost_max,
                act_priority, act_resources, act_monitoring, seclevel, secgroup,
                updateddate, updatedby, updateid, act_chain_id, act_step_no, act_start_after_type, act_start_after_days_no, act_due_days_no,
                mod_title
            FROM
                ca_actions, modules
            WHERE
                ca_actions.act_module = modules.mod_module
                AND
                recordid = :recordid
        ';

        if ($ACTWhereClause)
        {
            $sql .= " AND $ACTWhereClause";
        }

        $action = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $recordid));

        if (!$action || $ACTPerms == 'ACT_INPUT_ONLY' || !$ACTPerms)
        {
            $msg = "You do not have the necessary permissions to view action ID $recordid.";

            if ($dtxdebug)
            {
                $msg .= "\n$sql";
            }

            fatal_error($msg, "Information", \DatixDBQuery::PDO_fetch('SELECT act_module FROM ca_actions WHERE recordid = :recordid', array('recordid' => $recordid), \PDO::FETCH_COLUMN));
        }

        if (!empty($ModuleDefs[$action['act_module']]['FIELD_NAMES']['NAME']))
        {
            $sql = '
                SELECT
                    '.$ModuleDefs[$action['act_module']]['FIELD_NAMES']['NAME'].' as name'
                .' FROM
                '. ($ModuleDefs[$action['act_module']]['VIEW'] ? $ModuleDefs[$action['act_module']]['VIEW'] : $ModuleDefs[$action['act_module']]['TABLE'])
                .' WHERE
                    recordid  = :recordid';

            $record = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $action['act_cas_id']));

            $action['act_recordname'] = $record['name'];
        }

        if ($action['act_chain_id'] != '')
        {
            $sql = '
                SELECT
                    ach_title
                FROM
                    action_chains
                WHERE
                    recordid = :recordid
            ';

            $action['ach_title'] = \DatixDBQuery::PDO_fetch($sql, array('recordid' => $action['act_chain_id']), \PDO::FETCH_COLUMN);
        }

        if ($ACTPerms == 'ACT_READ_ONLY')
        {
            $FormAction = 'ReadOnly';
        }

        if ($this->request->getParameter('print') == 1)
        {
            $FormAction = 'Print';
        }

        // Check if we need to show full audit trail
        if ($action && $this->request->getParameter('full_audit'))
        {
            $FullAudit = GetFullAudit(array('Module' => 'ACT', 'recordid' => $action['recordid']));

            if ($FullAudit)
            {
                $action['full_audit'] = $FullAudit;
            }
        }

        $Parameters = array(
            'FormType' => ($FormAction ? $FormAction : null),
            'module' => $action['act_module'],
            'act_cas_id' => $action['act_cas_id']
        );

        $_SESSION['ACTIONS']['act'] = $action;

        $this->call('src\actions\controllers\ShowActionFormTemplateController', 'showactionform', $Parameters);
    }
}