<?php

namespace src\actions\controllers;

use src\framework\controller\TemplateController;

class ShowSaveActionTemplateController extends TemplateController
{
    function showsaveaction()
    {
        $ModuleDefs = $this->registry->getModuleDefs();

        $action_number = $_SESSION['action_number'];
        $action_whichmodule = $_SESSION['action_whichmodule'];

        $this->title = "Action Number $action_number";
        $this->module = 'ACT';
        $this->hasPadding = false;

        $recordurl = $scripturl . "?action=" . $ModuleDefs['ACT']['ACTION'] . "&recordid=" . htmlspecialchars($action_number);

        if ($this->request->getParameter('panel'))
        {
            $recordurl .= '&panel=' . $this->request->getParameter('panel');
        }

        $status_message = _tk('action_saved');

        AddSessionMessage('INFO', $status_message);

        $this->redirect($recordurl);

        $this->response->build('src/actions/views/ShowSaveAction.php', array(
            'recordid' => $action_number,
            'action_whichmodule' => $action_whichmodule
        ));
    }
}