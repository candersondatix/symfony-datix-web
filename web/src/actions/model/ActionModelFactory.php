<?php
namespace src\actions\model;

use src\framework\model\ModelFactory;

class ActionModelFactory extends ModelFactory
{
    /**
     * {@inheritdoc}
     */
    public function getEntityFactory()
    {
        return new ActionFactory();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMapper()
    {
        return new ActionMapper(new \DatixDBQuery(), $this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        return new ActionCollection($this->getMapper(), $this->getEntityFactory());
    }
}