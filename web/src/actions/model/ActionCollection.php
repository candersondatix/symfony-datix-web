<?php
namespace src\actions\model;

use src\framework\model\EntityCollection;

class ActionCollection extends EntityCollection
{
    /**
     * {@inheritdoc}
     */
    public function targetClass()
    {
        return 'src\\actions\\model\\Action';
    }
}