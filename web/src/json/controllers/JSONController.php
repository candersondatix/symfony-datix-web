<?php
namespace src\json\controllers;

use src\framework\controller\Controller;
use Source\classes\Filters\Container;

/**
 * Controller class for json-related actions.
 */
class JSONController extends Controller
{
    /**
     *
     */
    public function outputjson()
    {
        $dataArray = $this->request->getParameter('data');

        $this->response->build('src/json/views/OutputString.php', array(
            'stringToOutput' => json_encode($dataArray)
            ));
    }
}
