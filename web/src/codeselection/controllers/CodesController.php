<?php

namespace src\codeselection\controllers;

use src\framework\controller\Controller;
use src\system\database\CodeFieldInterface;
use src\users\model\field\StaffField;
use src\system\database\extrafield\CodeExtraField;

class CodesController extends Controller
{
    /**
     * Returns a list of options for a select field.
     */
    public function getCodes()
    {
        $fieldDefs = $this->registry->getFieldDefs();
        $logger    = $this->registry->getLogger();
        $fieldName = $this->get('table').'.'.$this->get('field');
        
        if (null === ($field = $fieldDefs[$fieldName]))
        {
            $message = 'Error retrieving codes: Unable to find field object "'.$fieldName.'"';
            $logger->logEmergency($message);
            throw new \Exception($message);
        }
        
        if (!($field instanceof CodeFieldInterface))
        {
            $message = 'Error retrieving codes: Field object "'.$fieldName.'" is not a code field';
            $logger->logEmergency($message);
            throw new \Exception($message);
        }
        
        /* Revert to the old method of code retrieval for all cases for now.
           This line will need to be removed (along with the codelist() function) when implementing DW-10208.
           Note that this will cause all dropdowns to use the new method of code retrieval, so it will be better
           to make this change in the context of a fully regression tested release.
        */ 
        return $this->codelist();

        $field->useInFormContext();
        $field->setFormData($this->request->getParameters());
        
        $this->response->setBody($field->getCodes()->toJSON());
    }
    
    /**
     * Returns a list of codes keys and values in JSON or XML format
     */
    function codelist()
    {
        global $ClientFolder;

        if($this->request->getParameter('responsetype') == 'autocomplete' || $this->request->getParameter('responsetype') == 'json')
        {
            header('Content-type: application/json; charset=utf-8');
        }
        else
        {
            header('Content-type: text/xml; charset=utf-8');
        }

        require_once 'Source/libs/CodeSelectionCtrl.php';

        $module = \Sanitize::SanitizeString($this->request->getParameter('module'));
        $field = \Sanitize::SanitizeString($this->request->getParameter('field'));
        $FreeText = (\Sanitize::SanitizeString($this->request->getParameter('freetext')) == 'freetext');
        $Multiple = (\Sanitize::SanitizeString($this->request->getParameter('multiple')) == 'multiple');
        $multilistbox = (\Sanitize::SanitizeString($this->request->getParameter('multilistbox')) == 'multilistbox');
        $fieldName = \Sanitize::SanitizeString($this->request->getParameter('fieldname'));
        $udf = (\Sanitize::SanitizeString(\UnicodeString::substr($field, 0, 3)) == 'UDF');
        $fieldmode = \Sanitize::SanitizeString($this->request->getParameter('fieldmode'));
        $overrideTitle = \Sanitize::SanitizeString($this->request->getParameter('overridetitle'));
        $codecheck = \Sanitize::SanitizeInt($this->request->getParameter('codecheck'));
        $childcheck = (\Sanitize::SanitizeInt($this->request->getParameter('childcheck')) == 1);
        $select_sql = $this->request->getParameter('select_sql');
        $table = \Sanitize::SanitizeString($this->request->getParameter('table'));
        $search = (\UnicodeString::strtolower($fieldmode) == 'search');
        $XMLResponse = '';

        if ($this->request->getParameter('parent_value'))
        {
            if (is_array($this->request->getParameter('parent_value')))
            {
                $ParentValues = $this->request->getParameter('parent_value');
            }
            else
            {
                $ParentValues[0] = $this->request->getParameter('parent_value');
            }
        }

        if (isset($_SESSION['Globals']['WEB_SHOW_CODE']) && $_SESSION['Globals']['WEB_SHOW_CODE'] == 'Y')
        {
            $ShowCodes = true;
        }

        $currentValues = array();
        $Value = \Sanitize::SanitizeString($this->request->getParameter('value'));

        if ($this->request->getParameter('value') && $this->request->getParameter('value') != 'null')
        {
            if (is_array($Value))
            {
                $currentValues = $Value;
            }
            elseif (preg_match('/[<>=:!]/u', $Value) == 0)
            {
                $currentValues = explode(($search ? '|' : ' '), $Value);
            }
        }

        $bindArray = array();

        if ($select_sql)
        {
            $sql = $this->whitelistPostedSql($select_sql);
        }
        else
        {
            $sql = GetCodeListSQL($module, $field, $ParentValues, $search, $udf, $FreeText, $currentValues, $title,
                $bindArray, str_replace(' ', ' ', $this->request->getParameter('term')), $table, $childcheck);
        }

        if ($sql)
        {
            try
            {
                $result = PDO_fetch_all($sql, $bindArray);
            }
            catch (\DatixDBQueryException $e)
            {
                returnError($e);
            }
        }


        if ($this->request->getParameter('responsetype') == 'autocomplete')
        {
            if (count($result) > 0)
            {
                foreach ($result as $row)
                {
                	// handle relabelled approval statuses
                	if ($field == 'rep_approved' && ( $new_desc = _tk('approval_status_'.$module.'_'.$row['value']) ) )
                	{
                		$row['description'] = $new_desc;
                		unset( $new_desc );
                	}
                	
                    $description = htmlspecialchars($row['description'], ENT_COMPAT, 'UTF-8', false);

                    $code = array(
                        'value' => htmlspecialchars($row['value'], ENT_QUOTES),
                        'description' => \UnicodeString::rtrim($description, ' ,'),
                        'colour' => ($row['colour'] != '' ? htmlspecialchars($row['colour']) : 'FFF')
                    );

                    $JSONdata[] = $code;
                }
            }
            else
            {
                $code = array(
                    'value' => '!NOCODES!',
                    'description' => 'No codes available'
                );

                $JSONdata[] = $code;
            }

            //encode and return json data...
            echo json_encode($JSONdata);
        }
        elseif ($this->request->getParameter('responsetype') == 'json')
        {
            if (file_exists($_SESSION['LASTUSEDFORMDESIGN']))
            {
                include($_SESSION['LASTUSEDFORMDESIGN']);
                $formlevel = $_SESSION['LASTUSEDFORMDESIGNFORMLEVEL'];
            }

            if (is_array($GLOBALS['UserLabels']))
            {
                $TmpLabels = $GLOBALS['UserLabels'];
            }
            else
            {
                $TmpLabels = array();
            }

            if (is_array($_SESSION['CONTACTLABELS']) && is_array($TmpLabels))
            {
                $GLOBALS['UserLabels'] = array_merge($_SESSION['CONTACTLABELS'], $TmpLabels);
            }

            if ($overrideTitle)
            {
                $FieldTitle = $overrideTitle;
            }
            else
            {
                if ($udf)
                {
                    if ($GLOBALS['UserLabels'][$field])
                    {
                        $FieldTitle = $GLOBALS['UserLabels'][$field];
                    }
                    else
                    {
                        // Format:
                        // - Edit mode:   UDF_<Type>_<GroupID>_<FieldID>
                        // - Search mode: UDF_<FieldID>_<Type>
                        $udf_parts = explode('_', $field);
                        $field_id = $udf_parts[3];
                        $FieldTitle = GetUDFFieldLabel($field_id);
                    }
                }
                else
                {
                    $FieldTitle = GetFormFieldLabel($field, $title, $module, $fieldName);
                }
            }

            $FieldTitle = preg_replace('/&(?![A-Za-z]{0,7};)/u', '&amp;', $FieldTitle);
            $FieldTitle = StripHTML($FieldTitle);

            $JSONdata['title'] = $FieldTitle;

            $JSONdata['ctrlidx'] = $this->request->getParameter('ctrlidx');

            if (count($result) > 0)
            {
                if ($search || $Multiple)
                {
                    $JSONdata['multiple'] = true;
                }

                if ($search)
                {
                    $JSONdata['search'] = true;
                }

                if ($FreeText)
                {
                    $JSONdata['freetext'] = true;
                }

                if (!$search && !$multilistbox)
                {
                    $JSONdata['code'][] = array('value' => '', 'description' => '');
                }

                foreach ($result as $row)
                {
                    $Code = array();

                    if (!$search && !$codecheck && $multilistbox && is_array($currentValues) &&
                        in_array($row['value'], $currentValues))
                    {
                        continue;
                    }

                    $row['value'] = htmlspecialchars($row['value'], ENT_QUOTES);
                    $row['description'] = htmlfriendly($row['description']);

                    $Code['value'] = $row['value'];
                    $Code['description'] = \UnicodeString::rtrim($row['description'], ' ,') . ($ShowCodes ? ': ' . $row['value'] : '' );

                    if (is_array($currentValues) && in_array($row['value'], $currentValues))
                    {
                        $Code['selected'] = true;
                    }

                    $JSONdata['code'][] = $Code;
                }

                if (!$JSONdata['code'])
                {
                    $JSONdata['empty'] = true;
                }
            }
            else
            {
                $JSONdata['empty'] = true;
            }

            //encode and return json data...
            echo json_encode($JSONdata);
        }
        else
        {
            $XMLResponse = '<?xml version="1.0" encoding="UTF-8"?>';

            // Create DTD for XML data to be returned
            // Explicitly declare &pound; entity to fixed browser problem
            $XMLResponse .= '<!DOCTYPE codes [';
            $XMLResponse .= '<!ELEMENT value (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT title (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT description (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT selected (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT multiple (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT search (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT freetext (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT empty (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT ctrlidx (#PCDATA)>';
            $XMLResponse .= '<!ELEMENT codes ((title, ctrlidx, multiple?, search?, freetext?, empty?, code*))>';
            $XMLResponse .= '<!ELEMENT code ((value, description, selected?))>';

            $XMLResponse .= GetEntityCodes();

            $XMLResponse .= ']>';

            $XMLResponse .= '<codes>';
            $XMLResponse .= '<title>';

            if (file_exists($_SESSION['LASTUSEDFORMDESIGN']))
            {
                include($_SESSION['LASTUSEDFORMDESIGN']);
                $formlevel = $_SESSION['LASTUSEDFORMDESIGNFORMLEVEL'];
            }

            if (is_array($GLOBALS['UserLabels']))
            {
                $TmpLabels = $GLOBALS['UserLabels'];
            }
            else
            {
                $TmpLabels = array();
            }

            if (is_array($_SESSION['CONTACTLABELS']) && is_array($TmpLabels))
            {
                $GLOBALS['UserLabels'] = array_merge($_SESSION['CONTACTLABELS'], $TmpLabels);
            }

            if ($overrideTitle)
            {
                $FieldTitle = $overrideTitle;
            }
            else
            {
                if ($udf)
                {
                    if ($GLOBALS['UserLabels'][$field])
                    {
                        $FieldTitle = $GLOBALS['UserLabels'][$field];
                    }
                    else
                    {
                        // Format:
                        // - Edit mode:   UDF_<Type>_<GroupID>_<FieldID>
                        // - Search mode: UDF_<FieldID>_<Type>
                        $udf_parts = explode('_', $field);
                        $field_id = $udf_parts[3];
                        $FieldTitle = GetUDFFieldLabel($field_id);
                    }
                }
                else
                {
                    $FieldTitle = GetFormFieldLabel($field, $title, $module, $fieldName);
                }
            }

            $FieldTitle = preg_replace('/&(?![A-Za-z]{0,7};)/u', '&amp;', $FieldTitle);
            $FieldTitle = StripHTML($FieldTitle);

            $XMLResponse .= $FieldTitle;
            $XMLResponse .= '</title>';
            $XMLResponse .= '<ctrlidx>'.\Sanitize::SanitizeString($this->request->getParameter('ctrlidx')).'</ctrlidx>';

            if (count($result) > 0)
            {
                if ($search || $Multiple)
                {
                    $XMLResponse .= '<multiple>multiple</multiple>';
                }

                if ($search)
                {
                    $XMLResponse .= '<search>search</search>';
                }

                if ($FreeText)
                {
                    $XMLResponse .= '<freetext>freetext</freetext>';
                }

                if (!$search && !$multilistbox)
                {
                    $XMLResponse .= '<code><value/><description/></code>';
                }

                foreach ($result as $row)
                {
                    if (!$search && !$codecheck && $multilistbox && is_array($currentValues) &&
                        in_array($row['value'], $currentValues))
                    {
                        continue;
                    }

                    $codeToDisplay = true;

                    $row['value'] = \Escape::EscapeEntities($row['value'], ENT_QUOTES);
                    $row['description'] = htmlfriendly($row['description']);

                    $XMLResponse .= '<code>';
                    $XMLResponse .= '<value>';
                    $XMLResponse .= $row['value'];
                    $XMLResponse .= '</value>';

                    $XMLResponse .= '<description>';
                    $XMLResponse .= \UnicodeString::rtrim($row['description'], ' ,') . ($ShowCodes ? ': ' . $row['value'] : '' );
                    $XMLResponse .= '</description>';

                    if (is_array($currentValues) && in_array($row['value'], $currentValues))
                    {
                        $XMLResponse .= '<selected>selected</selected>';
                    }

                    $XMLResponse .= '</code>';
                }

                if (!$codeToDisplay)
                {
                    $XMLResponse .= '<empty>empty</empty>';
                }
            }
            else
            {
                $XMLResponse .= '<empty>empty</empty>';
            }

            $XMLResponse .= '</codes>';

            echo $XMLResponse;
        }

        obExit();
    }

    /**
     * Checks SQL POSTed to the page against a whitelist before attempting to execute.
     * Currently only used for field mappings when setting up codes.
     *
     * @param string $sql
     *
     * @return mixed Returns the original query if matched against the whitelist, or null
     */
    protected function whitelistPostedSql($sql)
    {
        if (isset($_SESSION['select_sql_whitelist']) && !$GLOBALS['devNoCache'])
        {
            $whitelist = $_SESSION['select_sql_whitelist'];
        }
        else
        {
            $Query = '
                SELECT
                    fsm_setup_col_select_sql, fsm_setup_col_type
                FROM
                    field_setup_mappings
                WHERE
                    datalength(fsm_setup_col_select_sql) > 0
            ';

            $whitelist = \DatixDBQuery::PDO_fetch_all($Query, array());

            foreach ($whitelist as $key => $sqlData)
            {
                //excpetion because the str_replace is stupid, no one knows why it is here, and it breaks more complex sql mappings.
                if ($sqlData['fsm_setup_col_type'] != 'MSE')
                {
                    $whitelist[$key] = \UnicodeString::str_ireplace(
                        array('cod_code', 'cod_descr'),
                        array('cod_code as value', 'cod_descr as description'),
                        $sqlData['fsm_setup_col_select_sql']
                    );
                }
                else
                {
                    $whitelist[$key] = $sqlData['fsm_setup_col_select_sql'];
                }
            }

            $_SESSION['select_sql_whitelist'] = $whitelist;
        }

        if (in_array($sql, $whitelist))
        {
            return $sql;
        }
        else
        {
            return null;
        }
    }
}