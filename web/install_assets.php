<?php
// create symlinks to legacy static asset directories
symlink(__DIR__.'\\..\\src\\Datix\\LegacyBundle\\Legacy\\css', __DIR__.'\\css');
symlink(__DIR__.'\\..\\src\\Datix\\LegacyBundle\\Legacy\\Images', __DIR__.'\\Images');
symlink(__DIR__.'\\..\\src\\Datix\\LegacyBundle\\Legacy\\js_functions', __DIR__.'\\js_functions');
symlink(__DIR__.'\\..\\src\\Datix\\LegacyBundle\\Legacy\\src', __DIR__.'\\src');
symlink(__DIR__.'\\..\\src\\Datix\\LegacyBundle\\Legacy\\thirdpartylibs', __DIR__.'\\thirdpartylibs');