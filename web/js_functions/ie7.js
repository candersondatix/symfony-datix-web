jQuery(function(){
    if (browser.isIE) {
        if (parseInt(browser.version) <= 7) {

            // workaround for IE7 z-index bug which can cause menu to get hidden
            jQuery(".options").closest('.item-container').each(function() {

                var p = jQuery(this);
                var options = p.find('.options');
                var pos = p.css("position");

                if (pos == "relative" || pos == "absolute" || pos == "fixed") {

                    jQuery('.more-options').on('click', function(){
                        if(options.is(':visible')) {
                            p.addClass("onTop");
                        }
                        else {
                            p.removeClass("onTop");
                        }
                    });
                }
            });
        }
    }
});