window.onload = displaymessages;

function displaymessages()
{
    if (agreementMessages.length > 0)
    {
        setTimeout("displayAgreementMessage(agreementMessages[agreementMessages.length-1])", 300);    
    }
    else
    {
        continueLogin();
    }    
}

function agree()
{
    agreementMessages.pop();
    displaymessages();
}

function disagree()
{
    SendTo(scripturl + '?action=logout&disagreementmessage=true');
}

function continueLogin()
{
    if(redirectLocation)
    {
        SendTo(redirectLocation);
    }
    else
    {
        SendTo(scripturl);
    }
}

function displayAgreementMessage(agreementMessage)
{
    var AlertWindow = AddNewFloatingDiv("AgreementMessage");
    var message = '<div id="messageWindow" style="width:350px; height:250px; border: solid 1px #000000; background-color: #ffffff; padding:5px; overflow: auto;">' + agreementMessage + '</div>';
    
    AlertWindow.setContents(message);
    AlertWindow.setTitle("Usage Agreement");
    AlertWindow.hideCloseControl();
    
    buttonHTML =  '<input type="button" value="Agree" onClick="var div = GetFloatingDiv(\'AgreementMessage\');div.CloseFloatingControl();agree()" />';
    buttonHTML += '<input type="button" value="Disagree" onClick="var div = GetFloatingDiv(\'AgreementMessage\');div.CloseFloatingControl();disagree()" />';
    AlertWindow.setHTMLSection("floating_window_buttons", buttonHTML);
    AlertWindow.display();
}