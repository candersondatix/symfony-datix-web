jQuery(document).ready(function(){

	jQuery("ul.subnav").before(jQuery('<span/>', { 'class': 'nav-arrow' })); //Only shows drop down trigger when js is enabled - Adds empty span tag after ul.subnav

	jQuery("ul.topnav li span").click(function() { //When trigger is clicked...

        var $subnav = jQuery(this).parent().find("ul.subnav");
		//Following events are applied to the subnav itself (moving subnav up and down)
        $subnav.slideDown('fast').show(); //Drop down the subnav on click

        // fix for IE to ensure the subnav has a defined width based on the max child element width
        var maxWidth = 0;
        $subnav.find('li').each(function(){

            var itemWidth = jQuery(this).outerWidth(true);

            if(itemWidth > maxWidth) {

                maxWidth = itemWidth;
            }
        });
        // if there's no width set for the subnav, set one
        if(! $subnav.prop('style')['width']) {

            $subnav.width(maxWidth);
        }

		jQuery(this).parent().hover(function() {
		}, function(){
			jQuery(this).parent().find("ul.subnav").slideUp('slow'); //When the mouse hovers out of the subnav, move it back up
		});

		//Following events are applied to the trigger (Hover events for the trigger)
		}).hover(function() {
			jQuery(this).addClass("subhover"); //On hover over, add class "subhover"
		}, function(){	//On Hover Out
			jQuery(this).removeClass("subhover"); //On hover out, remove class "subhover"
	});

    if(navigator.platform == "iPad")
    {
        jQuery('#wrapper').width('1024px');
    }

});


jQuery(function( $ ){
    var menuRoot = $( "#menu-root" );
    var menu = $( "#menu-2" );

    // Hook up menu root click event.
    menuRoot
        .attr( "href", "javascript:void( 0 )" )
        .click(
            function(){
                // Toggle the menu display.
                menu.toggle();

                // Blur the link to remove focus.
                menuRoot.blur();

                // Cancel event (and its bubbling).
                return( false );
            }
        )
    ;

    // Hook up a click handler on the document so that
    // we can hide the menu if it is not the target of
    // the mouse click.
    $( document ).click(
        function( event ){
            // Check to see if this came from the menu.
            if (
                menu.is( ":visible" ) &&
                !$( event.target ).closest( "#menu-2" ).size()
                ){

                // The click came outside the menu, so
                // close the menu.
                menu.hide();

            }
        }
    );

});


