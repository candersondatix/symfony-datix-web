Rico.loadModule('LiveGridAjax','LiveGridMenu');

Rico.onLoad(function() {
	options = {
	  TableSelectNew: "___new___",
	  TableSelectNone: "",
	  canAdd: true,
	  canEdit: true,
	  canDelete: true,
	  ConfirmDelete: true,
	  ConfirmDeleteCol: 0,
	  DebugFlag: false,
	  prefetchBuffer: true,
	  PanelNamesOnTabHdr: true,
	  highlightElem: "menuRow",
	  menuEvent: "click",
	  formOpen: setupForm,
	  //FilterLocation: -1,
	  columnSpecs : [
	    { FieldName:'holidays_0',
	      panelIdx: 0,
	      ReadOnly: false,
	      ColName: "HOL_DATE",
	      Hdg: "Date",
	      EntryType: "T",
	      ColData: "",
	      // type: "date",
	      width: 300,
	      // filterUI: "t",
		  insertOnly: true,
	      isNullable:false,
		  required: true,
	      Writeable:true,
	      isKey:true,
		  pattern: "^\\d{1,2}/\\d{1,2}/\\d{4}$",
		  Help: "Please enter in the format " + publicHolidays.ricoDateFmt,
	      Length:23 },
	    { FieldName:'holidays_1',
	      panelIdx: 0,
	      ReadOnly: false,
	      ColName: "HOL_NAME",
	      Hdg: "Holiday",
	      EntryType: "T",
	      ColData: "",
	      width: 300,
	      // filterUI: "t",
	      isNullable:false,
		  required: true,
	      Writeable:true,
	      isKey:false,
	      Length:64 }
	  ]
	};
	
	// var options = {};
	var buffer = new Rico.Buffer.AjaxJSON('app.php?action=publicholidaysupdate');
	var grid   = new Rico.LiveGrid('holidays', buffer, options);
	grid.menu  = new Rico.GridMenu();
	var edit   = new Rico.TableEdit(grid);
	
	function setupForm() {
		// Add datepicker
        jQuery(document).ready(function() {
            createCalendar(jQuery('#holidays_0'), publicHolidays.calendarOnClick, publicHolidays.calendarDateFmt, publicHolidays.weekStartDay, publicHolidays.calendarWeekend);
        });
	}
	
	// overwrite the Rico Table error message functionality
	Rico.TableEdit.addMethods({
		validationMsg: function(elem, colnum, phraseId) {
			var col=this.grid.columns[colnum];
		    var msg=RicoTranslate.getPhraseById(phraseId," \"" + col.formLabel.innerHTML + "\"");

		    Rico.writeDebugMsg(' Validation error: '+msg);
		
			if (col.format.Help && phraseId == 'formInvalidFmt') {
				msg+=". \n\n"+col.format.Help;
			}
		    
			alert(msg);
		    setTimeout(function() { try { elem.focus(); elem.select(); } catch(e) {}; }, 10);
		    return false;
		}
	});
});
