//TODO: Currently this plugin is creating a lot of new functions as part of the jQuery namespace which could cause overwritting or issues with other plugins. Ideally this plugin would be refactored to ensure the functions it needs are contained within itself and not available outside of the plugin, however this would require checking that these functions are not used by other areas of the system, such as exists() which is used instead of jQuery('elem').length in various places. If these features are deemed useful to the system in general, then they can be created as extended functions to jQuery itself to ensure consistent availability of these functions.

(function($)
{
    /**
     * class DatixSelect
     */
    var DatixSelect = Class.extend(
        {
            /**
             * Constructor: initialises properties, sets up event handlers, and resizes input field if necessary.
             */
            init: function(input)
            {
                /**
                 * The input field.
                 */
                this.input = $(input);

                this.parent = this.input.closest('.dropdown-wrapper');

                this.button = this.parent.find('.dropdown_button_image');

                /**
                 * The hidden input which holds the actual coded value.
                 *
                 * This element doesn't exist in search mode, and bypassing this step speeds up page load considerably.
                 * But instead of ommitting this step completely, we instantiate an empty jQuery object to prevent
                 * script errors when trying to call methods on this property elsewhere.
                 */
                this.field = this.input.data("search") ? $() : $("#"+this.input.data("fieldId"));

                this.accessibility = this.input.closest('.field_input_div').find('.aria');

                /**
                 * Constants used when tracking key presses.
                 */
                this.keyCode = {
                    DOWN: 40,
                    ENTER: 13,
                    ESCAPE: 27,
                    NUMPAD_ENTER: 108,
                    TAB: 9,
                    UP: 38
                };

                /**
                 * Stores cached search results.
                 */
                this.cache = new Array();

                /**
                 * The current search term.
                 */
                this.term = this.input.val();

                /**
                 * The select list element for this field.
                 */
                this.list = null;

                /**
                 * Holds the search timeout value.
                 */
                this.searching = null;

                /**
                 * Tracks the focus state of this field.
                 */
                this.hasFocus = false;

                /**
                 * Place holder for the currently-active list item.
                 */
                this.activeItem = null;

                /**
                 * Holds the description of the currently selected code.
                 */
                this.description = this.term;

                this.canAdd = this.input.data('can-add') ? true : false;

                /**
                 * The current repeating operation (used when building code lists).
                 */
                this.ro = null;

                this.ajax = null;

                this._setupEventHandlers();
                if (!this.input.data("search"))
                {
                    this.resizeInput();
                }
            },

            /**
             * Provides functionality for dropdown button.  Opens a list with all codes (i.e. with no search term)
             * or closes a list if already open.
             */
            dropdownButton: function()
            {
                if (this._isListVisible())
                {
                    this._closeList();
                }
                else
                {
                    if(this.input.data('search-min-chars')) {

                        this.input.focus();

                        this.button.qtip({
                            content: 'Please type at least four character to start search',
                            style: {
                                classes: 'qtip-blue'
                            },
                            show: {
                                event: 'click',
                                solo: true,
                                ready: true
                            },
                            hide: {
                                event: 'unfocus'
                            },
                            position: {
                                my: 'left center',
                                at: 'right center'
                            }
                        });
                    }
                    else {

                        this._search("");
                    }
                }
            },

            closeList: function()
            {
                this._closeList();
            },

            /**
             * Provides fuctionality for dropdown button.  Opens a list with all codes (i.e. with no search term)
             * or closes a list if already open.
             */
            getList: function()
            {
                return this.list;
            },

            /**
             * Handles selection of items from list.
             *
             * @param object  item The list item that's been selected
             * @param boolean blur Manually trigger a blur event once the item is selected (used when selecting via keyboard)
             */
            selectItem: function(item, blur)
            {
                item = $(item);
                var id = item.attr("id");

                // fix issue caused by empty id attr in some browsers
                if (id == undefined)
                {
                    id = "";
                }

                if (item.attr("id") != "!NOCODES!" && item.attr("id") != "NEWCODE__") {

                    // set value of hidden form field to item code
                    this.field.val(id.replace(/&nbsp;/g, ' ')).change();

                    var description = item.text().replace(/&nbsp;/g, ' ');

                    // set value of input field to item description
                    this.input.val(description);
                    this.description = description;

                    // set field colour if required
                    if (this.input.data("showColour")) {

                        this.input.css("background-color", item.data("datix-colour"));
                    }

                    if(this.input.data('has-plus')) {

                        this.input.removeData('has-plus').unwrap().next('.plus-icon').remove();
                    }

                    this.resizeInput();

                    if (blur != null)
                    {
                        this.input.blur();
                    }
                }
                else if(this.canAdd && item.attr("id") == "NEWCODE__" && this.input.val().length) {

                    var newCode = jQuery.trim(this.input.val());

                    // set value of hidden form field to item code
                    this.field.val('!NEWCODE!=' + newCode).change();

                    // set value of input field to item description
                    this.description = newCode;

                    /**
                     * Add an icon to show that this code is being added into the system
                     */
                    if( ! this.input.data('has-plus')) {

                        this.input.data('has-plus', true).wrap('<span class="input-wrapper"></span>').after('<span class="ui-icon hexff6600 ui-icon-plus plus-icon" title="Adding as new">+</span>');
                    }

                    this.resizeInput();
                    this.input.blur();
                }
                else {

                    if(this.input.data('has-plus')) {

                        this.input.removeData('has-plus').unwrap().next('.plus-icon').remove();
                    }
                }

                /**
                 * Adds button to allow clearing of the field, unless it already has a plus added form above
                 */
                if(this.input.data('mobile') && ! this.input.data('has-clear') && ! this.input.data('has-plus')) {

                    this.input.data('has-clear', true).wrap('<span class="input-wrapper"></span>').after('<span class="ui-icon hex666666 ui-icon-close clear-icon clear-field" title="Clear field">x</span>');
                }

                this._closeList();

                if(!this.input.data("suppressFieldChangedFlag"))
                {
                    FieldChanged(this.input.data("fieldId"));
                }
            },

            /**
             * "Activates" a list item.  An item becomes active when it is being focussed on,
             * either as a result of a mouseover or nav9igating to it via up/down keys.
             */
            activate: function(item)
            {
                if (this.activeItem != null)
                {
                    this.deactivate(this.activeItem);
                }
                this.activeItem = $(item);
                if( ! this.input.data('mobile')) { this.activeItem.addClass("datixSelectOption-hover"); }

                if (this.list != null)
                {
                    this._scrollList(this.activeItem, this.list);
                }
            },

            /**
             * "Deactivates" a list item when it is no longer focussed on.
             */
            deactivate: function()
            {
                if (this.activeItem != null)
                {
                    this.activeItem.removeClass("datixSelectOption-hover");
                    this.activeItem = null;
                }
            },

            /**
             * Called when the field's parent value has changed in order to remove the current value.
             *
             * @param boolean performCheck If true then we check to see if the current value is still valid, if not then we remove regardless.
             */
            checkClearChildField: function(performCheck)
            {
                if (!this.isEmpty() && (!this.input.data("search") || globals["COMBO_LINK_IN_SEARCH"] == "Y"))
                {
                    if (performCheck)
                    {
                        displayLoadPopup();
                        this._checkValue();
                    }
                    else
                    {
                        this.clearField();
                    }
                }
            },

            /**
             * Called when the field's child value has changed in order to autopopulate.
             */
            cascadeDataToParentField: function()
            {
                if(this.field.val())
                {
                    jQuery.getJSON(scripturl+'?action=httprequest&type=getparentvalue&responsetype=json&module='+this.input.data("module")+'&field='+this.input.data("fieldId")+'&value='+this.field.val(), function(data)
                    {
                        //OldAlert(data.value);
                        //OldAlert(data.description);
                        //OldAlert(data.fieldname);

                        /*html = "<li class=\"datixSelectOption\" id=\"" + data.value + "\""
                         "datixcolour=\"#" + data.colour + "\">" + data.description + "</li>";

                         selection = {'id':data.value, 'datixcolour':data.colour, '*/
                        if (data.multiparent == 1)
                        {
                            var li = $('<li/>').attr('id', data.value).data('datix-colour', data.colour).text(data.description);

                            var $parentField = jQuery('#'+data.fieldname+'_title');

                            if ($parentField.length)
                            {
                                $parentField.selectItem(li);
                            }
                        }
                    });
                }
            },

            /**
             * Removes the field's current value/cache.
             */
            clearField: function()
            {
                this.selectItem($('<li/>', {'id': ''}));
                this.cache = new Array();
                this.input.css("background-color", '');
                this._setAccessibility('');
                if(this.list && this.input.data('mobile')) {

                    this.list.remove();
                    this.list = null;
                }

                if(this.input.data('has-plus')) {

                    this.input.removeData('has-plus').unwrap().next('.plus-icon').remove();
                }

                if(this.input.data('has-clear')) {

                    this.input.removeData('has-clear').unwrap().next('.clear-icon').remove();
                }
            },

            /**
             * Disables the input field and hides the dropdown button.
             */
            disable: function()
            {
                this.input.prop("disabled", true);
                this.input.next().css("display", "none");
            },

            /**
             * Enables the input field and displays the dropdown button.
             */
            enable: function()
            {
                this.input.prop("disabled", false);
                this.input.next().css("display", "");
            },

            /**
             * Resizes the input field to fit the current description.
             */
            resizeInput: function()
            {
                resizeInput(this.input);
            },

            /**
             * Sets the value of the field to the free text in the input.
             */
            setFreeText: function()
            {
                if (this.input.val() != this.field.val())
                {
                    // we don't want non-breaking spaces being sent to the server
                    var value = escape(this.input.val()).replace(/%A0/g, ' '); // need to escape the value because non-breaking spaces are not found by the regexp (not sure why)
                    value = unescape(value);

                    this.field.val(value);
                    this.description = this.input.val();
                    this.resizeInput();
                    FieldChanged(this.input.data("fieldId"));
                }
            },

            /**
             * Check whether or not this field currently has a value.
             *
             * @return boolean
             */
            isEmpty: function()
            {
                return this.field.val() == "";
            },

            /**
             * Defines event handlers which control the operation of the datixselect field.
             */
            _setupEventHandlers: function()
            {
                var self = this,
                    fieldChanged;

                this.input.keydown(function(event)
                {
                    switch (event.keyCode)
                    {
                        case self.keyCode.UP:
                            self._moveUp();
                            // prevent moving cursor to beginning of text field in some browsers
                            event.preventDefault();
                            break;
                        case self.keyCode.DOWN:
                            self._moveDown();
                            // prevent moving cursor to end of text field in some browsers
                            event.preventDefault();
                            break;
                        case self.keyCode.ENTER:
                        case self.keyCode.NUMPAD_ENTER:
                            if (self.list != null && self.list.is(":visible") && self.activeItem != null && !self.activeItems)
                            {
                                self.selectItem(self.activeItem, true);
                            }
                            return false; // prevent event from bubbling up to stop e.g. form being submitted on enter
                            break;
                        case self.keyCode.TAB:
                            if (self.list != null && self.list.is(":visible") && self.activeItem != null && !self.activeItems)
                            {
                                self.selectItem(self.activeItem, true);
                            }
                            break;
                        case self.keyCode.ESCAPE:
                            self._closeList();
                            break;
                        default:
                            // keydown triggered before input change, so set a slight delay
                            clearTimeout(self.searching);
                            self.searching = setTimeout(function() {

                                self._search()
                            }, 300);
                            break;
                    }
                });

                this.input.focus(function()
                {
                    self.hasFocus = true;
                });

                this.parent.on('click', '.clear-field', function() {

                    self.input.clearField();

                    self.input.trigger('focus');
                });
            },

            /**
             * Initiates retrieval of codes.  Sets focus to input field and displays loading gif.
             *
             * @param string term Allows search term to be manually overriden.
             */
            _search: function(term)
            {
                this.button.qtip('destroy');

                /**
                 * This limits how searches work on fields which require a minimum number of characters to reduce issues
                 * with performance with fields that have very large code lists.
                 *
                 * If the field doesn't have search-min-chars data set or the length of the value for the input field
                 * is greater than equal to the search-min-chars value, start the search process
                 *
                 * Else if the length of the value of the input field is less than search-min-chars value close the list
                 */
                if(! this.input.data('search-min-chars') || jQuery.trim(this.input.val()).length >= this.input.data('search-min-chars')) {

                    if (this.input.val() != "" || term != undefined) {

                        // only search if the input value has changed or we're overriding the search value
                        this.input.addClass("datixSelect-loading");

                        if( ! this.input.data('mobile')) {

                            this.input.focus();
                        }

                        this.term = jQuery.trim(term != undefined ? term : this.input.val());

                        if (this.list == null) {

                            this._createList();
                        }
                        this._getCodes();
                    }
                    else if(this.input.val() == "" || term == undefined) {

                        this.clearfieldWhenEmptySearchRun();

                        this.input.trigger('focus');
                    }
                }
                else if(this.input.data('search-min-chars') && jQuery.trim(this.input.val()).length < this.input.data('search-min-chars')){

                    this._closeList();
                }
            },

            /**
             * Needed because different functionality is needed for multiselects.
             */
            clearfieldWhenEmptySearchRun: function()
            {
                this.clearField();
            },

            /**
             * Creates a list element for this field.
             */
            _createList: function()
            {
                var self = this;

                if(self.input.data('mobile')) {

                    self.list = $("<div/>")
                        .addClass("datixSelectMobile")
                        .appendTo(self.parent);
                    self.input.data('selected', self.field.val());
                }
                else {
                    self.list = $("<div/>")
                        .addClass("datixSelect")
                        .zIndex(self.input.zIndex() + 1)
                        .hide()
                        .appendTo("body")
                        .mousedown(function(event)
                        {
                            // prevent list closing when using scrollbar
                            if ($(event.target).attr("class") == "datixSelect")
                            {
                                self.input.focus();
                            }
                        });
                }

                if(self.list.outerWidth(true) > 500) {

                    self.list.css({maxWidth: self.input.outerWidth(true) < 500 ? 500 :self.input.outerWidth(true) + 'px'});
                }
            },

            /**
             * Builds and opens the select list or deals with any errors passed back in the code array.
             *
             * @param array codes
             */
            _buildList: function(codes)
            {
                var description;

                if (codes['!ERROR!'])
                {
                    // custom error message returned by select functions
                    alert(codes['!ERROR!']);
                    return;
                }

                this._getListHtml(codes);

                this._setAccessibility(codes);
            },

            /**
             * Builds the Accessiblity text for the current list
             *
             * @private
             */
            _setAccessibility: function(data)
            {
                if(data[0]) {

                    if(data[0]["value"] == "!NOCODES!") {

                        totalItems = 'no';
                    }
                    else {

                        totalItems = data.length
                    }
                }
                else {

                    totalItems = data;
                }

                if(totalItems != '') {

                    if(totalItems == 1) {

                        this.accessibility.text('There is 1 result');
                    }
                    else {
                        this.accessibility.text('There are ' + totalItems + ' results');
                    }
                }
                else{

                    this.accessibility.text('');
                }
            },

            /**
             * Browser-specific display fixes for code lists.
             */
            _fixListStyles: function()
            {
                var self = this;

                this._setListWidth(this.list);
                this._setListElementWidths(this.list);

                if ($.browser.msie && $.browser.version == "6.0")
                {
                    if (this.list.height() > 208)
                    {
                        // max-height workaround
                        this.list.height(208);
                    }

                    // fix for z-index issues
                    if (!$("#"+this.input.data("fieldId")+"_bgiframe").length)
                    {
                        $('<iframe src="javascript:false;" id="'+self.input.data("fieldId")+'_bgiframe" frameborder="0" tabindex="-1"></iframe>')
                            .css({
                                "display":  "block",
                                "position": "absolute",
                                "width":    self.list.outerWidth(),
                                "height":   self.list.outerHeight()
                            })
                            .insertBefore(self.list)
                            .position({of: self.list});
                    }
                    else
                    {
                        $("#"+this.input.data("fieldId")+"_bgiframe")
                            .css({
                                "width":  self.list.outerWidth(),
                                "height": self.list.outerHeight()
                            })
                            .show();
                    }
                }
            },

            /**
             * Generates the HTML for the select list.
             *
             * Uses a RepeatingOperation object to split up the list-building loop into chunks
             * to prevent browser displaying "script taking too long" messages.
             *
             * @param  array  codes
             */
            _getListHtml: function(codes)
            {
                var html = this.input.data("mobile") ? '<select width="150" onchange="var selectedValue = jQuery(this).val(); jQuery(\'#' + this.input.data('fieldId') + '_title\').selectItem(jQuery(this).find(\'option\').filter(function(){ return this.value == selectedValue; }).first())">'
                        : "<ul>",
                    description,
                    self = this,
                    i = 0;

                if (this.ro != null)
                {
                    // cancel any current operation in progress to build code list
                    clearTimeout(this.ro.timeout);
                    this.ro = null;
                    this._closeList();
                    this.list.html("");
                }

                if(this.input.data("mobile") && ((self.canAdd && this.input.val().length) || codes[i]["value"] != "!NOCODES!")) {

                    html += '<option id="NOSELECT__" class="datixSelectOption no-select" value="NOSELECT__" disabled="disabled" selected="selected">Select option</option>';
                }

                if(self.canAdd && (jQuery.trim(self.term).length || jQuery.trim(self.term).length || self.input.data('has-plus'))) {

                    if(self.input.data("mobile")) {

                        html += '<option id="NEWCODE__" class="datixSelectOption new-code" value="NEWCODE__">Create new</option>';
                    }
                    else {

                        html += '<li id="NEWCODE__" class="datixSelectOption new-code" value="NEWCODE__"' +
                        ' onmouseover="jQuery(\'#' + self.input.data("fieldId") + '_title\').activate(this);"' +
                        ' onmouseout="jQuery(\'#' + self.input.data("fieldId") + '_title\').deactivate();"' +
                        ' onmousedown="jQuery(\'#' + self.input.data("fieldId") + '_title\').selectItem(this);" +' +
                        '>Create new</li>';
                    }
                }

                this.ro = new RepeatingOperation(function()
                {
                    var description = String(codes[i]["description"]),
                        $element = $();

                    if (codes[i]["value"] != "!NOCODES!")
                    {
                        if (self.input.data("showCodes") && !self.input.data("freeText"))
                        {
                            // add code to description
                            description += ": " + codes[i]["value"];
                        }

                        if (self.term != "")
                        {
                            // highlight search term matches
                            description = self._highlightMatches(description);
                        }
                    }

                    /**
                     * Limits when the NOCODES field is created so that if you're able to add new codes you don't see the no codes field
                     */
                    if( ! self.canAdd || (self.canAdd && codes[i]["value"] != "!NOCODES!") || (self.canAdd && ! self.term && ! self.input.data('has-plus'))) {

                        if(self.input.data("mobile")) {

                            $element = $('<option/>', {
                                'value': codes[i]["value"],
                                'disabled': codes[i]["value"] == "!NOCODES!"
                            });

                            html += '<option class="datixSelectOption" id="' + codes[i]["value"] + '" value="' + codes[i]["value"] + '"' + (codes[i]["value"] == "!NOCODES!" ? ' disabled="disabled"' : '') + '>' + description + '</option>';
                        }
                        else {

                            html += "<li class=\"datixSelectOption\" id=\"" + codes[i]["value"] + "\" onmouseover=\"jQuery('#" + self.input.data("fieldId") + "_title').activate(this);\"" +
                            " onmouseout=\"jQuery('#" + self.input.data("fieldId") + "_title').deactivate();\"" +
                            " onmousedown=\"jQuery('#" + self.input.data("fieldId") + "_title').selectItem(this);\"" +
                            " datix-colour=\"#" + codes[i]["colour"] + "\">" + description + "</li>";
                        }
                    }

                    if (++i < codes.length) {

                        self.ro.step();
                    }
                    else {

                        // clear the repeating operation var so that it doesn't cause issues when bluring
                        self.ro = null;

                        html +=  self.input.data("mobile") ? "</select>" : "</ul>";
                        self.list.html(html);
                        self._openList();
                        self._fixListStyles();
                    }

                }, 100);

                // Triggers the repeating operation just created
                this.ro.step();

                /*if(self.input.data("mobile") && list.children().length == 1 && list.children().prop('disabled')) {

                    list.prop('disabled', true);
                }*/
            },

            /**
             * Returns true if the list is visible on screen
             * @returns {boolean|*}
             * @private
             */
            _isListVisible: function() {

                return (this.list != null && this.list.is(":visible"));
            },

            /**
             * Displays the field's select list and removes the loading gif.
             */
            _openList: function()
            {
                if (this.list != null)
                {
                    var self = this;

                    if( ! this.input.data('mobile')) {

                        this.list.show().position({
                            of: self.input,
                            my: "left top",
                            at: "left bottom",
                            collision: "none"
                        });
                    }
                    else
                    {
                        /**
                         * Show the list and focus on the select so the list opens automatically
                         */
                        this.list.show(0);
                    }

                    if(self.list.outerWidth(true) > 500) {

                        this.list.css({maxWidth: self.input.outerWidth(true) < 500 ? 500 : self.input.outerWidth(true) + 'px'});
                    }
                }
                if( ! this.input.data('mobile')) {

                    this.input.focus();
                }
                else {

                    this.list.find('option').prop('selected', false);
                    this.list.find('option').first().prop('selected', true);
                    //this.list.find(this.field.val() ? '#' + this.field.val() : '.no-select').prop('selected', true);
                    this.parent.find('#img_add_' + this.field.attr('id')).addClass('dropdown_button_image_mobile_close').removeClass('dropdown_button_image_mobile');
                }
                this.input.removeClass("datixSelect-loading");
                this._highlightCurrentValue();

                $(document).on('mousedown.dropdown', function(event) {

                    if(self._isListVisible()) {

                        var $clickTarget = $(event.target);

                        /**
                         * Check to see if the clicked element isn't either contained within the dropdown parent or the dropdown list
                         */
                        var isField = ($clickTarget.get(0) === self.parent.get(0) || $clickTarget.closest(self.parent).length),
                            isList = ($clickTarget.get(0) === self.list.get(0) || $clickTarget.closest(self.list.get(0)).length);

                        if (!isField && !isList) {

                            self.abortAjax();

                            if (self.ro != null) {
                                // cancel any current operation in progress to build code list
                                clearTimeout(self.ro.timeout);
                                self.ro = null;
                                self._closeList();
                                self.list.html("");
                            }

                            if (!self.input.data('mobile')) {

                                fieldChanged = false;
                                self.hasFocus = false;

                                /*if (browserScrollbarClicked || self.isScrolling) {

                                 // focus is lost in IE7 when clicking on browser scrollbar, so prevent this from closing the dropdown list
                                 self.input.focus();
                                 return;
                                 }*/

                                self._closeList();
                                if (self.input.val() == "") {

                                    // call to FieldChanged() needs to be delayed until after the field's value is removed
                                    if (self.input.val() != self.description) {

                                        fieldChanged = true;
                                    }

                                    // remove the description/code value for this field
                                    self.field.val("");
                                    self.description = "";
                                    self.term = "";
                                    self.resizeInput();

                                    // reset background colour if necessary
                                    if (self.input.data("showColour")) {

                                        self.input.css("background-color", "#FFF");
                                    }

                                    // trigger field changed routines if value changed
                                    if (fieldChanged) {

                                        FieldChanged(self.input.data("fieldId"));
                                    }

                                    if (self.input.data('has-plus')) {

                                        self.input.removeData('has-plus').unwrap().next('.plus-icon').remove();
                                    }
                                }
                                else if (self.input.data("freeText")) {

                                    // users can save whatever text they want, but also have the ability to look up a list of values
                                    self.setFreeText();
                                }
                                else {

                                    // reload description (reverts to previous description if new code not selected)
                                    self.input.val(self.description);
                                    self.term = self.description;
                                }
                                self.input.removeClass("datixSelect-loading");
                            }
                        }
                        else {

                            self.input.focus();
                        }
                    }
                });
            },

            /**
             * Hides the field's select list.
             */
            _closeList: function()
            {
                if(this._isListVisible()) {

                    if (this.list != null) {

                        this.list.hide();
                    }

                    var bgIframe = $("#" + this.input.data("fieldId") + "_bgiframe")

                    if (bgIframe.length) {

                        bgIframe.hide();
                    }

                    if (this.input.data('mobile')) {

                        this.parent.find('#img_add_' + this.field.attr('id')).addClass('dropdown_button_image_mobile').removeClass('dropdown_button_image_mobile_close');
                    }

                    this.activeItem = null;

                    this._setAccessibility('');

                    $(document).off('mousedown.dropdown');
                }
            },

            /**
             * Retrieves codes for this field based on the current search term (if any).
             *
             * Codes are retrieved from a local cache if the search term has been used already.
             *
             * @param boolean check Set if we're retrieving codes in order to check that the current value is still valid.
             */
            _getCodes: function(check)
            {
                this.abortAjax();

                if (this.input.data("customCodes"))
                {
                    // retrieve codes from hardcoded javascript array
                    this._buildList(this._filterCustomCodes(customCodes[this.input.data("fieldId")]));
                }
                else if (!check && this._useCache())
                {
                    // use codes from local cache if search term already used and any parent values are unchanged.
                    this._buildList(this.cache[this.term]);
                }
                else
                {
                    // build data to send via POST
                    var self = this,
                        url  = this._getAjaxUrl(),
                        data = this._getAjaxData(check);

                    // retrieve codes via ajax
                    this.ajax = $.ajax(
                        {
                            url: url,
                            type: "POST",
                            dataType: "text", // setting the dataType as text instead of json bypasses the parser and vastly increases performance
                            data: data,
                            global: false,
                            success: function(data)
                            {
                                eval("data = " + data + ";"); // because we're not using jQuery to evaluate the response as a json object
                                if (check)
                                {
                                    // we're retrieving codes to check that the current value is still valid
                                    self._checkValue(data);
                                }
                                else
                                {
                                    self.cache[self.term] = data;
                                    if (self.input.data('mobile') || self.hasFocus)
                                    {
                                        // only display dropdown if the focus is still on the input field when data is returned
                                        self._buildList(data);
                                    }
                                }
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown)
                            {
                                if(textStatus != 'abort') {

                                    alert("Error retrieving codes.");
                                }
                            }
                        });
                }
            },

            /**
             * Aborts the current ajax process
             */
            abortAjax: function() {

                if(this.ajax) {

                    this.ajax.abort();
                }
            },

            /**
             * Generates the URL used when retrieving codes via AJAX.
             *
             * @return string url
             */
            _getAjaxUrl: function()
            {
                var url;
                if (this.input.data("selectFunction"))
                {
                    url = "?action=httprequest&type=selectfunction&responsetype=autocomplete";
                }
                else
                {
                    url = "?action=httprequest&type=getcodes&responsetype=autocomplete";
                }

                url = scripturl + url;
                return url;
            },

            /**
             * Generates the POST data used when retrieving codes via AJAX.
             *
             * @param boolean check Set if we're retrieving codes in order to check that the current value is still valid.
             *
             * @return array data
             */
            _getAjaxData: function(check)
            {
                var data  = {};
                data["term"] = check ? "" : this.term;

                var parentValues = this._getParentValues();
                if (parentValues.length > 0)
                {
                    data["parent_value"] = parentValues;
                }

                if (this.input.data("selectFunction"))
                {
                    data["function"] = this.input.data("selectFunction");
                    if (this.input.data("selectFunctionParameters"))
                    {
                        for (var parm in this.input.data("selectFunctionParameters"))
                        {
                            data[parm] = this.input.data("selectFunctionParameters")[parm];
                        }
                    }
                    data["current"] = this.field.val();
                }
                else
                {
                    data["field"]     = this.input.data("field");
                    data["fieldname"] = this.input.data("fieldId");
                    data["module"]    = this.input.data("module");
                    data["fieldmode"] = this.input.data("fieldmode");
                    data["table"]     = this.input.data("table");
                    data["showCodes"] = this.input.data("showCodes");

                    if (this.input.data("parents") != undefined)
                    {
                        $.each(this.input.data("parents"), function(key, value)
                        {
                            data["parent"+(key+1)] = value;
                        });
                    }

                    if (this.input.data("children") != undefined)
                    {
                        $.each(this.input.data("children"), function(key, value)
                        {
                            data["child"+(key+1)] = value;
                        });
                    }
                }

                data["childcheck"] = check ? 1 : 0;

                return data;
            },

            /**
             * Checks that the current value is valid given the value(s) of the parent field(s).
             * Removes value if not.
             *
             * Used when RETAIN_COMBO_CHILDREN = Y.
             *
             * @param JSON object codes The current valid codes for this field.
             */
            _checkValue: function(codes)
            {
                if (codes == null)
                {
                    this._getCodes(true);
                }
                else
                {
                    // returning here having retrieved a list of all valid codes
                    var validCodes = new Array();
                    for (var i = 0; i < codes.length; i++)
                    {
                        validCodes[(codes[i]["value"]).toUpperCase()] = true;
                    }

                    if (validCodes[this.field.val().toUpperCase()] !== true)
                    {
                        if ($(this.input).is("span"))
                        {
                            // if the child field is read-only
                            this.input.text("");
                        }
                        else
                        {
                            this.input.val("");
                        }
                        this.field.val("");
                        this.description = "";
                        this.resizeInput();
                        this.input.css("background-color", '');
                        FieldChanged(this.input.data("fieldId"));
                    }

                    hideLoadPopup();
                }
            },

            /**
             * Enables scrolling upwards through a list using the "up" key.
             */
            _moveUp: function()
            {
                if ( ! this.input.data('mobile') && this.list != null && this.list.is(":visible"))
                {
                    if (this.activeItem == null)
                    {
                        this.activate(this.list.find("li:last"));
                    }
                    else
                    {
                        var prev = this.activeItem.prev();
                        if (!prev.length && !this.shiftIsDown)
                        {
                            // we're at the top of the list, so rotate to the bottom
                            prev = this.list.find("li:last");
                        }

                        if (prev.length)
                        {
                            this.activate(prev);
                        }
                    }
                }
            },

            /**
             * Enables scrolling downwards through a list using the "down" key.
             */
            _moveDown: function()
            {
                if ( ! this.input.data('mobile') && this.list != null && this.list.is(":visible"))
                {
                    if (this.activeItem == null)
                    {
                        this.activate(this.list.find("li:first"));
                    }
                    else
                    {
                        var next = this.activeItem.next();
                        if (!next.length && !this.shiftIsDown)
                        {
                            // we're at the bottom of the list, so rotate to the top
                            next = this.list.find("li:first");
                        }

                        if (next.length)
                        {
                            this.activate(next);
                        }
                    }
                }
            },

            /**
             * Automatically scrolls the div when moving up/down through the list.
             *
             * @param object item The item we've just activated.
             * @param object list The list we need to scroll.
             */
            _scrollList: function(item, list)
            {
                if ( ! this.input.data('mobile') && list.height() < list.prop("scrollHeight"))
                {
                    var offset = item.offset().top - list.offset().top,
                        scroll = list.prop("scrollTop"),
                        listHeight = list.height();

                    if (offset < 0)
                    {
                        list.prop("scrollTop", scroll + offset - 2);
                    }
                    else if (offset >= listHeight)
                    {
                        list.prop("scrollTop", scroll + offset + 8 - listHeight + item.height());
                    }
                }
            },

            /**
             * Highlights the parts of the code description which matches the search term used.
             *
             * @param  string description  The code description to highlight.
             *
             * @return string description  The highlighted description.
             */
            _highlightMatches: function(description)
            {
                return description.replace( new RegExp(Encoder.htmlEncode(this.term.escapeRegex()), "gi"), function(str){
                    return '<b>'+str+'</b>';
                });
            },

            /**
             * Gets the current values of this field's parent field(s).
             *
             * @return array parentValues.
             */
            _getParentValues: function()
            {
                var parentValues = new Array(),
                    self = this;

                if (this.input.data("parents") != undefined)
                {
                    $.each(this.input.data("parents"), function(key, value)
                    {
                        if (self.input.data("search"))
                        {
                            // value is already in correct format when searching
                            parentValues[key] = $("#"+value+"_title").length ? $("#"+value+"_title").val() : '';
                        }
                        else
                        {
                            parentValues[key] = $("#"+value).length ? getValuesArray(value).join('|') : '';
                        }
                    });
                }

                return parentValues;
            },

            /**
             * Determines whether to use cached values to build the code list.
             *
             * @return boolean $useCache.
             */
            _useCache: function()
            {
                var useCache = this.input.data("cacheCodes") && this.term in this.cache;

                if (useCache && this.input.data("parents") != undefined)
                {
                    jQuery.each(this.input.data("parents"), function()
                    {
                        useCache = useCache && (!$("#CHANGED-"+this).length || $("#CHANGED-"+this).val() != "1");
                    });
                }

                return useCache;
            },

            /**
             * Filters the custom codes array based on the current search term.
             *
             * @return array codes The filtered code list.
             */
            _filterCustomCodes: function()
            {
                var codes = $.extend([], customCodes[this.input.data("fieldId")]),
                    index = new Array();

                if (this.term != "")
                {
                    for (var i = 0; i < codes.length; i++)
                    {
                        if (codes[i]["description"].match(new RegExp(this.term.escapeRegex(), "i")) == null)
                        {
                            if (!this.input.data("showCodes") || codes[i]["value"].match(new RegExp(this.term.escapeRegex(), "i")) == null)
                            {
                                // store index for removal outside of loop
                                index.push(i);
                            }
                        }
                    }

                    if (index.length > 0)
                    {
                        // remove values at specified indices
                        for (var j = index.length - 1; j > -1; j--)
                        {
                            codes.splice(index[j], 1);
                        }
                    }
                }

                if (codes.length == 0)
                {
                    codes.push({description: "No codes available", value: "!NOCODES!"});
                }

                return codes;
            },

            /**
             * Fixes issues setting list widths in various browsers.
             */
            _setListWidth: function(list)
            {
                if( ! this.input.data('mobile')) {

                    if ($.browser.msie && $.browser.version == "6.0")
                    {
                        // list.width("auto") does strange things in IE6, so...
                        var testDiv = $("#testSize");
                        testDiv.html(list.html());
                        list.width(testDiv.width());

                        // workaround for min-width in IE6
                        if (list.width() < 200)
                        {
                            list.width(200);
                        }
                    }
                    else
                    {
                        list.width("auto");
                    }

                    if (list.innerHeight() < list.prop("scrollHeight"))
                    {
                        // browsers other than IE8+ don't account for scrollbar when setting list width
                        if (!$.browser.msie || parseInt($.browser.version) < 8)
                        {
                            list.width(list.width() + $.scrollbarWidth());
                        }
                    }
                }
            },

            /**
             * Manually sets the width of each list element when building select lists
             * so that they are all they same size (i.e. the same width as the longest element).
             *
             * Necessary for IE7 and lower.
             *
             * @param object list The list whose elements we're resizing.
             */
            _setListElementWidths: function(list)
            {
                if( ! this.input.data('mobile')) {

                    if ($.browser.msie && parseInt($.browser.version) <= 7)
                    {
                        var list = list.find("ul"),
                            width = 0,
                            widest = 190;

                        list.children().each(function()
                        {
                            width = $(this).width();
                            if (width > widest)
                            {
                                widest = width;
                            }
                        });

                        list.children().width(widest);
                    }
                }
            },

            /**
             * Highlights the currently selected value in the select list or the first in the list if no current value set.
             */
            _highlightCurrentValue: function()
            {
                if( ! this.input.data('mobile')) {

                    if (this.field.val() != "")
                    {
                        var selectedValue = this.field.val();

                        try {var current = this.list.find('option').filter(function(){ return this.value == selectedValue; })} catch (e){};
                        if (current != undefined && current.length)
                        {
                            this.activate(current);
                        }
                        else {

                            this.activate(this.list.find("li:first"));
                        }
                    }
                    else {

                        this.activate(this.list.find("li:first"));
                    }
                }
            }

        });

    /**
     * class DatixMultiSelect extends DatixSelect
     */
    var DatixMultiSelect = DatixSelect.extend(
        {
            /**
             * Constructor: Extends parent constructor by adding multiselect-specific properties.
             */
            init: function(input)
            {
                /**
                 * Place holder for the currently-active select list item(s).
                 */
                this.activeItems = {};

                /**
                 * Place holder for the currently-active values list item(s).
                 */
                this.activeValues = [];

                /**
                 * The visible list of selected items for this field.
                 */
                this.valuesList = $("#"+$(input).data("fieldId")+"_values").disableTextSelect();
                //this.resizeValuesList();

                /**
                 * Used to track whether or not mouse button is being held down.
                 */
                this.mouseIsDown = false;

                /**
                 * Used to track whether or not shift key is being held down.
                 */
                this.shiftIsDown = false;

                /**
                 * Used to track whether or not mouse button is being held down on the values list.
                 */
                this.valuesMouseIsDown = false;

                // call parent constructor
                this._super(input);

                this.resizeValuesList();

                // define additional keycode constants
                $.extend(this.keyCode, {SHIFT: 16});

                // necessary to clear any transparent divs added when e.g. disabling the field and then replacing the section
                this.enable();

                /**
                 * The following properies are used to ensure all list items are selected when dragging mouse quickly.
                 */

                    // Is set to the first list item on mousedown.
                this.firstSelected = null;

                // List of items that have been activated when clicking/dragging to select items.
                this.changedItems = [];
            },

            /**
             * Overrides parent function and adds/removes items to/from selected list.  Also handles selection of items in values list.
             *
             * Pre-selected items are added to values list when user presses enter/double clicks the select list.
             * Items selected in the values list are removed from the field when user clicks on the delete button.
             *
             * @param object  item               The list item that's been selected
             * @param boolean suppressSkipHandle True if we don't want to account for skipped items here
             */
            selectItem: function(item, suppressSkipHandle)
            {
                item = $(item);

                if (item.closest("div[id$=_values]").length)
                {
                    // handle selection of item from the values list
                    var index = this.activeValues.indexOf(item.attr("id"));
                    if (index == -1)
                    {
                        // select value item
                        this.activeValues.push(item.attr("id"));
                        item.addClass("datixSelectOption-selected");
                    }
                    else
                    {
                        //deselect value item
                        this.activeValues.splice(index, 1);
                        item.removeClass("datixSelectOption-selected");
                    }
                }
                else if (item.attr("id") != "!NOCODES!")
                {
                    // handle selection of item from the select list
                    if (this.activeItems[item.attr("id")] === undefined)
                    {
                        // select item
                        this.activeItems[item.attr("id")] = item.text();
                        item.addClass("datixSelectOption-selected");
                    }
                    else
                    {
                        // deselect item
                        delete this.activeItems[item.attr("id")];
                        item.removeClass("datixSelectOption-selected");
                    }
                }

                if (this.mouseIsDown || this.valuesMouseIsDown)
                {
                    // prevent items being missed when mouseover quickly
                    this.changedItems.push(item.attr("id"));
                    if (!suppressSkipHandle && item.attr("id") != $(this.firstSelected).attr("id"))
                    {
                        this._handleSkippedItems(item);
                    }
                }
            },

            /**
             * Extends parent function to handle selected items for both select and values lists.
             */
            activate: function(item)
            {
                if (this.mouseIsDown || this.valuesMouseIsDown)
                {
                    // add to/remove from active items list if mousebutton held down
                    this.selectItem(item);
                }

                // remove selected class
                $(item).removeClass("datixSelectOption-selected");

                // call parent function
                this._super(item);

                if (this.valuesMouseIsDown)
                {
                    // scroll values list
                    this._scrollList($(item), this.valuesList);
                }
            },

            /**
             * Extends parent function to handle selected items.
             */
            deactivate: function(item)
            {
                // call parent function
                this._super();

                if (this.shiftIsDown)
                {
                    // select at this point when using shift key as mouse selection happens on mousedown
                    this.selectItem(item);
                }

                if (this.activeItems[$(item).attr("id")] != undefined || this.activeValues.indexOf($(item).attr("id")) != -1)
                {
                    // add styles if this is a selected item
                    $(item).addClass("datixSelectOption-selected");
                }
            },

            /**
             * Takes all the currently selected list items and adds them to the values list.
             */
            addSelectedItems: function()
            {
                var self = this,
                    values = self.valuesList.find('ul').children(),
                    insertElement,
                    fieldChanged = false,
                    searchString = "",
                    newWidth = 0,
                    numItems = values.length,
                    maxLength = $(self.input).prop("maxLength"),
                    maxItems;

                if (this.activeItem != null && this.activeItems[this.activeItem.attr("id")] === undefined)
                {
                    // automatically select the active item
                    this.selectItem(this.activeItem);
                }

                maxItems = Math.round(maxLength/7);

                $.each(this.activeItems, function(code, description)
                {
                    if(!self.input.data("search") && numItems >= maxItems && maxItems != 0)
                    {
                        alert('There is a limit of ' + maxItems + ' codes for this field.');
                        return false;
                    }

                    if (self.input.data("search"))
                    {
                        // add search values
                        searchString += searchString == "" ? code : "|" + code;

                        // invoke field changed routines
                        fieldChanged = true;
                    }
                    else
                    {
                        // add values to field
                        insertElement = null;
                        values.each(function(i, item)
                        {
                            if ($(item).text().toLowerCase() > description.toLowerCase())
                            {
                                // the new list item will be inserted before to this element
                                insertElement = $(item);
                                return false;
                            }
                        });

                        // check that the new item won't cause the values list to become too wide
                        newWidth = self._checkValuesWidth(description);

                        // add new list item
                        if (insertElement == null)
                        {
                            if (values.size() > 0)
                            {
                                // insert after the last list element
                                $(self._getValuesListElement(code, description)).insertAfter(self.valuesList.find("li:last"));
                            }
                            else
                            {
                                // append to the unordered list element as the first list item
                                $(self._getValuesListElement(code, description)).appendTo(self.valuesList.find("ul"));
                            }
                        }
                        else
                        {
                            $(self._getValuesListElement(code, description)).insertBefore(insertElement);
                        }

                        // resize values list if necessary
                        if (newWidth > 0 && newWidth < self.valuesList.width())
                        {
                            self.valuesList.width(newWidth);
                        }

                        // add code to hidden select field
                        $('<option/>', {
                            'value': code,
                            'selected': true
                        }).appendTo(self.field);
                        fieldChanged = true;
                    }
                    numItems++;

                });

                // resize list elements
                this._setListElementWidths(this.valuesList);

                // ensure values list is the correct width
                this._setListWidth(this.valuesList);

                if (searchString != "")
                {
                    this.input.val(searchString);
                    this.description = searchString;
                }

                if (fieldChanged)
                {
                    FieldChanged(this.input.data("fieldId"));
                }

                this._closeList();
            },

            /**
             * Removes all items currently selected in the values list from the field.
             */
            deleteSelectedItems: function()
            {
                var self = this,
                    newWidth = 0;

                self._closeList();

                // ensure values list shrinks appropriately when values deleted
                this.valuesList.width("auto");
                this.valuesList.find("li").width("auto");

                $.each(this.activeValues, function(i, code)
                {
                    self.valuesList.find("li[id='"+jqescape(code)+"']").remove();
                    self.field.find("option[value='"+jqescape(code)+"']").remove();
                });

                // check values list is still the correct width
                newWidth = this._checkValuesWidth(this.valuesList.html());
                if (newWidth > 0)
                {
                    this.valuesList.width(newWidth);
                }
                this._setListWidth(this.valuesList);

                // ensure values list elements are still correct width
                this._setListElementWidths(this.valuesList);

                if (this.activeValues.length > 0)
                {
                    // cache needs to be cleared because selected values will have been omitted from previous code queries
                    this.cache = new Array();

                    // run field changed routine
                    FieldChanged(this.input.data("fieldId"));
                }

                // reset selected values array
                this.activeValues = [];
            },

            /**
             * Overrides select function: we don't want to clear the data in this situation for multicodes.
             */
            clearfieldWhenEmptySearchRun: function()
            {

            },

            /**
             * Overrides parent function in order to handle clearing all multicode values.
             */
            clearField: function()
            {
                var self = this;

                if (this.input.data("search"))
                {
                    // only need to clear the text input value, since there's no values list
                    this.input.val("");
                }
                else
                {
                    // select and remove all of the values in the values list
                    this.activeValues = [];
                    this.valuesList.find("li").each(function(i, item)
                    {
                        self.activeValues.push($(item).attr("id"))
                    });
                    this.deleteSelectedItems();
                }

                this.cache = new Array();
            },

            /**
             * Extends parent function to disable values list as well as input field.
             */
            disable: function()
            {
                var self = this;

                // call parent function
                this._super();

                // "disable" values list
                $("<div/>")
                    .attr("id", this.input.data("fieldId")+"_disable_div")
                    .addClass("multiSelectListDisable")
                    .innerWidth(self.valuesList.innerWidth())
                    .zIndex(self.valuesList.zIndex() + 1)
                    .appendTo("body")
                    .show().position({
                        of: self.valuesList,
                        my: "left top",
                        at: "left top",
                        collision: "none"
                    });

                // hide delete button
                this.valuesList.next().css("display", "none");
            },

            /**
             * Extends parent function to enable values list as well as input field.
             */
            enable: function()
            {
                // call parent function
                this._super();

                // "enable" values list
                if ($("#"+this.input.data("fieldId")+"_disable_div").length)
                {
                    $("#"+this.input.data("fieldId")+"_disable_div").remove();
                }

                // display delete button
                this.valuesList.next().css("display", "");
            },

            /**
             * Check whether or not this field currently has a value.
             *
             * @return boolean
             */
            isEmpty: function()
            {
                if (this.input.data("search"))
                {
                    return this.input.val() == "";
                }
                else
                {
                    return !this.valuesList.find("ul").children().length;
                }
            },

            /**
             * Setter for firstSelected property.
             *
             * @param object item The list item that's been selected.
             */
            setFirstSelected: function(item)
            {
                this.firstSelected = item;
            },

            /**
             * Ensures the values list is displayed correctly.
             */
            resizeValuesList: function()
            {
                if ($(this.valuesList).is(":visible"))
                {
                    this._setListElementWidths(this.valuesList);
                    this._setListWidth(this.valuesList);
                }
            },

            /**
             * Extend parent function for additional event handlers.
             */
            _setupEventHandlers: function()
            {
                var self = this;

                // call parent function
                this._super();

                // define additional event handlers
                this.input.keydown(function(event)
                {
                    switch (event.keyCode)
                    {
                        case self.keyCode.ENTER:
                        case self.keyCode.NUMPAD_ENTER:
                        case self.keyCode.TAB:
                            self.addSelectedItems();
                            break;
                        case self.keyCode.SHIFT:
                            self.shiftIsDown = true;
                            break;
                    }
                });

                this.input.keyup(function(event)
                {
                    switch (event.keyCode)
                    {
                        case self.keyCode.SHIFT:
                            if (self.activeItem != null)
                            {
                                // selection with shift happens on key up as holding the key down fires multiple events (unlike mousedown)
                                self.selectItem(self.activeItem);
                                self.activeItem.removeClass("datixSelectOption-selected");
                            }
                            self.shiftIsDown = false;
                            break;
                    }
                });

                // track mouse button for selection of items in values list
                this.valuesList
                    .mousedown(function(event)
                    {
                        // track mousedown on document as a whole
                        documentMouseDown = true;

                        if ($(event.target).attr("class") != "codefield multiSelectList")
                        {
                            // set valuesMouseIsDown if not clicking on scrollbar
                            self.valuesMouseIsDown = true;
                        }
                    })
                    .mouseleave(function(event)
                    {
                        if (self.valuesMouseIsDown)
                        {
                            self._scrollAndSelect($(this), event);
                        }
                    })
                    .bind("mouseup mouseleave", function(event)
                    {
                        // track mousedown on values list
                        self.valuesMouseIsDown = false;

                        // reset tracking for missed items when moving cursor quickly
                        self.firstSelected = null;
                        self.changedItems = [];
                    });
            },

            /**
             * Extends parent function to modify event handlers for select list.
             */
            _createList: function()
            {
                var self = this;

                // call parent function
                this._super();

                if(!self.input.data('mobile')) {

                    // modify event handlers
                    this.list.unbind("mousedown")
                        .mousedown(function(event)
                        {
                            // track mousedown on document as a whole
                            documentMouseDown = true;

                            // track mousedown on list if not clicking on scrollbar
                            if ($(event.target).attr("class") != "datixSelect")
                            {
                                self.mouseIsDown = true;
                            }
                            else if($(event.target).attr("class") == "datixSelect") {

                                self.isScrolling = true;
                            }

                            // return focus to input to prevent list from closing
                            setTimeout(function(){self.input.focus();}, 10);
                        })
                        .mouseleave(function(event)
                        {
                            if (self.mouseIsDown)
                            {
                                self._scrollAndSelect($(this), event);
                            }
                        })
                        .bind("mouseup mouseleave", function()
                        {
                            // track mousedown on list
                            self.mouseIsDown = false;
                            self.isScrolling = false;

                            // reset tracking for missed items when moving cursor quickly
                            self.firstSelected = null;
                            self.changedItems = [];
                        })
                        .disableTextSelect();
                }
            },

            /**
             * Extends parent function so activeItems are reset whenever list is built.
             *
             * @param array codes
             */
            _buildList: function(codes)
            {
                // reset selected items array
                this.activeItems = {};

                // call parent function
                this._super(codes);
            },

            /**
             * Overrides parent function to create multiselect-specific HTML.
             *
             * Uses a RepeatingOperation object to split up the list-building loop into chunks
             * to prevent browser displaying "script taking too long" messages.
             *
             * @param  array  codes
             */
            _getListHtml: function(codes)
            {
                if(this.input.data('mobile')) {

                    this._super(codes);
                }

                /**
                 * Because iOS and Android work differently on multiselect fields, we need to use blur for iOS and change for
                 * Android. It also needs to be done here, so that the event can be set using a variable
                 * @type {string}
                 */
                if(this.input.data("mobile")) {

                    var triggerEvent = (globals.deviceDetect.isIOS ? 'blur' : 'change');

                    var mobileEvent = ' on' + triggerEvent + '="var selectField = jQuery(this); selectField.val().each(function(value){ jQuery(\'#' + this.input.data('fieldId') + '_title\').selectItem(selectField.find(\'option\').filter(function(){ return this.value == value; }).first()); }); jQuery(\'#' + this.input.data('fieldId') + '_title\').addSelectedItems()"';
                }

                var html = this.input.data("mobile") ? '<select width="150" multiple="multiple"'+mobileEvent+'>'
                : "<ul>",
                    description,
                    self = this,
                    i = 0;

                /**
                 * Because iOS and Android work differently on multiselect fields, we need to use blur for iOS and change for
                 * Android. It also needs to be done here, so that the event can be set using a variable
                 * @type {string}
                 */
                /*if(this.input.data("mobile")) {

                    var triggerEvent = (globals.deviceDetect.isIOS ? 'blur' : 'change');

                    list.on(triggerEvent, function(){ $(this).val().each(function(value){ $('#' + self.input.data('fieldId') + '_title').selectItem(self.list.find('option').filter(function(){ return this.value == value; }).first()); }); $('#' + self.input.data('fieldId') + '_title').addSelectedItems()});
                }*/

                if (this.ro != null)
                {
                    // cancel any current operation in progress to build code list
                    clearTimeout(this.ro.timeout);
                    this.ro = null;
                    this._closeList();
                    this.list.html("");
                }

                if(this.input.data("mobile") && ((self.canAdd && this.input.val().length) || codes[i]["value"] != "!NOCODES!")) {

                    html += '<option id="NOSELECT__" class="datixSelectOption no-select" value="NOSELECT__" disabled="disabled">Select option</option>';
                }

                this.ro = new RepeatingOperation(function()
                {
                    if (self._includeListOption(codes[i]["value"]))
                    {
                        description = String(codes[i]["description"]);
                        if (codes[i]["value"] != "!NOCODES!")
                        {
                            if (self.input.data("showCodes"))
                            {
                                // add code to description
                                description += ": " + codes[i]["value"];
                            }

                            if (self.term != "")
                            {
                                // highlight search term matches
                                description = self._highlightMatches(description);
                            }
                        }
                        description = description.replace(/ /g, "&nbsp;");

                        if(self.input.data("mobile")) {

                            html += '<option class="datixSelectOption" id="' + codes[i]["value"] + '" value="' + codes[i]["value"] + '"' + (codes[i]["value"] == "!NOCODES!" ? ' disabled="disabled"' : '') + '>' + description + '</option>';
                        }
                        else {

                            html += "<li class=\"datixSelectOption\" id=\"" + codes[i]["value"] + "\"" +
                            " onmouseover=\"jQuery('#" + self.input.data("fieldId") + "_title').activate(this);\"" +
                            " onmouseout=\"jQuery('#" + self.input.data("fieldId") + "_title').deactivate(this);\"" +
                            " onmousedown=\"jQuery('#" + self.input.data("fieldId") + "_title').setFirstSelected(this);jQuery('#" + self.input.data("fieldId") + "_title').selectItem(this);\"" +
                            " ondblclick=\"jQuery('#" + self.input.data("fieldId") + "_title').selectItem(this);jQuery('#" + self.input.data("fieldId") + "_title').addSelectedItems();\"" +
                            ">" + description + "</li>";
                        }
                    }

                    if (++i < codes.length) {

                        self.ro.step();
                    }
                    else {

                        // clear the repeating operation var so that it doesn't cause issues when bluring
                        self.ro = null;

                        if (html == "<ul>") {

                            // no codes left to select
                            html += "<li class=\"datixSelectOption\">No codes available</li>";
                        }
                        else if(html == '<select width="150" multiple="multiple">') {

                            html += "<option class=\"datixSelectOption\" disabled=\"disabled\">No codes available</option>";
                        }

                        html +=  self.input.data("mobile") ? "</select>" : "</ul>";
                        self.list.html(html);
                        self._openList();
                        self._fixListStyles();
                    }

                }, 100);

                this.ro.step();
            },

            /**
             * Defines the condition(s) for including a code in the select list for this field.
             *
             * @param string code
             */
            _includeListOption: function(code)
            {
                if (this.input.data("customIncludeListOption") != undefined)
                {
                    // execute the custom function
                    var result = true;
                    eval(this.input.data("customIncludeListOption")); // the onus is on the custom function to set result to false if appropriate
                    return result;
                }
                // only display codes not already selected
                return !this.field.find("option[value='"+code+"']").length;
            },

            _openList: function() {

                var self = this;

                self._super();

                if (self.input.data("search"))
                {
                    // override default functionality to allow any search term to be entered
                    $(document).off('.dropdown');
                    $(document).on('mousedown.dropdown', function(event) {
                        if(self._isListVisible()) {

                            var $clickTarget = $(event.target);

                            /**
                             * Check to see if the clicked element isn't either contained within the dropdown parent or the dropdown list
                             */
                            var isField = ($clickTarget.get(0) === self.parent.get(0) || $clickTarget.closest(self.parent).length),
                                isList = ($clickTarget.get(0) === self.list.get(0) || $clickTarget.closest(self.list.get(0)).length);

                            if (!isField && !isList) {

                                self.abortAjax();

                                if (self.ro != null) {
                                    // cancel any current operation in progress to build code list
                                    clearTimeout(self.ro.timeout);
                                    self.ro = null;
                                    self._closeList();
                                    self.list.html("");
                                }

                                if (!self.input.data('mobile')) {

                                    self.hasFocus = false;

                                    if (browserScrollbarClicked || self.isScrolling) {

                                        // focus is lost in IE7 when clicking on browser scrollbar, so prevent this from closing the dropdown list
                                        self.input.focus();
                                        return;
                                    }

                                    self._closeList();

                                    // trigger field changed routines if value changed
                                    if (self.input.val() != self.description) {
                                        FieldChanged(self.input.data("fieldId"));
                                    }

                                    // set the description/code value for this field
                                    self.field.val(self.input.val());
                                    self.description = self.input.val();
                                    self.input.removeClass("datixSelect-loading");
                                }
                            }
                        }
                    });
                }
            },

            /**
             * Extends parent function to reset multiselect properties.
             */
            _closeList: function()
            {
                // call parent function
                this._super();

                // reset selected items array
                this.activeItems = {};

                // reset IsDown properties
                self.shiftIsDown = false;
                self.mouseIsDown = false;
                self.isScrolling = false;

                $(document).off('.dropdown');
            },

            /**
             * Extends parent function to add multiselect-specific POST data.
             *
             * @param boolean check Set if we're retrieving codes in order to check that the current value is still valid.
             *
             * @return array data
             */
            _getAjaxData: function(check)
            {
                // call parent function
                var data = this._super(check);

                // add multiselect data
                data["multilistbox"] = "multilistbox";

                if (!check)
                {
                    data["value"] = this.field.val();
                }

                return data;
            },

            /**
             * Overrides parent function to handle removal of multicode child field values.
             *
             * @param JSON object codes The current valid codes for this field.
             */
            _checkValue: function(codes)
            {
                if (codes == null)
                {
                    this._getCodes(true);
                }
                else
                {
                    // returning here having retrieved a list of all valid codes
                    var validCodes = new Array(),
                        self = this,
                        values;

                    for (var i = 0; i < codes.length; i++)
                    {
                        validCodes[codes[i]["value"]] = null;
                    }

                    if (this.input.data("search"))
                    {
                        //invalid values need to be removed from the search string itself
                        var invalidCodes = []
                        fieldChanged = false;

                        values = this.input.val().split("|");
                        $.each(values, function(i, code)
                        {
                            if (validCodes[code] === undefined)
                            {
                                // remove code
                                values.splice(i, 1);
                                fieldChanged = true;
                            }
                        });

                        // rebuild and assign field value
                        this.input.val(values.join("|"));

                        if (fieldChanged)
                        {
                            FieldChanged(this.input.data("fieldId"));
                        }
                    }
                    else
                    {
                        // remove invalid values from values list
                        values = this.valuesList.find("ul").children();

                        this.activeValues = [];

                        values.each(function(i, item)
                        {
                            if (validCodes[$(item).attr("id")] === undefined)
                            {
                                // add invalid code to active values for deletion later
                                self.activeValues.push($(item).attr("id"));
                            }
                        });

                        if (this.activeValues.length > 0)
                        {
                            this.deleteSelectedItems();
                        }
                    }

                    hideLoadPopup();
                }
            },

            /**
             * Creates the HTML for each element in the values list.
             *
             * @param string id          The code, used as the list element's identifier.
             * @param string description The code's description
             *
             * @return string
             */
            _getValuesListElement: function(id, description)
            {
                return "<li id="+id+" class=\"datixSelectOption\" onmouseover=\"jQuery('#" + this.input.data("fieldId") + "_title').activate(this);\"" +
                    "onmouseout=\"jQuery('#" + this.input.data("fieldId") + "_title').deactivate(this);\"" +
                    "onmousedown=\"jQuery('#" + this.input.data("fieldId") + "_title').setFirstSelected(this);jQuery('#" + this.input.data("fieldId") + "_title').selectItem(this);\">"+description+"</li>";
            },

            /**
             * Ensures the width of the values list doesn't exceed that of it's container.
             *
             * @param  string contents    The contents we're using to check the width.
             *
             * @return int    width       The width the values list should be set to.
             */
            _checkValuesWidth: function(contents)
            {
                var testDiv = $("#testSize"),
                    width = 0;
                testDiv.html(contents);
                if (testDiv.width() > (this.valuesList.parent().width() - 30))
                {
                    width = this.valuesList.parent().width() - 30;
                }
                return width;
            },

            /**
             * Attempts to prevent list items being skipped over when holding down the mouse button and dragging the cursor quickly.
             *
             * @param object item The item that's being currently selected.
             */
            _handleSkippedItems: function(item)
            {
                var first = $(this.firstSelected),
                    current = $(item),
                    next = null,
                    direction = current.isAfter("li[id="+first.attr("id")+"]") ? "next" : "prev",
                    iteration = 0,
                    limit = 10;

                next = eval("first."+direction+"()");
                while (next.attr("id") != current.attr("id") && iteration < limit)
                {
                    if (this.changedItems.indexOf(next.attr("id")) == -1)
                    {
                        // automatically select any items in between the first selected and the one being currently selected
                        // which haven't already been changed during this event (i.e. which have been skipped over)
                        this.selectItem(next, true);
                    }
                    next = eval("next."+direction+"()");
                    iteration++;
                }
            },

            /**
             * Handles the continued scrolling/selection of items when mouse button is held down and cursor is dragged off the top/bottom.
             *
             * @param object list  The list we're scrolling.
             * @param object event The event object.
             */
            _scrollAndSelect: function(list, event)
            {
                var pos = list.offset(),
                    top = event.pageY < pos.top,
                    bottom = event.pageY > (pos.top + list.height()),
                    self = this,
                    item = null;

                if (top || bottom)
                {
                    // cursor has been dragged off the top/bottom of the list element
                    if (list.innerHeight() < list.prop("scrollHeight"))
                    {
                        // need to figure out the top-most/bottom-most visible list element
                        listElementNumber = Math.round((list.prop("scrollTop")/list.find("li:first").outerHeight())) + 1;
                        if (bottom)
                        {
                            listElementNumber += (Math.round((list.height()/list.find("li:first").outerHeight())) - 1);
                        }
                        item = list.find("ul > :nth-child("+listElementNumber+")");

                        if (this.changedItems.indexOf(item.attr("id")) == -1)
                        {
                            // the first/last item was also skipped, so needs to be selected
                            this.selectItem(item);
                        }
                        else
                        {
                            this._handleSkippedItems(item);
                        }

                        // keep scrolling/selecting until the mouse button is released, or the end of the list is reached
                        var intId = setInterval(function()
                        {
                            item = top ? item.prev() : item.next();
                            if (!item.length)
                            {
                                clearInterval(intId);
                            }
                            else
                            {
                                self.selectItem(item, true);
                                self._scrollList(item, list);
                                if (!documentMouseDown)
                                {
                                    clearInterval(intId);
                                }
                            }
                        }, 30);
                    }
                    else
                    {
                        // no need to account for scroll, so we can simply fill in all the items
                        // between the first/last item on the list and the first item selected
                        item = top ? list.find("li:first") : list.find("li:last");
                        if (this.changedItems.indexOf(item.attr("id")) == -1)
                        {
                            // the first/last item was also skipped, so needs to be selected
                            this.selectItem(item);
                        }
                        else
                        {
                            this._handleSkippedItems(item);
                        }
                    }
                }
            }
        });

    /**
     * DatixSelect plugin.
     */
        //TODO - this really shouldn't be done like this as it's adding a load of functions into jQuery that aren't generic
    $.fn.datixselect = function()
    {
        return this.each(function()
        {
            if (!$(this).data("datixselect"))
            {
                // create and initialise new datixselect object
                var datixselect = new DatixSelect(this);

                // store object in the input element's data so it can be accessed externally
                $(this).data("datixselect", datixselect);
            }
        });
    };

    /**
     * DatixMultiSelect plugin.
     */
    $.fn.datixmultiselect = function()
    {
        return this.each(function()
        {
            if (!$(this).data("datixselect"))
            {
                // create and initialise new datixmultiselect object
                var datixmultiselect = new DatixMultiSelect(this);

                // store object in the input element's data so it can be accessed externally
                $(this).data("datixselect", datixmultiselect);
            }
        });
    };

    /**
     * Prevent text selection.
     */
    $.fn.disableTextSelect = function()
    {
        return this.each(function()
        {
            $(this).css({'MozUserSelect' : 'none'})
                .bind('selectstart', function()
                {
                    return false;
                })
                .mousedown(function()
                {
                    return false;
                });
        });
    };

    /**
     * Check whether this element appears after a selected element in a list of siblings.
     */
    $.fn.isAfter = function(sel)
    {
        return this.prevAll(sel).length > 0;
    };

    /**
     * Check whether this element appears before a selected element in a list of siblings.
     */
    $.fn.isBefore= function(sel)
    {
        return this.nextAll(sel).length > 0;
    };

    /**
     * jQuery Scrollbar Width v1.0
     *
     * Copyright 2011, Rasmus Schultz
     * Licensed under LGPL v3.0
     * http://www.gnu.org/licenses/lgpl-3.0.txt
     */
    $.scrollbarWidth = function()
    {
        if (!$._scrollbarWidth)
        {
            // TODO: The $.browser property was removed in jQuery 1.9,
            // we need to use a different method before upgrading
            if ($.browser.msie && $.browser.version == "6.0")
            {
                // this function seems to crash IE6 sometimes, so:
                $._scrollbarWidth = 17;
            }
            else
            {
                var $body = $('body');
                var w = $body.css('overflow', 'hidden').width();
                $body.css('overflow','scroll');
                w -= $body.width();
                if (!w) w=$body.width()-$body[0].clientWidth; // IE in standards mode
                $body.css('overflow','');
                $._scrollbarWidth = w;
            }
        }
        return $._scrollbarWidth;
    };

    /**
     * Plugin to delegate to DatixSelect::dropdownButton().
     */
    $.fn.dropdownButton = function()
    {
        initDropdown(this);
        this.data("datixselect").dropdownButton();
    };

    $.fn.closeList = function()
    {
        initDropdown(this);
        this.data("datixselect").closeList();
    };

    $.fn.getList = function()
    {
        initDropdown(this);
        this.data("datixselect").dropdownButton();
        return this.data("datixselect").getList();
    };

    /**
     * Plugin to delegate to DatixSelect::selectItem().
     */
    $.fn.selectItem = function(item)
    {
        initDropdown(this);
        this.data("datixselect").selectItem(item);
    };

    /**
     * Plugin to delegate to DatixSelect::activate().
     */
    $.fn.activate = function(item)
    {
        initDropdown(this);
        this.data("datixselect").activate(item);
    };

    /**
     * Plugin to delegate to DatixSelect::deactivate().
     */
    $.fn.deactivate = function(item)
    {
        initDropdown(this);
        this.data("datixselect").deactivate(item);
    };

    /**
     * Plugin to delegate to DatixSelect::checkClearChildField().
     */
    $.fn.checkClearChildField = function(performCheck)
    {
        initDropdown(this);
        this.data("datixselect").checkClearChildField(performCheck);
    };

    /**
     * Plugin to delegate to DatixSelect::cascadeDataToParentField().
     */
    $.fn.cascadeDataToParentField = function()
    {
        initDropdown(this);
        this.data("datixselect").cascadeDataToParentField();
    };

    /**
     * Plugin to delegate to DatixSelect::clearField().
     */
    $.fn.clearField = function()
    {
        initDropdown(this);
        this.data("datixselect").clearField();
    };

    /**
     * Plugin to delegate to DatixSelect::disable().
     */
    $.fn.disable = function()
    {
        initDropdown(this);
        this.data("datixselect").disable();
    };

    /**
     * Plugin to delegate to DatixSelect::enable().
     */
    $.fn.enable = function()
    {
        initDropdown(this);
        this.data("datixselect").enable();
    };

    /**
     * Plugin to delegate to DatixSelect::resizeInput().
     */
    $.fn.resizeInput = function()
    {
        initDropdown(this);
        this.data("datixselect").resizeInput();
    };

    /**
     * Plugin to delegate to DatixSelect::setFreeText().
     */
    $.fn.setFreeText = function()
    {
        initDropdown(this);
        this.data("datixselect").setFreeText();
    };

    /**
     * Plugin to delegate to DatixSelect::isEmpty().
     */
    $.fn.isEmpty = function()
    {
        initDropdown(this);
        this.data("datixselect").isEmpty();
    };

    /**
     * Plugin to delegate to DatixSelect::addSelectedItems().
     */
    $.fn.addSelectedItems = function()
    {
        initDropdown(this);
        this.data("datixselect").addSelectedItems();
    };

    /**
     * Plugin to delegate to DatixSelect::deleteSelectedItems().
     */
    $.fn.deleteSelectedItems = function()
    {
        initDropdown(this);
        this.data("datixselect").deleteSelectedItems();
    };

    /**
     * Plugin to delegate to DatixSelect::setFirstSelected().
     */
    $.fn.setFirstSelected = function(item)
    {
        initDropdown(this);
        this.data("datixselect").setFirstSelected(item);
    };

    /**
     * Plugin to delegate to DatixSelect::resizeValuesList().
     */
    $.fn.resizeValuesList = function()
    {
        initDropdown(this);
        this.data("datixselect").resizeValuesList();
    };

})(jQuery);

function resizeInput($target, minWidth)
{
    if(typeof minWidth === 'undefined'){ minWidth = 200; }

    if ($target.data("noresize") || $target.val().length == 0)
    {
        return;
    }

    var $testDiv = jQuery("#testSize"),
        containerWidth;

    $testDiv.text($target.val());

    if ($testDiv.width() > minWidth)
    {
        if (!$target.closest(".field_input_div").length)
        {
            // the field is in a non-standard form section, so use the width of the direct ancestor element instead
            containerWidth = $target.parent().width() - 27;
        }
        else
        {
            if (formWidth == null)
            {
                // store this value in a global variable to prevent issues resizing fields on hidden panels (should also improve performance)
                formWidth = $target.closest(".field_input_div").width() - 30;
            }
            containerWidth = formWidth;
        }

        if (containerWidth > 0 && $testDiv.width() > containerWidth)
        {
            // don't exceed the width of the container element
            $target.width(containerWidth);
        }
        else
        {
            $target.width($testDiv.width() + 2);
        }
    }
    else
    {
        $target.width(minWidth);
    }
}

jQuery(document).ready(function()
{
    $selectInput = jQuery("input[class~=ff_select]");

    // add hidden div used in resizing inputs to fit current description
    jQuery("<div id=\"testSize\"></div>").css({
        position: "absolute",
        top: -9999,
        left: -9999,
        width: "auto",
        fontSize: $selectInput.css("fontSize"),
        fontFamily: $selectInput.css("fontFamily"),
        fontWeight: $selectInput.css("fontWeight"),
        letterSpacing: $selectInput.css("letterSpacing"),
        whiteSpace: "nowrap"
    }).appendTo("body");

    if (browser.isIE && browser.version <= 7)
    {
        // need to prevent code list closing when scrolling the browser in IE7
        jQuery(document).mousedown(function(event)
        {
            browserScrollbarClicked = event.srcElement.tagName == "HTML";
        });
    }

    jQuery(document)
        .mousedown(function(){documentMouseDown = true;})
        .bind("mouseup mouseleave", function(){documentMouseDown = false;});

    jQuery('.ff_select').each(function(){

        resizeInput(jQuery(this));
    });
});

/**
 * Prototype function for array objects to remove duplicates.
 */
Array.prototype.unique = function ()
{
    var r = new Array();
    o:for(var i = 0, n = this.length; i < n; i++)
    {
        for(var x = 0, y = r.length; x < y; x++)
        {
            if(r[x]==this[i])
            {
                continue o;
            }
        }
        r[r.length] = this[i];
    }
    return r;
};

/**
 * Prototype function for Strings to escape regex metacharacters.
 */
String.prototype.escapeRegex = function()
{
    return this.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
};

/**
 * Used when passing in hard-coded array of codes.
 */
var customCodes = new Array();

/**
 * Used to when resizing fields.
 */
var formWidth = null;
jQuery(window).resize(function()
{
    formWidth = null;
});

/**
 * Used to track whether the previous click event was on the scrollbar (e.g. outside of the document)
 */
var browserScrollbarClicked = false;

/**
 * Used to track whether the mouse button is being held down anywhere in the document.
 */
var documentMouseDown;

/**
 * Instantiates the DatixSelect object for this field, if not already created.
 *
 * @param jQuery field The dropdown field.
 */
function initDropdown(field)
{
    if (field.data("datixselect") == undefined)
    {
        if (field.hasClass("multi_select"))
        {
            field.datixmultiselect();
        }
        else
        {
            field.datixselect();
        }
    }
}

/**
 * Ensures values for all Datix Selects are properly set.
 *
 * Deleting a description/changing the value of a free text field and then immediately clicking save causes a problem
 * because the actual setting of the field value in done using the onblur event, which doesn't occur in this case.
 * So, this function is run when validating the form before submission so all Datix Select field values are updated correctly.
 */
function checkDatixSelects()
{
    jQuery("input[class~=ff_select]").each(function()
    {
        initDropdown(jQuery(this));
        if (jQuery(this).data("freeText"))
        {
            jQuery(this).setFreeText();
        }
        else if (jQuery(this).data("datixselect").input.val() == "")
        {
            jQuery(this).data("datixselect").field.val("");
        }
    });
}

/**
 * Breaks loops up into manageable chunks to avoid browser displaying a "script taking too long" message.
 *
 * http://www.picnet.com.au/blogs/Guido/post/2010/03/04/How-to-prevent-Stop-running-this-script-message-in-browsers.aspx
 *
 * @param Function op                  The function (closure) that you wish to run on every iteration.
 * @param int      yieldEveryIteration The amount of iterations per "chunk".
 */
RepeatingOperation = function(op, yieldEveryIteration)
{
    var count = 0;
    this.step = function(args)
    {
        if (++count >= yieldEveryIteration)
        {
            count = 0;
            this.timeout = setTimeout(function() { op(args); }, 1, []);
            return;
        }
        op(args);
    };
};