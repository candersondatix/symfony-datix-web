/**
 * Closure wrapper for rewriting window.open so that we can add a token to the url
 */
window.open = function (open) {
    return function (url, name, specs, replace) {
        // set name if missing here
        name = name || "_blank";

        url = appendTokenToUrl(url);

        if(typeof specs === 'undefined') {

            specs = '';
        }

        if(typeof replace === 'undefined') {

            replace = null;
        }

        return open(url, name, specs, replace);
    };
}(window.open);

function appendTokenToAjax() {

    // if there's no token, bail
    if (typeof token === 'undefined' || ! token.length) {

        return;
    }

    //  Append token to all jQuery ajax requests
    jQuery.ajaxPrefilter (function (options, originalOptions, jqXHR) {

        options.url = appendTokenToUrl(originalOptions.url);
    });

    /**
     * This seems to work, but I'm not sure if there will be problems with redefining a method for a third party class
     * like this, particularly when it comes to the scope of 'this'
     */
    if(typeof Rico != 'undefined') {

        Rico.Buffer.AjaxJSON.addMethods({

            initialize: function(url,options,ajaxOptions) {
                Object.extend(this, new Rico.Buffer.AjaxSQL());
                Object.extend(this, new Rico.Buffer.AjaxJSONMethods());
                this.dataSource=appendTokenToUrl(url);
                Object.extend(this.options, options || {});
                Object.extend(this.ajaxOptions, ajaxOptions || {});
            }
        });
    }
}

function appendTokenToUrl(url) {

    var voidPattern = /^javascript:/i

    // return if token already exists, url is just an anchor or if it contains javascript:void()
    if (/token=[a-z0-9]*$/i.test(url) || url[0] == '#' || voidPattern.test(url) || url.search(/\.htm/i) > -1 || typeof token === 'undefined') {

        return url;
    }

    var anchorParts = url.split ('#');
    var separator = anchorParts[0].split ('?').length == 2 ? '&' : '?';

    return anchorParts[0] + separator + 'token=' + token + (anchorParts.length > 1 ? '#'+anchorParts[1] : '');
}

function appendTokenToForms() {

    // if token already exists or there's no token, bail
    if (jQuery('input[name="token"]').length || typeof token === 'undefined' || ! token.length) {

        return;
    }

    // Append token to all forms
    jQuery('form').each(function() {

        jQuery(this).prepend('<input type="hidden" name="token" value="'+token+'"/>');
    });
}

function appendTokenToLinks() {

    if(typeof suppressClientSideCSRF === 'undefined') {

        suppressClientSideCSRF = false;
    }

    if (suppressClientSideCSRF) {
        // tokens are added server-side for performance reasons
        return;
    }

    // if there's no token, bail
    if (typeof token === 'undefined' || ! token.length) {
        return;
    }

    // Append token to all links (via click event bubbling)
    jQuery('body').on('click', 'a, area', function ( e ) {

        var $link = jQuery(this),
            href = $link.attr('href');

        // no href or href empty?
        if (typeof href == 'undefined' || href == '') {
            return;
        }

        // href is just an anchor link
        if (href.charAt(0) == '#') {
            return;
        }

        // token already present?
        if (/token=/.test(href)) {
            return;
        }

        // javascript href
        if (/^javascript:/i.test(href)) {
            return;
        }

        // we'll deal with the link
        e.preventDefault();
        e.stopPropagation();

        // preserve target
        var target = '_self';
        if (typeof $link.attr('target') != 'undefined') {

            target = $link.attr('target');
        }

        window.open(href, target);
    });
}

jQuery(function( $ ) {

    //  Append token to all jQuery ajax requests
    appendTokenToAjax();

    // Append token to all forms
    appendTokenToForms();

    // Append token to all <a href> links
    // (this query catches A and AREA tags while filtering javascript: links)
    appendTokenToLinks();
});