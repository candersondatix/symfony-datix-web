function FloatingWindow(id, numKey)
{
    this.oBgPanel = null;
    this.oDivWindow = null;

    this.Draggable = null;

    this.id = id;

    this.numKey = numKey; //key in popup global array

    this.buttons = new Array(); //array of buttons
    this.maxWidth = null; //in case we want to resize this popup manually

    this.codelist = false;
    /**
     * Action to trigger before floating window is destroyed
     * @constructor
     */
    this.OnBeforeCloseAction = function() {};

    /**
     * Action to trigger after flaoting window is destroyed
     * @constructor
     */
    this.OnCloseAction = function() {};

    this.CreateWindow();
}

FloatingWindow.prototype.CreateWindow = function()
{
   /* if (this.id)
    {
        // close controls with the same flag
        this.CloseFloatingControl();
    } */

    if(!$('floating_window_back_panel'))
    {
        this.oBgPanel = document.createElement("div");
        this.oBgPanel.id = "floating_window_back_panel";
        setClass(this.oBgPanel, "PopupBackgroundBlock");
        if (isIE6())
        {
            // in IE6 the background div doesn't stretch across entire screen (despite width: 100%), so...
            var viewportSize = getViewPortSize();
            this.oBgPanel.style.width = viewportSize[0];
            this.oBgPanel.style.height = jQuery(document).height();
        }
        if (!browser.isIE || browser.isIE && browser.version > 6) { this.oBgPanel.style.position = "fixed"; this.oBgPanel.style.zIndex = "100"; }
        document.body.appendChild(this.oBgPanel);
    }
    else
    {
        this.oBgPanel = $('floating_window_back_panel');
        this.oBgPanel.style.visibility = 'visible';
    }

    // If a floating winbdow with this id already exists, then just use that instead of adding a new one, cos why the
    // hell would you want to duplicate something that you can't even interact with?!?
    if($("floating_window_encasement_"+this.id)) {

        this.oDivWindow = $("floating_window_encasement_"+this.id);
    }
    // If it's a new window, then make a new one
    else {

        // Add to the count for active popups
        ActivePopupDivs++

        this.oDivWindow = document.createElement("div");
        this.oDivWindow.id = "floating_window_encasement_"+this.id;
        this.oDivWindow.style.display = "none";
        this.oDivWindow.style.align = "center";
        if (!browser.isIE || browser.isIE && browser.version > 6) { this.oDivWindow.style.position = "absolute"; }
        document.body.appendChild(this.oDivWindow);

        this.oDivWindow.innerHTML =

        '<div id="floating_window_'+this.id+'" name="floating_window_'+this.id+'" class="floating_window">'

        + '<input type="hidden" id="floating_window_numkey_'+this.id+'" value="'+parseInt(this.numKey)+'">'

        + '<div id="floating_window_title_'+this.id+'" class="floating_window_title" onmousedown="var div = GetFloatingDiv(\''+this.id+'\');div.createDraggable()" onmouseup="var div = GetFloatingDiv(\''+this.id+'\');div.destroyDraggable()">'

        + '<div id="floating_title_'+this.id+'" style="font-size:0.8em;color:white;padding-right:20px;cursor:default"></div><span id="closeControl_'+this.id+'" style="font-size:0.8em;cursor:pointer;color:white;position:absolute;right:2px;top:1px" onClick="var div = GetFloatingDiv(\''+this.id+'\');div.CloseFloatingControl();hideLoadPopup();enableAllButtons();destroyQtips();">[x]</span>'

        + '</div>'

        + '<div id="floating_window_container_'+this.id+'" class="floating_window_container">'

        + '<div id="floating_window_contents_'+this.id+'" class="floating_window_contents">'
        + '</div>'

        + '<div id="floating_window_buttons_'+this.id+'" class="floating_window_buttons">'
        + '</div>'

        + '</div>'

        + '</div>';

        this.oDivWindow.style.left = "0px";
        this.oDivWindow.style.top = "0px";
        this.oDivWindow.style.right = "0px";
        this.oDivWindow.style.bottom = "0px";

        this.oDivWindow.style.visibility = "visible";
    }

    this.PushBackOtherWindows();
    this.currentWindow = true;
}

/*FloatingWindow.prototype.findPos(obj) {
    var curleft = curtop = 0;
    if (obj.offsetParent) {
        curleft = obj.offsetLeft
        curtop = obj.offsetTop
        while (obj = obj.offsetParent) {
            curleft += obj.offsetLeft
            curtop += obj.offsetTop
        }
    }
    return [curleft,curtop];
}

FloatingWindow.prototype.getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return new Array(scrOfX, scrOfY);
}         */

FloatingWindow.prototype.PushBackOtherWindows = function()
{
    for(var i=0; i<PopupDivs.length; i++)
    {
        if(PopupDivs[i])
        {
            if(PopupDivs[i].currentWindow = true)
            {
                this.previousWindow = i;
            }

            if (!browser.isIE || browser.isIE && browser.version > 6) { PopupDivs[i].oDivWindow.style.zIndex = "0"; }
        }
    }

    if (!browser.isIE || browser.isIE && browser.version > 6) { this.oDivWindow.style.zIndex = "200"; }
}

FloatingWindow.prototype.setButtons = function(buttons)
{
    this.buttons = buttons;

    var buttonHTML = '';

    for(var i=0; i<buttons.length; i++)
    {
        var clickHandler = (buttons[i].onclick) ? ' onClick="' + buttons[i].onclick + '"' : '';
        buttonHTML += '<input id="button_'+i+'" name="button_'+i+'" tabindex="'+(1003+i)+'" type="button" value="'+buttons[i].value + '"' + clickHandler + ' />';
    }

    this.setHTMLSection('floating_window_buttons', buttonHTML);
}

FloatingWindow.prototype.CloseFloatingControl = function()
{
    this.OnBeforeCloseAction();

    if($("floating_window_encasement_"+this.id))
    {
        var divToDelete = $("floating_window_encasement_"+this.id);
        
        // make sure any open dropdown lists are closed
        jQuery("#floating_window_encasement_"+this.id).find(".ff_select,.multi_select").each(function()
        {
            if (jQuery(this).data("datixselect") != undefined)
            {
                jQuery(this).data("datixselect")._closeList();      
            }
        });
        
        this.oDivWindow.parentNode.removeChild(this.oDivWindow);
    }

    ActivePopupDivs--;
    // need to remove the element from the PopupDivs array rather than just set it to false so that it isn't iterated over in the document.mouselevel event (see end of this file)
    PopupDivs.splice(this.numKey, 1);
    this.hideElements();

    if(ActivePopupDivs == 0 && this.oBgPanel)
    {
        this.oBgPanel.style.visibility = 'hidden';
    }
    else if(this.previousWindow != null)
    {
        if (!browser.isIE || browser.isIE && browser.version > 6) {
            // bring previous window back up above background.
            PopupDivs[this.previousWindow].oDivWindow.style.zIndex = "200";
        }

        PopupDivs[this.previousWindow].currentWindow = true;
        PopupDivs[this.previousWindow].hideElements();
    }

    if(this.codelist)
    {
        windowopen = false; //global that prevents other code windows being opened.
    }

    this.OnCloseAction();
}

FloatingWindow.prototype.setMaxWidth = function()
{
    if(this.maxWidth != null)
    {
        var buttonwidth = 0;

        for(var i=0; i<this.buttons.length; i++)
        {
            buttonwidth += $('button_'+i).offsetWidth; //set text wrapper for popups to be width of the buttons to avoid gaps.
        }

        if(buttonwidth < 300)
        {
            buttonwidth = 300; //min width for popups
        }

        $('floating_window_contents_'+this.id).style.width = buttonwidth+'px';
    }
}

FloatingWindow.prototype.display = function()
{
    this.oDivWindow.style.display = "block";

    this.setMaxWidth();

    this.fixPosition();

    if($('searchtext'))
    {
        $('searchtext').focus();
    }
    else if($('button_0'))
    {
        $('button_0').focus();
    }
}

FloatingWindow.prototype.getHTMLSection = function(section)
{
    return $(section+'_'+this.id).innerHTML;
}

FloatingWindow.prototype.setHTMLSection = function(section, HTML)
{
    if($(section+'_'+this.id))
    {
        $(section+'_'+this.id).innerHTML = HTML;
    }
}

FloatingWindow.prototype.setContents = function(HTML)
{
    this.setHTMLSection('floating_window_contents', HTML);
}
FloatingWindow.prototype.setTitle = function(HTML)
{
    this.setHTMLSection('floating_title', HTML);
}

FloatingWindow.prototype.setWidth = function(sWidth)
{
    if(sWidth != '')
    {
        $('floating_window_'+this.id).style.width = sWidth;
    }
}

FloatingWindow.prototype.fixPosition = function(elePopup)
{
    var viewportSize,
        viewportXY,
        ctrlHeight,
        ctrlWidth,
        currentPos,
        Top,
        Left;

    viewportSize = getViewPortSize();
    viewportXY = Position.page($('floating_window_'+this.id));
    ctrlHeight = $('floating_window_'+this.id).offsetHeight;
    ctrlWidth = $('floating_window_'+this.id).offsetWidth;
    currentPos = getElementPosition($('floating_window_'+this.id));
    Top = currentPos[1] - viewportXY[1] + (viewportSize[1]/2 - ctrlHeight/2);
    Left = currentPos[0] - viewportXY[0] + (viewportSize[0]/2 - ctrlWidth/2);

    if (isIE6())
    {
        Top = Top / 2;
    }

    $('floating_window_'+this.id).style.top =  Top + "px";
    $('floating_window_'+this.id).style.left = Left + "px";

    this.hideElements();
}

FloatingWindow.prototype.getDocumentHeight = function()
{
    var docHeight;
    if (typeof document.height != 'undefined') {
        docHeight = document.height;
    }
    else if (document.body && typeof document.body.scrollHeight !=
    'undefined') {
        docHeight = document.body.scrollHeight;
    }
    return docHeight;
};

// checks whether an element is a child of the floating div.
FloatingWindow.prototype.InFloatingDiv = function(obj)
{
    if(obj.parentNode.id == this.oDivWindow.id)
    {
        return true;
    }
    else if(obj.parentNode == document.body)
    {
        return false;
    }
    else
    {
        return this.InFloatingDiv(obj.parentNode);
    }

}

FloatingWindow.prototype.hideElements = function()
{
    if (!browser.isIE || browser.isIE && browser.version > 6) {
        return;
    }

    var tags = new Array("applet", "iframe", "select");

    if (document.getElementById('floating_window_'+this.id) == null) {
        FloatingWindow.__created = false;
    }
    else {
        FloatingWindow.__created = true;
        var pos = getElementPosition($('floating_window_'+this.id));
        FloatingWindow.__position = {
            left: pos[0],
            top: pos[1],
            right: pos[0] + $('floating_window_'+this.id).offsetWidth,
            bottom: pos[1] + $('floating_window_'+this.id).offsetHeight
        }
    }

    //THE FOLLOWING CODE IS PRETTY FRAGILE - MAKE SURE TO TEST ALL APPROPRIATE
    //FUNCTIONALITY IF CHANGES ARE MADE.

    for (var i = 0; i < tags.length; i++)
    {
        var objects = document.getElementsByTagName(tags[i]);
        var curEle = null;
        for (var j = 0; j < objects.length; j++)
        {
            curEle = objects[j];
            if (!this.InFloatingDiv(curEle))
            {
                curEle.__visible = Element.visible(curEle);

                if(curEle.__visible) {
                    var pos = getElementPosition(curEle);
                    curEle.__position = {
                        left: pos[0],
                        top: pos[1],
                        right: pos[0] + curEle.offsetWidth,
                        bottom: pos[1] + curEle.offsetHeight
                    }
                }

                if (curEle.__visible)  //This element is not marked as "display:none", and so has a position
                {
                    if (!FloatingWindow.__created ||
                        (curEle.__position.left > FloatingWindow.__position.right) ||
                        (curEle.__position.right < FloatingWindow.__position.left) ||
                        (curEle.__position.top > FloatingWindow.__position.bottom) ||
                        (curEle.__position.bottom < FloatingWindow.__position.top))
                    {
                        //Either the window has been closed, or the current element is outside of the
                        //borders of the window.

                        MakeVisible(curEle);

                        try {
                            if (curEle.multiple) {
                                // also show add and delete buttons
                                MakeVisible($('img_add_' + curEle.id));
                                MakeVisible($('img_del_' + curEle.id));
                                checkMultiWidth(curEle);
                            }
                        } catch(e) {}
                    }
                    else
                    {
                        //The floating window overlaps at least part of the current element - we need to hide it.

                        MakeInvisible(curEle);

                        try {
                            if (curEle.multiple) {
                                // also hide add and delete buttons
                                MakeInvisible($('img_add_' + curEle.id));
                                MakeInvisible($('img_del_' + curEle.id));
                            }
                        } catch(e) {}
                    }
                }
            }
        }
    }//END OF FRAGILE CODE
}

FloatingWindow.prototype.createDraggable = function()
{
    this.Draggable = new Draggable('floating_window_'+this.id, {starteffect:'', endeffect:'', onEnd:DragEnd});
    this.Draggable.numKey = this.numKey;
}

FloatingWindow.prototype.destroyDraggable = function()
{
    if (this.Draggable != null)
    {
        this.Draggable.destroy();
    }    
}

FloatingWindow.prototype.hideCloseControl = function()
{
    $('closeControl_'+this.id).hide();
}

function DragEnd(d, e)
{
    var Div = GetFloatingDiv(d.element.id.replace('floating_window_', ''));

    if (Div != undefined)
    {
        Div.hideElements();   
    }

}

function MakeInvisible(obj)
{
    obj.style.visibility = 'hidden';
}

function MakeVisible(obj)
{
    obj.style.visibility = 'visible';
}

/**
* Remove drag event when mouseout of document so floating window doesn't get stuck to cursor.
*/
jQuery(document).ready(function()
{
    jQuery(document).mouseleave(function()
    {
        if (PopupDivs.length > 0)
        {
            for (var i = 0; i < PopupDivs.length; i++)
            {
                PopupDivs[i].destroyDraggable();        
            }    
        }
        ContactSelectionCtrl.destroyDraggable();
        EquipmentSelectionCtrl.destroyDraggable();
        MedicationSelectionCtrl.destroyDraggable();
        OrganisationSelectionCtrl.destroyDraggable();
    });
});
