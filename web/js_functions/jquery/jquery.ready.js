jQuery(function( $ ){

    $('.toggle-trigger').on('click', function() {

        var $trigger = $(this),
            $icon = $trigger.find('img'),
            $group = $trigger.closest('.toggle-group'),
            $target = $group.find('.toggle-target').first(),
            sectionTitle = $trigger.closest('.section_div').attr('id');

        if( ! $trigger.data('render')) {

            $target.toggle();
        }
        else if($target.data('rendered')) {

            $trigger.data('render', null);
        }

        if($target.is(':visible')) {

            $icon.attr('src','Images/collapse.gif');
        }
        else {

            $icon.attr('src','Images/expand.gif');
        }

        /**
         * Returns true if the trigger has data=render or false if it doesn't
         * Used to stop renderFields() from firing if the section is already rendered, though this is also handled in
         * renderFields() as it does a check to see if the data tag 'rendered' is set
         *
         * This doesn't function to stop onClick working as onClick will trigger before this anyway.
         */
        return ($trigger.data('render') === true);
    });

    /**
     * Initialise the spellchecker on the page for any textareas which require it
     */
    initialiseSpellchecker();

    /**
     * Initialise Calendars
     */
    initialiseCalendars();

    if(globals.deviceDetect.isTablet && Modernizr.inputtypes.date && globals.FormType == 'Search') {

        $('input[type="date"]').atpromptable();
    }
    else if(globals.deviceDetect.isTablet && ! Modernizr.inputtypes.date) {

        $('.hasDatepicker').on('focus', function() {

            var $target = $(this);

            $target.datepicker('widget').position({

                my: 'left top',
                at: 'left bottom',
                of: $target
            });
        });
    }
});