/**
 * jQuery Datix Tree
 * Widget for handling hierarchical structures in the DOM
 *
 * Depends
 *	jquery.ui.core.js
 */
;(function($, undefined) {
	$.widget("datix.tree", {
		options: {
			listItemMargin: 48
		},
		
		_create: function() {
			this.redraw();
		},
		
		// TODO This is implementation specific and doesn't really belong here
		_createNode: function(name, id) {
			return $('<li id="location-' + id + '">' + 
						'<div class="node">' +
							'<span class="name">' + name + '</span>' +
							'<div class="controls" style="visibility: hidden">' + 
								'<a class="edit" href="#">Edit</a><a class="delete" href="#">Delete</a>' +
							'</div>' + 
						'</div>' +
					'</li>');
		},
		
		/**
		 * Re-calculates the width for all subtrees, to ensure all sibling nodes float side-by-side, instead of wrapping at the window's edge
		 *
		 * @return {Void}
		 */
		redraw: function() {
			var subtrees = $(this.element.find('ul').get().reverse());
			var listItemMargin = this.options.listItemMargin; // TODO This is implementation-specific and doesn't belong here
			var count = subtrees.length;

			subtrees.each(function(i, element) {
				var width = 0;
				var tree  = this;

				$(this).children('li').each(function() {
					width += $(this).outerWidth(true) + 1; // Add one to mitigate rounding errors

					if ((i + 1) == count) { // only top-level nodes get the margin
						width += listItemMargin;
					}
				});

				$(this).css({
					'width' : width
				});
			});
		},
		
		/**
		 * Appends a new node to a given node's subtree
		 *
		 * @param {String} name Name for the node
		 * @param {Number} id Id for the node
		 * @param {Number||NULL} parentId The id of the parent node to append the new node to. If null, the node will be appended to the top-level of the tree
		 * @return {Void}
		 */
		appendTo: function(name, id, parentId) {
			var newNode = this._createNode(name, id);
			
			if (!parentId) {
				if (this.element.find('ul:first').length > 0) {
					this.element.find('ul:first').append(newNode);
				} else {
					newNode.appendTo(this.element).wrap('<ul />');
				}
			} else {
				var parent = $('#location-' + parentId);
				var className = (parent.find('li').length < 1) ? 'only-child' : 'last-child';
				
				newNode.addClass(className);
				
				if (parent.find('ul').length < 1) {
					newNode.appendTo(parent).wrap('<ul />');
				} else {
					parent.children('ul').children('li').removeClass('only-child last-child');
					parent.children('ul').append(newNode);
				}
			}
			
			this.redraw();
		},
		
		/**
		 * Not implemented
		 */
		prependTo: function(name, id, parentId) {},
		
		/**
		 * Not implemented
		 */
		insertBefore: function(name, id, siblingId) {},
		
		/**
		 * Not implemented
		 */
		insertAfter: function(name, id, siblingId) {},
		
		/**
		 * Remove a node from the the tree
		 *
		 * @param {Number} id The id of the node to remove
		 * @return {Void}
		 */
		remove: function(id) {
			var node = $('#location-' + id);
			var parent = node.parent('ul');
			
			if (node.siblings() < 1) {
				parent.remove();
			} else {
				node.remove();
			}
			
			// reset class names
			if (parent.children('li').length == 1) {
				parent.children('li').removeClass('first-child last-child').addClass('only-child');
			} else {
				parent.children('li').removeClass('first-child last-child only-child');
				
				parent.children('li:first').addClass('first-child');
				parent.children('li:last').addClass('last-child');
			}
			
			this.redraw();
		},
		
		/**
		 * Gets all nodes at a particular depth in the tree
		 *
		 * @param {Number} depth The depth of the subtree to get nodes from
		 * @return {jQuery} jQuery object containing all nodes found at depth
		 */
		getNodesAtDepth: function(depth) {
			return this.element.find('li').filter(function() {
				return $(this).parentsUntil(this.element, 'ul').length == depth;
			});
		},
		
		/**
		 * Gets the depth of a given node
		 *
		 * @param {Number} Id of node
		 * @return {Number} depth of node
		 */
		getDepthOfNode: function(nodeId) {
			return $('#location-' + nodeId).parentsUntil(this.element, 'ul').length;
		}
	});
})(jQuery);