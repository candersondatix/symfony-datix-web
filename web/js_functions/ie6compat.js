if (isIE6())
{
    var menuFixed = false;      // placeholder to ensure menu only resizes once

    window.attachEvent("onload", function()
    {
        var formArray = $$('div.new_mainform_menu');
        if (formArray.length > 0)
        {
            // call the resize function once the page has loaded
            resizeContainer(formArray[0]);
            
            formArray[0].attachEvent("onresize", function()
            {
                // recall the function everytime the form resizes
                resizeContainer(formArray[0]);
            });            
        }
    });
    
    function resizeContainer(thisForm)
    {        
        var dimensions = thisForm.getDimensions();
        var newWidth = dimensions.width;
        var newHeight = dimensions.height;
        
        var mainSectionArray = $$('div.main_section_border');
        if (mainSectionArray.length > 0)
        { 
            var menuArray = $$('div.menu_container');
            if (menuArray.length > 0)
            {
                var menuWidth = menuArray[0].getWidth();
                
                // since the menu width is defined as a percentage, we need to fix the width
                // so the menu isn't resized with the containing div.
                if (!menuFixed)
                { 
                    menuArray[0].setStyle({width: menuWidth});
                    menuFixed = true;
                }
                
                // take menu width into account when resizing containing div
                newWidth += menuWidth;
            }
            
            // resize the containing div's width if the combined width of its contents is greater
            var mainWidth = mainSectionArray[0].getWidth();
            if (newWidth > mainWidth)
            { 
                newWidth += 20;     // allow for padding
                mainSectionArray[0].setStyle({width: newWidth});
            }
            else
            {
                // fix containing div width to prevent display issues when window resized after page loads
                // (border width needs to be subtracted from mainWidth) 
                mainSectionArray[0].setStyle({width: mainWidth - 2});
            }
            
            // take title height into account when resizing containing div
            var titleArray = $$('div.page_title_background');
            if (titleArray.length > 0)
            {
                var titleHeight = titleArray[0].getHeight();
                newHeight += titleHeight;      
            }
            
            // resize the containing div's height if the combined height of its contents is greater/less
            var mainHeight = mainSectionArray[0].getHeight();
            if (newHeight > mainHeight)
            {
                newHeight += 10;    // allow for padding
                mainSectionArray[0].setStyle({height: newHeight});    
            }
            else if (newHeight < mainHeight)
            {
                // shrink the containing div to avoid excess whitespace below the form
                var menuHeight = menuArray[0].getHeight();
                if (newHeight < menuHeight)
                {
                    // resize containing div to menu height
                    mainSectionArray[0].setStyle({height: menuHeight + 50});
                }
                else
                {
                    // resize containing div to form height
                    mainSectionArray[0].setStyle({height: newHeight + 10});
                }    
            }
            else
            {
                // fix containing div height to prevent display issues when window resized after page loads
                mainSectionArray[0].setStyle({height: mainHeight});
            }      
        }
        // the form div is initially hidden (/IE6.css) for ie6 only before the containing div is resized
        thisForm.setStyle({visibility: 'visible'});    
    }
}