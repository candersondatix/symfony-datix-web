var LocationHierarchy = (function($) {
	var _element,
		_activeTier,
		_view;
	
	function saveLocation(e) {
		var name,
			parentId,
			id,
			invalid = false,
			parent,
			errorMsg,
			data,
			isNew;
			
		e.preventDefault();
		
		name  = jQuery('#name').val();
		id    = jQuery('#recordid').val();
		isNew = (id == '') ? true : false;
		
		if (jQuery('#parent-id').val() != '') {
			parentId = jQuery('#parent-id').val().match(/\d+/)[0];
		} else {
			parentId = null;
		}
		
		if (name.match(/^\s*$/)) {
			invalid = true;
			errorMsg = 'Name is a mandatory field';
		}
		
		// check if there are any locations that exist with the same name and parent
		if (parentId) {
			parent = _view.columnview('findNode', parentId);
			
			$.each(parent.children, function(i, child) {
				if (child.loc_name.toLowerCase() == name.toLowerCase()) {
					invalid = true;
					errorMsg = 'Location cannot have the same name and parent as another location';
					
					return false;
				}
			});
		}
		
		if (invalid) {
			if (jQuery('#name').next('p.msg').length < 1) {
				jQuery('#name').after('<p class="msg">' + errorMsg + '</p>');
			} else {
				jQuery('#name').next('p.msg').html(errorMsg);
			}
			
			return false;
		}
		
		// change the value of the parent-id form field to a form the server will understand
		jQuery('#parent-id').val(parentId);
		data = jQuery('#node-form form').serialize();

        jQuery.post(window.scripturl + '?action=saveasmlocation', data, function(data) {
			if (data.success) {
				id = data.id
			}
			
			if (isNew) {
				_view.columnview('appendTo', name.replace('<','&lt;'), id, (parentId  ? 'location-' + parentId : null), true);
			} else {
				jQuery('#location-' + id).find('.desc').html(name.replace('<','&lt;'));
			}

            // scroll list to the item 
            jQuery('#location-' + id).parent().scrollTop (jQuery('#location-' + id).position().top);
            

			changeStatus(id,true);
		}, 'json');
		
		closeLocationForm();
		
		return false;
	}


	/**
	 * Changes nodes and his descendants active status
	 *
	 * @param {Number} Id of node
	 * @param {Bool} node or descendant
	 */
	function changeStatus(id, first) {
		node = _view.columnview('findNode', id);

		if (jQuery('#active').val() == 'N') {
			jQuery('#location-' + id).addClass('loc_active_inactive');
			if (first) {
				node.loc_active = "N";
			} else {
				node.loc_active_inherit = "N";
			}
		} else {
			if (node.loc_active != "N" || first) {
				jQuery('#location-' + id).removeClass('loc_active_inactive');
			}
			if (first) {
				node.loc_active = "Y";
			} else {
				node.loc_active_inherit = "";
			}
		}
		
		jQuery('#location-'+id).data('loc_active_inherit',node.loc_active_inherit);
		
		if (jQuery('#active').val() == 'N' || (jQuery('#active').val() == 'Y' && node.loc_active != "N")) {
			$.each(node.children, function(i, child) {
				changeStatus(child.recordid, false);
			});
		}
	}
	
	
	
	function closeLocationForm(e) {
		var formWidth = jQuery('#node-form').width();
        
		jQuery('#node-form').animate({
			'right' : -formWidth
		});
		
		_view.columnview('enable');

		return false;
	}
	
	function showLocationForm() {
		var distance;
		
		jQuery('#node-form').animate({
			'right' : 0
		});

        jQuery ('#name').focus();
		
		_view.columnview('disable');
	}
	
	function newLocation(e) {
		_activeTier = $(e.target).parent().index();

		var parentNode = _element.find('.column:eq(' + (_activeTier - 1) + ') li.active');
		
		jQuery('#location-tier').val(_activeTier);

        // clear form values
        jQuery('#node-form').find('input, textarea').val('');

        // re-set the token value
        jQuery('input[name=token]').val(token);

        if (_activeTier > 0) {
		    jQuery('#parent').val(parentNode.find('.desc').text());
		    jQuery('#parent-id').val(parentNode.attr('id'));
        }
        
		showLocationForm();
		
		return false;
	}
	
	function editLocation(e) {
		var node = $(e.target).parent().parent();
		var name = node.find('.desc').text();
		var id   = node.attr('id').match(/\d+/)[0];
		var parentNode;
		
		e.stopPropagation();
		
		_activeTier = node.parentsUntil(_element, '.column').index();
		
		parentNode = jQuery('.column:eq(' + (_activeTier - 1) + ')', _element).find('.active'); // TODO This should probably be pulled from the server as well
		
		jQuery('#name').val(name);

        if (_activeTier > 0) {
		    jQuery('#parent').val(parentNode.find('.desc').text());
        }
        
		jQuery('#recordid').val(id);
		node.addClass('active');
		
		if(node.data('loc_active_inherit') == 'N') {
			jQuery('#active').attr('disabled','disabled');
			jQuery('#info_active').show();
		} else {
			jQuery('#active').removeAttr('disabled','disabled');
			jQuery('#info_active').hide();
		}
		
		jQuery.get(window.scripturl + '?action=getlocation&id=' + id, function(data) {
			var options = '';
			
			jQuery('#address').val(data.loc_address);
			jQuery('#active').val(data.loc_active);
			
			$.each(data.loc_types, function(i, value) {
				var selected = (value.cod_code == data.loc_type) ? 'selected' : '';
				options += '<option value="' + value.cod_code + '" ' + selected +'>' + value.cod_descr + '</option>';
			});
			
			jQuery('#location-type').html(options);
			
			showLocationForm();
		}, 'json');

		return false;
	}
	
	function deleteLocation(e) {
		var node,
			id;
		
		if (!window.confirm("Are you sure you want to delete this location?")) {
			return false;
		}
		
		node = $(e.target).parent().parent();
		id   = node.attr('id').match(/\d+/)[0];

        jQuery.post(window.scripturl + '?action=deletelocation', {
			"id" : id
		}, function(data) {
			if (data.success == true) {
				_view.columnview('remove', id);
			}
		}, 'json');
	}
	
	function showTier(e) {
		var target = $(e.target),
			activeTier;
		
		// check if node form is being displayed
		if (parseInt(jQuery('#node-form').css('right')) == 0) {
			return;
		}
		
		activeTier = target.parentsUntil(_element, '.column').index() + 1;
		jQuery('.tiers li:eq(' + activeTier + ')').find('a').show();
		jQuery('.tiers li:gt(' + activeTier + ')').find('a').hide();
	}
	
	function addTier(e) {
		var newTier = jQuery('<li><span title="Double-click to edit">New tier</span><a href="#" class="new-node" style="display: none">Add</a></li>'),
			lastTier = jQuery('.tiers li:last'),
			left = lastTier.length > 0 ? parseInt(lastTier.css('left')) + lastTier.outerWidth() : 0;

		newTier.appendTo('.tiers', _element).css('left', left).find('span').dblclick();

        // scroll to the right
        var viewportDiv = jQuery('.viewport').first();
        viewportDiv.scrollLeft( left );
	}
	
	function editTier(e) {
		var tier = $(e.target).parent(),
			originalValue = tier.find('span').text();
		
		tier.html('<input type="text" value="' + originalValue + '"><button class="save">Save</button><button class="cancel">Cancel</button>');
		tier.data('originalValue', originalValue);
		tier.find('input').focus().select();
	}
	
	function saveTier(e) {
		var tier = $(e.target).parent(),
			name = tier.find('input').val(),
			depth = tier.index();
		
		e.preventDefault();
		
		if (name.match(/^\s*$/)) {
			tier.find('input').qtip({
				content: "You must give the tier a name",
				show: 'dblclick',
				position: {
					corner: {
						target: 'topMiddle',
						tooltip: 'bottomMiddle'
					}
				},
				style: {
					tip: 'bottomMiddle',
					name: 'dark'
				}
			}).dblclick();
			
			tier.find('input').val('');
			
			return;
		}
		
		if (tier.find('input').data('qtip')) {
			tier.find('input').tqip('destroy');
		}
		
		if (name == tier.data('originalValue')) {
			restoreTier(tier);
			return;
		}

        jQuery.post(window.scripturl + '?action=addtier', {
			"name"  : name,
			"depth" : depth
		}, function() {
			tier.data('originalValue', name);
			tier.data('isSaved', true);
			restoreTier(tier);
		});
	}
	
	function restoreTier(tier) {
		if (tier.find('input').data('qtip')) {
			tier.find('input').qtip('destroy');
		}
		
		tier.html('<span title="Double-click to edit">' + tier.data('originalValue') + '</span><a href="#" class="new-node" style="display: none">Add</a>');
	}
	
	return {
		init: function(domEl, data, selectedLocation) {
			$(document).ready(function() {
				_element = $(domEl);

				_activeTier = jQuery('.tiers .active', _element).index();

				jQuery('.tiers li').each(function(i, el) {
					$(el).css('left', i * $(el).outerWidth());
				});

				jQuery('.column .controls', _element).hide();

				jQuery('.tiers li:gt(0) a').hide();
				
				jQuery('.tiers li', _element).each(function() {
					$(this).data('isSaved', true);
				});

				// attach events
				_element.on('click', '.new-node', newLocation);
				_element.on('click', '.columns li', showTier);
				_element.on('click', '#node-form .cancel', closeLocationForm);
				_element.on('submit', '#node-form', saveLocation);
				_element.on('mouseenter', '.columns li', function(e) {
					jQuery('.controls').hide();
					$(e.target).find('.controls').show();
					e.stopPropagation();
				}).on('mouseleave', '.columns li', function(e) {
					$(e.target).find('.controls').hide();
					e.stopPropagation();
				});
				_element.on('click', '.columns .edit', editLocation);
				_element.on('click', '.columns .delete', deleteLocation);
				_element.on('click', '#navigation .back', function() {
					_view.columnview('prev');
				});
				jQuery('#new-tier').on('click', 'a', function(e) {
					e.preventDefault();
					addTier(e);
				});
				_element.on('dblclick', '.tiers li span', editTier);
				_element.on('click', '.tiers .save', saveTier);
				_element.on('click', '.tiers .cancel', function(e) {
					var tier = $(e.target).parent();
					
					if (tier.data('isSaved')) {
						restoreTier(tier);
					} else {
						tier.remove();
					}
				});
				
				// create column view plugin
				_view = jQuery('#locations').columnview({
					nodes: data,
					selectedLocation: selectedLocation
				});
				
				jQuery('#node-form').width(_element.width() - jQuery('.column:first').width());
			});
		}
	}
})(jQuery);