var LocationHierarchy = (function($) {
	var _context;
	var _data;
	var _popUp;
	var _treeWidget; // view used to manipulate DOM
	var _isEmpty = /^\s*$/;
	
	function _addTier() {
		var newLevel   = $('<li><span>New level</span></li>');
	    
		newLevel.appendTo('.tiers ul', _context).find('span').editable({
			editBy: 'dblclick', 
			onEdit: _editTier,
			submit: 'Save',
			cancel: 'Cancel',
			onSubmit: _saveTier,
			onCancel: function() {
				_context.find('div:not(#location-legend)').equalHeights();
				
				if (!$(this).data('isSaved')) {
					$(this).parent('li').remove();
				} else {
					var func = $.proxy(_restoreTier, this); // make sure the _restoreTier function has the correct context
					func();
				}
				
			}
		}).dblclick();
		
		return false;
	}
	
	function _editTier(content) {
		_context.find('div:not(#location-legend)').equalHeights();
		
		$(this).nextAll('a').remove(); // remove add location and delete tier links temporarily
		$(this).find('input').select();
		
		if (!$(this).data('editable.original')) {
			$(this).data('editable.original', $(this).find('input').val());
		}
	}
	
	function _restoreTier() {
		$(this).html($(this).data('editable.original'));
		$(this).after('<a href="#" class="new-node">Add value</a>');
		
		if (_treeWidget.tree('getNodesAtDepth', ($(this).parent().index() + 1)).length < 1) {
			$(this).next().after('<a href="#" class="delete-tier">Delete tier</a>');
		}
		
		if ($(this).data('qtip')) { // if qtip has previously been applied
            _destroyQtip($(this));
		}
	}
	
	function _saveTier(content) {
		var tier  = $(this);
		var name  = tier.html();
		var depth = tier.parent('li').index();

		if (content.current.match(_isEmpty)) {
			tier.qtip({ // use qtip for validation messages
				content: "You must give the tier a name",
				show: 'dblclick',
				position: {
					corner: {
						target: 'topMiddle',
						tooltip: 'bottomMiddle'
					}
				},
				style: {
					tip: 'bottomMiddle',
					name: 'dark'
				}
			}).dblclick();
			
			tier.find('input').val('');
			
			return;
		}
		
		jQuery.post(window.scripturl + '?action=addtier', {
			"name"  : name,
			"depth" : depth
		}, function() {
			tier.data('editable.original', name);
			tier.data('isSaved', true);
		});
		
		if (tier.data('qtip')) { // if qtip has previously been applied
            _destroyQtip(tier);
		}
		
		tier.attr('title', 'Double-click to edit');
		tier.after('<a href="#" class="new-node">Add value</a>');
		
		if (_treeWidget.tree('getNodesAtDepth', ($(this).parent().index() + 1)).length < 1) {
			$(this).next().after('<a href="#" class="delete-tier">Delete tier</a>');
		}
	}
	
	function _deleteTier() {
		if (!window.confirm("Are you sure you want to delete this tier?")) {
			return false;
		}
		
		var tier  = $(this).parent();
		var depth = tier.index();
		
		jQuery.post(window.scripturl + '?action=deletetier', {
			"depth": depth
		}, function(data) {
			if (data.success) {
				tier.remove();
			}
		}, 'json');
	}
	
	function _newLocation() {
		var level = $(this).parents('li').index(); // level goes from top to bottom
		
		if (level > 0 && _treeWidget.tree('getNodesAtDepth', level).length < 1) {
			alert('You must add a location to the tier above before adding locations to this tier');
			return false;
		}
		
		_popUp = AddNewFloatingDiv('window');
		_popUp.setTitle('Add value');
		_popUp.setButtons([
			{
				"value" : 'Add'
			}
		]);


		jQuery.get(window.scripturl + '?action=newlocation', function(data) {
			_popUp.setContents(data);

			if (level > 0) {
				_populateParentSelector(level);
			} else {
				$('#parent', '#new-node').parent().hide();
			}

			_popUp.display();

			jQuery('#name').focus();
		});
		
		return false;
	}
	
	function _saveLocation() {
		var name     = $('#name').val();
		var level    = $('#level').val();
		var parentId = ($('#parent').is(':hidden')) ? null : $('#parent').val().match(/\d+/)[0]; // if the parent selector exists, strip "location-" and just get numerical id
		var invalid  = false;
		var errorMsg = '';

		if (name.match(_isEmpty)) {
			invalid = true;
			errorMsg = 'Name is a mandatory field';
		}
		
		// check if there are any locations that exist with the same name and parent
		if (parentId) {
			var parent = _findNode(parentId);
			var error  = false;
			
			$.each(parent.children, function(i, child) {
				if (child.loc_name.toLowerCase() == name.toLowerCase()) {
					invalid = true;
					errorMsg = 'Location cannot have the same name and parent as another location';
					
					return false;
				}
			});
		}
		
		if (invalid) {
			if ($('#name').next('p.msg').length < 1) {
				$('#name').after('<p class="msg">' + errorMsg + '</p>');
			} else {
				$('#name').next('p.msg').html(errorMsg);
			}
			
			return false;
		}
		
		jQuery.post(window.scripturl + '?action=savelocation', {
			"name" : name,
			"parentId" : parentId
		}, function(data) {
			if (data.success == true) {
				_insertNode(name, data.id, parentId);
			}
			
			_popUp.CloseFloatingControl();
		}, 'json');
			
        return false;
	}
	
	function _insertNode(name, id, parentId) {
		var parent, depth;
		
		if ($('.tree p', _context).length > 0) {
			$('.tree p', _context).remove();
		}
		
		_treeWidget.tree('appendTo', name.replace('<','&lt;'), id, parentId);
		
		if (parentId) {
			parent = _findNode(parentId);
			
			parent.children.push({
				'recordid' : id,
		    	'loc_name': name,
		    	'children': []
			});
		} else {
			_data.push({
				'recordid' : id,
			    'loc_name': name,
			    'children': []
			});
		}
		
		depth = _treeWidget.tree('getDepthOfNode', id);
		$('#location-tiers', _context).find('li').eq(depth - 1).find('a.delete-tier').remove();
	}
	
	function _editLocation(event) {
		var name = $(this).parents('.controls').prev();

        // Save the original Location for comparison purposes on _updateLocation
        name.parent().parent().data('original', name.html());
		
		if (!name.data('editable.options')) { // has not previously been set
			name.editable({
				editBy: 'focus',
				onEdit: function() {
					var tree  = $(this).parents('.tree');
					var width = $(this).data('width');
					var pos   = tree.scrollLeft();
					
					$(this).find('input').width(width).select();
					_treeWidget.tree('redraw');
					tree.scrollLeft(pos);
				},
				onSubmit: _updateLocation
			});
		}
		
		name.data('width', name.width()).focus();
		
		return false;
	}
	
	function _deleteLocation(event) {
		if (!window.confirm("Are you sure you want to delete this location?")) {
			return false;
		}
		
		var node  = $(this).parentsUntil('ul', 'li');
		var id    = node.attr('id').match(/\d+/)[0];
		var depth = _treeWidget.tree('getDepthOfNode', id);
		
		jQuery.post(window.scripturl + '?action=deletelocation', {
			"id" : id
		}, function(data) {
			if (data.success == true) {
				_deleteNode(id);
				_treeWidget.tree('remove', id);
				
				if (_treeWidget.tree('getNodesAtDepth', depth).length < 1) {
					$('#location-tiers', _context).find('li').eq(depth - 1).append('<a href="#" class="delete-tier">Delete tier</a>');
				}
			}
		}, 'json');
		
		return false;
	}
	
	function _updateLocation(content) {
		var node     = $(this).closest('li');
		var parentId = (node.parents('li').length > 0) ? node.parents('li').attr('id').match(/\d+/)[0] : null;
		var errorMsg = '';
		var nodeId   = node.attr('id').match(/\d+/)[0];

        if (node.data('original') == content.current) {
            if ($(this).data('qtip')) {
                _destroyQtip($(this));
            }

            return;
        }
		
		if (content.current.match(_isEmpty)) {
			errorMsg = "The location name cannot be blank";
		}

        if (content.current.length > 50) {
            errorMsg = "The name must not contain more than 50 characters.";
        }
		
		if (parentId) {
			var parent = _findNode(parentId);
			var error = false;
			
			$.each(parent.children, function(i, child) {
				if (child.loc_name.toLowerCase() == content.current.toLowerCase()) {
					error = true;
					return false;
				}
			});
			
			if (error) {
				errorMsg = "The location cannot have the same name and parent as another location";
			}
		}
		
		if (errorMsg) {
            if ($(this).data('qtip')) {
                _destroyQtip($(this));
            }

            $(this).qtip({ // use qtip for validation messages
                content: errorMsg,
                show: 'focus',
                hide: 'blur',
                position: {
                    corner: {
                        target: 'topMiddle',
                        tooltip: 'bottomMiddle'
                    }
                },
                style: {
                    tip: 'bottomMiddle',
                    name: 'dark'
                }
            }).focus();
			
			return;
		}

		jQuery.post(window.scripturl + '?action=editlocation', {
			"name" : $(this).text(),
			"id"   : nodeId
		});
		
		_findNode(nodeId).loc_name = $(this).text();
		
		_treeWidget.tree('redraw');
		
		if ($(this).data('qtip')) {
            _destroyQtip($(this));
		}
	}
	
	function _populateParentSelector(lvl) {
		var nodes = $('.tree li', _context).filter(function() {
			return ($(this).parentsUntil('.tree', 'ul').length == lvl);
		});
		var options = '';
		
		nodes.each(function() {
			var fqn = '';
			
			$(this).parentsUntil('.tree', 'li').each(function() {
				fqn += $(this).find('> .node .name').text().replace('<','&lt;') + '/';
			});
			
			options += '<option value="' + $(this).attr('id') + '">' + fqn + $(this).find('> .node .name').text().replace('<','&lt;') + '</option>';
		});
		
		$('#parent', '#new-node').html(options);
	}
	
	function _findNode(lookupId) {
		var node;
        
        (function _descendTree(data) {
            $.each(data, function(index, value) {
                if (value.recordid == lookupId) {
                    node = value;
					return false;
                }
                
				if (value.children) {
					_descendTree(value.children);
				} else {
					return;
				}
                
            });
        })(_data);
        
        return node;
	}
	
	function _deleteNode(lookupId) {
		(function _descendTree(data) {
            $.each(data, function(index, value) {
                if (value.recordid == lookupId) {
                    data.splice(index, 1);
					return false;
                }
                
				if (value.children.length > 0) {
					_descendTree(value.children);
				} else {
					return;
				}
                
            });
        })(_data);
	}

    function _destroyQtip(node) {
        node.qtip('destroy');
    }
	
	return {
		init: function(el, data) {
			_context = $(el);
			_data	 = data;
			
			_treeWidget = $('#location-nodes').tree({
				listItemMargin: 48
			});
			
			_context.find('div:not(#location-legend)').equalHeights();
			
			$('.new-tier', _context).click(_addTier);
			$('.new-node', _context).live('click', _newLocation);
			$(_context).on('click', '.delete-tier', _deleteTier);
			
			$(document).on('submit', '#new-node', _saveLocation);
			$(document).on('click', '#floating_window_buttons_window input', _saveLocation);
			
			$('li span', '#location-tiers').editable({
				editBy: 'dblclick',
				submit: 'Save',
				cancel: 'Cancel',
				onEdit: _editTier,
				onSubmit: _saveTier,
				onCancel: _restoreTier
			});
			
			// controls for individual nodes
			$('.tree li .controls').css('visibility', 'hidden');
			
			_context.on('mouseenter', '.node', function(e) {
				$(this).find('.controls').css('visibility', 'visible');
				e.stopPropagation();
			});
			
			_context.on('mouseleave', '.node', function(e) {
				$(this).find('.controls').css('visibility', 'hidden');
				e.stopPropagation();
			});
			
			_context.on('click', '.node .controls .delete', _deleteLocation);
			_context.on('click', '.node .controls .edit', _editLocation);
		}
	}
})(jQuery);