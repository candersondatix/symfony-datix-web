var OrganisationSelectionCtrl = {};

OrganisationSelectionCtrl.create = function(config) {

    this.config = config;

    if (document.getElementById("organisationlist_window"))
    {
        this.destroy();
    }

    var popupWindow = document.createElement("div");
    popupWindow.id = "organisationlist_window";
    popupWindow.style.visibility = "hidden";
    document.body.appendChild(popupWindow);

    var posArray = getElementPosition($('check_btn_' + config.fieldname));

    var ctrlPosX = posArray[0];
    var ctrlPosY = posArray[1];

    popupWindow.style.left = ctrlPosX + 20 + "px";
    popupWindow.style.top = ctrlPosY + "px";

    OrganisationSelectionCtrl.MatchExistingOrganisations(popupWindow);

};

OrganisationSelectionCtrl.destroy = function() {

    var ele = document.getElementById("organisationlist_window");
    ele.parentNode.removeChild(ele);
    OrganisationSelectionCtrl.hideElements();
};

OrganisationSelectionCtrl.fixControlSize = function(elePopup) {

    // Default width for select element is 400px if greater, increase control width
    var diff = 0;

    $('organisationlist_window').style.width = 408 + diff + 'px';
    $('organisationlist_title').style.width = 400 + diff + 'px';
    $('organisationlist_container').style.width = 400 + diff + 'px';
    jQuery("#organisationlist_container").css("overflow-x", "auto");

    var viewportSize = getViewPortSize();
    var viewportXY = Position.page(elePopup);

    var ctrlHeight = elePopup.offsetHeight;
    var ctrlWidth = elePopup.offsetWidth;

    var currentPos = getElementPosition($('organisationlist_window'));

    if (viewportXY[1] + ctrlHeight > viewportSize[1])
    {
        elePopup.style.top = currentPos[1] - (viewportXY[1] + ctrlHeight - viewportSize[1]) + "px";
    }

    if (viewportXY[0] + ctrlWidth > viewportSize[0])
    {
        elePopup.style.left = currentPos[0] - viewportXY[0] - ctrlWidth + viewportSize[0] + "px";
    }
};

OrganisationSelectionCtrl.hideElements = function(d) {

    var pos = null;
    if (!browser.isIE || browser.isIE && browser.version > 6)
    {
        return;
    }

    var tags = new Array("applet", "iframe", "select");

    if (document.getElementById('organisationlist_window') == null)
    {
        OrganisationSelectionCtrl.__created = false;
    }
    else
    {
        OrganisationSelectionCtrl.__created = true;
        pos = getElementPosition($('organisationlist_window'));
        OrganisationSelectionCtrl.__position = {
            left: pos[0],
            top: pos[1],
            right: pos[0] + $('organisationlist_window').offsetWidth,
            bottom: pos[1] + $('organisationlist_window').offsetHeight
        }
    }

    for (var i = 0; i < tags.length; i++)
    {
        var objects = document.getElementsByTagName(tags[i]);
        var curEle = null;

        for (var j = 0; j < objects.length; j++)
        {
            curEle = objects[j];

            if (curEle.id != 'code_list_select')
            {
                if (typeof curEle.__position == 'undefined')
                {
                    pos = getElementPosition(curEle);
                    curEle.__position = {
                        left: pos[0],
                        top: pos[1],
                        right: pos[0] + curEle.offsetWidth,
                        bottom: pos[1] + curEle.offsetHeight
                    }
                }

                if (typeof curEle.__visible == 'undefined')
                {
                    curEle.__visible = Element.visible(curEle);
                }

                if (!OrganisationSelectionCtrl.__created ||
                    (curEle.__position.left > OrganisationSelectionCtrl.__position.right) ||
                    (curEle.__position.right < OrganisationSelectionCtrl.__position.left) ||
                    (curEle.__position.top > OrganisationSelectionCtrl.__position.bottom) ||
                    (curEle.__position.bottom < OrganisationSelectionCtrl.__position.top))
                {
                    if (curEle.__visible)
                    {
                        Element.show(curEle);

                        try
                        {
                            if (curEle.multiple)
                            {
                                // also hide add and delete buttons
                                Element.show($('img_add_' + curEle.id));
                                Element.show($('img_del_' + curEle.id));
                            }
                        }
                        catch(e) {}
                    }
                }
                else
                {
                    try
                    {
                        if (curEle.multiple)
                        {
                            // also hide add and delete buttons
                            Element.hide($('img_add_' + curEle.id));
                            Element.hide($('img_del_' + curEle.id));
                        }
                    }
                    catch(e) {}

                    Element.hide(curEle);
                }
            }
        }
    }
};

var overrideArray = new Array();

OrganisationSelectionCtrl.MatchExistingOrganisations = function(popupWindow) {

    var postbodystring = "";
    var fields = this.config.fieldlist.split(",");
    var value;
    var postData = {};

    for (var i=0; i < fields.length; i++)
    {
        if ($(this.config[fields[i]]).tagName.toLowerCase() == 'select')
        {
            var opts = $(this.config[fields[i]]).options;
            var temp = [];

            for (var x=0, y=opts.length; x < y; x++)
            {
                temp.push(opts[x].value);
            }

            postData[fields[i]] = temp;
        }
        else
        {
            postData[fields[i]] = jQuery("#"+this.config[fields[i]]).val();
        }
    }

    var extraFieldsOnForm = [];

    jQuery("#organisation_section_div_"+this.config.suffix).find("[id^=UDF]").each(function() {

        if (jQuery(this).attr("id").match(/^UDF_\w_\d+_\d+_\d+$/) != null)
        {
            extraFieldsOnForm.push(jQuery(this).attr("id"));
        }
    });

    postData['module'] = this.config.module;
    postData['parentmodule'] = this.config.parentmodule;
    postData['fieldname'] = this.config.fieldname;
    postData['suffix'] = this.config.suffix;
    postData['fieldlist'] = this.config.fieldlist;
    postData['extraFieldsOnForm'] = extraFieldsOnForm;

    jQuery.ajax({
        url: scripturl + '?action=httprequest&type=organisationlist',
        type: "POST",
        dataType: "text",
        data: postData,
        global: false,
        success: function(response) {

            document.getElementById('organisationlist_window').innerHTML = response;
            popupWindow.style.visibility = "visible";

            document.getElementById('organisationlist_container').style.maxHeight = '250px';
            document.getElementById('organisationlist_container').style.overflowY = 'scroll';

            OrganisationSelectionCtrl.fixControlSize(popupWindow);

            jQuery('#organisationlist_window').draggable({
                handle: '#organisationlist_title'
            });

            OrganisationSelectionCtrl.hideElements();

            if (jQuery('#organisationlist_container').innerWidth() < jQuery('#organisationlist_container').attr("scrollWidth"))
            {
                if (!jQuery.browser.msie || parseInt(jQuery.browser.version) < 8)
                {
                    // ensure horizontal scrollbars don't obscure the bottom of the listing
                    jQuery('#organisationlist_container').html(jQuery('#organisationlist_container').html() + "<br />");
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

            alert("Error retrieving codes.");
        }
    });
};

OrganisationSelectionCtrl.createDraggable = function() {

    this.Draggable = new Draggable('organisationlist_window', {starteffect:'', endeffect:'', onEnd:DragEnd});
};

OrganisationSelectionCtrl.destroyDraggable = function() {
    if (this.Draggable != null)
    {
        this.Draggable.destroy();
    }
};