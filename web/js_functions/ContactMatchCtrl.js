var ContactSelectionCtrl = {};

ContactSelectionCtrl.create = function(config)
{

	this.config = config;

	if (document.getElementById("contactlist_window")) {
		this.destroy();
	}

	var popupWindow = document.createElement("div");
	popupWindow.id = "contactlist_window";
	popupWindow.style.visibility = "hidden";
	document.body.appendChild(popupWindow);

	var posArray = getElementPosition($('check_btn_' + config.fieldname));

	var ctrlPosX = posArray[0];
	var ctrlPosY = posArray[1];

	popupWindow.style.left = ctrlPosX + 20 + "px";
	popupWindow.style.top = ctrlPosY + "px";

    ContactSelectionCtrl.MatchExistingContacts(popupWindow);

};

ContactSelectionCtrl.destroy = function()
{
	var ele = document.getElementById("contactlist_window");
	ele.parentNode.removeChild(ele);
	ContactSelectionCtrl.hideElements();
};

ContactSelectionCtrl.fixControlSize = function(elePopup)
{
	// Default width for select element is 400px if greater, increase control width
	var diff = 0;
/*	if ($('code_list_select').offsetWidth > 400 && $('code_list_select').options.length)
	{
		diff = $('code_list_select').offsetWidth - 400;
	}        */

	$('contactlist_window').style.width = 408 + diff + 'px';
	$('contactlist_title').style.width = 400 + diff + 'px';
    $('contactlist_container').style.width = 400 + diff + 'px';
	jQuery("#contactlist_container").css("overflow-x", "auto");
	//$('codelist_search').style.width = 400 + diff + 'px';
	//$('searchtext').style.width = 400 + diff - 6 + 'px';
	//$('codelist_select').style.width = 400 + diff + 'px';
	//$('code_list_select').style.width = 400 + diff + 'px';

	var viewportSize = getViewPortSize();
	var viewportXY = Position.page(elePopup);

    var ctrlHeight = elePopup.offsetHeight;
    var ctrlWidth = elePopup.offsetWidth;

	var currentPos = getElementPosition($('contactlist_window'));


	if (viewportXY[1] + ctrlHeight > viewportSize[1])
	{
		elePopup.style.top = currentPos[1] - (viewportXY[1] + ctrlHeight - viewportSize[1]) + "px";
	}

	if (viewportXY[0] + ctrlWidth > viewportSize[0])
	{
		elePopup.style.left = currentPos[0] - viewportXY[0] - ctrlWidth + viewportSize[0] + "px";
	}

};

ContactSelectionCtrl.hideElements = function(d) 
{
	if (!browser.isIE || browser.isIE && browser.version > 6) {
		return;
	}

	var tags = new Array("applet", "iframe", "select");

	if (document.getElementById('contactlist_window') == null) {
		ContactSelectionCtrl.__created = false;
	}
	else {
		ContactSelectionCtrl.__created = true;
		var pos = getElementPosition($('contactlist_window'));
		ContactSelectionCtrl.__position = {
			left: pos[0],
			top: pos[1],
			right: pos[0] + $('contactlist_window').offsetWidth,
			bottom: pos[1] + $('contactlist_window').offsetHeight
		}
	}

	for (var i = 0; i < tags.length; i++) {
		var objects = document.getElementsByTagName(tags[i]);
		var curEle = null;
		for (var j = 0; j < objects.length; j++) {
			curEle = objects[j];
			if (curEle.id != 'code_list_select') {
				if (typeof curEle.__position == 'undefined') {
					var pos = getElementPosition(curEle);
					curEle.__position = {
						left: pos[0],
						top: pos[1],
						right: pos[0] + curEle.offsetWidth,
						bottom: pos[1] + curEle.offsetHeight
					}
				}

				if (typeof curEle.__visible == 'undefined') {
					curEle.__visible = Element.visible(curEle);
				}

				if (!ContactSelectionCtrl.__created ||
					(curEle.__position.left > ContactSelectionCtrl.__position.right) ||
					(curEle.__position.right < ContactSelectionCtrl.__position.left) ||
					(curEle.__position.top > ContactSelectionCtrl.__position.bottom) ||
					(curEle.__position.bottom < ContactSelectionCtrl.__position.top)) {
					if (curEle.__visible) {
						Element.show(curEle);
						try {
							if (curEle.multiple) {
								// also hide add and delete buttons
								Element.show($('img_add_' + curEle.id));
								Element.show($('img_del_' + curEle.id));
							}
						} catch(e) {}
					}
				}
				else {
					try {
						if (curEle.multiple) {
							// also hide add and delete buttons
							Element.hide($('img_add_' + curEle.id));
							Element.hide($('img_del_' + curEle.id));
						}
					} catch(e) {}
					Element.hide(curEle);
				}
			}
		}
	}
};

var overrideArray = new Array();


ContactSelectionCtrl.MatchExistingContacts = function(popupWindow)
{

    var postbodystring = "";
    var fields = this.config.fieldlist.split(",");
    var value;
    var postData = {};

    for (var i=0; i < fields.length; i++)
    {
		if ($(this.config[fields[i]]).tagName.toLowerCase() == 'select')
		{
			var opts = $(this.config[fields[i]]).options;
			var temp = [];
            
			for (var x=0, y=opts.length; x < y; x++)
			{
				temp.push(opts[x].value);
			}
            
            postData[fields[i]] = temp;
		} 
		else
		{
            postData[fields[i]] = jQuery("#"+this.config[fields[i]]).val();
		}
    }

    var extraFieldsOnForm = [];
    jQuery("#contact_section_div_"+this.config.suffix).find("[id^=UDF]").each(function()
    {
        if (jQuery(this).attr("id").match(/^UDF_\w_\d+_\d+_\d+$/) != null)
        {
            extraFieldsOnForm.push(jQuery(this).attr("id"));        
        }  
    });
    
    postData['module'] = this.config.module;
    postData['parentmodule'] = this.config.parentmodule;
    postData['fieldname'] = this.config.fieldname;
    postData['suffix'] = this.config.suffix;
    postData['fieldlist'] = this.config.fieldlist;
    postData['extraFieldsOnForm'] = extraFieldsOnForm;
    
    jQuery.ajax(
    {
        url: scripturl + '?action=httprequest&type=contactlist',
        type: "POST",
        dataType: "text",
        data: postData,
        global: false,
        success: function(response)
        {
            document.getElementById('contactlist_window').innerHTML = response;
            popupWindow.style.visibility = "visible";

            document.getElementById('contactlist_container').style.maxHeight = '250px';

            ContactSelectionCtrl.fixControlSize(popupWindow);

            jQuery('#contactlist_window').draggable({
                handle: '#contactlist_title'
            });

            ContactSelectionCtrl.hideElements();
            
            if (jQuery('#contactlist_container').innerWidth() < jQuery('#contactlist_container').attr("scrollWidth"))
            {
                if (!jQuery.browser.msie || parseInt(jQuery.browser.version) < 8)
                {
                    // ensure horizontal scrollbars don't obscure the bottom of the listing
                    jQuery('#contactlist_container').html(jQuery('#contactlist_container').html() + "<br />");      
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error retrieving codes.");
        }
    });
};

ContactSelectionCtrl.createDraggable = function()
{
    this.Draggable = new Draggable('contactlist_window', {starteffect:'', endeffect:'', onEnd:DragEnd});
};

ContactSelectionCtrl.destroyDraggable = function()
{
    if (this.Draggable != null)
    {
        this.Draggable.destroy();   
    }
};