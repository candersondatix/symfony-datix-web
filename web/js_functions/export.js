/**
* Manages the exporting of files.
*
* @param string url    The page we're requesting for the file export
* @param string onFail The function to run if the export process has failed
* @param object vars   An object containing additional hidden inputs to submit with the form, where the key/value pairs are input names/values
*/
function exportFile(url, onFail, vars)
{
    // set deault value for onFail callback function
    onFail = onFail == null ? 'displayExportError("Error")' : onFail;

    // the request is submitted in an iframe to prevent redirection when not returning a file (i.e. on fail)
    jQuery("<iframe src=\"javascript:false;\" id=\"export_iframe\" name=\"export_iframe\" />")
        .css({
            "width":  "0",
            "height": "0",
            "border": "none"
        })
        .bind("load", function()
        {
            jQuery(this).remove();
        })
        .appendTo('body');

    // create form used to submit request for file export
    var form = jQuery('<form></form>')
        .attr({
            id:     'export_form',
            method: 'POST',
            target: 'export_iframe',
            action: url+'&token='+token
        })
        .append('<input type="hidden" id="export_token_value" name="export_token_value" />')
        .submit(function()
        {
            // generate the hidden token value used to check against the cookie which is set when the export completes
            var exportToken = new Date().getTime();
            jQuery('#export_token_value').val(exportToken);

            // check for the cookie every 0.5 seconds
            fileExportCheckTimer = window.setInterval(function()
            {
                var cookieValue = jQuery.cookie('fileExportToken');

                if (cookieValue != null && cookieValue.substr(0, exportToken.toString().length) == exportToken)
                {
                    hideLoadPopup();

                    // the cookie value is in the format timestamp|(success/fail)
                    if (cookieValue.split('|')[1] == 'fail')
                    {
                        eval(onFail);
                    }

                    window.clearInterval(fileExportCheckTimer);
                    jQuery.cookie('fileExportToken', null);
                    jQuery('#export_form').remove();
                }
            }, 500);
        });

    if (vars != null)
    {
        // add additional input values to form
        jQuery.each(vars, function(name, value)
        {
            form.append('<input type="hidden" name="'+name+'" value="'+value+'" />');
        })
    }

    form.appendTo('body').submit();
}

/**
* The default callback function for file export errors.
*
* @param string title   The title of the error popup dialogue
*/
function displayExportError(title)
{
    // retrieve export error set in session via ajax and display in popup dialogue
    jQuery.get('app.php?service=export&event=getExportError', function(data, status, xhr, dataType)
    {
        var exportError = AddNewFloatingDiv('export_error');
        try
        {
            // response returned in json format
            eval("data = " + data + ";");
            exportError.setContents(data['contents']);
            exportError.setTitle(data['title']);
        }
        catch (e)
        {
            exportError.setContents(data);
            exportError.setTitle(title);
        }
        exportError.display();
    });
}

/**
* Prompts user to set a value for the SIRS_TRUST global.
*
* Global must be set before CFSMS export proceeds.
*/
function setSirsTrustGlobal()
{
    var setDialogue = AddNewFloatingDiv('set_dialogue');
    var contents = '<div style="font-weight:bold; margin-bottom:5px;">Please enter your Trust number</div>' +
                   '<form><div style="margin-bottom:5px;"><input type="text" id="sirs_trust" name="sirs_trust" /></div>' +
                   '<input type="button" value="Ok" onclick="exportFile(scripturl+\'?service=export&event=cfsms\', \'setSirsTrustGlobal()\', {\'sirs_trust\' : jQuery(\'#sirs_trust\').val()});var div=GetFloatingDiv(\'set_dialogue\');div.CloseFloatingControl()" /></form>';
    setDialogue.setTitle('&nbsp;');
    setDialogue.setContents(contents);
    setDialogue.display();
}

/**
* Prompts user to enter details for ISD export and then runs the export.
*
* @param string title The dialogue box title.
* @param string agent The value entered for the ISD agent (when called from the ISD agent dialogue).
*/
function isdExport(title, agent)
{
    var url = scripturl + "?service=export&event=isd" + (agent != undefined ? "&agent="+agent : "");
    jQuery.get(url, function(data)
    {
        var isdDialogue = AddNewFloatingDiv("isd_dialogue");

        isdDialogue.setContents(SplitJSAndHTML(data));
        isdDialogue.setTitle(title);

        if (data.indexOf("isd_agent") == -1)
        {
            // we're displaying the standard export dialogue, rather than the ISD agent
            RunGlobalJavascript();

            var cssObj = {
                "min-width" : "50px",
                "width"     : "50px"
            };
            jQuery("#isd_month_title").css(cssObj);
            jQuery("#isd_year_title").css(cssObj);
        }

        isdDialogue.display();
    });
}

/**
* Checks that all the necessary ISD export values have been filled in.
*/
function isdFormValidated()
{
    if (jQuery("#isd_year").val() == "")
    {
        alert(txt['isd_validate_year']);
        return false;
    }
    else if (jQuery("#isd_edition").val() == "")
    {
        alert(txt['isd_validate_edition']);
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * Custom function for handling exporting/printing of FusionCharts.
 */
function FCExport(action, format, reportId, riskGradeTracker)
{
    var width  = '1280',
        height = '1024',
        url    = null;

    if (action == 'export')
    {
        displayLoadPopup();
    }

    var $printInput = jQuery('input[name="print"]');

    if($printInput.length)
    {
        $printInput.val('0');
    }

    var $exportInput = jQuery('input[name="export"]');

    if($exportInput.length)
    {
        $exportInput.val('0');
    }

    var $hiddenFrame = jQuery('#hiddenFCFrame');

    if ( ! $hiddenFrame.length)
    {
        $hiddenFrame = jQuery('<iframe src="javascript:false;" id="hiddenFCFrame" name="hiddenFCFrame" />')
        .css({
                'width'    : jQuery("#H").width() - 20,
                'height'   : jQuery("#H").height() + jQuery("#B").height() + jQuery("#F").height() - 20,
                'border'   : 'none',
                'position' : 'absolute',
                'overflow' : 'hidden',
                'left'     : 20,
                'top'      : 20,
                'z-index'  : -1
        })
        .appendTo('body');
    }

    if (reportId != undefined && reportId != '')
    {
        // exporting a packaged report, so we only need the report id to retrieve the data
        url = 'app.php?action=reportdesigner&designer_state=closed&export=1&recordid='+reportId+'&format='+format;
    }
    else if( riskGradeTracker )
    {
        url = 'app.php?action=riskTrackerReport&export=1&format='+format+'&recordid='+riskGradeTracker['recordid']+'&reporton='+riskGradeTracker['reporton']+'&startdate='+riskGradeTracker['startdate']+'&enddate='+riskGradeTracker['enddate'];
    }
    else
    {
        url = 'app.php?action=reportdesigner&designer_state=closed&export=1&format='+format;
    }

    if (globals['CSRF_PREVENTION'] == 'Y') {
        url += '&token='+token;
    }

    $hiddenFrame.attr('src', url);
}

function startFusionChartExport(format, reportId)
{
    if (reportId === undefined)
    {
        reportId = '';
    }
    
    var intId = setInterval(function()
    {
        var chartObj = FusionCharts(fusionChartId);

        if (chartObj.hasOwnProperty('hasRendered'))
        {
            if (chartObj.hasRendered() == true)
            {
                // TODO It looks like some kind of timing issue causes non-Flash charts to have no data when exporting to pdf (especially with c. 50+ rows)
                //      so the hacky fix for now is to delay this action by half a second, even though the report "hasRendered".  We should investigate this further
                //      and see if we can provide a more stable and sustainable fix.
                setTimeout(function(){ chartObj.exportChart({ exportFormat: format });},500);
                clearInterval(intId);
            }
        }
    }, 500);
}

function FC_Exported(exportObj)
{
    if (exportObj.statusCode == "1")
    {
        parent.finishFCExport(exportObj.fileName);
    }
}

/**
* Callback function used for cleanup after exporting from FusionCharts.
*/
function finishFCExport(filename)
{
    jQuery('iframe[id^=exportFrame]').remove();
    hideLoadPopup();
    SendTo(scripturl + '?service=export&event=exportChartPdf&file=' + filename);
}

//determines whether the user wants to export to excel or pdf and directs them appropriately.
function exportReport(type, reportId, widgetId)
{
    if (reportId === undefined)
    {
        reportId = '';
    }

    if (widgetId === undefined)
    {
        widgetId = '';
    }

    var outputFormat = jQuery("input[name=reportoutputformat]:checked").val();

    if (outputFormat == 'pdf')
    {
        if (type == 1 || type == 12) // crosstab or listing report
        {
            var orientation = jQuery("input[name=orientation]:checked").val(),
                papersize =  jQuery('#papersize').val();

            SendTo('app.php?action=httprequest&type=exportreport&format=pdf'
                +'&orientation='+orientation
                +'&papersize='+papersize
                +'&recordid='+reportId);
            GetFloatingDiv('exportoptions').CloseFloatingControl();
        }
        else
        {
            GetFloatingDiv('exportoptions').CloseFloatingControl();
            FCExport('export', 'pdf', (reportId ? reportId : ''));
        }
    }
    else if (outputFormat == 'excel')
    {
        GetFloatingDiv('exportoptions').CloseFloatingControl();
        SendTo('app.php?action=httprequest&type=exportreport&format=excel&recordid='+reportId);
    }
}

function exportRiskGradeTrackerReport(type, recordid, reporton, startdate, enddate)
{	
	if (jQuery("input[name=reportoutputformat]:checked").val() == 'pdf')
    {
		GetFloatingDiv('exportoptions').CloseFloatingControl();
		var riskGradeTrackerData = new Array();		
		riskGradeTrackerData['recordid'] = recordid;
		riskGradeTrackerData['reporton'] = reporton;
		riskGradeTrackerData['startdate'] = startdate;
		riskGradeTrackerData['enddate'] = enddate;
		
		FCExport('export', 'pdf', '', riskGradeTrackerData );
    }
	else if (jQuery("input[name=reportoutputformat]:checked").val() == 'excel')
    {
		GetFloatingDiv('exportoptions').CloseFloatingControl();
        SendTo('app.php?action=httprequest&type=riskTrackerReport&format=excel&recordid='+recordid+'&reporton='+reporton+'&startdate='+startdate+'&enddate='+enddate);
    }
}

function setFloatingWindowContentSize(id, fixPos) {

    if(typeof fixPos == 'undefined') {

        fixPos = false;
    }

    var printDialog = GetFloatingDiv(id),
        $floatingWindow = jQuery('#floating_window_' + id),
        $viewport = $floatingWindow.find('.viewport'),
        windowHeight = jQuery(window).height();

    if($floatingWindow.outerHeight() > windowHeight) {

        var viewportHeight = windowHeight - ($floatingWindow.outerHeight() - $viewport.outerHeight());

        $viewport.height(viewportHeight).css('overflow', 'auto');

        var scrollbarWidth = $viewport.width() - $viewport.children().first().outerWidth(true);

        /**
         * Add the scrollbar width to the width of the options to ensure the content isn't being squished; add 10 to make IE play nicely
         */
        $viewport.width($viewport.width() + scrollbarWidth + 10);
    }
    else
    {
        $viewport.height('').width('').css('overflow', 'visible');
    }

    if(fixPos) {

        printDialog.fixPosition();
    }
}

/**
 * Function responsible for displaying the @prompt values in a popup.
 * Used in the "My reports" screen.
 *
 * @param {int} reportId The id of the packaged report.
 * @param module
 */
function displayAtPrompt(reportId, module)
{
    jQuery.get("app.php?action=getatpromptsection&rep_id="+reportId+"&context=ajax", function(data) {

        jQuery.get("app.php?action=httprequest&type=exporttopdfoptions", function(exportData) {

            var atPromptPopup = AddNewFloatingDiv("at_prompt");

            var contents = '<form id="at_prompt_form" method="POST" action="app.php?action=openinexcel"><span style="display: inline-block; vertical-align: top; margin: 0 10px 0 0;"><h3 style="margin: 0 0 10px;">@prompt options:</h3><div class="viewport">' +
                '<input type="hidden" name="recordid" value="'+reportId+'" />'+
                SplitJSAndHTML(data)+
                '</div></span>' +
                '<span style="display: inline-block; vertical-align: top;">' + SplitJSAndHTML(exportData) + '</span></form>';

            var buttons = [];

            buttons[0] = {
                'value'  : 'Continue',
                'onclick': 'jQuery(\'#at_prompt_form\').submit();GetFloatingDiv(\'at_prompt\').CloseFloatingControl();'
            };
            buttons[1] = {
                'value'  : 'Cancel',
                'onclick': 'GetFloatingDiv(\'at_prompt\').CloseFloatingControl();'
            };
            atPromptPopup.setContents(contents);
            atPromptPopup.setTitle("Export");
            atPromptPopup.setButtons(buttons);

            RunGlobalJavascript();
            initialiseCalendars();

            if (globals['CSRF_PREVENTION'] == 'Y') {
                appendTokenToForms();
            }

            atPromptPopup.display();

            setFloatingWindowContentSize('at_prompt', true);
        });
    });
}

function exportNoRenderMyReports(exportFormat, exportOrientation, exportPaperSize, reportId, module)
{
    window.open("app.php?action=openinexcel&reportoutputformat="+exportFormat+"&orientation="+exportOrientation+"&papersize="+exportPaperSize+"&recordid="+reportId+"&module="+module, '_self');
}
