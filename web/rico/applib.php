<?php
//
// This is where the database connection settings go.
// Logical place to put security checks.

require "plugins/php/dbClassPDO.php";

function CreateDbClass() {
  global $oDB;

  $oDB = new dbClass();  // from dbClass2

  // What dialect of SQL will we be speaking?
  $oDB->Dialect="MSSql";
}

function OpenDB() {
  global $oDB;
  global $Database, $ServerName, $UserName, $Password;
  CreateDbClass();

  // This is where the database connection is made
  // Uncomment the appropriate line for your database

  // Using MSSQL
    return $oDB->MSSqlLogon($ServerName, $Database, $UserName, $Password);
}

function OpenApp($title, $accessRights = "rw") {
  $_retval=false;
  if (!OpenDB()) {
    return $_retval;
  }
  if (!empty($title)) {
    AppHeader($GLOBALS['appName']."-".$title);
  }
  $GLOBALS['accessRights']=$accessRights;
  // CHECK APPLICATION SECURITY HERE  (in this example, "r" gives read-only access and "rw" gives read/write access)
  if (empty($GLOBALS['accessRights']) || !isset($GLOBALS['accessRights']) || substr($GLOBALS['accessRights'],0,1) != "r") {
    echo "<p class='error'>You do not have permission to access this application";
  }
  else {
    $_retval=true;
  }
  return $_retval;
}

function OpenTableEdit($tabname, $WhereClause = "", $uniqueId = '') {
  $obj= new TableEditClass();
  $obj->SetTableName($tabname, $uniqueId);
  if (!empty($WhereClause))
  {
    $obj->TableFilter = $WhereClause;
  }
  $obj->AltTableKeyWhereClause($AltTabIdx);
  $obj->Tables[0]->arColInfo[0]->IsPKey = true;
  if (!empty($WhereClause))
  {
    $obj->Tables[0]->arColInfo[2]->IsPKey = true;
  }

  $obj->options["XMLprovider"]="rico/ricoXMLquery.php";
  $obj->options["highlightElem"]="cursorRow";
//  $obj->options["highlightMethod"]="outline";
  $obj->convertCharSet=false;
  return $obj;
}

function OpenGridForm($title, $tabname, $WhereClause = "", $accessRights = "rw", $uniqueId = '') {
  $_retval=false;
  if (!OpenApp($title, $accessRights)) {
    return $_retval;
  }
  $GLOBALS['oForm']= OpenTableEdit($tabname, $WhereClause, $uniqueId);
  $CanModify=($GLOBALS['accessRights'] == "rw");
  $GLOBALS['oForm']->options["canAdd"]=$CanModify;
  $GLOBALS['oForm']->options["canEdit"]=$CanModify;
  $GLOBALS['oForm']->options["canDelete"]=$CanModify;
  session_set_cookie_params(60*60);
  $GLOBALS['sqltext']='.';
  return true;
}

function CloseApp() {
  global $oDB;
  if (is_object($oDB)) $oDB->dbClose();
  $oDB=NULL;
  $GLOBALS['oForm']=NULL;
}

function AppHeader($hdg) {
  echo "<h2 class='appHeader'>".str_replace("<dialect>",$GLOBALS['oDB']->Dialect,$hdg)."</h2>";
}
?>

